<?php
include_once "../../../Highchart.php";

$chart = new Highchart();

$chart->chart->renderTo = "container";
$chart->chart->type = "column";
$chart->title->text = "Stacked column chart";
$chart->xAxis->categories = array('Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas');
$chart->yAxis->min = 0;
$chart->yAxis->title->text = "Total fruit consumption";

//$chart->tooltip->formatter = new HighchartJsExpr("function() { return ''+ this.series.name +': '+ this.y +' ('+ Math.round(this.percentage) +'%)';}");

$chart->plotOptions->column->stacking = "percent";

$chart->series[] = array('name' => "John",
                         'data' => array(5, 3, 4, 7, 2));

$chart->series[] = array('name' => "Jane",
                         'data' => array(2, 2, 3, 2, 1));

$chart->series[] = array('name' => "Joe",
                         'data' => array(3, 4, 4, 2, 5));
?>

<html>
  <head>
    <title>Stacked percentage column</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
      foreach ($chart->getScripts() as $script) {
         echo '<script type="text/javascript" src="' . $script . '"></script>';
      }
    ?>
	<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script> 
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>
  </head>
  <body>
    <div id="container"></div>
	<canvas id="canvas" width="400px" height="600px"></canvas> 
    <script type="text/javascript">
    <?php
      echo $chart->render("chart1");
    ?>
	svg = $('#container').children().html();
	canvg(document.getElementById('canvas'), svg)
    
var canvas = document.getElementById("canvas");
var img = canvas.toDataURL("image/png");
    </script>
  </body>
</html>