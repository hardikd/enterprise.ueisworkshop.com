var Script = function () {

    $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        console.log('onNext');
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(tab);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');
       
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
        /*if(index==1){
            if($('#student').val()==''){
                $('#errmsg').html('Please select Student.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 0);')
                return false;
            }
            if($('#grade_id').val()==''){
                $('#errmsg').html('Please select Student.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 0);')
                return false;
            }
            if($('#dp2').val()==''){
                $('#errmsg').html('Please select Date of birth.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 0);')
                return false;
            }
            if($('#ethnic_code').val()==''){
                $('#errmsg').html('Please select Ethnic Code.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 0);')
                return false;
            }
           // 
        }
        if(index==2){
            console.log('2');
            if($('[name="dates_description_1"]').val()==''){
                
                 $('#errmsg').html('Please select Date of intial IEP team meeting.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 1);')
                
                return false;
            }
            if($('[name="dates_description_2"]').val()==''){
               $('#errmsg').html('Please select  Date of present meeting.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 1);')
                return false;
            }
            if($('[name="dates_description_3"]').val()==''){
                $('#errmsg').html('Please select Annual review tobe conducted by.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 1);')
                return false;
            }
            if($('[name="dates_description_4"]').val()==''){
                $('#errmsg').html('Please select Next three year review will be conducted by.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 1);')
                return false;
            }
            if($('[name="dates_description_5"]').val()==''){
                $('#errmsg').html('Please select Three year review or evaluation was conducted on.');
                
                $('#errorbtn').modal('show');
                $('#errorbtn').find('button').attr('onClick','$("#pills").bootstrapWizard("show", 1);')
                return false;
            }
        }*/
         
    }});

    $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        console.log('onNext');
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabClick: function(tab, navigation, index) {
        console.log('onTabClick');

    }, onTabShow: function(tab, navigation, index) {
        console.log('onTabShow2');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#tabsleft').find('.bar').css({width:$percent+'%'});

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
            $('#tabsleft').find('.pager .next').hide();
            $('#tabsleft').find('.pager .finish').show();
            $('#tabsleft').find('.pager .finish').removeClass('disabled');
        } else {
            $('#tabsleft').find('.pager .next').show();
            $('#tabsleft').find('.pager .finish').hide();
        }

    }});


    $('#tabsleft .finish').click(function() {
        alert('Finished!, Starting over!');
        $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
    });

}();