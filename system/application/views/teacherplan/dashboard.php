
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" style="overflow: hidden;"><!--<![endif]--><!-- BEGIN HEAD --><head>
   <meta charset="utf-8">
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport">
   <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
   <link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
   <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="css/style.css" rel="stylesheet">
   <link href="css/style-responsive.css" rel="stylesheet">
   <link href="css/style-purple.css" rel="stylesheet" id="style_color">
   <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
   <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen">
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid sidebar-closed">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll" style="overflow: hidden; outline: none;" tabindex="5000">
          <div id="sidebar" class="nav-collapse collapse" style="margin-left: -180px;">
           <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" style="margin-left: 0px;">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12" >
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-home"></i>&nbsp; Home
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="#">Home</a>
                          
                       </li>
                       
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <!--BEGIN METRO STATES-->
                
           
                    
                <div class="metro-nav">
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-green long-dash">
                        <a href="planning-manager" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-book"></i><br><br><br>
                                Planning Manager
                            </span>
                           
                        </a>
                    </div>
                    
                     <div class="metro-nav-block nav-block-blue long-dash">
                        <a href="attendance" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-calendar"></i><br><br><br>
                               Attendance Manager
                            </span>
                            
                        </a>
                    </div>
                    
                    
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-red long-dash">
                        <a href="implementation" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-pencil"></i><br><br><br>
                             Implementation Manager 
                            </span>
                            
                        </a>
                    </div>
                    
                      <div class="metro-nav-block nav-block-yellow long-dash">
                        <a href="classroom" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-group"></i><br><br><br>
                                Classroom Management Assistant
                            </span>
                            
                        </a>
                    </div>
                    
                          <div class="metro-nav-block nav-block-orange long-dash">
                        <a href="assessment" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-bar-chart"></i><br><br><br>
                               Assessment Manager
                            </span>
                            
                        </a>
                    </div>
                    
                    
                         
                    
                    
                        
                    
                    
                          <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="tools" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-wrench"></i><br><br><br>
                               Tools &amp; Resources 
                            </span>
                           
                        </a>
                    </div>
                </div>
              
                <div class="space10"></div>
                <!--END METRO STATES-->
            </div>
             </div>
           
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
      UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="js/jquery-1.8.3.min.js"></script>
   <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
   <script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="assets/chart-master/Chart.js"></script>

   <!--common script for all pages-->
   <script src="js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="js/easy-pie-chart.js"></script>
   <script src="js/sparkline-chart.js"></script>
   <script src="js/home-page-calender.js"></script>
   <script src="js/chartjs.js"></script>

   <!-- END JAVASCRIPTS -->   

<!-- END BODY -->
</div></body></html>