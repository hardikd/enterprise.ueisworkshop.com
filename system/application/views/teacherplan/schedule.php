<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
#button {
    background-color: #74B749;
    border: 0 none;
    color: #FFFFFF;
    cursor: default;
	border-radius: 15px;
    display: inline-block;
    padding: 5px 14px;
	float: right;
    margin-left: 5px;
	text-decoration: none;
	outline: 0 none;
	text-shadow: none !important;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
       <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu" >
              <li class="sub-menu">
                  <a class="" href="../index.html">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
                <!-- FIRST LEVEL MENU Planning Manager-->
            <li class="sub-menu" id="lesson">
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="../planning-manager/lesson-plan.html">Lesson Plan Creator</a></li>
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="third">
                    <li><a class="" href="../planning-manager/lesson-plan.html">Create Lesson Plan</a></li></ul>
                    
                     <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../planning-manager/create-plan.html">Add Plan</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../planning-manager/upload-plan.html">Upload Plan</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/edit-plan.html">Edit Lesson Plan</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/completed-plan.html">Retrieve Lesson Plan</a></li>
                </ul>
                
                   <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="../planning-manager/smart-goals.html">SMART Goals Creator</a></li>
                     
                      <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/create-goal.html">Create Goal</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/edit-goal.html">Edit Goal</a></li>
                </ul>
                
                <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="../planning-manager/calendar.html">Calendar Creator</a></li>
                             
                         <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/new-event.html">Create New Event</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/retrieve-event.html">Retrieve Event</a></li>
                </ul>
                
                </ul>
            </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu">
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Attendance Manager</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/instruction.html">Instruction</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/daily-attendance.html">Daily Attendance</a></li>
                   </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/take-roll.html">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/retrieve-roll.html">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="../attendance/new-student-enrollment.html">New Student Enrollment</a></li>
                   </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/modify-enrollment.html">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/enroll-student-enrollment.html">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../attendance/roster.html">Student Roster</a></li>
                   </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="../attendance/create-roster.html">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/edit-roster.html">Edit</a></li>
                </ul>  
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
					  <li><a class="" href="../attendance/data.html">Add-Map Data</a></li>
                   </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/add-parent-info.html">Add Parent Information</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/map-parent.html">Map Parent/Student Data</a></li>
                </ul>  
                
                
                <!-- SECOND LEVEL MENU -->
                
                      <li><a class="" href="../attendance/assessment.html">Assessment</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/daily-attendance.html">Daily Attendance</a></li>
                   </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/take-roll.html">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/retrieve-roll.html">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="../attendance/new-student-enrollment.html">New Student Enrollment</a></li>
                   </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/modify-enrollment.html">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/enroll-student-enrollment.html">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../attendance/roster.html">Student Roster</a></li>
                   </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="../attendance/create-roster.html">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/edit-roster.html">Edit</a></li>
                </ul>  
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
					  <li><a class="" href="../attendance/data.html">Add-Map Data</a></li>
                   </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/add-parent-info.html">Add Parent Information</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/map-parent.html">Map Parent/Student Data</a></li>
                </ul>  
                      
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu">
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Implementation Manager</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../implementation/grade-tracker.html">Grade Tracker</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-grade.html">Enter Grade</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-grade.html">Retrieve Grade</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       <li><a class="" href="../implementation/homework-tracker.html">Homework Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-homework-record.html">Enter Homework Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-homework-record.html">Retrieve Homework Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                        <li><a class="" href="../implementation/iep-tracker.html">IEP Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-iep-record.html">Enter IEP Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-iep-record.html">Retrieve IEP Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="../implementation/eld-tracker.html">ELD Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-eld-record.html">Enter ELD Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-eld-record.html">Retrieve ELD Record</a></li>
                </ul>  
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="../implementation/instructional-efficacy.html">Instructional Efficacy</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/observation.html">Lesson Observation Feedback</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/observation-full-report.html">Retrieve Full Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/observation-sectional-report.html">Retrieve Sectional Data Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/self-reflection.html">Self-Reflection</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/generate-report.html">Create/Edit Report</a></li></ul>
              
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/retrieve-report.html">Retrieve Report</a></li>
                </ul>
                
                
                  </ul>
              </li>
              
              
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
              <li class="sub-menu">
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Classroom Management</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                      <li><a class="" href="../classroom/progress-report.html">Student Progress Report</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-progress-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-progress-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-progress-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                          <li><a class="" href="../classroom/success-team.html">Student Success Team</a></li>
                           <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-success-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-success-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-success-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                              <li><a class="" href="../classroom/parent-teacher.html">Parent/Teacher Conference</a></li>
                              <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-parent-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-parent-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-parent-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="../classroom/teacher-student.html">Teacher/Student Conference</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-student-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-student-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-student-report.html">Retrieve</a></li>
                </ul>   
                
                  <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="../classroom/behavior-record.html">Behavior Running Record</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-behavior.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/edit-behavior.html">Edit</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-behavior.html">Upload</a></li>
                </ul>   
                
                </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
              <li class="sub-menu">
                  <a href="javascript:;" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Assesment Manager</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../assessment/diagnostic.html">Diagonstic Screener</a></li>
                      <li><a class="" href="../assessment/progress.html">Progress Monitoring</a></li>
                      <li><a class="" href="../assessment/benchmark.html">Benchmark Assesment</a></li>
 </ul>
              </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
              <li class="sub-menu">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Tools &amp; Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  	  <li><a class="" href="../tools/data-tracker.html">Data Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/data-planning-manager.html">Planning Manager</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/assessment-manager.html">Assessment Manager</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/growth.html">View a Classroom's Growth</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/class-standard.html">Class Performance Score by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/graphical-report.html">Class Performance Graphs by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-summary.html">Individual Student Performance Summary</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-standard.html">Individual Student Performance by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/progress-report.html">Student Progress Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/attendance-manager.html">Attendance Manager</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../tools/implementation-manager.html">Implementation Manager</a></li>
                </ul>   
                
                  <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/grade-tracker.html">Grade Tracker</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/homework-tracker.html">Homework Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/iep-tracker.html">IEP Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/eld-tracker.html">ELD Tracker</a></li>
                </ul>
                
                   <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/observation.html">Lesson Observations</a></li>
                </ul>

<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/viewer-observation.html">Observation by Viewer</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/grade-observation.html">Observation by Grade</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/count-observation.html">Observation Count</a></li>
                </ul>

  <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/pd-activity.html">Professional Development Activity</a></li>
                </ul>
                
                
                
                   <ul class="sub">
                     <li><a class="" href="../tools/classroom-management.html">Classroom Management</a></li>
                     
                </ul> 
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-progress-report.html">Student Progress Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/success-team.html">Student Success Team Meeting Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/parent-teacher.html">Parent/Teacher Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/teacher-student.html">Teacher/Student Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/behavior-record.html">Behavior Learning & Running Record Report</a></li>
                </ul>
                <!-- SECOND LEVEL MENU -->  
                      <li><a class="" href="../tools/development.html">Professional Development</a></li>
                         <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/pd-individual.html">Individual</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/pd-group.html">Group</a></li>
                </ul> 
                <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="../tools/bridge.html">Parent Bridge</a></li> 
                     <!-- THIRD LEVEL MENU -->
                      <ul class="sub">
                     <li><a class="" href="../tools/send-notification.html">Parent Notification</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/notification-records.html">Parent Notification Records</a></li>
                </ul>                  
                      <li><a class="" href="../tools/guide.html">User/Training Guide</a></li>
                      
                  </ul><BR><BR>
              </li>
             
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                     <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="../index.html">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          <a href="../planning-manager/">Planning Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="../planning-manager/calendar.html">Calendar Creator</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="../planning-manager/retrieve-event.html">Retrieve Event</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget green">
           <div class="widget-title">
                             <h4>Retrieve Event</h4>
                          
                       </div>
                         <div class="widget-body">
                            <div class="form-horizontal">
                                <div id="pills" class="custom-wizard-pills-green2">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     
                                     
                                     
           </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                           <h3 style="color:#000000;">STEP 1</h3>
                                   <form method="post" class="form-horizontal" action="teacherplan/schedule">
                                   <?php 
							 if(isset($selectdate)) { 
							 $selectdate=$selectdate;
							 } else { 
							 $selectdate= date('d-m-Y');
							 }
							?>
						<div class="control-group">
                          <label class="control-label">Select week</label>
                          <div class="controls">
						<input type="text" name="selectdate" size="16" class="m-ctrl-medium" id="selectdate" readonly value="<?php echo $selectdate;?>">
                                        
                          </div>
                                </div>
                                
                                
                                 <?php /*?>    <div class="control-group">
                              <label class="control-label">Select Title</label>
                                       <div class="controls">
                                       	 <?php if($getscheduleplans!=false)
										{
										$le=0;
										  $je=0;
										  
										  foreach($getscheduleplans as $getplan)
										  {
										  
										  ?>
                                       <input class="span12 chzn-select" value="" tabindex="1" style="width: 300px;">    	            																									                                 <?php }}?>
	   							</div>
                                       </div><?php */?>
                             <div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div>
                             <div class="space20"></div><div class="space20"></div>
                             
                             
                              <ul class="pager wizard">
                                <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><input  id="button" class="next red" type="submit" name="go" value="Next"></li>
                                       
                                     </ul>
                              </div>
                               
                                      <!-- BEGIN STEP 2-->
                                   <div class="tab-pane" id="pills-tab2">
                                      
                                   <h3 style="color:#000000">STEP 2</h3>
                                         <div class="control-group">
                                 
                                 		<table id="datetable" align="center" style="width:650px;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
    <tr>
  <td colspan="4" align="left" bgcolor="#657455" style="color:#FFF" >
  <b><img style="float:left;margin:3px;" src="<?php echo SITEURLM?>/images/task.jpg" height="20">&nbsp;&nbsp;&nbsp;&nbsp;Scheduler</b>&nbsp;&nbsp;&nbsp;&nbsp; <b>From: <?php echo $fromdate;?> To:<?php echo $todate;?></b>
  </td>
  </tr>
  <tr align="center" style="color:#333;">
  <td width="120px" bgcolor="#CCCCCC">Date</td>
  <td width="120px" bgcolor="#CCCCCC">Time</td>
  <td width="150px" bgcolor="#CCCCCC">Task</td>
  <td width="150px" bgcolor="#CCCCCC">Details</td>
  </tr>
	<?php if($getscheduleplans!=false)
	{
	$le=0;
	  $je=0;
	  
	  foreach($getscheduleplans as $getplan)
	  {
	  $le=$getplan['schedule_week_plan_id'];
		$lecomments=$getplan['comments'];
	  ?>
		<tr align="center">
	     <td width="120px"><?php echo $getplan['date']?></td>
		 <td width="120px">
		<?php 
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		 if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		echo $start2." to ".$end2 ?>
		</td><td width="150px"><?php echo $getplan['task']?></td>
	<td width="150px" >
	<a class="title" style="float:left;padding-left:80px;" href="#" title="Details|<?php if($lecomments!=''){ echo $lecomments; } else { echo "No Details Found. "; }  ?> ">View</a>
	<?php if($getplan['task_id']==3 || $getplan['task_id']==2 || $getplan['task_id']==1 )
	{ 
	if($allstatus[$getplan['schedule_week_plan_id']]==0 &&  $getplan['task_id']==1)
	{?><form name="form<?php echo $getplan['schedule_week_plan_id'];?>" action="teacherplan/index" method="post">
	 <input type="hidden" name="date" id="date" value="<?php echo $getplan['date'];?>">
	 <input type="submit" name="complete" id="complete" value="Pending" style="display:none;">
	 <a href="javascript:;"onclick="javascript:document.form<?php echo $getplan['schedule_week_plan_id'];?>.submit();">Pending</a>
	 </form>
	
	<?php
	}else if($allstatus[$getplan['schedule_week_plan_id']]==0)
	{
	?>
	/<a <?php if($getplan['task_id']==3) { ?> href="teacherreport/goalplan" <?php } ?> <?php if($getplan['task_id']==2) { ?> href="teacherreport/observationplan" <?php } ?>  ><input type="button" name="complete" id="complete" value="Pending"></a>
	<?php } } ?>
	</td>
	</tr><tr align="center">
	 </tr> <?php $le=$getplan['schedule_week_plan_id'];
		$lecomments=$getplan['comments'];  } }  else {?>
	<tr>
	<td colspan="4" align="center">
	No Task Found
	</td>
	</tr>
	
	<?php } ?>
	
		</table>	       
                                         
                        <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget green">
                         <div class="widget-title">
                             <h4>Event Title</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">Event Content Appears Here</h3>
                                 
                                  </div>
                                
                                </div> 
                                
                                
                    </div>
                                         
                                         
                     <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                    
                                         
                                         
                                           <center><a href="#" role="button" class="btn btn-large btn-success"><i class="icon-print icon-white"></i> Print</button></a>  <button class="btn btn-large btn-success"><i class="icon-envelope"></i> Send to Colleague</button> <button class="btn btn-large btn-success"><i class="icon-arrow-right"></i> Done</button></center>
                                            
                                           
                                  </div>
                                   <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;">Next</a></li>
                                     </ul>

                                   </div>
                                  </div>
                                    
                              </div>
                           </div>
                           </form>
        </div>
     </div>
                     <!-- END BLANK PAGE PORTLET-->
</div>
          </div>
            
            <!-- END PAGE CONTENT-->
   </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
<script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
<script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>

<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

<script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>

   <!--common script for all pages-->
<script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
<script src="<?php echo SITEURLM?>js/form-component.js"></script>





<!--start old script -->

<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/calebderical.js" type="text/javascript"></script>

<script src="<?php echo SITEURLM.$view_path; ?>js/schedule.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
$('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});
});

</script>
<!--end old script -->
   <!-- END JAVASCRIPTS --> 
   
<script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
		    
       });



   </script>  
   
<script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}
$("#pills-tab2").bootstrapWizard("show",2)
</script>
</body>
<!-- END BODY -->
</html>