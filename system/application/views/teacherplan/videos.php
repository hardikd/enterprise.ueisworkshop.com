<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head> 
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

<!-- START OLD SCRIPT-->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM.$view_path; ?>js/videos.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/photos.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
var SITEURLM1 = '<?php echo SITEURLM; ?>';
</script>	
<script src="<?php echo SITEURLM?>js/jquery.lightbox.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<LINK href="<?php echo SITEURLM?>css/video.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
function openpdf(d)
{

var gradestr='';

gradestr+='<td id="fileview"><object  data="'+d+'" type="application/pdf"  width="610" height="500"></object> </td>';    
	$('#fileview').replaceWith(gradestr); 

}

function openfiles(d)
{
var gradestr='';

gradestr+='<td id="fileview" align="center" width="600px"><a href="'+d+'">DOWNLOAD</a></td>';    
	$('#fileview').replaceWith(gradestr); 

}
</script>


<!-- END OLD SCRIPT -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
      <?php require_once($view_path.'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid"> 
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          
          <h3 class="page-title"> <i class="icon-wrench"></i>&nbsp; Tools & Resources </h3>
          <ul class="breadcrumb" >
            <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
            <li> <i class="icon-home"></i> <a href="<?php base_url();?>">Home</a> <span class="divider">></span> </li>
            <li> <a href="<?php base_url();?>tools">Tools & Resources</a> <span class="divider">></span> </li>
            <li> <a href="<?php base_url();?>tools/professional_development">Professional Development</a> <span class="divider">></span> </li>
            <li> <a href="<?php base_url();?>teacherplan/videos">Individual</a> </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN BLANK PAGE PORTLET-->
          <div class="widget purple">
            <div class="widget-title">
              <h4>Individual</h4>
            </div>
            <div class="widget-body">
              <div class="widget widget-tabs purple">
                <div class="widget-title"> </div>
                <div class="widget-body">
                  <div class="tabbable ">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#widget_tab1" data-toggle="tab">Videos</a></li>
                      <li class=""><a href="#widget_tab2" data-toggle="tab">Articles</a></li>
                      <li class=""><a href="#widget_tab3" data-toggle="tab">Journaling</a></li>
                      <li class=""><a href="#widget_tab4" data-toggle="tab">Journaling Response</a></li>
                      <li class=""><a href="#widget_tab5" data-toggle="tab">Artifacts</a></li>
                      <li class=""><a href="#widget_tab6" data-toggle="tab">Archive</a></li>
                    </ul>
                  </div>
                  <div class="tab-content">
                    <div class="tab-pane active" id="widget_tab1">
                      <div class="space20"></div>
                      <h3 style="text-align: center;">
                        <?php if(!empty($videos)) { ?>
                        <table class='tabcontent' style="width:535px;" cellspacing='0' cellpadding='0' id='conf'>
                          <tr class='tchead'>
                            <th> Standard Name </th>
                            <th> Video Name </th>
                            <th> Link </th>
                            <th> Video </th>
                          </tr>
                          <?php 
	 $i=1;
	 foreach($videos as $val) { 
	 
	 if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
	 				?>
                          <tr class="<?php echo $c;?>">
                            <td><?php echo  $val['group_name'];?></td>
                            <td><?php echo  $val['name'];?></td>
                            <td><a href="<?php echo  $val['link'];?>" target="_blank">Click Here</a></td>
                            <td onclick="show(<?php echo $val['prof_dev_id'];?>)"><a href="javascript:void(0)" style="cursor:pointer;">Watch</a></td>
                          </tr>
                          <?php $i++; } ?>
                        </table>
                        <?php } else { ?>
                        No Videos Found
                        <?php } ?>
                      </h3>
                    </div>
                    <div class="tab-pane" id="widget_tab2">
                      <div class="space20"></div>
                      <?php if($articles!=false) { ?>
                      <table class="table table-striped table-bordered" >
                        <thead>
                          <tr>
                            <th >Standard Name</th>
                            <th >Article Name</th>
                            <th >Link</th>
                            <th >View</th>
                          </tr>
                        </thead>
                        <?php 
								 $i=1;
								 foreach($articles as $val) { 
	 								if($i%2==0)
										{
					 						$c='tcrow2';
										}
									else
										{
										 $c='tcrow1';
										}
	 								?>
                         <tbody>
                          <tr class="<?php echo $c;?>">
                            <td class="hidden-phone"><?php echo  $val['group_name'];?></td>
                            <td><?php echo  $val['name'];?></td>
        <td><a href="<?php echo WORKSHOP_DISPLAY_FILES;?>articles/<?php echo $val['article_dev_id'].'.'.$val['link'];?>" target="_blank"  style="cursor:pointer;">Link</a></td>
                            <td><?php 
	  if($val['link']=='pdf')
	  {
	  ?>
    <a  style="cursor:pointer;" onclick="javascript:openpdf('<?php echo WORKSHOP_DISPLAY_FILES;?>articles/<?php echo $val['article_dev_id'].'.'.$val['link'];?>')">View</a>
        <?php } else { ?>
   <a  style="cursor:pointer;" onclick="javascript:openfiles('<?php echo WORKSHOP_DISPLAY_FILES;?>articles/<?php echo $val['article_dev_id'].'.'.$val['link'];?>')">View</a>
		<?php } ?></td>
                          </tr>
                          <?php $i++; } ?>
                        </tbody>
                      </table>
                      <br />
                      <br />
                      <div class="space20"></div>
                      <div id="reportDiv" style="display:block;" class="answer_list" >
                        <div class="widget purple">
                          <div class="widget-title">
                            <h4>Article Title</h4>
                          </div>
                          <div class="widget-body" style="min-height: 150px;">
                            <h3>
                            <table>
                        	<tr>
                          <td id="fileview"></td>
						</tr>
                      </table>
                      <?php } else { ?>
                      No Articles Found
                      <?php } ?>
						
                     
                            </h3>
                          </div>
                        
                    <div class="tab-pane " id="widget_tab3">
                      <div class="space20"></div>
          <div class="form-horizontal" >  
      					<div class="control-group">
                                             <label class="control-label">Select Video</label>
                                             <div class="controls">
                                             <select style="width:300px;" name="video" id="video" onchange="change()">
                                             <option value="">-Please Select-</option>
											<?php if(!empty($videos)) { ?>
													<?php foreach($videos as $val) { ?>
										<option value="<?php echo $val['prof_dev_assign_id'] ;?>"><?php echo $val['name'];?></option>
													<?php } } ?>
											</select>
                                             </div>
                                         </div>
    
   
										<h4 style="margin:0px 0px 19px 313px; color:#000">(or)</h4>
	
    									<div class="control-group">
                                             <label class="control-label">Select Article</label>
                                             <div class="controls">
							<select style="width:300px;" name="article" id="article" onchange="change()">
                        	<option value="">-Please Select-</option>
					<?php if(!empty($articles)) { ?>
							<?php foreach($articles as $val) { ?>
						<option value="<?php echo $val['article_dev_assign_id'] ;?>"><?php echo $val['name'];?></option>
				<?php } } ?>
			</select>   
            	</div>
             </div>
    

    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
<input  class="btn btn-small btn-purple" type="submit" name="journal" id="journal" value="submit" onclick="getthis()">
                                             </div>
                                         </div>

    
    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
												<div id="comments"></div>
                                             </div>
                                         </div>
                                         </div>
    
                      <div class="space20"></div>
                      <div class="space20"></div>
                      <div id="journaling_video">
                        <div class="widget purple">
                          <div class="widget-title">
                            <h4>Video Title Here</h4>
                          </div>
                          <div class="widget-body" style="min-height: 150px;">
                            <h3 style="text-align:center;">Video Appears Here</h3>
                          </div>
                        </div>
                      </div>
                      <div id="journaling_article">
                        <div class="widget purple">
                          <div class="widget-title">
                            <h4>Article Title Here</h4>
                          </div>
                          <div class="widget-body" style="min-height: 150px;">
                            <form action="#" class="form-horizontal">
                              <div class="control-group">
                                <label class="control-label">Article Comments</label>
                                <div class="controls">
                                  <textarea class="span6 " rows="3"></textarea>
                                </div>
                              </div>
                              <div class="form-actions">
                                <button type="submit" class="btn btn-purple">Add</button>
                                <button type="button" class="btn">Cancel</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane " id="widget_tab4">
                      <div class="space20"></div>
                      
                 <div class="form-horizontal" >  
      					<div class="control-group">
                                             <label class="control-label">Select Video</label>
                                             <div class="controls">
                                            <select style="width:300px;" name="refvideo" id="refvideo" onchange="refchange()">
                                            <option value="">-Please Select-</option>
													<?php if(!empty($videos)) { ?>
														<?php foreach($videos as $val) { ?>
											<option value="<?php echo $val['prof_dev_assign_id'] ;?>"><?php echo $val['name'];?></option>
												<?php } } ?>
											</select> 
									    </div>
                                         </div>
    
   
										<h4 style="margin:0px 0px 19px 313px; color:#000">(or)</h4>
	
    									<div class="control-group">
                                             <label class="control-label">Select Article</label>
                                             <div class="controls">
                            <select style="width:300px;" name="refarticle" id="refarticle" onchange="refchange()">
                        	<option value="">-Please Select-</option>
								<?php if(!empty($articles)) { ?>
							<?php foreach($articles as $val) { ?>
							<option value="<?php echo $val['article_dev_assign_id'] ;?>"><?php echo $val['name'];?></option>
						<?php } } ?>
					</select>
            	</div>
             </div>
    

    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
<input class="btn btn-small btn-purple" type="submit" name="journal" id="journal" value="submit" onclick="getrefthis()">
                                             </div>
                                         </div>

    
    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
												<div id="refcomments"></div>
                                             </div>
                                         </div>
                                         </div>
                      

                      <div class="space20"></div>
                      <div class="space20"></div>
                      <div id="journaling_video2">
                        <div class="widget purple">
                          <div class="widget-title">
                            <h4>Video Title Here</h4>
                          </div>
                          <div class="widget-body" style="min-height: 150px;">
                            <h3 style="text-align:center;">Video Appears Here</h3>
                          </div>
                        </div>
                      </div>
                      <div id="journaling_article2">
                        <table class="table table-striped table-bordered table-advance table-hover">
                          <thead>
                          <th>Article Comments</th>
                              </thead>
                          <tbody>
                            <tr>
                              <td><p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="tab-pane" id="widget_tab5">
                      <div class="space20"></div>
                      <div class="widget-body"><!-- BEGIN WIDGET BODY FIRST -->
                        <div class="widget widget-tabs purple"> <!-- BEGIN WIDGET TABS -->
                          <div class="widget-title"></div>
                          <div class="widget-body"><!-- BEGIN TABBABLE BODY -->
                            <div class="tabbable ">
                              <ul class="nav nav-tabs">
                                <li class="active"><a href="#widget_tab7" data-toggle="tab">Notes</a></li>
                                <li class=""><a href="#widget_tab8" data-toggle="tab">Links</a></li>
                                <li class=""><a href="#widget_tab9" data-toggle="tab">Photos</a></li>
                                <li class=""><a href="#widget_tab10" data-toggle="tab">Videos</a></li>
                                <li class=""><a href="#widget_tab11" data-toggle="tab">Files</a></li>
                              </ul>
                            </div>
                            <!-- END TABBABLE -->
                            
                            <div class="tab-content"><!-- TAB CONTENT START -->
                              <div class="tab-pane active" id="widget_tab7"><!-- TAB NOTES START -->
                                <div class="space20"></div>
                                <div class="control-group">
                                  <label class="control-label">Notes</label>
                                  <div class="controls">
                                    <textarea name="notes" id="notes" class="span6 " rows="3"></textarea>
                                  </div>
                                </div>
                                <div class="form-actions">
                                  <input class="btn btn-purple" type="submit" name="submit" value="Add" onclick="addnotes()">
                                  <input type="button" name="Cancel" value="Cancel" class="btn">
                                </div>
                              </div>
                              <!-- TAB NOTES END -->
                              
                              <div class="tab-pane" id="widget_tab8"><!-- TAB LINKS START -->
                                <div class="space20"></div>
                                <form id="linkuploadForm" class="form-horizontal" action="teacherplan/uploadlink" method="POST" >
                                  <div class="control-group">
                                    <label class="control-label">Link Name</label>
                                    <div class="controls">
                                      <input type="textbox" class="span6 " name="linkname" id="linkname">
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Link URL</label>
                                    <div class="controls">
                                      <input type="textbox" class="span6" name="link" id="link">
                                    </div>
                                  </div>
                                  <label class="control-label"></label>
                                  <div class="controls">
                                  <div class="space20"></div>
                                  <input type="submit" class="btn btn-small btn-purple" name="submit" value="Submit"  style="margin-top: -40px; padding: 5px; " />
                                  <div align="center" id="linkhtmlExampleTarget" style="color:red"></div>
                                </form>
                              </div>
                              <table>
                                <tr>
                                  <td align="center"><div id="linkdetails" style="display:none;">
                                      <input type="hidden" id="linkpageid" value="">
                                      <div id="linkmsgContainer"> </div>
                                    </div></td>
                                </tr>
                              </table>
                            </div>
                            <!-- TAB LINKS END -->
                            
                            <div class="tab-pane" id="widget_tab9"><!-- TAB PHOTOS START -->
                              <div class="space20"></div>
                  <form id="uploadForm" class="form-horizontal" action="teacherplan/uploadphoto" method="POST" enctype="multipart/form-data">
                                <div class="control-group">
                                  <label class="control-label">Photo Name</label>
                                  <div class="controls">
                                    <input type="text" name="name" id="name" class="span6 " />
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label">Upload Image</label>
                                  <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                      <div class="input-append">
                    <div class="uneditable-input"> <i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span> </div>
                    <span class="btn btn-file"> <span class="fileupload-new">Select file</span> <span class="fileupload-exists">Change</span>
                                        <input type="file" name="photo" class="default" id="photo">
                                        </span> <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a> </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label"></label>
                                  <div class="controls">
                                    <div class="space20"></div>
                                    <input type="submit" class="btn btn-small btn-purple" value="submit"  style="margin-top: -40px; padding: 5px; " />
                                  </div>
                                </div>
                              </form>
                              <div id="upload1"> 
                                <table>
                                  <tr>
                                    <td align="center"><div id="photodetails" style="display:block;">
                                        <input type="hidden" id="pageid" value="">
                                        <div id="msgContainer"> </div>
                                      </div></td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                            <!-- TAB PHOTOS END -->
                            
                            <div class="tab-pane" id="widget_tab10"><!-- TAB VIDEOS START -->
                              <div class="space20"></div>
                              <form id="videouploadForm" class="form-horizontal" action="teacherplan/uploadvideo" method="POST" enctype="multipart/form-data" >
                                <div class="control-group">
                                  <label class="control-label">Video Name</label>
                                  <div class="controls">
                                    <input type="textbox" class="span6" name="videoname" id="videoname">
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label">Upload Video</label>
                                  <div class="controls">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                      <div class="input-append">
                                        <div class="uneditable-input"> <i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span> </div>
                                        <span class="btn btn-file"> <span class="fileupload-new">Select file</span> <span class="fileupload-exists">Change</span>
                                        <input type="file" name="video" class="default" id="video">
                                        </span> <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a> </div>
                                    </div>
                                  </div>
                                </div>
                                <label class="control-label"></label>
                                <div class="controls">
                                <div class="space20"></div>
                                <input type="button" class="btn btn-small btn-purple" name="answer" value="Submit"  style="margin-top: -40px; padding: 5px; " />
                              </form>
                            </div>
                            
                            <!--                         <table class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th >Video Name</th>
                                <th >Actions</th>                              
                                
                            </tr>
                            </thead>
                            <tbody>
      
                            <tr class="odd gradeX">                               
                                <td class="hidden-phone">Video Name Here</td>
                                <td><a href="#myModal-delete-student" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a></td>
                               
                                                            </tr>
                            <tr class="odd gradeX">                 
                                <td class="hidden-phone">Video Name Here</td>
                                <td><a href="#myModal-delete-student" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a></td>                              </tr>
                                        <tr class="odd gradeX">                              
                                <td class="hidden-phone">Video Name Here</td>
                                <td><a href="#myModal-delete-student" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a></td>                           </tr>
                                </tbody>
                                </table>-->
                            
                            <table>
                              <tr>
                                <td align="center"><div id="videodetails" style="display:none;">
                                    <input type="hidden" id="videopageid" value="">
                                    <div id="videomsgContainer"> </div>
                                  </div></td>
                              </tr>
                            </table>
                          </div>
                          <!-- TAB VIDEOS END -->
                          
                          <div class="tab-pane" id="widget_tab11"><!-- TAB FILES START -->
                            <div class="space20"></div>
                        <form id="fileuploadForm" class="form-horizontal" action="teacherplan/uploadfile" method="POST" enctype="multipart/form-data" >
                              <div class="control-group">
                                <label class="control-label">File Name</label>
                                <div class="controls">
                                  <input type="textbox" name="filename" class="span6 " id="filename">
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label">Upload File</label>
                                <div class="controls">
                                  <div data-provides="fileupload" class="fileupload fileupload-new">
                                    <div class="input-append">
                                      <div class="uneditable-input"> <i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span> </div>
                                      <span class="btn btn-file"> <span class="fileupload-new">Select file</span> <span class="fileupload-exists">Change</span>
                                      <input type="file" name="file" class="default" id="file">
                                      </span> <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a> </div>
                                  </div>
                                </div>
                              </div>
                              <label class="control-label"></label>
                              <div class="controls">
                              <div class="space20"></div>
                              <input type="submit" class="btn btn-small btn-purple" name="submit" value="Submit"  style="margin-top: -40px; padding: 5px; " />
                          </div>
				</form>
 
                        
                          <table >
                            <tr>
                              <td align="center"><div id="filedetails" style="display:none;">
                                  <input type="hidden" id="filepageid" value="">
                                  <div id="filemsgContainer"> </div>
                                </div></td>
                            </tr>
                          </table>
                        </div>
                        <!-- TAB FILES END --> 
                        
                      </div>
                      <!-- END  TAB CONTENT --> 
                    </div>
                    <!-- END  TABBABLE BODY --> 
                  </div>
                  <!-- END WIDGET TABS --> 
                </div>
                <!-- END WIDGET BODY FIRST--> 
                
              </div>
              <!-- END TAB 5 ARTIFACTS-->
              
              <div class="tab-pane" id="widget_tab6">
                <div class="space20"></div>
                <h4 style="font-weight: bold;">
               
                </h3>
                <a href="teacherplan/pdarchived/<?php echo $this->session->userdata('teacher_id');?>">Archived Data</a>
              </div>
            </div>
          </div>
          <!-- END BLANK PAGE PORTLET--> 
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div id="myModal-delete-student" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
  </div>
  <div class="modal-body"> Please select "Yes" to remove this file. </div>
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
    <button data-dismiss="modal" class="btn btn-success"><i class="icon-check"></i> Yes</button>
  </div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 

<!-- ie8 fixes --> 
<!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 

<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script> 


<!--common script for all pages--> 
<script src="<?php echo SITEURLM?>js/common-scripts.js"></script> 
<!--script for this page--> 
<script src="<?php echo SITEURLM?>js/dynamic-table.js"></script> 
<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script> 
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script> 
<script src="<?php echo SITEURLM?>js/form-component.js"></script> 

<!-- END JAVASCRIPTS --> 

<script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script> 
<script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script> 
<script>
 var elem = document.getElementById("select_1");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script> 
<script>
 var elem = document.getElementById("select_2");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script> 
<script>
 var elem = document.getElementById("select_3");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script> 
<script>
 var elem = document.getElementById("select_4");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script> 
<script>
function show(ele)    {      
    var links = ['upload1','upload2'];
    var srcElement = document.getElementById(ele);      
    var doShow = true;
    if(srcElement != null && srcElement.style.display == "block")
        doShow = false;
    for( var i = 0; i < links.length; ++i )    {
        var otherElement = document.getElementById(links[i]);      
        if( otherElement != null )
            otherElement.style.display = 'none';
    }
    if( doShow )
        srcElement.style.display='block';         
    return false;
  }
</script> 
<script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script> 
<script>
   
   var button = document.getElementById('reportDiv2_btn');

button.onclick = function() {
    var div = document.getElementById('reportDiv2');
    if (div.style.display !== 'block') {
        div.style.display = 'block';
    }
    else {
        div.style.display = 'none';
    }
};

</script>
</body>
<!-- END BODY -->
</html>