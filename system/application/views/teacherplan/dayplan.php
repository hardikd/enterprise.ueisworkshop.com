<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::LessonPlan::</title>
<base href="<?php echo base_url();?>"/>
<LINK href="<?php echo SITEURLM?>css/sample.css" type=text/css rel=stylesheet>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/calebderical.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/adapters/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/sample.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/teacherplan.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/video.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
$(function() {
$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
        
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
	
	
	
	$(".sub_tab_content").hide(); //Hide all content
	$("ul.subtabs li:first").addClass("active").show(); //Activate first tab
	$(".sub_tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.subtabs li").click(function() {

		
		$("ul.subtabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".sub_tab_content").hide(); //Hide all tab content
        
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
});
</script>
<script type="text/javascript">
var arrlessonplans = <?php echo json_encode($lessonplans); ?>;
var arrlessonplansub = <?php echo json_encode($lessonplansub); ?>;
var custom_diff = <?php echo json_encode($custom_diff); ?>;
var siteurlm = '<?php echo SITEURLM; ?>';
var WORKSHOP_DISPLAY_FILES = '<?php echo WORKSHOP_DISPLAY_FILES; ?>';
$(document).ready(function() {
$('.ckeditor1').ckeditor();
/*$('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});*/

$('a.title').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false
});
$('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});
		$('#save').click(function() {
            pdata={};
			pdata.plan_id=$('#comment_id').val();
			pdata.comments=$('#commentsvalue').val();
			
		   $.post('teacherplan/addcomments',{'pdata':pdata},function(data) {
		  
		  
		  if(data.status==1)
		  {
            
			 

		 }
		 else
		 {
		 
			 alert('Failed Please Try Again ');
		 
		 }
		 $('#comments').dialog('close');

},'json');	

});	

});
function comments(id)
{
 
 var p_url='teacherplan/getcomments/'+id;

    $.getJSON(p_url,function(result)
	{
	   if(status!='')
	  {
	  $('#commentsvalue').val(result.status);
	  }
	  else
	  {
	    $('#commentsvalue').val('');
	  }
	
	
	});
 $("#comment_id").val(id);
 
 $("#comments").dialog({
			modal: true,
           	height: 300,
			width: 400
			});
	
}
</script>
<style type="text/css">
select#standards option
    {
      height:25px;    	        
    }

	
</style>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
		<form method="post" action="teacherplan/dayview">
		<table align="center">
		<tr>
		<td colspan="5" style="padding-left:230px;color:#657455;font-size:20px;font-weight:bold;">
		eLesson Planning
		</td>
		</tr>
		<tr>
		<td style="padding-right:100px;">
		<input onclick="document.location.href='<?php echo REDIRECTURL;?>teacherplan/index'" type="button" name="Week View" id="Week View" value="Week View" >
		</td>
		<td>
		Select Day:
		</td>
		<td>
		<input type="text" name="selectdate" id="selectdate" readonly value="<?php if(isset($selectdate)) { echo $selectdate;} else { echo date('m-d-Y');}?>">
		</td>
		<td>
		<input type="submit" name="go" value="go" id="go">
		</td>
		<td style="padding-left:100px;">
		
		<input onclick="window.open('<?php echo REDIRECTURL;?>teacherplan/printdayview/<?php if(isset($selectdate)) { echo $selectdate;} else { echo date('m-d-Y');}?>');" type="button" name="Print View" id="Print View" value="Print View" >
		</td>
		</tr>
		</table>
		</form>
		<?php
		if($pending!=false)
		{
		?>
		 <table  align="center" style="width:650px;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="<?php echo count($lessonplans)+3?>" align="center" bgcolor="#657455" style="color:#FFF"><b>Schedule Pending</b></td>
  </tr>
  <tr align="center" style="color:#333;">
    <td width="80px" bgcolor="#CCCCCC">Time</td>
    <td width="80px" bgcolor="#CCCCCC">Date</td>
	<td width="80px" bgcolor="#CCCCCC">Actions</td>
	
	
	
  </tr>
  
  <?php foreach($pending as $pendingvalue) { ?>
  <tr id="pendingrow<?php echo $pendingvalue['lesson_week_plan_id'];?>" align="center">
  <td width="80px">
		<?php 
		$start1=explode(':',$pendingvalue['starttime']);
		$end1=explode(':',$pendingvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		echo $start2." to ".$end2 ?>
		</td>
		<td width="80px">
		<?php echo $pendingvalue['date'];?>
		</td>
		<td width="80px">
		<input type="button" name="Pending" id="Pending" value="Pending" onclick="addplanpending('<?php echo $pendingvalue['date'];?>','<?php echo $start2;?>','<?php echo $end2;?>','<?php echo $pendingvalue['lesson_week_plan_id'];?>')">
		</td>
</tr>
  
  <?php } ?>
  
  </table> <br /><br />
		
		<?php }
		
		?>
		<?php 
		//print_r($dates);
		foreach($dates as $val)
		
		{
		$k=0;
		?>
		<table  align="center" style="width:670px;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
  <tr>
    <?php
	
	 list($month,$day,$year) = explode("-", $val['date']);
	$ct=$year.'-'.$month.'-'.$day;
	if(strtotime($ct)>=strtotime(date('Y-m-d')))
		 {
		 ?>
	
  <td bgcolor="#657455">&nbsp;&nbsp;&nbsp;<span id="a_<?php echo $val['date'];?>" style="align:left;cursor:pointer;" onclick="showplan('<?php echo $val['date'];?>')"><img src="<?php echo SITEURLM?>images/show_lesson_plan.png" height="16" width="16" alt="Show LessonPlan"  ></span>&nbsp;&nbsp;&nbsp;<!--<span style="align:left;cursor:pointer;" onclick="addplan('<?php echo $val['date'];?>')"><img src="<?php echo SITEURLM?>images/add_plan.png" height="20" width="20" alt="Add LessonPlan"  ></span>--></td>
  
  <?php } else { ?>
  <td bgcolor="#657455">&nbsp;&nbsp;&nbsp;<span id="a_<?php echo $val['date'];?>" style="align:left;cursor:pointer;" onclick="showplan('<?php echo $val['date'];?>')"><img src="<?php echo SITEURLM?>images/show_lesson_plan.png" height="16" width="16" alt="Show LessonPlan"  ></span>&nbsp;&nbsp;&nbsp;<!--<span style="align:left;cursor:pointer;" onclick="addplan('<?php echo $val['date'];?>')"><img src="<?php echo SITEURLM?>images/add_plan.png" height="20" width="20" alt="Add LessonPlan"  ></span>--></td>
  <?php } ?>
  
	<td colspan="<?php echo count($lessonplans)+5?>" align="center" bgcolor="#657455" style="color:#FFF"><b>Date:<?php echo $val['date'];?> &nbsp;&nbsp;&nbsp;Week:<?php echo $val['week'];?></b></td>
  </tr>
  <tr align="center" style="color:#333;">
    <td width="80px" bgcolor="#CCCCCC">Time</td>
	<td width="80px" bgcolor="#CCCCCC">Subject</td>
    <?php 
	$j=4;
	if($lessonplans!=false){
	 foreach($lessonplans as $plan)
	 {
	 $j++;
	 ?>
	<td width="80px" bgcolor="#CCCCCC"><?php echo $plan['tab'];?></td>
    <?php } ?>
	<td width="40px" bgcolor="#CCCCCC">Details</td>
	<td width="80px" bgcolor="#CCCCCC">Actions</td>
	<td width="80px" bgcolor="#CCCCCC">Comments</td>
	
	
	<?php } ?>
	<td width="20px" bgcolor="#CCCCCC"></td>
  </tr>
    <tr align="center"  >
	<td colspan="<?php echo $j+2;?>">
	<table id="<?php echo $val['date'];?>" style="display:none;">
    <?php if($getlessonplans!=false)
	{
	
	$le=0;
	  $je=0;
	  
	  foreach($getlessonplans as $getplan)
	  {
      if( $val['date']==$getplan['date'])
	{	  
	  $je++;
	  if($le!=$getplan['lesson_week_plan_id'])
		{	
	     $les=0;
		 if($je==1)
		 {
		    ?>
			<tr align="center">
		 
		 <?php }
		 else
		 {
		  list($month,$day,$year) = explode("-", $val['date']);
	$ct=$year.'-'.$month.'-'.$day;
	
	
		 if(strtotime($ct)>=strtotime(date('Y-m-d')) && !isset($value_feature[$le]))
		 {
		 ?>
		  
		  <td width="40px"><a class="standard" href="#" title="Other" rel="teacherplan/getother/<?php echo $le;?>">View</a></td><td width="80px"><input style="border:1px #cccccc solid;background-color:#eeeeee;float:left;width:25px;font-size:9px;" type="button" name="Edit" id="Edit" value="Edit" onclick='edit(<?php echo $le;?>)' ><input  type="button" style="border:1px #cccccc solid;background-color:#eeeeee;width:35px;font-size:9px;" name="Delete" id="Delete" value="Delete" onclick="deleteweek(<?php echo $le;?>)"></td><td width="80px"><a href='javascript:;' title="Add"  onclick='comments(<?php echo $le;?>)' >Add&nbsp;&nbsp;</a><a class="title" href="#" title="Comments" rel="teacherplan/getcommentsbyid/<?php echo $le;?>">View</a></td><td><?php if($le_id[$le]!='') { ?> <a href="teacherplan/createlessonplanpdf/<?php echo $getplan['teacher_id'];?>/<?php echo $subject_le_id[$le];?>/<?php echo $ct;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a> <?php } ?></td></tr><tr align='center'>
		 
		 <?php
		 }
		 else
		 {?>
		 
		 <td width="40px"><a class="standard" href="#" title="Other" rel="teacherplan/getother/<?php echo $le;?>">View</a></td><td width="80px">&nbsp;</td><td width="80px"><a class="title" href="#" title="Comments" rel="teacherplan/getcommentsbyid/<?php echo $le;?>">View</a></td><td><?php if($le_id[$le]!='') { ?> <a href="teacherplan/createlessonplanpdf/<?php echo $getplan['teacher_id'];?>/<?php echo $subject_le_id[$le];?>/<?php echo $ct;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a> <?php } ?></td></tr><tr align="center">
		 
		 <?php }
		 }
		 ?>
		
		<td width="80px">
		<?php 
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		echo $start2." to ".$end2 ?>
		</td>
		<td width="80px"><?php echo $getplan['subject_name']?></td>
		<td width="80px"><?php echo $getplan['subtab']?></td>
		
		<?php 
		}
		else
		{
		$les++;
		?>
		  <td width="80px"><?php echo $getplan['subtab']?></td>
		
		<?php
		}
		
		$le=$getplan['lesson_week_plan_id'];
		$le_id[$le]=$getplan['lesson_plan_sub_id'];
		$subject_le_id[$le]=$getplan['subject_id'];
		$lecomments[$le]=$getplan['comments'];
	  
	  }
	  
	  }
	  if($je==0)
	  {
	  ?>
	    <tr align="center">
		<td colspan="<?php echo $j;?>">No Plans Found </td>
		
	<?php } if($le!=0) {
	/*if(($les+2)>=count($lessonplans))
	{
	  
	
	}
	else
	{
	 
	 ?>
	 <td width="<?php echo (count($lessonplans)-$les-1)*120;?>px" ></td>
	<?php } */
	 list($month,$day,$year) = explode("-", $val['date']);
	$ct=$year.'-'.$month.'-'.$day;
	
	if(strtotime($ct)>=strtotime(date('Y-m-d')) && !isset($value_feature[$le]))
		 {
	?>	<td width="40px"><a class="standard" href="#" title="Other" rel="teacherplan/getother/<?php echo $le;?>">View</a></td>
	<td width="80px"><input style="border:1px #cccccc solid;background-color:#eeeeee;float:left;width:25px;font-size:9px;" type="button" name="Edit" id="Edit" value="Edit" onclick='edit(<?php echo $le;?>)'><input type="button" style="border:1px #cccccc solid;background-color:#eeeeee;width:35px;font-size:9px;" name="Delete" id="Delete" value="Delete" onclick="deleteweek(<?php echo $le;?>)"></td><td width="80px"><a href='javascript:;' title="Add"  onclick='comments(<?php echo $le;?>)' >Add&nbsp;&nbsp;</a><a class="title" href="#" title="Comments" rel="teacherplan/getcommentsbyid/<?php echo $le;?>">View</a></td><td><?php if($le_id[$le]!='') { ?> <a href="teacherplan/createlessonplanpdf/<?php echo $getplan['teacher_id'];?>/<?php echo $subject_le_id[$le];?>/<?php echo $ct;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a> <?php } ?></td>
	<?php } else { ?>
	
	<td width="40px"><a class="standard" href="#" title="Other" rel="teacherplan/getother/<?php echo $le;?>">View</a></td>
	<td width="80px">&nbsp;</td>
	<td width="80px"><a class="title" href="#" title="Comments" rel="teacherplan/getcommentsbyid/<?php echo $le;?>">View</a></td><td><?php if($le_id[$le]!='') { ?> <a href="teacherplan/createlessonplanpdf/<?php echo $getplan['teacher_id'];?>/<?php echo $subject_le_id[$le];?>/<?php echo $ct;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a> <?php } ?></td>
	<?php } } ?> </tr></table>
	
	


	<?php } else { ?>
	<tr align="center"><td colspan="<?php echo $j;?>">No Plans Found  </td></tr></table></td></tr>
	<?php } ?>
	<?php
	 list($month,$day,$year) = explode("-", $val['date']);
	$ct=$year.'-'.$month.'-'.$day;
	if(strtotime($ct)>=strtotime(date('Y-m-d')))
		 {
		 ?>
	<!--<tr>
  <td align="right" colspan="<?php echo $j+1;?>"><input type="button" name="Add" id="Add" value="Add" onclick="addplan('<?php echo $val['date'];?>')"></td>
  </tr>-->
  <?php } ?>
	</table>
	</td>
  </tr>
 
  
  
</table>
<br />
		<?php } ?>

		
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Lesson Plan" style="display:none;"> 

<form name='teacherplan' id='teacherplan' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform" style="float: left">
<tr><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
<tr><td>
<ul class="tabs">
   
	<li><a href="#tab1" >Tab1</a></li>
	<li><a href="#tab2" >Tab2</a></li>
	<li><a href="#tab3" >Tab3</a></li>
	</tr></td>

			<tr class="tab_container">			
			<td>	
			<div id="tab1" class="tab_content" >
			<table style="width:900px;">
			<tr>
			<td valign="top" class="style1" colspan="2">				
				<input class="txtbox" type='hidden' readonly  id='date' name='date' value=''>
			</td>	
			</tr>
			<tr>
				<td valign="top" class="style1" colspan="2">
				
				<b>Periods:</b>
				&nbsp;
				<!--<select name="period_id" id="period_id" onchange="selectsubject(this.value,0)">
				<option value="">-Please Select-</option>
				<?php if($periods!=false) { 
				
				foreach($periods as $periodval)
				{
				?>
				
				<option value="<?php echo $periodval['period_id'];?>"><?php echo $periodval['start_time'];?>-<?php echo $periodval['end_time'];?></option>
				<?php } } ?>
				</select>-->
				<span id="period_display"></span>
				<input class='txtbox'  type='hidden'  id='period_id' name='period_id' readonly >
				<input class='txtbox'  type='hidden'  id='start' name='start' readonly >
				<input  type='hidden'  id='lesson_week_plan_id' name='lesson_week_plan_id'>
				<input class="txtbox"  type='hidden'  id='end' name='end' readonly >
				<input class="txtbox"  type='hidden'  id='subject_id' name='subject_id' readonly >
				<input class="txtbox"  type='hidden'  id='grade' name='grade' readonly >
				&nbsp;
				<b>Subject:</b>
				&nbsp;
				<span id="subject_display"></span>
				&nbsp;
				<b>Grade:</b>
				&nbsp;
				<span id="grade_display"></span>
			
				
				</td>
				
			</tr>
			<tr>
			
				<td valign="top" >				
				<div style="width:300px;">
				<table id="material" border="0" style="float: left;">
<tr>
<td>

</td>
</tr>
<tr>
<td>

</td>
</tr>
</table>
				</div>
				</td>
				
			</tr>
		<?php	if($lessonplans!=false){
	 $i=0;
	 //foreach($lessonplans as $all)
	 {
	 
	 ?>
	 
			<?php if($i==0) { ?>
			
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font><span class="eldstyle"></span>&nbsp;Strand:
				</td>
			<td valign="top">
			<select name="strand" id="strand">
				<option value="">-Please Select-</option>
			</select>	
			</td>
			</tr>
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font><span class="eldstyle"></span>&nbsp;Element Descriptor:
				</td>
			<td valign="top">
			<select name="standards" id="standards"  >
				<option value="">-Please Select-</option>
			</select>	
			<input type="hidden" name="standard_id" id="standard_id" value="">
			<input type="hidden" name="standarddata_id" id="standarddata_id" value="">
			</td>
			</tr>
			<tr class="newst">
	 <td colspan="2" align="center">
	 Math PDF&nbsp;&nbsp
      ELA PDF

	 </td>
	 </tr>
	 <tr class="newst">
	 <td colspan="2" align="center">
	 &nbsp;&nbsp;<a href="<?php echo SITEURLM;?>CCSSI_Math_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo SITEURLM;?>CCSSI_ELA_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a>

	 </td>
	 </tr>
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font>Element Descriptor:
				</td>
			<td valign="top">
			<textarea style="width:600px;height:100px;"  id='standarddata' name='standarddata'  ></textarea>
			
			</td>
			</tr>
			<?php } ?>
	 
	 
	 <?php  $i++; } }?>
			<tr class="oldst">
	<td colspan="2" align="center">
	 Math PDF&nbsp;&nbsp
      ELA PDF

	 </td>
	 </tr>
	 <tr class="oldst">
	 <td colspan="2" align="center">
	 &nbsp;&nbsp;<a href="<?php echo SITEURLM;?>CCSSI_Math_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo SITEURLM;?>CCSSI_ELA_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a>

	 </td>
	 </tr>
			
			
			</table>
			</div>			
			<div id="tab2" class="tab_content" >
			<table style="width:900px;">
			<tr class="oldst">
				<td valign="top" class="style1">
				<font color="red">*</font>Standard:
				</td>
				<td valign="top" >
				<textarea style="width:600px;height:100px;"  id='standard' name='standard'  ></textarea>
				
				</td>
				
			</tr>
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font>Standard:
				</td>
			<td valign="top">
			<textarea style="width:600px;height:100px;"  readonly id='standarddisplay' name='standarddisplay'  ></textarea>
			
			</td>
			</tr>
			<?php	if($lessonplans!=false){
	 
	 foreach($lessonplans as $all)
	 {
	 
	 ?>
			<tr id="<?php echo $all['lesson_plan_id']?>" class="lessonplan">
				<td valign="top" class="style1">
				<font color="red">*</font><?php echo $all['tab']?>:
				</td>
				<td valign="top" >
				<select name="<?php echo $all['lesson_plan_id']?>" id="<?php echo $all['lesson_plan_id']?>" onchange="selectgrade(<?php echo $all['lesson_plan_id']?>,this.value)">
				<option value="">-Please Select-</option>
				<?php 
				foreach($lessonplansub as $sub)
				{
				if($all['lesson_plan_id']==$sub['lesson_plan_id']) {?>
				<option value="<?php echo $sub['lesson_plan_sub_id']?>"><?php echo $sub['subtab']?></option>
				<?php } } ?>
				
				</td>
				
			</tr>
			<?php } } ?>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Differentiated Instruction:
				</td>
				<td valign="top" >
				<textarea   class="ckeditor1"   id='diff_instruction' name='diff_instruction'  ></textarea>
				
				</td>
				
			</tr>
			</table>
			</div>
			<div id="tab3" class="tab_content" >
			<table style="width:900px;">
			<?php	if($custom_diff!=false){
	 
	 foreach($custom_diff as $all)
	 {
	 
	 ?>
			<tr >
				<td valign="top" class="style1">
				<div style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b><?php echo $all['tab']?></b>
		</div>
				</td>
			</tr>	
			<tr>
				<td valign="top" >
				<textarea class="obplans" style="width:900px;" name="c_<?php echo $all['custom_differentiated_id']?>" id="c_<?php echo $all['custom_differentiated_id']?>" ></textarea>
				
				</td>
				
			</tr>
			<?php } } ?>
			
			</table>
			</div>
			</td>
			
						
</tr><tr><td valign="top" align="center"><input class="btnbig" type='submit' name="submit" id='teacheradd' value='Add' title="Add New" > <input class="btnbig" type='button' name='cancel' id='cancel' value='Cancel' title="Cancel"></td></tr></table>


</form>
</div>
<div id="pendingdialog" title="Lesson Plan" style="display:none;"> 

<form name='teacherplanpending' id='teacherplanpending' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform" style="float: left">
<tr><td>
<span style="color: Red;display:none" id="pendingmessage"></span>
				</td>
			</tr>
			<tr><td>
<ul class="subtabs">
   
	<li><a href="#tab4" >Tab1</a></li>
<li><a href="#tab5" >Tab2</a></li>	
<li><a href="#tab6" >Tab3</a></li>	
	</tr></td>

			<tr class="sub_tab_container">			
			<td>	
			<div id="tab4" class="sub_tab_content" >
			<table style="width:900px;">
			<tr>
				<td valign="top" class="style1" colspan="2">				
				<input class="txtbox" type='hidden' readonly  id='pendingdate' name='date' value=''>				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1" colspan="2">
				<font color="red">*</font>Periods:
				&nbsp;
				<select name="pendingperiod_id" id="pendingperiod_id" onchange="pendingselectsubject(this.value,0)">
				<option value="">-Please Select-</option>
				<?php if($periods!=false) { 
				
				foreach($periods as $periodval)
				{
				?>
				
				<option value="<?php echo $periodval['period_id'];?>"><?php echo $periodval['start_time'];?>-<?php echo $periodval['end_time'];?></option>
				<?php } } ?>
				</select>
				<input class='txtbox'  type='hidden'  id='pendingstart' name='start' readonly >
				<input  type='hidden'  id='lesson_week_plan_id_pending' name='lesson_week_plan_id_pending'>
				<input class="txtbox"  type='hidden'  id='pendingend' name='end' readonly >
				&nbsp;
				<font color="red">*</font>Subject:
				&nbsp;
				<select name="pendingsubject_id" id="pendingsubject_id" >
				<option value="">-Please Select-</option>				
				</select>
				&nbsp;
				<font color="red">*</font>Grade:
				&nbsp;
			<select name="pendinggrade" id="pendinggrade">
				<option value="">-Please Select-</option>
			</select>	
			
				</td>
				
			</tr>
			<tr>
			
				<td valign="top" >				
				<div style="width:300px;">
				<table id="pendingmaterial" border="0" style="float: left;">
<tr>
<td>

</td>
</tr>
<tr>
<td>

</td>
</tr>
</table>
				</div>
				</td>
				
			</tr>
		<?php	if($lessonplans!=false){
		$i=0;
	// foreach($lessonplans as $all)
	 {
	 
	 ?>
	<!-- <tr id="pending<?php echo $all['lesson_plan_id']?>" class="lessonplan">
				<td valign="top" class="style1">
				<font color="red">*</font><?php echo $all['tab']?>:
				</td>
				<td valign="top" >
				<select name="pending<?php echo $all['lesson_plan_id']?>" id="pending<?php echo $all['lesson_plan_id']?>">
				<option value="">-Please Select-</option>
				<?php 
				foreach($lessonplansub as $sub)
				{
				if($all['lesson_plan_id']==$sub['lesson_plan_id']) {?>
				<option value="<?php echo $sub['lesson_plan_sub_id']?>"><?php echo $sub['subtab']?></option>
				<?php } } ?>
				
				</td>
				
			</tr>-->
			
			<?php if($i==0) { ?>			
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font><span class="eldstyle"></span>&nbsp;Strand:
				</td>
			<td valign="top">
			<select name="pendingstrand" id="pendingstrand">
				<option value="">-Please Select-</option>
			</select>	
			</td>
			</tr>
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font><span class="eldstyle"></span>Element Descriptor:
				</td>
			<td valign="top">
			<select name="pendingstandards" id="pendingstandards"  >
				<option value="">-Please Select-</option>
			</select>	
			<input type="hidden" name="pendingstandard_id" id="pendingstandard_id" value="">
			<input type="hidden" name="pendingstandarddata_id" id="pendingstandarddata_id" value="">
			</td>
			</tr>
			<tr class="newst">
	 <td colspan="2" align="center">
	 Math PDF&nbsp;&nbsp
      ELA PDF

	 </td>
	 </tr>
	 <tr class="newst">
	 <td colspan="2" align="center">
	 &nbsp;&nbsp;<a href="<?php echo SITEURLM;?>CCSSI_Math_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo SITEURLM;?>CCSSI_ELA_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a>

	 </td>
	 </tr>
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font>Element Descriptor:
				</td>
			<td valign="top">
			<textarea style="width:600px;height:100px;"  id='pendingstandarddata' name='pendingstandarddata'  ></textarea>
			
			</td>
			</tr>
			<?php } ?>
	 
	 
	 
	 <?php $i++; } }?>
	 <tr class="oldst">
	<td colspan="2" align="center">
	 Math PDF&nbsp;&nbsp
      ELA PDF

	 </td>
	 </tr>
	 <tr  class="oldst">
	<td colspan="2" align="center">
	 &nbsp;&nbsp;<a style="color: rgb(255,255,255);" href="<?php echo SITEURLM;?>CCSSI_Math_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;<a style="color: rgb(255,255,255);" href="<?php echo SITEURLM;?>CCSSI_ELA_Standards.pdf" style="color:#ffffff" target="_blank"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a>

	 </td>
	 </tr>
	</table>
			</div>			
			<div id="tab5" class="sub_tab_content" >
			<table style="width:900px;">
			 <tr class="oldst">
				<td valign="top" class="style1">
				<font color="red">*</font>Standard:
				</td>
				<td valign="top" >
				<textarea style="width:600px;height:100px;"  id='pending_standard' name='pending_standard'  ></textarea>
				
				</td>
				
			</tr>
			<tr class="newst" style="display:none;">
			<td valign="top" class="style1">
				<font color="red">*</font>Standard:
				</td>
			<td valign="top">
			<textarea style="width:600px;height:100px;"  readonly id='pending_standarddisplay' name='pending_standarddisplay'  ></textarea>
			
			</td>
			</tr>
			<?php	if($lessonplans!=false){
		
	 foreach($lessonplans as $all)
	 {
	 
	 ?>
			<tr id="pending<?php echo $all['lesson_plan_id']?>" class="lessonplan">
				<td valign="top" class="style1">
				<font color="red">*</font><?php echo $all['tab']?>:
				</td>
				<td valign="top" >
				<select name="pending<?php echo $all['lesson_plan_id']?>" id="pending<?php echo $all['lesson_plan_id']?>">
				<option value="">-Please Select-</option>
				<?php 
				foreach($lessonplansub as $sub)
				{
				if($all['lesson_plan_id']==$sub['lesson_plan_id']) {?>
				<option value="<?php echo $sub['lesson_plan_sub_id']?>"><?php echo $sub['subtab']?></option>
				<?php } } ?>
				
				</td>
				
			</tr>
			<?php } } ?>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Differentiated Instruction:
				</td>
				<td valign="top" >
				<textarea   class="ckeditor1"   id='pending_diff_instruction' name='pending_diff_instruction'  ></textarea>
				
				</td>
				
			</tr>
			</table>
			</div>
			<div id="tab6" class="sub_tab_content" >
			<table style="width:900px;">
			<?php	if($custom_diff!=false){
	 
	 foreach($custom_diff as $all)
	 {
	 
	 ?>
			<tr >
				<td valign="top" class="style1">
				<div style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b><?php echo $all['tab']?></b>
		</div>
				</td>
			</tr>	
			<tr>
				<td valign="top" >
				<textarea class="obplans" style="width:900px;" name="p_c_<?php echo $all['custom_differentiated_id']?>" id="p_c_<?php echo $all['custom_differentiated_id']?>" ></textarea>
				
				</td>
				
			</tr>
			<?php } } ?>
			
			</table>
			</div>
			</td>
			
			
						
</tr><tr><td valign="top" align="center"><input class="btnbig" type='submit' name="submit" id='teacheraddpending' value='Add' title="Add New" > <input class="btnbig" type='button' name='cancel' id='pendingcancel' value='Cancel' title="Cancel"></td></tr></table>

</form>
</div>
<div id="comments" title="comments" style="display:none;">
<table>
<tr>
<td>
Comments:
</td>
<td>
<textarea style="width:200px;" name="commentsvalue" id="commentsvalue"></textarea>
<input type="hidden" name="comment_id" id="comment_id" value="">
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input type="button" name="save" id="save" value="save">
</td>
</tr>
</table>

</div>
</body>
</html>