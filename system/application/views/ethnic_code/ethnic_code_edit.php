﻿
<table class='table table-striped table-bordered' id='editable-sample'>
  <tr>
    <td>Id</td>
    <td>District_id</td>
    <td>Ethnic Code</td>
    <td>Status</td>
    <td>Edit</td>
    <td>Remove</td>
    <?php 
  $con=1+($page - 1)*10;
foreach($alldata as $data)
{?>
  </tr>
  <tr id="home_<?php echo $data->id;?>">
    <td><?php echo $con;?></td>
    <td><?php echo $data->district_id;?></td>
    <td><?php echo $data->ethnic_code;?></td>
    <td><?php echo $data->status;?></td>
    <td><button class="edit_ethnic_code btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button></td>
    <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
    <td><button type="Submit" id="remove_ethnic_code" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_ethnic_code btn btn-danger"><i class="icon-trash"></i></button></td>
  </tr>
  <?php  
  $con++;
  }
  ?>
</table>
<?php print $pagination;?> 
<script>
     $('.edit_ethnic_code ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'ethnic_code/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#ethnic_code_id').val(result[0].id);
       $('#ethnic_code').val(result[0].ethnic_code);
       $('#status').val(result[0].status);
       console.log(result[0].ethnic_code);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_ethnic_code").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'ethnic_code/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed Ethnic Code!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script> 
