<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Checklist Standards::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/observationgroup.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/countries1.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
		<table align="center" cellpadding="5" >
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { 
		foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		District:
		</td>
		<td>
		<select class="combobox" name="district" id="district">
		<?php if(!empty($district)) { ?>
		<option value="all">All</option>
		<?php foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } else { ?>
		<option value="">No Districts Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getobservationgroup" id="getobservationgroup" value="Submit">
		</td>
		</tr>
		</table>
		<table>
		<tr>
		<td align="left">
		<a href="observationgroup/copy">Copy Checklist Standards</a>
		</td>
		</tr>
		</table>
        <div id="observationgroupdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New" class="btnbig" type="button" name="observationgroup_add" id="observationgroup_add" value="Add New">
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Checklist Standards" style="display:none;"> 

<form name='observationgroupform' id='observationgroupform' method='post' onsubmit="return false" >
<table cellpadding="0" cellspacing="0" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Country:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="country_id" id="country_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>State:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="state_id" id="state_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>	
				<td valign="top" class="style1">
				<font color="red">*</font>District:
				</td>
				<td>
				<select class="combobox" name="district_id" id="district_id">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Group Name:
				</td>
				<td valign="top" style="height:40px;">
				<input class='txtbox' type='text'  id='group_name' name='group_name'>
				<input  type='hidden'  id='group_id' name='group_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Description :
				</td>
				<td valign="top" style="height:40px;">
				<textarea class='txtarea' type='text'  id='description' name='description'></textarea>
				
				</td>
				
			</tr>
			
						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New"  class="btnbig" type='submit' name="submit" id='observationgroupadd' value='Add' > <input title="Cancel"  class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
