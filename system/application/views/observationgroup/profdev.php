<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
   <script type="text/javascript">
var base_url='<?php echo SITEURLM?>';

</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/headerv1.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">
 <?php 
	if($this->session->userdata("login_type")=='admin')
		{
			require_once($view_path.'inc/pleftmenu.php'); 
		}
		else
		{
			require_once($view_path.'inc/developmentmenu_new.php'); 
		}
		?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      Welcome <?php echo $this->session->userdata('username');?>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                             <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span>
                       </li>
						<li>
							<a href="<?php echo base_url();?>observationgroup">Checklist Standards</a>
                            <span class="divider">></span>
                       </li>

                        <li>
							<a href="<?php echo base_url();?>observationgroup/dev/<?php echo $group_id;?>"> Videos / Articles</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
							<a href="<?php echo base_url();?>observationgroup/profdev/<?php echo $group[0]['group_id'];?>">Videos</a>
                       </li>
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Videos</h4>
                         </div>
                          <div class="widget-body">
						<button type='button' name="add" id='add' value='Add New' class="btn btn-success" onclick="add(<?php echo $group[0]['group_id'];?>)"><i class="icon-plus"></i >  Add New</button>
                        
        <div class="content">
		<table>
		<tr>
		<td>
		Standard : <b><?php echo $group[0]['group_name'];?></b>
		</td>
		<td>
		District : <b><?php echo $group[0]['districts_name'];?></b>
		</td>
		</tr>
		</table>
		<div class='htitle'></div>
			<table class='table table-striped table-bordered' id='editable-sample'>
        <tr>
        <td>Name</td>
        <td>Link</td>
        <td>Video</td>
        <td >Actions</td>
        </tr>
		<?php if($videos!=false) { 
		$i=1;
		foreach($videos as $val)
		{
		if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
		?>
		
		<tr class="<?php echo $c;?>">
		<td><?php echo $val['name'];?></td>
		<td><a href="<?php echo $val['link'];?>" target="_blank">Click Here</a></td>
		<td onclick="show(<?php echo $val['prof_dev_id'];?>)"><a href="javascript:void(0)" style="cursor:pointer;">Watch</a></td>
		<td>
        	<button class="btn btn-primary" data-dismiss="modal" value="Edit" name="Edit" aria-hidden="true" onclick="distedit(<?php echo $val['prof_dev_id'];?>)"><i class="icon-pencil"></i></button>
  <button data-dismiss="modal" value="Delete" name="Delete" class="btn btn-danger" onclick="distdelete(<?php echo $val['prof_dev_id'];?>)"><i class="icon-trash"></i></button>
        
        
        
        </td>
		</tr>
		<?php } } else { ?>
		<tr>
		<td colspan="4" align="center">
		No Videos Found
		</td>
		</tr>
		<?php } ?>
		</table>
		
		
			
        </div>
            </div>
			</div>
            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <!--notification -->
  <div id="dialog" title="Videos" style="display:none;"> 

<form name='profform' id='profform' method='post' onsubmit="return false" >
<table cellpadding="0" cellspacing="0" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			
			<tr>
				<td valign="top" >
				<font color="red">*</font>Name:
				</td>
				<td valign="top" style="height:40px;">
				<input class='txtbox' type='text'  id='name' name='name'>
				<input  type='hidden'  id='group_id' name='group_id' value="<?php echo $group[0]['group_id'];?>">
				<input  type='hidden'  id='prof_dev_id' name='prof_dev_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Link :
				</td>
				<td valign="top" style="height:40px;">
				<textarea style="width:250px;" class='txtarea' type='text'  id='link' name='link'></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Embed :
				</td>
				<td valign="top" style="height:40px;">
				<textarea style="width:250px;" class='txtarea' type='text'  id='desc' name='desc'></textarea>
				
				</td>
				
			</tr>
			
						
</tr><tr><td valign="top"></td><td valign="top">
 <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<button type='submit' name="submit" id='observationgroupadd' value='Add' class="btn btn-success"><i class="icon-plus"></i> Add New</button>
 


</td></tr></table></form>
</div>
<div id="watch" title="watch a video" style="display:none;">
<table cellpadding="0" cellspacing="0" border=0 class="jqform">
<tr>
<td id="avideo">
</td>
</tr>
</table>
</div>
   <!-- notification ends -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.form.js"></script>
     
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/profdev.js" type="text/javascript"></script>

   <!-- END JAVASCRIPTS --> 
 
</body>
<!-- END BODY -->
</html>