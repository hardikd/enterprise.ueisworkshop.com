<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Articles::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">
var base_url='<?php echo SITEURLM?>';
</script>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
		<div class='htitle'>articles</div>
		<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td  >Name</td><td>Link</td><td >District</td></tr>
		<?php if($articles!=false) { 
		$i=1;
		foreach($articles as $val)
		{
		if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
		?>
		
		<tr class="<?php echo $c;?>">
		<td><?php echo $val['name'];?></td>
		<td><a href="<?php echo WORKSHOP_DISPLAY_FILES;?>articles/<?php echo $val['article_dev_id'].'.'.$val['link'];?>" target="_blank">Click Here</a></td>
		<td><?php echo $val['districts_name'];?></td>
		</tr>
		<?php } } else { ?>
		<tr>
		<td colspan="4" align="center">
		No articles Found
		</td>
		</tr>
		<?php } ?>
		</table>
		
		<table><tr><td><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td></tr></table>	
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
