<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Videos::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script type="text/javascript">
var base_url='<?php echo SITEURLM?>';
function show(id)
		{
		  var p_url='observationgroup/getvideoinfo/'+id;
    $.getJSON(p_url,function(result)
	{
			   
	
	var str="<td id='avideo'>"+result.video.descr+"</td>";
	
	
	
	$('#avideo').replaceWith(str);
		
	}); 
		
		 $("#watch").dialog({
			modal: true,
           	height: 450,
			width: 700,
			 close: function(){
            var str="<td id='avideo'></td>";
				$('#avideo').replaceWith(str);
				}
			});
		
		
		
		
		}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
		<div class='htitle'>Videos</div>
		<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td  >Name</td><td>Link</td><td>Video</td><td >District</td></tr>
		<?php if($videos!=false) { 
		$i=1;
		foreach($videos as $val)
		{
		if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
		?>
		
		<tr class="<?php echo $c;?>">
		<td><?php echo $val['name'];?></td>
		<td><a href="<?php echo $val['link'];?>" target="_blank">Click Here</a></td>
		<td onclick="show(<?php echo $val['prof_dev_id'];?>)"><a href="javascript:void(0)" style="cursor:pointer;">Watch</a></td>
		<td><?php echo $val['districts_name'];?></td>
		</tr>
		<?php } } else { ?>
		<tr>
		<td colspan="4" align="center">
		No Videos Found
		</td>
		</tr>
		<?php } ?>
		</table>
		
		<table><tr><td><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td></tr></table>	
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="watch" title="watch a video" style="display:none;">
<table cellpadding="0" cellspacing="0" border=0 class="jqform">
<tr>
<td id="avideo">
</td>
</tr>
</table>
</div>
</body>
</html>
