<?php error_reporting(0);?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

 <!--start old script -->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/jscal2.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/lang/en.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>Quiz/css/jscal2.css" />


<!--end old script -->
   

<script type="text/javascript">

function updatecategory(tag)
{
	var gradeid=''; var schoolid='';
	<?php if($records[0]['district_id']!='' || $records[0]['observer_id']!=''){ ?> 
	 gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	 <?php }  if($records[0]['district_id']!=''){ ?>
 	 schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value; <?php }?>
	
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;

	if(document.getElementById('class').value != 'null')
	{
	 var classid = document.getElementById('class').options[document.getElementById('class').selectedIndex].value;
	
	if(gradeid!=-1 && classid!= -1 && tag!=-1 && fDate!='' && tDate!='')
	{	
		//$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/classroomgrowthgraph/getReportHtml?cat_id='+tag+'&grade_id='+gradeid+'&school_id='+schoolid+'&class_id='+classid+'&fdate='+fDate+'&tdate='+tDate,
		
		success:function(clounmValue)
		{ 
			$("#loader").hide();
			//getGraph1(clounmValue);
			 var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
			 if(is_safari)
			 {
                             //$('#classroomgrwthrep').html();
				window.location= '<?php echo base_url();?>classroomgrowthgraphbig';
			 }else
			 {
                             
                             
	 			window.open('<?php echo base_url();?>classroomgrowthgraphbig',"View Classroom Growth","directories=0, status=0, menubar=0, scrollbars=1, resizable=1,width=1300, height=800,top=100,left=100");
			 }
			//	popupWindow =window.open('classroomgrowthgraphbig',"View Classroom Growth","directories=0, status=0, menubar=0, scrollbars=1, resizable=1,width=1300, height=800,top=100,left=100");

		}
		});
     } 
	}  
}



function updateclass(tag)
{
	var gradeid=''; var schoolid='';
	<?php if($records[0]['district_id']!='' || $records[0]['observer_id']!=''){ ?>
	 gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	  <?php }  if($records[0]['district_id']!=''){ ?>
	 schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value;
	<?php }?>
	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;

	
	if(gradeid!=-1 && schoolid!= -1 && tag!=-1 && fDate!='' && tDate!='')
	{	
//		$("#container").html('&nbsp;');
//  		$("#loader").show();
	 	 $.ajax({
		url:'<?php echo base_url();?>/classroomgrowthgraph/getReportHtml?class_id='+tag+'&grade_id='+gradeid+'&school_id='+schoolid+'&cat_id='+catid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 
                   
                    $.ajax({
                    url:'<?php echo base_url();?>classroomgrowthgraphbig',
                    success:function(result)
                    { 
                        $('#classroomgrowthrep').html(result);
                        $('#reportwidget').show();
                    }
                    
                    });
//                        $("#loader").hide();
//			//getGraph1(clounmValue);
//				 var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
//			 if(is_safari)
//			 {
//				window.location= '<?php echo base_url();?>classroomgrowthgraphbig';
//			 }else
//			 {
//	 			window.open('<?php echo base_url();?>classroomgrowthgraphbig',"View Classroom Growth","directories=0, status=0, menubar=0, scrollbars=1, resizable=1,width=1300, height=800,top=100,left=100");
//			 }

		}
		});
	} 
}
 
<?php if($records[0]['district_id']!='' || $records[0]['observer_id']!='') {?>  
function updategrade(tag)
{
	var schoolid ='';
	<?php if($records[0]['district_id']!='') {?>
 	schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value; <?php } ?>
	if(schoolid!=-1)
	{	
		$.ajax({
		url:'<?php echo base_url();?>/classroomgrowthgraph/getclassroomHtml?grade_id='+tag+'&school_id='+schoolid,
		success:function(classhtml)
		{ 				
			document.getElementById("classroomdrp").innerHTML= classhtml;
		}
		});
	}
}
<?php }?>
function select_f_date()
{
	var schoolid=''; var gradeid='';
	<?php if($records[0]['district_id']!='' ){?> 
	var schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value; 
	<?php } if($records[0]['district_id']!='' || $records[0]['observer_id']!=''){ ?>
 	var gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	<?php } ?>
 	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var classid = document.getElementById('class').options[document.getElementById('class').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	
	if(catid!=-1 && gradeid!=-1 && schoolid!=-1 && fDate!='' && tDate!='')
	{
		//$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/classroomgrowthgraph/getReportHtml?class_id='+classid+'&grade_id='+gradeid+'&school_id='+schoolid+'&cat_id='+catid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 		
			$("#loader").hide();
			//getGraph1(clounmValue);
			 var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
			 if(is_safari)
			 {
				window.location= '<?php echo base_url();?>classroomgrowthgraphbig';
			 }else
			 {
	 			window.open('<?php echo base_url();?>classroomgrowthgraphbig',"View Classroom Growth","directories=0, status=0, menubar=0, scrollbars=1, resizable=1,width=1300, height=800,top=100,left=100");
			 }	
		}
		});
	}
}

function select_t_date()
{
	var schoolid=''; var gradeid='';
	<?php if($records[0]['district_id']!='' ){?> 
	var schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value; 
	<?php } if($records[0]['district_id']!='' || $records[0]['observer_id']!=''){ ?>
 	var gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	<?php } ?>
	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var classid = document.getElementById('class').options[document.getElementById('class').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	
	if(catid!=-1 && gradeid!=-1 && schoolid!=-1 && fDate!='' && tDate!='')
	{
		//$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/classroomgrowthgraph/getReportHtml?class_id='+classid+'&grade_id='+gradeid+'&school_id='+schoolid+'&cat_id='+catid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 				
			$("#loader").hide();
			//getGraph1(clounmValue);				
			 var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
			 if(is_safari)
			 {
				window.location= '<?php echo base_url();?>classroomgrowthgraphbig';
			 }else
			 {
	 			window.open('<?php echo base_url();?>classroomgrowthgraphbig',"View Classroom Growth","directories=0, status=0, menubar=0, scrollbars=1, resizable=1,width=1300, height=800,top=100,left=100");
			 }
			//popupWindow.focus();
		}
		});
	}
}

function getGraph1(clounmValue)
 {
 //alert(clounmValue);

	$(function () {								
        //$('#container').highcharts({
		var options = {
           chart: {
		   		renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'View a Classrooms Growth'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: []  		
			              
            },
            yAxis: {
                min: 0,
				max: 100,
                title: {
                    text: 'score Point'
                }
            },
			 tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
	       series: [ ]
        }
        //alert(clounmValue);
		if(clounmValue!="")
		{
			var result = JSON.parse(clounmValue);
			//	alert(result);
			var Dispayname ='';
			var count = 0;
			for (var strDispayname in result) 
			{
				if(count == 0)
				{
					var dispN = result[strDispayname];
					
					for(var strDispN in dispN)
					{			
						if(strDispN != '')
							Dispayname = Dispayname + "'"+strDispN +"',"; 
					}
				}
				count++;
				
			}
			Dispayname = Dispayname.substr(0,Dispayname.length-1);
			//alert(Dispayname);
			eval("options.xAxis.categories = new Array("+Dispayname+");");
			
	 		options.series = new Array();		
			var result = JSON.parse(clounmValue);	
			var i=0;				
			for (var strTestName in result) 
			{
			 	options.series[i] = new Object();
				options.series[i].name = strTestName;
			
				var objTest = result[strTestName];
	 			 arrScore = new Array();
				var j=0;
				for(var strGrade in objTest)
				{			
					if(objTest[strGrade]==null)
						 objTest[strGrade] = 0;
					arrScore[j] = parseFloat(objTest[strGrade]);
	 				j++;
				}									
			   options.series[i].data = arrScore;
				i++;						 
			}
		} 
		chart = new Highcharts.Chart(options);
			
		});			
  }
   </script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroomgrowthgraph">View a Classroom's Growth</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->

                     <div class="widget purple">
                     <fieldset>
                         <div class="widget-title">
                                               
                             <h4>View a Classroom's Growth</h4>
                          
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                                   <div class="space20"></div>
                                   <div class="space15"></div>
                                   <table class="form-horizontal">
                                   <tr>
                                   <td>
                                   
                                   <div class="control-group">
                                             <label class="control-label">Select Category</label>
                                             <div class="controls">
<select class="span12 chzn-select" tabindex="1" style="width: 300px;" name="category" id="category" onchange="updatecategory(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
	        <?php 
		     foreach($records['catarray'] as $key => $value)
			{
				if(isset($_POST['category']) && $_POST['category']==$value['school_id'])
				{
				echo '<option value="'.$value['id'].'" selected = "selected">'.$value['cat_name'].'</option>';
				}
				else
				{
					echo '<option value="'.$value['id'].'">'.$value['cat_name'].'</option>';
			}
		}
        ?>
        </select>
            </div>
            </div> 
                  <?php if(@$records[0]['district_id']!=''){ ?>
                                         <div class="control-group">
                                             <label class="control-label">Select Classroom</label>
                                             <div class="controls">

<select class="span12 chzn-select" name="school" id="school" tabindex="1" style="width: 300px;" onchange="javascript: document.getElementById('grades').value=-1">
        <option value="-1"  selected="selected">-Please Select-</option>
       <?php 
		foreach($records['schoolarray'] as $key => $value)
				{
				if(isset($_POST['school']) && $_POST['school']==$value['school_id'])
				{
				echo '<option value="'.$value['school_id'].'" selected = "selected">'.$value['school_name'].'</option>';
				}
				else
				{
					echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
				}
			}
        ?>
</select>           
   </div>
</div> 

 <?php } if(@$records[0]['district_id']!='' || @$records[0]['observer_id']!=''){ ?>
 
              <div class="control-group">
                                             <label class="control-label">Select Grade :</label>
                                             <div class="controls">
 <select class="span12 chzn-select" tabindex="1" style="width: 300px;" name="grades" id="grades" onchange="updategrade(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
      	 <?php 
	    
		   foreach($records['gradearray'] as $key => $value)
			{
				if(isset($_POST['grades']) && $_POST['grades']==$value['dist_grade_id'])
				{
					echo '<option value="'.$value['dist_grade_id'].'" selected = "selected">'.$value['grade_name'].'</option>';
					
				}
				else
				{
					echo '<option value="'.$value['dist_grade_id'].'">'.$value['grade_name'].'</option>';
				}
			}
        ?>
</select> 
</div>
</div> 
  <?php } if($records[0]['teacher_id']!=''){?>

   <div class="control-group">
                                             <label class="control-label">Select Classroom : </label>
                                             <div class="controls">

<select class="span12 chzn-select" name="class" id="class" tabindex="1" style="width: 300px;" onchange="updateclass(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
        <?php 
	     foreach($records['classarray'] as $key => $value)
		{
			if(isset($_POST['class']) && $_POST['class']==$value['class_room_id'])
			{
				echo '<option value="'.$value['class_room_id'].'" selected = "selected">'.$value['name'].'</option>';
			}
			else
			{
				echo '<option value="'.$value['class_room_id'].'">'.$value['name'].'</option>';
			}
		}
        ?></select>
						</div>
                    </div> 
                   
                   <?php  } ?>   
                   <div id="classroomdrp"></div>
                   
                                      
                                         <div class="control-group">
                                             <label class="control-label">Select Time Period</label>
                                             <div class="controls">
                                        <div class="input-prepend">
                                        <label>From</label>
          <span class="add-on"><i class="icon-calendar"></i></span>
          <input value="" name="fromDate" style="width:80px" type="text" class=" m-ctrl-medium icon-calendar"  id="f_date"/>
		 <script type="text/javascript"> 
      	Calendar.setup({
        inputField : "f_date",
        trigger    : "f_date",
        onSelect   : function() { this.hide();
		select_f_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>
                                        </div>
                                    </div>
                                         <div class="controls">
                                        <div class="input-prepend">
								<label>To</label>
							<span class="add-on"><i class="icon-calendar"></i></span>
                 <input value="" name="toDate" style="width:80px" type="text" class=" m-ctrl-medium icon-calendar"  id="t_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "t_date",
        trigger    : "t_date",
        onSelect   : function() { this.hide();
		select_t_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>       
                                        </div>
                                    </div>
                                </div>
                                 </td></tr> 
                                </table>
                                
                                <br/><br/><br/>
                                          <div id="chartContainer">
 	  </div>
      <div id="loader" style="display:none" align="center"> <img src="<?php echo LOADERURL;?>"/></div>

    				 
                                
                                <div id="reportDiv"  style="display:block;" class="answer_list" >
                                    <div class="widget purple" id="reportwidget" style="display:none;">
                                        <div class="widget-title">
                                            <h4>Classroom Growth Report</h4>

                                        </div> 
                                       <div class="widget-body" style="min-height: 150px;" id="classroomgrwthrep" >



                                         <div class="space20"></div>

                                        <h3 style="text-align:center;">
                                        <div id="dialog" title="Student Details" style="display:none;"> Student Details </div>


                                        </h3>
                                        </div>
                                   </div>
                                </div>
                                   
                                   
                                      </fieldset>  

                                           
                           
                                   

          
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>

   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS -->

  
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
   
   
   
   
 
</body>
<!-- END BODY -->
</html>