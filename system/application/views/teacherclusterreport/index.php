<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
    <!--start old script -->
   
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/jscal2.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/lang/en.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>Quiz/css/jscal2.css" />
 <style type="text/css">
 table #innertab {
  table-layout: fixed;
    }
th {
  position:absolute;
   left:0; 
   height:23px;
 }

.outer {position:relative}
.inner {
  overflow-x:auto;
  overflow-y:hidden;
  margin-left:170px;
  width:875px;
}
.rotate_text
  {
	   float: left; 
		position: relative;
		-moz-transform: rotate(270deg);
		-moz-transform-origin: top left;  /* FF3.5+ */        
		-o-transform: rotate(270deg);  /* Opera 10.5 */  
		-o-transform-origin:  top left;
	   -webkit-transform: rotate(270deg);  /* Saf3.1+, Chrome */  
	   -webkit-transform-origin:top left;            
		filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=3);  /* IE6,IE7 */          
		-ms-filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3); /* IE8 */           
  }
  .rotated_cell
  {
	 height:100px;
	 vertical-align:bottom;
 	 white-space:nowrap;
	 
	 width:40px;
  }
  .rotated_cell_sn
  {
	 height:100px;
	 vertical-align:bottom;
   }
 </style>
<script type="text/javascript">

function updatecategory(tag)
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var test_cluster = document.getElementById('test_cluster').options[document.getElementById('test_cluster').selectedIndex].value;
 	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!=''&& test_cluster!=-1)
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/teacherclusterreport/getReportHtml?catid='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment+'&test_cluster='+test_cluster,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
		if(document.getElementById("csvbutton")!=undefined)
			document.getElementById("csvbutton").style.display="none";
	}
}
 function getTestcluster(tag)
{
	if(tag!=-1)
	{	
		$.ajax({
		url:'<?php echo base_url();?>/teacherclusterreport/getTestclusterhtml?assignment_id='+tag,
		success:function(classhtml)
		{ 	
			$("#testclusterdrp").html('&nbsp;');			
			document.getElementById("testclusterdrp").innerHTML= classhtml;
		}
		});
	}
}

function updateTestcluster(tag)
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var category = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
  
	if(tag!=-1 && category!=-1 && fDate!='' && tDate!='' && assessment!=-1)
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/teacherclusterreport/getReportHtml?catid='+category+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment+'&test_cluster='+tag,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
		if(document.getElementById("csvbutton")!=undefined)
			document.getElementById("csvbutton").style.display="none";
	}
}

function exporttocsv()
{	
	var tag = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var test_cluster = document.getElementById('test_cluster').options[document.getElementById('test_cluster').selectedIndex].value;
 	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='' && test_cluster!=-1)
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
	 	$.ajax({
		url:'<?php echo base_url();?>/teacherclusterreport/getCsv?catid='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment+'&test_cluster='+test_cluster,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#exportcsvlink").html(result);
		}
		});
	}
}
 function select_f_date()
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var test_cluster = document.getElementById('test_cluster').options[document.getElementById('test_cluster').selectedIndex].value;
 	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='' && test_cluster!=-1)
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/teacherclusterreport/getReportHtml?catid='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment+'&test_cluster='+test_cluster,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
		if(document.getElementById("csvbutton")!=undefined)
			document.getElementById("csvbutton").style.display="none";
	}
}
 function select_t_date()
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var test_cluster = document.getElementById('test_cluster').options[document.getElementById('test_cluster').selectedIndex].value;
 	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='' && test_cluster!=-1)
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/teacherclusterreport/getReportHtml?catid='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment+'&test_cluster='+test_cluster,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
		if(document.getElementById("csvbutton")!=undefined)
			document.getElementById("csvbutton").style.display="none";
	}
}
function exporttopdf()
{	
	var tag = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var test_cluster = document.getElementById('test_cluster').options[document.getElementById('test_cluster').selectedIndex].value;
 	if(tag!=-1 && assessment > 0 && fDate!='' && tDate!='' && test_cluster!=-1)
	{
		 var url = '<?php echo base_url();?>/teacherclusterreport/getPDF?catid='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment+'&test_cluster='+test_cluster;
                 window.open(url);
//		 window.location.href=url;
	}
}
function importlink()
{
	window.location.href = '<?php echo base_url();?>teacherarchivedata/'; 
}
   </script>
  
    <!--end old script -->
<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.12/video.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   	  <!--<a href="#jobaidesModal" data-toggle="modal" ><span>Job Aides</span>&nbsp;<i class="icon-folder-open"></i></a>-->
                      <a href="#myModal-video" data-toggle="modal" style="margin-right:5px;"><span>Video Tutorial</span>&nbsp;<i class="icon-play-circle"></i></a>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>teacherclusterreport">Class Performance Score by Standard</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                     <fieldset>
                         <div class="widget-title">
                            
                             <h4>Class Performance Score by Standard</h4>
                          
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                          
                          
                                   
                                   <div class="space20"></div>
                                   
                                   
                                   <div class="space15"></div>
                                   <table>
                                   <tr>
                                   <td>
							<form name="frmschoolreport" class="form-horizontal" action="teacherclustergraph" method="post" >  
                                   <div class="control-group">
                                             <label class="control-label">Select Category</label>
                                             <div class="controls">
		<select class="span12 chzn-select" name="category" id="category" tabindex="1" style="width: 300px;" onchange="updatecategory(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
        <option value="0" >All</option>
        <?php 
		
	     foreach($records['cat'] as $key => $value)
		{
			echo '<option value="'.$value['id'].'">'.$value['cat_name'].'</option>';
		}
        ?> 
        </select>
                                     </div>
                                         </div> 
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Assessment</label>
                                             <div class="controls">
<select class="span12 chzn-select" name="assessment" id="assessment" tabindex="1" style="width: 300px;" onchange="getTestcluster(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
        <option value="0" >All</option>
        <?php 
		foreach($records['Assessments'] as $key => $value)
			{
			echo '<option value="'.$value['id'].'">'.$value['assignment_name'].'</option>';
			}
        	?>
		</select>
    </div>
    </div> 
    
     								     <div class="control-group">
                                             <label class="control-label">Select Skill</label>
                                             <div class="controls" id="testclusterdrp">
                                      <select class="span12 chzn-select" tabindex="1" style="width: 300px;" name="test_cluster" id="test_cluster" >
										<option value="-1"  selected="selected">-Please Select-</option></select>
                                             </div>
                                         </div>   
    
    
    
                                
                                    <div class="control-group">
                                             <label class="control-label">Select Time Period</label>
                                             <div class="controls">
                                        <div class="input-prepend">
                                        <label>From</label>
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                            
           <input value="<?php if(isset($_POST['fromDate'])) echo $_POST['fromDate']; ?>" class=" m-ctrl-medium icon-calendar" name="fromDate" style="width:95px" type="text"  id="f_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "f_date",
        trigger    : "f_date",
        onSelect   : function() { this.hide();
		select_f_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>
                                        </div>
                                    </div>
                                
                       
                          <div class="controls">
                                        <div class="input-prepend">
								<label>To</label>
							<span class="add-on"><i class="icon-calendar"></i></span>
							<input value="<?php if(isset($_POST['toDate'])) echo $_POST['toDate']; ?>" name="toDate" class=" m-ctrl-medium icon-calendar" style="width:95px" type="text"  id="t_date"/>
<script type="text/javascript"> 
      Calendar.setup({
        inputField : "t_date",
        trigger    : "t_date",
        onSelect   : function() { this.hide();
		select_t_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>       
                                        </div>
                                    </div>
       </div>  
                                
                                
                                
                                         <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
                                             <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-purple dropdown-toggle">Select Report Type <span class="caret"></span></button>
                                     <ul class="dropdown-menu">                                         				                                  <!--      <li><a href="../tools/graphical-report.html">Graphical View</a></li>-->
                                         <li><input type="button" name="exportcsv" value="Generate CSV File" onclick="exporttocsv();"  /></li>                                          
                                         <li><input type="button" name="exportpdf" value="Generate PDF File" onclick="exporttopdf();"  /></li>
                                          <!-- <li><a href="../tools/archive-data.html">Archive Data</a></li>-->
										  <li><input type="button" name="" value="Archive Data" onclick="importlink();"  /></li>
                                         
                                     </ul>
                                 </div>
                                         </div>
                                             </div>
								  
                                   <td id="exportcsvlink">  </td>
      </tr>
       <tr>
      <td></td>
       <td valign="bottom" height="30" id="loader" colspan="3" style="display:none;"><img src="<?php echo LOADERURL;?>"/></td>
      </tr>
       
       <tr><font style="size:4">
     
       <td id="reporthtml" colspan="3" width="875px">  </td> </font>
      </tr>
   </table>
                	  </form>                 
                                <div class="space20"></div>
                                
                                                  
                           
                                   
                                      
                                     
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     </fieldset>
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->
   <div id="myModal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:700px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Class Performance Score by Standard</h3>
                                </div>
                              
                                <div class="modal-body">
                                        
                                    <video id="example_video_1" class="video-js vjs-default-skin"
  controls preload="auto" width="640" height="360"
 
  data-setup='{"example_option":true}'>
 <source src="<?php echo SITEURLM;?>convertedVideos/Class Assessment SummaryConverted.mp4" type='video/mp4' />
 
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
                                </div>
                                <div class="modal-footer">
 									<button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<!--<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>-->
                                </div>
                    
                            </div>
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}
jQuery('#myModal-video').on('hidden.bs.modal', function (e) {
                var myPlayer = videojs('example_video_1');
                myPlayer.pause();
              });
             
          $('#myModal-video').modal('show');
          var myPlayer = videojs('example_video_1');
        myPlayer.play();
     
</script>
   
   
   
   
 
</body>
<!-- END BODY -->
</html>