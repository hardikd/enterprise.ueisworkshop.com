<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Students::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/king.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content"> 
<!--<a href="parents/student_upload" class="htitle">Students Bulk Upload</a>-->		
		<br />
		<span class="htitle">kindergarten</span>
		<div id="studentdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input class="btnbig" type="button" name="student_add" id="student_add" title="Add New" value="Add New" >
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Add kindergarten" style="display:none;"> 

<form name='studentform' id='studentform' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>			
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>First Name:
				</td>
				<td valign="top">
				<input class='txtbox' type='text'  id='firstname' name='firstname'>
				<input  type='hidden'  id='student_id' name='student_id'>				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Last Name:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='lastname' name='lastname'>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>School Name:
				</td>
				<td valign="top" >
				
				<input class='txtbox' type='text'  id='schoolname' name='schoolname'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Subject:
				</td>
				<td valign="top" >
				<input  id='subject' name='subject' class='txtbox' />
				
				</td>
				
			</tr>
				
			
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Grade:
				</td>
				<td valign="top" >
				<input  id='grade' name='grade' class='txtbox' />
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Student Number:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='student_number' name='student_number'>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Total Marks:
				</td>
				<td valign="top" >
				
				<input class='txtbox' type='text'  id='totalmarks' name='totalmarks'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Obtained Marks:
				</td>
				<td valign="top" >
				
				<input class='txtbox' type='text'  id='obtainedmark' name='obtainedmark'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red"></font>Gender:
				</td>
				<td valign="top" >
				<input type='radio' name='gender' id="gender" value='male' />Male
				<input type='radio' name='gender' id="gender" value='female' />Female
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red"></font>Address (optional):
				</td>
				<td valign="top" >
				<input class='txtbox' type='text'  id='address' name='address'>
				
				
				</td>
				
			</tr>
			
			<!--<tr>
				<td valign="top" class="style1">
				<font color="red"></font>Phone Number (optional):
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='phone_number' name='phone_number'>
				
				</td>
				
			</tr>-->
			
			
						
<tr><td valign="top"></td><td valign="top"><input class="btnbig" type='submit' name="submit" id='studentadd' value='Add' title="Add New"> <input class="btnbig" type='button' name='cancel' id='cancel' value='Cancel' title="Cancel"></td></tr></table></form>
</div>
</body>
</html>
