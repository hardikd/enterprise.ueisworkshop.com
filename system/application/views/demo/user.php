<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::District::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.blockUI.js"></script>
<script type="text/javascript">
$(document).ready(function() { 
    
       
		if($('#days').val()==30)
		{
		$.blockUI({ message: $('#loginForm'),css: { 
                width: '600px' 
            }  }); 
        }
		else if($('#days').val()==90)
		{
			$.blockUI({ message: $('#validForm'),css: { 
                width: '600px' 
            }  });
		
		
		}
		
     $('#submit').click(function() { 
        if($('#feedback').val()=='')
		{
			alert('Please Enter Feedback');
		}
		else		
		{
		var surl='comments/send_feedback';
		selected={};
		selected.feedback=$('#feedback').val();
	 selected.dist_id=$('#dist_id').val();
	 selected.email=$('#email').val();
	
	 
	 $.post(surl,{'selected':selected},function(data) {
	    if(data.status==0)
		{
		
		alert('failed Please Try Again');
		}
		else
		{
		 alert('Feedback Submitted Please Login Again');
		  location='/';
		}
	 
	  },'json');
		
		
		}
});	 
    
}); 
</script>
</head>

<body>

<div class="wrapper">

	<div class="header">
    	<div class="logo"></div>
        <div class="book_img"></div>
        <div class="header_img"></div>
		<div class="user">
		<?php
		   echo "<b>Name: </b>".$this->session->userdata("demousername");
		   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Type: </b>District";
		    
		
		
		
		?>
		
		<div class="logout"> <a href="admin/logout/">Logout</a></div>
	   
		<!--<div onclick="toggleSubmenu(this)" class="mm"><a href="javascript:void(0);" >Options</a></div>-->
		
			
			
			
		
		
       
		
		</div>
        
    </div>
	
	
    <div class="mbody">
    	<?php require_once($view_path.'inc/demomenu.php'); ?>
        <div class="content">
        <table >
		<tr>
		<td>
		Welcome <?php echo $this->session->userdata('username');?>
		<input type="hidden" name="days" id="days" value="<?php echo $days;?>">
		</td>
		</tr>
		<!--<tr>
		<td align="center">
		<img src="<?php echo SITEURLM?>images/MainImage.jpg">
		</td>
		</tr>-->
		</table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="loginForm" style="display:none">
			<table width="100%" align="center">
			<tr><td align="center" colspan="2" class="htitle">FeedBack Form</td></tr>
			<tr><td width="20%">Email:</td><td align="left"><input style="border: 1px solid #70B8BA;" type="text" id="email" name="email" size="50" readonly value="<?php echo $this->session->userdata('demoemail')?>" />
			<input  type="hidden" id="dist_id" name="dist_id"  value="<?php echo $this->session->userdata('demodistrict_id')?>" />
			</td></tr>
			<tr><td width="20%">Feedback:</td><td align="left"><textarea style="width:200px;" id="feedback" name="feedback" /></textarea></td></tr>
			<tr><td align="center" colspan="2"><input type="submit" id="submit" name="submit" value="submit"></td></tr>
			</table>
            
        </div>
		<div id="validForm" style="display:none">
		<table width="100%" align="center">
			<tr><td align="center" colspan="2" class="htitle">Your trial subscription period of 90 has ended. Please convert your account to a regular account. </td></tr>
			
			</table>
		</div>

</body>
</html>
