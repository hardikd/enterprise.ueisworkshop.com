<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
<style type="text/css" media="screen">
	
	#paginationstudent  
	{
		padding-top:10px;
		font-size: 12px;
    font-weight: bolder;	
	}
	#paginationstudent a, #paginationstudent strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;


	}

	#paginationstudent strong, #paginationstudent a:hover {
	 font-weight: normal;
	 background: #657455;
	}		
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/headerv1.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">
<?php if($this->session->userdata('login_type')=='observer') { ?>
        
        <?php require_once($view_path.'inc/observermenu.php'); 
		}
		
		else
		{
					require_once($view_path.'inc/developmentmenu_new.php');
		}
		?>
          <!-- BEGIN SIDEBAR MENU -->
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                       Welcome <?php echo $this->session->userdata('username');?>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
        <?php if($this->session->userdata('login_special') == 'district_management'){?>
        <a href="<?php echo base_url();?>tools/data_tracker">Data Analysis Resources</a>
        <?php }else{?>
					         <a href="<?php echo base_url();?>index/userindex">Exam/Assessment Setup</a>	
			<?php }?>
                   
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                       <?php    if($this->session->userdata('login_type')=="observer") { ?>
							<a href="<?php echo base_url();?>trainingmaterial">Training Material</a>
                            <?php }else if($this->session->userdata('login_type')=="teacher") { ?>
                            <a href="<?php echo base_url();?>trainingmaterial">Training Material</a>
                            <?php   } if($this->session->userdata('login_type')=='user') { ?>
                            <a href="<?php echo base_url();?>trainingmaterial">Training Materials</a>
                            <?php }?>
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Training Material</h4>
                          
                         </div>
                        
                        <div class="widget-body">
 
 	
				<?php    if($this->session->userdata('login_type')=='user') { ?>
				 <form name="training_material" id="training_material" class="form-horizontal" method="post" enctype="multipart/form-data"action="trainingmaterial/uploadtrainingmaterial">
                                     <div class="control-group">
                                             <label class="control-label-required">Title</label>
                                             <div class="controls">
												<input style="width:300px;" class="span8 txtbox" type="text" name="title" id="title" />
						                        </div>
                                             </div>
                                            <div class="control-group">
                                             <label class="control-label-required">Description</label>
                                             <div class="controls">
                                            <textarea class="txtbox" name="description" id="description" rows="3" style="resize: none; width: 300px;"></textarea>
                                                 </div>
                                             </div>
                                             
                                    
                                          <div class="control-group">
                                    <label class="control-label-modal">Upload Document</label>
                                    <div class="controls">
                                              <input  type="file" name="file" id="file" />
                                            </div>
                                        </div>
                                        
                                         <div class="control-group">
                        			<div class="controls">
						<button type='button' name="uploaddoc" id='uploaddoc' value='Add' onclick="validatematerial()" class="btn btn-success"><i class="icon-plus"></i> Add Training Material</button>
                                            </div>
                                        </div>
                         
				</form>
				<?php 
	  }
	  ?>


				<?php if($this->session->userdata('login_type')=='user') { ?>

				<table class='table table-striped table-bordered' id='editable-sample'>

					<tbody>
						<tr>
							<th>Title</th>
							<th>Description</th>
							<th>Download</th>
							<th>Delete</th>
						</tr>
						<?php
		   	$this->load->model('Assesstesttypemodel');
		//   $docs = Trainingmaterialmodel::gettrainingmaterial();
		    if(count($docs)>0)
		    {
		    	$i=1;
		    	foreach($docs as $key => $value)
		    	{
		    		?>
						<tr
							class="<?php if($i%2==0){ echo "even"?>  <?php } else{  echo "odd";}?>">
							<td><?php echo $value['title']; ?></td>
							<td><?php echo $value['description']; ?></td>
                            
                         <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>    
							<td><a target="_new"
								href="<?php echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                            <?php 
						 }
						 else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){?>
						
                        <td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
						    <?php 
						 }
						 else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){?>
						
                        <td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>	
							
						<?php	 
						 }
						 					 
						 else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						 
						 else if($_SERVER["HTTP_HOST"]=="workshop2.ueisworkshop.com"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						     else if($_SERVER["HTTP_HOST"]=="localhost"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						 ?>
                         

<td>
 <button data-dismiss="modal" type="button" value="Delete"  class="btn btn-danger" onclick="deletedoc('<?php echo $value['doc_id'] ?>','<?php echo $value['filename'] ?>');"><i class="icon-trash"></i>Delete</button>
				

</td>

						</tr>

						<?php
						$i++;
		    	}
		    }
		    else
		    {
		    	?>
						<tr>
							<td colspan="4">No Records Found</td>
						</tr>
						<?php
		    }
		     
		    ?>
					</tbody>
				</table>
 <div id="paginationstudent">
        <?php echo $this->paginationnew->create_links(); ?>
        </div>
				<?php 
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {

	  	?>

				<table class='table table-striped table-bordered' id='editable-sample'>

					<tbody>
						<tr>
							<th>Title</th>
							<th>Description</th>
							<th>Download</th>
						</tr>
						<?php
		   	$this->load->model('Assesstesttypemodel');
		    //$docs = Trainingmaterialmodel::gettrainingmaterial();
		    if(count($docs)>0)
		    {
		    	$i=1;
		    	foreach($docs as $key => $value)
		    	{
		    		?>
						<tr
							class="<?php if($i%2==0){ echo "even"?>  <?php } else{  echo "odd";}?>">
							<td><?php echo $value['title']; ?></td>
							<td><?php echo $value['description']; ?></td>
							
                             
                             
                               <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>    
							<td><a target="_new"
								href="<?php echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                            <?php 
						 }
						 else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){?>
						
                        <td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
							     <?php 
						 }
						 else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){?>
						
                        <td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
						
						
						<?php	 
						 }
						 					 
						 else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						     else if($_SERVER["HTTP_HOST"]=="localhost"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						 ?>
                     
                     <!--       
                            <td><a target="_new"
								href="<?php //echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">

									<img src="<?php //echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php //echo $value['filename']; ?>" />
							</a>
							</td>-->
                            
                            


						</tr>

						<?php
						$i++;
		    	}
		    }
		    else
		    {
		    	?>
						<tr>
							<td colspan="3">No Record Found</td>
						</tr>
						<?php
		    }
		     
		    ?>
					</tbody>
				</table>
	 <div id="paginationstudent">
        <?php echo $this->paginationnew->create_links(); ?>
        </div>

				<?php
	  }
	  else if($this->session->userdata('login_type')=='teacher')
	  {

		  ?>

				<table class='table table-striped table-bordered' id='editable-sample'>

					<tbody>
						<tr>
							<th>Title</th>
							<th>Description</th>
							<th>Download</th>
						</tr>
						<?php
		   	$this->load->model('Assesstesttypemodel');
		  //  $docs = Trainingmaterialmodel::gettrainingmaterial();
		    if(count($docs)>0)
		    {
		    	$i=1;
		    	foreach($docs as $key => $value)
		    	{
		    		?>
						<tr
							class="<?php if($i%2==0){ echo "even"?>  <?php } else{  echo "odd";}?>">
							<td><?php echo $value['title']; ?></td>
							<td><?php echo $value['description']; ?></td>
				
                 
                               <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>    
							<td><a target="_new"
								href="<?php echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                            <?php 
						 }
						 else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){?>
						
                        <td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
						     <?php 
						 }
						 else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){?>
						
                        <td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>	
							
						<?php	 
						 }
						 					 
						 else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						     else if($_SERVER["HTTP_HOST"]=="localhost"){
						 ?>
                         	<td><a target="_new"
								href="<?php echo '/testbank/workshop/docfile/trainingmaterial/'.$value['filename']; ?>">
									<img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif"
									width="25" height="25" alt="<?php echo $value['filename']; ?>" /></a>
							</td>
                         <?php
						 }
						 ?>
                            
                           
						</tr>

						<?php
						$i++;
		    	}
		    }
		    else
		    {
		    	?>
						<tr>
							<td colspan="3">No Record Found</td>
						</tr>
						<?php
		    }
		     
		    ?>
					</tbody>
				</table>
 <div id="paginationstudent">
        <?php echo $this->paginationnew->create_links(); ?>
        </div>

				<?php
	  }
	  ?>
 
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <!--notification -->
 


        <!-- Add Document Popup Starts here-->
        
        
        

		<div id="fade" class="black_overlay"></div>
   <!-- notification ends -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.form.js"></script>


<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<LINK href="<?php echo SITEURLM?>css/buttonstyle.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script src="<?php echo SITEURLM.$view_path; ?>js/assessScoreall.js" type="text/javascript"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

<script type="text/javascript">
function validatematerial()
{

	var title = document.training_material.title.value;
	var desc = document.training_material.description.value;
	var file = document.training_material.file.value;
	var fileType=file.split('.');
	
	if(title=="")
	{
		$("#title").css('border-color','red');
		document.training_material.title.focus();
		return false;
	}
	else
	{
	$("#title").css('border-color','#70B8BA');	
	}
	
	
	if(desc=="")
	{
		$("#description").css('border-color','red');
		document.training_material.description.focus();
		return false;
	}
	else
	{
	$("#description").css('border-color','#70B8BA');	
	}
	
	if(file=="")
	{
		$("#file").css('border-color','red');
		document.training_material.file.focus();
		return false;
	}
	if(file)
	{ var j=0
	var filearray=new Array('pdf','docx');
	for(i=0;i<filearray.length;i++)
		{
			//alert(filearray[i]+fileType[fileType.length-1]);
			if(filearray[i]==fileType[fileType.length-1])
			{
				j++;	
			}
			
		}
		if(j)
		{
			//alert(j+'aa');
			//return true;
		}
		else
		{
			alert('Please select only pdf or docx files.');
			return false;
		}
		
		
	}
	else
	{
	$("#file").css('border-color','#70B8BA');	
	}
	
document.training_material.submit();
	
}
</script>
<script type="text/javascript">
function deletedoc(docid,filename)
{
	var data = confirm("Are you sure want to delete");
	if(data)
	{
	$.ajax({
		url:'<?php echo base_url(); ?>trainingmaterial/deletetrainingdoc?docsid='+docid+"&name="+filename,
		success:function(result)
		{
			window.location.reload();
		}
		});
	}
	else
	{
	}
}
</script>
   <!-- END JAVASCRIPTS --> 

   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>