<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
 <meta charset="utf-8" />
 <title>UEIS Workshop</title>
 <meta content="width=device-width, initial-scale=1.0" name="viewport" />
 <meta content="" name="description" />
 <meta content="" name="author" />
 <base href="<?php echo base_url();?>"/>
 <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
 <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
 <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
 <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
 <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
 <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
 <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

 <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
 <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style>
.icon-reorder {display:none !important;}
.sidebar-toggle-box {background:#5e3364 !important;}
th {text-align:left;}
.popoverinfo {font-size:18px !important;}
.infoicon {  position: absolute;
  margin-left: 155px;
  margin-top: 10px;
width:60%;}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
 <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
 <!-- END HEADER -->
 <!-- BEGIN CONTAINER -->
 <div id="container" class="row-fluid sidebar-closed">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll" style="display:none;">
    <div id="sidebar" class="nav-collapse collapse">


     <!-- BEGIN SIDEBAR MENU -->
     <?php if($this->session->userdata("login_type")!='user'){?>
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          <?php } else {?>
           <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
          <?php }?>
     <!-- END SIDEBAR MENU -->
   </div>
 </div>
 <!-- END SIDEBAR -->
 <!-- BEGIN PAGE -->  
 <div id="main-content">
   <!-- BEGIN PAGE CONTAINER-->
   <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->   
    <div class="row-fluid">
     <div class="span12">

       <!-- BEGIN PAGE TITLE & BREADCRUMB-->

       <h3 class="page-title">
         <i class="icon-book"></i>&nbsp; Create Workshop Account
       </h3>
       <ul class="breadcrumb" >

         <li>
           UEIS Workshop
           <span class="divider">&nbsp; | &nbsp;</span>
         </li>
       </ul>
       <!-- END PAGE TITLE & BREADCRUMB-->
     </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
     <div class="span12">
       <!-- BEGIN BLANK PAGE PORTLET-->


       <div class="widget green">
         <div class="widget-title">
           <h4><?php echo $type;?> Assign Grades & Class Scheduling</h4>

         </div>
         <div class="widget-body">
          <!--                            <form class="form-horizontal" action="#">-->
          <div id="pills" class="custom-wizard-pills-green4">
           <ul>
             <li><a href="#pills-tab1" data-toggle="tab" onclick="step(1);">Step 1</a></li>
             <li><a href="#pills-tab2" data-toggle="tab" id="pillsstep2" >Step 2</a></li>
             <li><a href="#pills-tab3" data-toggle="tab" id="pillsstep3" onclick="step(3);">Step 3</a></li>
           </ul>
           <div class="progress progress-success progress-striped active">
             <div class="bar"></div>
           </div>
           <div class="tab-content">

            <!-- BEGIN STEP 1-->


            <div class="tab-pane" id="pills-tab1">

            </div>
            <div class="tab-pane" id="pills-tab2">

            </div>

            <!-- Begin Step 2 -->
            <div class="tab-pane" id="pills-tab3">
            <div class="infoicon"><a id="popoverData" class="popoverinfo" href="#" data-content="Class schedule is a list of planned instruction-related activities." rel="popover" data-placement="bottom" data-original-title="Class Schedule" data-trigger="hover"><i class="icon-info-sign"></i></a></div>  
            <h3 style="color:#000000;">Class Schedule</h3>

            <form name="period_scheduling" id="period_scheduling" action="<?php echo base_url();?>teacherplan/add_schedule" method="post">
            <div style="text-align:center;">
            <!--<button type="submit" class="btn btn-large btn-green" name="save_schedule" value="save" >Save Schedule</button>-->
             <button class="btn btn-large btn-green" name="continue" value="continue">Save schedule & Continue</button>
           </div>
            <table cellspacing="5" cellpadding="15">
              <tr>
                <th>Period</th>
                <th>
                  Start Time
                </th>
                <th>
                  End Time
                </th>
                <th>
                  Grade
                </th>
                <th>
                  Subject
                </th>
                <th>
                  &nbsp;
                </th>
                </tr> 
              <tr>
                <td>
                  <label class="control-label-required">Period One</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp1" type="text" class="input-small" name="start_time_p1" value="08:00 AM">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep1" type="text" class="input-small" name="end_time_p1" value="09:00 AM">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select required class="span6 chzn-select gradechzn" id='grade_1' name='grade1' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select required class="span6 chzn-select" id='subject_1' name='subject1' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                  </div>
                </td>
                <td><a id="popoverData1" class="popoverinfo" href="#" data-content="Start and end times can be adjusted using the clock icon." rel="popover" data-placement="bottom" data-original-title="Period" data-trigger="hover"><i class="icon-info-sign"></i></a>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Two</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp2" type="text" class="input-small" name="start_time_p2" value="09:00 AM">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep2" type="text" class="input-small" name="end_time_p2" value="10:00 AM">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select required class="span6 chzn-select gradechzn" id='grade_2' name='grade2' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select required class="span6 chzn-select" id='subject_2' name='subject2' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Three</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp3" type="text" class="input-small" name="start_time_p3" value="10:00 AM">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep3" type="text" class="input-small" name="end_time_p3" value="11:00 AM">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select required class="span6 chzn-select gradechzn" id='grade_3' name='grade3' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select required class="span6 chzn-select" id='subject_3' name='subject3' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Four</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp4" type="text" class="input-small" name="start_time_p4">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep4" type="text" class="input-small" name="end_time_p4">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_4' name='grade4' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='subject_4' name='subject4' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Five</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp5" type="text" class="input-small" name="start_time_p5">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep5" type="text" class="input-small" name="end_time_p5">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_5' name='grade5' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select" id='subject_5' name='subject5' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Six</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp6" type="text" class="input-small" name="start_time_p6">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep6" type="text" class="input-small" name="end_time_p6">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_6' name='grade6' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select" id='subject_6' name='subject6' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Seven</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp7" type="text" class="input-small" name="start_time_p7">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep7" type="text" class="input-small" name="end_time_p7">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_7' name='grade7' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='subject_7' name='subject7' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Eight</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp8" type="text" class="input-small" name="start_time_p8">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep8" type="text" class="input-small" name="end_time_p8">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_8' name='grade8' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='subject_8' name='subject8' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Nine</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp9" type="text" class="input-small" name="start_time_p9">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep9" type="text" class="input-small" name="end_time_p9">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_9' name='grade9' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select" id='subject_9' name='subject9' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                      </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label class="control-label-required">Period Ten</label>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickersp10" type="text" class="input-small" name="start_time_p10">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="input-append bootstrap-timepicker">
                          <input id="timepickerep10" type="text" class="input-small" name="end_time_p10">
                          <span class="add-on"><i class="icon-time"></i></span>
                      </div>
                  </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select gradechzn" id='grade_10' name='grade10' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          <?php if($grades!=false)
                          {
                          foreach($grades as $gradeval)
                          {
                          ?>
                        <option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
                          <?php
                          }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                </td>
                <td>
                  <div class="controls">
                      <div class="">
                          <select class="span6 chzn-select" id='subject_10' name='subject10' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
                        <option value=''>-Please Select-</option>
                          
                        </select>
                      </div>
                      
                  </div>
                </td>
              </tr>
            </table>
            
            <div class="space20"></div>

           </div>
           <div style="text-align:center;">
            <!--<button type="submit" class="btn btn-large btn-green" name="save_schedule" value="save" >Save Schedule</button>-->
             <button class="btn btn-large btn-green" name="continue" value="continue">Save schedule & Continue</button>
           </div>
           <!-- End step 2 -->
</form>
         </div>
       </div>

       <!--<ul class="pager wizard">
         <li class="previous first green"><a href="javascript:;">First</a></li>
         <li class="previous green"><a href="javascript:;">Previous</a></li>
         <li class="next last green"><a href="javascript:;">Last</a></li>
         <li class="next green"><a  href="javascript:void(0);">Next</a></li>
       </ul>-->
     </div>
   </div>

 </div>
</div>
<!-- END BLANK PAGE PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
<!--      </div>
       END PAGE   
     </div>-->
     <!--notification -->


    <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
      <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
      </div>

      <div class="modal-footer" style="text-align:center;">
        <button data-dismiss="modal" class="btn btn-success" onclick="location.reload(true);">OK</button>
      </div>

      <!-- END POP UP CODE-->
    </div>

    <!-- BEGIN POP UP CODE -->

    <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
      <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
      </div>
      <div id="errmsg"></div>
      <div class="modal-footer" style="text-align:center;">
        <button data-dismiss="modal" class="btn btn-red">OK</button>
      </div>


    </div>
    <!-- END POP UP CODE-->
    <!-- notification ends -->
    <!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    <div id="footer">
      UEIS © Copyright 2012. All Rights Reserved.
    </div>
    <!-- END FOOTER -->

    <!-- BEGIN JAVASCRIPTS -->
    <!-- Load javascripts at bottom, this will reduce page load time -->
    <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
    <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>


    <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
    <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>

   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>


   <script>

$('.gradechzn').on('change',function(){
 var ids = $(this).attr('id');
       console.log(ids);
     $.ajax({
      type: 'post',
      url: base_url+'teacherplan/get_subject/'+$(this).val()
    })
     .done(function( data ) {
       data = jQuery.parseJSON(data);
       console.log(data);
       var idarr = ids.split('_');
       //console.log(idarr[1]);
       $.each(data,function(index,value){
        console.log(value);
                    $('#subject_'+idarr[1]).append('<option value="'+value.grade_subject_id+'">'+value.subject_name+'</option>');
              });
        $('#subject_'+idarr[1]).trigger("liszt:updated");
       //$('#subject_'+idarr[1]).html
    });
   });

$('#popoverData').popover({
   placement: 'right'
});
$('#popoverData1').popover({
   placement: 'right'
});
   var base_url = '<?php echo base_url();?>';
   jQuery(document).ready(function() {
     EditableTable.init();
   });
   
   $('#timepickerep1,#timepickersp2').timepicker({defaultTime:'09:00 AM',setTime:'09:00 AM'});
   $('#timepickerep2,#timepickersp3').timepicker({defaultTime:'10:00 AM',setTime:'10:00 AM'});
   $('#timepickerep3,#timepickersp4').timepicker({defaultTime:'11:00 AM',setTime:'11:00 AM'});
   $('#timepickerep4,#timepickersp5').timepicker({defaultTime:'12:00 PM',setTime:'12:00 PM'});
   $('#timepickerep5,#timepickersp6').timepicker({defaultTime:'01:00 PM',setTime:'01:00 PM'});
   $('#timepickerep6,#timepickersp7').timepicker({defaultTime:'02:00 PM',setTime:'02:00 PM'});
   $('#timepickerep7,#timepickersp8').timepicker({defaultTime:'03:00 PM',setTime:'03:00 PM'});
   $('#timepickerep8,#timepickersp9').timepicker({defaultTime:'04:00 PM',setTime:'04:00 PM'});
   $('#timepickerep9,#timepickersp10').timepicker({defaultTime:'05:00 PM',setTime:'05:00 PM'});
   $('#timepickerep10').timepicker({defaultTime:'06:00 PM',setTime:'06:00 PM'});
   function step(id){
    if(id==1){
      window.location.href = '<?php base_url();?>index/add_student';
    } else if(id==2) {
        window.location.href = '<?php base_url();?>period/add_schedule';
    } else if(id==3) {
      /*window.location.href = '<?php base_url();?>time_table/add_schedule';*/

    }
   }

    $(function () {
     $(" input[type=radio], input[type=checkbox]").uniform();
   });

   $('#add_period').click(function(){

     $.ajax({
      type: 'post',
      url: base_url+'period/add_period',
      data: $("#addperiod").serialize()
    })
     .done(function( data ) {
       data = jQuery.parseJSON(data);
       if (data.status==0){
        $('#errorbtn').modal('show');
      } else if (data.status==1){
        $('#myModal-add-new-parent').modal('hide');
        $('#successbtn').modal('show');
      }
    });
   });
   $('#edit_period').click(function(){

     $.ajax({
      type: 'post',
      url: base_url+'period/update_period',
      data: $("#editperiod").serialize()
    })
     .done(function( data ) {
       data = jQuery.parseJSON(data);
       if (data.status==0){
        $('#errorbtn').modal('show');
      } else if (data.status==1){
        $('#myModal-edit-room1').modal('hide');
        $('#successbtn').modal('show');
      }
    });
   });
   function getperiod(period_id){
     $.ajax({
      type: 'post',
      url: base_url+'period/getperiodinfo/'+period_id
    })
     .done(function( data ) {
       data = jQuery.parseJSON(data);
       $('#period_id').val(data.period.period_id);
       $('#timepicker8').val(data.period.start_time);
       $('#timepicker9').val(data.period.end_time);
       $('#school_id').val(data.period.school_id);
     });
   }
   function deletepreiod(period_id){
     $.ajax({
      type: 'post',
      url: base_url+'period/delete/'+period_id
    })
     .done(function( data ) {
       data = jQuery.parseJSON(data);
       if (data.status==0){
        $('#errorbtn').modal('show');
      } else if (data.status==1){
        $('#myModal-delete-student'+period_id).modal('hide');
        $('#successbtn').modal('show');
      }

    });
   }
   $('#timepicker8, #timepicker3').timepicker({defaultTime:'08:00 AM',setTime:'08:00 AM'});
   $('#timepicker9, #timepicker3').timepicker({defaultTime:'08:00 AM',setTime:'08:00 AM'});
   $("#pills").bootstrapWizard("show",2);
   </script>  
   <!-- END JAVASCRIPTS --> 

 </body>
 <!-- END BODY -->
 </html>