<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 

	<title>::Workshop::Home::</title>
	<base href="<?php echo base_url();?>"/>
	<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo SITEURLM;?>js/jquery.js"></script>
	<link rel="stylesheet" href="<?php echo SITEURLM?>css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo SITEURLM?>css/style_quiz.css" />
	<script src="<?php echo SITEURLM.$view_path; ?>js/selectusers.js" type="text/javascript"></script>
	</head>

	<body style="  line-height: 38px;">
<div class="wrapperhome">
      <?php require_once($view_path.'inc/headerWithoutLogin.php'); ?>
      <div class="mbody">
    <div class="content1">
          <div class="login_box" style="width:420px">
			 <form name="frm" method="post" action="index/prod_spec_login">
			 <div class="top_b"> </div>
              <div class="alert alert-info alert-login"> <u>Product Specialist Login</u> <br>
            <font color="red"></font> </div>
              <div class="cnt_b" style="width:93%">
			 <div class="formRow">
			<div class="input-prepend">
			<span class="add-on" style="width:87px;text-align:left;"><b>Districts:</b>	</span>		
			<select name="district" id="district" onchange="dist_user(this.value)">
			<?php if(!empty($districts)) { ?>
			<option value="">-Please Select a district-</option>
			<?php foreach($districts as $distvalue)
			{
			?>
			<option value="<?php echo $distvalue['district_id'];?>"><?php echo $distvalue['districts_name'];?></option>
			<?php } } else { ?>
			<option value="">-Please Select a district-</option>
			<?php } ?>
			</select>
			</div>
			</div>
			
			<div class="formRow">
			<div class="input-prepend">
			<span class="add-on" style="width:87px;text-align:left;"><b>District:	</b>	</span>	
			<select name="district_user" id="district_user">			
			<option value="">-Please Select a district user-</option>			
			</select>
			<input type="submit"  class='btn btn-inverse pull-right' style="float:none;margin-left:10px;" name="dist_login" value="Login" onclick="return DoSubmit('dist')">
			</div></div>
                  
                        <div class="formRow">
			<div class="input-prepend">
			<span class="add-on" style="width:87px;text-align:left;"><b>District CMS:	</b>	</span>	
			<select name="districtcms_user" id="districtcms_user">			
			<option value="">-Please Select a district user-</option>			
			</select>
			<input type="submit"  class='btn btn-inverse pull-right' style="float:none;margin-left:10px;" name="distcms_login" value="Login" onclick="return DoSubmit('dist_cms')">
			</div></div>
			
			<div class="formRow">
			<div class="input-prepend">
			<span class="add-on" style="width:87px;text-align:left;"><b>School:		</b>	</span>		
			<select name="school" id="school">			
			<option value="">-Please Select a school-</option>			
			</select>
			</div></div>
			
			<div class="formRow">
			<div class="input-prepend">
			<span class="add-on" style="width:87px;text-align:left;"><b>Observer:	</b>	</span>			
			<select name="observer" id="observer">			
			<option value="">-Please Select a observer-</option>			
			</select>
			<input type="submit"  name="ob_login" class='btn btn-inverse pull-right' style="float:none;margin-left:10px;" value="Login" onclick="return DoSubmit('ob')">
			</div></div>
			
			<div class="formRow">
			<div class="input-prepend">
			<span class="add-on" style="width:87px;text-align:left;"><b>Teacher:	</b>	</span>			
			<select name="teacher" id="teacher">			
			<option value="">-Please Select a teacher-</option>			
			</select>
			<input type="submit"  name="te_login" class='btn btn-inverse pull-right' style="float:none;margin-left:10px;" value="Login" onclick="return DoSubmit('te')">
			</div></div>
			
			
			</div>
        </form>
      </div>
        </div>
  </div>
      <?php require_once($view_path.'inc/footer.php'); ?>
    </div>


</body>
</html>
