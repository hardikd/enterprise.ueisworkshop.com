<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>UEIS Workshop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <base href="<?php echo base_url(); ?>"/>
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

        <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php require_once($view_path . 'inc/header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar-scroll">
                <div id="sidebar" class="nav-collapse collapse">


                    <!-- BEGIN SIDEBAR MENU -->
                    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE --> 
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->   
                    <div class="row-fluid">
                        <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <h3 class="page-title">
                                <i class="icon-wrench"></i>&nbsp; Access Content & Campus Data
                            </h3>
                            <ul class="breadcrumb" >

                                <li>
                                    UEIS Workshop
                                    <span class="divider">&nbsp; | &nbsp;</span>
                                </li>

                                <li>
                                    <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>tools">Tools & Resources</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>index/acces_campus_data">Access Content & Campus Data</a>
                                </li>







                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN BLANK PAGE PORTLET-->
                            <div class="widget purple">
                                <div class="widget-title">
                                    <h4>Planning Manager</h4>

                                </div>
                                <div class="widget-body">

                                    <form  class="form-horizontal" action='index/access_campus_data' name="report" method="post" onsubmit="return check()">

                                            <div class="control-group">
                                                <label class="control-label">Select School</label>
                                                <div class="controls">
                                                    <select class="span12 chzn-select" name="school_id" id="school_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value="">-Please Select-</option>
                                                        <?php foreach ($schools as $school): ?>
                                                            <option value="<?php echo $school['school_id']; ?>"><?php echo $school['school_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label">Select User</label>
                                                <div class="controls">
                                                    <select class="span12 chzn-select" name="observer_id" id="observer_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value="">-Please Select-</option>
                                                    </select>
                                                </div>
                                            </div> 

                                        <div class="control-group">
                                            <label class="control-label"></label>
                                            <div class="controls">
                                                <input type="submit" class="btn btn-small btn-purple" name="submit" value="Login"  style=" padding: 5px; " />          
                                            </div>    </div>


                                    </form>

                                </div>





                            </div>
                        </div>



                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- END BLANK PAGE PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->  
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div id="footer">
    UEIS © Copyright 2012. All Rights Reserved.
</div>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>


<script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
<!-- ie8 fixes -->
<!--[if lt IE 9]>
<script src="js/excanvas.js"></script>
<script src="js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>



<!--common script for all pages-->
<script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
<!--script for this page-->
<script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>
<script src="<?php echo SITEURLM ?>js/form-component.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/DT_bootstrap.js"></script>

<!--script for this page only-->
<!--old script -->
<script type="text/javascript">

                                                    function check()
                                                    {
                                                        <?php if ($this->session->userdata('login_type') == 'user'): ?>
                                                                if ($('#school_id').val()==''){
                                                                    alert('Please select School');
                                                                    return false;
                                                                }
                                                                if ($('#teacher_id').val()==''){
                                                                    alert('Please select teacher');
                                                                    return false;
                                                                }
                                                        <?php endif;?>
                                                        if (document.getElementById('year').value == '')
                                                        {
                                                            alert('Please Select Year');
                                                            return false;

                                                        }
                                                        else
                                                        {
                                                            return true;

                                                        }
                                                    }
</script>
<!--end old script -->


<script>
    $(function() {
        $(" input[type=radio], input[type=checkbox]").uniform();
    });
    
    var base_url = '<?php echo base_url();?>';

 <?php if ($this->session->userdata('login_type') == 'user'): ?>
     $('#school_id').on('change',function(){
         $.ajax({
            type: "POST",
            url: base_url+"teacherself/getobserverBySchoolId",
            data: { school_id: $('#school_id').val()}
          })
            .done(function( result) {
//              console.log(teachers);
                $('#observer_id').html('');
                
              var teachers = jQuery.parseJSON(result);
              $.each(teachers,function(index,value){
                    console.log(value);
                    $('#observer_id').append('<option value="'+value.observer_id+'">'+value.observer_name+'</option>');
              });
              $('#observer_id').trigger("liszt:updated");
            });
     });
 <?php endif;?>

</script>  


</body>
<!-- END BODY -->
</html>