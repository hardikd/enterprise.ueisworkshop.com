<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Product Specialists::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/prod_spec.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
        <div id="plandetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New" class="btnbig" type="button" name="plan_add" id="plan_add" value="Add New" >
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Product Specialist" style="display:none;"> 

<form name='planform' id='planform' method='post' onsubmit="return false" >
<table cellpadding="0" cellspacing="10" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>First Name:
				</td>
				<td valign="top">
				<input class='txtbox' type='text'  id='firstname' name='firstname'>
				<input  type='hidden'  id='plan_id' name='plan_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Last Name:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='lastname' name='lastname'>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>User Name:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='userid' name='userid'>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Password:
				</td>
				<td valign="top" >
				<input class="txtbox" type='password'  id='password' name='password'>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				Email:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='email' name='email'>
				
				</td>
				
			</tr>
			
				
				
			
						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New" class="btnbig" type='submit' name="submit" id='planadd' value='Add' > <input title="Cancel" class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
