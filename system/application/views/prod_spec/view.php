<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Product Specialist Login::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script language="javascript">
	// remove multiple, leading or trailing spaces
	function trim(s) {
		s = s.replace(/(^\s*)|(\s*$)/gi,"");
		s = s.replace(/[ ]{2,}/gi," ");
		s = s.replace(/\n /,"\n");
		return s;
	}
	
	 
	
	function submit_login(){
		if(trim(document.frmLogin.username.value)==""){
			alert("Please enter your username.");
			return false;
		}
		
		if(trim(document.frmLogin.password.value)==""){
			alert("Please enter your password.");
			return false;
		}
		
		return true;
	}
	</script>
</head>

<body>
	
	<div class="wrapperhome">

	<?php require_once($view_path.'inc/headerWithoutLogin.php'); ?>
    <div class="mbody">
    	
        <div class="content1">
        
		<table class="hometab" cellpadding="2">
        <form id="nwfrmLogin" name="frmLogin" action="index/product_specialist" method="post">
		<tr>
		<td  colspan="2" align="center">
		Product Specialist Login
		</td>
		</tr>
		<tr>
		<td>
		Username:
		</td>
		<td>
		<input class="txtbox" name="username" type="text"  value="<?php if(isset($username)) {echo $username;}?>" />
		</td>
		</tr>
		<tr>
		<td>
		Password:
		</td>
		<td>
		<input class="txtbox" type="password" name="password" >
		</td>
		</tr>
		
		<tr>
		<td colspan="2" class="error">
		<?php if(isset($error_msg)) {echo $error_msg; }?>
		</td>
		</tr><tr><td></td><td ><input class="btnbig" type="submit"  id="nwlogbtn" name="submit" value="Submit" onclick="return submit_login();"></td></tr>
				   
        </form>
		</table>
		
        </div>
    </div>
   <?php require_once($view_path.'inc/footer.php'); ?>

</div>
	<!--<div >Admin Login</div>
                   
                    <form id="nwfrmLogin" name="frmLogin" action="admin/home" method="post"><br />
                    User Name:<input   id="username" name="username" name="" type="text" value="<?php if(isset($username)) { echo $username; }?>"/><br />
                    Password :<input  id="password" name="password" type="password" /><br />
					<?php if(isset($error_msg)) {echo $error_msg; }?>
                    <input type="submit"  id="nwlogbtn" name="submit" value="submit" onclick="return submit_login();">
				   
                  </form>-->
				 
</body>
</html>