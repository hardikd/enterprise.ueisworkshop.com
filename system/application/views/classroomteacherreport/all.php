<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::District::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
 

function showSchools(stid)
{
	var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	
	$.ajax({
	type:'post',
	url: path,
	success:function(result)
	{
		$('#school_name_id').html(result);
	}
	
	});	
}

function checkfields()
{
//var schooltype = document.single_test_report.school_type.value;
var classname = document.single_test_report.school_type.value;

 if(classname=="")
{
alert('Select School Type');
}
else
{
document.single_test_report.submit();
}



}
</script>


</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <table align="center">
		<tr>
		<td >
		<input type="hidden" name="countries" id="countries" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
		<input type="hidden" name="states" id="states" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
		<input type="hidden" name="district" id="district" value="<?php echo $this->session->userdata('district_id') ?>" >
		</td>
		
		</tr>
		
		
		</table>
		<table>
		<tr>
		<td colspan="2" align="right">
<!--		<h3> District: Comparison of All Schools by Grade </h3>-->
        <h3>Assessment Completion Roster </h3>
		</td>
		</tr>
		</table>
		<div id="teacherdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
	<!--	<input class="btnbig" type="button" name="teacher_add" id="teacher_add" title="Add New" value="Add New" > -->
		</div>
		

		<form  action='classroomteacherreport/outputfun' name="single_test_report" method="post" >
			<table cellpadding="5">			
            
            <tr>
			<td>
			 School Type:  
			 </td>
			  <td>

              <select class="combobox" name="school_type" id="school_type" onchange="showSchools(this.value)">
              <option value="">-Please Select-</option>
                 <?php
			 foreach($schools_type as $val)
			 {
				 $schooltypeid = $val['school_type_id'];
				 $name = $val['tab'];
				 ?>
                 <option value="<?php echo $schooltypeid;?>"><?php echo $name;?></option>
                 <?php
			 }
			  ?>
			  
			  
           
			  
			  </select>
			  </td>
			  
			</tr>
             <tr>
			<td>
			 Select School:  
			 </td>
			  <td>

              <select class="combobox" name="school_name_id" id="school_name_id" >
              <option value="">-Please Select-</option>			  
           
			  </select>
			  </td>
			  
			</tr>
			
			<?Php /* ?>
            <tr>
			<td>
			 Grades:
			  </td>
			  <td>
			  <select class="combobox" name="grade" id="grade" >
			  <option value="">-Please Select-</option>
                <?php
			 foreach($grades as $grade)
			 {
				 
				 $gradeid = $grade['dist_grade_id'];
				 $gradename = $grade['grade_name'];
				 ?>
                 <option value="<?php echo $gradeid;?>"><?php echo $gradename;?></option>
                 <?php
			 }
			  ?>
              			  
			  </select>
			  </td>
			  
			</tr>
			<?php */ ?>
   
			<tr>			
			<td>
			</td>
<td ><input title="Get Report" class="btnbig" type="button" name="submitform" value="School Report" onclick="checkfields();"></td>
			</tr>
	</table>
   </form>
		
<!-- Graph -->

<?Php

//echo '<pre>';
//print_r($allschools);



if(!empty($allschools))
{
	$valarry =  array() ;		  // if empty  
	$schoolnamearr =  array();    // school name array
	$avgarr = array();            // for pie graph 
	
	?>
	<table>
		<tr>
			<td colspan="2" align="right">
				<h3> 
				<?php
				   		echo 'Comparison of All Schools by Grade -> &nbsp';
						echo $cid_resut_1 = $this->classroommodel->fetchGradeName(@$gradeid1); // Grade Name
						echo '&nbsp ->  &nbsp According Current District School Type ';
				?>
				</h3>
			</td>
		</tr>
</table>
	<?php
	
	// 1 school
		
		if(!empty($allschools[0]))
		{
				$allschools[0][0]['school_id'];				
				$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[0][0]['school_id']);
				
				$schoolTestAvg = array();
				
				foreach($allschools[0] as $key=>$val)
				{
					$UserID = $allschools[0][$key]['UserID'];					
					// 1 school student avr					
					$schoolTestAvg[] = $this->classroommodel->schoolStdAvg($UserID);									
				}
				
	//print_r($schoolTestAvg);
				
				// sc1 st1
				if(!empty($schoolTestAvg[0][0]['avg(pass_score_perc)']))
				{
					$sc1st1 = $schoolTestAvg[0][0]['avg(pass_score_perc)'];				
				}
				else
				{
					$sc1st1 = 0;
				}
				
				// sc1 st2
				if(!empty($schoolTestAvg[1][0]['avg(pass_score_perc)']))
				{
					$sc1st2 = $schoolTestAvg[1][0]['avg(pass_score_perc)'];
				}
				else
				{
					$sc1st2 = 0;
				}
				//sc1 st3
				
				if(!empty($schoolTestAvg[2][0]['avg(pass_score_perc)']))
				{
					$sc1st3 = $schoolTestAvg[2][0]['avg(pass_score_perc)'];				
				}
				else
				{
					$sc1st3 = 0;
				}
				
				
				
					$avg_1 = round(($sc1st1+$sc1st2+$sc1st3)/3);     // avrage line record
					$avgarr[] = $avg_1;	
		}
		else
		{
			$avg_1 =0;
			$avgarr[] =0;
			$schoolnamearr[] ='';
			$valarry[]=1;
			$sc1st1 =0;
			$sc1st2=0;
			$sc1st3 = 0;
		}
		
	// 2 school
		if(!empty($allschools[1]))
		{
		
		
		
			$allschools[1][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[1][0]['school_id']);
			
		
			$schoolTestAvgsc2 = array();
				
				foreach($allschools[1] as $key=>$val)
				{
					$UserID = $allschools[1][$key]['UserID'];					
					// 1 school student avr					
					$schoolTestAvgsc2[] = $this->classroommodel->schoolStdAvg($UserID);									
				}
					
	//print_r($schoolTestAvg);
				
				// sc1 st1
				if(!empty($schoolTestAvgsc2[0][0]['avg(pass_score_perc)']))
				{
					$sc2st1 = round($schoolTestAvgsc2[0][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc2st1 = 0;
				}
				
				// sc1 st2
				if(!empty($schoolTestAvgsc2[1][0]['avg(pass_score_perc)']))
				{
					$sc2st2 = round($schoolTestAvgsc2[1][0]['avg(pass_score_perc)']);
				}
				else
				{
					$sc2st2 = 0;
				}
				//sc1 st3
				
				if(!empty($schoolTestAvgsc2[2][0]['avg(pass_score_perc)']))
				{
					$sc2st3 = round($schoolTestAvgsc2[2][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc2st3 = 0;
				}
				
				$avg_2 = round(($sc2st1+$sc2st2+$sc2st3)/3);     // avrage line record
					$avgarr[] = $avg_2;	
			
		}
		else
		{
			$avg_2 =0;
		$avgarr[] =0;
			$schoolnamearr[] ='';
		$valarry[]=2;
			$sc2st1 =0;
			$sc2st2=0;
			$sc2st3 = 0;
		}
		
	// 3 school
	
	if(!empty($allschools[2]))
		{
			$allschools[2][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[2][0]['school_id']);
			
				$schoolTestAvgsc3 = array();
				
				foreach($allschools[2] as $key=>$val)
				{
					$UserID = $allschools[2][$key]['UserID'];					
					// 1 school student avr					
					$schoolTestAvgsc3[] = $this->classroommodel->schoolStdAvg($UserID);									
				}
				
	//print_r($schoolTestAvg);
				
				// sc1 st1
				if(!empty($schoolTestAvgsc3[0][0]['avg(pass_score_perc)']))
				{
					$sc3st1 = round($schoolTestAvgsc3[0][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc3st1 = 0;
				}
				
				// sc1 st2
				if(!empty($schoolTestAvgsc3[1][0]['avg(pass_score_perc)']))
				{
					$sc3st2 = round($schoolTestAvgsc3[1][0]['avg(pass_score_perc)']);
				}
				else
				{
					$sc3st2 = 0;
				}
				//sc1 st3
				
				if(!empty($schoolTestAvgsc3[2][0]['avg(pass_score_perc)']))
				{
					$sc3st3 = round($schoolTestAvgsc3[2][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc3st3 = 0;
				}
				
					$avg_3 = round(($sc3st1+$sc3st2+$sc3st3)/3);     // avrage line record
					$avgarr[] = $avg_3;	
		}
		else
		{
			$avg_3 =0;
		$avgarr[] =0;
			$schoolnamearr[] ='';
		$valarry[]=3;
			$sc3st1 =0;
			$sc3st2=0;
			$sc3st3 = 0;
		}
	// 4 school
		if(!empty($allschools[3]))
		{
			$allschools[3][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[3][0]['school_id']);
			
			$schoolTestAvgsc4 = array();
				
				foreach($allschools[3] as $key=>$val)
				{
					$UserID = $allschools[3][$key]['UserID'];					
					// 1 school student avr					
					$schoolTestAvgsc4[] = $this->classroommodel->schoolStdAvg($UserID);									
				}
				
	//print_r($schoolTestAvg);
				
				// sc1 st1
				if(!empty($schoolTestAvgsc4[0][0]['avg(pass_score_perc)']))
				{
					$sc4st1 = round($schoolTestAvgsc4[0][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc4st1 = 0;
				}
				
				// sc1 st2
				if(!empty($schoolTestAvgsc4[1][0]['avg(pass_score_perc)']))
				{
					$sc4st2 = round($schoolTestAvgsc4[1][0]['avg(pass_score_perc)']);
				}
				else
				{
					$sc4st2 = 0;
				}
				//sc1 st3
				
				if(!empty($schoolTestAvgsc4[2][0]['avg(pass_score_perc)']))
				{
					$sc4st3 = round($schoolTestAvgsc4[2][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc4st3 = 0;
				}
				
				$avg_4 = round(($sc4st1+$sc4st2+$sc4st3)/3);     // avrage line record
					$avgarr[] = $avg_4;	
		}
		else
		{
			$avg_4 = 0;
			$avgarr[] =0;
			$schoolnamearr[] ='';
			$valarry[]=4;
			$sc4st1 =0;
			$sc4st2=0;
			$sc4st3 = 0;
		}
	// 5 school
		if(!empty($allschools[4]))
		{
			$allschools[4][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[4][0]['school_id']);
			
					
			$schoolTestAvgsc5 = array();
				
				foreach($allschools[4] as $key=>$val)
				{
					$UserID = $allschools[4][$key]['UserID'];					
					// 1 school student avr					
					$schoolTestAvgsc5[] = $this->classroommodel->schoolStdAvg($UserID);									
				}
				
	//print_r($schoolTestAvg);
				
				// sc1 st1
				if(!empty($schoolTestAvgsc5[0][0]['avg(pass_score_perc)']))
				{
					$sc5st1 = round($schoolTestAvgsc5[0][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc5st1 = 0;
				}
				
				// sc1 st2
				if(!empty($schoolTestAvgsc5[1][0]['avg(pass_score_perc)']))
				{
					$sc5st2 = round($schoolTestAvgsc5[1][0]['avg(pass_score_perc)']);
				}
				else
				{
					$sc5st2 = 0;
				}
				//sc1 st3
				
				if(!empty($schoolTestAvgsc5[2][0]['avg(pass_score_perc)']))
				{
					$sc5st3 = round($schoolTestAvgsc5[2][0]['avg(pass_score_perc)']);				
				}
				else
				{
					$sc5st3 = 0;
				}
				
				$avg_5 = round(($sc5st1+$sc5st2+$sc5st3)/3);     // avrage line record
					$avgarr[] = $avg_5;	
			
		}
		else
		{
			$avg_5 = 0;
			$avgarr[] =0;
			$schoolnamearr[] ='';
			$valarry[]=5;
			$sc5st1 =0;
			$sc5st2=0;
			$sc5st3 = 0;
		}

		
		//echo 'avrag val';
		
		//print_r($avgarr);
	//	print_r($schoolnamearr);
		
	//	echo 'val====='.$sc2st1;
		
	

	if(count($valarry)<5)
	{
$numarr = array();
$namearr = array();
$uidarr = array();

// pie graph work 
	
	//print_r($avgarr);
	
	
			$adv = array();
			$pro = array();
			$basic = array();
			$bbasic = array();
			
	for($i=0;$i<count($avgarr);$i++)
		{
				$num = $avgarr[$i];
				echo '<br>';
				if($num>=0 && $num<40)
				{					
					$bbasic[] = $num;					
				}
				else if($num>=40 && $num<77)
				{
					 $basic[] = $num;
				}
				if($num>=77 && $num<100)
				{
					$pro[] = $num;
				}
				
		}
				//1
			
		 $ttarr = array_sum($bbasic);
		 
		  if($ttarr==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			  $avgbbbasic = $ttarr/$total;
			 }
		  
		 // 2
		  	 $ttarr1 = array_sum($basic);	
		  
		  	 if($ttarr1==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $ttarr1/$total1;
			 }
		 //3
		 
		  $ttarr2 = array_sum($pro);		 
		 if($ttarr2==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
				  $avgpro = $ttarr2/$total2;
			 }
	
	
	
 
//print_r($numarr);
$ncat = implode("','",$schoolnamearr);

$ncat = "['".$ncat."']";

/*
//1
echo '1=>';
echo ','.$sc1st1;
echo ','.$sc1st2;
echo ','.$sc1st3;
echo '2=>';
echo ','.$sc2st1;
echo ','.$sc2st2;
echo ','.$sc2st3;
echo '3=>';
echo ','.$sc3st1;
echo ','.$sc3st2;
echo ','.$sc3st3;
echo '4=>';
echo ','.$sc4st1;
echo ','.$sc4st2;
echo ','.$sc4st3;
echo '5=>';
echo ','.$sc5st1;
echo ','.$sc5st2;
echo ','.$sc5st3;
*/

?>

<b><font color="#2f7ed8">Basic = <?Php echo @$avgbbbasic; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#0d233a">Basic Blow =  <?Php echo @$avgbasic; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#8bbc21">Proficient =<?Php echo @$avgpro; ?>% </font></b>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: <?Php echo $ncat; ?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' %';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
			
            labels: {
                items: [{
                    html: 'Compare student performance',
                    style: {
                        left: '350px',
                        top: '10px',
                        color: 'black'
                    }
                },{
                    html: 'Blow=<?Php echo @$avgbbbasic; ?>%',
                    style: {
                        left: '190px',
                        top: '0px',
                        color: '#2f7ed8'
                    }
                },{				
                    html: 'Basic Basic=<?Php echo @$avgbasic; ?>%',
                    style: {
                        left: '190px',
                        top: '15px',
                        color: '#0d233a'
                    }
                },{
				
                    html: 'Proficient=<?Php echo @$avgpro; ?>%',
                    style: {
                        left: '190px',
                        top: '30px',
                        color: '#8bbc21'
                    }
                }
				
				]
            },
            series: [{
                type: 'column',
                name: 'Test1',
                data: [<?Php echo @$sc1st1; ?>, <?Php echo @$sc2st1; ?>, <?Php echo @$sc3st1; ?>, <?Php echo @$sc4st1; ?>, <?Php echo @$sc5st1; ?>]
            }, {
                type: 'column',
                name: 'Test2',
                data: [<?Php echo @$sc1st2; ?>, <?Php echo @$sc2st2; ?>, <?Php echo @$sc3st2; ?>, <?Php echo @$sc4st2; ?>, <?Php echo @$sc5st2; ?>]
            }, {
                type: 'column',
                name: 'Test3',
                data: [<?Php echo @$sc1st3; ?>, <?Php echo @$sc2st3; ?>, <?Php echo @$sc3st3; ?>, <?Php echo @$sc4st3; ?>, <?Php echo @$sc5st3; ?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [<?php echo @$avg_1; ?>, <?php echo @$avg_2; ?>, <?php echo @$avg_3; ?>, <?php echo @$avg_4; ?>, <?php echo @$avg_5; ?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }, {
                type: 'pie',
                name: 'Total consumption',
                data: [{
                    name: 'Below Basic',
                    y: <?Php echo @$avgbbbasic; ?>,
                    color: Highcharts.getOptions().colors[0] // Below Basic's color
                }, {
                    name: 'Basic Scores ',
                    y: <?Php echo @$avgbasic; ?>,
                    color: Highcharts.getOptions().colors[1] // Basic's color
                }, {
                    name: 'Proficient',
                    y: <?Php echo @$avgpro; ?>,
                    color: Highcharts.getOptions().colors[2] // Proficient's color
                }],
                center: [50, 10],
                size: 100,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    });
    

		</script>
	
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
		<!-- End Graph -->			
		<?Php 
		}  // count if END
		else
		{		
			if(isset($allschools))
			{
		?>
			<script type="text/javascript">
			alert("No Record Found");
			</script>
		<?php
			}
			echo $this->session->flashdata('item');			
		
		}

	}  // Main if end 
	
	
		
		
		?>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
