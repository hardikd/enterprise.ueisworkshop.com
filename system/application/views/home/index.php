<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
            <title>UEIS Workshop</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />

	
	<base href="<?php echo base_url();?>"/>
        <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<!--   <link href="<?php echo SITEURLM?>css_new/style-default.css" rel="stylesheet" id="style_color" />-->
<!--	<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />-->
<!--    	<script type="text/javascript" src="<?php echo SITEURLM;?>js/jquery.js"></script>-->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
	<script language="javascript">
function submit_home_login()
{
		
		if(document.frmLogin.username.value==""){
			alert("Please Enter Your Username.");
			document.frmLogin.username.focus();
			return false;
		}
		
		if(document.frmLogin.password.value==""){
			alert("Please enter your Password.");
			document.frmLogin.password.focus();
			return false;
		}
		
		return true;
	}
</script>

	<script language = "javascript">
            
function restore_password()
{
	
	var email = document.forgot_pass.email_address.value;
	var loginas = document.forgot_pass.loginas.value;
	
	
	
	  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if (email =="") {
		$("#email_error").html('Please enter email address').css('color','#F00');
		document.forgot_pass.email_address.focus();
		return false;
	 }
    if (!filter.test(email)) {
    $("#email_error").html('Please provide a valid email address').css('color','#F00');
    document.forgot_pass.email_address.focus();
    return false;
 }
	
		var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	
	$.ajax({
		type:'post',
		url:path,
		success:function(result)
		{
		
			if(result=='invalid email address')
			{
				$("#msg_success").html("Invalid email").css('color','#F00');
			}
			else if(result=='emailsendingfailed')
			{
				$("#msg_success").html("Email Sending Failed").css('color','#F00');
			}
			else if(result=='recordnotfound')
			{
				$("#msg_success").html("Record Not Found.").css('color','#F00');
			}
			else
			{
				$("#msg_success").html("Email sent successfully...").css('color','#060');
			}
			
			
			
			}
		
		});
		
}
</script>
	<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
</script>
        	<style>
.black_overlay {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content {
	background-color: white;
	border: 5px solid #677659;
	height: auto;
	left: 36%;
	overflow: auto;
	padding: 1px;
	position: fixed;
	top: 26%;
	width: auto;
	z-index: 1002;
}
</style>
	</head>

	<body class="login">
            
            <div class="error-wrap error-wrap-404">
                
<!-- BEGIN LOGO -->
     <div class="lock-header">
         <span style="color:red; margin-left:50px;"> 
                    <?php echo $this->session->flashdata('parentlogout');?>
                    <?php echo $this->session->flashdata('invalidwap');?>
                    <?php if(isset($error_msg)) {echo $error_msg; }?>
                </span>
        <!-- BEGIN LOGO -->
        <a class="center" id="logo" href="<?php echo base_url();?>">
            <img class="center" alt="logo" src="<?php echo SITEURLM?>img/logo-home.png"><BR><BR><BR>
        </a>
        <!-- END LOGO -->
    </div>
        <!-- END LOGO -->
        <form id="nwfrmLogin" name="frmLogin" action="<?php echo base_url(); ?>index/login" method="post" onsubmit="return validate();">
        <div class="metro big white">
        <p></p>
            
                <div class="input-append lock-input">
                       <input type="text" value="" placeholder="Login" name="username" id="username" tabindex="1" /> <BR><BR>
                     <input type="password" value="" placeholder="Password" name="password" id="password" tabindex="2" /> <BR><BR>
                             <select  data-placeholder="--Please Select--"  tabindex="3" name="loginas" id="loginas">
                                        <option value="">-Please select level-</option>
					                      <option value="teacher">Classroom</option>
                                           <option value="observer">Campus</option>
											<option value="district">District</option>
                							<!--<option value="district_management" >Administrator</option>-->
						                      <option value="parent">Parent</option>
                      <!-- <option value="student">Student</option>-->
<!--                      <option value="elsphpwebquiz">WAP Administrator</option>-->
                      <!-- <option value="product_specialist">Product Specialist</option>-->
                                    </select>
                </div>
            
        </div>
        <div class="metro double green">
          
                <button type="submit" class="btn login-btn" tabindex="4">
                    Login
                    <i class=" icon-long-arrow-right"></i>
                </button>
            
        </div>
            
        
        <div class="metro purple">
             
            
             <a href="index.html" class="home"><i class="icon-lock"></i> </a>
             
        </div>
        <div class="metro double purple">
            <span class="page-txt"> <a href="<?php echo SITEURLM;?>Quiz/register.php"><!-- Not registered? Sign up here --></a><BR><BR>
                        <a href="javascript:void(0);" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"> <!-- Forgot Password --></a> </span>
        </div>
        <div class="metro gray">
            <a href="#" class="home"><i class="icon-question"></i> 
        </div>
</form>
    </div>
    
    
   
    


<div id="light" class="white_content" style="display:none;">
      <div style="background-color:#C1B38E;	 padding: 5px; width:auto;"> <span style=" color: #222222; font-family: Tahoma;">Forgot Password</span> <span style=" float: right;   margin-right: 5px;"> <a  style="color:#222222" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" href="javascript:void(0)" class="ui-icon ui-icon-closethick"> <img src="<?php echo SITEURLM;?>images/popup-closeButton.png" width="25" height="25"/ style="margin: -3px -4px 0 0;"> </a> </span> </div>
      <div id="msg_success" style="text-align:center;"></div>
      <br/>
      <form id="forgot_pass" method="post" name="forgot_pass" >
    <table align="center">
          <tr>
        <td>Forgot Password As:</td>
        <td><select name="loginas" id="loginas" >
            <option value="district" selected="selected">District</option>
            <option value="Classroom">Teacher</option>
            <option value="observer">Observer</option>
            <option value="parent">Parent</option>
            <!--<option value="elsphpwebquiz">WAP Administrator</option>-->
          </select></td>
      </tr>
          <tr>
        <td>Email Address</td>
        <td><input type="text" name="email_address" id="email_address" /></td>
      </tr>
          <tr>
        <td colspan="2"><span id="email_error" style="margin-left:88px;"></span>
      </tr>
          <tr>
        <td colspan="2"><input type="button" value="Forgot password" class="btn btn-inverse" onclick="restore_password();" /></td>
      </tr>
        </table>
  </form>
    </div>
<div id="fade" class="black_overlay"></div>

<div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>

 <!-- Load javascripts at bottom, this will reduce page load time -->
   
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script>
        function validate(){
            if($('#username').val()==''){
                $('#errmsg').html('Please Enter Your Username.');    
                $('#errorbtn').modal('show');
                    
                return false;
            }
            if($('#password').val()==''){
                $('#errmsg').html('Please Enter Your Password.');
                    $('#errorbtn').modal('show');
                return false;
            }
            if($('#loginas').val()==''){
                $('#errmsg').html('Please select level.');
                    $('#errorbtn').modal('show');
                return false;
            }
        }
        </script>

</body>
    
</html>