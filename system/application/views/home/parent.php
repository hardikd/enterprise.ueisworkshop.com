<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Parent::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script language="javascript">
function submit_home_login(){
		
		if(document.frmLogin.username.value==""){
			alert("Please Enter Your Username.");
			document.frmLogin.username.focus();
			return false;
		}
		
		if(document.frmLogin.password.value==""){
			alert("Please enter your Password.");
			document.frmLogin.password.focus();
			return false;
		}
		
	
if ( (document.frmLogin.loginas[0].checked == false ) && (document.frmLogin.loginas[1].checked == false ) ) 	 {
		
			alert ( "Please choose login type" ); 
			return false;	
		}

return true;

	}
</script>
<link rel="stylesheet" href="<?php echo SITEURLM?>css/style_quiz.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>css/bootstrap.min.css" />	
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script language = "javascript">
            
function restore_password()
{
	var email = document.forgot_pass.email_address.value;
	var loginas = document.forgot_pass.loginas.value;
	
	
	
	  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if (email =="") {
		$("#email_error").html('Please enter email address').css('color','#F00');
		document.forgot_pass.email_address.focus();
		return false;
	 }
    if (!filter.test(email)) {
    $("#email_error").html('Please provide a valid email address').css('color','#F00');
    document.forgot_pass.email_address.focus();
    return false;
 }
 	var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/forgotpassusers.php?emailaddress='+email+"&loginas="+loginas;
	}
	
	$.ajax({
		type:'post',
		url:path,
		success:function(result)
		{
		
			if(result=='invalid email address')
			{
				$("#msg_success").html("Invalid email").css('color','#F00');
			}
			else if(result=='emailsendingfailed')
			{
				$("#msg_success").html("Email Sending Failed").css('color','#F00');
			}
			else if(result=='recordnotfound')
			{
				$("#msg_success").html("Record Not Found.").css('color','#F00');
			}
			else
			{
				$("#msg_success").html("Email sent successfully...").css('color','#060');
			}
			
			
			
			}
		
		});
		
}
</script>
<script type="text/javascript">
$(document).keyup(function(e) {
  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
   } 
});
</script>
<style>
		.black_overlay{
			display: none;
			position: absolute;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 100%;
			background-color: black;
			z-index:1001;
			-moz-opacity: 0.8;
			opacity:.80;
			filter: alpha(opacity=80);
		}
		.white_content {
			background-color: white;
			border: 5px solid #677659;
			height: auto;
			left: 36%;
			overflow: auto;
			padding: 1px;
			position: fixed;
			top: 26%;
			width: auto;
			z-index: 1002;
		}
	</style>
</head>

<body>

<div class="wrapperhome">

	<?php require_once($view_path.'inc/headerWithoutLogin.php'); ?>
    <div class="mbody">
    	
        <div class="content1">
        
        <div class="login_box">
       

		 <form id="nwfrmLogin" name="frmLogin" action="parentlogin/login" method="post">
				<div class="top_b">   </div>    
				<div class="alert alert-info alert-login">
	<u>Parent/Student Login</u>
<br>
                                        <font color="red"></font>
                                        <span style="color:#FF0000"><?php echo $this->session->flashdata('invalidlogin');?></span>
				</div>
				<div class="cnt_b">
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input type="text" value="" placeholder="Login" name="username" id="username">
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i><!--<i class="icon-lock"></i>--></span><input type="password" value="" placeholder="Password" name="password" id="password">
						</div>
					</div>
                                        <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span> <!--<select name="drpLang"><option value="english.php" selected="">English</option><option value="spanish.php">Spanish</option></select>-->
           
            <select name="loginas" id="loginas" >
            <option value="parents" selected="selected">Parent</option>
            <option value="student">Student</option>
            </select>            
                            
						</div>
					</div>
					
				</div>
                
				<div class="btm_b clearfix">
					<button type="submit" name="btnSubmit" class="btn btn-inverse pull-right" onclick="return submit_home_login();">Sign in</button>
                    
                    
<span>
<a href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Forgot password ?</a>

</span>
       </div>  
			</form>
	    
		</div>
        

		
        </div>
    </div>
   <?php require_once($view_path.'inc/footer.php'); ?>

</div>


<div id="light" class="white_content" style="display:none;">

<div style="background-color:#C1B38E; padding: 5px; width: auto;">
      
      
      <span style=" color: #222222; font-family: Tahoma;">Forgot Password</span> 
      
      <span style=" float: right;   margin-right: 5px;">
      
      <a  style="color:#222222" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" href="javascript:void(0)" class="ui-icon ui-icon-closethick">
      
      <img src="<?php echo SITEURLM;?>images/popup-closeButton.png" width="25" height="25"/ style="margin: -3px -4px 0 0;">
      
      </a>
      </span>
      
       </div>
       <div id="msg_success" style="text-align:center;"></div> <br/>       
        
        <form id="forgot_pass" method="post" name="forgot_pass" >
        <table align="center">
        <tr>
     
        <td>Forgot Password As:</td>
        <td>
         <select name="loginas" id="loginas" >
            <option value="parent" selected="selected">Parent</option>
     
            </select>  
            </td>
            </tr>
        <tr>
        <td>Email Address</td><td><input type="text" name="email_address" id="email_address" /></td>
        </tr>
        <tr><td colspan="2"><span id="email_error" style="margin-left:88px;"></span></tr>
        <tr>
        <td colspan="2"><input type="button" value="Forgot password" class="btn btn-inverse" onclick="restore_password();" /></td>
        </tr>
        </table>
        
        </form>
        
       
     
</div>
		<div id="fade" class="black_overlay"></div>

</body>
</html>
