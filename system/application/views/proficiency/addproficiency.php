<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/headerv1.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">
<?php if($this->session->userdata('login_type')=='observer') { ?>
        
        <?php require_once($view_path.'inc/observermenu.php'); 
		}
		
		else
		{
					require_once($view_path.'inc/developmentmenu_new.php');
		}
		?>
          <!-- BEGIN SIDEBAR MENU -->
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                       Welcome <?php echo $this->session->userdata('username');?>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools/data_tracker">Data Analysis Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
							<a href="<?php echo base_url();?>addproficiency">Assessment Proficiency Ranges</a>
                       </li>
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Assessment Proficiency Ranges</h4>
                          
                         </div>
                        
                        <div class="widget-body">
                        
                         <?php if($this->session->keep_flashdata('success')){
								echo $this->session->flashdata('success');
								}
						?>
      <form name="addproficiency" id="addproficiency" method="post" action="addproficiency/createprof">
        <table cellpadding="5" cellspacing="0" width="100%">
        <tr>
        <td>Select Assessment</td>
        <td>
        <select id="assessment" name="assessment" onchange="getdetail(this.value)">
        <option value="">Please Select</option>
        <?php
		foreach($assessments as $kk => $vv)
		{
			?>
            <option value="<?php echo $vv['id'];?>"><?php echo $vv['assignment_name'];?></option>
            <?php

		}
		?>
        </select></td>
        </tr>
          <tr>
            <td>Below Basic</td>
            <span style="color:red; margin-left:180px;">Eg: Enter the values in the below fields like 0  to 30</span>
            <td><input type="text" name="bbasicfrom" id="bbasicfrom" size="10" maxlength="3" value=""  />
              to
              <input type="text" name="bbasicto" id="bbasicto" size="10" maxlength="3" value="" />
              </td>
          </tr>
          <tr>
            <td>Basic</td>
            <td><input type="text" name="basicfrom" id="basicfrom" size="10" maxlength="3"  value=""/>
              to
              <input type="text" name="basicto" id="basicto" size="10" maxlength="3" value="" />
              </td>
          </tr>
          <tr>
            <td>Proficient</td>
            <td><input type="text" name="proficientfrom" size="10" id="proficientfrom" maxlength="3" value="" />
              to
              <input type="text" name="proficientto" id="proficientto" size="10" maxlength="3" value="" />
             </td>
          </tr>
          <tr>
            <td></td>
       <td>
       <button type='button' id='btnprof' value="Create Proficiency" onclick="validatefields();" class="btn btn-success"><i class="icon-plus"></i> Create Proficiency</button>
          </tr>
        </table>
        <input type="hidden" name="assessmentid" id="assessmentid" value="" />
      </form>
     
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <!-- notification ends -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.form.js"></script>

 
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<LINK href="<?php echo SITEURLM?>css/buttonstyle" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light3').style.display='none';
	 document.getElementById('fade3').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
function validatefields()
{
	var assessment = document.getElementById('assessment');
	
	var filterNumeric = /^[0-9]+$/;
	
	var belowbasicfrom=document.getElementById('bbasicfrom').value;
	var belowbasicto=document.getElementById('bbasicto').value;
	var basicfrom = document.getElementById('basicfrom').value;
	var basicto = document.getElementById('basicto').value;
	var profrom = document.getElementById('proficientfrom').value;
	var proto = document.getElementById('proficientto').value;

	if(assessment.selectedIndex  ==false)
	{
		alert('Please select the assessment');
		return false;
	}
	
	if(belowbasicfrom=="")
	{
		alert('Please enter below basic score');
		document.getElementById('bbasicfrom').focus();
		return false;
	}
	if(filterNumeric.test(belowbasicfrom)==false)
	{
		alert('Please enter only numeric');
		document.getElementById('bbasicfrom').focus();
		return false;
	}
	
	if(belowbasicto=="")
	{
		alert('Please enter below basic score');
			document.getElementById('bbasicto').focus();
		return false;
	}
	if(filterNumeric.test(belowbasicto)==false)
	{
		alert('Please enter only numeric');
			document.getElementById('bbasicto').focus();
		return false;
	}
	
	if(eval(belowbasicfrom)>eval(belowbasicto))
	{
		alert('Please enter valid proficiency');
		document.getElementById('bbasicfrom').focus();
		return false;
	}
	
	if(basicfrom=="")
	{
		alert('Please enter basic score');
				document.getElementById('basicfrom').focus();
		return false;
	}
	if(filterNumeric.test(basicfrom)==false)
	{
		alert('Please enter only numeric');
				document.getElementById('basicfrom').focus();
		return false;
	}
	
	if(basicto=="")
	{
		alert('Please enter basic score');
			document.getElementById('basicto').focus();
		return false;
	}
	if(filterNumeric.test(basicto)==false)
	{
		alert('Please enter only numeric');
			document.getElementById('basicto').focus();
		return false;
	}
	if(eval(basicfrom)>eval(basicto))
	{
		alert('Please enter valid proficiency');
		document.getElementById('basicfrom').focus();
		return false;
	}
	if(profrom=="")
	{
		alert('Please enter proficient score');
				document.getElementById('proficientfrom').focus();
		return false;
	}
	if(filterNumeric.test(profrom)==false)
	{
		alert('Please enter only numeric');
				document.getElementById('proficientfrom').focus();
		return false;
	}
	
	if(proto=="")
	{
		alert('Please enter proficient score');
				document.getElementById('proficientto').focus();
		return false;
	}
	if(filterNumeric.test(proto)==false)
	{
		alert('Please enter only numeric');
				document.getElementById('proficientto').focus();
		return false;
	}
	if(eval(profrom)>eval(proto))
	{
		alert('Please enter valid proficiency');
		document.getElementById('proficientfrom').focus();
		return false;
	}
	
	if(eval(basicfrom<=belowbasicto))
	{
	alert('Please enter valid proficiency');
		document.getElementById('basicfrom').focus();
		return false;	
	}
	
	if(eval(profrom<=basicto))
	{
	alert('Please enter valid proficiency');
		document.getElementById('proficientfrom').focus();
		return false;	
	}
	
document.addproficiency.submit();
	
}
</script>
<script type="text/javascript">
function getdetail(testid)
{
	$.ajax({
url:'<?php echo base_url(); ?>addproficiency/getproficiency?assessment_id='+testid,
		success:function(result)
		{
			
	
		var split_data = result.split('|');
			
		
		if(split_data == 'emp')
		{
		$("#bbasicfrom").val('');
		$("#bbasicto").val('');
		$("#basicfrom").val('');
		$("#basicto").val('');
		$("#proficientfrom").val('');
		$("#proficientto").val('');
		$("#assessmentid").val('');
		$("#btnprof").val("Create Proficiency");
		alert("Please Enter Proficiency");
		document.getElementById('bbasicfrom').focus();
		return false;
		}
		else
		{
		$("#bbasicfrom").val(split_data[1]);
		$("#bbasicto").val(split_data[2]);
		$("#basicfrom").val(split_data[3]);
		$("#basicto").val(split_data[4]);
		$("#proficientfrom").val(split_data[5]);
		$("#proficientto").val(split_data[6]);
		$("#assessmentid").val(split_data[0]); 
		$("#btnprof").val("Update Proficiency");
		
		}
		}
		});
}
</script>
   <!-- END JAVASCRIPTS --> 

   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>