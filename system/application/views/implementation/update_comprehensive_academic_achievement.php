<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
     <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                     <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a>
                            <span class="divider">></span>
                       </li>
					    <li>
                            <a href="<?php echo base_url();?>implementation/upload_iep_tracker">Edit IEP Record </a>
                            <span class="divider">></span>
                       </li>
                   
                     <li>
                            <a href="<?php echo base_url();?>implementation/edit_develop_iep/<?php echo $id;?>">Develop IEP
</a>
                            <span class="divider">></span>    
                       </li>
                       <li>
                          <a href="<?php echo base_url();?>implementation/update_comprehensive_academic_achievement/<?php echo $id;?>">Edit Comprehensive Academic Achievement</a>
						
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Comprehensive Academic Achievement</h4>
                          
                         </div>
                         <div class="widget-body">
                             <form class="form-horizontal" method="post" action="<?php echo base_url().'implementation/edit_comprehensive_academic_achievement';?>" name="comprehensive_academic" id="comprehensive_academic">
                                <div id="pills" class="custom-wizard-pills-red3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                    
                                   
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                             
                                     
                                                                           <!-- BEGIN STEP 3-->
                                                <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000">STEP 1</h3>
                                         <div >
                                         <div id="strengths" >
                                      <?php foreach($retrieve_reports as $reports_val){ 
									  	$last_id = $reports_val->comprehensive_academic_id;
									  ?>   
                                         <p></p>
									 <div class="control-group" style="border:1px solid #000;padding:30px;">
                           
                                         
                                         <div class="control-group">
                                             <label class="control-label">Performance Area</label>
              <input type="hidden" name="comprehensive_academic_id[]" value="<?php echo $reports_val->comprehensive_academic_id;?>" id="" />
              <input type="hidden" name="iep_id" value="<?php echo $id;?>" id="" />
                                             <div class="controls">
<?php $per_data = $reports_val->performance_area;?>
                  <select name="performance_area_<?php echo $reports_val->comprehensive_academic_id;?>" id="" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
           <?php if(!empty($performance)) { 
			foreach($performance as $performancevalue)
			{?>
            <?php 
				if($per_data == $performancevalue->performance_area){
			?>
		<option value="<?php echo $performancevalue->performance_area;?>" selected><?php echo $performancevalue->performance_area;?></option>
        <?php }else{?>
        <option value="<?php echo $performancevalue->performance_area;?>"><?php echo $performancevalue->performance_area;?></option>
      	  <?php  }}} ?>			   
		      </select> 
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Assessment/Monitoring Process User</label>
                                             <div class="controls">
                     <?php $asse_data = $reports_val->assessment_monitoring;?>
            <select name="assessment_monitoring_<?php echo $reports_val->comprehensive_academic_id;?>" id="" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
           <?php if(!empty($assessment)) { 
			foreach($assessment as $assessmentvalue)
			{?>
             <?php 
				if($asse_data == $assessmentvalue->assessment_monitoring){
			?>
<option value="<?php echo $assessmentvalue->assessment_monitoring;?>" selected><?php echo $assessmentvalue->assessment_monitoring;?></option>
<?php }else{?>
<option value="<?php echo $assessmentvalue->assessment_monitoring;?>"><?php echo $assessmentvalue->assessment_monitoring;?></option>
			  <?php }} } ?>			   
		      </select> 
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">State/District Assessment Results</label>
                                             <div class="controls">
                        <?php $state_data = $reports_val->state_district_assessment;?>
                   <select name="state_district_assessment_<?php echo $reports_val->comprehensive_academic_id;?>" id="" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
           <?php if(!empty($state_district)) { 
			foreach($state_district as $state_districtvalue)
			{?>
              <?php 
				if($state_data == $state_districtvalue->state_district_assessment){
			?>
            <option value="<?php echo $state_districtvalue->state_district_assessment;?>" selected><?php echo $state_districtvalue->state_district_assessment;?></option>
             <?php }else{?> 
               <option value="<?php echo $state_districtvalue->state_district_assessment;?>"><?php echo $state_districtvalue->state_district_assessment;?></option>
			 
			  <?php } }} ?>			   
		      </select>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                           <span style="font-size:15px"><b>current performance/assessment summary<br />(include student stenghts,student needs and impact of disability on student performance)</b>&nbsp; 
                                             <div >
            <textarea class="span12" name="current_performance_<?php echo $reports_val->comprehensive_academic_id;?>" rows="12" style="width: 600px;"><?php echo $reports_val->current_performance; ?></textarea>

                                             </div>
                                              <div style="padding-top:20px;"> &nbsp;<a href="#" id="addNew" class="btn btn-large btn-red">Add New</i></a></div>
                                         </div>
                                         </div>
									
                                     
                                     <?php }?>
                                     </div>
                                         
                                         
                                     </div>
                                     </div>
                                      <center>
                        <button name="submit" value="submit" id="submit" class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record</button>
                        <!--<button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Review Record</button> 
                        <button class="btn btn-large btn-red"><i class="icon-edit icon-white"></i> Modify Record</button>
                         <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record and Send</button> 
                          <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record and Exit</button>
                          -->
                          </center>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
   
	$(function() {
	var addDiv = $('#strengths');
	var i = $('#strengths p').size() + 1;
	 console.log(i);
	$('#addNew').live('click', function() {
	$('<p><div class="control-group" id="caa_'+i+'" style="border:1px solid #000; padding:30px;"><br /><div class="control-group"><label class="control-label">Performance Area</label><input type="hidden" name="comprehensive_academic_id[]" value="' + i +'" /><div class="controls"><select name="performance_area_' + i +'" id="performance_area_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;"><option value=""></option><?php if(!empty($performance)) {foreach($performance as $performancevalue){?><option value="<?php echo $performancevalue->performance_area;?>"><?php echo $performancevalue->performance_area;?></option><?php }}?></select></div></div><div class="control-group"><label class="control-label">Assessment/Monitoring Process User</label><div class="controls"><select name="assessment_monitoring_' + i +'" id="assessment_monitoring_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;"><option value=""></option><?php if(!empty($assessment)) {foreach($assessment as $assessmentvalue){?><option value="<?php echo $assessmentvalue->assessment_monitoring;?>"><?php echo $assessmentvalue->assessment_monitoring;?></option><?php }}?></select></div></div><div class="control-group"><label class="control-label">State/District Assessment Results</label><div class="controls"><select name="state_district_assessment_' + i +'" id="state_district_assessment_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;"><option value=""></option><?php if(!empty($state_district)) {foreach($state_district as $state_districtvalue){?><option value="<?php echo $state_districtvalue->state_district_assessment;?>"><?php echo $state_districtvalue->state_district_assessment;?></option><?php }}?></select></div></div><div class="control-group"><span style="font-size:15px"><b>current performance/assessment summary<br />(include student stenghts,student needs and impact of disability on student performance)</b></span>&nbsp;<div><textarea class="span12" name="current_performance_' + i +'" id"current_performance_' + i +'" rows="12" style="width: 600px;"></textarea><div style="padding-top:20px;"><a href="#" id="remNew_'+i+'" class="remNew btn btn-large btn-red">Remove</a></div></div></div></div> </p>').appendTo(addDiv);
	i++;
	return false;
	});
	 
	$('.remNew').live('click', function() {
	
if( i > 1) {
	var elemid = $(this).attr('id');
	var elemidarr = elemid.split('_');
	console.log(elemidarr);
	$('#caa_'+elemidarr[1]).remove();
	i--;
	}
	return false;
	});
	});

   </script>  
</body>
<!-- END BODY -->
</html>