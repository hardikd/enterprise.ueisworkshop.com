﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <style type="text/css">

.wrapper{
	min-height:1000px;
}
#button {
    background-color: #DE577B;
    border: 0 none;
    color: #FFFFFF;
    cursor: default;
	border-radius: 15px;
    display: inline-block;
    padding: 7px 14px;
	float: right;
    margin-left: 5px;
	text-decoration: none;
	outline: 0 none;
	text-shadow: none !important;
}
</style>

   

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); 
 require_once($view_path.'inc/highcharts/Highchart.php'); ?>
   <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
     <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/grade_tracker">Grade Tracker</a>
                            
                       </li>
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                  <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Grade Tracker</h4>
                          
                         </div>
                         <br /><br /><br />
                         <div class="content" align="center">
       <!-- <form method="post" name="dash" action="<?php echo base_url();?>report/dashboard" >-->
         <form method="post" name="dash" action="<?php echo base_url();?>implementation/grade_tracker_secondary" >
       
		<table>
		
		<?php if($school_types!=false) { ?>
		<tr>
		<td>
		School Type:<select name="school_type" id="school_type" class="chzn-select chzn-done">
		<?php 
		
		foreach($school_types as $schooltypeval)
		{?>
		<option value="<?php echo $schooltypeval['school_type_id'];?>"
		<?php if($school_type==$schooltypeval['school_type_id']) { ?> selected  <?php } ?>
		><?php echo $schooltypeval['tab'];?></option>
		<?php } ?>
		</select>
		</td>		
		<td align="center">
		<input type="submit" name="submit" value="Submit" style="margin:0px 0px 9px 0px" class="btn btn-small btn-red" >
		</td>
		</tr>
		<?php } else { ?>
		<tr>
		<td>
		No School Types Found.
		</td>
		</tr>
		<?php } ?>
		</table>
		</form>
		<?php 
		if(!empty($schools))
		{
		
		
		?>
		
		
		<table>
		<?php
		
		//start lessonplan
		
		$rcc=0;
		$rc=0;
		
		$lessonchart = new Highchart();
		 foreach ($lessonchart->getScripts() as $script) {
         echo '<script type="text/javascript" src="' . $script . '"></script>';
      }
		//echo '<script src='.SITEURLM.$view_path.'js/jqueryhighchart.js"></script>';
		//echo '<script src='.SITEURLM.$view_path.'js/highcharts.js"></script>';
		
		echo '<script src="'.SITEURLM.'js/exporting.js"></script>';
		echo "<script type='text/javascript'>
		var theme = {
   colors: ['#058DC7', '#50B432', '#910000', '#33C6E7', '#492970', '#F6F826', '#263C53', '#FFF263', '#6AF9C4'],
   chart: {
      backgroundColor: {
         
      }
      
      
   },
   title: {
      style: {
         color: '#000',
         font: 'bold 16px Trebuchet MS, Verdana, sans-serif'
      }
   },
   subtitle: {
      style: {
         color: '#666666',
         font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
      }
   },
   xAxis: {
      gridLineWidth: 1,
      lineColor: '#000',
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
      }
   },
   yAxis: {
      minorTickInterval: 'auto',
      lineColor: '#000',
      lineWidth: 1,
      tickWidth: 1,
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'
         }
      }
   },
   legend: {
      itemStyle: {
         font: '9pt Trebuchet MS, Verdana, sans-serif',
         color: 'black'

      },
      itemHoverStyle: {
         color: '#039'
      },
      itemHiddenStyle: {
         color: 'gray'
      }
   },
   labels: {
      style: {
         color: '#99b'
      }
   },

   navigation: {
      buttonOptions: {
         theme: {
            stroke: '#CCCCCC'
         }
      }
   }
};

var highchartsOptions = Highcharts.setOptions(theme);
</script>		";
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		
		
		$namelimit=array();
		$lessonplan=array();
		$rcc++;
		
		}
		$rc++;
		 $lessonplan[]=intval($schools[$pj]['countlessonplans']);  	 		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
		
						 
		
		
		
		
		
		
		if($rc==8)
			{
			
			$lessonchart='lessonchart'.$rcc;						
			$$lessonchart = new Highchart();
			$$lessonchart->series[] = array('name' => 'Lesson Plan','data' => $lessonplan);			
			
			
			$$lessonchart->chart->renderTo = "lessoncontainer".$rcc;
			$$lessonchart->chart->type = "column";
			$$lessonchart->title->text = "Lesson Planning";
			$$lessonchart->xAxis->categories = $namelimit;
			$$lessonchart->credits->text = '';						
			
			$$lessonchart->yAxis->min = 0;
			$$lessonchart->yAxis->allowDecimals =false;
			$$lessonchart->exporting->enabled =true;
$$lessonchart->yAxis->title->text = "";
$$lessonchart->yAxis->stackLabels->enabled = 1;
$$lessonchart->yAxis->stackLabels->style->fontWeight = "bold";
$$lessonchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$lessonchart->legend->align = "right";
$$lessonchart->legend->x = -100;
$$lessonchart->legend->verticalAlign = "top";
$$lessonchart->legend->y = 20;
$$lessonchart->legend->floating = 1;
$$lessonchart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$lessonchart->legend->borderColor = "#CCC";
$$lessonchart->legend->borderWidth = 1;
$$lessonchart->legend->shadow = false;
$$lessonchart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$lessonchart->plotOptions->column->stacking = "normal";
$$lessonchart->plotOptions->column->dataLabels->enabled = 1;
$$lessonchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$lessonchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");


			$rc=0;
			}
		
		}


			if($rc>0)
	{
			$lessonchart='lessonchart'.$rcc;						
			$$lessonchart = new Highchart();
			
			$$lessonchart->series[] = array('name' => 'Lesson Plan','data' => $lessonplan);			
			
			
			//exit;
			$$lessonchart->chart->renderTo = "lessoncontainer".$rcc;
			$$lessonchart->chart->type = "column";
			$$lessonchart->title->text = "Lesson Planning";
			$$lessonchart->xAxis->categories = $namelimit;
			$$lessonchart->credits->text = '';				
			
			$$lessonchart->yAxis->min = 0;
			$$lessonchart->yAxis->allowDecimals =false;
			$$lessonchart->exporting->enabled =true;
			
$$lessonchart->yAxis->title->text = "";
$$lessonchart->yAxis->stackLabels->enabled = 1;
$$lessonchart->yAxis->stackLabels->style->fontWeight = "bold";
$$lessonchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$lessonchart->legend->align = "right";
$$lessonchart->legend->x = -100;
$$lessonchart->legend->verticalAlign = "top";
$$lessonchart->legend->y = 20;
$$lessonchart->legend->floating = 1;
$$lessonchart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$lessonchart->legend->borderColor = "#CCC";
$$lessonchart->legend->borderWidth = 1;
$$lessonchart->legend->shadow = false;
$$lessonchart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$lessonchart->plotOptions->column->stacking = "normal";
$$lessonchart->plotOptions->column->dataLabels->enabled = 1;
$$lessonchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$lessonchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");

}	
		
		//stop lessonplan
		
		// start notification
		
		$rcc=0;
		$rc=0;
		
		
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		
		
		$namelimit=array();
		$notification=array();
		$behaviour=array();
		$rcc++;
		
		}
		$rc++;
		  $notification[]=intval($schools[$pj]['notification']); 
			$behaviour[]=intval($schools[$pj]['behaviour']); 			  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
		
						 
		
		
		
		
		
		
		if($rc==8)
			{
			
			$notechart='notechart'.$rcc;						
			$$notechart = new Highchart();
			$$notechart->series[] = array('name' => 'Academic Reporting','data' => $notification);
			$$notechart->series[] = array('name' => 'Behavior & Learning','data' => $behaviour);
			
			
			
			$$notechart->chart->renderTo = "notecontainer".$rcc;
			$$notechart->chart->type = "column";
			$$notechart->title->text = "Grade Tracker";
			$$notechart->xAxis->categories = $namelimit;
			$$notechart->credits->text = '';			
			
			$$notechart->yAxis->min = 0;
			$$notechart->yAxis->allowDecimals =false;
			$$notechart->exporting->enabled =true;
$$notechart->yAxis->title->text = "";
$$notechart->yAxis->stackLabels->enabled = 1;
$$notechart->yAxis->stackLabels->style->fontWeight = "bold";
$$notechart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$notechart->legend->align = "right";
$$notechart->legend->x = -100;
$$notechart->legend->verticalAlign = "top";
$$notechart->legend->y = 20;
$$notechart->legend->floating = 1;
$$notechart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$notechart->legend->borderColor = "#CCC";
$$notechart->legend->borderWidth = 1;
$$notechart->legend->shadow = false;
$$notechart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$notechart->plotOptions->column->stacking = "normal";
$$notechart->plotOptions->column->dataLabels->enabled = 1;
$$notechart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$notechart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");


			$rc=0;
			}
		
		}


			if($rc>0)
	{
			$notechart='notechart'.$rcc;						
			$$notechart = new Highchart();
			$$notechart->series[] = array('name' => 'Academic Reporting','data' => $notification);
			$$notechart->series[] = array('name' => 'Behavior & Learning','data' => $behaviour);
			
			
			//exit;
			$$notechart->chart->renderTo = "notecontainer".$rcc;
			$$notechart->chart->type = "column";
			$$notechart->title->text = "Notifications";
			$$notechart->xAxis->categories = $namelimit;
			$$notechart->credits->text = '';				
			
			$$notechart->yAxis->min = 0;
			$$notechart->yAxis->allowDecimals =false;
			$$notechart->exporting->enabled =true;
			
$$notechart->yAxis->title->text = "";
$$notechart->yAxis->stackLabels->enabled = 1;
$$notechart->yAxis->stackLabels->style->fontWeight = "bold";
$$notechart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$notechart->legend->align = "right";
$$notechart->legend->x = -100;
$$notechart->legend->verticalAlign = "top";
$$notechart->legend->y = 20;
$$notechart->legend->floating = 1;
$$notechart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$notechart->legend->borderColor = "#CCC";
$$notechart->legend->borderWidth = 1;
$$notechart->legend->shadow = false;
$$notechart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$notechart->plotOptions->column->stacking = "normal";
$$notechart->plotOptions->column->dataLabels->enabled = 1;
$$notechart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$notechart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");

}	
		
		//stop notification
      
	    //start highchart observation
		$rcc=0;
		$rc=0;
		
		
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		
		
		$namelimit=array();
		$checklist=array();
		$scaled=array();
		$proficiency=array();
		$likert=array();
		$rcc++;
		
		}
		$rc++;
		 $checklist[]=intval($schools[$pj]['checklist']); 
		  $scaled[]=intval($schools[$pj]['scaled']); 
		  $proficiency[]=intval($schools[$pj]['proficiency']); 
		  $likert[]=intval($schools[$pj]['likert']); 		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
		
						 
		
		
		
		
		
		
		if($rc==8)
			{
			
			$chart='chart'.$rcc;						
			$$chart = new Highchart();
			$$chart->series[] = array('name' => 'Checklist','data' => $checklist);
			$$chart->series[] = array('name' => 'Scaled Rubric','data' => $scaled);
			$$chart->series[] = array('name' => 'Proficiency Rubric','data' => $proficiency);
			$$chart->series[] = array('name' => 'Likert','data' => $likert);
			
			
			$$chart->chart->renderTo = "container".$rcc;
			$$chart->chart->type = "column";
			$$chart->title->text = "Observations";
			$$chart->xAxis->categories = $namelimit;
			$$chart->credits->text = '';				
			
			$$chart->yAxis->min = 0;
			$$chart->yAxis->allowDecimals =false;
			$$chart->exporting->enabled =true;
$$chart->yAxis->title->text = "";
$$chart->yAxis->stackLabels->enabled = 1;
$$chart->yAxis->stackLabels->style->fontWeight = "bold";
$$chart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$chart->legend->align = "right";
$$chart->legend->x = -100;
$$chart->legend->verticalAlign = "top";
$$chart->legend->y = 20;
$$chart->legend->floating = 1;
$$chart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$chart->legend->borderColor = "#CCC";
$$chart->legend->borderWidth = 1;
$$chart->legend->shadow = false;
$$chart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$chart->plotOptions->column->stacking = "normal";
$$chart->plotOptions->column->dataLabels->enabled = 1;
$$chart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$chart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");


			$rc=0;
			}
		
		}


			if($rc>0)
	{
			$chart='chart'.$rcc;						
			$$chart = new Highchart();
			$$chart->series[] = array('name' => 'Checklist','data' => $checklist);
			$$chart->series[] = array('name' => 'Scaled Rubric','data' => $scaled);
			$$chart->series[] = array('name' => 'Proficiency Rubric','data' => $proficiency);
			$$chart->series[] = array('name' => 'Likert','data' => $likert);
			
			
			//exit;
			$$chart->chart->renderTo = "container".$rcc;
			$$chart->chart->type = "column";
			$$chart->title->text = "Observations";
			$$chart->xAxis->categories = $namelimit;
			$$chart->credits->text = '';			
			
			$$chart->yAxis->min = 0;
			$$chart->yAxis->allowDecimals =false;
			$$chart->exporting->enabled =true;
			
$$chart->yAxis->title->text = "";
$$chart->yAxis->stackLabels->enabled = 1;
$$chart->yAxis->stackLabels->style->fontWeight = "bold";
$$chart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$chart->legend->align = "right";
$$chart->legend->x = -100;
$$chart->legend->verticalAlign = "top";
$$chart->legend->y = 20;
$$chart->legend->floating = 1;
$$chart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$chart->legend->borderColor = "#CCC";
$$chart->legend->borderWidth = 1;
$$chart->legend->shadow = false;
$$chart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$chart->plotOptions->column->stacking = "normal";
$$chart->plotOptions->column->dataLabels->enabled = 1;
$$chart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$chart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");

}	 
						
     
    
		
		
		
		//stop highchart observation
		
		
		
		
		//start pdlc
		$rcc=0;
		$rc=0;
		
		
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		
		
		$namelimit=array();
		$pdlc=array();
		$rcc++;
		
		}
		$rc++;
		   $pdlc[]=intval($schools[$pj]['countpdlc']);  			  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
		
						 
		
		
		
		
		
		
		if($rc==8)
			{
			
			$pdlcchart='pdlcchart'.$rcc;						
			$$pdlcchart = new Highchart();
			$$pdlcchart->series[] = array('name' => 'Professional Development','data' => $pdlc);
			
			
			
			$$pdlcchart->chart->renderTo = "pdlccontainer".$rcc;
			$$pdlcchart->chart->type = "column";
			$$pdlcchart->title->text = "Professional Development Activity";
			$$pdlcchart->xAxis->categories = $namelimit;
			$$pdlcchart->credits->text = '';			
			
			$$pdlcchart->yAxis->min = 0;
			$$pdlcchart->yAxis->allowDecimals =false;
			$$pdlcchart->exporting->enabled =true;
$$pdlcchart->yAxis->title->text = "";
$$pdlcchart->yAxis->stackLabels->enabled = 1;
$$pdlcchart->yAxis->stackLabels->style->fontWeight = "bold";
$$pdlcchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$pdlcchart->legend->align = "right";
$$pdlcchart->legend->x = -100;
$$pdlcchart->legend->verticalAlign = "top";
$$pdlcchart->legend->y = 20;
$$pdlcchart->legend->floating = 1;
$$pdlcchart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$pdlcchart->legend->borderColor = "#CCC";
$$pdlcchart->legend->borderWidth = 1;
$$pdlcchart->legend->shadow = false;
$$pdlcchart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$pdlcchart->plotOptions->column->stacking = "normal";
$$pdlcchart->plotOptions->column->dataLabels->enabled = 1;
$$pdlcchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$pdlcchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");


			$rc=0;
			}
		
		}


			if($rc>0)
	{
			$pdlcchart='pdlcchart'.$rcc;						
			$$pdlcchart = new Highchart();
			$$pdlcchart->series[] = array('name' => 'Professional Development','data' => $pdlc);
			
			
			//exit;
			$$pdlcchart->chart->renderTo = "pdlccontainer".$rcc;
			$$pdlcchart->chart->type = "column";
			$$pdlcchart->title->text = "Professional Development Activity";
			$$pdlcchart->xAxis->categories = $namelimit;
			$$pdlcchart->credits->text = '';			
			
			$$pdlcchart->yAxis->min = 0;
			$$pdlcchart->yAxis->allowDecimals =false;
			$$pdlcchart->exporting->enabled =true;
			
$$pdlcchart->yAxis->title->text = "";
$$pdlcchart->yAxis->stackLabels->enabled = 1;
$$pdlcchart->yAxis->stackLabels->style->fontWeight = "bold";
$$pdlcchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$pdlcchart->legend->align = "right";
$$pdlcchart->legend->x = -100;
$$pdlcchart->legend->verticalAlign = "top";
$$pdlcchart->legend->y = 20;
$$pdlcchart->legend->floating = 1;
$$pdlcchart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$pdlcchart->legend->borderColor = "#CCC";
$$pdlcchart->legend->borderWidth = 1;
$$pdlcchart->legend->shadow = false;
$$pdlcchart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$pdlcchart->plotOptions->column->stacking = "normal";
$$pdlcchart->plotOptions->column->dataLabels->enabled = 1;
$$pdlcchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$pdlcchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");

}	
		
		
		
		// stop pdlc
		?>
		<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
		<?php 
		if($rcc>1) { ?>
			<div style="float:left;padding-left:380px;width:500px;">
			<input type="button" id="previous button" name="previous" value="Previous Data" style="display:none">
			<input type="button" id="next button" name="next" value="Data Continued" class="btn btn-small btn-red" style="display:none;" >
			</div>
			<?php } ?>
			
		<?php 
		
		for($jk=1;$jk<=$rcc;$jk++)
		{	
	?>		
	
	   <table class="rc<?php echo $jk;?>" <?php if($jk!=1) { ?> style="display:none;" <?php } ?>>
	   <tr>
	   <td>
		<table  id="first_<?php echo $jk;?>"  border="0">
		<tr>
		<td colspan="2">
		<input type="button" id="button" name="nextdashboard" value="Next Dashboard" onclick="javascript:document.getElementById('first_<?php echo $jk;?>').style.display='none';document.getElementById('second_<?php echo $jk;?>').style.display='';" style="display:none;">
		</td>
		</tr>		
		<tr>
		<td>		
	
	<div id="container<?php echo $jk?>" style="width:650px; display:none;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='chart'.$jk;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
		</td>
		</tr>
		<tr>
		<td>		
	
	<div id="notecontainer<?php echo $jk?>" style="width:1050px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='notechart'.$jk;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<input type="button" id="button" name="nextdashboard" value="Next Dashboard" onclick="javascript:document.getElementById('first_<?php echo $jk;?>').style.display='none';document.getElementById('second_<?php echo $jk;?>').style.display='';" style="display:none;">
		</td>
		</tr>
		</table>
		</td>
		<td>
		<table  id="second_<?php echo $jk;?>" border="0" style="display:none;">
		<tr>
		<td colspan="2">
		<input type="button" id="button" name="previousdashboard" value="Previous Dashboard" onclick="javascript:document.getElementById('second_<?php echo $jk;?>').style.display='none';document.getElementById('first_<?php echo $jk;?>').style.display='';">
		</td>
		</tr>		
		<tr>
		<td>
		<div id="lessoncontainer<?php echo $jk?>" style="width:650px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='lessonchart'.$jk;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
		</td>
		</tr>
		<tr>
		<td>
		<div id="pdlccontainer<?php echo $jk?>" style="width:650px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='pdlcchart'.$jk;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<input type="button" id="button" name="previousdashboard" value="Previous Dashboard" onclick="javascript:document.getElementById('second_<?php echo $jk;?>').style.display='none';document.getElementById('first_<?php echo $jk;?>').style.display='';">
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		
		<?php } } else { ?>
		<tr>
		<td>
		No Schools Found.
		</td>
		</tr>
		
		<?php } ?>
		</table>
        </div>
                         
                       
                             </div>
                         
                             
                             
                             
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>     
                  
                  
                  
                  
             </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!-- old script-->
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/reportnextdashboard.js" type="text/javascript"></script>
<!-- end old script -->
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>