<style type="text/css">
   .htitle{ font-size:16px; 
   color:#08a5ce; 
   margin-bottom:10px;
   }
 td, th{
	font-size:12px;
	padding-left:10px;
	}
 
 .my-table {
    page-break-before: always;
    page-break-after: always;
  }
 .break { page-break-before: always; }
 table {margin-left:30px;}

 
   </style>

   <page>
  <div style="margin-left:30px;" backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png"/>
        
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b><?php echo $this->session->userdata('district_name');?></b>		
		<br /><b>
		<?php  if($this->session->userdata('login_type')=='user'){?>
		     <?php echo ($success_data['details']->school_name); ?>		
       <?php }else  if ($this->session->userdata('login_type') == 'teacher') {?>
			<?php echo $this->session->userdata('school_self_name');?>
      <?php } else if ($this->session->userdata('login_type') == 'observer') {?>
      		<?php echo $this->session->userdata('school_name');?>
      <?php }?>
        </b>
		<b><br /><br />IEP Tracker Report</b>	
		</div>
	</div>
		</div>
	

<br /><br />

<br />

<table cellspacing='0' cellpadding='0' border='1' id='conf' style="width:100%;" >
                                     <thead>
                                     <tr>
                                     <th align="center" colspan="3" style="font-size:14px;width:100%;"> <b>Meeting Information</b></th>
                                     </tr>
                                     <tr>
                                       <th style="width:50%;">Pertinent Dates</th>                                       
                                        <th style="width:50%;"> Type of Meeting</th>
                                         
                                        
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <tr class="">
                                     <td style="width:50%;">
										                                <?php
	
												if(!empty($dates_data)) { 
											foreach($dates_data as $key=>$val)	{
												?>
											<div> <?php echo $val['dates'];?></div> 
                                            <div style="margin:-10px 0px 0px 250px;"> <?php echo $val['dates_description'];?></div><br />
							<?php }}?> 
                                        
                                        
                                        </td>
                                        
                                        
                                        <?php /*?> <td style="width:50%;"><?php echo $success_data[0]->meeting_type; ?></td><?php */?>
                                       <br />
                                  <td style="width:50%;">      
                                                <?php
			$data= $success_data[0]->meeting_type;
				 $iep_data_type = explode(",",$data);
			if(!empty($iep_data_type)) { 
			   foreach($meeting_type as $meeting_types)	{
				  $selected = "<img src=img/checkbox_no.png>";
			  foreach($iep_data_type as $iep_value){	
			  	if($iep_value == $meeting_types->meeting_type){
						$selected = "<img src=img/checkbox_yes.png>";
} }?> 
             <div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-12px 0px 0px 40px;" ><?php echo $meeting_types->meeting_type;?></div> <br />
                  <!--<div style="padding:0px; margin:-18px 0px 0px 25px" ></div>-->
                    
                  <?php }}?>
                                        
                             </td>            
                                     </tr>
                                     </tbody>
                                 </table>
                                
        <!-- Start Section B -->
                                 <br /> <br />
<table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Student Information</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;"><b>Student Name:</b>&nbsp;&nbsp;<?php echo ($success_data[0]->firstname. ' ' . $success_data[0]->lastname . ''); ?>
            
        </td>
        <td style="width:30%;"><b>Date of Birth:</b>&nbsp;&nbsp;
		<?php $date_bir =$success_data[0]->date; ?><?php echo date('m/d/Y', strtotime(str_replace('-', '/', $date_bir))) ?>
</td>
        <td style="width:30%;"> <b>Gender:</b>&nbsp;&nbsp;<?php echo ($success_data[0]->gender); ?></td>
       
    </tr>
    	<tr>
		<td>
		<br />
		</td>
		</tr>
        <tr>
		<td>
		<br />
		</td>
		</tr>
    <tr>
		<td style="width:40%;">
		<b>Limited English Proficiency:</b>&nbsp;&nbsp;<?php echo $success_data[0]->limited_english_proficiency; ?> 
		</td>
		<td style="width:30%;">
         <b>Ethnic Code:</b>&nbsp;&nbsp;<?php echo $success_data[0]->ethnic_code; ?>
		</td>
		 <td style="width:30%;">
         <b>City:</b>&nbsp;&nbsp;<?php echo ($success_data[0]->city);?>
         </td>
		</tr>
       <tr>
		<td>
		<br />
		</td>
		</tr> 
        <tr>
		<td>
		<br />
		</td>
		</tr>
      <tr>
		<td style="width:40%;">
		<b>Home Language:</b>&nbsp;&nbsp;<?php echo $success_data[0]->home_language; ?> 
		</td>
		<td style="width:30%;">
		<b>Student Language:</b>&nbsp;&nbsp;<?php echo $success_data[0]->student_language; ?>
		</td>
		 <td style="width:30%;">
         <b>Alternate Mode of Communication:</b>&nbsp;&nbsp;<?php echo $success_data[0]->alternate_mode_of_communication; ?>
         </td>
		</tr>
       <tr>
		<td>
		<br />
		</td>
		</tr> 
        <tr>
		<td>
		<br />
		</td>
		</tr>
      <tr>
		<td style="width:40%;">
		<b> Home Address of Student :</b>&nbsp;&nbsp;<?php echo $success_data[0]->address; ?> 
		</td>
		<td style="width:30%;">
		<b>Home Telephone:</b>&nbsp;&nbsp;<?php echo $success_data[0]->home_telephone; ?>
		</td>
		 <td style="width:30%;">
         <b>Daytime Telephone:</b>&nbsp;&nbsp;<?php echo $success_data[0]->daytime_telephone; ?>
         </td>
		</tr>
               <tr>
		<td>
		<br />
		</td>
		</tr> 
        <tr>
		<td>
		<br />
		</td>
		</tr>
      <tr>
		<td style="width:40%;">
		<b> Emergency Telephone :</b>&nbsp;&nbsp;<?php echo $success_data[0]->emergency_telephone; ?> 
		</td>
		<td style="width:30%;">
		<b>School of Attendance:</b>&nbsp;&nbsp;<?php echo $success_data[0]->school_of_attendance; ?>
		</td>
		 <td style="width:30%;">
         <b>School of Residence:</b>&nbsp;&nbsp;<?php echo $success_data[0]->school_of_residence; ?>
         </td>
		</tr>
                <tr>
		<td>
		<br />
		</td>
		</tr> 
        <tr>
		<td>
		<br />
		</td>
		</tr>
      <tr>
		<td style="width:40%;">
		<b> Location Code :</b>&nbsp;&nbsp;<?php echo $success_data[0]->location_coad; ?> 
		</td>
		<td style="width:30%;">
		
		</td>
		 <td style="width:30%;">
        
         </td>
		</tr>
        <tr>
		<td>
		<br />
		</td>
		</tr>  
</table>
<!-- End Section B-->
<br /><br />
<!-- Start Section E -->


<table style="width:100%;border: 1px solid #000000; " class="my-table" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Eligibility</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;"><b>Eligibility:</b>&nbsp;&nbsp;<?php echo $success_data[0]->eligibility; ?>
            
        </td>
       
       
    </tr>
    	<tr>
		<td>
		<br />
		</td>
		</tr>
        <tr>
		<td>
		<br />
		</td>
		</tr>
    <tr>
		<td style="width:40%;">
		<b>External Impact Factors:</b>&nbsp;&nbsp;<?php echo $success_data[0]->external_impact_factors; ?> 
		</td>
		
		</tr>
       <tr>
		<td>
		<br />
		</td>
		</tr>
        <tr>
		<td>
		<br />
		</td>
		</tr> 
      <tr>
		<td style="width:40%;">
		<b>Eligibility Status:</b>&nbsp;&nbsp;<?php echo $success_data[0]->eligibility_status; ?> 
		</td>
		
		</tr>
        <tr>
		<td>
		<br />
		</td>
		</tr>
  </table>
</page> 
<page> 
<!-- End Section E -->
<!-- Eligibility-->
 <?php foreach($retrieve_reports as $reports_val){ 
	?>

<table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Present Level of Performance</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;"><b>Performance Area:</b>&nbsp;&nbsp;<?php echo $reports_val->performance_area; ?>
            
        </td>
       
       
    </tr>
    	<tr>
		<td>
		<br />
		</td>
		</tr>
    <tr>
		<td style="width:40%;">
		<b>Assessment/Monitoring Process User:</b>&nbsp;&nbsp;<?php echo $reports_val->assessment_monitoring; ?> 
		</td>
		
		</tr>
       <tr>
		<td>
		<br />
		</td>
		</tr> 
      <tr>
		<td style="width:40%;">
		<b>State/District Assessment Results:</b>&nbsp;&nbsp;<?php echo $reports_val->state_district_assessment; ?> 
		</td>
		
		</tr>
       <tr>
		<td>
		<br />
		</td>
		</tr> 
      <tr>
		<td style="width:40%;">
		<b>Current performance/Assessment summary(Include student strengths,student needs and impact of disability on student performance) :</b><br />
        <div style="height:100px; border:1px #666;"><?php echo $reports_val->current_performance; ?> </div> 
		</td>
		</tr>
        
        
</table><br />
<?php }?>



</page>
<page>
 <?php foreach($goal_data as $goal_val){ 
 
 
 
 ?>    
<table cellspacing='0' cellpadding='0' border='1' id='conf' style="width:100%;">
                                    
                                     <tr>
                                     <th align="center" colspan="3" style="font-size:14px;width:100%;"> <b>Annual Goals And Objectives</b></th>
                                     </tr>
                                     

                                      <tr>
                                     <td colspan="2"><b>Standard :</b>&nbsp;&nbsp;<?php echo $goal_val->gol_standard;?></td>
                                     </tr>
                                     <tr>
                                       <th style="width:50%;">Incremental objective #1 related to the goal:</th>                                       
                                        <th style="width:50%;">Incremental objective #2 related to the goal:</th>
                                         
                                     </tr>
                                     <tr class="">
                                        <td style="width:50%;"><?php echo $goal_val->standarddata;?></td>
                                        <td style="width:50%;"><?php echo $goal_val->standarddisplay;?></td>
                                     </tr>
                                     <tr class="">
                                        <td style="width:50%;">Data To Be Achieved:&nbsp;&nbsp;
    							<?php $date1 =$goal_val->incremental_date_1; ?><?php echo date('m/d/Y', strtotime(str_replace('-', '/', $date1))) ?>
                                        </td>
                                         <td style="width:50%;">Data To Be Achieved:&nbsp;&nbsp;
                           		<?php $date2 =$goal_val->incremental_date_2; ?><?php echo date('m/d/Y', strtotime(str_replace('-', '/', $date2))) ?>
                                         </td>
                                         
                                     </tr>
                                    
                                 </table>  <br />
      <?php }?>
                           <!-- End Section G -->     
                  <!-- Start Individualized Education Program-->
                  </page>
<page>
                                    <br/><br/>
                                    
               <table cellspacing='0' cellpadding='0' border='1' id='conf' style="width:100%;">
                                     <tr>
                                     <th align="center" colspan="" style="font-size:14px;width:100%;"> <b>Create English Language Development Goals </b></th>
                                     </tr>
                                     <tr>
                                       <th style="width:90%;">Skill Area: Listening</th>                                       
                                     </tr>
                                     
                                     <tr class="">
                                     <td style="width:100%;">
		
             <?php
			$data= $success_data[0]->listening;
				 $listening_val = explode(",",$data);
			if(!empty($listening_val)) { 
			   foreach($listening as $listening_data){
				  $selected = "<img src=img/checkbox_no.png>";
			  foreach($listening_val as $listening_value){	
			  	if($listening_value == $listening_data->skill_type){
						$selected = "<img src=img/checkbox_yes.png>";
} }?> 

                    <div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-15px 0px 0px 40px;" ><?php echo $listening_data->skill_type;?> </div> <br />

                 <?php }}?>
            
        </td>
             </tr>
              <tr>
                 <th style="width:100%;">Skill Area:Speaking</th>
               </tr>                           
                                        
                                 
                    <tr>            
             <td style="width:100%;">
              <?php
			$data= $success_data[0]->speaking;
				 $speaking_val = explode(",",$data);
			if(!empty($speaking_val)) { 
			   foreach($speaking as $speaking_data){
				  $selected = "<img src=img/checkbox_no.png>";
			  foreach($speaking_val as $speaking_value){	
			  	if($speaking_value == $speaking_data->skill_type){
						$selected = "<img src=img/checkbox_yes.png>";
		} }?>
             <div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-15px 0px 0px 40px;" ><?php echo $speaking_data->skill_type;?> </div> <br />
				 <?php }}?>
                
        </td>   
        </tr>
                                     
                                     <tr>
                                       <th style="width:100%;">Skill Area: Reading</th>                                       
                                        
                                     </tr>
                                     
                                     <tr class="">
                                     <td style="width:100%;">
										      <?php
			$data= $success_data[0]->readnig;
				 $readnig_val = explode(",",$data);
			if(!empty($readnig_val)) { 
			   foreach($readnig as $readnig_data){
				  $selected = "<img src=img/checkbox_no.png>";
			  foreach($readnig_val as $readnig_value){	
			  	if($readnig_value == $readnig_data->skill_type){
						$selected = "<img src=img/checkbox_yes.png>";
				} }?>
         <div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-15px 0px 0px 40px;" ><?php echo $readnig_data->skill_type;?> </div> <br />
					 
                 <?php }}?>
                                        
                                        </td>
                                        </tr>
                      <tr>
                                       
                                        <th style="width:100%;">Skill Area: Writing</th>
                                         
                                        
                                     </tr>                   
                                      <tr>
                                       <br />
                                  <td style="width:100%;">      
                      	<?php
			$data= $success_data[0]->writing;
				 $writing_val = explode(",",$data);
			if(!empty($writing_val)) { 
			   foreach($writing as $writing_data){
				  $selected = "<img src=img/checkbox_no.png>";
			  foreach($writing_val as $writing_value){	
			  	if($writing_value == $writing_data->skill_type){
						$selected = "<img src=img/checkbox_yes.png>";
		} }?>
		<div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-15px 0px 0px 40px;" ><?php echo $writing_data->skill_type;?> </div> <br />
                 <?php }}?>
             </td>            
             </tr>
             
               <tr>
                                       
                                        <th style="width:100%;">English Language Development Goals </th>
                                         
                                        
                                     </tr>                   
                                      <tr>
                                       <br />
                                  <td style="width:100%;">      
                   <div style="height:100px; border:1px #666;"><?php echo $success_data[0]->english_language_development_goals; ?></div> 
             </td>            
             </tr>
             
          
                                     
                                 </table>   
                                 </page>                    

  <page> 
<!-- end Individualized Education Program-->  
<!-- Start parent participation and consent-->
               
<table cellspacing='0' cellpadding='0' border='1' id='conf' style="width:100%;">
                                   
                                     <tr>
                                    <th align="center" colspan="3" style="font-size:14px;width:100%;"> <b>Parent participation and consent</b></th>
                                     </tr>
                                      <tr>
                                     <td>
                                     
                                                     <?php
			$agreement_data= $agreement[0]->indicate_parent_consent;
	
				foreach($indicate_parent as $indicate){
				  $selected = "<img src=img/redio_no.png>";
			  	if($agreement_data == $indicate->id){
						$selected = "<img src=img/redio_yes.png>";
} ?>
             
              <div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-15px 0px 0px 40px;" ><?php echo $indicate->indicate_parent_consent;?></div> <br />
                 
                <?php }?>
                                     
                                     </td>
                                     
                                     
                                     </tr>
                                 </table>    
                                    <br/><br />
               
<table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Categorize Issues</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;">
      
      
      
        <?php
		 $data= unserialize($agreement[0]->categorize_issues);
				 $categorize_data = explode(",",$data);
		
			   foreach($categorize_issues as $categorize){
				  $selected = "<img src=img/checkbox_no.png>";
			  foreach($categorize_data as $categorize_value){	
			  	if($categorize_value == $categorize->categorize_issues){
						$selected = "<img src=img/checkbox_yes.png>";
} }?>
               <div style="margin:0px 0px 0px 20px;"><?php echo $selected;?></div> <div style="margin:-15px 0px 0px 40px;" ><?php echo $categorize->categorize_issues;?> </div> <br />
                 
                <?php }?>
      
      </td>
      
    </tr>
 
  </table>
                                 
                             <br/> <br /><br />
               
<table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Summarize</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;"><div style="height:100px;height:300px; border:1px #666;"><?php echo $agreement[0]->summarize; ?></div>
            
        </td>
       
       
    </tr>
 
  </table>
  
      </page>    
  <!-- End Section I-->
 <!-- start Summary of IEP Plan -->
 <page>
  <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>
<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Summary of IEP Plan </b></th>
</tr>
<tr style="width:100%;border: 1px solid #000000;">
										<th style="width:25%;"></th>
                                         <th style="width:25%;"></th>                                       
                                         <th style="width:25%;">Effective With This IEP</th>
                                         <th style="width:25%;" >Future Changes Related To This IEP</th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
           <tr>
                                         <th></th>
                                         <th>As of Date:</th>                                       
                                         <th> 
                                         <?php $effective =$summary_of_iep[0]->effective_date; ?>
												<?php echo date('m-d-Y', strtotime(str_replace('-', '/', $effective))) ?>
                						</th>
                                        <th>
                                         <?php $future_changes =$summary_of_iep[0]->future_changes_date; ?>
											<?php echo date('m-d-Y', strtotime(str_replace('-', '/', $future_changes))) ?>
                             			</th>
                                         
                                     </tr>
   
  </table>
  
<table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
<?php 
$count = count($plan_eligibility);
$eligibility_count = $count +1;
?>
      <th rowspan="<?php echo $eligibility_count; ?>" style="width:25%;">Eligibility:</th>
    </tr>
<?php /*?><?php foreach($plan_eligibility as $val){?> <?php */?>
<?php
	
	if(!empty($updateatt)) { 
foreach($updateatt as $key=>$val)	{
?> 	
 <tr>
    <td style="width:25%;padding-bottom:5px;"> <?php echo $val['plan_eligibility'];?></td>
     <td style="width:25%;padding-bottom:5px;"><?php echo $val['eligibility_effective'];?></td>
     <td style="width:25%;padding-bottom:5px;"><?php echo $val['eligibility_future_changes'];?></td>
 </tr> 
    
 <?php }}?>
 </table>
 
 
  
  <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
    <?php 
$count = count($curriculum);
$curriculum_count = $count +1;
?>
     <th rowspan="<?php echo $curriculum_count; ?>" style="width:25%;">Curriculum:</th>
    </tr>
 <?php
	
foreach($updateatt_curriculum as $key=>$curriculum_val)	{

	?>   	
 <tr>
    <td style="width:25%;padding-bottom:5px;"><?php echo $curriculum_val['plan_curriculum'];?></td>
     <td style="width:25%;padding-bottom:5px;"><?php echo $curriculum_val['curriculum_effective'];?></td>
     <td style="width:25%;padding-bottom:5px;"><?php echo $curriculum_val['curriculum_future_changes'];?></td> 
 </tr>
     
 <?php }?>
  </table> 
  
  <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">


<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
    <?php 
$count = count($placement);
$placement_count = $count +1;
?>
     <th rowspan="<?php echo $placement_count; ?>" style="width:25%;">Placement:</th>
    </tr>

 <?php
foreach($updateatt_placement as $key=>$placement_val)	{
?> 
<tr>
      <td style="width:25%;padding-bottom:5px;"><?php echo $placement_val['plan_placement'];?></td>
    <td style="width:25%;padding-bottom:5px;"><?php echo $placement_val['placement_effective'];?></td>
     <td style="width:25%;padding-bottom:5px;"><?php echo $placement_val['placement_future_changes'];?></td> 
  </tr>
 <?php }?>
 </table>
 
 <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
<?php 
$count = count($instructionl_setting);
$instructionl_count = $count +1;
?>
     <th rowspan="<?php echo $instructionl_count;?>" style="width:25%;">Instructional setting</th>
    </tr>
<?php /*?><?php foreach($plan_eligibility as $val){?> <?php */?>
 <?php
if(!empty($updateatt_instructionl)) { 
foreach($updateatt_instructionl as $key=>$instructionl_val)	{
?> 
   
<tr>
      	<td style="width:25%;padding-bottom:5px;"><?php echo $instructionl_val['instructionl_setting'];?></td>
 		<td style="width:25%;padding-bottom:5px;"><?php echo $instructionl_val['instructionl_setting_effective'];?></td>
 	    <td style="width:25%;padding-bottom:5px;"><?php echo $instructionl_val['instructionl_setting_future_changes'];?></td> 
  </tr>
 <?php }}?>
 </table>
 
 <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
<?php 
$count = count($additional_factors);
$additional_count = $count +1;
?>
     <th rowspan="<?php echo $additional_count;?>" style="width:25%;">additional Factors</th>
    </tr>
<?php /*?><?php foreach($plan_eligibility as $val){?> <?php */?>
 <?php
if(!empty($updateatt_additional)) { 
foreach($updateatt_additional as $key=>$additional_val)	{
		  ?> 
   
<tr>
      	<td style="width:25%;padding-bottom:5px;"><?php echo $additional_val['additional_factors'];?></td> 
   		<td style="width:25%;padding-bottom:5px;"><?php echo $additional_val['additional_effective'];?></td>
    	<td style="width:25%;padding-bottom:5px;"><?php echo $additional_val['additional_future_changes'];?></td> 
  </tr>
 <?php }}?>
 </table>
 
 <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
    <?php 
$count = count($accommodation);
$accommodation_count = $count +1;
?>

    <th rowspan="<?php echo $accommodation_count;?>" style="width:25%;">Accommodation,<br />Modifications,Supports</th>
    </tr>
<?php /*?><?php foreach($plan_eligibility as $val){?> <?php */?>
   <?php
if(!empty($accommodation_data)) { 
foreach($accommodation_data as $key=>$accommodation_val){
		  ?> 
   
<tr>
        <td style="width:25%;padding-bottom:5px;"> <?php echo $accommodation_val['accommodation_modifications'];?></td> 
       <td style="width:25%;padding-bottom:5px;"><?php echo $accommodation_val['accommodation_modifications_effective'];?></td>
    	<td style="width:25%;padding-bottom:5px;"><?php echo $accommodation_val['accommodation_modifications_future_changes'];?></td> 
  </tr>
 <?php }}?>
 </table>
 
 <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
   <?php 
$count = count($preparation);
$preparation_count = $count +1;
?>

     <th rowspan="<?php echo $preparation_count ;?>" style="width:25%;">Preparation for three year review IEP(complete at second annual review IEP meeting</th>
    </tr>
<?php /*?><?php foreach($plan_eligibility as $val){?> <?php */?>
 <?php
if(!empty($preparation_data)) { 
foreach($preparation_data as $key=>$preparation_val){
		  ?> 
   
<tr>
         <td style="width:25%;padding-bottom:5px;"><?php echo $preparation_val['preparation'];?></td>
         <td style="width:25%;padding-bottom:5px;"><?php echo $preparation_val['preparation_effective'];?></td>
    	<td style="width:25%;padding-bottom:5px;"><?php echo $preparation_val['preparation_future_changes'];?></td>
  </tr>
 <?php }}?>
 </table>
 
 <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
     <th rowspan="<?php echo $preparation_count ;?>" style="width:25%;">%Time Outside Of General Education</th>
     
    </tr>
<tr>
         <td style="width:25%;">  <div style="margin:-15px 0px 0px 50px; border:1px #000;" >% <?php echo $summary_of_iep[0]->time_outside;?></div></td>
         <td style="width:25%;"></td>
    	<td style="width:25%;"></td>
  </tr>

 </table>
 
 <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="4" style="font-size:14px; width:100%; border:0px solid #000;"></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    
    
    <tr style="border: 1px solid #000000;">
     
    <th colspan="4"style="">
     <?php $team = $summary_of_iep[0]->team_exceeds;?>
   <?php if ($team =='1'){?>
   
   <div style="margin:0px 0px 0px 20px;"><img src=img/checkbox_yes.png></div>
      <?php }else{ ?>
      <div style="margin:0px 0px 0px 20px;"><img src=img/checkbox_no.png></div>
       <?php }?>
       
       <div style="margin:-15px 0px 0px 40px;" >
     The IEP acknowledges that the parent of time outside of the general education classroom as determined by the team exceeds 60%  </div></th>

    </tr>    
    

 </table>
 
 <br /><br /><br />
  <!--End Section J --> 
  <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Description of IEP Services
</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;"><b>Services Grid:</b>&nbsp;&nbsp;<?php echo unserialize($success_data[0]->services_grid); ?>
      </td>
    </tr>
 
  </table>
 <br />
    <br/> 
  <table style="width:100%;border: 1px solid #000000;" cellspacing='0' cellpadding='0' id='conf'>

<tr style="border: 1px solid #000000;">
<th align="center" colspan="3" style="font-size:14px;width:100%; border:1px solid #000;"> <b>Individualized Education Program</b></th>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
    <tr>
        <td style="width:40%;"><b>Testing:</b>&nbsp;&nbsp;<?php echo unserialize($success_data[0]->determine_student_assessments); ?>
            
        </td>
       
       
    </tr>
 
  </table>
 </page>
 <page>
 <table style="width:100%;" border="1" cellspacing='0' cellpadding='0' id='conf'>

<tr>
<th align="center" colspan="3" style="font-size:14px;width:100%; "> <b>Attendance
</b></th>
</tr>
    <tr>
 <th height="10%">Team Member</th>                                       
 <th height="10%">Print Name</th>
 <th height="10%">Signature</th>
 </tr>
 <?php
	
	if(!empty($updateatt)) { 
foreach($updateatt as $key=>$val)	{
	if($val['team_member_name']!=''){

			  ?>
		<tr height="30%">
         			<td style="height:20px;" ><?php echo $val['team_member_name'];?></td> 
                 	 <td><?php echo $val['print_name'];?></td>
					<td><?php echo $val['signature'];?></td>
                                         
                                     </tr>
                                     <?php }} }?>
 
 
  </table>  
</page>
 
                                       