<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
.table th, .table td {
    border-top: 1px solid #ddd;
    line-height: 20px;
    padding: 8px;
    text-align: left;
    vertical-align: middle;
}
</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
     <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                      <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a>
                            <span class="divider">></span>
                       </li>
					    <li>
                            <a href="<?php echo base_url();?>implementation/upload_iep_tracker">Edit IEP Record </a>
                            <span class="divider">></span>
                       </li>
                             <li>
                            <a href="<?php echo base_url();?>implementation/edit_iep_plan_summary//<?php echo $id;?>">IEP Plan Summary 
</a>
                            <span class="divider">></span>    
                       </li>
					   
                         <li>
                          </i> <a href="<?php echo base_url();?>implementation/update_summary_of_iep_plan/<?php echo $id;?>">Edit Summary of IEP Plan </a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Summary of IEP Plan </h4>
                          
                         </div>
                         <div class="widget-body">
                            
                            <form class="form-horizontal" action="<?php echo base_url(); ?>implementation/edit_summary_of_iep_plan" name="summary_of_iep_plan" id="summary_of_iep_plan" method="post">
                    
                                <div id="pills" class="custom-wizard-pills-red3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                             
                                     
                                                                           <!-- BEGIN STEP 3-->
                                                <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000">STEP 1</h3>
                                         <div class="control-group">
                           
                                      <input type="hidden" name="iep_id" value="<?php echo $id;?>" id="" />     
                          <table cellpadding="2" class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
                          
                          <thead>
                                     <tr>
                                         <th></th>
                                         <th></th>                                       
                                         <th>Effective With This IEP</th>
                                         <th class="no-sorting sorting">Future Changes Related To This IEP</th>
                                         
                                     </tr>
                                     <tr>
                                         <th></th>
                                         <th>As of Date:</th>                                       
                                         <th> 
                                         <?php $effective =$summary_of_iep[0]->effective_date; ?>
							<input id="dp1" type="text" value="<?php echo date('m-d-Y', strtotime(str_replace('-', '/', $effective))) ?>" name="effective_date" size="16" class="m-ctrl-medium" style="width: 150px;"></input>
                						</th>
                                         <th>
                                         <?php $future_changes =$summary_of_iep[0]->future_changes_date; ?>
                             <input id="dp2" type="text" value="<?php echo date('m-d-Y', strtotime(str_replace('-', '/', $future_changes))) ?>" name="future_changes_date" size="16" class="m-ctrl-medium" style="width: 150px;"></input>
                             </th>
                                         
                                     </tr>
                                     </thead>
 <!--start Eligibility:(from page 4) -->                         
<tr>

    <th style="width:20%;">Eligibility:(from page 4)</th>
    <td>
    <select name="plan_eligibility_id" id="plan_eligibility_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <?php if(!empty($plan_eligibility)) { 
			foreach($plan_eligibility as $val)
			{?>
             <?php $eligib = $eligibility[0]->plan_eligibility_id;
				if($eligib == $val->id){?>
			  <option value="<?php echo $val->id;?>" selected><?php echo $val->plan_eligibility;?> </option>
              <?php }else{?>
              <option value="<?php echo $val->id;?>"><?php echo $val->plan_eligibility;?> </option>
			  <?php } }} ?>			   
		      </select> 
    
    </td>
    
     <td><input type="text" name="eligibility_effective" value="<?php echo $eligibility[0]->eligibility_effective;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
     <td><input type="text" name="eligibility_future_changes" value="<?php echo $eligibility[0]->eligibility_future_changes;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td> 
 </tr>

<tr>
    <th style="width:20%;">Curriculum</th>
    <td><select name="plan_curriculum_id" id="plan_curriculum_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <?php if(!empty($curriculum)) { 
			 foreach($curriculum as $curriculum_val){?> 
			
             <?php $curri = $plan_curriculum[0]->plan_curriculum_id;
				if($curri == $curriculum_val->id){?>
               <option value="<?php echo $curriculum_val->id;?>" selected><?php echo $curriculum_val->plan_curriculum;?> </option>
              <?php }else{ ?>
               <option value="<?php echo $curriculum_val->id;?>"><?php echo $curriculum_val->plan_curriculum;?> </option>
			  <?php } }} ?>			   
		      </select></td>

     <td><input type="text" name="curriculum_effective" value="<?php echo $plan_curriculum[0]->curriculum_effective;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
     <td><input type="text" name="curriculum_future_changes" value="<?php echo $plan_curriculum[0]->curriculum_future_changes;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td> 
 </tr>


<tr>
    <th style="width:20%;">Placement</th>
    <td><select name="plan_placement_id" id="plan_placement_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <?php if(!empty($placement)) { 
			   foreach($placement as $placement_val){?> 
				<?php $plac = $plan_placement[0]->plan_placement_id;
				if($plac == $placement_val->id){?>
            
			  <option value="<?php echo $placement_val->id;?>" selected><?php echo $placement_val->plan_placement;?> </option>
              <?php }else{?>
              <option value="<?php echo $placement_val->id;?>"><?php echo $placement_val->plan_placement;?> </option>
			  <?php } }}?>			   
		      </select></td>
     
     
     <td><input type="text" name="placement_effective" value="<?php echo $plan_placement[0]->placement_effective;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
     <td><input type="text" name="placement_future_changes" value="<?php echo $plan_placement[0]->placement_future_changes;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td> 
 </tr>

   <!--end Eligibility:(from page 4) -->     
<!--start Instructionl setting -->    
<tr>
<?php 
$count = count($instructionl_setting);
$instructionl_count = $count +1;
?>
    <th rowspan="<?php echo $instructionl_count;?>" style="width:20%;">Instructionl setting</th>
   </tr> 
   <?php /*?> <?php foreach($instructionl_setting as $instructionl){?> <?php */?>
   <?php
	
	if(!empty($updateatt_instructionl)) { 
foreach($updateatt_instructionl as $key=>$instructionl_val)	{

			  ?> 
   
   <tr>
    <td><?php echo $instructionl_val['instructionl_setting'];?></td>
         <input type="hidden" name="instructionl_setting_id[]" value="<?php echo $key;?>" id="instructionl_setting" />
     <td><input type="text" name="instructionl_setting_effective_<?php echo $key;?>" value="<?php echo $instructionl_val['instructionl_setting_effective'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
     <td><input type="text" name="instructionl_setting_future_changes_<?php echo $key;?>" value="<?php echo $instructionl_val['instructionl_setting_future_changes'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td> 
 </tr>
    <?php }}?>
<!--end Instructionl setting-->     
<!--start additional Factors-->    
  <tr>
  <?php 
$count = count($additional_factors);
$additional_count = $count +1;
?>
    <th rowspan="<?php echo $additional_count;?>" style="width:20%;">additional Factors</th>
   </tr>
   <?php /*?><?php foreach($additional_factors as $additional){ ?><?php */?>
    <?php
if(!empty($updateatt_additional)) { 
foreach($updateatt_additional as $key=>$additional_val)	{
		  ?> 
   <tr>
    <td><?php echo $additional_val['additional_factors'];?></td> 
    <input type="hidden" name="additional_factors_id[]" value="<?php echo $key;?>" id="additional_factors" />
    <td><input type="text" name="additional_effective_<?php echo $key;?>" value="<?php echo $additional_val['additional_effective'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
    <td><input type="text" name="additional_future_changes_<?php echo $key;?>" value="<?php echo $additional_val['additional_future_changes'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
   </tr>
   <?php }}?>

<!--end additional Factors-->  

<!--start Accommodation,Modifications,Supports-->    
  <tr>
    <?php 
$count = count($accommodation);
$accommodation_count = $count +1;
?>

    <th rowspan="<?php echo $accommodation_count;?>" style="width:20%;">Accommodation,Modifications,Supports</th>
  </tr> 
   <?php /*?> <?php foreach($accommodation as $accommodation_val){ ?><?php */?>
    <?php
if(!empty($accommodation_data)) { 
foreach($accommodation_data as $key=>$accommodation_val){
		  ?> 
 
<tr>
    <td><?php echo $accommodation_val['accommodation_modifications'];?></td> 
        <input type="hidden" name="accommodation_modifications_id[]" value="<?php echo $key;?>" id="accommodation_modifications" />
    <td><input type="text" name="accommodation_modifications_effective_<?php echo $key;?>" value="<?php echo $accommodation_val['accommodation_modifications_effective'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
    <td><input type="text" name="accommodation_modifications_future_changes_<?php echo $key;?>" value="<?php echo $accommodation_val['accommodation_modifications_future_changes'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
   </tr>
   <?php }}?>
<!--end additional Factors--> 

<!--start additional Factors-->    
  <tr>
   <?php 
$count = count($preparation);
$preparation_count = $count +1;
?>
  <th rowspan="<?php echo $preparation_count ;?>" style="width:20%;">Preparation for three year review IEP(complete at second annual review aiep meeting</th>
    </tr>
   <?php /*?><?php foreach($preparation as $preparation_val){ ?> <?php */?>
    <?php
if(!empty($preparation_data)) { 
foreach($preparation_data as $key=>$preparation_val){
		  ?> 
<tr>
    <td><?php echo $preparation_val['preparation'];?></td>
        <input type="hidden" name="preparation_id[]" value="<?php echo $key;?>" id="preparation" />
     <td><input type="text" name="preparation_effective_<?php echo $key;?>" value="<?php echo $preparation_val['preparation_effective'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
    <td><input type="text" name="preparation_future_changes_<?php echo $key;?>" value="<?php echo $preparation_val['preparation_future_changes'];?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td>
</tr>    
<?php }}?>
    
<!--end Accommodation,Modifications,Supports--> 
  
  <tr>
    <th  style="width:20%;">%Time Outside Of General Education</th>
    <td>% &nbsp;<input type="text" name="time_outside" value="<?php echo $summary_of_iep[0]->time_outside;?>" size="16" class="m-ctrl-medium" style="width: 200px;"></input></td> 
    <td></td>
    <td></td>
    </tr>
  
    <tr>
    <th colspan="4"style="width:20%;">
     <?php $team = $summary_of_iep[0]->team_exceeds;?>
    <input type="checkbox" class="checkboxes" name="team_exceeds"  <?php if ($team =='1') echo 'checked="checked"';?> value="1" />
    
     The IEP acknowledges that the parent of time outside of the general education classroom as determined by the team exceeds 60%</th>
    
    </tr>
  
</table>
                                         
                                         
                                     
                                     <BR><BR><BR>
                                           
                                           <div class="span12">
                      
                        <center>
                        <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record</button>
                        <!--<button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Review Record</button>
                        <button class="btn btn-large btn-red"><i class="icon-edit icon-white"></i> Modify Record</button> 
                        <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record and Send</button> 
                         <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record and Exit</button>
                         -->
                        </center>
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     <!--<ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>-->
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>   
</body>
<!-- END BODY -->
</html>