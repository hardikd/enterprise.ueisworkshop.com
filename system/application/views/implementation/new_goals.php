<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
       <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                     <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker">Create IEP Record </a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/develop_iep">Develop IEP</a>
                             <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>implementation/create_student_goals ">Create Student Goals </a>
                           
                       </li>
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <form class="form-horizontal" action="<?php echo base_url();?>implementation/create_student_goals_insert" name="createplanstep1" id="createplanstep1" method="post">
                        
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Create Student Goals</h4>
                          
                         </div>
                         <div class="widget-body">
<!--                            <form class="form-horizontal" action="#">-->
                                <div id="pills" class="custom-wizard-pills-red4">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                    
                                    
                               
                                       
                                     
                                 </ul>
                                   <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                  
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                                   <div class="control-group">
                    
                                         
                                        
                                       <!--  <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="cdate" required></input>
                                            </div>
                                        </div>-->
                                  <input type="hidden" name="iep_id" value="<?php echo $id;?>" id="" />
                                   <input type="hidden" name="gol_standard" value="" id="gol_standard" />
                                 <input type="hidden" name="grade" id="grade" value=''>
            						<input type="hidden" name="standard_id" id="standard_id" value=''>
            						<input type="hidden" name="standarddata_id" id="standarddata_id" value="">
                                     
                                     <div class="control-group">
                                  <label class="control-label">Select Subject</label>
                                       <div class="controls">
                                        
          
		<select class="span12 chzn-select" name="subject_id" id="subject_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                            <option value=""></option>
                                            <option value="all">All</option>
                                            <?php if(!empty($subjects)) { 
                                            foreach($subjects as $val)
                                            {
                                            ?>
                                            <option value="<?php echo $val['subject_id'];?>" ><?php echo $val['subject'];?>  </option>
                                            <?php } } ?>
                                            </select>  
                                           </div>
                                         </div>
                                         
                                         
                                        
                                             <div class="control-group">
                        <label class="control-label">Goal Bank</label>
                        <div class="controls">
                          <select class="span12 chzn-select" name="strand" id="commoncorestrand" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" onChange="selectstandard(this.value, 0, 0);" required>
                            <option value="0">Please select</option>
                            
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Common Core Cluster</label>
                        <div class="controls" id="standards">
                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" required>
                            <option value=""></option>
                       
                          </select>
                        </div>
                      </div>
                      
                      <div class="space20"></div>
                      <div class="space20"></div>
                      <div class="control-group">
                        <label class="control-label">Incremental objective #1 related to the goal:</label>
                        <div class="controls">
                          <textarea class="span12 " rows="5" name="standarddata" id="standardeledesc" required></textarea>
                        </div>
                      </div>
                      
                       <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="incremental_date_1" required></input>
                                            </div>
                                        </div>
                      
                           <div class="control-group">
                        <label class="control-label">Incremental objective #2 related to the goal:</label>
                        <div class="controls">
                          <textarea class="span12 " rows="3" name="standarddisplay" id="standarddisplay" required></textarea>
                        </div>
                      </div>
                      
                       <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp2" type="text" value="" size="16" class="m-ctrl-medium" name="incremental_date_2" required></input>
                                            </div>
                                        </div>
                              
                              
                         
                              
                         
                         

                                 <center>
                                 <button class="btn btn-large btn-red" name="add" value="add" type="submit" style="float:left;"><i class="icon-save icon-white"></i> Add Goal</button>
     		<button class="btn btn-large btn-red" name="submit" value="submit" type="submit"><i class="icon-save icon-white"></i> Save Goal</button>
				<a class="btn btn-large btn-red" href="<?php echo base_url() . 'implementation/determine_student_assessments/' .$id?>"> Exit Without Save</a>
                              
                              </center>            
                                     </div>
                                     
                                  

                                     </div>
                                     </div>
                                    
                                     
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
<!--      </div>
       END PAGE   
   </div>-->
<!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

<script src="<?php echo SITEURLM ?><?php echo $view_path; ?>js/teacherplan.js"></script> 


<script>
var base_url = '<?php echo base_url();?>';
$('#subject_id').change(function(){
	$('#commoncorestrand').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>implementation/get_subject_by_strand",
	  data: { subject_id: $('#subject_id').val() }
	})
	  .done(function( msg ) {
		  $('#commoncorestrand').html('');
		  var result = jQuery.parseJSON( msg )
		   $('#commoncorestrand').append('<option value="all">All</option>');
		 $(result.strands).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#commoncorestrand').append('<option value="'+Element.strand+'">'+Element.strand+'</option>');
		});  
	$(result.grades).each(function(index, Element){ 
		console.log(Element);
		  $('#grade').val(Element.grade_id);
		  $('#standard_id').val(Element.standard_id);
		}); 
		
		  $("#commoncorestrand").trigger("liszt:updated");
		  
	  });
  	
});
	
   </script> 


</body>
<!-- END BODY -->
</html>