<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<script>base_url = '<?php echo base_url(); ?>';</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path . 'inc/header.php'); ?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
      <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid"> 
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          
          <h3 class="page-title"> <i class="icon-book"></i>&nbsp; Planning Manager </h3>
          <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker">Create IEP Record </a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/develop_iep">Develop IEP</a>
                             <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>implementation/create_student_goals ">Create Student Goals </a>
                           
                       </li>
                       
                       
                      
                   </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <form class="form-horizontal" action="<?php echo base_url(); ?>implementation/create_student_goals_insert" name="createplanstep2" id="createplanstep2" method="post">
        <div class="row-fluid">
          <div class="span12"> 
            <!-- BEGIN BLANK PAGE PORTLET-->
          <input type="hidden" name="iep_id" value="<?php echo $id;?>" id="" />
         <input type="hidden" name="grade" id="grade" value='<?php echo $grades[0]['grade_id']; ?>'>
            <input type="hidden" name="standard_id" id="standard_id" value='<?php echo $grades[0]['standard_id']; ?>'>
            <input type="hidden" name="standarddata_id" id="standarddata_id" value="">
             
           
            <div class="widget red">
              <div class="widget-title">
                <h4>Create Student Goals</h4>
              </div>
              <div class="widget-body">
                <div id="pills" class="custom-wizard-pills-red4">
                  <ul>
                    <li><a href="#pills-tab1" data-toggle="tab" id="pillstab1">Step 1</a></li>
                    <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                   
                  </ul>
                  <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                  <div class="tab-content"> 
                    
                    <!-- BEGIN STEP 2-->
                    <div class="tab-pane" id="pills-tab2">
                      <h3 style="color:#000000;">STEP 2</h3>
                      <div class="control-group">
                        <label class="control-label">Goal Bank</label>
                        <div class="controls">
                          <select class="span12 chzn-select" name="strand" id="commoncorestrand" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" onChange="selectstandard(this.value, 0, 0);" required>
                            <option value="0">Please select</option>
                            <?php foreach ($strands as $val) { ?>
                            <option value="<?php print_r($val['strand']); ?>"><?php print_r($val['strand']); ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Common Core Cluster</label>
                        <div class="controls" id="standards">
                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" required>
                            <option value=""></option>
                            <!--                                        <option>Common Core Element Description 1</option>
                                                                                                        <option>Common Core Element Description 2</option>
                                                                                                        <option>Common Core Element Description 3</option>-->
                          </select>
                        </div>
                      </div>
                      
                      <div class="space20"></div>
                      <div class="space20"></div>
                      <div class="control-group">
                        <label class="control-label">Incremental objective #1 related to the goal:</label>
                        <div class="controls">
                          <textarea class="span12 " rows="5" name="standarddata" id="standardeledesc" required></textarea>
                        </div>
                      </div>
                      
                       <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="incremental_date_1" required></input>
                                            </div>
                                        </div>
                      
                           <div class="control-group">
                        <label class="control-label">Incremental objective #2 related to the goal:</label>
                        <div class="controls">
                          <textarea class="span12 " rows="3" name="standarddisplay" id="standarddisplay" required></textarea>
                        </div>
                      </div>
                      
                       <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp2" type="text" value="" size="16" class="m-ctrl-medium" name="incremental_date_2" required></input>
                                            </div>
                                        </div>
                           <center>
                                <button class="btn btn-large btn-red" name="submit" value="submit" type="submit"><i class="icon-save icon-white"></i> Save Changes</button>
                               <!-- <a  role="button" href="javascript:void(0);" class="btn btn-large btn-red" onclick="submitfrm('print');" ><i class="icon-print icon-white"></i> Print</a>
                                <button class="btn btn-large btn-red" onclick="submitfrm('viewlessonplan');"><i class="icon-eye-open icon-white"></i> View Lesson Plan</button>-->
                              </center>
                              
                             </div>
                    
                    <!-- BEGIN STEP 3-->
                    
                    
                    <!-- BEGIN STEP 4-->
                    
                  </div>
                  <ul class="pager wizard">
                    <li class="previous first red"><a href="javascript:;">First</a></li>
                    <li class="previous red"><a href="javascript:;">Previous</a></li>
                    <li class="next last red"><a href="javascript:;">Last</a></li>
                    <li class="next red"><a  href="javascript:;" onClick="$('#createplanstep1').submit();" >Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- END BLANK PAGE PORTLET--> 
          
        </div>
      </form>
    </div>
    
    <!-- END PAGE CONTENT--> 
    
  </div>
  <!-- END PAGE CONTAINER--> 
</div>
<!-- END PAGE --> 
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script> 
<script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script> 
<script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script> 
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
                                                                <script src="js/excanvas.js"></script>
                                                                <script src="js/respond.js"></script>
                                                                <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/gritter/js/jquery.gritter.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>js/jquery.pulsate.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>js/jquery.validate.js"></script> 

<!--common script for all pages--> 
<script src="<?php echo SITEURLM ?>js/common-scripts.js"></script> 
<!--script for this page--> 

<script src="<?php echo SITEURLM ?>js/form-component.js"></script> 
<script src="<?php echo SITEURLM ?><?php echo $view_path; ?>js/teacherplan.js"></script> 

<!-- END JAVASCRIPTS --> 

<script>
                                                                        var Script = function() {


                                                                            $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
                                                                                    console.log('onShow1');
                                                                                }, onNext: function(tab, navigation, index) {

                                                                                    console.log('onNext');
                                                                                }, onPrevious: function(tab, navigation, index) {
                                                                                    console.log('onPrevious');
                                                                                }, onLast: function(tab, navigation, index) {
                                                                                    console.log('onLast');
                                                                                }, onTabShow: function(tab, navigation, index) {
                                                                                    //        console.log(tab);
                                                                                    //        console.log(navigation);
                                                                                    //        console.log(index);
                                                                                    //        console.log('onTabShow1');
                                                                                    var $total = navigation.find('li').length;
                                                                                    var $current = index + 1;
                                                                                    console.log($current);
                                                                                    if ($current == 4) {
                                                                                        $('#createplanstep1').submit();
                                                                                    }
                                                                                    var $percent = ($current / $total) * 100;
                                                                                    $('#pills').find('.bar').css({width: $percent + '%'});
                                                                                }});

                                                                            $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
                                                                                    console.log('onShow2');
                                                                                }, onNext: function(tab, navigation, index) {
                                                                                    console.log('onNext');
                                                                                }, onPrevious: function(tab, navigation, index) {
                                                                                    console.log('onPrevious');
                                                                                }, onLast: function(tab, navigation, index) {
                                                                                    console.log('onLast');
                                                                                }, onTabClick: function(tab, navigation, index) {
                                                                                    console.log('onTabClick');

                                                                                }, onTabShow: function(tab, navigation, index) {
                                                                                    console.log('onTabShow2');
                                                                                    var $total = navigation.find('li').length;
                                                                                    var $current = index + 1;
                                                                                    var $percent = ($current / $total) * 100;
                                                                                    $('#tabsleft').find('.bar').css({width: $percent + '%'});

                                                                                    // If it's the last tab then hide the last button and show the finish instead
                                                                                    if ($current >= $total) {
                                                                                        $('#tabsleft').find('.pager .next').hide();
                                                                                        $('#tabsleft').find('.pager .finish').show();
                                                                                        $('#tabsleft').find('.pager .finish').removeClass('disabled');
                                                                                    } else {
                                                                                        $('#tabsleft').find('.pager .next').show();
                                                                                        $('#tabsleft').find('.pager .finish').hide();
                                                                                    }

                                                                                }});


                                                                            $('#tabsleft .finish').click(function() {
                                                                                alert('Finished!, Starting over!');
                                                                                $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
                                                                            });

                                                                        }();


                                                                        $(document).ready(function() {

                                                                            $('#2').change(function() {
                                                                                $('#lesson_type').val($('#2 option:selected').text());
                                                                            });

                                                                            $('#pillstab1').click(function() {
                                                                                redirecturl = "<?php echo base_url() . 'implementation/create_student_goals'; ?>";
                                                                                $(location).attr('href', redirecturl);

                                                                            });

                                                                            $("#createplanstep2").validate({
                                                                                rules: {
                                                                                    commoncorestrand: {
                                                                                        required: function() {
                                                                                            if ($("#commoncorestrand option[value='0']")) {
                                                                                                return true;
                                                                                            } else {
                                                                                                return false;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });

                                                                            $("select").change(function() {
                                                                                var divId = $(this).find(":selected").attr("data-div-id");
                                                                                $(".standard-topic").hide();
                                                                                $("#" + divId).show();

                                                                            });


                                                                        });
                                                                </script> 
<script>
                                                                    $(function() {
                                                                        $(" input[type=radio], input[type=checkbox]").uniform();
                                                                    });

                                                                    $("#pills").bootstrapWizard("show", 1);

                                                                       function submitfrm(btnname){
                                                                           $('#btnaction').val(btnname);
                                                                           $('#createplanstep2').submit();
                                                                       }
                                                                       <?php if(strtolower($type)=='edit'){?>
                                                                            $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getteacherplaninfo/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  
                                                                                  result = jQuery.parseJSON(result);
//                                                                                  console.log(result);
                                                                                  $('#commoncorestrand').val(result.lesson_week[0].strand).trigger("liszt:updated");
                                                                                  selectstandard(result.lesson_week[0].strand, 0, 0);
//                                                                                  $('#standardeledesc').val(result.lesson_week[0].standard);
                                                                                  selectst(result.lesson_week[0].standard,<?php echo $lesson_week_plan_id;?>);
                                                                                  $.each(result.lesson_week, function(seindex, sevalue) {
                                                                                      console.log(sevalue);
                                                                                        $('#'+sevalue.lesson_plan_id).val(sevalue.lesson_plan_sub_id).trigger("liszt:updated");


                                                                                        });
//                                                                                        $('#diff_instruction').val(result.lesson_week[0].diff_instruction);
//                                                                                        $('#diff_instruction').data("wysihtml5").editor.setValue(result.lesson_week[0].diff_instruction);
                                                                                        CKEDITOR.instances.diff_instruction.setData( result.lesson_week[0].diff_instruction );
                                                                                        CKEDITOR.instances.diff_instruction.updateElement();
                                                                                        
                                                                //                alert('done');  
                                                                //                $('#lesson_plans').html(result);
                                                                                //do something
                                                                              }
                                                                         });
                                                                         
                                                                         $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getcustomdiff/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  result = jQuery.parseJSON(result);
                                                                                  console.log(result);
                                                                               $.each(result.custom, function(seindex, sevalue) {
//                                                                                      console.log(sevalue);
                                                                                        $("textarea[name='c_"+sevalue.custom_differentiated_id+"']").val(sevalue.answer)
//                                                                                        $('#c_'+sevalue.custom_differentiated_id).val(sevalue.answer);
                                                                                        });   
                                                                           }
                                                                         });
                                                                       <?php }?>
                                                                       
                                                                </script>
</body>
<!-- END BODY -->
</html>