<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

<style type="text/css">
.ac_results {
	padding: 0px;
	border: 1px solid black;
	background-color:#FFF;
	overflow: hidden;
	z-index: 99999;
}

.ac_results ul {
	width: 100%;
	list-style-position: outside;
	list-style: none;
	padding: 0;
	margin: 0;
}

.ac_results li {
	margin: 0px;
	padding: 2px 5px;
	cursor: default;
	display: block;
	/* 
	if width will be 100% horizontal scrollbar will apear 
	when scroll mode will be used
	*/
	/*width: 100%;*/
	font: menu;
	font-size: 12px;
	/* 
	it is very important, if line-height not setted or setted 
	in relative units scroll will be broken in firefox
	*/
	line-height: 16px;
	overflow: hidden;
}

.ac_loading {
	background: white url('../indicator.gif') right center no-repeat;
}

.ac_odd {
	background-color: #eee;
}

.ac_over {
	background-color:#999;
	color: white;
}

</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
     <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                      <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/iep_tracker">Create IEP Record </a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
						</i> <a href="<?php echo base_url();?>implementation/basic_student_meeting_information">Basic Student & Meeting Information</a>
                        <span class="divider">></span>   
                       </li>
                       
					   <li>
                   </i> <a href="<?php echo base_url();?>implementation/basic_student_information">Basic Student Information </a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Basic Student Information </h4>
                          
                         </div>
                         <div class="widget-body">
                  <form method="post" class="form-horizontal" action="<?php echo base_url() . 'implementation/basic_student_information_insert'; ?>" name="basic_student_information_form" id="basic_student_information_form">
                                <div id="pills" class="custom-wizard-pills-red3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                    <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                      
                                      
                     <?php if($this->session->userdata("login_type")=='user') { ?>                     
                                        
                                       <?php if(isset($school_type)) $countst = count($school_type);
								if(isset($school_type) && $countst >0){ ?>   
                                             <div class="control-group">
                                  
                                             <label class="control-label">Select School Type :</label>
                                             <div class="controls">
<select class="span12 chzn-select" name="schools_type" id="schools_type" tabindex="1" style="width: 300px;" onchange="updateschool(this.value);">
      									<option value=""  selected="selected">Please Select</optgroup>
									       <?php  foreach($school_type as $k => $v)
	  										{
											?>
								           <option value="<?php echo $v['school_type_id']; ?>"><?php echo $v['tab']; ?></option>
						            	  <?php } ?>
                        			    </select>
                                             </div>
                                         </div> 
                                         
                                          <div class="control-group">
                                             <label class="control-label">Schools :</label>
                                             <div class="controls">
				<select class="span12 chzn-select" name="school_id" id="school_id" tabindex="1" style="width: 300px;" onchange="updateclass(this.value)">
                 	<option value=""  selected="selected">Please Select</option>
              						</select>
                                             </div>
                                         </div> 
                                         
                    <div class="control-group">
                        <label class="control-label">Teachers :</label>
                           <div class="controls">
                   		<select class="combobox span12 chzn-select" name="teacher_id" id="teacher_id" style="width: 300px;" >
               <option value=""  selected="selected">Please Select</option>
          </select>
                         </div>
                      </div> 
                      
                                         
               
        <?php }}?>      
                                      
      <?php  if ($this->session->userdata('login_type') == 'observer') {?>
      <div class="control-group">
			<label class="control-label">Select Teacher</label>
           <div class="controls">
<select data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id" class="span12 chzn-select">
			    	<option value=""></option>
			    	<?php if(!empty($teachers)) { 
							foreach($teachers as $val)
							{
							?>
									<option value="<?php echo $val['teacher_id'];?>" 
							<?php if(isset($teacher_id) && $teacher_id==$val['teacher_id']) {?> selected <?php } ?>
							><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
									<?php } } ?>
								  </select>                                            
                                   </div>
                               </div>
                               
 
      <?php }?>
      
<?php  if ($this->session->userdata('login_type') == 'observer' || $this->session->userdata("login_type")=='user'){?>
                  <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
           <input type="text" class="span6 " name="studentlist" value="" id="studentlist" style="width:300px;"  />
            <input type="hidden" name="student_id" id="student_id"  />
          
              </div>
      </div>                   
      <?php }?>
                                            
      <?php  if ($this->session->userdata('login_type') == 'teacher') {?>                                   
      <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select name="student_id" id="student" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <?php if(!empty($students)) { 
			foreach($students as $studentsvalue)
			{?>
			  <option value="<?php echo $studentsvalue['student_id'];?>"><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
			  <?php } } ?>			   
		      </select> 
              </div>
      </div>
      <?php }?>
     										 <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
							<input id="grade_id" type="text" name="" value="" size="16" readonly class="m-ctrl-medium" style="width: 300px;"></input> 
                            	<input id="grade" type="hidden" name="grade_id" value="" size="16" class="m-ctrl-medium" style="width: 300px;"></input>
                                             </div>
                                         </div>
                                         
                                            <div class="control-group">
                                    <label class="control-label">Select Date Of Birth</label>
                                    <div class="controls">
                                   <input id="dp2" type="text" name="birth_date" value="" size="16" class="m-ctrl-medium" style="width: 300px;"></input>
                                    </div>
                                </div>
                                         
                                        <div class="control-group">
                                    <label class="control-label">Gender</label>
                                    <div class="controls">
				<input type="radio" name="gender" checked value="male" id="gender" class="m-ctrl-medium" style="width: 250px;">Male</input>
                <input type="radio" name="gender" value="female" id="gender" class="m-ctrl-medium" style="width: 250px;">Female</input>
                                    </div>
                                </div> 
                                
                               <div class="control-group">
                                    <label class="control-label">Limited English Proficiency	</label>
                                    <div class="controls">
				<input type="radio" name="limited_english_proficiency" checked value="yes" id="gender" class="m-ctrl-medium" style="width: 250px;">Yes</input>
                <input type="radio" name="limited_english_proficiency" value="no" id="gender" class="m-ctrl-medium" style="width: 250px;">No</input>
                                    </div>
                                </div> 
                                
                                
                                <div class="control-group">
                                    <label class="control-label">Ethnic code </label>
                                    <div class="controls">
			<select name="ethnic_code" id="ethnic_code" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <?php if(!empty($ethnic_code)) { 
			foreach($ethnic_code as $ethnic_value)
			{?>
			  <option value="<?php echo $ethnic_value->ethnic_code;?>"><?php echo $ethnic_value->ethnic_code;?> </option>
			  <?php } } ?>			   
		      </select> 
                

                                    </div>
                                </div>         
                                         <BR><BR>
                                      
                                      
                                     </div>
                                     
                                     
                                      <!-- BEGIN STEP 2-->
                                   <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <div class="control-group" style="display:none;">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="" name="date" size="16" class="m-ctrl-medium" style="width: 285px;"></input>
                                    </div>
                                </div>
   
   
   						                             <div class="space20"></div>
                                                     
                                                     
				<div class="control-group">
<!--                <label class="control-label">Dates</label>-->
                </div>   
                                      <?php
									  $date_number = 4;
                                        if (!empty($dates)) {
                                        foreach ($dates as $dates_data) {
                                      ?>
                                 
                                   <div class="control-group">
                                             <label class="control-label"><?php echo $dates_data->dates;?></label>
                                             <input  type="hidden" name="dates_id[]" value="<?php echo $dates_data->id;?>" id="dates" />
                                             <div class="controls">
                                             
     <input <?php if($dates_data->field_type == Date){?> type="text" id="dp<?php echo $date_number ?>" <?php }else{?> type="text"<?php }?> value="" name="dates_description_<?php echo $dates_data->id;?>" size="16" class="m-ctrl-medium" style="width: 300px;"></input>                  
          							</div>
                                         </div>
                                        <?php $date_number++?> 
							<?php }}?>                                         
					
                                         
                                 
                                 
                                
                                               <div class="space20"></div>

            <div>
              <div class="control-group">
                <label class="control-label">Meeting Type</label>
                <div class="space15"></div>
              <?php foreach($meeting_type as $meeting_types){?>
                    <div class="span3"><div>
  <input type="checkbox" name="meeting_type[]" value="<?php echo $meeting_types->meeting_type;?>"><?php echo $meeting_types->meeting_type;?></br>
                </div>
                  <!--<div style="padding:0px; margin:-18px 0px 0px 25px" ></div>-->
                    </div>
                 
                <?php }?>
              </div>
            </div>    
            <BR><BR>
                                   
            </div>
                                  
                                      <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000">STEP 3</h3>
                                         <div class="control-group">
                             <div class="control-group">
                                             <label class="control-label">Home Language</label>
                                             <div class="controls">
          <select name="home_language" id="home_language" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;">
             <option value=''></option>
            <?php if(!empty($language)) { 
			foreach($language as $language_value)
			{?>
			  <option value="<?php echo $language_value->language;?>"><?php echo $language_value->language;?> </option>
			  <?php } } ?>			   
		      </select>
             
                                             </div>
                                         </div>
                                         <div class="control-group">
                                             <label class="control-label">Student Language</label>
                                             <div class="controls">
    <select name="student_language" id="student_language" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;">
            <option value=''></option>
            <?php if(!empty($language)) { 
			foreach($language as $language_value)
			{?>
			  <option value="<?php echo $language_value->language;?>"><?php echo $language_value->language;?> </option>
			  <?php } } ?>			   
		      </select> 
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Alternate Mode of Communication</label>
                                             <div class="controls">
             <input type="text" name="alternate_mode_of_communication" value="" id="" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Home Address of Student</label>
                                             <div class="controls">
             <input type="text" name="address" value="" id="address" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">City</label>
                                             <div class="controls">
             <input type="text" name="city" value="" id="city" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Zip Code</label>
                                             <div class="controls">
             <input type="text" name="zip_code" value="" id="zip_code" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                     
                                     
                                     
                                     
                                     <BR><BR><BR>
                                           
                                           <div class="span12">
                      
                        
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                                                           <!-- BEGIN STEP 3-->
                                                <div class="tab-pane" id="pills-tab4">
                                         <h3 style="color:#000000">STEP 4</h3>
                                         <div class="control-group">
                           
                                         
                                         <div class="control-group">
                                             <label class="control-label">Home Telephone</label>
                                             <div class="controls">
             <input type="text" name="home_telephone" value="" id="telephone" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Daytime Telephone</label>
                                             <div class="controls">
             <input type="text" name="daytime_telephone" value="" id="" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Emergency Telephone</label>
                                             <div class="controls">
             <input type="text" name="emergency_telephone" value="" id="emergency_telephone" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">School of Attendance</label>
                                             <div class="controls">
             <input type="text" name="school_of_attendance" value="" id="" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">School of Residence</label>                                            
                                              <div class="controls">
             <input type="text" name="school_of_residence" value="" id="" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Location Code</label>
                                             <div class="controls">
             <input type="text" name="location_coad" value="" id="location_coad" class="m-ctrl-medium" style="width: 250px;"></input>
                                             </div>
                                         </div>
                                         
                                         
                                     
                                     <BR><BR><BR>
                                           
                                           <div class="span12">
                      
                        <center>
<button name="submit" id="submit" value="submit" class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record</button> 
                       </center>
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                
       <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red" onclick='$("#pills").bootstrapWizard("show", 0);'>OK</button>
                                </div>
                                
                                 
                            </div>

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
 
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
     <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>



   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
</script>  

<script>

$('#student').change(function(){
	$('#grade_id').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>implementation/get_student_by_grade",
	  data: { student_id: $('#student').val() }
	})
	  .done(function( msg ) {
		  $('#grade_id').html('');
		  var result = jQuery.parseJSON( msg )
		  
		  $(result.grade).each(function(index, Element){ 
		 
		 console.log(Element.grade_name);
		  $('#grade_id').val(Element.grade_name);
		   $('#grade').val(Element.dist_grade_id);
		});  
		  $("#grade_id").trigger("liszt:updated");
		  
	  });
  	
});

   </script>
 <script>

$('#studentlist').blur(function(){
	$('#grade_id').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>implementation/get_student_by_grade",
	  data: { student_id: $('#student_id').val() }
	})
	  .done(function( msg ) {
		  $('#grade_id').html('');
		  var result = jQuery.parseJSON( msg )
		  
		  $(result.grade).each(function(index, Element){ 
		 
		 console.log(Element.grade_name);
		  $('#grade_id').val(Element.grade_name);
		   $('#grade').val(Element.dist_grade_id);
		});  
		  $("#grade_id").trigger("liszt:updated");
		  
	  });
  	
});


   </script>  
   
   
 <script>
$('#student').change(function(){
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>implementation/getstudentinfo",
	  data: { student_id: $('#student').val() }
	})
	  .done(function( msg ) {
		  var result = jQuery.parseJSON( msg )
		  
		  $(result.student).each(function(index, Element){ 
		 
		 console.log(Element.grade_name);
		  $('#address').val(Element.address);
		   $('#telephone').val(Element.phone);
		    $('#home_language').val(Element.language);
			$('#student_language').val(Element.lastnamesub);
		});  
		 
		  
	  });
  	
});


	var newurl= '';
	$('#teacher_id').change(function(){
		newurl = "<?php echo base_url();?>implementation/getStudentsByTeacher/"+$(this).val();

		});
	
	 $("#studentlist").autocomplete("<?php echo base_url();?>implementation/getStudentsByTeacher/", {
        width: 260,
        matchContains: true,
        selectFirst: false,
		max:15,
		
					//select end
    });
	 $("#studentlist").result(function(event, data, formatted) {
        $("#student_id").val(data[1]);
		
		
    });
	
	$('#studentlist').setOptions({
        extraParams: {
          teacher_id: function(){
            return $('#teacher_id').val();
          }
  }
})


   </script>  
   
   
     <script type="text/javascript">
function updateschool(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#school_id").html(result);
			$("#school_id").trigger("liszt:updated");
		}
		});
	
}
</script>
<script type="text/javascript">
$('#school_id').change(function(){
   $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>teacherself/getteacherBySchoolId",
            data: { school_id: $('#school_id').val()}
          })
            .done(function( result) {
//              console.log(teachers);
                $('#teacher_id').html('');
                $('#teacher_id').append('<option></option>');
              var teachers = jQuery.parseJSON(result);
              $.each(teachers,function(index,value){
//                    console.log(value);
                    $('#teacher_id').append('<option value="'+value.teacher_id+'">'+value.firstname+' '+value.lastname+'</option>');
              });
              $('#teacher_id').trigger("liszt:updated");
              
              
            });

});</script> 

   
</body>
<!-- END BODY -->
</html>