<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
     <base href="<?php echo base_url(); ?>"/>
   <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
      <?php require_once($view_path . 'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/observation">Lesson Observation </a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/observation_edit">Edit Report</a>
                       
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Edit Report</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-red5">
                                 <ul>
                                 <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                 <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                 <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                 <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                 <li><a href="#pills-tab5" data-toggle="tab">Step 5</a></li>   
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Teacher</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Teacher 1</option>
                                        <option>Teacher 2</option>
                                        <option>Teacher 3</option>
                                    </select>
                                             </div>
                                         </div>
                                         
                                         <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="02-18-2014" size="16" class="m-ctrl-medium">
                                    </div>
                                </div>
                                
                                 <div class="control-group">
                                             <label class="control-label">Edit Observation Tool</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="Proficiency Rubric" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Scaled Rubric</option>
                                        <option>Proficiency Rubric</option>
                                        <option>Likert</option>  
                                    </select>
                                             </div>
                                         </div>
                                         
                                       
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <form class="form-horizontal" action="#">
                                      
                                         
                                 
                                
                                         
                                           <div class="control-group">
                                      <label class="control-label">Edit Lesson Correlation</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="Observation" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Walkthrough</option>
                                        <option>Lesson Plan</option>
                                        <option>Observation</option>
                                    </select>
                                             </div>
                                         </div>
                                         
                                          </div>
                                         
                                         
                                          <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                         <form class="form-horizontal" action="#">
                                         
                                         
                                
                                 <div class="control-group">
                                             <label class="control-label">Edit Subject</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="History" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Math</option>
                                        <option>English</option>
                                        <option>History</option>  
                                    </select>
                                             </div>
                                         </div>
                                         
                                 
                                   <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="5th Grade" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                         <option >Pre-K</option>
                                        <option >Kindergarten</option>
                                        <option >1st Grade</option>
                                        <option>2nd Grade</option>
                                        <option >3rd Grade</option>
                                        <option >4th Grade</option>
                                        <option >5th Grade</option>
                                        <option >6th Grade</option>
                                        <option >7th Grade</option>
                                        <option >8th Grade</option>
                                        <option >9th Grade</option>
                                        <option >10th Grade</option>
                                        <option >11th Grade</option>
                                        <option >12th Grade</option>  
                                    </select>
                                             </div>
                                         </div>
                                        
                                       
                                     </div>
                                     
                                   
                                        
                                           <!-- BEGIN STEP 4-->
                                     <div class="tab-pane" id="pills-tab4">
                                         <h3 style="color:#000000;">STEP 4</h3>
                                         
                                         <div class="control-group">
                                <label class="control-label">Edit Number of Students Present</label>
                                <div class="controls">
                                    <input type="text" class="span6 " value="12">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">Endit Number of Paraprofessionals Present</label>
                                <div class="controls">
                                    <input type="text" class="span6 " value="2">
                                </div>
                            </div>
                                         
                                         
                                        </div>
                                     
                                     
                                     
                                                   <!-- BEGIN STEP 5-->
                                     <div class="tab-pane" id="pills-tab5">
                                         <h3 style="color:#000000;">STEP 5</h3>
                                         
                                 <div class="control-group">
                                             <label class="control-label">Edit Observer</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="Observer 2" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Observer 1</option>
                                        <option>Observer 2</option>
                                       
                                    </select>
                                             </div>
                                         </div>
                            
                                         
                                          <div class="control-group">
                                             <label class="control-label">Edit Evaluation Type</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="Informal" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Formal</option>
                                        <option>Informal</option>
                                       
                                    </select>
                                             </div>
                                         </div>
                                         
                                      
                                                                               
                                          <div class="control-group">
                                             <label class="control-label">Edit Comments</label>
                                             </div>
                                        
                                        <div class="space10"></div>
                                      
                                     <div class="control-group">
                                
                                        <textarea class="span12 ckeditor" name="editor1" rows="12" value="">Observation comments will appear here and can be edited</textarea>
                                 
                                </div>
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save File</button> <button class="btn btn-large btn-red"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                                
                                
                                  
                                      
                                     </div>
                                       
                                         
                                         
                                         
                                           
                                         </div>
                                    
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM ?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  


   
 
</body>
<!-- END BODY -->
</html>