﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District Id</td>
     <td>First Name</td>
     <td>Last Name</td>
     <td>Email</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1 +($page -1)*10;
   $sno = 1;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="problem_<?php echo $data->id;?>">
   <td><?php echo $sno;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->firstname;?></td>
   <td><?php echo $data->lastname;?></td>
   <td><?php echo $data->email;?></td>
   
      <td>
       <button class="edit_behaviour btn btn-primary" type="button" name="<?php echo $data->colleague_id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->colleague_id;?>" />
    <td>
          <button type="Submit" id="remove_behaviour" value="Remove" name="<?php echo $data->colleague_id;?>" data-dismiss="modal" class="remove_behaviour btn btn-danger"><i class="icon-trash"></i></button>  
            
        </td>
  </tr>
  <?php  
  $con++;
  $sno++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_behaviour').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'incident_receiptients/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#colleague_id').val(result[0].colleague_id);
       $('#firstname').val(result[0].firstname);
       $('#lastname').val(result[0].lastname);
       $('#email').val(result[0].email);
      $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_behaviour").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'incident_receiptients/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#problem_"+id).css('display','none');
          alert('Successfully removed record from list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

