<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Qualitative Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper2">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/lubricreportmenu.php'); ?>
        <?php 
		
		if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<div class="content"  >
        	
			<table>
			<tr>
			<td >
			<?php if($groups!=false) {
			
			?>			
			<div class="htitle"><?php
			echo $group_name;?></div>
			
			
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
			</td>
			</tr>
			</table>
			<?php if($qualitative!=false) { ?>
			<table>
			<tr>
			<td>
			Qualitative Data
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $this->session->userdata('report_criteria');?>     Name:<?php echo $this->session->userdata('report_name');?>
			</td>
			</tr>
			
			<?php foreach($qualitative as $val) {
			?>
			<tr>
			<td>
			<?php echo $val['report_date'];?>
			</td>
			</tr>
			<tr>
			<td>
			<textarea name="data"><?php echo $val['responsetext'];?>
			</textarea>
			</td>
			</tr>
			
			<?php } ?>
			</table>
			<?php } else { ?>
			<table>
			<tr>
			<td>
			<?php echo $this->session->userdata('report_criteria');?>     Name:<?php echo $this->session->userdata('report_name');?>
			</td>
			</tr>
			<tr>
			<td>
			No Data Found
			</td>
			</tr>
			</table>
			<?php } ?>
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
