<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">

function check()
{

 if(document.getElementById('school').value=='')
{
 alert('Please Select School');
 return false;

}else if(document.getElementById('year').value=='')
{
 alert('Please Select Year');
 return false;

}
else
{
	return true;

}
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<form  action='report/get_teacher_observation' name="report" method="post" onsubmit="return check()">
			<table cellpadding="5">			
			<tr>
			<td>
			 Select School:
			  </td>
			  <td>
			  <select class="combobox" name="school" id="school" >
			  <option value="">-Please Select-</option>
			   <?php if($school!=false) { 
			   foreach($school as $schoolval) {
			   ?>
			   <option value="<?php echo $schoolval['school_id'];?>"><?php echo $schoolval['school_name'];?></option>
			   <?php } } ?>
			  
			  </select>
			  </td>
			  
			</tr>			
			<tr>
			<td>
			 Select Year:
			  </td>
			  <td>
			  <select class="combobox" name="year" id="year" >
			  <option value="">-Please Select-</option>
			  <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i;			
			echo " >".$i."</option>";    
			} ?>
			  
			  </select>
			  </td>
			  
			</tr>			
			<tr >			
			<td>
			</td>
			<td ><input title="Get Report" class="btnbig" type="submit" name="submit" value="Get Report"></td>
			</tr>
			</table>
      </form>
			
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>