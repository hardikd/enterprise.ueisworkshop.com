<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM;?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body>
<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
      <span style="font-size:16px;">Single Assessment Report by Standard</span>
      
                  
        	<?php
			
		if(!empty($finalscore))
				{
				
			   	$schoolnames= $finalscore[2][0]['school_name'];
			 
				if(empty($finalscore[0][0]['schoolaverage']))
				{
					$schoolsavg=0;
				}	
				else
				{
				 	 $schoolsavg= $finalscore[0][0]['schoolaverage'];
				}
				
			

			 $finalname = "['".$schoolnames."']";  // get all school names
			
			$res = mysql_query('select * from proficiency where assessment_id="'.$assessmentid.'"');
		$row = mysql_fetch_assoc($res);
		
			$bbfrom = $row['bbasicfrom'];
			$bbto = $row['bbasicto'];
			$basicfrom = $row['basicfrom'];
			$basicto = $row['basicto'];
			$pro_from = $row['pro_from'];
			$pro_to = $row['pro_to'];
			
			$bbasic=0;
			$basic=0;
			$pro=0;
			//$adv=0;
			$num = round($schoolsavg);
			if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					 $basic = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro = $num;
				}
		
			
			}
				else
				{
				$schoolsavg=0;
				
				
				}
			
			?>

	            
<?php if(!empty($schoolsavg))
			{
			?>
            <div style=" color:#000000; font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size:16px">
<table cellpadding="5" cellspacing="0" width="100%">
<tr>
<td width="200" style="font-weight:bold;">Test Item Cluster</td><td><?php

	if(!empty($testcluster) && $testcluster!="")
	{
 		echo substr(@$testcluster,0,500);
	}
 
 ?></td>
</tr>
</table>

  </div>		
<script src="<?php echo SITEURLM;?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM;?>js/exporting.js"></script>

       <script type="text/javascript">
$(function () {
        $('#piechart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Pie Chart Proficiency'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
			
            series: [{
                type: 'pie',
                name: 'School Proficiency ',
                data: [
                    ['Below Basic',  <?php echo $bbasic; ?>],
                    ['Basic',      <?php echo $basic; ?>],
					
                   
					{
                        name: 'Proficient',
                        y: <?php echo $pro; ?>,
                        sliced: true,
                        selected: true
                    }
                ]
            }]
        });
    });
    

		</script>
        
<script type="text/javascript">
$(function () {

        $('#barchart').highcharts({
            chart: {
            },
            title: {
                text: 'Bar Chart'
            },
            xAxis: {
                categories: <?php echo $finalname;?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' fruits';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
            labels: {
                items: [{
                    html: 'Proficiency By School',
                    style: {
                        left: '40px',
                        top: '-25px',
                        color: 'black'
                    }
                }]
            },
            series: [  {
                type: 'column',
                name: 'Proficiency By School',
                data: [<?php echo $schoolsavg; ?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [<?php echo $schoolsavg; ?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }]
        });
    });
    

		</script>
      
        
        
<div id="piechart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div id="barchart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
			
	<?php } else {
			echo "<div style='color:red;'>Records Not Available</div>";  
		}?>	
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>