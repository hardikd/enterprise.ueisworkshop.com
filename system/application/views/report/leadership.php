<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/leadership.js" type="text/javascript"></script>
<script type="text/javascript">
function school_change(id)
{
	
	 
	 var g_url='observer/getobserverByschoolId/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="observer_id" id="observer_id" ><option value="">-Select-</option>';
	if(result.observer!=false)
	{
	$.each(result.observer, function(index, value) {
	str+='<option value="'+value['observer_id']+'">'+value['observer_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Observers Found</option></select>';
	  
	}
     $('#observer_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<form name="reportform" id="reportform" action="report/createleadership" method="post">
			<table class="tabreport">
			<tr>
			<td colspan="2" align="center" class="htitle">
			Create New Report
			</td>
			</tr>
			<tr>
			<td>
			Report Date:
			</td>
			<td>
			<input  class="txtbox" type="textbox" name="report_date" id="report_date">
			</td>
			</tr>
			<tr>
			<td>
			School Name:
			</td>
			<td>
			<?php if($this->session->userdata('login_type')=='user')
			{
			?>
			<select class="combobox1" name="school_id" id="school_id" onchange="school_change(this.value)" >
		<?php if($school!=false) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>"  ><?php echo $val['school_name'];?></option>
		<?php } } else { ?>
		<option value="">No Schools Found</option>
		<?php } ?>
		</select>
			<?php
			}
			else
			{
			?>
			<?php echo $this->session->userdata("school_name");?>
			<input type="hidden" name="school_id" value="<?php echo $this->session->userdata("school_id");?>">
			<?php } ?>
			</td>
			</tr><tr>
			<td>
			Observer:
			</td>
			<td>
			<select  class="combobox" name="observer_id" id="observer_id">
		<option value="">-select-</option>
		<?php if(!empty($observers)) { 
		foreach($observers as $val)
		{
		?>
		<option value="<?php echo $val['observer_id'];?>" ><?php echo $val['observer_name'];?>  </option>
		<?php } } ?>
		</select>
			</td>
			</tr>
			<tr>
			<td>
			<input  class="btnsmall" type="submit" name="submit" value="Next">
			</td>
			</tr>
			</table>
			</form>	
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
