<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<?php if(!empty($reportdata)) {?>
			<table class="tabcontent1">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $sno;?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			
            
			</tr>
			<tr>
			
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
			
			</tr>
			
			</table><br />
			<?php } ?>
			<table>
			<?php 
			$group_again=0;
			$c=0;
			if($alldata!=false) {	?>
			<?php
			foreach($alldata as $key=> $val)
			{
			$check=0;
			$scheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			if($c>0)
			{?>
			
			</table>
			<?php 
			}
			?>
			<br />
			<table border="0" cellpadding="5" cellspacing="0" style="border:1px #a2ba88 solid; width:667px;">
			<tr>
    <td style="width:100px;font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $val['group_name'];?></td>
  </tr>
  <tr>
    <td style="font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $val['description'];?></td>
  </tr>
			<?php } $c++; ?>
			<tr>
			<td style="border-top:1px #a2ba88 solid;border-bottom:1px #a2ba88 solid; background:#f0f5eb; width:100px;">
			Element
			</td>
			<?php foreach($val['names'] as $names)
			{
			
			?>
			<td style="border-top:1px #a2ba88 solid;border-bottom:1px #a2ba88 solid; border-left:1px #a2ba88 solid; background:#f0f5eb;">
			<?php echo $names;?>
			</td>
			<?php } ?>
			</tr>
			<tr>
			<td style="border-bottom:1px #a2ba88 solid;">
			<?php echo $val['question'];?>
			</td>
			<?php foreach($val['text'] as $text)
			{
			
			?>
			<td style="border-bottom:1px #a2ba88 solid; border-left:1px #a2ba88 solid;">
			<?php echo $text;?>
			</td>
			<?php } ?>
			</tr>
			<?php if($this->session->userdata('login_type')=='user')
			{
			?>
			<tr>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			 <?php 
			if(!isset($getselfreportpoints[0]))
			{
			if($val['ques_type']=='radio' && !isset($getselfreportpoints[$key]['response'])) {
			
			//$scheck=1;
			}
			if(isset($getselfreportpoints[$key]['response']) && $getselfreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else  
			if(isset($getselfreportpoints[$key][$subgroupid])) {
			?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg">
			<?php } else  if($scheck==1 && $key1==0) {?> <img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else { ?>
			<img src="<?php echo SITEURLM?>images/unchecked.jpg">
			<?php } ?>
			
			<?php }
			else {	
			if(isset($getselfreportpoints[$key]['response']) && $getselfreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> <img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else  
			if(isset($getselfreportpoints[$key][$subgroupid])) {
			?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg">
			<?php } else   {?> <img src="<?php echo SITEURLM?>images/unchecked.jpg"> <?php } } ?>
			Self Assessment
			</td>
			<?php } ?>
			
			</tr>
			<tr>
			<td>
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td>
			 <?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$key]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$key]['response']) && $getreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else  
			if(isset($getreportpoints[$key][$subgroupid])) {
			?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg">
			<?php } else  if($check==1 && $key1==0) {?> <img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else { ?>
			<img src="<?php echo SITEURLM?>images/unchecked.jpg">
			<?php } ?>
			District Assessment
			</td>
			<?php } ?>
			
			</tr>
		 
			<?php
			} 
			else
			{
			?>
			<tr>
			<td>
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td>
			 <?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$key]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$key]['response']) && $getreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else  
			if(isset($getreportpoints[$key][$subgroupid])) {
			?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg">
			<?php } else  if($check==1 && $key1==0) {?> <img src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else { ?>
			<img src="<?php echo SITEURLM?>images/unchecked.jpg">
			<?php } ?>
			Self Assessment
			</td>
			<?php } ?>
			
			</tr>
			
			<?php
			
			
			
			}
			
			$group_again=$val['group_id'];
			}
		  ?>
		    </table>
			<?php
			} 
			else
			{
			?>
			<table>
			<tr>
			<td>
			No Observation Points Found
			</td>
			</tr>
			</table>
			<?php } ?>
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
