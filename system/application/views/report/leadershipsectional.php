<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Sectional Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/reportnext.js" type="text/javascript"></script>

</head>

<body>

<div class="wrapper1">

	<?php require_once($view_path.'inc/header.php'); ?>
	<?php require_once($view_path.'inc/class.htmlgraph.php'); 
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	?>

	
    <div class="mbody">
    	<?php require_once($view_path.'inc/leadershipsectionalmenu.php'); ?>
        <?php 
		
		
	
		if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<div class="content"  >
       	  <table width="100%" border="0">
        	  <tr>
        	    <td>
                
                	<?php if($groups!=false) {
			
			?>			
			<div class="htitle"><?php
			echo $group_name;?></div>
			
			
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
            
            </td>
      	    </tr>
        	  <tr>
        	    <td><div style="font-size:14px;">Sectional Data</div>
		
			<div class="desc"><?php echo ucfirst($this->session->userdata('report_criteria'));?>     Name:&nbsp;&nbsp;<?php echo $this->session->userdata('report_name');?></div>
			
			</td>
      	    
			</tr>
        	  <tr>
        	    <td>
                
                	<?php if($points!=false) {
			$login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('school_id');

			}	?>

			<?php  if($this->session->userdata('reportform')=='forma' && $group_set==1)
			{ ?>
			<table>
			<tr>
						  <td colspan="2">
						  <font color="#08A5D6"><b>All Elements Pie chart</b></font>
						  </td>
						  </tr>
						  <tr>
  <td colspan="2">
  <img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$point_set;?>.png" style="border: 1px solid gray;"/>
  </td>
</tr>
</table>
<?php } ?>
			<?php			
			
			//echo "<pre>";
			//print_r($points);
			//print_r($sectional);
			//exit;
			$point_again=0;
			$group_id=0;
			$allpiec=0;
			$allpiedata=array();
			$previousnext=0;
			foreach($points as $val)
			{
			$previousnext++;
			if($this->session->userdata('reportform')=='forma')
			{
			if($point_again!=$val['point_id'] && $group_id==2)
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			?>
			  <tr>
  <td colspan="2">
 <?php $chart = new PieChart(600, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	?>
	<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$piedata[0]['point_id'];?>.png" style="border: 1px solid gray;"/>
  </td>
  </tr>
			
			<?php } } 
			
			if($previousnext==1)
			{
			?>
			
			<table width="100%">
			<tr>
			<td>
			<div style="float:right; "><input type="button" id="previous" name="previous" value="<<" style="display:none">
			<input type="button" id="next" name="next" value=">>" style="display:none"></div>
			</td>
			</tr>
			</table>
			
			<?php }
			}
			if($val['group_type_id']!=2)
			{
			?>
                
                <table class="questab" >
  <tr>
    <td colspan="2"><div class="ques1"><?php echo $val['question'];?></div></td>
    </tr>
  <tr>
    <td class="hlft">Report Number:</td>
    <td class="rc1"><?php 
			$rcc=1;
			if($reports!=false) { 
			$rc=0;
			
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $reportval['report_id'];?>
			</div>
			<?php 
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			} } else { ?>
			<div>
			No Reports Found
			</div>
			<?php } ?>
			</div>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
			</td>
		 
  </tr>
  <tr>
    <td><div class="hlft">Frequency:</div></td>
    <td class="rcf1"><?php 
			$count=0;
			
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			<?php 
			
			
			 }
			else
			{?>
			
	<div class="hbox">&nbsp;</div>
			
			<?php }
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} } else { ?>
			
			<?php } ?></td>
			<?php if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  <div>
  <?php 
  $gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();
	?>
	</div>
  </td>
  <?php } ?>
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div>
	
			</td>
 
  </tr>
  <?php if($this->session->userdata('reportform')=='forma')
			{ 
			
			$allpiec++;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiedata[$allpiec]['count']=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];

			?>
  <!--<tr>
  <td colspan="2">
 <?php /*$chart = new PieChart(600, 300);

	$dataSet = new XYDataSet();
	$dataSet->addPoint(new Point($val['question'].'('.$count.')', $count));
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$val['point_id'].".png");*/
	?>
	<img alt="Pie chart"  src="<?php //echo WORKSHOP_DISPLAY_FILES;?>generated/<?php //echo $val['point_id'];?>.png" style="border: 1px solid gray;"/>
  </td>
  </tr>-->
  <?php } ?>
</table>
</td>
      	    </tr>
   	      <tr>
        	    <td>	<?php 
			}
			else if($val['group_type_id']==2) {
			
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			?><table class="questab">
  <tr>
    <td colspan="3"><div class="ques1"><?php echo $val['question'];?></div></td>
    </tr>
  <tr>
    <td><div class="hlft">Report Number:</div></td>
    <td class="rc1">	<?php 
			$rcc=1;
			if($reports!=false) { 
			$rc=0;
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $reportval['report_id'];?>
			</div>
			<?php 
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			} } else { ?>
			<div>
			No Reports Found
			</div>
			<?php } ?>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
			</td>
  
  </tr>
  <tr>
    <td><div class="hlft"><?php echo $val['sub_group_name'];?></div></td>
    <td class="rcf1">	<?php 
			$count=0;
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			<?php 
			
			
			 }
			else
			{?>
				<div class="hbox">&nbsp;</div>
			<?php 
			
			 }
			 if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} 
			
			} else { ?>
	
			<?php } ?></td>
   
		<?php if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  <div>
  <?php 
  $gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();
	?>
	</div>
  </td>
  <?php } ?>
			
  </tr>  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
  
  </tr>
<?php  
$piec=0;
$piedata[$piec]['name']=$val['sub_group_name'];
$piedata[$piec]['count']=$count;
$piedata[$piec]['point_id']=$val['point_id'];
} 
			else {
			$piec++;
			?>

  <tr>
    <td ><div class="hlft"> <?php echo $val['sub_group_name'];?></div></td>
    <td class="rcf1">			<?php 
			$count=0;
			if($reports!=false && $sectional!=false) {
			$rc=0;
			$rcc=1;			
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			<?php 
			
			
			 }
			else
			{?>
				<div class="hbox">&nbsp;</div>
			<?php 
			
			 }
			 if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			}?> 
			
			<?php } else { ?>

			<?php } ?></td>
			 <?php if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  <div>
  <?php 
  $gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();
	?>
	</div>
  </td>
  <?php } ?>
    
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
 
  </tr>
               		
        	      <?php 
				  
				  $piedata[$piec]['name']=$val['sub_group_name'];
				  $piedata[$piec]['count']=$count;
				  $piedata[$piec]['point_id']=$val['point_id'];
				  } ?> 
			
			
			<?php }
			
			$point_again=$val['point_id'];
			$group_id=$val['group_type_id'];
			
			}
			if($this->session->userdata('reportform')=='forma')
			{
			if(!isset($plotteddata[$point_again]) )
			{
			if(isset($piedata[0]['point_id']))
			{
			if($point_again==$piedata[0]['point_id'])
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			?>
			  <tr>
  <td colspan="2">
 <?php $chart = new PieChart(600, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	?>
	<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$piedata[0]['point_id'];?>.png" style="border: 1px solid gray;"/>
  </td>
  </tr>
			
			 
			
			<?php
			}
			}
			}
			}
			}
			
			} 
			else
			{
			?>
			
	
			No Observation Points Found
	
			<?php } //print_r($getreportpoints); ?>
			<?php  if($this->session->userdata('reportform')=='forma' && isset($allpiec) && $allpiec!=0)
			{ ?>
						  
 <?php $chart = new PieChart(650, 350);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=1;$pj<=$allpiec;$pj++)
	{
		//print_r($piedata[$pj]['name']);
		
		
		if(strlen($allpiedata[$pj]['name'])>35)
		{
		   
		   $limitname=substr($allpiedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$allpiedata[$pj]['name'];
		
		}
		$dataSet->addPoint(new Point(trim($limitname).'('.$allpiedata[$pj]['count'].')', $allpiedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$allpiedata[1]['point_id'].".png");
	?>
	
<?php } ?>
 </table>
</td>

      	  </table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
