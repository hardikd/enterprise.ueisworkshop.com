<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu" >
              <li class="sub-menu">
                  <a class="" href="../index.html">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
                <!-- FIRST LEVEL MENU Planning Manager-->
            <li class="sub-menu" >
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="../planning-manager/lesson-plan.html">Lesson Plan Creator</a></li>
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="third">
                    <li><a class="" href="../planning-manager/lesson-plan.html">Create Lesson Plan</a></li></ul>
                    
                     <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../planning-manager/create-plan.html">Add Plan</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../planning-manager/upload-plan.html">Upload Plan</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/edit-plan.html">Edit Lesson Plan</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/completed-plan.html">Retrieve Lesson Plan</a></li>
                </ul>
                
                   <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="../planning-manager/smart-goals.html">SMART Goals Creator</a></li>
                     
                      <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/create-goal.html">Create Goal</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/edit-goal.html">Edit Goal</a></li>
                </ul>
                
                <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="../planning-manager/calendar.html">Calendar Creator</a></li>
                             
                         <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/new-event.html">Create New Event</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/retrieve-event.html">Retrieve Event</a></li>
                </ul>
                
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Attendance Manager</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/daily-attendance.html">Daily Attendance</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../attendance/take-roll.html">Take Roll</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../attendance/retrieve-roll.html">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="../attendance/new-student-enrollment.html">New Student Enrollment</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../attendance/modify-enrollment.html">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../attendance/enroll-student-enrollment.html">Enroll Student</a></li>
                </ul>  
                  <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="../attendance/roster.html">Student Roster</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../attendance/create-roster.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../attendance/edit-roster.html">Edit</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
					  <li><a class="" href="../attendance/data.html">Add-Map Data</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../attendance/add-parent-info.html">Add Parent Information</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../attendance/map-parent.html">Map Parent/Student Data</a></li>
                </ul>  
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu" >
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Implementation Manager</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../implementation/grade-tracker.html">Grade Tracker</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-grade.html">Enter Grade</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-grade.html">Retrieve Grade</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       <li><a class="" href="../implementation/homework-tracker.html">Homework Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-homework-record.html">Enter Homework Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-homework-record.html">Retrieve Homework Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                        <li><a class="" href="../implementation/iep-tracker.html">IEP Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-iep-record.html">Enter IEP Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-iep-record.html">Retrieve IEP Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="../implementation/eld-tracker.html">ELD Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-eld-record.html">Enter ELD Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-eld-record.html">Retrieve ELD Record</a></li>
                </ul>  
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="../implementation/instructional-efficacy.html">Instructional Efficacy</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/observation.html">Lesson Observation Feedback</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/observation-full-report.html">Retrieve Full Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/observation-sectional-report.html">Retrieve Sectional Data Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/self-reflection.html">Self-Reflection</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/generate-report.html">Create/Edit Report</a></li></ul>
              
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/retrieve-report.html">Retrieve Report</a></li>
                </ul>
                
                
                  </ul>
              </li>
              
              
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
              <li class="sub-menu" >
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Classroom Management</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                      <li><a class="" href="../classroom/progress-report.html">Student Progress Report</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-progress-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-progress-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-progress-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                          <li><a class="" href="../classroom/success-team.html">Student Success Team</a></li>
                           <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-success-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-success-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-success-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                              <li><a class="" href="../classroom/parent-teacher.html">Parent/Teacher Conference</a></li>
                              <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-parent-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-parent-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-parent-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="../classroom/teacher-student.html">Teacher/Student Conference</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-student-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-student-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-student-report.html">Retrieve</a></li>
                </ul>   
                
                  <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="../classroom/behavior-record.html">Behavior Running Record</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-behavior.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/edit-behavior.html">Edit</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-behavior.html">Upload</a></li>
                </ul>   
                
                  </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
              <li class="sub-menu" >
                  <a href="javascript:;" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Assesment Manager</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../assessment/diagnostic.html">Diagonstic Screener</a></li>
                      <li><a class="" href="../assessment/progress.html">Progress Monitoring</a></li>
                      <li><a class="" href="../assessment/benchmark.html">Benchmark Assesment</a></li>
 </ul>
              </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
              <li class="sub-menu" id="tools">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Tools &amp; Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  	  <li><a class="" href="../tools/data-tracker.html">Data Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/data-planning-manager.html">Planning Manager</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/assessment-manager.html">Assessment Manager</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/growth.html">View a Classroom's Growth</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/class-standard.html">Class Performance Score by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/graphical-report.html">Class Performance Graphs by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-summary.html">Individual Student Performance Summary</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-standard.html">Individual Student Performance by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/progress-report.html">Student Progress Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/attendance-manager.html">Attendance Manager</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../tools/implementation-manager.html">Implementation Manager</a></li>
                </ul>   
                
                  <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/grade-tracker.html">Grade Tracker</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/homework-tracker.html">Homework Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/iep-tracker.html">IEP Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/eld-tracker.html">ELD Tracker</a></li>
                </ul>
                
                   <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/observation.html">Lesson Observations</a></li>
                </ul>

<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/viewer-observation.html">Observation by Viewer</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/grade-observation.html">Observation by Grade</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/count-observation.html">Observation Count</a></li>
                </ul>

  <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/pd-activity.html">Professional Development Activity</a></li>
                </ul>
                
                
                
                   <ul class="sub">
                     <li><a class="" href="../tools/classroom-management.html">Classroom Management</a></li>
                     
                </ul> 
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-progress-report.html">Student Progress Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/success-team.html">Student Success Team Meeting Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/parent-teacher.html">Parent/Teacher Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/teacher-student.html">Teacher/Student Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/behavior-record.html">Behavior Learning & Running Record Report</a></li>
                </ul>
                <!-- SECOND LEVEL MENU -->  
                      <li><a class="" href="../tools/development.html">Professional Development</a></li>
                         <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/pd-individual.html">Individual</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/pd-group.html">Group</a></li>
                </ul> 
                <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="../tools/bridge.html">Parent Bridge</a></li> 
                     <!-- THIRD LEVEL MENU -->
                      <ul class="sub">
                     <li><a class="" href="../tools/send-notification.html">Parent Notification</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/notification-records.html">Parent Notification Records</a></li>
                </ul>                  
                      <li><a class="" href="../tools/guide.html">User/Training Guide</a></li>
                      
                  </ul><BR><BR>
              </li>
             
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                      <li>
                           <i class="icon-home"></i> <a href="../index.html">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="../tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="../tools/data-tracker.html">Data Tracker</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="../tools/implementation-manager.html">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="../tools/observation.html">Lesson Observations</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="../tools/grade-observation.html">Observation by Grade</a>
                           
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Observer Full Report</h4>
                          
                         </div>
                     
                     <div class="widget-body" style="min-height: 150px;">
                                   <div class="space20"></div>
                                  
                                      <div class="content">
        	<?php if(!empty($reportdata)) {?>
			<div style="font-size:14px;">
			<b>Report ID:</b><u> &nbsp;&nbsp;&nbsp;<?php echo $sno;?> &nbsp;&nbsp;&nbsp;</u>
			<b>Report Date:</b><u> &nbsp;&nbsp;&nbsp;<?php echo $reportdata['report_date'];?>&nbsp;&nbsp;&nbsp;</u>
			<b>School Name:</b><u> &nbsp;&nbsp;&nbsp;<?php echo $reportdata['school_name'];?>&nbsp;&nbsp;&nbsp;</u>
			<br /><br />		
			<b>Subject:</b><u>&nbsp;&nbsp;&nbsp;<?php echo $reportdata['subject_name'];?>&nbsp;&nbsp;&nbsp;</u>
			<b>Grade:</b><u>&nbsp;&nbsp;&nbsp;<?php echo $reportdata['grade_name'];?>&nbsp;&nbsp;&nbsp;</u>
			<b>Students Present:</b><u>&nbsp;&nbsp;&nbsp;<?php echo $reportdata['students'];?>&nbsp;&nbsp;&nbsp;</u>
			<b>Lesson Correlation:</b><u>&nbsp;&nbsp;&nbsp;<?php echo $reportdata['lesson_correlation'];?>&nbsp;&nbsp;&nbsp;</u>
			</div>
			<br /><br />
			<?php 
			if($reportdata['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { ?>
			<br />
			<table class="tabcontent1">
			<tr>
			<td class="htitle">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $standarddata[0]['standard'];?>
			</td>
			</tr>
			</table>
			<br />
			<table class="tabcontent1">
			<tr>
			<td class="htitle">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $standarddata[0]['diff_instruction'];?>
			</td>
			</tr>
			</table>
			<br />
			<?php } } ?>
			<?php 
			if($reportdata['lesson_correlation_id']==3)
			{
			
			if ( file_exists(WORKSHOP_FILES.'observationplans/'.$reportdata['report_id'].'.pdf') )
    {
			?>
			 
			 <table>
			 <tr>
			 <td>
			 <object  data="<?php echo WORKSHOP_DISPLAY_FILES.'observationplans/'.$reportdata['report_id'].'.pdf'?>" type="application/pdf"  width="650" height="220"></object>
			 </td>
			 </tr>
			 </table>
			 
			 <?php 
	         }		 
			 } ?>
			<table width="100px" align="right">
			<tr>
			<td>
			<a href="comments/viewreportformpdf/<?php echo $reportdata['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			</td>
			</tr>
			</table>
			<?php } ?>
			<table cellspacing="0" cellpadding="5">
			
			<?php if($points!=false) {	
			
			//echo "<pre>";
			//print_r($points);
		    $i=1;
			foreach($points as $val)
			{
			$i++;
			if($i%2==0)
			{
			
			?>
            
			 <tr> 
			<?php  } ?>
			<td > 
			<table style="border:1px #cccccc solid;width:300px;" cellspacing="0" cellpadding="3">
			<tr style="background-color:#cccccc;color:#000000;font-weight:bold;">
			<td colspan=2>
			<?php echo $val['scale_name'] ; ?>  <?php  if(!empty($val['sub_scale_name'])) { ?> --> <?php  echo $val['sub_scale_name'] ;  } ?>
			</td>
			
			</tr>
			<tr>
			<td>
			Strengths:
			</td>
			<td>
			<?php if(isset($val['strengths'])) { echo $val['strengths']; } ?>
			</td>
			</tr>
			<tr>
			<td>
			Concerns:
			</td>
			<td>
			<?php if(isset($val['concerns'])) { echo $val['concerns'] ; } ?>
			</td>
			</tr>
			</table>
			</td>
			<?php
			if($i%2!=0)
			{
			?>
            
			 </tr>
			<?php  } ?>
			<?php } 			
			} 
			else
			{
			?>
			
			<tr>
			<td>
			No  Points Found
			</td>
			</tr>
			<?php } ?>
			</table>
        </div>
                                 
                                  </div>
                                       
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     
                                 </div>
                             </div>
                           
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="../assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  <!--start old script -->
  <link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
if($('#form').val()=='formp'){ 
$('.formp').hide();
 
  }
  else
  {
    $('.formp').show();
  }
});
function check()
{

if(document.getElementById('grade_id').value=='')
{
 alert('Please Select Grade');
 return false;

}
else
{
	return true;

}
}
function changeform(val)
{
  if(val=='formp')
  {
     $('.formp').hide();
  
  
  }
  else
  {
    $('.formp').show();
  }


}
</script>
  <!--end old script -->
  
  

    <script>

$("#fullreport").on('click', function() {
   document.getElementById('fullreportDiv').style.display = "block";
   document.getElementById('sectionalreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#sectionalreport").on('click', function() {
   document.getElementById('sectionalreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#qreport").on('click', function() {
   document.getElementById('qreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('sectionalreportDiv').style.display = "none";
});




</script>


   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>