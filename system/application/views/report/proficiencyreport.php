<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">
function check()
{

if(document.getElementById('observer_id').value=='')
{
 alert('Please Select Observer');
 return false;

}
else
{
	return true;

}
}
function observer_change(id)
{
	var g_url='observer/getobserverByschoolId/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="observer_id" id="observer_id" ><option value="">-Select-</option>';
	if(result.observer!=false)
	{
	$.each(result.observer, function(index, value) {
	str+='<option value="'+value['observer_id']+'">'+value['observer_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Observers Found</option></select>';
	  
	}
     $('#observer_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<form action='report/getproficiencyreport' name="report" method="post" onsubmit="return check()">
			<table cellpadding="5">
			<?php
			if($this->session->userdata('login_type')=='user')
		{
		?>
			<tr>
			<td >School:</td>
		<td>
		<select class="combobox1" name="school" id="school" onchange="observer_change(this.value)" >
		<?php if($school!=false) { ?>
		<?php foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>" 
		<?php if(isset($school_id) && $school_id==$val['school_id']) {?> selected <?php } ?>
		><?php echo $val['school_name'];?></option>
		<?php } } else { ?>
		<option value="0">No Schools Found</option>
		<?php } ?>
		</select>
		</td>
		</tr>
		<?php } ?>
			<tr>
			<td valign="middle" class="f14">
			Observer:
			</td>
			<td colspan="2" valign="middle">
			  <select class="combobox" name="observer_id" id="observer_id">
			    <option value="">-select-</option>
			    <?php if(!empty($observers)) { 
		foreach($observers as $val)
		{
		?>
			    <option value="<?php echo $val['observer_id'];?>" 
		<?php if(isset($observer_id) && $observer_id==$val['observer_id']) {?> selected <?php } ?>
		><?php echo $val['observer_name'];?>  </option>
			    <?php } } ?>
		      </select>
			  </td>
			  </tr>
			  <tr>
			  <td>
			  </td>
			<td colspan="2" ><input title="Get Reports" class="btnbig" type="submit" name="submit" value="Get Reports" /></td>
			</tr>
			<!--<tr>
			<td valign="middle" class="f14">Sectional Data Report: </td>
			<td valign="middle" ><input title="Get Sectional Report" class="btnsmall_long2" type="submit" name="submit" value="Get Sectional Report" /></td>
			</tr>
			<tr>
			<td valign="middle" class="f14">Qualitative Data Report: </td>
			<td valign="middle">From: <select  class="comboboxsml" name="from" >
			
	<option value="2011">2011</option>
	<option value="2010">2010</option>
	<option value="2009">2009</option>
	<option value="2008">2008</option>
	<option value="2007">2007</option>
	<option value="2006">2006</option>
	<option value="2005">2005</option>
	<option value="2004">2004</option>
	<option value="2003">2003</option>
	<option value="2002">2002</option>
	<option value="2001">2001</option>
	<option value="2000">2000</option>
	<option value="1999">1999</option>
	<option value="1998">1998</option>
	<option value="1997">1997</option>
	<option value="1996">1996</option>
	<option value="1995">1995</option>
	<option value="1994">1994</option>
	<option value="1993">1993</option>
	<option value="1992">1992</option>
	<option value="1991">1991</option>
	<option value="1990">1990</option>
	<option value="1989">1989</option>
	<option value="1988">1988</option>
	<option value="1987">1987</option>
	<option value="1986">1986</option>
	<option value="1985">1985</option>
	<option value="1984">1984</option>
	<option value="1983">1983</option>
	<option value="1982">1982</option>


			</select>
			</td>
			<td valign="middle">To: <select class="comboboxsml" name="to" >
			
	<option value="2011">2011</option>
	<option value="2010">2010</option>
	<option value="2009">2009</option>
	<option value="2008">2008</option>
	<option value="2007">2007</option>
	<option value="2006">2006</option>
	<option value="2005">2005</option>
	<option value="2004">2004</option>
	<option value="2003">2003</option>
	<option value="2002">2002</option>
	<option value="2001">2001</option>
	<option value="2000">2000</option>
	<option value="1999">1999</option>
	<option value="1998">1998</option>
	<option value="1997">1997</option>
	<option value="1996">1996</option>
	<option value="1995">1995</option>
	<option value="1994">1994</option>
	<option value="1993">1993</option>
	<option value="1992">1992</option>
	<option value="1991">1991</option>
	<option value="1990">1990</option>
	<option value="1989">1989</option>
	<option value="1988">1988</option>
	<option value="1987">1987</option>
	<option value="1986">1986</option>
	<option value="1985">1985</option>
	<option value="1984">1984</option>
	<option value="1983">1983</option>
	<option value="1982">1982</option>


			</select>
			</td>
			<td valign="middle"><input title="Get Report" class="btnbig" type="submit" name="submit" value="Get Report"></td>
			</tr>-->
			</table>
      </form>
			<?php if(isset($reports) && !empty($reports)) { ?>
			<table  class="tabcontent" cellspacing="0">
			<tr class="tchead">
			<th>
			Report Id
			</th>
			<th>
			Date
			</th>
			<th>
			Observer
			</th>
			</tr>
			<?php foreach($reports as $reportval) { ?>
			<tr>
			<td valign="middle"  class="tdlink">
			<a href="report/viewproficiency/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><?php echo $sno;?><a>
			
			</td>
			<td valign="middle">
			<?php echo $reportval['report_date'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['observer_name'];?>
			</td>
			
			</tr>
			<?php 
			$sno++;
			} ?>
			</table>
			<table><tr><td valign="middle"><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td></tr></table>
			<?php } else if(isset($reports)) { ?>
			No Reports Found
			<?php } ?>
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
