<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />




</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
<?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                  <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/summative">Summative Report</a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>report/summativereport">Generate Report</a>
                       
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Generate Self-Reflection</h4>
                          
                         </div>
                         <div class="widget-body">
						 	<?php
			if($this->session->userdata('login_type')!='teacher')
		{
		?>
			<form class="form-horizontal" action='report/getsummativereport' name="report" method="post" onsubmit="return check()">
			<?php }  else { ?>
			<form class="form-horizontal" action='teacherreport/getsummativereport' name="report" method="post" onsubmit="return check()">
			<?php } ?>
                        
                                			<?php
			if($this->session->userdata('login_type')=='user')
		{
		?>

                                         <div class="control-group">
                                             <label class="control-label">School Name</label>
                                             <div class="controls"> 
                  <select class="span12 chzn-select" name="school_id" id="school_id"  style="width:300px;" onchange="school_change(this.value)" >
									<?php if($school!=false) { 
									foreach($school as $val)
									{
									?>
									<option value="<?php echo $val['school_id'];?>"  ><?php echo $val['school_name'];?></option>
									<?php } } else { ?>
									<option value="">No Schools Found</option>
									<?php } ?>
									</select>
                                   </div>
                                   </div>
<?php }?>
                                    
                                         <div class="control-group">
                                             <label class="control-label">Teacher</label>
                                             <div class="controls"> 
  <select class="span12 chzn-select" name="teacher_id" id="teacher_id"  style="width:300px;" >
			    <option value="all">-All-</option>
			    <?php if(!empty($teachers)) { 
		foreach($teachers as $val)
		{
		?>
			    <option value="<?php echo $val['teacher_id'];?>" 
		<?php if(isset($teacher_id) && $teacher_id==$val['teacher_id']) {?> selected <?php } ?>
		><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
			    <?php } } else { ?>
				<option value="0">No Teachers Found</option>
				<?php } ?>
		      </select>									
                    </div>
                    	</div>
                                <div class="control-group">
                                             <label class="control-label">Status</label>
                                             <div class="controls">
  <select class="span12 chzn-select" name="status" id="status"  style="width:300px;"  >
	  <option value="">-All-</option>
			    <?php if(!empty($statuses)) { 
		foreach($statuses as $val)
		{
		?>
			    <option value="<?php echo $val['status_name'];?>"
<?php if(isset($status) && $status==$val['status_name']) {?> selected <?php } ?>				
		><?php echo $val['status_name'];?>  </option>
			    <?php } } ?>	  
	  </select>
                                             </div>
                                         </div>
                                         
                                          <div class="control-group">
                                  <label class="control-label">Score</label>
                                             <div class="controls">
			<select name="score" id="score" data-placeholder="--Please Select--" class="span12 chzn-select" tabindex="1" style="width: 300px;">
      			  <option value="">-All-</option>
	   <?php if(!empty($scores)) { 
		foreach($scores as $val)
		{
		?>
			    <option value="<?php echo $val['score_name'];?>"
<?php if(isset($score) && $score==$val['score_name']) {?> selected <?php } ?>					
		><?php echo $val['score_name'];?>  </option>
			    <?php } } ?>	  
	  </select>
										</div>
                                         </div> 
                                
                                
                                
                                
                                <div class="control-group">
                                  <label class="control-label"></label>
                                             <div class="controls">
<input title="Get Report" class="btn btn-small btn-red btnbig" type="submit" name="submit" value="Get Report" style=" padding: 5px; ">
                                           </div>
                                         </div> 
                          </form>   
                   <div class="space20"></div>
                   
                   			<?php if(isset($reports) && !empty($reports)) { ?>
			
            <table  cellspacing="0" class="table table-striped table-hover table-bordered" id="editable-sample" style="min-width:100px;"> 
            <tr>
			<th>
			Report Id
			</th>
			<th>
			Teacher
			</th>
			<th>
			Status
			</th>
			<th>
			Score
			</th>
			<th>
			PDF
			</th>
			</tr>
			<?php foreach($reports as $reportval) { ?>
			<tr>
			
			<td>
			<?php echo $sno;?>
			</td>
			<td valign="middle">
			<?php echo $reportval['teacher_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['status'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['score'];?>
			</td>
			<td valign="middle">
			<a target="_none" href="pdf/reportpdf/<?php echo $reportval['file_path'];?>">PDF</a>
			</td>
			
			</tr>
			<?php 
			$sno++;
			} ?>
			</table>
			<table> 
            <tr>
            <td valign="middle"><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td>
            </tr>
            </table>
			
			 <table  width="100%" >
            <tr>
            <td><div style="float:right;"><a href="http://get.adobe.com/reader/" target="_blank" style="color:#ffffff;text-decoration:none;"><img src="<?php echo SITEURLM?>images/get_adobe_reader.png"></a></div></td>
            </tr>
            </table>
			
			<?php } else if(isset($reports)) { ?>
			<table  cellspacing="0" class="table table-striped table-hover table-bordered" id="editable-sample" style="min-width:100px;" align="center"> 
            <tr>
            <td><b>No Reports Found</b></td>
            </tr>
            </table>
			<?php } ?>
                            
                                    </div>
                                      
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
 <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script> 
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  <script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>

   <script type="text/javascript">
function check()
{


if($('#teacher_id').val()=='')
		   {
		     alert('Please Select Teacher');
			 return false;
		   
		   }
		   

           return true;
}
function school_change(id)
{
	var g_url='teacher/getTeachersBySchool/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="teacher_id" id="teacher_id" ><option value="all">-All-</option>';
	if(result.teacher!=false)
	{
	$.each(result.teacher, function(index, value) {
	str+='<option value="'+value['teacher_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="0">No Teachers Found</option></select>';
	  
	}
     $('#teacher_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
</script>

    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
</script>  
   
 
</body>
<!-- END BODY -->
</html>