<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<?php if(!empty($reportdata)) {?>
			
			<table class="tabcontent1">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $sno;?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['teacher_name'];?></a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['subject_name'];?></a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a class="bl"><?php echo $reportdata['grade_name'];?></a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a class="bl"><?php echo $reportdata['period_lesson'];?></a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['students'];?></a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['paraprofessionals'];?></a></td>
			<td align="left">
			<b>Lesson Correlation:</b></td><td align="left"><a class="bl"><?php echo $reportdata['lesson_correlation'];?></a></td>
			</tr>
			</table><br />
			<?php } ?>
			
			<?php 
			if($reportdata['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { ?>
			<table class="tabcontent1">
			<tr>
			<td class="htitle">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $standarddata[0]['standard'];?>
			</td>
			</tr>
			</table>
			<br />
			<table class="tabcontent1">
			<tr>
			<td class="htitle">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $standarddata[0]['diff_instruction'];?>
			</td>
			</tr>
			</table>
			<br />
			<?php } } ?>
			<?php 
			if($reportdata['lesson_correlation_id']==3)
			{
			
			if ( file_exists(WORKSHOP_FILES.'observationplans/'.$reportdata['report_id'].'.pdf') )
    {
			?>
			 
			 <table>
			 <tr>
			 <td>
			 <object  data="<?php echo WORKSHOP_DISPLAY_FILES.'observationplans/'.$reportdata['report_id'].'.pdf'?>" type="application/pdf"  width="650" height="220"></object>
			 </td>
			 </tr>
			 </table>
			 
			 <?php 
	         }		 
			 } ?>
			<?php if(!empty($reportdata)) { 
			
			?>
			
			<table width="100px" align="right">
			<tr>
			<td>
			<a href="comments/viewreportpdf/<?php echo $reportdata['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			</td>
			</tr>
			</table>
			<?php  } ?>
			<?php if($points!=false) {	?><table><?php
			
			//echo "<pre>";
		//	print_r($points);
			$point_again=0;
			$group_again=0;
			foreach($points as $val)
			{
			
			$gcheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			$gcounter=0;
			
			}
			else
			{
				$gcounter=1;
			
			}
			if($gcounter==0)
			{
			?>
			<tr>
			<td class="htitle">
			<?php echo $val['group_name'];?>
			</td>
			</tr>
			<tr>
			<td class="desc">
			<?php echo $val['description'];?>
			</td>
			</tr>
			
			
			<?php }
			
			
			if($val['group_type_id']!=2)
			{
			?>
			
			<tr>
			<td>
			
			
			
			
<?php if(isset($getreportpoints[$val['point_id']]['response']) && $getreportpoints[$val['point_id']]['response']==1 ) { ?> <img style="float:left" src="<?php echo SITEURLM?>images/checked.jpg"> <?php }else{ ?> <img style="float:left" src="<?php echo SITEURLM?>images/unchecked.jpg"> <?php }?> <div> <?php  echo $val['question'];?>
			</div>
			</td>
			</tr>
			
			<?php 
			}
			else if($val['group_type_id']==2) {
			$check=0;
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			?>
			
			<tr>
			<td class="ques">
			<?php echo $val['question'];?>
			</td>
			</tr>
			<tr>
			<td style="padding-left:34px;">
			<?php 
			//echo "<pre>";
			//print_r($getreportpoints[$point_id]);?>
			<?php 
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  ?>
			
			<?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$point_id]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$point_id]['response']) && $getreportpoints[$point_id]['response']==$val['sub_group_id'] && $val['ques_type']!='checkbox' ) { ?> <img style="float:left" src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else  
			if(isset($getreportpoints[$point_id][$val['sub_group_id']])) {
			?> 
			<img style="float:left" src="<?php echo SITEURLM?>images/checked.jpg">
			<?php } else if ($check!=1){?> <img style="float:left" src="<?php echo SITEURLM?>images/unchecked.jpg"> <?php }
			 if($check==1) {?> <img style="float:left" src="<?php echo SITEURLM?>images/checked.jpg"> <?php } ?>
			  <div style="" ><?php echo $val['sub_group_name'];?></div>
			
			
			</td>
			</tr>
			
			<?php }
			else {
			?>
			
			<tr>
			<td style="padding-left:34px;">
			
			
			<?php 
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  ?>
			
			<?php if(isset($getreportpoints[$point_id]['response']) && $getreportpoints[$point_id]['response']==$val['sub_group_id']  && $val['ques_type']!='checkbox' ) { ?> <img style="float:left" src="<?php echo SITEURLM?>images/checked.jpg"> <?php } else 
			if(isset($getreportpoints[$point_id][$val['sub_group_id']])) {
			?> 
			<img style="float:left" src="<?php echo SITEURLM?>images/checked.jpg">
			<?php }  else {?> <img style="float:left" src="<?php echo SITEURLM?>images/unchecked.jpg"> <?php } ?>
			
			 <div style=""> <?php echo $val['sub_group_name'];?></div>
			</td>
			</tr>
			<?php
			}
			
			}
			$point_again=$val['point_id'];
			$group_again=$val['group_id'];
			}
			} 
			else
			{
			?>
			
			<tr>
			<td>
			No Observation Points Found
			</td>
			</tr>
			<?php } ?>
			</table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
