<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM;?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body>
<div class="wrapper">
 
	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
       <h2 style="color: #08A5CE">Multiple Assessment Reports by Standard</h2>
      
      
        	<?php

			$finalschoolnames=array();
			for($i=0;$i<count($schname);$i++)
			{
				$finalschoolnames[]=$schname[$i][0];
			}
		
			  $namedata = implode("','",$finalschoolnames);
		
			$finalname = "['".$namedata."']";  // get all school names
		
			
			
			?>
           
<?php /*if(count($schoolsavg)!=$count)
			{*/
	
			$barvalues = implode(',',$bargrph);
			
			?>
			
<?php
		if(!empty($barvalues))
		{
		
		?>    
			   		     
<div style=" color:#000000; font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size:16px">
      <table cellpadding="5" cellspacing="0" width="100%">
      <tr>
  <td width="200" style="font-weight:bold;">Test Item Cluster</td><td><?php echo substr(@$cluster,0,500);?></td>
      </tr>
      </table>
      
  </div>
            
<script src="<?php echo SITEURLM;?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM;?>js/exporting.js"></script>
<script type="text/javascript">
$(function () {

        $('#barchart').highcharts({
            chart: {
            },
            title: {
                text: 'Bar Chart'
            },
            xAxis: {
                categories: <?php echo $finalname;?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' fruits';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
            labels: {
                items: [{
                    html: 'Proficiency By School',
                    style: {
                        left: '40px',
                        top: '-25px',
                        color: 'black'
                    }
                }]
            },
            series: [  {
                type: 'column',
                name: 'Proficiency By School',
                data: [<?php echo $barvalues;?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [<?php echo $barvalues;?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }]
        });
    });

		</script>
        <script type="text/javascript">
$(function () {
        $('#piechart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Pie Chart Proficiency'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
			
            series: [{
                type: 'pie',
                name: 'School Proficiency ',
                data: [
                    ['Below Basic',   <?php echo $finalbbasic; ?>],
                    ['Basic', <?php echo $finalbasic;?>],
					{
                        name: 'Proficient',
                        y: <?php echo $finalproficient;?>,
                        sliced: true,
                        selected: true
                    }
                ]
            }]
        });
    });
    

		</script>
        
        
<div id="piechart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div id="barchart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
			
		<?php } else {
			echo "<div style='color:red;'>Records Not Found</div>";  
		}?>	
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>