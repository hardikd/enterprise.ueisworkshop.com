<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<!--start old script -->
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
var subject_id = '<?php echo $reportdata['subject_id']; ?>';
var grade_id = '<?php echo $reportdata['grade_id']; ?>';
var school_id = '<?php echo $reportdata['school_id']; ?>';
var t = '<?php echo $reportdata['report_id']; ?>';
var WORKSHOP_DISPLAY_FILES = '<?php echo WORKSHOP_DISPLAY_FILES; ?>';
var lesson_correlation_id = '<?php echo $reportdata['lesson_correlation_id']; ?>';
var login_type = '<?php echo $this->session->userdata('login_type'); ?>';
var login_id = '<?php if($this->session->userdata('login_type')=='user') { echo $this->session->userdata('dist_user_id'); } else if($this->session->userdata('login_type')=='observer') { echo $this->session->userdata('observer_id'); } else {echo $this->session->userdata('teacher_id'); }?>';
function showm(v)
{
if(v=='m')
{
$('.materialshow').show();
$('.lessonshow').hide();
}
else if(v=='l')
{
$('.materialshow').hide();
$('.lessonshow').show();

}

}
selectmaterial();
function selectmaterial()
{

if(lesson_correlation_id==2)
{

var g_url='lesson_plan_material/getmaterial/'+subject_id+'/'+grade_id+'/'+school_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='';
	gradestr+='<table id="material" border="0" style="float: left;"><tr><td><b>Show In The Viewer:</b><input type="radio" name="materialid" checked value="m" onclick="showm(this.value)">Material&nbsp;&nbsp;<input type="radio" name="materialid" value="l" onclick="showm(this.value)">Lesson Plan</td></tr><tr><td class="materialshow">';
	if(result.materials!=false)
	{
	var c=(result.materials).length;
	
	$.each(result.materials, function(index, value) {
	
	if(index==0)
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="220"></object> </td></tr>';
	if(c>1)
	{
	gradestr+="<tr><td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td></tr>";
	}
	gradestr+='</table>';
	}
	else
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'" style="display:none;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="220"></object> </td></tr>';
	gradestr+="<tr><td><input type='button' name='previous' id='previous' value='<<' onclick='previouspage("+index+");'></td>";
	if(index<(c-1))
	{
	gradestr+="<td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td>";
	}
	gradestr+='</tr></table>';
	}
	
	});
	
    }
	else
	{
     
	 gradestr+='';
	}
	gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
	 
	 
	 
	 
	 
	 });
	 
	 
	 var g_url='report/createlessoncompdf/'+t+'/'+login_type+'/'+login_id;
    $.getJSON(g_url,function(result)
	{
		
		if(result.status==0)
		{
			var gradestr='<table  class="lessonshow" style="display:none;" border="0" style="float: left;"><tr><td>';

gradestr+='</td></tr></table>';    
	 $("#material").append(gradestr);

		
		
		}
		
		else
		{
		
		var gradestr='<table class="lessonshow" style="display:none;"  border="0" style="float: left;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'observation_lessonplan/'+t+'.pdf" target="_blank" style="color: #02AAD2;">LessonPlan.pdf</a></td></tr><tr><td>';

gradestr+='<object  data="'+WORKSHOP_DISPLAY_FILES+'observation_lessonplan/'+t+'.pdf" type="application/pdf"  width="650" height="220"></object> </td></tr></table>';    
	
	 $("#material").append(gradestr);
		
		
		}
	
	});
	 
	 }
else if(lesson_correlation_id==3)
{

   
	var g_url='report/createcompdf/'+t+'/'+login_type+'/'+login_id;
    $.getJSON(g_url,function(result)
	{
		
		if(result.status==0)
		{
			var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';

gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 

		
		
		}
		
		else
		{
		
		var gradestr='<table id="material" border="0" style="float: left;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'observationplans/'+t+'.pdf" target="_blank" style="color: #02AAD2;">ObservationPlan.pdf</a></td></tr><tr><td>';

gradestr+='<object  data="'+WORKSHOP_DISPLAY_FILES+'observationplans/'+t+'.pdf" type="application/pdf"  width="650" height="220"></object> </td></tr></table>';    
	
	$('#material').replaceWith(gradestr); 
		
		
		}
	
	});
}	 
else if(lesson_correlation_id==1)
{

var g_url='lesson_plan_material/getmaterial/'+subject_id+'/'+grade_id+'/'+school_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
	if(result.materials!=false)
	{
	var c=(result.materials).length;
	
	$.each(result.materials, function(index, value) {
	
	if(index==0)
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="200"></object> </td></tr>';
	if(c>1)
	{
	gradestr+="<tr><td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td></tr>";
	}
	gradestr+='</table>';
	}
	else
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'" style="display:none;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="200"></object> </td></tr>';
	gradestr+="<tr><td><input type='button' name='previous' id='previous' value='<<' onclick='previouspage("+index+");'></td>";
	if(index<(c-1))
	{
	gradestr+="<td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td>";
	}
	gradestr+='</tr></table>';
	}
	
	});
	
    }
	else
	{
     
	 gradestr+='No Materials Found.</td></tr><tr><td>';
	}
	gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
	 
	 
	 
	 
	 
	 });
	 } 

}
function nextpage(i)
{

var j=i+1;
document.getElementById('book_'+i).style.display='none';
document.getElementById('book_'+j).style.display='';
}
function previouspage(i)
{

var j=i-1;
document.getElementById('book_'+i).style.display='none';
document.getElementById('book_'+j).style.display='';
}
</script>
<style type="text/css">
.wrapper{
min-height:1000px;
}

#button {
    background-color: #DE577B;
    border: 0 none;
    color: #FFFFFF;
    cursor: default;
	border-radius: 15px;
    display: inline-block;
    padding: 7px 14px;
	float:left;
    margin-left: 5px;
	text-decoration: none;
	outline: 0 none;
	text-shadow: none !important;
}
.content {
    color: #657455;
    font-size: 11px;
    height: auto;
    margin-left: 10px;
    max-width: 646px;
}
</style>

<!--end old script -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
  
   <?php require_once($view_path.'inc/teacher_menu.php'); ?>

         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/observation">Lesson Observation </a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>report/index">Generate Report</a>
                       
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Generate Report</h4>
                          
                         </div>
                         <div class="widget-body">
								
                               <div class="content">
    	
        <?php if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<?php /* if($countgroups>=13) { ?> style="min-height:<?php echo $countgroups*70;?>px;height:auto;" <?php  }  */?>
		
        	<?php if(!empty($reportdata)) {?>
                    <table class="tabcontent1" style="width:100%;">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $this->session->userdata('report_number_new');?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['teacher_name'];?></a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['subject_name'];?></a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a class="bl"><?php echo $reportdata['grade_name'];?></a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a class="bl"><?php echo $reportdata['period_lesson'];?></a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['students'];?></a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['paraprofessionals'];?></a></td>
			<td align="left">
                            <b>Lesson Correlation:</b></td><td align="left" colspan="3"><a class="bl"><?php echo $reportdata['lesson_correlation'];?></a></td>
			</tr>
			</table>
            <br />
			<?php } ?>
			<div style=" width:665px; height:auto; border-left:#a2ba88 1px solid; border-top:#a2ba88 1px solid; border-bottom:#a2ba88 1px solid; font-size:11px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">
			<?php if($groups!=false) {
			
			?>			
			
			
			<table width="100%" border="0" cellpadding="5" style=" border-right:#a2ba88 1px solid; font-size:12px;  ">
  <tr>
    <td style="width:100px;font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $group_name;?></td>
  </tr>
  <tr>
    <td style="font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $description;?></td>
  </tr>
</table>

<!--			<div style="width:100px;height:auto; float:left; border-right:#a2ba88 1px solid;">
       		 	<div  style="padding:5px; font-weight:bold; color:#657455;font-size:12px;">Header:</div>
 		       <div style="padding:5px 5px 5px 5px; font-weight:bold;font-size:12px; color:#657455;">Standard:</div>
    	
   			 </div>
			<div style="padding:5px;font-size:12px; margin-left:100px; border-right:#a2ba88 1px solid;"><?php /*?><?php echo $group_name;?><?php */?></div>
     	   <div style="padding:5px 5px 5px 5px;font-size:12px; margin-left:100px; border-right:#a2ba88 1px solid;"><?php /*?><?php echo $description;?><?php */?></div>
			
			</div>-->
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
			
			<?php if($groups!=false) { ?>
			<form action="report/proficiencysave" name="" method="post">
			<table cellpadding="0" cellspacing="0">
			<tr>
			<td>
			<input type="hidden" name="group_id" value="<?php echo $group_id;?>">
			<input type="hidden" name="next_group_id" value="<?php echo $next_group_id;?>">
			</td>
			</tr>
			</table>
			<div style=" min-width:551px;">
			<table width="100%" cellpadding="5" cellspacing="0" >
			
            
		  <?php if($alldata!=false) { 
		    
		    foreach($alldata as $key=> $val)
			{
			$check=0;
			$scheck=0;
			

		  ?>	
			<tr>
			<td style="background:#f2f6ed;  font-weight:bold; border-top:#a2ba88 1px solid; border-bottom:#a2ba88 1px solid;color:#657455; border-right:#a2ba88 1px solid;font-size:12px; width:100px;">
			
            Element
			</td>
			<?php foreach($val['names'] as $names)
			{
			
			?>
			<td style="background:#f2f6ed; font-weight:bold; border-top:#a2ba88 1px solid; border-bottom:#a2ba88 1px solid; border-right:#a2ba88 1px solid;color:#657455;font-size:12px;">
			<?php echo $names;?>
			</td>
			<?php } ?>
			</tr>
			<tr>
			<td style="vertical-align:top;border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			<?php echo $val['question'];?>
			</td>
			<?php foreach($val['text'] as $text)
			{
			
			?>
			<td style="vertical-align:top;border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			<?php echo $text;?>
			</td>
			<?php } ?>
			</tr>
			
			<tr>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			<input type="<?php echo $val['ques_type']?>" name="<?php echo $name;?>" value="<?php echo $subgroupid;?>" <?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$key]['response'])) {
			
			//$check=1;
			}
			
			if(isset($getreportpoints[$key]['response']) && $getreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> checked=checked <?php } else  
			if(isset($getreportpoints[$key][$subgroupid])) {
			?> 
			checked=checked
			<?php } ?>
			<?php if($check==1 && $key1==0) {?> checked=checked <?php } ?>
			>Assessment
			</td>
			<?php } ?>
			
			</tr>
			
			
			
			
			<?php
			}
			} 
			else
			{
			?>
			
			<tr>
			<td>
			No proficiency Points Found
			</td>
			</tr>
			<?php } //print_r($getreportpoints); ?>
			
			<!--<tr>
			<td>
			Notes:<textarea name="response-text"><?php if(isset($getreportpoints[$group_id]['response-text'])) { echo $getreportpoints[$group_id]['response-text']; } ?></textarea>
			</td>
			</tr>-->
			<tr>
			<td colspan="5" align="center" style=" border-right:#a2ba88 1px solid;">
			<input style="margin:0px 0px 0px 300px;" id="button" type="submit" name="submit" value="next">
			</td>
			</tr>
			</table>
			</div>
			<?php } ?>
<table id="material" border="0" style="float: left;"><tr><td></td></tr></table>
			</div>
        
    </div>
                                
                                
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  

    <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>