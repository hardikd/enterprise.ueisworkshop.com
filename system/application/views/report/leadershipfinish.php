<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper3">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php 
		if($this->session->userdata('newform')=='formb')
		{
		require_once($view_path.'inc/rubricmenu.php');
		} else { require_once($view_path.'inc/leadershipmenu.php'); } ?>
        <div class="content">
        	<?php if(!empty($reportdata)) {?>
			<table class="tabcontent1">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $this->session->userdata('report_number_new');?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			
            
			</tr>
			<tr>
			
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
			
			</tr>
			
			</table><br />
			<?php } ?>
			<table>
			<tr>
			<td>
			<div class="htitle"><font style="text-transform: uppercase;"><?php
			echo $this->session->userdata('newform');?></font></div>
			</td>
			</tr>
			</table>
			<form action="report/leadershipfinishall" name="finishall" method="post">
			Finish The Report
			<input class="btnsmall" type="submit" value="Finish" name="submit" title="Finish">
			</form>
			<?php 
			if($this->session->userdata('login_type')=='observer')
			{
			?>
			<br />
			<br />
			<form action="report/leadershipfinishselfall" name="finishselfall" method="post">
			Finish And Save
			<input class="btnsmall" type="submit" value="Finish All" name="submit" title="Finish">
			</form>
			<?php } ?>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
