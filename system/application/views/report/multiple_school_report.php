<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM;?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>


<!--add calendar js and css-->
<script src="<?php echo SITEURLM;?>src/js/jscal2.js"></script>
<script src="<?php echo SITEURLM;?>src/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM;?>src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM;?>src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM;?>src/css/steel/steel.css" />
<!--add calendar js and css-->




<script type="text/javascript">
function checkfieldsmultipletest()
{

var from_year = document.getElementById('from');
var to_year = document.getElementById('to');
var schooltype = document.getElementById('school_type');
var classname = document.getElementById('grade');
var series_from = document.getElementById('seriesfrom').value; 
var series_to = document.getElementById('seriesto').value;
var category = document.getElementById('catname');
var testname = document.getElementById('testname');


if(from_year.selectedIndex  ==false)
{
 $('#from').css('border-color','red');
 return false;
}

if(to_year.selectedIndex  ==false)
{
 $('#from').css('border-color','#70B8BA');
 $('#to').css('border-color','red');
 return false;
}

if(schooltype.selectedIndex  ==false)
{
 $('#to').css('border-color','#70B8BA');
 $('#school_type').css('border-color','red');
 return false;
}

if(classname.selectedIndex  ==false)
{
 $('#school_type').css('border-color','#70B8BA');
 $('#grade').css('border-color','red');
 return false;
}
if(series_from  =="")
{

 $('#grade').css('border-color','#70B8BA');
 $('#seriesfrom').css('border-color','red');
 return false;
}
if(series_to  =="")
{
 $('#seriesfrom').css('border-color','#70B8BA');
 $('#seriesto').css('border-color','red');
 return false;
}
if(category.selectedIndex  ==false)
{
 $('#seriesto').css('border-color','#70B8BA');
 $('#catname').css('border-color','red');
 return false;
}
if(testname.selectedIndex  ==false)
{
 $('#catname').css('border-color','#70B8BA');
 $('#testname').css('border-color','red');
 return false;
}

document.multipletest.submit();

}
</script>

<script type="text/javascript">
function updatetest(catid)
{
	var series_from = $("#seriesfrom").val();
	var series_to = $("#seriesto").val();
	
	$.ajax({
url:'<?php echo base_url(); ?>report/gettestnames?cid='+catid+'&seriesfrom='+series_from+"&seriesto="+series_to,
		success:function(result)
		{
		
		$("#testname").html(result);
			
		}
		});
}
</script>
<script type="text/javascript">
function getcluster(assessmentid)
{

$.ajax({
url:'<?php echo base_url(); ?>report/getquestions?testid='+assessmentid,
		success:function(result)
		{
			$("#testcluster").html(result);
		}
		});
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
      
      
      		  <h2 style="color: #08A5CE">Multiple Assessment Reports by Standard
</h2>
      
        	<form  action='report/get_multiple_school_test' name="multipletest" id="multipletest" method="post" >
			<table cellpadding="5">			
			<tr>
			<td>
			 Year(s):
			  </td>
			  <td>
			  <select class="combobox" name="from" id="from" >
			  <option value="">-Please Select-</option>
              <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i.">".$i."</option>";    
			} ?> 
         	  </select>
              
            
              
			  </td>
             
              <td>
			  <select class="combobox" name="to" id="to" >
			  <option value="">-Please Select-</option>
			  <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i.">".$i."</option>";    
			} ?> 
			  </select>
			  </td>
			  
			</tr>	
            
            <tr>
			<td>
			 School Type:  
			  </td>
			  <td>
     <!--         <script type="text/javascript">
			  function updateschools(tag)
			  {
				  $.ajax({
					  
					  url:'<?php //echo base_url();?>report/getschoolbyschooltype?id='+tag,
					  success:function(result)
					  {
						$("#schools").html(result);
						// alert(result);
					  }
					  
					  });
			  }
			  </script>
              -->
           
              
			<!--  <select class="combobox" name="school_type" id="school_type" onchange="updateschools(this.value)" >-->
              <select class="combobox" name="school_type" id="school_type">
              <option value="">-Please Select-</option>
                 <?php
			 foreach($schools_type as $val)
			 {
				 $schooltypeid = $val['school_type_id'];
				 $name = $val['tab'];
				 ?>
                 <option value="<?php echo $schooltypeid;?>"><?php echo $name;?></option>
                 <?php
			 }
			  ?>
			  
			  
           
			  
			  </select>
			  </td>
			  
			</tr>
            
            <tr>
			<td>
			 Grades:
			  </td>
			  <td>
			  <select class="combobox" name="grade" id="grade" >
			  <option value="">-Please Select-</option>
                <?php
			 foreach($grades as $grade)
			 {
				 
				 $gradeid = $grade['dist_grade_id'];
				 $gradename = $grade['grade_name'];
				 ?>
                 <option value="<?php echo $gradeid;?>"><?php echo $gradename;?></option>
                 <?php
			 }
			  ?>
              			  
			  </select>
			  </td>
			  
			</tr>
            
           <!--  <tr>
			<td>
			 Test Name:
			  </td>
			  <td>
			   <select  name="testname[]" id="testname" multiple="multiple" >
			  <option value="" selected="selected">-Please Select-</option>
              
              <?php
			// foreach($testname as $test_name)
			// {
			
				// $testnameid = $test_name['id'];
				// $testcatname = $test_name['assignment_name'];
				 ?>
                 <option value="<?php //echo $testnameid;?>"><?php //echo $testcatname;?></option>
                 <?php
			// }
			  ?>
			  
			  
			  </select>
			  </td>
			  
			</tr>-->
            
             
            	
              <tr>
			<td>
			 Test Series:
			  </td>
			  <td>
               <input type="text" class="txtbox" id="seriesfrom" name="seriesfrom" />
                  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "seriesfrom",
        trigger    : "seriesfrom",
        onSelect   : function() { this.hide() },
        showTime   : 12,
        dateFormat : "%Y-%m-%d"
      });
    //]]></script>      
			  </td>
             
              <td><input class="txtbox" type="text" id="seriesto" name="seriesto" />
                  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "seriesto",
        trigger    : "seriesto",
        onSelect   : function() { this.hide() },
        showTime   : 12,
        dateFormat : "%Y-%m-%d"
      });
    //]]></script>       </td>
			  
			</tr>
            
            <!--   <tr>
			<td>
			 
             School(s):
			  </td>
			  <td>
               <input type="text" readonly="readonly" name="schools" id="schools" value="All" />-->
			  <!-- <select class="combobox" name="schools" id="schools" >
			  <option value="">-Please Select-</option>
			 		  <option value="all">All</option>-->
                      
            <?php
			 /*foreach($schools as $school)
			 {
			
				 $schoolid = $school['school_id'];
				 $schoolname = $school['school_name'];
				 ?>
                 <option value="<?php echo $schoolid;?>"><?php echo $schoolname;?></option>
                 <?php
			 }*/
			  ?>     
             
              			  
			<!--  </select>-->
 			<!--  </td>
			  
			</tr> -->
                	
			
            	<tr>
			<td>
			 Category:
			  </td>
			  <td>
              
				<select id="catname" name="catname" class="combobox" onchange="updatetest(this.value)">
                	<option>Select Category/Cluster</option>
                    
                    <?php foreach($getCategory as $key1=>$val1)
					{
					?>
                    
  <option value="<?Php echo $getCategory[$key1]['id'];?>"> <?Php echo $getCategory[$key1]['cat_name'];?></option>
                    
                    <?php
					}
					?>
                    
                </select>
 			  </td>
			  
			</tr>
            
            
            <tr>
			<td>
			 Test Name:
			  </td>
			  <td>
              <select  name="testname[]" id="testname" multiple="multiple" onchange="getcluster(this.value)" >
			
			  <option value="">Please Select</option>
              
			  </select>
			  </td>
			  
			</tr>	
              <tr>
			<td>
			 Test Cluster:
			  </td>
			  <td>
			  <select class="combobox" name="testcluster" id="testcluster" >
			  <option value=""></option>
             </select>
			  </td>
			  
			</tr>	
			<tr >			
			<td>
			</td>
	<td ><input title="Get Report" class="btnbig" type="button" name="getreport" value="Get Report" onclick="checkfieldsmultipletest();"></td>
			</tr>
			</table>
      </form>
			
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>