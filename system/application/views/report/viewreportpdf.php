<?php
ob_start();
require($view_path.'inc/html2pdf/html2pdf.class.php');
$html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 2));


$str='';

?>
<?php if(!empty($reportdata)) {
			$str.=' <style type="text/css">
	 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
font-size:9px;
}

</style>
        <page backtop="4mm" backbottom="17mm" backleft="1mm" backright="10mm">
        <page_header> 
              <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="'.SITEURLM.$view_path.'inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:5px;">
                    <b><img alt="pencil" width="12px"  src="'.SITEURLM.$view_path.'inc/logo/pencil1.png"/>&nbsp;Observation Report</b></div>
		</div>
        </page_header>
        
        <page_footer> 
                <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#'.$fontcolor.';">'.$dis.'</div>
		<br />
                <table style="margin-left:10px;">
                    <tr>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#'.$fontcolor.';">
                            &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                        </td>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#'.$fontcolor.';">
                           Page [[page_cu]] of [[page_nb]] 
                        </td>
                        <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#'.$fontcolor.';">
                            Printed on:'.date("F d,Y").'
                        </td>
                    </tr>
                </table>
        </page_footer> 
        
<table style="width:750px;border:2px #939393 solid;">
                            <tr>
                                <td align="left" style="width:270px;">
                                <b>Report Number:</b> '.$sno.'
                                </td>
                                <td align="left" style="width:240px;">
                                <b>Report Date:</b> '.date('F d, Y',strtotime($reportdata['report_date'])).'
                                </td>
                                <td align="left" style="width:220px;">
                                <b>Observer Name:</b> '.$reportdata['observer_name'].'
                                </td>
                            </tr>
                            <tr>
                            <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <b>Teacher Name:</b>
                                 '.$reportdata['teacher_name'].' 
                                </td>
                                <td  align="left">
                                    <b>School Name:</b>
                                 '.$reportdata['school_name'].' 
                                </td>
                                <td align="left">
                                    <b>Subject:</b>
                                 '.$reportdata['subject_name'].' 
                                </td>
                            </tr>
                            <tr>
                            <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <b>Grade:</b>
                                 '.$reportdata['grade_name'].' 
                                </td>
                                <td align="left">
                                    <b>Type of Observation:</b>
                                 '.$reportdata['period_lesson'].' 
                                </td>
                                <td  align="left">
                                    <b>Students Present:</b>
                                 '.$reportdata['students'].' 
                                </td>
                            </tr>
                            <tr>
                            <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td align="left"><b>Paraprofessionals:</b> '.$reportdata['paraprofessionals'].'</td>
                                <td align="left" colspan="2"><b>Lesson Correlation:</b> '.$reportdata['lesson_correlation'].'</td>
                            </tr>
			</table><br /><br />';
			 } 
			
			if($reportdata['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { 
			$str.='<table style="width:700px;border:1px #939393 solid;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['standard'].'</td></tr>
			</table>
			<br />
			<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['diff_instruction'].'
			</td></tr>
			</table><br />';
			
			 } } 
			 
			 
			 
			if($reportdata['lesson_correlation_id']==3)
			{
			
			if ( file_exists(WORKSHOP_FILES.'observationplans/'.$reportdata['report_id'].'.pdf') )
    {
			
			 
			/* $str.='<table>
			 <tr>
			 <td>
			 <object  data="'.WORKSHOP_DISPLAY_FILES.'observationplans/'.$reportdata['report_id'].'.pdf" type="application/pdf"  width="650" height="220"></object>
			 </td>
			 </tr>
			 </table>';*/
			 
			 
	         }		 
			 } 
			 
			 
			 if($points!=false) {	
			
			
			$str.='<table style="width:700px;border:1px #939393 solid;padding-bottom:10px;padding-top:10px;">';
			$point_again=0;
			$group_again=0;
			foreach($points as $val)
			{
			
			$gcheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			$gcounter=0;
			
			}
			else
			{
				$gcounter=1;
			
			}
			if($gcounter==0)
			{
			$str.='<tr>
			<td style="color:#048DC4; font-size: 16px; margin-bottom: 10px;">'.$val['group_name'].'
			</td>
			</tr>
			<tr>
			<td style="font-size: 10px;">
			'.$val['description'].'
			</td>
			</tr>';
			
			
			 }
			
			
			if($val['group_type_id']!=2)
			{
			
			$str.='<tr><td style="font-size: 10px;padding-left:5px;">';			
			
			
			
 if(isset($getreportpoints[$val['point_id']]['response']) && $getreportpoints[$val['point_id']]['response']==1 ) { 
$str.='<img style="float:left" src="images/checked.jpg">';  }else{  $str.='<img style="float:left" src="images/unchecked.jpg">';  } $str.=''.wordwrap($val['question'], 150, "<br />\n").'</td></tr>';
			
			 
			}
			else if($val['group_type_id']==2) {
			$check=0;
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			
			
			$str.='<tr>
			<td style="color: #333333;font-size: 10px;font-weight: bold;margin-top: 5px;">
			'.wordwrap($val['question'], 150, "<br />\n").'
			</td>
			</tr>
			<tr>
			<td style="padding-left:34px;font-size: 10px;">';
			
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  
			
			 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$point_id]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$point_id]['response']) && $getreportpoints[$point_id]['response']==$val['sub_group_id'] && $val['ques_type']!='checkbox' ) { $str.='<img style="float:left" src="images/checked.jpg">';  } else  
			if(isset($getreportpoints[$point_id][$val['sub_group_id']])) {
			 
			$str.='<img style="float:left" src="images/checked.jpg">';
			 } else if ($check!=1){ $str.='<img style="float:left" src="images/unchecked.jpg">';  }
			 if($check==1) { $str.='<img style="float:left" src="images/checked.jpg">';  } 
			  $str.='<div style="font-size: 10px;" >'.wordwrap($val['sub_group_name'], 150, "<br />\n").'</div>
			
			</td>
			</tr>';
			
			 }
			else {
			
			
			$str.='<tr>
			<td style="padding-left:34px;font-size: 10px;">';
			
			
			 
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  
			
			 if(isset($getreportpoints[$point_id]['response']) && $getreportpoints[$point_id]['response']==$val['sub_group_id']  && $val['ques_type']!='checkbox' ) {  $str.='<img style="float:left" src="images/checked.jpg">';  } else 
			if(isset($getreportpoints[$point_id][$val['sub_group_id']])) {
			 
			$str.='<img style="float:left" src="images/checked.jpg">';
			 }  else { $str.='<img style="float:left" src="images/unchecked.jpg">';  } 
			
			 $str.='<div style="font-size: 10px;"> '.wordwrap($val['sub_group_name'], 150, "<br />\n").'</div>
			</td>
			</tr>';
			
			}
			
			}
			$point_again=$val['point_id'];
			$group_again=$val['group_id'];
			}
			} 
			else
			{
			
			
			$str.='<tr>
			<td>
			No Observation Points Found
			</td>
			</tr>';
			 } 
			$str.='</table></page>'; 
			 
			 $content = ob_get_clean();
			// $html2pdf->setModeDebug();
			
			$html2pdf->WriteHTML($str,isset($_GET['vuehtml']));
			if($reportdata['lesson_correlation_id']==3)
			{
			
			if ( file_exists(WORKSHOP_FILES.'observationplans/'.$reportdata['report_id'].'.pdf') )
			{
				 $login_type=$this->session->userdata('login_type');
				 if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}	
			else if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
			
				$html2pdf->Output(WORKSHOP_FILES.'observationplans/view_'.$login_type.'_'.$login_id.'.pdf', 'F');
				header('Location: '.REDIRECTURL.'pdf/viewpdf/'.$login_type.'/'.$login_id.'/'.$reportdata['report_id']);
			}
			else
			{
			$html2pdf->Output();
			
			}
	
			}
			else
			{
			$html2pdf->Output();
			
			}
			
			
			
			?>