<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />



</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
 <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/summative">Summative Report</a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>report/summativereport">Generate Report</a>
                       
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
   
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Generate Report</h4>
                          
                         </div>
                         <div class="widget-body">
                            <div class="form-horizontal">
                                <div id="pills" class="custom-wizard-pills-red5">
                                 <ul>
                                 <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                 <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                 <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                 <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                 <li><a href="#pills-tab5" data-toggle="tab">Step 5</a></li>   
                                    
                                     
                                     
                                 </ul>
                                          <?php
          
		$login_type=$this->session->userdata('login_type');
            if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');
				$school_id_se=$this->session->userdata('school_id');

			}
		?>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <div class="form-horizontal">
                                         	<input type="hidden" name="logintype" id="logintype" value="<?php echo $login_type;?>">
											<input type="hidden" name="login_id" id="login_id" value="<?php echo $login_id;?>">
											<?php
                                            if($this->session->userdata('login_type')=='user')
                                        {
                                        ?>
                                         <div class="control-group">
                                             <label class="control-label">Select School</label>
                                             <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="school" id="school" onchange="school_change(this.value)" >
										<?php if($school!=false) { ?>
                                        <?php foreach($school as $val)
                                        {
                                        ?>
                                        <option value="<?php echo $val['school_id'];?>" 
                                        <?php if(isset($school_id) && $school_id==$val['school_id']) {?> selected <?php } ?>
                                        ><?php echo $val['school_name'];?></option>
                                        <?php } } else { ?>
                                        <option value="0">No Schools Found</option>
                                        <?php } ?>
                                        </select>
                                             </div>
                                         </div>
                                         <?php } else { ?>
                                         <div class="control-group">
                                    <div class="controls">
                                        	<input class="m-ctrl-medium" type="hidden" name="school" id="school" value="<?php echo $school_id_se;?>">
                                    </div>
                                </div>
                                     	<?php } ?>    
                                         
                                         <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input type="text" class="m-ctrl-medium" size="16" name="dates" id="dp1" value="">
                                    </div>
                                </div>
                                
                                 <div class="control-group">
                                             <label class="control-label">Select Teacher</label>
                                             <div class="controls">

<select data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id" class="span12 chzn-select">
			    	<option value=""></option>
			    	<?php if(!empty($teachers)) { 
							foreach($teachers as $val)
							{
							?>
									<option value="<?php echo $val['teacher_id'];?>" 
							<?php if(isset($teacher_id) && $teacher_id==$val['teacher_id']) {?> selected <?php } ?>
							><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
									<?php } } ?>
								  </select>                                            
                                   </div>
                               
                                   <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                   <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                   <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
									</div>
                                         
                                      </div>   
                                       
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <div class="form-horizontal">
                                      
                                           <div class="control-group">
                                      <label class="control-label">Select Window #1</label>
                                             <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="window1" id="window1"  onchange="reportchange(this.value)" >
                           <option value=""></option>
                          <option value="winpdf">PDF Report</option>
                          <option value="sectionalreport">Sectional Report</option>			  
                          </select> 
                              
                                                </div>
                                                     </div>
                                         
                                            <div class="control-group">
                                      <label class="control-label">Select Report Type</label>
                                             <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="reporttype" id="reporttype" onchange="changeform()"   >
   								  <option value=""></option>	
                                  <option value="forma">Checklist</option>
                                  <option value="formb">Scaled Rubric</option>
                                  <option value="formp">Proficiency Rubric</option>
                                  <option value="formc">Likert</option>			  
                                  </select>
                                  </div>
                                         </div>
                                              <div class="reportdatec" style="display:none;">
                                      
                                             <div class="controls">
									<input type="button" class="btn btn-large btn-red" id="getpdfreport" value="Get Report">
                                       </div>
								 </div>
                                       <br />
                                                <div class="reportdatec" style="display:none;">
                                      <label class="control-label">Select Report </label>
                                             <div class="controls">
									  <select class="combobox" name="report_id" id="report_id">
			    <option value="">-select-</option>
				</select>

                                  </div>
                                         </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                  <!--      <div class="reportdatec" style="display:none;">
                                       <label class="control-label">Select Report </label>
                                    
                                             <div class="controls">
                                      <div style="width:300px;" id="report_id">
                                     
                                       </div>                         
                                         </div>
                                         </div>-->
                                         
                      
                                 
                           <div class="control-group">
<input title="Get Reports" id="window1button" class="btn btn-large btn-red" type="button" name="button" value="Get Window1" />
                                 <div id="loading" style="display:none;" class="controls">
						 		 <img src="<?php echo SITEURLM?>/images/loading.gif" >
  						            </div>
								 </div>                     
                          
                              <div class="control-group reportdatec" style="display:none;">
                                      <label class="control-label">Window1 </label>
                                             <div class="controls">
		  				</div>
                        <div id="resizable" style=" border: 1px solid black;height:320px;width:630px;overflow: auto;">
						<div id="resizabledata">
        </div>
		</div>
        </div>
                                         </div>
                                 
                                 
                                         
                                   <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                   <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                   <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                         
                                          </div>
                                         
                                          <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
							<form class="form-horizontal" id="uploadForm" enctype="multipart/form-data" method="POST" action="<?php echo base_url();?>summative/upload">
                                         <div class="control-group">
                                      <label class="control-label">Select Window #2</label>
                                             <div class="controls">
                  <select class="span12 chzn-select" name="window2" style="width:150px;" onchange="change(this.value)" data-placeholder="--Please Select--" tabindex="1" id="window2selector">
                                        <option value=""></option>
                                        <option value="url">URL</option>
                                        <option value="file">File Upload</option>
                                    </select>
                                             </div>
                                        
                                         
                            <div id="url" class="window2" style="display:none">
                            <div class="control-group">
                                <label class="control-label">Enter URL</label>
                                <div class="controls">
                                    <input type="text" name="url" id="url" class="span6 " value="http://">

                                </div>
                            </div>
                            
                            </div>
                            
                            
                             <div id="file" class="window2" style="display:none">
                              
                              <div class="control-group">
                                             <label class="control-label">Upload File</label>
                                             <div class="controls">
                                             
                                                <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" name="upload" id="upload" class="default">
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                     <div class="control-group">
										 <div class="controls">
						<input title="Get Reports" id="window2button" class="btn btn-large btn-red" type="submit"  value="Get Window2" />
                      
                                  	</div>
                                </div>
                                               
                            		 </div>
                        	<div class="control-group reportdatec" style="display:none;">
                            <label class="control-label">Window2 </label>
                            <div class="controls">
                            <div id="resizable1" style=" border: 1px solid black;height:320px;width:630px;overflow: auto;">
                            <div id="resizabledata1"></div>
                            </div>
                            </div>
                            </div> 
                            
                           </div>  
                                   </div>
                                    <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                   <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                   <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                   </div>  
                            </form>
                            
							</div>      
                                        
                                           <!-- BEGIN STEP 4-->
                                     <div class="tab-pane" id="pills-tab4">
                                         <h3 style="color:#000000;">STEP 4</h3>
                          
                          <div class="control-group">
                                             <label class="control-label">Enter Comments</label>
                                             </div>
                                        
                                        <div class="space10"></div>
                                      
                                     <div class="control-group">
                                        <textarea class="span12 ckeditor" name="comments" rows="12" id="comments"></textarea>
 								 </div>
                                        <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      				<div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      				<ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                         
                                         
                                        </div>
                                     
                                     
                                     
                                                   <!-- BEGIN STEP 5-->
                                     <div class="tab-pane" id="pills-tab5">
                                         <h3 style="color:#000000;">STEP 5</h3>
                                         
                                 <div class="control-group">
                                             <label class="control-label">Select Status</label>
                                             <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="status" id="status"  >
	  			<option value=""></option>
			    <?php if(!empty($statuses)) { 
					foreach($statuses as $val)
					{
					?>
			    <option value="<?php echo $val['status_name'];?>" 
				><?php echo $val['status_name'];?>  </option>
			    <?php } } ?>	  
	  		</select>
                     </div>
            	</div>
                            
                                         
                                          <div class="control-group">
                                             <label class="control-label">Select Score </label>
                                             <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="score" id="score" >
                                        <option value=""></option>
                                           <?php if(!empty($scores)) { 
                                            foreach($scores as $val)
                                            {
                                            ?>
                                           <option value="<?php echo $val['score_name'];?>" 
                                            ><?php echo $val['score_name'];?>  </option>
                                        <?php } } ?>	  
                                      </select> 
                                              </div>
	                                  </div>
                                         
                        <div class="space20"></div> 
                                                                               
					<center><button type="button" name="createpdf" id="createpdf" value="Create Pdf" class="btn btn-large btn-red"><i class="icon-file icon-white"></i> Create PDF</button>                										
                  <img id="loading1" src="<?php echo SITEURLM?>/images/loading.gif" style="display:none;"></td>
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                                
                        <center><button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save File</button> <button class="btn btn-large btn-red"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                          <div class="space20"></div> <div class="space20"></div><div class="space20"></div>                      
                          <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                </div>  
                                     </div>
                                       
                                         
                                         
                                         
                                           
                                         </div>
                                    
                                     
                                    
                                     
                                 </div>
                                 </div>
                             </div>
                       
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tableswindow2/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  

    <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>

<script>
 $(function() {
        $('#window2selector').change(function(){
            $('.window2').hide();
            $('#' + $(this).val()).show();
        });
    });
	
	</script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });


//$("#window2button").click(function(){
////    alert('test');
//var formData = new FormData($('#uploadform')[0]);
//
//$.ajax({
//url:'<?php echo base_url();?>summative/upload',
//type: 'POST',
//data: formData,
//async: false,
//success: function (data) {
//console.log(data);
////location.reload();
//},
//cache: false,
//contentType: false,
//processData: false
//});
//return false;
//});

   </script>  

   <!--start ols script -->



<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
   <script type="text/javascript">
       
function change(th)
{
  
 if(th=='url')
 {
    $('#urldis').show();
	$('#uploaddis').hide();
 
 }
 else
 {
	 $('#urldis').hide();
	$('#uploaddis').show();
	
 
 }
}
function changeform()
{
  
 $('#reportdate').val('');
  var str='<select style="width:300px;" class="span12 chzn-select" name="report_id" id="report_id" ><option value="">-Select-</option>';
	   $('#report_id').replaceWith(str); 
}
function reportchange(th)
{
  
 if(th=='winpdf')
 {
   $('.reportdatec').show();
	
 
 }
 else
 {
	  $('.reportdatec').hide();
	
 
 }
}



function school_change(id)
{
	var g_url='teacher/getTeachersBySchool/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select style="width:300px;" class="span12 chzn-select" name="teacher_id" id="teacher_id" ><option value="">-Select-</option>';
	if(result.teacher!=false)
	{
	$.each(result.teacher, function(index, value) {
	str+='<option value="'+value['teacher_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Teachers Found</option></select>';
	  
	}
     $('#teacher_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
function process(data)
{
    
        //alert(data);
  if(data.status==0)
  {
    alert(data.mesage);
  
  }
  else
  {
     
   var window2=$('#window2selector').val();
     var $msgContainer1 = $("div#resizabledata1");
    //console.log(window2);return false;
   if(window2!='url')
   {

    var logintype=$('#logintype').val();
	var siteurlm='<?php echo SITEURLM ;?>';
	var WORKSHOP_DISPLAY_FILES='<?php echo WORKSHOP_DISPLAY_FILES ;?>'; 
        
var login_id=$('#login_id').val();

var ht1='<object  data="'+WORKSHOP_DISPLAY_FILES+'uploads/'+logintype+login_id+'.pdf" type="application/pdf"  width="630" height="320"></object>';
	 
	 $msgContainer1.html(ht1);
	$('.reportdatec').show();
   }
   else
   {
      
     var ht2='<iframe src="'+data.mesage+'" height="320" width="630"></iframe>';
    $msgContainer1.html(ht2);
	
  

   
   }
   
//   $( "#resizable1" ).resizable({
//			animation:true
//		});
  }


}
$(document).ready(function() {
var siteurlm='<?php echo SITEURLM ;?>'; 
var WORKSHOP_DISPLAY_FILES='<?php echo WORKSHOP_DISPLAY_FILES ;?>'; 
 /*$( "#reportdate" ).datepicker({changeYear:true,changeMonth:true,dateFormat:'yy-mm-dd'});*/

 
 var options = { 
	    	    success:process,
				dataType:  'json' 
		};
 
 $('#uploadForm').submit(function() { 
//	        $(this).ajaxSubmit(options); 
                var formData = new FormData($(this)[0]);
                
                $.ajax({
                    url:'<?php echo base_url();?>summative/upload',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                console.log(data);
                //location.reload();
                process(data);
                },
                cache: false,
                contentType: false,
                processData: false
                });
	        return false; 
	    });		

$('#getpdfreport').click(function () {

if(document.getElementById('teacher_id').value=='')
{
 alert('Please Select Teacher');
 return false;

}

 var teacher_id=$('#teacher_id').val();
var reporttype=$('#reporttype').val();

var school_id=$('#school').val();
 $.get("summative/pdfreport/"+school_id+"/"+teacher_id+"/"+reporttype+"?num=" + Math.random(), function(datareport){
   var str='<select style="width:300px;" class="span12 chzn-select" name="report_id" id="report_id" ><option value="">-Select-</option>';
	if(datareport!=false)
	{
	var j=0;
	$.each(datareport, function(index, value) {
	j++;
	str+='<option value="'+value['report_id']+'-'+j+'">('+j+')'+value['report_date']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Reports Found</option></select>';
	  
	}
     $('#report_id').replaceWith(str); 
 },'json');

 });
$('#window1button').click(function () {
if(document.getElementById('teacher_id').value=='')
{
 alert('Please Select Teacher');
 return false;

}
if(document.getElementById('window1').value=='')
{
 alert('Please Select Window1');
 return false;

}
else{
$('#loading').show();
var teacher_id=$('#teacher_id').val();
var reporttype=$('#reporttype').val();

var logintype=$('#logintype').val();
var login_id=$('#login_id').val();
var window1=$('#window1').val();
var school_id=$('#school').val();
var $msgContainer = $("div#resizabledata");
if(window1=='winpdf')
{
 if($('#report_id').val()=='')
 {
    alert('Please Select A Report');
	$('#loading').hide();
	return false;
 }
 else
 {
 var report_id=$('#report_id').val();
  $.get("summative/getreport/"+report_id+"/"+reporttype+"?num=" + Math.random(), function(msg){
pdata={};
pdata.login_id=login_id;
pdata.logintype=logintype;
pdata.msg=msg;

 $.post('summative/createpdf',{'pdata':pdata},function(data) {
  if(data.status==1)
  {
    
	var ht='<object  data="'+WORKSHOP_DISPLAY_FILES+'pdf/'+logintype+login_id+'.pdf" type="application/pdf"  width="630" height="320"></object>';
	 
	 $msgContainer.html(ht);
	
	
	$('#loading').hide();
  }
  else
  {
    alert('Failed To Create Pdf Please Try Again.');
	$('#loading').hide();
  
  }

},'json');

$( "#resizable" ).resizable({
			animation:true
		});
		$( "#resizable1" ).resizable({
			animation:true
		});
});
 
 }
}
else
{
$.get("summative/getwindow1/"+school_id+"/"+teacher_id+"/"+reporttype+"?num=" + Math.random(), function(msg){
pdata={};
pdata.login_id=login_id;
pdata.logintype=logintype;
pdata.msg=msg;

 $.post('summative/createpdf',{'pdata':pdata},function(data) {
  if(data.status==1)
  {
    
	if(window1=='sectionalreport')
	{
	$msgContainer.html(msg);
	}
	else
	{
	 var ht='<object  data="'+WORKSHOP_DISPLAY_FILES+'pdf/'+logintype+login_id+'.pdf" type="application/pdf"  width="630" height="320"></object>';
	 
	 $msgContainer.html(ht);
	}
	$('#loading').hide();
  }
  else
  {
    alert('Failed To Create Pdf Please Try Again.');
	$('#loading').hide();
  
  }

},'json');

$( "#resizable" ).resizable({
			animation:true
		});
		$( "#resizable1" ).resizable({
			animation:true
		});
});
}
}

});
$('#createpdf').click(function () {
 
 
 
 
 
 
 if($('#resizabledata').html()=='')
 {
    alert('please select window1');
	return false;
 }
 if($('#resizabledata1').html()=='')
 {
    alert('please select window2');
	return false;
 }
 
 if($('#window2').val()=='url')
 {
    alert('From The URL Generate PDF is not possible');
	return false;
 }
// if($('#comments').val()=='')
// {
//    alert('please Enter Comments');
//	return false;
// }
 if($('#status').val()=='')
 {
    alert('please Select Status');
	return false;
 }
 if($('#score').val()=='')
 {
    alert('please Select Score');
	return false;
 }
 
 if($('#dates').val()=='')
 {
    alert('please select dates');
	return false;
 }
 var  teacher_id=$('#teacher_id').val();
 var status=$('#status').val();
 var score=$('#score').val();
 var dates=$('#dates').val();
 var comments=$('#comments').val();
 $('#loading1').show();
 pdata={};
pdata.teacher_id=teacher_id;
pdata.status=status;
pdata.score=score;
pdata.dates=dates;
pdata.comments=comments;
$.post('summative/general',{'pdata':pdata},function(data) { 
$.post('pdf/index',{'pdata':pdata},function(data) { 
 alert(data.status);
 $('#loading1').hide();
 location='report/summative';
 },'json');
});
 
 
});
});
</script>

<!--end old script -->

 
</body>
<!-- END BODY -->
</html>