<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>



<style type="text/css">

.wrapper{
	min-height:1000px;
}
#button {
    background-color: #DE577B;
    border: 0 none;
    color: #FFFFFF;
    cursor: default;
	border-radius: 15px;
    display: inline-block;
    padding: 7px 14px;
	float: right;
    margin-left: 5px;
	text-decoration: none;
	outline: 0 none;
	text-shadow: none !important;
}
.ui-widget-header { border-bottom: 1px solid #eee; 
/*					background:none repeat-x scroll 50% 50% #9d9d9d; */
					color: #FFFFFF; 
					font-weight: bold;
                   	background: #DE577B;
                    /*font-family: "MyriadPro-Light";*/
                    font-weight: normal;
                    font-size: 14px;
					font-weight:bold;
}
			
</style>



</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
  <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/observation">Lesson Observation </a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>report/index">Generate Report</a>
                       
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Generate Report</h4>
                          
                         </div>
                         <div class="widget-body">
							<form class="form-horizontal" name="reportform" id="reportform" action="report/create" method="post">
                                <div id="pills" class="custom-wizard-pills-red5">
                                 <ul>
                                 <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                 <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                 <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                 <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                 <li><a href="#pills-tab5" data-toggle="tab">Step 5</a></li>   
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content tabreport">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                          <form class="form-horizontal" action="#">
                                         
                                         <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                    <input class="m-ctrl-medium" type="text" name="report_date" id="report_date" onchange="doit()">

                                    </div>
                                </div>
                                
                                 <div class="control-group">
                                             <label class="control-label">Select Observation Tool</label>
                                             <div class="controls">
       
       				<select name="form" id="form" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width:300px;">
								<option value=""></option>
                                <option value="forma">Checklist</option>
								<option value="formb">Scaled Rubric</option>
								<option value="formp">Proficiency Rubric</option>
								<option value="formc">Likert</option>		                 
                                </select>    
                                    </div>
                                         </div>
                             <div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                         <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <form class="form-horizontal" action="#">
                              
                                <div class="control-group">
                                             <label class="control-label">School Name</label>
                                             <div class="controls">
                                    
                             			<?php if($this->session->userdata('login_type')=='user')
										{
										?>
										<select class="span12 chzn-select" name="school_id" id="school_id"  style="width:300px;" onchange="school_change(this.value)" >
									<?php if($school!=false) { 
									foreach($school as $val)
									{
									?>
									<option value="<?php echo $val['school_id'];?>"  ><?php echo $val['school_name'];?></option>
									<?php } } else { ?>
									<option value="">No Schools Found</option>
									<?php } ?>
									</select>
										<?php
										}
										else
										{
										?>
										<?php echo $this->session->userdata("school_name");?>
								<input type="hidden" id="school_id" name="school_id" value="<?php echo $this->session->userdata("school_id");?>">
										<?php } ?>
                                             </div>
                                             
                                         </div>
                              
                                      
                                         
                                 
                                <div class="control-group">
                                             <label class="control-label">Select Teacher</label>
                                             <div class="controls">
 
<select data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id" onchange="doit()">
                                        <option value=""></option>
                                        <?php if(!empty($teachers)) { 
                                        foreach($teachers as $val)
                                        {
                                        ?>
                                     <option value="<?php echo $val['teacher_id'];?>" ><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
                                        <?php } } ?>
                                        </select>
                                             </div>
                                         </div>
                                         
                                           <div class="control-group">
                                      <label class="control-label">Select Lesson Correlation</label>
                                             <div class="controls">

<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="lesson_correlation" id="lesson_correlation" onchange="change_corelation(this.value)">
                                        <option value=""></option>
                                        <option value="1">Walkthrough</option>
                                        <option value="2">Lesson Plan</option>
                                        <option value="3">Observation</option>
                                        </select>
                                         </div>
                                         </div>
                                         
                                              <div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                         <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                         </div>
                                         
                                         
                                          <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                         <form class="form-horizontal" action="#">
                                         
                                 <div class="control-group">
                                             <label class="control-label">Select Subject</label>
                                             <div class="controls">

<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="subject_id">
		<option value=""></option>
		<?php if(!empty($subjects)) { 
		foreach($subjects as $val)
		{
		?>
		<option value="<?php echo $val['subject_id'];?>" ><?php echo $val['subject_name'];?>  </option>
		<?php } } ?>
		</select>
                                                                     
                                             </div>
                                         </div>         
                                
                                
                                         
                                 
                                   <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="grade_id" id="grade_id" onchange="selectmaterial()">
                                <option value=""></option>
                                <?php if(!empty($grades)) { 
                                foreach($grades as $val)
                                {
                                ?>
                                <option value="<?php echo $val['grade_id'];?>" ><?php echo $val['grade_name'];?>  </option>
                                <?php } } ?>
                                </select>    
                                    </div>
                                    </div>
                                            <div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                         <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul> 
                                       
                                     </div>
                                     
                                   
                                        
                                           <!-- BEGIN STEP 4-->
                                     <div class="tab-pane" id="pills-tab4">
                                         <h3 style="color:#000000;">STEP 4</h3>
                                         
                                         <div class="control-group">
                                <label class="control-label">Enter Number of Students Present</label>
                                <div class="controls">
                                    
                                    <input type="text"  name="students" id="students" class="span6 ">
                                    
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">Enter Number of Paraprofessionals Present</label>
                                <div class="controls">
                                    <input type="text" name="paraprofessionals" id="paraprofessionals" class="span6 ">

                                </div>
                            </div>
                                              <div class="space20"></div><div class="space20"></div><div class="space20"></div>
                                         <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                         
                                        </div>
                                     
                                     
                                     
                                                   <!-- BEGIN STEP 5-->
                                     <div class="tab-pane" id="pills-tab5">
                                         <h3 style="color:#000000;">STEP 5</h3>
                                         
                                 <div class="control-group">
                                             <label class="control-label">Select Observer</label>
                                             <div class="controls">

		<select class="" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="observer_id" id="observer_id">
                                        <option value=""></option>
                                        <?php if(!empty($observers)) { 
                                        foreach($observers as $val)
                                        {
                                        ?>
                                        <option value="<?php echo $val['observer_id'];?>" ><?php echo $val['observer_name'];?>  </option>
                                        <?php } } ?>
                                        </select>
                                              </div>
                                           </div>
                            
                                          <div class="control-group">
                                             <label class="control-label">Select Evaluation Type</label>
                                             <div class="controls">
					<select class="span12 chzn-select" name="period_lesson" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option value="Formal">Formal</option>
                                        <option value="Informal">Informal</option>
                                       
                                    </select>
                                             </div>
                                         </div>
                                         <div class="control-group" style="display:none;" >
                                             <label class="control-label">Enter Comments</label>
<!--                                             </div>
                                        
                                        <div class="space10"></div>
                                      
                                     <div class="control-group">-->
                                
                                        <textarea class="span12 ckeditor" name="editor1" rows="12"></textarea>
                                 
                                </div>
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
<!--                        <center><button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save File</button> <button class="btn btn-large btn-red"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>-->
                        
                        
                           <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                       
                                          <li class="">

                                     <input  id="button" class="next red" type="submit" name="submit" value="Next">
                                     </li>
                                     </ul> 
                          
                                </div> 
                                 
                                  <table id="material" border="0" style="float: left;">
			<tr>
			<td>
			</td>
			</tr>
			</table>
                                     </div>
                        		</div>
                                   
                                 </div>
                             </div>
                             </form>
                             
                             
                             
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
    <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  <!--start old script -->
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>

<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>

<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/report.js" type="text/javascript"></script>
<script type="text/javascript">
var WORKSHOP_DISPLAY_FILES = '<?php echo WORKSHOP_DISPLAY_FILES; ?>';
var login_type = '<?php echo $this->session->userdata('login_type'); ?>';
var login_id = '<?php if($this->session->userdata('login_type')=='user') { echo $this->session->userdata('dist_user_id'); } else if($this->session->userdata('login_type')=='observer') { echo $this->session->userdata('observer_id'); } else {echo $this->session->userdata('teacher_id'); }?>';
function school_change(id)
{
	doit();
	var g_url='teacher/getTeachersBySchool/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="teacher_id" id="teacher_id" onchange="doit()" ><option value="">-Select-</option>';
	if(result.teacher!=false)
	{
	$.each(result.teacher, function(index, value) {
	str+='<option value="'+value['teacher_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Teachers Found</option></select>';
	  
	}
     $('#teacher_id').replaceWith(str); 
	 
	 
	 
	 
	 });
	 
	 var g_url='observer/getobserverByschoolId/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="observer_id" id="observer_id" ><option value="">-Select-</option>';
	if(result.observer!=false)
	{
	$.each(result.observer, function(index, value) {
	str+='<option value="'+value['observer_id']+'">'+value['observer_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Observers Found</option></select>';
	  
	}
     $('#observer_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
</script>
<!--end old script -->

   
   
   
 
</body>
<!-- END BODY -->
</html>