<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
if($('#form').val()=='formp'){ 
$('.formp').hide();
 
  }
  else
  {
    $('.formp').show();
  }
});
function check()
{

if(document.getElementById('school_id').value=='')
{
 alert('Please Select School');
 return false;

}
else
{
	return true;

}
}
function changeform(val)
{
  if(val=='formp')
  {
     $('.formp').hide();
  
  
  }
  else
  {
    $('.formp').show();
  }


}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<form action='report/getschoolreport' name="report" method="post" onsubmit="return check()">
			<table cellpadding="5">
			<tr>
			<td valign="middle" class="f14">
			School:
			</td>
			<td colspan="2" valign="middle">
			  <select class="combobox" name="school_id" id="school_id">
			    <option value="">-select-</option>
			    <?php if(!empty($schools)) { 
		foreach($schools as $val)
		{
		?>
			    <option value="<?php echo $val['school_id'];?>" 
		<?php if(isset($school_id) && $school_id==$val['school_id']) {?> selected <?php } ?>
		><?php echo $val['school_name'];?>  </option>
			    <?php } } ?>
		      </select>
			  </td>
			  </tr>
			  <tr>
			  <td class="f14">
			 Observation Tool:
			  </td>
			  <td>
			    <select class="combobox" name="form" id="form" onchange="changeform(this.value)">
			  <option value="forma" <?php if($this->session->userdata('reportform')=='forma') {?> selected <?php } ?> >Checklist</option>
			  <option value="formb" <?php if($this->session->userdata('reportform')=='formb') {?> selected <?php } ?> >Scaled Rubric</option>
			  <option value="formp" <?php if($this->session->userdata('reportform')=='formp') {?> selected <?php } ?> >Proficiency Rubric</option>
			  <option value="formc" <?php if($this->session->userdata('reportform')=='formc') {?> selected <?php } ?> >Likert</option>			  
			  </select>
			  </td>
			  
			</tr>
			<tr>
			  <td class="f14">
				Evaluation Type:
			  </td>
			  <td>
			  <select class="combobox" name="formtype" id="formtype" >
				<option value="all"  >-All-</option>			 
			 <option value="Formal" <?php if($this->session->userdata('formtype')=='Formal') {?> selected <?php } ?> >Formal</option>
			  <option value="Informal" <?php if($this->session->userdata('formtype')=='Informal') {?> selected <?php } ?> >Informal</option>
			  
			  </select>
			  </td>	<td><input title="Get Reports" class="btnbig" type="submit" name="submit" value="Get Reports" /></td>		
			  </tr>
			<tr class="formpa">
			<td valign="middle" class="f14">Sectional Data Report: </td>
			<td valign="middle" ><input title="Get Sectional Report" class="btnsmall_long2" type="submit" name="submit" value="Get Sectional Report" /></td>
			</tr>
			<tr class="formp">
			<td valign="middle" class="f14">Qualitative Data Report: </td>
			<td valign="middle">From: <select class="comboboxsml" name="from" >
			
	<?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i.">".$i."</option>n";    
			} ?> 


			</select>
			</td>
			<td valign="middle">To: <select class="comboboxsml" name="to" >
			
	<?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i.">".$i."</option>n";    
			} ?> 


			</select>
			</td>
			<td valign="middle"><input title="Get Report" class="btnbig"  type="submit" name="submit" value="Get Report"></td>
			</tr>
			</table>

      </form>
			<?php if(isset($reports) && !empty($reports)) { ?>
			<table  class="tabcontent" cellspacing="0">
			<tr class="tchead">
			<th>
			Report Id
			</th>
			<th>
			Date
			</th>
			<th>
			Teacher
			</th>
			<th>
			Subject
			</th>
			<th>
			Grade
			</th>
			<th>
			Observer
			</th>
			</tr>
			<?php foreach($reports as $reportval) { ?>
			<tr>
			<td width="60" valign="top"  class="tdlink">
			<?php if($this->session->userdata('reportform')!='formb') { 
			if($this->session->userdata('reportform')=='forma' || $this->session->userdata('reportform')=='formc')
			{
			?>
			<a href="comments/viewreportpdf/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			
			<?php
			}
			if($this->session->userdata('reportform')=='formp')
			{
			?>
			<a href="comments/viewproficiencypdf/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			<a href="report/viewproficiency/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><div style="margin:5px 0px 0px 5px;float:left;"><?php echo $sno;?></div></a>
			<?php } else {?>
			
			<a href="report/viewreport/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><div style="margin:5px 0px 0px 5px;float:left;"><?php echo $sno;?></div></a>
			<?php
			} } else { ?>
			<a href="comments/viewreportformpdf/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			<a href="report/viewreportform/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><?php echo $sno;?></a>
			<?php } ?>
			</td>
			<td valign="middle">
			<?php echo $reportval['report_date'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['teacher_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['subject_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['grade_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['observer_name'];?>
			</td>
			
			</tr>
			<?php 
			$sno++;
			} ?>
			</table>
			<table><tr><td valign="middle"><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td></tr></table>
			
			<table width="100%" ><tr><td><div style="float:right;"><a href="http://get.adobe.com/reader/" target="_blank" style="color:#ffffff;text-decoration:none;"><img src="<?php echo SITEURLM?>images/get_adobe_reader.png"></a></div></td></tr></table>
			
			<?php } else if(isset($reports)) { ?>
			<table align="center"><tr><td><b>No Reports Found</b></td></tr></table>
			<?php } ?>
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
