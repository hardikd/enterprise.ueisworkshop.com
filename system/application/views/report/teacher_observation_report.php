<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<base href="<?php echo base_url();?>"/>
	<LINK href="<?php echo SITEURLM?>css/style.css" type=text/css rel=stylesheet>	
	<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<style type="text/css">
.ui-jqgrid {
    font-size: 12px;
    position: relative;
}
</style>
<script type="text/javascript">
 
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){

$("#list1").jqGrid({
   	url:'<?=base_url().'comments/getteacherobservationreportdata/'.$year.'/'.$school_id?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Teacher',<?=$form?>],	//Grid column headings
   	colModel:[
   		{name:'Teacher',index:'Teacher', width:120, align:"left", editable:false, classes: 'time_fix'},<?=$cols?>
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:650,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,
	gridComplete: function(){
	if($("#list1").parents('div.ui-jqgrid-bdiv').height()>650)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","650px");
	}
	},	
    caption:"&nbsp;&nbsp;&nbsp;Observation Count"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?=$headers;?> ] });

$("#list1").jqGrid("setColProp","Teacher",{frozen:true});
$("#list1").jqGrid("setFrozenColumns");

});
  

</script>

	</head>
<body>
<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
	     <table>
		 <tr>
		 <td>
		 Year:
		 </td>
		 <td><?php echo $year?></td>		 
		 </tr>
		 <tr>
		 <td >
		 Legend:
		 </td>
		 </tr>
		  <tr>
		 <td>Blue:&nbsp;&nbsp;<font color='#5987D3'>Formal</font></td>
		 </tr>
		 <tr>
		 <td>Red:&nbsp;&nbsp;<font color='#DD6435'>Informal</font></td>
		 </tr>
			<tr>
			<td>
			PDF:
			</td>
			<td>
			<a target="_blank" style="color:#ffffff;" href="report/teacher_observation_pdf/<?php echo $year?>/<?php echo $school_id?>">
			<img src="<?php echo SITEURLM?>images/pdf_icon.gif" >
			</a>
			</td>
			</tr>
		 </table>
	<div style="margin:10px;">	
	<table id="list1"></table>	
	
	</div>
	 </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>