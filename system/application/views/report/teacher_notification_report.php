<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

<style type="text/css">
.ui-jqgrid {
    font-size: 12px;
    position: relative;
}
.content {
    color: #657455;
    font-size: 11px;
    height: auto;
    margin-left: 10px;
    max-width: 646px;
}	
</style>


</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
        <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                      <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools/observation">Lesson Observations</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>report/getteacher_notification/">Observation Count</a>
                           
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Observation Count Report</h4>
                          
                         </div>
                         <div class="widget-body">
                       
                                
                                <div id="fullreportDiv" class="answer_list" >
                                   <div class="widget purple">
                      
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                    <div class="content">
	     <table >
		 <tr>
		 <td>
		 Year:
		 </td>
		 <td><?php echo $year?></td>		 
		 </tr>
		 <tr>
		 <td >
		 Legend:
		 </td>
		 </tr>
		 <tr>
		 <td>LP:&nbsp;&nbsp;Lesson Plan</td>
		 </tr>
		 <tr>
		 <td>HN:&nbsp;&nbsp;Home Notification</td>
		 </tr>
		 <tr>
		 <td>BL:&nbsp;&nbsp;Behavior & Learning</td>
		 </tr>
			<tr>
			<td>
			PDF:
			</td>
			<td>
			<a target="_blank" style="color:#ffffff;" href="report/teacher_notification_pdf/<?php echo $year?>/<?php echo $school_id?>">
			<img src="<?php echo SITEURLM?>images/pdf_icon.gif" >
			</a>
			</td>
			</tr>
		 </table>
	<div style="margin:10px;">	
	<table id="list1"></table>	
	
	</div>
	 </div>
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-purple"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                                
                            
                                      
                                     </div>
                                       
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     
                                 </div>
                             </div>
                             
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
<!--START OLD SCRIPT -->

	<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
 
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){

$("#list1").jqGrid({
   	url:'<?=base_url().'comments/getteachernotificationreportdata/'.$year.'/'.$school_id?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Teacher',<?=$form?>],	//Grid column headings
   	colModel:[
   		{name:'Teacher',index:'Teacher', width:120, align:"left", editable:false, classes: 'time_fix'},<?=$cols?>
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:650,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,
	gridComplete: function(){
	if($("#list1").parents('div.ui-jqgrid-bdiv').height()>650)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","650px");
	}
	},	
    caption:"&nbsp;&nbsp;&nbsp;Notification Activity Count"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?=$headers;?> ] });

$("#list1").jqGrid("setColProp","Teacher",{frozen:true});
$("#list1").jqGrid("setFrozenColumns");

});
 

</script>
	

<!--END OLD SCRIPT -->
    <script>

$("#fullreport").on('click', function() {
   document.getElementById('fullreportDiv').style.display = "block";
});


</script>


   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>