<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Leadership Points::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper2">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/leadershipmenu.php'); ?>
        <?php if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<?php /* if($countgroups>=13) { ?> style="min-height:<?php echo $countgroups*70;?>px;height:auto;" <?php  }  */?>
		<div class="content"  >
        	<?php if(!empty($reportdata)) {?>
			<table class="tabcontent1">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $this->session->userdata('report_number_new');?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			
            
			</tr>
			<tr>
			
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
			
			</tr>
			
			</table>
            <br />
			<?php } ?>
			<div style=" width:665px; height:auto; border-left:#a2ba88 1px solid; border-top:#a2ba88 1px solid; border-bottom:#a2ba88 1px solid; font-size:11px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">
			<?php if($groups!=false) {
			
			?>			
			
			
			<table width="100%" border="0" cellpadding="5" style=" border-right:#a2ba88 1px solid; font-size:12px;  ">
  <tr>
    <td style="width:100px;font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $group_name;?></td>
  </tr>
  <tr>
    <td style="font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $description;?></td>
  </tr>
</table>

<!--			<div style="width:100px;height:auto; float:left; border-right:#a2ba88 1px solid;">
       		 	<div  style="padding:5px; font-weight:bold; color:#657455;font-size:12px;">Header:</div>
 		       <div style="padding:5px 5px 5px 5px; font-weight:bold;font-size:12px; color:#657455;">Standard:</div>
    	
   			 </div>
			<div style="padding:5px;font-size:12px; margin-left:100px; border-right:#a2ba88 1px solid;"><?php /*?><?php echo $group_name;?><?php */?></div>
     	   <div style="padding:5px 5px 5px 5px;font-size:12px; margin-left:100px; border-right:#a2ba88 1px solid;"><?php /*?><?php echo $description;?><?php */?></div>
			
			</div>-->
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
			
			<?php if($groups!=false) { ?>
			<form action="report/leadershipselfsave" name="" method="post">
			<table cellpadding="0" cellspacing="0">
			<tr>
			<td>
			<input type="hidden" name="group_id" value="<?php echo $group_id;?>">
			<input type="hidden" name="next_group_id" value="<?php echo $next_group_id;?>">
			</td>
			</tr>
			</table>
			<div style=" min-width:551px;">
			<table width="100%" cellpadding="5" cellspacing="0" >
			
            
		  <?php if($alldata!=false) { 
		    
		    foreach($alldata as $key=> $val)
			{
			$check=0;
			$scheck=0;
			
		  ?>	
			<tr>
			<td style="background:#f2f6ed;  font-weight:bold; border-top:#a2ba88 1px solid; border-bottom:#a2ba88 1px solid;color:#657455; border-right:#a2ba88 1px solid;font-size:12px; width:100px;">
			
            Element
			</td>
			<?php foreach($val['names'] as $names)
			{
			
			?>
			<td style="background:#f2f6ed; font-weight:bold; border-top:#a2ba88 1px solid; border-bottom:#a2ba88 1px solid; border-right:#a2ba88 1px solid;color:#657455;font-size:12px;">
			<?php echo $names;?>
			</td>
			<?php } ?>
			</tr>
			<tr>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  "><div style="padding:5px;">
			<?php echo $val['question'];?></div>
			</td>
			<?php foreach($val['text'] as $text)
			{
			
			?>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			<?php echo $text;?>
			</td>
			<?php } ?>
			</tr>
			<tr>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td style="border-right:#a2ba88 1px solid;border-bottom:#a2ba88 1px solid;  ">
			<input type="<?php echo $val['ques_type']?>" name="<?php echo $name;?>" value="<?php echo $subgroupid;?>" <?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$key]['response'])) {
			
			//$check=1;
			}
			
			if(isset($getreportpoints[$key]['response']) && $getreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> checked=checked <?php } else  
			if(isset($getreportpoints[$key][$subgroupid])) {
			?> 
			checked=checked
			<?php } ?>
			<?php if($check==1 && $key1==0) {?> checked=checked <?php } ?>
			>Self Assessment
			</td>
			<?php } ?>
			
			</tr>
			
			
			
			
			<?php
			}
			} 
			else
			{
			?>
			
			<tr>
			<td>
			No Leadership Points Found
			</td>
			</tr>
			<?php } //print_r($getreportpoints); ?>
			
			<!--<tr>
			<td>
			Notes:<textarea name="response-text"><?php if(isset($getreportpoints[$group_id]['response-text'])) { echo $getreportpoints[$group_id]['response-text']; } ?></textarea>
			</td>
			</tr>-->
			<tr>
			<td colspan="5" align="center" style=" border-right:#a2ba88 1px solid;">
			<input class="btnsmall" type="submit" name="submit" value="next">
			</td>
			</tr>
			</table>
			</div>
			<?php } ?>
			</div>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
