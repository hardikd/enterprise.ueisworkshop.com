<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM;?>css/style.css"  rel="stylesheet" type="text/css" />
<LINK href="<?php echo SITEURLM?>css/buttonstyle.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body>
<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        
        <h2 style="color: #08A5CE">Single Assessment Report by Standard</h2>
 
      
        	<?php
			$schoolsavg =array();
			$schoolnames=array();
			$count=0;



			foreach($finalscore as $key => $value)
			{
				if(!empty($value))
				{
				
			   	$schoolnames[]= $finalscore[$key][2][0]['school_name'];
				if(empty($finalscore[$key][0][0]['schoolaverage']))
				{
					$schoolsavg[]=0;
				}	
				else
				{
				 $schoolsavg[]= $finalscore[$key][0][0]['schoolaverage'];
				}
				}
				else
				{
					
				$schoolsavg[]=0;
				//$count++;
				
				}
				
				
			}
			
			//$adv = array();
			$pro = array();
			$basic = array();
			$bbasic = array();
			foreach($schoolsavg as $h=>$l)
			{
				if($l==0)
				{
					$count++;
				}
			}
			if(count($schoolsavg)==$count)
			{
			}
			
			
			$res = mysql_query('select * from proficiency where assessment_id="'.$assessmentid.'"');
			$row = mysql_fetch_assoc($res);

			$bbfrom = $row['bbasicfrom'];
			$bbto = $row['bbasicto'];
			$basicfrom = $row['basicfrom'];
			$basicto = $row['basicto'];
			$pro_from = $row['pro_from'];
			$pro_to = $row['pro_to'];
			
			
			if(!empty($schoolsavg) && $schoolsavg!="")
			{
			for($i=0;$i<count($schoolsavg);$i++)
			{
			
			 $num = round($schoolsavg[$i]);
			
			
			if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					$basic[] = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro[] = $num;
				}
			
			}
			}
		
			//1
			
		 $ttarr = array_sum($bbasic);
		 
		  if($ttarr==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 $total =  count($bbasic);			
			  $avgbbbasic = $ttarr/$total;
			 }
		  
		 //2
		  	 $ttarr1 = array_sum($basic);	
		  
		  	 if($ttarr1==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $ttarr1/$total1;
			 }
		 //3
		 
		  $ttarr2 = array_sum($pro);		 
		 if($ttarr2==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
				  $avgpro = $ttarr2/$total2;
			 }
			 
	
			
			
			
			 $namedata = implode("','",$schoolnames);
			 
			 $scavg = implode(',',$schoolsavg);
			
		
					
			
		 $finalavg= '['.$scavg.']';  // get all school avg
			
			 $finalname = "['".$namedata."']";  // get all school names
			
			?>
            
<?php if(count($schoolsavg)!=$count)
			{
			?>
            
            
           
      <div style=" color:#000000; font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif; font-size:16px">
      <table cellpadding="5" cellspacing="0" width="100%">
      <tr>
      <td width="200" style="font-weight:bold;">Test Item Cluster</td><td><?php
	  if(!empty($testcluster) && $testcluster!="")
	  {
	   echo substr(@$testcluster,0,500);
	  }
	      
	   ?></td>
      </tr>
      </table>
      
  </div>     
			
<script src="<?php echo SITEURLM;?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM;?>js/exporting.js"></script>
<script type="text/javascript">
$(function () {

        $('#barchart').highcharts({
            chart: {
            },
            title: {
                text: 'Bar Chart'
            },
            xAxis: {
                categories: <?php echo $finalname;?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' fruits';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
            labels: {
                items: [{
                    html: 'Proficiency By School',
                    style: {
                        left: '40px',
                        top: '-25px',
                        color: 'black'
                    }
                }]
            },
            series: [  {
                type: 'column',
                name: 'Proficiency By School',
                data: <?php echo $finalavg;?>
            }, {
                type: 'spline',
                name: 'Average',
                data: <?php echo $finalavg;?>,
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }]
        });
    });
    

		</script>
        <script type="text/javascript">
$(function () {
        $('#piechart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Pie Chart Proficiency'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
			
            series: [{
                type: 'pie',
                name: 'School Proficiency ',
                data: [
                    ['Below Basic',   <?php echo $avgbbbasic; ?>],
                    ['Basic',      <?php echo $avgbasic; ?>],
					
                  	{
                        name: 'Proficient',
                        y: <?php echo $avgpro; ?>,
                        sliced: true,
                        selected: true
                    }
                    
                ]
            }]
        });
    });
    

		</script>
        
        
<div id="piechart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div id="barchart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
			
		<?php } else {
			echo "<div style='color:red;'>Records Not Available</div>";  
		}?>	
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>