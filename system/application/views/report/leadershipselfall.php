<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/leadershipmenu.php'); ?>
        <div class="content">
        	
			<?php if(isset($reports) && !empty($reports)) { ?>
			<table  class="tabcontent" cellspacing="0">
			<tr class="tchead">
			<th>
			Report Id
			</th>
			<th>
			Date
			</th>
			<th>
			Observer
			</th>
			</tr>
			<?php foreach($reports as $reportval) { ?>
			<tr>
			<td valign="middle"  class="tdlink">
			<a href="report/viewleadershipself/<?php echo $reportval['report_id'];?>/1">1</a>
			
			</td>
			<td valign="middle">
			<?php echo $reportval['report_date'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['observer_name'];?>
			</td>
			
			</tr>
			<?php 
			
			} ?>
			</table>
			
			<?php } else if(isset($reports)) { ?>
			No Reports Found
			<?php } ?>
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
