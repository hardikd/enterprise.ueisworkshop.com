<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); 
	include($view_path.'inc/pChart/pData.class');
  include($view_path.'inc/pChart/pChart.class'); 
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        	<?php if(!empty($reportdata)) {?>
			<table class="tabcontent1">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $sno;?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['teacher_name'];?></a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['subject_name'];?></a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a class="bl"><?php echo $reportdata['grade_name'];?></a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a class="bl"><?php echo $reportdata['period_lesson'];?></a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['students'];?></a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['paraprofessionals'];?></a></td>
			<td align="left">
			<b>Lesson Correlation:</b></td><td align="left"><a class="bl"><?php echo $reportdata['lesson_correlation'];?></a></td>
			</tr>
			</table>
			<?php 
			if($reportdata['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { ?>
			<br />
			<table class="tabcontent1">
			<tr>
			<td class="htitle">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $standarddata[0]['standard'];?>
			</td>
			</tr>
			</table>
			<br />
			<table class="tabcontent1">
			<tr>
			<td class="htitle">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>
			<?php echo $standarddata[0]['diff_instruction'];?>
			</td>
			</tr>
			</table>
			
			<?php } } ?>
			<?php 
			if($reportdata['lesson_correlation_id']==3)
			{
			
			if ( file_exists(WORKSHOP_FILES.'observationplans/'.$reportdata['report_id'].'.pdf') )
    {
			?>
			 
			 <table>
			 <tr>
			 <td>
			 <object  data="<?php echo WORKSHOP_DISPLAY_FILES.'observationplans/'.$reportdata['report_id'].'.pdf'?>" type="application/pdf"  width="650" height="220"></object>
			 </td>
			 </tr>
			 </table>
			 
			 <?php 
	         }		 
			 } ?>
			 
            <br />
			<table width="100px" align="right">
			<tr>
			<td>
			<a href="comments/viewproficiencypdf/<?php echo $reportdata['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			</td>
			</tr>
			</table>
			<?php } 
			
			$login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('school_id');

			}	
			?>
			<?php if($alldata!=false)
			{
			$allpiec=0;
			foreach($alldata as $key=> $val)
			{
			 $allpiec=$val['group_id'];
			 break;
			}
			?>
			<table>
			<tr>
						  <td colspan="2">
						  <font color="#08A5D6"><b></b></font>
						  </td>
						  </tr>
						  <tr>
  <td colspan="2">
  <img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>stacked/<?php echo $login_type.'_'.$login_id.'_'.$allpiec;?>.png" style="border: 1px solid gray;"/>
  </td>
</tr>
</table>
<?php } ?>
			<table>
			<?php 
			$group_again=0;
			$c=0;
			//$allpiec=0;
			$allpiedata=array();
			$piedata=array();
			if($alldata!=false) {
						
			
			?>
			<?php
			
			
			foreach($alldata as $key=> $val)
			{
			$check=0;
			$scheck=0;
			
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			 $tempdata=$piedata;
			 $piedata=array();
			
			if($c>0)
			{?>
			
			</table>
			<table>
			<tr>
			<td>
			<?php if(isset($tempdata) && !empty($tempdata))
			{
			$chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	foreach($tempdata as $tempkey=>$valtemp)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $name=preg_replace($sPattern, $sReplace, $tempkey);
		if(strlen($name)>35)
		{
		   $limitname=substr($name,0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($name,0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$name;
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$valtemp.')', $valtemp));
	}	
	
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piecdata.".png");
	?>
	<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>bar/<?php echo $login_type.'_'.$login_id.'_'.$piecdata;?>.png" style="border: 1px solid gray;"/>
			<?php } ?>
			</td>
			</tr>
			</table>
			<?php 
			}
			?>
			<br />
			<table border="0" cellpadding="5" cellspacing="0" style="border:1px #a2ba88 solid; width:667px;">
			<tr>
    <td style="width:100px;font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $val['group_name'];?></td>
  </tr>
  <tr>
    <td style="font-size:13px;font-weight:bold;color:#444444;" colspan="5"><?php echo $val['description'];?></td>
  </tr>
			<?php } 
			
			$piedata[$val['question']]=0;
			$piecdata=$val['group_id'];
			$c++; ?>
			<tr>
			<td style="border-top:1px #a2ba88 solid;border-bottom:1px #a2ba88 solid; background:#f0f5eb; width:100px;">
			Element
			</td>
			<?php 
			
			foreach($val['names'] as $names)
			{
			
			?>
			<td style="border-top:1px #a2ba88 solid;border-bottom:1px #a2ba88 solid; border-left:1px #a2ba88 solid; background:#f0f5eb;">
			<?php echo $names;?>
			</td>
			<?php } ?>
			</tr>
			<tr>
			<td style="vertical-align: top;width:130px;border-bottom:1px #a2ba88 solid;">
			<?php 
			
			echo $val['question'];?>
			</td>
			<?php foreach($val['text'] as $text)
			{
			 
			?>
			<td style="vertical-align: top;width:130px;border-bottom:1px #a2ba88 solid; border-left:1px #a2ba88 solid;">
			<?php echo $text;?>
			</td>
			<?php } ?>
			</tr>
			<?php if($this->session->userdata('login_type')=='user')
			{
			?>
			
			<tr>
			<td>
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if(!isset($allpiedata[$val['group_name']][$key1]) )
			{
			$allpiedata[$val['group_name']][$key1]=0;
			
			}
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td>
			 <?php 
			 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$key]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$key]['response']) && $getreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg"> <?php 
			$allpiedata[$val['group_name']][$key1]++;
			$piedata[$val['question']]++;} else  
			if(isset($getreportpoints[$key][$subgroupid])) {
			?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg">
			<?php 
			$allpiedata[$val['group_name']][$key1]++;
			$piedata[$val['question']]++; } else  if($check==1 && $key1==0) {?> <img src="<?php echo SITEURLM?>images/checked.jpg"> <?php 
			 $allpiedata[$val['group_name']][$key1]++;
			 $piedata[$val['question']]++;
			} else { ?>
			<img src="<?php echo SITEURLM?>images/unchecked.jpg">
			<?php } ?>
			 Assessment
			</td>
			<?php 
			//$allpiec++;
			} ?>
			
			</tr>
		 
			<?php
			} 
			else
			{
			?>
			<tr>
			<td>
			</td>
			<?php 
			
			  ?>
			 <?php foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if(!isset($allpiedata[$val['group_name']][$key1]) )
			{
			$allpiedata[$val['group_name']][$key1]=0;
			
			}
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			?>
			<td>
			 <?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$key]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$key]['response']) && $getreportpoints[$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { ?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg"> <?php 
			$allpiedata[$val['group_name']][$key1]++;
			$piedata[$val['question']]++;
			} else  
			if(isset($getreportpoints[$key][$subgroupid])) {
			?> 
			<img src="<?php echo SITEURLM?>images/checked.jpg">
			<?php 
			
			$allpiedata[$val['group_name']][$key1]++;
			$piedata[$val['question']]++;
			} else  if($check==1 && $key1==0) {?> <img src="<?php echo SITEURLM?>images/checked.jpg"> <?php 
			
			$allpiedata[$val['group_name']][$key1]++;
			$piedata[$val['question']]++;
			} else { ?>
			<img src="<?php echo SITEURLM?>images/unchecked.jpg">
			<?php } ?>
			 Assessment
			</td>
			<?php } ?>
			
			</tr>
			
			<?php
			
			
			
			}
			
			$group_again=$val['group_id'];
			
			}
		  ?>
		    </table>
			<table>
			<tr>
			<td>
			<?php
			//print_r($allpiedata);
			if(isset($piedata) && !empty($piedata))
			{
			$chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	foreach($piedata as $key=>$val)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $name=preg_replace($sPattern, $sReplace, $key);
		if(strlen($name)>35)
		{
		   $limitname=substr($name,0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($name,0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$name;
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$val.')', $val));
	}	
	
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piecdata.".png");
	?>
	<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>bar/<?php echo $login_type.'_'.$login_id.'_'.$piecdata;?>.png" style="border: 1px solid gray;"/>
			<?php } ?>
			</td>
			</tr>
			</table>
			<?php
			}
			if(!empty($allpiedata))
			{
			$pdc=0;
			
			 
			foreach($allpiedata as $keyall=>$val)
	{
	   
	   $cd=count($val);
       if($cd>$pdc)
	   {
	     $pdc=$cd;
	   
	   }
	
	}
	
	
	for($kj=0;$kj<$pdc;$kj++)
	{
	  
	  
	foreach($allpiedata as $keyall=>$val)
	{
	  	if(isset($val[$kj]))
		{
		 $allpiecdata[$keyall][$kj]=$val[$kj];
		
		}
		else
		{
		 $allpiecdata[$keyall][$kj]=0;
		
		}
	}
	
	
	}
			
			$DataSet = new pData;
foreach($allpiedata as $key=>$val)
		{
	    
		//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $name=preg_replace($sPattern, $sReplace, $key);
		
		if(strlen($name)>35)
		{
		   
		   $limitname=substr($name,0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($name,0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$name;
		
		}
		
		
		$DataSet->AddPoint($val,trim($limitname));
		
		$DataSet->AddSerie(trim($limitname)); 
		
      $DataSet->SetSerieName(trim($limitname),trim($limitname)); 
 	  
		
	}
				for($dr=1;$dr<=$pdc;$dr++)
	{
	$jhk=$dr-1;
	$drawgraph[$jhk]='Element'.$dr;
	}

	$DataSet->AddPoint($drawgraph,'Element');
	$DataSet->SetAbsciseLabelSerie("Element");  

  // Initialise the graph
  $Test = new pChart(600,230);
  $Test->setFontProperties($view_path."inc/Fonts/tahoma.ttf",8);
  $Test->setGraphArea(60,30,300,200);
  $Test->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);
  $Test->drawRoundedRectangle(5,5,695,225,5,230,230,230);
  $Test->drawGraphArea(255,255,255,TRUE);
  $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_ADDALL,150,150,150,TRUE,0,2,TRUE);
  $Test->drawGrid(4,TRUE,230,230,230,50);

  // Draw the 0 line
  $Test->setFontProperties($view_path."inc/Fonts/tahoma.ttf",6);
  $Test->drawTreshold(0,143,55,72,TRUE,TRUE);

  // Draw the bar graph
  $Test->drawStackedBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(),TRUE);

  // Finish the graph
  $Test->setFontProperties($view_path."inc/Fonts/tahoma.ttf",8);
  $Test->drawLegend(346,20,$DataSet->GetDataDescription(),255,255,255);
  $Test->setFontProperties($view_path."inc/Fonts/tahoma.ttf",10);
  $Test->drawTitle(40,12,"Summative Bar Graph",50,50,50,585);
  $Test->Render(WORKSHOP_FILES."stacked/".$login_type.'_'.$login_id.'_'.$allpiec.".png");			
			}
			else
			{
			?>
			<table>
			<tr>
			<td>
			No Observation Points Found.
			</td>
			</tr>
			</table>
			<?php } ?>
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
