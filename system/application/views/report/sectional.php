<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
    
   <style>
    table {border-collapse: separate !important;}
    h1, h2, h3 {line-height: 20px !important; text-align: left !important;}
    .subobs {float: none !important;}
	
	#leftt{margin-left: 15px;}
		.content {
    color: #657455;
    font-size: 11px;
    height: auto;
    margin-left: 10px;
    max-width: 646px;
}
	
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); 
//	include($view_path.'inc/class/pData.class.php');
//include($view_path.'inc/class/pDraw.class.php');
//include($view_path.'inc/class/pImage.class.php');
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	
	?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                      <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools/observation">Lesson Observations</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                       <?php if($this->session->userdata('login_type')=='teacher'){?>
                            <a href="<?php echo base_url();?>teacherreport/observer">Observation by Viewer</a>
                            <?php }else{ ?>
                             <a href="<?php echo base_url();?>report/observerreport">Observation by Viewer</a>
                           <?php }?>
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Observation by Viewer</h4>
                          
                         </div>
                         
                        <div class="widget-body" style="min-height: 150px;">  
                         
                                  
                                  
        <?php 
$cach = date("H:i:s");

if($this->session->userdata('reportform')!='forma')
			{
			?>                         





        <?php 
		if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<div id="leftt"  class="content">
       	  <table width="100%" border="0">
        	  <tr>
        	    <td>
                <?php if($groups!=false) { ?>			
			<div class="htitle"><?php
			echo $group_name;?></div>
			
			
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
            
            </td>
      	    </tr>
        	  <tr>
        	    <td><div style="font-size:14px;">Sectional Data &nbsp; <a style="color: rgb(255,255,255);" href="pdfreport/getpdf"  target="_blank" ><img src="<?php echo SITEURLM?>images/pdf_icon.gif" /></a></div>
		
			<div class="desc"><?php echo ucfirst($this->session->userdata('report_criteria'));?>     Name:&nbsp;&nbsp;<?php echo $this->session->userdata('report_name');?></div>
			
			</td>
      	    
			</tr>
        	  <tr>
        	    <td>
                
                	<?php if($points!=false) {
			$login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('school_id');

			}	?>

			<?php  if($group_set==1)
			{ ?>
			<table>
			<tr>
						  <td colspan="2">
						  <font color="#08A5D6"><b></b></font>
						  </td>
						  </tr>
						  <tr>
  <td colspan="2">
  <?php echo  '<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'stacked/'.$login_type.'_'.$login_id.'_'.$point_set.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
  <!--<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>stacked/<?php echo $login_type.'_'.$login_id.'_'.$point_set;?>.png" style="border-right:2px solid gray;"/>-->
  </td>
</tr>
</table>
<?php } ?>
			<?php			
			
			//echo "<pre>";
			//print_r($points);
			//print_r($sectional);
			//exit;
			$point_again=0;
			$group_id=0;
			$allpiec=0;
			$allpiedata=array();
			$previousnext=0;
			foreach($points as $val)
			{
			$previousnext++;
			//if($this->session->userdata('reportform')=='forma')
			{
			if($point_again!=$val['point_id'] && $group_id==2)
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			?>
			  <tr>
  <td colspan="2">
 <?php /*$chart = new PieChart(575, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");*/
	$chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	?>
	<?php echo  '<img alt="Bar chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
	<!--<img alt="Pie chart"  src="<?php echo SITEURLM.$view_path;?>inc/bar/<?php echo $login_type.'_'.$login_id.'_'.$piedata[0]['point_id'];?>.png" style="border: 1px solid gray;"/>-->
  </td>
  </tr>
			
			<?php } } 
			if($previousnext==1)
			{
			?>
			
			<table width="100%">
			<tr>
			<td>
			<div style="float:right; "><input type="button" id="previous" name="previous" value="<<" style="display:none">
			<input type="button" id="next" name="next" value=">>" style="display:none"></div>
			</td>
			</tr>
			</table>
			
			<?php } }
			if($val['group_type_id']!=2)
			{
			?>
                
                <table class="questab" >
  
  <tr>
    <td colspan="2"><div class="ques1"><?php echo $val['question'];?></div></td>
    </tr>
  <tr>
    <td class="hlft">Report Number:</td>
    <td class="rc1"><?php 
			$rcc=1;
			if($reports!=false) { 
			$rc=0;
			$showreport=1;
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $showreport;?>
			</div>
			<?php 
			if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			$showreport++;
			
			} } else { ?>
			<div>
			No Reports Found
			</div>
			<?php } ?>
			</div>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
			</td>
		 
  </tr>
  <tr>
    <td><div class="hlft">Frequency:</div></td>
    <td class="rcf1"><?php 
			$count=0;
			
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			<?php 
			
			
			 }
			else
			{?>
			
	<div class="hbox">&nbsp;</div>
			
			<?php }
			if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} } else { ?>
			
			<?php } ?></td>
			</tr>
			
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div>
	
			</td>
 
  </tr>
  <tr>
			<?php //if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td colspan="20">
  <div>
  <?php 
  /*$gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();*/
	$chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	//$dataSet->addPoint(new Point("score", $countreport));
	$dataSet->addPoint(new Point("report",$count ));
	
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$val['point_id'].".png");
	?>
	<?php echo  '<img alt="Bar chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$val['point_id'].'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
	<!--<img alt="Bar chart"  src="<?php echo SITEURLM.$view_path;?>inc/bar/<?php echo $login_type.'_'.$login_id.'_'.$val['point_id'];?>.png" style="border: 1px solid gray;"/>-->
	</div>
  </td>
  <?php } ?>
  </tr>
  <?php //if($this->session->userdata('reportform')=='forma')
			{ 
			
			$allpiec++;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiecdata[$allpiec][]=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];

			?>
  <!--<tr>
  <td colspan="2">
 <?php /*$chart = new PieChart(575, 300);

	$dataSet = new XYDataSet();
	$dataSet->addPoint(new Point($val['question'].'('.$count.')', $count));
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$val['point_id'].".png");*/
	?>
	<img alt="Pie chart"  src="<?php //echo WORKSHOP_DISPLAY_FILES;?>generated/<?php //echo $val['point_id'];?>.png" style="border: 1px solid gray;"/>
  </td>
  </tr>-->
  <?php } ?>
</table>
</td>
      	    </tr>
   	      <tr>
        	    <td>	<?php 
			}
			else if($val['group_type_id']==2) {
			
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			?><table class="questab">
  <tr>
    <td colspan="3"><div class="ques1"><?php echo $val['question'];?></div></td>
    </tr>
  <tr>
    <td><div class="hlft">Report Number:</div></td>
    <td class="rc1">	<?php 
			$rcc=1;
			if($reports!=false) { 
			$rc=0;
			$showreport=1;
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $showreport;?>
			</div>
			<?php 
			if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			$showreport++;
			} } else { ?>
			<div>
			No Reports Found
			</div>
			<?php } ?>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
			</td>
  
  </tr>
  <tr>
    <td><div class="hlft"><?php echo $val['sub_group_name'];?></div></td>
    <td class="rcf1">	<?php 
			$count=0;
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			<?php 
			
			
			 }
			else
			{?>
				<div class="hbox">&nbsp;</div>
			<?php 
			
			 }
			 if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} 
			
			} else { ?>
	
			<?php } ?></td>
   
		<?php //if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  <div>
  <?php 
  /*$gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();*/
	?>
	</div>
  </td>
  <?php } ?>
			
  </tr>  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
  
  </tr>
<?php  
$piec=0;
$piedata[$piec]['name']=$val['sub_group_name'];
$piedata[$piec]['count']=$count;
$piedata[$piec]['point_id']=$val['point_id'];
			$allpiec++;
			$allpiecsub=0;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiedata[$allpiec]['subgroup']=1;
			$allpiedata[$allpiec][$allpiecsub]=$val['sub_group_name'];
			$allpiecdata[$allpiec][]=$count;
			//$allpiedata[$allpiec][$allpiecsub]['count']=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];
} 
			else {
			$piec++;
			$allpiecsub++;
			?>

  <tr>
    <td ><div class="hlft"> <?php echo $val['sub_group_name'];?></div></td>
    <td class="rcf1">			<?php 
			$count=0;
			if($reports!=false && $sectional!=false) {
			$rc=0;
			$rcc=1;			
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			<?php 
			
			
			 }
			else
			{?>
				<div class="hbox">&nbsp;</div>
			<?php 
			
			 }
			 if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			}?> 
			
			<?php } else { ?>

			<?php } ?></td>
			 <?php //if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  <div>
  <?php 
  /*$gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();*/
	?>
	</div>
  </td>
  <?php } ?>
    
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
 
  </tr>
               		
        	      <?php 
				  
				  $piedata[$piec]['name']=$val['sub_group_name'];
				  $piedata[$piec]['count']=$count;
				  $piedata[$piec]['point_id']=$val['point_id'];
				  
				  
			
			
			$allpiedata[$allpiec][$allpiecsub]=$val['sub_group_name'];
			//$allpiedata[$allpiec][$allpiecsub]['count']=$count;
			$allpiecdata[$allpiec][]=$count;
			
				  } ?> 
			
			
			<?php }
			
			$point_again=$val['point_id'];
			$group_id=$val['group_type_id'];
			
			}
			//if($this->session->userdata('reportform')=='forma')
			{
			if(!isset($plotteddata[$point_again]) )
			{
			if(isset($piedata[0]['point_id']))
			{
			if($point_again==$piedata[0]['point_id'])
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			?>
			  <tr>
  <td colspan="20">
 <?php 
 $chart = new VerticalBarChart(500, 250);  
 //$chart = new PieChart(575, 300);


	$dataSet = new XYDataSet();
	//print_r($piedata);
	//$dataSet->addPoint(new Point('Score', $countreport));
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Bar Chart");
	
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	?>
	<?php echo  '<img alt="Bar chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
	<!--<img alt="Bar chart"  src="<?php echo SITEURLM.$view_path;?>inc/bar/<?php echo $login_type.'_'.$login_id.'_'.$piedata[0]['point_id'];?>.png" style="border: 1px solid gray;"/>-->
  </td>
  </tr>
			
			 
			
			<?php
			}
			}
			}
			}
			}
			
			} 
			else
			{
			?>
			
	
			No Observation Points Found
	
			<?php } //print_r($getreportpoints); ?>
			<?php  if(isset($allpiec) && $allpiec!=0)
			{ ?>
						  
 <?php /*$chart = new PieChart(650, 350);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=1;$pj<=$allpiec;$pj++)
	{
		//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $allpiedata[$pj]['name']=preg_replace($sPattern, $sReplace, $allpiedata[$pj]['name']);
		
		if(strlen($allpiedata[$pj]['name'])>35)
		{
		   
		   $limitname=substr($allpiedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$allpiedata[$pj]['name'];
		
		}
		$dataSet->addPoint(new Point(trim($limitname).'('.$allpiedata[$pj]['count'].')', $allpiedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$allpiedata[1]['point_id'].".png");*/
	$MyData = new pData(); 
	/*$DataSet->AddPoint(array(1,4,6,2),"Serie1");
  $DataSet->AddPoint(array(3,3,2,1),"Serie2");
  $DataSet->AddPoint(array(4,1,2,0),"Serie3");*/
  
  $pdc=0;
  for($pj=1;$pj<=$allpiec;$pj++)
	{
	   
	   $cd=count($allpiecdata[$pj]);
       if($cd>$pdc)
	   {
	     $pdc=$cd;
	   
	   }
	
	}
	
	//echo '<pre>';
	//print_r($allpiecdata);
	
	for($kj=0;$kj<$pdc;$kj++)
	{
	  
	  
	foreach($allpiecdata as $keyall=>$val)
	{
	  	if(isset($val[$kj]))
		{
		 $newallpiecdata[$kj][]=$val[$kj];
		
		}
		else
		{
		 $newallpiecdata[$kj][]=0;
		
		}
	}
	
	
	}
	//echo '<pre>';
	//print_r($newallpiecdata);
	$drawgraph=array();
	for($dr=1;$dr<=$pdc;$dr++)
	{
	$jhk=$dr-1;
	if(isset($allpiedata[1][$jhk]))
	{
		$drawgraph[$jhk]=$allpiedata[1][$jhk];
		
		$MyData->addPoints($newallpiecdata[$jhk],$allpiedata[1][$jhk]);
		
		
		
      $MyData->setSerieDescription($allpiedata[1][$jhk],$allpiedata[1][$jhk]); 		
	}
	else
	{
       $drawgraph[$jhk]='Element'.$dr;
	   
	   $MyData->addPoints($newallpiecdata[$jhk],'Element'.$dr);
		
		
		
      $MyData->setSerieDescription('Element'.$dr,'Element'.$dr); 		
	}		
	}
	
  for($pj=1;$pj<=count($allpiedata);$pj++)
	{
		
	    
		//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $allpiedata[$pj]['name']=preg_replace($sPattern, $sReplace, $allpiedata[$pj]['name']);
		
		if(strlen($allpiedata[$pj]['name'])>30)
		{
		   
		   $limitname=substr($allpiedata[$pj]['name'],0,30);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$allpiedata[$pj]['name'];
		
		}
		
		$namelimit[]=$limitname;
		
	}
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 $myPicture = new pImage(600,350,$MyData);

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>50);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(250,55,"Summative Bar Graph",array("FontSize"=>20,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 if(count($allpiedata)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else
 {
    $myPicture->setGraphArea(80,60,250,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(510,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render(WORKSHOP_FILES."stacked/".$login_type.'_'.$login_id.'_'.$allpiedata[1]['point_id'].".png");
	?>
	
<?php } ?>
 </table>
</td>

      	  </table>
      
    </div>



</body>
</html>
<?php } else { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Sectional Report::</title>
<base href="<?php echo base_url();?>"/>

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/reportnext.js" type="text/javascript"></script>
<style type="text/css">
.questab{
background-color:#FFFFFF;
max-width:600px;
}
.ques1{
width:384px;
font-size:13px;

}
</style>
</head>

<body>

	<?php //require_once($view_path.'inc/class.htmlgraph.php'); 
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	require_once($view_path.'inc/highcharts/Highchart.php'); 
	
	$lessonchart = new Highchart();
		 foreach ($lessonchart->getScripts() as $script) {
         echo '<script type="text/javascript" src="' . $script . '"></script>';
      }
		//echo '<script src='.SITEURLM.$view_path.'js/jqueryhighchart.js"></script>';
		//echo '<script src='.SITEURLM.$view_path.'js/highcharts.js"></script>';
		
		echo '<script src="'.SITEURLM.'js/exporting.js"></script>';
		echo "<script type='text/javascript'>
		var theme = {
   colors: ['#058DC7', '#50B432', '#910000', '#33C6E7', '#492970', '#F6F826', '#263C53', '#FFF263', '#6AF9C4'],
   chart: {
      backgroundColor: {
         
      }
      
      
   },
   title: {
      style: {
         color: '#000',
         font: 'bold 16px Trebuchet MS, Verdana, sans-serif'
      }
   },
   subtitle: {
      style: {
         color: '#666666',
         font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
      }
   },
   xAxis: {
      gridLineWidth: 1,
      lineColor: '#000',
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
      }
   },
   yAxis: {
      minorTickInterval: 'auto',
      lineColor: '#000',
      lineWidth: 1,
      tickWidth: 1,
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'
         }
      }
   },
   legend: {
      itemStyle: {
         font: '9pt Trebuchet MS, Verdana, sans-serif',
         color: 'black'

      },
      itemHoverStyle: {
         color: '#039'
      },
      itemHiddenStyle: {
         color: 'gray'
      }
   },
   labels: {
      style: {
         color: '#99b'
      }
   },

   navigation: {
      buttonOptions: {
         theme: {
            stroke: '#CCCCCC'
         }
      }
   }
};

var highchartsOptions = Highcharts.setOptions(theme);
</script>		";
	?>

	
    <div class="mbody">
    	
        <?php 
		
		
	
		if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<div class="content"  >
       	  <table width="100%" border="0" >
        	  <tr>
        	    <td>
                
                	<?php if($groups!=false) {
			
			?>			
			<div class="htitle"><?php
			echo $group_name;?></div>
			
			
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
            
            </td>
      	    </tr>
        	  <tr>
        	    <td><div style="font-size:14px;">Sectional Data &nbsp; <a style="color: rgb(255,255,255);" href="pdfreport/getpdf"  target="_blank" > <img src="<?php echo SITEURLM?>images/pdf_icon.gif" /> </a></div>
		
			<div class="desc"><?php echo ucfirst($this->session->userdata('report_criteria'));?>     Name:&nbsp;&nbsp;<?php echo $this->session->userdata('report_name');?></div>
			
			</td>
			
      	    
			</tr>
        	  <tr>
        	    <td>
                
                	<?php if($points!=false) {
			$login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('school_id');

			}	?>

			<?php  if($this->session->userdata('reportform')=='forma' && $group_set==1)
			{ ?>
			<!--<table>
			<tr>
						  <td colspan="2">
						  <font color="#08A5D6"><b>All Elements Pie chart</b></font>
						  </td>
						  </tr>
						  <tr>
  <td colspan="2">
  <?php //echo  '<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$point_set.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
  <img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$point_set;?>.png" style="border: 1px solid gray;"/>
  </td>
</tr>
</table>-->
<?php } ?>
			<?php			
			
			//echo "<pre>";
			//print_r($points);
			//print_r($sectional);
			//exit;
			$point_again=0;
			$group_id=0;
			$allpiec=0;
			$allpiedata=array();
			$previousnext=0;
			foreach($points as $val)
			{
			$previousnext++;
			if($this->session->userdata('reportform')=='forma')
			{
			if($point_again!=$val['point_id'] && $group_id==2)
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			?>
			  <!--<tr>
  <td colspan="2">-->
 <?php /*$chart = new PieChart(575, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");*/
	?>
	<?php //echo  '<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
	<!--<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$piedata[0]['point_id'];?>.png" style="border: 1px solid gray;"/>-->
  <!--</td>
  </tr>-->
			
			<?php } } 
			if($previousnext==1)
			{
			?>
			
			<table width="100%">
			<tr>
			<td>
			<div style="float:right; "><input type="button" id="previous" name="previous" value="<<" style="display:none">
			<input type="button" id="next" name="next" value=">>" style="display:none"></div>
			</td>
			</tr>
			</table>
			
			<?php } }
			if($val['group_type_id']!=2)
			{
			?>
                
                <table class="questab" >
  
  <tr>
    <td valign="top" ><div class="ques1"><?php echo $val['question'];?></div></td>
   <!-- </tr>-->
  <?php /*<tr>
    <td class="hlft">Report Number:</td>
    <td class="rc1"><?php 
			$rcc=1;
			if($reports!=false) { 
			$rc=0;
			$showreport=1;
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $showreport;?>
			</div>
			<?php 
			if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			$showreport++;
			} } else { ?>
			<div>
			No Reports Found
			</div>
			<?php } ?>
			</div>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
			</td>
		 
  </tr> */ ?>
  <!--<tr>-->
    <!--<td><div class="hlft">Frequency:</div></td>-->
    <!--<td class="rcf1">--><?php 
			$count=0;
			
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			

			<!--<div class="hbox">X</div>-->
			<?php 
			
			
			 }
			else
			{?>
			
	<!--<div class="hbox">&nbsp;</div>-->
			
			<?php }
			if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			<!--</td>
			<td class="rcf<?php //echo $rcc;?>" style="display:none">-->
			<?php
			
			}
			
			} } else { ?>
			
			<?php } ?> <!--</td>-->
			<?php if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td valign="top">
  
  <?php 
  /*$gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();*/
	    $namelimit_cs=array();
		$lessonplan_cs=array();$lessonplan_ts=array();	$lessonplan_cs[]=intval($count);$lessonplan_ts[]=intval($countreport); 
		$namelimit_cs[]='  ';
	     

	        $rcc_cs=$val['point_id'];
	        $lessonchart='lessonchart'.$rcc_cs;						
			$$lessonchart = new Highchart();
			$$lessonchart->series[] = array('name' => 'Teaching Standard Observed','data' => $lessonplan_cs);$$lessonchart->series[] = array('name' => 'Number of Classroom Observations','data' => $lessonplan_ts);						
			
			
			$$lessonchart->chart->renderTo = "lessoncontainer".$rcc_cs;
			$$lessonchart->chart->type = "column";
			$$lessonchart->title->text = "";
			$$lessonchart->xAxis->categories = $namelimit_cs;
			$$lessonchart->credits->text = '';						
			
			$$lessonchart->yAxis->min = 0;
			if($countreport<=5)
			{
			$$lessonchart->yAxis->tickInterval = 1;
			}
			else if($countreport<=10)
			{
			$$lessonchart->yAxis->tickInterval = 2;
			}
			else
			{
			$$lessonchart->yAxis->tickInterval = 5;
			
			}
			
			$$lessonchart->yAxis->allowDecimals =false;
			$$lessonchart->exporting->enabled =false;
$$lessonchart->yAxis->title->text = "";
$$lessonchart->yAxis->stackLabels->enabled = 1;
$$lessonchart->yAxis->stackLabels->style->fontWeight = "bold";
$$lessonchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");

$$lessonchart->tooltip->formatter = new HighchartJsExpr("function() {
     return ''+ this.series.name +':'+ this.y ;}");


$$lessonchart->plotOptions->column->dataLabels->enabled = 0;
$$lessonchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$lessonchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
	?>
	<div id="lessoncontainer<?php echo $rcc_cs?>" style="width:270px;height:200px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='lessonchart'.$rcc_cs;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
	
  </td>
  <?php } ?>
  </tr>
  <tr>
    <!--<td>Element Observed:</td>-->
    <!--<td><div class="subobs"><?php //echo $count;?> of <?php //echo $countreport;?> observations</div>
	
			</td>-->
 
  </tr>
  <?php if($this->session->userdata('reportform')=='forma')
			{ 
			
			$allpiec++;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiedata[$allpiec]['count']=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];

			?>
  <!--<tr>
  <td colspan="2">
 <?php /*$chart = new PieChart(575, 300);

	$dataSet = new XYDataSet();
	$dataSet->addPoint(new Point($val['question'].'('.$count.')', $count));
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$val['point_id'].".png");*/
	?>
	<img alt="Pie chart"  src="<?php //echo WORKSHOP_DISPLAY_FILES;?>generated/<?php //echo $val['point_id'];?>.png" style="border: 1px solid gray;"/>
  </td>
  </tr>-->
  <?php } ?>
</table>
</td>
      	    </tr>
   	      <tr>
        	    <td>	<?php 
			}
			else if($val['group_type_id']==2) {
			
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			?><table class="questab">
  <tr>
    <td valign="top" ><div class="ques1"><?php echo $val['question'];?></div></td>
    </tr>
  <?php /* <tr>
    <td><div class="hlft">Report Number:</div></td>
    <td class="rc1">	<?php 
			$rcc=1;
			if($reports!=false) { 
			$rc=0;
			$showreport=1;
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $showreport;?>
			</div>
			<?php 
			if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			$showreport++;
			} } else { ?>
			<div>
			No Reports Found
			</div>
			<?php } ?>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
			</td>
  
  </tr> */ ?>
  <tr>
    <td valign="top"><div class="hlft"><?php echo $val['sub_group_name'];?></div></td>
    <!--<td class="rcf1">-->	<?php 
			$count=0;
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<!--<div class="hbox">X</div>-->
			<?php 
			
			
			 }
			else
			{?>
				<!--<div class="hbox">&nbsp;</div>-->
			<?php 
			
			 }
			 if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			<!--</td>
			<td class="rcf<?php //echo $rcc;?>" style="display:none">-->
			<?php
			
			}
			
			} 
			
			} else { ?>
	
			<?php } ?><!--</td>-->
   
		<?php if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  
  <?php 
  /*$gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)


    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();*/
	    
		$namelimit_cs=array();
		$lessonplan_cs=array();$lessonplan_ts=array();	$lessonplan_cs[]=intval($count);$lessonplan_ts[]=intval($countreport); 
		$namelimit_cs[]='  ';
	

	        $rcc_cs='sub'.$val['sub_group_id'];
	        $lessonchart='lessonchart'.$rcc_cs;						
			$$lessonchart = new Highchart();
			$$lessonchart->series[] = array('name' => 'Teaching Standard Observed','data' => $lessonplan_cs);$$lessonchart->series[] = array('name' => 'Number of Classroom Observations','data' => $lessonplan_ts);						
			
			
			$$lessonchart->chart->renderTo = "lessoncontainer".$rcc_cs;
			$$lessonchart->chart->type = "column";
			$$lessonchart->title->text = "";
			$$lessonchart->xAxis->categories = $namelimit_cs;
			$$lessonchart->credits->text = '';						
			
			$$lessonchart->yAxis->min = 0;
			if($countreport<=5)
			{
			$$lessonchart->yAxis->tickInterval = 1;
			}
			else if($countreport<=10)
			{
			$$lessonchart->yAxis->tickInterval = 2;
			}
			else
			{
			$$lessonchart->yAxis->tickInterval = 5;
			
			}
			$$lessonchart->yAxis->allowDecimals =false;
			$$lessonchart->exporting->enabled =false;
$$lessonchart->yAxis->title->text = "";

$$lessonchart->yAxis->stackLabels->enabled = 1;
$$lessonchart->yAxis->stackLabels->style->fontWeight = "bold";
$$lessonchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");

$$lessonchart->tooltip->formatter = new HighchartJsExpr("function() {
     return ''+ this.series.name +':'+ this.y ;}");


$$lessonchart->plotOptions->column->dataLabels->enabled = 0;
$$lessonchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$lessonchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
	?>
	<div id="lessoncontainer<?php echo $rcc_cs?>" style="width:270px;height:200px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='lessonchart'.$rcc_cs;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>		
  </td>
  <?php } ?>
			
  </tr>  <tr>
    <!--<td>Element Observed:</td>-->
    <!--<td><div class="subobs"><?php //echo $count;?> of <?php //echo $countreport;?> observations</div></td>-->
  
  </tr>
<?php  
$piec=0;
$piedata[$piec]['name']=$val['sub_group_name'];
$piedata[$piec]['count']=$count;
$piedata[$piec]['point_id']=$val['point_id'];
} 
			else {
			$piec++;
			?>

  <tr>
    <td valign="top" ><div class="hlft"> <?php echo $val['sub_group_name'];?></div></td>
    <!--<td class="rcf1">-->			<?php 
			$count=0;
			if($reports!=false && $sectional!=false) {
			$rc=0;
			$rcc=1;			
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<!--<div class="hbox">X</div>-->
			<?php 
			
			
			 }
			else
			{?>
				<!--<div class="hbox">&nbsp;</div>-->
			<?php 
			
			 }
			 if($rc>15)
			{
			$rcc++;
			$rc=0;
			?>
			<!--</td>
			<td class="rcf<?php //echo $rcc;?>" style="display:none">-->
			<?php
			
			}
			
			}?> 
			
			<?php } else { ?>

			<?php } ?><!--</td>-->
			 <?php if($this->session->userdata('reportform')=='forma')
			{ ?>
  <td rowspan="2">
  
  <?php 
  /*$gr = new HTMLGraph("37", "100", " ");
    
    // if you want to override the settings from the template related to title
    //$gr->title->style = "text-decoration:underline";
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();*/
	    $namelimit_cs=array();
		$lessonplan_cs=array();$lessonplan_ts=array();	$lessonplan_cs[]=intval($count);$lessonplan_ts[]=intval($countreport); 
		$namelimit_cs[]='  ';
	        $rcc_cs='sub'.$val['sub_group_id'];
	        $lessonchart='lessonchart'.$rcc_cs;						
			$$lessonchart = new Highchart();
			$$lessonchart->series[] = array('name' => 'Teaching Standard Observed','data' => $lessonplan_cs);$$lessonchart->series[] = array('name' => 'Number of Classroom Observations','data' => $lessonplan_ts);						
			
			
			$$lessonchart->chart->renderTo = "lessoncontainer".$rcc_cs;
			$$lessonchart->chart->type = "column";
			$$lessonchart->title->text = "";
			$$lessonchart->xAxis->categories = $namelimit_cs;
			$$lessonchart->credits->text = '';						
			
			$$lessonchart->yAxis->min = 0;
			if($countreport<=5)
			{
			$$lessonchart->yAxis->tickInterval = 1;
			}
			else if($countreport<=10)
			{
			$$lessonchart->yAxis->tickInterval = 2;
			}
			else
			{
			$$lessonchart->yAxis->tickInterval = 5;
			
			}
			$$lessonchart->yAxis->allowDecimals =false;
			$$lessonchart->exporting->enabled =false;
$$lessonchart->yAxis->title->text = "";
$$lessonchart->yAxis->stackLabels->enabled = 1;
$$lessonchart->yAxis->stackLabels->style->fontWeight = "bold";
$$lessonchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");

$$lessonchart->tooltip->formatter = new HighchartJsExpr("function() {
     return ''+ this.series.name +':'+ this.y ;}");


$$lessonchart->plotOptions->column->dataLabels->enabled = 0;
$$lessonchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$lessonchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
	?>
	<div id="lessoncontainer<?php echo $rcc_cs?>" style="width:270px;height:200px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='lessonchart'.$rcc_cs;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
	
	
  </td>
  <?php } ?>
    
  </tr>
  <tr>
    <!--<td>Element Observed:</td>-->
    <!--<td><div class="subobs"><?php //echo $count;?> of <?php //echo $countreport;?> observations</div></td>-->
 
  </tr>
               		
        	      <?php 
				  
				  $piedata[$piec]['name']=$val['sub_group_name'];
				  $piedata[$piec]['count']=$count;
				  $piedata[$piec]['point_id']=$val['point_id'];
				  } ?> 
			
			
			<?php }
			
			$point_again=$val['point_id'];
			$group_id=$val['group_type_id'];
			
			}
			if($this->session->userdata('reportform')=='forma')
			{
			if(!isset($plotteddata[$point_again]) )
			{
			if(isset($piedata[0]['point_id']))
			{
			if($point_again==$piedata[0]['point_id'])
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			?>
			  <!--<tr>
  <td colspan="2">-->
 <?php /*$chart = new PieChart(575, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");*/
	?>
	<?php //echo  '<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
	<!--<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$piedata[0]['point_id'];?>.png" style="border: 1px solid gray;"/>-->
  <!--</td>
  </tr>-->
			
			 
			
			<?php
			}
			}
			}
			}
			}
			
			} 
			else
			{
			?>
			
	
			No Observation Points Found
	
			<?php } //print_r($getreportpoints); ?>
			<?php  if($this->session->userdata('reportform')=='forma' && isset($allpiec) && $allpiec!=0)
			{ ?>
						  
 <?php /*$chart = new PieChart(575, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=1;$pj<=$allpiec;$pj++)
	{
		//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $allpiedata[$pj]['name']=preg_replace($sPattern, $sReplace, $allpiedata[$pj]['name']);
		
		if(strlen($allpiedata[$pj]['name'])>35)
		{
		   
		   $limitname=substr($allpiedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$allpiedata[$pj]['name'];
		
		}
		$dataSet->addPoint(new Point(trim($limitname).'('.$allpiedata[$pj]['count'].')', $allpiedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$allpiedata[1]['point_id'].".png");*/
	?>
	
<?php } ?>
 </table>
</td>

      	  </table>
        </div>
    </div>



</body>
</html>


<?php } ?> 
                                  
                                  
                                     
                                     
                                     
                                     
                                         </div>
                                     </div>
                                 </div>
                             </div>
                      
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  
    <!--start old script -->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/reportnext.js" type="text/javascript"></script>
    <!--end old script -->

    <script>

$("#fullreport").on('click', function() {
   document.getElementById('fullreportDiv').style.display = "block";
   document.getElementById('sectionalreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#sectionalreport").on('click', function() {
   document.getElementById('sectionalreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#qreport").on('click', function() {
   document.getElementById('qreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('sectionalreportDiv').style.display = "none";
});




</script>


   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>