<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');
ob_start();
require($view_path.'inc/html2pdf/html2pdf.class.php');
$html2pdf = new HTML2PDF('P', 'A4', 'en');


$str='';

?>
<?php if(!empty($reportdata)) {
			$str.='<HTML>
        <BODY style="font-size:10px;padding:10px;">
			<div style="padding-left:620px;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
		</div>
<table  align="center"><tr><td style="color:#C28100;">Report Data</td></tr></table>
			<table width="100%" style="width:667px; border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td align="left" >
			<b>Report Number:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$sno.'</a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['report_date'].'</a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['observer_name'].'</a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['teacher_name'].'</a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['school_name'].'</a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['subject_name'].'</a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['grade_name'].'</a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['period_lesson'].'</a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['students'].'</a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['paraprofessionals'].'</a></td>
			<td align="left"><b>Lesson Correlation:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$reportdata['lesson_correlation'].'</a></td>
			</tr>
			</table><br />';
			 } 

			
			if($reportdata['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { 
			$str.='<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['standard'].'</td></tr>
			</table>
			<br />
			<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['diff_instruction'].'
			</td></tr>
			</table><br />';
			
			 } } 
			
			$str.='<table cellspacing="0" cellpadding="5" style="width:300px;padding:5px;border:#a2ba88 1px solid;">';
			
			 if($points!=false) {	
			
			//echo "<pre>";
			//print_r($points);
		    $i=1;
			foreach($points as $val)
			{
			$i++;
			//if($i%2==0)
			{			
            
			 $str.='<tr><td >'; 
			  } 
			$str.='<div style="margin-top:10px;">
			<table style="padding:5px;border:1px #cccccc solid;width:300px;" cellspacing="0" cellpadding="3">
			<tr style="background-color:#cccccc;color:#000000;font-weight:bold;">
			<td colspan=2>'.$val['scale_name'];
			if(!empty($val['sub_scale_name'])) { 
 			$str.='-->'.$val['sub_scale_name'] ;  
			}
			$str.='</td>			
			</tr>
			<tr>
			<td>
			Strengths:
			</td>
			<td><div style="width:455px;">';
			if(isset($val['strengths'])) { 
			$str.=$val['strengths']; 
			} 
			$str.='</div></td>
			</tr>
			<tr>
			<td>
			Concerns:
			</td>
			<td><div style="width:455px;">';
			if(isset($val['concerns'])) { 
			$str.=$val['concerns'] ; 
			
			} 
			$str.='</div></td>
			</tr>
			</table></div>
			';
			
			//if($i%2!=0)
			{
			
            
			 $str.='</td></tr>';
			 } 
			 } 			
			} 
			else
			{
			$str.='	<tr>
			<td>
			No  Points Found
			</td>
			</tr>';
			 } 
			
			$str.='</table></BODY></HTML>'; 
			
			//echo $str;
			//exit;
			 $content = ob_get_clean();
			// $html2pdf->setModeDebug();
			
			$html2pdf->WriteHTML($str,isset($_GET['vuehtml']));
			if($reportdata['lesson_correlation_id']==3)
			{
			
			if ( file_exists(WORKSHOP_FILES.'observationplans/'.$reportdata['report_id'].'.pdf') )
			{
				 $login_type=$this->session->userdata('login_type');
				 if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}	
			else if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
			
				$html2pdf->Output(WORKSHOP_FILES.'observationplans/view_'.$login_type.'_'.$login_id.'.pdf', 'F');
				header('Location: '.REDIRECTURL.'pdf/viewpdf/'.$login_type.'/'.$login_id.'/'.$reportdata['report_id']);
			}
			else
			{
			$html2pdf->Output();
			
			}
	
			}
			else
			{
			$html2pdf->Output();
			
			}
			
			?>