<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
      <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

   <!--start old script -->
 
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/jscal2.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/lang/en.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>Quiz/css/jscal2.css" />
<style type="text/css">
 table #innertab {
  table-layout: fixed; 
 }
th {
  position:absolute;
   left:0; 
   height:23px;
 }
.outer {position:relative}
.inner {
  overflow-x:auto;
  overflow-y:hidden;
  margin-left:180px;
  width:467px;
}
 </style>
<script type="text/javascript">
 
function updateassessment(tag)
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var school = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
	var assessment = encodeURIComponent(tag);
 	if(tag!=-1 && fDate!='' && tDate!='' && school!=-1)
	{
		$("#reporthtml").html('&nbsp;');
  		$("#loader").show();
 		$.ajax({
		url:'<?php echo base_url();?>/importdata/getReportHtml?school_id='+school+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
 }
 
 function getAssessments(tag)
{
	if(tag!=-1)
	{	
		$.ajax({
		url:'<?php echo base_url();?>/importdata/getassessmentHtml?school_id='+tag,
		success:function(classhtml)
		{ 	
			$("#assessmentdrp").html('&nbsp;');			
			document.getElementById("assessmentdrp").innerHTML= classhtml;
		}
		});
	}
}
 
 function select_f_date()
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
	var assessment1 = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var assessment = encodeURIComponent(assessment1);
 	
	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/importdata/getReportHtml?school_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}
function select_t_date()
{
 	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
	var assessment1 = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
 	var assessment = encodeURIComponent(assessment1);
	if(tag!=-1  && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/importdata/getReportHtml?school_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}

function backlink()
{
	window.location.href = '<?php echo base_url();?>schooldistrictreport/'; 
}


</script>
   <!--end old script -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
  <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>schooldistrictreport">View a Classroom's Growth</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>View a Classroom's Growth</h4>
                          
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                          
                           <fieldset>
                                   
                                   <div class="space20"></div>
                                   
                                   
                                   <div class="space15"></div>
                                   <table>
                                   <tr>
                                   <td>

                                    <form class="form-horizontal" name="frmimportreport" action="importdatagraph" method="post" > 
                                   <div class="control-group">
                                             <label class="control-label">Select School</label>
                                             <div class="controls">
				<select class="span12 chzn-select" name="schools" id="schools" onchange="getAssessments(this.value)" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                    <option value="-1"  selected="selected">Please Select</option>
                                    <?php if(!empty($records)) {?>
                                     <option value="0" >All</option> 
                                    <?php 
                                    }
                                     foreach($records as $key => $value)
                                    {
                                        echo '<option value="'.$value['school_name'].'">'.$value['school_name'].'</option>';
                                    }
                                    ?>
                                              </select> 
                      
                	                         </div>
                                         </div> 
                                         
                     <div style="width:700px;" align="right">
                	 <input type="button" class="btn btn-small btn-purple" value="Back" name="Back" onclick="backlink()"/>  
                	</div>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Assessment</label>
                                             <div class="controls" id="assessmentdrp">
                               
                               
       <select  class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="assessment" id="assessment" >
        <option value="-1"  selected="selected">Please Select</option></select>
                               
                                             </div>
                                         </div> 
                                      	<div class="control-group">
                                             <label class="control-label">Select Time Period</label>
                                             <div class="controls">
                                        <div class="input-prepend">
                                        <label>From</label>
          <span class="add-on"><i class="icon-calendar"></i></span>
      <input value="" name="fromDate" style="width:80px" type="text"  class=" m-ctrl-medium icon-calendar"  id="f_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "f_date",
        trigger    : "f_date",
        onSelect   : function() { this.hide();
		select_f_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>
            </div>
         </div>   
          <div class="controls">
                        <div class="input-prepend">
								<label>To</label>
							<span class="add-on"><i class="icon-calendar"></i></span>
                
				 <input value="" name="toDate" style="width:80px" type="text" class=" m-ctrl-medium icon-calendar"  id="t_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "t_date",
        trigger    : "t_date",
        onSelect   : function() { this.hide();
		select_t_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script> 
          
                                        </div>
                                    </div>       
                                    
                     <div class="control-group" >
                                         <label class="control-label"></label>
                                             <div class="controls" id="exportcsvlink"> 
            
              </div>  </div>               
                     <div class="control-group" >
                                         <label class="control-label"></label>
                                             <div valign="bottom" height="30" id="loader" colspan="3" style="display:none;" class="controls" > 
            <img src="<?php echo LOADERURL;?>"/>
              </div>      </div>           
      
       <div class="control-group" ><font style="size:4">
                                         <label class="control-label"></label>
                                             <div class="controls" colspan="3" id="reporthtml" > </font>
            
              </div>      </div>           
          
                                         
                                         <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
             <input type="button" class="btn btn-small btn-purple" name="answer" value="Get Report" onClick="showDiv()" style=" padding: 5px; " />                         </div>   
              </div> 
              </form>   
                                </td></tr> 
                                </table>
                                
                                <div class="space20"></div>
                                
                                <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Classroom Growth Report</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                   
                                  <h3 style="text-align:center;">Report Appears Here</h3>
                                  </div>
                                
                                </div>   
                                </div>                    
                           
                                   
                                      
                                     
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                 
                 
   </fieldset>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   

   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
   
   
   
   
 
</body>
<!-- END BODY -->
</html>