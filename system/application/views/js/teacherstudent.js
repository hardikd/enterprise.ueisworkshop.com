$(document).ready(function() {
getthisroster(0);
$("#studentform").validate({

rules: {
grade:{
required: true
},
student_id:{
required: true
}

},
messages: {
grade:{
required: "Please Select Grade"
},
student_id:{
required: "Please Select Student"
}
}
,
errorElement:"div" 

});





		
/*$('#studentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'student/add_student',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtudentJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			
// onclick  pagination Load
$('#msgContainer .student li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
var p=$(this).parent().parent().attr('class');
var exploded = p.split('_');

$.get(base_url+"students/getstudentsdata/"+exploded[1]+"/"+exploded[2]+"/"+page+"?num=" + Math.random(), function(msg){

var msgContainer = $("div.msgContainer_"+exploded[1]+"_"+exploded[2]);

 msgContainer.html(msg);
			loading_hide();

});
});
function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function add_student(day,period_id,class_room_id)
		{
		studentadd();	 
		$('#message').text('');	
		$('#fname').val('');	 		
		$('#lname').val('');
		
		$('#message').hide();
		$('#class_room_id').val(class_room_id);	 
		$('#period_id').val(period_id);	 
		$('#day').val(day);	 				
		
		 $("select[name='grade'] option[value='']").attr("selected", true);
		  var sstr='<select class="combobox" name="student_id" id="student_id">';
	 sstr+='<option value="">No Students Found</option></select>';
	  $('#student_id').replaceWith(sstr); 
	  
		
       
		$('#studentupdate').replaceWith('<input class="btnbig" type="submit" name="submit" id="studentadd" value="Add" onclick="studentadd()">');
		//$('#studentadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="studentadd" value="Add" onclick="studentadd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 600,
			width: 700
			});
				
		
		
		}
function copytoperiods(day,period,class_room_id)
{

pdata={};
var id=day+'_'+period+'_'+class_room_id;
pdata.id=id;
pdata.copy=$('#'+id).val();
pdata.day=day;
pdata.period=period;
pdata.class_room_id=class_room_id;

pdata.teacher_id = $('#teacher_id').val();

	

$.post(base_url+'students/copy',{'pdata':pdata},function(data) {
		  
		  
		  if(data.status==1)
		  {
//                      $('#myModal-replicate-periods').modal('toggle');
                      //alert('Sucessfully Copied ');
                      $('#succmsg').html('Sucessfully Copied ');
                      $('#successbtn').modal('show');
                      getthisroster(day);	

		 }
		 else
		 {
                    $('#errmsg').html('Failed Please Try Again ');
                    $('#errorbtn').modal('show');
			 //alert('Failed Please Try Again ');
		 
		 }

},'json');	

}
function copytodays()
{
var select_id = $('.tabbable .active').attr('id');
var idarr = select_id.split('_');
var id = idarr[1];
//alert(select_id);

pdata={};

pdata.id=id;
//pdata.day=$('#copy_day_'+id).val();
pdata.day=$('#copyday').val();
pdata.teacher_id = $('#teacher_id').val();


$.post(base_url+'students/copyall',{'pdata':pdata},function(data) {
		  
		  
		  if(data.status==1)
		  {
                      //$('#myModal-replicate').modal('toggle');
                      $('#succmsg').html('Sucessfully Copied ');
                      $('#successbtn').modal('show');
                      
                      
                      getthisroster(id);	
            
		 }
		 else
		 {
		 $('#errmsg').html('Failed Please Try Again ');
                      $('#errorbtn').modal('show');
			 //alert('Failed Please Try Again ');
		 
		 }

},'json');	

}		
function emptystudent()
{
var sstr='<select class="combobox" name="student_id" id="student_id">';
	 sstr+='<option value="">No Students Found</option></select>';
	  $('#student_id').replaceWith(sstr); 

}
function getthis(id)
{  
var page=1;
if(periods!=false)
{

$.each(periods,function(index,value){

var pr=value['period_id'];
$.get(base_url+"students/getstudentsdata/"+id+"/"+pr+"/"+page+"?num=" + Math.random(), function(msg){

var msgContainer = $("div.msgContainer_"+id+"_"+pr);

 msgContainer.html(msg);
			

});

});


}

}
//This function to get the roster for selected day
    function getthisroster(id)
    {  
        
        if($('#teacher_id').length==0){
           
        if(periods!=false)
        {
            $.each(periods,function(index,value){
                var pr=value['period_id'];
                $.get(base_url+"attendance/getstudentsdata1/"+id+"/"+pr+"?num=" + Math.random(), function(msg){
                var msgContainer = $("#period_"+pr);
                 msgContainer.html(msg);
                });
            });
        }
    } else if($('#teacher_id').length>0){
        
        if($('#teacher_id').val()!=''){
            
        if(periods!=false)
        {
            $.each(periods,function(index,value){
                var pr=value['period_id'];
                $.get(base_url+"attendance/getstudentsdata1/"+id+"/"+pr+"/"+$('#teacher_id').val()+"?num=" + Math.random(), function(msg){
                var msgContainer = $("#period_"+pr);
                 msgContainer.html(msg);
                });
            });
        }
    }
        
    }
    }
    
    $('#teacher_id').on('change',function(){
        var selectday = $('.tabbable .nav-tabs .active').attr('id');
        
        var idarr = selectday.split('_');
        
        getthisroster(idarr[1]);
        $('.widget-tabs').show();
    });
function students(id)
{
	pdata={};
	pdata.id=$('#grade').val();
	pdata.fname=$('#fname').val();
	pdata.lname=$('#lname').val();
    if(pdata.id!='')
	{
	var s_url='students/getstudents/';
     $.post(s_url,{'pdata':pdata},function(sresult) {
	//$.getJSON(s_url,function(sresult)	{
		var sstr='<select class="combobox" name="student_id" id="student_id" >';
		if(sresult.student!=false)
	{
	sstr+='<option value="">-Please Select-</option>';
	$.each(sresult.student, function(index, value) {
	sstr+='<option value="'+value['student_id']+'">'+value['firstname']+' '+value['lastname']+'('+value['student_number']+')</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='<option value="">No Students Found</option></select>';
	}
     $('#student_id').replaceWith(sstr); 
	},'json');
    }
	else
	{
	$('#errmsg').html('Sucessfully Copied ');
                      $('#errorbtn').modal('show');	
        //alert('Please Select Grade.');
	
	}

}
function studentadd()
{

$('#studentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'students/add_student',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtudentJsonadd 
    });

}		


function studentdelete(day,student_id)
{

		
		var p_url = base_url+'students/delete/'+student_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
                    $('#myModal-delete-student'+student_id).modal('toggle');
                    $('#succmsg').html('Student is successfully removed');
                      $('#successbtn').modal('show');
			getthisroster(day);
		}
		else
		{
                    $('#errmsg').html('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
                      $('#errorbtn').modal('show');
			//alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function teacherstudentdelete(day,student_id)
{

		
		var p_url = base_url+'students/delete/'+student_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
                    $('#myModal-delete-student'+student_id).modal('toggle');
                    $('#succmsg').html('Student is successfully removed');
                      $('#successbtn').modal('show');
			getthisroster(day);
		}
		else
		{
                    $('#errmsg').html('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
                      $('#errorbtn').modal('show');
			//alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}



function processtudentJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    getthis($('#day').val());
	$('#dialog').dialog('close');
	
  }
  $('#message').show();
}
function inactive(day,id)
{
$.post("students/changestatus/inactive",{ user_id:id } ,function(data)
 {
    if(data==0)    
	{
            $('#errmsg').html('Failed Please Try Again ');
                      $('#errorbtn').modal('show');
	  //alert('Failed Please Try Again');
	}
	else
	{
	 getthis(day);
	 //$('#status_'+id).html('<img src="'+site_url+'/images/home_off.jpg" style="cursor:pointer;" title="Active" onclick="active('+id+')" />');
	
	}
 });


}


function active(day,id)
{
  
$.post("students/changestatus/active",{ user_id:id } ,function(data)
 {
    if(data==0)    
	{
            $('#errmsg').html('Failed Please Try Again ');
                      $('#errorbtn').modal('show');
	  //alert('Failed Please Try Again');
	}
	else
	{
	  getthis(day);
	 //$('#status_'+id).html('<img src="'+site_url+'/images/home_on.jpg" style="cursor:pointer;" title="InActive" onclick="inactive('+id+')" />');
	
	}
 });



}
 