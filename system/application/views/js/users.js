$(document).ready(function() {
$("#dist_userform").validate({

rules: {

username:{
required: true,
maxlength:40
},
password:{
required: true
},
email:{
email: function(element){if($("#email").val()!='' ){ return true ;} else {return false;}},
maxlength:60
}

},
messages: {

username:{
required: "Please enter User Name"
},password:{required: "Please Enter password"}

}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("district/getdist_users/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#dist_userdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .dist_users li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
$.get("district/getdist_users/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#dist_user_add').click(function()
		{
			 
		dist_useradd();
		$('#message').hide();		
		$('#dist_user_id').val('');
		$('#username').val('');
		$('#password').val('');
		$('#email').val('');
		$('#dist_userupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_useradd" value="Add" onclick="dist_useradd()">');
		
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#dist_userform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_user/add_dist_user',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_userJson 
    });*/ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function dist_useradd()
{

$('#dist_userform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'district/add_dist_user',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_userJsonadd 
    });

}		

function dist_useredit(dist_user_id)
{
	dist_userupdate();
	$('#message').hide();
	var p_url='district/getdist_userinfo/'+dist_user_id;
    $.getJSON(p_url,function(result)
	{
			   
	
	$('#username').val(result.dist_user.username);
	$('#password').val('torvertex');
	$('#dist_user_id').val(result.dist_user.dist_user_id);
	$('#email').val(result.dist_user.email);
	$('#dist_useradd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_userupdate" value="Update" onclick="dist_userupdate()">');
		
	
	
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function dist_userdelete(dist_user_id)
{


	if(window.confirm("Are you sure you want to delete this user?")==false){
			return;
		}
		
		
		var p_url = 'district/delete_user/'+dist_user_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            $.get("district/getdist_users/"+page+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function dist_userupdate()
{

 $('#dist_userform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'district/update_dist_user',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_userJsonupdate 
    }); 



}

function procesdist_userJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    $.get("district/getdist_users/"+page+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
}
function procesdist_userJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    $.get("district/getdist_users/"+page+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
} 