$(document).ready(function() {
$("#schoolform").validate({

rules: {
school_name:{
required: true
},
school_type_id:{
required: true
},
username:{
required: true
},
password:{
required: true
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
school_name:{
required: "Please Enter School Name"
},
school_type_id:{
required: "Please Select School Type"
},
username:{
required: "Please Enter User Name"
},
password:{
required: "Please Enter Password"
},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("school/getschools/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#schooldetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .schools li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("school/getschools/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("school/getschools/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#school_add').click(function()
		{
			 
			schooladd(); 
		$('#message').hide();			
		$('#school_name').val('');
		$('#school_id').val('');
		$('#username').val('');
		$("select[name='school_type_id'] option[value='']").attr("selected", true);
		$('#password').val('');
	
	
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 var sstr='<select class="combobox" name="school_type_id" id="school_type_id"><option value="">-Select-</option></select>';
		$('#school_type_id').replaceWith(sstr);
		$("select[name='school_type_id'] option[value='']").attr("selected", true);
	 
	 });
	
		
		$('#schoolupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		//$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#schoolform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/add_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function schooladd()
{

$('#schoolform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/add_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonadd 
    });

}		

function schooledit(school_id)
{
	schoolupdate();
	$('#message').hide();
	var p_url='school/getschoolinfo/'+school_id;
    $.getJSON(p_url,function(result)
	{
	$("select[name='school_type_id'] option[value='']").attr("selected", true);		   
	$('#school_name').val(result.school.school_name);
	$('#school_id').val(result.school.school_id);
	$('#password').val('torvertex');
	$('#username').val(result.school.username);
	$("select[name='school_type_id'] option[value="+result.school.school_type_id+"]").attr("selected", true);
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.school.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.school.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.school.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.school.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.school.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	 var s_url='district/getDistrictsByschooltype/'+result.school.district_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="school_type_id" id="school_type_id" ><option value="">-Select-</option>';
		if(sresult.school_type!=false)
	{
	$.each(sresult.school_type, function(index, value) {
	sstr+='<option value="'+value['school_type_id']+'">'+value['tab']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#school_type_id').replaceWith(sstr); 
	 $("select[name='school_type_id'] option[value="+result.school.school_type_id+"]").attr("selected", true);
	});
	
	 
	 });
	$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schoolupdate" value="Update" onclick="schoolupdate()">');
	
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function schooldelete(school_id)
{


	if(window.confirm("Are you sure you want to delete this school?")==false){
			return;
		}
		
		
		var p_url = 'school/delete/'+school_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("school/getschools/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function schoolupdate()
{

 $('#schoolform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/update_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}

function processchoolJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
   var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("school/getschools/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("school/getschools/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 