$(document).ready(function() {
$("#dist_subjectform").validate({

rules: {
subject_name:{
required: true,
maxlength:80
},eld:{
required: true
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
subject_name:{
required: "Please Enter Subject Name"
},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_subject/getdist_subjects/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#dist_subjectdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .dist_subjects li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_subject/getdist_subjects/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_subject/getdist_subjects/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#dist_subject_add').click(function()
		{
			 
			dist_subjectadd(); 
		$('#message').hide();			
		$('#subject_name').val('');
		$('#dist_subject_id').val('');
		$("select[name='eld'] option[value='0']").attr("selected", true);
	
	
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 var sstr='<select class="combobox" name="dist_subject_type_id" id="dist_subject_type_id"><option value="">-Select-</option></select>';
		$('#dist_subject_type_id').replaceWith(sstr);
		$("select[name='dist_subject_type_id'] option[value='']").attr("selected", true);
	 
	 });
	
		
		$('#dist_subjectupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_subjectadd" value="Add" onclick="dist_subjectadd()">');
		//$('#dist_subjectadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_subjectadd" value="Add" onclick="dist_subjectadd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#dist_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_subject/add_dist_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_subjectJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function dist_subjectadd()
{

$('#dist_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_subject/add_dist_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_subjectJsonadd 
    });

}		

function dist_subjectedit(dist_subject_id)
{
	dist_subjectupdate();
	$('#message').hide();
	var p_url='dist_subject/getdist_subjectinfo/'+dist_subject_id;
    $.getJSON(p_url,function(result)
	{
	
	$('#subject_name').val(result.dist_subject.subject_name);
	$('#dist_subject_id').val(result.dist_subject.dist_subject_id);
	$("select[name='eld'] option[value="+result.dist_subject.eld+"]").attr("selected", true);
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.dist_subject.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.dist_subject.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.dist_subject.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.dist_subject.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.dist_subject.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	
	
	 
	 });
	$('#dist_subjectadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_subjectupdate" value="Update" onclick="dist_subjectupdate()">');
	
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function dist_subjectdelete(dist_subject_id)
{


	if(window.confirm("Are you sure you want to delete this dist_subject?")==false){
			return;
		}
		
		
		var p_url = 'dist_subject/delete/'+dist_subject_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_subject/getdist_subjects/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function dist_subjectupdate()
{

 $('#dist_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_subject/update_dist_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_subjectJsonupdate 
    }); 



}

function procesdist_subjectJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
   var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("dist_subject/getdist_subjects/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesdist_subjectJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("dist_subject/getdist_subjects/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 