	
	//Media start


	$(document).ready(function() {
	
	var options = {
		beforeSubmit:  mediashowRequest,
		success:       mediashowResponse,
		url:       base_url+'lesson_plan_material/mediaupload',  // your upload script
		dataType:  'json'
	};
	
	$('#mediaForm').submit(function() {
		document.getElementById('mediamessage').innerHTML = '';
		$(this).ajaxSubmit(options);
		return false;
	});
	
	jQuery.extend({
    handleError: function( s, xhr, status, e ) {
        // If a local callback was specified, fire it
        if ( s.error )
            s.error( xhr, status, e );
        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
        else if(xhr.responseText)
            console.log(xhr.responseText);
        var obj = jQuery.parseJSON( xhr.responseText );
        //console.log(obj);
        mediashowResponse(obj,'success');
    }
});
	
	var $mediamsgContainer = $("div#mediamsgContainer");


	//deafault load media
	mediaload();

	// onclick  pagination Load
	$('#mediamsgContainer .media li.active').live('click',function(){
	
	var school_id=$('#media_school_id').val();
	var mediasearch=$('#mediasearchvalue').val();
	
	if(mediasearch=='')
	{
	   mediasearch='empty';
	
	
	}
	if(school_id=='')
	{
	   school_id='empty';
	
	
	}
	
	  var page = $(this).attr('p');
	$('#mediapageid').val(page);
	
	  $.get("lesson_plan_material/getmedia/"+page+"/"+school_id+"/"+mediasearch+"?num=" + Math.random(), function(msg){
	
	$mediamsgContainer.html(msg);
	
	
	});
	
	
	
	
	
	
	});
	
	});
	
function mediashowRequest(formData, jqForm, options) {
	$("#mediaflash").show();
$("#mediaflash").fadeIn(400).html('Loading Update...');
	
var school_id = $("#media_school_id").val();
var subject_id = $("#subject_id").val();
var grade_id = $("#grade_id").val();

 if(school_id=='')
{

alert("Please Select Some School");
$("#mediaflash").fadeOut('slow');
return false;
}
else if(subject_id=='')
{

alert("Please Select Subejct");
$("#mediaflash").fadeOut('slow');
return false;
}
else if(grade_id=='')
{
alert("Please Select Grade");
$("#mediaflash").fadeOut('slow');
return false;

}
else
{ 

	return true;
	
}	
} 

function mediashowResponse(data, statusText)  {
	
	console.log(data);
	if (statusText == 'success') {
		
		
var school_id = $("#media_school_id").val();
var subject_id = $("#subject_id").val();
var grade_id = $("#grade_id").val();
var dataString = 'school_id=' + school_id+'&filename='+data.name+'&fileupload=' + data.img+'&subject_id=' + subject_id+'&grade_id=' + grade_id;

		if (data.img != '') {
			
			$.ajax({
type: "POST",
url: 	base_url+'lesson_plan_material/media_upload',
data: dataString,
cache: false,
success: function(html)
{
$("#mediaflash").fadeOut('slow');
	
		
		
		$("#mediafileToUpload").val('');
		 $("select[name='subject_id'] option[value='']").attr("selected", true);
		 $("select[name='grade_id'] option[value='']").attr("selected", true);		 
		
		mediaload();
		
		
  }    
 });
		} else {
			$("#mediaflash").fadeOut('slow');
			document.getElementById('mediamessage').innerHTML = data.error;
		
		}
	} else {
	$("#mediaflash").fadeOut('slow');
		document.getElementById('mediamessage').innerHTML = 'Unknown error!';
	}
}
function mediadelete(id)
{


	if(window.confirm("Are you sure you want to delete this file?")==false){
			return;
		}
		
		
		var p_url = 'lesson_plan_material/delete/'+id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $mediamsgContainer = $("div#mediamsgContainer");
var school_id=$('#media_school_id').val();
	var mediasearch=$('#mediasearchvalue').val();
	
	if(mediasearch=='')
	{
	   mediasearch='empty';
	
	
	}
	if(school_id=='')
	{
	   school_id='empty';
	
	
	}
	
	  var page = 1;
	$('#mediapageid').val(page);
	
	  $.get("lesson_plan_material/getmedia/"+page+"/"+school_id+"/"+mediasearch+"?num=" + Math.random(), function(msg){
	
	$mediamsgContainer.html(msg);
	
	
	});
	
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}
function mediaload()
{

    var page =1;
	var $mediamsgContainer = $("div#mediamsgContainer");
	var school_id=$('#media_school_id').val();
	var mediasearch=$('#mediasearchvalue').val();
	
	if(mediasearch=='')
	{
	   mediasearch='empty';
	
	
	}
	if(school_id=='')
	{
	   school_id='empty';
	
	
	}
	
	
	
	  $.get("lesson_plan_material/getmedia/"+page+"/"+school_id+"/"+mediasearch+"?num=" + Math.random(), function(msg){
	
	$mediamsgContainer.html(msg);
	$('#mediadetails').show();
	
	});
	$('#mediapageid').val(page);
	


}


//Media Upload End



