$(document).ready(function() {
$("#observationgroupform").validate({

rules: {
group_name:{
required: true
}
,
state_id:{
required: true
},
country_id:{
required: true
}
,district_id:{
required: true
}

},
messages: {
group_name:{
required: "Please enter group Name"


},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: " Please Select District"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observationgroup/getobservationgroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#observationgroupdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .observationgroups li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observationgroup/getobservationgroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#getobservationgroup').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observationgroup/getobservationgroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#observationgroup_add').click(function()
		{
			 
		observationgroupadd();
		$('#message').hide();		
		$('#group_name').val('');
		$('#group_id').val('');
		$('#description').val('');
		var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 });
		$('#observationgroupupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="observationgroupadd" value="Add" onclick="observationgroupadd()">');
		/*var s_url='district/getAlldistricts/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="district_id" id="district_id"><option value="">-Select-</option>';
	if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	str+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	});*/
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#observationgroupform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observationgroup/add_observationgroup',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesobservationgroupJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function observationgroupadd()
{

$('#observationgroupform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observationgroup/add_observationgroup',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesobservationgroupJsonadd 
    });

}		

function distedit(ob_id)
{
	observationgroupupdate();
	$('#message').hide();
	var p_url='observationgroup/getobservationgroupinfo/'+ob_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#group_name').val(result.observationgroup.group_name);
	$('#group_id').val(result.observationgroup.group_id);
	$('#description').val(result.observationgroup.description);
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.observationgroup.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.observationgroup.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.observationgroup.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.observationgroup.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.observationgroup.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	 
	
	 
	 });
	$('#observationgroupadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="observationgroupupdate" value="Update" onclick="observationgroupupdate()">');
		
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function distdelete(ob_id)
{


	if(window.confirm("Are you sure you want to delete this group?")==false){
			return;
		}
		
		
		var p_url = 'observationgroup/delete/'+ob_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
             var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observationgroup/getobservationgroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function observationgroupupdate()
{

 $('#observationgroupform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observationgroup/update_observationgroup',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesobservationgroupJsonupdate 
    }); 



}

function procesobservationgroupJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select_only(country_id);
district_all(state_id);

   $.get("observationgroup/getobservationgroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 
function procesobservationgroupJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select_only(country_id);
district_all(state_id);

   $.get("observationgroup/getobservationgroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
	
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 