$(document).ready(function() { 
$('#myModal-add-new').on('hidden.bs.modal',function(){
    $('#studentform1').find('#firstname').val('') ;
$('#studentform1').find('#lastname').val('') ;
$('#studentform1').find('#loginname').val('');
$('#studentform1').find('#passwordid').val('');
$('#studentform1').find('#grade').val('').trigger("liszt:updated");
$('#studentform1').find('#student_number').val('');
$('#studentform1').find('#student_number').val('') ;
$('#studentform1').find('#emailaddress').val('') ;
$('#studentform1').find('#addressid').val('') ;
$('#studentform1').find('#phone_number').val('') ;
});	
	

$.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    }, "Student Number must contain only letters, numbers.");

$("#studentform").validate({

rules: {
firstname:{
required: true,
maxlength:30
},
lastname:{
required: true,
maxlength:30
},
loginname:{
required: true,
maxlength:30
},
passwordid:{
required: true,
maxlength:30
},
/*emailaddress:{
required: true,
maxlength:30
},*/


grade:{
required: true
}
,
student_number:{
required: true,
alphanumeric: true,
maxlength:15
}
},
messages: {
firstname:{
required: "Please enter First Name"
},
lastname:{
required: "Please enter Last Name"
},
loginname:{
required: "Please enter User Name"
},
passwordid:{
required: "Please enter Password"
},
/*emailaddress:{
required: "Please enter Email Address"
},
*/
grade:{
required: "Please Select Grade"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
//$.get("attendance/getstudents/?num=" + Math.random(), function(msg){
//$msgContainer.html(msg);
//$('#studentdetails').show();
//loading_hide();
});

// onclick  pagination Load
$('#msgContainer .student li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);

$.get("parents/getstudents/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

			 
	$('#cancel').click( function() {
//	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

//});
function studentadd()
{

$.ajax({
        type: 'post',
    url: 'parents/add_student',
    data: $("#studentform1").serialize()

    })
  .done(function( data ) {
      processtudentJsonadd(data);
          });


}		

function studentedit(student_id)
{
	
	
    $.ajax({
    url: base_url+'parents/getstudentinfo/'+student_id,
    })
  .done(function( data ) {
      result = jQuery.parseJSON(data);
//      console.log(result.student.firstname);

        if (result.student!=null){
      $('#studentform2 #student_id').val(result.student.student_id);	
        $('#studentform2 #firstname').val(result.student.firstname);	
	$('#studentform2 #lastname').val(result.student.lastname);
        $('#studentform2 #loginname').val(result.student.UserName);
        $('#studentform2 #passwordid').val(result.student.pass);
        $('#studentform2 #emailaddress').val(result.student.email);
        $("#studentform2 select[name='grade'] option[value='"+result.student.grade+"']").attr("selected", true);
        
        var selectboxid = $("#studentform2 select[name='grade']").attr('id');
//        console.log(selectboxid);
        var selectid = $("#studentform2 select[name='grade'] option[value='"+result.student.grade+"']").attr("id");
        var splitarr = selectid.split('_');
        var gradeid = splitarr[1];
        console.log(gradeid);
        $(".chzn-results>li.result-selected").removeClass("result-selected");
        $(".chzn-results>li.highlighted").removeClass("highlighted");
        $('#'+selectboxid+'_chzn span').html($('#'+selectboxid+'_chzn_o_'+gradeid).html());
        $('#'+selectboxid+'_chzn_o_'+gradeid).addClass('result-selected highlighted');

        
        $('#studentform2 #address').val(result.student.address);
        $('#studentform2 #phone').val(result.student.phone);
        $('#studentform2 #student_number').val(result.student.student_number);
    }
        
  });
}
function studentdelete(student_id)
{


	if(window.confirm("Are you sure you want to delete this student?")==false){
			return;
		}
		
		
		var p_url = 'parents/delete_student/'+student_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            
			$.get("parents/getstudents/"+page+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function studentupdate()
{
data = $("#studentform2").serialize();
console.log(data);return false;
 $('#studentform2').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'parents/update_student',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtudentJsonupdate 
    }); 



}
function resetform(){
    $('#studentform2 #student_id').val('');
    $('#studentform2 #firstname').val('');	
	$('#studentform2 #lastname').val('');
        $('#studentform2 #loginname').val('');
        $('#studentform2 #passwordid').val('');
        $('#studentform2 #emailaddress').val('');

        $("#studentform2 select[name='grade']").attr("selected", false);
        $('#studentform2 #student_number').val('');
}
function updatestudent(){

    $.ajax({
        type: 'post',
    url: base_url+'parents/update_student',
    data: $("#studentform2").serialize()
    })
  .done(function( data ) {
      result = jQuery.parseJSON(data);
      if (result.status){

          var studentid = $('#studentform2 #student_id').val();
//          $('#row_'+studentid+' #tdstudentnumber').html('')
//          alert($("#studentform2 #studentnumber").val());

		  $("#studentform2 #firstname").val()
          $('#row_'+studentid+' #tdfirstname').html($("#studentform2 #firstname").val());
          $('#row_'+studentid+' #tdlastname').html($("#studentform2 #lastname").val());
          $('#row_'+studentid+' #tdgrade').html($("#studentform2 select[name='grade'] option:selected").text());
          $('#row_'+studentid+' #tdstudentnumber').html($("#studentform2 #student_number").val());

          
      }
          });
}

function updatestudent1(){

    $.ajax({
        type: 'post',
    url: base_url+'parents/update_student',
    data: $("#studentform2").serialize()
    })
  .done(function( data ) {
      result = jQuery.parseJSON(data);
      if (result.status){
		   $("#myModal-edit-student").modal("hide");
          var studentid = $('#studentform2 #student_id').val();
//          $('#row_'+studentid+' #tdstudentnumber').html('')
//          alert($("#studentform2 #studentnumber").val());
			location.reload();
		  $("#studentform2 #firstname").val()
          $('#row_'+studentid+' #tdfirstname').html($("#studentform2 #firstname").val());
          $('#row_'+studentid+' #tdlastname').html($("#studentform2 #lastname").val());
          $('#row_'+studentid+' #tdgrade').html($("#studentform2 select[name='grade'] option:selected").text());
          $('#row_'+studentid+' #tdstudentnumber').html($("#studentform2 #student_number").val());

          
      }
          });
}


function processtudentJsonadd(data)
{
if(data.status===0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 
	$.get("parents/getstudents/"+page+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		
//		$('#dialog').dialog('close');
		
     
    });
	//location.reload()
  }
  $('#message').show();
}
function processtudentJsonupdate(data)
{
    data = jQuery.parseJSON(data);
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     
	$.get("parents/getstudents/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);		
		$('#dialog').dialog('close');
      // window.location.reload();
    });
	
  }
  $('#message').show();
} 

$('#cancelupdate').click(function(){
    resetform();
});

$('.close').click(function() {
  //Code to be executed when close is clicked
  resetform();
});




// (function() {
// 
//            var bar = $('.bar');
//            var percent = $('.percent');
//            var status = $('#status');
// 
//            $('form').ajaxForm({
//                beforeSend: function() {
//                    status.empty();
//                    var percentVal = '0%';
//                    bar.width(percentVal)
//                    percent.html(percentVal);
//                },
//                uploadProgress: function(event, position, total, percentComplete) {
//                    var percentVal = percentComplete + '%';
//                    bar.width(percentVal)
//                    percent.html(percentVal);
//                },
//                success: function() {
//                    var percentVal = '100%';
//                    bar.width(percentVal)
//                    percent.html(percentVal);
//                },
//                complete: function(xhr) {
//                    status.html(xhr.responseText);
//                    $("#myModal-upload").modal("hide");
//                    
//                }
//            });
//        })();
        
        function deletestudent(student_id)
{
	
		var p_url = base_url+'parents/delete_student/'+student_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
                $('#row_'+student_id).hide();
//			var $msgContainer = $("div#msgContainer");
//            var page = $('#pageid').val();
//            
//			$.get("parents/getstudents/"+page+"?num=" + Math.random(), function(msg){
//			
//		   $msgContainer.html(msg);
//		   });
                    $('#successmsg').html('Successfully deleted.');
                    $('#successbtn').modal('show');
		}
		else
		{
//			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
                        $('#errmsg').html('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
                         $('#errorbtn').modal('show');
		}
		});

}

function addstudent()
{
    
$.ajax({
        type: 'post',
    url: base_url+'parents/add_student',
    data: $("#studentform1").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
//          alert("could not able to add new student , please try after some time");
        $('#errmsg').html("could not able to add new student, "+data.message);
          $('#errorbtn').modal('show');
      } else if (data.status==1){
          $('#successbtn').modal('show');
//		location.reload();  
          location.reload(true);
      }
//      processtudentJsonadd(data);
    });


}



$('#saveupdate').on('click',function(){
    updatestudent();
	

});

$('#student_add').on('click',function(){
//    alert($('#studentform1').find('#firstname').val());
    var errormsg = '';
    var validated = true;
    if($('#studentform1').find('#firstname').val()==''){
        errormsg += 'Please enter student First Name.<br />';
        validated = false;
    }
    if($('#studentform1').find('#lastname').val()==''){
        errormsg += 'Please enter student Last Name.<br />';
        validated = false;
    }
    if($('#studentform1').find('#loginname').val()==''){
        errormsg += 'Please enter student Login Name.<br />';
        validated = false;
    }
    if($('#studentform1').find('#passwordid').val()==''){
        errormsg += 'Please enter student Password.<br />';
        validated = false;
    }
    if($('#studentform1').find('#grade').val()==''){
        errormsg += 'Please select student Grade.<br />';
        validated = false;
    }
    if($('#studentform1').find('#student_number').val()==''){
        errormsg += 'Please select student ID.<br />';
        validated = false;
    }
    if(validated==false){
        $('#errmsg').html(errormsg);
        $('#errorbtn').modal('show');
        return false;
    }else{
    addstudent();
    }

});

