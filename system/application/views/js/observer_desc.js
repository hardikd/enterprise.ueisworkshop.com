$(document).ready(function() {
$("#planform").validate({

rules: {
desc:{
required: true
}

},
messages: {

desc:{
required: "Please Enter Description"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("observerview/getgoalplans/?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#plandetails').show();
loading_hide();
});






		
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
		

function planedit(plan_id)
{
	planupdate();
	$('#message').hide();
	var p_url='observerview/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
			 
	$('#tab').val(result.goalplan.tab);
	$('#desc').val(result.goalplan.school_description);
	$('#plan_id').val(result.goalplan.goal_plan_id);
	 
	
	 
	 });
	 
	 
	$('#planadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planupdate" value="Update" onclick="planupdate()">');
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}


function planupdate()
{

 $('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observerview/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}


function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    
     var page = 1;
     
	
$.get("observerview/getgoalplans/?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 