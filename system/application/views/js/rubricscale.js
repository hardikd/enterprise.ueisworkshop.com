$(document).ready(function() {
$("#rubricscaleform").validate({

rules: {
scale_name:{
required: true
},
state_id:{
required: true
},
country_id:{
required: true
},district_id:{
required: true
}

},
messages: {
scale_name:{
required: "Please enter scale Name"


},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: " Please Select District"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("rubricscale/getrubricscales/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#rubricscaledetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .rubricscales li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("rubricscale/getrubricscales/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#getrubiscale').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("rubricscale/getrubricscales/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});


$('#rubricscale_add').click(function()
		{
			 
		rubricscaleadd();
		$('#message').hide();		
		$('#scale_name').val('');
		$('#scale_id').val('');
		$('#description').val('');
		$('#rubricscaleupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="rubricscaleadd" value="Add" onclick="rubricscaleadd()">');
		var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 });
		$("#dialog").dialog({
			modal: true,
           	height: 550,
			width: 700
			});
				
		
		
		}); 
		
/*$('#rubricscaleform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'rubricscale/add_rubricscale',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesrubricscaleJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function rubricscaleadd()
{

$('#rubricscaleform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'rubricscale/add_rubricscale',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesrubricscaleJsonadd 
    });

}		

function distedit(ob_id)
{
	rubricscaleupdate();
	$('#message').hide();
	var p_url='rubricscale/getrubricscaleinfo/'+ob_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#scale_name').val(result.rubricscale.scale_name);
	$('#scale_id').val(result.rubricscale.scale_id);
	$('#description').val(result.rubricscale.description);
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.rubricscale.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.rubricscale.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.rubricscale.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.rubricscale.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.rubricscale.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	 
	
	 
	 });
	$('#rubricscaleadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="rubricscaleupdate" value="Update" onclick="rubricscaleupdate()">');
		
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 550,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function distdelete(ob_id)
{


	if(window.confirm("Are you sure you want to delete this scale?")==false){
			return;
		}
		
		
		var p_url = 'rubricscale/delete/'+ob_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
           var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("rubricscale/getrubricscales/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function rubricscaleupdate()
{

 $('#rubricscaleform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'rubricscale/update_rubricscale',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesrubricscaleJsonupdate 
    }); 



}

function procesrubricscaleJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
    
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select_only(country_id);
district_all(state_id);
$.get("rubricscale/getrubricscales/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 
function procesrubricscaleJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
     var page = 1;
   var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select_only(country_id);
district_all(state_id);
$.get("rubricscale/getrubricscales/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 