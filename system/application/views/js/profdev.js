$(document).ready(function() {
//var base_url=base_url;

$("#profform").validate({

rules: {
name:{
required: true
}
,
link:{
required: true
},
desc:{
required: true
}


},
messages: {
name:{
required: "Please Enter  Name"


},
link:{
required: "Please Enter a link"
},
desc:{
required: "Please Enter Desc"
}
}
,
errorElement:"div" 

});




		
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});

function add(id)
		{
			// alert(id);
		observationgroupadd();
		$('#message').hide();		
		$('#name').val('');
		$('#link').val('');
		$('#desc').val('');
		$('#group_id').val(id);
		
		$('#observationgroupupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="observationgroupadd" value="Add" onclick="observationgroupadd()">');
		
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}
		
		
		function show(id)
		{
		  var p_url='observationgroup/getvideoinfo/'+id;
    $.getJSON(p_url,function(result)
	{
			   
	
	var str="<td id='avideo'>"+result.video.descr+"</td>";
	
	
	
	$('#avideo').replaceWith(str);
		
	}); 
		
		 $("#watch").dialog({
			modal: true,
           	height: 450,
			width: 700,
			 close: function(){
            var str="<td id='avideo'></td>";
				$('#avideo').replaceWith(str);
				}
			});
		
		
		
		
		}
function observationgroupadd()
{

$('#profform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observationgroup/add_video',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   process
    });

}		

function distedit(ob_id)
{
	observationgroupupdate();
	$('#message').hide();
	var p_url='observationgroup/getvideoinfo/'+ob_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#name').val(result.video.name);
	$('#link').val(result.video.link);
	$('#desc').val(result.video.descr);
	$('#prof_dev_id').val(result.video.prof_dev_id);
	
	$('#observationgroupadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="observationgroupupdate" value="Update" onclick="observationgroupupdate()">');
		
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function distdelete(ob_id)
{


	if(window.confirm("Are you sure you want to delete this Video?")==false){
			return;
		}
		
		
		var p_url = 'observationgroup/delete_video/'+ob_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		  
		 var id=$('#group_id').val();
	location= base_url+"index.php/observationgroup/profdev/"+id; 
			
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function observationgroupupdate()
{

 $('#profform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observationgroup/update_video',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   process 
    }); 



}

function process(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    $('#dialog').dialog('close');
	var gid=$('#group_id').val();
	location= base_url+'index.php/observationgroup/profdev/'+gid; 
	
	
  }
  $('#message').show();
} 
