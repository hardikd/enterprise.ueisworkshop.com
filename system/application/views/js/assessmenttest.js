$(document).ready(function() {

$("#teacherform").validate({

rules: {
firstname:{
required: true,
maxlength:40
},
username:{
required: true,
maxlength:40
},
password:{
required: true
},
school_id:{required: true},
email:{
email: function(element){if($("#email").val()!='' ){ return true ;} else {return false;}},
maxlength:60
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
firstname:{
required: "Please enter first Name"
},
username:{
required: "Please enter user Name"
}
,
password:{
required: "Please enter password"
},
school_id:{required: "Please select school"},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
var school_id=$('#school').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
//$.get("teacher/getteachers/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#teacherdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .teachers li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
var school_id=$('#school').val();
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool').click(function(){
loading_show(); 
var page = 1;
var school_id=$('#school').val();
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#teacher_add').click(function()
		{
		teacheradd();	 
		$('#message').hide();	 		
		$('#firstname').val('');
		$('#lastname').val('');
        $('#username').val('');
        $('#password').val('');				
		$('#teacher_id').val('');
		$('#email').val('');
		$('#teacherupdate').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacheradd" value="Add" onclick="teacheradd()">');
		//$('#teacheradd').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacheradd" value="Add" onclick="teacheradd()">');
		var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="school_id" id="school_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#school_id').replaceWith(str); 
	 $("select[name='school_id'] option[value='']").attr("selected", true);
	
	 
	 });
		$("#dialog").dialog({
			modal: true,
           	height: 500,
			width: 400
			});
				
		
		
		}); 
		
/*$('#teacherform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher/add_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacherJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function teacheradd()
{

$('#teacherform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher/add_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacherJsonadd 
    });

}		

function assessmentedit(id)
{
	alert(id);
	
	$.ajax({
		url:'assessTest/getassessment?tag='+id,
		success:function(result){alert(result);}
		
		});	
	

}
function teacherdelete(teacher_id)
{


	if(window.confirm("Are you sure you want to delete this teacher?")==false){
			return;
		}
		
		
		var p_url = 'teacher/delete/'+teacher_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
var school_id=$('#school').val();
var page = $('#pageid').val();
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){          
			
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function teacherupdate()
{

 $('#teacherform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher/update_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacherJsonupdate 
    }); 



}

function procesteacherJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
var school_id=$('#school_id').val();
 $("select[name='countries'] option[value="+country_id+"]").attr("selected", true);
states_select(country_id);
district_all(state_id);
			$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){    
    
		$msgContainer.html(msg);
		$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesteacherJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
    var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
var school_id=$('#school_id').val();
 $("select[name='countries'] option[value="+country_id+"]").attr("selected", true);
states_select(country_id);
district_all(state_id);
			$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){    
	
		$msgContainer.html(msg);
		$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 