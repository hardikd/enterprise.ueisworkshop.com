$(document).ready(function() {
$("#periodform").validate({

rules: {
start_time:{
required: true
},
end_time:{
required: true
},
school_id:{required: true}

},
messages: {
start_time:{
required: "Please Select Start Time"
},
end_time:{
required: "Please Select End Time"
}
,
school_id:{required: "Please select school"}
}
,
errorElement:"div" 

});

$('#start_time').timepicker({
   ampm: true,
   timeFormat:'hh:mm TT'
 });
 
 $('#end_time').timepicker({
 ampm: true,
 timeFormat:'hh:mm TT'
 });
 
 
var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var school_id=$('#school').val();
$.get("period/getperiods/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#perioddetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .periods li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
var school_id=$('#school').val();
$('#pageid').val(page);

$.get("period/getperiods/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool').click(function(){
loading_show(); 
var page = 1;
var school_id=$('#school').val();
$('#pageid').val(page);

$.get("period/getperiods/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#period_add').click(function()
		{
		periodadd();	 
		$('#message').hide();	 		
		$('#start_time').val('12:00 AM');
		$('#end_time').val('12:00 AM');       
		$('#period_id').val('');
		
		$('#periodupdate').replaceWith('<button data-dismiss="modal" type="submit" name="submit" id="periodadd" value="Add" onclick="periodadd()" title="Add New" class="btn btn-success"><i class="icon-plus"></i> Add Period</button>');
		//$('#periodadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="periodadd" value="Add" onclick="periodadd()">');
		
		
		
		
		$("#dialog").dialog({
			modal: true,
           	height: 400,
			width: 450
			});
				
		
		
		}); 
		
/*$('#periodform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'period/add_period',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesperiodJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function periodadd()
{

$('#periodform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'period/add_period',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesperiodJsonadd 
    });

}		

function periodedit(period_id)
{
	periodupdate();
	$('#message').hide();
	var p_url='period/getperiodinfo/'+period_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#start_time').val(result.period.start_time);
	$('#end_time').val(result.period.end_time);  
	$('#period_id').val(result.period.period_id);
	
	
	$('#periodadd').replaceWith('<button data-dismiss="modal" type="submit" name="submit" id="periodupdate" value="Update" onclick="periodupdate()" class="btn btn-success"><i class="icon-plus"></i>Update</button>');
		
	
	
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 400,
			width: 450
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function perioddelete(period_id)
{


	if(window.confirm("Time table already created on these Periods will be deleted?")==false){
			return;
		}
		
		
		var p_url = 'period/delete/'+period_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var school_id=$('#school').val();
			$.get("period/getperiods/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function periodupdate()
{

 $('#periodform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'period/update_period',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesperiodJsonupdate 
    }); 



}

function procesperiodJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 var school_id=$('#school_id').val();
			$.get("period/getperiods/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		//$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesperiodJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var school_id=$('#school_id').val();
	$.get("period/getperiods/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		//$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 