$(document).ready(function() {
$("#observerform").validate({

rules: {
observer_name:{
required: true,
maxlength:40
},
username:{
required: true,
maxlength:40
},
password:{
required: true
},
school_id:{required: true},
email:{
email: function(element){if($("#email").val()!='' ){ return true ;} else {return false;}},
maxlength:60
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
observer_name:{
required: "Please enter observer Name"
},
username:{
required: "Please enter User Name"
},password:{required: "Please Enter password"}
,school_id:{required: "Please select school"},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observer/getobservers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#observerdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .observers li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observer/getobservers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observer/getobservers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#observer_add').click(function()
		{
			 
		observeradd();
		$('#message').hide();		
		$('#observer_name').val('');
		$('#observer_id').val('');
		$('#username').val('');
		$('#password').val('');
		$('#email').val('');
		$('#observerupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="observeradd" value="Add" onclick="observeradd()">');
		var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="school_id" id="school_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#school_id').replaceWith(str); 
	 $("select[name='school_id'] option[value='']").attr("selected", true);
	
	 
	 });
	 
	 
		
		$("#dialog").dialog({
			modal: true,
           	height: 500,
			width: 700
			});
				
		
		
		}); 
		
/*$('#observerform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observer/add_observer',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesobserverJson 
    });*/ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function observeradd()
{

$('#observerform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observer/add_observer',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesobserverJsonadd 
    });

}		

function observeredit(observer_id)
{
	observerupdate();
	$('#message').hide();
	var p_url='observer/getobserverinfo/'+observer_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#observer_name').val(result.observer.observer_name);
	$('#username').val(result.observer.username);
	$('#password').val('torvertex');
	$('#email').val(result.observer.email);
	$('#observer_id').val(result.observer.observer_id);
	$('#observeradd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="observerupdate" value="Update" onclick="observerupdate()">');
		
	
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.observer.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.observer.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.observer.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.observer.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id" onchange="school_type_all(this.value)"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.observer.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	 var s_url='school/getschoolbydistrict/'+result.observer.district_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="school_id" id="school_id" ><option value="">-Select-</option>';
		if(sresult.school!=false)
	{
	$.each(sresult.school, function(index, value) {
	sstr+='<option value="'+value['school_id']+'">'+value['school_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#school_id').replaceWith(sstr); 
	 $("select[name='school_id'] option[value="+result.observer.school_id+"]").attr("selected", true);
	});
	
	 
	 });
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 500,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function observerdelete(observer_id)
{


	if(window.confirm("Are you sure you want to delete this observer?")==false){
			return;
		}
		
		
		var p_url = 'observer/delete/'+observer_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
          var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("observer/getobservers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function observerupdate()
{

 $('#observerform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'observer/update_observer',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesobserverJsonupdate 
    }); 



}

function procesobserverJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
   var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
 $("select[name='countries'] option[value="+country_id+"]").attr("selected", true);
states_select(country_id);
district_all(state_id);
	
$.get("observer/getobservers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
}
function procesobserverJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
   var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
$("select[name='countries'] option[value="+country_id+"]").attr("selected", true);
states_select(country_id);
district_all(state_id);
	
$.get("observer/getobservers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
} 