$(document).ready(function() {
$("#selectdate").datepicker({ dateFormat: 'mm-dd-yy',changeYear: true
		 });
$("#teacherplan").validate({

rules: {
start:{
required: true
},
end:{
required: true
}
},
messages: {
start:{
required: "Please Select Start time"
},
end:{
required: "Please Select End time"
}

}
,
errorElement:"div" 

});

$("#teacherplanpending").validate({

rules: {
start:{
required: true
},
end:{
required: true
}
},
messages: {
start:{
required: "Please Select Start time"
},
end:{
required: "Please Select End time"
}

}
,
errorElement:"div" 

});



/*$('#start').timepicker({
   ampm: true
 });
 
 $('#end').timepicker({
 ampm: true
 });*/
 
});

function addplan(date)
{
 
$('#message').hide();
$('#date').val(date);
$('#standard').val('');
$('#diff_instruction').val('');
$('#start').val('12:00 am');
$('#end').val('12:00 am');
$('.newst').hide();
var gradestr='<table id="material" border="0" style="float: left;"><tr><td></td></tr><tr><td></td></tr></table>';
$('#material').replaceWith(gradestr); 

	var gradestr='<select class="combobox" name="grade" id="grade" ><option value="">-Please Select-</option></select>';
	 $('#grade').replaceWith(gradestr); 
	var strandstr='<select class="combobox" name="strand" id="strand" ><option value="">-Please Select-</option></select>';
	 $('#strand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="standards" id="standards" ><option value="">-Please Select-</option></select>';
	 $('#standards').replaceWith(standardstr); 
	  var subjectstr='<select class="combobox" name="subject_id" id="subject_id" onchange="selectgrade(1,this.value)" ><option value="">-Please Select-</option></select>';
	 
	 $('#subject_id').replaceWith(subjectstr); 
	$('#period_id').val('');	
	$('#standard_id').val('');
	$('#standarddata_id').val('');
	 $('#standarddata').val('');$('#standarddisplay').val(''); 
	 $('.oldst').show();
if(arrlessonplans!=false){
		 var tablehtml='';
		for(var i=0;i<arrlessonplans.length;i++){
		tablehtml='';
		 tablehtml += "<tr id='"+arrlessonplans[i].lesson_plan_id+"' class='lessonplan' >";
			
			tablehtml += "<td valign='top' class='style1'>	<font color='red'>*</font>"+arrlessonplans[i].tab+":	</td><td valign='top' ><select name='"+arrlessonplans[i].lesson_plan_id+"' onchange='selectgrade("+arrlessonplans[i].lesson_plan_id+",this.value)' id='"+arrlessonplans[i].lesson_plan_id+"'> <option value=''>-Please Select-</option>";
				
				for(var j=0;j<arrlessonplansub.length;j++)
				{
				 if(arrlessonplans[i].lesson_plan_id==arrlessonplansub[j].lesson_plan_id) {
				 tablehtml += "<option value='"+arrlessonplansub[j].lesson_plan_sub_id+"'>"+arrlessonplansub[j].subtab+"</option>";
				 }
				  
				 } 
				
				tablehtml +="</select></td>";
				
			tablehtml +="</tr>";
			
			$('#'+arrlessonplans[i].lesson_plan_id).replaceWith(tablehtml);
		
		}
		
		
		}
$('#teacheradd').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacheradd" value="Add" >');

$("#dialog").dialog({
			modal: true,
           	height: 600,
			width:950
			});
			
			$("#dialog").dialog('option', 'title', 'Date:'+date);
$('#teacherplan').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacherplan/add_plan',
		
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   planadd
    });

$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
}
function selectgrade(id,subject_id,grade,st)
{


  if(id==1)
  {
 $('.newst').hide();
	var gradestr='<select class="combobox" name="grade" id="grade" ><option value="">-Please Select-</option></select>';
	// $('#grade').replaceWith(gradestr); 
	var strandstr='<select class="combobox" name="strand" id="strand" ><option value="">-Please Select-</option></select>';
	 $('#strand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="standards" id="standards" ><option value="">-Please Select-</option></select>';
	 $('#standards').replaceWith(standardstr); 
	 $('#standarddata').val('');$('#standarddisplay').val(''); 
	 $('#standard_id').val('');
	 
	 
	 
	 
	var g_url='teacherplan/getActiveGrades/'+subject_id+'/'+grade;
    $.getJSON(g_url,function(result)
	{
	 
	var eld=2;
	if(result.grades!=false)
	{
	
	gradestr='';
	$.each(result.grades, function(index, value) {
	if(value['grade_id']==grade)
	{
	
	if(st==0)
	{
	selectstrand(value['grade_id'],value['standard_id']);
	}
	//gradestr+='<span id="grade_display">'+value['grade']+'</span>';
	}
	$('#standard_id').val(value['standard_id']);
	
	 eld=value['eld'];
	
	
	
	});
	
	 //$('#grade_display').replaceWith(gradestr); 
	 
	 var g_url='teacherplan/getGrades/';
    $.getJSON(g_url,function(resultgrade)
	{
	
	if(resultgrade.grades!=false)
	{
	
	$.each(resultgrade.grades, function(gradeindex, gradevalue) {
	if(gradevalue['grade_id']==grade)
	{
	gradestr+='<span id="grade_display">'+gradevalue['grade_name']+'</span>';	
	$('#grade_display').replaceWith(gradestr); 
	}
	});
	
    }
	 
	  
	 
	});
	//alert(gradestr);
	 
	
    }
	else
	{
     var gg='';
	 //selectmaterial(subject_id,grade);
	 gradestr='';
	 $('#standard_id').val('');
	
	var g_url='teacherplan/getGrades/';
    $.getJSON(g_url,function(resultgrade)
	{
	
	if(resultgrade.grades!=false)
	{
	
	$.each(resultgrade.grades, function(gradeindex, gradevalue) {
	if(gradevalue['grade_id']==grade)
	{
	gg+='<span id="grade_display">'+gradevalue['grade_name']+'</span>';	
	}
	});
	
    }
	 
	 gg+='';
	  $('#grade_display').replaceWith(gradestr+gg); 
	});
	
	
	
	 
	}
   // console.log(gradestr);
	
	 
	 //$("select[name='grade'] option[value='']").attr("selected", true);
	 
	 if(eld==0)
	 {
	    $('.eldstyle').replaceWith('<span class="eldstyle">Common Core</span>');
	 }
	 else if(eld==1)
	 {
	   $('.eldstyle').replaceWith('<span class="eldstyle">ELD</span>');
	 
	 }
	 else
	 {
	   $('.eldstyle').replaceWith('<span class="eldstyle"></span>');
	 
	 }
	 if(result.grades!=false)
	{
	  $('.newst').show();
	  $('.oldst').hide();
	
	}
	else
	{
	 $('.newst').hide();
	$('.oldst').show();
	
	}
	 
	 });
	 
  
  }

}
function selectstrand(grade,standard_id)
{	
	 //selectmaterial($('#subject_id').val(),grade);
	 //$("select[name='grade'] option[value='"+grade+"']").attr("selected", true);
	 var strandstr='<select class="combobox" name="strand" id="strand" ><option value="">-Please Select-</option></select>';
	 $('#strand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="standards" id="standards" ><option value="">-Please Select-</option></select>';
	 $('#standards').replaceWith(standardstr); 
	 $('#standarddata').val('');$('#standarddisplay').val(''); 
	 if(standard_id==0)
		{	
			standard_id=$('#standard_id').val();
		}
	
	 
	 
	var g_url='teacherplan/getstrand/'+grade+'/'+standard_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<select class="combobox" name="strand" id="strand" onchange="selectstandard(this.value,0,0)"><option value="">-Please Select-</option>';
	if(result.strands!=false)
	{
	
	$.each(result.strands, function(index, value) {
	gradestr+='<option value="'+value['strand']+'">'+value['strand']+'</option>';
	
	});
	gradestr+='</select>';
    }
	else
	{
     
	 gradestr+='</select>';
	}
     $('#strand').replaceWith(gradestr); 
	 
	 $("select[name='strand'] option[value='']").attr("selected", true);
	 
	 
	 
	 });
	 
  
  

}


function selectstandard(strand,standard_id,grade)
{	
	
	$('#comdomain').val(strand);
//	 var standardstr='<select class="combobox" name="standards" id="standards" ><option value="">-Please Select-</option></select>';
//	 $('#standards').replaceWith(standardstr); 
//	 $('#standarddata').val('');$('#standarddisplay').val(''); 
	 if(standard_id==0)
	 {
		 standard_id=$('#standard_id').val();
	 }
	if(grade==0)
	 {
		 grade=$('#grade').val();;
	 }	 
	 
	  pdata={};
			pdata.strand=strand;
			pdata.grade=grade;
			pdata.standard_id=standard_id;
		   $.post(base_url+'teacherplan/getstandard',{'pdata':pdata},function(result) 
		   
		   
	 
	//var g_url='teacherplan/getstandard/'+strand+'/'+grade+'/'+standard_id;
   // $.getJSON(g_url,function(result)
	{
	var gradestr='<div  name="standards" id="standards"  style="padding-left:10px;height:200px; background-color: #F6FFFF;   overflow: scroll;    border: 1px solid #70B8BA;    color: #02AAD2; "  >';
	//var gradestr='<select class="combobox" name="standards" id="standards" size="4" style="height:155px;width:500px; "  onchange="selectst(this.value)"><option value="">-Please Select-</option>';
	if(result.standard!=false)
	{
	
	$.each(result.standard, function(index, value) {
	
	//gradestr+='<option value="'+value['standard']+'">'+value['standard']+'</option>';
	var t='"'+value['standard']+'"';
	
	gradestr+="<span style='cursor:pointer;'  onclick='selectst("+t+","+value['standarddata_id']+");'>"+value['standard']+"</span><br/><hr style='color: #02AAD2;' >";
	
	});
	gradestr+='</div>';
    }
	else
	{
     
	 gradestr+='</div>';
	}
     $('#standards').replaceWith(gradestr); 
	 
	// $("select[name='standards'] option[value='']").attr("selected", true);
	 
	 
	 
	 },'json');
	 
  
  

}

function selectst(v,s)
{
$('#standardeledesc').val(v); 
$('#standarddisplay').val(v);
$('#gol_standard').val(v); 
$('#standarddata_id').val(s); 

}
function addplanpending(date,start,end,lesson)
{
$("ul.subtabs li").removeClass("active"); //Remove any "active" class
 $(".sub_tab_content").hide(); //Hide all content
	$("ul.subtabs li:first").addClass("active").show(); //Activate first tab
	$(".sub_tab_content:first").show(); //Show first tab content
$('#pendingmessage').hide();
$('#pendingdate').val(date);
$('#pendingstart').val('12:00 am');
$('#pendingend').val('12:00 am');
//$('#pendingstart').val(start);
$('#pending_standard').val('');
$('#pending_diff_instruction').val('');
$('#lesson_week_plan_id_pending').val(lesson);
//$('#pendingend').val(end);

$('.newst').hide();
var gradestr='<table id="pendingmaterial" border="0" style="float: left;"><tr><td></td></tr><tr><td></td></tr></table>';
$('#pendingmaterial').replaceWith(gradestr); 
	var gradestr='<select class="combobox" name="pendinggrade" id="pendinggrade" ><option value="">-Please Select-</option></select>';
	 $('#pendinggrade').replaceWith(gradestr); 
	var strandstr='<select class="combobox" name="pendingstrand" id="pendingstrand" ><option value="">-Please Select-</option></select>';
	 $('#pendingstrand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="pendingstandards" id="pendingstandards" ><option value="">-Please Select-</option></select>';
	 $('#pendingstandards').replaceWith(standardstr); 
	
 var subjectstr='<select class="combobox" name="pendingsubject_id" id="pendingsubject_id" onchange="pendingselectgrade(1,this.value)" ><option value="">-Please Select-</option></select>';
	 
	 $('#pendingsubject_id').replaceWith(subjectstr); 
	$('#pendingperiod_id').val('');
	
	
	$('#pendingstandard_id').val('');
	 $('#pendingstandarddata').val('');  $('#pending_standarddisplay').val(''); 
	 $('.oldst').show();
	 
if(arrlessonplans!=false){
		 var tablehtml='';
		for(var i=0;i<arrlessonplans.length;i++){
		tablehtml='';
		 tablehtml += "<tr id='pending"+arrlessonplans[i].lesson_plan_id+"' class='lessonplan' >";
			
			tablehtml += "<td valign='top' class='style1'>	<font color='red'>*</font>"+arrlessonplans[i].tab+":	</td><td valign='top' ><select name='pending"+arrlessonplans[i].lesson_plan_id+"' onchange='pendingselectgrade("+arrlessonplans[i].lesson_plan_id+",this.value)' id='pending"+arrlessonplans[i].lesson_plan_id+"'> <option value=''>-Please Select-</option>";
				
				for(var j=0;j<arrlessonplansub.length;j++)
				{
				 if(arrlessonplans[i].lesson_plan_id==arrlessonplansub[j].lesson_plan_id) {
				 tablehtml += "<option value='"+arrlessonplansub[j].lesson_plan_sub_id+"'>"+arrlessonplansub[j].subtab+"</option>";
				 }
				  
				 } 
				
				tablehtml +="</select></td>";
				
			tablehtml +="</tr>";
			
			$('#pending'+arrlessonplans[i].lesson_plan_id).replaceWith(tablehtml);
		
		}
		
		
		}
		if(custom_diff!=false)
		{
		for(var i=0;i<custom_diff.length;i++){
			$('#p_c_'+custom_diff[i].custom_differentiated_id).val('');
		
		}
		
		
		}
$('#teacheraddpending').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacheraddpending" value="Add" >');
$("#pendingdialog").dialog({
			modal: true,
           	height: 600,
			width:950
			});
			
			$("#pendingdialog").dialog('option', 'title', 'Date:'+date);
$('#teacherplanpending').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacherplan/add_plan_pending',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   planaddpending
    });

$('#pendingcancel').click( function() {
	$('#pendingdialog').dialog('close');
	});	
}
function selectsubject(period_id,s)
{
  var elt = document.getElementById('period_id');
  var date=  document.getElementById('date').value;
  var gradestr='<table id="material" border="0" style="float: left;"><tr><td></td></tr><tr><td></td></tr></table>';
$('#material').replaceWith(gradestr); 
  //alert(date);
    /*if (elt.selectedIndex == 0)
	{
        $('#start').val('12:00 am');
		$('#end').val('12:00 am');
		return null;
	}	

    var times= elt.options[elt.selectedIndex].text;
	
 var t=times.split("-");
 
 var ts=t[0].split(" ");
 var sa=ts[1].toLowerCase();
 $('#start').val(ts[0]+' '+sa);
 
 var s1=t[1].split(" ");
 var sa1=s1[1].toLowerCase();
 $('#end').val(s1[0]+' '+sa1);*/

 

var g_url='teacherplan/getsubjects/'+period_id+'/'+date;
    $.getJSON(g_url,function(result)
	{
	var gradestr='';
	if(result.subjects!=false)
	{
	
	$.each(result.subjects, function(index, value) {
	if(value['subject_id']==s)
	{
	gradestr+='<span id="subject_display">'+value['subject_name']+'</span>';
	}
	
	});
	
    }
	else
	{
     
	 gradestr+='';
	}
      $('#subject_display').replaceWith(gradestr); 
	 $('#subject_id').val(s); 
	 
	 
	 
	 });
	 
	 

}
function pendingselectsubject(period_id,s)
{
  var elt = document.getElementById('pendingperiod_id');
  var date=  document.getElementById('pendingdate').value;
  
  var gradestr='<table id="pendingmaterial" border="0" style="float: left;"><tr><td></td></tr><tr><td></td></tr></table>';
$('#pendingmaterial').replaceWith(gradestr); 
  //alert(date);
    if (elt.selectedIndex == 0)
	{
        $('#pendingstart').val('12:00 am');
		$('#pendingend').val('12:00 am');
		return null;
	}	

    var times= elt.options[elt.selectedIndex].text;
	
 var t=times.split("-");
 $('#pendingstart').val(t[0]);
$('#pendingend').val(t[1]);

var g_url='teacherplan/getsubjects/'+period_id+'/'+date;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<select class="combobox" name="pendingsubject_id" id="pendingsubject_id" onchange="pendingselectgrade(1,this.value)" ><option value="">-Please Select-</option>';
	if(result.subjects!=false)
	{
	
	$.each(result.subjects, function(index, value) {
	gradestr+='<option value="'+value['subject_id']+'">'+value['subject_name']+'</option>';
	
	});
	gradestr+='</select>';
    }
	else
	{
     
	 gradestr+='</select>';
	}
     $('#pendingsubject_id').replaceWith(gradestr); 
	 if(s==0)
	 {
	 $("select[name='pendingsubject_id'] option[value='']").attr("selected", true);
	 }
	 else
	 {
	 $("select[name='pendingsubject_id'] option[value='"+s+"']").attr("selected", true);
	 
	 }
	 
	 
	 });
	 
	 

}
function pendingselectgrade(id,subject_id)
{


  if(id==1)
  {
 $('.newst').hide();
	var gradestr='<select class="combobox" name="pendinggrade" id="pendinggrade" ><option value="">-Please Select-</option></select>';
	 $('#pendinggrade').replaceWith(gradestr); 
	var strandstr='<select class="combobox" name="pendingstrand" id="pendingstrand" ><option value="">-Please Select-</option></select>';
	 $('#pendingstrand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="pendingstandards" id="pendingstandards" ><option value="">-Please Select-</option></select>';
	 $('#pendingstandards').replaceWith(standardstr); 
	 $('#pendingstandarddata').val('');  $('#pending_standarddisplay').val(''); 
	 $('#pendingstandard_id').val('');
	 
	 
	 
	 
	var g_url='teacherplan/getActiveGrades/'+subject_id;
    $.getJSON(g_url,function(result)
	{
	var eld=2;
	
	if(result.grades!=false)
	{
	gradestr='<select class="combobox" name="pendinggrade" id="pendinggrade" onchange="pendingselectstrand(this.value,0)"><option value="">-Please Select-</option>';
	$.each(result.grades, function(index, value) {
	gradestr+='<option value="'+value['grade_id']+'">'+value['grade']+'</option>';
	$('#pendingstandard_id').val(value['standard_id']);
	eld=value['eld'];
	});
	gradestr+='</select>';
	 $('#pendinggrade').replaceWith(gradestr); 
    }
	else
	{
     var gg='';
	 gradestr='<select class="combobox" name="pendinggrade" id="pendinggrade" onchange="pendingselectmaterial('+subject_id+',this.value)" ><option value="">-Please Select-</option>';
	 $('#pendingstandard_id').val('');
	 
	 var g_url='teacherplan/getGrades/';
    $.getJSON(g_url,function(resultgrade)
	{
	
	if(resultgrade.grades!=false)
	{
	
	$.each(resultgrade.grades, function(index, value) {
	gg+='<option value="'+value['grade_id']+'">'+value['grade_name']+'</option>';
	
	
	
	
	});
	
    }
	 gg+='</select>';
	  $('#pendinggrade').replaceWith(gradestr+gg); 
	 
	});
	
	
	
	}
    //console.log(gradestr);
	
	 
	 $("select[name='pendinggrade'] option[value='']").attr("selected", true);
	 if(eld==0)
	 {
	    $('.eldstyle').replaceWith('<span class="eldstyle">Common Core</span>');
	 }
	 else if(eld==1)
	 {
	   $('.eldstyle').replaceWith('<span class="eldstyle">ELD</span>');
	 
	 }
	 else
	 {
	   $('.eldstyle').replaceWith('<span class="eldstyle"></span>');
	 
	 }
	 if(result.grades!=false)
	{
	  $('.newst').show();
	  $('.oldst').hide();
	
	}
	else
	{
	 $('.newst').hide();
	$('.oldst').show();
	
	}
	 
	 });
	 
  
  }

}
function selectmaterial(subject_id,grade_id)
{



var g_url='lesson_plan_material/getmaterial/'+subject_id+'/'+grade_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
	if(result.materials!=false)
	{
	var c=(result.materials).length;
	
	$.each(result.materials, function(index, value) {
	
	if(index==0)
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="850" height="320"></object> </td></tr>';
	if(c>1)
	{
	gradestr+="<tr><td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td></tr>";
	}
	gradestr+='</table>';
	}
	else
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'" style="display:none;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="850" height="320"></object> </td></tr>';
	gradestr+="<tr><td><input type='button' name='previous' id='previous' value='<<' onclick='previouspage("+index+");'></td>";
	if(index<(c-1))
	{
	gradestr+="<td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td>";
	}
	gradestr+='</tr></table>';
	}
	
	});
	
    }
	else
	{
     
	 gradestr+='No Materials Found.</td></tr><tr><td>';
	}
gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
	 
	 
	 
	 
	 
	 });

}
function pendingselectmaterial(subject_id,grade_id)
{



var g_url='lesson_plan_material/getmaterial/'+subject_id+'/'+grade_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<table id="pendingmaterial" border="0" style="float: left;"><tr><td>';
	if(result.materials!=false)
	{
	var c=(result.materials).length;
	
	$.each(result.materials, function(index, value) {
	
	if(index==0)
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="850" height="320"></object> </td></tr>';
	if(c>1)
	{
	gradestr+="<tr><td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td></tr>";
	}
	gradestr+='</table>';
	}
	else
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'" style="display:none;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="850" height="320"></object> </td></tr>';
	gradestr+="<tr><td><input type='button' name='previous' id='previous' value='<<' onclick='previouspage("+index+");'></td>";
	if(index<(c-1))
	{
	gradestr+="<td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td>";
	}
	gradestr+='</tr></table>';
	}
	
	});
	
    }
	else
	{
     
	 gradestr+='No Materials Found.</td></tr><tr><td>';
	}
gradestr+='</td></tr></table>';    
	$('#pendingmaterial').replaceWith(gradestr); 
	 
	 
	 
	 
	 
	 });

}
function nextpage(i)
{

var j=i+1;
document.getElementById('book_'+i).style.display='none';
document.getElementById('book_'+j).style.display='';
}
function previouspage(i)
{

var j=i-1;
document.getElementById('book_'+i).style.display='none';
document.getElementById('book_'+j).style.display='';
}
function openbook(id)
{

//var s='<td id="'+id+'" ><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+id+'.pdf" type="application/pdf"  width="850" height="320"></object> <span style="cursor:pointer;" onclick="closebook('+id+')">X</span></td>';

//$('#'+id).replaceWith(s); 
}

function closebook(id)
{

var s='<td id="'+id+'" onclick="openbook('+id+')"><img src="'+siteurlm+'images/book.jpg"></td>';

$('#'+id).replaceWith(s); 
}
function pendingselectstrand(grade,standard_id)
{	
	pendingselectmaterial($('#pendingsubject_id').val(),grade);
	 var strandstr='<select class="combobox" name="pendingstrand" id="pendingstrand" ><option value="">-Please Select-</option></select>';
	 $('#pendingstrand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="pendingstandards" id="pendingstandards" ><option value="">-Please Select-</option></select>';
	 $('#pendingstandards').replaceWith(standardstr); 
	 $('#pendingstandarddata').val('');  $('#pending_standarddisplay').val(''); 
	 if(standard_id==0)
		{	
			standard_id=$('#pendingstandard_id').val();
		}
	
	 
	 
	var g_url='teacherplan/getstrand/'+grade+'/'+standard_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<select class="combobox" name="pendingstrand" id="pendingstrand" onchange="pendingselectstandard(this.value,0,0)"><option value="">-Please Select-</option>';
	if(result.strands!=false)
	{
	
	$.each(result.strands, function(index, value) {
	gradestr+='<option value="'+value['strand']+'">'+value['strand']+'</option>';
	
	});
	gradestr+='</select>';
    }
	else
	{
     
	 gradestr+='</select>';
	}
     $('#pendingstrand').replaceWith(gradestr); 
	 
	 $("select[name='pendingstrand'] option[value='']").attr("selected", true);
	 
	 
	 
	 });
	 
  
  

}
function pendingselectstandard(strand,standard_id,grade)
{	
	
	
	 var standardstr='<select class="combobox" name="pendingstandards" id="pendingstandards" ><option value="">-Please Select-</option></select>';
	 $('#pendingstandards').replaceWith(standardstr); 
	 $('#pendingstandarddata').val('');  $('#pending_standarddisplay').val(''); 
	 if(standard_id==0)
	 {
		 standard_id=$('#pendingstandard_id').val();
	 }
	if(grade==0)
	 {
		 grade=$('#pendinggrade').val();;
	 }	 
	 
	  pdata={};
			pdata.strand=strand;
			pdata.grade=grade;
			pdata.standard_id=standard_id;
		   $.post('teacherplan/getstandard',{'pdata':pdata},function(result) 
		   
		   
	 
	//var g_url='teacherplan/getstandard/'+strand+'/'+grade+'/'+standard_id;
   // $.getJSON(g_url,function(result)
	{
	var gradestr='<div  name="pendingstandards" id="pendingstandards"  style="height:155px;width:500px; background-color: #F6FFFF;   overflow: scroll;    border: 1px solid #70B8BA;    color: #02AAD2; "  >';
	//var gradestr='<select class="combobox" name="pendingstandards" id="pendingstandards" onchange="pendingselectst(this.value)"><option value="">-Please Select-</option>';
	if(result.standard!=false)
	{
	
	$.each(result.standard, function(index, value) {
	
	var t='"'+value['standard']+'"';
	//gradestr+='<option value="'+value['standard']+'">'+value['standard']+'</option>';
	gradestr+="<span style='cursor:pointer;'  onclick='pendingselectst("+t+","+value['standarddata_id']+");'>"+value['standard']+"</span><br/><hr style='color: #02AAD2;' >";
	});
	gradestr+='</div>';
    }
	else
	{
     
	 gradestr+='</div>';
	}
     $('#pendingstandards').replaceWith(gradestr); 
	 
	 $("select[name='pendingstandards'] option[value='']").attr("selected", true);
	 
	 
	 
	 },'json');
	 
  
  

}

function pendingselectst(v,s)
{
$('#pendingstandarddata').val(v);
$('#pending_standarddisplay').val(v); 
$('#pendingstandarddata_id').val(s); 

}
function edit(lesson_week_plan_id)
{
$("ul.tabs li").removeClass("active"); //Remove any "active" class
$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
$('#message').hide();
$('#lesson_week_plan_id').val(lesson_week_plan_id);
var grade='';
var strand='';
var standard_id='';
var subject_id=0;
var stranddata='';
var da='';


$('#standard').val('');
$('#diff_instruction').val('');

var gradestr='<table id="material" border="0" style="float: left;"><tr><td></td></tr><tr><td></td></tr></table>';
$('#material').replaceWith(gradestr); 

	var gradestr='<span id="grade_display"></span>';
	 $('#grade_display').replaceWith(gradestr); 
	var strandstr='<select class="combobox" name="strand" id="strand" ><option value="">-Please Select-</option></select>';
	 $('#strand').replaceWith(strandstr); 
	 var standardstr='<select class="combobox" name="standards" id="standards" ><option value="">-Please Select-</option></select>';
	 $('#standards').replaceWith(standardstr); 
	  var subjectstr='<span id="subject_display"></span>';
	 
	 $('#subject_display').replaceWith(subjectstr); 
	$('#grade').val('');	
	$('#subject_id').val('');	
	$('#period_id').val('');	
	$('#standard_id').val('');
	$('#standarddata_id').val('');
	 $('#standarddata').val('');$('#standarddisplay').val(''); 
	 $('.oldst').show();
	 
	 
 $('.newst').hide();
 
 if(arrlessonplans!=false){
		 var tablehtml='';
		for(var i=0;i<arrlessonplans.length;i++){
		tablehtml='';
		 tablehtml += "<tr id='"+arrlessonplans[i].lesson_plan_id+"' class='lessonplan' >";
			
			tablehtml += "<td valign='top' class='style1'>	<font color='red'>*</font>"+arrlessonplans[i].tab+":	</td><td valign='top' ><select name='"+arrlessonplans[i].lesson_plan_id+"' onchange='selectgrade("+arrlessonplans[i].lesson_plan_id+",this.value)' id='"+arrlessonplans[i].lesson_plan_id+"'> <option value=''>-Please Select-</option>";
				
				for(var j=0;j<arrlessonplansub.length;j++)
				{
				 if(arrlessonplans[i].lesson_plan_id==arrlessonplansub[j].lesson_plan_id) {
				 tablehtml += "<option value='"+arrlessonplansub[j].lesson_plan_sub_id+"'>"+arrlessonplansub[j].subtab+"</option>";
				 }
				  
				 } 
				
				tablehtml +="</select></td>";
				
			tablehtml +="</tr>";
			
			$('#'+arrlessonplans[i].lesson_plan_id).replaceWith(tablehtml);
		
		}
		
		
		}
		if(custom_diff!=false)
		{
		for(var i=0;i<custom_diff.length;i++){
			$('#c_'+custom_diff[i].custom_differentiated_id).val('');
		
		}
		
		
		}
var p_url='teacherplan/getteacherplaninfo/'+lesson_week_plan_id;

    $.getJSON(p_url,function(result)
	{
	  	
		
		$.each(result.lesson_week, function(index, value) {
		if(index==0)
		{
		        var start1=[];
				var end1=[];
				
				$('#date').val(value['date']);
					da=value['date'];
				  end1=value['endtime'].split(':');
				  start1=value['starttime'].split(':');
				if(start1[0]>=12)
				{
				 if(start1[0]==12)
				 {
				    
				 
				 }
				 else
				 {
				 start1[0]=start1[0]-12;
				 }
				 start1[2]='pm';
				
				}
				else
				{
				 
				 if(start1[0]==0)
				 {
				    start1[0]=12;
				 
				 }
				 start1[2]='am';
				
				}
				if(end1[0]>=12)
				{
				 if(end1[0]==12)
				 {
				 
				 
				 }
				 else
				 {
				 end1[0]=end1[0]-12;
				 }
				 end1[2]='pm';
				
				}
				else
				{
				 
				 if(end1[0]==0)
				 {
				    end1[0]=12;
				 
				 }
				 end1[2]='am';
				
				}
				
				$('#start').val(start1[0]+':'+start1[1]+' '+start1[2]);
				$('#end').val(end1[0]+':'+end1[1]+' '+end1[2]);
				if(value['standard'])
				{				
				$('#standard').val(value['standard']);
				}
				else
				{
				$('#standard').val('');
				
				}
				$('#period_display').replaceWith('<span id="period_display">'+$('#start').val()+'-'+$('#end').val()+'</span>');
				$('#standard_id').val(value['standard_id']);
				$('#standarddata_id').val(value['standarddata_id']);
				$('#diff_instruction').val(value['diff_instruction']);
                standard_id=value['standard_id'];
			    grade=value['grade'];
				strand=value['strand'];
				stranddata=value['standard'];
				
				//$("select[name='period_id'] option[value='"+value['period_id']+"']").attr("selected", true);
				$('#period_id').val(value['period_id']);
				selectsubject(value['period_id'],value['subject_id']);
				 subject_id=value['subject_id'];
				selectmaterial(subject_id,grade);	
		}
		 var lesson_plan_id=value['lesson_plan_id'];
		 
		 var lesson_plan_sub_id=value['lesson_plan_sub_id'];
		  
		 
		 $("select[name="+lesson_plan_id+"] option[value='"+lesson_plan_sub_id+"']").attr("selected", true);
		 
				
        
});

$('#teacheradd').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacheradd" value="Update" >');

   
   if(standard_id && standard_id!='')
   {
		$('.newst').show();
		
		//alert(grade);
		selectgrade(1,subject_id,grade,1);	
		
		//$('#grade').val(grade);
		//alert(grade);
		//$("select[name='grade']").find("option:contains('"+grade+"')").attr("selected", true);
		selectstrand(grade,standard_id);
		//$("select[name='strand'] option[value='"+strand+"']").attr("selected", true);	
		selectstandard(strand,standard_id,grade);
		selectst(stranddata);
		//$("#grade").val("K").attr("selected",true);
    
   }
   else
   {
	  //alert(grade);
	  selectgrade(1,subject_id,grade,0);	
	 //selectstrand(grade,standard_id);
      $('.oldst').show();
   
   }
	var myVar=setTimeout(function(){selectItemByValue(lesson_week_plan_id);},2000);
	
	});
	
	
	
	
	
	
	//$("select[name='grade']").find("option:contains('"+grade+"')").attr("selected", true);
	
		
	

$("#dialog").dialog({
			modal: true,
           	height: 600,
			width:950
			});
			
			var myVar1=setTimeout(function(){$("#dialog").dialog('option', 'title', 'Date:'+da);},500);
			
$('#teacherplan').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacherplan/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   planadd
    });

$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	



}
function selectItemByValue(lesson_week_plan_id){



var p_url='teacherplan/getteacherplaninfo/'+lesson_week_plan_id;

    $.getJSON(p_url,function(result)
	{
	  	
		
		$.each(result.lesson_week, function(seindex, sevalue) {
		if(seindex==0 && sevalue['standard_id']!='')
		{
		
		 //alert('Loading');
		
		// $("select[name='grade']").find("option:contains('"+value['grade']+"')").attr("selected", true);
		 $("select[name='strand']").find("option:contains('"+sevalue['strand']+"')").attr("selected", true);
		 $("select[name='standards']").find("option:contains('"+sevalue['standard']+"')").attr("selected", true);
		 
		
		}
		 if(seindex==0)
		 {
		 //alert('Loading Data');
		 //console.log(sevalue['grade']);
		//alert('Loading Data');
		 
		// $("select[name='grade'] option[value='"+sevalue['grade']+"']").attr("selected", true);
		$('#grade').val(sevalue['grade']);
		 }
		
		});
		
	});	
	
	
	var p_url='teacherplan/getcustomdiff/'+lesson_week_plan_id;

    $.getJSON(p_url,function(result)
	{
	  	
		if(result!=false)
		{
		$.each(result.custom, function(seindex, sevalue) {
		
		$('#c_'+sevalue['custom_differentiated_id']).val(sevalue['answer']);
		 
		
		});
		
		}
		
	});	


  }
function deleteweek(lesson_week_plan_id)
{

if(window.confirm("Are you sure you want to delete this Plan?")==false){
			return;
		}
		
var p_url = 'teacherplan/deleteplan/'+lesson_week_plan_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			
			planadd(data);
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});




}

function planadd(data)
{

 if(data.status==1)
 {
     
	 $.post('teacherplan/getallteacherplans/'+data.date,function(pdata) {
	     var $msgContainer = $("table#"+data.date);
		 $msgContainer.replaceWith(pdata.html);
		 $('#dialog').dialog('close');
		 
  $('a.title').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false
});

  $('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});
		// alert(data.date);
		// alert(pdata.html);
	 
	 },"json");
 
 
 
 }
 else
 {
      $('#message').text(data.message);
	  $('#message').show();
 
 
 }

}
function planaddpending(data)
{

 if(data.status==1)
 {
    $('#pendingrow'+data.lesson_week_plan_id).remove();   
	 $.post('teacherplan/getallteacherplans/'+data.date,function(pdata) {
	     var $msgContainer = $("table#"+data.date);
		 $msgContainer.replaceWith(pdata.html);
		 $('#pendingdialog').dialog('close');
		 $('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'});
		// alert(data.date);
		// alert(pdata.html);
	 
	 },"json");
 
 
 
 }
 else
 {
      $('#pendingmessage').text(data.message);
	  $('#pendingmessage').show();
 
 
 }

}

function showplan(da)
{

$('#'+da).show();
var $msgContainer = $("#a_"+da);
var a="'"+da+"'";

$msgContainer.replaceWith('<span id="a_'+da+'" style="align:left;cursor:pointer;" onclick="hideplan('+a+')"><img src="'+siteurlm+'images/hide_lesson_plan.png" height="16" width="16" alt="Hide LessonPlan"  ></span>');


}
function hideplan(da)
{

$('#'+da).hide();
var $msgContainer = $("#a_"+da);
var a="'"+da+"'";
$msgContainer.replaceWith('<span id="a_'+da+'" style="align:left;cursor:pointer;" onclick="showplan('+a+')"><img src="'+siteurlm+'images/show_lesson_plan.png" height="16" width="16" alt="Show LessonPlan"  ></span>');


}
