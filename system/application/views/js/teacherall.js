$(document).ready(function() {

$("#teacherform").validate({

rules: {
firstname:{
required: true,
maxlength:40
},
username:{
required: true,
maxlength:40
},
password:{
required: true
},
school_id:{required: true},
email:{
email: function(element){if($("#email").val()!='' ){ return true ;} else {return false;}},
maxlength:60
}

},
messages: {
firstname:{
required: "Please enter first Name"
},
username:{
required: "Please enter user Name"
}
,
password:{
required: "Please enter password"
},
school_id:{required: "Please select school"}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
//var page =1;
//$('#pageid').val(page);
//var state_id=$('#states').val();
//var country_id=$('#countries').val();
//var district_id=$('#district').val();
//var school_id=$('#school').val();
//$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
//$msgContainer.html(msg);
//$('#teacherdetails').show();
//loading_hide();
//});

// onclick  pagination Load
$('#msgContainer .teachers li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');

$('#pageid').val(page);

var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
var school_id=$('#school').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);

var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
var school_id=$('#school').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#teacher_add').click(function()
		{
		teacheradd();	 
		$('#message').hide();	 		
		$('#firstname').val('');
		$('#lastname').val('');
        $('#username').val('');
        $('#password').val('');				
		$('#teacher_id').val('');
		$('#email').val('');
		$('input:radio[name=avatar]').filter('[value=1]').attr('checked', true);
		$('#teacherupdate').replaceWith('<button data-dismiss="modal" type="submit" name="submit" id="teacheradd" value="Add" onclick="teacheradd()" class="btn btn-success"><i class="icon-plus"></i> Add Teacher</button>');
		
		//$('#teacheradd').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacheradd" value="Add" onclick="teacheradd()">');
		var s_url='school/getschoolbydistrict/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="school_id" id="school_id"><option value="">-Select-</option>';
	if(sresult.school!=false)
	{
	$.each(sresult.school, function(index, value) {
	str+='<option value="'+value['school_id']+'">'+value['school_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#school_id').replaceWith(str); 
	 $("select[name='school_id'] option[value='']").attr("selected", true);
	});
		$("#dialog").dialog({
			modal: true,
           	height: 525,
			width: 500
			});
				
		
		
		}); 
		
/*$('#teacherform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher/add_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacherJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function teacheradd()
{

$('#teacherform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher/add_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacherJsonadd 
    });

}		

function teacheredit(teacher_id)
{
	teacherupdate();
	$('#message').hide();
	var p_url='teacher/getteacherinfo/'+teacher_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#firstname').val(result.teacher.firstname);
	$('#lastname').val(result.teacher.lastname);
	$('#teacher_id').val(result.teacher.teacher_id);
	$('#username').val(result.teacher.username);
	$('#password').val('torvertex');
	$('#email').val(result.teacher.email);
	$('input:radio[name=avatar]').filter('[value='+result.teacher.avatar+']').attr('checked', true);
	$('#teacheradd').replaceWith('<button data-dismiss="modal" type="submit" name="submit" id="teacherupdate" value="Update" onclick="teacherupdate()" class="btn btn-success"><i class="icon-save"></i>Save Changes</button>');
	var s_url='school/getschoolbydistrict/';
    $.getJSON(s_url,function(sresult)
	{
	var str='<select name="school_id" id="school_id"><option value="">-Select-</option>';
	if(sresult.school!=false)
	{
	$.each(sresult.school, function(index, value) {
	str+='<option value="'+value['school_id']+'">'+value['school_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#school_id').replaceWith(str); 
	 $("select[name='school_id'] option[value='"+result.teacher.school_id+"']").attr("selected", true);
	});
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 525,
			width: 500
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function teacherdelete(teacher_id)
{


	if(window.confirm("Are you sure you want to delete this teacher?")==false){
			return;
		}
		
		
		var p_url = 'teacher/delete/'+teacher_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
           var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
var school_id=$('#school').val();
$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function teacherupdate()
{

 $('#teacherform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher/update_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacherJsonupdate 
    }); 



}

function procesteacherJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 var school_id=$('#school_id').val();
	var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesteacherJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var school_id=$('#school_id').val();
	var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("teacher/getteachers/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"/"+school_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 

$('#add_teacher').on('click',function(){
    
    $.ajax({
        type: 'post',
    url: base_url+'teacher/add_teacher',
    data: $("#addteacherform").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          $('#myModal-add-new-parent').modal('hide');
          $('#successbtn').modal('show');
          
          location.reload(true);
        
      }
      processtudentJsonadd(data);
    });
});

$('#add_teacher').on('click',function(){
    
    $.ajax({
        type: 'post',
    url: base_url+'teacher/update_teacher',
    data: $("#addteacherform").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          $('#myModal-add-new-parent').modal('hide');
          $('#successbtn').modal('show');
          
         
        
      }
      processtudentJsonadd(data);
    });
});

function edit_teacher(teacher_id)
{
    
	//teacherupdate();
	//$('#message').hide();
	var p_url='teacher/getteacherinfo/'+teacher_id;
    $.getJSON(p_url,function(result)
	{
		console.log(result);	   
	$('#firstname').val(result.teacher.firstname);
	$('#lastname').val(result.teacher.lastname);
	$('#teacher_id').val(result.teacher.teacher_id);
	$('#username').val(result.teacher.username);
	$('#password').val('torvertex');
	$('#email').val(result.teacher.email);
	$('#avatar').val(result.teacher.avatar);
        $('#phone').val(result.teacher.phone);
	$('#school_name').val(result.teacher.school_name);
        $('#school_id').val(result.teacher.school_id).trigger("liszt:updated");
        
	}); 

}

$('#update_teacher').on('click',function(){
     $.ajax({
        type: 'post',
    url: base_url+'teacher/update_teacher',
    data: $("#editform").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          $('#myModal-edit-teachercb').modal('hide');
          $('#successbtn').modal('show');
          
          
        
      }
      processtudentJsonadd(data);
    });
});

function deleteteacher(teacher_id){
    $.ajax({
        type: 'post',
    url: base_url+'teacher/delete/'+teacher_id
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          $('#myModal-add-new-parent').modal('hide');
          $('#successbtn').modal('show');
          
          
        
      }
      
    });
    }