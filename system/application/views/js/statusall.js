$(document).ready(function() {
$("#statusform").validate({

rules: {
status_name:{
required: true
}
},
messages: {
status_name:{
required: "Please enter status Name"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("status/getstatuss/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#statusdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .statuss li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
$.get("status/getstatuss/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#status_add').click(function()
		{
			 
		statusadd();
		$('#message').hide();		
		$('#status_name').val('');
		$('#status_id').val('');
		$('#statusupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="statusadd" value="Add" onclick="statusadd()">');
		
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#statusform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'status/add_status',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtatusJson 
    });*/ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function statusadd()
{

$('#statusform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'status/add_status',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtatusJsonadd 
    });

}		

function statusedit(status_id)
{
	statusupdate();
	$('#message').hide();
	var p_url='status/getstatusinfo/'+status_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#status_name').val(result.status.status_name);
	
	$('#status_id').val(result.status.status_id);
	$('#statusadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="statusupdate" value="Update" onclick="statusupdate()">');
		
	
	
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function statusdelete(status_id)
{


	if(window.confirm("Are you sure you want to delete this status?")==false){
			return;
		}
		
		
		var p_url = 'status/delete/'+status_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            $.get("status/getstatuss/"+page+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function statusupdate()
{

 $('#statusform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'status/update_status',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtatusJsonupdate 
    }); 



}

function processtatusJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    $.get("status/getstatuss/"+page+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
}
function processtatusJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    $.get("status/getstatuss/"+page+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
} 