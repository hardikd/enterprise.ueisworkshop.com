$(document).ready(function() {
$("#planform").validate({

rules: {
option:{
required: true
},
link:{
required: true
}

},
messages: {
option:{
required: "Please Enter option Name"
},
link:{
required: "Please Enter Link"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("option/getoptions/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#plandetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .option li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
$.get("option/getoptions/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#plan_add').click(function()
		{
			 
			planadd(); 
		$('#message').hide();			
		$('#option').val('');
		$('#link').val('');
		
	
	
	
		
		$('#planupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planadd" value="Add" onclick="planadd()">');
		//$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/add_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function planadd()
{

$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'option/add_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonadd 
    });

}		

function planedit(plan_id)
{
	planupdate();
	$('#message').hide();
	var p_url='option/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
			 
	$('#option').val(result.option.optionname);
	$('#plan_id').val(result.option.option_id);
	$('#link').val(result.option.link);
	 
	 });
	$('#planadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planupdate" value="Update" onclick="planupdate()">');
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function plandelete(plan_id)
{


	if(window.confirm("Are you sure you want to delete this option?")==false){
			return;
		}
		
		
		var p_url = 'option/delete/'+plan_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
           
$.get("option/getoptions/"+page+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function planupdate()
{

 $('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'option/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}

function processchoolJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
  
	
$.get("option/getoptions/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    
	
$.get("option/getoptions/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 