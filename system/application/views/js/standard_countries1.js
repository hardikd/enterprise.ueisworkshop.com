function states_select(id)
{
	var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="states" id="states" onchange="district_all(this.value)">';
	if(result.states!=false)
	{
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	if(index==0)
	{
		district_all(value['state_id']);
		
	}
	});
	str+='</select>';
	
	
    }
	else
	{
     var str='<select class="combobox" name="states" id="states" >';
	 str+='<option value="0">No States Found</option></select>';
     var sstr='<select class="combobox" name="district" id="district">';
	 sstr+='<option value="">No Districts Found</option></select>';
	  $('#district').replaceWith(sstr); 
	}
     $('#states').replaceWith(str); 
	 
	 
	 
	 
	 });

}

function states_select_only(id)
{

    var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="states" id="states" onchange="district_all(this.value)">';
	if(result.states!=false)
	{
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
     var str='<select class="combobox" name="states" id="states" >';
	 str+='<option value="0">No States Found</option></select>';
     var sstr='<select class="combobox" name="district" id="district">';
	 sstr+='<option value="">No Districts Found</option></select>';
	  $('#district').replaceWith(sstr); 
	}
     $('#states').replaceWith(str); 

});

}
function states1_select(id)
{
	var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="states1" id="states1" onchange="district1_all(this.value)">';
	if(result.states!=false)
	{
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	if(index==0)
	{
		district1_all(value['state_id']);
		
	}
	});
	str+='</select>';
	
	
    }
	else
	{
     var str='<select class="combobox" name="states1" id="states1" >';
	 str+='<option value="0">No States Found</option></select>';
     var sstr='<select class="combobox" name="districtcopy" id="districtcopy">';
	 sstr+='<option value="">No Districts Found</option></select>';
	  $('#districtcopy').replaceWith(sstr); 
	}
     $('#states1').replaceWith(str); 
	 
	 
	 
	 
	 });

}

function district_all(id)
{

    
	var s_url='district/getDistrictsByStateId/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district" id="district">';
		if(sresult.district!=false)
	{
	//sstr+='<option value="all">All</option>';
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='<option value="">No Districts Found</option></select>';
	}
     $('#district').replaceWith(sstr); 
	});


}

function district1_all(id)
{

    var s_url='district/getDistrictsByStateId/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="districtcopy" id="districtcopy">';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='<option value="">No Districts Found</option></select>';
	}
     $('#districtcopy').replaceWith(sstr); 
	});


}

function states_select_option(id)
{
	var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(result.states!=false)
	{
	$.each(result.states, function(index, value) {
	if(index==0)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option></select>';
		$('#district_id').replaceWith(sstr);
		
	}
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     var sstr='<select class="combobox" name="district_id" id="district_id">';
	 sstr+='<option value="">-Select-</option></select>';
	 $('#district_id').replaceWith(sstr);
	 str+='</select>';
	}
	 var ssstr='<select class="combobox" name="subject_id" id="subject_id">';
	 ssstr+='<option value="">-Select-</option></select>';
	 $('#subject_id').replaceWith(ssstr);
	 
     $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
    });
	 
	
}

function district_select_option(id)
{

	var s_url='district/getDistrictsByStateId/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id" onchange="select_subject(this.value)"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     
	 var str='<select class="combobox" name="subject_id" id="subject_id">';
	 str+='<option value="">-Select-</option></select>';
	 $('#subject_id').replaceWith(str);
	 $('#district_id').replaceWith(sstr); 
	});



}
function select_subject(id)
{

	var s_url='standard/getdist_subjectsById/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="subject_id" id="subject_id" ><option value="">-Select-</option>';
		if(sresult.dist_subject!=false)
	{
	$.each(sresult.dist_subject, function(index, value) {
	sstr+='<option value="'+value['subject_id']+'">'+value['subject_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#subject_id').replaceWith(sstr); 
	});



}