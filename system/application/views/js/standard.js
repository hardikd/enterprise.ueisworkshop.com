$(document).ready(function() {
$("#standardform").validate({

rules: {
subject_id:{
required: true
},
status:{
required: true
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
subject_id:{
required: "Please Select Subject"
},
status:{
required: "Please Select Status"
}
,
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("standard/getstandards/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#standarddetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .standards li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("standard/getstandards/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("standard/getstandards/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#standard_add').click(function()
		{
			 
			standardadd(); 
		$('#message').hide();			
		$("select[name='subject_id'] option[value='']").attr("selected", true);
		//$("select[name='subject_id']").attr('disabled', false)	 
		$("select[name='status'] option[value='']").attr("selected", true);
		//$("select[name='eld'] option[value='']").attr("selected", true);
		
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	  var str='<select class="combobox" name="subject_id" id="subject_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#subject_id').replaceWith(str); 
	 $("select[name='subject_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 });
	

		
		$('#standardupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="standardadd" value="Add" onclick="standardadd()">');
		//$('#standardadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="standardadd" value="Add" onclick="standardadd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#standardform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'standard/add_standard',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtandardJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function standardadd()
{

$('#standardform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'standard/add_standard',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtandardJsonadd 
    });

}		

function standardedit(standard_id)
{
	standardupdate();
	$('#message').hide();
	var p_url='standard/getstandardinfo/'+standard_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#standard_id').val(standard_id);
	$("select[name='status'] option[value="+result.standard.status+"]").attr("selected", true);
	//$("select[name='eld'] option[value="+result.standard.eld+"]").attr("selected", true);
	$("select[name='subject_id'] option[value="+result.standard.subject_id+"]").attr("selected", true);
	$("select[name='subject_id']").attr('disabled', true)	 
	
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.standard.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.standard.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.standard.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.standard.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.standard.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	
	});
	$('#standardadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="standardupdate" value="Update" onclick="standardupdate()">');
	
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}


function standardupdate()
{

 $('#standardform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'standard/update_standard',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtandardJsonupdate 
    }); 



}

function processtandardJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
  var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district_id').val();

	
$.get("standard/getstandards/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processtandardJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
    var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district_id').val();

	
$.get("standard/getstandards/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 