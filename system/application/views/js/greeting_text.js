$(document).ready(function() {
$("#planform").validate({

rules: {
tab:{
required: true
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}
,
report:{
required: true
}
,
size:{
required: true,
digits:true
} 
},
messages: {
tab:{
required: "Please Enter Greeting text"
},

district_id:{
required: "Please Enter Districts"
}
,
report:{
required: "Please Select Report"
}
,
size:{
required: "Please Enter Font Size"
}

}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var district_id=$('#district').val();
$.get("greetings_text/getgreetings_texts/"+page, function(msg){
$msgContainer.html(msg);
$('#plandetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .greetings_text li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
 var district_id=$('#district').val();
$.get("greetings_text/getgreetings_texts/"+page, function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getgreetings_text').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
 var district_id=$('#district').val();

$.get("greetings_text/getgreetings_texts/"+page, function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#plan_add').click(function()
		{
			 
			planadd(); 
		$('#message').hide();			
		$('#tab').val('');
		 $("select[name='report'] option[value='']").attr("selected", true);
		$('#size').val('');
 
 		$('#planupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planadd" value="Add" onclick="planadd()">');
		//$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 450,
			width: 700
			});
				
 		}); 
 	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function planadd()
{

$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'greetings_text/add_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success: processchoolJsonadd
    });

}		

function planedit(plan_id)
{
	planupdate();
	$('#message').hide();
	var p_url='greetings_text/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
 	$('#tab').val(result.greetings_text.greeting_text);

	 $("select[name='report'] option[value="+result.greetings_text.report+"]").attr("selected", true);
		$('#size').val(result.greetings_text.size);
		
	 $('#plan_id').val(result.greetings_text.greeting_text_id);
  
	 });
   
	 	$('#planadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planupdate" value="Update" onclick="planupdate()">');
	
	$("#dialog").dialog({
			modal: true,
           	height: 450,
			width: 700
			});
 			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function plandelete(plan_id)
{
	if(window.confirm("Are you sure you want to delete this Text?")==false){
			return;
		}
		
		var p_url = 'greetings_text/delete/'+plan_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
			var district_id=$('#district').val();
           
$.get("greetings_text/getgreetings_texts/"+page, function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function planupdate()
{

 $('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'greetings_text/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}

function processchoolJsonadd(data)
{
	
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
	 $('#message').text(data.message);
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
 
 var district_id=$('#district_id').val();
	
$.get("greetings_text/getgreetings_texts/"+page, function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
 }
function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
     $('#message').text('');
     var page = $('#pageid').val();
	
	$.get("greetings_text/getgreetings_texts/"+page, function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
 } 