$(document).ready(function() {
$("#proficiencygroupform").validate({

rules: {
group_name:{
required: true
}
,
state_id:{
required: true
},
country_id:{
required: true
}
,district_id:{
required: true
}

},
messages: {
group_name:{
required: "Please enter group Name"


},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: " Please Select District"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("proficiencygroup/getproficiencygroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#proficiencygroupdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .proficiencygroups li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("proficiencygroup/getproficiencygroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#getproficiencygroup').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("proficiencygroup/getproficiencygroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#proficiencygroup_add').click(function()
		{
			 
		proficiencygroupadd();
		$('#message').hide();		
		$('#group_name').val('');
		$('#group_id').val('');
		$('#description').val('');
		var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 });
		$('#proficiencygroupupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="proficiencygroupadd" value="Add" onclick="proficiencygroupadd()">');
		/*var s_url='district/getAlldistricts/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="district_id" id="district_id"><option value="">-Select-</option>';
	if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	str+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	});*/
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#proficiencygroupform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'proficiencygroup/add_proficiencygroup',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesproficiencygroupJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function proficiencygroupadd()
{

$('#proficiencygroupform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'proficiencygroup/add_proficiencygroup',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesproficiencygroupJsonadd 
    });

}		

function distedit(ob_id)
{
	proficiencygroupupdate();
	$('#message').hide();
	var p_url='proficiencygroup/getproficiencygroupinfo/'+ob_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#group_name').val(result.proficiencygroup.group_name);
	$('#group_id').val(result.proficiencygroup.group_id);
	$('#description').val(result.proficiencygroup.description);
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.proficiencygroup.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.proficiencygroup.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.proficiencygroup.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.proficiencygroup.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.proficiencygroup.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	 
	
	 
	 });
	$('#proficiencygroupadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="proficiencygroupupdate" value="Update" onclick="proficiencygroupupdate()">');
		
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function distdelete(ob_id)
{


	if(window.confirm("Are you sure you want to delete this group?")==false){
			return;
		}
		
		
		var p_url = 'proficiencygroup/delete/'+ob_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
             var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("proficiencygroup/getproficiencygroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function proficiencygroupupdate()
{

 $('#proficiencygroupform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'proficiencygroup/update_proficiencygroup',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesproficiencygroupJsonupdate 
    }); 



}

function procesproficiencygroupJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select_only(country_id);
district_all(state_id);

   $.get("proficiencygroup/getproficiencygroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 
function procesproficiencygroupJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select_only(country_id);
district_all(state_id);

   $.get("proficiencygroup/getproficiencygroups/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
	
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 