$(document).ready(function() {
$("#teacher_grade_subjectform").validate({
rules: {
teacher_id:{
required: true
},
grade_id:{required: true},
grade_subject_id:{required: true}
},
messages: {
teacher_id:{
required: "Please Select Teacher"
},
grade_id:{
required: "Please Select Grade"
},
grade_subject_id:{
required: "Please Select Subject"
}

}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var school_id=$('#school').val();
$.get("teacher_grade_subject/getteacher_grade_subjects/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#teacher_grade_subjectdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .teacher_grade_subjects li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
var school_id=$('#school').val();
$('#pageid').val(page);

$.get("teacher_grade_subject/getteacher_grade_subjects/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool').click(function(){
loading_show(); 
var page = 1;
var school_id=$('#school').val();
$('#pageid').val(page);

$.get("teacher_grade_subject/getteacher_grade_subjects/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#teacher_grade_subject_add').click(function()
		{
		teacher_grade_subjectadd();	 
		$('#message').hide();	 		
		
		$('#teacher_grade_subject_id').val('');
		
		
		$('#teacher_grade_subjectupdate').replaceWith('<button data-dismiss="modal" type="submit" name="submit" id="teacher_grade_subjectadd" value="Add" onclick="teacher_grade_subjectadd()" class="btn btn-success"><i class="icon-plus"></i>Add Clasroom</button>');
		//$('#teacher_grade_subjectadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="teacher_grade_subjectadd" value="Add" onclick="teacher_grade_subjectadd()">');
		
		if($('#school_id').val()=='')
		{
	var sstr='<select name="teacher_id" id="teacher_id" ><option value="">-Select-</option></select>';
	 $('#teacher_id').replaceWith(sstr); 
	 $("select[name='teacher_id'] option[value='']").attr("selected", true);
	}
	else
	{
	changeschool($('#school_id').val(),0);
	
	}
	$("select[name='grade_id'] option[value='']").attr("selected", true);
	
	var sstr='<select class="combobox" name="grade_subject_id" id="grade_subject_id" ><option value="">-Select-</option></select>';
	 $('#grade_subject_id').replaceWith(sstr); 
	 $("select[name='grade_subject_id'] option[value='']").attr("selected", true);
	
		$("#dialog").dialog({
			modal: true,
           	height: 400,
			width: 550
			});
				
		
		
		}); 
		
/*$('#teacher_grade_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher_grade_subject/add_teacher_grade_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacher_grade_subjectJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
	


function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});

function changeschool(school_id,teacher_id)
{

var s_url='teacher/getTeachersBySchool/'+school_id;
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="teacher_id" id="teacher_id" class="combobox" ><option value="">-Select-</option>';
	if(sresult.teacher!=false)
	{
	$.each(sresult.teacher, function(index, value) {
	str+='<option value="'+value['teacher_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#teacher_id').replaceWith(str); 
	 if(teacher_id==0)
	 {
		$("select[name='teacher_id'] option[value='']").attr("selected", true);
	 }
	 else
	 {
	 $("select[name='teacher_id'] option[value='"+teacher_id+"']").attr("selected", true);
	 }
	});

}
function changegrade(grade_id,grade_subject_id)
{

 $("select[name='grade_id'] option[value='"+grade_id+"']").attr("selected", true);
var s_url='grade_subject/getgrade_subjectall/'+grade_id;
		$.getJSON(s_url,function(sresult)
	{
	var str='<select class="combobox"  name="grade_subject_id" id="grade_subject_id" ><option value="">- Please Select-</option>';
	if(sresult.grade_subject!=false)
	{
	$.each(sresult.grade_subject, function(index, value) {
	str+='<option value="'+value['grade_subject_id']+'">'+value['grade_name']+' '+value['subject_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#grade_subject_id').replaceWith(str); 
	 if(grade_subject_id==0)
	 {
	 $("select[name='grade_subject_id'] option[value='']").attr("selected", true);
	 }
	 else
	 {
	 $("select[name='grade_subject_id'] option[value='"+grade_subject_id+"']").attr("selected", true);
	 
	 }
	});

}
function teacher_grade_subjectadd()
{

$('#teacher_grade_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher_grade_subject/add_teacher_grade_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacher_grade_subjectJsonadd 
    });

}		

function teacher_grade_subjectedit(teacher_grade_subject_id)
{
	teacher_grade_subjectupdate();
	$('#message').hide();
	var p_url='teacher_grade_subject/getteacher_grade_subjectinfo/'+teacher_grade_subject_id;
    $.getJSON(p_url,function(result)
	{
			   
	
	$('#teacher_grade_subject_id').val(result.teacher_grade_subject.teacher_grade_subject_id);
	
	$('#teacher_grade_subjectadd').replaceWith('<button data-dismiss="modal" type="submit" name="submit" id="teacher_grade_subjectupdate" value="Update" onclick="teacher_grade_subjectupdate()" class="btn btn-success"><i class="icon-plus"></i>Update</button> ');
		
	
	
	changeschool(result.teacher_grade_subject.school_id,result.teacher_grade_subject.teacher_id);
	//alert(result.teacher_grade_subject.teacher_id);
	//$("select[name='teacher_id'] option[value='"+result.teacher_grade_subject.teacher_id+"']").attr("selected", true);
	changegrade(result.teacher_grade_subject.grade_id,result.teacher_grade_subject.grade_subject_id);
	
	
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 400,
			width: 550
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function teacher_grade_subjectdelete(teacher_grade_subject_id)
{


	if(window.confirm("Are you sure you want to delete this teacher_grade_subject?")==false){
			return;
		}
		
		
		var p_url = 'teacher_grade_subject/delete/'+teacher_grade_subject_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var school_id=$('#school').val();
			$.get("teacher_grade_subject/getteacher_grade_subjects/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function teacher_grade_subjectupdate()
{

 $('#teacher_grade_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'teacher_grade_subject/update_teacher_grade_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesteacher_grade_subjectJsonupdate 
    }); 



}

function procesteacher_grade_subjectJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 var school_id=$('#school_id').val();
			$.get("teacher_grade_subject/getteacher_grade_subjects/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		//$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesteacher_grade_subjectJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var school_id=$('#school_id').val();
	$.get("teacher_grade_subject/getteacher_grade_subjects/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		//$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 