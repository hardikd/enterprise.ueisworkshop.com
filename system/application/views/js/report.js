$(document).ready(function() {
$("#reportform").validate({

rules: {
report_date:{
required: true
},
teacher_id:{
required: true
},
subject_id:{
required: true
},
grade_id:{
required: true
},
students:{
required: true,
digits:true
},
paraprofessionals:{
required: true,
digits:true
},observer_id:{
required: true
},lesson_correlation:{
required: true
}



},
messages: {
report_date:{
required: "Please enter Report Date"

},
teacher_id:{
required: "Please Select Teacher"

},
subject_id:{
required: "Please Select Subject"

},
grade_id:{
required: "Please Select Grade"

},
students:{
required: "Please Enter Number Of Students",
digits:"Please Enter Numeric Only"

},
paraprofessionals:{
required: "Please Enter Number Of paraprofessionals",
digits:"Please Enter Numeric Only"

},
observer_id:{
required: "Please Select Observer"

}
,
lesson_correlation:{
required: "Please Select Lesson Correlation"

}
}
,
errorElement:"div" 

});
var date = new Date();
                 var currentMonth = date.getMonth();
                 var currentDate = date.getDate();
                 var currentYear = date.getFullYear();
		$("#report_date").datepicker({ minDate: new Date(currentYear, currentMonth, currentDate),dateFormat: 'yy-mm-dd',changeYear: true
		 });
});

function change_corelation(id)
{
var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
gradestr+='</td></tr></table>';    
$('#material').replaceWith(gradestr); 
if($('#report_date').val()=='')
{
 $("select[name='lesson_correlation'] option[value='']").attr("selected", true);
alert('Please Select Date');
return false;

}
if($('#teacher_id').val()=='')
{
 $("select[name='lesson_correlation'] option[value='']").attr("selected", true);
alert('Please Select Teacher');
return false;

}
if(id==1 || id==3)
{
$('#subject_id').attr('disabled', 'disabled');
var g_url='report/getsubjectbydistrict/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="subject_id" id="subject_id" onchange="change_subject()"  ><option value="">-Please Select-</option>';
	if(result.subject!=false)
	{
	$.each(result.subject, function(index, value) {
	str+='<option value="'+value['subject_id']+'">'+value['subject_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
     str+='</select>';
	}
     $('#subject_id').replaceWith(str); 
	 
	 
	 
	 
	 });
 
}
if(id==2)
{

$('#subject_id').attr('disabled', 'disabled');

var d=$('#report_date').val();
var t=$('#teacher_id').val();

var g_url='report/getsubjectbyteacher/'+d+'/'+t;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="subject_id" id="subject_id" onchange="change_subject()" ><option value="">-Please Select-</option>';
	if(result.subject!=false)
	{
	$.each(result.subject, function(index, value) {
	str+='<option value="'+value['subject_id']+'">'+value['subject_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
     str+='</select>';
	}
     $('#subject_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}

if(id==3)
{
var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';

gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
	var t=$('#teacher_id').val();
	var g_url='report/createpdf/'+t+'/'+login_type+'/'+login_id;
    $.getJSON(g_url,function(result)
	{
		if(result.status==0)
		{
			var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';

gradestr+='Error Occured Please Try Again.</td></tr></table>';    
	$('#material').replaceWith(gradestr); 

		
		
		}
		else if(result.status==2)
		{
		
		
		var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';

gradestr+='Observation Plan Not Completed.</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
		
		
		}
		else
		{
		
		var gradestr='<table id="material" border="0" style="float: left;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'observationplans/temp_'+login_type+'_'+login_id+'.pdf" target="_blank" style="color: #02AAD2;">ObservationPlan.pdf</a></td></tr><tr><td>';

gradestr+='<object  data="'+WORKSHOP_DISPLAY_FILES+'observationplans/temp_'+login_type+'_'+login_id+'.pdf" type="application/pdf"  width="650" height="220"></object> </td></tr></table>';    
	$('#material').replaceWith(gradestr); 
		
		
		}
	
	});
}


}

function doit()
{
$("select[name='lesson_correlation'] option[value='']").attr("selected", true);
var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
gradestr+='</td></tr></table>';    
$('#material').replaceWith(gradestr); 

}
function change_subject()
{
$("select[name='grade_id'] option[value='']").attr("selected", true);
if($('#lesson_correlation').val()!=3)
{
var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
gradestr+='</td></tr></table>';    
$('#material').replaceWith(gradestr); 
}
}
function showm(v)
{
if(v=='m')
{
$('.materialshow').show();
$('.lessonshow').hide();
}
else if(v=='l')
{
$('.materialshow').hide();
$('.lessonshow').show();

}

}
function selectmaterial()
{
var subject_id= $('#subject_id').val();
var grade_id=$('#grade_id').val();
var school_id=$('#school_id').val();
if($('#lesson_correlation').val()==2)
{

var g_url='lesson_plan_material/getmaterial/'+subject_id+'/'+grade_id+'/'+school_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='';
	gradestr+='<table id="material" border="0" style="float: left;"><tr><td><b>Show In The Viewer:</b><input type="radio" name="materialid" checked value="m" onclick="showm(this.value)">Material&nbsp;&nbsp;<input type="radio" name="materialid" value="l" onclick="showm(this.value)">Lesson Plan</td></tr><tr><td class="materialshow">';
	if(result.materials!=false)
	{
	
	var c=(result.materials).length;
	
	$.each(result.materials, function(index, value) {
	
	if(index==0)
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table   id="book_'+index+'"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="200"></object> </td></tr>';
	if(c>1)
	{
	gradestr+="<tr><td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td></tr>";
	}
	gradestr+='</table>';
	}
	else
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table   id="book_'+index+'" style="display:none;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="200"></object> </td></tr>';
	gradestr+="<tr><td><input type='button' name='previous' id='previous' value='<<' onclick='previouspage("+index+");'></td>";
	if(index<(c-1))
	{
	gradestr+="<td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td>";
	}
	gradestr+='</tr></table>';
	}
	
	});
	
    }
	else
	{
     gradestr+='<table ><tr><td>';
	 gradestr+='No Materials Found.</td></tr><tr><td>';
	}
	gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
	 
	 
	 
	 
	 
	 });
	 
	 // lesson plan pdf 
	 
	 var t=$('#teacher_id').val();
	 var s=$('#subject_id').val();
	 var d=$('#report_date').val();
	var g_url='report/createlessonplanpdf/'+t+'/'+login_type+'/'+login_id+'/'+s+'/'+d;
    $.getJSON(g_url,function(result)
	{
		if(result.status==0)
		{
			var gradestr='<table  class="lessonshow" style="display:none;" border="0" style="float: left;"><tr><td>';

gradestr+='Error Occured Please Try Again.</td></tr></table>';    
	 $("#material").append(gradestr);

		
		
		}
		
		else
		{
		
		var gradestr='<table  class="lessonshow" style="display:none;" border="0" style="float: left;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'observation_lessonplan/temp_'+login_type+'_'+login_id+'.pdf" target="_blank" style="color: #02AAD2;">LessonPlan.pdf</a></td></tr><tr><td>';

gradestr+='<object  data="'+WORKSHOP_DISPLAY_FILES+'observation_lessonplan/temp_'+login_type+'_'+login_id+'.pdf" type="application/pdf"  width="650" height="220"></object> </td></tr></table>';    
	 $("#material").append(gradestr);
		
		
		}
	
	});
	 
	 }	 
	 else
	 {
	 if($('#lesson_correlation').val()==1)
{

var g_url='lesson_plan_material/getmaterial/'+subject_id+'/'+grade_id+'/'+school_id;
    $.getJSON(g_url,function(result)
	{
	var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
	if(result.materials!=false)
	{
	var c=(result.materials).length;
	
	$.each(result.materials, function(index, value) {
	
	if(index==0)
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="200"></object> </td></tr>';
	if(c>1)
	{
	gradestr+="<tr><td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td></tr>";
	}
	gradestr+='</table>';
	}
	else
	{
	var s=value['file_path'];
	var f=s.split('.pdf');
	gradestr+='<table id="book_'+index+'" style="display:none;"><tr><td><a href="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" target="_blank" style="color: #02AAD2;">Material Name:'+value['name']+'.pdf</a></td></tr>';
	
	gradestr+='<tr><td align="center" id="'+f[0]+'" onclick="openbook('+f[0]+')"><object  data="'+WORKSHOP_DISPLAY_FILES+'medialibrary/lesson_plan/'+f[0]+'.pdf" type="application/pdf"  width="650" height="200"></object> </td></tr>';
	gradestr+="<tr><td><input type='button' name='previous' id='previous' value='<<' onclick='previouspage("+index+");'></td>";
	if(index<(c-1))
	{
	gradestr+="<td><input type='button' name='next' id='next' value='>>' onclick='nextpage("+index+");'></td>";
	}
	gradestr+='</tr></table>';
	}
	
	});
	
    }
	else
	{
     
	 gradestr+='No Materials Found.</td></tr><tr><td>';
	}
	gradestr+='</td></tr></table>';    
	$('#material').replaceWith(gradestr); 
	 
	 
	 
	 
	 
	 });
	 } else  if($('#lesson_correlation').val()!=3){
	 var gradestr='<table id="material" border="0" style="float: left;"><tr><td>';
	 
	 gradestr+='</td></tr></table>';
	 $('#material').replaceWith(gradestr); 
	 }
	 
	 }

}
function nextpage(i)
{

var j=i+1;
document.getElementById('book_'+i).style.display='none';
document.getElementById('book_'+j).style.display='';
}
function previouspage(i)
{

var j=i-1;
document.getElementById('book_'+i).style.display='none';
document.getElementById('book_'+j).style.display='';
}