$(document).ready(function() {
$("#parentform").validate({

rules: {
parentfirstname:{
required: true,
maxlength:30
},
parentlastname:{
required: true,
maxlength:30
},
username:{
required: true,
maxlength:50
},
password:{
required: true,
maxlength:20
},
email:{
email:function(element){if($("#email").val()!=''){ return true ;} else {return false;}},
maxlength:60
}
,
language:{
required: true
},
firstnamesub:{
required: function(element){if($("#lastnamesub").val()!='' ||  $("#languagesub").val()!='' ){ return true ;} else {return false;}},
maxlength:30
},
lastnamesub:{
required: function(element){if($("#firstnamesub").val()!='' ||  $("#languagesub").val()!='' ){ return true ;} else {return false;}},
maxlength:30
},
emailsub:{
email:function(element){if($("#emailsub").val()!=''){ return true ;} else {return false;}},
maxlength:60
},
languagesub:{
required: function(element){if($("#firstnamesub").val()!='' || $("#lastnamesub").val()!='' ){ return true ;} else {return false;}}
}

},
messages: {
parentfirstname:{
required: "Please enter First Name"
},
parentlastname:{
required: "Please enter Last Name"
},username:{
required: "Please enter Username"
},password:{
required: "Please enter Password"
}
,
email:{
required: "Please enter email"
},
language:{
required: "Please Select Language"
}
,firstnamesub:{
required: "Please enter First Name"
},
lastnamesub:{
required: "Please enter Last Name"
}
,
emailsub:{
required: "Please enter email"
}
,
languagesub:{
required: "Please Select Language"
}
}
,
errorElement:"div" 

});



var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
//var page =1;
//$('#pageid').val(page);
//$.get("parents/getparents/"+page+"?num=" + Math.random(), function(msg){
//$msgContainer.html(msg);
//$('#parentdetails').show();
//loading_hide();
//});

// onclick  pagination Load
$('#msgContainer .parents li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);

$.get("parents/getparents/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});


/*$('#parentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'parent/add_parent',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesparentJson 
    }); */
			 
	$('#parentcancel').click( function() {
	$('#parentdialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function parent_add()

		{
		parentadd();	 
		$('#parentmessage').text('');	 		
		$('#parentmessage').hide();	 		
		$('#parentfirstname').val('');		
		$('#parentlastname').val('');
       	$('#email').val('');
		$('#language').val('');
		$('#username').val('');
		$('#password').val('');
		$('#firstnamesub').val('');
		$('#lastnamesub').val('');
       	$('#emailsub').val('');
		$('#languagesub').val('');
		
		
		$('#parentupdate').replaceWith('<input class="btnbig" type="submit" name="submit" id="parentadd" value="Add" onclick="parentadd()">');
		//$('#parentadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="parentadd" value="Add" onclick="parentadd()">');
		$("#parentdialog").dialog({
			modal: true,
           	height: 600,
			width: 500
			});
				
		
		
		}
		
                
function parentadd()
{

$('#parentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'parents/add_parent',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesparentJsonadd 
    });

}		

function parentedit(parent_id)
{
	parentupdate();
	$('#parentmessage').text('');	 		
	$('#parentmessage').hide();
	$('#parentfirstname').val('');
	
		$('#parentlastname').val('');
       	$('#email').val('');
		$('#language').val('');
		$('#username').val('');
		$('#password').val('');
		$('#firstnamesub').val('');
		$('#lastnamesub').val('');
       	$('#emailsub').val('');
		$('#languagesub').val('');
	var p_url='parents/getparentinfo/'+parent_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#parentfirstname').val(result.parent.firstname);
	
	$('#parentlastname').val(result.parent.lastname);	
	$('#email').val(result.parent.email);
	$('#username').val(result.parent.username);
	$('#password').val('torvertex');
	$('#language').val(result.parent.language);
	$('#firstnamesub').val(result.parent.firstnamesub);
	$('#lastnamesub').val(result.parent.lastnamesub);	
	$('#emailsub').val(result.parent.emailsub);
	$('#languagesub').val(result.parent.languagesub);
	$('#parent_id').val(result.parent.parents_id);
	$('#parentadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="parentupdate" value="Update" onclick="parentupdate()">');
		
	
	
	}); 
	
	
			 
	$("#parentdialog").dialog({
			modal: true,
           	height: 600,
			width: 500
			});
			
			
	$('#parentcancel').click( function() {
	$('#parentdialog').dialog('close');
	});	
		

}
function parentdelete(parent_id)
{


	if(window.confirm("Are you sure you want to delete this parent?")==false){
			return;
		}
		
		
		var p_url = 'parents/delete/'+parent_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            
			$.get("parents/getparents/"+page+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function parentupdate()
{

 $('#parentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'parents/update_parent',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesparentJsonupdate 
    }); 



}

function procesparentJsonadd(data)
{
if(data.status==0)
 {
     $('#parentmessage').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 
			$.get("parents/getparents/"+page+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		
		$('#parentdialog').dialog('close');
       
    });
	
  }
  $('#parentmessage').show();
}
function procesparentJsonupdate(data)
{
if(data.status==0)
 {
     $('#parentmessage').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     
	$.get("parents/getparents/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);		
		$('#parentdialog').dialog('close');
       
    });
	
  }
  $('#parentmessage').show();
} 

$('#add_new_parent').click(function(){
    $.ajax({
        type: 'post',
    url: base_url+'parents/add_parent',
    data: $("#frmaddparent").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      //$('#parentlist').append(data);
      if (data.status==1)
      {
          location.reload(true);
      } else {
          alert(data.message);
      }
          });
});

function editparent(parents_id){
    $.ajax({
        type: 'post',
    url: base_url+'parents/getparentinfo/'+parents_id
    })
  .done(function( data ) {
      result = jQuery.parseJSON(data);
        $('#parentfirstname').val(result.parent.firstname);
	$('#parentlastname').val(result.parent.lastname);	
	$('#email').val(result.parent.email);
	$('#username').val(result.parent.username);
	$('#password').val('torvertex');
	$('#language').val(result.parent.language).trigger("liszt:updated");
	$('#firstnamesub').val(result.parent.firstnamesub);
	$('#lastnamesub').val(result.parent.lastnamesub);	
	$('#emailsub').val(result.parent.emailsub);
	$('#phone_val').val(result.parent.phone);
	$('#phonesub_val').val(result.parent.phonesub);
	$('#languagesub').val(result.parent.languagesub).trigger("liszt:updated");
	$('#parent_id').val(result.parent.parents_id);
          });
          
}

$('#editparent').click(function (){
    $.ajax({
        type: 'post',
    url: base_url+'parents/update_parent',
    data: $("#parentform").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      //$('#parentlist').append(data);
      if (data.status==1)
      {
          location.reload(true);
      } else {
          alert(data.message);
      }
          });
});
function deleteparent(parent_id){
    
    $.ajax({
        type: 'post',
    url: base_url+'parents/delete/'+parent_id
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      //$('#parentlist').append(data);
      if (data.status==1)
      {
          location.reload(true);
      } else {
          alert(data.message);
      }
          });
}