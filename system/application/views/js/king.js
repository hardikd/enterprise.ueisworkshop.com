$(document).ready(function() {

$.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    }, "Student Number must contain only letters, numbers.");

$("#studentform").validate({

rules: {
firstname:{
required: true,
maxlength:30
},
lastname:{
required: true,
maxlength:30
},
schoolname:{
required: true,
maxlength:30
},
subject:{
required: true,
maxlength:30
},


totalmarks:{
required: true,
maxlength:30
},
obtainedmark:{
required: true,
maxlength:30
},

grade:{
required: true
}
,
student_number:{
required: true,
alphanumeric: true,
maxlength:15
}
},
messages: {
firstname:{
required: "Please enter First Name"
},
lastname:{
required: "Please enter Last Name"
},
schoolname:{
required: "Please enter School Name"
},
subject:{
required: "Please enter Subject "
},

totalmarks:{
required: "Please enter Total Marks "
},
obtainedmark:{
required: "Please enter Obtained Mark "
},


grade:{
required: "Please Select Grade"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("addkinder/getteachers/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#studentdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .student li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);

$.get("parents/getteachers/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});



$('#student_add').click(function()
		{
		studentadd();	 
		$('#message').text('');	 		
		$('#message').hide();	 		
		$('#firstname').val('');		
		$('#lastname').val('');
		$("select[name='grade'] option[value='']").attr("selected", true);		
		$('#student_number').val('');		
		$('#studentupdate').replaceWith('<input class="btnbig" type="submit" name="submit" id="studentadd" value="Add" onclick="studentadd()">');
		//$('#studentadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="studentadd" value="Add" onclick="studentadd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 500,
			width: 500
			});
				
		
		
		}); 
		
/*$('#studentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'student/add_student',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtudentJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function studentadd()
{

$('#studentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'addkinder/add_teacher',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtudentJsonadd 
    });

}		

function studentedit(student_id)
{
	studentupdate();
	$('#message').text('');	 		
	$('#message').hide();
	$('#loginname').val('aa');
	$('#passwordid').val('aa');
	
	$('#logid').hide();
	$('#passid').hide();
	
	$('#firstname').val('');
	
		$('#lastname').val('');
       	
	var p_url='addkinder/getstudentinfo/'+student_id;
    $.getJSON(p_url,function(result)
	{
	
	
	
	$('#student_id').val(result.teacher.kindergarten_id);	
	$('#firstname').val(result.teacher.firstname);	
	$('#lastname').val(result.teacher.lastname);
	$('#schoolname').val(result.teacher.school_id);
	
	$('#subject').val(result.teacher.subject_id);
	$('#grade').val(result.teacher.grade);		
	$('#student_number').val(result.teacher.kindergarten_number);	 			
	$('#obtainedmark').val(result.teacher.obtained_marks);
	$('#totalmarks').val(result.teacher.total_marks);
	$('#gender').val(result.teacher.gender).attr('checked', true);
	
	
	$('#studentadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="studentupdate" value="Update" onclick="studentupdate()">');
	
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 500,
			width: 500
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	window.location.reload();
	});	
		

}
function studentdelete(student_id)
{


	if(window.confirm("Are you sure you want to delete this student?")==false){
			return;
		}		
		
		var p_url = 'addkinder/delete_student/'+student_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){	
		
		window.location.reload();
		 
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            
			$.get("addkinder/getteachers/"+page+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		  
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function studentupdate()
{

 $('#studentform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'addkinder/update_student',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtudentJsonupdate 
    }); 
}

function processtudentJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
  window.location.reload();
 
 
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 
			$.get("addkinder/getteachers/"+page+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processtudentJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
 window.location.reload();
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     
	$.get("addkinder/getteachers/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);		
		$('#dialog').dialog('close');
       window.location.reload();
    });
	
  }
  $('#message').show();
} 