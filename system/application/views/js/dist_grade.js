$(document).ready(function() {
$("#dist_gradeform").validate({

rules: {
grade_name:{
required: true,
maxlength:80
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
grade_name:{
required: "Please Enter Grade Name"
},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_grade/getdist_grades/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#dist_gradedetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .dist_grades li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_grade/getdist_grades/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_grade/getdist_grades/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#dist_grade_add').click(function()
		{
			 
			dist_gradeadd(); 
		$('#message').hide();			
		$('#grade_name').val('');
		$('#dist_grade_id').val('');
		
	
	
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 var sstr='<select class="combobox" name="dist_grade_type_id" id="dist_grade_type_id"><option value="">-Select-</option></select>';
		$('#dist_grade_type_id').replaceWith(sstr);
		$("select[name='dist_grade_type_id'] option[value='']").attr("selected", true);
	 
	 });
	
		
		$('#dist_gradeupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_gradeadd" value="Add" onclick="dist_gradeadd()">');
		//$('#dist_gradeadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_gradeadd" value="Add" onclick="dist_gradeadd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#dist_gradeform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_grade/add_dist_grade',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_gradeJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function dist_gradeadd()
{

$('#dist_gradeform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_grade/add_dist_grade',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_gradeJsonadd 
    });

}		

function dist_gradeedit(dist_grade_id)
{
	dist_gradeupdate();
	$('#message').hide();
	var p_url='dist_grade/getdist_gradeinfo/'+dist_grade_id;
    $.getJSON(p_url,function(result)
	{
	
	$('#grade_name').val(result.dist_grade.grade_name);
	$('#dist_grade_id').val(result.dist_grade.dist_grade_id);
	
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.dist_grade.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.dist_grade.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.dist_grade.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.dist_grade.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.dist_grade.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	
	
	 
	 });
	$('#dist_gradeadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="dist_gradeupdate" value="Update" onclick="dist_gradeupdate()">');
	
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function dist_gradedelete(dist_grade_id)
{


	if(window.confirm("Are you sure you want to delete this dist_grade?")==false){
			return;
		}
		
		
		var p_url = 'dist_grade/delete/'+dist_grade_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("dist_grade/getdist_grades/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function dist_gradeupdate()
{

 $('#dist_gradeform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'dist_grade/update_dist_grade',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdist_gradeJsonupdate 
    }); 



}

function procesdist_gradeJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
   var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("dist_grade/getdist_grades/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesdist_gradeJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("dist_grade/getdist_grades/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$("select[name='district'] option[value='"+district_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 