$(document).ready(function() {
$("#grade_subjectform").validate({

rules: {

subject_id:{required: true},
grade_id:{required: true}
},
messages: {
name:{
required: "Please Enter Class Room Name"
},
subject_id:{required: "Please Select Subject"},
grade_id:{required: "Please Select Grade"}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
/*var page =1;
$('#pageid').val(page);
var grade_id=$('#grade').val();
$.get("grade_subject/getgrade_subjects/"+page+"/"+grade_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#grade_subjectdetails').show();
loading_hide();
});*/

// onclick  pagination Load
$('#msgContainer .grade_subjects li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
var grade_id=$('#grade').val();
$('#pageid').val(page);

$.get("grade_subject/getgrade_subjects/"+page+"/"+grade_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool').click(function(){
loading_show(); 
var page = 1;
var grade_id=$('#grade').val();
$('#pageid').val(page);

$.get("grade_subject/getgrade_subjects/"+page+"/"+grade_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#grade_subject_add').click(function()
		{
		grade_subjectadd();	 
		$('#message').hide();	 		
		
		$('#grade_subject_id').val('');		
		
		$('#grade_subjectupdate').replaceWith('<input class="btnbig" type="submit" name="submit" id="grade_subjectadd" value="Add" onclick="grade_subjectadd()">');
		//$('#grade_subjectadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="grade_subjectadd" value="Add" onclick="grade_subjectadd()">');
		var s_url='class_room/getsubjectbydistrict/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="subject_id" id="subject_id"><option value="">-Select-</option>';
	if(sresult.subject!=false)
	{
	$.each(sresult.subject, function(index, value) {
	str+='<option value="'+value['subject_id']+'">'+value['subject_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#subject_id').replaceWith(str); 
	 $("select[name='subject_id'] option[value='']").attr("selected", true);
	});
	
	//grades
	var s_url='class_room/getallgrades/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="grade_id" id="grade_id"><option value="">-Select-</option>';
	if(sresult.grade!=false)
	{
	$.each(sresult.grade, function(index, value) {
	str+='<option value="'+value['grade_id']+'">'+value['grade_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#grade_id').replaceWith(str); 
	 $("select[name='grade_id'] option[value='']").attr("selected", true);
	});
		$("#dialog").dialog({
			modal: true,
           	height: 300,
			width: 500
			});
				
		
		
		}); 
		
/*$('#grade_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'grade_subject/add_grade_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesgrade_subjectJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function grade_subjectadd()
{

$('#grade_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'grade_subject/add_grade_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesgrade_subjectJsonadd 
    });

}		

function grade_subjectedit(grade_subject_id)
{
	grade_subjectupdate();
	$('#message').hide();
	var p_url='grade_subject/getgrade_subjectinfo/'+grade_subject_id;
    $.getJSON(p_url,function(result)
	{
			   
	
	
	$('#grade_subject_id').val(result.grade_subject.grade_subject_id);
	
	$('#grade_subjectadd').replaceWith('<button data-dismiss="modal" class="btn btn-success" type="submit" name="submit" id="grade_subjectupdate" value="Update" onclick="grade_subjectupdate()"><i class="icon-save"></i> Save Changes</button>');
		
	
	var s_url='class_room/getsubjectbydistrict/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="subject_id" id="subject_id"><option value="">-Select-</option>';
	if(sresult.subject!=false)
	{
	$.each(sresult.subject, function(index, value) {
	str+='<option value="'+value['subject_id']+'">'+value['subject_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#subject_id').replaceWith(str); 
	 $("select[name='subject_id'] option[value='"+result.grade_subject.subject_id+"']").attr("selected", true);
	});
	
	//grades
	
	var s_url='class_room/getallgrades/';
    $.getJSON(s_url,function(sresult)
	{
	var str='<select name="grade_id" id="grade_id"><option value="">-Select-</option>';
	if(sresult.grade!=false)
	{
	$.each(sresult.grade, function(index, value) {
	str+='<option value="'+value['grade_id']+'">'+value['grade_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#grade_id').replaceWith(str); 
	 $("select[name='grade_id'] option[value='"+result.grade_subject.grade_id+"']").attr("selected", true);
	});
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 300,
			width: 500
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function grade_subjectdelete(grade_subject_id)
{
	if(window.confirm("Are you sure you want to delete this grade_subject?")==false){
			return;
		}
		var p_url = 'grade_subject/delete/'+grade_subject_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var grade_id=$('#grade').val();
			$.get("grade_subject/getgrade_subjects/"+page+"/"+grade_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function grade_subjectupdate()
{

 $('#grade_subjectform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'grade_subject/update_grade_subject',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesgrade_subjectJsonupdate 
    }); 



}

function procesgrade_subjectJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 var grade_id=$('#grade_id').val();
			$.get("grade_subject/getgrade_subjects/"+page+"/"+grade_id+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		$("select[name='grade'] option[value='"+grade_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesgrade_subjectJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
      var grade_id=$('#grade_id').val();
	$.get("grade_subject/getgrade_subjects/"+page+"/"+grade_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		$("select[name='grade'] option[value='"+grade_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 