$(document).ready(function() {
$("#planform").validate({

rules: {
tab:{
required: true
}

},
messages: {
tab:{
required: "Please Enter tab"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("lessonplan/getlessonplans/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#plandetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .lessonplan li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
$.get("lessonplan/getlessonplans/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#plan_add').click(function()
		{
			 
			planadd(); 
		$('#message').hide();			
		$('#tab').val('');
		
	
	
	
		
		$('#planupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planadd" value="Add" onclick="planadd()">');
		//$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/add_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function planadd()
{

$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'lessonplan/add_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonadd 
    });

}		

function planedit(plan_id)
{
	planupdate();
	$('#message').hide();
	var p_url='lessonplan/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
			 
	$('#tab').val(result.lessonplan.tab);
	$('#plan_id').val(result.lessonplan.lesson_plan_id);
	 
	 });
	$('#planadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planupdate" value="Update" onclick="planupdate()">');
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function plandelete(plan_id)
{


	if(window.confirm("Are you sure you want to delete this tab?")==false){
			return;
		}
		
		
		var p_url = 'lessonplan/delete/'+plan_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
           
$.get("lessonplan/getlessonplans/"+page+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function planupdate()
{

 $('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'lessonplan/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}

function processchoolJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
  
	
$.get("lessonplan/getlessonplans/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    
	
$.get("lessonplan/getlessonplans/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 