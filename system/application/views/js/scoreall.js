$(document).ready(function() {
$("#scoreform").validate({

rules: {
score_name:{
required: true
}
},
messages: {
score_name:{
required: "Please enter score Name"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("score/getscores/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#scoredetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .scores li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
$.get("score/getscores/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#score_add').click(function()
		{
			 
		scoreadd();
		$('#message').hide();		
		$('#score_name').val('');
		$('#score_id').val('');
		$('#scoreupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="scoreadd" value="Add" onclick="scoreadd()">');
		
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#scoreform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'score/add_score',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processcoreJson 
    });*/ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function scoreadd()
{

$('#scoreform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'score/add_score',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processcoreJsonadd 
    });

}		

function scoreedit(score_id)
{
	scoreupdate();
	$('#message').hide();
	var p_url='score/getscoreinfo/'+score_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#score_name').val(result.score.score_name);
	
	$('#score_id').val(result.score.score_id);
	$('#scoreadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="scoreupdate" value="Update" onclick="scoreupdate()">');
		
	
	
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function scoredelete(score_id)
{


	if(window.confirm("Are you sure you want to delete this score?")==false){
			return;
		}
		
		
		var p_url = 'score/delete/'+score_id;
		$.getJSON(p_url,function(data) {
		if(data.score==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            $.get("score/getscores/"+page+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function scoreupdate()
{

 $('#scoreform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'score/update_score',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processcoreJsonupdate 
    }); 



}

function processcoreJsonadd(data)
{
if(data.score==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.score==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    $.get("score/getscores/"+page+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
}
function processcoreJsonupdate(data)
{
if(data.score==0)
 {
     $('#message').text(data.message);
	 $('#message').show();
 
 }
 else if(data.score==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    $.get("score/getscores/"+page+"?num=" + Math.random(), function(msg){
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  
} 