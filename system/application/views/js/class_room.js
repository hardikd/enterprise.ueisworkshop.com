$(document).ready(function() {
$("#class_roomform").validate({

rules: {
name:{
required: true,
maxlength:80
},
school_id:{required: true},
grade_id:{required: true}
},
messages: {
name:{
required: "Please Enter Class Room Name"
},
school_id:{required: "Please Select School"},
grade_id:{required: "Please Select Grade"}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
//var page =1;
//$('#pageid').val(page);
//var school_id=$('#school').val();
//$.get("class_room/getclass_rooms/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
//$msgContainer.html(msg);
//$('#class_roomdetails').show();
//loading_hide();
//});

// onclick  pagination Load
$('#msgContainer .class_rooms li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
if($('#school').val())
    var school_id=$('#school').val();
else 
    var school_id = 'all';
$('#pageid').val(page);

$.get("class_room/getclass_rooms/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool').click(function(){
loading_show(); 
var page = 1;
var school_id=$('#school').val();
$('#pageid').val(page);

$.get("class_room/getclass_rooms/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#class_room_add').click(function()
		{
		class_roomadd();	 
		$('#message').hide();	 		
		$('#name').val('');		
		$('#class_room_id').val('');		
		
		$('#class_roomupdate').replaceWith('<input class="btnbig" type="submit" name="submit" id="class_roomadd" value="Add" onclick="class_roomadd()">');
		//$('#class_roomadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="class_roomadd" value="Add" onclick="class_roomadd()">');
		
	
	//grades
	var s_url='class_room/getallgrades/';
		$.getJSON(s_url,function(sresult)
	{
	var str='<select name="grade_id" id="grade_id"><option value="">-Select-</option>';
	if(sresult.grade!=false)
	{
	$.each(sresult.grade, function(index, value) {
	str+='<option value="'+value['grade_id']+'">'+value['grade_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#grade_id').replaceWith(str); 
	 $("select[name='grade_id'] option[value='']").attr("selected", true);
	});
		$("#dialog").dialog({
			modal: true,
           	height: 300,
			width: 500
			});
	}); 
		
/*$('#class_roomform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'class_room/add_class_room',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesclass_roomJson 
    }); */
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function class_roomadd()
{

$('#class_roomform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'class_room/add_class_room',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesclass_roomJsonadd 
    });

}		

function class_roomedit(class_room_id)
{
	class_roomupdate();
	$('#message').hide();
	var p_url='class_room/getclass_roominfo/'+class_room_id;
    $.getJSON(p_url,function(result)
	{
			   
	$('#name').val(result.class_room.name);
	
	$('#class_room_id').val(result.class_room.class_room_id);
	
	$('#class_roomadd').replaceWith('<input class="btnbig" type="submit" name="submit" id="class_roomupdate" value="Update" onclick="class_roomupdate()">');
		
	
	
	
	//grades
	
	var s_url='class_room/getallgrades/';
    $.getJSON(s_url,function(sresult)
	{
	var str='<select name="grade_id" id="grade_id"><option value="">-Select-</option>';
	if(sresult.grade!=false)
	{
	$.each(sresult.grade, function(index, value) {
	str+='<option value="'+value['grade_id']+'">'+value['grade_name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#grade_id').replaceWith(str); 
	 $("select[name='grade_id'] option[value='"+result.class_room.grade_id+"']").attr("selected", true);
	});
	}); 
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 300,
			width: 500
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function class_roomdelete(class_room_id)
{

	if(window.confirm("Are you sure you want to delete this class_room?")==false){
			return;
		}
		
		
		var p_url = 'class_room/delete/'+class_room_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            var school_id=$('#school').val();
			$.get("class_room/getclass_rooms/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function class_roomupdate()
{

 $('#class_roomform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'class_room/update_class_room',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesclass_roomJsonupdate 
    }); 



}

function procesclass_roomJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	 var school_id=$('#school_id').val();
			$.get("class_room/getclass_rooms/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
    
		$msgContainer.html(msg);
		//$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function procesclass_roomJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
     var school_id=$('#school_id').val();
	$.get("class_room/getclass_rooms/"+page+"/"+school_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		//$("select[name='school'] option[value='"+school_id+"']").attr("selected", true);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 