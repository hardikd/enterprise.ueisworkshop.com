$(document).ready(function() {
$("#planform").validate({

rules: {
tab:{
required: true
},
state_id:{
required: true
},
country_id:{
required: true
},
district_id:{
required: true
}

},
messages: {
tab:{
required: "Please Enter tab"
},
state_id:{
required: "Please Select State"
},
country_id:{
required: "Please Select Country"
},
district_id:{
required: "Please Enter Districts"
}
}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
//$('#pageid').val(page);
//var state_id=$('#states').val();
//var country_id=$('#countries').val();
//var district_id=$('#district').val();
//$.get("school_type/getschool_types/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
//$msgContainer.html(msg);
//$('#plandetails').show();
//loading_hide();
//});

// onclick  pagination Load
$('#msgContainer .school_type li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
$.get("school_type/getschool_types/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#getschool_type').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();

$.get("school_type/getschool_types/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#plan_add').click(function()
		{
                   
			 
			planadd(); 
		$('#message').hide();			
		$('#tab').val('');
		
		
	
	  var g_url='countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 
	 var str='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
	 str+='</select>';
	 $('#district_id').replaceWith(str); 
	 $("select[name='district_id'] option[value='']").attr("selected", true);
	 
	 });
	
		
		$('#planupdate').replaceWith('<button type="submit" name="submit" id="planadd" value="Add" onclick="planadd()" data-dismiss="modal" class="btn btn-success"><i class="icon-plus"></i> Add Teacher</button>');
		//$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		  
		
		
		$("#dialog").dialog({
			modal: true,
           	height: 270,
			width: 500
			});
				
		
		
		}); 
		
/*$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/add_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function planadd()
{

$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school_type/add_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonadd 
    });

}		

function planedit(plan_id)
{
	planupdate();
	$('#message').hide();
	var p_url='school_type/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
			 
	$('#tab').val(result.school_type.tab);
	
	$('#plan_id').val(result.school_type.school_type_id);
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="states_select_option(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.school_type.country_id+"]").attr("selected", true);
	 
	 //states
	  var g_url='countries/getStates/'+result.school_type.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id" onchange="district_select_option(this.value)"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 
	 
	 $("select[name='state_id'] option[value="+result.school_type.state_id+"]").attr("selected", true);
	 
	 });
	 // End OF States
	 // districts
	 
	 var s_url='district/getDistrictsByStateId/'+result.school_type.state_id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<select class="combobox" name="district_id" id="district_id"><option value="">-Select-</option>';
		if(sresult.district!=false)
	{
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	
	});
	sstr+='</select>';
	
	}
	else
	{
     sstr+='</select>';
	}
     $('#district_id').replaceWith(sstr); 
	 $("select[name='district_id'] option[value="+result.school_type.district_id+"]").attr("selected", true);
	});
	 
	  
	 
	 // End OF Districts
	 
	 
	
	 
	 });
	 
	 });
	$('#planadd').replaceWith('<button type="submit" name="submit" id="planupdate" value="Update" onclick="planupdate()" data-dismiss="modal" class="btn btn-success"><i class="icon-save"></i> Save Changes</button>');
			 
	$("#dialog").dialog({
			modal: true,
           	height: 270,
			width: 500
			});
                        
	$("#dialog").dialog({
			modal: true,
           	height: 270,
			width: 500
			});		
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function plandelete(plan_id)
{


	if(window.confirm("Are you sure you want to delete this tab?")==false){
			return;
		}
		
		
		var p_url = 'school_type/delete/'+plan_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
			var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
           
$.get("school_type/getschool_types/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function planupdate()
{

 $('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school_type/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}

function processchoolJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
	var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
  
	
$.get("school_type/getschool_types/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    
     var page = 1;
     var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
var district_id=$('#district_id').val();
states_select(country_id);
district_all(state_id);
	
$.get("school_type/getschool_types/"+page+"/"+state_id+"/"+country_id+"/"+district_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 

$('#addschooltype').click(function(){
    
    $.ajax({
        type: 'post',
    url: base_url+'school_type/add_plan',
    data: $("#add_schooltype").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          $('#myModal-add-new-parent').modal('hide');
          $('#successbtn').modal('show');
          
          
        
      }
      
    });
});

function editscholltype(plan_id)
{
	var p_url=base_url+'school_type/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
			 
	$('#tab').val(result.school_type.tab);
	
	$('#plan_id').val(result.school_type.school_type_id);
	 
	 });

}
$('#edit_plan').click(function(){
    
    $.ajax({
        type: 'post',
    url: base_url+'school_type/update_plan',
    data: $("#editplan").serialize()
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          $('#myModal-add-new-parent').modal('hide');
          $('#successbtn').modal('show');
          
          
        
      }
    
    });
});

function deleteplan(plan_id){
    $.ajax({
        type: 'post',
        url: base_url+'school_type/delete/'+plan_id
    })
  .done(function( data ) {
      data = jQuery.parseJSON(data);
      if (data.status==0){
          $('#errorbtn').modal('show');
          
      } else if (data.status==1){
          
          $('#successbtn').modal('show');
          
         
        
      }
    
    });
}
