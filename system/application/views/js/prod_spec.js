$(document).ready(function() {
$("#planform").validate({


rules: {
firstname:{
required: true,
maxlength:40
},
lastname:{
required: true,
maxlength:40
},
userid:{
required: true,
maxlength:40
},
password:{
required: true
},
email:{
email: function(element){if($("#email").val()!='' ){ return true ;} else {return false;}},
maxlength:60
}

},
messages: {
firstname:{
required: "Please enter First Name"
},
lastname:{
required: "Please enter Last Name"
},
userid:{
required: "Please enter User Name"
}
,
password:{
required: "Please enter Password"
},

}
,
errorElement:"div" 
});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
$.get("prod_spec/getprod_specs/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#plandetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .prod_spec li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
$.get("prod_spec/getprod_specs/"+page+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});

$('#plan_add').click(function()
		{
			 
			planadd(); 
		$('#message').hide();			
		$('#firstname').val('');
		$('#lastname').val('');
		$('#userid').val('');
		$('#password').val('');
		$('#email').val('');
		
	
	
	
		
		$('#planupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planadd" value="Add" onclick="planadd()">');
		//$('#schooladd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="schooladd" value="Add" onclick="schooladd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'school/add_school',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function planadd()
{

$('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'prod_spec/add_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonadd 
    });

}		

function planedit(plan_id)
{
	planupdate();
	$('#message').hide();
	var p_url='prod_spec/getplaninfo/'+plan_id;
    $.getJSON(p_url,function(result)
	{
			 
	$('#firstname').val(result.prod_spec.firstname);
	$('#lastname').val(result.prod_spec.lastname);
	$('#userid').val(result.prod_spec.userid);
	$('#email').val(result.prod_spec.email);
	$('#password').val('torvertex');
	$('#plan_id').val(result.prod_spec.prod_spec_id);
	 
	 });
	$('#planadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="planupdate" value="Update" onclick="planupdate()">');
	
	
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function plandelete(plan_id)
{


	if(window.confirm("Are you sure you want to delete this Product Specialist?")==false){
			return;
		}
		
		
		var p_url = 'prod_spec/delete/'+plan_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
           
$.get("prod_spec/getprod_specs/"+page+"?num=" + Math.random(), function(msg){
			
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function planupdate()
{

 $('#planform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'prod_spec/update_plan',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processchoolJsonupdate 
    }); 



}

function processchoolJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
  
	
$.get("prod_spec/getprod_specs/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
}
function processchoolJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    
	
$.get("prod_spec/getprod_specs/"+page+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 