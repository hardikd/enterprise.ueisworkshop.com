$(document).ready(function() {
$("#districtform").validate({

rules: {
district_name:{
required: true
},country_id:{
required: true
},
state_id:{
required: true
}

},
messages: {
district_name:{
required: "Please enter District Name"
},country_id:{
required: "Please Select Country"
},
state_id:{
required: "Please Select State"
}

}
,
errorElement:"div" 

});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
$.get("district/getDistricts/"+page+"/"+state_id+"/"+country_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#distdetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .districts li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();

$.get("district/getDistricts/"+page+"/"+state_id+"/"+country_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
$.get("district/getDistricts/"+page+"/"+state_id+"/"+country_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
$('#district_add').click(function()
		{
			 
		districtadd();
		$('#message').hide();		
		$('#district_name').val('');
		$('#district_id').val('');
		$('#districtupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="districtadd" value="Add" onclick="districtadd()">');
		
		var g_url=base_url+'countries/getCountries/';
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="countriesd_change(this.value)" ><option value="">-Select-</option>';
	if(result.countries!=false)
	{
	$.each(result.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value='']").attr("selected", true);
	 $("#copystate").removeAttr("checked");
	 var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	
     str+='</select>';
	
     $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
	 });
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#districtform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'district/add_district',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdistrictJsonadd 
    });*/ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 

});
function districtadd()
{

$('#districtform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:base_url+'district/add_district',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdistrictJsonadd 
    });

}		

function distedit(dist_id)
{
	districtupdate();
	$('#message').hide();
	var p_url='district/getdistrictinfo/'+dist_id;
    $.getJSON(p_url,function(result)
	{
			   
	var g_url='countries/getCountries/';
    $.getJSON(g_url,function(aresult)
	{
	var str='<select class="combobox" name="country_id" id="country_id" onchange="countriesd_change(this.value)" ><option value="">-Select-</option>';
	if(aresult.countries!=false)
	{
	$.each(aresult.countries, function(index, value) {
	str+='<option value="'+value['id']+'">'+value['country']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#country_id').replaceWith(str); 
	 $("select[name='country_id'] option[value="+result.district.country_id+"]").attr("selected", true);
	 
	 });
	 if(result.district.copystate==1)
	 {
	    $("#copystate").attr("checked", "checked"); 
	 
	 }
	 else
	 {
	    $("#copystate").removeAttr("checked");
	 
	 
	 }
	 var g_url='countries/getStates/'+result.district.country_id;
    $.getJSON(g_url,function(sresult)
	{
var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	if(sresult.states!=false)
	{
	$.each(sresult.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value="+result.district.state_id+"]").attr("selected", true);
    });

	$('#district_name').val(result.district.districts_name);
	$('#district_id').val(result.district.district_id);
	$('#districtadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="districtupdate" value="Update" onclick="districtupdate()">');
		
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 250,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function distdelete(dist_id)
{


	if(window.confirm("Are you sure you want to delete this District?")==false){
			return;
		}
		
		
		var p_url = 'district/delete/'+dist_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
             var state_id=$('#states').val();
var country_id=$('#countries').val();
$.get("district/getDistricts/"+page+"/"+state_id+"/"+country_id+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function districtupdate()
{
  
 $('#districtform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'district/update_district',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   procesdistrictJsonupdate 
    }); 



}

function procesdistrictJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
countries_change(country_id)
$.get("district/getDistricts/"+page+"/"+state_id+"/"+country_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 
function procesdistrictJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = $('#pageid').val();
    var state_id=$('#state_id').val();
var country_id=$('#country_id').val();
countries_change(country_id)
$.get("district/getDistricts/"+page+"/"+state_id+"/"+country_id+"?num=" + Math.random(), function(msg){
		$("select[name='countries'] option[value='"+country_id+"']").attr("selected", true);
		$("select[name='states'] option[value='"+state_id+"']").attr("selected", true);
		$msgContainer.html(msg);
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 

function countriesd_change(id)
{
var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
var str='<select class="combobox" name="state_id" id="state_id"><option value="">-Select-</option>';
	if(result.states!=false)
	{
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	});
	str+='</select>';
    }
	else
	{
     str+='</select>';
	}
     $('#state_id').replaceWith(str); 
	 $("select[name='state_id'] option[value='']").attr("selected", true);
    });
}