$(document).ready(function() {
//$("#standarddataform").validate({
//
//rules: {
//grade:{
//required: true
//},
//strand:{
//required: true
//},
//standard:{
//required: true
//},
//standard_id:{
//required: true
//}
//
//},
//messages: {
//grade:{
//required: "Please Select Grade"
//},
//strand:{
//required: "Please Enter Strand"
//},
//standard:{
//required: "Please Enter Standard"
//}
//}
//,
//errorElement:"div" 
//
//});

var $msgContainer = $("div#msgContainer");
loading_show(); 

//deafault load
var page =1;
$('#pageid').val(page);

var grade_id=$('#grade_id').val();
var standards=$('#standards').val();

	
$.get("standarddata/getstandarddatas/"+page+"/"+standards+"/"+grade_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
$('#standarddatadetails').show();
loading_hide();
});

// onclick  pagination Load
$('#msgContainer .standarddatas li.active').live('click',function(){
loading_show(); 
var page = $(this).attr('p');
$('#pageid').val(page);

var grade_id=$('#grade_id').val();
var standards=$('#standards').val();

	
$.get("standarddata/getstandarddatas/"+page+"/"+standards+"/"+grade_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});


$('#standarddata_add').click(function()
		{
                   
			 
			standarddataadd(); 
		$('#message').hide();			
		$("select[name='grade'] option[value='']").attr("selected", true);
		$('#strand').val('');
		$('#standard').val('');
		
	
	

		
		$('#standarddataupdate').replaceWith('<input class="btnconf93" type="submit" name="submit" id="standarddataadd" value="Add" onclick="standarddataadd()">');
		//$('#standarddataadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="standarddataadd" value="Add" onclick="standarddataadd()">');
		$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
				
		
		
		}); 
		
/*$('#standarddataform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'standarddata/add_standarddata',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtandarddataJson 
    }); */ 
			 
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
			

function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 
$('#getdistrict').click(function(){
loading_show(); 
var page = 1;

$('#pageid').val(page);
var grade_id=$('#grade_id').val();
var standards=$('#standards').val();

	
$.get("standarddata/getstandarddatas/"+page+"/"+standards+"/"+grade_id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
loading_hide();
});
});
});
function standarddataadd()
{

$('#standarddataform').ajaxForm({ 
   
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:baseurl+'standarddata/add_standarddata',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtandarddataJsonadd 
    });

}		

function standarddataedit(standarddata_id)
{
	standarddataupdate();
	$('#message').hide();
	var p_url='standarddata/getstandarddatainfo/'+standarddata_id;
    $.getJSON(p_url,function(result)
	{
			   
	 $('#standarddata_id').val(standarddata_id);
$("select[name='grade'] option[value='"+result.standarddata.grade+"']").attr("selected", true);	 
	 
		$('#strand').val(result.standarddata.strand);
		$('#standard').val(result.standarddata.standard);
	
	$('#standarddataadd').replaceWith('<input class="btnconf93" type="submit" name="submit" id="standarddataupdate" value="Update" onclick="standarddataupdate()">');
	
	}); 
			 
	$("#dialog").dialog({
			modal: true,
           	height: 350,
			width: 700
			});
			
			
	$('#cancel').click( function() {
	$('#dialog').dialog('close');
	});	
		

}
function standarddelete(standard_id)
{


	if(window.confirm("Are you sure you want to delete this standard?")==false){
			return;
		}
		
		
		var p_url = 'standarddata/delete/'+standard_id;
		$.getJSON(p_url,function(data) {
		if(data.status==1){
		
			var $msgContainer = $("div#msgContainer");
            var page = $('#pageid').val();
            
			var grade_id=$('#grade_id').val();
var standards=$('#standards').val();

	
$.get("standarddata/getstandarddatas/"+page+"/"+standards+"/"+grade_id+"?num=" + Math.random(), function(msg){
		   $msgContainer.html(msg);
		   });
		}
		else
		{
			alert('An error occurred while performing this action. Error is: ' + data.error_msg + '\n\nPlease contact technical support.');
		}
		});

}

function standarddataupdate()
{

 $('#standarddataform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
		url:'standarddata/update_standarddata',
        
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processtandarddataJsonupdate 
    }); 



}

function processtandarddataJsonadd(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
  

var standards=$('#standards').val();

	var grade_id=$('#grade').val();
	 $("select[name='grade_id'] option[value="+grade_id+"]").attr("selected", true);
	
$.get("standarddata/getstandarddatas/"+page+"/"+standards+"/"+grade_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
	
  }
  $('#message').show();
}
function processtandarddataJsonupdate(data)
{
if(data.status==0)
 {
     $('#message').text(data.message);
 
 }
 else if(data.status==1)
 {
    var $msgContainer = $("div#msgContainer");
    var page = 1;
    
var grade_id=$('#grade_id').val();
var standards=$('#standards').val();

	
$.get("standarddata/getstandarddatas/"+page+"/"+standards+"/"+grade_id+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html(msg);
		
		$('#dialog').dialog('close');
       
    });
	
  }
  $('#message').show();
} 