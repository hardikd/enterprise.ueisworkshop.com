<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style>
.tabcontent TD {
  
    padding: 0 7px;
    text-align: center;
  
}
</style>
<script>

var base_url = '<?php echo base_url();?>';

</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php if($this->session->userdata("login_special")=='district_management') { 
      require_once($view_path.'inc/headerv1.php'); 
   }else{
    require_once($view_path.'inc/header.php');
   }
   ?>
   
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
        <?php if($this->session->userdata("login_special")=='district_management') {  
     require_once($view_path.'inc/developmentmenu_new.php');    
    }else{
     require_once($view_path.'inc/teacher_menu.php');
    }
    ?>
     
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-edit"></i>&nbsp; Lesson Plan Creator
                   </h3>
                   
         <?php if($this->session->userdata("login_special")=='district_management') { ?>
        <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                     <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                              <a href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Planning Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                           <a href="<?php echo base_url();?>lesson_plan_material">Lesson Plan Material</a>
                            
                       </li>
                      
                   </ul>
        <?php   }else{?>
                  <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                     <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>lesson_plan_material">Curriculum Upload</a>
                       </li>
                      
                   </ul>
             <?php }?>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                      <?php if($this->session->userdata("login_special")=='district_management') {  ?>
                     <div class="widget green">
                     <?php }else{ ?>
                     <div class="widget purple">
                     <?php }?>
                         <div class="widget-title">
            <?php if($this->session->userdata("login_special")=='district_management') {  ?>
                             <h4>Lesson Plan Material</h4>
                        <?php }else{?>     
                             <h4>Curriculum Upload</h4>
                        <?php }?>
                          
                         </div>
                         <form class="form-horizontal" id="mediaForm" name="mediaForm" method="post" enctype="multipart/form-data">
                         <div class="widget-body">
<div id="temp" style="display:none;"></div>
                            
              <input type="hidden" name="MEDIA_MAX_FILE_SIZE" value="2097152" />
        <?php if($this->session->userdata("login_special")=='district_management') {  ?>
                   <div id="pills" class="custom-wizard-pills-green2">    
                <?php }else{?>
               <div id="pills" class="custom-wizard-pills-purple2">
                           <?php }?>    
                                
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     
                                     
                                     
                                 </ul>
                                 <?php if($this->session->userdata("login_special")=='district_management') { ?>
                                  <div class="progress progress-success progress-striped active">
                                 <?php }else{?>
                                 <div class="progress progress-success-purple progress-striped active">
                                 <?php }?>
                                     <div class="bar"></div>
                                 </div>
                                 
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <div class="form-horizontal">
                             
                             <?php if($this->session->userdata('login_type')=='user')
                      {
                      ?>                
                <div class="control-group">
                                
          <label class="control-label">Select School</label>
                                    <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="media_school_id" id="media_school_id">     
                      <?php if(!empty($school)) { 
                                            foreach($school as $schoolval)
                                            {
                                            ?>
                                            <option value="<?php echo $schoolval['school_id'];?>"><?php echo $schoolval['school_name'];?></option>
                                            <?php } } else { ?>
                                            <option value="">No Schools Found.</option>
                                            <?php } ?>
                                            </select>
                                          
                                    </div>
                                </div>
                                  <?php } else { ?>
                                  <div class="control-group" <?php if($this->session->userdata('login_type')=='teacher') echo 'style="display:none"';?>>
          <label class="control-label">Select Name</label>
                                    <div class="controls">
                     
                                        <?php echo $this->session->userdata("school_name");?>
      <input type="hidden" id="media_school_id" name="media_school_id" value="<?php echo $this->session->userdata("school_id");?>">     
    
                                    </div>
                                </div>
                                  <?php } ?>
                                <div class="control-group">
                                   <label class="control-label">Select Subject</label>
                                    <div class="controls">
                                         
          
          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="subject_id" id="subject_id">
                                            <option value=""></option>
                                            <?php if(!empty($subjects)) { 
                                            foreach($subjects as $subjectval)
                                            {
                                            ?>
                                            <option value="<?php echo $subjectval['subject_id'];?>"><?php echo $subjectval['subject_name'];?></option>
                                            
                                            
                                            <?php } } ?>
                                            </select>
                                    </div>
                                </div>
                                        
                                     </div>
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                   
                                         
                                          <div class="control-group">
                                          <label class="control-label">Select Grade</label>
                                    <div class="controls">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="grade_id" id="grade_id">
                                        <option value=""></option>
                                        <?php if(!empty($grades)) { 
                                        foreach($grades as $gradeval)
                                        {
                                        ?>
                                        <option value="<?php echo $gradeval['grade_id'];?>"><?php echo $gradeval['grade_name'];?></option>
                                        <?php } } ?>
                                        </select>
                                    </div>
                                </div>   
                                        
                                
                    <p><b>Select one method below</b></p>           
                                
                                 <div class="control-group">
                                             <label class="control-label">Upload File</label>
                                             <div class="controls">
                                             
                                                <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default" name="mediafileToUpload" id="mediafileToUpload" size="38" />
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="control-group">
                                    
                                    <div class="controls">
                                    
 <p id="mediamessage"></p>
<input type="Submit"  value="Submit"  id="media_update_button"  class="media_update_button btn btn-large btn-purple"/>
                                    </div>
                                </div>  
                                    
                                  
                                    
                                
                                  <div class="control-group">
                                          <label class="control-label">Search By Name</label>
                                    <div class="controls">
                                       
    <input type="textbox" name="mediasearchvalue" size="16" class="m-ctrl-medium" id="mediasearchvalue" value="<?php echo $this->session->userdata('search_mediasearch_school_id');?>"> 
                             <button class="btn btn-large btn-purple" type="button" id="mediasearch" onClick="mediaload()" name="search" value="Search"><i class="icon-save icon-white"></i> Search</button>           
                                    </div>
                                </div>  
                                    
                                      
                                    <BR><BR>
                                    
                          <center>
               
              
                          </center>
                          
                          
    <div id='mediaflashmessage'>
  <div id="mediaflash" align="left"  ></div>
  </div>  
    <table style="width:100%;">
  <tr>
  <td align="center">
  <div id="mediadetails" style="display:none;">
    <input type="hidden" id="mediapageid" value="">
    <div id="mediamsgContainer" style="width:100%;">
      </div>
    </div>
  </td>
  </tr>
  </table>
                          
                          
                          
                          
                               </div>
                                          </div>  
                              </div>

                              
                              
                                      <?php if($this->session->userdata("login_special")=='district_management') {  ?>
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                     <?php }else{?>
                                     
                                     <ul class="pager wizard">
                                         <li class="previous first purple"><a href="javascript:;">First</a></li>
                                         <li class="previous purple"><a href="javascript:;">Previous</a></li>
                                         <li class="next last purple"><a href="javascript:;">Last</a></li>
                                         <li class="next purple"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                     <?php }?>
                                 </div>
                               
                             </div>
                             </form>
                         </div>
                         
  
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
 

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <!--start old script -->
   

<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
var siteurlm='<?php echo SITEURLM ;?>';
</script>
<script type="text/javascript" src="<?php echo SITEURLM.$view_path; ?>js/lesson_plan_material.js"></script>
<LINK href="<?php echo SITEURLM?>css/video.css" type="text/css" rel="stylesheet">
   <!--end old script -->

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>