<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop:: Copy Assessment from one District to another ::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
 <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">

function updatedistrict(tag)
{
	if(tag!=-1)
	{
		$.ajax({
		url:'<?php echo base_url();?>/copy_assessments/getassessmentselectionHtml?dist_id='+tag,
		success:function(assessmentHtml)
		{ 
			document.getElementById("msgdisplay").innerHTML= '';
			$("#assessmentdrp").html('&nbsp;');			
			document.getElementById("assessmentdrp").innerHTML= assessmentHtml;
		}
		});
	}
}

function getRemainDistict(tag)
{
 	var distid = document.getElementById('district').options[document.getElementById('district').selectedIndex].value;
	if(tag!=-1)
	{
		$.ajax({
		url:'<?php echo base_url();?>/copy_assessments/getToDistrictselectionHtml?dist_id='+distid,
		success:function(toDistrictdrpHtml)
		{ 
			document.getElementById("msgdisplay").innerHTML= '';
			$("#toDistrictdrp").html('&nbsp;');			
			document.getElementById("toDistrictdrp").innerHTML= toDistrictdrpHtml;
		}
		});
	}
}
function copyassessments()
{
 	var distId = document.getElementById('district').options[document.getElementById('district').selectedIndex].value;
	var toDist = document.getElementById('todistrict').options[document.getElementById('todistrict').selectedIndex].value;
	var aId = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	if(distId != -1 && toDist!=-1 && aId != -1)
	{
		$.ajax({
		url:'<?php echo base_url();?>/copy_assessments/copyAssessment?dist_id='+distId+'&to_dist='+toDist+'&a_id='+aId,
		success:function(msg)
		{ 
			document.getElementById("msgdisplay").innerHTML= msg;
		}
		});
	}	
}

function removeMsg()
{
	document.getElementById("msgdisplay").innerHTML= '';
}
 </script>
  
 
</head>

<body>
<div class="wrapper">
  <?php require_once($view_path.'inc/headerv1.php'); ?>
  <div class="mbody">
    <?php require_once($view_path.'inc/pleftmenu.php'); ?>
    <div class="content">
      <div class="search">
    <!--  <span style="text-decoration:underline; font-size:16px;">Search Students</span>-->
    <fieldset>
    <legend>  Copy Assessment from one District to Another </legend>
        
      <form name="frmcopyassesment" action="copy_assessments" method="post" >  
   <table style="width:100%;" >
      <tr>
     <td></td> 
     <td align="left" id="msgdisplay" width="25px">
 	</td>
      </tr>
      <tr>
           <td align="right" width="25%" > From District: &nbsp; </td>
          <td>
         		  <?php  //echo"<pre>"; print_r($dist); ?>

    <select class="combobox" name="district" id="district" onchange="updatedistrict(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
        <?php 
	     foreach($dist as $key => $value)
		{
			echo '<option value="'.$value['district_id'].'">'.$value['districts_name'].'</option>';
		}
        ?>    	 </select>
           </td valign="top">
                  
      </tr>		
      
        <tr id = "assessmentdrp">
           <td align="right" > Select Assessment : &nbsp; </td>
          <td>
     <select class="combobox" name="assessment" id="assessment" onchange="getRemainDistict(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
	 </select></td>
        </tr>	
        
         <tr id = "toDistrictdrp">
           <td align="right" > To District : &nbsp; </td>
          <td>
     <select class="combobox" name="todistrict" id="todistrict" onchange="removeMsg()"  >
        <option value="-1"  selected="selected">-Please Select-</option>
	 </select></td>
     
     <tr>
     <td></td> 
     <td align="left">
		<input type="button" class="btnsmall" title="Submit" name="copyassessment" id="copyassessment" value="Submit" onclick="copyassessments()">
		</td>
        </tr>
    
        
        <tr><font style="size:4">
     
       <td id="reporthtml" colspan="3" width="100%">  </td> </font>
       </tr>
     
 
 
 
   </table>
   </form>
        
        
   </fieldset>
   </div>
      
      
    
    </div>
    </div>
  </div>
  <?php require_once($view_path.'inc/footer.php'); ?>
</div>


<!-- Single Student graph  --> 

<!-- LIGHT BOX ENDS HERE-->
</body>
</html>
