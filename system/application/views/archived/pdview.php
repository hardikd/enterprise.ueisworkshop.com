<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
    <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    <!--0ld script start -->

 
<style type="text/css">

ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 32px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 100%;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 31px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 31px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: 1.2em;
	padding: 0 20px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
.tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left; width: 100%;
	background: #fff;
	margin:0px 0px 0px 0px;
	border:none;
}
.tab_content {
	padding: 15px;
	font-size: 1.2em;
}
</style>
    <!--end old scirpt -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
    <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/professional_development">Professional Development Reports</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>teacherplan/videos">Individual </a>
                          
                       </li>
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Archived Data</h4>
                          
                         </div>
                         <div class="widget-body">
                         
                         <b>Teacher Name:</b><?php echo $teacher_name;?>  <b>Date:</b><?php echo $date;?><br><BR>
                         
                         
                         <div class="widget widget-tabs purple">
                        <div class="widget-title">
                            
                        </div>
                         <div class="widget-body">
                            <div class="tabbable ">
                            <table width="100%" cellpadding=0 cellspacing=0 style="margin-left:10px;">
		<tr><td>
		<input type="hidden" name="date" id="date" value="<?php echo $date;?>">
                                <ul class="nav nav-tabs">
                            <li class="active"><a href="#widget_tab1" data-toggle="tab">Videos</a></li>
                            <li class=""><a href="#widget_tab2" data-toggle="tab">Articles</a></li>
                            <li class=""><a href="#widget_tab5" data-toggle="tab">Artifacts</a></li>
                                    
                                   
                                </ul>
                                	</td>
				</tr>
					<tr class="tab_container">
					<td>
                                </div>
                                
                                
                                <div class="tab-content">
  <div class="tab-pane active" id="widget_tab1">


	 <?php if($prof_groups!=false) { ?>
	 <table class="table table-striped table-bordered" style="width:535px;" cellspacing='0' cellpadding='0' id='conf'>
     
	 <tr>
	 <th>Standard Name</th>
	 <th>Video Name</th>
	 <th>Video</th>
	 <th>Teacher Comments</th>
	 <th>Observer Comments</th>
	 </tr>
	 <?php 
	 $i=1;
	 foreach($prof_groups as $val) { 
		if($i%2==0)
		{
		$c='tcrow2';
		}
		else
		{
		$c='tcrow1';
		}
	 ?>
	 <tr class="<?php echo $c;?>">
	 <td class="hidden-phone">
	 <?php echo  $val['group_name'];?>
	 </td>
	 <td>
	 <?php echo  $val['name'];?>
	 </td>
	 <td onClick="show(<?php echo $val['prof_dev_id'];?>)"><a class="btn btn-purple" href="javascript:void(0)" style="cursor:pointer;">Watch</a></td>
	 <td><a class="btn btn-purple" href="#" title="Details|<?php if($val['teacher_comments']!=''){ echo $val['teacher_comments']; } else { echo "No Details Found. "; }  ?> ">View</a></td>
	 <td><a class="btn btn-purple" href="#" title="Details|<?php if($val['observer_comments']!=''){ echo $val['observer_comments']; } else { echo "No Details Found. "; }  ?> ">View</a></td>
	 </tr>
	 <?php $i++; } ?>
	 </table>
	 <?php } else { ?>
	 No Videos Found
	 <?php } ?>
	</div>   
                                
                         <div class="tab-pane" id="widget_tab2">
                            
                     <?php if($article_groups!=false) { ?>
	 <table class="table table-striped table-bordered" style="width:535px;" cellspacing='0' cellpadding='0' id='conf'>
     <thead>
	 <tr>
	 <th>Standard Name</th>
	 <th>Article Name</th>
	 <th>Link</th>
	 <th>Teacher Comments</th>
	 <th>Observer Comments</th>
	 </tr>
     </thead>
	 <?php 
	 $i=1;
	 foreach($article_groups as $val) { 
	 
	 if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
	 ?>
	 <tr class="<?php echo $c;?>">
	 <td class="hidden-phone">
	 <?php echo  $val['group_name'];?>
	 </td>
	 <td>
	 <?php echo  $val['name'];?>
	 </td>
	 <td ><a class="btn btn-purple" href="<?php echo WORKSHOP_DISPLAY_FILES;?>archived/<?php echo $val['link'];?>"  target="_blank" style="cursor:pointer;">Link</a></td>
	<td><a class="btn btn-purple" href="#" title="Details|<?php if($val['teacher_comments']!=''){ echo $val['teacher_comments']; } else { echo "No Details Found. "; }  ?> ">View</a></td>
	 <td><a class="btn btn-purple" href="#" title="Details|<?php if($val['observer_comments']!=''){ echo $val['observer_comments']; } else { echo "No Details Found. "; }  ?> ">View</a></td>
	 </tr>
	 <?php $i++; } ?>
	 </table>
	 <?php } else { ?>
	 No Articles Found
	 <?php } ?>
                                
           </div>                      
                                
                               <div class="tab-pane" id="widget_tab5">
                                 <div class="space20"></div>
                                 
                                          
          <div class="widget-body"><!-- BEGIN WIDGET BODY FIRST -->
           <div class="widget widget-tabs purple"> <!-- BEGIN WIDGET TABS -->
              <div class="widget-title"></div>
                       <div class="widget-body"><!-- BEGIN TABBABLE BODY -->                        
                            <div class="tabbable ">
                                <ul class="nav nav-tabs">
                              <li class="active"><a href="#widget_tab7" data-toggle="tab">Notes</a></li>
                              <li class=""><a href="#widget_tab8" data-toggle="tab">Links</a></li>
                              <li class=""><a href="#widget_tab9" data-toggle="tab">Photos</a></li>
                              <li class=""><a href="#widget_tab10" data-toggle="tab">Videos</a></li>
                              <li class=""><a href="#widget_tab11" data-toggle="tab">Files</a></li>
                                  
                                   
                                </ul>
                                
                                 </div><!-- END TABBABLE -->
                                
                                 <div class="tab-content"><!-- TAB CONTENT START -->
                                 <div class="tab-pane active" id="widget_tab7"><!-- TAB NOTES START -->
                                <div class="space20"></div>
                                
                               
                                 <table style="width:800px;" class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th >Note</th>
                                <th >Actions</th>                              
                                
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">                               
                                <td class="hidden-phone">Note Name Here</td>
                                <td><a href="#myModal-view-note" role="button" class="btn btn-purple" data-toggle="modal">View</a></td>     
                               
                                                            </tr>
                            <tr class="odd gradeX">                 
                                <td class="hidden-phone">Note Name Here</td>
                                <td><a href="#myModal-view-note" role="button" class="btn btn-purple" data-toggle="modal">View</a></td>                                                  </tr>
                                        <tr class="odd gradeX">                              
                                <td class="hidden-phone">Note Name Here</td>
                                <td><a href="#myModal-view-note" role="button" class="btn btn-purple" data-toggle="modal">View</a></td>           </tr>
                                </tbody>
                                </table>

                                
                                 </div><!-- TAB NOTES END -->
                                 
                                 
                                 
                               <div class="tab-pane" id="widget_tab8"><!-- TAB LINKS START -->
                                <div class="space20"></div>
                                
                       
                               
                         
          <form id="linkuploadForm" action="teacherplan/uploadlink" method="POST" >
	<table align="center">	
     <tr>
	<td colspan="2" align="center">
	 <div id="linkhtmlExampleTarget" style="color:red"></div>
	 </td>
	 </tr>
	</table>
	</form>
	<table >
	<tr>
	<td align="center">
	<div id="linkdetails" style="display:none;">
		<input type="hidden" id="linkpageid" value="">
		<div id="linkmsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
                                 </div><!-- TAB LINKS END -->
                                 
                                
                                <div class="tab-pane" id="widget_tab9"><!-- TAB PHOTOS START -->
                                <div class="space20"></div>
   
   <table>
	<tr>
	<td align="center">
	<div id="photodetails" style="display:block;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>         
                                 
                                 
                                 </div><!-- TAB PHOTOS END -->
                                 
                                 
<div class="tab-pane" id="widget_tab10"><!-- TAB VIDEOS START -->
<div class="space20"></div>
                        
      
	<table >
	<tr>
	<td align="center">
	<div id="videodetails" style="display:none;">
		<input type="hidden" id="videopageid" value="">
		<div id="videomsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>  
                                 
    </div><!-- TAB VIDEOS END -->
                                 
                                
                                 <div class="tab-pane" id="widget_tab11"><!-- TAB FILES START -->
                                <div class="space20"></div>
                  <!--<table class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th >File Name</th>
                                <th >Actions</th>                              
                              </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">                               
                                <td class="hidden-phone">File Name Here</td>
                                <td><a href="#" role="button" class="btn btn-purple" target="_blank">Download</a></td>
                              </tr>
                            <tr class="odd gradeX">                 
                                <td class="hidden-phone">File Name Here</td>
                              <td><a href="#" role="button" class="btn btn-purple" target="_blank">Download</a></td>                              </tr>
                                        <tr class="odd gradeX">                              
                                <td class="hidden-phone">File Name Here</td>
                                 <td><a href="#" role="button" class="btn btn-purple" target="_blank">Download</a></td>                           </tr>
                                </tbody>
                                </table>-->
                
	<table>
	<tr>
	<td align="center">
	<div id="filedetails" style="display:block;">
		<input type="hidden" id="filepageid" value="">
		<div id="filemsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
	</div>
	
	
	</td>
	</tr>
	</table>  
                  
                          
                                 </div><!-- TAB FILES END -->
                                
                                
                               
                               </div><!-- END  TAB CONTENT -->
                                </div><!-- END  TABBABLE BODY -->                                
                                  </div><!-- END WIDGET TABS -->                               
                                  </div><!-- END WIDGET BODY FIRST-->
                              
                        
                               
                                </div><!-- END TAB 5 ARTIFACTS-->
                                
                                
            
                                
                                  
                                                       
                                
                                
                                </div>
                           
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     
                  
                            
                            
                 </div>
             </div>
               </div>
             </div>  </div>
             </div>
            
             
     <div id="myModal-view-note" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Note Name Here</h3>
                                </div>
                                <div class="modal-body">
                                     Note appear here.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Close</button>
                                    
                                </div>
                            </div>
                                    
             
             
             
               <div id="myModal-teacher" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Teacher Comments</h3>
                                </div>
                                <div class="modal-body">
                                     Teacher comments appear here.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Close</button>
                                    
                                </div>
                            </div>
                            
                            
                               <div id="myModal-observer" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Observer Comments</h3>
                                </div>
                                <div class="modal-body">
                                     Observer comments appear here.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Close</button>
                                    
                                </div>
                            </div>
            
            
            <div id="watch" title="watch a video" style="display:none;">
<table cellpadding="0" cellspacing="0" border=0 class="jqform">
<tr>
<td id="alink">
Link:
</td>
</tr>
<tr>
<td id="avideo">
</td>
</tr>
</table>   
   
</div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
  
 
 
 

   <!--start old script -->

   <script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/photosarchived.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">var SITEURLM1 = '<?php echo SITEURLM; ?>';</script>	
<script src="<?php echo SITEURLM?>js/jquery.lightbox.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<LINK href="<?php echo SITEURLM?>css/video.css" type="text/css" rel="stylesheet">

<script type="text/javascript">
$(document).ready(function() {

	$('a.teacher').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});
$('a.observer').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});
	
	
	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
        
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
function show(id)
		{
		  var p_url='comments/getwatchvideo/'+id;
    $.getJSON(p_url,function(result)
	{
			   
	var sstr="<td id='alink'>Link:<a href="+result.video.link+" target='_blank'>Click here</a></td>";
	var str="<td id='avideo'>"+result.video.descr+"</td>";
	
	
	
	$('#avideo').replaceWith(str);
	$('#alink').replaceWith(sstr);
		
	}); 
		
		 $("#watch").dialog({
			modal: true,
           	height: 450,
			width: 475,
			 close: function(){
            var str="<td id='avideo'></td>";
				$('#avideo').replaceWith(str);
				}
			});
		
		
		
		
		}
</script>

   <!-- END JAVASCRIPTS --> 

   
   


    


   
<?php /*?>   
     <script>
 var elem = document.getElementById("select_1");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script>

    <script>
 var elem = document.getElementById("select_2");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script>



 <script>
 var elem = document.getElementById("select_3");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script>

    <script>
 var elem = document.getElementById("select_4");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script><?php */?>

 




 
   
   
   
   
 
</body>
<!-- END BODY -->
</html>

