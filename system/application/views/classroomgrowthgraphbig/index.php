<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View A Classrooms Growth::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>

<script type="text/javascript">

 //function displayGraph()
//{
 //	var datavalue =;
	//alert('datavalue');
 getGraph('<?php echo $dataRecord;?>');				

//}
 function getGraph(clounmValue)
 {

	$(function () {								
        //$('#container').highcharts({
		var options = {
           chart: {
		   		renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'View a Classrooms Growth'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [],
				labels: {
                    rotation: -65,
                    align: 'right',
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }  					              
            },
            yAxis: {
                min: 0,
				max: 100,
                title: {
                    text: 'score Point'
                }
            },
			 tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
	       series: [ ]
        }
        //alert(clounmValue);
		if(clounmValue!="")
		{
			var result = JSON.parse(clounmValue);
			//	alert(result);
			var Dispayname ='';
			var count = 0;
			for (var strDispayname in result) 
			{
				if(count == 0)
				{
					var dispN = result[strDispayname];
					
					for(var strDispN in dispN)
					{			
						if(strDispN != '')
							Dispayname = Dispayname + "'"+strDispN +"',"; 
					}
				}
				count++;
				
			}
			Dispayname = Dispayname.substr(0,Dispayname.length-1);
			//alert(Dispayname);
			eval("options.xAxis.categories = new Array("+Dispayname+");");
			
	 		options.series = new Array();		
			var result = JSON.parse(clounmValue);	
			var i=0;				
			for (var strTestName in result) 
			{
			 	options.series[i] = new Object();
				options.series[i].name = strTestName;
			
				var objTest = result[strTestName];
	 			 arrScore = new Array();
				var j=0;
				for(var strGrade in objTest)
				{			
					if(objTest[strGrade]==null)
						 objTest[strGrade] = 0;
					arrScore[j] = parseFloat(objTest[strGrade]);
	 				j++;
				}									
			   options.series[i].data = arrScore;
				i++;						 
			}
		} 
		chart = new Highcharts.Chart(options);
			
		});			
  }
 

   </script>

</head>
<body>
   <div >
     <fieldset>
    
    <legend>View a Classrooms Growth</legend>
   
     <div id="container" style="height:600px"></div>   
   </fieldset>
       
  </div>
  
 
 </body>
</html>
