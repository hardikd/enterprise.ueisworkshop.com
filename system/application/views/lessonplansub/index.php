<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Lessonplan::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/addnew.js" type="text/javascript"></script>
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/lessonplansub.js" type="text/javascript"></script>

</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
        <table align="center"  >
		<tr>
		<td>
		 Lesson plan Tabs:
		</td>
		<td>
		<select class="combobox" name="lessonplan" id="lessonplan">
		
		<?php if(!empty($lessonplan)) { ?>
		<option value="all">All</option>
		<?php foreach($lessonplan as $val)
		{
		?>
		<option value="<?php echo $val['lesson_plan_id'];?>" <?php if(isset($lesson_plan_id) && $lesson_plan_id==$val['lesson_plan_id'] ) {?> selected <?php  } ?> ><?php echo $val['tab'];?></option>
		<?php } } else { ?>
		<option value="0">No Lesson Plan Tabs Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getlessonplan" id="getlessonplan" value="Submit">
		</td>
		</tr>
		</table>
		<table>
		</table>
		<div id="lessonplansubdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New" class="btnbig" type="button" name="lessonplansub_add" id="lessonplansub_add" value="Add New">
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Lesson Plan SubTab" style="display:none;"> 

<form name='lessonplansubform' id='lessonplansubform' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			
			<tr>
				<td valign="top" >
				<font color="red">*</font>Lesson plan Tab :
				</td>
				<td valign="top" style="height:40px;">
				<select class="combobox" name="lesson_plan_id" id="lesson_plan_id">
				<option value="">-select</option>
				</select>
				<input  type='hidden'  id='lesson_plan_sub_id' name='lesson_plan_sub_id'>
				<input  type='hidden'  id='actual_lesson_plan_id' name='actual_lesson_plan_id' value="">
				</td>
				
			</tr>
			<tr>
			<td colspan="2">
			<table id="myTable" width="100%" >
			<tr>
			<td>
			<input type="hidden" name="counterscale" id="counterscale" value="">
			</td>
			</tr>
			<tr id="TextBoxDiv1">
			<td valign="top" >
			Sub lessonplan  Tab: 
		</td>
		<td valign="top" style="height:40px;">
		<input class="txtbox" type='text' id='textbox1' name="textbox1"><input class="btnbig" title="Remove" type='button' value='Remove Button' id='removeButton' onclick="remove(1);">
		</td>
	    </tr>
		</table>
		</td>
       </tr>
<tr id="addsub" >
<td>
<input class="btnsmall_long" title="Add SubGroup" type='button' value='Add SubGroup' id='addButton'>
</td>
</tr>
<!--<input type='button' value='Remove Button' id='removeButton'>-->


						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New" class="btnbig" type='submit' name="submit" id='lessonplansubadd' value='Add' > <input title="Cancel" class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
