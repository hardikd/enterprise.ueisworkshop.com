﻿<table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
      <td>Subject Name</td>
      <td>Quarter</td>
   <td>Standard Name</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page -1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->standard_name_id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->subject_name ;?></td>
     <td><?php echo $data->quarter_name ;?></td>
     <td><?php echo $data->standard_name;?></td>
     <td><?php echo $data->status;?></td>
      <td><button class="edit_standard_name btn btn-primary" data-dismiss="modal" type="button" name="<?php echo $data->standard_name_id;?>" value="Edit" aria-hidden="true"><i class="icon-pencil"></i></button></td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->standard_name_id;?>" />
     <td><button data-dismiss="modal" value="Remove" name="<?php echo $data->standard_name_id;?>" class="remove_standard_name btn btn-danger" id="remove_standard_name"><i class="icon-trash"></i></button></td>


  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_standard_name').click(function(){
  var id = $(this).attr('name');
  $.ajax({
    type: "POST",
       url: "<?php echo base_url().'standard_name/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#standard_name_id').val(result[0].standard_name_id);
       $('#grade_id').val(result[0].grade_id);
       $('#subject_id').val(result[0].subject_id);
       $('#quarter_name').val(result[0].quarter_name);
        $('#standard_name').val(result[0].standard_name);
      $('#status').val(result[0].status);
       console.log(result[0].behavior_location);
       $("#dialog").dialog({
      modal: true,
            height:400,
      width: 500
      });
       }
     });  
});

$(".remove_standard_name").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'standard_name/delete';?>",
      data: { 'standard_name_id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed standard list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

