<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    <script>
        var base_url = '<?php echo base_url();?>';
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/headerv1.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
          <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                       Welcome <?php echo $this->session->userdata('username');?>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>assessment/class_setup">Class Setup</a>
                            <span class="divider">></span>
                        </li>
                       <li>
                            <a href="<?php echo base_url();?>assessment/classroom_management">Classroom Management</a>
                            <span class="divider">></span>
            </li>
                       <li>
                            <a href="<?php echo base_url();?>assessment/progress_report">Progress Report</a>
                           <span class="divider">></span>
            </li>
                       <li>
              <a href="<?php echo base_url();?>standard_name">Progress Report Marks by Standard</a>
                       </li>
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget orange">
                         <div class="widget-title">
                             <h4>Progress Report Marks by Standard</h4>
                          
                         </div>
                        
                        <div class="widget-body">
 <button type='button' name="statements_name_add" id='statements_name_add' value='Add New' class="btn btn-success" ><i class="icon-plus"></i > Add New</button>
             
                         <div class="space20"></div>
                              
        
       
    <div id="msgContainer">
       
  
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <!--notification -->
  <div id="dialog" title="Standard" style="display:none;"> 
       <form method="post" action="#" id="statements_name_form"> 
  <table >
    <tr>
    <td>Select Grade</td>
    <td>
    <select  style="width: 300px;" name="grade_id" id="grade_id">
                                        <option value=""></option>
                                        <?php if(!empty($grades)) { 
                                    foreach($grades as $val)
                                    {
                                     ?>
                                        <option value="<?php echo $val['grade_id'];?>" 
                                  <?php if(isset($grade_id) && $grade_id==$val['grade_id']) {?> selected <?php } ?>
                                 ><?php echo $val['grade_name'];?>  </option>
                                        <?php } } ?>
                                      </select>  
    
    </td>
    </tr>
    <tr>
    <td>Select Subject:</td>
    <td>
    <input type="hidden" name="standard_name_id" id="standard_name_id" value="" />
    <input type="hidden" name="district_id" value="<?php echo $this->session->userdata('district_id');?>"  /> 
    
    <select name="subject_id" id="subject_id"  style="width: 300px;">
                                            <option value="">Please Select</option>
                                        
                                      </select>  
    
                    <?php /*?><select name="subject_id" id="subject_id"  style="width: 300px;">
                                            <option value="">Please Select</option>
                                            <?php if(!empty($subjects)) { 
                                            foreach($subjects as $val)
                                            {
                                            ?>
                                            <option value="<?php echo $val['subject_id'];?>" ><?php echo $val['subject_name'];?>  </option>
                                            <?php } } ?>
                                      </select>  
    <?php */?>
    </td>
    </tr>



 
    <tr>
    <td>Standard Name:</td>
    <td>
     <input type="text" name="standard_name" id="standard_name" class="txtbox valid" value="" />
    </td>
    </tr>
          <tr>
    <td>Select Quarter:</td>
    <td>
    <select name="quarter_name" id="quarter_name"  style="width: 300px;">
                        <option value="">Select Quarter</option>
                                            <option value="q1">Q1</option>
                                            <option value="q2">Q2</option>
                                            <option value="q3">Q3</option>
                                            <option value="q4">Q4</option>
                                            
                                        
                                      </select>  
    </td>
    </tr>
 
    <tr>
    <td>
    Status
    </td>
    <td>
    <select name="status" id="status" class="combobox">
    <option value="">select</option>
    <option value="Active">Active</option>
        <option value="Inactive">Inactive</option>
    </select>
    </td>
    </tr>
    <tr>
    <td align="center" colspan="2">
    <button type='submit' name="submit"  value='submit' class="btn btn-success"><i class="icon-plus"></i>Submit</button>
    </td>
    </tr>
    </table>
</form>
</div>
<div class="dialog"></div>
   <!-- notification ends -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.form.js"></script>

 <script src="<?php echo SITEURLM.$view_path; ?>js/addnew.js" type="text/javascript"></script>
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>



<script type="text/javascript">
var dist_user_district_id= <?php if($this->session->userdata('district_id')) { echo $this->session->userdata('district_id'); } ?>;
</script>
<script type="text/javascript">
 
$(function(){
   $("#statements_name_form").submit(function(){});
});
</script>
<script type="text/javascript">
/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */
(function($,W,D)
{
    var JQUERY4U = {};

    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //form validation rules
            $("#statements_name_form").validate({
                rules: {
                    standard_name: "required",
          grade_id:"required",
          subject_id:"required",
          quarter_name:"required",
          standard_language_id:"required",
                    status: "required",
          
                },
                messages: {
                    standard_name: "Please enter your standard name",
          grade_id:"Please select your grade",
          subject_id:"Please select your subject",
          quarter_name:"Please select your Quarter",
          standard_language_id:"Please select your standard_language",
                    status: "Please select your status",
                },
                submitHandler: function(form) {
                    
     var pageno = $('#msgContainer .standard_name li.current').attr('p');
   dataString = $("#statements_name_form").serialize();
   $.ajax({
       type: "POST",
       url: "<?php echo base_url().'standard_name/standard_name_insert';?>",
       data: dataString,
       success: function(data)
       {
       $('#dialog').dialog('close');
           alert('Successful!');
       $.ajax({
    type: "POST",
    url: "<?php echo base_url().'standard_name/standard_name_list';?>/"+pageno,
    success: function(msg){
      $('#grade_id').val('');
      $('#subject_id').val('');
      $('#quarter_name').val('');
      $('#standard_name').val('');
      $('#status').val('');
      $('#msgContainer').html(msg);
    }
  });
       }
     });
     return false;  //stop the actual form post !important!

  
                }
            });
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });

})(jQuery, window, document);
</script>



<script>
$(document).ready(function(){

   $.ajax({
    type: "POST",
    url: "<?php echo base_url().'standard_name/standard_name_list';?>/1",
    success: function(msg){
      $('#msgContainer').html(msg);
    }
  });


 $('#msgContainer .standard_name li.active').live('click',function(){

var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
var group_id=$('#lickertgroup').val();
$.get("<?php echo base_url().'standard_name/standard_name_list';?>/"+page, function(msg){
$('#msgContainer').html(msg);
});
});
});
</script>
 
 
<script>
$('#statements_name_add').click(function(){
  $('#standard_name_id').val('');
    $("#dialog").dialog({
      modal: true,
            height:500,
      width: 400
      });
  
});
</script>
 <script>
    
// $("#pills-tab2").bootstrapWizard("show",2)

$('#grade_id').change(function(){
  
  
  $.ajax({
    
    type: "POST",
    url: "<?php echo base_url();?>standard_name/grade_by_subject",
    data: { grade_id: $('#grade_id').val() },
    success: function( msg ) {
      $('#subject_id').html('');
      var result = JSON.parse( msg );
      
      
     $(result.subjects).each(function(index, Element){ 
     
     console.log(Element);
      $('#subject_id').append('<option value="'+Element.subject_id+'">'+Element.subject_name+'</option>');
    });  
      
      
    }
  })
    
    
});

   </script>  
   
   

   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>