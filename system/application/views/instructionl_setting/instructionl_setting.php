<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Lickert Elements::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/addnew.js" type="text/javascript"></script>
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>



<script type="text/javascript">
var dist_user_district_id= <?php if($this->session->userdata('district_id')) { echo $this->session->userdata('district_id'); } ?>;
</script>
<script type="text/javascript">
 
$(function(){
   $("#instructionl_setting_form").submit(function(){});
});
</script>
<script type="text/javascript">
/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */
(function($,W,D)
{
    var JQUERY4U = {};

    JQUERY4U.UTIL =
    {
        setupFormValidation: function()
        {
            //form validation rules
            $("#instructionl_setting_form").validate({
                rules: {
                    instructionl_setting: "required",
                    status: "required",
                    
                },
                messages: {
                    instructionl_setting: "Please enter your Instructionl Setting",
                    status: "Please select your status",
               	},
                submitHandler: function(form) {
                    
	   var pageno = $('#msgContainer .instructionl_setting li.current').attr('p');
	 dataString = $("#instructionl_setting_form").serialize();
	 $.ajax({
       type: "POST",
       url: "<?php echo base_url().'instructionl_setting/instructionl_setting_insert';?>",
       data: dataString,
       success: function(data)
       {
			 $('#dialog').dialog('close');
           alert('Successful!');
		   $.ajax({
		type: "POST",
		url: "<?php echo base_url().'instructionl_setting/instructionl_setting_list';?>/"+pageno,
		success: function(msg){
			$('#instructionl_setting').val('');
			$('#status').val('');
			$('#msgContainer').html(msg);
		}
	});
       }
     });
     return false;  //stop the actual form post !important!

  
                }
            });
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($) {
        JQUERY4U.UTIL.setupFormValidation();
    });

})(jQuery, window, document);
</script>



<script>
$(document).ready(function(){

	 $.ajax({
		type: "POST",
		url: "<?php echo base_url().'instructionl_setting/instructionl_setting_list';?>/1",
		success: function(msg){
			$('#msgContainer').html(msg);
		}
	});


 $('#msgContainer .instructionl_setting li.active').live('click',function(){

var page = $(this).attr('p');
$('#pageid').val(page);
var state_id=$('#states').val();
var country_id=$('#countries').val();
var district_id=$('#district').val();
var group_id=$('#lickertgroup').val();
$.get("<?php echo base_url().'instructionl_setting/instructionl_setting_list';?>/"+page, function(msg){
$('#msgContainer').html(msg);
});
});
});
</script>


</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/developmentmenu.php'); ?>

      <div class="content">
		<div id="msgContainer">
        </div>
 	

	    <div>
		<input title="Add New" class="btnbig" type="button" name="instructionl_setting_add" id="instructionl_setting_add" value="Add New">
		</div>		
     </div></div>
     </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Instructionl Setting" style="display:none;"> 
       <form method="post" action="#" id="instructionl_setting_form"> 
	<table >
    <tr>
    <td>Instructionl Setting</td>
    <td>
    <input type="hidden" name="id" id="instructionl_setting_id" value="" />
   	<input type="hidden" name="district_id" value="<?php echo $this->session->userdata('district_id');?>"  /> 
    <input type="text" name="instructionl_setting" id="instructionl_setting" class="txtbox valid" value="" />
    </td>
    </tr>
    <tr>
    <td>
    Status
    </td>
    <td>
    <select name="status" id="status" class="combobox">
    <option value="">select</option>
    <option value="Active">Active</option>
        <option value="Inactive">Inactive</option>
    </select>
    </td>
    </tr>
    <tr>
    <td align="center" colspan="2"><input type="submit" name="submit" value="submit" class="btnbig" /></td>
    </tr>
    </table>
</form>
</div>
<div class="dialog"></div>
<script>
$('#instructionl_setting_add').click(function(){
		$('#instructionl_setting_id').val('');
		$("#dialog").dialog({
			modal: true,
           	height:200,
			width: 400
			});
	
});
</script>
</body>
</html>