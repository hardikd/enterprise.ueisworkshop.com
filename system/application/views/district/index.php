<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::districts::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/additional-methods.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/district.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/countries.js" type="text/javascript"></script>
<script>
var base_url = "<?php echo base_url();?>"
</script>
</head>

<body>

<div class="wrapper">

	<?php 
//        echo $this->session->userdata('login_type');
        if($this->session->userdata('login_type')!='admin')
            require_once($view_path.'inc/headerv1.php'); 
        else 
            require_once($view_path.'inc/headerv1.php'); 
        
        ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
		<table align="center">
		<tr>
		<td class="htitle">
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="countries_change(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td class="htitle">
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states"  >
		<option value="all">All</option>
		<?php if($states!=false) { 
		foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getdistrict" id="getdistrict" value="Submit">
		</td>
		</tr>
		</table>
		
        <div id="distdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New"  class="btnbig" type="button" name="district_add" id="district_add" value="Add New">
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="District" style="display:none;"> 

<form name='districtform' id='districtform' method='post' onsubmit="return false;">
<table cellpadding="0" cellspacing="0" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Country:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="country_id" id="country_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>State:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="state_id" id="state_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>District Name:
				</td>
				<td valign="top"  style="height:40px;">
				<input class='txtbox' type='text'  id='district_name' name='district_name'>
				<input  type='hidden'  id='district_id' name='district_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>District Copy:
				</td>
				<td valign="top"  style="height:40px;">
				<input  type='checkbox'  id='copystate' name='copystate' value="1">
				</td>
				
			</tr>
                        <tr>
				<td valign="top" >
				<font color="red">*</font>Copy Master Data:
				</td>
				<td valign="top"  style="height:40px;">
				<input  type='checkbox'  id='copymasterdata' name='copymasterdata' value="1" />
				</td>
				
			</tr>
           
			<tr>
				<td valign="top" >
				<font color="red">*</font>PERSONNEL EVALUATION SETUP:
				</td>
				<td valign="top"  style="height:40px;">
				<input  type='checkbox'  id='checklist_personnel_evalution_setup' name='checklist_personnel_evalution_setup' value="1" />
				</td>
				
			</tr> 
      
            
            
						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New"  class="btnbig" type='submit' name="submit" id='districtadd' value='Add' > <input title="Cancel"  class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
