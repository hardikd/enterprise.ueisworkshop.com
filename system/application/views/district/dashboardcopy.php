<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::District::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/reportnext.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); 
	include($view_path.'inc/class/pData.class.php');
include($view_path.'inc/class/pDraw.class.php');
include($view_path.'inc/class/pImage.class.php');
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <form method="post" name="dash" action="report/dashboard">
		<table>
		<tr>
		<td>
		Welcome <?php echo $this->session->userdata('username');
		$cach = date("H:i:s");
		$login_type=$this->session->userdata('login_type');
           
				$login_id=$this->session->userdata('dist_user_id');

			
		?>
		</td>
		</tr>
		<?php if($school_types!=false) { ?>
		<tr>
		<td>
		School Type:<select name="school_type" id="school_type">
		<?php 
		
		foreach($school_types as $schooltypeval)
		{?>
		<option value="<?php echo $schooltypeval['school_type_id'];?>"
		<?php if($school_type==$schooltypeval['school_type_id']) { ?> selected  <?php } ?>
		><?php echo $schooltypeval['tab'];?></option>
		<?php } ?>
		</select>
		</td>		
		<td align="center">
		<input type="submit" name="submit" value="Submit">
		</td>
		</tr>
		<?php } else { ?>
		<tr>
		<td>
		No School Types Found.
		</td>
		</tr>
		<?php } ?>
		</table>
		</form>
		<?php 
		if(!empty($schools))
		{
		
		
		?>
		
		
		<table>
		<?php
		$rcc=0;
		$rc=0;
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		$MyData = new pData();
		$namelimit=array();
		$lessonplan=array();
		$rcc++;
		}
		$rc++;
		  
		  $lessonplan[$pj]=$schools[$pj]['countlessonplans'];  	
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
			
		if($rc==10)
			{
			
			$MyData->addPoints($lessonplan,'Lesson Plan');
					
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 
 
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Lesson Planning",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/lesson/".$login_type.'_'.$login_id.'_'.$rcc.".png");

			
			$rc=0;
			}			
		}
		
		
	  
	if($rc>0)
	{
	$MyData->addPoints($lessonplan,'Lesson Plan');
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Lesson Planning",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 

		
		$myPicture->Render("$view_path/inc/dashboard/lesson/".$login_type.'_'.$login_id.'_'.$rcc.".png");
		
		
		}
		
		// start notification
		
		$rcc=0;
		$rc=0;
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		$MyData = new pData();
		$namelimit=array();
		$notification=array();
		$behaviour=array();
		$rcc++;
		}
		$rc++;
		  
		  $notification[$pj]=$schools[$pj]['notification']; 
			$behaviour[$pj]=$schools[$pj]['behaviour']; 		  
		  
		  
//print_r($piedata[$pj]['name']);


		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		}
		
		$namelimit[]=$limitname;
			
		if($rc==10)
			{
			
			$MyData->addPoints($notification,'Home Notification');
			$MyData->addPoints($behaviour,'Behavior & Learning');
					
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			
			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Notifications",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/notification/".$login_type.'_'.$login_id.'_'.$rcc.".png");

			
			$rc=0;
			}			
		}
		
		
	  
	if($rc>0)
	{
	$MyData->addPoints($notification,'Home Notification');
	$MyData->addPoints($behaviour,'Behavior & Learning');
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Notifications",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 

		
		$myPicture->Render("$view_path/inc/dashboard/notification/".$login_type.'_'.$login_id.'_'.$rcc.".png");
		
		
		}
		
		//stop notification
      
	    //start observation
		$rcc=0;
		$rc=0;
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		$MyData = new pData();
		$namelimit=array();
		$checklist=array();
		$scaled=array();
		$proficiency=array();
		$likert=array();
		$rcc++;
		
		}
		$rc++;
		  
		  $checklist[$pj]=$schools[$pj]['checklist']; 
		  $scaled[$pj]=$schools[$pj]['scaled']; 
		  $proficiency[$pj]=$schools[$pj]['proficiency']; 
		  $likert[$pj]=$schools[$pj]['likert']; 		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
			
		if($rc==10)
			{
			
			$MyData->addPoints($checklist,'Checklist');
			$MyData->addPoints($scaled,'Scaled Rubric');
			$MyData->addPoints($proficiency,'Proficiency Rubric');
			$MyData->addPoints($likert,'Likert');
					
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Observations",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/observation/".$login_type.'_'.$login_id.'_'.$rcc.".png");

			
			$rc=0;
			}			
		}
		
		
	  
	if($rc>0)
	{
		$MyData->addPoints($checklist,'Checklist');
			$MyData->addPoints($scaled,'Scaled Rubric');
			$MyData->addPoints($proficiency,'Proficiency Rubric');
			$MyData->addPoints($likert,'Likert');
	
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Observations",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 

		
		$myPicture->Render("$view_path/inc/dashboard/observation/".$login_type.'_'.$login_id.'_'.$rcc.".png");
		
		
		}
		
		
		
		// stop observation
		
		//start pdlc
		$rcc=0;
		$rc=0;
		for($pj=0;$pj<count($schools);$pj++)
		{
		if($rc==0)
		{
		$rcc++;
		$MyData = new pData();
		$namelimit=array();
		$pdlc=array();
		
		}
		$rc++;
		  
		  $pdlc[$pj]=$schools[$pj]['countpdlc'];  	
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $schools[$pj]['school_name']=preg_replace($sPattern, $sReplace, $schools[$pj]['school_name']);
		
		if(strlen($schools[$pj]['school_name'])>18)
		{
		   
		   $limitname=substr($schools[$pj]['school_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($schools[$pj]['school_name'],0,$last);
		   $limitname.='...'.'('.$schools[$pj]['countteachers'].')';
		
		}
		else
		{
		  $limitname=$schools[$pj]['school_name'].'('.$schools[$pj]['countteachers'].')';
		
		}
		
		$namelimit[]=$limitname;
			
		if($rc==10)
			{
			
			$MyData->addPoints($pdlc,'Professional Development');
					
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(250,55,"Professional Development Activity",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/professional/".$login_type.'_'.$login_id.'_'.$rcc.".png");

			
			$rc=0;
			}			
		}
		
		
	  
	if($rc>0)
	{
	$MyData->addPoints($pdlc,'Professional Development');
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(250,55,"Professional Development Activity",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($schools)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 

		
		$myPicture->Render("$view_path/inc/dashboard/professional/".$login_type.'_'.$login_id.'_'.$rcc.".png");
		
		
		}
		
		
		
		// stop pdlc
		?>
		<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
		<?php 
		if($rcc>1) { ?>
			<div style="float:right;margin-bottom:-80px;">
			<input type="button" id="previous" name="previous" value="Previous Data" style="display:none">
			<input type="button" id="next" name="next" value="Data Continued" style="display:none">
			</div>
			<?php } ?>
			
		<?php 
		
		for($jk=1;$jk<=$rcc;$jk++)
		{	
	?>		
	
	   <table class="rc<?php echo $jk;?>" <?php if($jk!=1) { ?> style="display:none;" <?php } ?>>
	   <tr>
	   <td>
		<table  id="first_<?php echo $jk;?>"  border="0">
		<tr>
		<td colspan="2">
		<input type="button" name="nextdashboard" value="Next Dashboard" onclick="javascript:document.getElementById('first_<?php echo $jk;?>').style.display='none';document.getElementById('second_<?php echo $jk;?>').style.display='';">
		</td>
		</tr>
		<tr>
		<td>
		Observations		
		</td>		
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/observation/'.$login_type.'_'.$login_id.'_'.$jk.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		<tr>
		<td>
		Notifications 
		</td>
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/notification/'.$login_type.'_'.$login_id.'_'.$jk.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<input type="button" name="nextdashboard" value="Next Dashboard" onclick="javascript:document.getElementById('first_<?php echo $jk;?>').style.display='none';document.getElementById('second_<?php echo $jk;?>').style.display='';">
		</td>
		</tr>
		</table>
		</td>
		<td>
		<table  id="second_<?php echo $jk;?>" border="0" style="display:none;">
		<tr>
		<td colspan="2">
		<input type="button" name="previousdashboard" value="Previous Dashboard" onclick="javascript:document.getElementById('second_<?php echo $jk;?>').style.display='none';document.getElementById('first_<?php echo $jk;?>').style.display='';">
		</td>
		</tr>
		<tr >
		<td>
		Lesson Planning
		</td>
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/lesson/'.$login_type.'_'.$login_id.'_'.$jk.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		<tr>
		<td>
		Professional Development Activity
		</td>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/professional/'.$login_type.'_'.$login_id.'_'.$jk.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<input type="button" name="previousdashboard" value="Previous Dashboard" onclick="javascript:document.getElementById('second_<?php echo $jk;?>').style.display='none';document.getElementById('first_<?php echo $jk;?>').style.display='';">
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		
		<?php } } else { ?>
		<tr>
		<td>
		No Schools Found.
		</td>
		</tr>
		
		<?php } ?>
		</table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
