<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Bank Time::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#changeform").validate({

rules: {
current:{
required: true
},
newp:{
required: true,
maxlength:15,
minlength:2
},
retype:{
required: true,
maxlength:15,
minlength:2,
equalTo: "#newp"
}


},
messages: {
current:{
required: "Please Enter Current Password "

},
newp:{
required: "Please Enter New Password"

},
retype:{
required: "Please  Retype New  Password"

}
}
,
errorElement:"div" 

});

});
</script>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
	    <form action="changepassword/change" name="changeform" id="changeform" method="post" autocomplete="off">
		<table>
		<tr>
		<td>
		<div class="htitle">Change Password</div>
		</td>
		</tr>
		<?php if($msg!='') { ?>
		<tr>
		<td colspan="2" align="center">
		<?php if($type==1) { ?>
		<font color="#990000"><?php echo $msg;?></font>
		<?php } else {  ?>
		<font color="green"><?php echo $msg;?></font>
		<?php } ?>
		</td>
		</tr>
		<?php } ?>
		<tr>
		<td>
		Current Password:
		</td>
		<td>
		<input type="password" name="current" id="current" class="txtbox" value="">
		</td>
		</tr>
		<tr>
		<td>
		New Password:
		</td>
		<td>
		<input type="password" name="newp" id="newp" class="txtbox" value="">
		</td>
		</tr>
		<tr>
		<td>
		Retype New Password:
		</td>
		<td>
		<input type="password" name="retype" id="retype" class="txtbox" value="">
		</td>
		</tr>
		<tr>
		<td colspan="2" align="center"><input type="submit" name="submit" value="Change Password"></td>
		</tr>
		</table>
		</form>		
		</div>
	
     
	
			
			
        
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>



</body>
</html>