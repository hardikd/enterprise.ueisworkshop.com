<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::District::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
$('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});
});

</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <form name="form" id="form" method="post" action="report/schoolhome">
		<table style="margin-left:50px;"  cellpadding="4">
		<tr>
		<td>
		Welcome <?php echo $this->session->userdata('username');?>
		</td>
		</tr>
		<?php if($schools!=false) { ?>
		<tr>
		<td >
		Select School:<select name="school" id="school">
		<option value="0">-Please Select-</option>
		<?php foreach($schools as $val) { ?>
		<option value="<?php echo $val['school_id'];?>"
		<?php if(isset($school_id)&& $school_id==$val['school_id']) { ?> selected <?php } ?>
		><?php echo $val['school_name'];?></option>
		<?php } ?>
		</select>&nbsp;<input type="submit" name="submit" value="Submit">
		</td>
		
		</tr>
		<?php } else { ?>
		<tr>
		<td ">
		Select School:<select name="school" id="school">
		<option value="0">-Please Select-</option>
		</select>&nbsp;<input type="submit" name="submit" value="Submit">
		</td>		
		</tr>
		<?php } ?>
		<tr>
		
		</tr>
		<?php if(isset($set)) { ?>
		<!--start of Periodic Goal Plan-->
		<tr>
		<td>
		<b>Periodic Goal-Setting (Year: <?php echo date('Y');?>-<?php echo date('Y')+1;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteachers;?> of <?php echo $totalsubmitted;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernames!=false){ $k=1;foreach($suteachernames as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernames!=''){ $k=1;foreach($teachernames as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Periodic Goal Plan-->
		<!--start of Lesson Plan-->
		<tr>
		<td>
		<b>Lesson Plans (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteacherslesson;?> of <?php echo $totallesson;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernameslesson!=false){ $k=1;foreach($suteachernameslesson as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernameslesson!=''){ $k=1;foreach($teachernameslesson as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Lesson  Plan-->
		<!--start of Observation Plan-->
		<tr>
		<td>
		<b>Observation Plans (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteachersobservation;?> of <?php echo $totalobservation;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernamesobservation!=false){ $k=1;foreach($suteachernamesobservation as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernamesobservation!=''){ $k=1;foreach($teachernamesobservation as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Observation Plan-->
		<!--start of Observation Plan-->
		<tr>
		<td>
		<b>Scheduled Observations (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Scheduled:&nbsp;&nbsp;</b><?php echo $suteachersobservations;?>
		</td>
		</tr>
		<?php if($suteachernamesobservations!='') { 
		$na=0;
		foreach ($suteachernamesobservations as $namessum) {
		if($na!=$namessum['observer_id']) {
		?>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Observer Name:</b><?php echo $namessum['observer_name'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Scheduled|<?php if($tea[$namessum['observer_id']]!=false){ $k=1;foreach($tea[$namessum['observer_id']] as $name) { echo $k.'. '.$name; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<?php }
         else
		{


        }		
		
		$na=$namessum['observer_id'];
		} } ?>
		<!--<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php //if($teachernamesobservations!=''){ $k=1;foreach($teachernamesobservations as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>-->
		<!--End of Observations-->
		<?php } ?>
		</table>
		</form>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
