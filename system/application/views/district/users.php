<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Dist Users::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/users.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
        <div>District Name:<?php echo $this->session->userdata('user_dist_name');?></div>
		<div id="dist_userdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New" class="btnbig" type="button" name="dist_user_add" id="dist_user_add" value="Add New">
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="District User" style="display:none;"> 

<form name='dist_userform' id='dist_userform' method='post' onsubmit="return false"  >
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>District Name:
				</td>
				<td valign="top">
				<?php echo $this->session->userdata('user_dist_name');?>
				<input  type='hidden'  id='dist_user_id_insert' name='dist_user_id_insert' value="<?php echo $this->session->userdata('user_dist_id');?> ">
				<input  type='hidden'  id='dist_user_id' name='dist_user_id' value="">
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>User Name:
				</td>
				<td valign="top">
				<input class='txtbox' type='text'  id='username' name='username'>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Password:
				</td>
				<td valign="top">
				<input class='txtbox' type='password'  id='password' name='password' >
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Email:
				</td>
				<td valign="top">
				<input class='txtbox' type='text'  id='email' name='email' >
				
				</td>
				
			</tr>
			
			
						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New" class="btnbig" type='submit' name="submit" id='dist_useradd' value='Add' > <input title="Cancel" class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
