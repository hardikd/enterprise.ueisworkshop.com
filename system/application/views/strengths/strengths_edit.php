﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District_id</td>
     <td>Strengths Name</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1 +($page -1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->strengths_name;?></td>
   <td><?php echo $data->status;?></td>
      <td>
      <button class="edit_strengths_name btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>  
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
  <td>
     <button type="Submit" id="remove_strengths_name" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_strengths_name btn btn-danger"><i class="icon-trash"></i></button>
    </td>
    
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_strengths_name').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'strengths/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#strengths_name_id').val(result[0].id);
       $('#strengths_name').val(result[0].strengths_name);
       $('#status').val(result[0].status);
       console.log(result[0].behavior_location);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_strengths_name").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'strengths/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed Strengths Name list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

