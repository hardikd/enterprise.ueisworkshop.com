﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
   <td>Id</td>
     <td>District_id</td>
     <td>Student Assessments</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->student_assessments;?></td>
   <td><?php echo $data->status;?></td>
      <td>
            <button class="edit_student_assessments btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
    </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
  <td>
             <button type="Submit" id="remove_student_assessments" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_student_assessments btn btn-danger"><i class="icon-trash"></i></button>
    </td>
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_student_assessments ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'student_assessments/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#student_assessments_id').val(result[0].id);
       $('#student_assessments').val(result[0].student_assessments);
       $('#status').val(result[0].status);
       console.log(result[0].student_assessments);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_student_assessments").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'student_assessments/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed Student Assessments list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

