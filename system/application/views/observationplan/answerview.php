<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Workshop::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />

</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
        <div style="border:1px #E6B7BF solid;width:650px; padding:10px;">
		<table align="center">
				<tr>
		<td class="htitle">
		<font size="4px">Observation Plan</font>
		</td>
		</tr>

		</table>
		<table  width="650px" cellpadding="0" cellspacing="0" border="0" >
		<tr >
		<td align="left" style="font-size:13px;font-weight:bold;">
		<b>Teacher Name:</b><?php echo $teacher_name;?>
		</td>
		<td align="right" style="font-size:13px;font-weight:bold;">
		<b>Date:</b><?php echo date('m-d-Y');?>
		</td>
		</tr>
		<tr>
		<td>
		PDF:<a target="_blank" style="color:#ffffff;" href="comments/createobservationplan_pdf/<?php echo $teacher_id;?>"><img src="<?php echo SITEURLM?>images/pdf_icon.gif" /></a>
		</td>
		</tr>
		</table>
		<?php if($observationplan!=false) { ?>
		<form action="observerview/commentssave" method="post">
		<table  cellspacing='0' cellpadding='0' border="0" id='conf' width="100%" style="">
		
		<?php 
		if(!empty($message)) { ?>
		<tr >
		<td>
		<font color="green"><?php echo $message;?></font>
		</td>
		</tr>
		<?php }
		$an=0;
		foreach($observationplan as $plan) { ?>
		<tr >
		<td >
       <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b><?php echo $plan['question'];?></b>
        </div>
		</td>
		</tr>
		<?php if(isset($plan['answer'])) {?>
		<tr>
		<td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		<?php echo $plan['answer'];?></div>
		</td>

		<tr>
		<td><div  style="border-left:1px #dce6d0 solid; background:#fdfffa;border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px;color:#466229;">
		<div>Add Comment:</div><textarea style="width:630px;" name="comment<?php echo $plan['observation_ans_id']?>" id="comment<?php echo $plan['observation_ans_id']?>" style="border:1px #dce6d0 solid;padding:"><?php echo $plan['comment']?></textarea></div>
		</td>
		</tr>
		<?php } else {
		$an=1;
		?>
		<tr>
		<td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">No Answer</div>
		</td>
		</tr>
		<?php } ?>
		<tr>
		</tr>
		<?php }  ?>
		
		<tr>
		<td colspan="2" align="center" >
		<input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $teacher_id;?>">
		<input type="hidden" name="an" id="an" value="<?php echo $an;?>">
		<input class="btnsmall" type="submit" name="submit" value="Submit">
		<input class="btnsmall" type="submit" name="submit" value="Archive">
		</td>
		
		</tr>
		</table>
		</form>
		<?php } else { ?>	
		<table>
		<tr>
		<td>
		No Data Found
		</td>
		</tr>
		</table>
		<?php } ?>
			</div>	
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Observation Plan" style="display:none;"> 

<form name='planform' id='planform' method='post' onsubmit="return false" >
<table cellpadding="0" cellspacing="10" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Question:
				</td>
				<td valign="top">
				<textarea name="question" id="question" type="text" class="txtarea"></textarea>
				<input  type='hidden'  id='plan_id' name='plan_id'>
				</td>
			</tr>
			
				
				
			
						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New" class="btnbig" type='submit' name="submit" id='planadd' value='Add' > <input title="Cancel" class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
