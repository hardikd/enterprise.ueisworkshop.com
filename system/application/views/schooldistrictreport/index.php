<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

<!--start old script -->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/jscal2.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/lang/en.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>Quiz/css/jscal2.css" />

<script type="text/javascript">
 
<?php if($records[0]['observer_id']==''){ ?>

function updateassessment(tag)
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var school = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
 	if(tag!=-1 && fDate!='' && tDate!='' && school!=-1)
	{
		$("#reporthtml").html('&nbsp;');
  		$("#loader").show();
 		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getReportHtml?school_id='+school+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+tag,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
 }
 
 function getAssessments(tag)
{
	if(tag!=-1)
	{	
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getassessmentHtml?school_id='+tag,
		success:function(classhtml)
		{ 	
			$("#assessmentdrp").html('&nbsp;');			
			document.getElementById("assessmentdrp").innerHTML= classhtml;
		}
		});
	}
}
 
 function select_f_date()
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
 	
	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getReportHtml?school_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}
function select_t_date()
{
 	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
 	
	if(tag!=-1  && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getReportHtml?school_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}

function importlink()
{
	window.location.href = '<?php echo base_url();?>importdata/'; 
}

<?php }elseif($records[0]['observer_id']!='') { ?> 

function getAssessmentsOb(tag)
{
	if(tag!=-1)
	{	
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getassessmentHtmlObserver?grade_id='+tag,
		success:function(classhtml)
		{ 	
			$("#assessmentdrp").html('&nbsp;');			
			document.getElementById("assessmentdrp").innerHTML= classhtml;
		}
		});
	}
}
function updateassessmentOb(tag)
{
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var grades = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
 	if(tag!=-1 && fDate!='' && tDate!='' && grades!=-1)
	{
		$("#reporthtml").html('&nbsp;');
  		$("#loader").show();
 		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getgradeReportHtml?observerid='+<?php echo $records[0]['observer_id'];?>+'&grade_id='+grades+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+tag,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}  
function select_f_date()
{
 	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
  
  	if(tag!=-1  && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getgradeReportHtml?observerid='+<?php echo $records[0]['observer_id'];?>+'&grade_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}
function select_t_date()
{
 	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
  	
	if(tag!=-1  && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getgradeReportHtml?observerid='+<?php echo $records[0]['observer_id'];?>+'&grade_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
	}
	if(document.getElementById("csvbutton")!=undefined)
		document.getElementById("csvbutton").style.display="none";
}
<?php }   

if($records[0]['observer_id']==''){ ?>

function exporttocsv()
{
 	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('schools').options[document.getElementById('schools').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
 
	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getCsv?school_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#exportcsvlink").html(result);
		}
		});
	}
}
<?php }elseif($records[0]['observer_id']!='') { ?> 
function exporttocsv()
{ 
 	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	var tag = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
 

 	if(tag!=-1 && assessment!=-1 && fDate!='' && tDate!='')
	{
		$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/schooldistrictreport/getgradeCsv?observerid='+<?php echo $records[0]['observer_id'];?>+'&grade_id='+tag+'&fdate='+fDate+'&tdate='+tDate+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#exportcsvlink").html(result);
		}
		});
	}
}
<?php }?>
 
</script>
<!--end old script -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
        <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
   	<?php if($this->session->userdata('login_type')=='user') { ?>
                     <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>schooldistrictreport">District Performance Score by Standard</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                <?php	 }else{ ?>   
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>schooldistrictreport">Class Performance Score by Standard</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                  <?php } ?>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                            	<?php 
                     if($this->session->userdata('login_type')=='user') { ?>
                             <h4>District Performance Score by Standard</h4>
                          <?php }else{?>
                            <h4>Class Performance Score by Standard</h4>
                            <?php }?>
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                          
                          
                                   <fieldset>
   
                                   <div class="space20"></div>
                                   
                                   
                                   <div class="space15"></div>
                                   <form name="frmschoolreport" class="form-horizontal" action="schooldistrictgraph" method="post" > <!--schooldistrictgraph -->
                                   <table>
                                   <tr>
                                   <td>

				
                                   <div class="control-group">
                                             <label class="control-label"><?php if($records[0]['observer_id']==''){?>
		  School  <? } else { ?> Select Grade <?php } ?> &nbsp;</label>
                                             <div class="controls">
                           
                         <?php if($records[0]['observer_id']==''){
		   				?>
          <select class="span12 chzn-select" name="schools" id="schools" style="width: 300px;" onchange="getAssessments(this.value)">
        <option value="-1"  selected="selected">Please Select</option>
        <option value="0" >All</option>
        <?php 
		
	     foreach($records as $key => $value)
		{
			echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
		}
        ?>
                  </select> 
          <?php } else { ?>
				  
		  <select class="span12 chzn-select" name="grades" id="grades" style="width: 300px;" onchange="getAssessmentsOb(this.value)">
        <option value="-1"  selected="selected">Please Select</option>
        <option value="0" >All</option>
    	 <?php 
	    
		   for($i = 0; $i < count($records[1]);$i++)
			{
			
				echo '<option value="'.$records[1][$i]['dist_grade_id'].'">'.$records[1][$i]['grade_name'].'</option>';
			}
        ?>
				</select> 
                 <?php }?>  
                           
                                             </div>
                                         </div> 
                                    
                                             
                                            
                                         
                                         
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Assessment</label>
                                             <div class="controls" id="assessmentdrp">
<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="assessment" id="assessment" >
        <option value="-1"  selected="selected">Please Select</option>
        </select>
        <div>
      
        </div>
                                             </div>
                                             
                                         </div> 
                                         
                                         
                                        <?php
                                    if($this->session->userdata("login_type")=='user'){ ?>  
                                    <input class="btn btn-purple dropdown-toggle" type="button" name="" value="Archive Data" onclick="importlink();"  />  
                                      <?php } ?> 
								         
               
                  
                                         
                                         
                                         
                                         
  								<div class="control-group">
                                             <label class="control-label">Select Time Period</label>
                                             <div class="controls">
                                        <div class="input-prepend">
                                        <label>From</label>
          <span class="add-on"><i class="icon-calendar"></i></span>
          <input value="" name="fromDate" style="width:80px" type="text" class=" m-ctrl-medium icon-calendar"  id="f_date"/>
		 <script type="text/javascript"> 
      	Calendar.setup({
        inputField : "f_date",
        trigger    : "f_date",
        onSelect   : function() { this.hide();
		select_f_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>
                                        </div>
                                    </div>
                                         <div class="controls">
                                        <div class="input-prepend">
								<label>To</label>
							<span class="add-on"><i class="icon-calendar"></i></span>
                 <input value="" name="toDate" style="width:80px" type="text" class=" m-ctrl-medium icon-calendar"  id="t_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "t_date",
        trigger    : "t_date",
        onSelect   : function() { this.hide();
		select_t_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>   
          
                                        </div>
                                    </div>
                                </div>                       
                                         
                                         <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
                                             <div class="btn-group">
 
                    
                    <input class="btn btn-purple dropdown-toggle" type="button" name="exportcsv" value="Generate CSV File" onclick="exporttocsv();"  />
 
 			
 
 
                                 </div>
                                   </div>
                               </div>
                               <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
                                             <div class="btn-group">
                   <div id="exportcsvlink"> </div> 
                                 </div>
                                   </div>
                               </div>
                               
                               
                                
                        <tr>
      <td></td>
       <td valign="bottom" height="30" id="loader" colspan="3" style="display:none;">
       <img src="<?php echo LOADERURL;?>"/>
       </td>
      </tr>
      
       <tr><font style="size:4">
       <td id="reporthtml" colspan="3"></td> </font>
      </tr>
                             
         
                             
                               
                               
                                
                                </td></tr> 
                                </table>
                                  </form>  
                                </fieldset>
                                <div class="space20"></div>
                                
                                                  
                           
                                   
                                      
                                     
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
  
  
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
   <!--end old script -->
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
   
   
   
   
 
</body>
<!-- END BODY -->
</html>