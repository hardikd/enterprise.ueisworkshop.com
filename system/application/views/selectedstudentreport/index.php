<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />


 <!--start old script -->
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>


<script type="text/javascript">
function updateschool(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#schools").html(result);
		}
		});
	
}
</script>
<script type="text/javascript">
function updateclass(tag)
{
		
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getgrades?schoolid='+tag,
		success:function(result)
		{
			$("#grades").html(result);
		}
		});
	
}
</script>
<script type="text/javascript">
function checkvalid()
{
	
	var grade = document.getElementById('grades').value;
	var classrooms = document.getElementById('classrooms').value;
	<?php if(isset($school_type)) {?>
	var schooltype=document.getElementById('schools_type').value;
	var school = document.getElementById('schools').value;

	if(schooltype=="")
	{
		alert('Please select school type');
		return false;
	}
	
	if(school=="")
	{
		alert('Please select school');
		return false;
	}
		 <?php }?>	

	if(grade=="")
	{
		alert('Please select grade');
		return false;
	}
	
if(classrooms=="")
	{
		alert('Please select classroom');
		return false;
	}


document.searchstudent.submit();
	
}
</script>
<script type="text/javascript">
function updateclassroom(gradeid)
{
	var schoolid = $("#schools").val();
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getteachers?grade_id='+gradeid+"&schid="+schoolid,
		success:function(result)
		{
			$("#classrooms").html(result);
		}
		});
}
</script>
<!--end old script -->

<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.12/video.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
  <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                      <!--<a href="#jobaidesModal" data-toggle="modal" ><span>Job Aides</span>&nbsp;<i class="icon-folder-open"></i></a>-->
                      <a href="#myModal-video" data-toggle="modal" style="margin-right:5px;"><span>Video Tutorial</span>&nbsp;<i class="icon-play-circle"></i></a>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>selectedstudentreport">Individual Student Performance by Standard</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                     <fieldset>
                         <div class="widget-title">
                             <h4>Individual Student Performance by Standard</h4>
                          
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                          
                          
                                   
                                   <div class="space20"></div>
                                   
                                   
                                   <div class="space15"></div>
                                   <table>
                                   <tr>
                                   <td>
                                <form class="form-horizontal" action="selectedstudentreport/getstudentacctoschool" method="post" name="searchstudent" >
                                   <div class="control-group">
                                   <?php if(isset($school_type)) $countst = count($school_type);
								if(isset($school_type) && $countst >0){ ?>
                                             <label class="control-label">Select School Type :</label>
                                             <div class="controls">
<select class="span12 chzn-select" name="schools_type" id="schools_type" tabindex="1" style="width: 300px;" onchange="updateschool(this.value);">
      									<option value=""  selected="selected">Please Select</optgroup>
									       <?php  foreach($school_type as $k => $v)
	  										{
											?>
								           <option value="<?php echo $v['school_type_id']; ?>"><?php echo $v['tab']; ?></option>
						            	  <?php } ?>
                        			    </select>
                                             </div>
                                         </div> 
                                         
                                          <div class="control-group">
                                             <label class="control-label">Schools :</label>
                                             <div class="controls">
				<select class="span12 chzn-select" name="schools" id="schools" tabindex="1" style="width: 300px;" onchange="updateclass(this.value)">
                 	<option value=""  selected="selected">Please Select</optgroup>
              						</select>
                                             </div>
                                         </div> 
                                         
                                      <script type="application/javascript"> 
		   var schooltype = document.getElementById('schools_type').options[document.getElementById('schools_type').selectedIndex].value;
		   updateschool(schooltype) </script>
      
        <?php }?>      
                                        
                                        <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
							<?php if($this->session->userdata("login_type")=='teacher'){ ?> 
                			<select class="span12 chzn-select" name="grades" id="grades" tabindex="1" style="width: 300px;">
                		 <?php } elseif($this->session->userdata("login_type")=='observer' || $this->session->userdata("login_type")=='user'){ ?> 
                 <select class="span12 chzn-select" name="grades" id="grades" onchange="updateclassroom(this.value)" tabindex="1" style="width: 300px;">
                 <?php }?>
                   <option value=""  selected="selected">Please Select</optgroup>
                    <?php
					if(isset($school_grade)) $countst = count($school_grade["grades"]);
					if(isset($school_grade) && $countst >0){
						
					foreach($school_grade["grades"] as $k=> $v)
					{
					?>
					 <option value="<?php echo $v["dist_grade_id"];?>"> <?php echo $v["grade_name"]; ?></option>
					<?php } }?>
            	   </select>
                  </div>
                  <?php if(isset($school_grade) && $countst >0){?>
			         <input type="hidden" name="schools" id="schools" value="<?php echo $school_grade["school_id"][0]["school_id"];?>" />
         		<?php }?>
                </div>  
                    <?php if($this->session->userdata("login_type")=='teacher'){ ?>                     
                                        
                                         
                                          <div class="control-group">
                                             <label class="control-label">Select Classroom</label>
                                             <div class="controls">
                                      <select class="span12 chzn-select" name="classrooms" tabindex="1" style="width: 300px;" id="classrooms" >
                   						<option value=""  selected="selected">Please Select</optgroup>
               							 <?php   if(isset($school_grade)) $countcl = count($school_grade["class"]);
											 if(isset($school_grade) && $countcl >0){
							 				foreach($school_grade["class"] as $k=> $v)
											{
											?>
					 				<option value="<?php echo $v["class_room_id"];?>"> <?php echo $v["name"]; ?></option>
								<?php } } ?>
             			 </select>        
                         </div>
                      </div> 
                       <?php } elseif($this->session->userdata("login_type")=='observer' || $this->session->userdata("login_type")=='user') {?>
                       <div class="control-group">
                        <label class="control-label">Teachers :</label>
                           <div class="controls">
                   		 <select class="combobox" name="classrooms" id="classrooms" >
               <option value=""  selected="selected">Please Select</optgroup>
          </select>
                         </div>
                      </div> 
                      <?php }?>
                                         <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls">
 <input type="button"  class="btn btn-small btn-purple" style=" padding: 5px; " value="Search Student" onclick="checkvalid()"  />
 
 
                          </div>
   					</div>  
     				</form>   
                                </td></tr> 
                                </table>
                                
                                <div class="space20"></div>
                                
                                <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Student Report</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">Report Appears Here</h3>
                                 
                                  </div>
                                
                                </div>   
                                </div>                    
                           
                                   
                                      
                                     
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     </fieldset>
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->
   <div id="myModal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:700px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Individual Student Performance by Standard</h3>
                                </div>
                              
                                <div class="modal-body">
                                        
                                    <video id="example_video_1" class="video-js vjs-default-skin"
  controls preload="auto" width="640" height="360"
 
  data-setup='{"example_option":true}'>
 <source src="<?php echo SITEURLM;?>convertedVideos/Individual Student Assessment by StandardConverted.mp4" type='video/mp4' />
 
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
                                </div>
                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<!--<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>-->
                                </div>
                    
                            </div>
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
  
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <!--script for this page-->
    <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   
   
      <script>
       jQuery('#myModal-video').on('hidden.bs.modal', function (e) {
                var myPlayer = videojs('example_video_1');
                myPlayer.pause();
              });
             
          $('#myModal-video').modal('show');
          var myPlayer = videojs('example_video_1');
        myPlayer.play();
      
       </script>
   <!-- END JAVASCRIPTS --> 
  
    
   
   
 
</body>
<!-- END BODY -->
</html>