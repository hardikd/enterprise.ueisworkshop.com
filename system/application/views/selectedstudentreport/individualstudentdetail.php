<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
#leftpanel{background:#fff;float:left; width:auto;}
#rightpanel {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #657455;
    float: right;
    height: auto;
    margin-right: 100px;
    width: 268px;
}

.content {
    color: #5e3364;
    font-size: 11px;
    height: auto;
    margin-left: 10px;
    max-width: 1065px;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
  <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>selectedstudentreport">Individual Student Performance by Standard</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                     <fieldset>
                              
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Search Results</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                              
                                  
                                  
  <div class="content">
<?php
$this->load->model('selectedstudentreportmodel');
@$userdetail =$this->selectedstudentreportmodel->getrecord_student($individualtestdetail[0]['user_id']);
@$schoolname =$this->selectedstudentreportmodel->getschoolname($userdetail[0]['school_id']);
@$testname =$this->selectedstudentreportmodel->getassessment($individualtestdetail[0]['assignment_id']);


          ?>
              <?php   
	@$quizid =$this->selectedstudentreportmodel->getquizidbytestid($testname[0]['id']);
	
	//@$questions =$this->selectedstudentreportmodel->getquestionsbyquizid($quizid[0]['quiz_id']); 
 /*	echo"<pre>";
	print_r($questions);
		
		exit;
	*/
	$testname_id=$testname[0]['id'];
	$quiz_id=$quizid[0]['quiz_id'];
	$school_id=$userdetail[0]['school_id'];
	$student_id=$individualtestdetail[0]['user_id'];
	$assignment_id = $individualtestdetail[0]['assignment_id'];


	  ?>
      
      <span>
      <!--<form method="post" name="pdfform" action="selectedstudentreport/getindividualpdf" target="_blank" >
        <input type="hidden" name="quizid" value="<?php echo $quiz_id;?>" />
        <input type="hidden" name="schoolid" value="<?php echo $school_id;?>" />
        <input type="hidden" name="studentid" value="<?php echo $student_id;?>" />
        <input type="hidden" name="assignmentid" value="<?php echo $assignment_id;?>" />
        <input type="submit"  class="btnperformance" value="View As PDF" />
      </form>-->
</span>
      <fieldset style="border:1px groove #C0B28F; border-radius:3px;">
      <label><span style="font-size:18px; font-family:'Times New Roman', Times, serif;">Student Report</span></label>
         <div id="leftpanel">
  
      <table cellpadding="5" cellspacing="0" width="100%">
      <tr>
      <td><strong>Student</strong></td><td><?php echo @$userdetail[0]['Name'];?></td><td></td>
      </tr>
      <tr>
      <td><strong>School Name</strong></td><td><?php echo @$schoolname[0]['school_name'];?></td><td></td>
      </tr>
       <tr>
      <td><strong>Grade</strong></td><td><?php echo @$userdetail[0]['grade_name'];?></td>
      </tr>
      <tr>
      <td><strong>Assessment Name</strong></td> <td><?php echo @$testname[0]['assignment_name'];?></td><td></td>
      </tr>
      <tr>
      <td><strong>Number Correct</strong></td> <td><?php echo $individualtestdetail[0]['pass_score_point']; ?></td><td></td>
      </tr>
      <tr>
 <td><strong>Percentage Correct</strong></td><td><?php echo $individualtestdetail[0]['pass_score_perc'].' '.'%'; ?></td><td></td>
      </tr>
      <!-- <tr>
      <td>Student Status</td>
       <td><?php //if($individualtestdetail[0]['success'] ==1){ echo "Passed";} else { //echo "Failed"; }?></td><td></td>
      </tr>-->	
      </table>
      
     </div>
     
     <?php 
	 $res = mysql_query('select * from proficiency where assessment_id="'.$assignment_id.'"');
		$row = mysql_fetch_assoc($res);
		
			$bbfrom = $row['bbasicfrom'];
			$bbto = $row['bbasicto'];
			$basicfrom = $row['basicfrom'];
			$basicto = $row['basicto'];
			$pro_from = $row['pro_from'];
			$pro_to = $row['pro_to'];
	 ?>
     
     <div id="rightpanel">
   	<div style="background-color:#5e3364; width:auto; color:#FFFFFF; padding:2px;"> Reoprt Key</div>  

		<table>
       <!-- <tr>
              <td><img src="<?php //echo SITEURLM;?>images/triangle-512.png" width="15" height="15" /></td><td>Advanced</td><td>(86% - 100%)</td>
        </tr>-->
         <tr>
       <td><img src="<?php echo SITEURLM;?>images/circle_green.png" width="15" height="15" /></td> <td>Proficient</td><td>(<?php echo @$pro_from;?>% - <?php echo @$pro_to;?>%)</td>
        </tr>
         <tr>
        <td><img src="<?php echo SITEURLM;?>images/arrow_m01_down-20111002134017-00030.png" width="15" height="15" /></td> <td>Basic</td><td>(<?php echo @$basicfrom;?>% - <?php echo @$basicto;?>%)</td>
        </tr>
         <tr>
             <td><img src="<?php echo SITEURLM;?>images/120px-Orange_rectangle.svg.png" width="15" height="15" /></td> <td>Below Basic</td><td>(<?php echo @$bbfrom;?>% - <?php echo @$bbto;?>%)</td>
        </tr>
           <!--<tr>
        <td><img src="<?php //echo SITEURLM;?>images/red-rect.png" width="15" height="15" /></td><td>Far Below Basic</td><td>(0% - 31%)</td>
        </tr>-->
        
        </table>
        
     </div>
      <div align="right" style="margin-top:105px" >
      <?php if($assignment_id != ''){ 
      echo '<tr><td colspan="3" align="right"> <a href="'.base_url().'selectedstudentreport/pdf?stud_id='.$student_id.'&a_id='.$assignment_id.'" target="_blank"> <input type="button" name="pdfreport" id="pdfreport" value="PDF Report" class="btn btn-small btn-purple" style="margin-right:200px;"/></a>  </td> </tr>';
	  }
	  ?>
	  
     </div>
      <br />
      </fieldset>
      <br /><br /><br />
      <fieldset >
      <label><span style="font-size:18px; font-family:'Times New Roman', Times, serif">Standards Assessed</span></label>
       
     <table width="100%" cellpadding="5" cellspacing="1" >
      <tr style="color:#fff; background:#5e3364;">
      <th>Standards Assessed</th>
      <th>Questions</th>
      </tr>
      <?php
  	  	$cluster =  array();

$questionids = $this->selectedstudentreportmodel->getrepeatclusterinfo($quiz_id);	
		
	  foreach($questionids as $k => $v)
	  {
 	  ?>
      <tr bgcolor="#f2f2f2">
      <td width="53%"><?php $cl = $v['test_cluster'];  
	   // html_entity_decode($v['test_cluster'],ENT_QUOTES);
	   //  htmlspecialchars($v['test_cluster'],ENT_QUOTES);  
      echo utf8_decode($cl); ?></td>
       <td>
	<?php
	$clu = '';
	$clu1 = $v["test_cluster"];
	$clu = str_replace('"','*',$clu1);
	//$clu = utf8_encode($v["test_cluster"]);
	//$clu = htmlentities($v["test_cluster"],ENT_QUOTES);
	
	  	echo '<b>'.$v['cnt'].'</b>&nbsp;<font color="#ffffff">|</font>&nbsp;';
		
 		$res= $this->selectedstudentreportmodel->displayQuestionsResult($individualtestdetail[0]['assignment_id'],$v["test_cluster"],$individualtestdetail[0]['user_id']);
		
		echo '&nbsp;Correct : '.$res["disptrueQuecount"].'&nbsp;<font color="#ffffff">|</font>&nbsp;Incorrect : '.$res["dispfalseQuecount"];
		
		if($v['cnt'] >=1)
		{
		echo '&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-small btn-purple" type="button" name="View Questions" value="View Questions" onclick="displayQuestions';
		echo "('".$clu."')";
		echo '"/>';
		}
     ?>
      
      </td>
      <!--<td></td>-->
      </tr>
      <?php
 	  }
	 
	  ?>
       </table>
      </fieldset>
      
    </div>
                              
                                  
                                 
                              
                                
                                </div>   
                                </div>                    
                           
                                   
                                      
                                     
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     </fieldset>
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   
   <!-- END JAVASCRIPTS --> 
   <!--start old script -->
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/buttonstyle.css"  rel="stylesheet" type="text/css" />

<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	} 
});
</script>

<script type="text/javascript">
function getpdf()
{
	
	$.ajax({
		type: "POST",
   		
		url:'<?php echo base_url();?>/selectedstudentreport/downloadpdf',
		success:function(result)
		{
			
			
		
		}
	
	});
 	
}
function displayQuestions(clustername)
{
	var uid = <?php echo $individualtestdetail[0]['user_id'] ?>;
	var aid = <?php echo $individualtestdetail[0]['assignment_id']; ?>;
	var url = "selectedstudentreportquestions/displayQuestion?cluster_name="+clustername+"&aid="+aid+"&uid="+uid;
	popupWindow =window.open(url,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes,width=1000, height=500,top=100,left=100");

 }
</script>
<!--end old script -->

   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>

 
   
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
    
   
   
 
</body>
<!-- END BODY -->
</html>