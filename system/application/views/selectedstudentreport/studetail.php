<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Teacher Performance Rating::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
	
function delete_stu(userid)
{
	var con =confirm("Are you sure want to delete user");
	if(con)
	{
		$.ajax({
			url:'<?php echo base_url(); ?>studentdetail/delete_student?stuid='+userid,
			success:function(result)
			{
				window.location.reload();
				
				}
			
			});
	}
	else
	{
		return false;
	}
}
</script>
<script type="text/javascript">
function viewgraph(tag)
{
	document.getElementById('light2').style.display='block';
	document.getElementById('fade2').style.display='block';
	
	$.ajax({
	
			url:'<?php echo base_url(); ?>studentdetail/getstudentgraphdetail?stuid='+tag,
			success:function(result)
			{
				var split_data1=result.split("|");
				var getscored = split_data1[0];
				var pass_score = split_data1[1];
				var subj = split_data1[2];
				var status = split_data1[3];
				if(status==1)
				{
					var finalstatus = "passed";
				}
				else
				{
					var finalstatus = "failed";
				}
						
		    $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: false,
                plotShadow: false
            },
            title: {
                text: 'Pie Chart'+'<br/>'+subj+' '+'Student Score Detail'+' '+'Status'+' '+finalstatus
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Student Score',
                data: [
                    ['Pass Score',  eval(pass_score) ],
					['Student Score',   eval(getscored) ]
                ]
            }]
        });
		
		
        var colors = Highcharts.getOptions().colors,
            categories = ['Passed Score', 'Student Score '],
            name = 'Student Detail',
            data = [{
                    y: eval(pass_score),
                    color: colors[0],
                    drilldown: {
                        name: 'Passed Score',
                      
                        data: [eval(pass_score)],
                        color: colors[0]
                    }
                }, {
                    y: eval(getscored),
                    color: colors[1],
                    drilldown: {
                        name: 'Student Score',
                      
                        data: [eval(getscored)],
                        color: colors[1]
                    }
                }];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = $('#containerbar').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Bar Chart'
            },
            /*subtitle: {
                text: 'Click the columns to view versions. Click again to view brands.'
            },*/
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Student Detail'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +'%';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {/*
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +'% market share</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view '+ point.category +' versions';
                    } else {
                        s += 'Click to return to browser brands';
                    }
                    return s;
                */}
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        })
        .highcharts(); // return chart
   
		
			}
		});
	
}
</script>
<script type="text/javascript">
function getperformance(userid)
{

	
	document.getElementById('light1').style.display='block';
	document.getElementById('fade1').style.display='block';
	$.ajax({
			url:'<?php echo base_url(); ?>studentdetail/getstudentperformance?userid='+userid,
			success:function(result)
			{ 
			
				if(result=="norecfound")
				{
				//$("#per td.date").html('');
				$("#per td.pass_score").html('');
				$("#per td.scorepercen").html('');
				$("#per td.assignment").html('');
			$("#per td.error").css('display','block').html("No Record Found").css('text-decoration','underline');
					
				}
				else
				{
				var split_data=result.split("|");
			
			
			$("#per td.assignment").html(split_data[3]);
			$("#per td.scorepercen").html(split_data[2]);
			//	$("#per td.date").html(split_data[0]);
				$("#per td.pass_score").html(split_data[1]);
				
				
				$("#per td.error").css('display','none').html("");
				}
			}
		});
	
}
</script>
<script type="text/javascript">
function editstudent(userid)
{
	
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block';
	$.ajax({
			url:'<?php echo base_url(); ?>studentdetail/getstudentDetail?stuid='+userid,
			success:function(result)
			{
				
				var split_data=result.split("|");
				$("#userid").val(split_data[0]);
				$("#student_name").val(split_data[1]);
				$("#student_surname").val(split_data[2]);
				$("#student_email").val(split_data[3]);
			}
		});
	
}
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light2').style.display='none';
	 document.getElementById('fade2').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light1').style.display='none';
	 document.getElementById('fade1').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light3').style.display='none';
	 document.getElementById('fade3').style.display='none';
	   } 
});
</script>
<style>
.black_overlay {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 158%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content {
	display: none;
	position: absolute;
	top: 25%;
	left: 31%;
	width: 459px;
	padding: 16px;
	border: 1px solid black;
	background-color: white;
	z-index:1002;
	overflow: auto;
}
.black_overlay1 {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 158%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content1 {
	display: none;
	position: fixed;
	top: 25%;
	left: 31%;
	width: 513px;
	padding: 16px;
	border: 1px solid black;
	background-color: white;
	z-index:1002;
	overflow: auto;
}
.black_overlay2 {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 158%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content2 {
	background-color: white;
	border: 1px solid black;
	height: 500px;
	left: 30%;
	overflow: auto;
	padding: 16px;
	position: absolute;
	top: 12%;
	width: 443px;
	display: none;
	z-index: 1002;
}
a.button {
    text-decoration: none;
}
.btnperformance
{
	 display: block;
   height:17px;
    width: 100px;
    background: #8CA275;
    border: 2px solid rgba(33, 68, 72, 0.59);
     
    /*Step 3: Text Styles*/
    color: #FFFFFF;
    text-align: center;
	
    /*font: bold 3.2em/100px "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/
     
    /*Step 4: Fancy CSS3 Styles*/
    background: -webkit-linear-gradient(top, #657455, #657455);
    background: -moz-linear-gradient(top, #657455, #657455);
    background: -o-linear-gradient(top, #657455, #657455);
    background: -ms-linear-gradient(top, #657455, #657455);
    background: linear-gradient(top, #657455, #657455);
     
    -webkit-border-radius: 50px;
    -khtml-border-radius: 50px;
    -moz-border-radius: 50px;
    border-radius: 3px;
     
 
}

a.btnperformance {
    text-decoration: none;
}
 
/*Step 5: Hover Styles*/
a.btnperformance:hover {
    background: #3d7a80;
    background: -webkit-linear-gradient(top, #3d7a80, #2f5f63);
    background: -moz-linear-gradient(top, #3d7a80, #2f5f63);
    background: -o-linear-gradient(top, #3d7a80, #2f5f63);
    background: -ms-linear-gradient(top, #3d7a80, #2f5f63);
    background: linear-gradient(top, #3d7a80, #2f5f63);
}
</style>
<script type="text/javascript">
function updateschool(tag)
{

	$.ajax({
		url:'<?php echo base_url();?>/studentdetail/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#schools").html(result);
		}
		});
	
}
</script>
<script type="text/javascript">
function updateclass(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/studentdetail/getgrades?schoolid='+tag,
		success:function(result)
		{
			$("#grades").html(result);
		}
		});
	
}
</script>
<script type="text/javascript">
function checkvalid()
{
	var schooltype=document.getElementById('schools_type').value;
	var school = document.getElementById('schools').value;
	var grade = document.getElementById('grades').value;
	
	if(schooltype=="")
	{
		alert('Please select school type');
		return false;
	}
	if(school=="")
	{
		alert('Please select school');
		return false;
	}
	if(grade=="")
	{
		alert('Please select grade');
		return false;
	}

document.searchstudent.submit();
	
}
</script>

</head>

<body>
<div class="wrapper">
  <?php require_once($view_path.'inc/header.php'); ?>
  <div class="mbody">
    <?php require_once($view_path.'inc/obmenu.php'); ?>
    <div class="content">
      <div class="search">
      <span style="text-decoration:underline; font-size:16px;">Search Students</span>
        <form action="studentdetail/getstudents" method="post" name="searchstudent" >
        <table>
            <tr>
            <td>Select School Type</td><td>
  <select name="schools_type" id="schools_type" onchange="updateschool(this.value);">
      <option value=""  selected="selected">Please Select</optgroup>
       <?php  foreach($school_type as $k => $v)
	  	{
			?>
		 
              <option value="<?php echo $v['school_type_id']; ?>"><?php echo $v['tab']; ?></option>
              <?php } ?>
                            </select></td>
         
            <td>Schools</td><td><select name="schools" id="schools" onchange="updateclass(this.value)">
                  <option value=""  selected="selected">Please Select</optgroup>
              </select></td>
              
             <td>Grades</td><td><select name="grades" id="grades">
                   <option value=""  selected="selected">Please Select</optgroup>
              </select></td>
         
         
          <td><input type="button" value="Search" onclick="checkvalid()"  /></td>
          </tr>
          
        </table>
        </form>
      </div>
      
      <div id="result">
      
     
      </div>
      
      <div id="scoredetails" style="display:none;">
        <input type="hidden" id="pageid" value="">
        <div id="msgContainer"> </div>
      </div>
      <div class="content" style="margin-left:0;">
        <table cellpadding="5" align="center" id="stud">
          <tbody>
            <tr>
              <td align="left" colspan="5"><b><u> Student Details</u> </b></td>
            </tr>
            <tr class="tchead">
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>School</th>
              <th>View Performance</th>
              <?php if($this->session->userdata('login_type')=='user')
				{
					?>
              <th>Action</th>
              <?php
				}
				?>
            </tr>
            <?php
			
			
			foreach($records as $record)
			{
			
				?>
            <tr class="tcrow2">
              <td><?php echo $record['Name'];?></td>
              <td><?php echo $record['Surname'];?></td>
              <td><?php echo $record['email'];?></td>
              <?php
				 $schools = $this->Studentdetailmodel->getcschools($record['school_id']);
			
			
				?>
              <td><?php echo @$schools[0]['school_name'];?></td>
              <td><!--<input type="button" title="Edit" value="Student Performance" name="Edit" onclick="getperformance(<?php //echo $record['UserID'] ?>)">-->
                
                <!--  <input type="button" title="Edit" value="View As PDF" name="Edit" onclick="viewpdf(<?php echo $record['UserID'] ?>)"> --> 
            
                <a  class="btnperformance" target="_blank" 
 href="<?php echo base_url();?>studentdetail/studentreportaspdf/<?php echo $record['UserID']?>" >View as PDF</a> <br/>
                <?php // echo anchor('studentdetail/studentreportaspdf', 'View As PDF',array('target' => '_blank'));?>
               <!-- <input type="button" title="Edit" value="view As Graph" name="Edit" onclick="viewgraph(<?php //echo $record['UserID'] ?>)">-->
            
<a target="_blank" class="btnperformance" 
 href="<?php echo base_url();?>studentdetail/getsinglestudentdetail/<?php echo $record['UserID']?>" >View as Graph</a>               
            
               
                
                <!--  <a id="<?php //echo $record['UserID']; ?>" href ="javascript:void(0)" onclick = "document.getElementById('light1').style.display='block';document.getElementById('fade1').style.display='block'">Student performance</a>--></td>
              <?php if($this->session->userdata('login_type')=='user')
				{
					?>
              <td><input type="button" title="Edit" value="Edit" name="Edit" onclick="editstudent(<?php echo $record['UserID'] ?>)" class="btnsmall">
                <input type="button" title="Delete" value="Delete" name="Delete" 
onclick="delete_stu(<?php echo $record['UserID'] ?>)" class="btnsmall"></td>
              <?php } ?>
            </tr>
            <?php
			}
						?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php require_once($view_path.'inc/footer.php'); ?>
</div>
<div id="dialog" title="Student Details" style="display:none;"> Student Details </div>

<!-- LIGHT BOX STARTS HERE-->
<div id="light" class="white_content">
  <div style=" background-color:#9CB283; margin: -16px 0 0 -16px; padding: 5px; width: 481px;"> <span style=" color: white; font-family: Tahoma;">Edit Student Detail</span> <span style=" float: right;   margin-right: 5px;"> <a class="ui-icon ui-icon-closethick" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></a> </span> </div>
  <form name="update_student"  method="post" action="studentdetail/updatestudent" >
    <table cellpadding="5" align="center">
      <tbody>
        <tr>
          <td>First Name</td>
          <td><input type="text" name="student_name" id="student_name" value="" /></td>
        </tr>
        <tr>
          <td>Last Name</td>
          <td><input type="text" name="student_surname" id="student_surname" value="" /></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><input type="text" name="student_email"  id="student_email" value=""/></td>
        </tr>
      <input type="hidden" name="userid" value="" id="userid" />
      <tr>
        <td></td>
        <td><input type="submit" name="btn_submit" value="update" /></td>
      </tr>
        </tbody>
      
    </table>
  </form>
</div>
<div id="fade" class="black_overlay"></div>

<!-- LIGHT BOX ENDS HERE--> 

<!-- LIGHT BOX STARTS HERE-->
<div id="light1" class="white_content1">
  <div style=" background-color:#9CB283; margin: -16px 0 0 -16px; padding: 5px; width: 535px;"> <span style=" color: white; font-family: Tahoma;">Student Performance</span> <span style=" float: right;   margin-right: 5px;"> <a class="ui-icon ui-icon-closethick" href = "javascript:void(0)" onclick = "document.getElementById('light1').style.display='none';document.getElementById('fade1').style.display='none'"></a> </span> </div>
  <div id="detail"></div>
  <table cellpadding="5" align="center" id="per">
    <tbody>
      <tr>
        <th>Assignment Name </th>
        <th>Percentage Correct</th>
        <th>Total Points Scored</th>
      <tr> 
        <!-- <td class="date"></td>-->
        <td class="assignment"></td>
        <td class="scorepercen"></td>
        <td class="pass_score"></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td colspan="2" class="error" style="display:none";></td>
      </tr>
    </tbody>
  </table>
</div>
<div id="fade1" class="black_overlay1"></div>

<!-- LIGHT BOX ENDS HERE--> 

<!-- LIGHT BOX STARTS HERE--> 

<!-- Single Student graph  -->
<div id="light2" class="white_content2">
  <div style=" background-color: #9CB283;
    margin: -16px 0 0 -16px;
    padding: 5px;
    width: 448px;"> <span style=" color: white; font-family: Tahoma;">Student Performance ( As Graph )</span> <span style=" float: right;   margin-right: 5px;"> <a class="ui-icon ui-icon-closethick" href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade2').style.display='none'"></a> </span> </div>
  <div id="detail"></div>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
  <script src="<?php echo SITEURLM;?>js/highcharts.js"></script> 
  <script src="<?php echo SITEURLM;?>js/exporting.js"></script>
  <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
  <div id="containerbar" style="min-width: 400px; height: 400px; margin: 0 auto">bar chart</div>
</div>
<div id="fade2" class="black_overlay2"></div>




<!-- Single Student graph  --> 

<!-- LIGHT BOX ENDS HERE-->
</body>
</html>
