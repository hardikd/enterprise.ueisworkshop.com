﻿    <table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' >
     <tr class='tchead'>
     <td>Id</td>
     <td>Needs Home</td>
     <td>Needs For Home Child</td>
     <td>Status</td>
     <td>Add Action</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $cnt = 1;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $cnt;?></td>
     <td><?php echo $data->need_home;?></td>
     <td><?php echo $data->intervention_strategies_home;?></td>
   <td><?php echo $data->status;?></td>
     <td><a href="<?php base_url();?>actions_home/index/<?php echo $data->id;?>">Add Action</a></td>
      <td><input type="button" name="<?php echo $data->id;?>" value="Edit" class="edit_needs_for_home_child btnsmall" id="edit" /></td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
                <td><input type="Submit" id="remove_needs_for_home_child" value="Remove" name="<?php echo $data->id;?>" class="remove_needs_for_home_child btnsmall" /></td>

       <td>
    
  </tr>
  <?php  
  $cnt++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_needs_for_home_child ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'needs_for_home_child/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#needs_for_home_child_id').val(result[0].id);
       $('#needs_id').val(result[0].parent_id);
       $('#needs_for_home_child').val(result[0].intervention_strategies_home);
       $('#status').val(result[0].status);
       console.log(result[0].id);
       $("#dialog").dialog({
      modal: true,
            height:250,
      width: 500
      });
       }
     });  
});

$(".remove_needs_for_home_child").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'needs_for_home_child/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed Needs For Home Child!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

