<style>
    .unselectable {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<table class="table table-striped table-hover table-bordered" id="editable-sample" style="width:100%;">
<thead>
                                     <tr>
                                         <th class="check"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" id="allcheckbox" /></th>
                                         <th class="sorting">Student</th>                                       
                                         <th class="no-sorting sorting">Assignment Type</th>
                                         <th class="no-sorting sorting">Assignment Name</th>
                                         <th class="no-sorting sorting">Grade</th>
                                         
                                        <!-- <th class="no-sorting sorting">Edit</th>-->
<!--                                         <th class="no-sorting sorting">Delete</th>-->
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <?php foreach($studentresult as $studentresultval){?>
                                         <tr class="unselectable" >
                                             <td class="check"><input type="checkbox" class="checkboxes" value="1" id="<?php echo $studentresultval['student_id'];?>" /></td>
                                         <td><?php echo $studentresultval['firstname'].' '.$studentresultval['lastname'];?></td>                                   
                                         <td id="assigntype_<?php echo $studentresultval['student_id'];?>"><?php if(isset($studentresultval['assignment_type'])) echo $studentresultval['assignment_type'];?></td>
                                         
                                         <td  id="assignname_<?php echo $studentresultval['student_id'];?>"><?php if(isset($studentresultval['assignment_name'])) echo $studentresultval['assignment_name'];?></td>
                                         <td  id="grade_<?php echo $studentresultval['student_id'];?>" ondblclick="editrow(<?php echo $studentresultval['student_id'];?>);">
                                             <div id="gradehtml_<?php echo $studentresultval['student_id'];?>">
                                             <?php foreach($homework_profeciency as $profeciency):?>
                                                <?php if(isset($studentresultval['task']) && $profeciency->homework_proficiency_id==$studentresultval['task']) echo $profeciency->tab;?>
                                             <?php endforeach;?>
                                             </div>
                                             <select id="gradetxt_<?php echo $studentresultval['student_id'];?>" style="display:none;width:225px;" onchange="savevalues(<?php echo $studentresultval['student_id'];?>)">
                                                 <option>Select Grade</option>
                                                 <?php foreach($homework_profeciency as $profeciency):?>
                                                     <option value="<?php echo $profeciency->homework_proficiency_id;?>"><?php echo $profeciency->tab;?></option>
                                                 <?php endforeach;?>
                                                 </select>
                                         </td>                                         
                                        <!-- <td  id="editbtn_<?php //echo $studentresultval['student_id'];?>">
                                             <a class="edit" href="javascript:;" id="edit_<?php //echo $studentresultval['student_id'];?>">Edit</a> </td>-->
<!--                                         <td><a class="delete" href="javascript:;" id="delete_<?php //echo $studentresultval['student_id'];?>">Delete</a></td>-->
                                         <input type="hidden" id="homework_feature_<?php echo $studentresultval['student_id'];?>" value="<?php echo $studentresultval['homework_feature_id'];?>">
                                     </tr>
                                     <?php }?>
                                    </tbody>
</table>
<br />
<table class="table table-striped table-hover table-bordered" style="width:100%;display:none;" id="apply_all" >
<tbody>
                                     <tr>
                                         <td class="check" style="width:29.5%"><button class="btn btn-small btn-red" id="apply_to_all" >Apply To All</button></td> 
                                                                               
                                         <td class="no-sorting sorting"><input type="text" name="assignment_type_all" id="assignment_type_all" value=""/></td>
                                         <td class="no-sorting sorting"><input type="text" name="assignment_name_all" id="assignment_name_all" value=""/></td>
                                         <td class="no-sorting sorting">
                                             <select id="gradetxt_all" style="width:100px;" name="gradetxt_all" >
                                                 <option>Select Grade</option>
                                                 <?php foreach($homework_profeciency as $profeciency):?>
                                                     <option value="<?php echo $profeciency->homework_proficiency_id;?>"><?php echo $profeciency->tab;?></option>
                                                 <?php endforeach;?>
                                                 </select></td>
                                                 
                                        <!-- <th class="no-sorting sorting">Edit</th>-->
<!--                                         <th class="no-sorting sorting">Delete</th>-->
                                     </tr>
                                     </tbody>
</table>
                                    <script>
                                         
                                   $(function(){
                                    $('.edit').on('click',function(){
                                            var id = $(this).attr('id');
                                            console.log($(this).attr('id'));
                                            var splitid = id.split('_');
                                            var ass_type = $('#assigntype_'+splitid[1]).html();
                                            var ass_name = $('#assignname_'+splitid[1]).html();
                                            
                                            
                                            $('#assigntype_'+splitid[1]).html('<input type="text" value="'+ass_type+'" name="assignmenttype_'+splitid[1]+'" id="assignmenttype_'+splitid[1]+'" style="width:225px;">');
                                            $('#assignname_'+splitid[1]).html('<input type="text" value="'+ass_name+'" name="assignmentname_'+splitid[1]+'" id="assignmentname_'+splitid[1]+'" style="width:225px;">');
                                           // $('#grade_'+splitid[1]).html('<input type="text" name="gradetxt_'+splitid[1]+'" id="gradetxt_'+splitid[1]+'" style="width:100px;">');
                                           $('#gradetxt_'+splitid[1]).show();
                                           $('#gradehtml_'+splitid[1]).hide();
                                            $('#editbtn_'+splitid[1]).html('<a class="save" href="javascript:;" id="save_'+splitid[1]+'" onclick="savevalues('+splitid[1]+');">Save</a>');
                                        });
                                        });
                                        
                                        function editrow(id){
											
                                            var ass_type = $('#assigntype_'+id).html();
                                            var ass_name = $('#assignname_'+id).html();
                                            
                                            
                                            $('#assigntype_'+id).html('<input type="text" value="'+ass_type+'" name="assignmenttype_'+id+'" id="assignmenttype_'+id+'" style="width:225px;">');
                                            $('#assignname_'+id).html('<input type="text" value="'+ass_name+'" name="assignmentname_'+id+'" id="assignmentname_'+id+'" style="width:225px;">');
                                           // $('#grade_'+splitid[1]).html('<input type="text" name="gradetxt_'+splitid[1]+'" id="gradetxt_'+splitid[1]+'" style="width:100px;">');
                                           $('#gradetxt_'+id).show();
                                           $('#gradehtml_'+id).hide();
											}
                                        
                                        $(document).ready(function() {
                                            $('#allcheckbox').click(function(event) {  //on click 
                                                if(this.checked) { // check select status
                                                    $('.checkboxes').each(function() { //loop through each checkbox
                                                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                                                    });
                                                }else{
                                                    $('.checkboxes').each(function() { //loop through each checkbox
                                                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                                                    });         
                                                }
                                            });

                                        }); 
                                         $('#apply_to_all').click(function(){
    console.log('test');
     $('.checkboxes').each(function() { //loop through each checkbox
//         console.log($(this).attr('id'));
        if(this.checked == true)  
        {
            //$('#edit_'+$(this).attr('id')).trigger('click');
            saveallvalues($(this).attr('id'),$('#assignment_type_all').val(),$('#assignment_name_all').val(),$('#gradetxt_all').val());
        }
            <?php if ($this->session->userdata('login_type') == 'teacher'){?>
                $.ajax({
                url: "<?php echo base_url().'teacherself/gethomeworkrecord';?>",
                type: "POST",
                
                data: { report_date: $('#dp112').val(), lesson_week_plan_id: $('#lesson_week_plan_id').val() }

            })
            <?php }else {?>
              $.ajax({
                url: "<?php echo base_url().'teacherself/gethomeworkrecord';?>",
                type: "POST",
                
                data: { report_date: $('#dp112').val(), lesson_week_plan_id: $('#lesson_week_plan_id').val(),teacher_id:$('#teacher_id').val() }

            })
            <?php }?>
              .done(function( data ) {
                $('#editable-grade-day').html(data);
              });
    });
    
});
                                    </script>
                                    