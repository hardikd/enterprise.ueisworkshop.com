<style type="text/css">
	 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
font-size:9px;
}

</style>

<page backtop="7mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
        <page_header> 
              <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 500px">
                    <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/calendar.png"/>&nbsp;English Language Development Report</b></div>
		</div>
        </page_header> 
        <page_footer> 
                <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
		<br />
                <table style="margin-left:10px;">
                    <tr>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                        </td>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                           Page [[page_cu]] of [[page_nb]] 
                        </td>
                        <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            Printed on: <?php echo date('F d,Y');?>
                        </td>
                    </tr>
                </table>
        </page_footer> 
		
		
		    <table cellspacing="0" style="width:750px;border:2px #939393 solid;">
                    <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                        <td colspan="3" style="padding-left:5px;padding-top:2px;height:36px;" >
                            <b> <?php echo ucfirst($this->session->userdata('district_name'));?></b></td>
                        <td style="text-align:right;padding-right:10px;"></td>
<!--                            <br /><br />-->
                        
                   
                </tr>
                <tr >
                        <td colspan="2" >
                       &nbsp;
                        
                    </td>
                </tr>
                <tr style="margin-top:20px;">
		<td style="padding-left:10px;width:350px;color:#939393; vertical-align: top;">
                    <b>School:</b> <?php if ($this->session->userdata('login_type') == 'teacher'):?>
                        <?php echo ucfirst($this->session->userdata('school_self_name'));?>
                    <?php else:?>
                       <?php echo ucfirst($this->session->userdata('school_name'));?>
                    <?php endif;?>  
                    <br /><br />
		    <b>Teacher:</b> <?php echo $teachername; ?>   
                    <br />
                    <br />
		<b>Student:</b> <?php echo $studentname;?>
		<br />
                <br />
		<b>Year:</b> <?php echo $year;?>
                <br />
                <br />
		<b>Month:</b> <?php echo $month;?>
		<br />
                <br />
		<b>Grade:</b> <?php echo $grade[0]['grade_name'];?>
		</td>
                <td style="padding-left:10px;width:350px;color:#939393;height:50px;">
                    <table style="border:1px solid #939393;height:50px;width:350px;">
                        <tr>
                            <td style="width:350px;height:130px;vertical-align: top;">
                                <b>Description:</b> 
                            </td>
                        </tr>
                    </table>
                </td>
		</tr>
		
                <tr >
                        <td colspan="2" >
                       &nbsp;
                        
                    </td>
                </tr>
		</table>
   <br/>
                <br/>
		<div id="imgcontainer" style="border: 1px solid #939393;width:755px;">
                    <br />
                    <img alt="Logo" width="745" height="450"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/eld_<?php echo $student_id;?>.png" style="position: relative;text-align:right;float:left;margin-left:5px;"/>
                <br />
		</div>	
                
		
		<br />
		<br />
                <br />

  <?php
 $i = 0;
 $j = 0;
foreach ($weeksnumber as $key => $dayssplits) {

                if ($i % 1 == 0) {
                    $j++;
                }
                $dayssplit[$j][] = $dayssplits;
                $i++;
            }
//       print_r($dayssplit);exit;
        foreach ($dayssplit as $spkey => $days) {

           if ($spkey != 1) {?>
                </table>
                <br />
      <?php }?>
      
      
 <table class="gridtable">
        <tr><td></td>

            <?php 
  
  foreach ($days as $weeknumber) {
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 0; $first <= 6; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                    ?>
				<?php if (date('m-d', ($day_ts))!= '' ) {?>
                   <th style="width:54px;vertical-align: top; text-align: center;background: #FFF;font-size: 9px;"><span  >
                       <?php echo date('l', ($day_ts));?>
                       </span></th>
                <?php } else {?>
                    <th><span >&nbsp;</span></th>
				<?php  }}}?>
            </tr>
      <tr><td></td>

            <?php 
  
  foreach ($days as $weeknumber) {
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 0; $first <= 6; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                    ?>
				<?php if (date('m-d', ($day_ts))!= '' && (date('d', ($day_ts))>=1 && date('m', ($day_ts))>=$monthnumber)) {?>
                   <th style="text-align:center;"><span class='blue'><?php echo date('m/d/Y', ($day_ts));?></span></th>
                <?php } else {?>
                    <th><span class='blue'>&nbsp;</span></th>
				<?php  }}}?>
            </tr>
 <?php  //foreach($studentresulthead as $key => $studentvaluehead){?> 
           
            <tr>
            <td class='blue' style="width:100px;"><?php echo $studentresulthead[0]['subject_name'];?></td>
            
                 
			<?php 
  			foreach ($days as $weeknumber) {
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 0; $first <= 6; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                        $k = 0;
                        if ($studentresult != false) {
                            foreach ($studentresult as $studvalue) {
                                
							$date = $studvalue['dates'];
                                     $dates= date('m-d-Y', strtotime(str_replace('-', '/', $date))); 
						
								if ($dates == date('m-d-Y',($day_ts))) {
								$k = 1;?>
                                   <td class='blue'><?php echo $studvalue['tab'];?></td>
                                
                                <?php  }}}
                        if ($k == 0) {?>
							<td>&nbsp;</td>
                        <?php }}}?>
         </tr> 
<?php //}?>
<tr style="border:none;">
<td colspan="8" style="border:none;">&nbsp;</td>
</tr>
<?php }?>
</table>
        
		
   </page>
