﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php');?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/homework_tracker">Homework Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>teacherself/get_retrieve_homework_record">Retrieve Homework Record</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Retrieve Homework Record</h4>
                        </div>
             
                     <div class="widget-body" style="min-height: 75px;">
            <div class="space20"></div>
          
	<table>
		<tr>
		<td>
		Student Name:&nbsp;<?php echo $studentname;?>
		</td>
		</tr>
		<tr>
		<td>
		Year:&nbsp;<?php echo $year;?>
		</td>
		</tr>
		<tr>
		<td>
		Month:&nbsp;<?php echo $monthname;?>
		</td>
		</tr>
		</table>
	</div>
	<div align="center" style="margin:10px;">	
	<table id="list1"></table>	
	</div>
             
                             </div>
               <center>
          <a class="btn btn-large btn-red" target="_blank" href="<?php echo base_url();?>teacherself/homeworkstudentdatapdf/<?php echo $month;?>/<?php echo $year;?>/<?php echo $student_id;?>"><i class="icon-print icon-white"></i> Print</a>
          <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button>
        </center>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
  <!--start old script -->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script> 
	<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
.time_fix{


}
.ui-widget-content{background: none !important;}
</style>
<script type="text/javascript">
 
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){
$("#report_date").datepicker({ dateFormat: 'mm-dd-yy',changeYear: true
		 });
$("#list1").jqGrid({
   	url:'<?=base_url().'teacherself/gethomeworkstudentreportdata/'.$month.'/'.$year.'/'.$student_id?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Subject',<?=$dates?>],	//Grid column headings
   	colModel:[
   		{name:'Subject',index:'Subject', width:135, align:"left", editable:false, classes: 'time_fix'},<?=$cols?>
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:1100,
        hidegrid: false,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,
	gridComplete: function(){
	if($("#list1").parents('div.ui-jqgrid-bdiv').height()>350)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","350px");
	}
	},	
    caption:"&nbsp;&nbsp;&nbsp;Homework Report"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?=$headers;?> ] });

//$("#list1").jqGrid("setColProp","Subject",{frozen:true});
$("#list1").jqGrid("setFrozenColumns");

$(".ui-jqgrid-titlebar").hide();
});
 function getlessonplan()
{

var $msgContainer = $("div#msgContainer");
    
     var datele=$('#report_date').val();
	 
	$.get("teacherself/getlessonplan/"+datele+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html('');		
		$msgContainer.html(msg);
$('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});		
		
       
    });

} 

</script>

<!--end old script -->
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>