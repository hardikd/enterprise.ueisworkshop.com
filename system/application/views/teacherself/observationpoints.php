<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">

.content {
    color: #657455;
    font-size: 11px;
    height: auto;
    margin-left: 10px;
    max-width: 80%;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
 <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 

      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/self_reflection">Self-Reflection</a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>teacherself/index">Generate</a>
                          
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                <div class="space20"></div>
                
                                   <div class="widget red">
                         <div class="widget-title">
                             <h4>Observer Full Report</h4>
                          </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                 
                 
        <?php if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<?php /* if($countgroups>=13) { ?> style="min-height:<?php echo $countgroups*70;?>px;height:auto;" <?php  }  */?>
		<div class="content">
        	<?php if(!empty($reportdata)) {?>
			<table class="tabcontent1" style="width:100%;">
			<tr>
			<td align="left" >
			<?php 
			//echo "<pre>";
			//print_r($reportdata);
			?>
			<b>Report Number:</b></td><td align="left"><a class="bl"><?php echo $this->session->userdata('report_number_new');?></a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a class="bl"><?php echo $reportdata['report_date'];?></a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['observer_name'];?></a>
			</td>
		</tr>

           
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a class="bl"><?php echo $reportdata['teacher_name'];?></a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['school_name'];?></a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a class="bl"><?php echo $reportdata['subject_name'];?></a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a class="bl"><?php echo $reportdata['grade_name'];?></a></td>
			<td align="left">
			<b>Evaluation Type:</b></td><td align="left"><a class="bl"><?php echo $reportdata['period_lesson'];?></a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a class="bl"><?php echo $reportdata['students'];?></a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
                        <td  align="left" colspan="5"><a class="bl"><?php echo $reportdata['paraprofessionals'];?></a></td>
			</tr>
			</table>
            <br />
			<?php } ?>
			<table>
			<tr>
			<td >
			<?php if($groups!=false) {
			
			?>			
			<div class="htitle"><font style="text-transform: uppercase;"><?php
			if($this->session->userdata('newform')=='forma')
			{
				echo 'Checklist';
			}
			else if($this->session->userdata('newform')=='formb')
			{
			  echo 'Scaled Rubric';
			}
			else if($this->session->userdata('newform')=='formc')
			{
			  echo 'Likert';
			} ?></font></div>
			
			<div class="htitle"><?php
			echo $group_name;?></div>
			
			<?php
			echo $description;
			?>
			
			<?php } else { ?>
			No Groups Found
			<?php } ?>
			</td>
			</tr>
			</table>
			<?php if($groups!=false) { ?>
			<form action="teacherself/save" name="" method="post">
			<table>
			
            <tr>
			<td>
			<input type="hidden" name="group_id" value="<?php echo $group_id;?>">
			<input type="hidden" name="next_group_id" value="<?php echo $next_group_id;?>">
			</td>
			</tr>
		  <?php if($points!=false) {	
			
			//echo "<pre>";
		//	print_r($points);
			$point_again=0;
			foreach($points as $val)
			{
			if($val['group_type_id']!=2)
			{
			?>
			<tr>
			<td>
			
			
			<input style="float:left" type="<?php echo $val['ques_type']?>" name="<?php echo $val['point_id'];?>"   
			
<?php if(isset($getreportpoints[$val['point_id']]['response']) && $getreportpoints[$val['point_id']]['response']==1 ) { ?> checked=checked <?php } ?> value="1"> <div style="padding-left:20px;margin-top:3px;"> <?php echo $val['question'];?></div>
			
			</td>
			</tr>
			
			<?php 
			}
			else if($val['group_type_id']==2) {
			$check=0;
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			?>
			
			<tr>
			<td class="queshd" style="padding-left:0px;">
            <br />
			<?php echo $val['question'];?>
       
			</td>
			</tr>
			<tr>
			<td >
			<?php 
			//echo "<pre>";
			//print_r($getreportpoints[$point_id]);?>
			<input style="float:left" type="<?php echo $val['ques_type']?>" 
			<?php 
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  ?>
			name="<?php echo $name?>" value="<?php echo $val['sub_group_id'];?>"   
			<?php 
			if($val['ques_type']=='radio' && !isset($getreportpoints[$point_id]['response'])) {
			
			//$check=1;
			}
			if(isset($getreportpoints[$point_id]['response']) && $getreportpoints[$point_id]['response']==$val['sub_group_id'] && $val['ques_type']!='checkbox' ) { ?> checked=checked <?php } else  
			if(isset($getreportpoints[$point_id][$val['sub_group_id']])) {
			?> 
			checked=checked
			<?php } ?>
			<?php if($check==1) {?> checked=checked <?php } ?>
			> <div style="margin-top:3px;margin-left:20px;"> <?php echo $val['sub_group_name'];?>
			</div>
			</td>
			</tr>
			
			<?php }
			else {
			?>
			
			<tr>
			<td >
			
			<input style="float:left" type="<?php echo $val['ques_type']?>" 
			<?php 
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  ?>
			name="<?php echo $name?>" value="<?php echo $val['sub_group_id'];?>"    
			<?php if(isset($getreportpoints[$point_id]['response']) && $getreportpoints[$point_id]['response']==$val['sub_group_id']  && $val['ques_type']!='checkbox' ) { ?> checked=checked  <?php } else 
			if(isset($getreportpoints[$point_id][$val['sub_group_id']])) {
			?> 
			checked=checked
			<?php } ?>
			> <div style="margin-top:3px;margin-left:20px;">
			<?php echo $val['sub_group_name'];?>
			</div>
			</td>
			</tr>
			<?php
			}
			
			}
			$point_again=$val['point_id'];
			}
			} 
			else
			{
			?>
			
			<tr>
			<td>
			No Observation Points Found
			</td>
			</tr>
			<?php } //print_r($getreportpoints); ?>
		
			<tr>
            
			<td>
                 <br /> <br />
			Notes:&nbsp;&nbsp;&nbsp;<textarea class="ckeditor1" name="response-text"><?php if(isset($getreportpoints[$group_id]['response-text'])) { echo $getreportpoints[$group_id]['response-text']; } ?></textarea>
			</td>
			</tr>
                        <!--</table>-->
			<tr>
			<?php if(isset($prev_group_id) && $prev_group_id!=0 ) { ?>
			<td>
			<a onclick="javascript:document.location.href='<?php echo REDIRECTURL?>teacherself/observationpoints/<?php echo $prev_group_id;?>'" ><input class="btn btn-small btn-red" type="button" name="previous" value="Previous"></a>
			<input type="hidden" name="prev_group_id" value="<?php echo $prev_group_id;?>">
			</td>
			<?php } ?>
			<td align="right">
                            <input class="btn btn-small btn-red" type="submit" name="submit"  value="next" style="float:right;">
			</td>
			</tr>
			</table>
			<?php } ?>
			                 
                          </div>
                          </div> 
                                
                  
                     
                          
                                </div>  
                                    
                                     
                                    
                                     
                                    
                                    
                                 </div>
                             </div>
                 
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  <!-- start ols script-->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/adapters/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/sample.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/sample.css" type=text/css rel=stylesheet>
<script type="text/javascript">
$(function()
{	
	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.ckeditor1').ckeditor();
	});
</script>	



<!-- end old script-->
  
  

 <script>

$("#fullreport").on('click', function() {
   document.getElementById('fullreportDiv').style.display = "block";
   document.getElementById('sectionalreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#sectionalreport").on('click', function() {
   document.getElementById('sectionalreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#qreport").on('click', function() {
   document.getElementById('qreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('sectionalreportDiv').style.display = "none";
});




</script>
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
   </script>  
</body>
<!-- END BODY -->
</html>