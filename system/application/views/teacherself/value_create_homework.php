<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
          <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="../index.html">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="../implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="../implementation/grade-tracker.html">Grade Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="../implementation/enter-homework-record.html">Enter Homework Record</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Enter Homework Record</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-red2">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                 
                                
                               <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium"></input>
                                    </div>
                                </div>
                                        
                                         <div class="control-group">
                                             <label class="control-label">Select Lesson Plan</label>
                                             <div class="controls">
                                             
                                                  <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                            <option>Lesson Plan 1</option>
                                            <option>Lesson Plan 2</option>
                                            <option>Lesson Plan 3</option>
                                            
                                    </select>
                                             
                                            
                                                         
                                             </div>
                                         </div> <BR><BR>
                                      
                                      
                                     
                                     </div>
                                     
                                     
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000">STEP 2</h3>
                                         <div class="control-group">
                             
                                 
                                     
                                    
                                    
                                
                                <div class="space20"></div>
                                
                                
                                    <!-- BEGIN GRID-->
                                    
                                    
                                     <table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
                                     <thead>
                                     <tr>
                                         <th class="check"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                         <th class="sorting">Student</th>                                       
                                         <th class="no-sorting sorting">Assignment Type</th>
                                         <th class="no-sorting sorting">Assignment Name</th>
                                         <th class="no-sorting sorting">Grade</th>
                                         
                                         <th class="no-sorting sorting">Edit</th>
                                         <th class="no-sorting sorting">Delete</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Able Johnson</td>                                   
                                         <td></td>
                                         
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Kareem Barker</td>                                   
                                         <td></td>
                                        
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Blake Carlson</td>                                   
                                         <td></td>
                                        
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Dominque Peralta</td>                                   
                                         <td></td>
                                       
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Lizzy Artis</td>                                   
                                         <td></td>
                                         
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Thomas Carter</td>                                   
                                         <td></td>
                                         <td></td>
                                    
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Jay Tart</td>                                   
                                         <td></td>
                                        
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Chris Camp</td>                                   
                                         <td></td>
                                      
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Kristie Gandy</td>                                   
                                         <td></td>
                                        
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                     <tr class="">
                                         <td class="check"><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td>Xavier Willis</td>                                   
                                         <td></td>
                                        
                                         <td></td>
                                         <td></td>                                         
                                         <td><a class="edit" href="javascript:;">Edit</a> </td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                     </tr>
                                     
                                  
                                     
                                     
                                    
                                     </tbody>
                                 </table>
                             </div>
                         
                     
                     <!-- END EXAMPLE TABLE widget-->
                

            <!-- END EDITABLE TABLE widget--> 
                                    
                                        <!-- END GRID-->
                                
                                
                                 <div class="space20"></div>
                                           
                                           
                                           <div class="span12">
                      
                        <center><button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Review Grade</button> <button class="btn btn-large btn-red"><i class="icon-edit icon-white"></i> Modify Grade</button> <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Next</button> <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Send</button> <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Exit</button></center>
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>