﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php');?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
<?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/homework_tracker">Homework Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>teacherself/get_retrieve_homework_record">Retrieve Homework Record</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Retrieve Homework Record</h4>
                          
                         </div>
                         <div class="widget-body">
     <form  class="form-horizontal" action='teacherself/get_retrieve_homework_report' name="report" method="post" onsubmit="return check()">
                                <div id="pills" class="custom-wizard-pills-red3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <div class="form-horizontal">
                                 
                         <?php if($this->session->userdata('login_type')=='teacher' || $this->session->userdata('login_type')=='parent') { ?>
			<?php if($this->session->userdata('login_type')=='teacher') { ?>
      <div class="space20"></div>
 <?php /*?>  <h3>Teacher Name:<?php echo $this->session->userdata('teacher_name');?></h3><?php */?>
       <input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $this->session->userdata('teacher_id');?>">	
         <?php } ?>
			  <?php if($this->session->userdata('login_type')=='parent') { ?>
              <h3>Parent Name:<?php echo $this->session->userdata('username');?></h3>
                <input type="hidden" name="parent" id="parent" value="<?php echo $this->session->userdata('login_id');?>">		
      
      
      <div class="control-group">
        <label class="control-label">Select Teacher:</label>
        <div class="controls">
          <select class="span12 chzn-select" name="teacher_id" id="teacher_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
			<option value=''>-Please Select-</option>
			  <?php if(!empty($teachers)) { 
			 foreach($teachers as $teachervalue)
			  {?>
	 <option value="<?php echo $teachervalue['teacher_id'];?>"><?php echo $teachervalue['firstname'];?> <?php echo $teachervalue['lastname'];?></option>
			  <?php	 } ?>
			  <?php } ?>
			  </select>
        </div>
      </div>
       <?php } else { ?>
     
      
<!--      <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select name="student" id="student" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''>-Please Select-</option>
 			<?php if(!empty($students)) { 
			foreach($students as $studentsvalue)
			{?>
			  <option value="<?php echo $studentsvalue['student_id'];?>"><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
			  <?php } } ?>			   
		      </select> 
              </div>
      </div>-->
<div class="control-group">
                    <label class="control-label">Select Student</label>
                    <div class="controls">
                        <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;"  />
                        <input type="hidden" name="student" id="student"  />
                    </div>
                </div>
     <?php }  } else { ?>
     
     <div class="control-group">
                                <label class="control-label">Select Teacher</label>
                                <div class="controls">
                   		<select class="combobox span12 chzn-select" name="teacher_id" id="teacher" style="width: 300px;" >
                                    <option value=""  selected="selected">Please Select</option>
                                    <?php
                                        if (!empty($teachers)) {
                                            foreach($teachers as $teacher) {
                                                ?>
                                                <option value="<?php echo $teacher['teacher_id']; ?>" ><?php echo $teacher['firstname'].' '.$teacher['lastname']; ?>  </option>
                                            <?php }
                                        }
                                        ?>
                                </select>
                                </div>
                            </div> 
                      
<!--                            <div class="control-group">
                                <label class="control-label">Select Student</label>
                                <div class="controls">
                                <select name="student" id="students" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                    <option value=''></option>
                                    <option value='all'>All</option>
                                </select> 
                                </div>
                            </div>-->
                <div class="control-group">
                    <label class="control-label">Select Student</label>
                    <div class="controls">
                        <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;"  />
                        <input type="hidden" name="student" id="student"  />
                    </div>
                </div>
      <?php } ?>
			 <?php if($this->session->userdata('login_type')=='parent') { ?>
			 
      
      <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select class="span12 chzn-select"  name="student" id="student" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''>-Please Select-</option>
			 <?php if(!empty($students)) { 
			foreach($students as $studentsvalue)
			  {?>
<option value="<?php echo $studentsvalue['student_id'];?>"><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
			  <?php } } ?>			   
		      </select>
        </div>
      </div>
      	 <?php }  else { ?>
         <?php } ?>
   </div>
             
         
                         <BR><BR>
                         <div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div>
                          <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                         
                                          </div>
                                    
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                              <div class="control-group">
        <label class="control-label">Select Year</label>
        <div class="controls">
          <select class="span12 chzn-select" name="year" id="year" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value="">-Please Select-</option>
			  <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i;			
			echo " >".$i."</option>";    
			} ?>
			  
			  </select>
        </div>
      </div>
                                         <BR><BR>
                                        <div class="space20"></div>
                                         <div class="space20"></div>
                                          <div class="space20"></div>
                                           <div class="space20"></div>
                                            <div class="space20"></div>  
                                      <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                      
                                     </div>
                                     
                                     
                                     
                                      <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000">STEP 3</h3>
                                         <div class="control-group">
                             
                             
                             <div class="control-group">
        <label class="control-label">Select Month</label>
        <div class="controls">
         <select class="span12 chzn-select" name="month" id="month" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
             
			  <option value="1"  >January</option>
			  <option value="2"  >February</option>
			  <option value="3"  >March</option>
			  <option value="4"  >April</option>
			  <option value="5"  >May</option>
			  <option value="6"  >June</option>
			  <option value="7"  >July</option>
			  <option value="8"  >August</option>
			  <option value="9"  >September</option>
			  <option value="10" >October</option>
			  <option value="11" >November</option>
			  <option value="12" >December</option>
			  </select>
        </div>
      </div>             
                </div>
                                     

<!--                      <input type="submit" class="btn btn-large btn-red" name="submit" value="Retrieve Report" onClick="showDiv()" style=" padding: 5px; " />-->
                                     <BR><BR><BR><BR>
                                           
                                           <div class="span12">
                      
                        <center>
                        <button type="submit" class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Review Record</button> 
<!--                        <button class="btn btn-large btn-red"><i class="icon-edit icon-white"></i> Modify Record</button> 
                        <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Next</button> 
                         <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save Record and Exit</button>-->
                         </center>
                         <div class="space20"></div><div class="space20"></div><div class="space20"></div><div class="space20"></div>      <div class="space20"></div>                  
                              <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                    </div>
                    
                          </div>
                         
                                         
                          
                          </div>               
                                    
                                         
                                        
                                           
                                         </div>
                                     
                                     
                                    
                                  
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script>
       $('#teacher').change(function(){
    
	$('#students').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/get_student_by_teacher",
	  data: { teacher_id: $('#teacher').val() },
          success: function(msg){
              $('#students').html('');
		  var result = jQuery.parseJSON( msg )
//		   $('#students').append('<option value="all">All</option>');
		 $(result.students).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#students').append('<option value="'+Element.student_id+'">'+Element.firstname+' '+Element.lastname+'</option>');
		});  
		  $("#students").trigger("liszt:updated");
          }
	})
	  
});
       </script>
   <!-- END JAVASCRIPTS --> 
   <!--start old script -->
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->

	<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
.time_fix{


}
</style>
  <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
function changeparent(id)
{
	var g_url='teacherself/getstudents/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="student" id="student" >';
	if(result.students!=false)
	{
	 str+="<option value=''>-Please Select-</option>";
	$.each(result.students, function(index, value) {
	str+='<option value="'+value['teacher_student_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
     
     var str='<select class="combobox" name="student" id="student">';
	 str+="<option value=''>-No Students Found-</option></select>";
	  
	}
     $('#student').replaceWith(str); 
	 
	 
	 
	 
	 });

}

function check()
{

if(document.getElementById('teacher_id').value=='')
{
 alert('Please Select Teacher');
 return false;

}
else if(document.getElementById('student').value=='')
{
 alert('Please Select Student');
 return false;

}
else if(document.getElementById('year').value=='')
{
 alert('Please Select Year');
 return false;

}
else if(document.getElementById('month').value=='')
{
 alert('Please Select Month');
 return false;

}
else
{
	return true;

}
}
</script>
<script type="text/javascript">
 
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){
$("#report_date").datepicker({ dateFormat: 'mm-dd-yy',changeYear: true
		 });
$("#list1").jqGrid({
   	url:'<?=base_url().'teacherself/gethomeworkstudentreportdata/'.$month.'/'.$year.'/'.$student_id?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Subject',<?=$dates?>],	//Grid column headings
   	colModel:[
   		{name:'Subject',index:'Subject', width:115, align:"left", editable:false, classes: 'time_fix'},<?=$cols?>
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:650,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,
	gridComplete: function(){
	if($("#list1").parents('div.ui-jqgrid-bdiv').height()>350)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","350px");
	}
	},	
    caption:"&nbsp;&nbsp;&nbsp;Homework Report"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?=$headers;?> ] });

$("#list1").jqGrid("setColProp","Subject",{frozen:true});
$("#list1").jqGrid("setFrozenColumns");


});
 function getlessonplan()
{

var $msgContainer = $("div#msgContainer");
    
     var datele=$('#report_date').val();
	 
	$.get("teacherself/getlessonplan/"+datele+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html('');		
		$msgContainer.html(msg);
$('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});		
		
       
    });

} 

</script>
<!--end old script -->
   
   
    <script>
       
 $(document).ready(function() {
	<?php if($this->session->userdata('login_type')=='teacher'):?>				
        $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
            width: 260,
            matchContains: true,
            selectFirst: false
        });
<?php else:?>	
        $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
            width: 260,
            matchContains: true,
            selectFirst: false,
            extraParams:{teacher_id:function(){
      return $('#teacher_id').val();
    }}
        });
<?php endif;?>
	
	 $("#studentlist").result(function(event, data, formatted) {
        $("#student").val(data[1]);
    });
                
            });
            
            

   </script>  
</body>
<!-- END BODY -->
</html>