<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
      <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
        <style type="text/css">

#button {
    background-color: #DE577B;
    border: 0 none;
    color: #FFFFFF;
    cursor: default;
	border-radius: 15px;
    display: inline-block;
    padding: 7px 15px;
	float: right;
    margin-left: 5px;
	text-decoration: none;
	outline: 0 none;
	text-shadow: none !important;
	margin:0px;
}

.icon-reorder {display:none !important;}
.sidebar-toggle-box {background:#5e3364 !important;}
.popoverinfo {font-size:18px !important;}
.infoicon {  position: absolute;
  margin-left: 315px;
  margin-top: -30px;
width:60%;}
</style>
    
    <!--start old script -->


<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/value_feature.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){
var valuestr = '<?php echo $valuestr;?>';
var classworkstr = '<?php echo $classworkstr;?>';

$.fn.getCheckboxVal = function(){
    var vals = [];
    var i = 0;
    this.each(function(){
        vals[i++] = $(this).val();
    });
    return vals;
}

$("#list1").jqGrid({
   	url:'<?=base_url().'teacherself/gridServerPart'?>',	//Backend controller function generating grid XML
	editurl: '<?=base_url().'teacherself/edit_test_save'?>',	//Backend controller function save inline updates
	mtype : "post",		//Ajax request type. It also could be GET
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['','Actions',<?=$times?>],	//Grid column headings
   	colModel:[
   		{name:'firstname',index:'firstname', width:150, align:"left", editable:false, editrules: { edithidden: true }},{name:'act',index:'act', width:110, align:"center", editable:false, search:false},
		<?php foreach($timesvalue as $key=>$value)
		  	{	
		   $cla=explode('_',$value);
		  	?>
		{name:'value_<?php echo $key;?>',index:'time_<?php echo $key;?>', width:115, align:"left", editable:true, edittype:'select',editoptions: {
                 <?php if(isset($cla[1])&& $cla[1]=='classwork')
			{
			?>
				 value: classworkstr,
			<?php 
			
			}
			else
			{
			?>
			
				 value: valuestr,
			<?php } ?>	
			dataEvents: [
                    { type: 'change', fn: function (e) { 
					var rowid = $("#list1").jqGrid('getGridParam','selrow');
					if(this.value=='')
					{
					  var va=0;
					}
					else
					{
					 var va=this.value;
					}
					$.get('teacherself/valueblsave/'+va+'/'+<?php echo $key;?>+'/'+rowid
                            ,function(data){ 
                                /*var res = $(data).html(); 
								var rowid = $("#list1").jqGrid('getGridParam','selrow');
									
							   $("#"+rowid+"_value_<?php echo $key;?>_<?php echo $key;?>").html(res); */
							   $("#list1").trigger("reloadGrid");
							   
							   });
					} }
                ]
            } }, <?php } ?>
		<?=$cols?>   		
   		
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:650,
	height:'auto',
	toolbar: [true,"top"],
    viewrecords: true,
	gridComplete: function(){
		var ids = $("#list1").getDataIDs();
		for(var i=0;i < ids.length;i++){
			var cl = ids[i];
			//se = "<input type='button' value='Save' onclick=\"$('#list1').saveRow('"+cl+"',checksave);\"  />";		//checksave is a callback function to show the response of inline save controller function
			se='';
			ce = "<input type='button' value='Cancel' title='Cancel' onclick=\"$('#list1').restoreRow('"+cl+"');\" />";
			$("#list1").setRowData(ids[i],{act:se+ce});	//Save and Cancel buttons inserted via jqGrid setRowData function
		}
		if($("#list1").parents('div.ui-jqgrid-bdiv').height()>650)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","650px");
	}
	},
	onSelectRow: function(id){
		//if(id && id!==lastsel2)
		{
			$('#list1').restoreRow(lastsel2);	//restore last grid row
			$('#list1').editRow(id,true);		//show form elements for the row selected to enable updates
			lastsel2=id;	//save current row ID so that when focus is gone it can be restored
		}
	},
    caption:"&nbsp;&nbsp;&nbsp;Behavior & Learning  Running Record"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?=$headers;?> ] });


$("#list1").jqGrid("setColProp","firstname",{frozen:true});
$("#list1").jqGrid("setColProp","act",{frozen:true});
$("#list1").jqGrid("setFrozenColumns");
$('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});		

});
 function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 
function checksave(result)
{
	if(result.responseText!='') alert(result.responseText);
	else $("#list1").trigger("reloadGrid");
}
function sendemail()
{

var arr=[];

 
                //function to print the value of each checked checkboxes
	                var i=0;
					$("#list1_frozen :checked").each(function() {
	                    arr[i]=$(this).val();
	                       i++;
	            });
	
//var arr = $("input[name='studentvalues']:checked").getCheckboxVal();
if(arr=='')
{
  alert('Please Select Atleast One Students To Send Email.');

}
else
{
loading_show(); 
 $.post('teacherself/sendemailtostudents',{'pdata':arr},function(data) {
		  
		  
		  if(data.status=='success')
		{
		 alert('Email Send Successfully.');
		 loading_hide(); 

		}
		else
		{
		 alert('Failed Please Try Again.');
		 loading_hide(); 

		}
		  

},'json');	


}

}
</script>

    <!-- end old script -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/grade_tracker">Grade Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>teacherself/value_feature">Enter Grade</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Enter Grade</h4>
                          
                         </div>
                         <div class="widget-body">
                         
                            
                           
                                <div id="pills" class="custom-wizard-pills-red3">
                                 <ul>
                                     <?php 
                                     
                                     if($this->session->userdata('login_type') == 'observer'){?>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     <?php } else {?>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab" id="pillsstep2">Step 2</a></li>
                                     <?php }?>
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
									<?php if($this->session->userdata('login_type') == 'teacher'){?>
											<h3 style="color:#000000;">STEP 1</h3>
                                           <?php }else{ ?>
                                           	<h3 style="color:#000000;">STEP 2</h3>
                                            <?php }?>
                                         
                                     <form name="value_form" class="form-horizontal"  id="value_form" action="teacherself/createvalue" method="post">
           
                                          <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
      <input type="text" id="dp1" readonly name="report_date" size="16" class="m-ctrl-medium" value="<?php echo date('m-d-Y');?>">
                                    </div>
                                </div> 
                                
                                <div class="control-group">
                                             <label class="control-label">Select Lesson Plan</label>
                                             <div class="controls">
                                             
                                   
    <select name="lesson_week_plan_id" id="lesson_week_plan_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
			<option value="">-Please Select-</option>
			<?php if($lessonplans!=false) { foreach($lessonplans as $val) { ?>
			<option value="<?php echo $val['lesson_week_plan_id'];?>"><?php echo $val['starttime'];?>-<?php echo $val['endtime'];?></option>
			<?php } } ?>
			
			</select>
  <div class="infoicon"><a id="popoverData" class="popoverinfo" href="#" data-content="Please create a lesson plan before grading students" rel="popover" data-placement="bottom" data-original-title="Create Lesson Plan" data-trigger="hover"><i class="icon-info-sign"></i></a></div>                                    
                                    </div>
                                    </div>
                                             
                                         
           
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Time Interval</label>
                                             <div class="controls">
       
       <select name="time" id="time" class="span12 chzn-select" data-placeholder="--Please Select--"  tabindex="1" style="width: 300px;">
       
			<option value="">-Please Select-</option>
			<option value="1" onclick="time()">1 Min</option>
			<option value="3" onclick="time()">3 Min</option>
			<option value="5" onclick="time()">5 Min</option>
			<option value="10" onclick="time()">10 Min</option>
            <option value="15" onclick="time()">15 Min</option>
            <option value="30" onclick="time()">30 Min</option>
            <option value="45" onclick="time()">45 Min</option>
			</select>	
                      </div>
                   </div>

                   <div class="control-group" id="subject_name_group" style="display:none;">
                                            <label class="control-label">Subject</label>

                                             <div class="controls" id="subject_name">
                                            </div>
                                        </div>
                     <BR>
                 <BR>
                                  
                                 
                                
                            
                                     <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;">First</a></li>
                                         <li class="previous red"><a href="javascript:;">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
										<li class="next red"><a  href="javascript:void(0);" onClick="$('#value_form').submit();">Next</a></li>	                                     
                                     </ul> 
                                          </div>    
                                            
                                                     
                                      <!-- BEGIN STEP 2-->
                                   
                             </div>
                           </form> 
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
  <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; all ready created class.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $('#popoverData').popover({
   placement: 'right'
});
$('#popoverData1').popover({
   placement: 'right'
});
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });

<?php if ($this->session->userdata('login_type') == 'observer') {?>
    $("#pills").bootstrapWizard("show",1);
<?php } ?>

$('#time').on('change',function(){
		$.ajax({
		  type: "POST",
		  url: "<?php echo base_url();?>teacherself/check_value_feature",
		  data: { date: $('#dp1').val(), lesson_plan_id: $('#lesson_week_plan_id').val(),interval: $('#time').val() }
		})
		  .done(function( msg ) {
				if(msg){
					$('#errorbtn').modal('show');
					$('#errmsg').html('Warning, if you are editing saved data and change the time interval the previous data will reflect the new time interval.');
				}
		  });
	});

    
   </script> 
   <script>
   $(document).ready( function() { 
       
        $('#pillsstep2').click(function(){
           if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#lesson_week_plan_id').val()==''){
               alert('Please Select lesson Plan');
               return false;
           }
		   
           if ($('#time').val()==''){
               alert('Please Select Time Interval');
               return false;
           }
           $('#value_form').submit();
       });
    
     $('#lesson_week_plan_id').on('change',function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>teacherself/getSubjectByLessonPlan/"+$(this).val()
    })
      .done(function( msg ) {
        if(msg!=''){
            $('#subject_name').html(' ');
            $('#subject_name_group').show();
            $('#subject_name').html(msg);
        } else {
            $('#subject_name').html(' ');
            $('#subject_name_group').show();
            $('#subject_name').html('Not Assigned');
        }
      });
  });

    });
	</script> 
</body>
<!-- END BODY -->
</html>
