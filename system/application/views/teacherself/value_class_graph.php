﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<style type="text/css">
	
       .ui-widget-header { border-bottom: 1px solid #eee; 
/*					background:none repeat-x scroll 50% 50% #9d9d9d; */
					color: #FFFFFF; 
					font-weight: bold;
                   	background: #5e3364;
                    /*font-family: "MyriadPro-Light";*/
                    font-weight: normal;
                    font-size: 14px;
					font-weight:bold;
}
.ui-icon-circle-triangle-n {display: none !important;}


</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); 
require_once($view_path.'inc/highcharts/Highchart.php'); 
?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
  <?php require_once($view_path.'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        
        <h3 class="page-title"> <i class="icon-wrench"></i>&nbsp; Tools & Resources </h3>
       <ul class="breadcrumb" >
            <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
            <li> <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools">Tools & Resources</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a> <span class="divider">></span> </li>
             <li> <a href="<?php echo base_url();?>tools/data_tracker_report">Grade Tracker</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>teacherself/getclassworkstudentgraph">Grade Retrieve Graph Report</a> </li>
          </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER--> 
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
    <div class="span12">
    <!-- BEGIN BLANK PAGE PORTLET-->

      <div class="space20"></div>
      <div id="reportDiv"  style="display:block;" class="answer_list" >
        <div class="widget purple">
          <div class="widget-title">
            <h4>Grade Tracker Graph Report</h4>
          </div>
          <div class="widget-body" style="min-height: 150px;">
            <div class="space20"></div>
       
            
     <table>
		<tr>
		<td>
		Student Name:<?php echo $studentname;?>
		</td>
		</tr>
		<tr>
		<td>
		Year:<?php echo $year;?>
		</td>
		</tr>
		<tr>
		<td>
		Month:<?php echo $monthname;?>
		</td>
		</tr>
<!--		<tr>
		<td>
		PDF:<a target="_blank" style="color:#ffffff;" href="teacherself/eldstudentdatapdf/<?php echo $month;?>/<?php echo $year;?>/<?php echo $student_id;?>"><img src="<?php echo SITEURLM?>images/pdf_icon.gif" /></a>
		</td>
		</tr>-->
		</table>
	<table>
		<tr>
		<td>Date:</td><td><input type="text" name="report_date" id="report_date" readonly ></td><td><input type="button" onclick="getlessonplan()" name="getdata" id="getdata" value="Get Lesson Plan"></td>
		</tr>
		
		</table>
		
		<table align="center">
		<tr>
		<td>
		<div id="msgContainer">
		</div>
		</td>
		</tr>
		<tr>
		<td style="padding-left:10px;">
		<?php
		
		
		$cach = date("H:i:s");
		$login_type=$this->session->userdata('login_type');
           
				 if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='parent')
			{
				$login_id=$this->session->userdata('login_id');

			}
		$rcc=1;	
		$chart='chart'.$rcc;						
		$$chart = new Highchart();
		 foreach ($$chart->getScripts() as $script) {
         echo '<script type="text/javascript" src="' . $script . '"></script>';
      }
		
	echo '<script src="'.SITEURLM.'js/exporting.js"></script>
				<script type="text/javascript">jQuery.noConflict();</script>';
				echo "<script type='text/javascript'>
		var theme = {
   colors: ['#058DC7', '#50B432', '#910000', '#33C6E7', '#492970', '#F6F826', '#263C53', '#FFF263', '#6AF9C4'],
   chart: {
      backgroundColor: {
         
      }
   },
   title: {
      style: {
         color: '#000',
         font: 'bold 16px Trebuchet MS, Verdana, sans-serif'
      }
   },
   subtitle: {
      style: {
         color: '#666666',
         font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
      }
   },
   xAxis: {
      gridLineWidth: 1,
      lineColor: '#000',
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
			
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
      }
   },
   yAxis: {
      minorTickInterval: 'auto',
      lineColor: '#000',
      lineWidth: 1,
      tickWidth: 1,
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'
         }
      }
   },
   legend: {
      itemStyle: {
         font: '9pt Trebuchet MS, Verdana, sans-serif',
         color: 'black'

      },
      itemHoverStyle: {
         color: '#039'
      },
      itemHiddenStyle: {
         color: 'gray'
      }
   },
   labels: {
      style: {
         color: '#99b'
      }
   },

   navigation: {
      buttonOptions: {
         theme: {
            stroke: '#CCCCCC'
         }
      }
   }
};

var highchartsOptions = Highcharts.setOptions(theme);
</script>		";
		
		for($pj=0;$pj<count($weeks);$pj++)
		{
		  if(!empty($studentresult))
		  {
		  foreach($valueplans as $key=>$valupl)
			{
			$exist=0;
		  foreach($studentresult as $studenvalue)
		  {
		    
			
			
			if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab'])
			{
				
				$notify[$key][$pj]=intval($studenvalue['count']);
				$exist=1;
			}
			
			
		   }
		   if($exist==0)
			{
			  $notify[$key][$pj]=0;
			}
		  }
		  
		  }
		  else
		  {
		  foreach($valueplans as $key=>$valupl)
			{
		    $notify[$key][$pj]=0;
			}
		  }
		 $weeksdata[]=$weeks[$pj]['week']	;
		}
		
			
	  
		foreach($valueplans as $key=>$valupl)
			{
			
		
		//$MyData->addPoints($notify[$key],$valupl['tab']);
		$$chart->series[] = array('name' => $valupl['tab'],'data' => $notify[$key]);	
		
		
		}
		
		
      
	  
		
	
	//$MyData->addPoints($weeksdata,'Element');
	
	$$chart->chart->renderTo = "container".$rcc;
			$$chart->chart->type = "column";			
			$$chart->title->text = "Grade Tracker ";
			$$chart->xAxis->categories = $weeksdata;
			$$chart->xAxis->labels->rotation = -50;			
			$$chart->credits->text = '';			
			
			$$chart->yAxis->min = 0;
			$$chart->yAxis->allowDecimals =false;
			$$chart->exporting->enabled =true;
$$chart->yAxis->title->text = "";
$$chart->yAxis->stackLabels->enabled = 1;
$$chart->yAxis->stackLabels->style->fontWeight = "bold";
$$chart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$chart->legend->align = "right";
$$chart->legend->x = -100;
$$chart->legend->verticalAlign = "top";
$$chart->legend->y = 20;
$$chart->legend->floating = 1;
$$chart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$chart->legend->borderColor = "#CCC";
$$chart->legend->borderWidth = 1;
$$chart->legend->shadow = false;
$$chart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$chart->plotOptions->column->stacking = "normal";
$$chart->plotOptions->column->dataLabels->enabled = 1;
$$chart->plotOptions->column->dataLabels->align = 'center';
$$chart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$chart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
	
		?>
		
		
		<?php //echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/behaviour/'.$login_type.'_'.$login_id.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		<div id="container<?php echo $rcc?>" style="width:80%;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='chart'.$rcc;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
		</td>
		</tr>
		</table>
        </div>
        
	<!--<div align="center" style="margin:10px;">	
	<table id="list1"></table>	
	</div>
        -->    
         
          </div>
        </div>
        <div class="space20"></div>
        <div class="space20"></div>
        <div class="space20"></div>
        <center>
            <a class="btn btn-large btn-purple" href="<?php echo base_url();?>teacherself/classstudentdatapdf/<?php echo $month;?>/<?php echo $year;?>/<?php echo $student_id;?>" target="_blank"><i class="icon-print icon-white"></i> Print</a>
          <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Colleague</button>
        </center>
      </div>
      </div>
      </div>
   
  </div>
</div>
<!-- END BLANK PAGE PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

 <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 

<!--script for this page only--> 
<!-- START OLD SCRIPT-->

	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
<script src="<?php echo SITEURLM?>/js/jquery-1.8.3.min.js"></script> 	
	<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
.time_fix{


}
</style>
<script type="text/javascript">
function changeparent(id)
{
	var g_url='teacherself/getstudents/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox" name="student" id="student" >';
	if(result.students!=false)
	{
	 str+="<option value=''>-Please Select-</option>";
	$.each(result.students, function(index, value) {
	str+='<option value="'+value['teacher_student_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
     
     var str='<select class="combobox" name="student" id="student">';
	 str+="<option value=''>-No Students Found-</option></select>";
	  
	}
     $('#student').replaceWith(str); 
	 
	 
	 
	 
	 });

}

function check()
{

if(document.getElementById('teacher_id').value=='')
{
 alert('Please Select Teacher');
 return false;

}
else if(document.getElementById('student').value=='')
{
 alert('Please Select Student');
 return false;

}
else if(document.getElementById('year').value=='')
{
 alert('Please Select Year');
 return false;

}
else if(document.getElementById('month').value=='')
{
 alert('Please Select Month');
 return false;

}
else
{
	return true;

}
}
</script>


<script type="text/javascript">
 
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){
$("#report_date").datepicker({ dateFormat: 'mm-dd-yy',changeYear: true
		 });
$("#list1").jqGrid({
   	url:'<?=base_url().'teacherself/getclassstudentreportdata/'.$month.'/'.$year.'/'.$student_id?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Subject',<?=$dates?>],	//Grid column headings
   	colModel:[
   		{name:'Subject',index:'Subject', width:150, align:"left", editable:false, classes: 'time_fix'},<?=$cols?>
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:1050,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,
	gridComplete: function(){
	if($("#list1").parents('div.ui-jqgrid-bdiv').height()>350)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","350px");
	}
	},	
    caption:"&nbsp;&nbsp;&nbsp;Classwork Performance "
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?=$headers;?> ] });

$("#list1").jqGrid("setColProp","Subject",{frozen:true});
$("#list1").jqGrid("setFrozenColumns");


});
 function getlessonplan()
{

var $msgContainer = $("div#msgContainer");
    
     var datele=$('#report_date').val();
	 
	$.get("teacherself/getlessonplan/"+datele+"?num=" + Math.random(), function(msg){
	
		$msgContainer.html('');		
		$msgContainer.html(msg);
$('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});		
		
       
    });

} 

</script>


<!-- END OLD SCRIPT-->

<script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script> 
<script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>
   <style>
   h3#cluetip-title {background: #de577b !important;}
   
   </style>
</body>
<!-- END BODY -->
</html>