<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />


<!--end old script -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php');?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
  <?php require_once($view_path.'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12"> 
        
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        
        <h3 class="page-title"> <i class="icon-wrench"></i>&nbsp; Tools & Resources </h3>
        <ul class="breadcrumb" >
          <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
          <li> <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a> <span class="divider">></span> </li>
          <li> <a href="<?php echo base_url();?>tools">Tools & Resources</a> <span class="divider">></span> </li>
          <li> <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a> <span class="divider">></span> </li>
          <li> <a href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a> <span class="divider">></span> </li>
          <li> <a href="<?php echo base_url();?>teacherself/gethomeworkstudentreport">Homework Tracker</a> </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB--> 
      </div>
    </div>
    <!-- END PAGE HEADER--> 
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
    <div class="span12">
    <!-- BEGIN BLANK PAGE PORTLET-->
    <div class="widget purple">
    <div class="widget-title">
      <h4>Homework Tracker</h4>
    </div>
    <div class="widget-body">
	<form  class="form-horizontal" action='teacherself/get_student_homework_report' name="report" method="post" onsubmit="return check()">
    <?php if($this->session->userdata('login_type')=='teacher' || $this->session->userdata('login_type')=='parent') { ?>
			<?php if($this->session->userdata('login_type')=='teacher') { ?>
      <div class="space20"></div>
   <!--<h3>Teacher Name:<?php echo $this->session->userdata('teacher_name');?></h3>-->
       <input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $this->session->userdata('teacher_id');?>">	
         <?php } ?>
			  <?php if($this->session->userdata('login_type')=='parent') { ?>
              <h3>Parent Name:<?php echo $this->session->userdata('username');?></h3>
                <input type="hidden" name="parent" id="parent" value="<?php echo $this->session->userdata('login_id');?>">		
      
      
      <div class="control-group">
        <label class="control-label">Select Teacher:</label>
        <div class="controls">
          <select class="span12 chzn-select" name="teacher_id" id="teacher_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
			<option value=''>-Please Select-</option>
			  <?php if(!empty($teachers)) { 
			 foreach($teachers as $teachervalue)
			  {?>
	 <option value="<?php echo $teachervalue['teacher_id'];?>"><?php echo $teachervalue['firstname'];?> <?php echo $teachervalue['lastname'];?></option>
			  <?php	 } ?>
			  <?php } ?>
			  </select>
        </div>
      </div>
       <?php } else { ?>
     
      
      <div class="control-group">
            <label class="control-label">Select Student</label>
            <div class="controls">
                <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;"  />
                <input type="hidden" name="student" id="student"  />
            </div>
        </div>
     <?php }  } else { ?>
     
     <div class="control-group">
        <label class="control-label">Select Teacher</label>
        <div class="controls">
          <select class="span12 chzn-select" name="teacher_id" id="teacher_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''>-Please Select-</option>
			  <?php if(!empty($teachers)) { 
			foreach($teachers as $teachervalue)
			  {?>
			  <option value="<?php echo $teachervalue['teacher_id'];?>"><?php echo $teachervalue['firstname'];?> <?php echo $teachervalue['lastname'];?></option>
			  <?php	} ?>
			 <?php } ?>
			  </select>
        </div>
      </div>
     
     
<!--          <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select class="span12 chzn-select" name="student" id="student" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
             <option value=''>-Please Select-</option>
 						   
		      </select>
        </div>
      </div>-->
<div class="control-group">
            <label class="control-label">Select Student</label>
            <div class="controls">
                <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;"  />
                <input type="hidden" name="student" id="student"  />
            </div>
        </div>
      <?php } ?>
			 <?php if($this->session->userdata('login_type')=='parent') { ?>
			 
      
      <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select class="span12 chzn-select"  name="student" id="student" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''>-Please Select-</option>
			 <?php if(!empty($students)) { 
			foreach($students as $studentsvalue)
			  {?>
<option value="<?php echo $studentsvalue['student_id'];?>"><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
			  <?php } } ?>			   
		      </select>
        </div>
      </div>
      	 <?php }  else { ?>
         <?php } ?>
      <div class="control-group">
        <label class="control-label">Select Year</label>
        <div class="controls">
          <select class="span12 chzn-select" name="year" id="year" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value="">-Please Select-</option>
			  <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i;			
			echo " >".$i."</option>";    
			} ?>
			  
			  </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label">Select Month</label>
        <div class="controls">
          <select class="span12 chzn-select" name="month" id="month" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
             <option value="all">-All-</option>
			  <option value="1"  >January</option>
			  <option value="2"  >February</option>
			  <option value="3"  >March</option>
			  <option value="4"  >April</option>
			  <option value="5"  >May</option>
			  <option value="6"  >June</option>
			  <option value="7"  >July</option>
			  <option value="8"  >August</option>
			  <option value="9"  >September</option>
			  <option value="10" >October</option>
			  <option value="11" >November</option>
			  <option value="12" >December</option>
			  </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
          <input type="submit" class="btn btn-small btn-purple" name="submit" value="Retrieve Report" onClick="showDiv()" style=" padding: 5px; " />
        </div>
      </div>
  
      </div>
      </div>
      </div>
      </div>
      </div>

  </div>
</div>
<!-- END BLANK PAGE PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS Â© Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 

<!--common script for all pages--> 
<script src="<?php echo SITEURLM?>js/common-scripts.js"></script> 
<!--script for this page--> 
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script> 
<script src="<?php echo SITEURLM?>js/form-component.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script> 
<script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
<!--script for this page only--> 

<script>
    $(document).ready(function() {
				
        <?php if($this->session->userdata('login_type')=='teacher'):?>				
        $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
            width: 260,
            matchContains: true,
            selectFirst: false
        });
<?php else:?>	
        $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
            width: 260,
            matchContains: true,
            selectFirst: false,
            extraParams:{teacher_id:function(){
      return $('#teacher_id').val();
    }}
        });
<?php endif;?>
	
        $("#studentlist").result(function(event, data, formatted) {
            $("#student").val(data[1]);
        });
         <?php if($this->session->userdata('login_type')!='parent'){?>
        $('#teacher_id').change(function(){
	$('#students').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/get_student_by_teacher",
	  data: { teacher_id: $('#teacher_id').val() }
	})
	  .done(function( msg ) {
		  $('#student').html('');
		  var result = jQuery.parseJSON( msg )
//		   $('#student').append('<option value="all">All</option>');
		 $(result.students).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#student').append('<option value="'+Element.student_id+'">'+Element.firstname+' '+Element.lastname+'</option>');
		});  
		  $("#student").trigger("liszt:updated");
		  
	  });
  	<?php }?>
});
                
    });
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script> 
<script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>
</body>
<!-- END BODY -->
</html>