<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
	<style type="text/css">
.ui-jqgrid {
	font-size: 12px;
	position: relative;
}
.ui-widget-header {background: #5e3364 !important; color: #FFFFFF; height: 50px;line-height: 50px; text-align: center;}
.ui-icon-circle-triangle-n {display: none !important;}
</style>



</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a>
                           <span class="divider">></span> 
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>teacherself/getteacher_consolidated">Professional Development Activity</a>
                          
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     
        
                         
		
                        <div class="space20"></div>
                        <div id="reportDiv"  style="display:block;" class="answer_list" >
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Professional Development Activity Cumulative Report</h4>
                        	 </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                   <div class="space20"></div>
                                  
                                      
	       <table>
		 <tr>
		 <td>
		 Teacher Name:
		 </td>
		 <td><?php echo $this->session->userdata('teacher_name') ;?></td>		 
		 </tr>		 
		 <tr>
		 <td>
		 Year:
		 </td>
		 <td><?php echo $year?></td>		 
		 </tr>		 
<!--		 <tr>
		 <td>
		 PDF:
		 </td>
		 <td>
		 <a target="_blank" style="color:#ffffff;" href="teacherself/teacher_consolidated_pdf/<?php echo $year?>">
		 <img src="<?php echo SITEURLM?>images/pdf_icon.gif" >
		 </a>
		 </td>
		 </tr>-->
		 </table>
		 <table align="center">		
		 <tr>
		 <td>
		 <div style="margin-top:10px;">	
		 <table id="list1"></table>	
	    </div>
		 </td>
		 </tr>
		 <tr>
		 <td>
		 <div style="margin-top:10px;">	
		 <table id="list2"></table>	
	    </div>
		 </td>
		 </tr>
		 <tr>
		 <td >
		 Legend:
		 </td>
		 </tr>
		 <tr>
		 <td>Blue:&nbsp;&nbsp;<font color='#5987D3'>Formal</font></td>
		 </tr>
		 <tr>
		 <td>Red:&nbsp;&nbsp;<font color='#DD6435'>Informal</font></td>
		 </tr>
		 <tr>
		 <td>
		 <div style="margin-top:10px;">	
		 <table id="list3"></table>	
	    </div>
		 </td>
		 </tr>
		 </table>
	
	
                              
                              
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                       <center><a target="_blank" class="btn btn-large btn-purple" href="<?php echo base_url();?>teacherself/teacher_consolidated_pdf/<?php echo $year?>"><i class="icon-print icon-white"></i> Print</a> <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                                           
                                         </div>
                                     </div>
                                     
                                    
                                   
                                 </div>
                             </div>
                         
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>	
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
   <!--start old scrript -->
	<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	

	<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>

<script type="text/javascript">

function check()
{

 if(document.getElementById('year').value=='')
{
 alert('Please Select Year');
 return false;

}
else
{
	return true;

}
}
</script>
<script type="text/javascript">
 
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){

$("#list1").jqGrid({
   	url:'<?php echo base_url().'teacherself/getteacherprofessionalconsolidated/'.$year?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['',''],	//Grid column headings
   	colModel:[
   		{name:'Professional',index:'Professional', width:525, align:"left", editable:false},
		{name:'ProfessionalValue',index:'ProfessionalValue', width:515, align:"left", editable:false}
  	],
   	rowNum:100,
   	width:1050,  	
   	pager: '#pager1',
        
        shrinkToFit: false,
        hidegrid: false,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,		
    caption:"&nbsp;&nbsp;&nbsp;Professional Development Activity"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});


$("#list2").jqGrid({
   	url:'<?=base_url().'teacherself/getteachernotificationconsolidated/'.$year?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Month','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],	//Grid column headings
   	colModel:[
   		{name:'Month',index:'Month', width:120, align:"left", editable:false},
		{name:'Jan',index:'Jan', width:80, align:"left", editable:false},
		{name:'Feb',index:'Feb', width:80, align:"left", editable:false},
		{name:'Mar',index:'Mar', width:80, align:"left", editable:false},
		{name:'Apr',index:'Apr', width:80, align:"left", editable:false},
		{name:'May',index:'May', width:80, align:"left", editable:false},
		{name:'Jun',index:'Jun', width:80, align:"left", editable:false},
		{name:'Jul',index:'Jul', width:80, align:"left", editable:false},
		{name:'Aug',index:'Aug', width:80, align:"left", editable:false},
		{name:'Sep',index:'Sep', width:80, align:"left", editable:false},
		{name:'Oct',index:'Oct', width:80, align:"left", editable:false},
		{name:'Nov',index:'Nov', width:80, align:"left", editable:false},
		{name:'Dec',index:'Dec', width:80, align:"left", editable:false}
  	],
   	rowNum:100,
   	   	
   	pager: '#pager2',
	autowidth:true,
        shrinkToFit: true,
        hidegrid: false,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,	
    caption:"&nbsp;&nbsp;&nbsp;Notification Activity"
}).navGrid('#pager2',{edit:false,add:false,del:false,search:false});

$("#list2").jqGrid("setColProp","Month",{frozen:true});
$("#list2").jqGrid("setFrozenColumns");

$("#list3").jqGrid({
   	url:'<?=base_url().'teacherself/getteacherobservationconsolidated/'.$year?>',	
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Month','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],	//Grid column headings
   	colModel:[
   		{name:'Month',index:'Month', width:120, align:"left", editable:false},
		{name:'Jan',index:'Jan', width:120, align:"left", editable:false},
		{name:'Feb',index:'Feb', width:120, align:"left", editable:false},
		{name:'Mar',index:'Mar', width:120, align:"left", editable:false},
		{name:'Apr',index:'Apr', width:120, align:"left", editable:false},
		{name:'May',index:'May', width:120, align:"left", editable:false},
		{name:'Jun',index:'Jun', width:120, align:"left", editable:false},
		{name:'Jul',index:'Jul', width:120, align:"left", editable:false},
		{name:'Aug',index:'Aug', width:120, align:"left", editable:false},
		{name:'Sep',index:'Sep', width:120, align:"left", editable:false},
		{name:'Oct',index:'Oct', width:120, align:"left", editable:false},
		{name:'Nov',index:'Nov', width:120, align:"left", editable:false},
		{name:'Dec',index:'Dec', width:120, align:"left", editable:false}
  	],
   	rowNum:100,
   	  	
   	pager: '#pager3',
	autowidth:true,
        shrinkToFit: true,
        hidegrid: false,
	height:'auto',	
	toolbar: [true,"top"],
    viewrecords: true,	
    caption:"&nbsp;&nbsp;&nbsp;Instructional Observation Activity"
}).navGrid('#pager3',{edit:false,add:false,del:false,search:false});

$("#list3").jqGrid("setColProp","Month",{frozen:true});
$("#list3").jqGrid("setFrozenColumns");





});
 

</script>

<!-- end old script -->
   
   
   
   
      <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}
</script>
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
   </script>  
<!--start old script -->

<!--end old script -->   
 
</body>
<!-- END BODY -->
</html>