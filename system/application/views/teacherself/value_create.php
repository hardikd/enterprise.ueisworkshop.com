<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    


	
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/grade_tracker">Grade Tracker</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>teacherself/createvalue">Enter Grade</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Enter Grade</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-red3">
                                 <ul>
                                     <?php 
                                     
                                     if($this->session->userdata('login_type') == 'observer'){?>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     <?php } else {?>
                                     <li><a href="#pills-tab1" onclick="window.location='<?php echo base_url();?>teacherself/value_feature/'" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <?php }?>
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-red progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  
                                    
                                     
                                     
                                     
                                      <!-- BEGIN STEP 2-->
                                      <?php if($this->session->userdata('login_type') == 'observer'){?>
                                      <div class="tab-pane" id="pills-tab3">
                                      <?php } else {?>
                                      <div class="tab-pane" id="pills-tab2">
                                      <?php }?>
                                     <div class="tab-pane" id="pills-tab2">
                                     
                                     	<?php if($this->session->userdata('login_type') == 'teacher'){?>
											<h3 style="color:#000000;">STEP 2</h3>
                                           <?php }else{ ?>
                                           	<h3 style="color:#000000;">STEP 3</h3>
                                            <?php }?>

<!--                                         <div class="control-group">
                             
                                     <h3 style="color:#000000">Please use the input below to type in student's grade <B>or</B> choose 'select file' to upload    </h4><BR>
                           <div class="control-group">
                                             <label class="control-label">Enter Grade</label>
                                             <div class="controls">
                                                 <input type="text" class="span6" />
                                                 

                                             </div>
                                         </div>
                                     </div>   -->
                                     
<!--                                     <div class="control-group">
                                    <label class="control-label">Upload Grade</label>
                                    <div class="controls">
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default">
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                     </div>-->
                                         
                                 
                                <div class="control-group">
                             
      
                                 
                                          <div class="content" style="margin-left:0px;max-width:1099px;">
		
		
		
		<?php 
		//print_r($dates);
		
		foreach($dates as $val)
		
		{
		$d=explode('-',$val['date']);
		$ds=$d[0].'/'.$d['1'].'/'.$d['2'];
		$k=0;
		?>

	<table style="width:100%;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" class="table table-striped table-hover table-bordered" id="editable-sample" cellspacing="0">

  <tr>
    <td colspan="<?php echo count($lessonplansdup)+3?>" align="center" bgcolor="#657455" style="color:#888888"><b> Lesson Plan Book &nbsp;&nbsp;&nbsp; Date:&nbsp<?php echo $ds;?> &nbsp;&nbsp;&nbsp;Day:&nbsp;<?php echo $val['week'];?></b></td>
  </tr>
  <tr align="center" style="color:#333;">
    <td width="16%" bgcolor="#CCCCCC">Time</td>
	<td width="16%" bgcolor="#CCCCCC">Subject</td>
    <?php 
	$j=3;
	if($lessonplansdup!=false){
	 foreach($lessonplansdup as $plan)
	 {
	 $j++;
	 ?>
	<td width="16%" bgcolor="#CCCCCC"><?php echo $plan['tab'];?></td>
    <?php } ?>
	<td width="16%" bgcolor="#CCCCCC">Details</td>
	
	
	
	<?php } ?>
  </tr>
  
    <tr align="center"  >
	<td colspan="<?php echo $j;?>">
	<table width="100%" cellpadding="0"  id="<?php echo $val['date'];?>" >
    <?php if($getlessonplans!=false)
	{
	
	$le=0;
	  $je=0;
	 // echo "<pre>";
	  //print_r($getlessonplans);exit;
	  $tdcntr = 0;
	  foreach($getlessonplans as $getplan)
	  {
              $datevar = explode('-', $val['date']);
            $pdfdate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];
      if( $val['date']==$getplan['date'])
	{	
          
	  $je++;
	  if($le!=$getplan['lesson_week_plan_id'])
		{	
		$les=0;
	     if($je==1)
		 { 
		    ?>
			<tr align="center">
		 
		 <?php }
		 else
		 {
		 if($val['date']>=date('m-d-Y'))
		 {
		 ?>
		  
<!--		 onClick="getDetailsview('<?php echo $le;?>');"-->
	<td>NA</td>
		  
		 <td width='16%'>
<a class='standard' target="_blank" href='<?php echo base_url();?>planningmanager/createlessonplanpdf/<?php echo $teacher[0]['teacher_id'];?>/<?php echo $getlessonplans['0']['subject_id'];?>/<?php echo $pdfdate;?>/' title='Other' role="button" data-toggle="modal" >View</a>
                 </td> </tr><tr align='center'>
		 
		 <?php
		 }
		 else
		 {?>
		 
		 
		 <td>NA</td>
		<td width='16%'><a class='standard' target="_blank" href='<?php echo base_url();?>planningmanager/createlessonplanpdf/<?php echo $teacher[0]['teacher_id'];?>/<?php echo $getlessonplans['0']['subject_id'];?>/<?php echo $pdfdate;?>/' title='Other' role="button" data-toggle="modal" >View</a></td> </tr><tr align="center">
		 
		 <?php }
		 }
		 ?>
		
		<td width="16%"><?php 
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		 
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		 if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		echo $start2." to ".$end2 ?></td>
		<td width="16%"><?php echo $getplan['subject_name']?></td>
		<td width="16%"><?php echo $getplan['subtab']?></td>
		<?php $tdcntr = $tdcntr +3;?>
		<?php 
		}
		else
		{
		$les++;
		?>
		  
		  <td width="16%"><?php echo $getplan['subtab']?></td>
			<?php $tdcntr = $tdcntr+1;?>
		<?php
		}
		
		$le=$getplan['lesson_week_plan_id'];
		$lecomments[$le]=$getplan['comments'];
	  
	  }
	  
	  }
	  if($je==0)
	  {
	  ?>
	    <tr align="center">
		<td colspan="<?php echo $j;?>">No Plans Found </td>
		
	<?php } if($le!=0) {
	/*if(($les+1)>=count($lessonplansdup))
	{
	
	
	}
	else
	{
	 ?>
	 <td width="<?php echo (count($lessonplansdup)-$les-1)*100;?>px" ></td>
	<?php } */
	if($val['date']>=date('m-d-Y'))
		 {
	?>	
	
	
	<?php } else { ?>
	
	
	
	<!--data-toggle="modal" onClick="getDetailsview('<?php echo $le;?>');"-->
	<?php } } ?> <td width='16%'><a class='standard' target="_blank" href='<?php echo base_url();?>planningmanager/createlessonplanpdf/<?php echo $teacher[0]['teacher_id'];?>/<?php echo $getlessonplans['0']['subject_id'];?>/<?php echo $pdfdate;?>/' title='Other' role="button" >View</a></td></tr></table>
	
	


	<?php } else { ?>
	<tr><td colspan="<?php echo $j;?>">No Plans Found  </td></tr></table>
    
    </td></tr>
	<?php } ?>
	<?php
	if($val['date']>=date('m-d-Y'))
		 {
		 ?>
	
  <?php } ?>
	</table>
	</td>
  </tr>
 
  
  
</table>
<br />
		<?php } ?>
		
	<div style="margin:10px;">	
	<table id="list1"></table>
	<!--<div><input type="button" name="sendemail" id="sendemail" value="Send Email" onclick="sendemail()"><img src="<?php echo SITEURLM?>images/ajax-loader.gif" style="display:none;" id="loading"></div>-->
	</div>
			
	 </div>
                                 
                                 
                                 
                                 <div class="space20"></div><div class="space20"></div>
                                 
                                           
                                           <div class="span12">
                      
             <center>
              <!--   <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Review Grade</button> 
                 <button class="btn btn-large btn-red"><i class="icon-edit icon-white"></i> Modify Grade</button> -->
                 <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Next</button> 
                 <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Send</button> 
                 <button class="btn btn-large btn-red"><i class="icon-save icon-white"></i> Save and Exit</button>
		</center>
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first red"><a href="javascript:;" onclick="window.location='<?php echo base_url();?>teacherself/value_feature/'">First</a></li>
                                         <li class="previous red"><a href="javascript:;"  onclick="window.location='<?php echo base_url();?>teacherself/value_feature/'">Previous</a></li>
                                         <li class="next last red"><a href="javascript:;">Last</a></li>
                                         <li class="next red"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
 <div id="myModal-view" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">View Details</h3>
                                </div>
                                <div class="modal-body" id="viewdetails">
                                     
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Ok</button>
                                    
                                </div>
                            </div>
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   
   <script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM.$view_path; ?>js/value_feature.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){
var valuestr = '<?=$valuestr;?>';
var classworkstr = '<?=$classworkstr;?>';

$.fn.getCheckboxVal = function(){
    var vals = [];
    var i = 0;
    this.each(function(){
        vals[i++] = $(this).val();
    });
    return vals;
}

$("#list1").jqGrid({
   	url:'<?=base_url().'teacherself/gridServerPart'?>',	//Backend controller function generating grid XML
	editurl: '<?=base_url().'teacherself/edit_test_save'?>',	//Backend controller function save inline updates
	mtype : "post",		//Ajax request type. It also could be GET
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['Student','Actions',<?=$times?>],	//Grid column headings
   	colModel:[
   		{name:'firstname',index:'firstname', width:120,height:50, align:"left", editable:false, editrules: { edithidden: true }},{name:'act',index:'act', width:110, align:"center", editable:false, search:false},
		<?php foreach($timesvalue as $key=>$value)
		  	{	
		   $cla=explode('_',$value);
		  	?>
		{name:'value_<?php echo $key;?>',index:'time_<?php echo $key;?>', width:125, align:"left", editable:true, edittype:'select',editoptions: {
                 <?php if(isset($cla[1])&& $cla[1]=='classwork')
			{
			?>
				 value: classworkstr,
			<?php 
			
			}
			else
			{
			?>
			
				 value: valuestr,
			<?php } ?>	
			dataEvents: [
                    { type: 'change', fn: function (e) { 
					var rowid = $("#list1").jqGrid('getGridParam','selrow');
					if(this.value=='')
					{
					  var va=0;
					}
					else
					{
					 var va=this.value;
					}
					$.get('teacherself/valueblsave/'+va+'/'+<?php echo $key;?>+'/'+rowid
                            ,function(data){ 
                                /*var res = $(data).html(); 
								var rowid = $("#list1").jqGrid('getGridParam','selrow');
									
							   $("#"+rowid+"_value_<?php echo $key;?>_<?php echo $key;?>").html(res); */
							   $("#list1").trigger("reloadGrid");
							   
							   });
					} }
                ]
            } }, <?php } ?>
		<?=$cols?>   		
   		
  	],
   	rowNum:100,
   	pager: '#pager1',
	width:1100,
        autowidth:true,
        shrinkToFit: false,
        hidegrid: false,
	height:'auto',
	toolbar: false,
    viewrecords: true,
	gridComplete: function(){
		var ids = $("#list1").getDataIDs();
		for(var i=0;i < ids.length;i++){
			var cl = ids[i];
			//se = "<input type='button' value='Save' onclick=\"$('#list1').saveRow('"+cl+"',checksave);\"  />";		//checksave is a callback function to show the response of inline save controller function
			se='';
			ce = "<input type='button' value='Cancel' title='Cancel' onclick=\"$('#list1').restoreRow('"+cl+"');\" />";
			$("#list1").setRowData(ids[i],{act:se+ce});	//Save and Cancel buttons inserted via jqGrid setRowData function
		}
		if($("#list1").parents('div.ui-jqgrid-bdiv').height()>650)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","650px");
	}
	},
	onSelectRow: function(id){
		//if(id && id!==lastsel2)
		{
			$('#list1').restoreRow(lastsel2);	//restore last grid row
			$('#list1').editRow(id,true);		//show form elements for the row selected to enable updates
			lastsel2=id;	//save current row ID so that when focus is gone it can be restored
		}
	},loadComplete: function() {
    $("tr.jqgrow:odd").css("background", "#FFFFFF");
}
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});

//$("#list1").jqGrid('setGroupHeaders', { 
//useColSpanStyle: false, 
//groupHeaders:[ <?php //$headers;?> ] });

//$("#list1").jqGrid("setColProp","firstname",{frozen:true});
//$("#list1").jqGrid("setColProp","act",{frozen:true});
//$("#list1").jqGrid("setFrozenColumns");

});



</script> 
   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
<?php if ($this->session->userdata('login_type') == 'observer') {?>
    $("#pills").bootstrapWizard("show",2);
<?php } else {?>
    $("#pills").bootstrapWizard("show",1);
<?php }?>
function getDetailsview(le){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>teacherself/getother/"+le
		})
		.done(function( msg ) {
			$('#viewdetails').html(msg);

		});										
	}
   </script>  
   
   
   
   
   
</body>
<!-- END BODY -->
</html>

