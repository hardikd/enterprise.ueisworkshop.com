<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
    <!--start old script -->
   
  <script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/reportnext.js" type="text/javascript"></script>
    <!--end old script -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); 
	require_once($view_path.'inc/libchart/classes/libchart.php');
	?>
   
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                      <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools/observation">Lesson Observations</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>teacherreport/observer">Observation by Viewer</a>
                           
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                     <div class="mbody">
    	
        <?php if(!empty($groups)) { 
		
		$countgroups=count($groups);
		}
		else
		{
		  $countgroups=0;
		}
		
		?>
		<div class="content" <?php if($countgroups>=13) { ?> style="min-height:<?php echo $countgroups*70;?>px;height:auto;" <?php  } else { ?> style="min-height:800px;" <?php } ?> >
        	
			<table width="100%" border="0">
  <tr>
    <td class="htitle">			<?php echo $group_name;?>
			<?php if(isset($sub_scale_name) && !empty($sub_scale_name)) { ?> --->  <?php echo $sub_scale_name; } ?></td>
  </tr>
  <tr>
    <td style="font-size:14px;">Sectional Data  <a style="color: rgb(255,255,255);" href="pdfreport/getpdf" target="_blank" ><img src="<?php echo SITEURLM?>images/pdf_icon.gif" /></a></td>
  </tr>
  <tr>
    <td><div class="desc"><?php echo ucfirst($this->session->userdata('report_criteria'));?>     Name:&nbsp;&nbsp;<?php echo $this->session->userdata('report_name');?></div></td>
  </tr>
  <tr>
  <td>
  		
			<table width="100%">
			<tr>
			<td>
			<div style="float:right; "><input type="button" id="previous" name="previous" value="<<" style="display:none">
			<input type="button" id="next" name="next" value=">>" style="display:none"></div>
			</td>
			</tr>
			</table>
			
			
  </td>
  </tr>
  <tr>
    <td>
	 <div class="reportss">
    <table  class="questab">
  <tr>
    <td  class="hlft">Report Number</td>
    <td class="rc1">
    
    <?php 
	$rcc=1;
	if($reports!=false) {
		$rc=0;
$showreport=1;		
			foreach($reports as $reportval)
			{
			$rc++;
			?>
			<div class="hbox">
			<?php echo $showreport;?>
			</div>
			<?php 
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rc<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			$showreport++;
			 } } else { ?>
			
			<div>
			No Reports Found
			</div>
			<?php } ?>
			<input type="hidden" id="rc" name="rc" value="<?php echo $rcc;?>">
    </td>
  </tr>
  <tr>
    <td  class="hlft">Strengths:</td>
    <td class="rcf1"><?php 
			$count=0;
			$str=0;
			if($reports!=false && $sectional!=false) {
			$rc=0;
			$rcc=1;			
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && !empty($secval['strengths']) )
			{
			 $str++;
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			
			<?php }
			else
			{?>
			<div class="hbox">&nbsp;</div>
			
			<?php }
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} } else { ?>
			
			<?php } ?></td>
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
  </tr>
  <tr>
    <td class="hlft">Concerns:</td>
    <td class="rcf1"><?php 
			$count=0;
			$con=0;
			if($reports!=false && $sectional!=false) { 
			$rc=0;
			$rcc=1;	
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && !empty($secval['concerns']) )
			{
			 $con++;
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox">X</div>
			
			<?php }
			else
			{?>
			<div class="hbox">&nbsp;</div>
			
			<?php }
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} } else { ?>
			
			<?php } ?>
            
            </td>
			
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
  </tr>
  <tr>
  <td class="hlft">Score:</td>
    <td class="rcf1"><?php 
			$count=0;
			$scr=0;
			if($reports!=false && $sectional!=false) {
			$rc=0;
			$rcc=1;				
			foreach($reports as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($sectional as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && !empty($secval['score']) )
			{
			 $scr++;
			 $count++;
			 $check=1;
			 $score=$secval['score'];
			 } 
			
			
			} 
			if($check==1)
			{?>
			
			<div class="hbox"><?php echo $score;?> </div>
			
			<?php }
			else
			{?>
			<div class="hbox">&nbsp;</div>
			
			<?php }
			if($rc==18)
			{
			$rcc++;
			$rc=0;
			?>
			</td>
			<td class="rcf<?php echo $rcc;?>" style="display:none">
			<?php
			
			}
			
			} } else { ?>
			
			<?php } ?>
            
            </td>
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs"><?php echo $count;?> of <?php echo $countreport;?> observations</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
<div class="barchartss" >
	<?php
	if($reports!=false)
  {
  ?>
	<tr>
  <?php 
  
  $login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('school_id');

			}
  $chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	$dataSet->addPoint(new Point("Scaled Level 1", $str));
	$dataSet->addPoint(new Point("Scaled Level 2", $con));
	$dataSet->addPoint(new Point("Scaled Level 3", $scr));
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$reports[0]['report_id'].".png");
	?>
	<td>
	<?php echo  '<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$reports[0]['report_id'].'.png?dummy='.$count.'" style="border: 1px solid gray;"/>' ?>
	<img alt="Pie chart"  src="<?php echo WORKSHOP_DISPLAY_FILES;?>generated/<?php echo $login_type.'_'.$login_id.'_'.$reports[0]['report_id'];?>.png" style="border: 1px solid gray;"/>
	</td>
	
  </tr>
<?php } ?>

	</div>
	<style>
	.reportss{
	margin-top:290px;
	
	}
	.barchartss{
	 margin-top:-600px;
	}
	</style>
    

    </td>
  </tr>
</table>

			
			
        </div>
    </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                      
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  

    <script>

$("#fullreport").on('click', function() {
   document.getElementById('fullreportDiv').style.display = "block";
   document.getElementById('sectionalreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#sectionalreport").on('click', function() {
   document.getElementById('sectionalreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#qreport").on('click', function() {
   document.getElementById('qreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('sectionalreportDiv').style.display = "none";
});




</script>


   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>