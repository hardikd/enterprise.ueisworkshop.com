<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::GoalPlan::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo SITEURLM?>css/video.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/goalmedia.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {

	
	
	
	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
        
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
function btnsmall(id)
	{
	 var $msgContainer = $("div#comments");
	
	if($('#comment'+id).val()!='')
	{
	   pdata={};
	   pdata.id=id;
	  
	   pdata.comments=$('#comment'+id).val();
	   $.post('comments/addcomment',{'pdata':pdata},function(data) {
		  
		  
		  if(data.status==1)
		  {
            $.get("comments/getcomments/"+id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
			$('#comment'+id).val('');

});

		 }
		 else
		 {
		 
			 alert('Failed Please Try Again ');
		 
		 }

},'json');		 
	
	
	
	}
	
	}
function getthis(id)
{  
var year=$('#year').val();
$('.goalsubmit').replaceWith('<input type="submit" id="goalsubmit" name="goalsubmit" class="goalsubmit"  value="submit">');
var teacher_id=false;
$.get("comments/getgoalcomments/"+id+"/"+teacher_id+"/"+year+"?num=" + Math.random(), function(msg){
var $msgContainer = $("div#comments");
  $msgContainer.html(msg);
			

});
}
function addgoal(id)
{
  
  
  if($('#text').val('')=='')
  {
   alert('please Enter Goal');
   return;
  
  }
  
  $('#goalsubmit').replaceWith('<input type="submit" id="goalsubmit'+id+'" name="goalsubmit'+id+'" class="goalsubmit" value="submit">');
  $("#dialog").dialog({
			modal: true,
           	height: 250,
			width: 700
			});
 
  $('#goalsubmit'+id).click(function() {
    
	
	if($('#text').val()=='')
	{
	
	 alert('Please Enter Goal');
	 return;
	
	}
	var text=$('#text').val();
	var year=$('#year').val();
	var schedule_week_plan=$('#schedule_week_plan').val();
     pdata={};
	 pdata.id=id;
	 pdata.text=text;
	 pdata.year=year;
	 pdata.schedule_week_plan=schedule_week_plan;
	   
	$.post("comments/addgoal/",{'pdata':pdata}, function(msg){
	if(msg!==false)
	{
	  $('#goalsubmit'+id).replaceWith('<input type="submit" id="goalsubmit" name="goalsubmit" class="goalsubmit"  value="submit">');
	  $('#dialog').dialog('close');
	  var $msgContainer1 = $("div#tab"+id);
	  $msgContainer1.html(msg);
	
	}
	else
	{
	
	 alert( 'Failed Please Try Again');
	}
 
 });
  });

}
</script>

<style type="text/css">
.small_face
{
width:35px;height:35px
}
.stcommentimg
{
float:left;
height:35px;
width:35px;
border:solid 1px #dedede;
padding:2px;
}
.stcommenttext
{
margin-left:45px;
min-height:40px;
word-wrap:break-word;
overflow:hidden;
padding:2px;
display:block;
font-size:13px;
width:550px;

}
ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 100%; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 100%;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 31px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 31px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;

	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: 1.2em;
	padding: 0 20px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
.tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	margin-left:0px;
	float: left;
	background: #fff;
	
}
.tab_content {
	padding: 20px;
	font-size: 1.2em;
	width:610px;
}
</style>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
		<form action="teacherreport/goalplan" method="post"> 
		<table>
		<tr>
		<td>
		<b>School Name:</b>
			<?php echo $this->session->userdata("school_self_name");?>
		</td>
		</tr>
		<tr>
		<td class="htitle">
		Goal Plan
		</td>
		</tr>
		<tr>
		<td>
		<?php if($year) { $selectyear=$year;} else { $selectyear=date('Y');} ?>
		Year: <select class="combobox1" name="year" id="year">
		<?php for($i=2010;$i<=2045;$i++)
		{
		?>
		<option value="<?php echo $i;?>" <?php if($i==$selectyear) { ?> selected <?php } ?>><?php echo $i;?>-<?php echo $i+1;?></option>
		<?php } ?>
		</select>
		<td>
		<input type="submit" name="yearsubmit" id="yearsubmit" value="Go">
		</td>
		</td>
		</tr>
		</table>
		</form>
		<table width="100%" cellpadding=0 cellspacing=0 style="margin-left:10px;">
		<tr><td>
		<?php if($goalplans!=false) { ?>
	<ul class="tabs">
    <?php 
	$i=0;
	foreach($goalplans as $val) {
	$i++;
	?>
	<li><a href="#tab<?php echo $val['goal_plan_id'];?>" onclick="getthis(<?php echo $val['goal_plan_id'];?>)"><?php echo $val['tab'];?></a></li>
    
	<?php } ?>
	</ul>
<?php } else { ?>
No Goals Found
<?php } ?>
</td>
</tr>

<?php if($goalplans!=false) { ?>
<tr style="border: 1px solid #999;
	border-top: none;
	border-bottom: none;
	overflow: hidden;
	margin-left:0px;
	float: left;
	background: #fff;">
<td>
    
	<input type="hidden" name="schedule_week_plan" id="schedule_week_plan" value="<?php echo $schedule_week_plan_id;?>">
	<?php 
	//$j=0;
	foreach($goalplans as $val) {
	//$j++;
	if($teacherplans!=false) {
	$k=0;
	foreach($teacherplans as $plan)
	{
	
	if($val['goal_plan_id']==$plan['goal_plan_id'])
	{
	$k=1;
	?>
	<div id="tab<?php echo $val['goal_plan_id'];?>" class="tab_content" >
        
		<?php if($year>=date('Y')) { ?>		
		<div style="font-size:11px;color:#666666;">
		Add Comments:<textarea style="height: 57px;width:600px;" name="comments" id="comment<?php echo $plan['teacher_plan_id']?>"></textarea>
		<input type="submit" class="btnsmall" onclick="btnsmall(<?php echo $plan['teacher_plan_id']?>)" id="<?php echo $plan['teacher_plan_id']?>" name="submit" value="submit">
		</div>
		<?php } ?>
		
		<div id="description">District Task:<?php echo $val['description'];?></div>
		<div id="observer_description">School Level Task:<?php 
		//if($observer_desc!=false) { foreach($observer_desc as $descval) { if($descval['goal_plan_id']==$val['goal_plan_id']) { echo $descval['text'] ;} }  } 
		 echo $val['school_description'];
		?></div>
		<div style="color:#02AAD2;" id="disgoal">
		Goal:<?php echo $plan['text'];?>(<?php echo $plan['added'];?>)
		</div>
		
		<div style="color:#02AAD2;" id="discomments">
		Comments:
		</div>
		<?php 
		if($comments!=false) {
		if($plan['teacher_plan_id']==$comments[0]['teacher_plan_id']) {?>
		<div id="comments">
		<?php
		foreach($comments as $comm) {
		?>
		<div style="padding:4px 0px 4px 0px;border-bottom:1px #cccccc solid;">
		<div class="stcommentimg">
<img src="<?php echo SITEURLM;?>images/<?php echo $comm['avatar'];?>.gif" class='small_face'/>
</div> 
		<div class="stcommenttext">
		<b><?php echo $comm['role']?>:</b> <?php echo $comm['comments']?>
		<b>(<?php echo $comm['created']?>)</b>
		</div>
		</div>
		
		<?php  }?>
		</div>
		<?php } else { ?>  
		<div id="comments"></div>
		<?php }  } else { ?>
		<div id="comments">No Comments  Found</div>
		<?php } ?>
		

		
    </div>
	<?php } } if($k==0) {?>
	<div id="tab<?php echo $val['goal_plan_id'];?>" class="tab_content">
       <div id="description">District Task:<?php echo $val['description'];?></div>
	   <div id="observer_description">School Level Task:<?php 
	  // if($observer_desc!=false) { foreach($observer_desc as $descval) { if($descval['goal_plan_id']==$val['goal_plan_id']) { echo $descval['text'] ;} }  } 
	   echo $val['school_description'];
	   ?></div>
	   <div id="nogoal"> No Goal Set <?php if($year==date('Y') && $schedule_week_plan_id!=0 ) { ?><input type="button" onclick="addgoal(<?php echo $val['goal_plan_id'];?>)" name="add" value="Add"> <?php } ?></div>
	    
    </div>
	<?php } } else {?> 
	<div id="tab<?php echo $val['goal_plan_id'];?>" class="tab_content">
         <div id="description">District Task:<?php echo $val['description'];?></div>
		 <div id="observer_description">School Level Task:<?php 
		// if($observer_desc!=false) { foreach($observer_desc as $descval) { if($descval['goal_plan_id']==$val['goal_plan_id']) { echo $descval['text'] ;} }  } 
		 echo $val['school_description'];
		 ?></div>
		 <div id="nogoal"> No Goal Set </div>  <?php if($year==date('Y') && $schedule_week_plan_id!=0 ) { ?> <input type="button" onclick="addgoal(<?php echo $val['goal_plan_id'];?>)" name="add" value="Add"> <?php } ?>
    </div>
	
	<?php } } ?>
    
	</td>
	</tr>
	<tr style="border: 1px solid #999;
	border-top: none;	
	overflow: hidden;
	margin-left:0px;
	float: left;
	background: #fff;width:650px;">
	<td>
	<form id="mediaForm" name="mediaForm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="MEDIA_MAX_FILE_SIZE" value="2097152" />
		<div >
			
			<input type="hidden" id="media_school_id" name="media_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
			
			<span style="padding-left:20px;">Add File: &nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="mediafileToUpload" id="mediafileToUpload" size="38" />
		    
			 <br />
			<span style="padding-top:20px;padding-left:20px;text-align:top;">Add Comment:</span>&nbsp; <textarea name="descriptionmedia" id="descriptionmedia" style="height: 37px;width: 433px"  ></textarea>
			
			<input type="Submit"  class="btnsmall" value="Submit"  />
			
		</div>
            
	   

	<p id="mediamessage"></p>
    
	
	 
	
 	</form>
	</td>
	</tr>
	<?php }?>
	
	</table>

		<!-- Media Start -->
		
		
	<div id='mediaflashmessage'>
	<div id="mediaflash" align="left"  ></div>
	</div> 	
	<!-- Media End -->
	<table>
	<tr>
	<td align="center">
	<div id="mediadetails" style="display:none;">
		<input type="hidden" id="mediapageid" value="">
		<input type="hidden" id="media_school_id" name="media_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
		<div id="mediamsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
		
		<!-- Media End -->
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Add Goal" style="display:none;">
<table cellpadding="0" cellspacing="0" border=0 class="jqform1">
<tr>
				<td valign="top" >
				<font color="red">*</font>Goal Paln:
				</td>
				<td  valign="top"  style="height:40px;">
				<textarea name="text" id="text" style="width:400px;" ></textarea>
				</td>
				
			</tr>
			<tr>
			<td align="center" colspan="2"><input type="submit" id="goalsubmit" name="goalsubmit" class="goalsubmit"  value="submit"></td>
			</tr>
</table> 
</div>
</body>
</html>