<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<!--start old script -->
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
if($('#form').val()=='formp'){ 
$('.formp').hide();
 
  }
  else
  {
    $('.formp').show();
  }
});
function check()
{

if(document.getElementById('teacher_id').value=='')
{
 alert('Please Select Teacher');
 return false;

}
else
{
	return true;

}
}
function changeform(val)
{
  if(val=='formp')
  {
     $('.formp').hide();
  
  
  }
  else
  {
    $('.formp').show();
  }


}
</script>
<!--end old script -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
      <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-pencil"></i>&nbsp; Implementation Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>implementation">Implementation Mangager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>implementation/self_reflection">Self-Reflection</a>
                            <span class="divider">></span>  
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>teacherself">Generate</a>
                          
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Generate Self-Reflection</h4>
                          
                         </div>
                         <div class="widget-body">
					<form class="form-horizontal"  action='teacherreport/getteacherreport' name="report" method="post" onsubmit="return check()">
                            <div class="space20"></div>   
                                 <h3>Teacher Name: <?php echo $this->session->userdata('teacher_name');?></h3>
                             <input type="hidden" name="teacher_id" value="<?php echo $this->session->userdata('teacher_id');?>" >   
                                 
                                  
                                  <div class="control-group">
                                  <label class="control-label"> Observation Tool</label>
                                             <div class="controls">
				<select class="span12 chzn-select" name="form" tabindex="1" style="width: 300px;" id="form" onchange="changeform(this.value)">
               <option value="forma" <?php if($this->session->userdata('reportform')=='forma') {?> selected <?php } ?> >Checklist</option>
			  <option value="formb" <?php if($this->session->userdata('reportform')=='formb') {?> selected <?php } ?> >Scaled Rubric</option>
			  <option value="formp" <?php if($this->session->userdata('reportform')=='formp') {?> selected <?php } ?> >Proficiency Rubric</option>
			  <option value="formc" <?php if($this->session->userdata('reportform')=='formc') {?> selected <?php } ?> >Likert</option>			  
			  </select>           </div>
                                         </div> 
                                 
                                  <div class="control-group">
                                  <label class="control-label">Select Evaluation Type</label>
                                             <div class="controls">
  
                         <select class="span12 chzn-select" name="formtype" id="formtype" tabindex="1" style="width: 300px;" >
				<option value="all"  >-All-</option>			 
			 <option value="Formal" <?php if($this->session->userdata('formtype')=='Formal') {?> selected <?php } ?> >Formal</option>
			  <option value="Informal" <?php if($this->session->userdata('formtype')=='Informal') {?> selected <?php } ?> >Informal</option>
			  			  </select>
                                             </div>
                                         </div> 
                                         
                                          <div class="control-group">
                                  <label class="control-label">Subject</label>
                                             <div class="controls">
                                         
                                <select tabindex="1" style="width: 300px;" class="span12 chzn-select" name="subject" id="subject">
			   							 <option value="all">-All-</option>
			  						  <?php if(!empty($subjects)) { 
											foreach($subjects as $val)
											{
											?>
			 						   <option value="<?php echo $val['subject_id'];?>" 
										<?php if($this->session->userdata('report_query_subject')==$val['subject_id']) {?> selected <?php } ?>				
										><?php echo $val['subject_name'];?>  </option>
									    <?php } } ?>
		  			  				  </select>
                                             </div>
                                         </div> 
                                 
                                 
                                 
                                         
                                          <div class="control-group">
                                  <label class="control-label">Month:</label>
                                             <div class="controls">
                                      <select class="span12 chzn-select" name="month" tabindex="1" style="width: 300px;" id="month" >
                                      <option value="all">-All-</option>
                                      <option value="1" <?php if($this->session->userdata('reportmonth')==1) {?> selected <?php } ?> >January</option>
                                      <option value="2" <?php if($this->session->userdata('reportmonth')==2) {?> selected <?php } ?> >February</option>
                                      <option value="3" <?php if($this->session->userdata('reportmonth')==3) {?> selected <?php } ?> >March</option>
                                      <option value="4" <?php if($this->session->userdata('reportmonth')==4) {?> selected <?php } ?> >April</option>
                                      <option value="5" <?php if($this->session->userdata('reportmonth')==5) {?> selected <?php } ?> >May</option>
                                      <option value="6" <?php if($this->session->userdata('reportmonth')==6) {?> selected <?php } ?> >June</option>
                                      <option value="7" <?php if($this->session->userdata('reportmonth')==7) {?> selected <?php } ?> >July</option>
                                      <option value="8" <?php if($this->session->userdata('reportmonth')==8) {?> selected <?php } ?> >August</option>
                                      <option value="9" <?php if($this->session->userdata('reportmonth')==9) {?> selected <?php } ?> >September</option>
                                      <option value="10" <?php if($this->session->userdata('reportmonth')==10) {?> selected <?php } ?> >October</option>
 									<option value="11" <?php if($this->session->userdata('reportmonth')==11) {?> selected <?php } ?> >November</option>
                                     <option value="12" <?php if($this->session->userdata('reportmonth')==12) {?> selected <?php } ?> >December</option>
                                      </select>	
                                             </div>
                                         </div> 
                                         
                                         
                                        <div class="control-group">
                                  <label class="control-label">Year:</label>
                                             <div class="controls">
                                        <select class="span12 chzn-select" name="year" id="year" tabindex="1" style="width: 300px;" >
			 							 <option value="all">-All-</option>
										  <?php for ($i=date('Y');$i>=1982;$i--){
											echo "<option value=".$i;
											if($this->session->userdata('reportyear')==$i){ echo " selected ";  } 
											echo " >".$i."</option>";    
										} ?>
			  					  </select>
                                             </div>
                                         </div>  
                                         
                                         	
                                
                                      <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
<input title="Get Reports" class="btn btn-small btn-red" type="submit" name="submit" value="Retrieve Full Report" id="fullreport" style=" padding: 5px; " />
            
                        							</div>   
                                               </div>

					      <div class="control-group">
                                   <label class="control-label">Sectional Data Report</label>
                                  <div class="controls">
<input title="Get Sectional Report" class="btn btn-small btn-red" type="submit" name="submit" id="sectionalreport" style=" padding: 5px; " value="Retrieve Sectional Report" />
                                             </div>
                                         </div> 
                                         <div class="formp">
                                          <div class="control-group">
                                   <label class="control-label">Qualitative Data Report</label>
                                  <div class="controls">
                                
                                <table><tr>
                                <td style="padding-right:10px;">
                                <p style="margin-bottom: 5px;">From</p>                               
                                <select class="span12 chzn-select" tabindex="1" style="width: 150px;" name="from" >
								<?php for ($i=date('Y');$i>=1982;$i--){
								echo "<option value=".$i.">".$i."</option>n";    
								} ?> 
								</select>    
                                    </td>
                                    
                                    <td>
                                    
                                     <p style="margin-bottom: 5px;">To</p>                               
                                       <select class="span12 chzn-select" tabindex="1" style="width: 150px;" name="to" >
										<?php for ($i=date('Y');$i>=1982;$i--){
										echo "<option value=".$i.">".$i."</option>n";    
										} ?> 
										</select>
			
                                            
                                     </td>
                                     </tr>
                                     </table>
                                         
                                             </div>
                                         </div>
                                        
                                         <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 

<input title="Get Report" class="btn btn-small btn-red" type="submit" name="submit" id="qreport" value="Retrieve Qualitative Data Report" style=" padding: 5px; ">

                                              </div>   
                                               </div> 
                                         </div>   </form>    
                                             
                                             
                                                 <div class="space20"></div>
                                
                                <div id="fullreportDiv"  style="display:block;" class="answer_list" >
                                   <div class="widget red">
                         <div class="widget-title">
                             <h4>Observer Full Report</h4>
                          </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">
                                  
                                  	<?php if(isset($reports) && !empty($reports)) { ?>
			<table class="tabcontent" cellspacing="0">
			<tr class="tchead">
			<th>
			Report Id
			</th>
			<th>
			Date
			</th>
			<th>
			Teacher
			</th>
			<th>
			Subject
			</th>
			<th>
			Grade
			</th>
			<th>
			Observer
			</th>
			</tr>
			<?php foreach($reports as $reportval) { ?>
			<tr>
			<td width="60" valign="top" class="tdlink">
			<?php if($this->session->userdata('reportform')!='formb') { 
			if($this->session->userdata('reportform')=='forma' || $this->session->userdata('reportform')=='formc')
			{
			?>
			<a href="comments/viewreportpdf/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			
			<?php
			}
			if($this->session->userdata('reportform')=='formp')
			{
			?>
			<a href="comments/viewproficiencypdf/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			<a href="teacherreport/viewproficiency/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><div style="margin:5px 0px 0px 5px;float:left;"><?php echo $sno;?></div></a>
			<?php } else {?>
			
			<a href="teacherreport/viewreport/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><div style="margin:5px 0px 0px 5px;float:left;"><?php echo $sno;?></div></a>
			<?php
			} } else { ?>
			<a href="comments/viewreportformpdf/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img style="float:left;margin-left:10px;margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			<a href="teacherreport/viewreportform/<?php echo $reportval['report_id'];?>/<?php echo $sno;?>"><?php echo $sno;?></a>
			<?php } ?>
			</td>
			<td valign="middle">
			<?php echo $reportval['report_date'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['teacher_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['subject_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['grade_name'];?>
			</td>
			<td valign="middle">
			<?php echo $reportval['observer_name'];?>
			</td>
			
			</tr>
			<?php 
			$sno++;
			} ?>
			</table>
			<table><tr><td valign="middle"><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td></tr></table>
			
			<table width="100%" ><tr><td><div style="float:right;"><a href="http://get.adobe.com/reader/" target="_blank" style="color:#ffffff;text-decoration:none;"><img src="<?php echo SITEURLM?>images/get_adobe_reader.png"></a></div></td></tr></table>
			
			<?php } else if(isset($reports)) { ?>
			<table align="center"><tr><td><b>No Reports Found</b></td></tr></table>
			<?php } ?>
                                  
                                  </h3>
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-red"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                                
                                
                               
                                
                                <div id="sectionalreportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget red">
                         <div class="widget-title">
                             <h4>Sectional Full Report</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">Report Appears Here</h3>
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-red"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                                
                                
                                   <div id="qreportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget red">
                         <div class="widget-title">
                             <h4>Qualitative Data Report</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">Report Appears Here</h3>
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-red"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-red"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                            
                                    </div>
                                      </div>
                                        
                                     
                                     
                                    
                                     
                                    
                                    
                                 </div>
                             </div>
                           
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  

 <script>

$("#fullreport").on('click', function() {
   document.getElementById('fullreportDiv').style.display = "block";
   document.getElementById('sectionalreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#sectionalreport").on('click', function() {
   document.getElementById('sectionalreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('qreportDiv').style.display = "none";
});

$("#qreport").on('click', function() {
   document.getElementById('qreportDiv').style.display = "block";
   document.getElementById('fullreportDiv').style.display = "none";
   document.getElementById('sectionalreportDiv').style.display = "none";
});




</script>

   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>