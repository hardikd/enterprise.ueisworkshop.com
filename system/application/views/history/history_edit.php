﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>district_id</td>
     <td>History Name</td>
     <td>status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1 +($page -1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->history_name;?></td>
   <td><?php echo $data->status;?></td>
      <td>
       <button class="edit_history btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
                <td>
                 <button type="Submit" id="remove_history" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_history btn btn-danger"><i class="icon-trash"></i></button>
                
                </td>
    
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_history ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'history/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#history_name_id').val(result[0].id);
       $('#history_name').val(result[0].history_name);
       $('#status').val(result[0].status);
       console.log(result[0].intervention_strategies_home);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_history").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'history/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed history list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

