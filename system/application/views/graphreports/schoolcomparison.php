<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::View Report::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM;?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
<script type="text/javascript">
function checkschoolcompreport()
{
var from_year = document.getElementById('from');
var to_year = document.getElementById('to');
var schooltype = document.getElementById('school_type');
var classname = document.getElementById('grade');
var testname = document.getElementById('testname');

if(from_year.selectedIndex  ==false)
{
 $('#from').css('border-color','red');
 return false;
}

if(to_year.selectedIndex  ==false)
{
 $('#from').css('border-color','#70B8BA');
 $('#to').css('border-color','red');
 return false;
}

if(schooltype.selectedIndex  ==false)
{
 $('#to').css('border-color','#70B8BA');
 $('#school_type').css('border-color','red');
 return false;
}

if(classname.selectedIndex  ==false)
{
 $('#school_type').css('border-color','#70B8BA');
 $('#grade').css('border-color','red');
 return false;
}

if(testname.selectedIndex  ==false)
{
 $('#grade').css('border-color','#70B8BA');
 $('#testname').css('border-color','red');
 return false;
}

document.singletestform.submit();

}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
      
        	<form  action='report/get_single_school_test' name="singletestform" id="singletestform" method="post" >
			<table cellpadding="5">			
			<tr>
			<td>
			 Year(s):
			  </td>
			  <td>
			  <select class="combobox" name="from" id="from" >
			  <option value="">-Please Select-</option>
              <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i.">".$i."</option>";    
			} ?> 
         	  </select>
              
             <b> to </b>
              
			  </td>
             
              <td>
			  <select class="combobox" name="to" id="to" >
			  <option value="">-Please Select-</option>
			  <?php for ($i=date('Y');$i>=1982;$i--){
			echo "<option value=".$i.">".$i."</option>";    
			} ?> 
			  </select>
			  </td>
			  
			</tr>	
            
            <tr>
			<td>
			 School Type:  
			  </td>
			  <td>

              <select class="combobox" name="school_type" id="school_type">
              <option value="">-Please Select-</option>
                 <?php
			 foreach($schools_type as $val)
			 {
				 $schooltypeid = $val['school_type_id'];
				 $name = $val['tab'];
				 ?>
                 <option value="<?php echo $schooltypeid;?>"><?php echo $name;?></option>
                 <?php
			 }
			  ?>
			  
			  
           
			  
			  </select>
			  </td>
			  
			</tr>
            
            <tr>
			<td>
			 Grades:
			  </td>
			  <td>
			  <select class="combobox" name="grade" id="grade" >
			  <option value="">-Please Select-</option>
                <?php
			 foreach($grades as $grade)
			 {
				 
				 $gradeid = $grade['dist_grade_id'];
				 $gradename = $grade['grade_name'];
				 ?>
                 <option value="<?php echo $gradeid;?>"><?php echo $gradename;?></option>
                 <?php
			 }
			  ?>
              			  
			  </select>
			  </td>
			  
			</tr>
            
             <tr>
			<td>
			 Test Name:
			  </td>
			  <td>
			  <select class="combobox" name="testname" id="testname" >
			  <option value="">-Please Select-</option>
              
              <?php
			 foreach($testname as $test_name)
			 {
			
				 $testnameid = $test_name['id'];
				 $testcatname = $test_name['assignment_name'];
				 ?>
                 <option value="<?php echo $testnameid;?>"><?php echo $testcatname;?></option>
                 <?php
			 }
			  ?>
			  
			  
			  </select>
			  </td>
			  
			</tr>
            
          
               
               <tr>
			<td>
			 
             School(s):
			  </td>
			  <td>
              <input type="text" readonly="readonly" name="schools" id="schools" value="All" />
			
            <!--   <select class="combobox" name="schools" id="schools"  >
			<!--  <option value="">-Please Select-</option>
			 		  <option value="all">All</option>
                      -->
            <?php
			 /*foreach($schools as $school)
			 {
			
				 $schoolid = $school['school_id'];
				 $schoolname = $school['school_name'];
				 ?>
                 <option value="<?php echo $schoolid;?>"><?php echo $schoolname;?></option>
                 <?php
			 }*/
			  ?>     
             
              			  
			<!--  </select>-->
 			  </td>
			  
			</tr> 
                	
					
			<tr >			
			<td>
			</td>
<td ><input title="Get Report" class="btnbig" type="button" name="getreport" value="Get Report" onclick="checkschoolcompreport();"></td>
			</tr>
			</table>
      </form>
			
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>