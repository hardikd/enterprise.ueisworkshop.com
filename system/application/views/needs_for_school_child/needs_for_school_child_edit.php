﻿   <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>Needs school</td>
     <td>Needs For school Child</td>
     <td>Status</td>
     <td>Add Action</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $cnt =1+($page -1)*10;
foreach($alldata as $data)

{?>
     </tr>
     <tr id="school_<?php echo $data->id;?>">
   <td><?php echo $cnt;?></td>
     <td><?php echo $data->need_school;?></td>
     <td><?php echo $data->intervention_strategies_school;?></td>
   <td><?php echo $data->status;?></td>
 <td><a href="<?php base_url();?>actions_school/index/<?php echo $data->parent_id;?>/<?php echo $data->id;?>">Add Action</a></td>
      <td>
      <button class="edit_needs_for_school_child btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
                <td>
        <button type="Submit" id="remove_needs_for_school_child" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_needs_for_school_child btn btn-danger"><i class="icon-trash"></i></button>
        
                </td>

    
  </tr>
  
  <?php  
  $cnt ++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_needs_for_school_child ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'needs_for_school_child/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#needs_for_school_child_id').val(result[0].id);
       $('#needs_school_id').val(result[0].parent_id);
       $('#needs_for_school_child').val(result[0].intervention_strategies_school);
       $('#status').val(result[0].status);
       console.log(result[0].id);
       $("#dialog").dialog({
      modal: true,
            height:250,
      width: 500
      });
       }
     });  
});

$(".remove_needs_for_school_child").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'needs_for_school_child/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#school_"+id).css('display','none');
          alert('Successfully removed Needs For school Child!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

