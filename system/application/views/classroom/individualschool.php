<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Teachers::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
 

function showSchools(stid)
{
	var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;	
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;	
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;		
	}
	
	$.ajax({
	type:"post",
	url:path,
	success:function(result)
	{
		$('#school_name_id').html(result);
	}
	
	});	
}

function checkfields()
{
var schooltype = document.single_test_report.school_type.value;
var classname = document.single_test_report.grade.value;




if(schooltype=="")
{
 alert('Please Select Type');
 return false;

}
else if(classname=="")
{
alert('Select Garde');
}
else
{
document.single_test_report.submit();
}



}
</script>


</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <table align="center">
		<tr>
		<td >
		<input type="hidden" name="countries" id="countries" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
		<input type="hidden" name="states" id="states" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
		<input type="hidden" name="district" id="district" value="<?php echo $this->session->userdata('district_id') ?>" >
		</td>
		
		</tr>
		
		
		</table>
		<table>
		<tr>
		<td colspan="2" align="right">
			<h1>Grade Level Report</h1>
		</td>
		</tr>
		</table>
		<div id="teacherdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
	<!--	<input class="btnbig" type="button" name="teacher_add" id="teacher_add" title="Add New" value="Add New" > -->
		</div>
		

		<form  action='graphcontrollindividualschool/showclassesbygrade' name="single_test_report" method="post" >
			<table cellpadding="5">			
            
            <tr>
			<td>
			 School Type:  
			 </td>
			  <td>

              <select class="combobox" name="school_type" id="school_type" onchange="showSchools(this.value)">
              <option value="">-Please Select-</option>
                 <?php
			 foreach($schools_type as $val)
			 {
				 $schooltypeid = $val['school_type_id'];
				 $name = $val['tab'];
				 ?>
                 <option value="<?php echo $schooltypeid;?>"><?php echo $name;?></option>
                 <?php
			 }
			  ?>
			  
			  
           
			  
			  </select>
			  </td>
			  
			</tr>
             <tr>
			<td>
			 Select School:  
			 </td>
			  <td>

              <select class="combobox" name="school_name_id" id="school_name_id" >
              <option value="">-Please Select-</option>			  
           
			  </select>
			  </td>
			  
			</tr>
            <tr>
			<td>
			 Grades:
			  </td>
			  <td>
			  <select class="combobox" name="grade" id="grade" >
			  <option value="">-Please Select-</option>
                <?php
			 foreach($grades as $grade)
			 {
				 
				 $gradeid = $grade['dist_grade_id'];
				 $gradename = $grade['grade_name'];
				 ?>
                 <option value="<?php echo $gradeid;?>"><?php echo $gradename;?></option>
                 <?php
			 }
			  ?>
              			  
			  </select>
			  </td>
			  
			</tr>
   
			<tr>			
			<td>
			</td>
<td ><input title="Get Report" class="btnbig" type="button" name="submitform" value="School Report" onclick="checkfields();"></td>
			</tr>
	</table>
   </form>
		
<!-- Graph -->

<!-- show seleted fields -->
<?php 


// GET RECENTLY 3 ASSESSMENTS
	
	$this->load->model('classroommodel');
	$data = $this->classroommodel->getrecentlyassignments();

	$assignmentname = array();
	$assignmentids = array();
	for($i=0;$i<count($data);$i++)
	{
	$assignmentname[] = $data[$i]['assignment_name'];
	$assignmentids[] = $data[$i]['id'];
	}
	
	
	if(!empty($assignmentname))
	{
		if(!empty($assignmentname[0]) && $assignmentname[0]!="")
		{
		$first = $assignmentname[0];
		}
		else
		{
			$first = '';
		}
		if(!empty($assignmentname[1]) && $assignmentname[1]!="")
		{
		$second = $assignmentname[1];
		}
		else
		{
			$second = '';
		}
		if(!empty($assignmentname[2]) && $assignmentname[0]!="")
		{
		$third = $assignmentname[2];
		}
		else
		{
			$third = '';
		}
		
		
	}
	
// GET RECENTLY 3 ASSESSMENTS

if(isset($classroom))
{
 ?>
<table>
		<tr>
			<td colspan="2" align="right">
				<h3> 
				<?php
				   		echo $schooltypename = $this->classroommodel->fetchSchoolTypeName(@$schooltypeid1); // school type Name 
						echo '&nbsp&nbsp -> &nbsp';
						echo $schoolname = $this->classroommodel->fetchSchoolName(@$school_name_id1); // School Name 							 
						echo '&nbsp&nbsp -> &nbsp';
						echo $cid_resut_1 = $this->classroommodel->fetchGradeName(@$gradeid1); // Grade Name
				?>
				</h3>
			</td>
		</tr>
</table>
<?Php
}
?>

<!-- End selected -->

<?Php

$classId = array();
$classroomarr = array();
$classroomarr2 = array();

if(!empty($classroom))
{
	foreach($classroom as $key=>$val)
	{
		$classroomarr[] = $classroom[$key]['name'];
		$classroomarr2[] = $classroom[$key]['name'];
		$classId[] = $classroom[$key]['class_room_id'];
	}
	
	// class room name
	
	 $classroomarr =  implode("','",$classroomarr);
	
	 $classroomarr = "['','".$classroomarr."']";

//************************************************************************
	
	$avgarr = array();
	
	$bb = array();
	$b = array();
	$pf = array();
		

// fetch student id according according class id

// 1 class r

		
		
			if(!empty($classId[0]) && $classId[0]!="")
			{
			$cid_Stdent1 = $classId[0];
			
			$cid_resut_1 = $this->classroommodel->getallSchoolIdByClassid($cid_Stdent1);			
	

	
			if(!empty($cid_resut_1))
			{
				$sidarra =  array();
					foreach($cid_resut_1 as $key01 => $val01)
					{
						$sidarra[] = $cid_resut_1[$key01]['student_id'];				
					}					
					
		
					$studentids = array_unique($sidarra);    // student id
					if(!empty($studentids))
					{
					$numarr = array();
						foreach($studentids as $key => $val)
						{
							$userid = $studentids[$key];
							
							//$numarr[] = $this->classroommodel->studentscoreByWrkid($userid);
							
							$numarr[] = $this->classroommodel->studentscoreByWrkid1($userid,$assignmentids);
							
						}
	
	
		if(!empty($numarr))
			{
					
				$temp = array();
				for($i=0;$i<count($numarr);$i++)
				{
				
				for($j=0;$j<count($numarr[$i]);$j++)
				{
					
					if(!empty($numarr[$i][$j]) && $numarr[$i][$j]!="")
					{
					
					$temp[$j][] = $numarr[$i][$j][0]['pass_score_point'];
					}
					
				}
				
							
				}
				
				
			
				
				if(!empty($numarr))
				{
					
				if(!empty($temp[0]) && $temp[0]!="")
				{
				$sum1 = array_sum($temp[0]);
				$counttotal1 = count($temp[0]);
				 $first1 =  $sum1/$counttotal1;
				}
				else
				{
					$first1=0;
				}
				
				
				
				if(!empty($temp[1]) && $temp[1]!="")
				{			
				
				$sum2 = array_sum($temp[1]);
				$counttotal2 = count($temp[1]);
				 $first2 =  $sum2/$counttotal2;
				}
				else
				{
					$first2=0;
				}
				
				
				if(!empty($temp[2]) && $temp[2]!="")
				{
				
					$sum3 = array_sum($temp[2]);
					$counttotal3 = count($temp[2]);
					 $first3 =  $sum3/$counttotal3;
				}
				else
				{
					$first3=0;
				}
				
	
				 $sumschoolA = $first1 + $first2 + $first3;
				$countSchoolA = count($assignmentids);
				
				 $finalclassroom1Avg = round($sumschoolA/$countSchoolA);
	
	
				}
			}
			
		}
		else
		{
			echo '<script>alert("Record Not Available");</script>';
		}
				
		
		}
			}

		else
			{
				$first1=0;
				$first2=0;
				$first3=0;
				$finalclassroom1Avg=0;
			}
		
		
		// End 1 class r
			
			
// 2 class r

		if(!empty($classId[1]) && $classId[1]!="")
		{
		$cid_Stdent2c2 = $classId[1];
		
		$cid_resut_2c2 = $this->classroommodel->getallSchoolIdByClassid($cid_Stdent2c2);			
			
			
			if(!empty($cid_resut_2c2))
			{
				$sidarrac2 = array();
				
					foreach($cid_resut_2c2 as $key01 => $val01)
					{
						$sidarrac2[] = $cid_resut_2c2[$key01]['student_id'];				
					}					
					
					$studentidsc2 = array_unique($sidarrac2);    // student id class 2
					$numarrc2 = array();
						foreach($studentidsc2 as $key => $val)
						{
							$useridc2 = $studentidsc2[$key];							
							
							//$numarrc2[] = $this->classroommodel->studentscoreByWrkid($useridc2);
						$numarrc2[] = $this->classroommodel->studentscoreByWrkid1($useridc2,$assignmentids);
						}
			
					
			
	$temp2 = array();
		for($i=0;$i<count($numarrc2);$i++)
		{
			
			for($j=0;$j<count($numarrc2[$i]);$j++)
			{
				if(!empty($numarrc2[$i][$j]) && $numarrc2[$i][$j]!="")
					{
					$temp2[$j][] = $numarrc2[$i][$j][0]['pass_score_point'];
					}
			}
					
		}
		
			
		if(!empty($temp2[0]) && $temp2[0]!="")
		{
					
		$sum4 = array_sum($temp2[0]);
		$counttotal4 = count($temp2[0]);
		 $second1 =  $sum4/$counttotal4;
		
		}
		else
		{
			$second1=0;
		}
		
		if(!empty($temp2[1]) && $temp2[1]!="")
		{
		$sum5 = array_sum($temp2[1]);
		$counttotal5 = count($temp2[1]);
		 $second2 =  $sum5/$counttotal5;
		}
		else
		{
			$second2=0;
		}
		
		if(!empty($temp2[2]) && $temp2[2]!="")
		{
		$sum6 = array_sum($temp2[2]);
		$counttotal6 = count($temp2[2]);
		 $second3 =  $sum6/$counttotal6;
		}
		else
		{
			$second3=0;
		}
		
		

		 $sumschoolB = $second1 + $second2 + $second3;
		$countclassB = count($assignmentids);
		
		 $finalclassroom2Avg = round($sumschoolB/$countclassB);
		
	
			
		
		}
		}
		else
		{
			$second1=0;
			$second2=0;
			$second3=0;
			$finalclassroom2Avg=0;
			
		}
		
		// End 1 class 2 r
		
// 3 class r

		
		if(!empty($classId[2]) && $classId[2]!="")
		{
		$cid_Stdent3c3 = $classId[2];
			
			
		$cid_resut_3c3 = $this->classroommodel->getallSchoolIdByClassid($cid_Stdent3c3);			
			
			if(!empty($cid_resut_3c3))
			{
				$sidarrac3 = array();
					foreach($cid_resut_3c3 as $key01 => $val01)
					{
						$sidarrac3[] = $cid_resut_3c3[$key01]['student_id'];				
					}					
					
					$studentidsc3 = array_unique($sidarrac3);    // student id class 2
					$numarrc3 = array();
					
						foreach($studentidsc3 as $key => $val)
						{
							$useridc3 = $studentidsc3[$key];
							
							//$numarrc3[] = $this->classroommodel->studentscoreByWrkid($useridc3);    // 3 student ids
					$numarrc3[] = $this->classroommodel->studentscoreByWrkid1($useridc3,$assignmentids);
							
						}
			
		
					
	$temp3 = array();
		for($i=0;$i<count($numarrc3);$i++)
		{
			
			for($j=0;$j<count($numarrc3[$i]);$j++)
			{
				if(!empty($numarrc3[$i][$j]) && $numarrc3[$i][$j]!="")
					{
					$temp3[$j][] = $numarrc3[$i][$j][0]['pass_score_point'];
					}
			}
					
		}
		
		
		if(!empty($temp3[0]) && $temp3[0]!="")
		{
		$sum7 = array_sum($temp3[0]);
		$counttotal7 = count($temp3[0]);
		 $third1 =  $sum7/$counttotal7;
		}
		else
		{
			$third1=0;
		}
		
		if(!empty($temp3[1]) && $temp3[1]!="")
		{
		$sum8 = array_sum($temp3[1]);
		$counttotal8 = count($temp3[1]);
		 $third2 =  $sum8/$counttotal8;
		}
		else
		{
			$third2=0;
		}
		
		if(!empty($temp3[2]) && $temp3[2]!="")
		{
		$sum9 = array_sum($temp3[2]);
		$counttotal9 = count($temp3[2]);
		 $third3 =  $sum9/$counttotal9;
		}
		else
		{
			$third3=0;
		}
	

		
		 $sumschoolC = $third1 + $third2 + $third3;
		$countSchoolC = count($assignmentids);
		
		 $finalclassroom3Avg = round($sumschoolC/$countSchoolC);
	
		
		}
		}
		else
		{
			$third1=0;
			$third2=0;
			$third3=0;
			$finalclassroom3Avg=0;
		}
		// End 1 class 3 r
// 4 class r
		
		if(!empty($classId[3]) && $classId[3]!="")
		{
		$cid_Stdent4 = $classId[3];
		
		$cid_resut_3c4 = $this->classroommodel->getallSchoolIdByClassid($cid_Stdent4);			
			
			if(!empty($cid_resut_3c4))
			{
				$sidarrac4 = array();
					foreach($cid_resut_3c4 as $key01 => $val01)
					{
						$sidarrac4[] = $cid_resut_3c4[$key01]['student_id'];				
					}					
					
					$studentidsc4 = array_unique($sidarrac4);    // student id class 2
					$numarrc4 = array();
						foreach($studentidsc4 as $key => $val)
						{
							$useridc4 = $studentidsc4[$key];
							
							//$numarrc4[] = $this->classroommodel->studentscoreByWrkid($useridc4);    // 3 student ids
							
				$numarrc4[] = $this->classroommodel->studentscoreByWrkid1($useridc4,$assignmentids);
							
						}
			
		
		
		$temp4 = array();
		for($i=0;$i<count($numarrc4);$i++)
		{
			for($j=0;$j<count($numarrc4[$i]);$j++)
			{
				if(!empty($numarrc4[$i][$j]) && $numarrc4[$i][$j]!="")
					{
				$temp4[$j][] = $numarrc4[$i][$j][0]['pass_score_point'];
					}
			}
					
		}
		
		if(!empty($temp4[0]) && $temp4[0]!="")
		{
						
		$sum10 = array_sum($temp4[0]);
		$counttotal10 = count($temp4[0]);
		 $fourth1 =  $sum10/$counttotal10;
		}
		else
		{
			$fourth1=0;
		}
		
		if(!empty($temp4[1]) && $temp4[1]!="")
		{
		$sum11 = array_sum($temp4[1]);
		$counttotal11 = count($temp4[1]);
		 $fourth2 =  $sum11/$counttotal11;
		}
		else
		{
			$fourth2=0;
		}
		
		if(!empty($temp4[2]) && $temp4[2]!="")
		{
		$sum12 = array_sum($temp4[2]);
		$counttotal12= count($temp4[2]);
		 $fourth3 =  $sum12/$counttotal12;
		}
		else
		{
		$fourth3=0;	
		}


		
		 $sumschoolD = $fourth1 + $fourth2 + $fourth3;
		$countSchoolD = count($assignmentids);
		
		 $finalclassroom4Avg = round($sumschoolD/$countSchoolD);
	

		}
		}
		else
		{
			$fourth1=0;
			$fourth2=0;
			$fourth3=0;
			$finalclassroom4Avg=0;
		}
		
		
		// End  class 4 r
// 5 class r
	
		if(!empty($classId[4]) && $classId[4]!="")
		{
		$cid_Stdent5 = $classId[4];	

					$cid_resut_3c5 = $this->classroommodel->getallSchoolIdByClassid($cid_Stdent5);			
			
			if(!empty($cid_resut_3c5))
			{
				$sidarrac5 = array();
				$assessmentid = array();
					foreach($cid_resut_3c5 as $key01 => $val01)
					{
						$sidarrac5[] = $cid_resut_3c5[$key01]['student_id'];				
					}					
					
					$studentidsc5 = array_unique($sidarrac5);    // student id class 2
					$numarrc5 = array();
						foreach($studentidsc5 as $key => $val)
						{
							$useridc5 = $studentidsc5[$key];
							
						//	$numarrc5[] = $this->classroommodel->studentscoreByWrkid($useridc5);    // 3 student ids
							$numarrc5[] = $this->classroommodel->studentscoreByWrkid1($useridc5,$assignmentids);	
							
						}
						
						
		$temp5 = array();
		for($i=0;$i<count($numarrc5);$i++)
		{
		
	for($j=0;$j<count($numarrc5[$i]);$j++)
	{
		if(!empty($numarrc5[$i][$j]) && $numarrc5[$i][$j]!="")
			{
				$temp5[$j][] = $numarrc5[$i][$j][0]['pass_score_point'];
			}
	}
					
		}
		
			
		if(!empty($temp5[0]) && $temp5[0]!="")
		{ 		
		$sum13 = array_sum($temp5[0]);
		$counttotal13 = count($temp5[0]);
		$fifth1 =  $sum13/$counttotal13;
		}
		else
		{
			$fifth1=0;
		}
		
		if(!empty($temp5[1]) && $temp5[1]!="")
		{
		$sum14 = array_sum($temp5[1]);
		$counttotal14 = count($temp5[1]);
		 $fifth2 =  $sum14/$counttotal14;
		}
		else
		{
			$fifth2=0;
		}
		
		if(!empty($temp5[2]) && $temp5[2]!="")
		{
		$sum15= array_sum($temp5[2]);
		$counttotal15 = count($temp5[2]);
		 $fifth3 =  $sum15/$counttotal15;
		}
		else
		{
			$fifth3=0;
		}

		 $sumschoolE = $fifth1 + $fifth2 + $fifth3;
		$countSchoolE = count($assignmentids);
		
		 $finalclassroomEAvg = round($sumschoolE/$countSchoolE);

		
		}
		}
		else
		{
			$finalclassroomEAvg=0;
			$fifth1=0;
			$fifth2=0;
			$fifth3=0;
		}
		
	// pie chart working
	
	

		$arrfltr = array_filter($classroomarr2);
		$arrsumfltr = count($arrfltr);
		
		if(empty($arrsumfltr))
		{
			$arrsumfltr=1;
		}

//1

	if(empty($first1))
	{
		$first1 = 0;
	}
	if(empty($second1))
	{
		$second1 = 0;
	}
	if(empty($third1))
	{
		$third1 = 0;
	}
	if(empty($fourth1))
	{
		$fourth1 = 0;
	}
	if(empty($fifth1))
	{
		$fifth1 = 0;
	}
//2
	
	if(empty($first2))
	{
		$first2 = 0;
	}
	if(empty($second2))
	{
		$second2 = 0;
	}
	if(empty($third2))
	{
		$third2 = 0;
	}
	if(empty($fourth2))
	{
		$fourth2 = 0;
	}
	if(empty($fifth2))
	{
		$fifth2 = 0;
	}
	
	//3
	if(empty($first3))
	{
		$first3 = 0;
	}
	if(empty($second3))
	{
		$second3 = 0;
	}
	if(empty($third3))
	{
		$third3 = 0;
	}
	if(empty($fourth3))
	{
		$fourth3 = 0;
	}
	if(empty($fifth3))
	{
		$fifth3 = 0;
	}
	$avgass1 = array();
	
	 $avgass1[] = ($first1 + $second1 + $third1 + $fourth1 + $fifth1)/$arrsumfltr;
	 $avgass1[] = ($first2 + $second2 + $third2 + $fourth2 + $fifth2)/$arrsumfltr;
	 $avgass1[] = ($first3 + $second3 + $third3 + $fourth3 + $fifth3)/$arrsumfltr;
	 
	
	
	//echo '<pre>';
	//print_r($avgass1);
	//exit;	

	foreach($assignmentids as $key222=>$val222)
	{
		$res = mysql_query('select * from proficiency where assessment_id="'.$assignmentids[$key222].'"');
		$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					 $num = $avgass1[$key222];
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					$bb[] = $num;
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	}
	

	
// end pie chart working	
	// End  class 5 r

// For pie graph working		
		
	
			//$adv = array();
			/*$pro = array();
			$basic = array();
			$bbasic = array();  
			
			$res = mysql_query('select * from proficiency');
		$row = mysql_fetch_assoc($res);
		

			$bbfrom = $row['bbasicfrom'];
			$bbto = $row['bbasicto'];
			$basicfrom = $row['basicfrom'];
			$basicto = $row['basicto'];
			$pro_from = $row['pro_from'];
			$pro_to = $row['pro_to'];	*/
			
			
	/*for($i=0;$i<count($avgarr);$i++)
		{
				$num = $avgarr[$i];
				
				if($num>=$bbfrom && $num<$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$basicfrom && $num<$basicto)
				{
					 $basic[] = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro[] = $num;
				}*/
				
			/*	if($num>=0 && $num<40)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=40 && $num<77)
				{
					 $basic[] = $num;
				}
				if($num>=77 && $num<100)
				{
					$pro[] = $num;
				}*/
				
		//}
				//1
			
		 /*$ttarr = array_sum($bbasic);
		 
		  if($ttarr==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			  $avgbbbasic = $ttarr/$total;
			 }
		  
		 // 2
		  	 $ttarr1 = array_sum($basic);	
		  
		  	 if($ttarr1==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $ttarr1/$total1;
			 }
		 //3
		 
		  $ttarr2 = array_sum($pro);		 
		 if($ttarr2==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
				  $avgpro = $ttarr2/$total2;
			 }*/
	

if(empty($bb))
			{
				$finalbb=0;
			}
			else
			{
				$total1 = count($bb);
				$tt1 = array_sum($bb);
				$finalbb = $tt1/$total1;
			}
			
			if(empty($b))
			{
				$finalb=0;
			}
			else
			{
				$total2 = count($b);
				$tt2 = array_sum($b);
				$finalb = $tt2/$total2;	
			}
			
			if(empty($pf))
			{
				$finalpf=0;	
			}
			else
			{
				$total3 = count($pf);
				$tt3 = array_sum($pf);
				$finalpf = $tt3/$total3;
			}
			
				
	 $totalval = $finalbb + $finalb + $finalpf;
	
	if($finalbb!=0)
	{
		$frstval = ($finalbb/$totalval)*100;
	}
	else
	{
	$frstval=0;
	}
	if($finalb!=0)
	{
		$secval = ($finalb/$totalval)*100;
	}
	else
	{
	$secval=0;
	}
	if($finalpf!=0)
	{
		$thrdval = ($finalpf/$totalval)*100;
	}
	else
	{
	$thrdval=0;
	}
	
	 $finalbb=round($frstval);	
	 $finalb=round($secval);
	 $finalpf=round($thrdval);

/*
?>

<b><font color="#2f7ed8">Below Basic = <?Php echo @$finalbb; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#0d233a">Basic =  <?Php echo @$finalb; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#8bbc21">Proficient =<?Php echo @$finalpf; ?>% </font></b>
<?php 

*/ 

?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: <?Php echo $classroomarr; ?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' %';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y; 
                    }
                    return s;
                }
            },
			
           
            series: [
			{
				type: 'column',
				name: '',
				data: [,,,,],
			},
			{
                type: 'column',
                name: '<?php echo $first;?>',
                data: [0,<?Php echo $first1; ?>, <?Php echo $second1; ?>, <?Php echo $third1; ?>, <?Php echo $fourth1; ?>, <?Php echo $fifth1; ?>]
            }, {
                type: 'column',
                name: '<?php echo $second;?>',
                data: [0,<?Php echo $first2; ?>, <?Php echo $second2; ?>, <?Php echo $third2; ?>, <?Php echo $fourth2; ?>, <?Php echo $fifth2; ?>]
            }, {
                type: 'column',
                name: '<?php echo $third; ?>',
                data: [0,<?Php echo $first3; ?>, <?Php echo $second3; ?>, <?Php echo $third3; ?>, <?Php echo $fourth3; ?>, <?Php echo $fifth3; ?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [0,<?php echo $finalclassroom1Avg; ?>, <?php echo $finalclassroom2Avg; ?>, <?php echo $finalclassroom3Avg; ?>, <?php echo $finalclassroom4Avg; ?>, <?php echo $finalclassroomEAvg; ?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }, {
                type: 'pie',
                name: 'Total consumption',
                data: [{
                    name: 'Below Basic',
                    y: <?Php echo @$finalbb; ?>,
                    color: Highcharts.getOptions().colors[0] // Below Basic's color
                }, {
                    name: 'Basic Scores ',
                    y: <?Php echo @$finalb; ?>,
                    color: Highcharts.getOptions().colors[1] // Basic's color
                }, {
                    name: 'Proficient',
                    y: <?Php echo @$finalpf; ?>,
                    color: Highcharts.getOptions().colors[2] // Proficient's color
                }],
                 center: [25, 10],
                size: 100,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    });
    

		</script>
	
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
		<!-- End Graph -->			
		<?Php } 
		else
		{		
			if(isset($classroom))
			{
		?>
			<script type="text/javascript">
			alert("No Record Found");
			</script>
		<?php
			}
			echo $this->session->flashdata('item');			
			
		}
		
		
		?>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
