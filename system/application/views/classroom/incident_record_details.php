                                  
<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
    <thead>
        <tr>
            <th>Date & Time</th>
            <th>Location</th>
            <th>Victim(s)</th>
            <th>Suspect(s)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $incident_details['date'];?></td>
            <td><?php echo $incident_details['location'];?></td>
            <td><?php foreach($incident_details['victim'] as $victims):?><?php echo $victims[0]->firstname.' '.$victims[0]->lastname;?><br /><?php endforeach;?></td>
            <td><?php foreach($incident_details['suspect'] as $suspects):?><?php echo $suspects[0]->firstname.' '.$suspects[0]->lastname;?><br /><?php endforeach;?></td>
        </tr>
    </tbody>
</table>


<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
    <thead>
        <tr>
            <th>Problem Behavior(Additional Action/forms may be required)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
               <?php foreach ($incident_details['incident'] as $problem):?> <?php echo $problem->incident_name;?><br /><?php endforeach;?>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
    <thead>
        <tr>
            <th>Incident Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php echo $incident_details['description'];?>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
    <thead>
        <tr>
            <th>Possible Motivation</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php foreach ($incident_details['motivation'] as $motivation):?> <?php echo $motivation->possible_motivation;?><br /><?php endforeach;?>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
    <thead>
        <tr>
            <th>Interventions Prior To Office Discipline Referral</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
               <?php foreach ($incident_details['intervention'] as $intervention):?> <?php echo $intervention->interventions_prior;?><br /><?php endforeach;?>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
    <thead>
        <tr>
            <th>If Student Has Multiple Referrals,Referred To</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
               <?php foreach ($incident_details['referral'] as $referral):?> <?php echo $referral->student_has_multiple_referrals;?><br /><?php endforeach;?>
            </td>
        </tr>
    </tbody>
</table>
                                           
