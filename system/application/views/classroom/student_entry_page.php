<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
.chzn-choices {border:none !important;box-shadow:none !important;-webkit-box-shadow:none !important;}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/progress_report">Student Progress Report</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>classroom/create_progress_report">Create</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget yellow">
                         <div class="widget-body">
<?php /*?><form class="form-horizontal" method="post" action="<?php echo base_url().'classroom/create_progress';?>" name="parent_teacher_form" id="parent_teacher_form"><?php */?>
<form class="form-horizontal" method="post" action="<?php echo base_url().'classroom/student_progress_report';?>" name="parent_teacher_form" id="parent_teacher_form">
  <input type="hidden" name="quarter_number" id="quarter_number" value="<?php echo $quater_no;?>">
  <input type="hidden" name="period_id" id="period_id" value="<?php echo $period_id;?>">
  <input type="hidden" name="student_id" id="student_id" value="<?php echo $student_id;?>">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                                 <div class="tab-content">
                                 
                                    <table>
                                    <tr> 
                                      <td width="400px">
                                    Long Beach Unified School District<br/>
                                    <?php echo $this->session->district_name;?>
                                    <br/><?php echo $school_details[0]->school_name;?>
                                    <br/>Principal: <?php echo $observer_details[0]->observer_name;?>

                                    <br/>Teacher: <?php echo $this->session->userdata('teacher_name');?>
                                    <br/>School Year: <?php echo date('Y').' - '.date('Y',strtotime('+1 years'));?>
                                    <br/>Student: <?php echo $student_details[0]->firstname.' '.$student_details[0]->lastname;?>
                                    <br/>Grade: <?php echo $grade_subjects[0]['grade_name'];?>
                                    </td>
                                    <td width="300px">
                                    <img src="../../<?php echo $view_path.'inc/logo/u2.png';?>" >
                                    </td>
                                    <td>
                                      <table border="1" cellspacing="5" cellpadding="5">
                                        <tr>
                                          <td>

                                          </td>
                                          <td>Q 1</td>
                                          <td>Q 2</td>
                                          <td>Q 3</td>
                                          <td>Q 4</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            Days Present
                                          </td>
                                          <td>Q 1</td>
                                          <td>Q 2</td>
                                          <td>Q 3</td>
                                          <td>Q 4</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            Days Absent
                                          </td>
                                          <td>Q 1</td>
                                          <td>Q 2</td>
                                          <td>Q 3</td>
                                          <td>Q 4</td>
                                        </tr> 
                                        <tr>
                                          <td>
                                            Days Tardy
                                          </td>
                                          <td>Q 1</td>
                                          <td>Q 2</td>
                                          <td>Q 3</td>
                                          <td>Q 4</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            Days Left Early
                                          </td>
                                          <td>Q 1</td>
                                          <td>Q 2</td>
                                          <td>Q 3</td>
                                          <td>Q 4</td>
                                        </tr>                                                                          
                                      </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <br>
                                    <table>
                                      <tr>
                                      <?php foreach($grade_subjects as $key=>$grade_subject):?>
                                        <?php if ($key%2==0):?>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
                                          &nbsp;
                                        </td>
                                        </tr>
                                        <tr>
                                        <?php else:?>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                      <?php endif;?>
                                      <td style="vertical-align:top;">
                                        <table border="1" cellpadding="5">
                                          <tr>
                                            <td>
                                              <b><?php echo $grade_subject['subject_name'];?></b>
                                            </td>
                                            <td width="250px">

                                            </td>
                                            <td>
                                              AC
                                            </td>
                                            <td>
                                              EF
                                            </td> 
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 1 Standards
                                            </td>
                                            <td>
                                              <?php $q1Current = false;$currkey='';?>
                                              <?php foreach($getreports as $key=>$getreport):?>
                                              <?php if($quater_no==$getreport['quarter'] && $grade_subject['grade_subject_id']==$getreport['subject_id'] && $getreport['quarter']==1):?>
                                                <?php echo implode(",",unserialize($getreport['standards']));?>
                                                <?php $q1Current = true; $currkey=$key;break;?>
                                              <?php endif;?>
                                              <?php endforeach;?>
                                              <?php if($quater_no==1 && !$q1Current):?>
                                              <select class="span6 chzn-select" id='<?php echo $grade_subject['grade_subject_id'];?>_q1_standards' name='standards[<?php echo $grade_subject['grade_subject_id'];?>][]' data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" multiple>
                                              <option value=''>-Please Select-</option>
                                                <?php foreach($reportstandards as $reportstandard):?>
                                                  <option value="<?php echo $reportstandard['standards_abr'];?>"><?php echo $reportstandard['standards'];?></option>
                                                <?php endforeach;?>
                                              </select>
                                              <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php if($q1Current && $grade_subject['grade_subject_id']==$getreports[$currkey]['subject_id']):?>
                                                <?php echo $getreports[$currkey]['grade'];?>
                                              <?php else:?>
                                               <?php if($quater_no==1):?>
                                              <select name="grade1[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                              <?php endif;?>
                                            <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php if($q1Current && $grade_subject['grade_subject_id']==$getreports[$currkey]['subject_id']):?>
                                                <?php echo $getreports[$currkey]['effort'];?>
                                              <?php else:?>
                                              <?php if($quater_no==1):?>
                                               <select name="grade2[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;vertical-align:top;">
                                                  
                                              <?php foreach($reportEfforts as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['efforts'];?>"><?php echo $reportGrade['efforts'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                              <?php endif;?>
                                              <?php endif;?>
                                            </td> 
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 1 Comments<br />
                                              <br />
                                            </td>
                                            <td>
                                              <?php if($q1Current && $grade_subject['grade_subject_id']==$getreports[$currkey]['subject_id']):?>
                                                <?php echo implode("<br />",unserialize($getreports[$currkey]['comments']));?>
                                              <?php else:?>
                                              <?php if($quater_no==1):?>
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][1]" id="<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_1" ><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][2]" id="<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_2"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][3]" id="<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_3"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][4]" id="<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_4"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][5]" id="<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_5">
                                              <?php endif;?>
                                               <?php endif;?>
                                            </td>
                                            
                                             
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 2 Standards
                                            </td>
                                            <td>
                                              <?php $q2Current = false;$currkey='';?>
                                              <?php foreach($getreports as $key=>$getreport):?>
                                              <?php if($quater_no==$getreport['quarter'] && $grade_subject['grade_subject_id']==$getreport['subject_id'] && $getreport['quarter']==2):?>
                                                <?php echo implode(",",unserialize($getreport['standards']));?>
                                                <?php $q2Current = true; $currkey=$key;break;?>
                                              <?php endif;?>
                                              <?php endforeach;?>

                                              <?php if($quater_no==2 && !$q2Current):?>
                                              <select name="standards[<?php echo $grade_subject['grade_subject_id'];?>][]" class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width:150px;" multiple>
                                                <option>Select Standards</option>
                                                  
                                                <?php foreach($reportstandards as $reportstandard):?>
                                                  <option value="<?php echo $reportstandard['standards_abr'];?>"><?php echo $reportstandard['standards'];?></option>
                                                <?php endforeach;?>
                                              </select>
                                              <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php if($q2Current && $grade_subject['grade_subject_id']==$getreports[$currkey]['subject_id']):?>
                                                <?php echo $getreports[$currkey]['grade'];?>
                                              <?php else:?>
                                              <?php if($quater_no==2):?>
                                              <select name="grade1[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                              <?php endif;?>
                                              <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php if($q2Current && $grade_subject['grade_subject_id']==$getreports[$currkey]['subject_id']):?>
                                                <?php echo $getreports[$currkey]['effort'];?>
                                              <?php else:?>
                                              <?php if($quater_no==2):?>
                                               <select name="grade2[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;vertical-align:top;">
                                                  
                                              <?php foreach($reportEfforts as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['efforts'];?>"><?php echo $reportGrade['efforts'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                              <?php endif;?>
                                              <?php endif;?>
                                            </td>  
                                          </tr>
                                          <tr>
                                            <td>
                                             Q 2 Comments<br />
                                              <br />
                                            </td>
                                            <td>
                                              <?php if($q2Current && $grade_subject['grade_subject_id']==$getreports[$currkey]['subject_id']):?>
                                                <?php echo implode("<br />",unserialize($getreports[$currkey]['comments']));?>
                                              <?php else:?>
                                              <?php if($quater_no==2):?>
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][1]" id="<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_1"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][2]" id="<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_2"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][3]" id="<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_3"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][4]" id="<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_4"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][5]" id="<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_5">
                                            <?php endif;?>
                                            <?php endif;?>
                                            </td>
                                           
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 3 Standards
                                            </td>
                                            <td>
                                              <?php if($quater_no==3):?>
                                              <select name="standards[<?php echo $grade_subject['grade_subject_id'];?>][]" style="width:150px;" class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" multiple>
                                                <option>Select Standards</option>
                                                  
                                                <?php foreach($reportstandards as $reportstandard):?>
                                                  <option value="<?php echo $reportstandard['standards_abr'];?>"><?php echo $reportstandard['standards'];?></option>
                                                <?php endforeach;?>
                                              </select>
                                            <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php if($quater_no==3):?>
                                              <select name="grade1[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php if($quater_no==3):?>
                                               <select name="grade2[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;vertical-align:top;">
                                                  
                                              <?php foreach($reportEfforts as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['efforts'];?>"><?php echo $reportGrade['efforts'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>  
                                          </tr>
                                          <tr>
                                            <td>
                                             Q 3 Comments<br />
                                              <br />
                                            </td>
                                            <td>
                                              <?php if($quater_no==3):?>
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][1]" id="<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_1"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][2]" id="<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_2"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][3]" id="<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_3"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][4]" id="<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_4"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][5]" id="<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_5">
                                              <?php endif;?>
                                            </td>
                                            
                                          </tr>
                                          <tr>
                                            <td>
                                             Q 4 Standards
                                            </td>
                                            <td>
                                              <?php if($quater_no==4):?>
                                              <select name="standards[<?php echo $grade_subject['grade_subject_id'];?>][]" style="width:150px;" class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" multiple>
                                                <option>Select Standards</option>
                                                  
                                                <?php foreach($reportstandards as $reportstandard):?>
                                                  <option value="<?php echo $reportstandard['id'];?>"><?php echo $reportstandard['standards'];?></option>
                                                <?php endforeach;?>
                                              </select>
                                            <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                               <?php if($quater_no==4):?>
                                              <select name="grade1[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                               <?php if($quater_no==4):?>
                                               <select name="grade2[<?php echo $grade_subject['grade_subject_id'];?>]" style="width:50px;vertical-align:top;">
                                                  
                                              <?php foreach($reportEfforts as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['efforts'];?>"><?php echo $reportGrade['efforts'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td> 
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 4 Comments
                                              <br />
                                              <br />
                                            </td>
                                            <td>
                                               <?php if($quater_no==4):?>
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][1]" id="<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_1"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][2]" id="<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_2"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][3]" id="<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_3"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][4]" id="<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_4"><br /><br />
                                              <input type="text" name="comment[<?php echo $grade_subject['grade_subject_id'];?>][5]" id="<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_5">
                                              <?php endif;?>
                                            </td>
                                            
                                          </tr>

                                        </table>
                                      </td>

                                      <?php endforeach;?>
                                    </tr>
                                    </table>
                                    <br /><br /><br /><br />
                                    <table>
                                      <tr>
                                        <td>
                                          <table border="1" cellpadding="5">
                                            <tr>
                                              <td><b>Work Habits</b></td>
                                              <td> Q 1 </td>
                                              <td> Q 2 </td>
                                              <td> Q 3 </td>
                                              <td> Q 4 </td>
                                            </tr>
                                            <?php foreach($workinghabits as $workinghabit):?>
                                            <tr>
                                              <td><?php echo $workinghabit['summary'];?></td>
                                              <td>
                                                 <?php if($quater_no==1):?>
                                                <select name="work_habit[<?php echo str_replace(' ','_',$workinghabit['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                              </td>
                                              <td>
                                                 <?php if($quater_no==2):?>
                                                <select name="work_habit[<?php echo str_replace(' ','_',$workinghabit['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                              </td>
                                              <td>
                                                 <?php if($quater_no==3):?>
                                                <select name="work_habit[<?php echo str_replace(' ','_',$workinghabit['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                              </td>
                                              <td>
                                                 <?php if($quater_no==4):?>
                                                <select name="work_habit[<?php echo str_replace(' ','_',$workinghabit['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                              </td>
                                            </tr>
                                          <?php endforeach;?>
                                            
                                          </table>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                          <table border="1" cellpadding="5">
                                            <tr>
                                              <td><b>Learning & Social Skills</b></td>
                                              <td> Q 1 </td>
                                              <td> Q 2 </td>
                                              <td> Q 3 </td>
                                              <td> Q 4 </td>
                                            </tr>
                                            <?php foreach($learningSkills as $learningSkill):?>
                                            <tr>
                                              <td><?php echo $learningSkill['summary'];?></td>
                                              <td>
                                                 <?php if($quater_no==1):?>
                                              <select name="learning[<?php echo str_replace(' ','_',$learningSkill['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                              <td>
                                                 <?php if($quater_no==2):?>
                                              <select name="learning[<?php echo str_replace(' ','_',$learningSkill['summary']);?>]" style="width:50px;">
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                              <td>
                                                 <?php if($quater_no==3):?>
                                                <select name="learning[<?php echo str_replace(' ','_',$learningSkill['summary']);?>]" style="width:50px;">  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                              <td>
                                                 <?php if($quater_no==4):?>
                                                <select name="learning[<?php echo str_replace(' ','_',$learningSkill['summary']);?>]" style="width:50px;">
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                              <?php endif;?>
                                            </td>
                                            </tr>
                                            <?php endforeach;?>
                                          </table> 
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="vertical-align:top;">
                                           <table border="1" cellpadding="5">
                                            <tr>
                                              <td><b>Instructional Programs</b></td>
                                              <td> Q 1 </td>
                                              <td> Q 2 </td>
                                              <td> Q 3 </td>
                                              <td> Q 4 </td>
                                            </tr>
                                            <?php foreach($instProg as $instProgv):?>
                                            <tr>
                                              <td><?php echo $instProgv['summary'];?></td>
                                              <td>
                                                 <?php if($quater_no==1):?>
                                                <select name="instructional[<?php echo str_replace(' ','_',$instProgv['summary']);?>]" style="width:50px;">
                                                <?php foreach($reportGrades as $reportGrade):?>
                                                <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                              <td>
                                                 <?php if($quater_no==2):?>
                                              <select name="instructional[<?php echo str_replace(' ','_',$instProgv['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                              <td>
                                                 <?php if($quater_no==3):?>
                                              <select name="instructional[<?php echo str_replace(' ','_',$instProgv['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                              <td>
                                                 <?php if($quater_no==4):?>
                                              <select name="instructional[<?php echo str_replace(' ','_',$instProgv['summary']);?>]" style="width:50px;">
                                                  
                                              <?php foreach($reportGrades as $reportGrade):?>
                                                  <option value="<?php echo $reportGrade['grade'];?>"><?php echo $reportGrade['grade'];?></option>
                                                <?php endforeach;?>
                                              </select >
                                            <?php endif;?>
                                            </td>
                                            </tr>
                                            <?php endforeach;?>
                                          </table >
                                        </td>
                                      </tr>
                                    </table>
                                 </div>
                              <button type="submit" class="btn btn-medium btn-green" value="submit">Save</button>
                              <button type="button" class="btn btn-medium btn-red" id="cancel_btn" style="float:right;">Cancel</button>
                             </div>
                               </div> 

                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
 
       <script>
    
// $("#pills-tab2").bootstrapWizard("show",2)

$('#grade_id').change(function(){
	$.ajax({
		
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/grade_by_subject",
	  data: { grade_id: $('#grade_id').val() }
	})
	  .done(function( msg ) {
		  $('#subject_id').html('');
		  var result = jQuery.parseJSON( msg )
		  
		 $(result.subjects).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#subject_id').append('<option value="'+Element.subject_id+'">'+Element.subject_name+'</option>');
		});  
		  $("#subject_id").trigger("liszt:updated");
		  
	  });
	$.ajax({
		
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/grade_by_students",
	  data: { grade_id: $('#grade_id').val() }
	})
	  .done(function( msg ) {
		  $('#student').html('');
		  var result = jQuery.parseJSON( msg )
		  
		 $(result.students).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#student').append('<option value="'+Element.student_id+'">'+Element.firstname+' '+Element.lastname+'</option>');
		});  
		  $("#student").trigger("liszt:updated");
		  
	  });


  	
});

$('#period_id').on('change',function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>teacherself/getSubjectByLessonPlan/"+$(this).val()
    })
      .done(function( msg ) {
        if(msg!=''){
            $('#subject_name').html(' ');
            $('#subject_name_group').show();
            $('#subject_name').html(msg);
        } else {
            $('#subject_name').html(' ');
            $('#subject_name_group').show();
            $('#subject_name').html('Not Assigned');
        }
      });
  });
 <?php foreach($grade_subjects as $key=>$grade_subject):?>
$("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_1").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_1").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_2").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_2").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_3").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_3").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_4").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_4").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_5").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q1_comment_5").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_1").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_1").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_2").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_2").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_3").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_3").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_4").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_4").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_5").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q2_comment_5").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_1").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_1").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_1").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_1").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_1").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_2").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_2").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_3").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_3").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_4").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_4").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q3_comment_5").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });

   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_5").autocomplete("<?php echo base_url();?>classroom/getallcomments", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
  
   $("#<?php echo $grade_subject['grade_subject_id'];?>_q4_comment_1").result(function(event, data, formatted) {
       // $("#student_id").val(data[1]);
    });
   <?php endforeach;?>
   </script> 
 
</body>
<!-- END BODY -->
</html>