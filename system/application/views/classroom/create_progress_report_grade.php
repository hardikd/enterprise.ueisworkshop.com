<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/progress_report">Student Progress Report</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>classroom/create_progress_report">Create</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Create Report</h4>
                          
                         </div>
                         <div class="widget-body">
<?php /*?><form class="form-horizontal" method="post" action="<?php echo base_url().'classroom/create_progress';?>" name="parent_teacher_form" id="parent_teacher_form"><?php */?>
<form class="form-horizontal" method="post" action="<?php echo base_url().'classroom/grade_period_students';?>" name="student_entry_page" id="student_entry_page">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                 </ul>
                                 <div class="progress progress-success-yellow progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <div class="form-horizontal" >
                           <div class="control-group">
                         <label class="control-label">Select Period </label>
                                             <div class="controls">
                            	<select data-placeholder="--Please Select--" name="period_id" class="chzn-select span4" style="width: 300px;" tabindex="6" id="period_id">
                                        <option value=""></option>
										<?php foreach($periods as $value){?>
                                     <option value="<?php echo $value['period_id'];?>"><?php echo $value['start_time'].'-'.$value['end_time'];?></option>
                                            <?php }?>
                      					</select>
                                             </div>
                                         </div> 
                                        <div class="control-group" id="subject_name_group" style="display:none;">
                                  <label class="control-label">Select Subject</label>
                                       <div class="controls" id="subject_name">
                                           </div>
                                         </div>  
                                     </div>
                                     </div>
                                     
                               <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <div class="row-fluid">
                                         <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                        <a href="javascript:;" class="text-center" data-original-title="" id="first_quater">
                            <span class="value">
                                <i class="icon-upload"></i><br><br><br>
                             First Quater                     </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                                
                                </div>
                                <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                        <a href="javascript:;" class="text-center" data-original-title="" id="second_quater">
                            <span class="value">
                                <i class="icon-upload"></i><br><br><br>
                             Second Quater                     </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                                
                                </div>
                                <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                        <a href="javascript:;" class="text-center" data-original-title="" id="third_quater">
                            <span class="value">
                                <i class="icon-upload"></i><br><br><br>
                             Third Quater                     </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                                
                                </div>
                                <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                        <a href="javascript:;" class="text-center" data-original-title="" id="fourth_quater">
                            <span class="value">
                                <i class="icon-upload"></i><br><br><br>
                             Fourth Quater                     </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                                
                                </div>
                            </div>
                          </div>
                                     <input type="hidden" name="select_quater" id="select_quater" value="" >
                                     <br /><br />
                                     <ul class="pager wizard">
                                         <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                         <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                         <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                         <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                               </div> 
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });

       $('#first_quater').on('click',function(){
        console.log('test');
          $('#select_quater').val('first');
          $('#student_entry_page').submit();
       });
       $('#second_quater').on('click',function(){
          $('#select_quater').val('second');
          $('#student_entry_page').submit();
       });
       $('#third_quater').on('click',function(){
          $('#select_quater').val('third');
          $('#student_entry_page').submit();
       });
       $('#fourth_quater').on('click',function(){
          $('#select_quater').val('fourth');
          $('#student_entry_page').submit();
       });

   </script>  
 
       <script>
    
// $("#pills-tab2").bootstrapWizard("show",2)

$('#grade_id').change(function(){
	$.ajax({
		
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/grade_by_subject",
	  data: { grade_id: $('#grade_id').val() }
	})
	  .done(function( msg ) {
		  $('#subject_id').html('');
		  var result = jQuery.parseJSON( msg )
		  
		 $(result.subjects).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#subject_id').append('<option value="'+Element.subject_id+'">'+Element.subject_name+'</option>');
		});  
		  $("#subject_id").trigger("liszt:updated");
		  
	  });
	$.ajax({
		
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/grade_by_students",
	  data: { grade_id: $('#grade_id').val() }
	})
	  .done(function( msg ) {
		  $('#student').html('');
		  var result = jQuery.parseJSON( msg )
		  
		 $(result.students).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#student').append('<option value="'+Element.student_id+'">'+Element.firstname+' '+Element.lastname+'</option>');
		});  
		  $("#student").trigger("liszt:updated");
		  
	  });


  	
});

$('#period_id').on('change',function(){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>teacherself/getSubjectByLessonPlan/"+$(this).val()
    })
      .done(function( msg ) {
        if(msg!=''){
            $('#subject_name').html(' ');
            $('#subject_name_group').show();
            $('#subject_name').html(msg);
        } else {
            $('#subject_name').html(' ');
            $('#subject_name_group').show();
            $('#subject_name').html('Not Assigned');
        }
      });
  });

   </script> 
 
</body>
<!-- END BODY -->
</html>