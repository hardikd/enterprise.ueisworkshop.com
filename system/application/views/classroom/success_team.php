<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
  <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color">
   <link href="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen">
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.12/video.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
         <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
          <div id="sidebar" class="nav-collapse collapse">
        <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                      <a href="#jobaidesModal" data-toggle="modal" ><span>Job Aides</span>&nbsp;<i class="icon-folder-open"></i></a>
                      <a href="#myModal-video" data-toggle="modal" style="margin-right:5px;"><span>Video Tutorial</span>&nbsp;<i class="icon-play-circle"></i></a>
                   </h3>
           <div id="jobaidesModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="myModalLabel2"><i class="icon-wrench"></i> Job Aides</h3>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-success">OK</button>
            </div>
          </div>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/success_team">Student Success Team</a>
                            
                       </li>
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                    <div class="span4">
                             <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                        <a href="<?php echo base_url();?>classroom/create_success_report" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-plus"></i><br><br><br>
                              Create Report                          </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                     
                    <div class="span4">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                        <a href="<?php echo base_url();?>classroom/upload_success_report" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-upload"></i><br><br><br>
                              Upload Report                        </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                                
                                </div>
                                
                                 <div class="span4">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-yellow long-dash-half">
                     <a href="<?php echo base_url();?>classroom/sst_retrieve_report" class="text-center" data-original-title="">
                      <!--  <a href="<?php echo base_url();?>classroom/retrieve_success_report" class="text-center" data-original-title="">-->
                            <span class="value">
                                <i class="icon-download"></i><br><br><br>
                              Retrieve Report                        </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                 
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
      <!-- END PAGE -->
   </div>
   <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->
   <div id="myModal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:700px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Student Success Team</h3>
                                </div>
                              
                                <div class="modal-body">
                                        
                                    <video id="example_video_1" class="video-js vjs-default-skin"
  controls preload="auto" width="640" height="360"
 
  data-setup='{"example_option":true}'>
 <source src="<?php echo SITEURLM;?>convertedVideos/SST Video DemonstrationConverted.mp4" type='video/mp4' />
 
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
                                </div>
                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<!--<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>-->
                                </div>
                    
                            </div>
      
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   
     <script>
      	 $(document).ready( function() { 
        $('#success_team_form').submit();
		
      <!--  alert('<?php echo $this->session->flashdata('message');?>');-->
        
        <?php if ($this->session->flashdata('error')){?>
            $('#errmsg').html('<?php echo $this->session->flashdata('error');?>');
            $('#errorbtn').modal('show');
            <?php }?>
        <?php if ($this->session->flashdata('message')){?>
            $('#successbtn').modal('show');
            <?php }?>
		<?php if($this->session->flashdata('link')){?>
            window.open("<?php echo $this->session->flashdata('link');?>");
        <?php } else {?>
           jQuery('#myModal-video').on('hidden.bs.modal', function (e) {
        var myPlayer = videojs('example_video_1');
        myPlayer.pause();
      });
          $('#myModal-video').modal('show');
          var myPlayer = videojs('example_video_1');
        myPlayer.play();
        <?php } ?>	
      
    });

</script>
<script src="<?php echo SITEURLM?>js/jquery.easing.js" type="text/javascript"></script>
    <script src="<?php echo SITEURLM?>js/jqueryFileTree.js" type="text/javascript"></script>
    <link href="<?php echo SITEURLM?>css_new/jqueryFileTree.css" rel="stylesheet" type="text/css" media="screen" />
   <script>
   
   $(document).ready( function() {
    $('#jobaidesModal .modal-body').fileTree({
        root: '<?php echo $_SERVER['DOCUMENT_ROOT'];?>/Classroom Job Aides/CLASSROOM MANAGER/SST Manager/',
        script: '<?php echo base_url();?>index/foldertree/',
        expandSpeed: 1000,
        collapseSpeed: 1000,
        multiFolder: false
    }, function(file) {
//        alert(file);
        
        window.open(file);
    });
});
   </script>
   <script>
  
     
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>