<style type="text/css">
    table.gridtable { 
        font-size:9px;
        color:#333333;
        border-width: 1px;
        border-color: #666666;
        border-collapse: collapse;
    }
    table.gridtable b{ 
        font-size:11px;
    }
    table.gridtable th {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #dedede;
    }
    table.gridtable td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #ffffff;
        width:46.2px;}
    .blue{
        font-size:9px;
    }

</style>


<page backtop="2mm" backbottom="14mm" backleft="1mm" backright="10mm"> 
    <page_header> 
        <div style="padding-left:610px;position:relative;top:-50px;">
            <img alt="Logo"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
            <div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:7px;">
                <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/calendar.png"/>&nbsp;Student Success Team</b></div>
        </div>
    </page_header> 
    <page_footer> 
        <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:1045px;font-size:7px;color:#<?php echo $fontcolor; ?>;"><?php echo $dis; ?></div>
        <br />
        <table style="margin-left:10px;">
            <tr>
                <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:495px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                </td>
                <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:450px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Page [[page_cu]] of [[page_nb]] 
                </td>
                <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Printed on: <?php echo date('F d,Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer> 


    <table cellspacing="0" style="width:1050px;border:2px #939393 solid;">
        <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
            <td style="padding-left:5px;padding-top:2px;height:36px;" >
                <b> <?php echo ucfirst($this->session->userdata('district_name')); ?> | <?php if ($this->session->userdata('login_type') == 'teacher'): ?>
                        <?php echo ucfirst($this->session->userdata('school_self_name')); ?>
                    <?php else: ?>
                        <?php echo ucfirst($this->session->userdata('school_name')); ?>
                    <?php endif; ?></b></td>
            <td style="text-align:right;padding-right:10px;">
                <b>Meeting Date:</b> <?php echo date('F d, Y', strtotime($success_data['details']->date)); ?>
            </td>
        </tr>
        
        <tr style="margin-top:20px;">
            <td style="padding-left:10px;width:550px;color:#939393; vertical-align: top;">
                <br />
                <b>Type of Meeting:</b> <?php echo $success_data['meeting_type'][0]->progress_monitoring; ?>   
                <br />
                <br />
                <b>Student:</b> <?php echo $studentname; ?>
                <br />
                <br />
                <b>Reason for Referral:</b> <?php echo $success_data['referral_reason'][0]->referral_reason; ?> 
                <br />
                <br />
                <b>Grade:</b> <?php echo $success_data['details']->grade_name; ?>
                <br />
                <br />
                <?php if ($teachername[0]['firstname']!=''):?>
                <b>Teacher:</b> <?php echo $teachername[0]['firstname'] . ' ' . $teachername[0]['firstname']; ?>   
                <br />
                <br />
            <?php else:?>
                <b>Observer:</b> <?php echo $this->session->userdata('observer_name'); ?>   
                <br />
                <br />
            <?php endif;?>
                <b>Program Change:</b> <?php echo $success_data['program_change']; ?> 
                <br />
                <br />
                <b>Next Meeting Date:</b> <?php echo date('F d, Y', strtotime($success_data['details']->next_date)); ?>
                <br />
                
            </td>

            <td style="padding-left:10px;width:475px;color:#939393;height:130px;">
                <br />
                <table style="border:1px solid #939393;height:130px;width:340px;">
                    <tr>
                        <td style="width:440px;height:165px;vertical-align: top;">
                            <b>Description:</b> <?php echo $des;?>
                        </td>
                    </tr>
                </table>
            <br />
            </td>
            

        </tr>

        
    </table>
    <br />
    <!--Home start here-->
    <table class="gridtable" style="width:750px;" >

        <tr >
            <td style="text-align: center;width:150px;">
                <b>Student Strengths</b> 
            </td>
            <td style="text-align: center;width:150px;">
                <b>Background Information</b> 
            </td>
            <td style="text-align: center;width:500px;">
                <b>Challenges & Next Steps</b> 
            </td>
        </tr >
       
        <tr >
            <td style="text-align: center;" valign="top">
                
                             <table class="gridtable" style="border:none;width:225px;text-align:left;">
                                <tr>
                                    <td style="width:100%;">
                                        <b>Strenghts At Home</b>
                                    </td>
                                </tr>
                                <?php 
                                $strenghtParent = explode('|', unserialize($success_data['details']->strengths_parent));
                                $strhmcntr = 0;
                                foreach($strenghtParent as $strenghthome){
                                ?>
                                <tr>
                                    <td style="width:100%;height:3px;"><?php echo $strenghthome;?></td>
                                </tr>
                                <?php $strhmcntr++;
                                }?>
                                <?php if($strhmcntr<6){ ?>
                                    <?php for($i=$strhmcntr;$i<=6;$i++){?>
                                        <tr>
                                    <td style="width:100%;">&nbsp;</td>
                                </tr>
                                    <?php }?>
                                <?php }?>

                            </table>
                            
                        
            </td>
           <td valign="top">
           <table class="gridtable" style="border:none;width:225px;text-align:left;">
                                <tr>
                                    <td style="width:100%;">
                                        <b>Home Information</b>
                                    </td>
                                </tr>
                                <?php 
                                $infFamily = explode('|', unserialize($success_data['details']->information_family));
                                $infmcntr = 0;
                                foreach($infFamily as $backgroundHome){
                                ?>
                                <tr>
                                    <td style="width:100%;height:2px;"><?php echo $backgroundHome;?></td>
                                </tr>

                                <?php
                                $infmcntr++;
                                 }?>
                                <?php if($infmcntr<6){ ?>
                                    <?php for($i=$infmcntr;$i<=6;$i++){?>
                                        <tr>
                                    <td style="width:100%;">&nbsp;</td>
                                </tr>
                                    <?php }?>
                                <?php }?>
                            </table>
           </td>
           <td valign="top">
            <table class="gridtable" style="width:585px;" >
                <tr>
                    <td style="text-align: center;width:35%;">
                        <b>Home Challenges</b>
                    </td>
                    <td style="text-align: center;width:35%;">
                        <b>Next Steps</b>
                    </td>
                    <td style="text-align: center;width:12%;">
                        <b>Who</b>
                    </td>
                    <td style="text-align: center;width:15%;">
                        <b>When</b>
                    </td>
                </tr>
                <?php $actcntr = 0;?>
                <?php foreach ($success_data['action_home'] as $key => $action_home_need): ?>
            <tr>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;width:35%;height:15px;" valign="center"><?php echo trim($action_home_need['action_need_home']); ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;width:35%;height:15px;" valign="center"><?php echo trim($action_home_need['action_action_home']); ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;text-align:center;width:12%;height:15px;" valign="center"><?php echo trim($action_home_need['action_responsible_home']); ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;text-align:center;width:15%;height:15px;" valign="center"><?php echo trim($action_home_need['action_date_home']); ?></td>         
            </tr>
            <?php $actcntr++;?>
    <?php endforeach; ?>
        <?php if($actcntr<6){?>
            <?php for($i=$actcntr;$i<=6;$i++){?>
                <tr>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;width:35%;height:10px;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;width:35%;height:10px;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;text-align:center;width:12%;height:10px;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;text-align:center;width:15%;height:10px;" valign="center">&nbsp;</td>         
            </tr>
            <?php }?>
        <?php }?>
            </table>
           </td>

        </tr >
    
    </table>

    <!-- Home ends here -->
    <page>
         <page_header> 
        <div style="padding-left:610px;position:relative;top:-50px;">
            <img alt="Logo"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
            <div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:7px;">
                <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/calendar.png"/>&nbsp;Student Success Team</b></div>
        </div>
    </page_header> 
    <page_footer> 
        <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:1045px;font-size:7px;color:#<?php echo $fontcolor; ?>;"><?php echo $dis; ?></div>
        <br />
        <table style="margin-left:10px;">
            <tr>
                <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:495px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                </td>
                <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:450px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Page [[page_cu]] of [[page_nb]] 
                </td>
                <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Printed on: <?php echo date('F d,Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer> 
    <!-- Schhol Starts here -->
        <table class="gridtable" style="width:750px;" >

        <tr >
            <td style="text-align: center;width:150px;">
                <b>Student Strengths</b> 
            </td>
            <td style="text-align: center;width:150px;">
                <b>Background Information</b> 
            </td>
            <td style="text-align: center;width:500px;">
                <b>Challenges & Next Steps</b> 
            </td>
        </tr >
       
        <tr >
            <td style="text-align: center;" valign="top">
                
                             <table class="gridtable" style="border:none;width:225px;text-align:left;">
                                <tr>
                                    <td style="width:100%;">
                                        <b>Strenghts At School</b>
                                    </td>
                                </tr>
                                <?php 

                                $strenghtTeacher = explode('|', unserialize($success_data['details']->strengths_teacher));
                                $stteachcntr = 0;
                                foreach($strenghtTeacher as $strenghtschool){
                                ?>
                                <tr>
                                    <td style="width:100%;height:3px;"><?php echo $strenghtschool;?></td>
                                </tr>
                                <?php $stteachcntr++;?>
                                <?php }?>
                                <?php if($stteachcntr < 6){?>
                                <?php for($i=$stteachcntr;$i<=6;$i++){?>
                                    <tr>
                                    <td style="width:100%;">&nbsp;</td>
                                </tr>
                                <?php }?>
                                <?php }?>
                            </table>
                            
                        
            </td>
           <td valign="top">
           <table class="gridtable" style="border:none;width:225px;text-align:left;">
                                <tr>
                                    <td style="width:100%;">
                                        <b>School Information</b>
                                    </td>
                                </tr>
                                <?php 
                                $infSchool = explode('|', unserialize($success_data['details']->information_school));
                                $inschcntr = 0;
                                foreach($infSchool as $backgroundSchool){
                                ?>
                                <tr>
                                    <td style="width:100%;height:3px;"><?php echo $backgroundSchool;?> </td>
                                </tr>
                                <?php $inschcntr++; }?>
                                <?php if($inschcntr < 6){?>
                                    <?php for($i=$inschcntr;$i<=6;$i++){?>
                                    <tr>
                                    <td style="width:100%;">&nbsp;</td>
                                </tr>
                                <?php }?>
                                <?php }?>
                            </table>
           </td>
           <td valign="top">
            <table class="gridtable" style="width:575px;" >
                <tr>
                    <td style="text-align: center;width:35%;">
                        <b>School Challenges</b>
                    </td>
                    <td style="text-align: center;width:35%;">
                        <b>Next Steps</b>
                    </td>
                    <td style="text-align: center;width:12%;">
                        <b>Who</b>
                    </td>
                    <td style="text-align: center;width:15%;">
                        <b>When</b>
                    </td>
                </tr>
                <?php $actschcntr = 0;?>
                <?php foreach ($success_data['action_school'] as $key => $action_school_need): ?>
            <tr>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:15px;width:35%;" valign="center"><?php echo $action_school_need['action_need_school']; ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:15px;width:35%;" valign="center"><?php echo $action_school_need['action_action_school']; ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:15px;text-align:center;width:12%;" valign="center"><?php echo $action_school_need['action_responsible_school']; ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:15px;text-align:center;width:15%;" valign="center"><?php echo $action_school_need['action_date_school']; ?></td>         
            </tr>
            <?php $actschcntr++;?>
    <?php endforeach; ?>
    <?php if($actschcntr<6){?>
        <?php for($i=$actschcntr;$i<=6;$i++){?>
            <tr>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;width:35%;height:10px;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;width:35%;height:10px;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;text-align:center;width:12%;height:10px;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;text-align:center;width:15%;height:10px;" valign="center">&nbsp;</td>         
            </tr>
        <?php }?>
    <?php }?>
            </table>
           </td>

        </tr >
    
    </table>
    <!-- School Ends here -->
    <br /> <br />
    <!-- Medical Starts here -->
    <table class="gridtable" style="width:750px;" >

        <tr >
            <td style="text-align: center;width:150px;">
                <b>Additional Notes</b> 
            </td>
            <td style="text-align: center;width:150px;">
                <b>Background Information</b> 
            </td>
            <td style="text-align: center;width:500px;">
                <b>Challenges & Next Steps</b> 
            </td>
        </tr >
       
        <tr >
            <td style="text-align: center;width:205;" valign="top">
                
                             
                        
            </td>
           <td valign="top">
           <table class="gridtable" style="border:none;width:225px;text-align:left;">
                                <tr>
                                    <td style="width:100%;">
                                        <b>Medical Information</b>
                                    </td>
                                </tr>
                                <?php 
                                $infHealth = explode('|', unserialize($success_data['details']->information_health));
                                $inhealcntr = 0;
                                foreach($infHealth as $backgroundHealth){
                                ?>
                                <tr>
                                    <td style="width:100%;height:1px;">
                                        <?php echo $backgroundHealth;?>      
                                    </td>
                                </tr>
                                <?php $inhealcntr++;?>
                                <?php }?>
                                <?php if($inhealcntr < 6){?>
                                <?php for($i=$inhealcntr;$i<=6;$i++){?>
                                <tr>
                                    <td style="width:100%;">
                                        &nbsp;     
                                    </td>
                                </tr>
                                <?php }?>
                                <?php }?>
                            </table>
           </td>
           <td valign="top">
            <table class="gridtable" style="width:575px;" >
                <tr>
                    <td style="text-align: center;width:35%;">
                        <b>Medical Challenges</b>
                    </td>
                    <td style="text-align: center;width:35%;">
                        <b>Next Steps</b>
                    </td>
                    <td style="text-align: center;width:12%;">
                        <b>Who</b>
                    </td>
                    <td style="text-align: center;width:15%;">
                        <b>When</b>
                    </td>
                </tr>
                <?php $actmedcntr = 0;?>
                <?php foreach ($success_data['action_medical'] as $key => $action_medical_need): ?>
            <tr>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:15px;width:35%;" valign="center"><?php echo $action_medical_need['action_need_medical']; ?></td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:15px;width:35%;" valign="center"><?php echo $action_medical_need['action_action_medical']; ?></td>
                <td style="font-size:9px;text-align:center;padding:8px 0px 0px 2px !important;height:15px;width:12%;" valign="center"><?php echo $action_medical_need['action_responsible_medical']; ?></td>
                <td style="font-size:9px;text-align:center;padding:8px 0px 0px 2px !important;height:15px;width:15%;" valign="center"><?php echo $action_medical_need['action_date_medical']; ?></td>         
            </tr>
            <?php $actmedcntr++;?>
    <?php endforeach; ?>
    <?php if($actmedcntr<6){
        for($i=$actmedcntr;$i<=6;$i++){?>
            <tr>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:10px;width:35%;" valign="center">&nbsp;</td>
                <td style="font-size:9px;padding:8px 0px 0px 2px !important;height:10px;width:35%;" valign="center">&nbsp;</td>
                <td style="font-size:9px;text-align:center;padding:8px 0px 0px 2px !important;height:10px;width:12%;" valign="center">&nbsp;</td>
                <td style="font-size:9px;text-align:center;padding:8px 0px 0px 2px !important;height:10px;width:15%;" valign="center">&nbsp;</td>         
            </tr>
    <?php }
    }?>
            </table>
           </td>

        </tr >
    
    </table>
</page>
<br />
    <br />
    <page>
            <page_header> 
        <div style="padding-left:610px;position:relative;top:-50px;">
            <img alt="Logo"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
            <div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:7px;">
                <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/calendar.png"/>&nbsp;Student Success Team</b></div>
        </div>
    </page_header> 
    <page_footer> 
        <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:1045px;font-size:7px;color:#<?php echo $fontcolor; ?>;"><?php echo $dis; ?></div>
        <br />
        <table style="margin-left:10px;">
            <tr>
                <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:495px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                </td>
                <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:450px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Page [[page_cu]] of [[page_nb]] 
                </td>
                <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Printed on: <?php echo date('F d,Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer> 
    <table class="gridtable" style="width:1050px;">

        <tr >
            <td colspan="3" style="width:1050px;text-align: center;">
                <b>MEETING ATTENDEES</b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>&nbsp;</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>&nbsp;</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>&nbsp;</b>
            </td>
        </tr>

        <tr>

            <td style="width:200px;font-size:9px;vertical-align: top;">
                Guardian:
            </td>
            <td style="width:200px;font-size:9px;vertical-align: top;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;vertical-align: top;">
                School Rep:
            </td>         
        </tr>

        <tr>

            <td style="width:200px;font-size:9px;">
                Guardian:
            </td>
            <td style="width:200px;font-size:9px;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;">
                Health Care Rep:
            </td>         
        </tr>
        <tr>

            <td style="width:200px;font-size:9px;">
               Administrator:
            </td>
            <td style="width:200px;font-size:9px;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;">
                Health Care Rep:
            </td>         
        </tr>
        <tr>

            <td style="width:200px;font-size:9px;">
                Administrator:
            </td>
            <td style="width:200px;font-size:9px;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;">
                Other:
            </td>         
        </tr>

    </table>
</page>
</page>