﻿<table style="width:850px; text-align:center" align="center">
    <tr>
        <td align="center"><h1>Student Progress Report</h1></td>
    </tr>
</table>
<br />

<table style="width:850px;border:2px #7FA54E solid;">
		<tr>
		<td style="width:180px;color:#CCCCCC;">
        <?php
        if($this->session->userdata('login_type')=='teacher'){?>
           <b>Teacher:</b><?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?>   
           
           <?php } else if($this->session->userdata('login_type')=='observer'){?>
                 <b>Observer Name:</b><?php echo ($teachername[0]['observer_name']); ?>   
           <?php } else if($this->session->userdata('login_type')=='user'){?>
                 <b>User Name:</b><?php echo $this->session->userdata('username');?>
                 <?php }?>
		
		</td>
		<td style="width:205px;color:#CCCCCC;">
		<b>Student Name:</b><?php echo ($progress_report[0]->firstname . ' ' . $progress_report[0]->lastname . ''); ?>
		</td>
		<td style="width:185px;color:#CCCCCC;">
		<b>Subject:</b><?php echo ($progress_report[0]->subject_name); ?>
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Grade:</b><?php echo ($progress_report[0]->grade_name); ?>
		</td>
		</tr>
		<tr>
		<td>
		<br />
		</td>
		</tr>
		<tr>
		<td style="width:180px;color:#CCCCCC;">
		<b>Period:</b><?php echo ($progress_report[0]->start_time); ?> TO <?php echo ($progress_report[0]->end_time); ?> 
		</td>
		<td style="width:205px;color:#CCCCCC;">
		<b>School Name:</b><?php echo ($progress_report[0]->school_name); ?>
		</td>
		<td style="width:185px;color:#CCCCCC;">
		<b>District:</b><?php echo ($progress_report[0]->districts_name); ?>
		</td>
		</tr>
        </table>
         <table>
                <tr><th style="width:250px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                 subject area and state standards
                        </div>
                    </th>
                    <th style="width:120px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                        Q1
                        </div>
                    </th>
                    <th style="width:120px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                       Q2
                        </div>
                    </th>
                     <th style="width:115px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                       Q3
                        </div>
                    </th>
                     <th style="width:115px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                       Q4
                        </div>
                    </th>
        
        </tr>
     <?php
		foreach($subject_list as $values) 
	{?>
         <tr>
         <th style="width:250px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                 <?php  echo ucfirst($values['subject_name']);?>
                        </div>
                    </th>
                    
                    <th style="width:120px;">
                    
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                          <?php
					foreach ($master_data[$values['subject_id']] as $value) {?>
					<?php echo ucfirst($value->score);?>
                        <?php }?>
					</div>
                    
                    </th>
                    <th style="width:120px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                       B
                        </div>
                    </th>
                     <th style="width:115px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                       C
                        </div>
                    </th>
                     <th style="width:115px;">
                        <div  style=" font-size:13px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:5px;color:#7FA54E;">
                       D
                        </div>
                    </th>
                </tr>           
                    
                    
         
               <tr>
                    <td style="width:250px;">
                    <?php
					foreach ($master_data[$values['subject_id']] as $value) {?>
					<div style="float:left;width:250px;">
					<?php echo ucfirst($value->standard_name);?>
					</div>
                   <?php }?>
                    </td>
				    <td style="width:120px;">
                    <?php 
   foreach ($master_data[$values['subject_id']] as $value) {?>
					<div style="float:left;width:120px;">
					<?php echo ucfirst($value->standard_language);?>
					</div>
                   <?php }?>
                    </td>
                    <td style="width:120px;">
                      
                    </td>
                    <td style="width:115px;">
                      
                    </td>
                    <td style="width:115px;">
                      
                    </td>
          
                </tr>
               <?php } ?>
         </table>

