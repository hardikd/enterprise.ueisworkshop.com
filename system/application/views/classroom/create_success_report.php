<?php error_reporting(0); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>UEIS Workshop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

        <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />





        <style type="text/css">
            #resp {
                float: left;
                font-family: 'MyriadPro-Regular';
                font-size: 15px;
                font-weight: normal;
                line-height: 12px;
                margin: 0 0 0 280px;
                padding: 20px 11px 10px 15px;
                color: #FFFFFF;
            }
		
            .button_next {
                background-color: #FFB400;
                border: 0 none;
                color: #FFFFFF;
                cursor: default;
                float: right;
                margin-left: 5px;
                border-radius: 15px;
                display: inline-block;
                padding: 5px 14px;
                text-shadow: none !important;
                border-radius: 0 !important;
                outline: 0 none;
                text-decoration: none;
                line-height: 20px;
                list-style: none outside none;
                text-align: center;
            }


        </style>
        <style type="text/css">
.ac_results {
	padding: 0px;
	border: 1px solid black;
	background-color:#FFF;
	overflow: hidden;
	z-index: 99999;
}

.ac_results ul {
	width: 100%;
	list-style-position: outside;
	list-style: none;
	padding: 0;
	margin: 0;
}

.ac_results li {
	margin: 0px;
	padding: 2px 5px;
	cursor: default;
	display: block;
	/* 
	if width will be 100% horizontal scrollbar will apear 
	when scroll mode will be used
	*/
	/*width: 100%;*/
	font: menu;
	font-size: 12px;
	/* 
	it is very important, if line-height not setted or setted 
	in relative units scroll will be broken in firefox
	*/
	line-height: 16px;
	overflow: hidden;
}

.ac_loading {
	background: white url('../indicator.gif') right center no-repeat;
}

.ac_odd {
	background-color: #eee;
}

.ac_over {
	background-color: #ffb400;
	color: white;
}

</style>  

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php require_once($view_path . 'inc/header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar-scroll">
                <div id="sidebar" class="nav-collapse collapse">


                    <!-- BEGIN SIDEBAR MENU -->
                    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <h3 class="page-title">
                                <i class="icon-group"></i>&nbsp; Classroom Management
                            </h3>
                            <ul class="breadcrumb" >

                                <li>
                                    UEIS Workshop
                                    <span class="divider">&nbsp; | &nbsp;</span>
                                </li>

                                <li>
                                    <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>classroom">Classroom Management</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>classroom/success_team">Student Success Team</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    </i> <a href="<?php echo base_url(); ?>classroom/create_success_report">Create Report</a>

                                </li>




                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN BLANK PAGE PORTLET-->
                            <div class="widget yellow">
                                <div class="widget-title">
                                    <h4>Create Report</h4>
                                </div>
                                <div class="widget-body">
                                    <form method="post" action="<?php echo base_url() . 'classroom/strengths_create_insert'; ?>" name="success_team_form" id="success_team_form">
                                        <div id="pills" class="custom-wizard-pills-yellow3">
                                            <ul>
                                                <li><a href="#pills-tab1" data-toggle="tab">Strengths</a></li>
                                                <li><a href="#pills-tab2" data-toggle="tab">Background Information</a></li>
                                                <li><a href="#pills-tab3" data-toggle="tab">Needs</a></li>
                                                <li><a href="#pills-tab4" data-toggle="tab">Actions</a></li>


                                            </ul>
                                            <div class="progress progress-success-yellow progress-striped active">
                                                <div class="bar"></div>
                                            </div>
                                            <div class="tab-content">

                                                <!-- BEGIN STEP 1-->
                                                <div class="tab-pane" id="pills-tab1">

                                                    <div class="form-horizontal">

                                                        <div class="control-group">
                                                            <div class="span6">
                                                            <label class="control-label">Select Date</label>
                                                            <div class="controls">
                                                                <input id="dp1" name="date" type="text" value="" size="16" class="required m-ctrl-medium" style="width:300px;"/>
                                                            </div>
                                                        </div>
                                                        <div class="span6">
                                                            <label class="control-label">Type of Meeting</label>
                                                                <div class="controls">
                                                                    <select class="span12 chzn-select" name="meeting_type" id="meeting_type" tabindex="1" style="width: 300px;" >
                                                                        <option value=""  selected="selected">Please Select</option>
                                                                        <?php  foreach($meeting_type as $meeting_details)
                                                                        {
                                                                        ?>
                                                                         <option value="<?php echo $meeting_details['id']; ?>"><?php echo $meeting_details['meeting_type']; ?></option>
                                                                      <?php } ?>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    
                                                    
  <?php if($this->session->userdata("login_type")=='user'){ ?>                     
                                        
                                         
                                             <div class="control-group">
                                   <?php if(isset($school_type)) $countst = count($school_type);
								if(isset($school_type) && $countst >0){ ?>
                                             <label class="control-label">Select School Type :</label>
                                             <div class="controls">
<select class="span12 chzn-select" name="schools_type" id="schools_type" tabindex="1" style="width: 300px;" onchange="updateschool(this.value);">
      									<option value=""  selected="selected">Please Select</optgroup>
									       <?php  foreach($school_type as $k => $v)
	  										{
											?>
								           <option value="<?php echo $v['school_type_id']; ?>"><?php echo $v['tab']; ?></option>
						            	  <?php } ?>
                        			    </select>
                                             </div>
                                         </div> 
                                         
                                          <div class="control-group">
                                             <label class="control-label">Schools :</label>
                                             <div class="controls">
				<select class="span12 chzn-select" name="school_id" id="schools" tabindex="1" style="width: 300px;" onchange="updateclass(this.value)">
                 	<option value=""  selected="selected">Please Select</optgroup>
              						</select>
                                             </div>
                                         </div> 
                                         
                                      <script type="application/javascript"> 
		   var schooltype = document.getElementById('schools_type').options[document.getElementById('schools_type').selectedIndex].value;
		   updateschool(schooltype) </script>
      
        <?php }?>      
                                        
                                        <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
		 <select class="span12 chzn-select" name="grade_id" id="grades" onchange="updateclassroom(this.value)" tabindex="1" style="width: 300px;">
             
                   <option value=""  selected="selected">Please Select</option>
                    <?php
					if(isset($school_grade)) $countst = count($school_grade["grades"]);
					if(isset($school_grade) && $countst >0){
						
					foreach($school_grade["grades"] as $k=> $v)
					{
					?>
					 <option value="<?php echo $v["dist_grade_id"];?>"> <?php echo $v["grade_name"]; ?></option>
					<?php } }?>
            	   </select>
                  </div>

                  <?php if(isset($school_grade) && $countst >0){?>
			         <input type="hidden" name="schools" id="schools" value="<?php echo $school_grade["school_id"][0]["school_id"];?>" />
         		<?php }?>
                </div>  
                                         
                                               <div class="control-group">
                        <label class="control-label">Teachers :</label>
                           <div class="controls">
                   		 <select class="combobox span12 chzn-select" name="teacher_id" style="width: 300px;" id="classrooms" >
               <option value=""  selected="selected">Please Select</optgroup>
          </select>
                         </div>
                      </div> 
                      
                      
                      
                           <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select name="student_id" id="students" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <option value='all'>All</option>


		      </select> 
              </div>
      </div>
                                           
                       <?php } elseif($this->session->userdata("login_type")=='observer' || $this->session->userdata("login_type")=='teacher') {?>
                      
                                                     
                                                     
                                                        <div class="control-group">
                                                            <div class="span6">
                                                            <label class="control-label">Select Grade</label>
                                                            <div class="controls">
                                                                <select class="span12 chzn-select" name="grade_id" id="grade_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    if (!empty($grades)) {
                                                                        foreach ($grades as $val) {
                                                                            ?>
                                                                            <option value="<?php echo $val['grade_id']; ?>" ><?php echo $val['grade_name']; ?>  </option>
                                                                        <?php }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            </div>
                                                            <div class="span6">
                                                            <label class="control-label">Reason for Referral</label>
                                                                <div class="controls">
                                                                    <select class="span12 chzn-select" name="referral_reason" id="referral_reason" tabindex="1" style="width: 300px;" >
                                                                        <option value=""  selected="selected">Please Select</option>
                                                                        <?php  foreach($referral_reasons as $referral_reason)
                                                                        {
                                                                        ?>
                                                                         <option value="<?php echo $referral_reason['id']; ?>"><?php echo $referral_reason['referral_reason']; ?></option>
                                                                      <?php } ?>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        </div>
                                                       <?php  if($this->session->userdata("login_type")=='teacher'){?> 
                                                         <div class="control-group">
                                                            <div class="span6">
                                                            <label class="control-label">Select Student</label>
                                                            <div class="controls">
                                                                <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:315px;"  />
                                                                    <input type="hidden" name="student_id" id="student_id"  />
                                                            </div>
                                                            </div>
                                                            <div class="span6">
                                                            <label class="control-label">Type of Progress Monitoring</label>
                                                                <div class="controls">
                                                                    <select class="span12 chzn-select" name="progress_monitoring" id="progress_monitoring" tabindex="1" style="width: 300px;" >
                                                                        <option value=""  selected="selected">Please Select</option>
                                                                        <?php  foreach($progress_monitorings as $progressMonitoring)
                                                                        {
                                                                        ?>
                                                                         <option value="<?php echo $progressMonitoring['id']; ?>"><?php echo $progressMonitoring['progress_monitoring']; ?></option>
                                                                      <?php } ?>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        </div>  
                                                         
                      <?php }}?>

                        <?php if($this->session->userdata("login_type")=='observer'){ ?>                        
							<div class="control-group">
                                <div class="span6">
                                <label class="control-label">Select Student</label>
                                <div class="controls">
                                    <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;"  />
									<input type="hidden" name="student_id" id="student_id"  />
                                </div>
                                </div>
                                <div class="span6">
                                    <label class="control-label">Type of Progress Monitoring</label>
                                        <div class="controls">
                                            <select class="span12 chzn-select" name="progress_monitoring" id="progress_monitoring" tabindex="1" style="width: 300px;" >
                                                <option value=""  selected="selected">Please Select</option>
                                                <?php  foreach($progress_monitorings as $progress_monitoring)
                                                    {
                                                    ?>
                                                     <option value="<?php echo $progress_monitoring['id']; ?>"><?php echo $progress_monitoring['progress_monitoring']; ?></option>
                                                  <?php } ?>
                                            </select>
                                    </div>
                                </div>
                            </div>  
                         <?php }?>        

                                                        <h3 style="color:#000000;">Strengths</h3>

                                                        <div class="widget yellow">





                                                            <div class="widget-title">
                                                                <h4>Parent</h4>

                                                            </div>
                                                            <div class="widget-body">
                                                                <div class="form-horizontal">
                                                                    <div id="pills" class="custom-wizard-pills-yellow3">

                                                                        <div class="control-group" >
                                                                            <label class="control-label"></label>
                                                                            <div class="controls" id="strengths">
 <textarea id="parent" size="16" name="strengths_parent[]" class="m-ctrl-medium sst_class" placeholder="Input Value"  ></textarea>			 					&nbsp;<a href="#" id="addNew"><i class="icon-plus"></i></a>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- END BLANK PAGE PORTLET-->
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div class="widget yellow">
                                                            <div class="widget-title">
                                                                <h4>Teacher
                                                                </h4>

                                                            </div>
                                                            <div class="widget-body">
                                                                <div class="form-horizontal">
                                                                    <div id="pills" class="custom-wizard-pills-yellow3">

                                                                        <div class="control-group" >
                                                                            <label class="control-label"></label>
                                                                            <div class="controls" id="strengths_text">
<textarea id="teacher" size="16" name="strengths_teacher[]" class="m-ctrl-medium sst_class" placeholder="Input Value"  ></textarea> 
                                                                                &nbsp;<a href="#" id="addNew_text"><i class="icon-plus"></i></a>

                                                                            </div>
                                                                        </div>


                                                                    </div>





                                                                    <!-- END BLANK PAGE PORTLET-->
                                                                </div>

                                                            </div>

                                                        </div>



                                                    </div>
                                                    <br /><br /><br />
                                                    <!--                   <ul class="pager wizard">
                                                                                             <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                                                                             <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                                                                             <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                                                                             <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                                                                         </ul>-->
                                                </div>

                                                <!-- BEGIN STEP 2-->
                                                <div class="tab-pane" id="pills-tab2">
                                                    <h3 style="color:#000000;">Background Information</h3>

                                                    <div class="widget yellow">
                                                        <div class="widget-title">
                                                            <h4>Family

                                                            </h4>

                                                        </div>
                                                        <div class="widget-body">
                                                            <div class="form-horizontal">
                                                                <div id="pills" class="custom-wizard-pills-yellow3">

                                                                    <div class="control-group">
                                                                        <label class="control-label" ></label>
                                                                        <div class="controls" id="background">
<textarea id="family" size="16" name="information_family[]" class="m-ctrl-medium sst_class" placeholder="Input Value"  ></textarea>
                                                                            &nbsp;<a href="#" id="backgroundNew"><i class="icon-plus"></i></a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <!-- END BLANK PAGE PORTLET-->
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="widget yellow">
                                                        <div class="widget-title">
                                                            <h4>School

                                                            </h4>

                                                        </div>
                                                        <div class="widget-body">
                                                            <div class="form-horizontal">
                                                                <div id="pills" class="custom-wizard-pills-yellow3">

                                                                    <div class="control-group">
                                                                        <label class="control-label"></label>
                                                                        <div class="controls" id="background_text">
<textarea id="school" size="16" name="information_school[]" value="" class="m-ctrl-medium sst_class" placeholder="Input Value"  ></textarea>
                                                                            &nbsp;<a href="#" id="backgroundNew_text"><i class="icon-plus"></i></a>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                                <!-- END BLANK PAGE PORTLET-->
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="widget yellow">
                                                        <div class="widget-title">
                                                            <h4>Health

                                                            </h4>

                                                        </div>
                                                        <div class="widget-body">
                                                            <div class="form-horizontal">
                                                                <div id="pills" class="custom-wizard-pills-yellow3">

                                                                    <div class="control-group">
                                                                        <label class="control-label"></label>
                                                                        <div class="controls" id="health_text">
<textarea id="health" size="16" name="information_health[]" value="" class="m-ctrl-medium sst_class" placeholder="Input Value"  ></textarea>
			&nbsp;<a href="#" id="healthNew_text"><i class="icon-plus"></i></a>
                                                                        </div>
                                                                    </div>




                                                                </div>
                                                                <!-- END BLANK PAGE PORTLET-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br /><br /><br />
                                                    <!--                   <ul class="pager wizard">
                                                                                             <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                                                                             <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                                                                             <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                                                                             <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                                                                         </ul>       -->

                                                </div>



                                                <!-- BEGIN STEP 3-->

                                                <div class="tab-pane" id="pills-tab3">
                                                    <h3 style="color:#000000">Needs</h3>
                                                    <div class="control-group">

                                   				 <div class="widget yellow">
                                                                <div class="widget-title">
                                                                    <h4>Home </h4>
<!--                                                                    <div id="resp" style="margin:0 0 0 20px;"> Needs</div>
                                                                    <div id="resp" style="margin:0 0 0 150px;"> Action</div>
                                                                    <div id="resp"> Responsible person</div>-->
                                                                </div>
                                                                <div class="widget-body">
                                                                    <div class="form-horizontal">
                                                                        <div id="pills" class="custom-wizard-pills-yellow3">
                                                                            <div class="control-group">
                                                                                <div class="controls" id="needs" style="margin:0;">
                                                                       <div class="span6">
                                                                       <div style="margin:0 0 0 5px;"> <b>Home</b></div>
                                    <select name="needs_home[]" id="needs_home" data-placeholder="--Please Select--" tabindex="1" style="width: 450px;" onChange="homelist(1,$(this).attr('id'),'objective1')">
											<option value=''></option>
												<?php
                                                	if (!empty($strategies_home)) {
															foreach ($strategies_home as $strategies_value) {
                                                 ?>
                             <option value="<?php echo $strategies_value->id; ?>"><?php echo $strategies_value->intervention_strategies_home; ?></option>
    								<?php }} ?>
                                        </select>
                                                </div>
                                                
                                <div id="objective1" style="display:none;"></div> <br /> 
                             &nbsp;<a href="#" id="needsNew"><i class="icon-plus"></i></a>   
                                                                                
           </div>
           </div>

           <div class="control-group">
                <div class="controls" id="needs_home_text" style="margin:0;">
                   <div class="span6">
                       <div style="margin:0 0 0 5px;"> <b>Home</b></div>
                       <input type="text" name="needs_home[]" value="" id="needs_home_text" style="width: 440px;"> 
                                    </div>
                                    
                    <div id="objective1_text" style="display:none;"></div> <br /> 
                 &nbsp;<a href="#" id="needsNew1"><i class="icon-plus"></i></a>   
                                                                                
           </div>
           </div>
           </div>


                                                                        </div>
                                                                        <!-- END BLANK PAGE PORTLET-->
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <div class="widget yellow">
                                                            <div class="widget-title">
                                                                <h4>School
                                                                </h4>

                                                            </div>
                                                            <div class="widget-body">
                                                                <div class="form-horizontal">
                                                                    <div id="pills" class="custom-wizard-pills-yellow3">


                                                                    <div class="control-group">
	<div class="controls" id="needs_text" style="margin:0;">
	<div class="span6">
		<div style="margin:0 0 0 5px;"> <b>School</b></div>
			<select name="needs_school[]" id="needs_school" data-placeholder="--Please Select--" tabindex="1" style="width: 450px;"onChange="schoollist(1,$(this).attr('id'),'objective_school1')">
								<option value=''></option>
                              <?php
                           if (!empty($strategies_school)) {
                          foreach ($strategies_school as $strategies_value) {
                         ?>
                       <option value="<?php echo $strategies_value->id; ?>"><?php echo $strategies_value->intervention_strategies_school; ?></option>
    				<?php }}?>
                    </select>
            </div>
            <div id="objective_school1" style="display:none;"></div> <br /> 
            &nbsp;<a href="#" id="needsNew_text"><i class="icon-plus"></i></a>
           </div>
           </div>

           <div class="control-group">
    <div class="controls" id="needs_school_text" style="margin:0;">
    <div class="span6">
        <div style="margin:0 0 0 5px;"> <b>school</b></div>
            <input type="text" name="needs_school[]" value="" id="needs_school_text" style="width: 440px;"> 
            </div>
            <div id="objective_school1" style="display:none;"></div> <br /> 
            &nbsp;<a href="#" id="needsNew_text1"><i class="icon-plus"></i></a>
           </div>
           </div>




                                                                    </div>
                                                                    <!-- END BLANK PAGE PORTLET-->
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="widget yellow">
                                                            <div class="widget-title">
                                                                <h4>Medical

                                                                </h4>

                                                            </div>
                                                            <div class="widget-body">
                                                                <div class="form-horizontal">
                                                                    <div id="pills" class="custom-wizard-pills-yellow3">


        <div class="control-group">
            <div class="controls" id="medical_text" style="margin:0;">
					<div class="span6">
		<div style="margin:0 0 0 5px;"> <b>Medical</b></div>
			<select name="needs_medical[]" id="needs_medical" data-placeholder="--Please Select--" tabindex="1" style="width: 450px;" onChange="medicallist(1,$(this).attr('id'),'objective_medical1')">
								<option value=''></option>
                           <?php
                             if (!empty($strategies_medical)) {
                             foreach ($strategies_medical as $strategies_value) {
                           ?>
                        <option value="<?php echo $strategies_value->id; ?>"><?php echo $strategies_value->intervention_strategies_medical; ?></option>
    			<?php }}?>
		</select>
            </div>
          	  <div id="objective_medical1" style="display:none;"></div> <br /> 
          	  &nbsp;<a href="#" id="medicalNew_text"><i class="icon-plus"></i></a>
                                                                        </div>
                                                                    </div>

                                                                    <div class="control-group">
            <div class="controls" id="need_medical_text" style="margin:0;">
                    <div class="span6">
        <div style="margin:0 0 0 5px;"> <b>Medical</b></div>
            <input type="text" name="needs_medical[]" value="" id="needs_medical_text" style="width: 440px;"> 
            </div>
              <div id="objective_medical1" style="display:none;"></div> <br /> 
              &nbsp;<a href="#" id="medicalNew_text1"><i class="icon-plus"></i></a>
                                                                        </div>
                                                                    </div>
                                                                    <!-- END BLANK PAGE PORTLET-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="span12">
                                                        </div>
                                                    </div>

                                                    <br /><br /><br />
                                                    <!--                   <ul class="pager wizard">
                                                                                             <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                                                                             <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                                                                             <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                                                                            <button type="submit" name="submit" id="success_team_form" value="submit" class="button_next">Next</button>
                                                                                         </ul>-->

                                                </div>

                                                <!--BEGIN STEP 4-->
                                                
                                                    <div class="tab-pane" id="pills-tab4">
                                                        <h3 style="color:#000000">Actions</h3>

                                                        <div class="control-group">

                                                            <div class="widget yellow">
                                                                <div class="widget-title">
                                                                    <h4>Home </h4>
<!--                                                                    <div id="resp" style="margin:0 0 0 20px;"> Needs</div>
                                                                    <div id="resp" style="margin:0 0 0 150px;"> Action</div>
                                                                    <div id="resp"> Responsible person</div>-->
                                                                </div>
                                                                <div class="widget-body">
                                                                    <div class="form-horizontal">
                                                                        <div id="pills" class="custom-wizard-pills-yellow3">

                                                                            <div class="control-group">

                                                                                <div class="controls" id="action" style="margin:0;">
                                                                                    
                                                                                    <div class="span3">
                                                                                        <div  style="margin:0 0 0 100px;"> <b>Needs</b></div>
                                                                                   
      <select name="needs_action_home[]" id="needs_action_home" data-placeholder="--Please Select--" tabindex="1" style="width:250px;margin: 0;padding: 0;margin-bottom:0px;" onChange="need_home_by_actionlist(1,$(this).attr('id'),'action_home1')">
                                                                                                <option value=''></option>

                                                                                               
                                                                                            </select>
                                                                                    </div>
                                                                                    <div class="span3">
                                                                                            
                                                                                            <div style="margin:0 0 0 100px;"> <b>Action</b></div>
                                                                                            <select name="action_home[]" id="action_home1" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;">
<option value=''></option>
        <?php
        if (!empty($action_home)) {
            foreach ($action_home as $action_value) {
         ?>
<option value="<?php echo $action_value['id']; ?>"><?php echo $action_value['actions_home']; ?></option>
	 <?php }
			}
			?>
</select>
</div>
<div class="span3">
<div style="margin:0 0 0 50px;"> <b>Responsible person</b></div>
                                                                                           
      <input type="text"  name="action_home_responsible[]" id="action_home" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" />
<!--&nbsp;<a href="#" id="actionNew"><i class="icon-plus"></i></a><br /><br />-->
                                                                                    </div>

                     <div class="span3">
<div style="margin:0 0 0 50px;"> <b>Date</b></div>
                                                                                           
      <input type="text"  name="action_home_date[]" id="action_home_date" class="datep" data-placeholder="Enter Date" tabindex="1" style="width: 100px;" />
&nbsp;<a href="#" id="actionNew"><i class="icon-plus"></i></a><br /><br />
                                                                                    </div>                                                               
                                                                               
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                        <!-- END BLANK PAGE PORTLET-->
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="widget yellow">
                                                                <div class="widget-title">
                                                                    <h4>School</h4>
<!--                                                                    <div id="resp" style="margin:0 0 0 20px;"> Needs</div>
                                                                    <div id="resp" style="margin:0 0 0 150px;"> Action</div>
                                                                    <div id="resp"> Responsible person</div>-->
                                                                    

                                                                </div>
                                                                <div class="widget-body">
                                                                    <div class="form-horizontal">
                                                                        <div id="pills" class="custom-wizard-pills-yellow3">

                                                                            <div class="control-group">

                                                                                <div class="controls" id="action_text" style="margin:0;">

                                                                                       <div class="span3">
                   <div style="margin:0 0 0 100px;"> <b>Needs</b></div>

     <select name="needs_action_school[]" id="needs_action_school" data-placeholder="--Please Select--" tabindex="1" style="width:250px;margin: 0;padding: 0;margin-bottom:0px;" onChange="need_school_by_actionlist(1,$(this).attr('id'),'action_school1')">
                   <option value=''></option>
                   </select>
                   </div>
<div class="span3">
   <div style="margin:0 0 0 100px;"> <b>Action</b></div>
   <select name="action_school[]" id="action_school1" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;">
   <option value=''></option>
   <?php
   if (!empty($action_school)) {
   foreach ($action_school as $action_value) {
                                                                                                    ?>
                                                                                                        <option value="<?php echo $action_value['id']; ?>"><?php echo $action_value['actions_school']; ?></option>
            <?php }
        }
        ?>
                                                                                            </select>
</div>
    <div class="span3">
         <div style="margin:0 0 0 50px;"> <b>Responsible person</b></div>
            <input type="text"  name="action_school_responsible[]" id="action_school" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" />
<!--&nbsp;<a href="#" id="actionNew_text"><i class="icon-plus"></i></a><br /><br />-->
</div>

  <div class="span3">
         <div style="margin:0 0 0 50px;"> <b>Date</b></div>
        <input type="text"  name="action_school_date[]" id="action_school_date" class="datep" data-placeholder="--Please Select--" tabindex="1" style="width: 100px;" />
&nbsp;<a href="#" id="actionNew_text"><i class="icon-plus"></i></a><br /><br /></div>
                                                                                </div>
                                                                            </div>




                                                                        </div>
                                                                        <!-- END BLANK PAGE PORTLET-->
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="widget yellow">
                                                                <div class="widget-title">
                                                                    <h4>Medical</h4>
<!--                                                                    <div id="resp" style="margin:0 0 0 20px;"> Needs</div>
                                                                    <div id="resp" style="margin:0 0 0 150px;"> Action</div>
                                                                    <div id="resp"> Responsible person</div>-->
                                                                    

                                                                </div>
                                                                <div class="widget-body">
                                                                    <div class="form-horizontal">
                                                                        <div id="pills" class="custom-wizard-pills-yellow3">


                                                                            <div class="control-group">

			<div class="controls" id="health_act_text" style="margin:0;">
				<div class="span3">
					<div style="margin:0 0 0 100px;"> <b>Needs</b></div>
<select name="needs_action_medical[]" id="needs_action_medical" data-placeholder="--Please Select--" tabindex="1" style="width:250px;margin: 0;padding: 0;margin-bottom:0px;" onChange="need_medical_by_actionlist(1,$(this).attr('id'),'action_medical1')">
                                                                                                <option value=''></option>

                                                                                            </select>
</div>
                                                                                    <div class="span3">
                                                                                         <div style="margin:0 0 0 100px;"> <b>Action</b></div>
             <select name="action_medical[]" id="action_medical1" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;">
                                                                                                <option value=''></option>
                                                                                            <?php
                                                                                            if (!empty($action_medical)) {
                                                                                                foreach ($action_medical as $action_value) {
                                                                                                    ?>
						 <option value="<?php echo $action_value['id']; ?>"><?php echo $action_value['actions_medical']; ?></option>
        				    <?php }
       							 }
       					 ?>
                                                                                            </select>
                                                                                    </div>
                <div class="span3">
                     <div style="margin:0 0 0 50px;"> <b>Responsible person</b></div>
                        <input type="text"  name="action_medical_responsible[]" id="action_medical" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" />
                       <!-- &nbsp;<a href="#" id="health_act_New_text"><i class="icon-plus"></i></a><br /><br />-->
                </div>
                <div class="span3">
                     <div style="margin:0 0 0 50px;"> <b>Date</b></div>
                        <input type="text"  name="action_medical_date[]" id="action_medical_date" class="datep" data-placeholder="--Please Select--" tabindex="1" style="width: 100px;" />
                        &nbsp;<a href="#" id="health_act_New_text"><i class="icon-plus"></i></a><br /><br />
                </div>
                                                                                            

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <!-- END BLANK PAGE PORTLET-->
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="span4">
                                                                <label class="control-label" style="float:left;margin-top:5px;padding-right:10px;">Program Change</label>
                                                                <select name="program_change" id="program_change">
                                                                    <option>---Please Select---</option>
                                                                    <?php foreach($program_changes as $program_change):?>
                                                                        <option id="<?php echo $program_change['id'];?>"><?php echo $program_change['program_change'];?></option>
                                                                    <?php endforeach?>
                                                                </select>
                                                            </div>
                                                            <div class="span4">
                                                                <label class="control-label" style="float:left;margin-top:5px;padding-right:10px;">Next Meeting Date</label>
                                                                <input type="text" name="next_date" id="dp2" > 
                                                            </div>
                                                            <center>
                                                                <button type="submit" name="submit" id="success_team_form" value="submit"  class="btn btn-large btn-yellow"><i class="icon-save icon-white"></i> Save</button>


                                                                <button type="submit" name="submit" id="success_team_form" value="submit"  class="btn btn-large btn-yellow"><i class="icon-save icon-white"></i>Print</button>

                                                            </center>


                                                            <div class="span12">
                                                            </div>
                                                        </div>

                                                    </div>
                                                
                                                
                                                    <ul class="pager wizard">
                                                        <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                                        <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                                        <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                                        <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                                    </ul>
                                            </div>


                                    






                                </div>


                                </form>


                            </div>

                        </div>

                    </div>
                </div>
                <!-- END BLANK PAGE PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div id="footer">
    UEIS © Copyright 2012. All Rights Reserved.
</div>
<div class="dialog"></div>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>


<script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

<!-- ie8 fixes -->
<!--[if lt IE 9]>
<script src="js/excanvas.js"></script>
<script src="js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="<?php echo SITEURLM ?>js/form-component.js"></script>
<script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

<script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
<!--<script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>-->

<script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
   
 



<script>
    var need_action_home_opt = '<option>Select Value</option>';
    var need_action_school_opt = '<option>Select Value</option>';
    var need_action_medical_opt = '<option>Select Value</option>';
    var Script = function () {

    $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
//        console.log('onNext');
    if(index==3){
         need_action_home_opt = '<option>Select Value</option>';
     need_action_school_opt = '<option>Select Value</option>';
     need_action_medical_opt = '<option>Select Value</option>';
        $("select[name='needs_home[]']").each(function(){
			var thisID = $(this).attr('id');
			var thisArr = thisID.split('_');
			var objID = 0;
         	if(thisArr.length == 2){
					
					objID = 1;
					if($(this).parent('div.span6').parent('div#needs').children('div#objective'+objID).html()!=''){
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).parent('div.span6').parent('div#needs').children('div#objective'+objID).find("#needs_home_child").val()+'">'+$(this).parent('div.span6').parent('div#needs').children('div#objective'+objID).find("#needs_home_child option:selected").text()+'</option>';
					} else {
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			} else {
				
				objID = thisArr[2];
				console.log($.trim($(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).html()));
				if($.trim($(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).html())!=''){
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).find("#needs_home_child").val()+'">'+$(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).find("#needs_home_child option:selected").text()+'</option>';
					} else {
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			}
        });
		
        $("select[name='needs_school[]']").each(function(){
			
			var thisID = $(this).attr('id');
			var thisArr = thisID.split('_');
			var objID = 0;
			if(thisArr.length == 2){
					
					objID = 1;
					if($(this).parent('div.span6').parent('div#needs_text').children('div#objective_school'+objID).html()!=''){
						need_action_school_opt = need_action_school_opt + '<option value="'+$(this).parent('div.span6').parent('div#needs_text').children('div#objective_school'+objID).find("#needs_school_child").val()+'">'+$(this).parent('div.span6').parent('div#needs_text').children('div#objective_school'+objID).find("#needs_school_child option:selected").text()+'</option>';
					} else {
						need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			} else {
				
				objID = thisArr[2];
				console.log($.trim($(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).html()));
				if($.trim($(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).html())!=''){
						need_action_school_opt = need_action_school_opt + '<option value="'+$(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).find("#needs_school_child").val()+'">'+$(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).find("#needs_school_child option:selected").text()+'</option>';
					} else {
						need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			}
			
            //need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';
        });
        $("select[name='needs_medical[]']").each(function(){
			
			var thisID = $(this).attr('id');
			var thisArr = thisID.split('_');
			var objID = 0;
			if(thisArr.length == 2){
					
					objID = 1;
					if($(this).parent('div.span6').parent('div#medical_text').children('div#objective_medical'+objID).html()!=''){
						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).parent('div.span6').parent('div#medical_text').children('div#objective_medical'+objID).find("#needs_medical_child").val()+'">'+$(this).parent('div.span6').parent('div#medical_text').children('div#objective_medical'+objID).find("#needs_medical_child option:selected").text()+'</option>';
					} else {
						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			} else {
				
				objID = thisArr[2];
				console.log($.trim($(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_school'+objID).html()));
				if($.trim($(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_medical'+objID).html())!=''){
						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_medical'+objID).find("#needs_medical_child").val()+'">'+$(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_medical'+objID).find("#needs_medical_child option:selected").text()+'</option>';
					} else {
						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			}
			
			
			
         //   need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';
        });
//        alert(need_action_home_opt);
//        $('#needs_action_home').html('');
        $('#needs_action_home').html(need_action_home_opt);
        $('#needs_action_school').html(need_action_school_opt);
        $('#needs_action_medical').html(need_action_medical_opt);
    }
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(tab);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');

if(index==3){
         need_action_home_opt = '<option>Select Value</option>';
     need_action_school_opt = '<option>Select Value</option>';
     need_action_medical_opt = '<option>Select Value</option>';
        $("select[name='needs_home[]']").each(function(){
			var thisID = $(this).attr('id');
			var thisArr = thisID.split('_');
			var objID = 0;
          	if($(this).val()!=''){
            if(thisArr.length == 2){
					
					objID = 1;
					if($(this).parent('div.span6').parent('div#needs').children('div#objective'+objID).html()!=''){
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).parent('div.span6').parent('div#needs').children('div#objective'+objID).find("#needs_home_child").val()+'">'+$(this).parent('div.span6').parent('div#needs').children('div#objective'+objID).find("#needs_home_child option:selected").text()+'</option>';
					} else {

						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			} else {
				
				objID = thisArr[2];
				//console.log($.trim($(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).html()));
				if($.trim($(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).html())!=''){
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).find("#needs_home_child").val()+'">'+$(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).find("#needs_home_child option:selected").text()+'</option>';
					} else {
						need_action_home_opt = need_action_home_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
					}
			}
        }
        });

        $("input[name='needs_home[]']").each(function(){
            var thisID = $(this).attr('id');
            var thisArr = thisID.split('_');
            var objID = 0;
         if($(this).val()!=''){
            if(thisArr.length == 2){
                    
                    objID = 1;
                        need_action_home_opt = need_action_home_opt + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';    
                    
            } else {
                
                objID = thisArr[2];
                //console.log($.trim($(this).parent('div.span6').parent('div#addeddiv_'+objID).children('div#objective'+objID).html()));
                        need_action_home_opt = need_action_home_opt + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';    
                
            }
        }
        });
		
        $("input[name='needs_school[]']").each(function(){
			
			var thisID = $(this).attr('id');
			var thisArr = thisID.split('_');
			var objID = 0;
			if($(this).val()!=''){
                if(thisArr.length == 2){
    					
    					objID = 1;
    						need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';	
    					
    			} else {
    				
    				objID = thisArr[2];
    						need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';	
    				
    			}
			}
        });

        $("select[name='needs_school[]']").each(function(){
            
            var thisID = $(this).attr('id');
            var thisArr = thisID.split('_');
            var objID = 0;
            if($(this).val()!=''){
                if(thisArr.length == 2){
                        
                        objID = 1;
                        if($(this).parent('div.span6').parent('div#needs_text').children('div#objective_school'+objID).html()!=''){
                            need_action_school_opt = need_action_school_opt + '<option value="'+$(this).parent('div.span6').parent('div#needs_text').children('div#objective_school'+objID).find("#needs_school_child").val()+'">'+$(this).parent('div.span6').parent('div#needs_text').children('div#objective_school'+objID).find("#needs_school_child option:selected").text()+'</option>';
                        } else {
                            need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';    
                        }
                } else {
                    
                    objID = thisArr[2];
                    console.log($.trim($(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).html()));
                    if($.trim($(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).html())!=''){
                            need_action_school_opt = need_action_school_opt + '<option value="'+$(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).find("#needs_school_child").val()+'">'+$(this).parent('div.span6').parent('div#added_'+objID).children('div#objective_school'+objID).find("#needs_school_child option:selected").text()+'</option>';
                        } else {
                            need_action_school_opt = need_action_school_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';    
                        }
                }
            
            }
        });


        $("select[name='needs_medical[]']").each(function(){
			
			var thisID = $(this).attr('id');
			var thisArr = thisID.split('_');
			var objID = 0;
			if($(this).val()!=''){
                if(thisArr.length == 2){
    					
    					objID = 1;
    					if($(this).parent('div.span6').parent('div#medical_text').children('div#objective_medical'+objID).html()!=''){
    						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).parent('div.span6').parent('div#medical_text').children('div#objective_medical'+objID).find("#needs_medical_child").val()+'">'+$(this).parent('div.span6').parent('div#medical_text').children('div#objective_medical'+objID).find("#needs_medical_child option:selected").text()+'</option>';
    					} else {
    						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
    					}
    			} else {
    				
    				objID = thisArr[2];
    				console.log($.trim($(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_school'+objID).html()));
    				if($.trim($(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_medical'+objID).html())!=''){
    						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_medical'+objID).find("#needs_medical_child").val()+'">'+$(this).parent('div.span6').parent('div#addedmed_'+objID).children('div#objective_medical'+objID).find("#needs_medical_child option:selected").text()+'</option>';
    					} else {
    						need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$("option:selected",this).text()+'</option>';	
    					}
    			}
            }
        });
        
        $("input[name='needs_medical[]']").each(function(){
            
            var thisID = $(this).attr('id');
            var thisArr = thisID.split('_');
            var objID = 0;
            if($(this).val()!=''){
                if(thisArr.length == 2){
                        
                        objID = 1;
                            need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';  
                } else {
                    
                    objID = thisArr[2];
                            need_action_medical_opt = need_action_medical_opt + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';  
                }
            }
        });

        $('#needs_action_home').html(need_action_home_opt);
        $('#needs_action_school').html(need_action_school_opt);
        $('#needs_action_medical').html(need_action_medical_opt);
    }
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
    }});

    $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        console.log('onNext');
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabClick: function(tab, navigation, index) {
        console.log('onTabClick');

    }, onTabShow: function(tab, navigation, index) {
        console.log('onTabShow2');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#tabsleft').find('.bar').css({width:$percent+'%'});

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
            $('#tabsleft').find('.pager .next').hide();
            $('#tabsleft').find('.pager .finish').show();
            $('#tabsleft').find('.pager .finish').removeClass('disabled');
        } else {
            $('#tabsleft').find('.pager .next').show();
            $('#tabsleft').find('.pager .finish').hide();
        }

    }});


    $('#tabsleft .finish').click(function() {
        alert('Finished!, Starting over!');
        $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
    });

}();
//<!--start Strengths text -->	
	$(function() {
	var addDiv = $('#strengths');
	var i = $('#strengths p').size() + 1;
	$('#addNew').live('click', function() {
	$('<p><br /><textarea id="parent" size="16" name="strengths_parent[]" value="" class="m-ctrl-medium sst_class" placeholder="Input Value" ></textarea>&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	
	$(function() {
	var addDiv = $('#strengths_text');
	var i = $('#strengths_text p').size() + 1;
	 
	$('#addNew_text').live('click', function() {
	$('<p><br /><textarea id="teacher" size="16" name="strengths_teacher[]" class="m-ctrl-medium sst_class" placeholder="Input Value"></textarea>&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	 
//	 <!--end Strengths text -->
//	 
//	 <!--start Background Information text -->
//	
//<!--create family text box -->
//	
	$(function() {
	var addDiv = $('#background');
	var i = $('#background p').size() + 1;
	 
	$('#backgroundNew').live('click', function() {
	$('<p><br /><textarea id="family" size="16" name="information_family[]" class="m-ctrl-medium sst_class" placeholder="Input Value"></textarea>&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
//<!--end family text box -->	
//<!--create school text box -->
	$(function() {
	var addDiv = $('#background_text');
	var i = $('#background_text p').size() + 1;
	 
	$('#backgroundNew_text').live('click', function() {
	$('<p><br /><textarea id="school" size="16" name="information_school[]" value="" class="m-ctrl-medium sst_class" placeholder="Input Value"></textarea>&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
//	<!--end scool text box -->
//<!--create health text box -->
	$(function() {
	var addDiv = $('#health_text');
	var i = $('#health_text p').size() + 1;
	 
	$('#healthNew_text').live('click', function() {
	$('<p><br /><textarea id="health" size="16" name="information_health[]" value="" class="m-ctrl-medium sst_class" placeholder="Input Value"></textarea>&nbsp;<a href="#" id="rem_healthNew_New_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#rem_healthNew_New_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
//<!-- end health text box -->	
//	 <!--end Background Information text-->
//
	
//
//<!--create Action text box -->
	
	$(function() {
	var addDiv = $('#action');
	var i = $('#action p').size() + 2;
     console.log('action_home:'+i);
	 $('#actionNew').live('click', function() {

        if(i==1){
            i++;
        }
            
        var action_home = '<div id="add_home_'+i+'"><div class="span3" style="margin-left:0px;"><select name="needs_action_home[]" id="needs_action_home_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width:250px;margin: 0;padding: 0;margin-bottom:0px;" onchange="need_home_by_actionlist(1,$(this).attr(\'id\'),\'action_home'+i+'\')"><option value=""></option>'+need_action_home_opt+'</select></div><div class="span3"><select name="action_home[]" id="action_home'+i+'" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;"><option value=""></option> 				<?php if(!empty($action_home)) {foreach($action_home as $action_value){?><option value="<?php echo $action_value['action_home_id'];?>"><?php echo addslashes($action_value['actions_home']);?></option><?php	} } ?></select></div><div class="span3"><input type="text"  name="action_home_responsible[]" id="action_home_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" /></div> <div class="span3"><input type="text" name="action_home_date[]" id="action_home_date" class="datep" data-placeholder="--Please Select--" tabindex="1" style="width: 100px;">&nbsp;<a href="#" class="remNew" id="remNew_'+i+'"><i class="icon-minus-sign"></i></a><br /><br />  </div></div>';
        $(action_home).appendTo(addDiv);
	i++;
     $('.datep').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
	return false;
	});

	
		$('.remNew').live('click', function() {
	var clicked_id = '';		
	if( i > 1 ) {
		clicked_id = $(this).attr('id');
		var clickedarr = clicked_id.split('_');
		$('#add_home_'+clickedarr[1]).remove();
//		$(this).parents('div').remove();
	i--;
	}
	return false;
	});
	
	
	
	});
//<!--end family text box -->	
//<!--create school text box -->
	$(function() {
	var addDiv = $('#action_text');
	var i = $('#action_text p').size() + 2;

    console.log('action_school:'+i);
	 
	$('#actionNew_text').live('click', function() {
        if(i==1){
            i++;
        }
         var action_school = '<div id="add_school_'+i+'"><div class="span3" style="margin-left:0px;"><select name="needs_action_school[]" id="needs_action_school_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width:250px;margin: 0;padding: 0;margin-bottom:0px;" onChange="need_school_by_actionlist(1,$(this).attr(\'id\'),\'action_school'+i+'\')">'+need_action_school_opt+'</select></div><div class="span3"><select name="action_school[]" id="action_school'+i+'" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;"><option value=""></option> 				<?php if(!empty($action_school)) {foreach($action_school as $action_value){?><option value="<?php echo $action_value['id'];?>"><?php echo addslashes($action_value['actions_school']);?></option><?php	} } ?></select></div><div class="span3"><input type="text"  name="action_school_responsible[]" id="action_school_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" /></div><div class="span3"><input type="text" name="action_school_date[]" id="action_school_date" class="datep" data-placeholder="--Please Select--" tabindex="1" style="width: 100px;">&nbsp;<a href="#" class="remNew_text" id="remNew_text_'+i+'"><i class="icon-minus-sign"></i></a><br /><br /></div></div>'
	$(action_school).appendTo(addDiv);

	i++;
	  $('.datep').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
	return false;
	});
	 
		$('.remNew_text').live('click', function() {
	var clicked_id = '';		
	if( i > 1 ) {
		clicked_id = $(this).attr('id');
		var clickedarr = clicked_id.split('_');
		$('#add_school_'+clickedarr[2]).remove();
//		$(this).parents('div').remove();
	i--;
	}
	return false;
	});
	
	});
//	<!--end scool text box -->
//<!--create health text box -->
	$(function() {
	var addDiv = $('#health_act_text');
	var i = $('#health_act_text p').size() + 2;
	 console.log('action_health:'+i);
	$('#health_act_New_text').live('click', function() {
        if(i==1){
            i++;
        }
            var action_medical = '<div id="add_medical_'+i+'"><div class="span3" style="margin-left:0px;"><select name="needs_action_medical[]" id="needs_action_medical_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width:250px;margin: 0;padding: 0;margin-bottom:0px;" onChange="need_medical_by_actionlist(1,$(this).attr(\'id\'),\'action_medical'+i+'\')">'+need_action_medical_opt+'</select></div><div class="span3"><select name="action_medical[]" id="action_medical'+i+'" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;"><option value=""></option> 				<?php if(!empty($action_medical)) {foreach($action_medical as $action_value){?><option value="<?php echo $action_value->id;?>"><?php echo addslashes($action_value->actions_medical);?></option><?php	} } ?></select></div><div class="span3"><input type="text"  name="action_medical_responsible[]" id="action_medical_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" /></div><div class="span3"><input type="text" name="action_medical_date[]" id="action_medical_date" class="datep" data-placeholder="--Please Select--" tabindex="1" style="width: 100px;">&nbsp;<a href="#" class="rem_healthNew_New_text" id="rem_healthNew_New_text_'+ i +'"><i class="icon-minus-sign"></i></a><br /><br /></div> </div>'
	$(action_medical).appendTo(addDiv);
	
	
	i++;
	  $('.datep').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
	return false;
	});
	 
		$('.rem_healthNew_New_text').live('click', function() {
	var clicked_id = '';		
	if( i > 1 ) {
		clicked_id = $(this).attr('id');
		var clickedarr = clicked_id.split('_');
		$('#add_medical_'+clickedarr[4]).remove();
//		$(this).parents('div').remove();
	i--;
	}
	return false;
	});	
	
	
	
	});
//<!-- end health text box -->	
	
function updateschool(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#schools").html(result);
			$("#schools").trigger("liszt:updated");
		}
		});
	
}
function updateclass(tag)
{
		
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getgrades?schoolid='+tag,
		success:function(result)
		{
			$("#grades").html(result);
			$("#grades").trigger("liszt:updated");
		}
		});
	
}
function checkvalid()
{
	
	var grade = document.getElementById('grades').value;
	var classrooms = document.getElementById('classrooms').value;
	<?php if(isset($school_type)) {?>
	var schooltype=document.getElementById('schools_type').value;
	var school = document.getElementById('schools').value;

	if(schooltype=="")
	{
		alert('Please select school type');
		return false;
	}
	
	if(school=="")
	{
		alert('Please select school');
		return false;
	}
		 <?php }?>	

	if(grade=="")
	{
		alert('Please select grade');
		return false;
	}
	
if(classrooms=="")
	{
		alert('Please select classroom');
		return false;
	}


document.searchstudent.submit();
	
}
function updateclassroom(gradeid)
{
	var schoolid = $("#schools").val();
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getteachers?grade_id='+gradeid+"&schid="+schoolid,
		success:function(result)
		{
			$("#classrooms").html(result);
			$("#classrooms").trigger("liszt:updated");
		}
		});
}
$('#classrooms').change(function(){
	$('#students').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/get_student_by_teacher",
	  data: { teacher_id: $('#classrooms').val() }
	})
	  .done(function( msg ) {
		  $('#students').html('');
		  var result = jQuery.parseJSON( msg )
		   $('#students').append('<option value="all">All</option>');
		 $(result.students).each(function(index, Element){ 
		 
		// console.log(Element);
		  $('#students').append('<option value="'+Element.student_id+'">'+Element.firstname+' '+Element.lastname+'</option>');
		});  
		  $("#students").trigger("liszt:updated");
		  
	  });
  	
	});
$("#studentlist").autocomplete("<?php echo base_url();?>classroom/getallstudents", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
	
	 $("#studentlist").result(function(event, data, formatted) {
        $("#student_id").val(data[1]);
    });
   function homelist(domid,needs_home,dispbody) {
		//console.log(needs_home);
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url();?>classroom/get_home_to_child_info",
			  data: { needs_home: $('#'+needs_home).val() }
			})
	.done(function( msg ) {
				var result = jQuery.parseJSON( msg )
				var htmltable = '';	
				 if(result.child_data.length>0){ 	  
				htmltable += '<div class="span6" id="objective"><div style="margin:0 0 0 5px;"><b>Sub Category</b></div><select name="needs_home_child[]" id="needs_home_child" data-placeholder="--Please Select--" tabindex="1" style="width: 400px;">';
		$(result.child_data).each(function(index, Element){
		htmltable +='<option value="'+Element.id+'">'+Element.intervention_strategies_home+'</option>';
			});
			htmltable +='</select></div></div>';
				 }
				//console.log(htmltable);		 
				$('#'+dispbody).html(htmltable);  
				$('#'+dispbody).show();
			  });
	  }
   
    function schoollist(domid,needs_school,dispbody) {
		//console.log(needs_school);
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url();?>classroom/get_school_to_child_info",
			  data: { needs_school: $('#'+needs_school).val() }
			})
	.done(function( msg ) {
				var result = jQuery.parseJSON( msg )
				var htmltable = '';	
	 
				  if(result.school_child.length>0){ 
				htmltable += '<div class="span6" id="objective_school"><div style="margin:0 0 0 5px;"><b>Sub Category</b></div><select name="needs_school_child[]" id="needs_school_child" data-placeholder="--Please Select--" tabindex="1" style="width: 400px;">';
		$(result.school_child).each(function(index, Element){
		htmltable +='<option value="'+Element.id+'">'+Element.intervention_strategies_school+'</option>';
			});
			htmltable +='</select></div></div>';
				  }
				//console.log(htmltable);		 
				$('#'+dispbody).html(htmltable);  
				$('#'+dispbody).show();
			  });
	  }  
 function medicallist(domid,needs_medical,dispbody) {
		//console.log(needs_medical);
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url();?>classroom/get_medical_to_child_info",
			  data: { needs_medical: $('#'+needs_medical).val() }
			})
	.done(function( msg ) {
		
				var result = jQuery.parseJSON( msg )
				var htmltable = '';	
				  if(result.medical_child.length>0){
				htmltable += '<div class="span6" id="objective_medical"><div style="margin:0 0 0 5px;"><b>Sub Category</b></div><select name="needs_medical_child[]" id="needs_medical_child" data-placeholder="--Please Select--" tabindex="1" style="width: 400px;">';
			
		$(result.medical_child).each(function(index, Element){
		htmltable +='<option value="'+Element.id+'">'+Element.intervention_strategies_medical+'</option>';
			});

			htmltable +='</select></div></div>';
				  }
			//	console.log(htmltable);		 
				$('#'+dispbody).html(htmltable);  
				$('#'+dispbody).show();
			  });
	  }	  
	  
$(function() {
	var addDiv = $('#needs');
    var addDiv_text = $('#needs_home_text');
	var i = $('#needs p').size() + 2;
    var i_txt = $('#needs_home_text p').size() + 2;
	 
	$('#needsNew').live('click', function(){
	$('<p><div id="addeddiv_'+i+'"><div class="span6"><div style="margin:0 0 0 5px;"><b>Home</b></div><select name="needs_home[]" id="needs_home_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 450px;" onchange="homelist(1,$(this).attr(\'id\'),\'objective'+i+'\')"><option value=""></option> 				<?php if(!empty($strategies_home)) {foreach($strategies_home as $strategies_value){ ?><option value="<?php echo $strategies_value->id;?>"><?php echo addslashes($strategies_value->intervention_strategies_home);?></option><?php	} }?></select></div><div id="objective'+i+'" style="display:none;"></div>&nbsp;<br /><a href="#" class="remNew" id="remNew_'+i+'"><i class="icon-minus-sign"></i></a></div> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});

    $('#needsNew1').live('click', function(){
    $('<p><div id="addeddiv_'+i_txt+'"><div class="span6"><div style="margin:0 0 0 5px;"><b>Home</b></div><input type="text" name="needs_home[]" id="needs_home_' + i_txt +'" tabindex="1" style="width: 440px;" ></div><div id="objective'+i_txt+'" style="display:none;"></div>&nbsp;<br /><a href="#" class="remNew" id="remNew_'+i_txt+'"><i class="icon-minus-sign"></i></a></div> </p>').appendTo(addDiv_text);
    i++;
     
    return false;
    });
	 
	$('.remNew').live('click', function() {
	var clicked_id = '';		
	if( i > 1 ) {
		clicked_id = $(this).attr('id');
		var clickedarr = clicked_id.split('_');
		$('#addeddiv_'+clickedarr[1]).remove();
//		$(this).parents('div').remove();
	i--;
	}
	return false;
	});
	});
	
$(function() {
	var addDiv = $('#needs_text');
	var i = $('#needs_text p').size() + 2;

    var addDiv_txt = $('#needs_school_text');
    var i_txt = $('#needs_school_text p').size() + 2;
    
	 
	$('#needsNew_text').live('click', function() {
	$('<p><div id="added_'+i+'"><div class="span6"><div style="margin:0 0 0 5px;"><b>school</b></div><select name="needs_school[]" id="needs_school_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 450px;" onchange="schoollist(1,$(this).attr(\'id\'),\'objective_school'+i+'\')"><option value=""></option> 				<?php if(!empty($strategies_school)) {foreach($strategies_school as $strategies_value){?><option value="<?php echo $strategies_value->id;?>"><?php echo addslashes($strategies_value->intervention_strategies_school);?></option><?php	} } ?></select></div><div id="objective_school'+i+'" style="display:none;"></div>&nbsp;<br /><a href="#" class="remNew_text" id="remNew_text_'+i+'"><i class="icon-minus-sign"></i></a></div></p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	$('#needsNew_text1').live('click', function() {
    $('<p><div id="added_'+i_txt+'"><div class="span6"><div style="margin:0 0 0 5px;"><b>school</b></div><input type="text" name="needs_school[]" id="needs_school_text_' + i_txt +'" tabindex="1" style="width: 440px;"></div><div id="objective_school'+i_txt+'" style="display:none;"></div>&nbsp;<br /><a href="#" class="remNew_text" id="remNew_text_'+i_txt+'"><i class="icon-minus-sign"></i></a></div></p>').appendTo(addDiv_txt);
    i++;
     
    return false;
    });
	$('.remNew_text').live('click', function() {
	var clicked_id = '';		
	if( i > 1 ) {
		clicked_id = $(this).attr('id');
		var clickedarr = clicked_id.split('_');

		$('#added_'+clickedarr[2]).remove();
//		$(this).parents('div').remove();
	i--;
	}
	return false;
	});	

	});	
	
$(function() {
	var addDiv = $('#medical_text');
	var i = $('#medical_text p').size() + 2;

    var addDiv_txt = $('#need_medical_text');
    var i_txt = $('#need_medical_text p').size() + 2;

    
   $('#medicalNew_text').live('click', function() {
	$('<p><div id="addedmed_'+i+'"><div class="span6"><div style="margin:0 0 0 5px;"><b>Medical</b></div><select name="needs_medical[]" id="needs_medical_' + i +'" data-placeholder="--Please Select--" tabindex="1" style="width: 450px;" onchange="medicallist(1,$(this).attr(\'id\'),\'objective_medical'+i+'\')"><option value=""></option> 				<?php if(!empty($strategies_medical)) {foreach($strategies_medical as $strategies_value){?><option value="<?php echo $strategies_value->id;?>"><?php echo addslashes($strategies_value->intervention_strategies_medical);?></option><?php	} } ?></select></div><div id="objective_medical'+i+'" style="display:none;"></div>&nbsp;<br /><a href="#" class="rem_healthNew_New_text" id="rem_healthNew_New_text_'+i+'"><i class="icon-minus-sign"></i></a></div> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});

   $('#medicalNew_text1').live('click', function() {
    $('<p><div id="addedmed_'+i_txt+'"><div class="span6"><div style="margin:0 0 0 5px;"><b>Medical</b></div> <input type="text" name="needs_medical[]" value="" id="needs_medical_text" style="width: 440px;"> </div><div id="objective_medical'+i_txt+'" style="display:none;"></div>&nbsp;<br /><a href="#" class="rem_healthNew_New_text" id="rem_healthNew_New_text_'+i_txt+'"><i class="icon-minus-sign"></i></a></div> </p>').appendTo(addDiv_txt);
    i++;
     
    return false;
    });
	 
	$('.rem_healthNew_New_text').live('click', function() {
	var clicked_id = '';		
	if( i > 1 ) {
		clicked_id = $(this).attr('id');
		var clickedarr = clicked_id.split('_');
		$('#addedmed_'+clickedarr[4]).remove();
	//		$(this).parents('div').remove();
	i--;
	}
	return false;
	});
	});	
	

 function need_home_by_actionlist(domid,needs_action_home,dispbody) {
		//console.log(needs_action_home);
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url();?>classroom/get_need_home_by_action_home",
			  data: { needs_action_home: $('#'+needs_action_home).val() }
			})

.done(function( msg ) {
		
				var result = jQuery.parseJSON( msg );

				var htmltable = '';	
		if(result.action_data!=false){		
            $('#'+dispbody).replaceWith('<select name="action_home[]" id="'+dispbody+'" style="width:250px;" data-placeholder="--Please Select--"></select>');
          //  console.log('in this');
    		$(result.action_data).each(function(index, Element){
    		htmltable +='<option value="'+Element.action_home_id+'">'+Element.actions_home+'</option>';
    			});
	
				//console.log(htmltable);		 
				$('#'+dispbody).html(htmltable);  
				$('#'+dispbody).show();
            } else {
                $('#'+dispbody).replaceWith('<input type="text" name="action_home[]" id="'+dispbody+'" style="width:250px;">');
            }
			  });
  }

<!--needs child by action school start -->

 function need_school_by_actionlist(domid,needs_action_school,dispbody) {
		console.log(needs_action_school);
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url();?>classroom/get_need_school_by_action_school",
			  data: { needs_action_school: $('#'+needs_action_school).val() }
			})

.done(function( msg ) {
		var result = jQuery.parseJSON( msg )
				var htmltable = '';	
				if(result.action_data!=false){  
                    $('#'+dispbody).replaceWith('<select name="action_school[]" id="'+dispbody+'" style="width:250px;"></select>'); 
		$(result.action_data).each(function(index, Element){
		htmltable +='<option value="'+Element.action_school_id+'">'+Element.actions_school+'</option>';
			});
	
				console.log(htmltable);		 
				$('#'+dispbody).html(htmltable);  
				$('#'+dispbody).show();
                } else {
       $('#'+dispbody).replaceWith('<input type="text" name="action_school[]" id="'+dispbody+'" style="width:250px;">'); 
    }
			  });
    
  }

<!--needs medical child by action medical start -->

 function need_medical_by_actionlist(domid,needs_action_medical,dispbody) {
		console.log(needs_action_medical);
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url();?>classroom/get_need_medical_by_action_medical",
			  data: { needs_action_medical: $('#'+needs_action_medical).val() }
			})

.done(function( msg ) {
		var result = jQuery.parseJSON( msg )
				var htmltable = '';	
				if(result.action_data!=false){  
                    $('#'+dispbody).replaceWith('<select name="action_medical[]" id="'+dispbody+'" style="width:250px;"></select>');
		$(result.action_data).each(function(index, Element){
		htmltable +='<option value="'+Element.action_medical_id+'">'+Element.actions_medical+'</option>';
			});
	
				console.log(htmltable);		 
				$('#'+dispbody).html(htmltable);  
				$('#'+dispbody).show();
                } else {
        $('#'+dispbody).replaceWith('<input type="text" name="action_medical[]" id="'+dispbody+'" style="width:250px;">');
    }
			  });
    
  }


	


  $(document).ready(function() {
				
				 $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
	
	 $("#studentlist").result(function(event, data, formatted) {
        $("#student_id").val(data[1]);
    });
                
            });

  $('.datep').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

</script>
<!--end pdf javascript -->
</body>
<!-- END BODY -->
</html>