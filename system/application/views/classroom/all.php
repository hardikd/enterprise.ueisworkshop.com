<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Teachers::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
 

function showSchools(stid)
{
	var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;	
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;	
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;		
	}
	$.ajax({
	type:'post',
	url:path,
	success:function(result)
	{
		$('#school_name_id').html(result);
	}
	
	});	
}
function showClassroom(gid)
{
	var gid,sid;
	gid = gid;
	scid = $("#school_name_id").val();
	
	var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/fetchClassroomById.php?scid='+scid+'&gid='+gid;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/fetchClassroomById.php?scid='+scid+'&gid='+gid;	
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/fetchClassroomById.php?scid='+scid+'&gid='+gid;
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/fetchClassroomById.php?scid='+scid+'&gid='+gid;	
	}
	
	if(scid!="")
	{	
	$.ajax({
	type:'post',
	url:path,
	success:function(result)
	{
		$('#school_classroom_id').html(result);
	}	
	});	
	
	}
	else
	{
		alert("Please select School");
	}
}

function checkfields()
{
var schooltype = document.single_test_report.school_type.value;
var classname = document.single_test_report.grade.value;

var classnameroom = document.single_test_report.school_classroom_id.value;


if(schooltype=="")
{
 alert('Please Select Type');
 return false;

}

else if(classname=="")
{
alert('Select Grade');
}
else if(classnameroom=="")
{
	alert('Please Select Classroom');
	return false;
}
else
{
document.single_test_report.submit();
}



}
</script>


</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        
        
        <table align="center">
		<tr>
		<td >
<input type="hidden" name="countries" id="countries" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
		<input type="hidden" name="states" id="states" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
		<input type="hidden" name="district" id="district" value="<?php echo $this->session->userdata('district_id') ?>" >
		</td>
		
		</tr>
		
		
		</table>
		<table>
		<tr>
		<td colspan="2" align="right">
		<h3> School Type -> Schools -> Grades -> Class Rooms -> Students</h3>
		</td>
		</tr>
		</table>
		<div id="teacherdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
	<!--	<input class="btnbig" type="button" name="teacher_add" id="teacher_add" title="Add New" value="Add New" > -->
		</div>
		

		<form  action='graphcontroll/showclasses' name="single_test_report" method="post" >
			<table cellpadding="5">			
            
            <tr>
			<td>
			 School Type:  
			 </td>
			  <td>

              <select class="combobox" name="school_type" id="school_type" onchange="showSchools(this.value)">
              <option value="">-Please Select-</option>
                 <?php
			 foreach($schools_type as $val)
			 {
				 $schooltypeid = $val['school_type_id'];
				 $name = $val['tab'];
				 ?>
                 <option value="<?php echo $schooltypeid;?>"><?php echo $name;?></option>
                 <?php
			 }
			  ?>
			  
			  
           
			  
			  </select>
			  </td>
			  
			</tr>
             <tr>
			<td>
			 Select School:  
			 </td>
			  <td>

              <select class="combobox" name="school_name_id" id="school_name_id" >
              <option value="">-Please Select-</option>			  
           
			  </select>
			  </td>
			  
			</tr>
            <tr>
			<td>
			 Grades:
			  </td>
			  <td>
			  <select class="combobox" name="grade" id="grade" onchange="showClassroom(this.value);" >
			  <option value="">-Please Select-</option>
                <?php
			 foreach($grades as $grade)
			 {
				 
				 $gradeid = $grade['dist_grade_id'];
				 $gradename = $grade['grade_name'];
				 ?>
                 <option value="<?php echo $gradeid;?>"><?php echo $gradename;?></option>
                 <?php
			 }
			  ?>
              			  
			  </select>
			  </td>
			  
			</tr>
     <tr>
			<td>
			 Select Classroom:  
			 </td>
			  <td>

            <select class="combobox" name="school_classroom_id" id="school_classroom_id" >
              <option value="">-Please Select-</option>	           
			</select>
			  </td>
			  
			</tr>
			<tr>			
			<td>
			</td>
<td ><input title="Get Report" class="btnbig" type="button" name="submitform" value="School Report" onclick="checkfields();"></td>
			</tr>
	</table>
   </form>
		
<!-- Graph -->
<!-- show seleted fields -->
<?php 
if(isset($usernumrecord))
{
 ?>
<table>
		<tr>
			<td colspan="2" align="right">
				<h3> 
				<?php
				   		echo $schooltypename = $this->classroommodel->fetchSchoolTypeName(@$schooltypeid1); // school type Name 
						echo '&nbsp&nbsp -> &nbsp';
						echo $schoolname = $this->classroommodel->fetchSchoolName(@$school_name_id1); // School Name 							 
						echo '&nbsp&nbsp -> &nbsp';
						echo $cid_resut_1 = $this->classroommodel->fetchGradeName(@$gradeid1); // Grade Name
						echo '&nbsp&nbsp -> &nbsp';
						echo @$classroom[0]['name'];
				?>
                
				</h3>
			</td>
		</tr>
</table>
<?Php
}
?>

<!-- End selected -->
<?Php

if(!empty($usernumrecord))
{


// GET RECENTLY 3 ASSESSMENTS
	
	$this->load->model('classroommodel');
	$data = $this->classroommodel->getrecentlyassignments();

	$assignmentname = array();
	$assignmentids = array();
	for($i=0;$i<count($data);$i++)
	{
	$assignmentname[] = $data[$i]['assignment_name'];
	$assignmentids[] = $data[$i]['id'];
	}
	
	
	if(!empty($assignmentname))
	{
		if(!empty($assignmentname[0]) && $assignmentname[0]!="")
		{
		$first = $assignmentname[0];
		}
		else
		{
			$first = '';
		}
		if(!empty($assignmentname[1]) && $assignmentname[1]!="")
		{
		$second = $assignmentname[1];
		}
		else
		{
			$second = '';
		}
		if(!empty($assignmentname[2]) && $assignmentname[0]!="")
		{
		$third = $assignmentname[2];
		}
		else
		{
			$third = '';
		}
		
		
	}
	
// GET RECENTLY 3 ASSESSMENTS


$numarr = array();
$namearr = array();
$uidarr = array();
foreach($usernumrecord as $key => $val)
{

	
	$namearr[] = $usernumrecord[$key][1];
	$uidarr[] = $usernumrecord[$key][2];
	
}



$this->load->model('classroommodel');
foreach($uidarr as $key => $val)
{
	$userid = $uidarr[$key];
	
	$numarr[] = $this->classroommodel->studentscoreByid($userid,$assignmentids);
	
	
}

		$bb = array();
		$b = array();
		$pf = array();
		
		$avgarr =  array();
// 1

if(!empty($numarr[0][0][0]['pass_score_point']) && $numarr[0][0][0]['pass_score_point']!="")
	{
  	$pass_score_point1_1 = round($numarr[0][0][0]['pass_score_point']);
	
	if(!empty($numarr[0][0][0]['assignment_id']) && $numarr[0][0][0]['assignment_id']!="")
	{
	  	$assessment_id = $numarr[0][0][0]['assignment_id'];
	
 		$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point1_1);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
				
 }
  

	
}
	else
	{
		$pass_score_point1_1 = 0;
	}

if(!empty($numarr[0][1][0]['pass_score_point']) && $numarr[0][1][0]['pass_score_point']!="")
	{
  		$pass_score_point1_2 = round($numarr[0][1][0]['pass_score_point']);
 
 	if(!empty($numarr[0][1][0]['assignment_id']) && $numarr[0][1][0]['assignment_id']!="")
	{

  	$assessment_id = $numarr[0][1][0]['assignment_id'];
 
 	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point1_2);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	}
  

}
 else
	{
		$pass_score_point1_2 = 0;
	}
	
	if(!empty($numarr[0][2][0]['pass_score_point']) && $numarr[0][2][0]['pass_score_point']!="")
	{
 		$pass_score_point1_3 = round($numarr[0][2][0]['pass_score_point']);
  
  if(!empty($numarr[0][2][0]['assignment_id']) && $numarr[0][2][0]['assignment_id']!="")
  {
  $assessment_id = $numarr[0][2][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point1_3);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
  }
  

}
else
{
		$pass_score_point1_3 = 0;
}
	//1 avg
	$avg_1 = round(($pass_score_point1_1+$pass_score_point1_2+$pass_score_point1_3)/3);
	$avgarr[] = $avg_1;
	
// 2

if(!empty($numarr[1][0][0]['pass_score_point']) && $numarr[1][0][0]['pass_score_point']!="")
	{
		
  $pass_score_point2_1 = round($numarr[1][0][0]['pass_score_point']);
  
  if(!empty($numarr[1][0][0]['assignment_id']) && $numarr[1][0][0]['assignment_id']!="")
  {
  $assessment_id = $numarr[1][0][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point2_1);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
  }
				

}
  else
	{
		$pass_score_point2_1 = 0;
	}

if(!empty($numarr[1][1][0]['pass_score_point']) && $numarr[1][1][0]['pass_score_point']!="")
	{
		

  		$pass_score_point2_2 = round($numarr[1][1][0]['pass_score_point']);
		
		if(!empty($numarr[1][1][0]['assignment_id']) && $numarr[1][1][0]['assignment_id']!="")
		{
		  $assessment_id = $numarr[1][1][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point2_2);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
		}
				

}
 else
	{
		$pass_score_point2_2 = 0;
	}


if(!empty($numarr[1][2][0]['pass_score_point']) && $numarr[1][2][0]['pass_score_point']!="")
	{
	
  		$pass_score_point2_3 = round($numarr[1][2][0]['pass_score_point']);
 	
	if(!empty($numarr[1][2][0]['assignment_id']) && $numarr[1][2][0]['assignment_id']!="")
	{
 		 $assessment_id = $numarr[1][2][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point2_3);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	}
				

	
}
  else
	{
		$pass_score_point2_3 = 0;
	}
	
	//2 avg
	$avg_2 = round(($pass_score_point2_1+$pass_score_point2_2+$pass_score_point2_3)/3);
	$avgarr[] = $avg_2;
// 3

if(!empty($numarr[2][0][0]['pass_score_point']) && $numarr[2][0][0]['pass_score_point']!="")
	{	
  $pass_score_point3_1 = round($numarr[2][0][0]['pass_score_point']);
  
  if(!empty($numarr[2][0][0]['assignment_id']) && $numarr[2][0][0]['assignment_id']!="")
	{
  
  $assessment_id = $numarr[2][0][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point3_1);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
				
	}

}
  else
	{
		$pass_score_point3_1 = 0;
	}


if(!empty($numarr[2][1][0]['pass_score_point']) && $numarr[2][1][0]['pass_score_point']!="")
	{
		
	  $pass_score_point3_2 = round($numarr[2][1][0]['pass_score_point']);
	  
	  if(!empty($numarr[2][1][0]['assignment_id']) && $numarr[2][1][0]['assignment_id']!="")
	  {
		  $assessment_id = $numarr[2][1][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point3_2);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	  }

}
else
	{
		$pass_score_point3_2 = 0;
	}

if(!empty($numarr[2][2][0]['pass_score_point']) && $numarr[2][2][0]['pass_score_point']!="")
	{

  $pass_score_point3_3 = round($numarr[2][2][0]['pass_score_point']);
  
  if(!empty($numarr[2][2][0]['assignment_id']) && $numarr[2][2][0]['assignment_id']!="")
  {
  $assessment_id = $numarr[2][2][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point3_3);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
  }

}
else
	{
		$pass_score_point3_3 = 0;
	}
	
	//3 avg
	$avg_3 = round(($pass_score_point3_1+$pass_score_point3_2+$pass_score_point3_3)/3);
	$avgarr[] = $avg_3;
// 4

if(!empty($numarr[3][0][0]['pass_score_point']) && $numarr[3][0][0]['pass_score_point']!="")
	{
		
 	 $pass_score_point4_1 = round($numarr[3][0][0]['pass_score_point']);
	 
	 if(!empty($numarr[3][0][0]['assignment_id']) && $numarr[3][0][0]['assignment_id']!="")
	 {
  $assessment_id = $numarr[3][0][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point4_1);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	 }

}
  else
	{
		$pass_score_point4_1 = 0;
	}


if(!empty($numarr[3][1][0]['pass_score_point']) && $numarr[3][1][0]['pass_score_point']!="")
	{
		
  		$pass_score_point4_2 = round($numarr[3][1][0]['pass_score_point']);
		
		if(!empty($numarr[3][1][0]['assignment_id']) && $numarr[3][1][0]['assignment_id']!="")
		{
			
		
  $assessment_id = $numarr[3][1][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point4_2);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
		}

	}
	else
	{
		$pass_score_point4_2 = 0;
	}
	
	
	if(!empty($numarr[3][2][0]['pass_score_point']) && $numarr[3][2][0]['pass_score_point']!="")
	{
	
	
  		$pass_score_point4_3 = round($numarr[3][2][0]['pass_score_point']);
		
		if(!empty($numarr[3][2][0]['assignment_id']) && $numarr[3][2][0]['assignment_id']!="")
		{
		
 		 $assessment_id = $numarr[3][2][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point4_3);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
				
		}
	}
	
 else
	{
		$pass_score_point4_3 = 0;
	}
	//4 avg
	$avg_4 = round(($pass_score_point4_1+$pass_score_point4_2+$pass_score_point4_3)/3);
	
	$avgarr[] = $avg_4;
// 5

if(!empty($numarr[4][0][0]['pass_score_point']) && $numarr[4][0][0]['pass_score_point']!="")
	{
		
 	 $pass_score_point5_1 = round($numarr[4][0][0]['pass_score_point']);
 	
	if(!empty($numarr[4][0][0]['assignment_id']) && $numarr[4][0][0]['assignment_id']!="")
	{
		
	 $assessment_id = $numarr[4][0][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point5_1);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	}
	}
	else
	{
		$pass_score_point5_1 = 0;
	}
	
if(!empty($numarr[4][1][0]['pass_score_point']) && $numarr[4][1][0]['pass_score_point']!="")
	{
		
 	$pass_score_point5_2 = round($numarr[4][1][0]['pass_score_point']);
	
	if(!empty($numarr[4][1][0]['assignment_id']) && $numarr[4][1][0]['assignment_id']!="")
	{
 	 $assessment_id = $numarr[4][1][0]['assignment_id'];
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point5_2);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	}
}
else
	{
		$pass_score_point5_2 = 0;
	}
	
	
if(!empty($numarr[4][2][0]['pass_score_point']) && $numarr[4][2][0]['pass_score_point']!="")
	{
			
  		$pass_score_point5_3 = round($numarr[4][2][0]['pass_score_point']);
		
		if(!empty($numarr[4][2][0]['assignment_id']) && $numarr[4][2][0]['assignment_id']!="")
		{
  			$assessment_id = $numarr[4][2][0]['assignment_id']; 
  
  	$res = mysql_query('select * from proficiency where assessment_id="'.$assessment_id.'"');
					$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = round($pass_score_point5_3);
					
			
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bb[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
		}
	}
	else
	{
		$pass_score_point5_3 = 0;
	}
 //5 avg
	$avg_5 = round(($pass_score_point5_1+$pass_score_point5_2+$pass_score_point5_3)/3);
	$avgarr[] = $avg_5;	
	

if(empty($bb))
			{
				$finalbb=0;
			}
			else
			{
				$total1 = count($bb);
				$tt1 = array_sum($bb);
				$finalbb = $tt1/$total1;
			}
			
			if(empty($b))
			{
				$finalb=0;
			}
			else
			{
				$total2 = count($b);
				$tt2 = array_sum($b);
				$finalb = $tt2/$total2;	
			}
			
			if(empty($pf))
			{
			$finalpf=0;	
			}
			else
			{
				$total3 = count($pf);
				$tt3 = array_sum($pf);
				$finalpf = $tt3/$total3;
			}
			
			
	 $totalval = $finalbb + $finalb + $finalpf;
	
	
	
	if($finalbb!=0)
	{
		$frstval = ($finalbb/$totalval)*100;
	}
	else
	{
	$frstval=0;
	}
	if($finalb!=0)
	{
		$secval = ($finalb/$totalval)*100;
	}
	else
	{
	$secval=0;
	}
	if($finalpf!=0)
	{
		$thrdval = ($finalpf/$totalval)*100;
	}
	else
	{
		$thrdval=0;
	}
	
	 $finalbb=round($frstval);	
	 $finalb=round($secval);
	 $finalpf=round($thrdval);


$ncat = implode("','",$namearr);

$ncat = "['','".$ncat."']";

/*
?>

<b><font color="#2f7ed8">Below Basic = <?Php echo @$finalbb; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#0d233a">Basic  =  <?Php echo @$finalb; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#8bbc21">Proficient =<?Php echo @$finalpf; ?>% </font></b>
<?Php
*/
?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: <?Php echo @$ncat; ?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' %';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
			
        
            series: [
			{
				type: 'column',
				name: '',
				data: [,,,,],
			},
			{
                type: 'column',
                name: '<?php echo $first;?>',
                data: [0,<?Php echo @$pass_score_point1_1; ?>, <?Php echo @$pass_score_point2_1; ?>, <?Php echo @$pass_score_point3_1; ?>, <?Php echo @$pass_score_point4_1; ?>, <?Php echo @$pass_score_point5_1; ?>]
            }, {
                type: 'column',
                name: '<?php echo  $second;?>',
                data: [0,<?Php echo @$pass_score_point1_2; ?>, <?Php echo @$pass_score_point2_2; ?>, <?Php echo @$pass_score_point3_2; ?>, <?Php echo @$pass_score_point4_2; ?>, <?Php echo @$pass_score_point5_2; ?>]
            }, {
                type: 'column',
                name: '<?php echo $third;?>',
                data: [0,<?Php echo @$pass_score_point1_3; ?>, <?Php echo @$pass_score_point2_3; ?>, <?Php echo @$pass_score_point3_3; ?>, <?Php echo @$pass_score_point4_3; ?>, <?Php echo @$pass_score_point5_3; ?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [0,<?php echo @$avg_1; ?>, <?php echo @$avg_2; ?>, <?php echo @$avg_3; ?>, <?php echo @$avg_4; ?>, <?php echo $avg_5; ?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }, {
                type: 'pie',
                name: 'Total consumption',
                data: [{
                    name: 'Below Basic',
                    y: <?Php echo @$finalbb; ?>,
                    color: Highcharts.getOptions().colors[0] // Below Basic's color
                }, {
                    name: 'Basic Scores ',
                    y: <?Php echo @$finalb; ?>,
                    color: Highcharts.getOptions().colors[1] // Basic's color
                }, {
                    name: 'Proficient',
                    y: <?Php echo @$finalpf; ?>,
                    color: Highcharts.getOptions().colors[2] // Proficient's color
                }],
                center: [25, 10],
                size: 100,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    });
    

		</script>
	
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
		<!-- End Graph -->			
		<?Php } 
		else
		{		
			if(isset($usernumrecord))
			{
		?>
			<script type="text/javascript">
			alert("No Record Found");
			</script>
		<?php
			}
			echo $this->session->flashdata('item');			
		
		}
		
		
		?>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
