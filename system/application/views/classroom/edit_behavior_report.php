﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />



<link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/app.css">
<script>
    var base_url = '<?php echo base_url();?>';
</script>    
<!-- Ends here -->  
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
      <?php require_once($view_path.'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid"> 
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          
          <h3 class="page-title"> <i class="icon-group"></i>&nbsp; Classroom Management </h3>
          <ul class="breadcrumb" >
            <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
            <li> <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>classroom">Classroom Management</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>classroom/behavior_record">Behavior Running Record</a> <span class="divider">></span> </li>
            <li> </i> <a href="<?php echo base_url();?>classroom/create_behavior">Create</a> </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <div class="row-fluid">
        <div class="span12">
        <!-- BEGIN BLANK PAGE PORTLET-->
        <div class="widget yellow">
        <div class="widget-title">
          <h4>Create Behavior Running Record</h4>
        </div>
        <div class="widget-body">
        <form class="form-horizontal" method="post" action="<?php echo base_url().'classroom/update_behavior_records';?>" name="update_behavior_form" id="update_behavior_form">
        <div id="pills" class="custom-wizard-pills-yellow2">
        <ul>
          <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
          <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
          <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
          <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
        </ul>
        <div class="progress progress-success-yellow progress-striped active">
          <div class="bar"></div>
        </div>
        <div class="tab-content">
        
        <!-- BEGIN STEP 1-->
        <div class="tab-pane" id="pills-tab1">
        <h3 style="color:#000000;">STEP 1</h3>
        
          <?php if ($this->session->userdata('login_type') == 'user'){ ?>
                                            <div class="control-group">
                                                <label class="control-label">Select School</label>
                                                <div class="controls">
                <select class="span12 chzn-select" name="school_id" id="school_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value=""></option>
                                                        <?php foreach ($schools as $school){ ?>
                                                         <?php echo $location = $incident_details['incident'][0]->school_id;
															if($location == $school['school_id'])
															{?>
                  <option value="<?php echo $school['school_id']; ?>" selected><?php echo $school['school_name']; ?></option>
                  <?php }else{?>
                   <option value="<?php echo $school['school_id']; ?>"><?php echo $school['school_name']; ?></option>
                                                        <?php }}?>
                                                    </select>
                                                </div>
                                            </div>
                                 <?php } ?>
        
        <div class="control-group">
        <label class="control-label">Select Location</label>
          <input type="hidden" name="behavior_id" id="behavior_id" value="<?php echo $incident_details['incident'][0]->behavior_id ?>">
        <div class="controls">
          <select class="span12 chzn-select" name="behavior_location" id="behavior_location" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
             <option value=''></option>
             <option value="0">All</option>
 				<?php if(!empty($behavior_location)) { 
			  foreach($behavior_location as $behavior_location_value)
			  {?>
               <?php echo $location = $incident_details['incident'][0]->behavior_location;
				if($location == $behavior_location_value->id)
				{?>
<option value="<?php echo $behavior_location_value->id;?>" selected><?php echo $behavior_location_value->behavior_location;?></option>
<?php }else{?>
<option value="<?php echo $behavior_location_value->id;?>"><?php echo $behavior_location_value->behavior_location;?></option>
			  <?php	}} } ?>			   
		      </select>
        </div>
      </div>
      
          <div class="control-group">
            <label class="control-label">Select Date</label>
            <div class="controls">
              <input id="dp1" type="text" value="<?php echo $incident_details['date'];?>" name="date" size="16" class="m-ctrl-medium">
              </input>
            </div>
          </div>
          
            <div class="control-group">
            <label class="control-label">Select Time</label>
            <div class="controls">
              <input id="timepicker1" type="text" value="<?php echo $incident_details['incident'][0]-> time;?>" name="time" size="16" class="m-ctrl-medium">
              </input>
            </div>
          </div>
          
          <div class="control-group">
            <label class="control-label">Select Victim</label>
            <div class="controls col-md-5">
              <!--<input id="demo5" type="text" class="col-md-12 form-control" placeholder="Search cities..." autocomplete="off" data-role="tagsinput" />-->
            

           <select data-placeholder="--Select Victim--" class="chzn-select " multiple="multiple" tabindex="6" id="victim_ids" name="victim_ids[]" style="width:500px;">
                    <option>Select Victim</option>
                </select>
<!--                <input type="text" value="" data-role="tagsinput" placeholder="Add tags" id="victim" />-->


            </div>
            
          </div>
            
            <div class="control-group">
            <label class="control-label">Select Suspect</label>
            <div class="controls col-md-5">
              <!--<input id="demo5" type="text" class="col-md-12 form-control" placeholder="Search cities..." autocomplete="off" data-role="tagsinput" />-->
            

                <select data-placeholder="--Select Suspect--" class="chzn-select " multiple="multiple" tabindex="6" id="suspect_ids" name="suspect_ids[]" style="width:500px;">
                    <option>Select Suspect</option>
                </select>
<!--                <input type="text" value="" data-role="tagsinput" placeholder="Add tags" id="victim" />-->


            </div>
            
          </div>
          
                                                
          </div>
          
          <!-- BEGIN STEP 2-->
          <div class="tab-pane" id="pills-tab2">
            <h3 style="color:#000000">STEP 2</h3>
            <div class="space20"></div>
            <h4><b>Problem Behavior(Additional Action/forms may be required)</b></h4>
            <div class="space20"></div>
            <div>
              <div class="control-group">
                <div class="space15"></div>
<?php foreach($behaviour_data as $behaviour){
	$selected_problem = "";
?>
                  
<?php 
	foreach($incident_details['incident'] as $problem){
		if($problem->problem_id==$behaviour->id){
			$selected_problem = "checked='checked'";	
			break;
		} 

		
	}?>

      
                    <div class="span3">
                    <div>
                    <input type="checkbox" class="checkboxes" id="problem_behaviour" value="<?php echo $behaviour->id;?>" name="problem_behaviour[]"  <?php echo $selected_problem;?>/>
                  </div>
                  <div style="padding:0px; margin:-18px 0px 0px 25px" ><?php echo $behaviour->behaviour_name;?></div>
                    </div>
                  <?php  }?>
               
              </div>
              
              
              <div class="control-group">
                  <label class="control-label"><b>About Incident</b></label>
                                <div class="controls">
                                    <textarea class="span12 ckeditor" rows="5" name="incident"><?php echo $incident_details['incident'][0]-> description;?></textarea>
                                </div>
                            </div> 
              
            </div>
          </div>
          <!-- BEGIN STEP 3-->
          <div class="tab-pane" id="pills-tab3">
            <h3 style="color:#000000">STEP 3</h3>
            <div class="space20"></div>
            <h4><b>Possible Motivation</b></h4>
            <div>
              <div class="control-group">
                <div class="space15"></div>
               
                  <?php foreach($possible_motivation as $motivation){
						$selected_problem = "";
					?>
                  
<?php 
	foreach($incident_details['motivation'] as $motivations){
		if($motivations->motivation_id == $motivation->id){
			$selected_problem = "checked='checked'";	
			break;
		} 

		
	}?>

                    <div class="span3">
                  <div>
                    <input type="checkbox" class="checkboxes" value="<?php echo $motivation->id;?>" <?php echo $selected_problem;?> name="possible_motivation[]" id="possible_motivation" />
                  </div>
                  <div style="padding:0px; margin:-18px 0px 0px 25px" ><?php echo $motivation->possible_motivation;?></div>
                    </div>
                  <?php }?>
                
              </div>
            </div>
            <h4><b>Interventions Prior To Office Discipline Referral</b></h4>
            <div>
              <div class="control-group">
                <div class="space15"></div>
                                <?php foreach($interventions_prior as $interventions){
										$selected_problem = "";
									?>
                  
<?php 
	foreach($incident_details['intervention'] as $intervention){
		if($intervention->interventions_id == $interventions->id){
			$selected_problem = "checked='checked'";	
			break;
		} 

		
	}?>
                  
                  
                  <div class="span3">
                  <div>
                 <input type="checkbox" class="checkboxes" value="<?php echo $interventions->id;?>" <?php echo $selected_problem;?> name="interventions_prior[]" id="interventions_prior" />
                  </div>
                  <div style="padding:0px; margin:-18px 0px 0px 25px" ><?php echo $interventions->interventions_prior;?></div>
                    </div>
                  <?php  }?>
              </div>
              <div class="space20"></div>
            </div>
          </div>
          
          <!--BEGIN STEP 4 -->
          <div class="tab-pane" id="pills-tab4">
            <h3 style="color:#000000">STEP 4</h3>
            <div class="space20"></div>
            <h4><b>If Student Has Multiple Referrals,Referred To</b></h4>
            <div>
              <div class="control-group">
                <div class="space15"></div>
         <?php foreach($student_has_multiple_referrals as $multiple_referrals){
			$selected_problem = "";
		?>
                  
<?php 
	foreach($incident_details['referral'] as $referral){
		if($referral->referral_id == $multiple_referrals->id){
			$selected_problem = "checked='checked'";	
			break;
		} 
	}?>
                 <div class="span3">
                  <div>
               <input type="checkbox" class="checkboxes" value="<?php echo $multiple_referrals->id;?>" <?php echo $selected_problem;?> name="student_has_multiple_referrals[]" id="student_has_multiple_referrals" />
                  </div>
                  <div style="padding:0px; margin:-18px 0px 0px 25px" ><?php echo $multiple_referrals->student_has_multiple_referrals;?></div>
                 </div>
                  <?php }?>
               </div>
              <div class="space20"></div>
              <center>
                  <button class="btn btn-large btn-yellow" type="submit" name="submit" value="submit"><i class="icon-save icon-white"></i> Save File</button>
              </center>
              <div class="space20"></div>
              <div class="space20"></div>
              <div class="space20"></div>
              <center>
                <button class="btn btn-large btn-yellow"><i class="icon-print icon-white"></i> Print</button>
                <button class="btn btn-large btn-yellow"><i class="icon-envelope icon-white"></i> Send to Parent</button>
                <button class="btn btn-large btn-yellow"><i class="icon-envelope icon-white"></i> Send to Colleague</button>
              </center>
            </div>
          </div>
          <ul class="pager wizard">
            <li class="previous first yellow"><a href="javascript:;">First</a></li>
            <li class="previous yellow"><a href="javascript:;">Previous</a></li>
            <li class="next last yellow"><a href="javascript:;">Last</a></li>
            <li class="next yellow"><a  href="javascript:;">Next</a></li>
          </ul>
          </div>
          </div>
        </form>
      </div>
    </div>
    <!-- END BLANK PAGE PORTLET--> 
  </div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
<script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 

      

<!--common script for all pages--> 
<script src="<?php echo SITEURLM?>js/common-scripts.js"></script> 
<!--script for this page--> 
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script> 
<script src="<?php echo SITEURLM?>js/form-component.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script> 


<script src="<?php echo SITEURLM?>js/jquery.mockjax.js"></script>

<!-- <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/bootstrap-typeahead.js"></script>
<script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
 <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/hogan-2.0.0.js"></script>-->
<!--
<script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/angular.min.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/prettify.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/bootstrap-tagsinput-angular.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/typeahead.min.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/hogan-2.0.0.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/app_bs3.js"></script>
    <script src="<?php echo SITEURLM?>js/plugins/bootstrap-tagsinput/app.js"></script>-->
    <script src="<?php echo SITEURLM?>js/ajax-chosen.js"></script> 

    

<!--script for this page only--> 

<script>
$(document).ready(function () {
      $("#victim_ids").ajaxChosen({
         type: 'post',
         url: '<?php echo base_url();?>classroom/autocompleteuser',
         dataType: 'json'
      },
      function (data)
      {
         var terms = {};
 
         $.each(data, function (i, val) {
            terms[i] = val;
         });
 
         return terms;
      }).change(function () {
         //you can see the IDs in console off all items in autocomplete and deal with them
         console.log($("#jacComplete").val());
      });
      
       $("#suspect_ids").ajaxChosen({
         type: 'post',
         url: '<?php echo base_url();?>classroom/autocompleteuser',
         dataType: 'json'
      },
      function (data)
      {
         var terms = {};
 
         $.each(data, function (i, val) {
            terms[i] = val;
         });
 
         return terms;
      }).change(function () {
         //you can see the IDs in console off all items in autocomplete and deal with them
         console.log($("#jacComplete").val());
      });
   });
    
</script>    
</body>
<!-- END BODY -->
</html>