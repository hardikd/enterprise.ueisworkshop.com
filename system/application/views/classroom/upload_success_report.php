<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/success_team">Student Success Team</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>classroom/upload_success_report">Upload Report</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Upload Report</h4>
                          
                         </div>
                   <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-yellow progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                 
                      <div style="max-width:100%; overflow-x:scroll ; overflow-y: visible; padding-bottom:20px; ">            
  						<div class="space15"></div>
                                 <table class="table table-striped table-hover table-bordered" id="editable-sample" style="min-width:3000px;">
                                     <thead>
                                     <tr>
										 <th>Id</th>	                                     
                                         <th>Student Name</th>
                                         <th>Edit</th>
                                         <th>Grade Name</th>
                                         <th>Parent Strengths</th>
                                         <th>Teacher Strengths</th>
                                         <th>Family Background Information</th>
                                         <th>School Background Information </th>
                                         <th>Health Background Information</th>
                                         <th>Needs Home </th>
                                         <th>Needs School </th>
                                         <th>Needs Medical </th>
                                         <th>Actions Home </th>
                                         <th>Actions School </th>
                                         <th>Actions Medical </th>
                                         <th>Home Responsible </th>
                                         <th>School Responsible </th>
                                         <th>Medical Responsible</th>
                                         
                                         <?php 
foreach($update_report as $retrieve_report)
{?>   
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <tr class="">
                                     	<td class="name"><?php echo $retrieve_report->id;?></td>
                                   		 <td class="name"> <?php echo ($retrieve_report->firstname . ' ' . $retrieve_report->lastname . ''); ?></td>
  <td><a class="edit" target="_blank" href="<?php echo base_url().'classroom/update_success_data/'.$retrieve_report->id?>">Edit</a></td>
                                         <td><?php echo $retrieve_report->grade_name;?> </td>
                                         <td><?php echo unserialize($retrieve_report->strengths_parent);?> </td>
                                         <td><?php echo unserialize($retrieve_report->strengths_teacher);?> </td>
                                         <td><?php echo unserialize($retrieve_report->information_family);?></td>
                                         <td><?php echo unserialize($retrieve_report->information_school);?> </td>
                                         <td><?php echo unserialize($retrieve_report->information_health);?> </td>
                                         <td><?php echo unserialize($retrieve_report->needs_home);?> </td>
                                         <td><?php echo unserialize($retrieve_report->needs_school);?> </td>
                                         <td><?php echo unserialize($retrieve_report->needs_medical);?> </td>
                                         <td><?php echo unserialize($retrieve_report->action_home);?> </td>
                                         <td><?php echo unserialize($retrieve_report->action_school);?> </td>
                                         <td><?php echo unserialize($retrieve_report->action_medical);?> </td>
                                           <td><?php echo unserialize($retrieve_report->action_home_responsible);?> </td>
                                         <td><?php echo unserialize($retrieve_report->action_school_responsible);?> </td>
                                         <td><?php echo unserialize($retrieve_report->action_medical_responsible);?> </td>
                                       
                                        
                                        
                                     </tr>
                                     <?php }?>
                                    
                                     </tbody>
                                 </table>
                                 
                                 </div>       
                                   
                                     </div>
                           
                                     </div>
                                    
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
<script>
<!--start Strengths text -->	
	$(function() {
	var addDiv = $('#strengths');
	var i = $('#strengths p').size() + 1;
	 
	$('#addNew').live('click', function() {
	$('<p></br><input type="text" id="parent_' + i +'" size="16" class="m-ctrl-medium" name="parent[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	
	$(function() {
	var addDiv = $('#strengths_text');
	var i = $('#strengths_text p').size() + 1;
	 
	$('#addNew_text').live('click', function() {
	$('<p></br><input type="text" id="teacher_' + i +'" size="16" class="m-ctrl-medium" name="teacher[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	 
	 <!--end Strengths text -->
	 
	 <!--start Background Information text -->
	
<!--create family text box -->
	
	$(function() {
	var addDiv = $('#background');
	var i = $('#background p').size() + 1;
	 
	$('#backgroundNew').live('click', function() {
	$('<p></br><input type="text" id="family_' + i +'" size="16" class="m-ctrl-medium" name="family[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!--end family text box -->	
<!--create school text box -->
	$(function() {
	var addDiv = $('#background_text');
	var i = $('#background_text p').size() + 1;
	 
	$('#backgroundNew_text').live('click', function() {
	$('<p></br><input type="text" id="school_' + i +'" size="16" class="m-ctrl-medium" name="school[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	<!--end scool text box -->
<!--create health text box -->
	$(function() {
	var addDiv = $('#health_text');
	var i = $('#health_text p').size() + 1;
	 
	$('#healthNew_text').live('click', function() {
	$('<p></br><input type="text" id="health_' + i +'" size="16" class="m-ctrl-medium" name="health[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="rem_healthNew_New_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#rem_healthNew_New_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!-- end health text box -->	
	 <!--end Background Information text-->

<!--create needs text box -->
	
	$(function() {
	var addDiv = $('#needs');
	var i = $('#needs p').size() + 1;
	 
	$('#needsNew').live('click', function() {
	$('<p></br><input type="text" id="needs_home_' + i +'" size="16" class="m-ctrl-medium" name="needs_home[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!--end family text box -->	
<!--create school text box -->
	$(function() {
	var addDiv = $('#needs_text');
	var i = $('#needs_text p').size() + 1;
	 
	$('#needsNew_text').live('click', function() {
	$('<p></br><input type="text" id="needs_school_' + i +'" size="16" class="m-ctrl-medium" name="needs_school[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	<!--end scool text box -->
<!--create health text box -->
	$(function() {
	var addDiv = $('#medical_text');
	var i = $('#medical_text p').size() + 1;
	 
	$('#medicalNew_text').live('click', function() {
	$('<p></br><input type="text" id="needs_medical_' + i +'" size="16" class="m-ctrl-medium" name="needs_medical[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="rem_healthNew_New_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#rem_healthNew_New_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!-- end health text box -->	

<!--create Action text box -->
	
	$(function() {
	var addDiv = $('#action');
	var i = $('#action p').size() + 1;
	 
	$('#actionNew').live('click', function() {
	$('<p></br><input type="text" id="action_home_' + i +'" size="16" class="m-ctrl-medium" name="action_home[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!--end family text box -->	
<!--create school text box -->
	$(function() {
	var addDiv = $('#action_text');
	var i = $('#action_text p').size() + 1;
	 
	$('#actionNew_text').live('click', function() {
	$('<p></br><input type="text" id="action_school_' + i +'" size="16" class="m-ctrl-medium" name="action_school[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	<!--end scool text box -->
<!--create health text box -->
	$(function() {
	var addDiv = $('#health_act_text');
	var i = $('#health_act_text p').size() + 1;
	 
	$('#health_act_New_text').live('click', function() {
	$('<p></br><input type="text" id="action_medical_' + i +'" size="16" class="m-ctrl-medium" name="action_medical[]" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="rem_healthNew_New_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#rem_healthNew_New_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!-- end health text box -->	
	 
	</script>   
   
 
</body>
<!-- END BODY -->
</html>