<style type="text/css">
    table.gridtable { 
        font-size:11px;
        color:#333333;
        border-width: 1px;
        border-color: #666666;
        border-collapse: collapse;
    }
    table.gridtable th {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #dedede;
    }
    table.gridtable td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #ffffff;
        width:46.2px;}
    .blue{
        font-size:9px;
    }

</style>


<page backtop="4mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
    <page_header> 
        <div style="padding-left:610px;position:relative;top:-40px;">
            <img alt="Logo"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
            <div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:7px;">
                <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/calendar.png"/>&nbsp;Student Success Team</b></div>
        </div>
    </page_header> 
    <page_footer> 
        <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor; ?>;"><?php echo $dis; ?></div>
        <br />
        <table style="margin-left:10px;">
            <tr>
                <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                </td>
                <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:270px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Page [[page_cu]] of [[page_nb]] 
                </td>
                <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Printed on: <?php echo date('F d,Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer> 


    <table cellspacing="0" style="width:750px;border:2px #939393 solid;">
        <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
            <td style="padding-left:5px;padding-top:2px;height:36px;" >
                <b> <?php echo ucfirst($this->session->userdata('district_name')); ?> | <?php if ($this->session->userdata('login_type') == 'teacher'): ?>
                        <?php echo ucfirst($this->session->userdata('school_self_name')); ?>
                    <?php else: ?>
                        <?php echo ucfirst($this->session->userdata('school_name')); ?>
                    <?php endif; ?></b></td>
            <td style="text-align:right;padding-right:10px;">
                <b>Meeting Date:</b> <?php echo date('F d, Y', strtotime($success_data['details']->date)); ?>
            </td>
            <!--                            <br /><br />-->


        </tr>
        
        <tr style="margin-top:20px;">
            <td style="padding-left:10px;width:350px;color:#939393; vertical-align: top;">
                <br />
                <b>Type of Meeting:</b> <?php echo $success_data['meeting_type'][0]->progress_monitoring; ?>   
                <br />
                <br />
                <b>Student:</b> <?php echo $studentname; ?>
                <br />
                <br />
                <b>Reason for Referral:</b> <?php echo $success_data['referral_reason'][0]->referral_reason; ?> 
                <br />
                <br />
                <b>Grade:</b> <?php echo $success_data['details']->grade_name; ?>
                <br />
                <br />
                <?php if ($teachername[0]['firstname']!=''):?>
                <b>Teacher:</b> <?php echo $teachername[0]['firstname'] . ' ' . $teachername[0]['firstname']; ?>   
                <br />
                <br />
            <?php else:?>
                <b>Observer:</b> <?php echo $this->session->userdata('observer_name'); ?>   
                <br />
                <br />
            <?php endif;?>
                <b>Program Change:</b> <?php echo $success_data['program_change']; ?> 
                <br />
                <br />
                <b>Next Meeting Date:</b> <?php echo date('F d, Y', strtotime($success_data['details']->next_date)); ?>
                <br />
                <br />
                

            </td>
            <td style="padding-left:10px;width:350px;color:#939393;height:130px;">
                <table style="border:1px solid #939393;height:130px;width:340px;">
                    <tr>
                        <td style="width:340px;height:165px;vertical-align: top;">
                            <b>Description:</b> <?php echo $des;?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        
    </table>
    <br /><br />
    <!--Strength start here-->
    <table class="gridtable" style="width:750px;" >

        <tr >
            <td colspan="2" style="width:720px;text-align: center;">
                <b>STUDENT STRENGTHS
                </b> 
            </td>
        </tr >
        <tr style="background:#939393;height: 10px;">
            <td style="background:#939393;text-align: center;vertical-align: middle;width:325px;font-size:9px;"><b>Student Strengths - Family Representative</b></td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:325px;font-size:9px;"><b>Student Strengths - School Representative</b></td>
        </tr>
        <?php
        $strenghtParent = explode('|', unserialize($success_data['details']->strengths_parent));
        $strenghtTeacher = explode('|', unserialize($success_data['details']->strengths_teacher));

        if (count($strenghtParent) > count($strenghtTeacher)) {
            $cntstrength = count($strenghtParent);
        } else {
            $cntstrength = count($strenghtTeacher);
        }
        if ($cntstrength < 5) {
            $cntstrength = 5;
        }
        for ($box = 0; $box < $cntstrength; $box++):
            ?>
            <tr>
                <td style="width:300px;font-size:9px;vertical-align: top;">
                    <div> <?php echo $strenghtParent[$box]; ?>&nbsp; </div>
                </td>
                <td style="width:300px;font-size:9px;vertical-align: top;">
                    <div> <?php echo $strenghtTeacher[$box]; ?>&nbsp; </div>
                </td>
            </tr>
<?php endfor; ?>

    </table>

    <!--Strength ends here-->
    <br />
    <br />
    <!--Background INFO starts-->

    <table class="gridtable" style="width:750px;">

        <tr >
            <td colspan="3" style="width:720px;text-align: center;">
                <b>STUDENT INFORMATION 
                </b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;"><b>Student Information. - Family Representative</b></td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;"><b>Student Information. - School Representative</b></td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;padding-top:15px;"><b>Student Information. - Health Care Representative</b></td>
        </tr>
        <?php
        $cntstrength = 0;
        $infFamily = explode('|', unserialize($success_data['details']->information_family));
        $infSchool = explode('|', unserialize($success_data['details']->information_school));
        $infHealth = explode('|', unserialize($success_data['details']->information_health));
        

        if (count($infFamily) > count($infSchool)) {
            if (count($infFamily) > count($infHealth)) {
                $cntstrength = count($infFamily);
            } else {
                $cntstrength = count($infHealth);
            }
        } else {
            if (count($infSchool) > count($infHealth)) {
                $cntstrength = count($infSchool);
            } else {
                $cntstrength = count($infHealth);
            }
        }
        if ($cntstrength < 5) {
            $cntstrength = 5;
        }
        for ($box = 0; $box < $cntstrength; $box++):
            ?>
            <tr>
                <td style="width:200px;font-size:9px;vertical-align: top;">
                    <?php echo $infFamily[$box]; ?>&nbsp;
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
                    <?php echo $infSchool[$box]; ?>&nbsp;
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
            <?php echo $infHealth[$box]; ?>&nbsp;
                </td>
            </tr>
<?php endfor; ?>
    </table>

    <!--Background INFO ends-->
    <br />
    <br />
    <!--Needs starts-->
    <table  class="gridtable" style="width:750px;" >

        <tr >
            <td colspan="3" style="width:720px;text-align: center;">
                <b>STUDENT CHALLENGES/ NEEDS 
                </b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;"><b>Student Needs - Family</b></td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;"><b>Student Needs - School</b></td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;"><b>Student Needs - Health Care</b></td>
        </tr>
        <?php
        if (count($success_data['action_home']) > count($success_data['action_school'])) {
            if (count($success_data['action_home']) > count($success_data['action_medical'])) {
                $cntNeed = count($success_data['action_home']);
            } else {
                $cntNeed = count($success_data['action_medical']);
            }
        } else {
            if (count($success_data['action_school']) > count($success_data['action_medical'])) {
                $cntNeed = count($success_data['action_school']);
            } else {
                $cntNeed = count($success_data['action_medical']);
            }
        }
        if ($cntNeed < 5) {
            $cntNeed = 4;
        }
        ?>
                <?php for ($box = 0; $box <= $cntNeed; $box++): ?>
            <tr>
                <td style="width:200px;font-size:9px;vertical-align: top;">
    <?php echo $success_data['need_home'][$box]->intervention_strategies_home; ?>&nbsp;

                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
                    <?php echo $success_data['need_school'][$box]->intervention_strategies_school; ?>&nbsp;
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
            <?php echo $success_data['need_medical'][$box]->intervention_strategies_medical; ?>&nbsp;
                </td>
            </tr>
<?php endfor; ?>
    </table>
    <!--Needs End-->
</page>
<page backtop="4mm" backbottom="17mm" backleft="1mm" backright="10mm">
    <page_header> 
        <div style="padding-left:610px;position:relative;top:-40px;">
            <img alt="Logo"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
            <div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:7px;">
                <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/calendar.png"/>&nbsp;Student Success Team</b></div>
        </div>
    </page_header> 
    <page_footer> 
        <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor; ?>;"><?php echo $dis; ?></div>
        <br />
        <table style="margin-left:10px;">
            <tr>
                <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                </td>
                <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Page [[page_cu]] of [[page_nb]] 
                </td>
                <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Printed on: <?php echo date('F d,Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer> 
    <table class="gridtable" style="width:750px;" >

        <tr >
            <td colspan="3" style="width:720px;text-align: center;">
                <b>HOME ACTION PLAN 
                </b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Home Need</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Home Action</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Responsible Representative</b>
            </td>
        </tr>
        <?php
        $ahcntr = 0;
        foreach ($success_data['action_home'] as $key => $action_home_need):
            ?>
            <tr>

                <td style="width:200px;font-size:9px;vertical-align: top;">
                    <?php echo $action_home_need['action_need_home']; ?>
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
                    <?php echo $action_home_need['action_action_home']; ?>
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
            <span><?php echo $action_home_need['action_responsible_home']; ?></span>&nbsp;<span style="float:right;"><b style="float:right;">Date:</b><?php echo $action_home_need['action_date_home']; ?></span>
                </td>         
            </tr>
    <?php $ahcntr++;
endforeach; ?>
<?php if ($ahcntr < 6): ?>
    <?php for ($i = $ahcntr; $i < 6; $i++): ?>
                <tr>

                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>
                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>
                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>         
                </tr>
    <?php endfor; ?>

<?php endif; ?>
    </table>

    <br />
    <br />
    <table class="gridtable" style="width:750px;" >

        <tr >
            <td colspan="3" style="width:720px;text-align: center;">
                <b>SCHOOL ACTION PLAN</b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>School Need</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>School Action</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Responsible Representative</b>
            </td>
        </tr>
<?php
$asncntr = 0;
foreach ($success_data['action_school'] as $key => $action_school_need):
    ?>
            <tr>

                <td style="width:200px;font-size:9px;vertical-align: top;">
                    <?php echo $action_school_need['action_need_school']; ?>
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
            <?php echo $action_school_need['action_action_school']; ?>
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
            <?php echo $action_school_need['action_responsible_school']; ?>&nbsp;<b style="float:right;">Date:</b><?php echo $action_school_need['action_date_school']; ?>
                </td>         
            </tr>
    <?php $asncntr++;
endforeach; ?>
<?php if ($asncntr < 6): ?>
    <?php for ($i = $asncntr; $i < 6; $i++): ?>
                <tr>

                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>
                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>
                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>         
                </tr>
    <?php endfor; ?>
<?php endif; ?>
    </table>
    <br />
    <br />
    <table class="gridtable" style="width:750px;">

        <tr >
            <td colspan="3" style="width:720px;text-align: center;">
                <b>HEALTH CARE ACTION PLAN</b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Health Care Need</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Health Care Action</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>Responsible Representative</b>
            </td>
        </tr>
                <?php
                $amncntr = 0;
                foreach ($success_data['action_medical'] as $key => $action_medical_need):
                    ?>
            <tr>

                <td style="width:200px;font-size:9px;vertical-align: top;">
    <?php echo $action_medical_need['action_need_medical']; ?>
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
            <?php echo $action_medical_need['action_action_medical']; ?>
                </td>
                <td style="width:200px;font-size:9px;vertical-align: top;">
    <?php echo $action_medical_need['action_responsible_medical']; ?>&nbsp;<b style="float:right;">Date:</b><?php echo $action_medical_need['action_date_medical']; ?>
                </td>         
            </tr>
    <?php $amncntr++;
endforeach; ?>
<?php if ($amncntr < 6): ?>
    <?php for ($i = $amncntr; $i < 6; $i++): ?>
                <tr>

                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>
                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>
                    <td style="width:200px;font-size:9px;">
                        &nbsp;
                    </td>         
                </tr>
    <?php endfor; ?>
<?php endif; ?>
    </table>

    <br />
    <br />
    <table class="gridtable" style="width:750px;">

        <tr >
            <td colspan="3" style="width:720px;text-align: center;">
                <b>MEETING ATTENDEES</b> 
            </td>
        </tr>
        <tr>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>&nbsp;</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>&nbsp;</b>
            </td>
            <td style="background:#939393;text-align: center;vertical-align: middle;width:200px;font-size:9px;">
                <b>&nbsp;</b>
            </td>
        </tr>

        <tr>

            <td style="width:200px;font-size:9px;vertical-align: top;">
                Guardian:
            </td>
            <td style="width:200px;font-size:9px;vertical-align: top;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;vertical-align: top;">
                School Rep:
            </td>         
        </tr>

        <tr>

            <td style="width:200px;font-size:9px;">
                Guardian:
            </td>
            <td style="width:200px;font-size:9px;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;">
                Health Care Rep:
            </td>         
        </tr>
        <tr>

            <td style="width:200px;font-size:9px;">
               Administrator:
            </td>
            <td style="width:200px;font-size:9px;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;">
                Health Care Rep:
            </td>         
        </tr>
        <tr>

            <td style="width:200px;font-size:9px;">
                Administrator:
            </td>
            <td style="width:200px;font-size:9px;">
                Teacher:
            </td>
            <td style="width:200px;font-size:9px;">
                Other:
            </td>         
        </tr>

    </table>

</page>
<!--Action Starts-->

<!--Action Ends-->