<style type="text/css">
    table.gridtable { 
        font-size:11px;
        color:#333333;
        border-width: 1px;
        border-color: #666666;
        border-collapse: collapse;
    }
    table.gridtable th {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #dedede;
    }
    table.gridtable td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #ffffff;
        width:46.2px;}
    .blue{
        font-size:9px;
    }

</style>


<page backtop="4mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
    <page_header> 
        <div style="padding-left:610px;position:relative;top:-40px;">
            <img alt="Logo"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
            <div style="font-size:24px;color:#000000;position:absolute;width: 500px;padding-left:7px;">
                <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM . $view_path; ?>inc/logo/calendar.png"/>&nbsp;Student Success Team</b></div>
        </div>
    </page_header> 
    <page_footer> 
        <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor; ?>;"><?php echo $dis; ?></div>
        <br />
        <table style="margin-left:10px;">
            <tr>
                <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                </td>
                <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:270px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Page [[page_cu]] of [[page_nb]] 
                </td>
                <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor; ?>;">
                    Printed on: <?php echo date('F d,Y'); ?>
                </td>
            </tr>
        </table>
    </page_footer> 


    <table cellspacing="0" style="width:750px;border:2px #939393 solid;">
        <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
            <td style="padding-left:5px;padding-top:2px;height:36px;" >
                <b> <?php echo ucfirst($this->session->userdata('district_name')); ?> | <?php if ($this->session->userdata('login_type') == 'teacher'): ?>
                        <?php echo ucfirst($this->session->userdata('school_self_name')); ?>
                    <?php else: ?>
                        <?php echo ucfirst($this->session->userdata('school_name')); ?>
                    <?php endif; ?></b></td>
            <td style="text-align:right;padding-right:10px;">
               
            </td>
            <!--                            <br /><br />-->


        </tr>
        
        <tr style="margin-top:20px;">
            <td style="padding-left:10px;width:350px;color:#939393; vertical-align: top;">
                <br />
                <b>Type of Meeting:</b> <?php echo $class_summarys[0]['meeting_type']; ?>   
                <br />
                <br />
                <?php if ($teachername[0]['firstname']!=''):?>
                <b>Teacher:</b> <?php echo $teachername[0]['firstname'] . ' ' . $teachername[0]['firstname']; ?>   
                <br />
                <br />
            <?php else:?>
                <b>Observer:</b> <?php echo $this->session->userdata('observer_name'); ?>   
                <br />
                <br />
            <?php endif;?>
                <b>Year:</b> <?php echo date('Y',strtotime($class_summarys[0]['date'])); ?> 
                <br />
                <br />
                

            </td>
            <td style="padding-left:10px;width:350px;color:#939393;height:130px;">
                <br />
                <table style="border:1px solid #939393;height:130px;width:340px;">
                    <tr>
                        <td style="width:340px;height:165px;vertical-align: top;">
                            <b>Description:</b> <?php echo $des;?>
                        </td>
                    </tr>
                </table>
            <br />
            </td>
            

        </tr>

        
    </table>
    <br /><br />
    <!--Strength start here-->
    <table class="gridtable" style="width:750px;" >

        <tr >
            <td style="text-align: center;width:110px;">
                <b>Reason for Referral</b> 
            </td>
            <td style="text-align: center;width:110px;">
                <b>Intervention Strategy</b> 
            </td>
            <td style="text-align: center;width:75px;">
                <b>Decision Date</b> 
            </td>
            <td style="text-align: center;width:75px;">
                <b>Grade</b> 
            </td>
            <td style="text-align: center;width:90px;">
                <b>Student</b> 
            </td>
            <td style="text-align: center;width:75px;">
                <b>Next Meeting</b> 
            </td>
        </tr >
       <?php $cntr = 0;?>
    <?php foreach($class_summarys as $class_summary):?>
        <tr >
            <td style="text-align: center;">
                <?php echo $class_summary['referral_reason'];?>
            </td>
            <td style="text-align: center;" valign="middle">
                <?php echo $class_summary['program_change'];?> 
            </td>
            <td style="text-align: center;">
                <?php echo date('M d, Y',strtotime($class_summary['date']));?> 
            </td>
            <td style="text-align: center;">
                <?php echo $class_summary['grade_name'];?>
            </td>
            <td style="text-align: center;">
                <?php echo $class_summary['firstname'].' '.$class_summary['lastname'];?>
            </td>
            <td style="text-align: center;">
                <?php echo date('M d, Y',strtotime($class_summary['next_date']));?>
            </td>
        </tr >
        <?php $cntr++;?>
    <?php endforeach;?>
    <?php if($cntr < 15):?>
        <?php for($i=$cntr;$i<=15;$i++):?>
            <tr >
            <td style="text-align: center;">
                &nbsp;
            </td>
            <td style="text-align: center;" valign="middle">
                &nbsp;
            </td>
            <td style="text-align: center;">
                &nbsp;
            </td>
            <td style="text-align: center;">
                &nbsp;
            </td>
            <td style="text-align: center;">
                &nbsp;
            </td>
            <td style="text-align: center;">
                &nbsp;
            </td>
        </tr >
    <?php endfor;?>
<?php endif;?>
    </table>

</page>