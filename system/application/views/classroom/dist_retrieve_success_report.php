<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>UEIS Workshop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

        <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />

        <style type="text/css">
            .button_next {
                background-color: #FFB400;
                border: 0 none;
                color: #FFFFFF;
                cursor: default;
                float: right;
                margin-left: 5px;
                border-radius: 15px;
                display: inline-block;
                padding: 5px 14px;
                text-shadow: none !important;
                border-radius: 0 !important;
                outline: 0 none;
                text-decoration: none;
                line-height: 20px;
                list-style: none outside none;
                text-align: center;
            }
        </style>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php require_once($view_path . 'inc/header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar-scroll">
                <div id="sidebar" class="nav-collapse collapse">


                    <!-- BEGIN SIDEBAR MENU -->
                    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->  
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->   
                    <div class="row-fluid">
                        <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <h3 class="page-title">
                                <i class="icon-group"></i>&nbsp; Classroom Management
                            </h3>
                            <ul class="breadcrumb" >

                                <li>
                                    UEIS Workshop
                                    <span class="divider">&nbsp; | &nbsp;</span>
                                </li>

                                <li>
                                    <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>classroom">Classroom Management</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>classroom/success_team">Student Success Team</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    </i> <a href="<?php echo base_url(); ?>classroom/sst_retrieve_report">Retrieve Report</a>
									<span class="divider">></span>	
                                </li>
								<li>
                                    </i> <a href="<?php echo base_url(); ?>classroom/retrieve_success_report">Student Success Team by Student Report</a>

                                </li>




                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN BLANK PAGE PORTLET-->
                            <div class="widget yellow">
                                <div class="widget-title">
                                    <h4>Student Success Team by Student Report</h4>

                                </div>
                                <div class="widget-body">

                                    <div id="pills" class="custom-wizard-pills-yellow2">
                                        <ul>
                                            <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                            <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                            <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                            <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                            <!--<li><a href="#pills-tab5" data-toggle="tab">Step 5</a></li>-->




                                        </ul>
                                        <div class="progress progress-success-yellow progress-striped active">
                                            <div class="bar"></div>
                                        </div>
                                        <div class="tab-content">

                                            <!-- BEGIN STEP 1-->
                                            <div class="tab-pane" id="pills-tab1">
                                                <h3 style="color:#000000;">STEP 1</h3>
                                                <div class="form-horizontal" >
       <!--                                      <form method="post" action="<?php echo base_url() . 'classroom/retrieve_success'; ?>" name="retrieve_success_report" id="retrieve_success_report">-->
                                                    <div class="control-group">
                                                        <label class="control-label">Select Date</label>
                                                        <div class="controls">
                                                            <input id="dpYears1" name="date" type="text" value="" size="16" class="m-ctrl-medium" style="width: 285px;" />
                                                        </div>
                                                    </div>
                                                    
                                                
                                                	<?php if ($this->session->userdata('login_type') == 'user'): ?>
                                            <div class="control-group">
                                                <label class="control-label">Select School</label>
                                                <div class="controls">
                <select class="span12 chzn-select" name="school_id" id="school_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value=""></option>
                                                        <?php foreach ($schools as $school): ?>
                  <option value="<?php echo $school['school_id']; ?>"><?php echo $school['school_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                             <div class="control-group">
                                                <label class="control-label">Select Teacher</label>
                                                <div class="controls">
                <select class="span12 chzn-select" name="teacher_id" id="teacher_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value=""></option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                 <?php endif; ?>    

<!--                                                    <div class="control-group">
                                                        <label class="control-label">Select Grade</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="grade_id" id="grade_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php
                                                                if (!empty($grades)) {
                                                                    foreach ($grades as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val['grade_id']; ?>" ><?php echo $val['grade_name']; ?>  </option>
                                                                    <?php }
                                                                } ?>
                                                            </select>
                                                        </div>
                                                    </div>-->

                                                    <div class="control-group">
                                                        <label class="control-label">Select Student</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="student_id" id="student" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
<!--                                                                <option value='0'>All</option>-->
                                                         	   
                                                            </select>
                                                        </div>
                                                    </div>     

                                                </div>
                                                <!--                                     <br /><br /><br /><br />-->

                                            </div>
                                            <!--                                     </form>-->

                                            <!-- BEGIN STEP 2-->
                                            <div class="tab-pane" id="pills-tab2">
                                                <h3 style="color:#000000;">STEP 2</h3>
                                                <div class="form-horizontal" >

                                                    <div class="control-group">
                                                        <label class="control-label">Home Strategies</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="home_strat" id="home_strat" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php foreach($need_homes as $need_home):?>
                                                                <option value="<?php echo $need_home->id;?>"><?php echo $need_home->intervention_strategies_home;?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="control-label">School Strategies</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="school_strat" id="school_strat" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php foreach($need_schools as $need_school):?>
                                                                <option value="<?php echo $need_school->id;?>"><?php echo $need_school->intervention_strategies_school;?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="control-label">Medical Strategies</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="medical_start" id="medical_start" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php foreach($need_medicals as $need_medical):?>
                                                                <option value="<?php echo $need_medical->id;?>"><?php echo $need_medical->intervention_strategies_medical;?></option>
                                                                <?php endforeach;?>

                                                            </select>
                                                        </div>
                                                    </div>     

                                                </div>
                                            </div>

                                            <!-- BEGIN STEP 3-->
<!--                                            <div class="tab-pane" id="pills-tab3">
                                                <h3 style="color:#000000;">STEP 3</h3>
                                                <div class="form-horizontal" >

                                                    <div class="control-group">
                                                        <label class="control-label">Home Actions</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="home_actions" id="home_actions" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php foreach($action_homes as $action_home):?>
                                                                <option value="<?php echo $action_home->id;?>"><?php echo $action_home->actions_home;?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="control-label">School Actions</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="school_actions" id="school_actions" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php foreach($action_schools as $action_school):?>
                                                                <option value="<?php echo $action_school->id;?>"><?php echo $action_school->actions_school;?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                        <label class="control-label">Medical Actions</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="medical_actions" id="medical_actions" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="0">All</option>
                                                                <?php foreach($action_medicals as $action_medical):?>
                                                                <option value="<?php echo $action_medical->id;?>"><?php echo $action_medical->actions_medical;?></option>
                                                                <?php endforeach;?>

                                                            </select>
                                                        </div>
                                                    </div>     

                                                </div>
                                            </div>-->
                                            
                                            <!--BEGIN STEP 3-->
                                            <div class="tab-pane" id="pills-tab3">
                                                <h3 style="color:#000000;">STEP 3</h3>
                                                <div class="form-horizontal" >
                                                    <div class="control-group">
                                                        <label class="control-label">Report Type</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="report_type" id="report_type" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="graph">Graph</option>
                                                                <option value="list">List</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- BEGIN STEP 4-->
                                            <div class="tab-pane" id="pills-tab4">
                                                <h3 style="color:#000000;">STEP 4</h3>
                                                <div id="successbtn">
                                                    
                                                    <div class="control-group" style="height:auto;min-height: 300px;">
                                                        <div class="space15" id="ajaxcontainer"></div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                            <ul class="pager wizard">
                                                <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                                <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                                <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                                <li class="next yellow"><a href="javascript:;">Next</a></li>
                                            </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <!-- END BLANK PAGE PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->



<!-- BEGIN FOOTER -->
<div id="footer">
    UEIS © Copyright 2012. All Rights Reserved.
</div>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>


<script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
<!-- ie8 fixes -->
<!--[if lt IE 9]>
<script src="js/excanvas.js"></script>
<script src="js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>



<!--common script for all pages-->
<script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
<!--script for this page-->
<!--<script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>-->
<script src="<?php echo SITEURLM ?>js/form-component.js"></script>

<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
          <script src="http://ueis/WAPTrunk/js/exporting.js"></script>

<!-- END JAVASCRIPTS --> 

<script>
    
    var Script = function () {

    $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
//        console.log('onNext');
            if(index==3){
                     $('#ajaxcontainer').html('');
                    
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>classroom/retrieve_success_need",
                        data: { date: $('#dpYears1').val(), grade_id: $('#grade_id').val(),student_id: $('#student').val(),home_strat:$('#home_strat').val(),school_strat: $('#school_strat').val(),medical_start:$('#medical_start').val(),home_actions:$('#home_actions').val(),school_actions:$('#school_actions').val(),medical_actions:$('#medical_actions').val(),report_type:$('#report_type').val(),submit:'submit' }
                      })
                        .done(function( msg ) {
                           if($('#report_type').val()=='graph'){
                                eval(msg);
                            } else {
                               $('#ajaxcontainer').html(msg);  
                            }
                        });
                }
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(tab);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
    }});

    $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
//        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        console.log(index);
        if(index==3){
                     $('#ajaxcontainer').html('');
                    
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>classroom/retrieve_success_need",
                        data: { date: $('#dpYears1').val(), grade_id: "all",student_id: $('#student').val(),home_strat:$('#home_strat').val(),school_strat: $('#school_strat').val(),medical_start:$('#medical_start').val(),report_type:$('#report_type').val(),teacher_id:$('#teacher_id').val(),school_id:$('#school_id').val() }
                      })
                        .done(function( msg ) {
                           if($('#report_type').val()=='graph'){
                                eval(msg);
                            } else {
                               $('#ajaxcontainer').html(msg);  
                            }
                        });
                }
    }, onPrevious: function(tab, navigation, index) {
//        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
//        console.log('onLast');
    }, onTabClick: function(tab, navigation, index) {
        console.log('onTabClick');

    }, onTabShow: function(tab, navigation, index) {
        console.log('onTabShow2');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#tabsleft').find('.bar').css({width:$percent+'%'});

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
            $('#tabsleft').find('.pager .next').hide();
            $('#tabsleft').find('.pager .finish').show();
            $('#tabsleft').find('.pager .finish').removeClass('disabled');
        } else {
            $('#tabsleft').find('.pager .next').show();
            $('#tabsleft').find('.pager .finish').hide();
        }

    }});


    $('#tabsleft .finish').click(function() {
        alert('Finished!, Starting over!');
        $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
    });

}();

    $(function() {
        $(" input[type=radio], input[type=checkbox]").uniform();
    });

$('#school_id').change(function(){
   $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>teacherself/getteacherBySchoolId",
            data: { school_id: $('#school_id').val()}
          })
            .done(function( result) {
//              console.log(teachers);
                $('#teacher_id').html('');
                $('#teacher_id').append('<option></option>');
              var teachers = jQuery.parseJSON(result);
              $.each(teachers,function(index,value){
//                    console.log(value);
                    $('#teacher_id').append('<option value="'+value.teacher_id+'">'+value.firstname+' '+value.lastname+'</option>');
              });
              $('#teacher_id').trigger("liszt:updated");
              
              
            });

});

$('#teacher_id').change(function(){
   $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>classroom/getStudentsByTeacher",
            data: { school_id: $('#school_id').val(),teacher_id:$('#teacher_id').val()}
          })
            .done(function( result) {
              
                $('#student').html('');
                $('#student').append('<option value="all">ALL</option>');
              var students = jQuery.parseJSON(result);
//              console.log(students);
              $.each(students,function(index,value){
//                    console.log(value);
                    $('#student').append('<option value="'+value.student_id+'">'+value.firstname+' '+value.lastname+'</option>');
              });
              $('#student').trigger("liszt:updated");
              
              
            });

});

</script>





</body>
<!-- END BODY -->
</html>