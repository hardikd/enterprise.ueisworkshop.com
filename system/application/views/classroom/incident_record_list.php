<table class="table table-striped table-hover table-bordered" id="editable-grade-day" style="width:100%;">
                                     <thead>
                                     <tr>
                                         <th class="check">S.No.</th>
                                         <th class="sorting">Date</th>                                       
                                         <th class="no-sorting sorting">Incident Types</th>
                                         <th class="no-sorting sorting">Description</th>
                                         <th class="no-sorting sorting">Victims</th>
                                         
                                         <th class="no-sorting sorting">Suspects</th>
                                         <th class="no-sorting sorting">View</th>
                                         <th class="no-sorting sorting">Print</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                         <?php $cnt = 1;
                                        // print_r($incident_list);exit;
                                         ?>
                                         <?php if($incident_list):?>
                                         <?php foreach($incident_list as $key=>$incident):?>
                                     <tr class="">
                                         <td class="check"><?php echo $cnt;?></td>
                                         <td><?php echo $incident['date'];?></td>                                   
                                         <td><?php foreach ($incident['incident'] as $problem):?> <?php echo $problem->incident_name;?><br /><?php endforeach;?></td>
                                         
                                         <td><?php echo substr(strip_tags($incident['description'],0, 100));?></td>
                                         <td><?php foreach($incident['victim'] as $victims):?><?php echo $victims[0]->firstname.' '.$victims[0]->lastname;?><br /><?php endforeach;?></td>                                         
                                         <td><?php foreach($incident['suspect'] as $suspects):?><?php echo $suspects[0]->firstname.' '.$suspects[0]->lastname;?><br /><?php endforeach;?></td>
                                         <td><a class="delete view" href="javascript:;" id="<?php echo $key;?>" >View</a></td>
                                         <td><a class="delete print" href="javascript:;" id="<?php echo $key;?>">Print</a></td>
                                     </tr>
                                     <?php $cnt++;?>
                                     <?php endforeach;?>
                                     <?php else: ?>
                                     <tr>
                                         <td colspan="8" style="text-align:center; font-weight: bold;">No Incident found for selected duration.</td>
                                     </tr>
                                     <?php endif;?>
                                     </tbody>
                                 </table>
<script>
            $('.view').on('click',function(){
                var incident_id = $(this).attr('id');
                $('#reportbody').html('');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>classroom/incident_record_details",
                    data: { incident: incident_id}
                  })
                    .done(function( msg ) {
                        $('#reportbody').html(msg);
                        $('#reportDiv').show();
                    });
            });
            
            $('.print').on('click',function(){
                var incident_id = $(this).attr('id');
                window.open("<?php echo base_url();?>classroom/incident_record_details_print/"+incident_id);
                
            });
        </script>