<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
  
   </style><page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="system/application/views/inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
        <div style="font-size:24px;color:#000000;position:absolute;width: 400px">
                    <b><img alt="pencil" width="12px"  src="system/application/views/inc/logo/pencil1.png"/>&nbsp;Behavior Running Record</b></div>
        </div>
        </page_header> 
       <page_footer> 
               <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
        <br />
              <div style="text-align:center;font-size:7px;color:#<?php echo $fontcolor;?>;">&copy; Copyright U.E.I.S. Corp. All Rights Reserved</div>
        
        </page_footer>  
                       <table cellspacing="0" style="width:750px;border:2px #939393 solid;">
                    <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                        <td colspan="4" style="padding-left:5px;padding-top:2px;height:36px;" >
                            <b> <?php echo ucfirst($this->session->userdata('district_name'));?>&nbsp;|&nbsp;
                                
                               <?php  if($this->session->userdata('login_type')=='user'){?>
                                    <?php echo ucfirst($incident_details['details']->school_name); ?>       
                                <?php }else  if ($this->session->userdata('login_type') == 'teacher') {?>
                                    <?php echo ucfirst($this->session->userdata('school_self_name'));?>
                               <?php } else if ($this->session->userdata('login_type') == 'observer') {?>
                                    <?php echo ucfirst($this->session->userdata('school_name'));?>
                               <?php }?>
                            </b></td>
<!--                            <br /><br />-->
                        
                   
                </tr>
                <tr >
                        <td colspan="4" >
                       &nbsp;
                        
                    </td>
                </tr>
                <tr style="margin-top:20px;">
        <td style="padding-left:10px;width:180px;color:#939393;vertical-align: top;">
        <b>Date:</b> <?php echo date('F d, Y',strtotime($incident_details['date']));?>
        </td>
        <td style="padding-left:10px;width:120px;color:#939393;vertical-align: top;">
        <b>Time:</b> <?php echo $incident_details['incident'][0]->time;?>
        </td>
        <td style="padding-left:10px;width:190px;color:#939393;vertical-align: top;">
                   <?php if($incident_details['incident'][0]->login_type=='teacher'):?>  
                    <b>Teacher:</b> <?php echo $teacher[0]['firstname'].' '.$teacher[0]['lastname'];?>
                    <?php elseif($incident_details['incident'][0]->login_type=='observer'):?>
                    <b>Observer:</b> <?php echo $observer[0]['observer_name'];?>
                    <?php endif;?>
        </td>
        <td style="width:190px;color:#939393;vertical-align: top;">
        <b>Location:</b> <?php echo $incident_details['location'];?>
        </td>
        </tr>
        <tr>
        <td>
        <br />
        </td>
        </tr>
        <tr>
        <td style="padding-left:10px;color:#939393;width:150px;vertical-align: top;">
        <b>Petitioner(s):</b>
        <?php foreach($incident_details['victim'] as $victims):?><?php echo $victims[0]->firstname.' '.$victims[0]->lastname;?><br /><?php endforeach;?>
                </td>
               
                
                <td style="width:180px;padding-left:10px;color:#939393;vertical-align: top;" colspan="3">
        <b>Respondent(s):</b>
        <?php foreach($incident_details['suspect'] as $suspects):?><?php echo $suspects[0]->firstname.' '.$suspects[0]->lastname;?><br /><?php endforeach;?>
                </td>
                </tr>
                <tr >
                        <td colspan="4" >
                       &nbsp;
                        
                    </td>
                </tr>
        </table>
   
 <br /><br />
   <table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
        <b>Problem Behavior</b>
        </div>
        </td>
        </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
        <td style="border:1px #939393 solid;padding-left:10px;">
                    <!--<div  style="font-size:12px;border-left:1px #5e3364 solid; border-right:1px #5e3364 solid; border-bottom:1px #5e3364 solid;padding:5px; color:#000;">-->
                    <div style="width:738px;">
                    <br />
                <?php foreach ($incident_details['incident'] as $problem):?> <?php echo $problem->behaviour_name;?><br /><?php endforeach;?>
                   
                    <br /><br />
                    </div>
        </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>    


<br />
<table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
        <b>Incident Description</b>
        </div>
        </td>
        </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
        <td style="border:1px #939393 solid;padding-left:10px;">

                    <div style="width:738px;">
                   
                <?php echo $incident_details['description'];?>
                   
                    <br /><br />
                    </div>
        </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>

<br />
<table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
        <b>Possible Motivation</b>
        </div>
        </td>
        </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
        <td style="border:1px #939393 solid;padding-left:10px;">

                    <div style="width:738px;">
                    <br />
               <?php foreach ($incident_details['motivation'] as $motivation):?> <?php echo $motivation->possible_motivation;?><br /><?php endforeach;?>
                   
                    <br /><br />
                    </div>
        </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>

<br />
<table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
        <b>Interventions Prior To Office Discipline Referral</b>
        </div>
        </td>
        </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
        <td style="border:1px #939393 solid;padding-left:10px;">

                    <div style="width:738px;">
                    <br />
               <?php foreach ($incident_details['intervention'] as $intervention):?> <?php echo $intervention->interventions_prior;?><br /><?php endforeach;?>
                   
                    <br /><br />
                    </div>
        </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>

<br />
<table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
        <b>If Student Has Multiple Referrals,Referred To</b>
        </div>
        </td>
        </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
        <td style="border:1px #939393 solid;padding-left:10px;">

                    <div style="width:738px;">
                    <br />
               <?php foreach ($incident_details['referral'] as $referral):?> <?php echo $referral->student_has_multiple_referrals;?><br /><?php endforeach;?>
                    
                    <br /><br />
                    </div>
        </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>
   </page>                              
