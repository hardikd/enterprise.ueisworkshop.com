<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
  
   </style><page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="system/application/views/inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
    <div style="font-size:24px;color:#000000;position:absolute;width: 400px">
                    <b><img alt="pencil" width="12px"  src="system/application/views/inc/logo/pencil1.png"/>&nbsp;Parent / Teacher Conference</b></div>
    </div>
        </page_header> 
        <page_footer> 
            <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
    <br />
              <div style="text-align:center;font-size:7px;color:#<?php echo $fontcolor;?>;">&copy; Copyright U.E.I.S. Corp. All Rights Reserved</div>
        </page_footer>

                
                <table cellspacing="0" style="width:700px;border:2px #939393 solid;">
                    <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                        <td colspan="3" style="padding-left:5px;padding-top:2px;height:36px;" >
                            <b><?php echo ucfirst($parent_conference[0]->districts_name); ?>&nbsp;|&nbsp;<?php echo ucfirst($parent_conference[0]->school_name); ?></b></td><td style="text-align:right;padding-right:10px;"><b><?php echo $date;?></b></td>
<!--                            <br /><br />-->
                        
                   
                </tr>
                <tr >
                        <td colspan="4" >
                       &nbsp;
                        
                    </td>
                </tr>
                <tr style="margin-top:20px;">
    <td style="padding-left:10px;width:155px;color:#939393;vertical-align: top;">
    <?php if($this->session->userdata('login_type')=='teacher'){?>
                        <b>Teacher:</b> <?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?>   
                   <?php } else if($this->session->userdata('login_type')=='observer'){?>
                        <b>Observer Name:</b> <?php echo ($teachername[0]['observer_name']); ?>   
                   <?php } else if($this->session->userdata('login_type')=='user'){?>
                        <b>User Name:</b> <?php echo $this->session->userdata('username');?>
                 <?php }?>
    </td>
    <td style="padding-left:10px;width:150px;color:#939393;vertical-align: top;">
    <b>Date:</b> <?php echo date('F d, Y',strtotime($parent_conference[0]->date)); ?>
    </td>
    <td style="padding-left:10px;width:185px;color:#939393;vertical-align: top;">
    <b>Period:</b> <?php echo (date('h:i A',strtotime($parent_conference[0]->start_time))); ?> - <?php echo (date('h:i A',strtotime($parent_conference[0]->end_time))); ?> 
    </td>
    <td style="width:190px;color:#939393;vertical-align: top;">
    <b>Student:</b> <?php echo ($parent_conference[0]->firstname . ' ' . $parent_conference[0]->lastname . ''); ?>
    </td>
    </tr>
    <tr>
    <td>
    <br />
    </td>
    </tr>
    <tr>
    <td style="padding-left:10px;color:#939393;width:155px;vertical-align: top;">
    <b>Subject:</b> <?php echo ($parent_conference[0]->subject_name); ?>
                </td>
                
                <td style="width:150px;padding-left:10px;color:#939393;vertical-align: top;">
                    <b>Grade:</b> <?php echo ($parent_conference[0]->grade_name); ?>
                </td>
                
                </tr>
                <tr >
                        <td colspan="3" >
                       &nbsp;
                        
                    </td>
                </tr>
                
    </table>
       <br /><br />
<table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
    <b>Comment</b>
        </div>
    </td>
    </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
    <td style="border:1px #939393 solid;padding-left:10px;">

                    <div style="width:738px;">
                    <br />
                <?php echo strip_tags($parent_conference[0]->comment); ?>
                    <br />
                    <br /> <br />
                    </div>
    </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>
       <br />
       <table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
    <b>Strengths</b>
        </div>
    </td>
    </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
    <td style="border:1px #939393 solid;padding-left:10px;">

                    <div style="width:738px;">
                    <br />
               <?php echo strip_tags($parent_conference[0]->strengths); ?>
                    <br /><br /> <br />
                    
                    </div>
    </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>
       <br />
       <table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
    <b>Concerns</b>
        </div>
    </td>
    </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
    <td style="border:1px #939393 solid;padding-left:10px;">
 
                    <div style="width:738px;">
                    <br />
              <?php echo strip_tags($parent_conference[0]->concerns); ?>
                    <br /><br /> <br />
                    
                    </div>
    </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>
    <br />
       <table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                   
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:738px;font-size:16px;">
    <b>Ideas For Parent/Student</b>
        </div>
    </td>
    </tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
    <td style="border:1px #939393 solid;padding-left:10px;">
 
                    <div style="width:738px;">
                    <br />
             <?php echo strip_tags($parent_conference[0]->ideas_for_parent_student); ?>
                    <br /><br /> <br />
                    
                    </div>
    </td></tr>
                <tr><td>&nbsp;</td></tr>
               
                </table>

   </page>