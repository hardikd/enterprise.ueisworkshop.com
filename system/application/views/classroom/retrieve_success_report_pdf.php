<table style="width:850px; text-align:center" align="center">
    <tr>
        <td align="center"><h1>Retrieve Success Report</h1></td>
    </tr>
</table>
<br />
<table style="width:850px;border:2px #7FA54E solid;">
    <tr>
        <td style="width:225px;color:#CCCCCC;"><b>Teacher:</b><?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?></td>
        <td style="width:300px;color:#CCCCCC;"><b>Student Name:</b><?php echo ($retrieve_report[0]->firstname . ' ' . $retrieve_report[0]->lastname . ''); ?></td>
        <td style="width:225px;color:#CCCCCC;"><b>Grade:<?php echo ($retrieve_report[0]->grade_name); ?></b></td>
    </tr>
</table>


<table  cellspacing='0' cellpadding='0' border='0' id='conf' style='width:850px;' >

    <tr >
        <td >
            <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> 
                <b>Strengths</b> 
            </div>
        </td>
    </tr>
    <tr>
        <td >
            <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> 
                <b>Parent</b> 
            </div>
        </td>
</tr>

<tr>
        <td >
            <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->strengths_parent); ?> </div></td>
    </tr>
    
    <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Teacher</b> </div></td>
    </tr>
    <tr>
        <td > <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->strengths_teacher); ?> </div></td>
    </tr>
    
    <!--Background Information data -->
    <tr >
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Background Information </b> </div></td>
    </tr>
    <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Family </b> </div></td>
    </tr>
	<tr>
        <td > <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->information_family); ?> </div></td>
    </tr>
    <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>School</b> </div></td>
    </tr>
    <tr>
        <td > <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->information_school); ?> </div></td>
    </tr>
    <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Health</b> </div></td>
    </tr>
    <tr>
        <td > <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->information_health); ?> </div></td>
    </tr>

    <!--Background Information end  --> 
    <!--needs start -->
    <tr >
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Needs</b> </div></td>
    </tr>
    <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Home</b> </div></td>
	</tr>
    <tr>
        <td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->needs_home); ?> </div></td>
    </tr>
    <tr>
        <td ><div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>School</b> </div></td>
    </tr>
    <tr>
        <td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->needs_school); ?> </div></td>
    </tr>
    <tr>
        <td ><div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Medical</b> </div></td>
    </tr>
    <tr>
        <td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->needs_medical); ?> </div></td>
    </tr>

    <!--end needs --> 
<!--action start -->
    <tr >
        <td ><div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Actions </b> </div></td>
    </tr>
    <tr>
        <td ><div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Home</b> </div></td>
		</tr>
    <tr>
        <td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->action_home); ?> </div></td>
    </tr>
    <tr>
        <td ><div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>School</b> </div></td>
    </tr>
    <tr>
        <td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->action_school); ?> </div></td>
    </tr>
    <tr>
        <td ><div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b>Medical</b> </div></td>
    </tr>
    <tr>
        <td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo unserialize($retrieve_report[0]->action_medical); ?> </div></td>
    </tr>
    <!--end action -->
</table>
