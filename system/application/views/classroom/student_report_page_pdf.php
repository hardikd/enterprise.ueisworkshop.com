<style type="text/css">
  table td {word-wrap:break-word;}
  table {
    border-spacing: 0px;
    /*border-color: grey;*/
    color:#0C0C0C;
}
</style>

<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
  
   </style><page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="system/application/views/inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
    <div style="font-size:24px;color:#000000;position:absolute;width: 400px">
                    <b><img alt="pencil" width="12px"  src="system/application/views/inc/logo/pencil1.png"/>&nbsp;Student Report</b></div>
    </div>
        </page_header> 
        <page_footer> 
            <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
    <br />
              <div style="text-align:center;font-size:7px;color:#<?php echo $fontcolor;?>;">&copy; Copyright U.E.I.S. Corp. All Rights Reserved</div>
        </page_footer>
                                    <table>
                                    <tr> 
                                      <td width="400px">
                                    Long Beach Unified School District<br/>
                                    <?php echo $this->session->district_name;?>
                                    <br/><?php echo $school_details[0]->school_name;?>
                                    <?php if($observer_details[0]->observer_name!=''):?>
                                    <br/>Principal: <?php echo $observer_details[0]->observer_name;?>
                                  <?php endif;?>
                                    <br/>Teacher: <?php echo $this->session->userdata('teacher_name');?>
                                    <br/>School Year: <?php echo date('Y').' - '.date('Y',strtotime('+1 years'));?>
                                    <br/>Student: <?php echo $student_details[0]->firstname.' '.$student_details[0]->lastname;?>
                                    <br/>Grade: <?php echo $grade_subjects[0]['grade_name'];?>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td >
                                    <img src="<?php echo $view_path.'inc/logo/u2.png';?>" >
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                      <table border="1" cellspacing="0" bordercolor="#CCCCCC">
                                        <tr>
                                          <td>

                                          </td>
                                          <td>Q 1</td>
                                          <td>Q 2</td>
                                          <td>Q 3</td>
                                          <td>Q 4</td>
                                        </tr>
                                        <tr>
                                          <td>
                                            Days Present
                                          </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            Days Absent
                                          </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                        </tr> 
                                        <tr>
                                          <td>
                                            Days Tardy
                                          </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            Days Left Early
                                          </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                          <td> </td>
                                        </tr>                                                                          
                                      </table>
                                    </td>
                                    </tr>
                                    </table>
                                    <br>
                                    <table>
                                      <tr>
                                      <?php foreach($grade_subjects as $key=>$grade_subject):?>
                                        <?php if ($key%2==0):?>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
                                          &nbsp;
                                        </td>
                                        </tr>
                                        <tr>
                                        <?php else:?>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                      <?php endif;?>
                                      <td>
                                        <table border="1" cellspacing="0" bordercolor="#CCCCCC" style="width:420px;">
                                          <tr>
                                            <td>
                                              <b><?php echo wordwrap($grade_subject['subject_name'],10,"<br />\n");?></b>
                                            </td>
                                            <td width="250px">
                                              
                                            </td>
                                            <td>
                                              AC
                                            </td>
                                            <td>
                                              EF
                                            </td> 
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 1 Standards
                                            </td>
                                            <td style="width:150px;">
                                              <?php  echo implode(",",$standards[$grade_subject['grade_subject_id']][1]);?> 
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $grade[$grade_subject['grade_subject_id']][1];?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $efforts[$grade_subject['grade_subject_id']][1];?>
                                            </td> 
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 1 Comments<br />
                                              <br />
                                            </td>
                                            <td>
                                              <?php echo $comments[$grade_subject['grade_subject_id']][1][1];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][1][2];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][1][3];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][1][4];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][1][5];?>
                                            </td>
                                            
                                             
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 2 Standards
                                            </td>
                                            <td style="width:200px;">
                                              <?php  echo implode(",",$standards[$grade_subject['grade_subject_id']][2]);?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $grade[$grade_subject['grade_subject_id']][2];?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $efforts[$grade_subject['grade_subject_id']][2];?>
                                            </td>  
                                          </tr>
                                          <tr>
                                            <td>
                                             Q 2 Comments<br />
                                              <br />
                                            </td>
                                            <td>
                                              <?php echo $comments[$grade_subject['grade_subject_id']][2][1];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][2][2];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][2][3];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][2][4];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][2][5];?>
                                            </td>
                                           
                                          </tr>
                                          <tr>
                                            <td >
                                              Q 3 Standards
                                            </td>
                                            <td style="width:150px;">
                                               <?php  echo implode(",",$standards[$grade_subject['grade_subject_id']][3]);?>
                                              
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $grade[$grade_subject['grade_subject_id']][3];?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $efforts[$grade_subject['grade_subject_id']][3];?>
                                            </td>  
                                          </tr>
                                          <tr>
                                            <td>
                                             Q 3 Comments<br />
                                              <br />
                                            </td>
                                            <td>
                                              <?php echo $comments[$grade_subject['grade_subject_id']][3][1];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][3][2];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][3][3];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][3][4];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][3][5];?>

                                            </td>
                                            
                                          </tr>
                                          <tr>
                                            <td  style="width:50px;">
                                             Q 4 Standards
                                            </td>
                                            <td  style="width:100px; ">
                                              <?php  echo implode(",",$standards[$grade_subject['grade_subject_id']][4]);?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                              <?php echo $grade[$grade_subject['grade_subject_id']][4];?>
                                            </td>
                                            <td rowspan="2" style="vertical-align:top;">
                                               <?php echo $efforts[$grade_subject['grade_subject_id']][4];?>
                                            </td> 
                                          </tr>
                                          <tr>
                                            <td>
                                              Q 4 Comments
                                              <br />
                                              <br />
                                            </td>
                                            <td>
                                               <?php echo $comments[$grade_subject['grade_subject_id']][4][1];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][4][2];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][4][3];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][4][4];?><br />
                                              <?php echo $comments[$grade_subject['grade_subject_id']][4][5];?>
                                            </td>
                                            
                                          </tr>

                                        </table>
                                      </td>

                                      <?php endforeach;?>
                                    </tr>
                                    </table>
                                    <br /><br /><br /><br />
                                    <table>
                                      <tr>
                                        <td style="vertical-align:top;">
                                          <table border="1" cellspacing="0">
                                            <tr>
                                              <td><b>Work Habits</b></td>
                                              <td> Q 1 </td>
                                              <td> Q 2 </td>
                                              <td> Q 3 </td>
                                              <td> Q 4 </td>
                                            </tr>
                                            <?php foreach($work_habit as $summary=>$whs):?>
                                            <tr>
                                              <td style="width:125px;"><?php echo str_replace('_',' ',$summary);?></td>
                                              <td>
                                                <?php 
                                                echo $whs[1];
                                                ?>
                                              </td>
                                              <td>
                                                <?php echo $whs[2];
                                                ?>
                                              </td>
                                              <td>
                                                <?php echo $whs[3];
                                                ?>
                                              </td>
                                              <td>
                                                <?php echo $whs[4];
                                                ?>
                                              </td>
                                            </tr>
                                          <?php endforeach;?>
                                          </table>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="vertical-align:top;">
                                          <table border="1" cellspacing="0">
                                            <tr>
                                              <td><b>Learning &<br> Social Skills</b></td>
                                              <td> Q 1 </td>
                                              <td> Q 2 </td>
                                              <td> Q 3 </td>
                                              <td> Q 4 </td>
                                            </tr>
                                            <?php foreach($learning_skill as $summary=>$lss):?>
                                            <tr>
                                              <td style="width:125px;"><?php echo str_replace('_',' ',$summary);?></td>
                                              <td>
                                              <?php echo $lss[1]; ?>
                                              </td>
                                              <td>
                                                <?php echo $lss[2]; ?>
                                              </td>
                                              <td>
                                                 <?php echo $lss[3]; ?>
                                              </td>
                                              <td>
                                                 <?php echo $lss[4]; ?>
                                              </td>
                                            </tr>
                                          <?php endforeach;?>
                                            
                                          </table>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="vertical-align:top;">
                                          <table border="1" cellspacing="0">
                                            <tr>
                                              <td><b>Instructional<br> Programs</b></td>
                                              <td> Q 1 </td>
                                              <td> Q 2 </td>
                                              <td> Q 3 </td>
                                              <td> Q 4 </td>
                                            </tr>
                                            <?php foreach($instructional_prog as $summary=>$ips):?>
                                            <tr>
                                              <td style="width:125px;"><?php echo str_replace('_',' ',$summary);?></td>
                                              <td>
                                              <?php echo $ips[1]; ?>
                                              </td>
                                              <td>
                                                <?php echo $ips[2]; ?>
                                              </td>
                                              <td>
                                                 <?php echo $ips[3]; ?>
                                              </td>
                                              <td>
                                                 <?php echo $ips[4]; ?>
                                              </td>
                                            </tr>
                                          <?php endforeach;?>
                                            
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </page>
                                 