﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/teacher_student">Teacher/Student Conference</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>classroom//upload_student_report">Upload Report</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Create Report</h4>
                          
                         </div>
                         <div class="widget-body">
                 <form class="form-horizontal" method="post" action="<?php echo base_url().'classroom/edit_student_educator_conference_report';?>" name="student_teacher_form" id="student_teacher_form">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                      <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                       <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                        <li><a href="#pills-tab5" data-toggle="tab">Step 5</a></li>
                                   
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-yellow progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                        
                                 
                                 <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
       
       <input id="dp1" type="text" name="date" value="<?php $date = $retrieve_reports[0]->date; echo date('m-d-Y', strtotime(str_replace('-', '/', $date))) ?>" size="16" class="m-ctrl-medium"></input>
                                    </div>
                                </div>
                                  <?php if ($this->session->userdata('login_type') == 'user'){ ?>
                                            <div class="control-group">
                                                <label class="control-label">Select School</label>
                                                <div class="controls">
                <select class="span12 chzn-select" name="school_id" id="school_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value=""></option>
                                                        <?php foreach ($schools as $school){ ?>
                                                         <?php $ret = $retrieve_reports[0]->school_id;
													  if($ret == $school['school_id']){?>
                  <option value="<?php echo $school['school_id']; ?>" selected><?php echo $school['school_name']; ?></option>
                  <?PHP }else{?>
                   <option value="<?php echo $school['school_id']; ?>"><?php echo $school['school_name']; ?></option>
                                                        <?php }}?>
                                                    </select>
                                                </div>
                                            </div>
                                 <?php } ?>    
                                
                                
                            <div class="control-group">
                                  <label class="control-label">Select Grade</label>
                                             <div class="controls">
                     <select class="span12 chzn-select" name="grade_id" id="grade_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                           	<option value=""></option>
                                            <?php if(!empty($grades)) { 
                                            foreach($grades as $val)
                                            {
                                            ?>
                                                <?php $ret = $retrieve_reports[0]->grade_id;
										  if($ret== $val['grade_id']){?>
                                            <option value="<?php echo $val['grade_id'];?>" selected ><?php echo $val['grade_name'];?>  </option>
                                            <?php }else{?>
                                            <option value="<?php echo $val['grade_id'];?>" ><?php echo $val['grade_name'];?>  </option>
                                            <?php } }} ?>
                                            </select>
                                                </div>
                                         </div> 
                                         
                          
                                         
                                               <div class="control-group">
                                             <label class="control-label">Select Period </label>
                                             <div class="controls">
                            	<select data-placeholder="--Please Select--" name="period_id" class="chzn-select span4" style="width: 300px;" tabindex="6" id="periods">
                                        <option value=""></option>
										<?php foreach($periods as $value){?>
                                        
                                         <?php $ret = $retrieve_reports[0]->period_id;
										  if($ret== $value['period_id']){?>
                                     <option value="<?php echo $value['period_id'];?>" selected><?php echo $value['start_time'].'-'.$value['end_time'];?></option>
                                     <?php }else{?>
                                     <option value="<?php echo $value['period_id'];?>"><?php echo $value['start_time'].'-'.$value['end_time'];?></option>
                                            <?php }}?>
                      					</select>
                                             </div>
                                         </div> 
                                        
                                      <div class="control-group">
                                  <label class="control-label">Select Subject</label>
                                       <div class="controls">
		<select class="span12 chzn-select" name="subject_id" id="subject_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                            <option value=""></option>
                                            <option value="all">All</option>
                                            <?php if(!empty($subjects)) { 
                                            foreach($subjects as $val)
                                            {
                                            ?>
                                             <?php $ret = $retrieve_reports[0]->subject_id;
										  if($ret== $val['subject_id']){?>
                                            <option value="<?php echo $val['subject_id'];?>" selected><?php echo $val['subject_name'];?>  </option>
                                            <?php }else{?>
                                            <option value="<?php echo $val['subject_id'];?>" ><?php echo $val['subject_name'];?>  </option>
                                            <?php } }} ?>
                                            </select>  
                                           </div>
                                         </div>
                                         
                                   
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                                     		<div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select name="student_id" id="student" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <option value='all'>All</option>
 			<?php if(!empty($students)) { 
			foreach($students as $studentsvalue)
			{?>
              <?php $ret = $retrieve_reports[0]->student_id;
					if($ret== $studentsvalue['student_id']){?>
	<option value="<?php echo $studentsvalue['student_id'];?>" selected><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
    <?php }else{?>
    <option value="<?php echo $studentsvalue['student_id'];?>"><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
			  <?php } } }?>			   
		      </select> 
              </div>
      </div>
   
   <div class="control-group">
                                <label class="control-label">Comment</label>
                                <div class="controls">
                               <textarea class="span12 ckeditor" id='comment' name="comment" rows="6"><?php echo $retrieve_reports[0]->comment ?></textarea>
                                   <input type="hidden" name="id" id="id" value="<?php echo $retrieve_reports[0]->id ?>">
                                </div>
                            </div>
 <div>  
  

   
   
 
  
 
                                      
      </div>                                
                                   
                                     </div> 
                                     
                                     
                                     
                                     <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                        
                                 
  <?php /*?>                                  <div style="margin:0px 0px 0px 0px; padding:0px">
   		<label class="sorting" style="padding-left:200px;"><b>History</b></label>
   		<div style="padding-left:200px;">
   <?php
	$data= $retrieve_reports[0]->history;
				 $history_data = explode(",",$data);
	if(!empty($history_data)) { 
			  foreach($strengths as $strength)	{
				  $selected = "";
			  foreach($history_data as $history_value){	
			  	if($history_value == $strength->strengths_name){
						$selected = "checked";
					} 
			  }
		?>
     <input type="checkbox" name="history[]" value="<?php echo $strength->strengths_name;?>" <?php echo $selected;?>><?php echo $strength->strengths_name;?></br>
   			<?php } }?>
   		</div>
  </div><?php */?>
          
     <div class="space20"></div>
            <h4><b>History</b></h4>
            <div>
              <div class="control-group">
                <div class="space15"></div>
          <?php
	$data= $retrieve_reports[0]->history;
				 $history_data = explode(",",$data);
	if(!empty($history_data)) { 
			  foreach($historys as $history)	{
				  $selected = "";
			  foreach($history_data as $history_value){	
			  	if($history_value == $history->history_name){
						$selected = "checked";
					} 
			  }
		?>
                    <div class="span3">
                  <div>
                 <input type="checkbox" name="history[]" value="<?php echo $history->history_name;?>" <?php echo $selected;?>><?php echo $history->history_name;?></br>
                </div>
                  <!--<div style="padding:0px; margin:-18px 0px 0px 25px" ></div>-->
                    </div>
                  <?php }}?>
                
              </div>
            </div>
       </div>
                                     
                                     
                                     <!-- BEGIN STEP 4-->
                                     <div class="tab-pane" id="pills-tab4">
                                         <h3 style="color:#000000;">STEP 4</h3>
                                 
<?php /*?>                                 
   <div style="margin:0px 0px 0px 0px; padding:0px">
   		<label class="sorting" style="padding-left:200px;"><b>Behavior</b></label>
   		<div style="padding-left:200px;">
   	   <?php
	$data= $retrieve_reports[0]->behavior;
				 $behavior_data = explode(",",$data);
				
		if(!empty($behavior_data)) { 
			  foreach($concerns as $concern)	{
				  $selected = "";
			  foreach($behavior_data as $behavior_value){	
			  	if($behavior_value == $concern->concerns_name){
						$selected = "checked";
					} }
			  ?>
   <input  type="checkbox" name="behavior[]" value="<?php echo $concern->concerns_name;?>" <?php echo $selected;?>><?php echo $concern->concerns_name;?></br>
		 <?php }}?>
   		</div>
  </div> <?php */?>  
  <div class="space20"></div>
            <h4><b>Behavior</b></h4>
            <div>
              <div class="control-group">
                <div class="space15"></div>
       	   <?php
	$data= $retrieve_reports[0]->behavior;
				 $behavior_data = explode(",",$data);
				
		if(!empty($behavior_data)) { 
			  foreach($behaviors as $behavior)	{
				  $selected = "";
			  foreach($behavior_data as $behavior_value){	
			  	if($behavior_value == $behavior->behavior_name){
						$selected = "checked";
					} }
			  ?>
                    <div class="span3">
                  <div>
                 <input  type="checkbox" name="behavior[]" value="<?php echo $behavior->behavior_name;?>" <?php echo $selected;?>><?php echo $behavior->behavior_name;?></br>
                </div>
                  <!--<div style="padding:0px; margin:-18px 0px 0px 25px" ></div>-->
                    </div>
                  <?php }}?>
                
              </div>
            </div>
</div>
                                     
                                     
                                     
                                     <!-- BEGIN STEP 5-->
                                     <div class="tab-pane" id="pills-tab5">
                                         <h3 style="color:#000000;">STEP 5</h3>
                                  <?php /*?> <div style="margin:0px 0px 0px 0px; padding:0px">
   		<label class="sorting" style="padding-left:200px;"><b>Strategy/Consequences</b></label>
   		<div style="padding-left:200px;">
               	   <?php
			$data= $retrieve_reports[0]->strategy_consequences;
				 $strategy_consequences_data = explode(",",$data);
		if(!empty($strategy_consequences_data)) { 
			foreach($ideas_for_parent_student as $data)	{
				  $selected = "";
			  foreach($strategy_consequences_data as $strategy_consequences_value){	
			  	if($strategy_consequences_value == $data->ideas_for_parent_student){
						$selected = "checked";
					} 
			  }
			 ?>
            <input type="checkbox" name="strategy_consequences[]" value="<?php echo $data->ideas_for_parent_student;?>" <?php echo $selected;?>><?php echo $data->ideas_for_parent_student;?></br>
   			<?php } }?>
   		</div>
  </div><?php */?>
                   <div class="space20"></div>
            <h4><b>Strategy/Consequences</b></h4>
            <div>
              <div class="control-group">
                <div class="space15"></div>
           	   <?php
	$data= $retrieve_reports[0]->strategy_consequences;
				 $strategy_consequences_data = explode(",",$data);
				
		if(!empty($strategy_consequences_data)) { 
			foreach($strategys as $data)	{
				  $selected = "";
			  foreach($strategy_consequences_data as $strategy_consequences_value){	
			  	if($strategy_consequences_value == $data->strategy_name){
						$selected = "checked";
					} 
			  }
			  
			  ?>
                    <div class="span3">
                  <div>
                <input type="checkbox" name="strategy_consequences[]" value="<?php echo $data->strategy_name;?>" <?php echo $selected;?>><?php echo $data->strategy_name;?></br>
                </div>
                  <!--<div style="padding:0px; margin:-18px 0px 0px 25px" ></div>-->
                    </div>
                  <?php }}?>
             </div>
          </div>
       </div>
               
                      
                      
                                
                                
                                 <br /><br />
                                      <center><button type="submit" name="submit" id="parent_teacher_form" value="submit"  class="btn btn-large btn-yellow"><i class="icon-save icon-white"></i> Save</button>
                                      
                                               <button type="submit" name="submit" id="success_team_form" value="submit"  class="btn btn-large btn-yellow"><i class="icon-save icon-white"></i>Print</button> 
                                      
                                      </center>   
                                     </div>
                                      
                                              <ul class="pager wizard">
                                         <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                         <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                         <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                         <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                     </div> 
                                     
                                    
                 </div>
                 </form>
</div></div></div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>