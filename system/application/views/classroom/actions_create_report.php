<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/student_success_team">Student Success Team</a>
                            <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom/student_success_actions">Actions </a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                           <a href="<?php echo base_url();?>classroom/actions_create_report">Create Report</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     
                     <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Create Report

						</h4>
                          
                         </div>
                         <div class="widget-body">
                     <form method="post" action="">
           
                     
                    <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Home

						</h4>
                          
                         </div>
                         <div class="widget-body">
                            <div class="form-horizontal">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                              
                                  <div class="control-group">
                                    <label class="control-label" ></label>
                                    <div class="controls" id="addinput">
             <input type="text" id="p_new" size="16" name="home" value="" class="m-ctrl-medium" placeholder="Input Value" />&nbsp;<a href="#" id="addNew"><i class="icon-plus"></i></a>
                                    </div>
                                </div>
                              
                                 
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     </div>
                 </div>
             </div>
             
             
              <div class="widget yellow">
                         <div class="widget-title">
                             <h4>School

						</h4>
                          
                         </div>
                         <div class="widget-body">
                            <div class="form-horizontal">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                              
                             <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls" id="addinput_text">
                                       <input type="text" id="school" size="16" name="p_new" value="" class="m-ctrl-medium" placeholder="Input Value" />&nbsp;<a href="#" id="addNew_text"><i class="icon-plus"></i></a>
                                    </div>
                                </div>
                              
                                 
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     </div>
                 </div>
             </div>
             
             
              <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Medical

						</h4>
                          
                         </div>
                         <div class="widget-body">
                            <div class="form-horizontal">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                              
                                       <div class="control-group">
                                    <label class="control-label"></label>
                                     <div class="controls" id="health_text">
  <input type="text" id="p_new" size="16" name="medical" value="" class="m-ctrl-medium" placeholder="Input Value" />&nbsp;<a href="#" id="healthNew_text"><i class="icon-plus"></i></a>
                                    </div>
                                </div>
                              
                                 
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                     </div>
                 </div>
             </div>
              <center><button type="submit" name="submit"  class="btn btn-large btn-yellow"><i class="icon-save icon-white"></i> Save </button> </center>
                       </form>
             
                     </div>
                     </div>
                     </div>

                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
               </div>
             </div>
                    
         <!-- END PAGE CONTAINER-->
   
      <!-- END PAGE -->  
   
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   
   <!-- END JAVASCRIPTS --> 
   
<script>
<!--create family text box -->
	
	$(function() {
	var addDiv = $('#addinput');
	var i = $('#addinput p').size() + 1;
	 
	$('#addNew').live('click', function() {
	$('<p></br><input type="text" id="p_new" size="16" class="m-ctrl-medium" name="home_' + i +'" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!--end family text box -->	
<!--create school text box -->
	$(function() {
	var addDiv = $('#addinput_text');
	var i = $('#addinput_text p').size() + 1;
	 
	$('#addNew_text').live('click', function() {
	$('<p></br><input type="text" id="p_new" size="16" class="m-ctrl-medium" name="school_' + i +'" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="remNew_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#remNew_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
	<!--end scool text box -->
<!--create health text box -->
	$(function() {
	var addDiv = $('#health_text');
	var i = $('#health_text p').size() + 1;
	 
	$('#healthNew_text').live('click', function() {
	$('<p></br><input type="text" id="p_new" size="16" class="m-ctrl-medium" name="medical_' + i +'" value="" placeholder="Enter your value" />&nbsp;<a href="#" id="rem_healthNew_New_text"><i class="icon-minus-sign"></i></a> </p>').appendTo(addDiv);
	i++;
	 
	return false;
	});
	 
	$('#rem_healthNew_New_text').live('click', function() {
	if( i > 1 ) {
	$(this).parents('p').remove();
	i--;
	}
	return false;
	});
	});
<!-- end health text box -->	
	</script>   
   
 
</body>
<!-- END BODY -->
</html>