<table class="table table-striped table-hover table-bordered" id="editable-sample" >
    <thead>
        <tr>
            <th>S.No</th>	                                     
            <th>Student Name</th>
            <th>PDF</th>
            <th>Grade Name</th> 
            <th>Date</th>
        </tr>
    <?php
    $cnt = 1;
    foreach ($retrieve_reports as $retrieve_report) {
        ?>       
        </thead>
        <tbody>
            <tr class="">
                <td class="name"><?php echo $cnt; // echo $retrieve_report->id; ?></td>
                <td class="name"> <?php echo ($retrieve_report['details']->firstname . ' ' . $retrieve_report['details']->lastname . ''); ?></td>
                <td>
                    <a class="edit" target="_blank" href="<?php echo base_url() . 'classroom/createsstpdf/' . $retrieve_report['details']->user_id . '/' . $retrieve_report['details']->student_id . '/' . $retrieve_report['details']->date . '/' . $retrieve_report['details']->id ?>">SST Portrait</a>
                    <a class="edit" target="_blank" href="<?php echo base_url() . 'classroom/createsstlandscape/' . $retrieve_report['details']->user_id . '/' . $retrieve_report['details']->student_id . '/' . $retrieve_report['details']->date . '/' . $retrieve_report['details']->id ?>">SST Landscape</a>
                </td>
                <td><?php echo $retrieve_report['details']->grade_name; ?> </td>
                <td><?php echo $retrieve_report['details']->date; ?> </td>
            </tr>
        <?php $cnt++;
    } ?>

                                                                    </tbody>
                                                                </table>