<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/behavior_record">Behavior Running Record</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>classroom/edit_behavior">Edit</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Edit Report</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-yellow3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-yellow progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                 
                                <div class="control-group">
                                    <label class="control-label">Select Teacher</label>
                                    <div class="controls">
                                        
                                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>Teacher 1</option>
                                        <option>Teacher 2</option>
                                        <option>Teacher 3</option>
                                    </select>
                                    </div>
                                </div>

 <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium"></input>
                                    </div>
                                </div>
                                
                                
                                
                               <div class="control-group">
                                             <label class="control-label">Select Lesson Time</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>8:00AM-9:00AM</option>
                                        <option>9:00AM-10:00AM</option>
                                        <option>10:00AM-11:00AM</option>
                                        <option>11:00AM-12:00PM</option>
                                        <option>12:00PM-1:00PM</option>
                                        <option>2:00PM-3:00PM</option>
                                            
                                    </select>
                                             </div>
                                         </div>
                                        
                                       
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                         
                                        <div class="control-group">
                                             <label class="control-label">Select Time Interval</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option>1 Min</option>
                                        <option>3 Min</option>
                                        <option>5 Min</option>
                                        <option>10 Min</option>
                                        <option>15 Min</option>
                                        <option>30 Min</option>
                                         <option>45 Min</option>   
                                    </select>
                                             </div>
                                         </div>
                                         
                                         
                                          <div class="control-group">
                                             <label class="control-label">Select Subject</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option value="Math">Math</option>
                                        <option value="English">English</option>
                                        <option value="Science">Science</option>
                                        <option value="History">History</option>
                                    </select>
                                             </div>
                                         </div>
                                      
                                      
                                     </div> 
                                     
                                     
                                     
                                      <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000">STEP 3</h3>
                                    <div class="space20"></div>
                                    
                               
                                         
                             <div class="space20"></div>
                             
                             <table class="table table-striped table-bordered table-advance table-hover">
                          
                              <thead>
                                    <tr>
                                    <th style="text-align:center; font-weight: bold;"><i class="icon-book"></i> Lesson Plan Book</th>
                                     <th style="text-align:center; font-weight: bold;"><i class="icon-calendar"></i> Date: 12/29/2013</th>
                                      <th style="text-align:center; font-weight: bold;"><i class="icon-sun"></i> Day: Sunday</th>
                                    </tr>
                                    
                                    </thead>

                                </table>
                                
                                <div class="space20"></div>
                                
                                <table class="table table-striped table-bordered table-advance table-hover">
                                <thead><tr>
                                <th style="text-align:center">Time</th>
                                <th style="text-align:center">Subject</th>
                                <th style="text-align:center">Lesson Type</th>
                                <th style="text-align:center">Instructional Strategies</th>
                                <th style="text-align:center">Response to Intervention</th>
                                <th style="text-align:center">Details</th>
                                
                                 <tbody>
                                    
                                   <tr>
                                   <td style="text-align:center">Time</td>                                  <td style="text-align:center">Subject Here</td>
                                   <td style="text-align:center">Lesson Type</td>                                   
                                   <td style="text-align:center">Strategy Here</td>
                                   <td style="text-align:center">Response Here</td>                                   
                                   <td style="text-align:center">Details Here</td>
                                   
                                   </tr>
                                   </tbody>
                                   </table>
                                               
                                        
                                         <div class="control-group">
                             
      <div style="max-width:980px; overflow-x:scroll ; overflow-y: visible; padding-bottom:20px; ">            
  
  
   <div class="space15"></div>
                                 <table class="table table-striped table-hover table-bordered" id="editable-sample" style="min-width:3000px;">
                                     <thead>
                                     <tr>
                                     <th style="width:8px;"><input type="checkbox" class="group-checkable"  /></th>
                                         <th>Student</th>
                                         <th>Edit</th>
                                         <th>Delete</th>
                                         <th>8:03 am</th>
                                         <th>8:06 am</th>
                                         <th>8:09 am</th>
                                         <th>8:12 am</th>
                                         <th>8:15 am</th>
                                         <th>8:18 am</th>
                                         <th>8:21 am</th>
                                         <th>8:24 am</th>
                                         <th>8:27 am</th>
                                         <th>8:30 am</th>
                                         <th>8:33 am</th>
                                         <th>8:36 am</th>
                                         <th>8:39 am</th>
                                         <th>8:42 am</th>
                                         <th>8:45 am</th>
                                         <th>8:48 am</th>
                                         <th>8:51 am</th>
                                         <th>8:54 am</th>
                                         <th>8:57 am</th>
                                         <th>9:00 am</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                     <tr class="">
                                     <td ><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td class="name">John Doe</td>
                                         <td><a class="edit" href="javascript:;">Edit</a></td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                         <td> </td>
                                         <td> </td>
                                         <td></td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         
                                     </tr>
                                     
                                        <tr class="">
                                     <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                         <td class="name">Jane Doe</td>
                                         <td><a class="edit" href="javascript:;">Edit</a></td>
                                         <td><a class="delete" href="javascript:;">Delete</a></td>
                                         <td> </td>
                                         <td> </td>
                                         <td></td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         <td> </td>
                                         
                                     </tr>
                    
                                     </tbody>
                                 </table>
                                 
                                 
                                 
                                 </div>
                                 
                                 <div class="space20"></div>
                                 
                                   <center><button class="btn btn-large btn-yellow"><i class="icon-save icon-white"></i> Save File</button> </center>
                                 
                                 <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-yellow"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-yellow"><i class="icon-envelope icon-white"></i> Send to Parent</button> <button class="btn btn-large btn-yellow"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                                 
                        
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                         <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                         <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                         <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  

 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>