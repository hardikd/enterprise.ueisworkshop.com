<?php

class Classroommodel extends Model {

    function __construct() {
        parent::__construct();
    }

    function getschooltype($district_id) {

        $qry = "Select * from  school_type where district_id='" . $district_id . "' ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getschoolgrades($district_id) {

        $qry = "Select * from dist_grades where district_id='" . $district_id . "'";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function strengths_insert($table, $field) {
        return $this->db->insert($table, $field);
    }

    public function parent_teacher_insert($table, $field) {
        $result = $this->db->insert($table, $field);
        if ($result) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {
            return false;
        }
    }

    public function sst_insert($table, $field) {
        $result = $this->db->insert($table, $field);
        if ($result) {
            $insert_id = $this->db->insert_id();
            //step3
            $need_home = $this->input->post('needs_home');
            $need_school = $this->input->post('needs_school');
            $need_medical = $this->input->post('needs_medical');
            foreach ($need_home as $needHome) {
                $needhomearr = array('sst_id' => $insert_id, 'need_id' => $needHome);
                $this->db->insert('sst_needs_home', $needhomearr);
            }
            foreach ($need_school as $needSchool) {
                $needschoolarr = array('sst_id' => $insert_id, 'need_id' => $needSchool);
                $this->db->insert('sst_needs_school', $needschoolarr);
            }
            foreach ($need_medical as $needMedical) {
                $needmedicalarr = array('sst_id' => $insert_id, 'need_id' => $needMedical);
                $this->db->insert('sst_needs_medical', $needmedicalarr);
            }

            //step 4

            $needs_action_home = $this->input->post('needs_action_home');
            $needs_action_school = $this->input->post('needs_action_school');
            $needs_action_medical = $this->input->post('needs_action_medical');

            $actions_home = $this->input->post('action_home');
            $actions_school = $this->input->post('action_school');
            $actions_medical = $this->input->post('action_medical');

            $school_id = $this->session->userdata('school_id');
            $action_home_responsible = $this->input->post('action_home_responsible');
            $action_school_responsible = $this->input->post('action_school_responsible');
            $action_medical_responsible = $this->input->post('action_medical_responsible');

            foreach ($needs_action_home as $key => $nah) {
                $naharr = array('sst_id' => $insert_id, 'need_id' => $nah, 'action_id' => $actions_home[$key], 'responsible_person' => $action_home_responsible[$key]);
                $this->db->insert('sst_actions_home', $naharr);
            }
            foreach ($needs_action_school as $key => $nas) {
                $nasarr = array('sst_id' => $insert_id, 'need_id' => $nas, 'action_id' => $actions_school[$key], 'responsible_person' => $action_school_responsible[$key]);
                $this->db->insert('sst_actions_school', $nasarr);
            }
            foreach ($needs_action_medical as $key => $nam) {
                $namarr = array('sst_id' => $insert_id, 'need_id' => $nam, 'action_id' => $actions_medical[$key], 'responsible_person' => $action_medical_responsible[$key]);
                $this->db->insert('sst_actions_medical', $namarr);
            }


            //echo $this->db->last_query();exit;
            return $insert_id;
        } else {
            return false;
        }
    }

    function getStudent($scid, $gid) {
        $qry = "Select * from users where school_id='" . $scid . "' and 	grade_id='" . $gid . "' ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function strengths() {
        $query = $this->db->get('strengths');
        $result = $query->result();
        return $result;
    }

    function concerns() {
        $query = $this->db->get('concerns');
        $result = $query->result();
        return $result;
    }

    function ideas_for_parent_student() {
        $query = $this->db->get('ideas_for_parent_student');
        $result = $query->result();
        return $result;
    }

    public function upload_success_report() {

        $this->db->where('teacher_id', $this->session->userdata('teacher_id'));
        $this->db->where('school_id', $this->session->userdata('school_id'));
        $query = $this->db->get('student_success_strengths');

        $result = $query->num_rows();
        return $result;
    }

    public function update_data($table, $field, $where) {
        // $where1 = ( id = $where );	
        //		print_r($where);
        //	exit;
        $this->db->where('id', $where);
        return $this->db->update($table, $field);
    }

    function getgradeBysubject($grade_id) {
        $this->db->select('*');
        $this->db->from('grade_subjects');
        $this->db->join('dist_subjects', 'grade_subjects.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->where('grade_subjects.grade_id', $grade_id);
        //print_r($this->db->last_query());exit;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getgradeBystudents($grade_id) {
        $this->db->select('*');
        $this->db->from('students');
        $this->db->where('students.grade', $grade_id);
        //print_r($this->db->last_query());exit;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getteacherBystudents($teacher_id) {
        $this->db->select('*');
        $this->db->from('teacher_students');
        $this->db->join('students', 'teacher_students.student_id = students.student_id');

        $this->db->where('teacher_students.teacher_id', $teacher_id);
        $this->db->group_by('students.student_id');
        $query = $this->db->get();
//		print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_students_By_grade($student_id) {
        $this->db->select('*');
        $this->db->from('students');
        $this->db->join('dist_grades', 'students.grade = dist_grades.dist_grade_id');

        $this->db->where('students.student_id', $student_id);
        //$this->db->group_by('students.student_id');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_students_number_By_grade($student_number = '', $lastname = '', $firstname = '') {
        $this->db->select('*');
        $this->db->from('students');
        $this->db->join('dist_grades', 'students.grade = dist_grades.dist_grade_id');

        $this->db->where('students.student_number', $student_number);
        $this->db->where('students.lastname', $lastname);
        $this->db->where('students.firstname', $firstname);
        //$this->db->group_by('students.student_id');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getstudentById($student_id) {
        $this->db->select('students.student_id,students.firstname,students.lastname,students.parents_id,students.grade,students.email,students.student_number,users.UserName,users.pass,users.address,users.phone,parents.language,parents.languagesub');
        $this->db->from('students');
        $this->db->join('users', 'students.student_id = users.student_id');
        $this->db->join('parents', 'users.parents_id = parents.parents_id');
        $this->db->where('students.student_id', $student_id);
        //$this->db->group_by('students.student_id');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_subject_By_objective($subject_id) {
        $this->db->select('*');
        $this->db->from('academic_achievement');
        $this->db->where('academic_achievement.subject_id', $subject_id);
        $this->db->where('is_delete', '0');
        //$this->db->group_by('students.student_id');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function success_report_pdf($teacher_id, $student_id, $date, $id) {

        $this->db->select('*');
        $this->db->from('student_success_strengths');
        if ($this->session->userdata('login_type') == 'teacher') {
            $this->db->join('teachers', 'student_success_strengths.user_id = teachers.teacher_id', 'LEFT');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $this->db->join('observers', 'student_success_strengths.user_id = observers.observer_id', 'LEFT');
        }
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id', 'LEFT');
        $this->db->join('districts', 'student_success_strengths.district_id = districts.district_id', 'LEFT');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id', 'LEFT');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('student_success_strengths.user_id', $teacher_id);
        $this->db->where('student_success_strengths.student_id', $student_id);
        $this->db->where('student_success_strengths.date', $date);
        $this->db->where('student_success_strengths.id', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function retrieve_success_report_need() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }
        if ($this->input->post('grade_id')) {
            $grade_id = $this->input->post('grade_id');
        }
        if ($this->input->post('student_id')) {
            $student_id = $this->input->post('student_id');
        }
        if ($this->input->post('date')) {
            $date = $this->input->post('date');
        }
        if ($this->input->post('school_id')) {
            $school_id = $this->input->post('school_id');
        }

        $this->db->select('sst_id');
        if ($this->input->post('home_strat') != 0) {
            $this->db->where('need_id', $this->input->post('home_strat'));
        }
        $this->db->from('sst_needs_home');
        $qryneedhome = $this->db->get();
        $resultneedhome = $qryneedhome->result();


        $this->db->select('sst_id');
        if ($this->input->post('school_strat') != 0) {
            $this->db->where('need_id', $this->input->post('school_strat'));
        }
        $this->db->from('sst_needs_school');
        $qryneedschool = $this->db->get();
        $resultneedschool = $qryneedschool->result();


        $this->db->select('sst_id');
        if ($this->input->post('medical_strat') != 0) {
            $this->db->where('need_id', $this->input->post('medical_strat'));
        }
        $this->db->from('sst_needs_medical');
        $qryneedmedical = $this->db->get();
        $resultneedmedical = $qryneedmedical->result();



        $sst_ids = array_merge_recursive($resultneedhome, $resultneedmedical, $resultneedschool);

        foreach ($sst_ids as $sst_id) {
            if (!in_array($sst_id->sst_id, $sst_id_arr))
                $sst_id_arr[] = $sst_id->sst_id;
        }
        
        
        if($this->session->userdata('login_type')=='teacher') {
            $this->db->select('teacher_students.student_id');
            $this->db->from('teacher_students');
            $this->db->join('students','teacher_students.student_id = students.student_id','LEFT');
            $this->db->where('teacher_students.teacher_id',$user_id);
            $this->db->group_by('teacher_students.student_id');
            $query = $this->db->get();
            $students_idss = $query->result_array();
            
            foreach($students_idss as $studentIds){
                $student_ids[] = $studentIds['student_id'];
            }
        }
        
//        print_r($sst_id_arr);exit;
        $this->db->select('student_success_strengths.id,student_success_strengths.user_type,student_success_strengths.user_id,student_success_strengths.school_id,student_success_strengths.district_id,student_success_strengths.date,student_success_strengths.student_id,student_success_strengths.grade_id,students.firstname,students.lastname,schools.school_name,dist_grades.grade_name,');

        $this->db->from('student_success_strengths');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id');
        
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id');
        // $this->db->where_in('student_success_strengths.id', $sst_id_arr);
        if($this->session->userdata('login_type')=='teacher') {
           $this->db->where_in('student_success_strengths.student_id', $student_ids); 
        }
        if($this->session->userdata('login_type')=='observer'){
         $this->db->where_in('student_success_strengths.school_id', $this->session->userdata('school_id'));   
        } else if($this->session->userdata('login_type')=='user'){
            $this->db->where_in('student_success_strengths.district_id', $this->session->userdata('district_id'));
        }

        
        if ($grade_id != '' && $grade_id != 0)
            $this->db->where('student_success_strengths.grade_id', $grade_id);
        if ($student_id != '' && $student_id != 0)
            $this->db->where('student_success_strengths.student_id', $student_id);
        if ($date != '')
            $this->db->where('YEAR(student_success_strengths.date)', $date);
        if ($school_id != '' && $school_id != 0)
            $this->db->where('student_success_strengths.school_id', $school_id);

//        if ($this->session->userdata('login_type') != 'user' && $this->session->userdata('login_type') != 'observer')
//            $this->db->where('student_success_strengths.user_id', $user_id);
        $query = $this->db->get();
       // echo $this->db->last_query();exit;
        $results = $query->result();
        if ($results) {
            foreach ($results as $key => $result) {

                $this->db->select('sst_needs_home.sst_id,sst_needs_home.need_id,intervention_strategies_home.intervention_strategies_home');
                $this->db->from('sst_needs_home');
                $this->db->join('intervention_strategies_home', 'intervention_strategies_home.id = sst_needs_home.need_id');
                if ($this->input->post('home_strat') != 0) {
                    $this->db->where('need_id', $this->input->post('home_strat'));
                }
                $this->db->where('sst_id', $result->id);

                $qryneedhome = $this->db->get();
                $arrneedhome = $qryneedhome->result();
                //print_r($arrneedhome);exit;


                $this->db->select('sst_needs_school.sst_id,sst_needs_school.need_id,intervention_strategies_school.intervention_strategies_school');
                $this->db->from('sst_needs_school');
                $this->db->join('intervention_strategies_school', 'intervention_strategies_school.id = sst_needs_school.need_id');
                if ($this->input->post('school_strat') != 0) {
                    $this->db->where('need_id', $this->input->post('school_strat'));
                }
                $this->db->where('sst_id', $result->id);
                $qryneedschool = $this->db->get();
                $arrneedschool = $qryneedschool->result();



                $this->db->select('sst_needs_medical.sst_id,sst_needs_medical.need_id,intervention_strategies_medical.intervention_strategies_medical');
                $this->db->from('sst_needs_medical');
                $this->db->join('intervention_strategies_medical', 'intervention_strategies_medical.id = sst_needs_medical.need_id');
                if ($this->input->post('medical_strat') != 0) {
                    $this->db->where('need_id', $this->input->post('medical_strat'));
                }
                $this->db->where('sst_id', $result->id);
                $qryneedmedical = $this->db->get();
                $arrultneedmedical = $qryneedmedical->result();

                $this->db->select('sst_actions_home.action_id,actions_home.actions_home,intervention_strategies_home.intervention_strategies_home');
                $this->db->from('sst_actions_home');
                $this->db->join('actions_home', 'actions_home.action_home_id = sst_actions_home.action_id');
                $this->db->join('intervention_strategies_home', 'intervention_strategies_home.id = sst_actions_home.need_id');
                $this->db->where('sst_id', $result->id);
                $qryactionhome = $this->db->get();
                $arractionhome = $qryactionhome->result();


                $this->db->select('sst_actions_school.action_id,actions_school.actions_school,intervention_strategies_school.intervention_strategies_school');
                $this->db->from('sst_actions_school');
                $this->db->join('actions_school', 'actions_school.action_school_id = sst_actions_school.action_id');
                $this->db->join('intervention_strategies_school', 'intervention_strategies_school.id = sst_actions_school.need_id');
                $this->db->where('sst_id', $result->id);
                $qryschoolhome = $this->db->get();
                $arractionschool = $qryschoolhome->result();


                $this->db->select('sst_actions_medical.action_id,actions_medical.actions_medical,intervention_strategies_medical.intervention_strategies_medical');
                $this->db->from('sst_actions_medical');
                $this->db->join('actions_medical', 'actions_medical.action_medical_id = sst_actions_medical.action_id');
                $this->db->join('intervention_strategies_medical', 'intervention_strategies_medical.id = sst_actions_medical.need_id');
                $this->db->where('sst_id', $result->id);
                $qryactionmedical = $this->db->get();
                $arractionmedical = $qryactionmedical->result();



                $returnarr[$result->id]['details'] = $result;
                $returnarr[$result->id]['need_home'] = $arrneedhome;
                $returnarr[$result->id]['need_school'] = $arrneedschool;
                $returnarr[$result->id]['need_medical'] = $arrultneedmedical;
                $returnarr[$result->id]['action_home'] = $arractionhome;
                $returnarr[$result->id]['action_school'] = $arractionschool;
                $returnarr[$result->id]['action_medical'] = $arractionmedical;
            }
            return $returnarr;
        } else {
            return false;
        }
    }

    public function retrieve_success_report_pdf($grade_id, $student_id, $date) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        if ($this->input->post('grade_id')) {
            $grade_id = $this->input->post('grade_id');
        }
        if ($this->input->post('student_id')) {
            $student_id = $this->input->post('student_id');
        }
        if ($this->input->post('date')) {
            $date = $this->input->post('date');
        }


//        print_r($sst_id_arr);exit;
        $this->db->select('student_success_strengths.id,student_success_strengths.user_type,student_success_strengths.user_id,student_success_strengths.school_id,student_success_strengths.district_id,student_success_strengths.date,student_success_strengths.student_id,student_success_strengths.grade_id,sst_needs_home.need_id as need_home,schools.school_name,students.firstname,students.lastname,intervention_strategies_home.intervention_strategies_home as need_home_name,sst_needs_school.need_id as school_need,sst_needs_medical.need_id as medical_need');
        $this->db->from('student_success_strengths');
        //$this->db->join('teachers','student_success_strengths.user_id = teachers.teacher_id','LEFT');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id', 'LEFT');
//	$this->db->join('districts','student_success_strengths.district_id = districts.district_id','LEFT');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id', 'LEFT');
//	$this->db->join('dist_grades','student_success_strengths.grade_id = dist_grades.dist_grade_id','LEFT');
        $this->db->join('sst_needs_home', 'student_success_strengths.id = sst_needs_home.sst_id');
        $this->db->join('sst_needs_school', 'student_success_strengths.id = sst_needs_school.sst_id', 'LEFT');
        $this->db->join('sst_needs_medical', 'student_success_strengths.id = sst_needs_medical.sst_id', 'LEFT');
        $this->db->join('intervention_strategies_home', 'intervention_strategies_home.id = sst_needs_home.need_id');
        $this->db->group_by(array('sst_needs_school.need_id', 'sst_needs_medical.need_id'));


        if ($grade_id != '' && $grade_id != 0)
            $this->db->where('student_success_strengths.grade_id', $grade_id);
        if ($student_id != '' && $student_id != 0)
            $this->db->where('student_success_strengths.student_id', $student_id);
        if ($date != '')
            $this->db->where('YEAR(student_success_strengths.date)', $date);

        $this->db->where('student_success_strengths.user_id', $user_id);

        $query = $this->db->get();

//			print_r($this->db->last_query());exit;
        $result = $query->result();
//                print_r($result);exit;
        return $result;
    }

    public function update_success_report() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }
        $this->db->select('*');
        $this->db->from('student_success_strengths');
//	$this->db->join('teachers','student_success_strengths.teacher_id = teachers.teacher_id','LEFT');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id', 'LEFT');
        $this->db->join('districts', 'student_success_strengths.district_id = districts.district_id', 'LEFT');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id', 'LEFT');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('student_success_strengths.user_id', $user_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;
        $result = $query->result();
        return $result;
    }

    public function get_need_data($result) {

        $this->db->select('*');
        $this->db->from('student_success_strengths');
        $this->db->join('teachers', 'student_success_strengths.user_id = teachers.teacher_id', 'LEFT');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id', 'LEFT');
        $this->db->join('districts', 'student_success_strengths.district_id = districts.district_id', 'LEFT');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id', 'LEFT');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('student_success_strengths.id', $result);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;
        $result = $query->result();
        return $result;
    }

    public function update_success($id) {

        $this->db->select('*');
        $this->db->from('student_success_strengths');
        //$this->db->join('teachers','student_success_strengths.teacher_id = teachers.teacher_id','LEFT');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id', 'LEFT');
        $this->db->join('districts', 'student_success_strengths.district_id = districts.district_id', 'LEFT');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id', 'LEFT');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('student_success_strengths.id', $id);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        $result = $query->result();
        return $result;
    }

    public function get_parent_conference() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('parent_teacher_conference');
        $this->db->join('dist_users', 'parent_teacher_conference.user_id = dist_users.dist_user_id');
//	$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
        $this->db->join('districts', 'parent_teacher_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'parent_teacher_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'parent_teacher_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'parent_teacher_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'parent_teacher_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'parent_teacher_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('parent_teacher_conference.user_id', $user_id);

        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_parent_conference_data($id) {
//	print_r($this->session->all_userdata());exit;
        $this->db->select('*');
        $this->db->from('parent_teacher_conference');
        $this->db->join('dist_users', 'parent_teacher_conference.user_id = dist_users.dist_user_id');
        //$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id');
        $this->db->join('districts', 'parent_teacher_conference.district_id = districts.district_id');
        $this->db->join('schools', 'parent_teacher_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'parent_teacher_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('dist_subjects', 'parent_teacher_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('periods', 'parent_teacher_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('students', 'parent_teacher_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('parent_teacher_conference.id', $id);
        $query = $this->db->get();
        $result = $query->result();
        //print_r($result);exit;
        return $result;
    }

    public function get_parent_conference_data_user($id) {
//	print_r($this->session->all_userdata());exit;
        $this->db->select('dist_users.dist_user_id,dist_users.username,schools.school_name,schools.school_name,dist_grades.dist_grade_id,dist_grades.grade_name,dist_subjects.dist_subject_id,dist_subjects.subject_name,periods.start_time,periods.end_time,students.firstname,students.lastname,parent_teacher_conference.id,parent_teacher_conference.user_type,parent_teacher_conference.user_id,parent_teacher_conference.district_id,parent_teacher_conference.school_id,parent_teacher_conference.date,parent_teacher_conference.grade_id,parent_teacher_conference.period_id,parent_teacher_conference.subject_id,parent_teacher_conference.student_id,parent_teacher_conference.comment,parent_teacher_conference.strengths,parent_teacher_conference.concerns,parent_teacher_conference.ideas_for_parent_student');
        $this->db->from('parent_teacher_conference');
        $this->db->join('dist_users', 'parent_teacher_conference.user_id = dist_users.dist_user_id');
        $this->db->join('districts', 'parent_teacher_conference.district_id = districts.district_id');
        $this->db->join('schools', 'parent_teacher_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'parent_teacher_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('dist_subjects', 'parent_teacher_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('periods', 'parent_teacher_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('students', 'parent_teacher_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('parent_teacher_conference.id', $id);
        $query = $this->db->get();
        $result = $query->result();
        //print_r($result);exit;
        return $result;
    }

    public function progress_subject_report($subject_id) {
        $district_id = $this->session->userdata('district_id');

        $this->db->select('*');
        $this->db->from('standard_name');
        $this->db->join('dist_subjects', 'standard_name.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        //$this->db->join('standard_language','standard_name.standard_language_id = standard_language.id','LEFT');
        $this->db->where('standard_name.subject_id', $subject_id);
        $this->db->where('standard_name.district_id', $district_id);
        $this->db->where('standard_name.is_delete', '0');
        $this->db->order_by('subject_id', 'desc');
        $query = $this->db->get();
//	print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function progress_report_pdf($student_id, $grade_id, $date, $id) {

        $this->db->select('*');
        $this->db->from('student_progress_report');
        $this->db->join('dist_users', 'student_progress_report.user_id = dist_users.dist_user_id');
        //$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
        $this->db->join('districts', 'student_progress_report.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'student_progress_report.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_progress_report.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'student_progress_report.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_progress_report.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'student_progress_report.student_id = students.student_id', 'LEFT');
        $this->db->where('student_progress_report.student_id', $student_id);
        $this->db->where('student_progress_report.grade_id', $grade_id);
        $this->db->where('student_progress_report.date', $date);
        $this->db->where('student_progress_report.progress_report_id', $id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function progress_subject_report_pdf($subject_id) {
        $district_id = $this->session->userdata('district_id');

        $this->db->select('*');
        $this->db->from('standard_name as sn');
        $this->db->join('dist_subjects as ds', 'sn.subject_id = ds.dist_subject_id', 'LEFT');
        $this->db->join('progress_report_score as prs', 'sn.subject_id = prs.subject_id', 'LEFT');
        $this->db->join('statements_score as ss1', 'prs.score_q1 = ss1.id', 'LEFT');
        $this->db->join('statements_score as ss2', 'prs.score_q2 = ss2.id', 'LEFT');
        $this->db->join('statements_score as ss3', 'prs.score_q3 = ss3.id', 'LEFT');
        $this->db->join('statements_score as ss4', 'prs.score_q4 = ss4.id', 'LEFT');
        //$this->db->join('statements_score','progress_report_score.score_q2 = statements_score.id');
        //$this->db->join('standard_language','standard_name.standard_language_id = standard_language.id','LEFT');


        /*
          $this->db->select('*');
          $this->db->from('team_has_fixture as tf');
          $this->db->join('team as t1', 'tf.team_team_id = t1.team_id');
          $this->db->join('team as t2', 'tf.team_team_id2 = t2.team_id');
          $this->db->join('fixture', 'tf.fixture_fixture_id = fixture.fixture_id');
         */


        $this->db->where('sn.subject_id', $subject_id);
        $this->db->where('sn.district_id', $district_id);
        $this->db->where('sn.is_delete', '0');
        //$this->db->order_by('subject_id','desc');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function parent_conference_pdf($student_id, $grade_id, $date, $id) {

        $this->db->select('*');
        $this->db->from('parent_teacher_conference');
        $this->db->join('dist_users', 'parent_teacher_conference.user_id = dist_users.dist_user_id', 'LEFT');
        //$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
        $this->db->join('districts', 'parent_teacher_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'parent_teacher_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'parent_teacher_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'parent_teacher_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'parent_teacher_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'parent_teacher_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('parent_teacher_conference.student_id', $student_id);
        $this->db->where('parent_teacher_conference.grade_id', $grade_id);
        $this->db->where('parent_teacher_conference.date', $date);
        $this->db->where('parent_teacher_conference.id', $id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function parent_conference_report_pdf($date, $grade_id, $student_id, $school_id) {

        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
            $this->db->select('teacher_students.student_id');
            $this->db->from('teacher_students');
            $this->db->join('students','teacher_students.student_id = students.student_id','LEFT');
            $this->db->where('teacher_students.teacher_id',$user_id);
            $this->db->group_by('teacher_students.student_id');
            $query = $this->db->get();
            $result_students = $query->result_array();
            foreach($result_students as $result_student){
                $student_ids[] = $result_student['student_id']; 
            }
        } else if ($this->session->userdata('login_type') == 'observer') {
            if ($this->input->post('teacher_id')){
                $user_id = $this->input->post('teacher_id');
                $this->db->select('teacher_students.student_id');
            $this->db->from('teacher_students');
            $this->db->join('students','teacher_students.student_id = students.student_id','LEFT');
            $this->db->where('teacher_students.teacher_id',$user_id);
            $this->db->group_by('teacher_students.student_id');
            $query = $this->db->get();
            $result_students = $query->result_array();
            foreach($result_students as $result_student){
                $student_ids[] = $result_student['student_id']; 
            }
            }
        } else if ($this->session->userdata('login_type') == 'user') {
            if ($this->input->post('teacher_id'))
                $user_id = $this->input->post('teacher_id');
        }

        $this->db->select('*');
        $this->db->from('parent_teacher_conference');
        $this->db->join('dist_users', 'parent_teacher_conference.user_id = dist_users.dist_user_id', 'LEFT');
//	$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
        $this->db->join('districts', 'parent_teacher_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'parent_teacher_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'parent_teacher_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'parent_teacher_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'parent_teacher_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'parent_teacher_conference.student_id = students.student_id', 'LEFT');
        if ($this->input->post('years'))
            $this->db->where('YEAR(parent_teacher_conference.date)', $this->input->post('years'));
        if ($this->input->post('month') != 'all')
            $this->db->where('MONTH(parent_teacher_conference.date)', $this->input->post('month'));


        if ($this->input->post('student_id') != 'all' && $this->input->post('student_id') != '') {
            if($grade_id!='')
            $this->db->where('parent_teacher_conference.grade_id', $grade_id);
            $this->db->where('parent_teacher_conference.student_id', $student_id);
            $this->db->where('parent_teacher_conference.school_id', $school_id);
        } else {
            $this->db->where_in('parent_teacher_conference.student_id', $student_ids);
            if($grade_id!='')
            $this->db->where('parent_teacher_conference.grade_id', $grade_id);
            $this->db->where('parent_teacher_conference.school_id', $school_id);
        }

        $query = $this->db->get();

//	print_r($this->db->last_query());exit;

        $result = $query->result();
//                print_r($result);exit;
        return $result;
    }

    public function student_conference_pdf($student_id, $grade_id, $date, $id) {

        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('student_educator_conference');
        //$this->db->join('dist_users','parent_teacher_conference.user_id = dist_users.dist_user_id');	
        $this->db->join('teachers', 'student_educator_conference.user_id = teachers.teacher_id', 'LEFT');
        $this->db->join('districts', 'student_educator_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'student_educator_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_educator_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'student_educator_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_educator_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'student_educator_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('student_educator_conference.student_id', $student_id);
        $this->db->where('student_educator_conference.grade_id', $grade_id);
        $this->db->where('student_educator_conference.date', $date);
        $this->db->where('student_educator_conference.id', $id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function student_conference_report_pdf($student_id, $grade_id, $date, $school_id) {

        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
            $this->db->select('teacher_students.student_id');
            $this->db->from('teacher_students');
            $this->db->join('students','teacher_students.student_id = students.student_id','LEFT');
            $this->db->where('teacher_students.teacher_id',$user_id);
            $this->db->group_by('teacher_students.student_id');
            $query = $this->db->get();
            $result_students = $query->result_array();
            foreach($result_students as $result_student){
                $student_ids[] = $result_student['student_id']; 
            }
        } else if ($this->session->userdata('login_type') == 'observer') {
            if ($this->input->post('teacher_id')){
                $user_id = $this->input->post('teacher_id');
                $this->db->select('teacher_students.student_id');
                $this->db->from('teacher_students');
                $this->db->join('students','teacher_students.student_id = students.student_id','LEFT');
                $this->db->where('teacher_students.teacher_id',$user_id);
                $this->db->group_by('teacher_students.student_id');
                $query = $this->db->get();
                $result_students = $query->result_array();
                foreach($result_students as $result_student){
                    $student_ids[] = $result_student['student_id']; 
                }
            }
        } else if ($this->session->userdata('login_type') == 'user') {
            if ($this->input->post('teacher_id'))
                $user_id = $this->input->post('teacher_id');
        }





        $this->db->select('*');
        $this->db->from('student_educator_conference');
        //$this->db->join('dist_users','parent_teacher_conference.user_id = dist_users.dist_user_id');	
        $this->db->join('teachers', 'student_educator_conference.user_id = teachers.teacher_id', 'LEFT');
        $this->db->join('districts', 'student_educator_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'student_educator_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_educator_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'student_educator_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_educator_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'student_educator_conference.student_id = students.student_id', 'LEFT');
        if ($this->input->post('years'))
            $this->db->where('YEAR(student_educator_conference.date)', $this->input->post('years'));
        if ($this->input->post('month') != 'all')
            $this->db->where('MONTH(student_educator_conference.date)', $this->input->post('month'));
        if ($this->input->post('student_id') != 'all' && $this->input->post('student_id') != '') {
            $this->db->where('student_educator_conference.student_id', $student_id);
            if($grade_id!='')
            $this->db->where('student_educator_conference.grade_id', $grade_id);
            $this->db->where('student_educator_conference.school_id', $school_id);
//            $this->db->where('student_educator_conference.user_id', $user_id);
        } else {
            $this->db->where_in('student_educator_conference.student_id', $student_ids);
            if($grade_id!='')
            $this->db->where('student_educator_conference.grade_id', $grade_id);
            $this->db->where('student_educator_conference.school_id', $school_id);
//            $this->db->where('student_educator_conference.user_id', $user_id);
        }

        $query = $this->db->get();

//        print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_student_teacher_conference() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('student_educator_conference');
        //$this->db->join('dist_users','parent_teacher_conference.user_id = dist_users.dist_user_id');	
        $this->db->join('teachers', 'student_educator_conference.user_id = teachers.teacher_id', 'LEFT');
        $this->db->join('districts', 'student_educator_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'student_educator_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_educator_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'student_educator_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_educator_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'student_educator_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('student_educator_conference.user_id', $user_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_student_teacher_data($id) {

        $this->db->select('*');
        $this->db->from('student_educator_conference');
        //$this->db->join('dist_users','parent_teacher_conference.user_id = dist_users.dist_user_id');	
        $this->db->join('teachers', 'student_educator_conference.user_id = teachers.teacher_id', 'LEFT');
        $this->db->join('districts', 'student_educator_conference.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'student_educator_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_educator_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'student_educator_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_educator_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'student_educator_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('student_educator_conference.id', $id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_student_teacher_data_user($id) {
        $this->db->select('dist_users.dist_user_id,dist_users.username,schools.school_name,schools.school_name,dist_grades.dist_grade_id,dist_grades.grade_name,dist_subjects.dist_subject_id,dist_subjects.subject_name,periods.start_time,periods.end_time,students.firstname,students.lastname,student_educator_conference.id,student_educator_conference.user_type,student_educator_conference.user_id,student_educator_conference.district_id,student_educator_conference.school_id,student_educator_conference.date,student_educator_conference.grade_id,student_educator_conference.period_id,student_educator_conference.subject_id,student_educator_conference.student_id,student_educator_conference.comment,student_educator_conference.history,student_educator_conference.behavior,student_educator_conference.strategy_consequences');
        $this->db->from('student_educator_conference');
        $this->db->join('dist_users', 'student_educator_conference.user_id = dist_users.dist_user_id');
        $this->db->join('districts', 'student_educator_conference.district_id = districts.district_id');
        $this->db->join('schools', 'student_educator_conference.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_educator_conference.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_educator_conference.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('periods', 'student_educator_conference.period_id = periods.period_id', 'LEFT');
        $this->db->join('students', 'student_educator_conference.student_id = students.student_id', 'LEFT');
        $this->db->where('student_educator_conference.id', $id);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function getClassroom($scid, $gid) {

        $qry = "Select * from class_rooms where school_id='" . $scid . "' and 	grade_id='" . $gid . "' limit 5 ";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getClassroom2($scid, $gid, $clid) {

        $qry = "Select * from class_rooms where school_id='" . $scid . "' and 	grade_id='" . $gid . "' and class_room_id='" . $clid . "' ";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getClassroomStudent($classroomid) {
        $str = "select distinct(student_id) from teacher_students where class_room_id=" . $classroomid . " ";
        $query = $this->db->query($str);
        $tempres = $query->result_array();

        $tempresarr = array();

        foreach ($tempres as $key => $val) {
            $student_id = $tempres[$key]['student_id'];
            $strdata = "select UserID,UserName,Name from users where student_id=" . $student_id . "";
            $query1 = $this->db->query($strdata);
            $tempresarr[] = $query1->result_array();
        }

        return $tempresarr;
    }

    public function fetchGradeName($grid) {
        $str = "select grade_name from dist_grades where dist_grade_id='$grid' ";
        $data = $this->db->query($str)->result_array();
        $grade_name = @$data[0]['grade_name'];
        return $grade_name;
        exit;
    }

    public function fetchSchoolTypeName($styid) {
        $str = "select tab from school_type where school_type_id='$styid' ";
        $data = $this->db->query($str)->result_array();
        $tab = @$data[0]['tab'];
        return $tab;
        exit;
    }

    public function fetchSchoolName($sid) {
        $str = "select school_name from schools where school_id='$sid' ";
        $data = $this->db->query($str)->result_array();
        $school_name = @$data[0]['school_name'];
        return $school_name;
        exit;
    }

    function getallSchoolIdByClassid($scid) {

        $qry = "Select * from teacher_students where class_room_id='" . $scid . "'  group by student_id";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();


            return $res;
        } else {
            return false;
        }
    }

    function getallusers($school_id, $gradeid) {
        $temp = array();
        for ($i = 0; $i < count($school_id); $i++) {
            $qry = "Select * from  users where  school_id='" . $school_id[$i] . "' and grade_id='" . $gradeid . "' and user_type='2' ";

            $query = $this->db->query($qry);
            $temp[] = $query->result_array();
        }


        foreach ($temp as $r => $v) {
            if (empty($v)) {

                return $temp = 0;
            } else {

                return $temp;
            }
        }
    }

    function studentscore($ssid, $usern, $uid) {
        $temp = array();

        $qry = "select * from user_quizzes where user_id='$ssid' ";

        $query2 = $this->db->query($qry);
        $temp[] = $query2->result_array();

        $temp[] = $usern;
        $temp[] = $uid;

        return $temp;
    }

    function studentscoreByid($ssid, $assids) {

        $temp = array();
        for ($i = 0; $i < count($assids); $i++) {
            $qry4 = "select * from user_quizzes where user_id='$ssid' and assignment_id='" . $assids[$i] . "' group by id  order by id desc limit 1";

            $query4 = $this->db->query($qry4);
            $temp[] = $query4->result_array();
        }
        return $temp;
    }

// 	<h1> Individual School Report</h1>	

    function studentscoreByWrkid($ssid) {
        $qry4 = "select * from users,user_quizzes where users.student_id='$ssid' and user_quizzes.user_id = users.UserID";
        $query4 = $this->db->query($qry4);
        $temp = $query4->result_array();

        return $temp;
    }

// all school by grade working
    public function getAllSchool($district_id, $grdid) {

        $str1 = "select * from schools where district_id='$district_id'";
        $query41 = $this->db->query($str1);
        $schlId = $query41->result_array();

        //echo '<pre>';
        //print_r($schlId);

        $stdata_by_gr_arr = array();

        foreach ($schlId as $key => $val) {
            $school_id = $schlId[$key]['school_id'];
            $school_name = $schlId[$key]['school_name'];

            $str = "select * from users where grade_id='$grdid' and school_id='$school_id'   limit 3";
            $query4 = $this->db->query($str);

            $stdata_by_gr_arr[] = $query4->result_array();
        }


        return $stdata_by_gr_arr;
    }

    public function schoolNameFun($scid) {
        $str = "select * from schools where  school_id='$scid'";
        $query4 = $this->db->query($str)->result_array();

        $stname = $query4[0]['school_name'];
        return $stname;
    }

    public function schoolStdAvg($uid) {

        //$str = "select avg(pass_score_perc) from user_quizzes where  user_id='$uid'";


        $str = "select avg(pass_score_point),assignment_id from user_quizzes where  user_id='$uid'";

        $query4 = $this->db->query($str)->result_array();



        return $query4;
    }

    public function schoolStdAvg1($uid, $assids) {

        //$str = "select avg(pass_score_perc) from user_quizzes where  user_id='$uid'";
        $query4 = array();
        for ($i = 0; $i < count($assids); $i++) {

            //echo $str = "select avg(pass_score_point),assignment_id from user_quizzes where  user_id='$uid' and  assignment_id='".$assids[$i]."'  group by user_id order by id desc";

            $str = "select  avg(pass_score_point),assignment_id from user_quizzes where user_id='$uid' and assignment_id='" . $assids[$i] . "'  group by id  order by id desc limit 1";




            $query4[] = $this->db->query($str)->result_array();
        }


        return $query4;
    }

// all school working


    public function getAllSchoolwithDid($district_id) {

        $str1 = "select * from schools where district_id='$district_id' limit 5";
        $query41 = $this->db->query($str1);
        $schlId = $query41->result_array();

        foreach ($schlId as $key => $val) {
            $school_id = $schlId[$key]['school_id'];
            $school_name = $schlId[$key]['school_name'];

            $str = "select * from users where school_id='$school_id'";
            $query4 = $this->db->query($str);

            $stdata_by_gr_arr[] = $query4->result_array();
        }

        return $stdata_by_gr_arr;
    }

    function getrecentlyassignments() {
        $district_id = $this->session->userdata('district_id');
        //$sql="SELECT * FROM `assignments`  where `district_id`='".$district_id."'  order by id DESC limit 3";


        $sql = 'SELECT `assignment_name`,`id` FROM assignments WHERE EXISTS (SELECT * FROM user_quizzes
      WHERE user_quizzes.assignment_id =assignments.id)  and assignments.district_id="' . $district_id . '"  order by id desc limit 3 ';



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        } else {
            return false;
        }
    }

    function studentscoreByWrkid1($ssid, $assids) {

        $temp = array();
        for ($i = 0; $i < count($assids); $i++) {
            //echo $qry4 = "select * from users,user_quizzes where users.student_id='$ssid' and user_quizzes.user_id = users.UserID and user_quizzes.assignment_id='".$assids[$i]."'";

            $qry4 = " select * from users,user_quizzes where users.student_id='$ssid' and user_quizzes.user_id = users.UserID and user_quizzes.assignment_id='" . $assids[$i] . "' group by user_quizzes.id order by user_quizzes.id desc limit 1";

            $query4 = $this->db->query($qry4);
            $temp[] = $query4->result_array();
        }

        return $temp;
    }

    function create_behavior_records($login_id) {
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $district_id = $this->session->userdata('district_id');
        $created_by = $this->session->userdata('login_type');
        $behavior_location = $this->input->post('behavior_location');
        $incident = $this->input->post('incident');

        if ($this->session->userdata("school_id")) {
            $school_id = $this->session->userdata("school_id");
        } else {
            $school_id = $this->input->post('school_id');
        }
        $behavior_records = array('date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))),
            'time' => $time,
            'school_id' => $school_id,
            'district_id' => $district_id,
            'login_type' => $created_by,
            'description' => $incident,
            'behavior_location' => $behavior_location,
            'login_id' => $login_id
        );

        $result = $this->db->insert('behavior_records', $behavior_records);
        if ($result) {
            $behavior_record_id = $this->db->insert_id();
            if (count($this->input->post('victim_ids')) > 0) {
                foreach ($this->input->post('victim_ids') as $victim) {
                    $victim_id_array = explode('_', $victim);
                    $victim_arr = array('behavior_id' => $behavior_record_id, 'user_type' => $victim_id_array[0], 'user_id' => $victim_id_array[1]);
                    $this->db->insert('br_victims', $victim_arr);
                }
            }

            if (count($this->input->post('suspect_ids')) > 0) {
                foreach ($this->input->post('suspect_ids') as $suspect) {
                    $suspect_id_array = explode('_', $suspect);
                    $suspect_arr = array('behavior_id' => $behavior_record_id, 'user_type' => $suspect_id_array[0], 'user_id' => $suspect_id_array[1]);
                    $this->db->insert('br_suspects', $suspect_arr);
                }
            }

            if (count($this->input->post('problem_behaviour')) > 0) {
                foreach ($this->input->post('problem_behaviour') as $problem) {
                    $problem_arr = array('behavior_id' => $behavior_record_id, 'problem_id' => $problem);
                    $this->db->insert('br_problem', $problem_arr);
                }
            }

            if (count($this->input->post('possible_motivation')) > 0) {
                foreach ($this->input->post('possible_motivation') as $motivation) {
                    $motivation_arr = array('behavior_id' => $behavior_record_id, 'motivation_id' => $motivation);
                    $this->db->insert('br_motivation', $motivation_arr);
                }
            }
            if (count($this->input->post('interventions_prior')) > 0) {
                foreach ($this->input->post('interventions_prior') as $interventions) {
                    $interventions_arr = array('behavior_id' => $behavior_record_id, 'interventions_id' => $interventions);
                    $this->db->insert('br_interventions', $interventions_arr);
                }
            }

            if (count($this->input->post('student_has_multiple_referrals')) > 0) {
                foreach ($this->input->post('student_has_multiple_referrals') as $referrals) {
                    $referrals_arr = array('behavior_id' => $behavior_record_id, 'referral_id' => $referrals);
                    $this->db->insert('br_referrals', $referrals_arr);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function update_behavior($table, $field, $where) {
        $this->db->where('behavior_id', $where);
        return $this->db->update($table, $field);
    }

    function update_behavior_records($login_id) {
//    echo 'test';exit;
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $district_id = $this->session->userdata('district_id');
        $created_by = $this->session->userdata('login_type');
        $behavior_location = $this->input->post('behavior_location');
        $incident = $this->input->post('incident');
        $behavior_id = $this->input->post('behavior_id');

        if ($this->session->userdata("school_id")) {
            $school_id = $this->session->userdata("school_id");
        } else {
            $school_id = $this->input->post('school_id');
        }

        $behavior_records = array('date' => date('Y-m-d', strtotime($date)),
            'time' => $time,
            'school_id' => $school_id,
            'district_id' => $district_id,
            'login_type' => $created_by,
            'description' => $incident,
            'behavior_location' => $behavior_location,
            'login_id' => $login_id,
            'behavior_id' => $behavior_id
        );


//print_r($behavior_records);exit;
        $result = $this->update_behavior('behavior_records', $behavior_records, $behavior_records['behavior_id']);
        //print_r($result);exit;

        if ($result) {
            $behavior_record_id = $this->input->post('behavior_id');
            //print_r($behavior_record_id);exit;
            if (count($this->input->post('victim_ids')) > 0) {
                foreach ($this->input->post('victim_ids') as $victim) {
                    $victim_id_array = explode('_', $victim);
                    $victim_arr = array('behavior_id' => $behavior_record_id, 'user_type' => $victim_id_array[0], 'user_id' => $victim_id_array[1]);
                    $this->update_behavior('br_victims', $victim_arr, $victim_arr['behavior_id']);
                }
            }

            if (count($this->input->post('suspect_ids')) > 0) {
                foreach ($this->input->post('suspect_ids') as $suspect) {
                    $suspect_id_array = explode('_', $suspect);
                    $suspect_arr = array('behavior_id' => $behavior_record_id, 'user_type' => $suspect_id_array[0], 'user_id' => $suspect_id_array[1]);
                    $this->update_behavior('br_suspects', $suspect_arr, $suspect_arr['behavior_id']);
                }
            }

            if (count($this->input->post('problem_behaviour')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_problem');
                foreach ($this->input->post('problem_behaviour') as $problem) {
                    //print_r($problem);
                    $problem_arr = array('behavior_id' => $behavior_record_id, 'problem_id' => $problem);
                    $this->db->insert('br_problem', $problem_arr);
//			$this->update_behavior('br_problem',$problem_arr,$problem_arr['behavior_id']);
                }
            }

            if (count($this->input->post('possible_motivation')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_motivation');
                foreach ($this->input->post('possible_motivation') as $motivation) {
                    $motivation_arr = array('behavior_id' => $behavior_record_id, 'motivation_id' => $motivation);
                    $this->db->insert('br_motivation', $motivation_arr);
                }
            }
            if (count($this->input->post('interventions_prior')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_interventions');
                foreach ($this->input->post('interventions_prior') as $interventions) {
                    $interventions_arr = array('behavior_id' => $behavior_record_id, 'interventions_id' => $interventions);
                    $this->db->insert('br_interventions', $interventions_arr);
                }
            }

            if (count($this->input->post('student_has_multiple_referrals')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_referrals');
                foreach ($this->input->post('student_has_multiple_referrals') as $referrals) {
                    $referrals_arr = array('behavior_id' => $behavior_record_id, 'referral_id' => $referrals);
                    $this->db->insert('br_referrals', $referrals_arr);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    function delete_behavior_records($behavior_id) {
//    echo 'test';exit;
        $this->db->where('behavior_id', $behavior_id);
        $result = $this->db->delete('behavior_records');
//print_r($behavior_records);exit;
        //print_r($result);exit;

        if ($result) {
            $behavior_record_id = $this->input->post('behavior_id');
            //print_r($behavior_record_id);exit;
            if (count($this->input->post('victim_ids')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_victims');
            }

            if (count($this->input->post('suspect_ids')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_suspects');
            }

            if (count($this->input->post('problem_behaviour')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_problem');
            }

            if (count($this->input->post('possible_motivation')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_motivation');
            }

            if (count($this->input->post('interventions_prior')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_interventions');
            }


            if (count($this->input->post('student_has_multiple_referrals')) > 0) {
                $this->db->where('behavior_id', $behavior_record_id);
                $this->db->delete('br_referrals');
            }


            return true;
        } else {
            return false;
        }
    }

    public function get_list_by_incident() {
        $this->db->select('*');
        $this->db->from('br_problem');
        $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
        $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
        if ($this->input->post('incident') != 0) {
            $this->db->where('br_problem.problem_id', $this->input->post('incident'));
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $this->db->where('YEAR(behavior_records.date)', $this->input->post('year'));
        }

        if ($this->input->post('month') != 'all') {
            $this->db->where('MONTH(behavior_records.date)', $this->input->post('month'));
        }
        if ($this->input->post('school_id')) {
            $school_id = $this->input->post('school_id');
        } else {
            $school_id = $this->session->userdata('school_id');
        }
        $this->db->where('school_id', $school_id);
        $this->db->group_by('br_problem.behavior_id');
        $query = $this->db->get();
        $results = $query->result();


//            echo $this->db->last_query();
        if ($results) {
            // print_r($results);

            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();



                //print_r($behavior);exit;
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date . ' ' . $result->time;
                $returnarr[$result->behavior_id]['description'] = strip_tags($result->description);
//               echo $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_graph_by_incident() {
        $this->db->select('br_problem.*,behavior_records.*,dist_prob_behaviour.*,schools.school_name');
        $this->db->from('br_problem');
        $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
        $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
        $this->db->join('schools', 'schools.school_id = behavior_records.school_id');
        if ($this->input->post('incident') != 0) {
            $this->db->where('br_problem.problem_id', $this->input->post('incident'));
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $this->db->where('YEAR(behavior_records.date)', $this->input->post('year'));
        }

        if ($this->input->post('month') != 'all') {
            $this->db->where('MONTH(behavior_records.date)', $this->input->post('month'));
        }

        if ($this->input->post('school_id')) {
            $school_id = $this->input->post('school_id');
        } else {
            $school_id = $this->session->userdata('school_id');
        }
        if ($this->session->userdata('login_type') == 'user'){
            $this->db->where('behavior_records.district_id', $this->session->userdata('district_id'));
        } else {
            $this->db->where('behavior_records.school_id', $school_id);    
        }
        
        
        $this->db->group_by('br_problem.behavior_id');
        
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('br_problem.*,behavior_records.*,dist_prob_behaviour.*,schools.school_name');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->join('schools', 'schools.school_id = behavior_records.school_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }
            }
            return $returnarr;
        } else
            return false;
    }

    function get_incident_by_id($id) {
        $this->db->select('*');
        $this->db->from('br_problem');
        $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
        $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
        $this->db->where('behavior_records.behavior_id', $id);
        $this->db->group_by('br_problem.behavior_id');
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                $this->db->select('*');
                $this->db->from('br_interventions');
                $this->db->join('behavior_records', 'br_interventions.behavior_id = behavior_records.behavior_id');
                $this->db->join('interventions_prior', 'interventions_prior.id = br_interventions.interventions_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $interventionqry = $this->db->get();
                $intervention = $interventionqry->result();

                $this->db->select('*');
                $this->db->from('br_referrals');
                $this->db->join('behavior_records', 'br_referrals.behavior_id = behavior_records.behavior_id');
                $this->db->join('student_has_multiple_referrals', 'student_has_multiple_referrals.id = br_referrals.referral_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $referralqry = $this->db->get();
                $referral = $referralqry->result();

                $this->db->select('*');
                $this->db->from('br_motivation');
                $this->db->join('behavior_records', 'br_motivation.behavior_id = behavior_records.behavior_id');
                $this->db->join('possible_motivation', 'possible_motivation.id = br_motivation.motivation_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $motivationqry = $this->db->get();
                $motivation = $motivationqry->result();

                $this->db->select('*');
                $this->db->from('behavior_location');
                $this->db->where('behavior_location.id', $result->behavior_location);
                $locationqry = $this->db->get();
                $location = $locationqry->result();

                $returnarr['incident'] = $behavior;
                $returnarr['victim'] = $usersvictim[$result->behavior_id];
                $returnarr['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr['date'] = $result->date;
                $returnarr['description'] = $result->description;
                $returnarr['intervention'] = $intervention;
                $returnarr['referral'] = $referral;
                $returnarr['motivation'] = $motivation;
                $returnarr['location'] = $location[0]->behavior_location;
            }
            return $returnarr;
        } else
            return false;
    }

    public function get_list_by_student() {
        if ($this->input->post('student_id') != 0) {
            $wherevictim = "br_victims.user_id = " . $this->input->post('student_id') . " AND";
            $wheresuspect = "br_suspects.user_id = " . $this->input->post('student_id') . " AND";
        } else {
            $wherevictim = "";
            $wheresuspect = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }


        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id where  $wherevictim br_victims.user_type = 'student' $whereyear $wheremonth
 
UNION 

SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id where $wheresuspect br_suspects.user_type = 'student' $whereyear $wheremonth");
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                //print_r($behavior);exit;
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_list_by_location() {
        if ($this->input->post('behavior_location') != 0) {
            $behavior_location = $this->input->post('behavior_location');
            $where = "AND behavior_records.behavior_location = '$behavior_location'"; //$behavior_location  = $this->input->post('behavior_location');
        } else {
            $where = "";
        }

        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND behavior_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id WHERE 1 $where $whereschool $wheremonth $whereyear UNION SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id where 1 $where $whereschool $wheremonth $whereyear");
      //  echo $this->db->last_query();exit;
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                //print_r($behavior);exit;
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_list_by_time() {


        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $year = $this->input->post('year');
        //$date1 = $this->input->post('date');





        if ($start_time != '' && $end_time != '') {
            if ($year != '') {
                $where = "where behavior_records.time >= '$start_time' AND behavior_records.time <= '$end_time'";
            }
        }
        /* else {
          if($date!=''){
          $where = "where behavior_records.date = '$date'";
          } else {
          $where = "";
          }
          } */

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id $where UNION SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id $where");


        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                //print_r($behavior);exit;
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_list_by_time_report() {


        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $year = $this->input->post('year');





        if ($start_time != '' && $end_time != '') {
            if ($year != '') {
                $where = "where behavior_records.time >= '$start_time' AND behavior_records.time <= '$end_time'";
            }
        }
        /* else {
          if($this->input->post('year')!=0 || $this->input->post('year')!=''){
          $where = "AND YEAR(behavior_records.date) = ".$this->input->post('year');
          } else {
          $where = "";
          }
          } */

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id $where UNION SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id $where");

//echo $this->db->last_query();exit;
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                //print_r($behavior);exit;
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_list_by_behavior_staff() {
        if ($this->input->post('teacher_id') != 'all') {
            $teacher_id = $this->input->post('teacher_id');
            $whereteacher = "AND behavior_records.login_id = '$teacher_id'";
        } else {
            $whereteacher = "";
        }

        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND behavior_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id where 1 $whereteacher $whereschool $wheremonth $whereyear 
 
UNION 

SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id where 1 $whereteacher $whereschool $wheremonth $whereyear ");

        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                }
                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                }
                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
            }
            return $returnarr;
        } else
            return false;
    }

    public function get_graph_by_student() {



        if ($this->input->post('student_id') != 0) {
            $wherevictim = "br_victims.user_id = " . $this->input->post('student_id') . " AND";
            $wheresuspect = "br_suspects.user_id = " . $this->input->post('student_id') . " AND";
        } else {
            $wherevictim = "";
            $wheresuspect = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }

        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND behavior_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }


        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id where  $wherevictim br_victims.user_type = 'student' $whereyear $wheremonth $whereschool
 
UNION 

SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id where $wheresuspect br_suspects.user_type = 'student' $whereyear $wheremonth $whereschool");
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
//                $this->db->select('*');
//                $this->db->from('br_victims');
//                $this->db->where('behavior_id',$result->behavior_id);
//                $victimqry = $this->db->get();
//                $victims = $victimqry->result();
//                foreach ($victims as $victim){
//                    $this->db->select('lastname,firstname');
//                    $this->db->from($victim->user_type.'s');
//                    $this->db->where($victim->user_type.'_id',$victim->user_id);
//                    $usersqry = $this->db->get();
//                    $usersvictim[$result->behavior_id][] = $usersqry->result();
//                }
//                
//                $this->db->select('*');
//                $this->db->from('br_suspects');
//                $this->db->where('behavior_id',$result->behavior_id);
//                $suspectqry = $this->db->get();
//                $suspects = $suspectqry->result();
//                foreach ($suspects as $suspect){
//                    $this->db->select('lastname,firstname');
//                    $this->db->from($suspect->user_type.'s');
//                    $this->db->where($suspect->user_type.'_id',$suspect->user_id);
//                    $usersqry = $this->db->get();
//                    $userssuspect[$result->behavior_id][] = $usersqry->result();
//                }



                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }
                //print_r($behavior);exit;
//               $returnarr[$result->behavior_id]['incident'] = $behavior;
//               $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
//               $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
//               $returnarr[$result->behavior_id]['date'] =  $result->date;
//               $returnarr[$result->behavior_id]['description'] =  $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_list_by_behavior1() {
        $this->db->select('*');
        $this->db->from('br_problem');
        $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
        $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');

        if ($this->input->post('school_id')) {
            $school_id = $this->input->post('school_id');
        } else {
            $school_id = $this->session->userdata('school_id');
        }
        $this->db->where('school_id', $school_id);
        $this->db->group_by('br_problem.behavior_id');
        $query = $this->db->get();
        $results = $query->result();


//            echo $this->db->last_query();
        if ($results) {
            // print_r($results);

            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                    //echo $this->db->last_query();
//                    echo '<br>';
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();



                //print_r($behavior);exit;
                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date . ' ' . $result->time;
                $returnarr[$result->behavior_id]['description'] = strip_tags($result->description);
//               echo $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    function get_list_by_behavior() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('behavior_records');
        $this->db->where('login_id', $user_id);
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_victims');
                $this->db->where('behavior_id', $result->behavior_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->behavior_id][] = $usersqry->result();
                }
                $this->db->select('*');
                $this->db->from('br_suspects');
                $this->db->where('behavior_id', $result->behavior_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->behavior_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                $this->db->select('*');
                $this->db->from('br_interventions');
                $this->db->join('behavior_records', 'br_interventions.behavior_id = behavior_records.behavior_id');
                $this->db->join('interventions_prior', 'interventions_prior.id = br_interventions.interventions_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $interventionqry = $this->db->get();
                $intervention = $interventionqry->result();

                $this->db->select('*');
                $this->db->from('br_referrals');
                $this->db->join('behavior_records', 'br_referrals.behavior_id = behavior_records.behavior_id');
                $this->db->join('student_has_multiple_referrals', 'student_has_multiple_referrals.id = br_referrals.referral_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $referralqry = $this->db->get();
                $referral = $referralqry->result();

                $this->db->select('*');
                $this->db->from('br_motivation');
                $this->db->join('behavior_records', 'br_motivation.behavior_id = behavior_records.behavior_id');
                $this->db->join('possible_motivation', 'possible_motivation.id = br_motivation.motivation_id');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                $motivationqry = $this->db->get();
                $motivation = $motivationqry->result();

                $this->db->select('*');
                $this->db->from('behavior_location');
                $this->db->where('behavior_location.id', $result->behavior_location);
                $locationqry = $this->db->get();
                $location = $locationqry->result();

                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
                $returnarr[$result->behavior_id]['intervention'] = $intervention;
                $returnarr[$result->behavior_id]['referral'] = $referral;
                $returnarr[$result->behavior_id]['motivation'] = $motivation;
                $returnarr[$result->behavior_id]['location'] = $result->behavior_location;
            }

            return $returnarr;
        } else
            return false;
    }



    public function get_graph_by_location() {
        if ($this->input->post('behavior_location') != 0) {
            $where = "where behavior_records.behavior_location = '$behavior_location'"; //$behavior_location  = $this->input->post('behavior_location');
        } else {
            $where = "";
        }



        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND behavior_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }

//            $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_victims ON br_victims.behavior_id = behavior_records.behavior_id $where UNION SELECT behavior_records.behavior_id,behavior_records.date,behavior_records.description FROM `behavior_records` JOIN br_suspects ON br_suspects.behavior_id = behavior_records.behavior_id $where $whereschool $wheremonth $whereyear");

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_location.behavior_location,behavior_location.id FROM `behavior_records` JOIN behavior_location on behavior_records.behavior_location = behavior_location.id");

        $results = $query->result();
//            print_r($results);exit;
        if ($results) {
            foreach ($results as $result) {
//                $this->db->select('*');
//                $this->db->from('br_victims');
//                $this->db->where('behavior_id',$result->behavior_id);
//                $victimqry = $this->db->get();
//                $victims = $victimqry->result();
//                foreach ($victims as $victim){
//                    $this->db->select('lastname,firstname');
//                    $this->db->from($victim->user_type.'s');
//                    $this->db->where($victim->user_type.'_id',$victim->user_id);
//                    $usersqry = $this->db->get();
//                    $usersvictim[$result->behavior_id][] = $usersqry->result();
//                    //echo $this->db->last_query();
////                    echo '<br>';
//                }
//                
//                $this->db->select('*');
//                $this->db->from('br_suspects');
//                $this->db->where('behavior_id',$result->behavior_id);
//                $suspectqry = $this->db->get();
//                $suspects = $suspectqry->result();
//                foreach ($suspects as $suspect){
//                    $this->db->select('lastname,firstname');
//                    $this->db->from($suspect->user_type.'s');
//                    $this->db->where($suspect->user_type.'_id',$suspect->user_id);
//                    $usersqry = $this->db->get();
//                    $userssuspect[$result->behavior_id][] = $usersqry->result();
//                    //echo $this->db->last_query();
////                    echo '<br>';
//                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->join('behavior_location', 'behavior_records.behavior_location = behavior_location.id', 'left');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);

                $behaviorqry = $this->db->get();
//                echo $this->db->last_query();exit;
                $behavior = $behaviorqry->result();

                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }

                //print_r($behavior);exit;
//               $returnarr[$result->behavior_id]['incident'] = $behavior;
//               $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->behavior_id];
//               $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->behavior_id];
//               $returnarr[$result->behavior_id]['date'] =  $result->date;
//               $returnarr[$result->behavior_id]['description'] =  $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    public function get_graph_by_staff() {
        if ($this->input->post('teacher_id') != 'all') {
            $teacher_id = $this->input->post('teacher_id');
            $whereteacher = "AND behavior_records.login_id = '$teacher_id'";
        } else {
            $whereteacher = "";
        }

        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND behavior_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,teachers.firstname,teachers.lastname,teachers.teacher_id,teachers.username FROM `behavior_records` JOIN teachers ON teachers.teacher_id = behavior_records.login_id where login_type='teacher' $whereteacher $whereschool $wheremonth $whereyear ");

        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->join('teachers', 'behavior_records.login_id = teachers.teacher_id', 'left');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);

                $behaviorqry = $this->db->get();
//                echo $this->db->last_query();exit;
                $behavior = $behaviorqry->result();

                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }
            }
            return $returnarr;
        } else
            return false;
    }

    public function get_graph_by_location1() {

        $behavior_location = $this->input->post('behavior_location');
        if ($this->input->post('behavior_location') != 0) {


            $where = "AND behavior_records.behavior_location = '$behavior_location'";
        } else {

            $where = "";
        }
        //print_r($this->session->all_userdata());exit;
        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND behavior_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }

        /*if ($this->session->userdata('district_id')) {
            $district_id = $this->session->userdata('district_id');
            $whereschool = "AND behavior_records.district_id = $district_id";
        } */

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(behavior_records.date) = " . $this->input->post('year');
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(behavior_records.date) = " . $this->input->post('month');
        } else {
            $wheremonth = "";
        }

        $query = $this->db->query("SELECT behavior_records.behavior_id,behavior_records.date,behavior_location.behavior_location,behavior_location.id FROM `behavior_records` JOIN behavior_location on behavior_records.behavior_location = behavior_location.id where 1 $where $whereschool $wheremonth $whereyear");
      //  echo $this->db->last_query();exit;
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('behavior_records', 'br_problem.behavior_id = behavior_records.behavior_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_behaviour.id = br_problem.problem_id');
                $this->db->join('behavior_location', 'behavior_records.behavior_location = behavior_location.id', 'left');
                $this->db->where('behavior_records.behavior_id', $result->behavior_id);
                if ($behavior_location != 'all' && $behavior_location != 0)
                    $this->db->where('behavior_location.id', $behavior_location);

                $behaviorqry = $this->db->get();
//                echo $this->db->last_query();exit;
                $behavior = $behaviorqry->result();

                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }
            }
            return $returnarr;
        } else
            return false;
    }

    function student_progress_report($login_id) {
//    echo 'test';exit;
        $date = $this->input->post('date');
        $school_id = $this->session->userdata('school_id');
        $district_id = $this->session->userdata('district_id');
        $created_by = $this->session->userdata('login_type');
        $grade_id = $this->input->post('grade_id');
        $period_id = $this->input->post('period_id');
        $subject_id = $this->input->post('subject_id');
        $student_id = $this->input->post('student_id');


        $progress_record = array('date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))),
            'school_id' => $school_id,
            'district_id' => $district_id,
            'user_type' => $created_by,
            'user_id' => $login_id,
            'grade_id' => $grade_id,
            'period_id' => $period_id,
            'subject_id' => $subject_id,
            'student_id' => $student_id,
        );

        $result = $this->db->insert('student_progress_report', $progress_record);

        if ($result) {
            $progress_record_id = $this->db->insert_id();
            if ($this->input->post('subjects_id') == 0) {

                $all_subjects_id = $this->input->post('all_subjects_id');
                //print_r($all_subjects_id);exit;
                //$subjects_array = implode(",",$all_subjects_id);
                $subject_score_q1 = array();
                $subject_score_q2 = array();
                $subject_score_q3 = array();
                $subject_score_q4 = array();

                foreach ($all_subjects_id as $subject_id) {

                    $subject_score_q1[$subject_id][0] = $this->input->post('score_q1_' . $subject_id);
                    $subject_score_q2[$subject_id][0] = $this->input->post('score_q2_' . $subject_id);
                    $subject_score_q3[$subject_id][0] = $this->input->post('score_q3_' . $subject_id);
                    $subject_score_q4[$subject_id][0] = $this->input->post('score_q4_' . $subject_id);
                }

                foreach ($subject_score_q1 as $key => $value) {
                    $score_q1 = array('subject_id' => $key, 'score_q1' => $value[0], 'score_q2' => $subject_score_q2[$key][0], 'score_q3' => $subject_score_q3[$key][0], 'score_q4' => $subject_score_q4[$key][0], 'progress_report_id' => $progress_record_id);


                    $this->db->insert('progress_report_score', $score_q1);
                }

                $standard_name_id = $this->input->post('standard_name_id');
                $language_q1 = array();
                $language_q2 = array();
                $language_q3 = array();
                $language_q4 = array();

                foreach ($standard_name_id as $standard_name) {

                    $language_q1[$standard_name][0] = $this->input->post('language_q1_' . $standard_name);
                    $language_q2[$standard_name][0] = $this->input->post('language_q2_' . $standard_name);
                    $language_q3[$standard_name][0] = $this->input->post('language_q3_' . $standard_name);
                    $language_q4[$standard_name][0] = $this->input->post('language_q4_' . $standard_name);
                }

                foreach ($language_q1 as $key => $value_data) {
                    $language = array('standard_name_id' => $key, 'language_q1' => $value_data[0], 'language_q2' => $language_q2[$key][0], 'language_q3' => $language_q3[$key][0], 'language_q4' => $language_q4[$key][0], 'progress_report_id' => $progress_record_id);

                    //print_r($language);
                    $this->db->insert('prog_standard_name', $language);
                }
            }//exit;

            return true;
        } else {
            return false;
        }
    }
    
    public function sst_report_pdf($teacher_id, $student_id, $date, $id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }


        $this->db->select('*');
        $this->db->from('student_success_strengths');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id');
        if ($this->session->userdata('login_type') == 'teacher') {
            //$this->db->join('teachers','student_success_strengths.user_id = teachers.teacher_id','LEFT');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $this->db->join('observers', 'student_success_strengths.user_id = observers.observer_id', 'LEFT');
        }

        $this->db->where('id', $id);
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
//            print_r($results);exit;
            
            foreach ($results as $key => $result) {
                if($result->needs_home_child==''){
                $this->db->select('*');
                $this->db->from('intervention_strategies_home');
                $this->db->where('id',  unserialize($result->needs_home));
                $qryneedhome = $this->db->get();
                $arrneedhome = $qryneedhome->result();
                } else {
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_home');
                    $this->db->where_in('id',  explode('|',$result->needs_home_child));
                    $qryneedhome = $this->db->get();
                    $arrneedhome = $qryneedhome->result();
                    
                }

                $need_home_uns = unserialize($result->needs_home);
                $need_home_arrs = explode('|',$need_home_uns);
                foreach($need_home_arrs as $need_home_arr){
                    if(!is_numeric($need_home_arr)){
                      $arrneedhome[] = (object)array('district_id'=>248,'intervention_strategies_home'=>$need_home_arr) ; 
                    }
                }
//                print_r($arrneedhome);exit;
//                print_r($arrneedhome);exit;
                if($result->needs_school_child==''){
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_school');
                    $this->db->where('id',  unserialize($result->needs_school));
                    $qryneedschool = $this->db->get();
                    $arrneedschool = $qryneedschool->result();
                } else {
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_school');
                    $this->db->where_in('id',  explode('|',$result->needs_school_child));
                    $qryneedschool = $this->db->get();
                    $arrneedschool = $qryneedschool->result();
                }

                $need_school_uns = unserialize($result->needs_school);
                $need_school_arrs = explode('|',$need_school_uns);
                foreach($need_school_arrs as $need_school_arr){
                    if(!is_numeric($need_school_arr)){
                      $arrneedschool[] = (object)array('district_id'=>248,'intervention_strategies_school'=>$need_school_arr) ; 
                    }
                }
                
                if($result->needs_medical_child==''){
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_medical');
                    $this->db->where('id',  unserialize($result->needs_medical));
                    $qryneedmedical = $this->db->get();
                    $arrneedmedical = $qryneedmedical->result();
                } else {
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_medical');
                    $this->db->where_in('id',  explode('|',$result->needs_medical_child));
                    $qryneedmedical = $this->db->get();
                    $arrneedmedical = $qryneedmedical->result();
                }

                $need_medical_uns = unserialize($result->needs_medical);
                $need_medical_arrs = explode('|',$need_medical_uns);
                //print_r($need_medical_arrs);exit;
                foreach($need_medical_arrs as $need_medical_arr){
                    if(!is_numeric($need_medical_arr)){
                      $arrneedmedical[] = (object)array('district_id'=>248,'intervention_strategies_medical'=>$need_medical_arr) ; 
                    }
                }
                //print_r($arrultneedmedical);exit;
//                echo unserialize($result->action_home);exit;
                    
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_home');
                    $this->db->where_in('id',  explode('|',$result->action_home_need));
                    $qryhomeneed = $this->db->get();
                    $arrulthomeneed = $qryhomeneed->result();
                
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_school');
                    $this->db->where_in('id',  explode('|',$result->action_school_need));
                    $qryschoolneed = $this->db->get();
                    $arrultschoolneed = $qryschoolneed->result();
                
                    $this->db->select('*');
                    $this->db->from('intervention_strategies_medical');
                    $this->db->where_in('id',  explode('|',$result->action_medical_need));
                    $qryneedmedical = $this->db->get();
                    $arrultneedmedical = $qryneedmedical->result();
                
                    

                    $arractionhomeuns = unserialize($result->action_home);
                    $arractionhomearr = explode('|',$arractionhomeuns);
                    
                    $arractionschooluns = unserialize($result->action_school);
                    $arractionschoolarr = explode('|',$arractionschooluns);
                    
                    $arractionmedicaluns = unserialize($result->action_medical);
                    $arractionmedicalarr = explode('|',$arractionmedicaluns);
                    
                    $responsible_home_action = explode('|',  unserialize($result->action_home_responsible));
                    $responsible_school_action = explode('|',  unserialize($result->action_school_responsible));
                    $responsible_medical_action = explode('|',  unserialize($result->action_medical_responsible));

                    $date_home_action = explode('|',  unserialize($result->action_home_date));
                    $date_school_action = explode('|',  unserialize($result->action_school_date));
                    $date_medical_action = explode('|',  unserialize($result->action_medical_date));
                    
                    $total_home_need = explode('|',$result->action_home_need);
                    $total_school_need = explode('|',$result->action_school_need);
                    $total_medical_need = explode('|',$result->action_medical_need);

                    //print_r($total_home_need);exit;
                    foreach($total_home_need as $arrkey=>$arrval){
                        $this->db->select('*');
                        $this->db->from('intervention_strategies_home');
                        $this->db->where('id',  $arrval);
                        $qryhomeneed = $this->db->get();
                        $arrulthomeneed = $qryhomeneed->result();
                    
                    
                    if(!is_numeric($arrval)){
                        $action_all_home[$arrkey]['action_need_home']= $arrval;
                        $action_all_home[$arrkey]['action_action_home']= $arractionhomearr[$arrkey];
                    } else {
                        $this->db->select('*');
                    $this->db->from('actions_home');
                    $this->db->where('action_home_id',$arractionhomearr[$arrkey]);
                    $qryactionhome = $this->db->get();
                    $arractionhome = $qryactionhome->result();

                        $action_all_home[$arrkey]['action_need_home']= $arrulthomeneed[0]->intervention_strategies_home;
                        $action_all_home[$arrkey]['action_action_home']= $arractionhome[0]->actions_home;
                    }
                    $action_all_home[$arrkey]['action_responsible_home']= $responsible_home_action[$arrkey];
                    $action_all_home[$arrkey]['action_date_home']= $date_home_action[$arrkey];
                    }
                   // print_r($action_all_home);exit;
                  //  print_r($arractionschool);exit;
                    foreach($total_school_need as $arrschkey=>$arrschval){
                        $this->db->select('*');
                        $this->db->from('intervention_strategies_school');
                        $this->db->where('id',  $arrschval);
                        $qryschoolneed = $this->db->get();
                        $arrultschoolneed = $qryschoolneed->result();

                    if(!is_numeric($arrschval)){    
                        $action_all_school[$arrschkey]['action_need_school']= $arrschval;
                        $action_all_school[$arrschkey]['action_action_school']= $arractionschoolarr[$arrschkey];
                    } else {
                        $this->db->select('*');
                    $this->db->from('actions_school');
                    $this->db->where('action_school_id',$arractionschoolarr[$arrschkey]);
                    $qryactionschool = $this->db->get();
                    $arractionschool = $qryactionschool->result();
                        $action_all_school[$arrschkey]['action_need_school']= $arrultschoolneed[0]->intervention_strategies_school;
                        $action_all_school[$arrschkey]['action_action_school']= $arractionschool[0]->actions_school;
                    }
                    $action_all_school[$arrschkey]['action_responsible_school']= $responsible_school_action[$arrschkey];
                    $action_all_school[$arrschkey]['action_date_school']= $date_school_action[$arrschkey];
                    }
                    foreach($total_medical_need as $arrmedkey=>$arrmedval){
                        $this->db->select('*');
                        $this->db->from('intervention_strategies_medical');
                        $this->db->where('id',  $arrmedval);
                        $qrymedicalneed = $this->db->get();
                        $arrultmedicalneed = $qrymedicalneed->result();
                        if(!is_numeric($arrmedval)){ 
                            $action_all_medical[$arrmedkey]['action_need_medical']= $arrmedval;
                            $action_all_medical[$arrmedkey]['action_action_medical']= $arractionmedicalarr[$arrmedkey];
                        } else {
                            $this->db->select('*');
                    $this->db->from('actions_medical');
                    $this->db->where('action_medical_id',$arractionmedicalarr[$arrmedkey]);
                    $qryactionmedical = $this->db->get();
                    $arractionmedical = $qryactionmedical->result();
                            $action_all_medical[$arrmedkey]['action_need_medical']= $arrultmedicalneed[0]->intervention_strategies_medical;
                            $action_all_medical[$arrmedkey]['action_action_medical']= $arractionmedical[0]->actions_medical;
                        }
                        $action_all_medical[$arrmedkey]['action_responsible_medical']= $responsible_medical_action[$arrmedkey];
                        $action_all_medical[$arrmedkey]['action_date_medical']= $date_medical_action[$arrmedkey];
                    }

                $arractionhome = '';
                $arractionschool = '';
                $arractionmedical = '';

                $this->db->select('*');
                $this->db->from('sst_progress_monitoring');
                $this->db->where('id',$result->progress_monitoring);
                $qryprogress_monitoring = $this->db->get();
                $progress_monitoring = $qryprogress_monitoring->result();
                
                $this->db->select('*');
                $this->db->from('sst_referral_reason');
                $this->db->where('id',$result->referral_reason);
                $qryreferral_reason = $this->db->get();
                $referral_reason = $qryreferral_reason->result();

               /* if ($this->session->userdata('login_type') != 'teacher'){
                    $this->db->select('*');
                    $this->db->from('schools');
                    $this->db->where('school_id',$this->session->userdata('school_id'));
                    $qryschool = $this->db->get();
                    $school = $qryschool->result_array();
                    $school_detail = $school[0]['school_name'];
                }
                */
                $returnarr['details'] = $result;
                $returnarr['need_home'] = $arrneedhome;
                $returnarr['need_school'] = $arrneedschool;
                $returnarr['need_medical'] = $arrneedmedical;
                $returnarr['action_home'] = $action_all_home;
                $returnarr['action_school'] = $action_all_school;
                $returnarr['action_medical'] = $action_all_medical;
                $returnarr['meeting_type'] = $progress_monitoring;
                $returnarr['program_change'] = $result->program_change;
                $returnarr['referral_reason'] = $referral_reason;
               // $returnarr['school_details'] = $school_detail;

               // print_r($returnarr);exit;
            }
            return $returnarr;
        } else {
            return false;
        }
    }

    /*public function sst_report_pdf($teacher_id, $student_id, $date, $id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }


        $this->db->select('*');
        $this->db->from('student_success_strengths');
        $this->db->join('schools', 'student_success_strengths.school_id = schools.school_id');
        $this->db->join('students', 'student_success_strengths.student_id = students.student_id');
        $this->db->join('dist_grades', 'student_success_strengths.grade_id = dist_grades.dist_grade_id');
        if ($this->session->userdata('login_type') == 'teacher') {
            //$this->db->join('teachers','student_success_strengths.user_id = teachers.teacher_id','LEFT');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $this->db->join('observers', 'student_success_strengths.user_id = observers.observer_id', 'LEFT');
        }

        $this->db->where('id', $id);
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $key => $result) {

                $this->db->select('sst_needs_home.sst_id,sst_needs_home.need_id,intervention_strategies_home.intervention_strategies_home');
                $this->db->from('sst_needs_home');
                $this->db->join('intervention_strategies_home', 'intervention_strategies_home.id = sst_needs_home.need_id');
                if ($this->input->post('home_strat') != 0) {
                    $this->db->where('need_id', $this->input->post('home_strat'));
                }
                $this->db->where('sst_id', $result->id);

                $qryneedhome = $this->db->get();
                $arrneedhome = $qryneedhome->result();


                $this->db->select('sst_needs_school.sst_id,sst_needs_school.need_id,intervention_strategies_school.intervention_strategies_school');
                $this->db->from('sst_needs_school');
                $this->db->join('intervention_strategies_school', 'intervention_strategies_school.id = sst_needs_school.need_id');
                if ($this->input->post('school_strat') != 0) {
                    $this->db->where('need_id', $this->input->post('school_strat'));
                }
                $this->db->where('sst_id', $result->id);
                $qryneedschool = $this->db->get();
                $arrneedschool = $qryneedschool->result();



                $this->db->select('sst_needs_medical.sst_id,sst_needs_medical.need_id,intervention_strategies_medical.intervention_strategies_medical');
                $this->db->from('sst_needs_medical');
                $this->db->join('intervention_strategies_medical', 'intervention_strategies_medical.id = sst_needs_medical.need_id');
                if ($this->input->post('medical_strat') != 0) {
                    $this->db->where('need_id', $this->input->post('medical_strat'));
                }
                $this->db->where('sst_id', $result->id);
                $qryneedmedical = $this->db->get();
                $arrultneedmedical = $qryneedmedical->result();

                
                
                $this->db->select('sst_actions_home.responsible_person,sst_actions_home.action_id,actions_home.actions_home,intervention_strategies_home.intervention_strategies_home');
                $this->db->from('sst_actions_home');
                $this->db->join('actions_home', 'actions_home.action_home_id = sst_actions_home.action_id');
                $this->db->join('intervention_strategies_home', 'intervention_strategies_home.id = sst_actions_home.need_id');
                $this->db->where('sst_id', $result->id);
                $qryactionhome = $this->db->get();
                echo $this->db->last_query();exit;
                $arractionhome = $qryactionhome->result();


                $this->db->select('sst_actions_school.responsible_person,sst_actions_school.action_id,actions_school.actions_school,intervention_strategies_school.intervention_strategies_school');
                $this->db->from('sst_actions_school');
                $this->db->join('actions_school', 'actions_school.action_school_id = sst_actions_school.action_id');
                $this->db->join('intervention_strategies_school', 'intervention_strategies_school.id = sst_actions_school.need_id');
                $this->db->where('sst_id', $result->id);
                $qryschoolhome = $this->db->get();
                $arractionschool = $qryschoolhome->result();


                $this->db->select('sst_actions_medical.responsible_person,sst_actions_medical.action_id,actions_medical.actions_medical,intervention_strategies_medical.intervention_strategies_medical');
                $this->db->from('sst_actions_medical');
                $this->db->join('actions_medical', 'actions_medical.action_medical_id = sst_actions_medical.action_id');
                $this->db->join('intervention_strategies_medical', 'intervention_strategies_medical.id = sst_actions_medical.need_id');
                $this->db->where('sst_id', $result->id);
                $qryactionmedical = $this->db->get();
                $arractionmedical = $qryactionmedical->result();



                $returnarr['details'] = $result;
                $returnarr['need_home'] = $arrneedhome;
                $returnarr['need_school'] = $arrneedschool;
                $returnarr['need_medical'] = $arrultneedmedical;
                $returnarr['action_home'] = $arractionhome;
                $returnarr['action_school'] = $arractionschool;
                $returnarr['action_medical'] = $arractionmedical;
            }
            return $returnarr;
        } else {
            return false;
        }
    }*/

    public function GetStudentBySchoolId($school_id) {
        $sql = "SELECT * FROM `students` where `school_id` = " . $school_id;
        $query = $this->db->query($sql);
        $students = $query->result_array();


        return $students;
    }

    function getgradeBystudents_data($grade_id) {

        if ($this->session->userdata("school_id")) {
            $school_id = $this->session->userdata("school_id");
        } else {
            $school_id = $this->input->post('school_id');
        }

        $this->db->select('*');
        $this->db->from('students');
        $this->db->where('students.grade', $grade_id);
        $this->db->where('students.school_id', $school_id);

        $query = $this->db->get();
//	print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function update_student_grade($table, $field, $where) {
        // $where1 = ( id = $where );	
        //		print_r($where);
        //	exit;

        $qryfetch = $this->db->get_where('students', array('student_id' => $where));
        $resultfetch = $qryfetch->result();
//                print_r($resultfetch);exit;
        $this->db->where('UserID', $resultfetch[0]->UserID);
        $this->db->update('users', array('grade_id' => $field));

        $this->db->where('student_id', $resultfetch[0]->student_id);
        $this->db->delete('teacher_students');
//                echo $this->db->last_query();exit;

        $this->db->where('student_id', $where);
        return $this->db->update($table, array('grade' => $field));
    }

    function get_home_By_child($needs_home) {
        $this->db->select('*');
        $this->db->from('intervention_strategies_home');
        $this->db->where('intervention_strategies_home.parent_id', $needs_home);
        $this->db->where('is_delete', '0');
        //$this->db->group_by('students.student_id');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_home_By_child_data() {
        $this->db->select('*,ish1.intervention_strategies_home as need_home');
        $this->db->from('intervention_strategies_home ish1');
        $this->db->join('intervention_strategies_home ish2', 'ish2.parent_id = ish1.id', 'INNER');
        $this->db->where('ish1.district_id', $this->session->userdata('district_id'));
        $this->db->where('ish2.is_delete', '0');

        $query = $this->db->get();
//print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function get_school_By_child($needs_school) {
        $this->db->select('*');
        $this->db->from('intervention_strategies_school');
        $this->db->where('intervention_strategies_school.parent_id', $needs_school);
        $this->db->where('is_delete', '0');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_medical_By_child($needs_medical) {
        $this->db->select('*');
        $this->db->from('intervention_strategies_medical');
        $this->db->where('intervention_strategies_medical.parent_id', $needs_medical);
        $this->db->where('is_delete', '0');
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function get_event_titel() {

        $this->db->select('*');
        $this->db->from('create_event');
        $this->db->where('create_event.district_id', $this->session->userdata('district_id'));
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_event_data($date, $school_id) {

        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            if ($this->input->post('teacher_id'))
                $user_id = $this->input->post('teacher_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            if ($this->input->post('teacher_id'))
                $user_id = $this->input->post('teacher_id');
        }

        $this->db->select('*');
        $this->db->from('create_event');
        $this->db->join('dist_users', 'create_event.user_id = dist_users.dist_user_id', 'LEFT');
        $this->db->join('districts', 'create_event.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'create_event.school_id = schools.school_id', 'LEFT');
        $this->db->where('create_event.date', $date);
        $this->db->where('create_event.school_id', $school_id);
        $this->db->where('create_event.user_id', $user_id);

        $query = $this->db->get();

//	print_r($this->db->last_query());exit;

        $result = $query->result();
//                print_r($result);exit;
        return $result;
    }

    public function event_pdf($event_id) {

        $this->db->select('*');
        $this->db->from('create_event');
        $this->db->join('dist_users', 'create_event.user_id = dist_users.dist_user_id', 'LEFT');
        $this->db->join('districts', 'create_event.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'create_event.school_id = schools.school_id', 'LEFT');
        $this->db->where('create_event.event_id', $event_id);
        $query = $this->db->get();
//	print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function event_list() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('create_event');
        $this->db->join('dist_users', 'create_event.user_id = dist_users.dist_user_id', 'LEFT');
        $this->db->join('districts', 'create_event.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'create_event.school_id = schools.school_id', 'LEFT');
        $this->db->where('create_event.user_id', $user_id);
        $this->db->or_where('create_event.teacher_id', $user_id);
        

        $query = $this->db->get();

      //  print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function edit_date_event($table, $field, $where) {
        // $where1 = ( id = $where );	
        //		print_r($where);
        //	exit;
        $this->db->where('event_id', $where);
        return $this->db->update($table, $field);
    }

    public function get_event_by_teacher($teacher_id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id[] = $this->session->userdata('observer_id');
            $user_id[] = $teacher_id;
            
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('create_event');
        $this->db->where('create_event.teacher_id', $teacher_id);
        $this->db->where('create_event.district_id', $this->session->userdata('district_id'));
        $this->db->where_in('create_event.user_id', $user_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_lesson_Plan_library() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('lesson_plan_library_data');
        $this->db->join('dist_users', 'lesson_plan_library_data.user_id = dist_users.dist_user_id');
        $this->db->join('dist_grades', 'lesson_plan_library_data.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('lesson_plan_library_data.user_id', $user_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_lesson_Plan_library_by_id($lesson_Plan_library_id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('lesson_plan_library_data');
        $this->db->join('dist_users', 'lesson_plan_library_data.user_id = dist_users.dist_user_id');
        $this->db->join('dist_grades', 'lesson_plan_library_data.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('lesson_plan_library_data.lesson_Plan_library_id', $lesson_Plan_library_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }
    
    function get_lesson_Plan_library_by($lesson_Plan_library_id) {

        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }


        $qry = "Select w.standarddata_id,w.lesson_title,w.grade_id,w.strand,w.standard_id,w.standarddata,w.diff_instruction,w.standarddisplay,wd.lesson_plan_id,wd.lesson_plan_data from lesson_plans l,lesson_plan_library_data w,lesson_plan_library_sub wd where w.user_id=$user_id and wd.lesson_Plan_library_id=w.lesson_Plan_library_id and wd.lesson_plan_id=l.lesson_plan_id and  w.lesson_Plan_library_id=$lesson_Plan_library_id ";
        $query = $this->db->query($qry);

        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function get_lesson_Plan_library_by_grade_id($grade_id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('lesson_plan_library_data.lesson_plan_library_id,lesson_plan_library_data.standarddata_id,lesson_plan_library_data.lesson_title,lesson_plan_library_data.grade_id,lesson_plan_library_data.strand,lesson_plan_library_data.standard_id,lesson_plan_library_data.standarddata,lesson_plan_library_data.diff_instruction,lesson_plan_library_data.standarddisplay,dist_users.username,dist_grades.grade_name');
        $this->db->from('lesson_plan_library_data');
        $this->db->join('dist_users', 'lesson_plan_library_data.user_id = dist_users.dist_user_id');
        $this->db->join('dist_grades', 'lesson_plan_library_data.grade_id = dist_grades.dist_grade_id', 'LEFT');
//	$this->db->join('lesson_plan_library_sub','lesson_plan_library_data.lesson_plan_library_id  = lesson_plan_library_sub.lesson_plan_library_id','LEFT');
        //$this->db->join('lesson_plans','lesson_plan_library_sub.lesson_plan_id  = lesson_plans.lesson_plan_id','LEFT');
        $this->db->where('lesson_plan_library_data.grade_id', $grade_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function get_lesson_Plan_library_by_grade_pdf($grade_id, $lesson_plan_library_id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('lesson_plan_library_data.lesson_plan_library_id,lesson_plan_library_data.standarddata_id,lesson_plan_library_data.lesson_title,lesson_plan_library_data.grade_id,lesson_plan_library_data.strand,lesson_plan_library_data.standard_id,lesson_plan_library_data.standarddata,lesson_plan_library_data.diff_instruction,lesson_plan_library_data.standarddisplay,dist_users.username,dist_grades.grade_name,districts.districts_name');
        $this->db->from('lesson_plan_library_data');
        $this->db->join('dist_users', 'lesson_plan_library_data.user_id = dist_users.dist_user_id');
        $this->db->join('dist_grades', 'lesson_plan_library_data.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('districts', 'lesson_plan_library_data.district_id = districts.district_id', 'LEFT');
//	$this->db->join('lesson_plan_library_sub','lesson_plan_library_data.lesson_plan_library_id  = lesson_plan_library_sub.lesson_plan_library_id','LEFT');
        //$this->db->join('lesson_plans','lesson_plan_library_sub.lesson_plan_id  = lesson_plans.lesson_plan_id','LEFT');
        $this->db->where('lesson_plan_library_data.grade_id', $grade_id);
        $this->db->where('lesson_plan_library_data.lesson_plan_library_id', $lesson_plan_library_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function get_lesson_plan_library_sub($lesson_plan_library_id) {

        $this->db->select('*');
        $this->db->from('lesson_plan_library_sub');
        $this->db->join('lesson_plans', 'lesson_plan_library_sub.lesson_plan_id  = lesson_plans.lesson_plan_id', 'LEFT');
        $this->db->join('lesson_plan_sub', 'lesson_plan_library_sub.lesson_plan_data  = lesson_plan_sub.lesson_plan_sub_id', 'LEFT');
        $this->db->where('lesson_plan_library_sub.lesson_plan_library_id', $lesson_plan_library_id);
        $query = $this->db->get();

        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function get_lesson_plan_library_custom($lesson_plan_library_id) {

        $this->db->select('*');
        $this->db->from('lesson_plan_library_custom');
        $this->db->join('custom_differentiated', 'lesson_plan_library_custom.custom_diff  = custom_differentiated.custom_differentiated_id', 'LEFT');
        //$this->db->join('lesson_plan_sub','lesson_plan_library_sub.lesson_plan_data  = lesson_plan_sub.lesson_plan_sub_id','LEFT');
        $this->db->where('lesson_plan_library_custom.lesson_plan_library_id', $lesson_plan_library_id);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function getlesson($page, $per_page, $lesson_grade_id) {
        $page -= 1;
        $start = $page * $per_page;


        $this->db->select('lesson_plan_library_data.lesson_plan_library_id,lesson_plan_library_data.standarddata_id,lesson_plan_library_data.lesson_title,lesson_plan_library_data.grade_id,lesson_plan_library_data.strand,lesson_plan_library_data.standard_id,lesson_plan_library_data.standarddata,lesson_plan_library_data.diff_instruction,lesson_plan_library_data.standarddisplay,dist_users.username,dist_grades.grade_name');
        $this->db->from('lesson_plan_library_data');
        $this->db->join('dist_users', 'lesson_plan_library_data.user_id = dist_users.dist_user_id', 'LEFT');
        $this->db->join('dist_grades', 'lesson_plan_library_data.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->where('lesson_plan_library_data.grade_id', $lesson_grade_id);
        $this->db->limit($start, $per_page);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    public function get_progress_data() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }
        $this->db->select('*');
        $this->db->from('student_progress_report');
        $this->db->join('dist_users', 'student_progress_report.user_id = dist_users.dist_user_id', 'LEFT');
//	$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
        $this->db->join('districts', 'student_progress_report.district_id = districts.district_id', 'LEFT');
        $this->db->join('schools', 'student_progress_report.school_id = schools.school_id', 'LEFT');
        $this->db->join('dist_grades', 'student_progress_report.grade_id = dist_grades.dist_grade_id', 'LEFT');
        $this->db->join('periods', 'student_progress_report.period_id = periods.period_id', 'LEFT');
        $this->db->join('dist_subjects', 'student_progress_report.subject_id = dist_subjects.dist_subject_id', 'LEFT');
        $this->db->join('students', 'student_progress_report.student_id = students.student_id', 'LEFT');
        $this->db->where('student_progress_report.user_id', $user_id);

        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_progress_By_id($progress_report_id) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }
        $this->db->select('*');
        $this->db->from('student_progress_report');
        $this->db->where('student_progress_report.user_id', $user_id);
        $this->db->where('student_progress_report.progress_report_id', $progress_report_id);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_progress_By_id_child($progress_report_id) {
        $this->db->select('*');
        $this->db->from('progress_report_score');
        $this->db->where('progress_report_score.progress_report_id', $progress_report_id);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function get_progress_By_id_standard_name($progress_report_id) {
        $this->db->select('*');
        $this->db->from('prog_standard_name');
        $this->db->where('prog_standard_name.progress_report_id', $progress_report_id);
        $query = $this->db->get();
        //print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    function get_class_summary($year,$meeting_id){
        $this->db->select('student_success_strengths.date,students.student_id,students.firstname,students.lastname,sst_meeting_type.meeting_type,sst_referral_reason.referral_reason,student_success_strengths.program_change,student_success_strengths.next_date,dist_grades.grade_name');
        $this->db->from('student_success_strengths');
        $this->db->where('YEAR(student_success_strengths.date)',$year);
        if($meeting_id!=0)
            $this->db->where('student_success_strengths.meeting_type',$meeting_id);
        $this->db->where('student_success_strengths.district_id',$this->session->userdata('district_id'));
        $this->db->join('students','students.student_id=student_success_strengths.student_id');
        $this->db->join('sst_meeting_type','sst_meeting_type.id=student_success_strengths.meeting_type');
        $this->db->join('sst_referral_reason','sst_referral_reason.id=student_success_strengths.referral_reason');
        $this->db->join('dist_grades','dist_grades.dist_grade_id=student_success_strengths.grade_id');
        $query = $this->db->get();
        if($query->num_rows()>0)
            return $result = $query->result_array();
        else
            return false;
    }

    public function progress_report_pdf1($student_id) {

        $this->db->select('*');
        $this->db->from('student_report1');
        // $this->db->join('teachers', 'student_report.teacher_id = teachers.teacher_id');
        // //$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
        // $this->db->join('districts', 'student_report.district_id = districts.district_id', 'LEFT');
        // $this->db->join('schools', 'student_report.school_id = schools.school_id', 'LEFT');
        // $this->db->join('report_grades', 'student_report.grade = report_grades.grade', 'LEFT');
        // $this->db->join('report_efforts', 'student_report.efforts = report_efforts.efforts', 'LEFT');
        // $this->db->join('periods', 'student_report.period_id = periods.period_id', 'LEFT');
        // // $this->db->join('dist_subjects', 'student_report.subject_id = dist_subjects.dist_subject_id', 'LEFT');
         //$this->db->join('report_grades', 'report_grades.id = students.student_id', 'LEFT');
        $this->db->where('student_report1.student_id', $student_id);
        // $this->db->where('student_report.grade_id', $grade);
        // $this->db->where('student_report.date', $date);
        // $this->db->where('student_report.id', $id);
        $query = $this->db->get();

        // print_r($this->db->last_query());exit;

        $result = $query->result();
        return $result;
    }

    public function insert_report($insert) {
        $res = $this->db->insert('student_report1',$insert);
        if (!$res) {
            $val = $this->db->_error_message();
        } else {
            $val = $this->db->insert_id();
        }
        return $val;
    }

    function getreport ($student_id,$quarter,$subject_id='') {
        $this->db->select('*');
        $this->db->from('student_report1');
        $this->db->where('student_id',$student_id);
        $this->db->where('quarter',$quarter);
        if($subject_id!='')
        $this->db->where('subject_id',$subject_id);
        $qry = $this->db->get();
        $result = $qry->result_array();
        return $result;
    }

    function getWorkingHabits() {
        // print_r($this->session->all_userdata());exit;
        $district_id = $this->session->userdata('district_id');
        $this->db->select('*');
        $this->db->from('work_habit');
        $this->db->where('district_id',$district_id);
        $qry = $this->db->get();
        $result = $qry->result_array();
        return $result;

    }

    function getLearningSkills() {
        // print_r($this->session->all_userdata());exit;
        $district_id = $this->session->userdata('district_id');
        $this->db->select('*');
        $this->db->from('learning_skills');
        $this->db->where('district_id',$district_id);
        $qry = $this->db->get();
        $result = $qry->result_array();
        return $result;

    }

    function getInstProg() {
        // print_r($this->session->all_userdata());exit;
        $district_id = $this->session->userdata('district_id');
        $this->db->select('*');
        $this->db->from('instructional_program');
        $this->db->where('district_id',$district_id);
        $qry = $this->db->get();
        $result = $qry->result_array();
        return $result;

    }

    function update_report($update,$where){
        $this->db->where($where);
        $res = $this->db->update('student_report1',$update);
        if($res)
            return true;
        else
            return false;
    }

    //added for incident record
    function create_incident_records($login_id) {
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $district_id = $this->session->userdata('district_id');
        $created_by = $this->session->userdata('login_type');
        $incident_location = $this->input->post('incident_location');
        $incident = $this->input->post('incident');

        if ($this->session->userdata("school_id")) {
            $school_id = $this->session->userdata("school_id");
        } else {
            $school_id = $this->input->post('school_id');
        }
        $incident_records = array('date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))),
            'time' => $time,
            'school_id' => $school_id,
            'district_id' => $district_id,
            'login_type' => $created_by,
            'description' => $incident,
            'incident_location' => $incident_location,
            'login_id' => $login_id
        );

        $result = $this->db->insert('incident_records', $incident_records);
        if ($result) {
            $incident_record_id = $this->db->insert_id();
            if (count($this->input->post('victim_ids')) > 0) {
                foreach ($this->input->post('victim_ids') as $victim) {
                    $victim_id_array = explode('_', $victim);
                    $victim_arr = array('incident_id' => $incident_record_id, 'user_type' => $victim_id_array[0], 'user_id' => $victim_id_array[1]);
                    $this->db->insert('incident_victims', $victim_arr);
                }
            }

            if (count($this->input->post('suspect_ids')) > 0) {
                foreach ($this->input->post('suspect_ids') as $suspect) {
                    $suspect_id_array = explode('_', $suspect);
                    $suspect_arr = array('incident_id' => $incident_record_id, 'user_type' => $suspect_id_array[0], 'user_id' => $suspect_id_array[1]);
                    $this->db->insert('incident_suspects', $suspect_arr);
                }
            }

            if (count($this->input->post('problem_incident')) > 0) {
                foreach ($this->input->post('problem_incident') as $problem) {
                    $problem_arr = array('incident_id' => $incident_record_id, 'problem_id' => $problem);
                    $this->db->insert('incident_problem', $problem_arr);
                }
            }

            if (count($this->input->post('possible_motivation')) > 0) {
                foreach ($this->input->post('possible_motivation') as $motivation) {
                    $motivation_arr = array('incident_id' => $incident_record_id, 'motivation_id' => $motivation);
                    $this->db->insert('incident_motivation', $motivation_arr);
                }
            }
            if (count($this->input->post('interventions_prior')) > 0) {
                foreach ($this->input->post('interventions_prior') as $interventions) {
                    $interventions_arr = array('incident_id' => $incident_record_id, 'interventions_id' => $interventions);
                    $this->db->insert('incident_interventions', $interventions_arr);
                }
            }

            if (count($this->input->post('student_has_multiple_referrals')) > 0) {
                foreach ($this->input->post('student_has_multiple_referrals') as $referrals) {
                    $referrals_arr = array('incident_id' => $incident_record_id, 'referral_id' => $referrals);
                    $this->db->insert('incident_referrals', $referrals_arr);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    function get_incident_list() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $user_id = $this->session->userdata('teacher_id');
        } else if ($this->session->userdata('login_type') == 'observer') {
            $user_id = $this->session->userdata('observer_id');
        } else if ($this->session->userdata('login_type') == 'user') {
            $user_id = $this->session->userdata('dist_user_id');
        }

        $this->db->select('*');
        $this->db->from('incident_records');
        $this->db->where('login_id', $user_id);
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_victims');
                $this->db->where('incident_id', $result->incident_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->incident_id][] = $usersqry->result();
                }
                $this->db->select('*');
                $this->db->from('incident_suspects');
                $this->db->where('behavior_id', $result->incident_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_behaviour', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                $this->db->select('*');
                $this->db->from('incident_interventions');
                $this->db->join('behavior_records', 'br_interventions.incident_id = behavior_records.incident_id');
                $this->db->join('interventions_prior', 'interventions_prior.id = incident_interventions.interventions_id');
                $this->db->where('incident_records.behavior_id', $result->behavior_id);
                $interventionqry = $this->db->get();
                $intervention = $interventionqry->result();

                $this->db->select('*');
                $this->db->from('incident_referrals');
                $this->db->join('incident_records', 'incident_referrals.incident_id = incident_records.incident_id');
                $this->db->join('student_has_multiple_referrals', 'student_has_multiple_referrals.id = br_referrals.referral_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $referralqry = $this->db->get();
                $referral = $referralqry->result();

                $this->db->select('*');
                $this->db->from('incident_motivation');
                $this->db->join('incident_records', 'incident_motivation.incident_id = incident_records.incident_id');
                $this->db->join('possible_motivation', 'possible_motivation.id = incident_motivation.motivation_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $motivationqry = $this->db->get();
                $motivation = $motivationqry->result();

                $this->db->select('*');
                $this->db->from('incident_location');
                $this->db->where('incident_location.id', $result->incident_location);
                $locationqry = $this->db->get();
                $location = $locationqry->result();

                $returnarr[$result->behavior_id]['incident'] = $behavior;
                $returnarr[$result->behavior_id]['victim'] = $usersvictim[$result->incident_id];
                $returnarr[$result->behavior_id]['suspect'] = $userssuspect[$result->incident_id];
                $returnarr[$result->behavior_id]['date'] = $result->date;
                $returnarr[$result->behavior_id]['description'] = $result->description;
                $returnarr[$result->behavior_id]['intervention'] = $intervention;
                $returnarr[$result->behavior_id]['referral'] = $referral;
                $returnarr[$result->behavior_id]['motivation'] = $motivation;
                $returnarr[$result->behavior_id]['location'] = $result->incident_location;
                // print_r($returnarr);exit;
            }

            return $returnarr;
        } else
            return false;
    }

    public function get_graph_incident_by_incident() {
        $this->db->select('incident_problem.*,incident_records.*,dist_prob_incident.*,schools.school_name');
        $this->db->from('incident_problem');
        $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
        $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
        $this->db->join('schools', 'schools.school_id = incident_records.school_id');

        if ($this->input->post('incident') != 0) {
            $this->db->where('incident_problem.problem_id', $this->input->post('incident'));
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $this->db->where('YEAR(incident_records.date)', $this->input->post('year'));
        }

        if ($this->input->post('month') != 'all') {
            $this->db->where('MONTH(incident_records.date)', $this->input->post('month'));
        }

if ($this->input->post('school_id')) {
            $school_id = $this->input->post('school_id');
        } else {
            $school_id = $this->session->userdata('school_id');
        }
        if ($this->session->userdata('login_type') == 'user'){
            $this->db->where('incident_records.district_id', $this->session->userdata('district_id'));
        } else {
            $this->db->where('incident_records.school_id', $school_id);
        }
        $this->db->group_by('incident_problem.incident_id');
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('incident_problem.*,incident_records.*,dist_prob_incident.*,schools.school_name');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->join('schools', 'schools.school_id = incident_records.school_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }
            }
            return $returnarr;
        } else
            return false;
    }

    public function get_list_incident_by_incident() {
        $this->db->select('*');
        $this->db->from('incident_problem');
        $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
        $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
        if ($this->input->post('incident') != 0) {
            $this->db->where('incident_problem.problem_id', $this->input->post('incident'));
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $this->db->where('YEAR(incident_records.date)', $this->input->post('year'));
        }

        if ($this->input->post('month') != 'all') {
            $this->db->where('MONTH(incident_records.date)', $this->input->post('month'));
        }
        if ($this->input->post('school_id')) {
            $school_id = $this->input->post('school_id');
        } else {
            $school_id = $this->session->userdata('school_id');
        }
        $this->db->where('school_id', $school_id);
        $this->db->group_by('incident_problem.incident_id');
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_victims');
                $this->db->where('incident_id', $result->incident_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                // print_r($victims);exit;
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->incident_id][] = $usersqry->result();
                }
                // print_r($usersvictim);exit;
                // echo $result->incident_id;exit;
                $this->db->select('*');
                $this->db->from('incident_suspects');
                $this->db->where('incident_id', $result->incident_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                $returnarr[$result->incident_id]['incident'] = $behavior;
                $returnarr[$result->incident_id]['victim'] = $usersvictim[$result->incident_id];
                $returnarr[$result->incident_id]['suspect'] = $userssuspect[$result->incident_id];
                $returnarr[$result->incident_id]['date'] = $result->date . ' ' . $result->time;
                $returnarr[$result->incident_id]['description'] = strip_tags($result->description);
                // print_r($returnarr);exit;
            }
            return $returnarr;
        } else
            return false;
    }

    public function get_graph_incident_by_student() {
        if ($this->input->post('student_id') != 0) {
            $wherevictim = "incident_victims.user_id = " . $this->input->post('student_id') . " AND";
            $wheresuspect = "incident_suspects.user_id = " . $this->input->post('student_id') . " AND";
        } else {
            $wherevictim = "";
            $wheresuspect = "";
        }
        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(incident_records.date) = " . $this->input->post('year');
        } else {
            $whereyear = "";
        }
        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(incident_records.date) = " . $this->input->post('month');
        } else {
            $wheremonth = "";
        }

        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND incident_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }
        $query = $this->db->query("SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_victims ON incident_victims.incident_id = incident_records.incident_id where  $wherevictim incident_victims.user_type = 'student' $whereyear $wheremonth $whereschool
                UNION 
            SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_suspects ON incident_suspects.incident_id = incident_records.incident_id where $wheresuspect incident_suspects.user_type = 'student' $whereyear $wheremonth $whereschool");
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                if (!empty($returnarr)) {
                    $returnarr = array_merge_recursive($returnarr, $behavior);
                } else {
                    $returnarr = $behavior;
                }
            }
            return $returnarr;
        } else
            return false;
    }

        public function get_list_incident_by_student() {
        if ($this->input->post('student_id') != 0) {
            $wherevictim = "incident_victims.user_id = " . $this->input->post('student_id') . " AND";
            $wheresuspect = "incident_suspects.user_id = " . $this->input->post('student_id') . " AND";
        } else {
            $wherevictim = "";
            $wheresuspect = "";
        }
        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(incident_records.date) = " . $this->input->post('year');
        } else {
            $whereyear = "";
        }
        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(incident_records.date) = " . $this->input->post('month');
        } else {
            $wheremonth = "";
        }

        $query = $this->db->query("SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_victims ON incident_victims.incident_id = incident_records.incident_id where  $wherevictim incident_victims.user_type = 'student' $whereyear $wheremonth
                                    UNION 
                                SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_suspects ON incident_suspects.incident_id = incident_records.incident_id where $wheresuspect incident_suspects.user_type = 'student' $whereyear $wheremonth");
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_victims');
                $this->db->where('incident_id', $result->incident_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_suspects');
                $this->db->where('incident_id', $result->incident_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                $returnarr[$result->incident_id]['incident'] = $behavior;
                $returnarr[$result->incident_id]['victim'] = $usersvictim[$result->incident_id];
                $returnarr[$result->incident_id]['suspect'] = $userssuspect[$result->incident_id];
                $returnarr[$result->incident_id]['date'] = $result->date;
                $returnarr[$result->incident_id]['description'] = $result->description;
            }
            return $returnarr;
        } else
            return false;
    }


    function incident_record_by_id($id) {
        $this->db->select('*');
        $this->db->from('incident_problem');
        $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
        $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
        $this->db->where('incident_records.incident_id', $id);
        $this->db->group_by('incident_problem.incident_id');
        $query = $this->db->get();
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_victims');
                $this->db->where('incident_id', $result->incident_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_suspects');
                $this->db->where('incident_id', $result->incident_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('br_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();

                $this->db->select('*');
                $this->db->from('incident_interventions');
                $this->db->join('incident_records', 'br_interventions.incident_id = incident_records.incident_id');
                $this->db->join('incident_interventions_prior', 'incident_interventions_prior.id = incident_interventions.interventions_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $interventionqry = $this->db->get();
                $intervention = $interventionqry->result();

                $this->db->select('*');
                $this->db->from('incident_referrals');
                $this->db->join('incident_records', 'br_referrals.behavior_id = behavior_records.behavior_id');
                $this->db->join('incident_student_has_multiple_referrals', 'incident_student_has_multiple_referrals.id = incident_referrals.referral_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $referralqry = $this->db->get();
                $referral = $referralqry->result();

                $this->db->select('*');
                $this->db->from('incident_motivation');
                $this->db->join('incident_records', 'incident_motivation.incident_id = incident_records.incident_id');
                $this->db->join('incident_possible_motivation', 'incident_possible_motivation.id = incident_motivation.motivation_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $motivationqry = $this->db->get();
                $motivation = $motivationqry->result();

                $this->db->select('*');
                $this->db->from('incident_location');
                $this->db->where('incident_location.id', $result->incident_location);
                $locationqry = $this->db->get();
                $location = $locationqry->result();

                $returnarr['incident'] = $behavior;
                $returnarr['victim'] = $usersvictim[$result->incident_id];
                $returnarr['suspect'] = $userssuspect[$result->incident_id];
                $returnarr['date'] = $result->date;
                $returnarr['description'] = $result->description;
                $returnarr['intervention'] = $intervention;
                $returnarr['referral'] = $referral;
                $returnarr['motivation'] = $motivation;
                $returnarr['location'] = $location[0]->incident_location;
            }
            return $returnarr;
        } else
            return false;
    }

    public function get_eincident_list_by_location() {
        if ($this->input->post('behavior_location') != 0) {
            $behavior_location = $this->input->post('behavior_location');
            $where = "AND incident_records.incident_location = '$behavior_location'"; //$behavior_location  = $this->input->post('behavior_location');
        } else {
            $where = "";
        }

        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
            $whereschool = "AND incident_records.school_id = $school_id";
        } else {
            $whereschool = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(incident_records.date) = " . $this->input->post('year');
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(incident_records.date) = " . $this->input->post('month');
        } else {
            $wheremonth = "";
        }

        $query = $this->db->query("SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_victims ON incident_victims.incident_id = incident_records.incident_id WHERE 1 $where $whereschool $wheremonth $whereyear UNION SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_suspects ON incident_suspects.incident_id = incident_records.incident_id where 1 $where $whereschool $wheremonth $whereyear");
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_victims');
                $this->db->where('incident_id', $result->incident_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_suspects');
                $this->db->where('incident_id', $result->incident_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                $returnarr[$result->incident_id]['incident'] = $behavior;
                $returnarr[$result->incident_id]['victim'] = $usersvictim[$result->incident_id];
                $returnarr[$result->incident_id]['suspect'] = $userssuspect[$result->incident_id];
                $returnarr[$result->incident_id]['date'] = $result->date;
                $returnarr[$result->incident_id]['description'] = $result->description;
            }
            return $returnarr;
        } else
            return false;
    }

     public function get_list_by_student_incident() {
        if ($this->input->post('student_id') != 0) {
            $wherevictim = "incident_victims.user_id = " . $this->input->post('student_id') . " AND";
            $wheresuspect = "incident_suspects.user_id = " . $this->input->post('student_id') . " AND";
        } else {
            $wherevictim = "";
            $wheresuspect = "";
        }

        if ($this->input->post('year') != 0 || $this->input->post('year') != '') {
            $whereyear = "AND YEAR(incident_records.date) = " . $this->input->post('year');
            //$this->db->where('YEAR(behavior_records.date)',$this->input->post('year')); 
        } else {
            $whereyear = "";
        }

        if ($this->input->post('month') != 'all') {
            $wheremonth = "AND MONTH(incident_records.date) = " . $this->input->post('month');
            //$this->db->where('MONTH(behavior_records.date)',$this->input->post('month')); 
        } else {
            $wheremonth = "";
        }


        $query = $this->db->query("SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_victims ON incident_victims.incident_id = incident_records.incident_id where  $wherevictim incident_victims.user_type = 'student' $whereyear $wheremonth
 
UNION 

SELECT incident_records.incident_id,incident_records.date,incident_records.description FROM `incident_records` JOIN incident_suspects ON incident_suspects.incident_id = incident_records.incident_id where $wheresuspect br_suspects.user_type = 'student' $whereyear $wheremonth");
        $results = $query->result();
        if ($results) {
            foreach ($results as $result) {
                $this->db->select('*');
                $this->db->from('incident_victims');
                $this->db->where('incident_id', $result->incident_id);
                $victimqry = $this->db->get();
                $victims = $victimqry->result();
                foreach ($victims as $victim) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($victim->user_type . 's');
                    $this->db->where($victim->user_type . '_id', $victim->user_id);
                    $usersqry = $this->db->get();
                    $usersvictim[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_suspects');
                $this->db->where('incident_id', $result->incident_id);
                $suspectqry = $this->db->get();
                $suspects = $suspectqry->result();
                foreach ($suspects as $suspect) {
                    $this->db->select('lastname,firstname');
                    $this->db->from($suspect->user_type . 's');
                    $this->db->where($suspect->user_type . '_id', $suspect->user_id);
                    $usersqry = $this->db->get();
                    $userssuspect[$result->incident_id][] = $usersqry->result();
                }

                $this->db->select('*');
                $this->db->from('incident_problem');
                $this->db->join('incident_records', 'incident_problem.incident_id = incident_records.incident_id');
                $this->db->join('dist_prob_incident', 'dist_prob_incident.id = incident_problem.problem_id');
                $this->db->where('incident_records.incident_id', $result->incident_id);
                $behaviorqry = $this->db->get();
                $behavior = $behaviorqry->result();
                //print_r($behavior);exit;
                $returnarr[$result->incident_id]['incident'] = $behavior;
                $returnarr[$result->incident_id]['victim'] = $usersvictim[$result->incident_id];
                $returnarr[$result->incident_id]['suspect'] = $userssuspect[$result->incident_id];
                $returnarr[$result->incident_id]['date'] = $result->date;
                $returnarr[$result->incident_id]['description'] = $result->description;
            }
//            echo "<pre>";
//            print_r($returnarr);exit;
//            echo '<br>';echo '<br>';echo '<br>';
//            print_r($userssuspect);exit;
            return $returnarr;
        } else
            return false;
    }

    function get_colleagues(){
        $district_id = $this->session->userdata('district_id');
        $this->db->select('*');
        $this->db->from('incident_colleague');
        $this->db->where('district_id',$district_id);
        $query = $this->db->get();
        $colleagues = $query->result_array();
        return $colleagues;
    }
}
