﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>UEIS Workshop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />



        <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
        <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>js/plugins/bootstrap-tagsinput/app.css">
        <script>
            var base_url = '<?php echo base_url(); ?>';
        </script>    
        <!-- Ends here -->  
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php require_once($view_path . 'inc/header.php'); ?>
        <!-- END HEADER --> 
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid"> 
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar-scroll">
                <div id="sidebar" class="nav-collapse collapse"> 

                    <!-- BEGIN SIDEBAR MENU -->
                    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
                    <!-- END SIDEBAR MENU --> 
                </div>
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN PAGE -->
            <div id="main-content"> 
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid"> 
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12"> 

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <h3 class="page-title"> <i class="icon-group"></i>&nbsp; Classroom Management </h3>
                            <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/incident_record"> Incident Running Record</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>classroom/incident_report_list">Retrieve Reports</a>
                            <span class="divider">></span>
                       </li>
                       <li>
                            <a href="<?php echo base_url();?>classroom/location_eincident_report">Incident Entries by Location Of Incident</a>
                            
                       </li>
                   </ul>
                            <!-- END PAGE TITLE & BREADCRUMB--> 
                        </div>
                    </div>
                    <!-- END PAGE HEADER--> 
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN BLANK PAGE PORTLET-->
                            <div class="widget yellow">
                                <div class="widget-title">
                                    <h4>Incident Entries by Location Of Incident</h4>
                                </div>
                                <div class="widget-body">
                                    <form class="form-horizontal" method="post" action="<?php echo base_url() . 'classroom/create_behavior_records'; ?>" name="create_behavior_form" id="create_behavior_form">
                                        <div id="pills" class="custom-wizard-pills-yellow2">
                                            <ul>
                                                <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                                <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                                <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                                <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                            </ul>
                                            <div class="progress progress-success-yellow progress-striped active">
                                                <div class="bar"></div>
                                            </div>
                                            <div class="tab-content">

                                                <!-- BEGIN STEP 1-->
                                                <div class="tab-pane" id="pills-tab1">
                                                    <h3 style="color:#000000;">STEP 1</h3>

   <?php if ($this->session->userdata('login_type') == 'user'): ?>
                                            <div class="control-group">
                                                <label class="control-label">Select School</label>
                                                <div class="controls">
                <select class="span12 chzn-select" name="school_id" id="school_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" >
                                                        <option value=""></option>
                                                        <?php foreach ($school as $schools): ?>
                  <option value="<?php echo $schools['school_id']; ?>"><?php echo $schools['school_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                 <?php endif; ?>


 									 <div class="control-group">
        <label class="control-label">Select Location</label>
        <div class="controls">
          <select class="span12 chzn-select" name="behavior_location" id="behavior_location" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
             <option value=''></option>
             <option value="0">All</option>
 				<?php if(!empty($behavior_location)) { 
			  foreach($behavior_location as $behavior_location_value)
			  {?>
<option value="<?php echo $behavior_location_value->id;?>"><?php echo $behavior_location_value->behavior_location;?></option>
			  <?php	} } ?>			   
		      </select>
        </div>
      </div>



                                                </div>

                                                <!-- BEGIN STEP 2-->
                                                <div class="tab-pane" id="pills-tab2">
                                                    <h3 style="color:#000000">STEP 2</h3>
                                                    <div class="control-group">
                                                        <label class="control-label">Select Year</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="year" id="year" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="">-Please Select-</option>
                                                                <?php
                                                                for ($i = date('Y'); $i >= 1982; $i--) {
                                                                    echo "<option value=" . $i;
                                                                    echo " >" . $i . "</option>";
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="control-group" id="start_date" style="display:none;">
                                                        <label class="control-label">Select Start Date</label>
                                                        <div class="controls">
                                                            <input type="text" name="start_date" id="dp1" value=""> 
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- BEGIN STEP 3-->
                                                <div class="tab-pane" id="pills-tab3">
                                                    <h3 style="color:#000000">STEP 3</h3>
                                                    <div class="control-group" >
                                                        <label class="control-label">Select Month</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="month" id="month" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="all" selected>-All-</option>
                                                                <option value="1"  >January</option>
                                                                <option value="2"  >February</option>
                                                                <option value="3"  >March</option>
                                                                <option value="4"  >April</option>
                                                                <option value="5"  >May</option>
                                                                <option value="6"  >June</option>
                                                                <option value="7"  >July</option>
                                                                <option value="8"  >August</option>
                                                                <option value="9"  >September</option>
                                                                <option value="10" >October</option>
                                                                <option value="11" >November</option>
                                                                <option value="12" >December</option>
                                                            </select>
                                                        </div>
                                                    </div>  
                                                    
                                                    <div class="control-group">
                                                        <label class="control-label">Select Report Type</label>
                                                        <div class="controls">
                                                            <select class="span12 chzn-select" name="report_type" id="report_type" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                <option value="graph">Graph</option>
                                                                <option value="list" >List</option>
                                                            </select>
                                                        </div>
                                                    </div>  
                                                </div>

                                                <!--BEGIN STEP 4 -->
                                                <div class="tab-pane" id="pills-tab4">
                                                    <h3 style="color:#000000">STEP 4</h3>
                                                    <div id="gridcontainer"></div>
                                                </div>
                                                <div class="space20"></div>
                                                <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget yellow">
                         <div class="widget-title">
                             <h4>Behavior Running Record Details</h4>
                          
                         </div> 
                                       <div class="widget-body" style="min-height: 150px;" id="reportbody">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">Report Appears Here</h3>
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
<!--                        <center><button class="btn btn-large btn-yellow"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-yellow"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>-->
                          
                                </div>  
                                                <ul class="pager wizard">
                                                    <li class="previous first yellow"><a href="javascript:;">First</a></li>
                                                    <li class="previous yellow"><a href="javascript:;">Previous</a></li>
                                                    <li class="next last yellow"><a href="javascript:;">Last</a></li>
                                                    <li class="next yellow"><a  href="javascript:;">Next</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END BLANK PAGE PORTLET--> 
                        </div>
                    </div>

                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
            </div>
            <!-- END PAGE -->
        </div>
        <!-- END CONTAINER --> 

        <!-- BEGIN FOOTER -->
        <div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
        <!-- END FOOTER --> 

        <!-- BEGIN JAVASCRIPTS --> 
        <!-- Load javascripts at bottom, this will reduce page load time --> 
        <script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script> 
        <script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script> 
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
        <script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
        <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script> 
        <script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
        <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script> 
        <!-- ie8 fixes --> 
        <!--[if lt IE 9]>
           <script src="js/excanvas.js"></script>
           <script src="js/respond.js"></script>
           <![endif]--> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
        <script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 



        <!--common script for all pages--> 
        <script src="<?php echo SITEURLM ?>js/common-scripts.js"></script> 
        <!--script for this page--> 
<!--        <script src="<?php echo SITEURLM ?>js/form-wizard.js"></script> -->
        <script src="<?php echo SITEURLM ?>js/form-component.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/jquery.dataTables.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/DT_bootstrap.js"></script> 


 
        <script src="<?php echo SITEURLM ?>js/ajax-chosen.js"></script> 
        <script type="text/javascript" src="<?php echo SITEURLM ?>js/highcharts.js"></script>
          <script src="<?php echo SITEURLM ?>js/exporting.js"></script>



        <!--script for this page only--> 

        <script>
            $(document).ready(function() {
                $("#victim_ids").ajaxChosen({
                    type: 'post',
                    url: '<?php echo base_url(); ?>classroom/autocompleteuser',
                    dataType: 'json'
                },
                function(data)
                {
                    var terms = {};

                    $.each(data, function(i, val) {
                        terms[i] = val;
                    });

                    return terms;
                }).change(function() {
                    //you can see the IDs in console off all items in autocomplete and deal with them
                    console.log($("#jacComplete").val());
                });

                $("#suspect_ids").ajaxChosen({
                    type: 'post',
                    url: '<?php echo base_url(); ?>classroom/autocompleteuser',
                    dataType: 'json'
                },
                function(data)
                {
                    var terms = {};

                    $.each(data, function(i, val) {
                        terms[i] = val;
                    });

                    return terms;
                }).change(function() {
                    //you can see the IDs in console off all items in autocomplete and deal with them
                    console.log($("#jacComplete").val());
                });

                
            });
        var Script = function () {

            $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
                console.log('onShow');
            }, onNext: function(tab, navigation, index) {
                 if(index < 3){
                    $('#reportDiv').hide();
                    $('#reportbody').html('');
                }
                if(index==3){
                    if($('#year').val()==''){
                        alert('Please select Year.');
                        $("#pills").bootstrapWizard("show",1);
                        return false;
                    }
                     $('#gridcontainer').html('');
                    $('#reportDiv').hide();
                    $('#reportbody').html('');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>classroom/eincident_location_by_list",
                        data: { behavior_location: $('#behavior_location').val(), year: $('#year').val(),month: $('#month').val(),reportType:$('#report_type').val(),student_name: $('#behavior_location option:selected').text() }
                      })
                        .done(function( msg ) {
                            if($('#report_type').val()=='graph'){
                                eval(msg);
                            } else {
                               $('#gridcontainer').html(msg);  
                            }
                        });
                }
            }, onPrevious: function(tab, navigation, index) {
                console.log('onPrevious');
            }, onLast: function(tab, navigation, index) {
                console.log('onLast');
            }, onTabShow: function(tab, navigation, index) {
        //        console.log(tab);
        //        console.log(navigation);
        //        console.log(index);
        //        console.log('onTabShow1');
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#pills').find('.bar').css({width:$percent+'%'});
                if(index < 3){
                    $('#reportDiv').hide();
                    $('#reportbody').html('');
                }
                if(index==3){
                    if($('#year').val()==''){
                        alert('Please select Year.');
                        $("#pills").bootstrapWizard("show",1);
                        return false;
                    }
                     $('#gridcontainer').html('');
                    $('#reportDiv').hide();
                    $('#reportbody').html('');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>classroom/location_by_list",
                        data: { behavior_location: $('#behavior_location').val(), year: $('#year').val(),month: $('#month').val(),reportType:$('#report_type').val(),student_name: $('#behavior_location option:selected').text() }
                      })
                        .done(function( msg ) {
                            if($('#report_type').val()=='graph'){
                                eval(msg);
                            } else {
                               $('#gridcontainer').html(msg);  
                            }
                        });
                }
            }});

            $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
                console.log('onShow');
            }, onNext: function(tab, navigation, index) {
                console.log('onNext');
            }, onPrevious: function(tab, navigation, index) {
                console.log('onPrevious');
            }, onLast: function(tab, navigation, index) {
                console.log('onLast');
            }, onTabClick: function(tab, navigation, index) {
                console.log('onTabClick');

            }, onTabShow: function(tab, navigation, index) {
                console.log('onTabShow2');
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#tabsleft').find('.bar').css({width:$percent+'%'});
                if(index < 3){
                    $('#reportDiv').hide();
                    $('#reportbody').html('');
                }
                if(index==3){
                    if($('#year').val()==''){
                        alert('Please select Year.');
                        $("#pills").bootstrapWizard("show",1);
                        return false;
                    }
                     $('#gridcontainer').html('');
                    $('#reportDiv').hide();
                    $('#reportbody').html('');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>classroom/location_by_list",
                        data: { behavior_location: $('#behavior_location').val(), year: $('#year').val(),month: $('#month').val(),reportType:$('#report_type').val(),student_name: $('#behavior_location option:selected').text() }
                      })
                        .done(function( msg ) {
                            if($('#report_type').val()=='graph'){
                                eval(msg);
                            } else {
                               $('#gridcontainer').html(msg);  
                            }
                        });
                }

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#tabsleft').find('.pager .next').hide();
                    $('#tabsleft').find('.pager .finish').show();
                    $('#tabsleft').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#tabsleft').find('.pager .next').show();
                    $('#tabsleft').find('.pager .finish').hide();
                }

            }});


            $('#tabsleft .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
            });

        }();
        </script>    
        
        
    </body>
    <!-- END BODY -->
</html>