﻿   <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District Id</td>
     <td>Interventions Prior To Office Discipline Referral</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page -1)*10;
   $sno=1;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="problem_<?php echo $data->id;?>">
   <td><?php echo $sno++;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->incident_interventions_prior;?></td>
   <td><?php echo $data->status;?></td>
      <td>
       <button class="edit_interventions btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
  <td>
    <button type="Submit" id="remove_interventions" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_interventions btn btn-danger"><i class="icon-trash"></i></button>
    </td>

  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_interventions').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'incident_interventions_prior/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#interventions_id').val(result[0].id);
       $('#incident_interventions_prior').val(result[0].incident_interventions_prior);
       $('#status').val(result[0].status);
       console.log(result[0].incident_interventions_prior);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 600
      });
       }
     });  
});

$(".remove_interventions").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'incident_interventions_prior/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#problem_"+id).css('display','none');
          alert('Successfully removed Interventions Prior To Office Discipline Referral from list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

