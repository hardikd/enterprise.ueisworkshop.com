﻿     <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>district_id</td>
     <td>school</td>
     <td>status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page-1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="school_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><a href="<?php base_url();?>needs_for_school_child/index/<?php echo $data->id;?>"><?php echo $data->intervention_strategies_school;?></a></td>
   <td><?php echo $data->status;?></td>
      <td>
      <button class="edit_strategies_school btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
      <td>
      <button type="Submit" id="remove_strategies_school" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_strategies_school btn btn-danger"><i class="icon-trash"></i></button>
      </td>
      
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_strategies_school ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'intervention_strategies_school/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#strategies_school_id').val(result[0].id);
       $('#intervention_strategies_school').val(result[0].intervention_strategies_school);
       $('#status').val(result[0].status);
       console.log(result[0].intervention_strategies_school);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_strategies_school").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'intervention_strategies_school/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#school_"+id).css('display','none');
          alert('Successfully removed School list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

