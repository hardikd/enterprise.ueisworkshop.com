<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
  <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-edit"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                         <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>planningmanager/">Planning Manager</a>
                            <span class="divider">></span>
                          
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>planningmanager/smart_goals">SMART Goal Review</a>
                            <span class="divider">></span>
                          
                       </li>
                       
                       
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/dist_smart_goals_retrieve">Retrieve Review</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/dist_retrieve_goal">View Smart Goals & Media Link</a>
                           
                       </li>
                       
                                                
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Edit SMART Goal Review in Progress</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-green4">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     
                                      
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                   <!-- BEGIN STEP 1-->
                                     <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                         <?php if ($this->session->userdata('login_type')=='user'):?>
                                                   <div class="control-group">
                                                    <label class="control-label">Select School</label>
                                                    <div class="controls">

                                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="school_id" id="school_id">
                                                       <option value=""></option>
                                                        <?php foreach($schools as $school):?>
                                                            <option value="<?php echo $school['school_id'];?>"><?php echo $school['school_name'];?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    </div>
                                                </div>
                                
                                         <div class="control-group">
                                            <label class="control-label">Select Teacher</label>
                                            <div class="controls">

                                                <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id">
                                                <!--<option value="all">All</option>-->
                                                <?php // foreach($teachers as $teacher):?>
                                                    <!--<option value="<?php // echo $teacher['teacher_id'];?>"><?php // echo $teacher['firstname'].' '.$teacher['lastname'];?></option>-->
                                                <?php // endforeach;?>

                                            </select>
                                            </div>
                                        </div>
                                          <?php elseif ($this->session->userdata('login_type')=='observer'):?>
                                             <input type="hidden" name="school_id" id="school_id" value="<?php echo $this->session->userdata('school_id');?>" />
                                                 <div class="control-group">
                                            <label class="control-label">Select Teacher</label>
                                            <div class="controls">

                                                <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id">
                                                <option value="all">All</option>
                                                <?php foreach($teachers as $teacher):?>
                                                    <option value="<?php echo $teacher['teacher_id'];?>"><?php echo $teacher['firstname'].' '.$teacher['lastname'];?></option>
                                                <?php endforeach;?>

                                            </select>
                                            </div>
                                        </div>
								<?php elseif ($this->session->userdata('login_type') == 'teacher'):?>
                                       <input type="hidden" name="school_id" id="school_id" value="<?php echo $this->session->userdata('school_id');?>" />
                                       <input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $this->session->userdata('teacher_id');?>" />
                                        
                                             <?php endif;?>
                                             
                                             
                                    
                                         <div class="control-group">
                                    <label class="control-label">Select Year</label>
                                    <div class="controls">
                                        <input id="dpYears" type="text" value="<?php echo date('Y');?>" size="16" class="m-ctrl-medium" ></input>
                                    </div>
                                </div>
                                
                                
                                        
                                                                             
                                     </div>
                                  
                                  
                                     <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <div id="goal_lsit">
                                         
                                         
                                         </div>
                                     </div>
                                     
                                       <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                         
                                        
                                         <div id="commentsview">
                                             
                                             
                                         </div>
                                         <!--Media Part starts-->
                                         <div id="reportDiv"  style="display:block;" class="answer_list" >
                                   <div class="widget green">
                        	 <div class="widget-title">
                             <h4>Media</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
        
                                   <div class="content" style="margin-left:0;" id="mediacontent">
        
       


      </div></div>
                                  </div>
                                
                                </div>   
                                     <!--Media Part Ends-->
                                     
                                         
                                     </div>
                                     
                                     
                                     
                                        <!-- BEGIN STEP 4-->
                                     
                                  </div>        
                                         
                                           
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
     
      <!-- END PAGE -->  
   
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-wizard.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
   
   <script>
       $(function() {
        $('#reviewselector').change(function(){
            $('.review').hide();
            $('#' + $(this).val()).show();
        });
    });
   </script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
var base_url = '<?php echo base_url();?>';


$('#school_id').on('change',function(){
   
         $.ajax({
            type: "POST",
            url: base_url+"teacherself/getteacherBySchoolId",
            data: { school_id: $('#school_id').val()}
          })
            .done(function( result) {
//              console.log(teachers);
                $('#teacher_id').html('');
                $('#teacher_id').append('<option value="all">ALL</option>');
              var teachers = jQuery.parseJSON(result);
              $.each(teachers,function(index,value){
//                    console.log(value);
                    $('#teacher_id').append('<option value="'+value.teacher_id+'">'+value.firstname+' '+value.lastname+'</option>');
              });
              $('#teacher_id').trigger("liszt:updated");
              
              $.ajax({
                type: "POST",
                url: base_url+"planningmanager/dist_retrieve_goal2",
                data: { school_id: $('#school_id').val(),teacher_id:$('#teacher_id').val()}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#goal_lsit').html(result);
                   
                });
              
            });
     });
     
     $('#teacher_id').on('change',function(){
         $.ajax({
                type: "POST",
                url: base_url+"planningmanager/dist_retrieve_goal2",
                data: { school_id: $('#school_id').val(),teacher_id:$('#teacher_id').val()}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#goal_lsit').html(result);
                   
                });
     });
     
     
     function commentsviews(teacher_id,grade_id,subject_id,school_id,goal_date){
         $.ajax({
                type: "POST",
                url: base_url+"planningmanager/dist_retrieve_goal3",
                data: { year: $('#dpYears').val(),teacher_id:teacher_id,grade_id:grade_id,subject_id:subject_id,school_id:school_id,goal_date:goal_date}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#commentsview').html(result);
//                   $("#pills").bootstrapWizard("show",2)
                   
                });
          $.ajax({
                type: "POST",
                url: base_url+"planningmanager/getmediasmart/1/"+$('#school_id').val()+"/"+teacher_id,
                data: { year: $('#dpYears').val(),teacher_id:teacher_id,grade_id:grade_id,subject_id:subject_id,school_id:school_id,goal_date:goal_date}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#mediacontent').html(result);
                   $("#pills").bootstrapWizard("show",2)
                   
                });
    }
         function commentsview(teacher_id){
         $.ajax({
                type: "POST",
                url: base_url+"planningmanager/dist_retrieve_goal3",
                data: { year: $('#dpYears').val(),teacher_id:teacher_id}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#commentsview').html(result);
//                   $("#pills").bootstrapWizard("show",2)
                   
                });
          $.ajax({
                type: "POST",
                url: base_url+"planningmanager/getmedia/1/"+$('#school_id').val()+"/"+teacher_id,
                data: { year: $('#dpYears').val(),teacher_id:teacher_id}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#mediacontent').html(result);
                   $("#pills").bootstrapWizard("show",2)
                   
                });
    }
    
    function gotopage(page){
    alert(page);
    $('#mediacontent').html('');
        $.ajax({
                type: "POST",
                url: base_url+"planningmanager/getmedia/"+page+"/<?php echo $this->session->userdata('school_id');?>/"+teacher_id,
                data: { year: $('#dpYears').val(),teacher_id:teacher_id}
              })
                .done(function( result) {
//                     result = jQuery.parseJSON(result);
//                  console.log(teachers);
                  $('#mediacontent').html(result);
                   
                   
                });
    }
    
    
    $('#mediacontent li.active').live('click',function(){
	
            var school_id=$('#school_id').val();
	
	var teacher_id = $('#selectedteacher').val();
	
	if(school_id=='')
	{
	   school_id='empty';
	
	
	}
	
	  var page = $(this).attr('p');
	$('#mediapageid').val(page);
	
	  $.get(base_url+"planningmanager/getmedia/"+page+"/"+school_id+"/"+teacher_id, function(msg){
	
	$('#mediacontent').html(msg);
	
	
	});
	
	
	
	
	
	
	});
        
        var Script = function () {

    $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        console.log(index);
        
        console.log('onNext');
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(index);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');
        if(index==1){
            $.ajax({
                type: "POST",
                url: base_url+"planningmanager/get_smartgoals_year",
                data: { year: $('#dpYears').val()}
              })
                .done(function( result) {
                    $('#goal_lsit').html(result);
                     result = jQuery.parseJSON(result);
                  console.log(result);
                  
                   
                   
                });
        }
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
        
    }});

    $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
        
    }, onNext: function(tab, navigation, index) {
        console.log('onNext');
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabClick: function(tab, navigation, index) {
        console.log('onTabClick');
        
    }, onTabShow: function(tab, navigation, index) {
        console.log('onTabShow2');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#tabsleft').find('.bar').css({width:$percent+'%'});
        

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
            $('#tabsleft').find('.pager .next').hide();
            $('#tabsleft').find('.pager .finish').show();
            $('#tabsleft').find('.pager .finish').removeClass('disabled');
        } else {
            $('#tabsleft').find('.pager .next').show();
            $('#tabsleft').find('.pager .finish').hide();
        }

    }});


    $('#tabsleft .finish').click(function() {
        alert('Finished!, Starting over!');
        $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
    });

}();

   </script>  
</body>
<!-- END BODY -->
</html>