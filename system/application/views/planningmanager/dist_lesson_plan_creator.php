﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
  <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
             <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
     <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                            <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>planningmanager">Planning Manager</a>
                            <span class="divider">></span>
                          
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Library</a>
                            
                          
                       </li>
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
                <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                        <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-green long-dash-half">
                        <a href="<?php echo base_url();?>planningmanager/lesson_Plan_library" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-plus"></i><br><br><br>
                              Create Lesson Plan Library                            </span>                        </a>                    </div></div></div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                    
                    
                    
                    
                   <?php /*?><div class="span4">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-green long-dash-half">
                        <a href="<?php echo base_url();?>planningmanager/completed_plan" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-download"></i><br><br><br>
                               Retrieve & Review                            </span>                        </a>                    </div></div></div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div><?php */?>
                 
                 <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-green long-dash-half">
                        <a href="<?php echo base_url();?>planningmanager/edit_lesson_Plan_library" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-edit"></i><br><br><br>
                               Edit Lesson Plan Library                            </span>                        </a>                    </div></div></div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                 
                 <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-green long-dash-half">
<!--                        <a href="<?php echo base_url();?>planningmanager/completed_plan" class="text-center" data-original-title="">-->
                        <a href="<?php echo base_url();?>planningmanager/retrieve_lesson_Plan_library" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-download"></i><br><br><br>
                               Retrieve & Review                            </span>                        </a>                    </div></div></div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                 
                 <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-green long-dash-half">
                        <a href="<?php echo base_url();?>planningmanager/lesson_plan_graph" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-download"></i><br><br><br>
                               Lesson Planning Graph                            </span>                        </a>                    </div></div></div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                 
                              
                              </div></div>
                                <!-- END GRID PORTLET-->
                 
                 
             
            <!-- END ROW 1-->
  
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   </div>
   <!-- END CONTAINER -->
   
   <!-- BEGIN FOOTER -->
   
         <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->

   
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <script>
      	 $(document).ready( function() { 
        $('#success_team_form').submit();
		
      <!--  alert('<?php echo $this->session->flashdata('message');?>');-->
        
        <?php if ($this->session->flashdata('error')){?>
            $('#errmsg').html('<?php echo $this->session->flashdata('error');?>');
            $('#errorbtn').modal('show');
            <?php }?>
        <?php if ($this->session->flashdata('message')){?>
            $('#successbtn').modal('show');
            <?php }?>
		<?php if($this->session->flashdata('link')){?>
            window.open("<?php echo $this->session->flashdata('link');?>");
        <?php }?>	
      
    });

</script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>