<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
       <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                     <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          <a href="<?php echo base_url();?>planningmanager">Planning Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Creator</a>
                           <span class="divider">></span>
                       </li>
                       
                        <?php if(strtolower($type)!='edit'){?>
                                <li>
                                    </i> <a href="<?php echo base_url(); ?>planningmanager/lesson_plan">Create Lesson Plan</a>
                                    <span class="divider">></span>
                                </li>
                                <li>
                                    </i> <a href="<?php echo base_url(); ?>planningmanager/create_plan">Add New</a>

                                </li>
                                 <?php } else {?>
                                <li>
                                    </i> <a href="<?php echo base_url(); ?>planningmanager/edit_plan">Edit Plan</a>

                                </li>
                                 <?php }?>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <form class="form-horizontal" action="<?php echo base_url();?>planningmanager/create_plan_step1" name="createplanstep1" id="createplanstep1" method="post">
                         <input type="hidden" name="plan_type" value="<?php echo $type;?>" />
                     <div class="widget green">
                         <div class="widget-title">
                             <h4><?php echo $type;?> Lesson Plan</h4>
                          
                         </div>
                         <div class="widget-body">
<!--                            <form class="form-horizontal" action="#">-->
                                <div id="pills" class="custom-wizard-pills-green4">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab" id="pillsstep2">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab" id="pillsstep3">Step 3</a></li>
                                     <li><a href="#pills-tab4" data-toggle="tab" id="pillsstep4">Step 4</a></li>
                               
                                       
                                     
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                  
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                                   <div class="control-group">
                                        <?php if($this->session->userdata('login_type')=='user')
										{
										?>

                                             <label class="control-label">School Name</label>
                                             <div class="controls">
                                    
                             		
										<select class="span12 chzn-select" name="school_id" id="school_id"  style="width:300px;" onchange="school_change(this.value)" >
									<?php if($school!=false) { 
									foreach($school as $val)
									{
									?>
									<option value="<?php echo $val['school_id'];?>"  ><?php echo $val['school_name'];?></option>
									<?php } } else { ?>
									<option value="">No Schools Found</option>
									<?php } ?>
									</select>
											
                                             </div>
                                             <?php } ?>
                                         </div>
                                         	  <?php if($this->session->userdata('login_type')=='user')
										{
										?>
                                           <div class="control-group">
                                             <label class="control-label">Select Teacher</label>
                                             <div class="controls">
 
<select class="" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id" onchange="doit()">
                                        <option value=""></option>
                                        <?php if(!empty($teachers)) { 
                                        foreach($teachers as $val)
                                        {
                                        ?>
                                     <option value="<?php echo $val['teacher_id'];?>" ><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
                                        <?php } } ?>
                                        </select>
                                             </div>
                                         </div>
                                         <?php }?>
                                         
                                         
                                               <?php if($this->session->userdata('login_type')=='observer'){
											 ?>
                                         <div class="control-group">
                                  <label class="control-label">Select Teacher</label>
                                             <div class="controls">
                                      <select name="teacher_id" id="teacher_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                           <option value=""></option>
			   								 <?php if(!empty($teachers)) { 
											foreach($teachers as $val)
											{

											?>
			    						<option value="<?php echo $val['teacher_id'];?>" 
										<?php if(isset($teacher_id) && $teacher_id==$val['teacher_id']) {?> selected <?php } ?>
										><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
			    						<?php } } ?>
                                    </select>
                                    	
			                                 </div>
                                         </div> 
                                         <?php }?>
                                         
                                        
                                         <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="cdate" required></input>
                                            </div>
                                        </div>
                                     
                                         
                                         
                                         <div class="control-group">
                                            <label class="control-label">Select Time </label>

                                             <div class="controls" id="lesson_plans">
                     <!--                              <select name="ctime" id="lesson_plans" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                            
                                                </select>-->
                                               <div class="input-append bootstrap-timepicker" id="lesson_plans">
                                                   <input id="timepicker101" type="text" class="input-small timepicker101" name="ctime" value="08:00 AM">
                                                    
                                                    <span class="add-on"><i class="icon-time"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="control-group" id="subject_name_group" style="display:none;">
                                            <label class="control-label">Subject</label>

                                             <div class="controls" id="subject_name">
                                            </div>
                                        </div>
                                             
                                     </div>
                                     
                                  

                                     </div>
                                     </div>
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:void(0);" onClick="$('#createplanstep1').submit();">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
<!--      </div>
       END PAGE   
   </div>-->
<!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
    <!--start old script -->
   <script src="<?php echo SITEURLM.$view_path; ?>js/report.js" type="text/javascript"></script>
   
<script type="text/javascript">

function school_change(id)
{
	doit();
	var g_url='teacher/getTeachersBySchool/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="teacher_id" id="teacher_id" onchange="doit()" ><option value="">-Select-</option>';
	if(result.teacher!=false)
	{
	$.each(result.teacher, function(index, value) {
	str+='<option value="'+value['teacher_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Teachers Found</option></select>';
	  
	}
     $('#teacher_id').replaceWith(str); 
	 
	 
	 
	 
	 });
	 
	 var g_url='observer/getobserverByschoolId/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="observer_id" id="observer_id" ><option value="">-Select-</option>';
	if(result.observer!=false)
	{
	$.each(result.observer, function(index, value) {
	str+='<option value="'+value['observer_id']+'">'+value['observer_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Observers Found</option></select>';
	  
	}
     $('#observer_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
</script>
   <!--end old script -->
   
   <script>
   $(document).ready( function() { 
       
        $('#pillsstep2').click(function(){
           if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#timepicker1').val()==''){
               alert('Please enter date');
               return false;
           }
           $('#createplanstep1').submit();
       });
         $(document).ready( function() { 
        
//        alert('<?php echo $this->session->flashdata('message');?>');
        
        <?php if ($this->session->flashdata('error')){?>
            $('#errmsg').html('<?php echo $this->session->flashdata('error');?>');
            $('#errorbtn').modal('show');
            <?php }?>
        <?php if ($this->session->flashdata('message')){?>
            $('#successbtn').modal('show');
            <?php }?>
        <?php if($this->session->flashdata('link')){?>
            window.open("<?php echo $this->session->flashdata('link');?>");
        <?php }?>
    });
        $('#pillsstep3').click(function(){
           if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#timepicker1').val()==''){
               alert('Please enter date');
               return false;
           }
           $('#createplanstep1').submit();
       });
       
        $('#pillsstep3').click(function(){
        if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#timepicker1').val()==''){
               alert('Please enter date');
               return false;
           }   
        $('#createplanstep1').submit();
       });
       
        $("select").change(function(){
            var divId = $(this).find(":selected").attr("data-div-id");
            $(".standard-topic").hide();
            $("#" + divId).show();
			
        });
        
        <?php if($this->session->userdata('login_type')=='observer'){?>
        $('#dp1').on('changeDate',function(){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_lesson_plan",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val()},
              success: function(result)
              {
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
        });
        <?php }else { ?>
        $('#dp1').on('changeDate',function(){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_lesson_plan",
              data: "cdate="+$('#dp1').val(),
              success: function(result)
              {
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
        });
        <?php }?>
    
    $('#dp1').on('changeDate',function(){
//        console.log('in changeDate');
        if($('#dp1').val()!='' && $('#timepicker101').val()!=''){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_subject",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val(),starttime:$('#timepicker101').val()},
              success: function(result)
              {
                  console.log(result);
                  if(result!=''){
                      $('#subject_name').html('');
                      $('#subject_name_group').show();
                      $('#subject_name').html(result);
                  }
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
         }
        });
    
    
    });
    
    
    $(function(){
    $('.timepicker101').timepicker().change(function(){
        $('.timepicker101').val($(this).val());
        if($('#dp1').val()!='' && $('#timepicker101').val()!=''){
          
        $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_subject",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val(),starttime:$(this).val()},
              success: function(result)
              {
                  if(result!=''){
                      $('#subject_name').html(' ');
                      $('#subject_name_group').show();
                      $('#subject_name').html(result);
                  } else {
                      $('#subject_name').html(' ');
                      $('#subject_name_group').show();
                      $('#subject_name').html('Not Assigned');
                  }
              }
         });
         }
});

});
    
	</script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>