<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="system/application/views/inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 400px">
                    <b><img alt="pencil" width="12px"  src="system/application/views/inc/logo/pencil1.png"/>&nbsp;Lesson Plan</b></div>
		</div>
        </page_header> 
        
<page_footer> 
    
              <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
              <br />
              <div style="text-align:center;font-size:7px;color:#<?php echo $fontcolor;?>;">&copy; Copyright U.E.I.S. Corp. All Rights Reserved</div>
		
        </page_footer> 	
                
                <table cellspacing="0" style="width:700px;border:2px #939393 solid;">
                    <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                        <td colspan="3" style="padding-left:5px;padding-top:2px;height:36px;" >
                            <b> <?php echo ucfirst($this->session->userdata('district_name'));?>&nbsp;|&nbsp;<?php echo ucfirst($teachername[0]['school_name']);?></b></td><td style="text-align:right;padding-right:10px;"><b><?php echo date('F d, Y',strtotime($date));?></b></td>
<!--                            <br /><br />-->
                        
                   
                </tr>
                <tr >
                        <td colspan="4" >
                       &nbsp;
                        
                    </td>
                </tr>
                <tr style="margin-top:20px;">
		<td style="padding-left:10px;width:120px;color:#939393;">
		<b>Teacher:</b><?php echo $teachername[0]['firstname'].'<br /> '.$teachername[0]['lastname'];?>
		</td>
		<td style="padding-left:10px;width:180px;color:#939393; vertical-align: top;">
		<b>Period:</b><?php echo $start2.' to '.$end2;?>
		</td>
		<td style="padding-left:10px;width:180px;color:#939393; vertical-align: top;">
		<b>Subject:</b><?php echo $getlessonplans[0]['subject_name'];?>
		</td>
		<td style="width:180px;padding-left:10px;color:#939393; vertical-align: top;">
		<b>Grade:</b><?php echo $gradename[0]['grade_name'];?>
		</td>
		</tr>
		<tr>
		<td>
		<br />
		</td>
		</tr>
		<tr>
		<td style="padding-left:10px;color:#939393;width:120px; vertical-align: top;">
		<b>Class Size:</b>
		<?php echo $total_students;?></td>
                <?php $cntr = 0;?>
                <?php foreach($lessonplans as $val):?>
                <td style="width:180px;padding-left:10px;color:#939393; vertical-align: top;">
		<b><?php echo $val['tab'];?>:</b>
		<?php echo $getlessonplans[$cntr]['subtab'];?></td>
                <?php $cntr++;?>
                <?php endforeach;?>
                </tr>
                <tr >
                        <td colspan="4" >
                       &nbsp;
                        
                    </td>
                </tr>
		</table>
		<?php if (!curriculum):?>
		<table style="border:2px #7FA54E solid;">
		<tr>
                    <td style="width:718px;color:#939393;font-size:12px;">
                        Standard:<?php echo $getlessonplans[0]['standard'];?>
                    </td>
                </tr>
                </table>
                <?php endif;?>
		<br />
                <br /> 
                <table  cellspacing='0' cellpadding='0' border='0' id='conf'   >
                    <?php foreach($observationplan as $plan):?>
                    <tr style="background:#FFF;color:#000;" >
                        <td style="padding-left:10px;padding-top:5px;padding-bottom:5px;border:2px solid #939393;">
                            <div style="width:736px;font-size:16px;">
		<b><?php echo $plan['tab'];?></b>
        </div>
		</td>
		</tr>
                
                <tr style="color:#000;border:1px #3f3f3f solid;">
		<td style="border:1px #939393 solid;padding-left:10px;">
                    <!--<div  style="font-size:12px;border-left:1px #5e3364 solid; border-right:1px #5e3364 solid; border-bottom:1px #5e3364 solid;padding:5px; color:#000;">-->
                    <div style="width:736px;">
                    <br />
                <?php if(isset($plan['answer'])){
                    echo $plan['answer'];
                }?>
                    <br /><br />
                    
                    </div>
		</td></tr>
                <tr><td>&nbsp;</td></tr>
                <?php endforeach;?>
                </table>	
                
   </page> 
