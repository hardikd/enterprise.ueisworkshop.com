<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                <th class="sorting">Smart Goal Name</th>
                                <th class="sorting">Subject</th>
                                <th class="sorting">Grade</th>
                                <th class="no-sorting">Goal Date</th>
                                <th class="no-sorting">PDF</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($smartgoals as $name_id=>$smartgoal):?>
                                <tr class="odd gradeX">
                                    <td><?php echo $smartgoal['smart_goal_name'];?></td>
                                    <td><?php echo $smartgoal['subject'];?></td>
                                    <td><?php echo $smartgoal['grade'];?></td>
                                    <td class="hidden-phone"><?php echo $smartgoal['goal_date'];?></td>
                                    <td class="hidden-phone">
                                        <button role="button" class="btn btn-primary generatepdf" id="<?php echo $name_id;?>" data-toggle="modal"><i class="icon-print"></i>PDF</button>
                                               </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
<script>
    $('.generatepdf').click(function(){
//            alert('test');
//alert($('#subject_id').val());
             <?php if($type!='goal_by_year'):?>
                 
             
                window.open('<?php echo base_url();?>planningmanager/generate_pdf_goal/'+$('#subject').val()+'/'+$('#grade').val()+'/'+$('#teacher_id').val()+'/'+$('#school_id').val()+'/'+$.trim($('#dpYears').val())+'/'+$(this).attr('id'));
                <?php else:?>
                window.open('<?php echo base_url();?>planningmanager/generate_pdf_goal_year/'+$('#teacher_id').val()+'/'+$('#school_id').val()+'/'+$('#dpYears').val()+'/'+$(this).attr('id'));
                <?php endif;?>
            });
    </script>

