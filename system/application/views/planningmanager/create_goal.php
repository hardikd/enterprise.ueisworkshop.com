<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          <a href="<?php echo base_url();?>planningmanager/">Planning Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/smart_goals">SMART Goals Creator</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/create_goal">Create SMART Goal</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Create SMART Goal</h4>
                          
                         </div>
                         <div class="widget-body">
                             <form class="form-horizontal" action="<?php echo base_url();?>planningmanager/savegoals" method="post" enctype="multipart/form-data">
                                <div id="pills" class="custom-wizard-pills-green3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <?php if($this->session->userdata('login_type')=='observer'):?>
                                         <div class="control-group">
                                            <label class="control-label">Select Teacher</label>
                                            <div class="controls">
                                                <select name="teacher_id" id="teacher_id" data-placeholder="--Please Select--" class="chzn-select span12" style="width: 300px;">
                                                    <option></option>
                                                    <?php foreach($teachers as $teacher):?>
                                                    <option value="<?php echo $teacher['teacher_id'];?>"><?php echo $teacher['firstname'].' '.$teacher['lastname'];?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php endif;?>
                                         <div class="control-group">
                                            <label class="control-label">Goal Name</label>
                                            <div class="controls">
                                                <input type="text" value="" size="16" class="m-ctrl-medium" name="goalname" id="goalname" />
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="goaldate" ></input>
                                            </div>
                                        </div>
                                        
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
                                                 <select data-placeholder="--Please Select--" class="chzn-select span12" tabindex="6" style="width: 300px;" name="grade" id="grade">
                                             <option value=""></option>
<!--                                        <optgroup label="">-->
                                            <?php foreach($grades as $gradesval){?>
                                            <option value="<?php echo $gradesval['grade_id'];?>"><?php echo $gradesval['grade_name'];?></option>
                                            <?php }?>
                                            
<!--                                        </optgroup>-->
                                        
                                        </select>
                                             </div>
                                         </div>
                                         <div class="control-group">
                                             <label class="control-label">Select Subject/Topic</label>
                                             <div class="controls" id="subtopic">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        
                                    </select>
<!--                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option value="Math">Math</option>
                                        <option value="English">English</option>
                                        <option value="Science">Science</option>
                                        <option value="History">History</option>
                                    </select>-->
                                             </div>
                                         </div>
                                         
                                     </div>
                                     
                                     
                                     
                                      <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000">STEP 3</h3>
                                         <div class="control-group">
                                         
                                         
                                           <div class="span12">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                        <div class="widget green">
                            <div class="widget-title">
                                <h4></i>Write SMART Goal</h4>
                                
                                            
                            </div>
                            <div class="widget-body">
                            <?php foreach($goalplans as $val){?>
                    <div class="control-group">
                                <label class="control-label"><?php echo $val['tab'];?></label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3" name="plan_<?php echo $val['goal_plan_id'];?>"></textarea>
                                </div>
                                
                                <label class="control-label"><?php echo $val['tab'];?> File</label>
                                <div class="controls">
                                    <input type="file" name="file_<?php echo $val['goal_plan_id'];?>" id="file_<?php echo $val['goal_plan_id'];?>" />
                                </div>
                            </div>     
                                
                            <?php }?>
                                <center><button class="btn btn-large btn-success" type="submit"><i class="icon-save icon-white"></i> Save/Submit GOALS</button>  <button class="btn btn-large btn-success"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                            </div>
                        </div>
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                    
                                         
                                         
                                         
                                         
                                       
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   
   <script>
       $(document).ready( function() { 
        
//        alert('<?php echo $this->session->flashdata('message');?>');
        
        <?php if ($this->session->flashdata('error')){?>
            $('#errorbtn').modal('show');
            <?php }?>
        <?php if ($this->session->flashdata('message')){?>
            $('#successbtn').modal('show');
            <?php }?>
		<?php if($this->session->flashdata('link')){?>
            window.open("<?php echo $this->session->flashdata('link');?>");
        <?php }?>	
    });
    
   $(".chzn-select").trigger("chosen:updated");
//            alert('chec');
            $('#grade').on('change',function(){
                
                       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>planningmanager/getgradesubjectsmart",
        data: { gradeid: $('#grade').val() }
      })
        .done(function( msg ) {
          $('#subtopic').html(msg);
            jQuery("#subject").chosen();
        });
//                alert($('#grade').val());
                
            });
   </script>
   
   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       
            
    
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
       
//$(".chzn-select").change(function(){
//       $.ajax({
//        type: "POST",
//        url: "<?php echo bae_url();?>planningmanager/getgradesubject",
//        data: { gradeid: $('#grade').val() }
//      })
//        .done(function( msg ) {
//          alert( "Data Saved: " + msg );
//        });
//});
//
//
//
//$(function() {
//
//$(".chzn-select").chosen();
//$(".chzn-select-deselect").chosen({allow_single_deselect:true});
//
//$('.chzn-choices input').autocomplete({
//   source: function( request, response ) {
//      $.ajax({
//          url: "/change/name/autocomplete/"+request.term+"/",
//          dataType: "json",
//          success: function( data ) {
//             response( $.map( data, function( item ) {
//                $('ul.chzn-results').append('<li class="active-result">' + item.name + '</li>');
//
//          }
//       });
//    }
//});

$("#grade").ajaxChosen({
    type: 'GET',
    url: '/ajax-chosen/data.php',
    dataType: 'json'
}, function (data) {
    var results = [];

    $.each(data, function (i, val) {
        results.push({ value: val.value, text: val.text });
    });

    return results;
});

   </script>  
</body>
<!-- END BODY -->
</html>