<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          <a href="<?php echo base_url();?>planningmanager/">Planning Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/smart_goals">SMART Goals Creator</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/edit_goal">Edit SMART Goal in Progress</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Edit SMART Goal in Progress</h4>
                          
                         </div>
                         <div class="widget-body">
                             <form class="form-horizontal" action="<?php echo base_url();?>planningmanager/savegoals" method="post">
                                <div id="pills" class="custom-wizard-pills-green3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     
                                     
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                        
                                         
<!--                                <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium"></input>
                                    </div>
                                </div>-->
                                <?php if($this->session->userdata('login_type')=='observer'):?>
                                        <div class="control-group">
                                   <label class="control-label">Select Teacher</label>
                                    <div class="controls">
                                         <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id">
                                        <option value=""></option>
                                        <?php foreach($teachers as $teacher){?>
                                            <option value="<?php echo $teacher[teacher_id];?>"><?php echo $teacher['firstname'].' '.$teacher['lastname'];?></option>
                                        <?php }?>
                                        
                                    </select>
<!--                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="smartgoalname" id="smartgoalname">
                                        <option value=""></option>
                                        <?php foreach($smartgoal as $smartgoal){?>
                                            <option value="<?php echo $smartgoal->name_id;?>"><?php echo $smartgoal->smart_goal_name;?></option>
                                        <?php }?>
                                        
                                    </select>-->
                                    </div>
                                </div>

<div class="control-group">
                                   <label class="control-label">Select Goat Name</label>
                                    <div class="controls">
                                    <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="smartgoalname" id="smartgoalname">
                                        <option value=""></option>
                                        
                                        
                                    </select>
                                    </div>
                                </div>
                                         <?php else:?>
<div class="control-group">
                                   <label class="control-label">Select Goal</label>
                                    <div class="controls">
                                         
                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="smartgoalname" id="smartgoalname">
                                        <option value=""></option>
                                        <?php foreach($smartgoal as $smartgoal){?>
                                            <option value="<?php echo $smartgoal->name_id;?>"><?php echo $smartgoal->smart_goal_name;?></option>
                                        <?php }?>
                                        
                                    </select>
                                    </div>
                                </div>
<?php endif;?>
                                        
                                     </div>
                                     
                                  <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                         <div class="control-group">
                                                <label class="control-label">Select Date</label>
                                                <div class="controls">
                                                    <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="goaldate"></input>
                                                </div>
                                            </div>
                                           <div class="control-group">
                                             <label class="control-label">Edit Grade</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" tabindex="1" style="width: 300px;" name="grade" id="grade">
                                        <option value=""></option>
                                       <?php foreach($grades as $gradesval){?>
                                            <option value="<?php echo $gradesval['grade_id'];?>"><?php echo $gradesval['grade_name'];?></option>
                                            <?php }?>
                                    </select>
                                             </div>
                                         </div>
                                         
                                         
                                         <div class="control-group">
                                             <label class="control-label">Edit Subject/Topic</label>
                                             <div class="controls" id="subtopic">
                                                 <select class="span12 chzn-select" data-placeholder="English" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        
                                    </select>
                                             </div>
                                         </div>
                                         
                                        
                                     </div>
                                     
                                     
                                     
                                     
                                     
                                      <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000">STEP 3</h3>
                                         <div class="control-group">
                                         
                                         
                                           <div class="span12">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                        <div class="widget green">
                            <div class="widget-title">
                                <h4></i>Write SMART Goal</h4>
                                
                                            
                            </div>
                            <div class="widget-body">
                            
<!--                    <div class="control-group">
                                <label class="control-label">Goal(s)</label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquam in neque id mollis. Vivamus vel scelerisque neque. Nulla vitae iaculis libero, quis fermentum nisi.</textarea>
                                </div>
                            </div>     -->
                            
                                <?php foreach($goalplans as $val){?>
                    <div class="control-group">
                                <label class="control-label"><?php echo $val['tab'];?></label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3" name="plan_<?php echo $val['goal_plan_id'];?>" id="plan_<?php echo $val['goal_plan_id'];?>"></textarea>
                                </div>
                                
                                <label class="control-label"><?php echo $val['tab'];?> File</label>
                                <div class="controls">
                                    <input type="file" name="file_<?php echo $val['goal_plan_id'];?>" id="file_<?php echo $val['goal_plan_id'];?>" />
                                    <img src="" id="img_<?php echo $val['goal_plan_id'];?>" style="display:none;"  />
                                </div>
                            </div>     
                                
                            <?php }?>
                                
<!--                             <div class="control-group">
                                <label class="control-label">Indicator(s)</label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquam in neque id mollis. Vivamus vel scelerisque neque. Nulla vitae iaculis libero, quis fermentum nisi.</textarea>
                                </div>
                            </div>     
                            
                             <div class="control-group">
                                <label class="control-label">Measure(s)</label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquam in neque id mollis. Vivamus vel scelerisque neque. Nulla vitae iaculis libero, quis fermentum nisi.</textarea>
                                </div>
                            </div>    
                            
                             <div class="control-group">
                                <label class="control-label">Target(s)</label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquam in neque id mollis. Vivamus vel scelerisque neque. Nulla vitae iaculis libero, quis fermentum nisi.</textarea>
                                </div>
                            </div>   
                            
                             <div class="control-group">
                                <label class="control-label">Method(s)</label>
                                <div class="controls">
                                    <textarea class="span12 " rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquam in neque id mollis. Vivamus vel scelerisque neque. Nulla vitae iaculis libero, quis fermentum nisi.</textarea>
                                </div>
                            </div>  -->
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
<center><button class="btn btn-large btn-success" type="submit"><i class="icon-save icon-white"></i> Save/Submit GOALS</button>  <button class="btn btn-large btn-success"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                            </div>
                        </div>
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                    
                                         
                                         
                                         
                                         
                                       
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;" onclick="">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
var base_url = '<?php echo base_url();?>';
$('#teacher_id').on('change',function(){
    $.ajax({
        type: "POST",
        url: base_url+"planningmanager/getsmargoaldetails_teacher",
        data: { teacher_id: $(this).val() }
      })
      .done(function( msg ) {
          result = jQuery.parseJSON(msg);
           $.each(result, function(i, item) {
               console.log(item);
           $('#smartgoalname').append('<option value="'+item.name_id+'">'+item.smart_goal_name+'</option>');
       });
          $('#smartgoalname').trigger("liszt:updated");
      });
});
$('#smartgoalname').on('change',function(){
    
    $.ajax({
        type: "POST",
        url: base_url+"planningmanager/getsmargoaldetails",
        data: { smartgoalname: $(this).val() }
      })
        .done(function( msg ) {
            result = jQuery.parseJSON(msg);
            var goaldate = result[0].goal_date;
//            alert(result[0].goal_date);
            var datearr = goaldate.split('-');
            $('#dp1').val(datearr[1]+'-'+datearr[2]+'-'+datearr[0]);
            $("#grade").val(result[0].grade_id).trigger("liszt:updated");
            
            $.ajax({
            type: "POST",
            url: base_url+"planningmanager/getgradesubjectsmart",
            data: { gradeid: result[0].grade_id }
          }).done(function( msg ) {
            $('#subtopic').html(msg);
            $('#subject').addClass('chzn-select');
            jQuery("#subject").chosen();
             $("#subject").val(result[0].subject_id).trigger("liszt:updated");
          });
         
//             $("#chosen-select").val(value).trigger("chosen:updated");
       $.each(result, function(i, item) {
           $('#plan_'+result[i].goal_plan_id).val(result[i].description);
           if(result[i].file_path!='NoFile'){
               var file = result[i].file_path
               var filename = file.substr( 0,file.lastIndexOf('.'));
//               alert(extension);
               $('#img_'+result[i].goal_plan_id).attr('src','<?php echo $_SERVER['HTTP_HOST'];?>workshop_files/goalmediathumb/'+filename+'.jpeg');
           }
//           alert(result[i].goal_plan_id);
       });
            console.log(result);
//                alert( "Data Saved: " + msg );
        });
});

   </script>  
</body>
<!-- END BODY -->
</html>