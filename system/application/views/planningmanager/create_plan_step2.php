<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<script>base_url = '<?php echo base_url(); ?>';</script>
<style>
.infoicon {  position: absolute;
  margin-left: 155px;
  margin-top: 10px;
width:60%;}
#infoicondetails p {padding-left:10px;}
#inst_strat {border:1px solid;}
#inst_strat h3 {text-align: center;
    background-color: #CCC;
    margin-top: 0px;}

#res_int {border:1px solid;display:none;}
#res_int h3 {text-align: center;
    background-color: #CCC;
    margin-top: 0px;}    

</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path . 'inc/header.php'); ?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
      <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid"> 
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          
          <h3 class="page-title"> <i class="icon-book"></i>&nbsp; Planning Manager </h3>
          <ul class="breadcrumb" >
            <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
            <li> <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url(); ?>planningmanager">Planning Manager</a> <span class="divider">></span> </li>
            <li> </i> <a href="<?php echo base_url(); ?>planningmanager/lesson_plan_creator">Lesson Plan Creator</a> <span class="divider">></span> </li>
            <?php if(strtolower($type)!='edit'){?>
            <li> </i> <a href="<?php echo base_url(); ?>planningmanager/lesson_plan">Create Lesson Plan</a> <span class="divider">></span> </li>
            <li> </i> <a href="<?php echo base_url(); ?>planningmanager/create_plan">Add New</a> </li>
            <?php } else {?>
            <li> </i> <a href="<?php echo base_url(); ?>planningmanager/edit_plan">Edit Plan</a> </li>
            <?php }?>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <form class="form-horizontal" action="<?php echo base_url(); ?>planningmanager/update_plan" name="createplanstep2" id="createplanstep2" method="post">
        <div class="row-fluid">
          <div class="span12"> 
            <!-- BEGIN BLANK PAGE PORTLET-->
            
            <input type="hidden" name="grade" id="grade" value='<?php echo $grades[0]['grade_id']; ?>'>
            <input type="hidden" name="standard_id" id="standard_id" value='<?php echo $grades[0]['standard_id']; ?>'>
            <input type="hidden" name="standarddata_id" id="standarddata_id" value="">
            <input type="hidden" name="subject_id" id="subject_id" value="<?php echo $subject_id; ?>">
            <input type="hidden" name="period_id" id="period_id" value="<?php echo $period_id; ?>">
            <input type="hidden" name="date" id="date" value="<?php echo $cdate; ?>">
            <input type="hidden" name="lesson_week_plan_id" id="lesson_week_plan_id" value="<?php echo $lesson_week_plan_id; ?>">
            <input type="hidden" name="start" id="start" value="<?php echo $starttime; ?>">
            <input type="hidden" name="end" id="end" value="<?php echo $endtime; ?>">
            <input type="hidden" name="plan_type" value="<?php echo $type;?>" />
            <?php if(isset($teacher_id)){?>
            <input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $teacher_id; ?>">
            <?php } else{?>
            <input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $this->session->userdata('teacher_id'); ?>">
            <?php }?>
            <div class="widget green">
              <div class="widget-title">
                <h4><?php echo $type;?> Lesson Plan</h4>
              </div>
              <div class="widget-body">
                <div id="pills" class="custom-wizard-pills-green4">
                  <ul>
                    <li><a href="#pills-tab1" data-toggle="tab" id="pillstab1">Step 1</a></li>
                    <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                    <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                    <li><a href="#pills-tab4" data-toggle="tab" id="pillsstep4">Step 4</a></li>
                  </ul>
                  <div class="progress progress-success progress-striped active">
                    <div class="bar"></div>
                  </div>
                  <div class="tab-content"> 
                    
                    <!-- BEGIN STEP 2-->
                    <div class="tab-pane" id="pills-tab2">
                      <h3 style="color:#000000;">STEP 2</h3>
                      <?php if($curriculum!='materials' && $curriculum!='textbook' && $curriculum!='othermaterials'):?>
                      <div class="control-group">
                        <label class="control-label">Common Core Domain</label>
                        <div class="controls">
                          <select class="span12 chzn-select" name="strand" id="commoncorestrand" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" onchange="selectstandard(this.value, 0, 0);" required>
                            <option value="0">Please select</option>
                            <?php foreach ($strands as $val) { ?>
                            <option value="<?php print_r($val['strand']); ?>"><?php print_r($val['strand']); ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Common Core Cluster</label>
                        <div class="controls" id="standards">
                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" required>
                            <option value=""></option>
                            <!--                                        <option>Common Core Element Description 1</option>
                                                                                                        <option>Common Core Element Description 2</option>
                                                                                                        <option>Common Core Element Description 3</option>-->
                          </select>
                        </div>
                      </div>
                      <div class="space20"></div>
                      <div class="control-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="controls">
                          <table class="table table-striped table-bordered" style="width: 50%;">
                        <tr>
                          <td><p style="font-size: 14px;"><img src="<?php echo SITEURLM ?>img/file-search/pdf.png"> &nbsp;&nbsp; Math PDF</p></td>
                          <td style="padding-left: 10px;"><p style="font-size: 14px;"><img src="<?php echo SITEURLM ?>img/file-search/pdf.png"> &nbsp;&nbsp;ELA PDF</p></td>
                        </tr>
                      </table>
                        </div>
                      </div>
                      
                      <div class="space20"></div>
                      <div class="space20"></div>
                      <div class="control-group">
                        <label class="control-label">Element Description</label>
                        <div class="controls">
                          <textarea class="span12 " rows="5" name="standarddata" id="standardeledesc" required></textarea>
                        </div>
                      </div>
                      <?php else:?>
                      <div class="control-group">
                           
                          <fieldset>
                            <legend>Textbook Library:<?php 
                            if (empty($materials)):?>
                                No Textbook or materials are uploaded for this subject.
                                <?php endif;?></legend>
                            
                            <div class="row-fluid">
                          <div class="span12">
                              <?php 
                              $cntr = 0;
                              foreach($materials as $material):
                               $filename=explode('.',$material['file_path']);
				$file=$filename[1];
                                if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
                                {
                                  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'medialibrary/lesson_plan/'.$material['file_path'].' height="75px" width="100px">';
                                } else {
                                      if (file_exists(WORKSHOP_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg')) {
                                                    $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg >';
                                            } else {
                                                    if($file=='pdf')
                                                    {
                                                     $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'medialibrary/lesson_plan/'.$material['file_path'].'[0] '.WORKSHOP_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg';

                                                    exec($commandtext);
                                                    }

                                                    $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg >';
                                            }



                                    }
                                    $name=$material['name'];
                                    $subject_name=$material['subject_name'];
                                    $grade_name=$material['grade_name'];
                                    $text='<br /><b>Name:</b>&nbsp;'.$name.'.pdf';
                                    $text.='<br /><b>Subject:</b>&nbsp;'.$subject_name;
                                    $text.='<br /><b>Grade:</b>&nbsp;'.$grade_name.'<br />';
                                  ?>
                            <div class="span4 offset1 material_css" id="<?php echo $material['lesson_plan_material_id'];?>">
                                <center>
                                <a href="javascript:;"  onclick="selectmaterial('<?php echo $material['lesson_plan_material_id'];?>','<?php echo $material['file_path'];?>');" ondblclick="openmaterial('<?php echo WORKSHOP_DISPLAY_FILES.'medialibrary/lesson_plan/'.$material['file_path'];?>');">
                                   <?php echo $fileas;?>
                                </a>
                                <br>
                                <?php echo $text;?>
                                </center>
                            </div>
                              <?php if($cntr%2==1):?>
                                    
                                    </div>
                              </div>
                            <div class="space20"></div>
                            <div class="row-fluid">
                          <div class="span12">
                              <?php endif;?>
                             <?php 
                             $cntr++;
                             endforeach;?> 
                            </div>
                              </div>
                              
                          </fieldset>
                              
                      </div>
                      <?php endif;?>
                    </div>
                    
                    <!-- BEGIN STEP 3-->
                    <div class="tab-pane" id="pills-tab3">
                      <h3 style="color:#000000;">STEP 3</h3>
                      <div class="span6" id="lesson_plan_template"> 
                      <?php if($curriculum!='materials' && $curriculum!='othermaterials' && $curriculum!='textbook'):?>
                      <div class="control-group">
                        <label class="control-label">Common Core Cluster</label>
                        <div class="controls">
                          <textarea class="span12 " rows="3" name="standarddisplay" id="standarddisplay" required></textarea>
                        </div>
                      </div>
                      <?php else:?>
                      <div class="control-group" style="display:none;">
                        <label class="control-label">Text Book</label>
                        <div class="controls">
                            <input type="hidden" name="selectedtextbook" value="" id="selectedtextbook">
                            <input type="hidden" name="selectedmaterial" value="" id="selectedmaterial">
                            <div id="textbookDisplay"></div>
                        </div>
                      </div>
                      <?php endif;?>

                      <?php foreach ($lessontype as $lessontypekey => $lessontypeval) { ?>
                      <div class="control-group">
                        <label class="control-label" style="width:185px;"><?php echo $lessontypekey ?>&nbsp;<?php if($lessontypeval['lesson_plan_id']!=2):?><a href="javascript:;" <?php if($lessontypeval['lesson_plan_id']==7): ?> id="responseinfo" <?php else:?> id="instructioninfo"<?php endif;?>><i class="icon-info-sign"></i></a><?php endif;?></label>
                        <div class="controls">
                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="<?php echo $lessontypeval['lesson_plan_id']; ?>" id="<?php echo $lessontypeval['lesson_plan_id']; ?>" <?php if($lessontypeval['lesson_plan_id']!=7): ?>required<?php endif;?>>
                            <option value=""></option>
                            <?php
                                                                    foreach ($lessontypeval as $lssntypekey => $lssntypeval) {
                                                                        if ($lssntypekey != 'lesson_plan_id') {
                                                                            ?>
                            <option value="<?php echo $lssntypekey; ?>"><?php echo $lssntypeval; ?></option>
                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                          </select><?php if($lessontypeval['lesson_plan_id']==7): ?><span style="vertical-align:top;line-height:35px;">(Optional)</span><?php endif;?>
                        </div>
                      </div>
                      <?php } ?>
                      

                      
                                                    <div class="control-group" style="display:none;">
                        <label class="control-label">Differentiated Instruction</label>
                        <div class="controls">
                          <textarea class="span12 ckeditor" id='diff_instruction' name='diff_instruction' rows="6" require></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="span5" id="infoicondetails"> 
                     <div id="inst_strat" style="display:none;">
                        <h3>INSTRUCTIONAL STRATEGIES</h3>

                        <p><b>Whole Group:</b> The instructor teaches the entire group of students the same lesson 
                        regardless of where any particular student is.
                      </p>
                      <p>
                        <b>Think-Pair-Share:</b> Students work together to solve a problem or answer a question about 
                        an assigned reading.
                        </p>
                      <p>
                        <b>Small Group:</b> The instructor works with a?small group?of students on a specific learning 
                        objective.
                        </p>
                      <p>
                        <b>Intervention:</b> A program designed to help a student improve in an area of need.
                        </p>
                      <p>
                        <b>Kinesthetic:</b> Students carry out physical activity, rather than listening to a lecture.
                        </p>
                      <p>
                        <b>Visual:</b> The instructor introduces ideas, concepts, data and other information that is 
                        associated with images.
                        </p>
                      <p>
                        <b>Auditory:</b> The student learns through listening.
                        </p>
                      <p>
                        <b>Independent:</b> The student develops competencies as an individual with the help and/or 
                        supervision of others.
                        </p>
                      <p>
                        <b>Peer Learning:</b> Students?learn?with and from each other.
                      </p>
                    </div>  

                    <div id="res_int">
                        <h3>RESPONSE TO INTERVENTION</h3>

                        <p>Response to Intervention (RTI) is not a specific program or type of teaching. It is a 

                        proactive approach to measuring student skills and using the information to decide 

                        which types of targeted teaching to use.

                        There is no single way of doing RTI, but it is often set up as a?three-tier system of support. 

                        One way to understand this tiered system is to think of it as a pyramid, with the intensity of 

                        support increasing from one level to the next.
                      </p>
                      <p>

                        <b>Tier 1-Whole Group/Class:</b> During the intervention, the RTI team monitors student progress 

                        to see who might need additional support.?
                      </p>
                      <p>
                        <b>Tier 2-Small Group Interventions:</b> If a child is not making adequate progress in Tier 1, he 

                        may receive more targeted help. This is?in addition?to regular instruction, not a replacement 

                        for it. Tier 2 interventions should take place a few times a week during electives or enrichment 

                        activities such as music or art so that the student does miss any core instruction.
                      </p>
                      <p>
                        <b>Tier 3-Intensive Interventions:</b> Typically, only a small percentage of children will require 
                        Tier 3 support. If a child needs Tier 3 support, instruction will be tailored to his needs. 

                        Every day the student will receive one-on-one instruction or work in very small groups that 

                        may include students who are receiving special education?services and who need to 

                        work on the same skills.
                      </p>
                    </div>  
                    </div>
                    </div>
                    
                    <!-- BEGIN STEP 4-->
                    <div class="tab-pane" id="pills-tab4">
                       
                       
                      <h3 style="color:#000000;">
                          
                          
                          <?php if($curriculum=='materials' || $curriculum=='othermaterials' || $curriculum=='textbook'):?>
                          <span style="float:right;" id="view_text_btn" style="display:none;">
                               <a class="btn btn-medium btn-green" id="view_textbook" style="display:none;">View Textbook</a>
                        </span>
                           <?php endif;?>
                          STEP 4
                          
                      </h3>
                        
                      <div class="control-group">
                          <?php if($curriculum=='materials' || $curriculum=='othermaterials' || $curriculum=='textbook'):?>
                          <div class="span12" id="lesson_plan_template" > 
                            <?php else:?>
                                <div class="span12" id="lesson_plan_template">
                            <?php endif;?>
                          <!-- BEGIN GRID SAMPLE PORTLET-->
                          <div class="widget green">
                            <div class="widget-title">
                              <h4></i>Lesson Plan Template</h4>
                            </div>
                              <!--<div class="span12">--> 
                            <div class="widget-body">
                                  
                             <?php if($curriculum!='materials' && $curriculum!='othermaterials' && $curriculum!='textbook'):?>
                              <div class="control-group">
                                <label class="control-label">Domain</label>
                                <div class="controls">
                                  <input class="span12" type="text" placeholder="Selected Standard Here" disabled id="comdomain" />
                                </div>
                              </div>
                              <?php endif;?>
                                <div class="control-group">
                                <label class="control-label">Subject</label>
                                <div class="controls">
                                  <input class="span12" type="text" placeholder="Selected Subject" value="<?php echo $subject; ?>" disabled />
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label">Lesson Type</label>
                                <div class="controls">
                                  <input class="span12" type="text" placeholder="Selected Lesson Type" id="lesson_type" disabled />
                                </div>
                              </div>
                                <div class="control-group" style="display:none;">
                                <label class="control-label">Unit Name</label>
                                <div class="controls">
                                  <input class="span12" type="text" placeholder="Unit Here" disabled />
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label">Grade & Period</label>
                                <div class="controls">
                                  <input class="span12" type="text" placeholder="Grade Level | Period Here" value="<?php echo $grades[0]['grade']; ?> | <?php echo $period; ?>" disabled />
                                </div>
                              </div>
                              <?php
                                                                  if ($custom_diff != false) {
                                                                        foreach ($custom_diff as $custom_diffkey => $custom_diffval) {
                                                                            ?>
                              <div class="control-group">
                                <label class="control-label"><?php echo $custom_diffval['tab']; ?></label>
                                <div class="controls">
                                    <textarea class="span12" rows="3" name="c_<?php echo $custom_diffval['custom_differentiated_id']; ?>" required></textarea>
                                </div>
                              </div>
                              <?php
                                                                        }
                                                                    }
                                                                    ?>
                              <input type="hidden" name="btnaction" id="btnaction" value="submit">
                              <center>
                                <button class="btn btn-large btn-success" type="submit"><i class="icon-save icon-white"></i> Save Changes</button>
                                <a  role="button" href="javascript:void(0);" class="btn btn-large btn-success" onclick="submitfrm('print');" ><i class="icon-print icon-white"></i> Print</a>
                                <button class="btn btn-large btn-success" onclick="submitfrm('viewlessonplan');"><i class="icon-eye-open icon-white"></i> View Lesson Plan</button>
                              </center>
                            <!--</div>-->
                            </div>
                          </div>
                          <!-- END GRID SAMPLE PORTLET--> 
                        </div>
                            <?php if($curriculum=='materials' || $curriculum=='othermaterials' || $curriculum=='textbook'):?>
                              <div class="" id="textbook_template" style="display:none;"> 
                          <!-- BEGIN GRID SAMPLE PORTLET-->
                          <div class="widget green">
                            <div class="widget-title">
                              <h4>Text Book</h4>
                            </div>
                              <!--<div class="span12">--> 
                              <div class="widget-body" style="min-height:400px;">
                                  <div class="control-group" id="textbookview">
                                      
                                  </div>
                              </div>
                          </div>
                          </div>
                            <?php endif;?>
                        <center>
                        <a href="#myModal2" role="button" class="btn btn-large btn-success" data-toggle="modal"><font size="6px"><i class="icon-wrench icon-white"></i> Tool Kit</font><BR>
                        <font size="2px">(view prompts that will assist in developing lesson plans)</font></a>
                        <center>
                        <BR>
                        <BR>
                        <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                          <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="myModalLabel2"><i class="icon-wrench"></i> Tool Kit</h3>
                          </div>
                          <div class="modal-body">
                            <center>
                              <u><b>PROMPTS: COMMON CORE ALIGNED LESSON PREPARATION</B></u><BR>
                            </center>
                            <BR>
                            <p align="justify"><b>Relevance/Rationale:</b> Why are the outcomes of this lesson important in the real world? Why are these outcomes essential for future learning? <BR>
                              <BR>
                            
                            <p align="justify"><b>Required Materials:</b> What are text, digital resources and materials that will be used this lesson?<BR>
                              <BR>
                            
                            <p align="justify"><b>Activities/Tasks:</b> What learning experiences will students engage in? How will you use these learning experiences or their student products as formative assesment opportunities?<BR>
                              <BR>
                            
                            <p align="justify"><b>Access for All:</b> How will you ensure that all students have access to engage appropriately in this lesson? Consider all aspects of student diversity. 
                          </div>
                          <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-success">OK</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="pager wizard">
                    <li class="previous first green"><a href="javascript:;">First</a></li>
                    <li class="previous green"><a href="javascript:;">Previous</a></li>
                    <li class="next last green"><a href="javascript:;">Last</a></li>
                    <li class="next green"><a  href="javascript:;" onclick="$('#createplanstep1').submit();" >Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- END BLANK PAGE PORTLET--> 
          
        </div>
      
    </div>
    </form>
    <!-- END PAGE CONTENT--> 
    
  </div>
  <!-- END PAGE CONTAINER--> 
</div>
<!-- END PAGE --> 
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script> 
<script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script> 
<script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
<script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script> 
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
                                                                <script src="js/excanvas.js"></script>
                                                                <script src="js/respond.js"></script>
                                                                <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>assets/gritter/js/jquery.gritter.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>js/jquery.pulsate.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM ?>js/jquery.validate.js"></script> 

<!--common script for all pages--> 
<script src="<?php echo SITEURLM ?>js/common-scripts.js"></script> 
<!--script for this page--> 

<script src="<?php echo SITEURLM ?>js/form-component.js"></script> 
<script src="<?php echo SITEURLM ?><?php echo $view_path; ?>js/teacherplan.js"></script> 

<!-- END JAVASCRIPTS --> 

<script>

$('#responseinfo').on('click',function(){
  if($(this).hasClass("clicked")) {
        $(this).removeClass("clicked");
        $('#res_int').fadeOut('slow');      
    } else {
        $('#res_int').fadeIn('slow');
        $(this).addClass("clicked");
    }
  
});
$('#responseinfo').on('hover',function(){
  if($(this).hasClass("clicked")) {
        $(this).removeClass("clicked");
        $('#res_int').fadeOut('slow');      
    } else {
        $('#res_int').fadeIn('slow');
        $(this).addClass("clicked");
    }
  
});

$('#instructioninfo').on('click',function(){
  if($(this).hasClass("clicked")) {
        $(this).removeClass("clicked");
        $('#inst_strat').fadeOut('slow');      
    } else {
        $('#inst_strat').fadeIn('slow');
        $(this).addClass("clicked");
    }
  
});
$('#instructioninfo').on('hover',function(){
  if($(this).hasClass("clicked")) {
        $(this).removeClass("clicked");
        $('#inst_strat').fadeOut('slow');      
    } else {
        $('#inst_strat').fadeIn('slow');
        $(this).addClass("clicked");
    }
  
});
                                                                        var Script = function() {


                                                                            $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
                                                                                    console.log('onShow1');
                                                                                }, onNext: function(tab, navigation, index) {

                                                                                    console.log('onNext');
                                                                                }, onPrevious: function(tab, navigation, index) {
                                                                                    console.log('onPrevious');
                                                                                }, onLast: function(tab, navigation, index) {
                                                                                    console.log('onLast');
                                                                                }, onTabShow: function(tab, navigation, index) {
                                                                                    //        console.log(tab);
                                                                                    //        console.log(navigation);
                                                                                    //        console.log(index);
                                                                                    //        console.log('onTabShow1');
                                                                                    var $total = navigation.find('li').length;
                                                                                    var $current = index + 1;
                                                                                    console.log($current);
                                                                                    if ($current == 4) {
                                                                                        $('#createplanstep1').submit();
                                                                                    }
                                                                                    var $percent = ($current / $total) * 100;
                                                                                    $('#pills').find('.bar').css({width: $percent + '%'});
                                                                                }});

                                                                            $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
                                                                                    console.log('onShow2');
                                                                                }, onNext: function(tab, navigation, index) {
                                                                                    console.log('onNext');
                                                                                }, onPrevious: function(tab, navigation, index) {
                                                                                    console.log('onPrevious');
                                                                                }, onLast: function(tab, navigation, index) {
                                                                                    console.log('onLast');
                                                                                }, onTabClick: function(tab, navigation, index) {
                                                                                    console.log('onTabClick');

                                                                                }, onTabShow: function(tab, navigation, index) {
                                                                                    console.log('onTabShow2');
                                                                                    var $total = navigation.find('li').length;
                                                                                    var $current = index + 1;
                                                                                    var $percent = ($current / $total) * 100;
                                                                                    $('#tabsleft').find('.bar').css({width: $percent + '%'});

                                                                                    // If it's the last tab then hide the last button and show the finish instead
                                                                                    if ($current >= $total) {
                                                                                        $('#tabsleft').find('.pager .next').hide();
                                                                                        $('#tabsleft').find('.pager .finish').show();
                                                                                        $('#tabsleft').find('.pager .finish').removeClass('disabled');
                                                                                    } else {
                                                                                        $('#tabsleft').find('.pager .next').show();
                                                                                        $('#tabsleft').find('.pager .finish').hide();
                                                                                    }

                                                                                }});


                                                                            $('#tabsleft .finish').click(function() {
                                                                                alert('Finished!, Starting over!');
                                                                                $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
                                                                            });

                                                                        }();


                                                                        $(document).ready(function() {

                                                                            $('#2').change(function() {
                                                                                $('#lesson_type').val($('#2 option:selected').text());
                                                                            });

                                                                            $('#pillstab1').click(function() {
                                                                                redirecturl = "<?php echo base_url() . 'planningmanager/create_plan'; ?>";
                                                                                $(location).attr('href', redirecturl);

                                                                            });

                                                                            $("#createplanstep2").validate({
                                                                                rules: {
                                                                                    commoncorestrand: {
                                                                                        required: function() {
                                                                                            if ($("#commoncorestrand option[value='0']")) {
                                                                                                return true;
                                                                                            } else {
                                                                                                return false;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });

                                                                            $("select").change(function() {
                                                                                var divId = $(this).find(":selected").attr("data-div-id");
                                                                                $(".standard-topic").hide();
                                                                                $("#" + divId).show();

                                                                            });

                                                                             $('#view_textbook').click(function(){
                                                                                    if ($('#lesson_plan_template').hasClass('span12')){
                                                                                            $('#lesson_plan_template').removeClass('span12');
                                                                                            $('#lesson_plan_template').addClass('span6');
                                                                                            $('#textbook_template').addClass('span6');
                                                                                            $('#textbook_template').show();
                                                                                            $(this).html('Hide Textbook');
                                                                                    } else if ($('#lesson_plan_template').hasClass('span6')){
                                                                                            $('#lesson_plan_template').removeClass('span6');
                                                                                            $('#lesson_plan_template').addClass('span12');
                                                                                            $('#textbook_template').removeClass('span6');
                                                                                            $('#textbook_template').hide();
                                                                                             $(this).html('View Textbook');
                                                                                    }

                                                                            });
                                                                        });
                                                                </script> 
<script>
                                                                    $(function() {
                                                                        $(" input[type=radio], input[type=checkbox]").uniform();
                                                                    });

                                                                    $("#pills").bootstrapWizard("show", 1);

                                                                       function submitfrm(btnname){
                                                                           $('#btnaction').val(btnname);
                                                                           $('#createplanstep2').submit();
                                                                       }
                                                                       <?php if(strtolower($type)=='edit' && ($curriculum!='materials' && $curriculum!='othermaterials' && $curriculum!='textbook')){?>
                                                                            $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getteacherplaninfo/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  
                                                                                  result = jQuery.parseJSON(result);
//                                                                                  console.log(result);
                                                                                  $('#commoncorestrand').val(result.lesson_week[0].strand).trigger("liszt:updated");
                                                                                  selectstandard(result.lesson_week[0].strand, 0, 0);
//                                                                                  $('#standardeledesc').val(result.lesson_week[0].standard);
                                                                                  selectst(result.lesson_week[0].standard,<?php echo $lesson_week_plan_id;?>);
                                                                                  $.each(result.lesson_week, function(seindex, sevalue) {
                                                                                     // console.log(sevalue);
                                                                                        $('#'+sevalue.lesson_plan_id).val(sevalue.lesson_plan_sub_id).trigger("liszt:updated");


                                                                                        });
//                                                                                        $('#diff_instruction').val(result.lesson_week[0].diff_instruction);
//                                                                                        $('#diff_instruction').data("wysihtml5").editor.setValue(result.lesson_week[0].diff_instruction);
                                                                                        CKEDITOR.instances.diff_instruction.setData( result.lesson_week[0].diff_instruction );
                                                                                        CKEDITOR.instances.diff_instruction.updateElement();
                                                                                        
                                                                //                alert('done');  
                                                                //                $('#lesson_plans').html(result);
                                                                                //do something
                                                                              }
                                                                         });
                                                                         
                                                                         $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getcustomdiff/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  result = jQuery.parseJSON(result);
                                                                                 // console.log(result);
                                                                               $.each(result.custom, function(seindex, sevalue) {
//                                                                                      console.log(sevalue);
                                                                                        $("textarea[name='c_"+sevalue.custom_differentiated_id+"']").val(sevalue.answer)
//                                                                                        $('#c_'+sevalue.custom_differentiated_id).val(sevalue.answer);
                                                                                        });   
                                                                           }
                                                                         });
                                                                       <?php } else if($curriculum=='materials' || $curriculum=='textbook' || $curriculum=='othermaterials'){?>
                                                                        $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getMaterial/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  result = jQuery.parseJSON(result);
                                                                                  //console.log(result.lesson_week);
                                                                                 if(result.lesson_week==false){
                                                                                     return false;
                                                                                 }
                                                                               $.each(result.lesson_week, function(seindex, sevalue) {
                                                                                   console.log(sevalue.file_path);
                                                                                        selectmaterial(sevalue.material_id,sevalue.file_path);
//                                                                                      console.log(sevalue);
                                                                                        //$("textarea[name='c_"+sevalue.custom_differentiated_id+"']").val(sevalue.answer)
//                                                                                        $('#c_'+sevalue.custom_differentiated_id).val(sevalue.answer);
                                                                                        });   
                                                                           }
                                                                         });
                                                                          
                                                                             $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getteacherplaninfo/<?php echo $lesson_week_plan_id;?>',
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  result = jQuery.parseJSON(result);
                                                                                  console.log(result);
                                                                                 if(result.lesson_week==false){
                                                                                     return false;
                                                                                 }
                                                                                 $.each(result.lesson_week, function(seindex, sevalue) {
                                                                                   //console.log(sevalue.file_path);
                                                                                        $('#'+sevalue.lesson_plan_id).val(sevalue.lesson_plan_sub_id).trigger("liszt:updated");
                                                                                        console.log(sevalue);
                                                                                        //$("textarea[name='c_"+sevalue.custom_differentiated_id+"']").val(sevalue.answer)
//                                                                                        $('#c_'+sevalue.custom_differentiated_id).val(sevalue.answer);
                                                                                        }); 
                                                                              
                                                                           }
                                                                         });

                                                                        $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getcustomdiff/<?php echo $lesson_week_plan_id;?>',
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  result = jQuery.parseJSON(result);
                                                                                  console.log(result);
                                                                                 if(result.lesson_week==false){
                                                                                     return false;
                                                                                 }
                                                                                 $.each(result.custom, function(seindex, sevalue) {
//                                                                                      console.log(sevalue);
                                                                                        $("textarea[name='c_"+sevalue.custom_differentiated_id+"']").val(sevalue.answer)
//                                                                                        $('#c_'+sevalue.custom_differentiated_id).val(sevalue.answer);
                                                                                        }); 
                                                                              
                                                                           }
                                                                         });

                                                                       <?php }?>
                                                                       function openmaterial(material){
                                                                           window.open(material);
                                                                       }
                                                                       function baseName(str)
                                                                        {
                                                                           return ((url=/(([^\/\\\.#\? ]+)(\.\w+)*)([?#].+)?$/.exec(url))!= null)? url[2]: '';
                                                                        }
                                                                        var added_iframe = 0;
                                                                       function selectmaterial(material_id,material){
                                                                           console.log(material_id);
                                                                           if(added_iframe>0){
//                                                                               return false;
                                                                           }
                                                                           $('.material_css').each(function(index){
                                                                               $(this).css('border','none');
                                                                           });
                                                                           $('#'+material_id).css('border','1px solid grey');
                                                                           $('#selectedtextbook').val(material);
                                                                           //var basename = baseName(material);
                                                                           $('#textbookDisplay').html(material);
                                                                           $('#selectedmaterial').val(material_id);
                                                                           $('<iframe>', {
                                                                            src: '<?php echo WORKSHOP_DISPLAY_FILES.'medialibrary/lesson_plan/';?>'+material,
                                                                            id:  'myFrame',
                                                                            frameborder: 0,
                                                                            scrolling: 'no',width:'100%',height:1155
                                                                            }).appendTo('#textbookview');
                                                                            $('#view_text_btn').show();
                                                                            $('#view_textbook').show();
                                                                            added_iframe = added_iframe + 1;
                                                                       }
                                                                       
                                                                </script>
</body>
<!-- END BODY -->
</html>