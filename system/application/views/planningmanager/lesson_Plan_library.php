<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />


<style type="text/css">
.ac_results {
	padding: 0px;
	border: 1px solid black;
	
	overflow: hidden;
	z-index: 99999;
}

.ac_results ul {
	width: 100%;
	list-style-position: outside;
	list-style: none;
	padding: 0;
	margin: 0;
}

.ac_results li {
	margin: 0px;
	padding: 2px 5px;
	cursor: default;
	display: block;
	/* 
	if width will be 100% horizontal scrollbar will apear 
	when scroll mode will be used
	*/
	/*width: 100%;*/
	font: menu;
	font-size: 12px;
	/* 
	it is very important, if line-height not setted or setted 
	in relative units scroll will be broken in firefox
	*/
	line-height: 16px;
	overflow: hidden;
}

.ac_loading {
	background: white url('../indicator.gif') right center no-repeat;
}

.ac_odd {
	background-color: #eee;
}

.ac_over {
	background-color: #74b749;
	color: white;
}

</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
       <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                     <i class="icon-book"></i>&nbsp; Planning Manager
                   </h3>
                    <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                            <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>planningmanager">Planning Manager</a>
                            <span class="divider">></span>
                          
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Library</a>
                             <span class="divider">></span>
                          
                       </li>

                         <li>
                            <a href="<?php echo base_url();?>planningmanager/lesson_Plan_library">Create Lesson Plan Library</a>
                       </li>
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <form class="form-horizontal" action="<?php echo base_url();?>planningmanager/lesson_Plan_library_insert" name="lesson_Plan_library" id="lesson_Plan_library" method="post">
                        
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Create Lesson Plan Library</h4>
                          
                         </div>
                         <div class="widget-body">
<!--                            <form class="form-horizontal" action="#">-->
                                <div id="pills" class="custom-wizard-pills-green4">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                      <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                       <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                    
                               
                                       
                                     
                                 </ul>
                              <div class="progress progress-success progress-striped active">
                    <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                  
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                                   <div class="control-group">
                    
                                         
                                        
                                       <!--  <div class="control-group">
                                            <label class="control-label">Select Date</label>
                                            <div class="controls">
                                                <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="cdate" requigreen></input>
                                            </div>
                                        </div>-->
                       
                                   <input type="hidden" name="gol_standard" value="" id="gol_standard" />
                                 <input type="hidden" name="grade" id="grade" value=''>
            						<input type="hidden" name="standard_id" id="standard_id" value=''>
            						<input type="hidden" name="standarddata_id" id="standarddata_id" value="">
                                    
                                           <div class="control-group">
                                <label class="control-label">Lesson Title</label>
                                <div class="controls">
                                  <input class="span6" type="text" id="lesson_title" name="lesson_title" value="" style="width: 300px;" />
                                </div>
                              </div> 
											                                    
                                         
                                                 <div class="control-group">
                                  <label class="control-label">Select Grade</label>
                                             <div class="controls">
                     <select class="span12 chzn-select" name="grade_id" id="grade_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                           	<option value=""></option>
                                            <?php if(!empty($grades)) { 
                                            foreach($grades as $val)
                                            {
                                            ?>
                                            <option value="<?php echo $val['grade_id'];?>" ><?php echo $val['grade_name'];?>  </option>
                                            <?php } } ?>
                                            </select>
                                                </div>
                                         </div> 
                              
                                     </div>
                                     
                                     </div>
                                     
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                                   <div class="control-group">
                                         
                                        
                                             <div class="control-group">
                        <label class="control-label">Common Core Domain</label>
                        <div class="controls">
                          <select class="span12 chzn-select" name="strand" id="commoncorestrand" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" onChange="selectstandard(this.value, 0, 0);" requigreen>
                            <option value="0">Please select</option>
                            
                          </select>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Common Core Cluster</label>
                        <div class="controls" id="standards">
                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" requigreen>
                            <option value=""></option>
                       
                          </select>
                        </div>
                      </div>
                      
                        <div class="space20"></div>
                      <div class="control-group">
                        <label class="control-label">&nbsp;</label>
                        <div class="controls">
                          <table class="table table-striped table-bordered" style="width: 50%;">
                        <tr>
                          <td><p style="font-size: 14px;"><img src="<?php echo SITEURLM ?>img/file-search/pdf.png"> &nbsp;&nbsp; Math PDF</p></td>
                          <td style="padding-left: 10px;"><p style="font-size: 14px;"><img src="<?php echo SITEURLM ?>img/file-search/pdf.png"> &nbsp;&nbsp;ELA PDF</p></td>
                        </tr>
                      </table>
                        </div>
                      </div>
                      
                      <div class="space20"></div>
                      <div class="space20"></div>
                      <div class="control-group">
                        <label class="control-label">Element Descripton</label>
                        <div class="controls">
                          <textarea class="span12 " rows="5" name="standarddata" id="standardeledesc" requigreen></textarea>
                        </div>
                      </div>
                      
                
                                             
                                     </div>
                                     
                                  

                                     </div>
                                     
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                                   <div class="control-group">
                    
                    
                      
                           <div class="control-group">
                        <label class="control-label">Common Core Cluster</label>
                        <div class="controls">
                          <textarea class="span12 " rows="3" name="standarddisplay" id="standarddisplay" requigreen></textarea>
                        </div>
                      </div>
                      
                         <?php foreach ($lessontype as $lessontypekey => $lessontypeval) { ?>
                      <div class="control-group">
                        <label class="control-label"><?php echo $lessontypekey ?></label>
                         <input  type="hidden" name="lesson_plan_id[]" value="<?php echo $lessontypeval['lesson_plan_id']; ?>" id="lesson_plan_id" />
                        <div class="controls">
                          <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="lesson_plan_data_<?php echo $lessontypeval['lesson_plan_id']; ?>" id="<?php echo $lessontypeval['lesson_plan_id']; ?>" required>
                            <option value=""></option>
                            <?php
                                                                    foreach ($lessontypeval as $lssntypekey => $lssntypeval) {
                                                                        if ($lssntypekey != 'lesson_plan_id') {
                                                                            ?>
                            <option value="<?php echo $lssntypekey; ?>"><?php echo $lssntypeval; ?></option>
                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                          </select>
                        </div>
                      </div>
                      <?php } ?>
                              
                            <div class="control-group">
                        <label class="control-label">Differentiated Instruction</label>
                        <div class="controls">
                          <textarea class="span12 ckeditor" id='diff_instruction' name='diff_instruction' rows="6" require></textarea>
                        </div>
                      </div>  
                         
                                     </div>
                                     
                                  

                                     </div>
                                     
                                         <div class="tab-pane" id="pills-tab4">
                      <h3 style="color:#000000">STEP 4</h3>
                      <div class="control-group">
                        <div class="span12"> 
                          <!-- BEGIN GRID SAMPLE PORTLET-->
                          <div class="widget green">
                            <div class="widget-title">
                              <h4></i>Lesson Plan Template</h4>
                            </div>
                            <div class="widget-body">
                              <div class="control-group">
                                <label class="control-label">Domain</label>
                                <div class="controls">
                                  <input class="span6 " type="text" placeholder="Selected Standard Here" disabled id="comdomain" />
                                </div>
                              </div>
                              
                              <div class="control-group">
                                <label class="control-label">Lesson Type</label>
                                <div class="controls">
                                  <input class="span6 " type="text" placeholder="Selected Lesson Type" id="lesson_type" disabled />
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label">Unit Name</label>
                                <div class="controls">
                                  <input class="span6 " type="text" placeholder="Unit Here" disabled />
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label">Grade</label>
                                <div class="controls">
                                  <input class="span6 " type="text" id="grade_name" value="" disabled />
                                </div>
                              </div>
                              <?php
                                                                  if ($custom_diff != false) {
                                                                        foreach ($custom_diff as $custom_diffkey => $custom_diffval) {
                                                                            ?>
                                                                            
                              <div class="control-group">
                                <label class="control-label"><?php echo $custom_diffval['tab']; ?></label>
                                 <input  type="hidden" name="custom_diff[]" value="<?php echo $custom_diffval['custom_differentiated_id']; ?>" id="custom_diff" />
                                 
                                <div class="controls">
                                  <textarea class="span12 " rows="3" name="custom_<?php echo $custom_diffval['custom_differentiated_id']; ?>" required></textarea>
                                </div>
                              </div>
                              <?php
                                                                        }
                                                                    }
                                                                    ?>
                             
                              <center>
                                <button class="btn btn-large btn-success" type="submit" name="submit" value="submit"><i class="icon-save icon-white"></i> Save</button>
                             
                              </center>
                            </div>
                          </div>
                          <!-- END GRID SAMPLE PORTLET--> 
                        </div>
                        <BR>
                        <BR>
                        <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                          <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="myModalLabel2"><i class="icon-wrench"></i> Tool Kit</h3>
                          </div>
                          <div class="modal-body">
                            <center>
                              <u><b>PROMPTS: COMMON CORE ALIGNED LESSON PREPARATION</B></u><BR>
                            </center>
                            <BR>
                            <p align="justify"><b>Relevance/Rationale:</b> Why are the outcomes of this lesson important in the real world? Why are these outcomes essential for future learning? <BR>
                              <BR>
                            
                            <p align="justify"><b>Required Materials:</b> What are text, digital resources and materials that will be used this lesson?<BR>
                              <BR>
                            
                            <p align="justify"><b>Activities/Tasks:</b> What learning experiences will students engage in? How will you use these learning experiences or their student products as formative assesment opportunities?<BR>
                              <BR>
                            
                            <p align="justify"><b>Access for All:</b> How will you ensure that all students have access to engage appropriately in this lesson? Consider all aspects of student diversity. 
                          </div>
                          <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-success">OK</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="pager wizard">
                    <li class="previous first green"><a href="javascript:;">First</a></li>
                    <li class="previous green"><a href="javascript:;">Previous</a></li>
                    <li class="next last green"><a href="javascript:;">Last</a></li>
                    <li class="next green"><a  href="javascript:;">Next</a></li>
                  </ul>
                </div>
                                     
                                     
                                     </div>
                                    
                                     
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
<!--      </div>
       END PAGE   
   </div>-->
<!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-green">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will greenuce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   
    <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>

<script src="<?php echo SITEURLM ?><?php echo $view_path; ?>js/teacherplan.js"></script> 


<script>
var base_url = '<?php echo base_url();?>';
$('#grade_id').change(function(){
	$('#commoncorestrand').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>planningmanager/get_grade_by_strand",
	  data: { grade_id: $('#grade_id').val() }
	})
	  .done(function( msg ) {
		  $('#commoncorestrand').html('');
		  var result = jQuery.parseJSON( msg )
		   $('#commoncorestrand').append('<option value="all">All</option>');
		 $(result.strands).each(function(index, Element){ 
		 
		 console.log(Element);
		  $('#commoncorestrand').append('<option value="'+Element.strand+'">'+Element.strand+'</option>');
		});  
	$(result.grades).each(function(index, Element){ 
		console.log(Element);
		  $('#grade').val(Element.grade_id);
		  $('#grade_name').val(Element.grade);
		  $('#standard_id').val(Element.standard_id);
		}); 
		
		  $("#commoncorestrand").trigger("liszt:updated");
		  
	  });
  	
});
	
   </script> 
   
   <script>
                                                                        var Script = function() {


                                                                            $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
                                                                                    console.log('onShow1');
                                                                                }, onNext: function(tab, navigation, index) {

                                                                                    console.log('onNext');
                                                                                }, onPrevious: function(tab, navigation, index) {
                                                                                    console.log('onPrevious');
                                                                                }, onLast: function(tab, navigation, index) {
                                                                                    console.log('onLast');
                                                                                }, onTabShow: function(tab, navigation, index) {
                                                                                    //        console.log(tab);
                                                                                    //        console.log(navigation);
                                                                                    //        console.log(index);
                                                                                    //        console.log('onTabShow1');
                                                                                    var $total = navigation.find('li').length;
                                                                                    var $current = index + 1;
                                                                                    console.log($current);
                                                                                    if ($current == 4) {
                                                                                        $('#createplanstep1').submit();
                                                                                    }
                                                                                    var $percent = ($current / $total) * 100;
                                                                                    $('#pills').find('.bar').css({width: $percent + '%'});
                                                                                }});

                                                                            $('#tabsleft').bootstrapWizard({'tabClass': 'nav nav-tabs', 'debug': false, onShow: function(tab, navigation, index) {
                                                                                    console.log('onShow2');
                                                                                }, onNext: function(tab, navigation, index) {
                                                                                    console.log('onNext');
                                                                                }, onPrevious: function(tab, navigation, index) {
                                                                                    console.log('onPrevious');
                                                                                }, onLast: function(tab, navigation, index) {
                                                                                    console.log('onLast');
                                                                                }, onTabClick: function(tab, navigation, index) {
                                                                                    console.log('onTabClick');

                                                                                }, onTabShow: function(tab, navigation, index) {
                                                                                    console.log('onTabShow2');
                                                                                    var $total = navigation.find('li').length;
                                                                                    var $current = index + 1;
                                                                                    var $percent = ($current / $total) * 100;
                                                                                    $('#tabsleft').find('.bar').css({width: $percent + '%'});

                                                                                    // If it's the last tab then hide the last button and show the finish instead
                                                                                    if ($current >= $total) {
                                                                                        $('#tabsleft').find('.pager .next').hide();
                                                                                        $('#tabsleft').find('.pager .finish').show();
                                                                                        $('#tabsleft').find('.pager .finish').removeClass('disabled');
                                                                                    } else {
                                                                                        $('#tabsleft').find('.pager .next').show();
                                                                                        $('#tabsleft').find('.pager .finish').hide();
                                                                                    }

                                                                                }});


                                                                            $('#tabsleft .finish').click(function() {
                                                                                alert('Finished!, Starting over!');
                                                                                $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
                                                                            });

                                                                        }();


                                                                        $(document).ready(function() {

                                                                            $('#2').change(function() {
                                                                                $('#lesson_type').val($('#2 option:selected').text());
                                                                            });

                                                                            $('#pillstab1').click(function() {
                                                                                redirecturl = "<?php echo base_url() . 'planningmanager/create_plan'; ?>";
                                                                                $(location).attr('href', redirecturl);

                                                                            });

                                                                            $("#createplanstep2").validate({
                                                                                rules: {
                                                                                    commoncorestrand: {
                                                                                        required: function() {
                                                                                            if ($("#commoncorestrand option[value='0']")) {
                                                                                                return true;
                                                                                            } else {
                                                                                                return false;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });

                                                                            $("select").change(function() {
                                                                                var divId = $(this).find(":selected").attr("data-div-id");
                                                                                $(".standard-topic").hide();
                                                                                $("#" + divId).show();

                                                                            });


                                                                        });
                                                                </script> 
<script>
                                                                    $(function() {
                                                                        $(" input[type=radio], input[type=checkbox]").uniform();
                                                                    });

                                                                    

                                                                       function submitfrm(btnname){
                                                                           $('#btnaction').val(btnname);
                                                                           $('#createplanstep2').submit();
                                                                       }
                                                                       <?php if(strtolower($type)=='edit'){?>
                                                                            $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getteacherplaninfo/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  
                                                                                  result = jQuery.parseJSON(result);
//                                                                                  console.log(result);
                                                                                  $('#commoncorestrand').val(result.lesson_week[0].strand).trigger("liszt:updated");
                                                                                  selectstandard(result.lesson_week[0].strand, 0, 0);
//                                                                                  $('#standardeledesc').val(result.lesson_week[0].standard);
                                                                                  selectst(result.lesson_week[0].standard,<?php echo $lesson_week_plan_id;?>);
                                                                                  $.each(result.lesson_week, function(seindex, sevalue) {
                                                                                     // console.log(sevalue);
                                                                                        $('#'+sevalue.lesson_plan_id).val(sevalue.lesson_plan_sub_id).trigger("liszt:updated");


                                                                                        });
//                                                                                        $('#diff_instruction').val(result.lesson_week[0].diff_instruction);
//                                                                                        $('#diff_instruction').data("wysihtml5").editor.setValue(result.lesson_week[0].diff_instruction);
                                                                                        CKEDITOR.instances.diff_instruction.setData( result.lesson_week[0].diff_instruction );
                                                                                        CKEDITOR.instances.diff_instruction.updateElement();
                                                                                        
                                                                //                alert('done');  
                                                                //                $('#lesson_plans').html(result);
                                                                                //do something
                                                                              }
                                                                         });
                                                                         
                                                                         $.ajax
                                                                                ({
                                                                              type: "Post",
                                                                              url: '<?php echo base_url();?>teacherplan/getcustomdiff/'+<?php echo $lesson_week_plan_id;?>,
                                                                              data: {teacher_id:$('#teacher_id').val()},
                                                                              success: function(result)
                                                                              {
                                                                                  result = jQuery.parseJSON(result);
                                                                                  console.log(result);
                                                                               $.each(result.custom, function(seindex, sevalue) {
//                                                                                      console.log(sevalue);
                                                                                        $("textarea[name='c_"+sevalue.custom_differentiated_id+"']").val(sevalue.answer)
//                                                                                        $('#c_'+sevalue.custom_differentiated_id).val(sevalue.answer);
                                                                                        });   
                                                                           }
                                                                         });
                                                                       <?php }?>
                                                                       
                                                                </script>
                                                                
             
       
 <script type="text/javascript">
function updateschool(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#school_id").html(result);
			$("#school_id").trigger("liszt:updated");
		}
		});
	
}
</script>
        <script>
            $(document).ready(function() {
				
				 $("#studentlist").autocomplete("<?php echo base_url();?>classroom/getallstudents", {
        width: 260,
        matchContains: true,
        selectFirst: false
    });
	
	 $("#studentlist").result(function(event, data, formatted) {
        $("#student_id").val(data[1]);
    });
                
            });

$('#school_id').change(function(){
   $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>teacherself/getteacherBySchoolId",
            data: { school_id: $('#school_id').val()}
          })
            .done(function( result) {
//              console.log(teachers);
                $('#teacher_id').html('');
                $('#teacher_id').append('<option></option>');
              var teachers = jQuery.parseJSON(result);
              $.each(teachers,function(index,value){
//                    console.log(value);
                    $('#teacher_id').append('<option value="'+value.teacher_id+'">'+value.firstname+' '+value.lastname+'</option>');
              });
              $('#teacher_id').trigger("liszt:updated");
              
              
            });

});

        </script>    
   
   <script>
 	var newurl= '';
	$('#teacher_id').change(function(){
		newurl = "<?php echo base_url();?>implementation/getStudentsByTeacher/"+$(this).val();

		});
	
	 $("#student").autocomplete("<?php echo base_url();?>implementation/getStudentsByTeacher/", {
        width: 260,
        matchContains: true,
        selectFirst: false,
		max:15,
		
					//select end
    });
	 $("#student").result(function(event, data, formatted) {
        $("#student_id").val(data[1]);
		
		
    });
	
	$('#student').setOptions({
        extraParams: {
          teacher_id: function(){
            return $('#teacher_id').val();
          }
  }
})

 
 </script>  
   
   
     


</body>
<!-- END BODY -->
</html>