<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
  <base href="<?php echo base_url();?>"/>
     <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
         <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
          <div id="sidebar" class="nav-collapse collapse">
        <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
        <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-edit"></i>&nbsp; Lesson Plan Creator
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>planningmanager">Planning Manager</a>
                            <span class="divider">></span>
                          
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Creator</a>
                            <span class="divider">></span>
                          
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>planningmanager/completed_plan">Retrieve & Review</a>
                            
                          
                       </li>
                       
                        
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Retrieve & Review</h4>
                          
                         </div>
                         <div class="widget-body">
                           <form class="form-horizontal" method="post" action="<?php echo base_url();?>planningmanager/completelessonplanreport" id="completelesson" name="completelesson">
                                <div id="pills" class="custom-wizard-pills-green4">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
<!--                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>-->
                                      
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                   <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         
                                         <?php if($this->session->userdata('login_type')=='observer'){
											 ?>
                                         <div class="control-group">
                                  <label class="control-label">Select Teacher</label>
                                             <div class="controls">
                                      <select name="teacher_id" id="teacher_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                           <option value=""></option>
			   								 <?php if(!empty($teachers)) { 
											foreach($teachers as $val)
											{

											?>
			    						<option value="<?php echo $val['teacher_id'];?>" 
										<?php if(isset($teacher_id) && $teacher_id==$val['teacher_id']) {?> selected <?php } ?>
										><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
			    						<?php } } ?>
                                    </select>
                                    	
			                                 </div>
                                         </div> 
                                         <?php } else if($this->session->userdata('login_type')=='user'){?>
                                              <div class="control-group">
                                                    <label class="control-label">Select School</label>
                                                    <div class="controls">

                                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="school_id" id="school_id">
                                                       <option value=""></option>
                                                        <?php foreach($schools as $school):?>
                                                            <option value="<?php echo $school['school_id'];?>"><?php echo $school['school_name'];?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                    </div>
                                                </div>
                                
                                         <div class="control-group">
                                            <label class="control-label">Select Teacher</label>
                                            <div class="controls">

                                                <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="teacher_id" id="teacher_id">
                                                <!--<option value="all">All</option>-->
                                                <?php // foreach($teachers as $teacher):?>
                                                    <!--<option value="<?php // echo $teacher['teacher_id'];?>"><?php // echo $teacher['firstname'].' '.$teacher['lastname'];?></option>-->
                                                <?php // endforeach;?>

                                            </select>
                                            </div>
                                        </div>
                                         <?php }?>
                                         <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="" size="16" class="m-ctrl-medium" name="cdate"></input>
                                    </div>
                                    
                                </div>
                                           
                                            <div class="control-group">
                                            <label class="control-label">Select Time </label>

                                             <div class="controls" id="lesson_plans">
                     <!--                              <select name="ctime" id="lesson_plans" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                            
                                                </select>-->
                                               <div class="input-append bootstrap-timepicker" id="lesson_plans">
                                                   <input id="timepicker1" type="text" class="input-small" name="ctime" value="08:00 AM" required>
                                                    
                                                    <span class="add-on"><i class="icon-time"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="control-group" id="subject_name_group" style="display:none;">
                                            <label class="control-label">Subject</label>

                                             <div class="controls" id="subject_name">
                                            </div>
                                        </div>
                                      
                                     </div>
                                  
                                
                                    
                                     
                                      <!-- BEGIN STEP 4-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000">STEP 2</h3>
                                         <div class="control-group">
                                         
                                         
                                           <div class="span12">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                        <div class="widget green">
                            <div class="widget-title">
                                <h4></i>View Lesson Plan</h4>
                                            
                            </div>
                            <div class="widget-body">
                                <center><a class="btn btn-large btn-success" href="javascript:void();" target="_blank" id="printlessonplan" onclick="$('#errmsg').html('Please select date & time in previous step');$('#errorbtn').modal('show');" ><i class="icon-print icon-white"></i> Print Lesson Plan</a>  <button class="btn btn-large btn-success"><i class="icon-envelope icon-white"></i> Send to Colleague</button>   </center>
                            </div>
                        </div>
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                          </div> </div>                
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;" onclick="getcompleteplan();">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->
   </div>
   <!--Notification-->
   <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
       <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red" onclick="$('#pills').bootstrapWizard('show', 0);">OK</button>
                                </div>
                                
                                 
                            </div>
   <!--Notification ends-->
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

      
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
<!--   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
       
       var Script = function () {
       $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
       if($('#dp1').val()=='')
            {
                $('#errmsg').html("Please Select Sate");
                $('#errorbtn').modal('show');
                return false;
            }
            if($("timepicker1").val()=="")
            {
                $('#errmsg').html("Please Select Time");
                $('#errorbtn').modal('show');
                return false
            }
            <?php if($this->session->userdata('login_type')=='observer' || $this->session->userdata('login_type')=='user'){?>
            $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>planningmanager/completelessonplanreport",
              async: false,
              data: { cdate: $('#dp1').val(), ctime: $('#timepicker1').val(),teacher_id:$('#teacher_id').val() }
            })
              .done(function( msg ) {
                var result = jQuery.parseJSON(msg);
                if(!result.err)
                {
                    var link = "<?php echo base_url();?>planningmanager/createlessonplanpdf/"+result.teacher_id+"/"+result.subject+"/"+result.date;
                    $('#printlessonplan').removeAttr('onclick');
                    $('#printlessonplan').attr('href',link);
                } else 
                {
                    $('#errmsg').html(result.msg);
                    $('#errorbtn').modal('show');
                    return false;
                    
                }
              });
            <?php } else {?>
                $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>planningmanager/completelessonplanreport",
              async: false,
              data: { cdate: $('#dp1').val(), ctime: $('#timepicker1').val() }
            })
              .done(function( msg ) {
                var result = jQuery.parseJSON(msg);
                if(!result.err)
                {
                    var link = "<?php echo base_url();?>planningmanager/createlessonplanpdf/"+result.teacher_id+"/"+result.subject+"/"+result.date;
                    $('#printlessonplan').removeAttr('onclick');
                    $('#printlessonplan').attr('href',link);
                } else 
                {
                    $('#errmsg').html(result.msg);
                    $('#errorbtn').modal('show');
                    return false;
                    
                }
              });
            <?php }?>
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(tab);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');
if(index==1){
    if($('#dp1').val()=='')
            {
                $("#pills").bootstrapWizard("show",0);
                $('#errmsg').html("Please Select Date");
                $('#errorbtn').modal('show');
                
                return false;
            }
            if($("timepicker1").val()=="")
            {
                $("#pills").bootstrapWizard("show",0);
                $('#errmsg').html("Please Select Time");
                $('#errorbtn').modal('show');
                return false
            }
            <?php if($this->session->userdata('login_type')=='observer' || $this->session->userdata('login_type')=='user'){?>
            $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>planningmanager/completelessonplanreport",
              async: false,
              data: { cdate: $('#dp1').val(), ctime: $('#timepicker1').val(),teacher_id:$('#teacher_id').val() }
            })
              .done(function( msg ) {
                var result = jQuery.parseJSON(msg);
                if(!result.err)
                {
                    var link = "<?php echo base_url();?>planningmanager/createlessonplanpdf/"+result.teacher_id+"/"+result.subject+"/"+result.date;
                    $('#printlessonplan').removeAttr('onclick');
                    $('#printlessonplan').attr('href',link);
                } else 
                {
                    $('#errmsg').html(result.msg);
                    $('#errorbtn').modal('show');
                    return false;
                    
                }
              });
            <?php } else {?>
                $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>planningmanager/completelessonplanreport",
              async: false,
              data: { cdate: $('#dp1').val(), ctime: $('#timepicker1').val() }
            })
              .done(function( msg ) {
                var result = jQuery.parseJSON(msg);
                if(!result.err)
                {
                    var link = "<?php echo base_url();?>planningmanager/createlessonplanpdf/"+result.teacher_id+"/"+result.subject+"/"+result.date;
                    $('#printlessonplan').removeAttr('onclick');
                    $('#printlessonplan').attr('href',link);
                } else 
                {
                    $('#errmsg').html(result.msg);
                    $('#errorbtn').modal('show');
                    return false;
                    
                }
              });
            <?php }?>
}
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
    }});
}();
//createlessonplanpdf
function getcompleteplan(){
    
  }
  
  var base_url = '<?php echo base_url();?>';
$('#school_id').on('change',function(){
   
         $.ajax({
            type: "POST",
            url: base_url+"teacherself/getteacherBySchoolId",
            data: { school_id: $('#school_id').val()}
          })
            .done(function( result) {
//              console.log(teachers);
                $('#teacher_id').html('');
                $('#teacher_id').append('<option></option>');
              var teachers = jQuery.parseJSON(result);
              $.each(teachers,function(index,value){
//                    console.log(value);
                    $('#teacher_id').append('<option value="'+value.teacher_id+'">'+value.firstname+' '+value.lastname+'</option>');
              });
              $('#teacher_id').trigger("liszt:updated");
              
              
            });
     });
     
     <?php if($this->session->userdata('login_type')=='observer'){?>
        $('#dp1').on('changeDate',function(){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_lesson_plan",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val()},
              success: function(result)
              {
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
        });
        <?php }else { ?>
        $('#dp1').on('changeDate',function(){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_lesson_plan",
              data: "cdate="+$('#dp1').val(),
              success: function(result)
              {
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
        });
        <?php }?>

         $('#dp1').on('changeDate',function(){
//        console.log('in changeDate');
        if($('#dp1').val()!='' && $('#timepicker101').val()!=''){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_subject",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val(),starttime:$('#timepicker1').val()},
              success: function(result)
              {
                  console.log(result);
                  if(result!=''){
                      $('#subject_name').html('');
                      $('#subject_name_group').show();
                      $('#subject_name').html(result);
                  }
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
         }
        });
             $(function(){
    $('.timepicker1').timepicker().change(function(){
        $('.timepicker1').val($(this).val());
        if($('#dp1').val()!='' && $('#timepicker1').val()!=''){
          
        $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_subject",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val(),starttime:$(this).val()},
              success: function(result)
              {
                  if(result!=''){
                      $('#subject_name').html(' ');
                      $('#subject_name_group').show();
                      $('#subject_name').html(result);
                  } else {
                      $('#subject_name').html(' ');
                      $('#subject_name_group').show();
                      $('#subject_name').html('Not Assigned');
                  }
              }
         });
         }
});

});

   </script>    
</body>
<!-- END BODY -->
</html>