<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
  <base href="<?php echo base_url();?>"/>
     <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
         <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
          <div id="sidebar" class="nav-collapse collapse">
        <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-edit"></i>&nbsp; Lesson Plan Creator
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>index">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          <a href="<?php echo base_url();?>planningmanager/">Planning Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Creator</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>planningmanager/edit_plan">Edit Lesson Plan in Progress</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Edit Lesson Plan in Progress</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-green5">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     <li><a href="#pills-tab4" data-toggle="tab">Step 4</a></li>
                                      <li><a href="#pills-tab5" data-toggle="tab">Step 5</a></li>
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                   <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                         
                                
                                        <div class="control-group">
                                   <label class="control-label">Select Lesson Plan</label>
                                    <div class="controls">
                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <?php foreach($lessonplans as $lessonplansval){?>
                                            <option value="<?php echo $lessonplansval;?>"><?php echo $lessonplansval;?></option>
                                        <?php }?>
                                        
                                    </select>
                                    </div>
                                </div>
                                         
                                        
                                     </div>
                                  
                                  <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         <form class="form-horizontal" action="#">
                                         <div class="control-group">
                                    <label class="control-label">Edit Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" value="12-12-2013" size="16" class="m-ctrl-medium"></input>
                                    </div>
                                </div>
                                         <div class="control-group">
                                             <label class="control-label">Edit Period (Optional)</label>
                                             <div class="controls">
                                             
                                             <select data-placeholder="--Please Select--" class="chzn-select " multiple="multiple" tabindex="6">
                                             <option value=""></option>
                                        <optgroup label="">
                                            <option>1st Period</option>
                                            <option selected>2nd Period</option>
                                            <option>3rd Period</option>
                                            <option>4th Period</option>
                                            <option >5th Period</option>
                                            <option>6th Period</option>
                                            <option>7th Period</option>
                                        </optgroup>
                                        
                                        </select>
                                             
                                                         
                                             </div>
                                         </div>
                                         
                                         
                                         <div class="control-group">
                                    <label class="control-label">Edit Time (Optional)</label>

                                     <div class="controls">
                                        <div class="input-append bootstrap-timepicker">
                                            <input id="timepicker1" type="text" class="input-small">
                                            <span class="add-on"><i class="icon-time"></i></span>
                                        </div>
                                    </div>
                                </div>
                                     </div>
                                     
                                      <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                         
                                         
                                         <div class="control-group">
                                             <label class="control-label">Edit Grade</label>
                                             <div class="controls">
                                                    <select data-placeholder="--Please Select--" class="chzn-select " multiple="multiple" tabindex="6" style="width: 300px;">
                                             <option value=""></option>
                                        <optgroup label="">
                                            <option >Kindergarten</option>
                                            <option>1st Grade</option>
                                            <option>2nd Grade</option>
                                            <option>3rd Grade</option>
                                            <option>4th Grade</option>
                                            <option selected>5th Grade</option>
                                            <option selected>6th Grade</option>
                                            <option>7th Grade</option>
                                            <option>8th Grade</option>
                                            <option>9th Grade</option>
                                            <option>10th Grade</option>
                                            <option>11th Grade</option>
                                            <option>12th Grade</option>
                                           
                                        </optgroup>
                                        
                                        </select>
                                             </div>
                                         </div>
                                         <div class="control-group">
                                             <label class="control-label">Edit Subject</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="English" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option value="Math">Math</option>
                                        <option value="English">English</option>
                                        <option value="Science">Science</option>
                                        <option value="History">History</option>
                                    </select>
                                             </div>
                                         </div>
                                         <div class="control-group">
                                             <label class="control-label">Edit Unit Name (Optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span6" value"Unit Name" />
                                                 
                                             </div>
                                         </div>
                                     </div>
                                     
                                      <!-- BEGIN STEP 4-->
                                     <div class="tab-pane" id="pills-tab4">
                                         <h3 style="color:#000000;">STEP 4</h3>
                                         <div class="control-group">
                                             <label class="control-label">Edit Topic & Standard</label>
                                             <div class="controls">
                                    
                                     <select data-placeholder="--Please Select--" class="chzn-select span12" multiple="multiple" tabindex="6" style="width: 300px;">
                                             <option value=""></option>
                                        <optgroup label="Reading Standards for Literature">
                                            <option>Point of View</option>
                                        <option> Main Idea</option>
                                        <option>Comprehension</option>
                                        <option selected>Range of Reading</option>
                                           
                                        </optgroup>
                                        
                                        
                                           <optgroup label="Topic 1">
                                            <option>Standard 1</option>
                                        <option> Standard 2</option>
                                        <option>Standard 3</option>
                                         <option>Standard 4</option>
                                           
                                        </optgroup>
                                        
                                        <optgroup label="Topic 2">
                                            <option>Standard 1</option>
                                        <option> Standard 2</option>
                                        <option>Standard 3</option>
                                         <option>Standard 4</option>
                                           
                                        </optgroup>
                                        
                                        <optgroup label="Topic 3">
                                            <option>Standard 1</option>
                                        <option> Standard 2</option>
                                        <option>Standard 3</option>
                                         <option>Standard 4</option>
                                           
                                        </optgroup>
                                        
                                        
                                        
                                        </select><BR><BR><BR><BR>
                                    
                                        </div>
                                             
                                             </div>
                                      

                                        
                                     </div>
                                     
                                      <!-- BEGIN STEP 5-->
                                     <div class="tab-pane" id="pills-tab5">
                                         <h3 style="color:#000000">STEP 5</h3>
                                         <div class="control-group">
                                         
                                         
                                           <div class="span12">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                        <div class="widget green">
                            <div class="widget-title">
                                <h4></i>Lesson Plan Template</h4>
                                            
                            </div>
                            <div class="widget-body">
                               <center><button class="btn btn-large btn-success"><i class="icon-save icon-white"></i> Save Changes</button>  <button class="btn btn-large btn-success"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                            </div>
                        </div>
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                         
                                           <center> <a href="#myModal2" role="button" class="btn btn-large btn-success" data-toggle="modal"><font size="6px"><i class="icon-wrench icon-white"></i> Tool Kit</font><BR><font size="2px">(view prompts that will assist in developing lesson plans)</font></a> <center><BR><BR>
                                            
                                            <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-wrench"></i> Tool Kit</h3>
                                </div>
                                <div class="modal-body">
                                    <center><u><b>PROMPTS: COMMON CORE ALIGNED LESSON PREPARATION</B></u><BR></center><BR>

<p align="justify"><b>Relevance/Rationale:</b> Why are the outcomes of this lesson important in the real world? Why are these outcomes essential for future learning? <BR><BR>

<p align="justify"><b>Required Materials:</b> What are text, digital resources and materials that will be used this lesson?<BR><BR>

<p align="justify"><b>Activities/Tasks:</b> What learning experiences will students engage in? How will you use these learning experiences or their student products as formative assesment opportunities?<BR><BR>

<p align="justify"><b>Access for All:</b> How will you ensure that all students have access to engage appropriately in this lesson? Consider all aspects of student diversity.


                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                            </div>
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->
   </div>
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

      
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>    
</body>
<!-- END BODY -->
</html>