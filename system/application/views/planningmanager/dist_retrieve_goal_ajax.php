<table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th class="sorting">School</th>
                                <th class="sorting">Teacher</th>
                                <th class="no-sorting">Review Plan</th>
                               
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($goals as $goal):?>
                                 <tr class="odd gradeX">                                
                                <td><?php echo $goal['school_name'];?></td>
                                <td class="hidden-phone"><?php echo $goal['firstname'].' '.$goal['lastname'];?></td>
                                <td class="hidden-phone"><a href="javascript:;" role="button" class="btn btn-primary" data-toggle="modal" onclick="commentsview(<?php echo $goal['teacher_id'];?>)">View</a>
                                
                                </td>
                                
                                </tr>
                                
                           <?php endforeach;?>
                            </tbody>
                        </table>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
       

   </script>