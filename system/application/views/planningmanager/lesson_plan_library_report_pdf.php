<style type="text/css">
   .htitle{ font-size:16px; 
   color:#08a5ce; 
   margin-bottom:10px;
   }
td{
	font-size:10px;
	}
   </style>


   
  <div backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png"/>
        
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<?php /*?>
        <b><?php echo ($parent_conference[0]->districts_name); ?></b>		
		<br /><b><?php echo ($parent_conference[0]->school_name); ?></b>
        <?php */?><br /><br /><b>Lesson Plan Library Report</b>
		
		</div>
	</div>
		</div>
        

<br /><br />

<table style="width:850px;border:2px #7FA54E solid;">
		<tr>
		<td style="width:255px;color:#CCCCCC;">
        <?php
        if($this->session->userdata('login_type')=='teacher'){?>
           <b>Teacher:</b><?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?>   
           
           <?php } else if($this->session->userdata('login_type')=='observer'){?>
                 <b>Observer Name:</b><?php echo ($teachername[0]['observer_name']); ?>   
           <?php } else if($this->session->userdata('login_type')=='user'){?>
                 <b>User Name:</b><?php echo $this->session->userdata('username');?>
                 <?php }?>
		
		</td>
		
		<td style="width:240px;color:#CCCCCC;">
		<b>Lesson Title:</b><?php echo ($lesson_Plan_library[0]->lesson_title); ?>
		</td>
		<td style="width:250px;color:#CCCCCC;">
		<b>Grade:</b><?php echo ($lesson_Plan_library[0]->grade_name); ?>
		</td>
		</tr>
		<tr>
		<td>
		<br />
		</td>
		</tr>
		<tr>
        <td style="width:180px;color:#CCCCCC;">
		<b>Common Core Domain:</b><?php echo ($lesson_Plan_library[0]->strand); ?>
		</td>
		
		<td style="width:185px;color:#CCCCCC;">
		<b>District:</b><?php echo ($lesson_Plan_library[0]->districts_name); ?>
		</td>
		</tr>
		</table>
		
<table  cellspacing='0' cellpadding='0' border='0' id='conf' style='width:770px;' >

   <?php /*?> <tr >
        <td >
            <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> 
                <b>Strengths</b> 
            </div>
        </td>
    </tr><?php */?>
    
       <tr>
        <td >
            <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> 
                <b>Element Descripton</b> 
            </div>
        </td>
</tr>

<tr>
        <td >
            <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo $lesson_Plan_library[0]->standarddata; ?> </div></td>
    </tr>
    
          <tr>
        <td >
            <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> 
                <b>Common Core Cluster</b> 
            </div>
        </td>
</tr>

<tr>
        <td >
            <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo $lesson_Plan_library[0]->standarddisplay; ?> </div></td>
    </tr>
    
    
    <tr>
        <td >
            <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> 
                <b>Differentiated Instruction</b> 
            </div>
        </td>
</tr>

<tr>
        <td >
            <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo $lesson_Plan_library[0]->diff_instruction; ?> </div></td>
    </tr>

<?php foreach($lesson_plan_library_sub as $library_sub){?>  
    <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b> <?php echo $library_sub->tab;?> </b> </div></td>
    </tr>
    
  
    <tr>
        <td > <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo $library_sub->subtab;?> </div></td>
    </tr>
   <?php }?> 
    
    <?php foreach($lesson_plan_library_custom as $library_custom){ ?>
    
        <tr>
        <td > <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;"> <b><?php echo $library_custom->tab?> </b> </div></td>
    </tr>
    <tr>
        <td > <div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;"> <?php echo $library_custom->custom?>  </div></td>
    </tr>
    <?php }?>
    
    
   
   
</table>
