﻿<table class='table table-striped table-bordered' id='editable-sample'>
     <tr> 
     <td>Id</td>
     <td>District_id</td>
      <td>subject Name</td>
     <td>objective</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page-1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
     <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->subject_name;?></td>
     <td><?php echo $data->objective;?></td>
   <td><?php echo $data->status;?></td>
      <td>
       <button class="edit_academic_achievement btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
    <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
        <td>
<button type="Submit" id="remove_academic_achievement" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_academic_achievement btn btn-danger"><i class="icon-trash"></i></button>        
        
        </td>
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_academic_achievement ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'academic_achievement/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#academic_achievement_id').val(result[0].id);
       $('#subject_id').val(result[0].subject_id);
       $('#objective').val(result[0].objective);
       $('#status').val(result[0].status);
       console.log(result[0].subject_id);
       $("#dialog").dialog({
      modal: true,
            height:250,
      width: 500
      });
       }
     });  
});

$(".remove_academic_achievement").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'academic_achievement/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed Academic Achievement list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

