<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
<style>
.content {
    display: none;
}
#msgContainer{width:95% !important;}
</style>    

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
	<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/parent_bridge">Parent Bridge</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>communication/parent_report">Parent Notification Records</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Parent Notification Records</h4>
                          
                         </div>
                         
                         
                         <div class="widget-body" style="min-height: 150px;">
                         
                         <div class="space20"></div>
                         
                      
                            
                            <form id='group' >
                            <label class="inline">
        <b>Parents</b>
    </label>
    <label class="inline">
        <input type="radio" class="family-btn trigger" data-rel="family" name="all" onclick="changegrade()" id="all" value="all">Family
    </label>
    <label class="inline">
       
        <input type="radio"  class="family-btn trigger" name="all" id="individual" onclick="changegrade()" value="individual">School Wide
    </label>
    
    <label class="inline">
        <input type="radio" name="all" id="grade"  class="grade-btn trigger" data-rel="grade" onclick="changegrade()"  value="grade">Grade
    </label>
    <label class="inline">
      <input type="radio" name="all" id="classroom" class="class-room-btn trigger" data-rel="class-room"  onclick="changegrade()" value="classroom">Class Room
    </label>
   
</form>

<div >
    <div class="family content">
  
    <div class="form-horizontal familytd">
                                            <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
     <select class="span12 chzn-select" id="studentgrade" name="studentgrade" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                 <option value="all">-All-</option>
                                            <?php if($grades!=false)
                                            {
                                            foreach($grades as $gradeval)
                                            {
                                            ?>
                                            <option value="<?php echo $gradeval['grade_id'];?>"><?php echo $gradeval['grade_name'];?></option>
                                            
                                            <?php 
                                            
                                            } 
                                            
                                            }
                                            ?>
                                            </select>
                                             </div>
                                         </div>
                                         
                                         
                                                 
                             
                            
                           
                                         </div>
                                         
                                    <div class="space15"></div>
                                         
                                      
    </div>
    
    
    <div class="grade content">
    
    <div class="form-horizontal">
                                            <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
     <select name="gradebox" id="gradebox" onchange="chagegradeteachers(this.value)" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
      								<option value="all">-All-</option>
                                        <?php if($grades!=false)
                                        {
                                        foreach($grades as $gradeval)
                                        {
                                        ?>
                                        <option value="<?php echo $gradeval['grade_id'];?>"><?php echo $gradeval['grade_name'];?></option>
                                        
                                        <?php 
                                        
                                        } 
                                        
                                        }
                                        ?>
                                        </select>
                                             </div>
                                         </div>
                                         
                                       
                                    
                                     
                                </div>
                                         
                               </div>          
    
    </div>
    
    
    <div class="class-room content">
    
     <div class="form-horizontal classroomtd" >
                                            <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
                                                 <select name="classgradebox" id="classgradebox"  class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                      
                                            <option value=""></option>
                                            <?php if($grades!=false)
                                            {
                                            foreach($grades as $gradeval)
                                            {
                                            ?>
                                            <option value="<?php echo $gradeval['grade_id'];?>"><?php echo $gradeval['grade_name'];?></option>
                                            
                                            <?php 
                                            
                                            } 
                                            
                                            }
                                            ?>
                                            </select>
                                             </div>
                                         </div>
                                         
                                  <div class="control-group">
                                             <label class="control-label">Class Room</label>
                                             <div class="controls">
            <select class="span12 chzn-select" name="classroombox" id="classroombox" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            						<option value=""></option>
                                            <?php if($classrooms!=false)
                                            {
                                            foreach($classrooms as $classroomsval)
                                            {
                                            ?>
                                            <option value="<?php echo $classroomsval['class_room_id'];?>"><?php echo $classroomsval['name'];?></option>
                                            
                                            <?php 
                                            
                                            } 
                                            
                                            }
                                            ?>
                                            </select>
                                             </div>
                                         </div>    
                                         
                                         <div class="control-group">
                                             <label class="control-label">Day</label>
                                             <div class="controls">
              <select  name="daybox" id="daybox"  class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
              <option value=""></option>
<?php 
for($i=0;$i<=6;$i++)
{
if($i==0)
		{
			$m = 'Mon';
		}
		if($i==1)
		{
			$m = 'Tue';
		}
		if($i==2)
		{
			$m = 'Wed';
		}
		if($i==3)
		{
			$m = 'Thu';
		}
		if($i==4)
		{
			$m = 'Fri';
		}
		if($i==5)
		{
			$m = 'Sat';
		}
		if($i==6)
		{
			$m = 'Sun';
		}	
		
?>
<option value="<?php echo $i;?>"><?php echo $m;?></option>

<?php 

} 


?>
</select>
                                             </div>
                                         </div> 
                                         
                                          <div class="control-group">
                                             <label class="control-label">Select Period</label>
                                             <div class="controls">
                                                 <select name="periodbox" id="periodbox" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        
                                        <option value=""></option>
                                        <?php if($pariods!=false)
                                        {
                                        foreach($pariods as $pariodsval)
                                        {
                                        ?>
               <option value="<?php echo $pariodsval['period_id'];?>"><?php echo $pariodsval['start_time'];?> <?php echo $pariodsval['end_time'];?></option>
                                        
                                        <?php 
                                        
                                        } 
                                        
                                        }
                                        ?>
                                        </select>
                                        </div>
                                         </div>      
                                         
                                         </div>
                                         
                                         
          </div>
</div>                                   


<div class="space20"></div>
<div class="form-horizontal" >
   <div class="control-group">
       <div class="controls">
    	<input class="btn btn-purple"  type="button" name="getmemorandumsasigned" id="getmemorandumsasigned" value="Submit">   
        </div>
   </div>
</div>


	
                       
                       
                       
           <div align="center" id="memorandumdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>    
                     </div>
                     
        
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                 
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>


<!--start old script -->
<?php /*?><link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" /><?php */?>

<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/parent_report.js" type="text/javascript"></script>
<!--end old script -->
   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   $('.trigger').click(function() {
    $('.content').hide();
    $('.' + $(this).data('rel')).show();
});$('.trigger').click(function() {
    $('.content').hide();
    $('.' + $(this).data('rel')).show();
});
   </script>
   
   
 
</body>
<!-- END BODY -->
</html>