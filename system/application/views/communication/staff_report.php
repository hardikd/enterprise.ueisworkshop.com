<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Staff Communciation Record::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/staff_report.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
		<div class='htitle' style="padding-left:40px;" >Staff Communciation Records</div>
		<table style="padding-left:40px;">			
<tr>
<td class="htitle">
Record Options:
</td>
<td>
<input type="radio"  checked="checked" name="all" onclick="changegrade()" id="all" value="all">All
</td>
<td>
<input type="radio" name="all" id="individual" onclick="changegrade()" value="individual">Individual
</td>
<td id="alltd" style="display:none;">
<select name="teacher_id" id="teacher_id">
<option value="">-Please Select</option>
<?php if($teachers!=false) { 
foreach($teachers as $teacherval)
{
?>
<option value="<?php echo $teacherval['teacher_id'];?>"><?php echo $teacherval['firstname'].' '.$teacherval['lastname'];?></option>
<?php } } ?>
</select>
</td>
<td>
<input type="radio" name="all" id="grade"  onclick="changegrade()"  value="grade">Grade
</td>
<td>
<select name="gradebox" id="gradebox"  style="display:none;">
<option value="all">-All-</option>
<?php if($grades!=false)
{
foreach($grades as $gradeval)
{
?>
<option value="<?php echo $gradeval['grade_id'];?>"><?php echo $gradeval['grade_name'];?></option>

<?php 

} 

}
?>
</select>
</td>
</tr>
<!--<tr>
<td class="htitle">
Select Date:
</td>
<td colspan="2">
<input type="text" class="txtbox1" name="date" id="date" readonly value="<?php echo date('m-d-Y');?>">
</td>
</tr>-->
</tr>
		<tr>
				<td colspan="4" align="center">
		<input  class="btnsmall" type="button" name="getmemorandumsasigned" id="getmemorandumsasigned" value="Submit">
		</td>
		</tr>
</table>

		<div id="memorandumdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
