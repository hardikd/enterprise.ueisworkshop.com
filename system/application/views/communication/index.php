<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Staff Communication::</title>
<base href="<?php echo base_url();?>"/>
<LINK href="<?php echo SITEURLM?>css/sample.css" type=text/css rel=stylesheet>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/calebderical.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/adapters/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/sample.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/staff.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.ckeditor1').ckeditor();
	
	});
</script>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">		
		<table cellpadding="0" cellspacing="5" border=0 class="jqform">
		<tr>
		<td class="htitle" >
		Staff Communication
		</td>
		</tr>
		<tr><td class='style1'></td><td>
		<span style="color: Red;display:none" id="homemessage"></span>
				</td>
			</tr>
			<tr>
				
				<td valign="top" >
				<input class="txtbox" type='text' readonly style="width:550px;"  id='homedate' name='homedate' value='<?php echo date('Y-m-d');?>'>
				</td>
				
			</tr>
			
			<tr>
				
				<td valign="top" >
				
				<textarea   class="ckeditor1"   id='homework' name='homework'  ></textarea>
				
				</td>
				
			</tr>
</table>
<table style="padding-left:40px;">			
<tr>
<td class="htitle">
Teachers:
</td>
</tr>
<tr>
<td>
<input type="radio" onclick="toggleCheckboxes('all_',this.checked)" checked="checked" name="all" id="all" value="all">All
</td>
<td>
<input type="radio" name="all" id="individual" onclick="changegrade()" value="individual">Individual
</td>
<td>
<input type="radio" name="all" id="grade"  onclick="changegrade()"  value="grade">Grade
</td>
<td>
<select name="gradebox" id="gradebox" onchange="chagegradeteachers(this.value)" >
<option value="all">-All-</option>
<?php if($grades!=false)
{
foreach($grades as $gradeval)
{
?>
<option value="<?php echo $gradeval['grade_id'];?>"><?php echo $gradeval['grade_name'];?></option>

<?php 

} 

}
?>
</select>
</td>
</tr>
</table>
<table border="0"  style="padding-left:40px;" class="allteachers">
<tr >
<?php if($teachers!=false)
{
$i=0;
foreach($teachers as $teachersval)
{
if($i%3==0 && $i!=0)
{
?>
</table>
</td>
<td >
    <table BORDER="0" >

	<?php } else if($i==0)
	
	{
?>
<td >
    <table BORDER="0" >
<?php } ?>
<tr>
<td>
<input type="checkbox" name="teacher[]" id="all_<?php echo $teachersval['teacher_id'];?>" value="<?php echo $teachersval['teacher_id'];?>"><?php echo $teachersval['firstname'].' '.$teachersval['lastname'];?>
</td>
</tr>
<?php
$i++; 
}
?>
</table>
</td>

<?php } else { ?>

<td>
No Teachers Found.
</td>

<?php } ?>
</tr>
</table>

<table class="gradeteachers" style="padding-left:40px;" style="display:none;">
<tr >
<?php if($gradeteachersall!=false)
{
$i=0;
foreach($gradeteachersall as $teachersval)
{
if($i%3==0 && $i!=0)
{
?>
</table>
</td>
<td >
    <table BORDER="0" >

	<?php } else if($i==0)
	
	{
?>
<td >
    <table BORDER="0" >
<?php } ?>
<tr  >
<td>
<input type="checkbox" name="gradeallt[]" id="gradeall_<?php echo $teachersval['teacher_id'];?>" value="<?php echo $teachersval['teacher_id'];?>"><?php echo $teachersval['firstname'].' '.$teachersval['lastname'];?>
</td>
<?php
$i++; 
}
?>
</table>
</td>

<?php } else { ?>

<td>
No Teachers Found.
</td>

<?php } ?>
</tr>
</table>
<table style="padding-left:40px;">

<tr><td valign="top"></td><td valign="top"><input class="btnbig" type='submit' name="submit" id='homeadd' value='Send' title="Add New" > &nbsp;&nbsp;&nbsp;<img src="<?php echo SITEURLM?>images/ajax-loader.gif" style="display:none;" id="loading"></td></tr>

</table>


			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>