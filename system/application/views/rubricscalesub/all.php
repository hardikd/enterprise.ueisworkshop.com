<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
   
   <script type="text/javascript">
var dist_user_district_id= <?php if($this->session->userdata('district_id')) { echo $this->session->userdata('district_id'); } ?>;
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/headerv1.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
          <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      Welcome <?php echo $this->session->userdata('username');?>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                           <a href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
							<a href="<?php echo base_url();?>rubricscalesub">Rubric Scales</a>
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget red">
                         <div class="widget-title">
                             <h4>Rubric Scales</h4>
           				</div>
                        <div class="widget-body">
					<button type='button' name="rubricscalesub_add" id='rubricscalesub_add' value='Add New' class="btn btn-success" ><i class="icon-plus"></i > Add New</button>
                         <div class="space20"></div>
				
              	  <table align="center" cellpadding="5" >
		<tr>
		<td>
	<input type="hidden" name="countries" id="countries" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
	<input type="hidden" name="states" id="states" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
	<input type="hidden" name="district" id="district" value="<?php echo $this->session->userdata('district_id') ?>" >
		</td>
		<td>
		Rubric Contents:
		</td>
		<td>
		<select class="combobox" name="rubricscale" id="rubricscale">
		
		<?php if(!empty($rubricscale)) { ?>
		<option value="all">All</option>
		<?php foreach($rubricscale as $val)
		{
		?>
		<option value="<?php echo $val['scale_id'];?>" <?php if(isset($scale_id) && $scale_id==$val['scale_id'] ) {?> selected <?php  } ?> ><?php echo $val['scale_name'];?></option>
		<?php } } else { ?>
		<option value="0">No Rubric Contents Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
        <button title="Submit" type="button" name="getrubricscale" id='getrubricscale' value='Submit' class="btn btn-success">Submit</button>
        
		</td>
		</tr>
		</table>
				<div id="rubricscalesubdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <!--notification -->
  <div id="dialog" title="Rubric Scales" style="display:none;"> 

<form name='rubricscalesubform' id='rubricscalesubform' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
			<td valign="top" >
			   <input type="hidden" name="country_id" id="country_id" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
				<input type="hidden" name="state_id" id="state_id" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
				<input type="hidden" name="district_id" id="district_id" value="<?php echo $this->session->userdata('district_id') ?>" >
			</td>				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Rubric Content:
				</td>
				<td valign="top" style="height:40px;">
				<select class="combobox" name="scale_id" id="scale_id">
				<option value="">-select</option>

				</select>
				<input  type='hidden'  id='sub_scale_id' name='sub_scale_id'>
				<input  type='hidden'  id='actual_district_id' name='actual_district_id' value="">
				<input  type='hidden'  id='actual_scale_id' name='actual_scale_id' value="">
				</td>
				
			</tr>
			<tr>
			<td colspan="2">
			<table id="myTable" width="100%" >
			<tr>
			<td>
			<input type="hidden" name="counterscale" id="counterscale" value="">
			</td>
			</tr>
			<tr id="TextBoxDiv1">
			<td valign="top" >
			Sub Rrubricscale  Name: 
		</td>
		<td valign="top" style="height:40px;">
		<input class="txtbox" type='text' id='textbox1' name="textbox1">&nbsp;&nbsp;
        <button title="Remove" class="btn btn-danger" type='button' id='removeButton' value='Remove' onclick="remove(1);" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Remove Button</button>
        
		</td>
	    </tr>
		</table>
		</td>
       </tr>
<tr id="addsub" >
<td>
<button type="button" id="addButton" value="Add SubGroup" class="btn btn-success"><i class="icon-plus"></i> Add SubGroup</button>

</td>
</tr>
<!--<input type='button' value='Remove Button' id='removeButton'>-->


						
</tr><tr><td valign="top"></td><td valign="top">
 <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<button type='submit' name="submit" id='rubricscalesubadd' value='Add' class="btn btn-success"><i class="icon-plus"></i> Add New</button>


</td></tr></table></form>
</div>
   <!-- notification ends -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.form.js"></script>
     
   
  
<script src="<?php echo SITEURLM.$view_path; ?>js/addnew.js" type="text/javascript"></script>
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/rubricscalesub.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/countries4.js" type="text/javascript"></script>


   <!-- END JAVASCRIPTS --> 
 
</body>
<!-- END BODY -->
</html>