<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>UEIS Workshop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <base href="<?php echo base_url(); ?>"/>
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

        <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />
        <style>
            .tabcontent TD {

                padding: 0 7px;
                text-align: center;

            }
        </style>
        <script>

            var base_url = '<?php echo base_url(); ?>';

        </script>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php require_once($view_path . 'inc/header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar-scroll">
                <div id="sidebar" class="nav-collapse collapse">


                    <!-- BEGIN SIDEBAR MENU -->
                    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>

                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->  
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->   
                    <div class="row-fluid">
                        <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <h3 class="page-title">
                                <i class="icon-edit"></i>&nbsp; Curriculum Selector
                            </h3>
                            <ul class="breadcrumb" >

                                <li>
                                    UEIS Workshop
                                    <span class="divider">&nbsp; | &nbsp;</span>
                                </li>

                                <li>
                                    <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>tools">Tools & Resources</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>curriculum_selector">Lesson Plan Set-up</a>
                                </li>




                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN BLANK PAGE PORTLET-->
                            <div class="widget purple">
                                <div class="widget-title">
                                    <h4>Curriculum Selector</h4>

                                </div>
                                <form class="form-horizontal" id="mediaForm" name="mediaForm" method="post" enctype="multipart/form-data">
                                    <div class="widget-body">
                                        <div id="temp" style="display:none;"></div>

                                       

                                        <div id="pills" class="custom-wizard-pills-purple2">
                                            <ul>
                                                <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
<!--                                                <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>-->



                                            </ul>
                                            <div class="progress progress-success-purple progress-striped active">
                                                <div class="bar"></div>
                                            </div>

                                            <div class="tab-content">

                                                <!-- BEGIN STEP 1-->
                                                <div class="tab-pane" id="pills-tab1">
                                                    <h3 style="color:#000000;">STEP 1</h3>
                                                    <div class="form-horizontal">

                                                       <div class="control-group">

                                                                <label class="control-label">Select Lesson Source</label>
                                                                <div class="controls">
                                                                    <select class="span12 chzn-select" data-placeholder="--Please Select Curriculum--" tabindex="1" style="width: 300px;" name="curriculum" id="curriculum">			
                                                                        <!--<option>Select Curriculum Type</option>-->
                                                                        <option value="textbook" <?php if ($curriculum=='textbook') echo 'selected="selected"';?>>Printed Textbook</option>
                                                                        <option value="materials" <?php if ($curriculum=='materials') echo 'selected="selected"';?>>e Textbook</option>
                                                                        <option value="othermaterials" <?php if ($curriculum=='othermaterials') echo 'selected="selected"';?>>Other Source Materials</option>
                                                                        <?php if($standards):?>
                                                                        <option value="standards" <?php if ($curriculum=='standards' || $curriculum=='') echo 'selected="selected"';?>>Instructional Standards</option>
                                                                    <?php endif;?>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        <div class="control-group">
                                                            <div class="space20"></div>
                                                            <div class="span12" id="curriculum_instruction">
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <div class="space20"></div>
                                                            <div class="span12">
                                                                <center>
                                                                    <button class="btn btn-large btn-purple" id="savensend" type="submit" name="submit" value="submit">
                                                                        <i class="icon-save icon-white"></i> Save
                                                                    </button>
                                                                </center>
                                                                
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <!-- BEGIN STEP 2-->
                                               
                                            </div>




                                            <ul class="pager wizard">
                                                <li class="previous first purple"><a href="javascript:;">First</a></li>
                                                <li class="previous purple"><a href="javascript:;">Previous</a></li>
                                                <li class="next last purple"><a href="javascript:;">Last</a></li>
                                                <li class="next purple"><a  href="javascript:;">Next</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </form>
                            </div>


                        </div>
                        <!-- END BLANK PAGE PORTLET-->
                    </div>
                </div>

                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  


        <!-- BEGIN FOOTER -->
        <div id="footer">
            UEIS © Copyright 2012. All Rights Reserved.
        </div>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS -->
        <!-- Load javascripts at bottom, this will reduce page load time -->
        <script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>


        <script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
        <!-- ie8 fixes -->
        <!--[if lt IE 9]>
        <script src="js/excanvas.js"></script>
        <script src="js/respond.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>



        <!--common script for all pages-->
        <script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
        <!--script for this page-->
        <script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>
        <script src="<?php echo SITEURLM ?>js/form-component.js"></script>
        <!--start old script -->


        <script type="text/javascript" src="<?php echo SITEURLM ?>js/jquery.form.js"></script>
        <script type="text/javascript">
                                         var siteurlm = '<?php echo SITEURLM; ?>';
        </script>
        <script type="text/javascript" src="<?php echo SITEURLM . $view_path; ?>js/lesson_plan_material.js"></script>
        <LINK href="<?php echo SITEURLM ?>css/video.css" type="text/css" rel="stylesheet">
        <!--end old script -->

        <!-- END JAVASCRIPTS --> 

        <script>
                                     $(function() {
                                         $(" input[type=radio], input[type=checkbox]").uniform();
                                         
                                         $('#curriculum').on('change',function(){
                                             
                                             if($(this).val()=='textbook'){
                                                 $('#curriculum_instruction').html('Save selected source and proceed to Planning Manager module.');
                                             } else if($(this).val()=='materials'){
                                                 $('#curriculum_instruction').html('Save and go to Upload Resources module to upload PDF version of textbook.');
                                             } else if($(this).val()=='othermaterials'){
                                                 $('#curriculum_instruction').html('If PDF, save and go to Upload Resources module to upload PDF version of document.<br />If printed copy, save and proceed to Planning Manager module.');
                                             }
                                         });
                                         if($('#curriculum').val()=='textbook'){
                                             $('#curriculum_instruction').html('Save selected source and proceed to Planning Manager module.');
                                         } else if($('#curriculum').val()=='materials'){
                                                 $('#curriculum_instruction').html('Save and go to Upload Resources module to upload PDF version of textbook.');
                                             } else if($('#curriculum').val()=='othermaterials'){
                                                 $('#curriculum_instruction').html('If PDF, save and go to Upload Resources  module to upload PDF version of document.<br />If printed copy, save and proceed to Planning Manager module.');
                                             }
                                     });



        </script>  
    </body>
    <!-- END BODY -->
</html>