<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::GoalPlan::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo SITEURLM?>css/video.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/goalmedia.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {

	
	
	
	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
        
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
function btnsmall(id)
	{
	 
	 
	 var $msgContainer = $("div#comments");
	
	if($('#comment'+id).val()!='')
	{
	   pdata={};
	   pdata.id=id;
	  
	   pdata.comments=$('#comment'+id).val();
	   $.post('comments/addcomment',{'pdata':pdata},function(data) {
		  
		  
		  if(data.status==1)
		  {
            $.get("comments/getcomments/"+id+"?num=" + Math.random(), function(msg){
$msgContainer.html(msg);
			$('#comment'+id).val('');

});

		 }
		 else
		 {
		 
			 alert('Failed Please Try Again ');
		 
		 }

},'json');		 
	
	
	
	}
	
	}
function getthis(id)
{  
var teacher_id=$('#teacher_id').val();
var year=$('#year').val();
$.get("comments/getgoalcomments/"+id+"/"+teacher_id+"/"+year+"?num=" + Math.random(), function(msg){
//var $msgContainer = $("div#comments");
  //$msgContainer.html(msg);
  	  var $msgContainer1 = $("div#tab"+id);
	  $msgContainer1.html(msg);

			

});
}
function addgoal(id)
{
  
  $('#text').val('');
  if($('#text').val('')=='')
  {
   alert('please Enter Goal');
   return;
  
  }
  $("#dialog").dialog({
			modal: true,
           	height: 250,
			width: 700
			});
 
  $('#goalsubmit').click(function() {
    var text=$('#text').val();
    $.get("comments/addgoal/"+id+"/"+text+"?num=" + Math.random(), function(msg){
	if(msg!==false)
	{
	  $('#dialog').dialog('close');
	  var $msgContainer1 = $("div#tab"+id);
	  $msgContainer1.html(msg);
	
	}
	else
	{
	
	 alert( 'Failed Please Try Again');
	}
 
 });
  });

}
</script>

<style type="text/css">
.small_face
{
width:35px;height:35px
}
.stcommentimg
{
float:left;
height:35px;
width:35px;
border:solid 1px #dedede;
padding:2px;
}
.stcommenttext
{
margin-left:45px;
min-height:40px;
word-wrap:break-word;
overflow:hidden;
padding:2px;
display:block;
font-size:13px;
width:550px;

}
ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 32px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 100%;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 31px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 31px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: 1.2em;
	padding: 0 20px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
.tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left; width: 100%;
	background: #fff;
	margin:0px 0px 0px 0px;
}
.tab_content {
	padding: 15px;
	font-size: 1.2em;
}</style>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php 
		
		if($this->session->userdata("login_type")=='admin')
		{
			require_once($view_path.'inc/pleftmenu.php'); 
		}
		else
		{
			require_once($view_path.'inc/obmenu.php'); 
		
		}
		?>
        <div class="content">
		<form action="goalplan/commentsview/<?php echo $teacher_id;?>" method="post"> 
		<table>
		<tr>
		<td class="htitle">
		Goal Plan
		<input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $teacher_id ?>">
 		</td>
		</tr>
		<tr>
		<td>
		<?php if($year) { $selectyear=$year;} else { $selectyear=date('Y');} ?>
		Year: <select class="combobox1" name="year" id="year">
		<?php for($i=2010;$i<=2045;$i++)
		{
		?>
		<option value="<?php echo $i;?>" <?php if($i==$selectyear) { ?> selected <?php } ?>><?php echo $i;?>-<?php echo $i+1;?></option>
		<?php } ?>
		</select>
		<td>
		<input type="submit" name="yearsubmit" id="yearsubmit" value="Go">
		</td>
		</td>
		</tr>
		<tr>
		<td>Teacher Name:
		<?php echo $teacher_name;?>
		</td>
		</tr>
		</table>
		</form>
		<table width="100%" cellpadding=0 cellspacing=0 style="margin-left:10px;">
		<tr><td>
		<?php if($goalplans!=false) { ?>
	<ul class="tabs">
    <?php 
	$i=0;
	foreach($goalplans as $val) {
	$i++;
	?>
	<li><a href="#tab<?php echo $val['goal_plan_id'];?>" onclick="getthis(<?php echo $val['goal_plan_id'];?>)"><?php echo $val['tab'];?></a></li>
    
	<?php } ?>
	</ul>
<?php } else { ?>
No Goals Found
<?php } ?>
</td>
</tr>

<?php if($goalplans!=false) { ?>
<tr class="tab_container">
<td>
    <?php 
	//$j=0;
	//print_r($goalplans);
	foreach($goalplans as $val) {
	//$j++;
	if($teacherplans!=false) {
	$k=0;
	foreach($teacherplans as $plan)
	{
	
	if($val['goal_plan_id']==$plan['goal_plan_id'])
	{
	$k=1;
	?>
	<div id="tab<?php echo $val['goal_plan_id'];?>" class="tab_content" >
        
		<?php if($year>=date('Y')) { ?>
		<br />
		<div style="font-size:11px;color:#666666;">
		Add Comments:<textarea style="height: 57px;width:600px;" name="comments" id="comment<?php echo $plan['teacher_plan_id']?>"></textarea>
		<input type="submit" class="btnsmall" onclick="btnsmall(<?php echo $plan['teacher_plan_id']?>)" id="<?php echo $plan['teacher_plan_id']?>" name="submit" value="submit">
		</div>
		<?php } ?>
		<div id="description">District Task:<?php echo $val['description'];?></div>
		<div id="observer_description">School Level Task:<?php echo $val['school_description'];?></div>
		<div style="color:#02AAD2;" id="disgoal">
		Goal:<?php echo $plan['text'];?>(<?php echo $plan['added'];?>)
		</div>
		
		<div style="color:#02AAD2;" id="discomments">
		Comments:
		</div>
		<?php 
		if($comments!=false) {
		if($plan['teacher_plan_id']==$comments[0]['teacher_plan_id']) {?>
		<div id="comments">
		<?php
		foreach($comments as $comm) {
		?>
		<div style="padding:4px 0px 4px 0px;border-bottom:1px #cccccc solid;">
		<div class="stcommentimg">
<img src="<?php echo SITEURLM;?>images/<?php echo $comm['avatar'];?>.gif" class='small_face'/>
</div> 
		<div class="stcommenttext">
		<b><?php echo $comm['role']?>:</b> <?php echo $comm['comments']?>
		<b>(<?php echo $comm['created']?>)</b>
		</div></div>
		
		<?php  }?>
		</div>
		<?php } else { ?>  
		<div id="comments"></div>
		<?php }  } else { ?>
		<div id="comments">No Comments  Found</div>
		<?php } ?>
		

		
    </div>
	<?php } } if($k==0) {?>
	<div id="tab<?php echo $val['goal_plan_id'];?>" class="tab_content">
       <div id="nogoal"> 
	   <!--<input type="button" onclick="addgoal(<?php echo $val['goal_plan_id'];?>)" name="add" value="Add">-->
	   </div>
	   <div id="description">District Task:<?php echo $val['description'];?></div>
	   <div id="observer_description">School Level Task:<?php echo $val['school_description'];?></div>
	    
    </div>
	<?php } } else {?> 
	<div id="tab<?php echo $val['goal_plan_id'];?>" class="tab_content">
         <div id="nogoal"> </div> 
		 <div id="description">District Task:<?php echo $val['description'];?></div>
		 <div id="observer_description">School Level Task:<?php echo $val['school_description'];?></div>
		 <!--<input type="button" onclick="addgoal(<?php echo $val['goal_plan_id'];?>)" name="add" value="Add">-->
    </div>
	
	<?php } } ?>
    
	</td>
	</tr>
	<?php }?>
	</table>
        <!-- Media Start -->
		
		
	<table>
	<tr>
	<td align="center">
	<div id="mediadetails" style="display:none;">
		<input type="hidden" id="mediapageid" value="">
		<input type="hidden" id="media_school_id" name="media_school_id" value="<?php echo $school[0]['school_id'];?>">
		<div id="mediamsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
		
		<!-- Media End -->
		
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Add Goal" style="display:none;">
<table cellpadding="0" cellspacing="0" border=0 class="jqform1">
<tr>
				<td valign="top" >
				<font color="red">*</font>Goal Paln:
				</td>
				<td  valign="top"  style="height:40px;">
				<textarea name="text" id="text" style="width:400px;" ></textarea>
				</td>
				
			</tr>
			<tr>
			<td align="center" colspan="2"><input type="submit" id="goalsubmit" name="goalsubmit" value="submit"></td>
			</tr>
</table> 
</div>
</body>
</html>