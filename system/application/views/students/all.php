<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Students::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo SITEURLM?>css/video.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/teacherstudent.js" type="text/javascript"></script>
<script type="text/javascript">
var site_url='<?php echo SITEURLM;?>';
var periods=<?php echo json_encode($periods);?>;
$(document).ready(function() {

	
	
	
	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
        
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});

</script>
<style type="text/css">
ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 100%; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 100%;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 31px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 31px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;

	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: 1.2em;
	padding: 0 20px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
.tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	margin-left:0px;
	float: left;
	background: #fff;
	
}
.tab_content {
	padding: 20px;
	font-size: 1.2em;
	width:610px;
}
</style>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">        
		
		
		<ul class="tabs">
		<?php 
		
	
	
		for($i=0;$i<=6;$i++)
		{
		
		 switch($i) {
        case '0': $numDaysToMon = 'Mon'; break;
        case '1': $numDaysToMon = 'Tue'; break;
        case '2': $numDaysToMon = 'Wed'; break;
        case '3': $numDaysToMon = 'Thu'; break;
        case '4': $numDaysToMon = 'Fri'; break;
        case '5': $numDaysToMon = 'Sat'; break;
        case '6': $numDaysToMon = 'Sun'; break;   
    }
		?>
		
		
		
		
   
	<li><a href="#tab<?php echo $i;?>" onclick="getthis(<?php echo $i;?>)" ><?php echo $numDaysToMon;?></a></li>
    
	<?php } ?>
	
	</ul>
	
		<?php 
		for($i=0;$i<=6;$i++)
		{
		?>
		<div id="tab<?php echo $i;?>" class="tab_content" >
		<table>
		<tr>
		<td>
		Replicate Student Roster: &nbsp;&nbsp;&nbsp; Select Day:<select name="copy_day_<?php echo $i;?>" id="copy_day_<?php echo $i;?>">
		<option value="all">All Days</option>
		<?php 
		for($dj=0;$dj<=6;$dj++)
		{
		if($dj!=$i)
		{
		 switch($dj) {
        case '0': $numDaysToMonday = 'Mon'; break;
        case '1': $numDaysToMonday = 'Tue'; break;
        case '2': $numDaysToMonday = 'Wed'; break;
        case '3': $numDaysToMonday = 'Thu'; break;
        case '4': $numDaysToMonday = 'Fri'; break;
        case '5': $numDaysToMonday = 'Sat'; break;
        case '6': $numDaysToMonday = 'Sun'; break;   
    }
	
		?>
		<option value="<?php echo $dj;?>"><?php echo $numDaysToMonday;?></option>
		<?php 
		} }
		?>
		</select>
		&nbsp;&nbsp;&nbsp;<input type="button" name="copy" id="copy" Value="Copy" onclick="copytodays(<?php echo $i;?>)">
		</td>
		</tr>
		</table>
		<?php 
		if($periods!=false)
		{ 
		foreach($periods as $periodvalue)	
		{
		?>
		<br />
		<table>		
		<tr>
		<td>
		Time:<?php echo $periodvalue['start_time'].'-'.$periodvalue['end_time'];?>
		</td>
		</tr>
		<tr>
		<td align="center">				
			<div class="msgContainer_<?php echo $i;?>_<?php echo $periodvalue['period_id'];?>" id="msgContainer">
				</div>
			
		</td>
		</tr>
		</table>
	<?php } } else { 
	
	echo 'No Periods Found.';
	}
	?>
	
	

		
		</div>
		
		<?php }		
		
		?>
		

        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Select Student" style="display:none;"> 

<form name='studentform' id='studentform' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform" style="width:600px;">
<tr><td class='style1'></td><td colspan="2">
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			
			<tr>
				
				<td valign="top" class="style1" colspan="2">
				<font color="red">*</font>Select Grade:
				</td>
				<td valign="top" colspan="2">
				<select name="grade" id="grade" >
				<option value=''>-Please Select-</option>
				<?php if($grades!=false)
				{
				foreach($grades as $gradeval)
				{
				?>
				<option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
				<?php
				}
				}
				?>
				</select>
				
				</td>
				
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
				<td valign="top" class="style1">
				First Name
				</td>
				<td valign="top">
				Last Name				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1" colspan="2">
				Student Search:
				</td>
				<td valign="top">
				<input type="text" name="fname" id="fname" value="">				
				</td>				
				<td valign="top">
				<input type="text" name="lname" id="lname" value="">				
				</td>
				<td valign="top">
				<input type="button" name="search"  value="Search" onclick="students()">				
				</td>
				
			</tr>
			<tr>
				
				<td valign="top" class="style1" colspan="2">
				<font color="red">*</font>Select Student:
				</td>
				<td valign="top"  colspan="2">
				<select name="student_id" id="student_id" >
				<option value="">-Please Select-</option>
				</select>
				
				</td>
				
			</tr>
			
			<tr>
				<td valign="top" class="style1">
				<input type="hidden" id="class_room_id" name="class_room_id"  value=''>
				<input type="hidden" id="day" name="day" value=''>
				<input type="hidden" id="period_id" name="period_id" value=''>
				</td>
				
			</tr>
			
			
						
</tr><tr><td valign="top" colspan="2"></td><td valign="top"><input class="btnbig" type='submit' name="submit" id='studentadd' value='Add' title="Add New"> <input class="btnbig" type='button' name='cancel' id='cancel' value='Cancel' title="Cancel"></td></tr></table></form>
</div>
</body>
</html>
