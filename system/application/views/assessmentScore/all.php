<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    
  <style>
.even{ background-color:#FFFFFF;}
			.odd{ background-color:#EEF9E3;}
		.black_overlay{
			display: none;
			position: absolute;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 167%;
			background-color: black;
			z-index:1001;
			-moz-opacity: 0.8;
			opacity:.80;
			filter: alpha(opacity=80);
		}
		.white_content {
			display: none;
			position: fixed;
			top: 25%;
			left: 34%;
			width: 30%;
			height: 40%;
			padding: 16px;
			border: 5px solid #657455;
			background-color: white;
			z-index:1002;
			overflow: auto;
		}
	
	.btn_upload{
	 background-color: #363636;
    background-image: -moz-linear-gradient(center top , #444444, #222222);
    background-repeat: repeat-x;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
    color: #FFFFFF;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	border-radius:3px 3px 3px 3px;
	}
	</style>
<style type="text/css" media="screen">
	
	#paginationstudent  
	{
		padding-top:10px;
		font-size: 12px;
    font-weight: bolder;	
	}
	#paginationstudent a, #paginationstudent strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;


	}

	#paginationstudent strong, #paginationstudent a:hover {
	 font-weight: normal;
	 background: #657455;
	}		
	</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/headerv1.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">
<?php if($this->session->userdata('login_type')=='observer') { ?>
        
        <?php require_once($view_path.'inc/observermenu.php'); 
		}
		
		else
		{
					require_once($view_path.'inc/developmentmenu_new.php');
		}
		?>
          <!-- BEGIN SIDEBAR MENU -->
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                       Welcome <?php echo $this->session->userdata('username');?>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                           <a href="<?php echo base_url();?>tools/data_tracker">Data Analysis Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
							<a href="<?php echo base_url();?>assessscoring">Assessment Scoring guides</a>
                       </li>
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Assessment Scoring guides</h4>
                          
                         </div>
                        
                        <div class="widget-body">
        <?php   if($this->session->userdata('login_type')=='user') {?> 
  <div style="float:right;">
        <a href = "javascript:void(0)" class="btn btn-success" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"> <i class="icon-plus"></i >Add Document</a>
     
            </div>
        <?php } ?>
             
             
                         <div class="space20"></div>
                              
   
        <table align="center">
		<tr>
		<td >
		<input type="hidden" name="countries" id="countries" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
		<input type="hidden" name="states" id="states" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
		<input type="hidden" name="district" id="district" value="<?php echo $this->session->userdata('district_id') ?>" >
		</td>
		
		</tr>
		<?Php /* ?>
		<tr>
		<td>
		School:
		</td>
		
		<?php if($this->session->userdata('login_type')=='observer')
		{
		?>
		<td>
		<?php
		echo $this->session->userdata('school_name');
		?>
		<input type="hidden" name="school" id="school" value="<?php echo $this->session->userdata('school_id');?>">
		</td>
		<?php }  else { ?>
		<td>
		<select class="combobox" name="school" id="school">
		<option value="all">All</option>
		<?php if(!empty($school)) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>" <?php if(isset($school_id) && $school_id==$val['school_id'] ) {?> selected <?php  } ?> ><?php echo $val['school_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getschool" id="getschool" value="Submit">
		</td>
		<?php } ?>
		
		</tr>
		<?Php */ ?>
		
		</table>
        
        
		<table>
		<tr>
		<td colspan="2" align="right">
		
		</td>
		</tr>
		</table>
		<div id="teacherdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
            
            
            
            <div>
           
			
			
             <table class='table table-striped table-bordered' id='editable-sample'>
            <tbody>
            <tr>
            <th>Title</th><th>Description</th><th>Download</th>
            </tr>
              <?php
		   	//$this->load->model('Assesstesttypemodel');
		   /// $docs = Assesstesttypemodel::getallscoredoc();
			if(count($docs)>0)
			{
			$i=1;
			
			foreach($docs as $key => $value)
			{
				?>
                <tr class="<?php if($i%2==0){ echo "even"?>  <?php } else{  echo "odd";}?>">
            <td><?php echo $value['title']; ?></td>
            <td><?php echo $value['description']; ?></td>
        
        
        
         <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>    
        
         <td>
           <a target="_new" href="<?php echo '/testbank/workshop/docfile/'.$value['filename']; ?>">
            <img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif" width="25" height="25" alt="<?php echo $value['filename']; ?>" />
         </a>
          </td>
          
     <?php
		  }
		   else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){?>
           
           <td>
           <a target="_new" href="<?php echo '/docfile/'.$value['filename']; ?>">
            <img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif" width="25" height="25" alt="<?php echo $value['filename']; ?>" />
         </a>
          </td>
			   
			    <?php
             }else if($_SERVER["HTTP_HOST"]=="workshop2.ueisworkshop.com"){?>
           
           <td>
           <a target="_new" href="<?php echo '/docfile/'.$value['filename']; ?>">
            <img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif" width="25" height="25" alt="<?php echo $value['filename']; ?>" />
         </a>
          </td>
			   
			    <?php
             }
			 else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){?>
           
           <td>
           <a target="_new" href="<?php echo '/docfile/'.$value['filename']; ?>">
            <img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif" width="25" height="25" alt="<?php echo $value['filename']; ?>" />
         </a>
          </td>
			   
			    <?php
             }
                 else if($_SERVER["HTTP_HOST"]=="localhost"){
             ?>
         <td>
           <a target="_new" href="<?php echo '/testbank/workshop/docfile/'.$value['filename']; ?>">
            <img src="<?php echo SITEURLM;?>images/PDF-Icon2.gif" width="25" height="25" alt="<?php echo $value['filename']; ?>" />
         </a>
          </td>
         
         <?php
             }
             ?>
          
          
          
          
            </tr>
	
                <?php
				$i++;
			}
			}
			else
			{
				?>
                <tr><td colspan="2">No Record Found</td></tr>
                <?php
			}
		   
		   
		   ?>
            </tbody>
            </table>
    
            </div>
       
        <div id="paginationstudent">
        <?php echo $this->paginationnew->create_links(); ?>
        </div>
            
		</div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   
   <!--notification -->
<div id="dialog" title="Assessment Scoring Guides" style="display:none;"> 

<form name='teacherform' id='teacherform' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Assessment Name :
				</td>
				<td valign="top">					
					<input class='txtbox' type='text'  id='firstname' name='firstname'>
					<input  type='hidden'  id='teacher_id' name='teacher_id'>
					<input  type='hidden'  id='assessment_id' name='assessment_id'>
					<input  type='hidden'  id='studentuser_id' name='studentuser_id'>
					<input  type='hidden'  id='quizid' name='quizid'>					
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Quiz Name :
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='quizname' name='quizname'>				
				</td>				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Quiz Desc :
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='quizdesc' name='quizdesc'>				
				</td>				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Pass Score :
				</td>
				<td valign="top" >
				<input class="txtbox" type='text'  id='pass_score' name='pass_score'>
				
				</td>
				
			</tr>			
		
			<tr>
				<td valign="top" >
				<input type="hidden" name="country_id" id="country_id" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
				<input type="hidden" name="state_id" id="state_id" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
				<input type="hidden" name="district_id" id="district_id" value="<?php echo $this->session->userdata('district_id') ?>" >
				
				</td>				
			</tr>
			
						
</tr><tr><td valign="top"></td><td valign="top">

    <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<button type='submit' name="submit" id='teacheradd' value='Add' class="btn btn-success"><i class="icon-plus"></i> Add Student</button>
    
    
    </td></tr></table></form>
</div>


        <!-- Add Document Popup Starts here-->
        
        
        
<div id="light" class="white_content">
<div style="background-color:#C1B38E; margin: -16px 0 0 -16px; padding: 5px; width: 426px;">
      
      
      <span style=" color: #222222; font-family: Tahoma;">Add Document</span> 
      
      <span style=" float: right;   margin-right: 5px;">
      
      <a  style="color:#222222" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" href="javascript:void(0)" class="ui-icon ui-icon-closethick">
      
      <img src="<?php echo SITEURLM;?>images/popup-closeButton.png" width="25" height="25"/ style="margin: -3px -4px 0 0;">
      
      </a>
      </span>
      
       </div>

<div style="margin: 15px 0 0 5px;">
<form name="adddoc" id="adddoc" method="post" enctype="multipart/form-data" action="assesstest/uploaddoc1" >
<table>

<tr>
<td>Title</td><td><input type="text" name="title" id="title" /></td>
</tr>
<tr>
<td>Description</td><td><textarea name="description" id="description" rows="3" style="resize:none; width:300px;"></textarea></td>
</tr>
<tr>
<td>Upload Document</td><td><input type="file" name="file" id="file" /></td>
</tr>
<tr>
<td></td><td>

<button type='button' name="uploaddoc" id='uploaddoc' value='Add' class="btn btn-success" onclick="validatedoc()"><i class="icon-plus"></i> Add Student</button>

</td>
</tr>
<input type="hidden" name="action" value="scoretype" />
</table>
</form>

</div>
  
</div>
		<div id="fade" class="black_overlay"></div>
   <!-- notification ends -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!--<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.form.js"></script>

 
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script src="<?php echo SITEURLM.$view_path; ?>js/assessScoreall.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
function validatedoc()
{

	var title = document.adddoc.title.value;
	var desc = document.adddoc.description.value;
	var file = document.adddoc.file.value;
	
	if(title=="")
	{
		$("#title").css('border-color','red');
		document.adddoc.title.focus();
		return false;
	}
	else
	{
	$("#title").css('border-color','#70B8BA');	
	}
	
	
	if(desc=="")
	{
		$("#description").css('border-color','red');
		document.adddoc.description.focus();
		return false;
	}
	else
	{
	$("#description").css('border-color','#70B8BA');	
	}
	
	if(file=="")
	{
		$("#file").css('border-color','red');
		document.adddoc.file.focus();
		return false;
	}
	else
	{
	$("#file").css('border-color','#70B8BA');	
	}
	
document.adddoc.submit();
	
}
</script>
   <!-- END JAVASCRIPTS --> 

   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>