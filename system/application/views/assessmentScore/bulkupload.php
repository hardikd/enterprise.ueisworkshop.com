<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Teachers::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/teacher_dist.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    	var options = { 
	    target:        '#output2',
	    success:       processJson,  // post-submit callback 
	    dataType:  'json' 
		};
	// bind form using ajaxForm
    $('#uploadForm').submit(function() { 
	        $(this).ajaxSubmit(options); 
	        return false; 
	    });
});
function processJson(pdata) {
if(pdata.status==2)
{
 alert(pdata.msg);
}
else if(pdata.status==1)
{
 alert('Uploaded Sucessfully');
}
else
{

 alert('Failed Please Try Again');

}
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
        <form  id="uploadForm" enctype="multipart/form-data" method="post" action="teacher/sendfile">
		<table >
		<tr>
		<td colspan="4" class="htitle">
		Bulk Upload
		</td>
		</tr>
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { ?>
		<?php foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		Districts:
		</td>
		<td>
		<select class="combobox" name="district" id="district" onchange="school_type(this.value)">
		
		<?php if($district!=false) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } else { ?>
		<option value="">No Districts Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		School:
		</td>
		<td>
		<select class="combobox" name="school" id="school">
		<option value="all">All</option>
		<?php if(!empty($school)) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>" <?php if(isset($school_id) && $school_id==$val['school_id'] ) {?> selected <?php  } ?> ><?php echo $val['school_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		
		</tr>
		<tr>
		<td colspan="4">
		<p class="h4f">Excel Sheet:(Only (.xls,.csv)files accepted) <label> <input id="upload" name="upload" type="file"> </label><label><input class="kwikbtnbrowse" type="submit" value="submit"></label></p>
<p class="h4f"> <label>(Note:1, First Row contains column headings.
2, Columns: FirstName, LastName, Username,password,Employee_number,Email(optional))</label></p>
		</td>
		</tr>
		<tr>
		<td colspan="4">
		Sample Excel Sheet:<a href="<?php echo SITEURLM;?>Teacher_bulk_upload.csv" target="_blank"  style="color:#ffffff"><img src="<?php echo SITEURLM;?>images/excel_icon.jpg"></a>
		</td>
		</tr>
		<tr>
		<td colspan="4">
		Email to Text Communications:<a href="<?php echo SITEURLM;?>EmailstoTextMessages.pdf" target="_blank"  style="color:#ffffff"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a>
		</td>
		</tr>
		</table>
		</form>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
