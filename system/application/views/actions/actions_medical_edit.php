﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>Needs Medical Child</td>
     <td>Action Medical</td>
     <td>status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page-1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="medical_<?php echo $data['action_medical_id'];?>">
   <td><?php echo $con;?></td>
  <td><?php echo $data['intervention_strategies_medical'];?></td>
   <td><?php echo $data['actions_medical'];?></td>
   <td><?php echo $data['status'];?></td>
      <td>
       <button class="edit_action_medical btn btn-primary" type="button" name="<?php echo $data['action_medical_id'];?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
    <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data['action_medical_id'];?>" />
      <td>
      <button type="Submit" id="remove_action_medical" value="Remove" name="<?php echo $data['action_medical_id'];?>" data-dismiss="modal" class="remove_action_medical btn btn-danger"><i class="icon-trash"></i></button>
      </td>
    
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_action_medical ').click(function(){
  var action_medical_id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'actions_medical/edit';?>/"+action_medical_id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#actions_medical_id').val(result[0].action_medical_id);
       $('#need_medical_child_id').val(result[0].need_medical_child_id);
       $('#actions_medical_name').val(result[0].actions_medical);
       $('#status').val(result[0].status);
       console.log(result[0].actions_medical);
       $("#dialog").dialog({
      modal: true,
            height:250,
      width: 450
      });
       }
     });  
});

$(".remove_action_medical").click(function(){
var action_medical_id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'actions_medical/delete';?>",
      data: { 'action_medical_id': action_medical_id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#medical_"+action_medical_id).css('display','none');
          alert('Successfully removed medical list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

