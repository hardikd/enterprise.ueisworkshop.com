﻿   <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>Needs school child</td>
     <td>Action School</td>
     <td>status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page-1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="school_<?php echo $data['action_school_id'];?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data['intervention_strategies_school'];?></td>
   <td><?php echo $data['actions_school'];?></td>
   <td><?php echo $data['status'];?></td>
      <td>
      <button class="edit_action_school btn btn-primary" type="button" name="<?php echo $data['action_school_id'];?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data['action_school_id'];?>" />
                <td>
        <button type="Submit" id="remove_action_school" value="Remove" name="<?php echo $data['action_school_id'];?>" data-dismiss="modal" class="remove_action_school btn btn-danger"><i class="icon-trash"></i></button>
        
                </td>
    
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_action_school ').click(function(){
  var action_school_id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'actions_school/edit';?>/"+action_school_id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#actions_school_id').val(result[0].action_school_id);
       $('#actions_school_data').val(result[0].actions_school);
       $('#needs_school_child_id').val(result[0].needs_school_child_id);
       $('#status').val(result[0].status);
       console.log(result[0].actions_school);
       $("#dialog").dialog({
      modal: true,
            height:250,
      width: 450
      });
       }
     });  
});

$(".remove_action_school").click(function(){
var action_school_id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'actions_school/delete';?>",
      data: { 'action_school_id': action_school_id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#school_"+action_school_id).css('display','none');
          alert('Successfully removed School list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

