<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Parents::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    	var options = { 
	    target:        '#output2',
	    success:       processJson,  // post-submit callback 
	    dataType:  'json' 
		};
	// bind form using ajaxForm
    $('#uploadForm').submit(function() { 
	        $(this).ajaxSubmit(options); 
	        return false; 
	    });
});
function processJson(pdata) {
if(pdata.status==2)
{
 alert(pdata.msg);
}
else if(pdata.status==1)
{
 alert('Uploaded Sucessfully');
}
else
{

 alert('Failed Please Try Again');

}
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <form  id="uploadForm" enctype="multipart/form-data" method="post" action="parents/parents_students_sendfile">
		<table align="center">
		<tr>
		<td class="htitle">
		Parents Students Bulk Upload
		</td>
		</tr>
		
		<tr>
		<td>
		<p class="h4f">Excel Sheet:(Only (.xls,.csv)files accepted) <label> <input id="upload" name="upload" type="file"> </label><label><input class="kwikbtnbrowse" type="submit" value="submit"></label></p>
<p class="h4f"> (Note:<br/>1, First Row contains column headings.<br/>
2, Columns: FirstName(Parent A), LastName (Parent A), Email(Parent A),Language(Parent A),Username,Password,FirstName(Parent B optional),LastName(Parent B optional),Email(Parent B optional),Language(Parent B optional)),Student FirstName,Student LastName,Student Grade,Student Number(Alpha Numeric)
<br/>
3,Language Option:default(<b>en</b>  English) Give <b>en</b> in Excel Sheet<br/>
<b>zh-CN</b>  Chinese (Simplified) Give <b>zh-CN</b> in Excel Sheet<br/>
<b>es</b> Spanish Give <b>es</b> in Excel Sheet
</p>
		</td>
		</tr>
		<tr>
		<td>
		Sample Excel Sheet:<a href="<?php echo SITEURLM;?>Parent_Student_bulk_upload.csv" target="_blank"  style="color:#ffffff"><img src="<?php echo SITEURLM;?>images/excel_icon.jpg"></a>
		</td>
		</tr>
		<tr>
		<td>
		Email to Text Communications:<a href="<?php echo SITEURLM;?>EmailstoTextMessages.pdf" target="_blank"  style="color:#ffffff"><img src="<?php echo SITEURLM;?>images/pdf_icon.gif"></a>
		</td>
		</tr>
		</table>
		</form>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
