<style type="text/css">
	 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
.blue{
font-size:9px;
}

</style>

<page backtop="7mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
        <page_header> 
              <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 500px">
                    <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/pencil1.png"/>&nbsp;Student Progress Report</b></div>
		</div>
        </page_header> 
        <page_footer> 
                <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
		<br />
                <table style="margin-left:10px;">
                    <tr>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                        </td>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                           Page [[page_cu]] of [[page_nb]] 
                        </td>
                        <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            Printed on: <?php echo date('F d,Y');?>
                        </td>
                    </tr>
                </table>
        </page_footer> 


        <table cellspacing="0" style="width:750px;border:2px #939393 solid;">
            <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                <td colspan="3" style="padding-left:5px;padding-top:2px;height:36px;" >
                    <b> <?php echo ucfirst($this->session->userdata('district_name'));?> | <?php echo @$schoolname[0]['school_name'];?></b>
                </td>
                <td style="text-align:right;padding-right:10px;"></td>
            </tr>
            <tr >
                <td colspan="2" >&nbsp;</td>
            </tr>
            <tr style="margin-top:20px;">
                <td style="padding-left:10px;width:350px;color:#939393; vertical-align: top;">
                    <b>Student:</b> <?php echo $studentname; ?>   
                    <br />
                    <br />
                    <b>Grade:</b> <?php echo $studentrecord[0]['grade_name'];?>
                    <br />
                    <br />
                    <b>Teacher:</b> <?php echo $teacher_name; ?>   
                    <br />
                    <br />
                    <b>Room Number:</b> <?php echo $room;?>
                    <br />
                    <br />
                    <b>Assessment:</b> <?php echo $assignment_name;?>
                    <br />
                    <br />
                    <b>Assessment Time:</b> <?php echo $assessmentdate;?>
        </td>
                <td style="padding-left:10px;width:350px;color:#939393;height:50px;">
                    <table style="border:1px solid #939393;height:50px;width:350px;">
                        <tr>
                            <td style="width:350px;height:130px;vertical-align: top;">
                                <b>Description:</b> <?php echo $description;?>
                            </td>
                        </tr>
                    </table>
                </td>
        </tr>
        
                <tr >
                        <td colspan="2" >
                       &nbsp;
                        
                    </td>
                </tr>
        </table>
		<br/><br/>
		<table class="gridtable">
            <tr>
                <th style="width:165px;">Assignment Name</th>
                <th style="width:150px;text-align:center;">Number Correct</th>
                <th style="width:150px;text-align:center;">Percentage Correct</th>
                <th style="width:150px;text-align:center;">Assessment Pass Score</th>
            </tr>
            <tr> 
                <td style="vertical-align: top; "><?php echo @$testname[0]['assignment_name'];?></td>
                <td style="vertical-align: top; text-align: center;"><?php echo $individualtestdetail[0]['pass_score_point']; ?></td>
                <td style="vertical-align: top; text-align: center;"><?php echo $individualtestdetail[0]['pass_score_perc'].' '.'%'; ?></td>
                <td style="vertical-align: top; text-align: center;"><?php echo @$testname[0]['pass_score'];?></td>
            </tr>
        </table>
        <br />
        <br />
        <table class="gridtable">
            <tr>
                <th style="width:165px;">Skill</th>
                <th style="width:150px;text-align:center;">Number of Questions</th>
                <th style="width:150px;text-align:center;">Correct</th>
                <th style="width:150px;text-align:center;">Incorrect</th>
            </tr>
            <?php
        $cluster =  array();

$questionids = $this->selectedstudentreportmodel->getrepeatclusterinfo($quizid[0]['quiz_id']);   
        
      foreach($questionids as $k => $v)
      {
      ?>
      <tr bgcolor="#f2f2f2">
      <td width="53%"><?php $cl = $v['test_cluster'];  
       // html_entity_decode($v['test_cluster'],ENT_QUOTES);
       //  htmlspecialchars($v['test_cluster'],ENT_QUOTES);  
      echo str_replace('Ã­','í',$cl); ?></td>
       <td style="text-align:center;">
    <?php
    $clu = '';
    $clu1 = $v["test_cluster"];
    $clu = str_replace('"','*',$clu1);
    //$clu = utf8_encode($v["test_cluster"]);
    //$clu = htmlentities($v["test_cluster"],ENT_QUOTES);
    
        echo ''.$v['cnt'].'</td>';
        
        $res= $this->selectedstudentreportmodel->displayQuestionsResult($individualtestdetail[0]['assignment_id'],$v["test_cluster"],$individualtestdetail[0]['user_id']);
        
        echo '<td style="text-align:center;">'.$res["disptrueQuecount"].'</td><td style="text-align:center;">'.$res["dispfalseQuecount"];
     ?>
      
      </td>
      </tr>
      <?php
      }
     
      ?>
        </table>
		   
        
                  
   </page>
