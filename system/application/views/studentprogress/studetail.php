<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/> 
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />


<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.12/video.js"></script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
  
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                      <!--<a href="#jobaidesModal" data-toggle="modal" ><span>Job Aides</span>&nbsp;<i class="icon-folder-open"></i></a>-->
                      <a href="#myModal-video" data-toggle="modal" style="margin-right:5px;"><span>Video Tutorial</span>&nbsp;<i class="icon-play-circle"></i></a>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>studentprogress">Student Progress Report</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Student Progress Report</h4>
                          
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                          
                          
                                   
                                   <div class="space20"></div>
                                   <?php if($this->session->userdata("login_type")=='user'):?>
                                   <form class="form-horizontal" action="" method="post">
                                   <div class="control-group">
                                             <label class="control-label">Select Observer</label>
                                             <div class="controls">
                                        <select name="grades" id="grades" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" >
                                            <option value=""></option>
                                            <?php foreach($observers as $observer):?>
                                                <option value="<?php echo $observer['observer_id'];?>"><?php echo $observer['observer_name'];?></option>
                                            <?php endforeach;?>
                                        </select>
                                             </div>
                                   </div>
                                   </form>
                                   <?php endif;?>
                                   
                                   <div class="space15"></div>
                                   <table>
                                   <tr>
                                   <td>
                                    <form action="" method="post" class="form-horizontal" name="searchstudent" >
                                   <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
                  <select name="grades" id="grades" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" >
                                  <?php  // echo "<pre>"; print_r($school_grade); ?>
                  <option value=""></option>
                      <?php
                    if(isset($school_grade)) $countst = count($school_grade["grades"]);
                    if(isset($school_grade) && $countst >0){
                        
                    foreach($school_grade["grades"] as $k=> $v)
                    {
                    ?>
                     <option value="<?php echo $v["dist_grade_id"];?>"> <?php echo $v["grade_name"]; ?></option>
                    
                    <?php } }?>
                       </select>
                                  </div>
                                         </div> 
                      <?php if(isset($school_grade) && $countst >0){?>
         <input type="hidden" name="schools" id="schools" value="<?php echo $school_grade["school_id"][0]["school_id"];?>" />
         <?php }?>
        					

                            <div class="control-group">
                                             <label class="control-label">Select Assessment</label>
                                             <div class="controls">
	<select name="assessment" id="assessment" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
							<option value=""  selected="selected">Please Select</option>
                            
                               <?php    if(isset($assessment)){
                             foreach($assessment as $k=> $v)
                                {
									//print_r($v);exit;
                                ?>
                                 <option value="<?php echo $v["id"];?>"> <?php echo $v["assignment_name"]; ?></option>
                            		 <?php } }?>
                                   </select>
                                              </div>
                                         </div> 
                                         
                                        
                                         <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
		<input type="button" class="btn btn-small btn-purple" name="answer" value="Search" onClick="checkvalid()" style=" padding: 5px; " />
                         		</div>
        
         				   </div> 
                            </form>   
                                </td></tr> 
                                </table>
                                
                                  <div id="scoredetails" style="display:none;">
      							  <input type="hidden" id="pageid" value="">
        								<div id="msgContainer"> </div>
      							</div>
                                
                                <div class="space20"></div>
                                <div id="reportDiv"  style="display:block;" class="answer_list" >
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Student Details</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                   <div class="space20"></div>
                               
       <table  width="100%" align="center" id="loader" style="display:none">
          <tbody>
          <tr> <td>&nbsp; </td> </tr>
            <tr style="width:100px"> <td align="center" valign="bottom" >
        <img src="<?php echo LOADERURL;?>"/ style="vertical-align:middle"> </td>
        </tr></tbody></table>
        
       <div id="reporthtml"> 
           <table class="table table-striped table-bordered" id="editable-sample loader">
          <thead>
            <tr class="tchead">
              <th width="8%">Sr.No.</th>
              <th width="15%">First Name</th>
              <th class="sorting" width="15%">Last Name</th>
              <th class="no-sorting" width="26%">Email</th>
              <th class="no-sorting" width="14%">Grade</th>
              <th class="no-sorting" width="22%">School Name</th>
            </tr>
            </thead>
                 
            <?php
			
			if(!empty($records))
			{ $cnt =0;
			foreach($records as $record)
			{ $cnt ++;
		?>   
             
            <tr class="tcrow2">
             <td  class="center hidden-phone" align="center"><?php echo  $cnt;?></td>
              <td align="center"><?php echo $record['Name'];?></td>
              <td class="hidden-phone" align="center"><?php echo $record['Surname'];?></td>
              <td class="hidden-phone" align="center"><?php echo $record['email'];?></td>
              <?php
				 $grades=$this->Studentprogressmodel->getgradename($record['grade_id']);
				?>
                 <td class="center hidden-phone" align="center"><?php echo @$grades[0]['grade_name'];?></td> 
              <?php
				 $schools = $this->Studentprogressmodel->getcschools($record['school_id']);
				?>
              <td class="center hidden-phone" align="center"><?php echo @$schools[0]['school_name'];?></td>
                         
            </tr>
            	
            <?php
			}
			}
			else
			{
				?>
                <tr class="tcrow2"><td colspan="6">No Record Available</td></tr> 
                <?php
			}
		?>
           </tbody>
          
        </table>
       
        <div id="paginationstudent">
        <?php echo $this->paginationnew->create_links(); ?>
        </div>
		</div>               
                           
                                   
                         </div>             
                               </div>
                               </div>      
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->
   <div id="myModal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:700px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Student Progress Report</h3>
                                </div>
                              
                                <div class="modal-body">
                                        
                                    <video id="example_video_1" class="video-js vjs-default-skin"
  controls preload="auto" width="640" height="360"
 
  data-setup='{"example_option":true}'>
 <source src="<?php echo SITEURLM;?>convertedVideos/Assessment Progress ReportConverted.mp4" type='video/mp4' />
 
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
                                </div>
                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<!--<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>-->
                                </div>
                    
                            </div>
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   
    
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <!--script for this page-->
  
 
  
  <script>
      jQuery('#myModal-video').on('hidden.bs.modal', function (e) {
                var myPlayer = videojs('example_video_1');
                myPlayer.pause();
              });
               
          $('#myModal-video').modal('show');
          var myPlayer = videojs('example_video_1');
        myPlayer.play();
      
  </script>
 
   
   <!-- END JAVASCRIPTS --> 
   <!--start old script -->
  
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light2').style.display='none';
	 document.getElementById('fade2').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light1').style.display='none';
	 document.getElementById('fade1').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light3').style.display='none';
	 document.getElementById('fade3').style.display='none';
	   } 
});
</script>

<script type="text/javascript">
function checkvalid()
{
   
	var school = document.getElementById('schools').value;
	var grade = document.getElementById('grades').value;
	var assessment = document.getElementById('assessment').value;
	<?php if(isset($school_type)) {?>
	var schooltype=document.getElementById('schools_type').value;

	if(schooltype=="")
	{
		alert('Please select school type');
		return false;
	}
	 <?php }?>
	 
	if(school=="")
	{
		alert('Please select school');
		return false;
	}
	if(grade=="")
	{
		alert('Please select grade');
		return false;
	}
	if(assessment=="")
	{
		alert('Please select Assessment');
		return false;
	}
getstudentdetail();
}
function getstudentdetail()
{
	var grade = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var assessment = document.getElementById('assessment').options[document.getElementById('assessment').selectedIndex].value;
	var school = document.getElementById('schools').value;
	
	$("#reporthtml").html('&nbsp;');
		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/studentprogress/getstudentdetailtable?schools='+school+'&grades='+grade+'&assignment_id='+assessment,
		success:function(result)
		{ 
			$("#loader").hide();
			$("#reporthtml").html(result);
		}
		});
}
function allstudentreport()
{
	var assessment = document.getElementById('selectedassignment').value;
	var stud = [];
	 $('input:hidden[name^="studidarray"]').each(function() {
		stud.push($(this).val());
	});
	var cntstud = stud.length;
	for(var i =0; i<cntstud ;i++)
	{
		var url = '<?php base_url();?>studentprogress/studentreportaspdf?user_id='+ stud[i] +'&assessment_id='+assessment;
		window.open(url, "_blank",'height=650, width=1300,scrollbars=1, resizable=1');
	}
}
</script>
   <!--end lod script -->
   
     

   
   
 
</body>
<!-- END BODY -->
</html>