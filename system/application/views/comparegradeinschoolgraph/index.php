<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop:::Grade level Performance by school::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/jscal2.js"></script>
<script src="<?php echo SITEURLM?>Quiz/js/lang/en.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>Quiz/css/jscal2.css" />
 
<script type="text/javascript">
  updategrade(94);
function updatecategory(tag)
{
	var gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
 	
	 if(gradeid!=-1 && schoolid!= -1 && tag!=-1 && fDate!='' && tDate!='')
	{	
		$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/comparegradeinschoolgraph/getReportHtml?cat_id='+tag+'&grade_id='+gradeid+'&school_id='+schoolid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 
			$("#loader").hide();
			getGraph1(clounmValue);
		}
		});
	} 
}

function updategrade(tag)
{
	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	
	if(catid!=-1 && schoolid!=-1 && tag!=-1 && fDate!='' && tDate!='')
	{	
		$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/comparegradeinschoolgraph/getReportHtml?grade_id='+tag+'&cat_id='+catid+'&school_id='+schoolid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 				
			$("#loader").hide();
			getGraph1(clounmValue);				
		}
		});
	}
}
	
function updateschool(tag)
{
	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;

	if(catid!=-1 && gradeid!=-1 && tag!=-1 && fDate!='' && tDate!='')
	{	
		$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/comparegradeinschoolgraph/getReportHtml?school_id='+tag+'&cat_id='+catid+'&grade_id='+gradeid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 				
			$("#loader").hide();
			getGraph1(clounmValue);				
		}
		});
	}
}

function select_f_date()
{
	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value;
	
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	
	if(catid!=-1 && gradeid!=-1 && schoolid!=-1 && fDate!='' && tDate!='')
	{
		$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/comparegradeinschoolgraph/getReportHtml?school_id='+schoolid+'&cat_id='+catid+'&grade_id='+gradeid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 				
			$("#loader").hide();
			getGraph1(clounmValue);				
		}
		});
	}
}

function select_t_date()
{
	var catid = document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
	var gradeid = document.getElementById('grades').options[document.getElementById('grades').selectedIndex].value;
	var schoolid = document.getElementById('school').options[document.getElementById('school').selectedIndex].value;
	
	var fDate = document.getElementById('f_date').value;
	var tDate = document.getElementById('t_date').value;
	
	if(catid!=-1 && gradeid!=-1 && schoolid!=-1 && fDate!='' && tDate!='')
	{
		$("#container").html('&nbsp;');
  		$("#loader").show();
		$.ajax({
		url:'<?php echo base_url();?>/comparegradeinschoolgraph/getReportHtml?school_id='+schoolid+'&cat_id='+catid+'&grade_id='+gradeid+'&fdate='+fDate+'&tdate='+tDate,
		success:function(clounmValue)
		{ 				
			$("#loader").hide();
			getGraph1(clounmValue);				
		}
		});
	}
}

function getGraph1(clounmValue)
 {
 //alert(clounmValue);
	//clounmValue = '{"Maven Maths Paper":{"maven":"90.000000","maven1":"50.000000"},"Maven2 Math Test":{"maven":"90.000000"}}';
	$(function () {								
        //$('#container').highcharts({
		var options = {
           chart: {
		   		renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Grade level Performance by school'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: []  		
			              
            },
            yAxis: {
                min: 0,
				max: 100,
                title: {
                    text: 'score Point'
                }
            },
			 tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
	       series: [ //{ name:'maven',data:[30]} 
		   			]
        }
        //alert(clounmValue);
		if(clounmValue!="")
		{
			var result = JSON.parse(clounmValue);
				
			var Dispayname ='';
			var count = 0;
			for (var strDispayname in result) 
			{
				if(count == 0)
				{
					var dispN = result[strDispayname];
					
					for(var strDispN in dispN)
					{			
						if(strDispN != '')
							Dispayname = Dispayname + "'"+strDispN +"',"; 
					}
				}
				count++;
				
			}
			Dispayname = Dispayname.substr(0,Dispayname.length-1);
			eval("options.xAxis.categories = new Array("+Dispayname+");"); //"+Dispayname+"
			
	 		options.series = new Array();		
			var result = JSON.parse(clounmValue);	
			var i=0;				
			for (var strTestName in result) 
			{
			 	options.series[i] = new Object();
				options.series[i].name = strTestName;			
				var objTest = result[strTestName];
		//		var strScore = "";//new Array();
				 arrScore = new Array();
				var j=0;
				for(var strGrade in objTest)
				{			
					if(objTest[strGrade]==null)
						 objTest[strGrade] = 0;
					arrScore[j] = parseFloat(objTest[strGrade]);	 
		//			strScore = strScore + objTest[strGrade] + ",";
					j++;
				}									
			/*	strScore = strScore.substr(0,strScore.length-1);
				var lan = strScore.split(",").length;
				if(lan <= 1)
				{
					strScore = strScore +','+0;
				} */
			//	eval("options.series[i].data = new Array("+strScore+");");	
			/*  	var lan = strScore.split(",").length;
				if(lan > 1)
				{
					eval("options.series[i].data = new Array("+strScore+");");
				}else
				{  
					eval("options.series[i].data = strScore;");
				}  */
				//alert(options.series[i].data);
				options.series[i].data = arrScore;		
			 	i++;	
							 
			}
			//{ name:'maven',
		   		//	 data:[30]}
			
		} 
		chart = new Highcharts.Chart(options);
			
		});			
  }
 
  </script>

</head>
<body>
<div class="wrapper">
  <?php require_once($view_path.'inc/header.php'); ?>
  <div class="mbody">
    <?php require_once($view_path.'inc/obmenu.php'); ?>
    <div class="content">
      <div class="search">
    <!--  <span style="text-decoration:underline; font-size:16px;">Search Students</span>-->
    <fieldset>
    
    <legend> Grade level Performance by school</legend>
         <?php // echo "<pre>";
		//print_r($records); exit;
		?>
   <table width="100%">
      <tr>
              <td align="right" >Category : &nbsp; </td>
          <td>
     <select class="combobox" name="category" id="category" onchange="updatecategory(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
        <?php 
	     foreach($records['catarray'] as $key => $value)
		{
			if(isset($_POST['category']) && $_POST['category']==$value['school_id'])
			{
				echo '<option value="'.$value['id'].'" selected = "selected">'.$value['cat_name'].'</option>';
			}
			else
			{
				echo '<option value="'.$value['id'].'">'.$value['cat_name'].'</option>';
			}
		}
        ?>
        </select> 
        </td></tr>
        <tr>
              <td align="right" >Select School : &nbsp; </td>
          <td>
         		  
		  <select class="combobox" name="school" id="school" onchange="updateschool(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
       <?php 
	    
		   foreach($records['schoolarray'] as $key => $value)
			{
				if(isset($_POST['school']) && $_POST['school']==$value['school_id'])
				{
					echo '<option value="'.$value['school_id'].'" selected = "selected">'.$value['school_name'].'</option>';
					
				}
				else
				{
					echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
				}
			}
        ?>
				</select> 
           </td>
      </tr>		
       <tr>
              <td align="right" >Select Grade : &nbsp; </td>
          <td>
      	  <select class="combobox" name="grades" id="grades" onchange="updategrade(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>
        <option value="0" >All</option>
    	 <?php 
	    
		   foreach($records['gradearray'] as $key => $value)
			{
				if(isset($_POST['grades']) && $_POST['grades']==$value['dist_grade_id'])
				{
					echo '<option value="'.$value['dist_grade_id'].'" selected = "selected">'.$value['grade_name'].'</option>';
					
				}
				else
				{
					echo '<option value="'.$value['dist_grade_id'].'">'.$value['grade_name'].'</option>';
				}
			}
        ?>
				</select> 
           </td>
      </tr>	
   
     <tr>
       <td align="right">From :</td><td>
        <input value="" name="fromDate" style="width:80px" type="text"  id="f_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "f_date",
        trigger    : "f_date",
        onSelect   : function() { this.hide();
		select_f_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>
       </td>
      <td >  </td>
      </tr>
      
      <tr>
       <td align="right">To :</td><td>
       <input value="" name="toDate" style="width:80px" type="text"  id="t_date"/>
				 <script type="text/javascript"> 
      Calendar.setup({
        inputField : "t_date",
        trigger    : "t_date",
        onSelect   : function() { this.hide();
		select_t_date(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		//min: new Date(),
       });	 
	    </script>       
       </td>
      <td >  </td>
      </tr>
  
   </table>
        
     <div id="chartContainer">
 	  </div>
      <div id="loader" style="display:none" align="center"> <img src="<?php echo LOADERURL;?>"/></div>

    <div id="container"></div>   
   </fieldset>
   </div>
      
      
    </div>
    </div>
  </div>
  <?php require_once($view_path.'inc/footer.php'); ?>
</div>
<div id="dialog" title="Student Details" style="display:none;"> Student Details </div>


<!-- Single Student graph  --> 

<!-- LIGHT BOX ENDS HERE-->
</body>
</html>
