﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
     <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
   <script>
        var base_url = '<?php echo base_url();?>';
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
<?php if($this->session->userdata("login_type")!='user'){?>
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          <?php } else {?>
           <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
          <?php }?>
                  <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-Up</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>attendance/individual_students">Individual Students</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                 <div class="widget blue" style="min-height:250px;">
                         <div class="widget-title">
                             <h4>Individual Students</h4>
                          
                         </div>
                         
                         
                     
                     <div class="widget-body">
                        
                         
                        
                         <a href="#myModal-add-new" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-plus"></i > Add New</a>
                         <div id="myModal-add-new" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Add Student</h3>
                                </div>
                              
                                <div class="modal-body">
                                <form name='studentform1' class="form-horizontal" id='studentform1' method='post' onsubmit="return false" >
                                <span style="color: Red;display:none" id="message"></span>
                                     <div class="control-group">
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
												<input class='span8' type='text'  id='firstname' name='firstname'>
												<input  type='hidden'  id='student_id' name='student_id'>				
						                        </div>
                                             </div>
                                            <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input class="span8" type='text'  id='lastname' name='lastname'>
                                                 </div>
                                             </div>
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Login Name</label>
                                             <div class="controls">
                                          <input class="span8" type='text' id='loginname' name='loginname'>	
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Password</label>
                                             <div class="controls">
														<input class="span8" type='text'  id='passwordid' name='passwordid'>	
 												</div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Email (optional)</label>
                                             <div class="controls">
												<input class="span8" type='text'  id='emailaddress' name='emailaddress'>				
                                               </div>
                                             </div>
                                           <div class="control-group">
                                             <label class="control-label-required">Grade</label>
                                             <div class="controls">
                    <select class="span6 chzn-select" id='grade' name='grade' data-placeholder="--Please Select--" tabindex="1" style="width: 150px;">
								<option value=''>-Please Select-</option>
									<?php if($grades!=false)
									{
									foreach($grades as $gradeval)
									{
									?>
								<option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
									<?php
									}
									}
								?>
								</select>
                                             </div>
                                         </div>
                                         
                                              <div class="control-group">
                                             <label class="control-label-modal">Address (optional)</label>
                                             <div class="controls">
                                                 <textarea class="span8" id='addressid' name='addressid'></textarea>
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Student ID</label>
                                             <div class="controls">
												<input class="span8" type='text'  id='student_number' name='student_number'>                 
                                             </div>
                                             </div>
                                             <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
												<input class="span8" type='text'  id='phone_number' name='phone_number'>    
                                             </div>
                                             </div>
                                     </form>
                                </div>
                                <div class="modal-footer">
 									<button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                                                        <button data-dismiss="modal" type='submit' name="submit" id='student_add' value='Add' class="btn btn-success" ><i class="icon-plus"></i> Add Student</button>
                                </div>
                            </div>
                            
                                          <a href="#myModal-upload" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-upload"></i > Upload Students</a>
                         <style>
                                              .progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; height: 2px;display: none;  }
        .bar { background-color: #B4F5B4; width:0%; height:2px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; }
        #status{margin-top: 30px;}
        #uploadstudents{margin:0px !important;}
                                          </style>
                                          <div id="myModal-upload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Upload Students</h3>
                                </div>
                                              
                                              <form id="uploadstudents" class="form-horizontal" action="parents/student_sendfile" enctype="multipart/form-data" method="post">
                                <div class="modal-body" style="min-height: 150px;">
                                    
                                     <div class="control-group">
                                    <label class="control-label">Upload your file</label>
                                    <div class="controls">
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default" name="upload">
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                        </div></div>
                                         <div class="progress">
                                            <div class="bar"></div >
                                            <div class="percent">0%</div >        
                                             <div id="status"></div>         
                                        </div>
                                    
                                                                     
                                </div>
                                   <div class="modal-footer">
                                    <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button type="submit" value="Submit" id='student_add' class="btn btn-success" ><i class="icon-upload"></i> Upload Students</button>
                                    
                                </div>               
                               </form>
                                
                            </div>
                                    
                                     
                                     
                                     
                         <div class="space20"></div>
                         
                          <table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th >First Name</th>
                                <th class="sorting">Last Name</th>
                                <th class="no-sorting">Grade</th>
                                <th class="no-sorting">Student ID</th>
                                <th class="no-sorting">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($students as $studentkey=>$studentval){?>
                                <tr class="odd gradeX" id="row_<?php echo $studentval['student_id'];?>">                                
                               
                                    <td id="tdfirstname"><?php echo $studentval['firstname'];?></td>
                                <td id="tdlastname" class="hidden-phone"><?php echo $studentval['lastname'];?></td>
                                <td id="tdgrade" class="hidden-phone"><?php echo $studentval['grade'];?></td>
                                <td id="tdstudentnumber" class="center hidden-phone"><?php echo $studentval['student_number'];?></td>
                                <td class="hidden-phone">
                                
                                    <a href="#myModal-edit-student" role="button" class="btn btn-primary" data-toggle="modal" onclick="studentedit(<?php echo $studentval['student_id'];?>);"><i class="icon-pencil"></i></a>
                                    
                         
                            
                            
                            
                            
                                           <a href="#myModal-delete-student_<?php echo $studentval['student_id'];?>" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                         <div id="myModal-delete-student_<?php echo $studentval['student_id'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
                                </div>
                                <div class="modal-body">
                                     Please select "Yes" to remove this student.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="deletestudent(<?php echo $studentval['student_id'];?>);"><i class="icon-check"></i> Yes</button>
                                </div>
                            </div>
                            
                            
                            
                            </tr>
                            <?php }?>   
                            
                            </tbody>
                        </table>
                        <div id="myModal-edit-student" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Edit Student</h3>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" action="#" id="studentform2">
                                     <div class="control-group">
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="firstname" name="firstname" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="lastname" name="lastname"/>
                                                 
                                             </div>
                                             </div>
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Login Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="loginname" name="loginname"/>
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group ">
                                    <label for="password" class="control-label-required">Password</label>
                                    <div class="controls">
                                        <input class="span6 " id="passwordid" name="password" type="password" />
                                    </div>
                                </div>
                                
                                <div class="control-group ">
                                    <label for="confirm_password" class="control-label-required">Confirm Password</label>
                                    <div class="controls">
                                        <input class="span6 " id="confirm_password" name="confirm_password" type="password" />
                                    </div>
                                </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Email (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="emailaddress" name="emailaddress"/>
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group">
                                             <label class="control-label-required">Grade</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" tabindex="1" style="width: 150px;" name="grade" id="grade" >
                                        <option value=""></option>
                                        <?php $cnt=1;foreach($grades as $gradeval){?>
                                        <option value="<?php echo $gradeval['grade_id'];?>" id="optid_<?php echo $cnt;?>" ><?php echo $gradeval['grade_name'];?></option>
                                        <?php $cnt++;}?>
                                           
                                    </select>
                                    
                                         
                                             </div>
                                         </div>
                                         
                                             
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Address (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="address" name="address" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Student number</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="student_number" name="student_number" />
                                                 
                                             </div>
                                             </div>
                                             
                                               <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="phone" name="phone"/>
                                                 
                                             </div>
                                             </div>
                                        <input type="hidden" name="student_id" value="" id="student_id" />
                                     </form>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="cancelupdate"><i class="icon-remove" ></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" id="saveupdate"><i class="icon-save"></i> Save Changes</button>
                                </div>
                            </div>
                            
                          <!-- BEGIN ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
                
                
                </div>            
                          <!-- END ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                <!-- END EXAMPLE TABLE widget-->
                         
                         
                         
                         </div>
                        
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
  <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              <div id="successmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success" onclick="location.reload(true);">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 
<script src="<?php echo SITEURLM.$view_path; ?>js/student.js" type="text/javascript"></script>
   <!-- END JAVASCRIPTS --> 
   
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>