<style type="text/css">
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
.blue{
font-size:9px;
}
</style>
<page backtop="15mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
        <page_header> 

                 <img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png" style="position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 700px">
                    <b><img alt="pencil" width="12px" style="margin-left:5px;"  src="system/application/views/inc/logo/calendar.png"/>
                        &nbsp;Class Attendance Report - Weekly
                    </b>
                </div>
        </page_header>
    <page_footer> 
              <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
		<br />
                <table style="margin-left:10px;">
                    <tr>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                        </td>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                           Page [[page_cu]] of [[page_nb]] 
                        </td>
                        <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            Printed on: <?php echo date('F d,Y');?>
                        </td>
                    </tr>
                </table>
        </page_footer> 
        

<br /><br />

<table cellspacing="0" style="width:750px;border:2px #939393 solid;">
                    <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                        <td colspan="3" style="padding-left:5px;padding-top:2px;height:36px;" >
                            <b> <?php echo ucfirst($this->session->userdata('district_name'));?> | <?php echo ucfirst($teachername[0]['school_name']);?></b></td><td style="text-align:right;padding-right:10px;"><b><?php echo $date;?></b></td>
<!--                            <br /><br />-->
                        
                   
                </tr>
                <tr >
                        <td colspan="2" >
                       &nbsp;
                        
                    </td>
                </tr>
                <tr style="margin-top:20px;">
		<td style="padding-left:10px;width:350px;color:#939393;">
                    
		<?php if($this->session->userdata('login_type')=='teacher'){?>
                    <b>Teacher:</b> <?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?>   
                <?php } else if($this->session->userdata('login_type')=='observer'){?>
                    <b>Observer Name:</b><?php echo ($teachername[0]['observer_name']); ?>   
                <?php } else if($this->session->userdata('login_type')=='user'){?>
                    <b>User Name:</b><?php echo $this->session->userdata('username');?>
                <?php }?>
                <br /><br />
		<b>Month(s):</b> <?php echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));?>
		<br /><br />
		<b>Period:</b> <?php echo $start2.' to '.$end2;?>
		<br /><br />
		<b>Subject:</b> <?php echo $subject;?>
		<br /><br />
		<b>Grade:</b> <?php echo $grade;?>
		</td>
                <td style="padding-left:10px;width:350px;color:#939393;height:50px;">
                    <table style="border:1px solid #939393;height:50px;width:350px;">
                        <tr>
                            <td style="width:350px;height:130px;vertical-align: top;">
                                <b>Description:</b> 
                            </td>
                        </tr>
                    </table>
                </td>
		</tr>
		
                <tr >
                        <td colspan="2" >
                       &nbsp;
                        
                    </td>
                </tr>
		</table>
		  
<br /><br />
<!--tabel start -->
<?php
 $i = 0;
 $j = 0;
foreach ($weeksnumber as $key => $dayssplits) {

                if ($i % 1 == 0) {
                    $j++;
                }
                $dayssplit[$j][] = $dayssplits;
                $i++;
            }
       
        foreach ($dayssplit as $spkey => $days) {

           if ($spkey != 1) {?>
                </table>
      <?php }?>
      
      
 <table class="gridtable">
        <tr><td></td>

            <?php 
  
  foreach ($days as $weeknumber) {
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 0; $first <= 6; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                    ?>
				<?php if (date('m-d', ($day_ts))!= '' ) {?>
                   <th style="width:57px;vertical-align: middle; text-align: center;background: #FFF;font-size: 9px;"><span  >
                       <?php echo date('l', ($day_ts));?>
                       </span></th>
                <?php } else {?>
                    <th><span >&nbsp;</span></th>
				<?php  }}}?>
            </tr>
      <tr><td></td>

            <?php 
  
  foreach ($days as $weeknumber) {
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 0; $first <= 6; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                    ?>
				<?php if (date('m-d', ($day_ts))!= '' && (date('d', ($day_ts))>=1 && date('m', ($day_ts))>=$monthsnumber[0]['month'])) {?>
          <th style="text-align:center;"><span class='blue'><?php echo date('m/d/Y', ($day_ts));?> </span></th>
                <?php } else {?>
                    <th><span class='blue'>&nbsp;</span></th>
				<?php  }}}?>
            </tr>
 <?php  foreach($student_details as $student_detail){?> 
           
            <tr>
             <?php if($student_data == 'all'){
					
					?>
                  <td style="width:80px;" class='blue'><?php echo ($student_detail[0]->firstname . ' ' . $student_detail[0]->lastname . ''); ?></td>
                  <?php }else{?>
                  <td style="width:80px;" class='blue'><?php echo ($student_details[0]->firstname . ' ' . $student_details[0]->lastname . ''); ?></td>
                  <?php }
?>
            
                 
			<?php 
  			foreach ($days as $weeknumber) {
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 0; $first <= 6; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                        $k = 0;
                        if ($studentsroll != false) {
                            foreach ($studentsroll as $studentroll) {
							$date = $studentroll->date;
                                     $dates= date('m-d-Y', strtotime(str_replace('-', '/', $date))); 
						if($student_data == 'all'){
								if ($dates == date('m-d-Y',($day_ts)) && $studentroll->student_id == $student_detail[0]->student_id) {
								$k = 1;?>
                                   <td class='blue'><?php echo $studentroll->status?></td>
                                
                                <?php  }}else{
								if ($dates == date('m-d',($day_ts))) {
									
							$k = 1;?>
                                   <td class='blue'><?php echo $studentroll->status?></td>
							<?php  }}}}
                        if ($k == 0) {?>
							<td>&nbsp;</td>
                        <?php }}}?>
         </tr> 
<?php }?>
<tr style="border:none;">
<td colspan="8" style="border:none;">&nbsp;</td>
</tr>
<?php }?>
</table>
 

 <!--
 <br /><br />
 <table class="gridtable">

        <tr>
            <td ></td>
            <th>04-04-2014</th>
            <th>05-04-2014</th>
            <th>06-04-2014</th>
            <th>07-04-2014</th>
            <th>08-04-2014</th>
            <th>09-04-2014</th>
            <th>10-04-2014</th>
		</tr>

     <tr>
			<td class='blue'>ritesh</td>
	        <td class='blue'>present</td>
             <td class='blue'>present</td>
              <td class='blue'>present</td>
             <td class='blue'>present</td>
              <td class='blue'>present</td>
             <td class='blue'>present</td>
              <td class='blue'>present</td>

              
   	</tr>
    

 </table>

-->
</page>	

