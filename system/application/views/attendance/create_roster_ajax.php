<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
    ul.ui-autocomplete.ui-menu {
  z-index: 1000;
}
.ui-autocomplete { z-index:2147483647; }
</style>
                         
                         <?php if(is_array($status)){?>
                         <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th><i class="icon-time"></i> Time: <?php echo $period_data[0]['start_time'].'-'.$period_data[0]['end_time'];?></th>
                                        <th class="hidden-phone"><i class="icon-pencil"></i> Classroom: <?php echo $status[0]['class_name'];?></th>
                                        <th><i class="icon-book"></i> Subject:<?php echo $status[0]['subject_name'];?></th>
                                       
                                    </tr>
                                    </thead>

                                </table>
                                <div class="space20"></div>
                          <table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th >First Name</th>
                                <th class="sorting">Last Name</th>
                                <th class="no-sorting">Grade</th>
                                <th class="no-sorting">Student ID</th>
                                <th class="no-sorting">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
//                            is_array($status);exit;
                            $class_room_id = '';
                            foreach($status as $val){?>
                            <tr class="odd gradeX">                                
                                <td><?php echo $val['firstname'];?></td>
                                <td class="hidden-phone"><?php echo $val['lastname'];?></td>
                                <td class="hidden-phone"><?php echo $val['grade'];?></td>
                                <td class="center hidden-phone"><?php echo $val['student_number'];?></td>
								<td class="hidden-phone">
                                
 <a href="#myModal-edit-student" role="button" class="btn btn-primary" data-toggle="modal" onclick="studentedit(<?php echo $val['student_id'];?>);"><i class="icon-pencil"></i></a>
                     
                        <div id="myModal-edit-student" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Edit Student</h3>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" action="#" id="studentform2">
                                     <div class="control-group">
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="firstname" name="firstname" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="lastname" name="lastname"/>
                                                 
                                             </div>
                                             </div>
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Login Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="loginname" name="loginname"/>
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group ">
                                    <label for="password" class="control-label-required">Password</label>
                                    <div class="controls">
                                        <input class="span6 " id="passwordid" name="password" type="password" />
                                    </div>
                                </div>
                                
                                <div class="control-group ">
                                    <label for="confirm_password" class="control-label-required">Confirm Password</label>
                                    <div class="controls">
                                        <input class="span6 " id="confirm_password" name="confirm_password" type="password" />
                                    </div>
                                </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Email (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="emailaddress" name="emailaddress"/>
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group">
                                             <label class="control-label-required">Grade</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" tabindex="1" style="width: 150px;" name="grade" >
                                        <option value=""></option>
                                        <?php $cnt=1;foreach($grades as $gradeval){?>
                                        <option value="<?php echo $gradeval['grade_id'];?>" id="optid_<?php echo $cnt;?>" ><?php echo $gradeval['grade_name'];?></option>
                                        <?php $cnt++;}?>
                                           
                                    </select>
                                    
                                         
                                             </div>
                                         </div>
                                         
                                             
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Address (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="address" name="address" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Student number</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="student_number" name="student_number" />
                                                 
                                             </div>
                                             </div>
                                             
                                               <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="phone" name="phone"/>
                                                 
                                             </div>
                                             </div>
                                        <input type="hidden" name="student_id" value="" id="student_id" />
                                     </form>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="cancelupdate"><i class="icon-remove" ></i> Cancel</button>
                                    <button class="btn btn-success" id="saveupdate" onclick="updatestudent1();"><i class="icon-save"></i> Save Changes</button>
                                </div>
                            </div>
                            
                            
                             <?php /*?>              <a href="#myModal-delete-student<?php echo $val['teacher_student_id'];?>" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                         <div id="myModal-delete-student<?php echo $val['teacher_student_id'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
                                </div>
                                <div class="modal-body">
                                     Please select "Yes" to remove this student.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button class="btn btn-success"  onclick="studentdelete(<?php echo $day;?>,<?php echo $val['teacher_student_id'];?>)" ><i class="icon-check"></i> Yes</button>
                                </div>
                            </div><?php */?>
                            
                                       <a href="#myModal-delete-student_<?php echo $val['student_id'];?>" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                         <div id="myModal-delete-student_<?php echo $val['student_id'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
                                </div>
                                <div class="modal-body">
                                     Please select "Yes" to remove this student.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="teacherstudentdelete(<?php echo $day;?>,<?php echo $val['teacher_student_id'];?>);"><i class="icon-check"></i> Yes</button>
                                </div>
                            </div>
                            
                            
                            
                                </td>
                            </tr>
                            <?php 
                            $class_room_id = $val['class_room_id'];

                            }
                            ?>
                            </tbody>
                        </table>
                                <div class="space20"></div>
                                                                   <a href="#myModal-add-new<?php echo $period;?>" role="button" class="btn btn-success" data-toggle="modal" ><i class="icon-plus"></i > Add New Student </a>
                                       
                         						
                 <div id="myModal-add-new<?php echo @$period;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                             <form class="form-horizontal" action="#" method="post" onsubmit="return false;" id="addstudentfrm<?php echo @$period;?>"> 
                                 
                                    <input type="hidden" name="class_room_id" value="<?php echo @$class_room_id;?>" />
                                    <input type="hidden" name="day" value="<?php echo @$day;?>" />
                                         <input type="hidden" name="period_id" value="<?php echo @$period;?>" />
                                    <input type="hidden" name="teacher_id" value="<?php echo @$teacher_id;?>" />
                                         
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Add Student </h3>
                                </div>
                                <div class="modal-body">
                                    
                                         
                                     <div class="control-group">
                                             <label class="control-label-required">Select Grade</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" name="grade_id" id="grade_id<?php echo $period;?>" >
                                                     <option>Select Grade</option>
                                                     <?php foreach($grades as $grade):?>
                                                     <option value="<?php echo $grade['grade_id'];?>"><?php echo $grade['grade_name'];?></option>
                                                       <?php endforeach;?>
                                                 </select>
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Search Student</label>
                                             <div class="controls">
                                                 <input type="text" class="span6" name="search_student<?php echo $period;?>" id="search_student<?php echo $period;?>" data-placeholder="Firstname" />
                                                 <button class="btn btn-success" onclick="searchstudent(<?php echo $period;?>);"><i class="icon-plus"></i>Search</button>
                                             </div>
                                             </div>
                                    
                                            <div class="control-group">
                                             <label class="control-label-required">Select Student</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" name="student_id" id="student_id<?php echo $period;?>">
                                                     <option>Select Student</option>
                                                     
                                                 </select>
                                             </div>
                                             </div>
                                     
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="addstudent(<?php echo $period;?>);"><i class="icon-plus"></i> Add Student</button>
                                </div>
                                </form>
                            </div>
                        
  <a href="#myModal-replicate-periods<?php echo $period;?>" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-copy"></i > Replicate Students Across Periods</a>
                                                           
                                                
                         <div id="myModal-replicate-periods<?php echo $period;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel5">Replicate Students</h3>
                                </div>
                                <div class="modal-body" style="min-height:150px;">
                                     <form class="form-horizontal" action="#">
                                           <div class="control-group">
                                             <label class="control-label-required">Select Period</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" name="<?php echo $day.'_'.$period.'_'.$status[0]['class_room_id'];?>" id="<?php echo $day.'_'.$period.'_'.$status[0]['class_room_id'];?>">
                                                     <option value="all">All Periods</option>
                                                <?php foreach($class_room_all as $class_room_all_val)
				{
					if($class_room_all_val['period_id']!=$period)
					{
					print '<option value='.$class_room_all_val['period_id'].'_'.$class_room_all_val['class_room_id'].'>'.$class_room_all_val['start_time'].' '.$class_room_all_val['end_time'].'</option>';
				   }
				}
                                                     ?>
                                    </select>
                                             </div>
                                         </div>
                                    
                                                                     </form>
                                </div>
                               
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="copytoperiods(<?php echo $day;?>,<?php echo $period;?>,<?php echo $status[0]['class_room_id'];?>)"><i class="icon-copy"></i> Copy</button>
                                </div>
                            </div>
                               <?php } else {?>
                                   <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th><i class="icon-time"></i> Time: <?php echo $period_data[0]['start_time'].'-'.$period_data[0]['end_time'];?></th>
                                        <th class="hidden-phone"><i class="icon-pencil"></i> Classroom: <?php echo $class_room[0]['class_name'];?></th>
                                        <th><i class="icon-book"></i> Subject:<?php echo $class_room[0]['subject_name'];?></th>
                                       
                                    </tr>
                                    </thead>

                                </table>
                                <div class="space20"></div>
                                
                                <?php   echo "No Roster found<div class=\"space20\"></div>";?>
                                
                                                                   <a href="#myModal-add-new<?php echo $period;?>" role="button" class="btn btn-success" data-toggle="modal" ><i class="icon-plus"></i > Add New Student </a>
                                       
                         						
                 <div id="myModal-add-new<?php echo @$period;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                             <form class="form-horizontal" action="#" method="post" onsubmit="return false;" id="addstudentfrm<?php echo @$period;?>"> 
                                 
                                    <input type="hidden" name="class_room_id" value="<?php echo $class_room[0]['class_room_id'];?>" />
                                    <input type="hidden" name="day" value="<?php echo @$day;?>" />
                                         <input type="hidden" name="period_id" value="<?php echo @$period;?>" />
                                         <input type="hidden" name="teacher_id" value="<?php echo @$teacher_id;?>" />
                                    
                                         
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Add Student </h3>
                                </div>
                                <div class="modal-body">
                                    
                                         
                                     <div class="control-group">
                                             <label class="control-label-required">Select Grade</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" name="grade_id" id="grade_id<?php echo $period;?>" >
                                                     <option>Select Grade</option>
                                                     <?php foreach($grades as $grade):?>
                                                     <option value="<?php echo $grade['grade_id'];?>"><?php echo $grade['grade_name'];?></option>
                                                       <?php endforeach;?>
                                                 </select>
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Search Student</label>
                                             <div class="controls">
                                                 <input type="text" class="span6" name="search_student<?php echo $period;?>" id="search_student<?php echo $period;?>" data-placeholder="Firstname" />
                                                 <button class="btn btn-success" onclick="searchstudent(<?php echo $period;?>);"><i class="icon-plus"></i>Search</button>
                                             </div>
                                             </div>
                                    
                                            <div class="control-group">
                                             <label class="control-label-required">Select Student</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 250px;" name="student_id" id="student_id<?php echo $period;?>">
                                                     <option>Select Student</option>
                                                     
                                                 </select>
                                             </div>
                                             </div>
                                     
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="addstudent(<?php echo $period;?>);"><i class="icon-plus"></i> Add Student</button>
                                </div>
                                </form>
                            </div>
                        
  <a href="#myModal-replicate-periods<?php echo $period;?>" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-copy"></i > Replicate Students Across Periods</a>
                                                           
                                                
                         <div id="myModal-replicate-periods<?php echo $period;?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel5">Replicate Students</h3>
                                </div>
                                <div class="modal-body" style="min-height:150px;">
                                     <form class="form-horizontal" action="#">
                                           <div class="control-group">
                                             <label class="control-label-required">Select Period</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" name="<?php echo $day.'_'.$period.'_'.$class_room[0]['class_room_id'];?>" id="<?php echo $day.'_'.$period.'_'.$class_room[0]['class_room_id'];?>">
                                                     <option value="all">All Periods</option>
                                                <?php foreach($class_room_all as $class_room_all_val)
				{
					if($class_room_all_val['period_id']!=$period)
					{
					print '<option value='.$class_room_all_val['period_id'].'_'.$class_room_all_val['class_room_id'].'>'.$class_room_all_val['start_time'].' '.$class_room_all_val['end_time'].'</option>';
				   }
				}
                                                     ?>
                                    </select>
                                             </div>
                                         </div>
                                    
                                                                     </form>
                                </div>
                               
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="copytoperiods(<?php echo $day;?>,<?php echo $period;?>,<?php echo $class_room[0]['class_room_id'];?>)"><i class="icon-copy"></i> Copy</button>
                                </div>
                            </div>
                                
                                <?php }?>
                                                 <div class="space20"></div> 
                       
              
   
