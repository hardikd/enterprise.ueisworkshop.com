﻿<html>
<head>
<style type="text/css">
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:35.2px;}
.blue{
font-size:7px;
}
</style>


</head>

<body>

<div backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png"/>
        
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b><?php echo ($parent_conference[0]->districts_name); ?></b>		
		<br /><b><?php echo ($parent_conference[0]->school_name); ?></b>
        <br /><br /><b>Attendance Report By Month -
		<?php 
	if($monthsnumber[0]['month']== 1){
		echo "January";
		}else if($monthsnumber[0]['month']==2){
		echo "February";
		}else if($monthsnumber[0]['month']==3){
		echo "March";
		}else if($monthsnumber[0]['month']==4){
		echo "April";		
		}else if($monthsnumber[0]['month']==5){
		echo "May";	
		}else if($monthsnumber[0]['month']==6){
		echo "June";
		}else if($monthsnumber[0]['month']==7){
		echo "July";
		}else if($monthsnumber[0]['month']==8){
		echo "August";
		}else if($monthsnumber[0]['month']==9){
		echo "September";
		}else if($monthsnumber[0]['month']==10){
		echo "October";
		}else if($monthsnumber[0]['month']==11){
		echo "November";
		}else if($monthsnumber[0]['month']==12){
		echo "December";
	}
		?> - <?php echo $yearnumber[0]['year'];?></b>
		
		</div>
	</div>
		</div>
        

<br /><br />
<table style="width:850px;border:2px #7FA54E solid;">
		<tr>
		<td style="width:180px;color:#CCCCCC;">
        <?php
        if($this->session->userdata('login_type')=='teacher'){?>
           <b>Teacher:</b><?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?>   
           
           <?php } else if($this->session->userdata('login_type')=='observer'){?>
                 <b>Observer Name:</b><?php echo ($teachername[0]['observer_name']); ?>   
           <?php } else if($this->session->userdata('login_type')=='user'){?>
                 <b>User Name:</b><?php echo $this->session->userdata('username');?>
                 <?php }?>
		
		</td>
		<td style="width:205px;color:#CCCCCC;">
		<b>Student Name:</b>
		 <?php if($student_data == 'all'){
				echo ucfirst($student_data);	 
		 }else{
			echo ($student_details[0]->firstname . ' ' . $student_details[0]->lastname . ''); 
		 }
		?>
		</td>
		<td style="width:185px;color:#CCCCCC;">
		<b>Subject:</b><?php echo ($students_all_detail[0]['subject_name']); ?>
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Grade:</b><?php echo ($students_all_detail[0]['grade']); ?>
		</td>
		</tr>
		</table>
		  
<br /><br />
<!--tabel start -->



 <br /><br />
 <table class="gridtable">

        <tr>
            <th>Teacher Name</th>
            <th>Student Name</th>
            <th>Room</th>
            <th>Present</th>
            <th>Absent</th>
            <th>Absent w/ Excuse</th>
            <th>Tardy</th>
            <th>Tardy w/ Excuse</th>
            <th>Total</th>
		</tr>
        
     
      <?php foreach($student_details as $student_detail){?>
     <?php  
	 $data = array();
	 $day=0;
		foreach ($monthsnumber as $monthnumber) {
                    $totalDaysofMonth = cal_days_in_month(CAL_GREGORIAN,$monthnumber['month'],$monthnumber['year']);
                    $firstdate = date('Y-m-d', mktime(0, 0, 0, $monthnumber['month'], 1, $monthnumber['year']));
                    for ($first = 1; $first <= $totalDaysofMonth; $first++) {
					$day_ts = strtotime('+'.($first-1).' day', strtotime($firstdate));
						
                    $day++;	
					

		foreach($studentsroll as $key=> $roll){
			$date = $roll->date;
				$dates= date('y-m-d', strtotime(str_replace('-', '/', $date))); 	
			
			if($student_data == 'all'){
					
					if ($dates == date('y-m-d',($day_ts)) && $roll->student_id == $student_detail[0]->student_id) {
			
				switch(strtolower($roll->status)){
					case 'present':
					$data['present'] = $data['present']+1;
					break;
					case 'absent':
					$data['absent'] = $data['absent'] + 1;
					break;
					case 'absent_excuse':
					$data['absent_excuse'] = $data['absent_excuse'] + 1;
					break;
					case 'tardy':
					$data['tardy'] = $data['tardy'] + 1;
					break;
					case 'tardy_excuse':
					$data['tardy_excuse'] = $data['tardy_excuse'] + 1;
					break;
					default:
					break;
					
				}
			}
		}else{
			
			if ($dates == date('y-m-d',($day_ts))) {
				switch(strtolower($roll->status)){
					case 'present':
					$data['present'] = $data['present']+1;
					break;
					case 'absent':
					$data['absent'] = $data['absent'] + 1;
					break;
					case 'absent_excuse':
					$data['absent_excuse'] = $data['absent_excuse'] + 1;
					break;
					case 'tardy':
					$data['tardy'] = $data['tardy'] + 1;
					break;
					case 'tardy_excuse':
					$data['tardy_excuse'] = $data['tardy_excuse'] + 1;
					break;
					default:
					break;
					
				}
			}
			
		}
	}

  }
	}
 ?>


	 <tr>
			<td class='blue'><?php echo $studentsroll[0]->firstname; ?> <?php echo $studentsroll[0]->lastname;?></td>
			<?php if($student_data == 'all'){?>
			<td style="width:50px;" class='blue'><?php echo ($student_detail[0]->firstname . ' ' . $student_detail[0]->lastname . ''); ?></td>
			<?php }else{?>
			<td style="width:50px;" class='blue'><?php echo ($student_details[0]->firstname . ' ' . $student_details[0]->lastname . ''); ?></td>
			<?php }?>
             <td class='blue'><?php echo $studentsroll[0]->name;?></td>
              <td class='blue'><?php echo $data['present'];?></td>
             <td class='blue'><?php echo $data['absent'];?></td>
              <td class='blue'><?php echo $data['absent_excuse'];?></td>
             <td class='blue'><?php echo $data['tardy'];?></td>
              <td class='blue'><?php echo $data['tardy_excuse'];?></td>
              <td class='blue'><?php echo $day;?></td>

              
   	</tr>
<?php }?>
 </table>


</body>
</html>		

