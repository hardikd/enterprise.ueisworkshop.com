
<style type="text/css">
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
.blue{
font-size:9px;
}
</style>

<page backtop="7mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
        <page_header> 

                 <img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 700px">
                    <b><img alt="pencil" width="12px" style="margin-left:5px;margin-top:5px;"  src="system/application/views/inc/logo/calendar.png"/>
                        &nbsp;Class Attendance Report - Weekly
                    </b>
                </div>
        </page_header>
    <page_footer> 
              <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
		<br />
                <table style="margin-left:10px;">
                    <tr>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                        </td>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                           Page [[page_cu]] of [[page_nb]] 
                        </td>
                        <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            Printed on: <?php echo date('F d,Y');?>
                        </td>
                    </tr>
                </table>
        </page_footer> 
        

<br /><br />
<table style="width:850px;border:2px #7FA54E solid;">
		<tr>
		<td style="width:180px;color:#CCCCCC;">
        <?php
        if($this->session->userdata('login_type')=='teacher'){?>
           <b>Teacher:</b><?php echo ($teachername[0]['firstname'] . ' ' . $teachername[0]['lastname'] . ''); ?>   
           
           <?php } else if($this->session->userdata('login_type')=='observer'){?>
                 <b>Observer Name:</b><?php echo ($teachername[0]['observer_name']); ?>   
           <?php } else if($this->session->userdata('login_type')=='user'){?>
                 <b>User Name:</b><?php echo $this->session->userdata('username');?>
                 <?php }?>
		
		</td>
		<td style="width:205px;color:#CCCCCC;">
		<b>Student Name:</b>
		 <?php if($student_data == 'all'){
				echo ucfirst($student_data);	 
		 }else{
			echo ($student_details[0]->firstname . ' ' . $student_details[0]->lastname . ''); 
		 }
		?>
		</td>
		<td style="width:185px;color:#CCCCCC;">
		<b>Subject:</b><?php echo ($students_all_detail[0]['subject_name']); ?>
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Grade:</b><?php echo ($students_all_detail[0]['grade']); ?>
		</td>
		</tr>
		</table>
		  
<br /><br />
<!--tabel start -->



 <br /><br />
 <table class="gridtable">

        <tr>
            <th>Teacher Name</th>
            <th>Student Name</th>
            <th>Room</th>
            <th>Present</th>
            <th>Absent</th>
            <th>Absent w/ Excuse</th>
            <th>Tardy</th>
            <th>Tardy w/ Excuse</th>
            <th>Total</th>
		</tr>
        
     
      <?php foreach($student_details as $student_detail){?>
     <?php  
	 $data = array();
	 $day=0;

	foreach ($weeksnumber as $weeknumber) {
		            $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 1; $first <= 7; $first ++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                    $day++;	
					

		foreach($studentsroll as $key=> $roll){
			$date = $roll->date;
				$dates= date('m-d', strtotime(str_replace('-', '/', $date))); 
			
			if($student_data == 'all'){
					if ($dates == date('m-d',($day_ts)) && $roll->student_id == $student_detail[0]->student_id) {
			
				switch(strtolower($roll->status)){
					case 'present':
					$data['present'] = $data['present']+1;
					break;
					case 'absent':
					$data['absent'] = $data['absent'] + 1;
					break;
					case 'absent_excuse':
					$data['absent_excuse'] = $data['absent_excuse'] + 1;
					break;
					case 'tardy':
					$data['tardy'] = $data['tardy'] + 1;
					break;
					case 'tardy_excuse':
					$data['tardy_excuse'] = $data['tardy_excuse'] + 1;
					break;
					default:
					break;
					
				}
			}
		}else{
			
		if ($dates == date('m-d',($day_ts))) {
				switch(strtolower($roll->status)){
					case 'present':
					$data['present'] = $data['present']+1;
					break;
					case 'absent':
					$data['absent'] = $data['absent'] + 1;
					break;
					case 'absent_excuse':
					$data['absent_excuse'] = $data['absent_excuse'] + 1;
					break;
					case 'tardy':
					$data['tardy'] = $data['tardy'] + 1;
					break;
					case 'tardy_excuse':
					$data['tardy_excuse'] = $data['tardy_excuse'] + 1;
					break;
					default:
					break;
					
				}
			}
			
		}
	}

  }
	}
 ?>


	 <tr>
			<td class='blue'><?php echo $studentsroll[0]->firstname; ?> <?php echo $studentsroll[0]->lastname;?></td>
			<?php if($student_data == 'all'){?>
			<td style="width:50px;" class='blue'><?php echo ($student_detail[0]->firstname . ' ' . $student_detail[0]->lastname . ''); ?></td>
			<?php }else{?>
			<td style="width:50px;" class='blue'><?php echo ($student_details[0]->firstname . ' ' . $student_details[0]->lastname . ''); ?></td>
			<?php }?>
             <td class='blue'><?php echo $studentsroll[0]->name;?></td>
              <td class='blue'><?php echo $data['present'];?></td>
             <td class='blue'><?php echo $data['absent'];?></td>
              <td class='blue'><?php echo $data['absent_excuse'];?></td>
             <td class='blue'><?php echo $data['tardy'];?></td>
              <td class='blue'><?php echo $data['tardy_excuse'];?></td>
              <td class='blue'><?php echo $day;?></td>

              
   	</tr>
<?php }?>
 </table>

</page>	

