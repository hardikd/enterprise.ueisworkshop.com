<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    <script>
        var base_url = '<?php echo base_url();?>';
        </script>
        

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
     <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
        <?php if($this->session->userdata("login_type")!='user'){?>
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          <?php } else {?>
           <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
          <?php }?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                     <?php if($this->session->userdata('login_type')=='teacher') { ?>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/map_data">Add Map/Data</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>attendance/add_parent_info">Add Parent Information</a>
                           
                       </li>
                      
                   </ul>
                   
                    <?php } else if($this->session->userdata('login_type')=='observer') {?>
                      <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                          <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-Up</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>attendance/add_parent_info">Parent Information</a>
                           
                       </li>
                   
                   </ul>   
                   <?php } else if($this->session->userdata('login_type')=='user') {?>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                          <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-Up</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>attendance/add_parent_info">Parent Information</a>
                           
                       </li>
                   
                   </ul>  
                   <?php }?>
                            
                   
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget blue">
                         <div class="widget-title">
                             <h4>Add Parent Information</h4>
                          
                         </div>
                        
                        <div class="widget-body">
                        
                          
                        
                         <a href="#myModal-add-new-parent" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-plus"></i > Add New</a>
                         <div id="myModal-add-new-parent" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                             <form class="form-horizontal" action="#" style="margin-bottom: 0px !important;" id="frmaddparent"> 
                             <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Add Parent</h3>
                                </div>
                                <div class="modal-body">
                                     
                                     <div class="control-group">
                                     <p><b>Parent A Information</p></b>
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8"  name="parentfirstname" required/>
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8"  name='parentlastname' required/>
                                                 
                                             </div>
                                             </div>
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Login Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8"  name='username' required/>
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Password</label>
                                             <div class="controls">
                                                 <input type="text" class="span8"  name='password' required/>
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal" >Email (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value=""  name='email'/>
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group">
                                             <label class="control-label-required">Language</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" name="language">
                                        <?php foreach($LANGUAGES as $key=>$val){?>
                                                     <option value="<?php echo $key;?>"><?php echo $val;?></option>
                                        <?php }?>
                                            
                                    </select>
                                             </div>
                                         </div>                                      
                                             
                                             
                                            
                                             
                                               <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8"  name='phone' id="phone"/>
                                                 
                                             </div>
                                             </div>
                                                
                                              <p><b>Parent B Information</p></b>
                                              <div class="control-group">
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8"  name='firstnamesub'/>
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" name='lastnamesub'/>
                                                 
                                             </div>
                                             </div>
                                             
                                             
                                              
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Email (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name='emailsub'/>
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group">
                                             <label class="control-label-required">Language</label>
                                             <div class="controls">
                                    <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" languagesub>
                                        <?php foreach($LANGUAGES as $key=>$val){?>
                                                     <option value="<?php echo $key;?>"><?php echo $val;?></option>
                                        <?php }?>                                         
                                    </select>
                                             </div>
                                         </div>                                      
                                             
                                             
                                            
                                             
                                               <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" name='phonesub' id="phonesub"/>
                                                 
                                             </div>
                                             </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" id="add_new_parent"><i class="icon-plus"></i> Add Parent</button>
                                </div>
                                  </form>
                            </div>
                            
                            
                                          <a href="#myModal-upload" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-upload"></i > Upload Parents</a>
                         <div id="myModal-upload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Upload Parents</h3>
                                </div>
                                <div class="modal-body">
                                     <form class="form-horizontal" action="#">
                                     <div class="control-group">
                                    <label class="control-label">Upload your file</label>
                                    <div class="controls">
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default">
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                        </div></div>
                                    
                                                                     </form>
                                </div>
                               
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success"><i class="icon-upload"></i> Upload Parents</button>
                                </div>
                            </div>
                                     
                                     
                                     
                                     <a href="#myModal-upload-ps" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-upload"></i > Upload Parents & Students</a>
                         <div id="myModal-upload-ps" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Upload Parents & Students</h3>
                                </div>
                                <div class="modal-body">
                                     <form class="form-horizontal" action="#">
                                     <div class="control-group">
                                    <label class="control-label">Upload your file</label>
                                    <div class="controls">
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default">
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                        </div></div>
                                    
                                                                     </form>
                                </div>
                               
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success"><i class="icon-upload"></i> Upload Parents & Students</button>
                                </div>
                            </div>
                                     
                                     
                                     
                                     
                         <div class="space20"></div>
                         
                       <table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th >Parent A & Student</th>
                                <th class="no-sorting">Email</th>
                                <th class="no-sorting">Lang</th>
                                <th >Parent B Name</th>
                                <th class="no-sorting">Email</th>
                                <th class="no-sorting">Lang</th>
                                
                                
                                
                                <th class="no-sorting">Actions</th>
                            </tr>
                            </thead>
                            <tbody id="parentlist">
                            <?php foreach($parents as $parentskey=>$parentsval){?>
                            <tr class="odd gradeX">    
                            
 <td><a href="<?php echo base_url().'parents/studentsview/'.$parentsval['parents_id']?>"><u><?php echo $parentsval['firstname'];?>&nbsp;<?php echo $parentsval['lastname'];?></u></a></td>                               
												  
                                <td class="hidden-phone"><a href="mailto:"<?php echo $parentsval['email'];?>""><?php echo $parentsval['email'];?></a></td>
                                <td class="center hidden-phone"><?php echo $parentsval['language'];?></td>
                               
                               <td><?php echo $parentsval['firstnamesub'];?>&nbsp;<?php echo $parentsval['lastnamesub'];?></td>                               
								<td class="hidden-phone"><a href="mailto:"<?php echo $parentsval['emailsub'];?>""><?php echo $parentsval['emailsub'];?></a></td>
                                <td class="center hidden-phone"><?php echo $parentsval['languagesub'];?></td> 
								
                                
                                
                                
                                <td class="hidden-phone">
                                
                                    <a href="#myModal-edit-parent" role="button" class="btn btn-primary" data-toggle="modal" onclick="editparent(<?php echo $parentsval['parents_id'];?>);"><i class="icon-pencil"></i></a>
                         
                            
                            
                            
                            
                                           <a href="#myModal-delete-student<?php echo $parentsval['parents_id'];?>" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                         <div id="myModal-delete-student<?php echo $parentsval['parents_id'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
                                </div>
                                <div class="modal-body">
                                     Please select "Yes" to remove this parent.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="deleteparent(<?php echo $parentsval['parents_id'];?>);"><i class="icon-check"></i> Yes</button>
                                </div>
                            </div>
                            
                            
                            
                            </tr>
                            <?php }?>
                            
                            </tbody>
                        </table>
                        
       
                        <div id="myModal-edit-parent" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Edit Parent Information</h3>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" action="#" id="parentform">
                                        <input type="hidden" name="parent_id" id="parent_id" value=""/>
                                     <div class="control-group">
                                     <p><b>Parent A Information</p></b>
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="parentfirstname" id="parentfirstname"/>
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="parentlastname" id="parentlastname" />
                                                 
                                             </div>
                                             </div>
                                             
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Login Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="username" id="username" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Password</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" id="password" name="password" value="" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Email (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="email" name="email" />
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group">
                                             <label class="control-label-required">Language</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" id="language" name="language">
                                                    <?php foreach($LANGUAGES as $key=>$val){?>
                                                     <option value="<?php echo $key;?>"><?php echo $val;?></option>
                                        <?php }?>
                                                                                      
                                    </select>
                                             </div>
                                         </div>                                      
                                             
                                             
                                            
                                             
                                               <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="phone" id="phone_val"/>
                                                 
                                             </div>
                                             </div>
                                                
                                              <p><b>Parent B Information</p></b>
                                              <div class="control-group">
                                             <label class="control-label-required">First Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="firstnamesub" id="firstnamesub" />
                                                 
                                             </div>
                                             </div>
                                             
                                              <div class="control-group">
                                             <label class="control-label-required">Last Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="lastnamesub" name="lastnamesub" />
                                                 
                                             </div>
                                             </div>
                                             
                                             
                                              
                                             
                                              <div class="control-group">
                                             <label class="control-label-modal">Email (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" id="emailsub" name="emailsub" />
                                                 
                                             </div>
                                             </div>
                                             
                                             <div class="control-group">
                                             <label class="control-label-required">Language</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="English" tabindex="1" style="width: 150px;" id="languagesub" name="languagesub">
                                        <?php foreach($LANGUAGES as $key=>$val){?>
                                                     <option value="<?php echo $key;?>"><?php echo $val;?></option>
                                        <?php }?>
                                                                                
                                    </select>
                                             </div>
                                         </div>                                      
                                             
                                             
                                            
                                             
                                               <div class="control-group">
                                             <label class="control-label-modal">Phone (optional)</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="phonesub" id="phonesub_val" />
                                                 
                                             </div>
                                             </div>
                                     </form>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button  class="btn btn-success" id="editparent"><i class="icon-save"></i> Save Changes</button>
                                </div>
                            </div>
                            
                          <!-- END ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                <!-- END EXAMPLE TABLE widget-->
                
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
   <script src="<?php echo SITEURLM.$view_path; ?>js/parent.js"></script>

 

   <!-- END JAVASCRIPTS --> 
   
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>