<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
    
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />


<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
         <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
              
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/roster">Student Roster</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>attendance/create_roster">Create Roster</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget blue">
                         <div class="widget-title">
                             <h4>Create Roster</h4>
                          
                         </div>
                        
                        <div class="widget-body">
                            <?php if($this->session->userdata('login_type')=='observer'):?>
                                
                            
                            <b style="position: relative;top:-10px;">Select Teacher:</b>
                            <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 350px;" name="teacher_id" id="teacher_id">
                                                 <option ></option>
		<?php 
		foreach($teachers as $teacher)
		{
		?>
		<option value="<?php echo $teacher['teacher_id'];?>"><?php echo $teacher['firstname'].' '.$teacher['lastname'];?></option>
		<?php 
		 }
		?>
                                    </select>
                            <?php endif;?>
                        
                         <div class="widget widget-tabs blue"  <?php if($this->session->userdata('login_type')=='observer') echo "style='display:none;'";?>>
                        <div class="widget-title">
                            
                        </div>
                        <div class="widget-body">
                            <div class="tabbable ">
                                <ul class="nav nav-tabs">
                                    <li class="active" id="day_0"><a href="#widget_tab1" data-toggle="tab" onclick="getthisroster(0)">MON</a></li>
                                    <li class="" id="day_1"><a href="#widget_tab2" data-toggle="tab" onclick="getthisroster(1)">TUE</a></li>
                                    <li class="" id="day_2"><a href="#widget_tab3" data-toggle="tab" onclick="getthisroster(2)">WED</a></li>
                                    <li class="" id="day_3"><a href="#widget_tab4" data-toggle="tab" onclick="getthisroster(3)">THU</a></li>
                                    <li class="" id="day_4"><a href="#widget_tab5" data-toggle="tab" onclick="getthisroster(4)">FRI</a></li>
                                    <li class="" id="day_5"><a href="#widget_tab6" data-toggle="tab" onclick="getthisroster(5)">SAT</a></li>
                                    <li class="" id="day_6"><a href="#widget_tab7" data-toggle="tab" onclick="getthisroster(6)">SUN</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="widget_tab1">
                                    <a href="#myModal-replicate" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-copy"></i > Replicate Student Roster Across Days</a>
                                                           
                                                
                         <div id="myModal-replicate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel5">Replicate Student Roster </h3>
                                </div>
                                <div class="modal-body" style="min-height:150px;">
                                     <form class="form-horizontal" action="#">
                                           <div class="control-group">
                                             <label class="control-label-required">Select Day</label>
                                             <div class="controls">
                                                 <select class="span6 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 150px;" name="copyday" id="copyday">
                                                 <option value="all">All Days</option>
		<?php 
		for($dj=0;$dj<=6;$dj++)
		{
		
		 switch($dj) {
        case '0': $numDaysToMonday = 'Mon'; break;
        case '1': $numDaysToMonday = 'Tue'; break;
        case '2': $numDaysToMonday = 'Wed'; break;
        case '3': $numDaysToMonday = 'Thu'; break;
        case '4': $numDaysToMonday = 'Fri'; break;
        case '5': $numDaysToMonday = 'Sat'; break;
        case '6': $numDaysToMonday = 'Sun'; break;   
    }
	
		?>
		<option value="<?php echo $dj;?>"><?php echo $numDaysToMonday;?></option>
		<?php 
		 }
		?>
                                                     
                                                     
                                    </select>
                                             </div>
                                         </div>
                  </form>
                                </div>
                               
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="copytodays();"><i class="icon-copy"></i> Copy</button>
                                </div>
                            </div>
                            
                                     
                         <div class="space20"></div>
                         <?php foreach($periods as $periodvalue){?>
                         <div id='period_<?php echo $periodvalue['period_id'];?>'>
                             
                         </div>
                            <div class="space20"></div>

                         <?php }?>

        


</div>


 <!-- END SUNDAY->
 
                     
                                </div>
                            </div>
                        </div>
                    </div>
                        
                         
                        
                            
                          <!-- BEGIN ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                
                        
                    </div>
                </div>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->
         </div>
                           
                           
                            <div class="space20"></div>
                              
                         
                        <!-- END GRID SAMPLE PORTLET-->
                    </div>
                                         
                                         
                                         
                                         
                                       
                      
                   
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
            <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                                <div id="succmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
  <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
 <script src="<?php echo SITEURLM.$view_path; ?>js/teacherstudent.js" type="text/javascript"></script>
 
 <script src="<?php echo SITEURLM.$view_path; ?>js/student.js" type="text/javascript"></script>

 
 
<script type="text/javascript">
var site_url='<?php echo SITEURLM;?>';
var periods=<?php echo json_encode($periods);?>;
var base_url='<?php echo base_url();?>';
</script>
   <!-- END JAVASCRIPTS --> 
   
    <script>
        base_url = '<?php echo base_url();?>';
       jQuery(document).ready(function() {
           EditableTable.init();
           
       });
   </script>
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });

function searchstudent(period){
    $('#student_id'+period).html();
        pdata={};
	pdata.id=$('#grade_id'+period).val();
	pdata.fname=$('#search_student'+period).val();
	pdata.lname='';//$('#search_student'+period).val();
    $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>students/getstudents/",
              data: {'pdata':pdata},
              success: function(result)
              {
//                alert('result');  
//                $('#lesson_plans').html(result);
                //do something
                var students = '';
                result = jQuery.parseJSON(result);
                $.each(result.student,function(key,value){
//                    console.log(value.firstname);
                    students += '<option value="'+value.student_id+'">'+value.firstname+' '+value.lastname+'</option>';
                    
                });
                $('#student_id'+period).html(students);
              }
         });
}

function addstudent(period){

$.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>students/add_student",
              data: $('#addstudentfrm'+period).serialize(),
              success: function(result)
              {
                  var select_id = $('.tabbable .active').attr('id');
                  var idarr = select_id.split('_');
                  var id = idarr[1];
                  
                  result = jQuery.parseJSON(result);
                  console.log(result.status);
                  if(result.status==true){
                        $('#succmsg').html(result.message);
                        $('#successbtn').modal('show');
                    } else {
                        $('#errmsg').html(result.message);
                        $('#errorbtn').modal('show');
                    }
                    getthisroster(id);
//                alert('result');  
//                $('#lesson_plans').html(result);
                //do something
//                var students = '';
//                result = jQuery.parseJSON(result);
//                console.log(result);
//                $.each(result.student,function(key,value){
////                    console.log(value.firstname);
//                    students += '<option value="'+value.student_id+'">'+value.firstname+' '+value.lastname+'</option>';
//                    
//                });
//                $('#student_id'+period).html(students);
              }
         });
}

   </script>  
   
 
</body>
<!-- END BODY -->
</html>