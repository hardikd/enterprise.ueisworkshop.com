﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
     <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
    <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance">Attendance Manager</a>
                             <span class="divider">></span>
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manager Set-Up</a>
                           
                       </li>
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                   
                      <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>teacher" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-plus"></i><br><br><br>
                               Create Teachers                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                       <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>school_type" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-bell"></i><br><br><br>
                               School Type                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                         <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>class_room" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-pencil"></i>  <i class="icon-female"></i><br><br><br>
                              Homeroom/Classroom                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                    <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>grade_subject" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-bold"></i><br><br><br>
                              Grade/Subject Association                         </span>                        </a>            </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                              
                              
                              </div></div>
                                <!-- END GRID PORTLET-->
                 
                 
             
            <!-- END ROW 1-->
  
         
         <div class="space20"></div><div class="space20"></div>
         
          <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     
                      <div class="span3">
                             <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>teacher_grade_subject" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-user"></i><br><br><br>
                              Teacher/Grade Association                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                              
                              
                  
                    <div class="span3">
                             <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>period" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-time"></i><br><br><br>
                              School Period Scheduling                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                      <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>time_table" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-book"></i><br><br><br>
                               Timetable Assignments                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                         <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/add_parent_info" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-group"></i> <br><br><br>
                              Parent Student Association                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                    
             </div>
            <!-- END ROW 2-->
         </div>
         
         <div class="space20"></div><div class="space20"></div>
         
         <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                  <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/individual_students" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-male"></i><br><br><br>
                               Individual Students                         </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                 
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                 
                   
                      <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/map_parent" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-location-arrow"></i><br><br><br>
                               Student Mapping                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                        
                 
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            <!-- END ROW 1-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>

   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>