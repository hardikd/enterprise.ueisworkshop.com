<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
  <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color">
   <link href="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen">
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
         <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
          <div id="sidebar" class="nav-collapse collapse">
        <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                     <a href="#jobaidesModal" data-toggle="modal" ><i class="icon-folder-open"></i><span>Job Aides</span></a>
                   </h3>
            <div id="jobaidesModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                          <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="myModalLabel2"><i class="icon-wrench"></i> Job Aides</h3>
                          </div>
                          <div class="modal-body">
                            
                          </div>
                          <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-success">OK</button>
                          </div>
                        </div>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance">Attendance Manager</a>
                             <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Assessment</a>
                       </li>
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                    <div class="span3">
                             <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/daily_attendance" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-calendar"></i><br><br><br>
                              Daily Attendance                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                      <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/new_student_enrollment" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-user"></i><br><br><br>
                               Student Enrollment                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                         <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                      <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/roster" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-male"></i>  <i class="icon-female"></i><br><br><br>
                              Student Roster                            </span>                        </a>                    </div></div></div>
                        <!-- END GRID PORTLET-->
                    </div>
                    <?php if($this->session->userdata('login_type')=='teacher'):?>
                     <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/map_data" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-plus-sign"></i><br><br><br>
                               Add Map/Data                         </span>                        </a>                   
                    </div></div></div>
                                <!-- END GRID PORTLET-->
                 
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                  <?php endif;?>
                   <?php if($this->session->userdata('login_type')=='observer'):?>
                     <div class="span3">
                        <!-- BEGIN GRID SAMPLE PORTLET-->
                         <div class="metro-nav">
                       <div class="metro-nav metro-fix-view" >
                    <div class="metro-nav-block nav-block-blue long-dash-half">
                        <a href="<?php echo base_url();?>attendance/set_up" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-list-ol"></i><br><br><br>
                               Attendance Manager Set-Up                         </span>                        </a>                    </div></div></div>
                                <!-- END GRID PORTLET-->
                 
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
                  <?php endif;?>   
             </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
      <!-- END PAGE -->
   </div>
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

      
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   
   <script src="<?php echo SITEURLM?>js/jquery.easing.js" type="text/javascript"></script>
    <script src="<?php echo SITEURLM?>js/jqueryFileTree.js" type="text/javascript"></script>
    <link href="<?php echo SITEURLM?>css_new/jqueryFileTree.css" rel="stylesheet" type="text/css" media="screen" />
   <script>
   
   $(document).ready( function() {
    $('.modal-body').fileTree({
        root: '<?php echo $_SERVER['DOCUMENT_ROOT'];?>/Classroom Job Aides/ATTENDANCE MANAGER/',
        script: '<?php echo base_url();?>index/foldertree/ATTENDANCE MANAGER/',
        expandSpeed: 1000,
        collapseSpeed: 1000,
        multiFolder: false
    }, function(file) {
//        alert(file);
        
        window.open(file);
    });
});
   </script>

   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>