<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Observer::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
$('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',  closeText: 'close',sticky: true,positionBy: 'bottomTop'});

$('#nextdash').click(function(){

$('#previous').hide();
$('#nextdash').hide();
$('#next').show();
$('#previousdash').show();

});
$('#previousdash').click(function(){
$('#next').hide();
$('#previousdash').hide();
$('#previous').show();
$('#nextdash').show();

});


});



</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); 
	
	require_once($view_path.'inc/highcharts/Highchart.php'); 
	?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
        <?PHP
		$cach = date("H:i:s");
		$login_type=$this->session->userdata('login_type');
        $login_id=$this->session->userdata('observer_id');
		$rcc=1;		
		if(!empty($grades))
		{
		 //start observation
			
		$chart='chart'.$rcc;						
		$$chart = new Highchart();
		 foreach ($$chart->getScripts() as $script) {
         echo '<script type="text/javascript" src="' . $script . '"></script>';
      }
		
	echo '<script src="'.SITEURLM.'js/exporting.js"></script>
				<script type="text/javascript">jQuery.noConflict();</script>';
				echo "<script type='text/javascript'>
		var theme = {
   colors: ['#058DC7', '#50B432', '#910000', '#33C6E7', '#492970', '#F6F826', '#263C53', '#FFF263', '#6AF9C4'],
   chart: {
      backgroundColor: {
         
      }
   },
   title: {
      style: {
         color: '#000',
         font: 'bold 16px Trebuchet MS, Verdana, sans-serif'
      }
   },
   subtitle: {
      style: {
         color: '#666666',
         font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
      }
   },
   xAxis: {
      gridLineWidth: 1,
      lineColor: '#000',
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'

         }
      }
   },
   yAxis: {
      minorTickInterval: 'auto',
      lineColor: '#000',
      lineWidth: 1,
      tickWidth: 1,
      tickColor: '#000',
      labels: {
         style: {
            color: '#000',
            font: '11px Trebuchet MS, Verdana, sans-serif'
         }
      },
      title: {
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'
         }
      }
   },
   legend: {
      itemStyle: {
         font: '9pt Trebuchet MS, Verdana, sans-serif',
         color: 'black'

      },
      itemHoverStyle: {
         color: '#039'
      },
      itemHiddenStyle: {
         color: 'gray'
      }
   },
   labels: {
      style: {
         color: '#99b'
      }
   },

   navigation: {
      buttonOptions: {
         theme: {
            stroke: '#CCCCCC'
         }
      }
   }
};

var highchartsOptions = Highcharts.setOptions(theme);
</script>		";
		
		
		$namelimit=array();
		$checklist=array();
		$scaled=array();
		$proficiency=array();
		$likert=array();
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $checklist[]=intval($grades[$pj]['checklist']); 
		  $scaled[]=intval($grades[$pj]['scaled']); 
		  $proficiency[]=intval($grades[$pj]['proficiency']); 
		  $likert[]=intval($grades[$pj]['likert']); 		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			
			$$chart->series[] = array('name' => 'Checklist','data' => $checklist);	
			$$chart->series[] = array('name' => 'Scaled Rubric','data' => $scaled);	
			$$chart->series[] = array('name' => 'Proficiency Rubric','data' => $proficiency);	
			$$chart->series[] = array('name' => 'Likert','data' => $likert);				
			
			
			$$chart->chart->renderTo = "container".$rcc;
			$$chart->chart->type = "column";
			$$chart->title->text = "Observations";
			$$chart->xAxis->categories = $namelimit;
			$$chart->credits->text = '';			
			
			$$chart->yAxis->min = 0;
			$$chart->yAxis->allowDecimals =false;
			$$chart->exporting->enabled =true;
$$chart->yAxis->title->text = "";
$$chart->yAxis->stackLabels->enabled = 1;
$$chart->yAxis->stackLabels->style->fontWeight = "bold";
$$chart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$chart->legend->align = "right";
$$chart->legend->x = -100;
$$chart->legend->verticalAlign = "top";
$$chart->legend->y = 20;
$$chart->legend->floating = 1;
$$chart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$chart->legend->borderColor = "#CCC";
$$chart->legend->borderWidth = 1;
$$chart->legend->shadow = false;
$$chart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$chart->plotOptions->column->stacking = "normal";
$$chart->plotOptions->column->dataLabels->enabled = 1;
$$chart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$chart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");


          //start pdlc
			
			
		
		$namelimit=array();
		$pdlc=array();
		
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $pdlc[]=intval($grades[$pj]['pdlc']); 
		   
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$pdlcchart='pdlcchart'.$rcc;
			$$pdlcchart = new Highchart();	
			$$pdlcchart->series[] = array('name' => 'Professional Development','data' => $pdlc);	
			
			
			
			$$pdlcchart->chart->renderTo = "pdlccontainer".$rcc;
			$$pdlcchart->chart->type = "column";
			$$pdlcchart->title->text = "Professional Development Activity";
			$$pdlcchart->xAxis->categories = $namelimit;
			$$pdlcchart->credits->text = '';
			
			$$pdlcchart->yAxis->min = 0;
			$$pdlcchart->yAxis->allowDecimals =false;
			$$pdlcchart->exporting->enabled =true;
$$pdlcchart->yAxis->title->text = "";
$$pdlcchart->yAxis->stackLabels->enabled = 1;
$$pdlcchart->yAxis->stackLabels->style->fontWeight = "bold";
$$pdlcchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$pdlcchart->legend->align = "right";
$$pdlcchart->legend->x = -100;
$$pdlcchart->legend->verticalAlign = "top";
$$pdlcchart->legend->y = 20;
$$pdlcchart->legend->floating = 1;
$$pdlcchart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$pdlcchart->legend->borderColor = "#CCC";
$$pdlcchart->legend->borderWidth = 1;
$$pdlcchart->legend->shadow = false;
$$pdlcchart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$pdlcchart->plotOptions->column->stacking = "normal";
$$pdlcchart->plotOptions->column->dataLabels->enabled = 1;
$$pdlcchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$pdlcchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
			
		
		//start lesson plan
			
		
		$namelimit=array();
		$planeld=array();
		$plan=array();
		
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $planeld[]=intval($grades[$pj]['planeld']); 
		  $plan[]=intval($grades[$pj]['plan']); 
		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$lessonchart='lessonchart'.$rcc;
			$$lessonchart = new Highchart();	
			$$lessonchart->series[] = array('name' => 'ELD Subject','data' => $planeld);	
			$$lessonchart->series[] = array('name' => 'Other','data' => $plan);	
			
			
			
			$$lessonchart->chart->renderTo = "lessoncontainer".$rcc;
			$$lessonchart->chart->type = "column";
			$$lessonchart->title->text = "Lesson Planning";
			$$lessonchart->xAxis->categories = $namelimit;
			$$lessonchart->credits->text = '';			
			
			$$lessonchart->yAxis->min = 0;
			$$lessonchart->yAxis->allowDecimals =false;
			$$lessonchart->exporting->enabled =true;
$$lessonchart->yAxis->title->text = "";
$$lessonchart->yAxis->stackLabels->enabled = 1;
$$lessonchart->yAxis->stackLabels->style->fontWeight = "bold";
$$lessonchart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$lessonchart->legend->align = "right";
$$lessonchart->legend->x = -100;
$$lessonchart->legend->verticalAlign = "top";
$$lessonchart->legend->y = 20;
$$lessonchart->legend->floating = 1;
$$lessonchart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$lessonchart->legend->borderColor = "#CCC";
$$lessonchart->legend->borderWidth = 1;
$$lessonchart->legend->shadow = false;
$$lessonchart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$lessonchart->plotOptions->column->stacking = "normal";
$$lessonchart->plotOptions->column->dataLabels->enabled = 1;
$$lessonchart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$lessonchart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
			
		

		//start lesson plan notification
			
		
		$namelimit=array();
		$schooltohome=array();
		$behviour=array();
		$homework=array();
		
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $schooltohome[]=intval($grades[$pj]['schooltohome']); 
		  $behviour[]=intval($grades[$pj]['behviour']); 
		   $homework[]=intval($grades[$pj]['homework']); 
		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$notechart='notechart'.$rcc;
			$$notechart = new Highchart();	
			$$notechart->series[] = array('name' => 'School-to-Home','data' => $schooltohome);	
			$$notechart->series[] = array('name' => 'Behavior & Learning','data' => $behviour);	
			$$notechart->series[] = array('name' => 'Homework','data' => $homework);	
			
			
			
			$$notechart->chart->renderTo = "notecontainer".$rcc;
			$$notechart->chart->type = "column";
			$$notechart->title->text = "Lesson Plan Notifications";
			$$notechart->xAxis->categories = $namelimit;
			$$notechart->credits->text = '';			
			
			$$notechart->yAxis->min = 0;
			$$notechart->yAxis->allowDecimals =false;
			$$notechart->exporting->enabled =true;
$$notechart->yAxis->title->text = "";
$$notechart->yAxis->stackLabels->enabled = 1;
$$notechart->yAxis->stackLabels->style->fontWeight = "bold";
$$notechart->yAxis->stackLabels->style->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
$$notechart->legend->align = "right";
$$notechart->legend->x = -100;
$$notechart->legend->verticalAlign = "top";
$$notechart->legend->y = 20;
$$notechart->legend->floating = 1;
$$notechart->legend->backgroundColor = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
$$notechart->legend->borderColor = "#CCC";
$$notechart->legend->borderWidth = 1;
$$notechart->legend->shadow = false;
$$notechart->tooltip->formatter = new HighchartJsExpr("function() {
    return '<b>'+ this.x +'</b><br/>'+
    this.series.name +': '+ this.y +'<br/>'+
    'Total: '+ this.point.stackTotal;}");

$$notechart->plotOptions->column->stacking = "normal";
$$notechart->plotOptions->column->dataLabels->enabled = 1;
$$notechart->plotOptions->column->dataLabels->formatter= new HighchartJsExpr("function() {
                            if (this.y != 0) {
                              return this.y;
                            } else {
                              return '';
                            }
							}");
$$notechart->plotOptions->column->dataLabels->color = new HighchartJsExpr("(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
			
		

		
		}
		?>
		<table>
		<tr>
		<td>
		Welcome <?php echo $this->session->userdata('observer_name');?>
		</td>
		</tr>
		<tr>
		<td>
		<input type="button" name="previousdash" id="previousdash" value="Previous Dashboard" style="display:none;">
		<input type="button" name="nextdash" value="Next Dashboard" id="nextdash">
		</td>
		</tr>
		</table>
		<table id="previous">
		<tr>
		<td>		
	
	<div id="lessoncontainer<?php echo $rcc?>" style="width:650px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='lessonchart'.$rcc;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
		</td>
		</tr>
		<tr>
		<td>		
	
	<div id="notecontainer<?php echo $rcc?>" style="width:650px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='notechart'.$rcc;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
		</td>
		</tr>
		</table>
		<table id="next" style="display:none;" >
		<tr>
		<td>		
	
	<div id="pdlccontainer<?php echo $rcc?>" style="width:650px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='pdlcchart'.$rcc;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
		</td>
		</tr>
		<tr>
		<td>		
	
	<div id="container<?php echo $rcc?>" style="width:650px;"></div>		
    <script type="text/javascript">    
	<?php
		 $chart='chart'.$rcc;	 
		
      echo $$chart->render("$chart");	  
    ?>
    </script>
	
		</td>
		</tr>
		</table>
		<table  cellpadding="4">
		
		
		<tr>
		<td>
		<b><font size="4px">Scheduled Activty Reports</font></b>
		</td>
		</tr>
		<!--start of Lesson Plan-->
		<tr>
		<td>
		<b>Lesson Plans (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteacherslesson;?> of <?php echo $totallesson;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernameslesson!=false){ $k=1;foreach($suteachernameslesson as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernameslesson!=''){ $k=1;foreach($teachernameslesson as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Lesson  Plan-->
		<!--start of Periodic Goal Plan-->
		<tr>
		<td>
		<b>Periodic Goal-Setting (Year: <?php echo date('Y');?>-<?php echo date('Y')+1;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteachers;?> of <?php echo $totalsubmitted;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernames!=false){ $k=1;foreach($suteachernames as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernames!=''){ $k=1;foreach($teachernames as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Periodic Goal Plan-->
		
		<!--start of Observation Plan-->
		<tr>
		<td>
		<b>Observation Plans (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteachersobservation;?> of <?php echo $totalobservation;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernamesobservation!=false){ $k=1;foreach($suteachernamesobservation as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernamesobservation!=''){ $k=1;foreach($teachernamesobservation as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Observation Plan-->
		<!--start of Observation Plan-->
		<tr>
		<td>
		<b><font size="4px">Upcoming Scheduled Activities</font></b>
		</td>
		</tr>
		<tr>
		<td>
		<b>Scheduled Observations (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Scheduled:&nbsp;&nbsp;</b><?php echo $suteachersobservations;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Scheduled|<?php if($suteachernamesobservations!=false){ $k=1;foreach($suteachernamesobservations as $name) { echo $k.'. '.$name['firstname'].' '.$name['lastname'].' ('.$name['created'].')<br />Start Time: '.$name['starttime'].' - End Time: '.$name['endtime']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<!--<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php //if($teachernamesobservations!=''){ $k=1;foreach($teachernamesobservations as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>-->
		<!--End of Observations-->
		</table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
