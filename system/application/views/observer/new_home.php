
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" style="overflow: hidden;"><!--<![endif]--><!-- BEGIN HEAD --><head>
   <meta charset="utf-8">
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color">
   <link href="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen">
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
.paddingClass{
    
    margin:20px;
    padding:5px;
}
.transparent {background: none;-webkit-box-shadow:none !important; box-shadow: none !important;border:none;width:95%;left:300px;height: 565px !important;overflow: hidden;}
.entrance-close {color:#FFFFFF;opacity: 1; font-size: 50px;}
.entrance-close:hover {color:#FFFFFF;opacity: .5; font-size: 50px;}
.entrance-label {font-size: 36px !important;opacity: 1; color: #FFFFFF;text-align: center;}
#myModal-entrance h3{font-size: 36px !important;opacity: 1; color: #FFFFFF;text-align: center; font-weight: bolder;font-family: 'ArialMT', 'Arial';}
#myModal-entrance .modal-header {border:none;}
.entrance-footer {font-size:36px;color:#FFFFFF;opacity: 1;text-align: center;}
#myModal-entrance ul {width: 100%;list-style: none;}
#myModal-entrance ul li {float: left;width: 21%;margin-left:40px;}
#myModal-entrance a{color:#FFFFFF;}
#myModal-entrance .modal-body {max-height: 500px;}
.entrance-list h3{height: 75px;}
.modal-header .close {height: 30px;}
</style></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid sidebar-closed">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll" style="overflow: hidden; outline: none;" tabindex="5000">
        <div id="sidebar" class="nav-collapse collapse" style="margin-left: -180px;">
         <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" style="margin-left: 0px;">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12" >
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-home"></i>&nbsp; Home
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="#">Home</a>
                          
                       </li>
                       
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <!--BEGIN METRO STATES-->
                
           
                    
                <div class="metro-nav">
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-green long-dash">
                        <a href="<?php echo base_url();?>planningmanager" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-book"></i><br><br><br>
                                Planning Manager
                            </span>
                           
                        </a>
                    </div>
                    
                     <div class="metro-nav-block nav-block-blue long-dash">
                        <a href="<?php echo base_url();?>attendance" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-calendar"></i><br><br><br>
                               Attendance Manager
                            </span>
                            
                        </a>
                    </div>
                    
                    
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-red long-dash">
                        <a href="<?php echo base_url();?>implementation" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-pencil"></i><br><br><br>
                             Implementation Manager 
                            </span>
                            
                        </a>
                    </div>
                    
                      <div class="metro-nav-block nav-block-yellow long-dash">
                        <a href="<?php echo base_url();?>classroom" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-group"></i><br><br><br>
                                Classroom Management Assistant
                            </span>
                            
                        </a>
                    </div>
                    
                          <div class="metro-nav-block nav-block-orange long-dash">
                        <a href="<?php echo base_url();?>assessment" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-bar-chart"></i><br><br><br>
                               Assessment Manager
                            </span>
                            
                        </a>
                    </div>
                    
                    
                         
                    
                    
                        
                    
                    
                          <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>tools" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-wrench"></i><br><br><br>
                               Tools &amp; Resources 
                            </span>
                           
                        </a>
                    </div>
                </div>
              
                <div class="space10"></div>
                <!--END METRO STATES-->
            </div>
             </div>
           
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
      UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

 <div id="myModal-add-new" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:60%;left:598px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Workshop Support Options</h3>
                                </div>
                              
                                <div class="modal-body">
                                <form enctype="multipart/form-data" name='studentform1' class="form-horizontal" id='studentform1' method='post'  >
                                <span style="color: Red;display:none" id="message"></span>
                                <div class="control-group" style="text-align:center;">
                                    <button class="btn workshop_option opt-btn" type='button' name='video_option' id='video_tutorial' value='with_support'  aria-hidden="true"><span style="float:left;margin-right:10px;"><img src="../../<?php echo $view_path;?>inc/logo/u15.png" alt="video"></span><span>Access Workshop with <br />Video Tutorial Support</span></button> 
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button class="btn workshop_option opt-btn" type='button' name='video_option' id='access_workshop' value='without_support' data-dismiss="modal" aria-hidden="true">Access Workshop</button>
                                             
                                             <div class="controls">
                                               
                                                
                                             </div>
                                    </div>
                                           <br />
                                          <div class="control-group" style="margin-left:9%;">
                                    
                                        
                                        <span style="float:left;margin-right:10px;">
                                            <input type="checkbox" class="default checkbox " id="showpopup" name="showpopup">&nbsp;
                                        </span>
                                              <span style="float:left;margin-top:3px;font-size:28px;"><b>Please do not show me this option again</b></span>
                                            
                                        </div>
                                      <br /><br />
                                          <div class="control-group" style="margin-left:1%;font-size:18px;">
                                    
                                              <b>Job Aides and Video Tutorials are located at the top right of each Workshop Modules</b>
<!--                                            To turn the Tutorial support back on, please go to Tools: User Training Guide-->
                                   
                                        </div>   
                                             
                                      </form>        
                                    
                                </div>
<!--                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>
                                </div>-->
                    
                            </div>

                        <div id="myModal-entrance" class="modal hide fade transparent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close entrance-close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4 entrance-label">How do you want to get started?</h3>
                                </div>
                              
                                <div class="modal-body">
                                <form enctype="multipart/form-data" name='studentform1' class="form-horizontal" id='studentform1' method='post'  >
                                <span style="color: Red;display:none" id="message"></span>
                                <br />
                                          <div class="control-group" >
                                    
                                        <ul class="entrance-list">
                                          <li>
                                            <h3>
                                            Plan a Lesson?</h3>
                                            
                                            <div>
                                              <a href="<?php echo base_url();?>index/video_player/Lesson PlanningConverted/" target="_blank">
                                              <img src="<?php echo SITEURLM;?>system/application/views/inc/logo/lessonplan.jpg" width="290px" />
                                              </a>
                                            </div>
                                          </li>
                                          <li>
                                            <div>
                                            <h3>Conduct a SST?</h3>
                                            </div>
                                            <div>
                                              <a href="<?php echo base_url();?>/index/video_player/SST%20Video%20DemonstrationConverted/" target="_blank">
                                                <img src="<?php echo SITEURLM;?>system/application/views/inc/logo/behavior.jpg" width="290px" />
                                              </a>
                                            </div>
                                          </li>
                                          <li>
                                            <div>
                                            <h3>Record Grades?</h3>
                                            </div>
                                            <div>
                                              <a href="<?php echo base_url();?>index/video_player/Grade TrackerConverted/" target="_blank">
                                              <img src="<?php echo SITEURLM;?>system/application/views/inc/logo/recordgrades.jpg" width="290px" />
                                            </a>
                                            </div>
                                          </li>
                                          <li>
                                            <div>
                                            <h3>Record an Incident?</h3>
                                            </div>
                                            <div>
                                              <a href="<?php echo base_url();?>index/video_player/Behavior%20Running%20RecordConverted/" target="_blank">
                                                <img src="<?php echo SITEURLM;?>system/application/views/inc/logo/behavior.jpg" width="290px" />
                                              </a>
                                            </div>
                                          </li>
                                          
                                          
                                        </ul>
                                       
                                            
                                        </div>
                                      <br /><br />
                                          <div class="control-group entrance-footer">
                                    
                                              <b><u><a href="javascript:;" onclick="openhelp();">Click here to view the entire help library.</a></u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-book"></i></b>
<!--                                            To turn the Tutorial support back on, please go to Tools: User Training Guide-->
                                   
                                        </div>   
                                             
                                      </form>        
                                    
                                </div>
<!--                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>
                                </div>-->
                    
                            </div>

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="<?php echo SITEURLM?>js/excanvas.js"></script>
   <script src="<?php echo SITEURLM?>js/respond.js"></script>
   <![endif]-->

   <script src="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/chart-master/Chart.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="<?php echo SITEURLM?>js/easy-pie-chart.js"></script>
   <script src="<?php echo SITEURLM?>js/sparkline-chart.js"></script>
   <script src="<?php echo SITEURLM?>js/home-page-calender.js"></script>
   <script src="<?php echo SITEURLM?>js/chartjs.js"></script>
   <script>
   

    function openhelp(){
      $('#myModal-entrance').modal('hide');
      $('#myModal-help-library').modal('show');
    }
//$('#myModal-entrance').modal('show');
      <?php if($popupcntr==1):?>
    $('#myModal-entrance').modal('show');
<?php endif;?>
   </script>

   <!-- END JAVASCRIPTS -->   

<!-- END BODY -->
</div></body></html>