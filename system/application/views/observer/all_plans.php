
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" style="overflow: hidden;"><!--<![endif]--><!-- BEGIN HEAD --><head>
   <meta charset="utf-8">
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color">
   <link href="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen">
<style type="text/css">
.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
.metro-fix-view .metro-nav-block.long-dash {width:100% !important;}
.Price {padding-top: 150px;font-size: 120px;}

.metro-nav .metro-nav-block a:hover i{
    transform:none !important;
    -moz-transform:none !important;
    -webkit-transform:none !important;
    -o-transform:none !important;
    font-size: 40px !important;
    font-weight: 400 !important;
}

.metro-nav .metro-nav-block.long a:hover i{

    font-size: 22px !important;
    font-weight: 400 !important;
}

.metro-nav .metro-nav-block a:hover .info {
    transform:none !important;
    -moz-transform:none !important;
    -webkit-transform:none !important;
    -o-transform:none !important;
    font-size: 40px !important;
    font-weight: 400 !important;
}
.metro-fix-view .metro-nav-block.long-dash {width:100% !important;}

  /* COMMON PRICING STYLES */
    .panel.price,
    .panel.price>.panel-heading{
      border-radius:0px;
       -moz-transition: all .3s ease;
      -o-transition:  all .3s ease;
      -webkit-transition:  all .3s ease;
    }
    .panel.price:hover{
      box-shadow: 0px 0px 30px rgba(0,0,0, .2);
    }
    .panel.price:hover>.panel-heading{
      box-shadow: 0px 0px 30px rgba(0,0,0, .2) inset;
    }
    
    .list-group {list-style:none;}
        
    .panel.price>.panel-heading{
      box-shadow: 0px 5px 0px rgba(50,50,50, .2) inset;
      text-shadow:0px 3px 0px rgba(50,50,50, .6);
    }
      
    .price .list-group-item{
      border-bottom-:1px solid rgba(250,250,250, .5);
      line-height:30px;
    }
    
    .panel.price .list-group-item:last-child {
      border-bottom-right-radius: 0px;
      border-bottom-left-radius: 0px;
    }
    .panel.price .list-group-item:first-child {
      border-top-right-radius: 0px;
      border-top-left-radius: 0px;
    }
    
    .price .panel-footer {
      color: #fff;
      border-bottom:0px;
      background-color:  rgba(0,0,0, .1);
      box-shadow: 0px 3px 0px rgba(0,0,0, .3);
    }
    
    
    .panel.price .btn{
      box-shadow: 0 -1px 0px rgba(50,50,50, .2) inset;
      border:0px;
    }
    
  /* green panel */
  
         .panel-green {
    border-color: #97c976;
      border: 1px solid #97c976;
      border-top:none;
  }
    .price.panel-green>.panel-heading {
      color: #fff;
      background-color: #74b749;
      border-color: #97c976;
      border-bottom: 1px solid #97c976;
    }
    
      
    .price.panel-green>.panel-body {
      color: #fff;
      background-color: #65C965;
    }
        
    
    .price.panel-green>.panel-body .lead{
        text-shadow: 0px 3px 0px rgba(50,50,50, .3);
    }
    
    .price.panel-green .list-group-item {
      color: #333;
      background-color: rgba(50,50,50, .01);
      font-weight:600;
      text-shadow: 0px 1px 0px rgba(250,250,250, .75);
    }
    
    /* blue panel */
  
      .panel-blue {
    border-color: #78AEE1;
      border: 1px solid #78AEE1;
      border-top:none;
  }
    .price.panel-blue>.panel-heading {
      color: #fff;
      background-color: #0DAED3;
      border-color: #78AEE1;
      border-bottom: 1px solid #78AEE1;
    }
    
      
    .price.panel-blue>.panel-body {
      color: #fff;
      background-color: #26cbf1;
    }
        
    
    .price.panel-blue>.panel-body .lead{
        text-shadow: 0px 3px 0px rgba(50,50,50, .3);
    }
    
    .price.panel-blue .list-group-item {
      color: #333;
      background-color: rgba(50,50,50, .01);
      font-weight:600;
      text-shadow: 0px 1px 0px rgba(250,250,250, .75);
    }
    
    /* red price */
    
  .panel-red {
    border-color: #ee4673;
      border: 1px solid #ee4673;
      border-top:none;
  }
    .price.panel-red>.panel-heading {
      color: #fff;
      background-color: #de577b;
      border-color: #ee4673;
      border-bottom: 1px solid #ee4673;
    }
    
      
    .price.panel-red>.panel-body {
      color: #fff;
      background-color: #ea97ad;
    }
    
    
    
    
    .price.panel-red>.panel-body .lead{
        text-shadow: 0px 3px 0px rgba(50,50,50, .3);
    }
    
    .price.panel-red .list-group-item {
      color: #333;
      background-color: rgba(50,50,50, .01);
      font-weight:600;
      text-shadow: 0px 1px 0px rgba(250,250,250, .75);
    }
    
    /* grey price */
    
  .panel-grey {
    border-color: #ffa500;
      border: 1px solid #ffa500;
      border-top:none;
  }
    .price.panel-grey>.panel-heading {
      color: #fff;
      background-color: #FFB400;
      border-color: #ffa500;
      border-bottom: 1px solid #ffa500;
    }
    
      
    .price.panel-grey>.panel-body {
      color: #fff;
      background-color: #ffc63f;
    }
    

    
    .price.panel-grey>.panel-body .lead{
        text-shadow: 0px 3px 0px rgba(50,50,50, .3);
    }
    
    .price.panel-grey .list-group-item {
      color: #333;
      background-color: rgba(50,50,50, .01);
      font-weight:600;
      text-shadow: 0px 1px 0px rgba(250,250,250, .75);
    }
    

   /* purple price */
    
  .panel-purple {
    border-color: #622c6a;
      border: 1px solid #622c6a;
      border-top:none;
  }
    .price.panel-purple>.panel-heading {
      color: #fff;
      background-color: #5e3364;
      border-color: #622c6a;
      border-bottom: 1px solid #622c6a;
    }
    
      
    .price.panel-purple>.panel-body {
      color: #fff;
      background-color: #753f7d;
    }
    

    
    .price.panel-purple>.panel-body .lead{
        text-shadow: 0px 3px 0px rgba(50,50,50, .3);
    }
    
    .price.panel-purple .list-group-item {
      color: #333;
      background-color: rgba(50,50,50, .01);
      font-weight:600;
      text-shadow: 0px 1px 0px rgba(250,250,250, .75);
    }

    /* white price */
    
  
    .price.panel-white>.panel-heading {
      color: #fff;
      background-color: #5e3364;
      border-color: #622c6a;
      border-bottom: 1px solid #622c6a;
      text-shadow: 0px 2px 0px rgba(250,250,250, .7);
    }
    
    
      
    .price.panel-white>.panel-body {
      color: #fff;
      background-color: #753f7d;
    }
        
    .price.panel-white>.panel-body .lead{
       text-shadow: 0px 3px 0px rgba(50,50,50, .3);
       
    }
    
    .price:hover.panel-white>.panel-body .lead{
        text-shadow: 0px 2px 0px rgba(250,250,250, .9);
        color:#333;
    }
    
    .price.panel-white .list-group-item {
      color: #333;
      background-color: rgba(50,50,50, .01);
      font-weight:600;
      text-shadow: 0px 1px 0px rgba(250,250,250, .75);
    }
  </style>
</style></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid sidebar-closed">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll" style="overflow: hidden; outline: none;" tabindex="5000">
        <div id="sidebar" class="nav-collapse collapse" style="margin-left: -180px;">
         <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" style="margin-left: 0px;">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12" >
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-home"></i>&nbsp; Home
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="#">Home</a>
                          
                       </li>
                       
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <!--BEGIN METRO STATES-->
                
           
          <div class="container">          
                  <div class="row">
          <div class="span4">
        
          <!-- PRICE ITEM -->
          <div class="panel price panel-green">
            <div class="panel-heading  text-center">
            <h3>Planning Module</h3>
            </div>
            <div class="panel-body text-center">
              <p class="lead" style="font-size:14px"><b><u>Pay As You Go:</u></b> $6 per month (each staff member)<br />
                                <b><u>Annual Fee:</u></b> $60 per year (each staff member)</p>
              
            </div>
            <ul class="list-group list-group-flush text-center">
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item"><i class="icon-ok text-danger"></i> Lesson Planning</li>
              <li class="list-group-item"><i class="icon-ok text-danger"></i> SMART Goals</li>
              <li class="list-group-item"><i class="icon-ok text-danger"></i> Calendar</li>
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item">&nbsp;</li>
            </ul>
            <div class="panel-footer">
              <a class="btn btn-lg btn-block btn-green" href="<?php echo base_url();?>index/subscribe/planning">BUY NOW!</a>
            </div>
          </div>
          <!-- /PRICE ITEM -->
          
          
        </div>
        
        <div class="span4">
        
          <!-- PRICE ITEM -->
          <div class="panel price panel-blue">
            <div class="panel-heading arrow_box text-center">
            <h3>Attendance Module</h3>
            </div>
            <div class="panel-body text-center">
             <p class="lead" style="font-size:14px"><b><u>Pay As You Go:</u></b> $6 per month (each staff member)<br />
                                <b><u>Annual Fee:</u></b> $60 per year (each staff member)</p>
            </div>
            <ul class="list-group list-group-flush text-center">
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item"><i class="icon-ok text-info"></i> Enrolling Students </li>
              <li class="list-group-item"><i class="icon-ok text-info"></i> Taking Attendance</li>
              <li class="list-group-item"><i class="icon-ok text-info"></i> Attendance Reports</li>
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item">&nbsp;</li>
            </ul>
            <div class="panel-footer">
              <a class="btn btn-lg btn-block btn-blue" href="<?php echo base_url();?>index/subscribe/attendance">BUY NOW!</a>
            </div>
          </div>
          <!-- /PRICE ITEM -->
          
          
        </div>
        
        <div class="span4">
        
          <!-- PRICE ITEM -->
          <div class="panel price panel-red">
            <div class="panel-heading arrow_box text-center">
            <h3>Implementation Module</h3>
            </div>
            <div class="panel-body text-center">
              <p class="lead" style="font-size:14px"><b><u>Pay As You Go:</u></b> $9 per month (each staff member)<br />
                                <b><u>Annual Fee:</u></b> $90 per year (each staff member)</p>
            </div>
            <ul class="list-group list-group-flush text-center">
              <li class="list-group-item"><i class="icon-ok text-success"></i> Grade Tracker</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> Homework</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> ELD Tracker</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> Instructional Efficacy</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> IEP Tracker</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Planning Manager</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Attendance Manager</li>
            </ul>
            <div class="panel-footer">
              <a class="btn btn-lg btn-block btn-red" href="<?php echo base_url();?>index/subscribe/implementation">BUY NOW!</a>
            </div>
          </div>
          <!-- /PRICE ITEM -->
          
          
        </div>
        </div>
        <br />
         <div class="row">
        <div class="span2">
        </div>  
        <div class="span4">
        
          <!-- PRICE ITEM -->
          <div class="panel price panel-grey">
            <div class="panel-heading arrow_box text-center">
            <h3>Classroom Module</h3>
            </div>
            <div class="panel-body text-center">
              <p class="lead" style="font-size:14px"><b><u>Pay As You Go:</u></b> $9 per month (each staff member)<br />
                                <b><u>Annual Fee:</u></b> $90 per year (each staff member)</p>
            </div>
            <ul class="list-group list-group-flush text-center">
              <li class="list-group-item"><i class="icon-ok text-success"></i> SST</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> Parent Conference</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> Student Conference</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> Behavior Running Record</li>
              <li class="list-group-item"><i class="icon-ok text-success"></i> Report Card (coming soon)</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Planning Module</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Attendance Module</li>
            </ul>
            <div class="panel-footer">
              <a class="btn btn-lg btn-block btn-yellow" href="<?php echo base_url();?>index/subscribe/classroom">BUY NOW!</a>
            </div>
          </div>
          <!-- /PRICE ITEM -->
          
          
        </div>
        
        <div class="span4">
        
          <!-- PRICE ITEM -->
          <div class="panel price panel-purple">
            <div class="panel-heading arrow_box text-center">
            <h3>Resource Module</h3>
            </div>
            <div class="panel-body text-center">
              <p class="lead" style="font-size:14px"><b><u>Pay As You Go:</u></b> $18 per month (each staff member)<br />
                                <b><u>Annual Fee:</u></b> $195 per year (each staff member)</p>
            </div>
            <ul class="list-group list-group-flush text-center">
              <li class="list-group-item">&nbsp;</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Data Tracker</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Planning Module</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Attendance Module</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Implementation Module</li>
              <li class="list-group-item"><i class="icon-asterisk text-success"></i> Classroom Module</li>
              <li class="list-group-item">&nbsp;</li>

            </ul>
            <div class="panel-footer">
              <a class="btn btn-lg btn-block btn-purple" href="<?php echo base_url();?>index/subscribe/resource">BUY NOW!</a>
            </div>
          </div>
          <!-- /PRICE ITEM -->
          
          
        </div>
      
      </div>
           </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
      UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="<?php echo SITEURLM?>js/excanvas.js"></script>
   <script src="<?php echo SITEURLM?>js/respond.js"></script>
   <![endif]-->

   <script src="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/chart-master/Chart.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="<?php echo SITEURLM?>js/easy-pie-chart.js"></script>
   <script src="<?php echo SITEURLM?>js/sparkline-chart.js"></script>
   <script src="<?php echo SITEURLM?>js/home-page-calender.js"></script>
   <script src="<?php echo SITEURLM?>js/chartjs.js"></script>

   <!-- END JAVASCRIPTS -->   

<!-- END BODY -->
</div></body></html>