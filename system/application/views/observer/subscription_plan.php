
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" style="overflow: hidden;"><!--<![endif]--><!-- BEGIN HEAD --><head>
   <meta charset="utf-8">
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color">
   <link href="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
   <link href="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen">
<style type="text/css">
.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
.metro-fix-view .metro-nav-block.long-dash {width:100% !important;}
.Price {padding-top: 150px;font-size: 120px;}

.metro-nav .metro-nav-block a:hover i{
    transform:none !important;
    -moz-transform:none !important;
    -webkit-transform:none !important;
    -o-transform:none !important;
    font-size: 40px !important;
    font-weight: 400 !important;
}

.metro-nav .metro-nav-block.long a:hover i{

    font-size: 22px !important;
    font-weight: 400 !important;
}

.metro-nav .metro-nav-block a:hover .info {
    transform:none !important;
    -moz-transform:none !important;
    -webkit-transform:none !important;
    -o-transform:none !important;
    font-size: 40px !important;
    font-weight: 400 !important;
}
.metro-fix-view .metro-nav-block.long-dash {width:100% !important;}
</style></head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid sidebar-closed">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll" style="overflow: hidden; outline: none;" tabindex="5000">
        <div id="sidebar" class="nav-collapse collapse" style="margin-left: -180px;">
         <!-- BEGIN SIDEBAR MENU -->
         <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content" style="margin-left: 0px;">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12" >
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-home"></i>&nbsp; Home
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="#">Home</a>
                          
                       </li>
                       
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <!--BEGIN METRO STATES-->
                
           
                    
                <div class="metro-nav">
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-green long-dash" style="width:100% !important;">
                        <a href="<?php echo base_url();?>index/payment/yearly/<?php echo $plan;?>" class="text-center" data-original-title="">
                            <span class="value">
                               <!-- <i class="icon-book"></i>--><br><br><br>
                                  Annual Subscription
                                  
                            </span>
                           <span class="Price"><i class="icon-usd"></i><?php echo $yearly;?></span>
                           <span style="width:33%;float:right;color:#000000;text-align:justify;font-weight:bolder;">
                                  <h2 style="text-align:center;"><u>Annual</u></h2>
                                  $<?php echo $yearly;?> for one year access (each Staff member).  
                                  <br /><br />
                                  You can choose number of staff members on next payment screen
                                  
                           </span>
                        </a>
                    </div>
                    
              
                <div class="space10"></div>
                <!--END METRO STATES-->
            </div>

                 <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-blue long-dash" style="width:100% !important;">
                        <a href="<?php echo base_url();?>index/payment/monthly/<?php echo $plan;?>" class="text-center" data-original-title="">
                            <span class="value">
                                <!--<i class="icon-book"></i>--><br><br><br>
                                  Monthly Payment
                            </span>
                            <span class="Price"><i class="icon-usd"></i><?php echo $monthly;?></span>
                           <span style="width:33%;float:right;color:#000000;text-align:justify;font-weight:bolder;">
                                  <h2 style="text-align:center;"><u>Pay as You Go</u></h2>
                                  Monthly payment of $<?php echo $monthly;?> (each staff member).  
                                  <br /><br />
                                  You can choose number of staff members on next payment screen
                          </span>
                        </a>
                    </div>
                    
              
                <div class="space10"></div>
                <!--END METRO STATES-->
            </div>
             </div>
           
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
      UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="<?php echo SITEURLM?>js/excanvas.js"></script>
   <script src="<?php echo SITEURLM?>js/respond.js"></script>
   <![endif]-->

   <script src="<?php echo SITEURLM?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/chart-master/Chart.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="<?php echo SITEURLM?>js/easy-pie-chart.js"></script>
   <script src="<?php echo SITEURLM?>js/sparkline-chart.js"></script>
   <script src="<?php echo SITEURLM?>js/home-page-calender.js"></script>
   <script src="<?php echo SITEURLM?>js/chartjs.js"></script>

   <!-- END JAVASCRIPTS -->   

<!-- END BODY -->
</div></body></html>