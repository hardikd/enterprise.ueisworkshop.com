<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style>
.icon-reorder {display:none !important;}
.sidebar-toggle-box {background:#5e3364 !important;}
.popoverinfo {font-size:18px !important;}
.infoicon {  position: absolute;
  margin-left: 205px;
  margin-top: 10px;
width:60%;}
.card-expiry-month {width:35px;}
.card-expiry-year {width: 65px;margin-left: 35px;}
#errorjs {color: #FF0000;font-size: 24px;text-align: center;}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid sidebar-closed">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll" style="display:none;">
        <div id="sidebar" class="nav-collapse collapse">

         
       <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                     <i class="icon-book"></i>&nbsp; Payment
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <form class="form-horizontal form-login" id="payment-form" action="" name="createplanstep1" method="post">
                         <input type="hidden" name="plan" value="<?php echo $plan;?>" />
                         <input type="hidden" name="plan" value="<?php echo $duration;?>" />
                         
                     <div class="widget green">
                         <div class="widget-title">
                             <h4>Payment</h4>
                          
                         </div>
                         <div class="widget-body">
<!--                            <form class="form-horizontal" action="#">-->
                                <div id="pills" class="custom-wizard-pills-green4">
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                  
                                  
                                      
                                         <h3 style="color:#000000;">Make Payment</h3>
                                                   <div class="control-group" id="errorjs">
                                     </div>
                                      <div class="control-group" id="calculates_amount">
                                            <label class="control-label">Amount</label>
                                            <div class="controls" id="total_amount">
                                                $<?php echo $price;?>
                                            </div>
                                        </div>
                                    
                                     <div class="control-group">
                                            <label class="control-label">First Name</label>
                                            <div class="controls">
                                                <input type="text" class="form-control firstname" size="20" autocomplete="off" autofocus>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Last Name</label>
                                            <div class="controls">
                                                <input type="text" class="form-control lastname" size="20" autocomplete="off" autofocus>
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label class="control-label">Card Number</label>
                                            <div class="controls">
                                                <input type="text" class="form-control card-number" onkeypress='return validateQty(event);'  size="20" autocomplete="off" autofocus>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">CVC</label>
                                            <div class="controls">
                                                <input type="text" onkeypress='return validateQty(event);' class="form-control card-cvc" size="4" autocomplete="off" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Expiration</label>
                                            <div class="controls">
                                                <input type="text" onkeypress='return validateQty(event);' class="form-control card-expiry-month" size="2" placeholder="(MM)" autocomplete="off" >
                                            <input type="text" onkeypress='return validateQty(event);' size="4" class="form-control card-expiry-year" placeholder="(YYYY)" autocomplete="off"/>
                                            </div>
                                        </div>
                                         <div class="control-group">
                                            <label class="control-label">Select Staff</label>
                                            <div class="controls">
                                                <select name="quantity" id="quantity" data-placeholder="--Please Select--" class="chzn-select span12" style="width: 300px;">
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                  <option value="3">3</option>
                                                  <option value="4">4</option>
                                                  <option value="5">5</option>
                                                  <option value="6">6</option>
                                                  <option value="7">7</option>
                                                  <option value="8">8</option>
                                                  <option value="9">9</option>
                                                  <option value="10">10</option>

                                                </select>
                                            </div>
                                        </div>
                                       
                                             <button class="btn btn-large btn-green" type="submit" name="make_payment" value="make_payment"><i class="icon-save icon-white"></i> Make Payment</button>
                                             <a class="btn btn-large btn-green" href="<?php echo base_url();?>index/observerindex" name="continue" value="continue">Cancel</a>
                                   
                                     </div>
                                     </div>

                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
<!--      </div>
       END PAGE   
   </div>-->

   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
   <script type="text/javascript" src="<?php echo $SECURE_URL;?>assets/js/jquery.backstretch.min.js"></script>
   <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
  <script>
     // $.backstretch("<?php echo $BASE_URL;?>assets/img/login-bg.jpg", {speed: 500});
  </script>
  
  
  <script type="text/javascript">
// this identifies your website in the createToken call below
Stripe.setPublishableKey('pk_test_ClxgaZTZoMrfoL00sQUrIyac');
//Stripe.setPublishableKey('pk_live_uXKFj5AMxC324PxCjjtsdAUV');


                                      $('#quantity').on('change',function(){
                                  var total = $(this).val()*<?php echo $price;?>;
                                  $('#total_amount').html('$'+total);
                                });
                                   

  
function stripeResponseHandler(status, response) {
  if (response.error) {
  // re-enable the submit button
  $('.submit-button').show();
  // show the errors on the form
  $("#errorjs").html(response.error.message);
  } else {
  var form$ = $("#payment-form");
  // token contains id, last4, and card type
  var token = response['id'];
  //alert(token);
  // insert the token into the form so it gets submitted to the server
  form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
  // and submit
  form$.get(0).submit();
  }
}
 
$(document).ready(function() {
  
  
$("#payment-form").submit(function(event) {
// disable the submit button to prevent repeated clicks
$('.submit-button').hide();
 
// createToken returns immediately - the supplied callback submits the form if there are no errors
Stripe.createToken({
  firstname: $('.firstname').val(),
  lastname: $('.lastname').val(),
  number: $('.card-number').val(),
  cvc: $('.card-cvc').val(),
  exp_month: $('.card-expiry-month').val(),
  exp_year: $('.card-expiry-year').val()
  }, stripeResponseHandler);
  return false; // submit from callback
  });
});

function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;

if (event.keyCode == 8 || event.keyCode == 46
 || event.keyCode == 37 || event.keyCode == 39) {
    return true;
}
else if ( key < 48 || key > 57 ) {
    return false;
}
else return true;
};
</script>
  
   <!-- END JAVASCRIPTS --> 
 
</body>
<!-- END BODY -->
</html>