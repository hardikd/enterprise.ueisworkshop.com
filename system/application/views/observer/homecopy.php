<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Observer::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
$('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});

$('#nextdash').click(function(){

$('#previous').hide();
$('#nextdash').hide();
$('#next').show();
$('#previousdash').show();

});
$('#previousdash').click(function(){
$('#next').hide();
$('#previousdash').hide();
$('#previous').show();
$('#nextdash').show();

});


});



</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); 
	include($view_path.'inc/class/pData.class.php');
include($view_path.'inc/class/pDraw.class.php');
include($view_path.'inc/class/pImage.class.php');
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
        <?PHP
		$cach = date("H:i:s");
		$login_type=$this->session->userdata('login_type');
        $login_id=$this->session->userdata('observer_id');
		$rcc=1;		
		if(!empty($grades))
		{
		 //start observation
			
		$MyData = new pData();
		$namelimit=array();
		$checklist=array();
		$scaled=array();
		$proficiency=array();
		$likert=array();
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $checklist[$pj]=$grades[$pj]['checklist']; 
		  $scaled[$pj]=$grades[$pj]['scaled']; 
		  $proficiency[$pj]=$grades[$pj]['proficiency']; 
		  $likert[$pj]=$grades[$pj]['likert']; 		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$MyData->addPoints($checklist,'Checklist');
			$MyData->addPoints($scaled,'Scaled Rubric');
			$MyData->addPoints($proficiency,'Proficiency Rubric');
			$MyData->addPoints($likert,'Likert');
	$MyData->addPoints($namelimit,'Element');
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Observations",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($grades)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/observation/ob_".$login_type.'_'.$login_id.'_'.$rcc.".png");

          //start pdlc
			
			
		$MyData = new pData();
		$namelimit=array();
		$pdlc=array();
		
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $pdlc[$pj]=$grades[$pj]['pdlc']; 
		   
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$MyData->addPoints($pdlc,'Professional Development');
			
	$MyData->addPoints($namelimit,'Element');
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(250,55,"Professional Development Activity",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($grades)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/professional/ob_".$login_type.'_'.$login_id.'_'.$rcc.".png");

						
		
		//start lesson plan
			
		$MyData = new pData();
		$namelimit=array();
		$planeld=array();
		$plan=array();
		
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $planeld[$pj]=$grades[$pj]['planeld']; 
		  $plan[$pj]=$grades[$pj]['plan']; 
		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$MyData->addPoints($planeld,'ELD Subject');
			$MyData->addPoints($plan,'Other');
			
	$MyData->addPoints($namelimit,'Element');
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(150,55,"Lesson Planning",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($grades)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/lesson/ob_".$login_type.'_'.$login_id.'_'.$rcc.".png");

		//start lesson plan notification
			
		$MyData = new pData();
		$namelimit=array();
		$schooltohome=array();
		$behviour=array();
		$homework=array();
		
		for($pj=0;$pj<count($grades);$pj++)
		{
		
		
		
		  
		  $schooltohome[$pj]=$grades[$pj]['schooltohome']; 
		  $behviour[$pj]=$grades[$pj]['behviour']; 
		   $homework[$pj]=$grades[$pj]['homework']; 
		  
		  
//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $grades[$pj]['grade_name']=preg_replace($sPattern, $sReplace, $grades[$pj]['grade_name']);
		
		if(strlen($grades[$pj]['grade_name'])>18)
		{
		   
		   $limitname=substr($grades[$pj]['grade_name'],0,18);
		   $last=strripos($limitname,' ');
		   $limitname=substr($grades[$pj]['grade_name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$grades[$pj]['grade_name'];
		
		}
		
		$namelimit[]=$limitname;
		
			
			
					
	
	
	}
			$MyData->addPoints($schooltohome,'School-to-Home');
			$MyData->addPoints($behviour,'Behavior & Learning ');
			$MyData->addPoints($homework,'Homework');
			
	$MyData->addPoints($namelimit,'Element');
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 			$myPicture = new pImage(650,300,$MyData);  
		
		
 

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>150);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(200,55,"Lesson Plan Notifications",array("FontSize"=>15,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 /*if(count($grades)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else*/
 {
    $myPicture->setGraphArea(80,60,600,220);
 
 }
// $myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL_START0,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(480,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render("$view_path/inc/dashboard/notification/ob_".$login_type.'_'.$login_id.'_'.$rcc.".png");

		
		
		}
		?>
		<table>
		<tr>
		<td>
		Welcome <?php echo $this->session->userdata('observer_name');?>
		</td>
		</tr>
		<tr>
		<td>
		<input type="button" name="previousdash" id="previousdash" value="Previous Dashboard" style="display:none;">
		<input type="button" name="nextdash" value="Next Dashboard" id="nextdash">
		</td>
		</tr>
		</table>
		<table id="previous">
		<tr>
		<td>
		Lesson Planning	
		</td>		
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/lesson/ob_'.$login_type.'_'.$login_id.'_'.$rcc.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		<tr>
		<td>
		Lesson Plan Notifications	
		</td>		
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/notification/ob_'.$login_type.'_'.$login_id.'_'.$rcc.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		</table>
		<table id="next" style="display:none;" >
		<tr>
		<td>
		Professional Development Activity		
		</td>		
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/professional/ob_'.$login_type.'_'.$login_id.'_'.$rcc.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		<tr>
		<td>
		Observations		
		</td>		
		</tr>
		<tr>
		<td>
		<?php echo  '<img alt="Bar chart"  src="'.SITEURLM.$view_path.'inc/dashboard/observation/ob_'.$login_type.'_'.$login_id.'_'.$rcc.'.png?dummy='.$cach.'" style="border: 1px solid gray;"/>' ?>
		</td>
		</tr>
		</table>
		<table  cellpadding="4">
		
		
		<tr>
		<td>
		<b><font size="4px">Scheduled Activty Reports</font></b>
		</td>
		</tr>
		<!--start of Lesson Plan-->
		<tr>
		<td>
		<b>Lesson Plans (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteacherslesson;?> of <?php echo $totallesson;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernameslesson!=false){ $k=1;foreach($suteachernameslesson as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernameslesson!=''){ $k=1;foreach($teachernameslesson as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Lesson  Plan-->
		<!--start of Periodic Goal Plan-->
		<tr>
		<td>
		<b>Periodic Goal-Setting (Year: <?php echo date('Y');?>-<?php echo date('Y')+1;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteachers;?> of <?php echo $totalsubmitted;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernames!=false){ $k=1;foreach($suteachernames as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernames!=''){ $k=1;foreach($teachernames as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Periodic Goal Plan-->
		
		<!--start of Observation Plan-->
		<tr>
		<td>
		<b>Observation Plans (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Submitted:&nbsp;&nbsp;</b><?php echo $suteachersobservation;?> of <?php echo $totalobservation;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Submitted|<?php if($suteachernamesobservation!=false){ $k=1;foreach($suteachernamesobservation as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php if($teachernamesobservation!=''){ $k=1;foreach($teachernamesobservation as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>
		<tr>
		<td height="15px">
		</td>
		</tr>
		<!--End of Observation Plan-->
		<!--start of Observation Plan-->
		<tr>
		<td>
		<b><font size="4px">Upcoming Scheduled Activities</font></b>
		</td>
		</tr>
		<tr>
		<td>
		<b>Scheduled Observations (Week: <?php echo $fromdate;?> - <?php echo $todate;?>)</b>
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Scheduled:&nbsp;&nbsp;</b><?php echo $suteachersobservations;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="title" href="#" title="Scheduled|<?php if($suteachernamesobservations!=false){ $k=1;foreach($suteachernamesobservations as $name) { echo $k.'. '.$name['firstname'].' '.$name['lastname'].' ('.$name['created'].')<br />Start Time: '.$name['starttime'].' - End Time: '.$name['endtime']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>		
		<!--<tr>
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pending:&nbsp;&nbsp;</b><a class="title" href="#" title="Pending|<?php //if($teachernamesobservations!=''){ $k=1;foreach($teachernamesobservations as $name) { echo $k.'.'.$name['firstname'].' '.$name['lastname']; echo "<br />" ; $k++;} } else { echo "No Data Found. "; }  ?> ">View</a>
		</td>
		</tr>-->
		<!--End of Observations-->
		</table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
