﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District Id</td>
     <td>Incident Name</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page - 1)*10;
   $sno = 1;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="problem_<?php echo $data->id;?>">
   <td><?php echo $sno;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->incident_name;?></td>
   <td><?php echo $data->status;?></td>
      <td>
      <button class="edit_incident btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
             <input  type="hidden" name="prob_incident_id" id="prob_incident_id" value="<?php echo $data->id;?>" />
       <td>
       <button type="Submit" id="remove_incident" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_incident btn btn-danger"><i class="icon-trash"></i></button>
       </td>
  </tr>
  <?php  
  $con++;
  $sno++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_incident').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'problem_incident/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#incident_id').val(result[0].id);
       $('#incident_name').val(result[0].incident_name);
       $('#status').val(result[0].status);
       console.log(result[0].incident_name);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_incident").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'problem_incident/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#problem_"+id).css('display','none');
          alert('Successfully removed problem incident from list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

