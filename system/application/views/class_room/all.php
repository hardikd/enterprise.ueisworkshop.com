<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
         <?php if($this->session->userdata("login_type")!='user'){?>
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          <?php } else {?>
           <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
          <?php }?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                  <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-Up</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>class_room">Homeroom/Classroom</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                 <div class="widget blue" style="min-height:250px;">
                         <div class="widget-title">
                             <h4>Homeroom/Classroom</h4>
                          
                         </div>
                         <div class="widget-body">
                         
                                               
                        
                         <a href="#myModal-add-new-parent" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-plus"></i > Add New</a>
                         <div id="myModal-add-new-parent" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Add Classroom</h3>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" action="#" id="addnewclassroom">
                                   <div class="control-group">
                                     
                                             <label class="control-label-required">Class Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" name="name" />
                                                 
                                             </div>
                                             </div> 
                                             
                                             <div class="control-group">
                                     
                                             <label class="control-label-modal">School Name</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 205px;" name="school_id" >
                                                     <option></option>
                                                     <?php foreach($schools as $school){?>
                                                        <option value="<?php echo $school['school_id'];?>"><?php echo $school['school_name'];?></option>
                                                     <?php }?>
                                                 </select>
                                                 
                                             </div>
                                             </div> 
                                             
                                             
                                                      <div class="control-group">
                                          <label class="control-label-modal">Select Grade</label>
                                    <div class="controls">
                                        
                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 205px;" name="grade_id">
                                        <option value=""></option>
                                        <?php foreach($grades as $grade){?>
                                        <option value="<?php echo $grade['grade_id'];?>"><?php echo $grade['grade_name'];?></option>
                                        <?php }?>
                                            
                                    </select>
                                    </div>
                                </div>   
                                             
                                             
                                             
                                     </form>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="addclassroom();"><i class="icon-plus"></i> Add Clasroom</button>
                                </div>
                            </div>
                            
                                          <a href="#myModal-upload" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-upload"></i > Upload Classrooms</a>
                         <div id="myModal-upload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Upload Classrooms</h3>
                                </div>
                                <div class="modal-body">
                                     <form class="form-horizontal" action="#">
                                     <div class="control-group">
                                    <label class="control-label">Upload your file</label>
                                    <div class="controls">
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="icon-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                               <span class="btn btn-file">
                                               <span class="fileupload-new">Select file</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default">
                                               </span>
                                                <a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remove</a>
                                            </div>
                                        </div>
                                        </div></div>
                                    
                                                                     </form>
                                </div>
                               
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success"><i class="icon-upload"></i> Upload Classrooms</button>
                                </div>
                            </div>
                                     
                                     
                                     
                                 <!-- BEGIN TABLE widget-->
                                     
                                     
                         <div class="space20"></div>
                           
                         
                          <table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th class="no-sorting">Class Name</th>
                                <th class="no-sorting">School Name</th>
                                <th class="no-sorting">Grade</th>
                                <th class="no-sorting">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php 
//                                print_r($class_rooms);exit;
                                if($class_rooms)
                                foreach ($class_rooms as $class_room){?>
                            <tr class="odd gradeX">                                
                                <td><?php echo $class_room['name'];?></td>
                                <td class="hidden-phone"><?php echo $class_room['school_name'];?></td>
                                <td class="center hidden-phone"><?php echo $class_room['grade_name'];?></td>
                                <td class="hidden-phone">
                                
                                    <a href="#myModal-edit-room1" role="button" class="btn btn-primary" data-toggle="modal" onclick="editclassroom(<?php echo $class_room['class_room_id'];?>);"><i class="icon-pencil"></i></a>
                         
                         
                            
                            
                            
                            
                         <a href="#myModal-delete-student<?php echo $class_room['class_room_id'];?>" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                         <div id="myModal-delete-student<?php echo $class_room['class_room_id'];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
                                </div>
                                <div class="modal-body">
                                     Please select "Yes" to remove this classroom.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success" onclick="delclassroom(<?php echo $class_room['class_room_id'];?>);"><i class="icon-check"></i> Yes</button>
                                </div>
                            </div>
                            
                            
                            
                            </tr>
                                <?php }?>
                           
                            </tbody>
                        </table>
                         
                         <div id="myModal-edit-room1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel3">Edit Homeroom/Classroom</h3>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" action="#" id="editform">
                                        <input type="hidden" name="class_room_id" id="class_room_id" value=""/>
                                     <div class="control-group">
                                     
                                             <label class="control-label-required">Class Name</label>
                                             <div class="controls">
                                                 <input type="text" class="span8" value="" name="name" id="name"/>
                                                 
                                             </div>
                                             </div> 
                                             
                                             <div class="control-group">
                                     
                                             <label class="control-label-modal">School Name</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select"  tabindex="1" style="width: 205px;" name="school_id" id="school_id">
                                                    <option value=""></option>
                                                    <?php foreach($schools as $school){?>
                                                        <option value="<?php echo $school['school_id'];?>"><?php echo $school['school_name'];?></option>
                                                     <?php }?>
                                                 </select>
                                             </div>
                                             </div> 
                                             
                                             
                                                      <div class="control-group">
                                          <label class="control-label-modal">Select Grade</label>
                                    <div class="controls">
                                        
                                        <select class="span12 chzn-select"  tabindex="1" style="width: 205px;" name="grade_id" id="grade_id">
                                        <option value=""></option>
                                         <?php foreach($grades as $grade){?>
                                        <option value="<?php echo $grade['grade_id'];?>"><?php echo $grade['grade_name'];?></option>
                                        <?php }?>
                                            
                                    </select>
                                    </div>
                                </div>   
                                     </form>
                                     
                                     <div class="space20"></div><div class="space20"></div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button  class="btn btn-success" onclick="saveclassroom();"><i class="icon-save"></i> Save Changes</button>
                                </div>
                            </div>
                        </div></div>     
                            
                          <!-- END ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                <!-- END EXAMPLE TABLE widget-->
                         
                         
                         
                         </div>
                        
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
<!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success" onclick="location.reload(true);">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 
<?php if($this->session->userdata('login_type')=='observer'){?>
    <script src="<?php echo SITEURLM.$view_path; ?>js/class_room.js" type="text/javascript"></script>
<?php } else {  ?>
    <script src="<?php echo SITEURLM.$view_path; ?>js/class_roomall.js" type="text/javascript"></script>
<?php } ?>
   <!-- END JAVASCRIPTS --> 
   
    <script>
        var base_url = '<?php echo base_url();?>';
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });

      function editclassroom(class_room_id){
           $.ajax({
                type: 'post',
            url: base_url+'class_room/getclass_roominfo/'+class_room_id
            })
          .done(function( data ) {
             var result = jQuery.parseJSON(data);
                $('#name').val(result.class_room.name);
                $('#class_room_id').val(result.class_room.class_room_id);
                $('#school_id').val(result.class_room.school_id).trigger("liszt:updated");
                $('#grade_id').val(result.class_room.grade_id).trigger("liszt:updated");
                
            });
      }
      
      function saveclassroom (){
          $.ajax({
                type: 'post',
            url: base_url+'class_room/update_class_room',
            data: $("#editform").serialize()
            })
          .done(function( data ) {
             data = jQuery.parseJSON(data);
                if (data.status==0){
                    $('#errorbtn').modal('show');
                } else if (data.status==1){
                    $('#myModal-add-new-parent').modal('hide');
                    $('#successbtn').modal('show');
                }
            });
      }

      function delclassroom(class_room_id){
         $.ajax({
                type: 'post',
            url: base_url+'class_room/delete/'+class_room_id
            })
          .done(function( data ) {
             data = jQuery.parseJSON(data);
                if (data.status==0){
                    $('#errorbtn').modal('show');
                } else if (data.status==1){
                    $('#myModal-add-new-parent').modal('hide');
                    $('#successbtn').modal('show');
                }
            }); 
      }
      function addclassroom() {
          $.ajax({
                type: 'post',
            url: base_url+'class_room/add_class_room',
            data: $("#addnewclassroom").serialize()
            })
          .done(function( data ) {
             data = jQuery.parseJSON(data);
                if (data.status==0){
                    $('#errorbtn').modal('show');
                } else if (data.status==1){
                    $('#myModal-add-new-parent').modal('hide');
                    $('#successbtn').modal('show');
                }
            });
      }
   </script>  
</body>
<!-- END BODY -->
</html>