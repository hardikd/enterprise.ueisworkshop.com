<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop:: Copy Assessment from one District to another ::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
 <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
  <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
<!-- <script src="<?php echo SITEURLM.$view_path; ?>js/teacher_admin.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/teacher_dist.js" type="text/javascript"></script>-->


  <style type="text/css">
.ac_results {
	padding: 0px;
	border: 1px solid black;
	background-color:#FFF;
	overflow: hidden;
	z-index: 99999;
}

.ac_results ul {
	width: 100%;
	list-style-position: outside;
	list-style: none;
	padding: 0;
	margin: 0;
}

.ac_results li {
	margin: 0px;
	padding: 2px 5px;
	cursor: default;
	display: block;
	/* 
	if width will be 100% horizontal scrollbar will apear 
	when scroll mode will be used
	*/
	/*width: 100%;*/
	font: menu;
	font-size: 12px;
	/* 
	it is very important, if line-height not setted or setted 
	in relative units scroll will be broken in firefox
	*/
	line-height: 16px;
	overflow: hidden;
}

.ac_loading {
	background: white url('../indicator.gif') right center no-repeat;
}

.ac_odd {
	background-color: #eee;
}

.ac_over {
	background-color:#999;
	color: white;
}

</style>
 
</head>

<body>
<div class="wrapper">
  <?php require_once($view_path.'inc/headerv1.php'); ?>
  <div class="mbody">
    <?php require_once($view_path.'inc/pleftmenu.php'); ?>
    <div class="content">
      <div class="search">
    <!--  <span style="text-decoration:underline; font-size:16px;">Search Students</span>-->
    <fieldset>
    <legend>  Select Student to transfer </legend>
        
      <form name="frmcopyassesment" action="transfer_student" method="post" >  
   <table align="center">
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { ?>
		<?php foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		Districts:
		</td>
		<td>
		<select class="combobox" name="district" id="district" onchange="school_type(this.value)">
		
		<?php if($district!=false) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } else { ?>
		<option value="">No Districts Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		School:
		</td>
		<td>
                    <select class="combobox" name="school" id="school" >
		<option value="">Select School</option>
		<?php if(!empty($school)) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>" ><?php echo $val['school_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		
		</td>
		</tr>
        
        
        
        <tr>
		<td>
		Student:
		</td>
		<td>
		 <input type="text" class="span6 " name="studentlist" value="" id="studentlist" style="width:210px;"  />
            <input type="hidden" name="student" id="student"  />
            <input type="hidden" name="lastname" id="lastname"  />
            <input type="hidden" name="firstname" id="firstname"  />
            <input type="hidden" name="UserID" id="UserID"  />
            <input type="hidden" name="student_id" id="student_id"  />
            
            
		</td>
		<td>
		Grade:
		</td>
		<td>
            <input id="grade_id" type="text" name="" value="" size="16" class="m-ctrl-medium" style="width: 210px;"  readonly="readonly"/> 
			<input id="grade" type="hidden" name="grade_id" value="" size="16" class="m-ctrl-medium" style="width: 210px;"  />
		</td>
		<td>
		
		</td>
		</tr>
       
		</table>
   </form>
        
        
   </fieldset>
   </div>
        <div class="search">
    <!--  <span style="text-decoration:underline; font-size:16px;">Search Students</span>-->
    <fieldset>
    <legend>  Select school for transfer </legend>
        
      <form name="tocopyassesment" action="transfer_student" method="post" >  
   <table align="center">
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="tocountries" id="tocountries" onchange="tostates_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="tostates" id="tostates" onchange="todistrict_all(this.value)" >
		<?php if($states!=false) { ?>
		<?php foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		Districts:
		</td>
		<td>
		<select class="combobox" name="todistrict" id="todistrict" onchange="toschool_type(this.value)">
		
		<?php if($district!=false) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } else { ?>
		<option value="">No Districts Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		School:
		</td>
		<td>
                    <select class="combobox" name="toschool" id="toschool" >
		<option value="">Select School</option>
		<?php if(!empty($school)) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>" <?php if(isset($school_id) && $school_id==$val['school_id'] ) {?> selected <?php  } ?> ><?php echo $val['school_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		
		</td>
		</tr>
       <tr>
		<td>
		Grade:
		</td>
		<td>
		<select class="combobox" name="tograde" id="tograde">
		<option value="">Select Grade</option>
		</select>
		</td>
           <td colspan="2">
           </td>

		
		</table>
   </form>
        
        
   </fieldset>
   </div>
      
      
    <input type="button" class="btnsmall" title="Transfer" name="transferstudent" id="transferstudent" value="Transfer" />
    </div>
    </div>
  </div>
  <?php require_once($view_path.'inc/footer.php'); ?>
</div>


<!-- Single Student graph  --> 

<!-- LIGHT BOX ENDS HERE-->
<script>
    
    function states_select(id)
{
	var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
//	var str='<select class="combobox" name="states" id="states" onchange="district_all(this.value)">';
        var str='';
	if(result.states!=false)
	{
//        str+='<option value="0">Select State</option>';
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	if(index==0)
	{
		district_all(value['state_id']);
		
	}
	});
//	str+='</select>';
	
	
    }
	else
	{
     var str='';
	 str+='<option value="0">No States Found</option>';
//     var sstr='';
//	 sstr+='<option value="empty">No Districts Found</option>';
//	  $('#district').html(sstr); 
//	   var sstr='';
//	 sstr+='<option value="empty">No Schools Found</option>';
//	  $('#school').html(sstr); 
	}
     $('#states').html(str); 
     sstr='<option value="empty">Select District</option>';
      $('#district').html(sstr);
      sstr='<option value="empty">Select School</option>';
	  $('#school').html(sstr); 
	 
	 
	 
	 
	 });

}

function district_all(id)
{

	var s_url='district/getDistrictsByStateId/'+id;
    $.getJSON(s_url,function(sresult)
	{
//		var sstr='<select class="combobox" name="district" id="district" onchange="school_type(this.value)">';
                var sstr= '';
                var sstr1 = '';
		if(sresult.district!=false)
	{
	//sstr+='<option value="all">All</option>';
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	if(index==0)
	{
		school_type(value['district_id']);
		
	}
	});
//	sstr+='</select>';
	
	}
	else
	{
     sstr+='<option value="empty">No Districts Found</option></select>';
//	  var sstr1='<select class="combobox" name="school" id="school">';
	 sstr1+='<option value="empty">No Schools Found</option>';
	  $('#school').html(sstr1); 
	  
	}
//        alert(sstr);
     $('#district').html(sstr); 
	});


}

function school_type(id)
{

	var s_url='school/getschoolbydistrict/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<option>Select School</option>';
		if(sresult.school!=false)
	{
	$.each(sresult.school, function(index, value) {
	sstr+='<option value="'+value['school_id']+'">'+value['school_name']+'</option>';
	
	});
//	sstr+='</select>';
	
	}
	else
	{
          sstr+='<option value="">No School found</option>';
//     sstr+='</select>';
	}
     $('#school').html(sstr); 
	});



}



/////////////////////////////////////////////////////////////////////////////////

function tostates_select(id)
{
//    alert('this');
	var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
//	var str='<select class="combobox" name="states" id="states" onchange="district_all(this.value)">';
        var str='';
	if(result.states!=false)
	{
//        str+='<option value="0">Select State</option>';
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	if(index==0)
	{
		todistrict_all(value['state_id']);
		
	}
	});
//	str+='</select>';
	
	
    }
	else
	{
     var str='';
	 str+='<option value="0">No States Found</option>';
//     var sstr='';
//	 sstr+='<option value="empty">No Districts Found</option>';
//	  $('#district').html(sstr); 
//	   var sstr='';
//	 sstr+='<option value="empty">No Schools Found</option>';
//	  $('#school').html(sstr); 
	}
     $('#tostates').html(str); 
     sstr='<option value="empty">Select District</option>';
      $('#todistrict').html(sstr);
      sstr='<option value="empty">Select School</option>';
	  $('#toschool').html(sstr); 
	 
	 
	 
	 
	 });

}

function todistrict_all(id)
{
//alert('this dist');
	var s_url='district/getDistrictsByStateId/'+id;
    $.getJSON(s_url,function(sresult)
	{
//		var sstr='<select class="combobox" name="district" id="district" onchange="school_type(this.value)">';
                var sstr= '';
                var sstr1 = '';
		if(sresult.district!=false)
	{
	//sstr+='<option value="all">All</option>';
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	if(index==0)
	{
		toschool_type(value['district_id']);
		
	}
	});
//	sstr+='</select>';
	
	}
	else
	{
     sstr+='<option value="empty">No Districts Found</option></select>';
//	  var sstr1='<select class="combobox" name="school" id="school">';
	 sstr1+='<option value="empty">No Schools Found</option>';
	  $('#toschool').html(sstr1); 
	  
	}
//        alert(sstr);
     $('#todistrict').html(sstr); 
	});


}

function toschool_type(id)
{
//alert('this');
	var s_url='school/getschoolbydistrict/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<option>Select School</option>';
		if(sresult.school!=false)
	{
	$.each(sresult.school, function(index, value) {
	sstr+='<option value="'+value['school_id']+'">'+value['school_name']+'</option>';
	
	});
//	sstr+='</select>';
	
	}
	else
	{
          sstr+='<option value="">No School found</option>';
//     sstr+='</select>';
	}
        console.log(sstr);
     $('#toschool').html(sstr); 
	});



}

$('#toschool').change(function(){
    $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>transfer_student/getSchoolGrades",
            data: { district_id: $('#todistrict').val() },
            success:function(result){
                var obj = jQuery.parseJSON(result);
                sstr = '<option>Select Grade</option>';
//                console.log(obj);
                $.each(obj, function(index, value) {
                    console.log(value);
                    sstr+='<option value="'+value.dist_grade_id+'">'+value.grade_name+'</option>';

                    });
              $('#tograde').html(sstr);
              }
          });
});
//////////////////////////////////////////////////////////////////////////////////
    
    /*$('#school').change(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>transfer_student/getStudentsBySchool",
            data: { school_id: $('#school').val() },
            success:function(result){
                var obj = jQuery.parseJSON(result);
                sstr = '<option>Select Student</option>';
//                console.log(obj);
                $.each(obj, function(index, value) {
                    console.log(value);
                    sstr+='<option value="'+value.UserID+'">'+value.firstname+' '+value.lastname+'</option>';

                    });
              $('#student').html(sstr);
              }
          });
            
    });*/
    $('#transferstudent').click(function(){
        if($('#UserID').val()==''){
            alert('Please select student you wanted to transfer');
            return false;
        }
        if($('#UserID').val()==''){
            alert('Please select school in which you wanted to transfer student');
            return false;
        }
       
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>transfer_student/transfer",
            data: { userID:$('#UserID').val(),todistrict:$('#todistrict').val(),toschool:$('#toschool').val(),grade:$('#tograde').val(),gradeName:$('#tograde option:selected').text()  },
            success:function(result){
                alert(result);
              }
          });
    });


 /*$('#school').change(function(){
	 alert('test');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>transfer_student/getStudentsBySchool",
            data: { school_id: $('#school').val() },
			
			success:function(result){
				
				
				    $("#studentlist").result(function(event, data, formatted) {
			        $("#student").val(data[1]);
					});
				}
          });
            
    });
	*/
	var newurl= '';
	$('#school').change(function(){
		newurl = "<?php echo base_url();?>transfer_student/getStudentsBySchool/"+$(this).val();

		});
	
	 $("#studentlist").autocomplete("<?php echo base_url();?>transfer_student/getStudentsBySchool/", {
        width: 260,
        matchContains: true,
        selectFirst: false,
		max:15,
    });
	 $("#studentlist").result(function(event, data, formatted) {
        $("#student").val(data[1]);
		$("#lastname").val(data[2]);
		$("#firstname").val(data[3]);
		$("#UserID").val(data[4]);
		$("#student_id").val(data[5]);
		
		
    });
	
	$('#studentlist').setOptions({
        extraParams: {
          school_id: function(){
            return $('#school').val();
          }
  }
})

	



$('#studentlist').change(function(){

	$('#grade_id').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>implementation/get_student_number_by_grade",
	  data: {student_number: $('#student').val(),lastname: $('#lastname').val(),firstname: $('#firstname').val() },
	  
	  success: function( msg ) {
		  $('#grade_id').html('');
		  var result = jQuery.parseJSON( msg )
		  
		  $(result.grade).each(function(index, Element){ 
		 
		 console.log(Element.grade_name);
		  $('#grade_id').val(Element.grade_name);
		   $('#grade').val(Element.dist_grade_id);
		});  
		  $("#grade_id").trigger("liszt:updated");
		  
	  }
	  })
  	
});



</script>
</body>
</html>
