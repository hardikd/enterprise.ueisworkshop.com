<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
     <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<!--start old script -->
<style type="text/css">
.ui-jqgrid tr.jqgrow td {
    white-space: normal !important;
    height:auto;
    vertical-align:text-top;
    padding-top:2px;
}
.content{
    margin-left: 10px !important;
}


</style>
<!--end old script -- >
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
     <?php if($this->session->userdata("login_type")!='user'){?>
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          <?php } else {?>
           <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
          <?php }?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-Up</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>time_table">Timetable Assignments</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                 <div class="widget blue" style="min-height:250px;">
                         <div class="widget-title">
                             <h4>Timetable</h4>
                      </div>
                  <div class="content">
	<div style="margin:10px;">	
	<table id="list1"></table>
		</div>
			
	 </div>
            <div class="row-fluid">
                <div class="span12">
                <!-- END EXAMPLE TABLE widget-->
                         
                         
                        </div> 
                         </div>
                        
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 <!--start old script -->
	
	<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo SITEURLM?>js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="<?php echo SITEURLM?>js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	
	
<script type="text/javascript">
var lastsel2;	//save row ID of the last row selected 
$(document).ready(function (){
var countries = '<?php echo $subjects;?>';
var tea=":";

$("#list1").jqGrid({
   	url:'<?php echo base_url().'time_table/gettimetable'?>',	//Backend controller function generating grid XML
	editurl: '<?php echo base_url().'time_table/edit_test_save'?>',	//Backend controller function save inline updates
	mtype : "post",		//Ajax request type. It also could be GET
	datatype: "xml",	//supported formats XML, JSON or Arrray
   	colNames:['','Actions',<?php echo $times?>],	//Grid column headings
   	colModel:[
   		{name:'date',index:'date', width:75, align:"left", editable:false, editrules: { edithidden: true }},{name:'act',index:'act', width:110, align:"center", editable:false, search:false},
		 <?php foreach($periods as $key=>$value)
		  {	
		  ?>
		{name:'value_<?php echo $key;?>',index:'time_<?php echo $key;?>', width:90, align:"left", editable:true, edittype:'select',editoptions: {
                 value: countries,
				dataEvents: [
                    { type: 'change', fn: function (e) { 
					$.get('time_table/get_teachers/'+this.value
                            ,function(data){ 
                                var res = $(data).html(); 
								var rowid = $("#list1").jqGrid('getGridParam','selrow');
									
							   $("#"+rowid+"_value_<?php echo $key;?>_<?php echo $key;?>").html(res); });
					} }
                ]
            } },{name:'value_<?php echo $key;?>_<?php echo $key;?>',index:'time_<?php echo $key;?>_<?php echo $key;?>', width:75, align:"left", editable:true, edittype:'select',editoptions: {
                 value: tea				
            } }, <?php } ?><?php echo $cols?>   		
   		
  	],
   	rowNum:100,
   	shrinkToFit:false,   	
   	pager: '#pager1',
	width:1050,
        hidegrid: false,
	height:'auto',
	toolbar: [false,"top"],
    viewrecords: true,
	gridComplete: function(){
		var ids = $("#list1").getDataIDs();
		for(var i=0;i < ids.length;i++){
			var cl = ids[i];
			se = "<input type='button' value='Save' onclick=\"$('#list1').saveRow('"+cl+"',checksave);\"  />";		//checksave is a callback function to show the response of inline save controller function
			ce = "<input type='button' value='Cancel' title='Cancel' onclick=\"$('#list1').restoreRow('"+cl+"');\" />";
			$("#list1").setRowData(ids[i],{act:se+ce});	//Save and Cancel buttons inserted via jqGrid setRowData function
		}
		if($("#list1").parents('div.ui-jqgrid-bdiv').height()>650)
	{
		$("#list1").parents('div.ui-jqgrid-bdiv').css("max-height","650px");
	}
	},
	onSelectRow: function(id){
		if(id && id!==lastsel2){
			$('#list1').restoreRow(lastsel2);	//restore last grid row
			
			$.post('time_table/get_allteachers/'+id
                            ,function(data){ 
                                //for(var x in data.res)
								//var res = $(teach).html(); 
								$.each(data.res, function(i, item) {
									
									 $('#'+id+i).html(item);
});
							  // $("#"+rowid+"_value_<?php echo $key;?>_<?php echo $key;?>").html(res);
							  },'json');
							   
			$('#list1').editRow(id,true);		//show form elements for the row selected to enable updates
			lastsel2=id;	//save current row ID so that when focus is gone it can be restored
		}
	},
    caption:"&nbsp;&nbsp;&nbsp;"
}).navGrid('#pager1',{edit:false,add:false,del:false,search:false});


$("#list1").jqGrid('setGroupHeaders', { 
useColSpanStyle: false, 
groupHeaders:[ <?php echo $headers;?> ] });

//$("#list1").jqGrid("setColProp","date",{frozen:true});
//$("#list1").jqGrid("setColProp","act",{frozen:true});
//$("#list1").jqGrid("setFrozenColumns");
		

});
 function loading_show()
{
$('#loading').fadeIn('fast');
}

function loading_hide()
{
$('#loading').fadeOut();
} 
function checksave(result)
{
	if(result.responseText!='') alert(result.responseText);
	else $("#list1").trigger("reloadGrid");
}

</script>
 <!--end old script -->

   <!-- END JAVASCRIPTS --> 
   
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
      <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
</body>
<!-- END BODY -->
</html>