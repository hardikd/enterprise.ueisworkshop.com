<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
     <link rel="stylesheet" href="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.css" />
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
                   <!-- BEGIN SIDEBAR MENU -->
          <?php if($this->session->userdata("login_type")!='user'){?>
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
          <?php } else {?>
           <?php require_once($view_path.'inc/developmentmenu_new.php'); ?>
          <?php }?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-calendar"></i>&nbsp; Attendance Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>attendance/assessment">Attendance Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-Up</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                          </i> <a href="<?php echo base_url();?>time_table">Timetable Assignments</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                 <div class="widget blue" style="min-height:250px;">
                         <div class="widget-title">
                             <h4>Timetable Assignments</h4>
                          
                         </div>
                         <div class="widget-body">
                         
                                               
                        
                       <div class="space20"></div>
                               <form class="form-horizontal" action='time_table/createvalue' name="report" method="post" onsubmit="return check()">
                                          <div class="control-group">
                                             <label class="control-label">Select School</label>
                                             <div class="controls">
								<input  class="txtbox" type="hidden" readonly name="report_date" id="report_date" value="<?php echo date('m/d/Y');?>">
                                <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;"name="school_id" id="school_id" onchange="changegrade()"  >
                                      <option value=''></option>
                                      <?php if(!empty($schools)) { 
                                      
                                      foreach($schools as $schoolvalue)
                                      {?>
                                      <option value="<?php echo $schoolvalue['school_id'];?>"><?php echo $schoolvalue['school_name'];?></option>
                                      <?php			  
                                      }
                                      ?>
                                      
                                      <?php } ?>
                                      </select>
										</div>
                                         </div>
                                         
                                        <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
				<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="grade_id" id="grade_id" onchange="changeclass_room(this.value)" >
                                          <option value=''></option>
                                          <?php if(!empty($grades)) { 
                                          foreach($grades as $gradevalue)
                                          {?>
                                          <option value="<?php echo $gradevalue['grade_id'];?>"><?php echo $gradevalue['grade_name'];?></option>
                                          <?php			  
                                          }
                                          ?>
                                          
                                          <?php } ?>
                                          </select> 
                                               </div>
                                                                     </div> 
                                         
                                           <div class="control-group">
                                             <label class="control-label">Select Class Room</label>
                                             <div class="controls">

                        <select data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" name="class_room_id" id="class_room_id" class="span12 chzn-select" >
                        <option value=''></option>
			 			</select>
								   </div>
                                         </div>
                                      
                                      
                                      
                                       
                                        
                                       <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
	<input title="Get Report" type="submit" class="btn btn-small btn-blue" name="submit" value="Submit"  style=" padding: 5px; " />    			</div> 
                   </div>
                               </form>
                       
                         <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget blue">
                         <div class="widget-title">
                             <h4>Timetable</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                                                    
                                     

                        <div class="space20"></div>
                      
                      
                     
                         <div id="myModal-save" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                   
                                    <h3 id="myModalLabel4">Save</h3>
                                </div>
                                <div class="modal-body">
                                     Are you sure you would like to save this timetable?
                                </div>
                                <div class="modal-footer">
                                    
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button> <button data-dismiss="modal" class="btn btn-success"><i class="icon-save"></i> Save</button>
                                </div>
                            </div>     
                            
                                 <div id="myModal-delete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                   
                                    <h3 id="myModalLabel4">Delete</h3>
                                </div>
                                <div class="modal-body">
                                     Are you sure you would like to delete this timetable?
                                </div>
                                <div class="modal-footer">
                                    
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Nol</button> <button data-dismiss="modal" class="btn btn-success"><i class="icon-check"></i> Yes</button>
                                </div>
                            </div>                                     
                                                                       
                                          
                        
                        
                </div></div>            
                          <!-- END ADVANCED TABLE widget-->
                          
                         </div></div>   
            <div class="row-fluid">
                <div class="span12">
                <!-- END EXAMPLE TABLE widget-->
                         
                         </div>
                         
                         </div>
                        
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 <!--start old script -->
 
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->

<script type="text/javascript">
function check()
{

if(document.getElementById('school_id').value=='')
{
 alert('Please Select School');
 return false;

}
else if(document.getElementById('grade_id').value=='')
{
 alert('Please Select Grade');
 return false;

}
else if(document.getElementById('class_room_id').value=='')
{
 alert('Please Select Class Room');
 return false;

}
else
{
	return true;

}
}

function changegrade()
{

//$("select[name='grade_id'] option[value='']").attr("selected", true);
//var str='<select name="class_room_id" class="combobox" id="class_room_id"  ><option value="">-Please Select-</option></select>';
// $('#class_room_id').replaceWith(str); 

}
function changeclass_room(grade_id)
{

if(document.getElementById('school_id').value=='')
{
alert('Please Select School');
return false;

}
else
{

school_id=document.getElementById('school_id').value;
}
var s_url='class_room/getclassroom/'+school_id+'/'+grade_id;
		$.getJSON(s_url,function(sresult)
	{
//	var str='<select name="class_room_id" class="combobox" id="class_room_id" ><option value="">-Please Select-</option>';
        var str = '';
	if(sresult.class_room!=false)
	{
	$.each(sresult.class_room, function(index, value) {
	str+='<option value="'+value['class_room_id']+'">'+value['name']+'</option>';
	
	});
//	str+='</select>';
    }
	else
	{
//     str+='</select>';
	}
//        alert(str);
     $('#class_room_id').html(str); 
     $('#class_room_id').trigger("liszt:updated");
	 
	});
$('#class_room_id').trigger("liszt:updated");

}
</script>
 <!--end old script -->

   <!-- END JAVASCRIPTS --> 
   
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
      <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
</body>
<!-- END BODY -->
</html>