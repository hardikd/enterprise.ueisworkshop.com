<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Teachers::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
 

function showSchools(stid)
{
	var sitename = '<?php echo $_SERVER['HTTP_HOST'];?>';
	
	if(sitename =="www.nanowebtech.com")
	{
		var path = 'http://www.nanowebtech.com/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename =="enterprise.ueisworkshop.com")
	{
		var path = 'https://enterprise.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename =="district.ueisworkshop.com")
	{
		var path = 'http://district.ueisworkshop.com/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	else if(sitename ="localhost")
	{
	var path = 'http://localhost/testbank/workshop/ajax_files/fetchSchoolById.php?stid='+stid;
	}
	
	
	$.ajax({
	
	type:'post',
	url:path,
	success:function(result)
	{
		$('#school_name_id').html(result);
	}
	
	});	
}

function checkfields()
{
//var schooltype = document.single_test_report.school_type.value;
var classname = document.single_test_report.grade.value;

 if(classname=="")
{
alert('Select Garde');
}
else
{
document.single_test_report.submit();
}



}
</script>


</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <table align="center">
		<tr>
		<td >
		<input type="hidden" name="countries" id="countries" value="<?php echo $this->session->userdata('dis_country_id') ?>" >
		<input type="hidden" name="states" id="states" value="<?php echo $this->session->userdata('dis_state_id') ?>" >
		<input type="hidden" name="district" id="district" value="<?php echo $this->session->userdata('district_id') ?>" >
		</td>
		
		</tr>
		
		
		</table>
		<table>
		<tr>
		<td colspan="2" align="right">
		<h3> District: Comparison of All Schools by Grade </h3>
		</td>
		</tr>
		</table>
		<div id="teacherdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
	<!--	<input class="btnbig" type="button" name="teacher_add" id="teacher_add" title="Add New" value="Add New" > -->
		</div>
		

		<form  action='allschoolbygrade/showallschools' name="single_test_report" method="post" >
			<table cellpadding="5">			
        
            <tr>
			<td>
			 Grades:
			  </td>
			  <td>
			  <select class="combobox" name="grade" id="grade" >
			  <option value="">-Please Select-</option>
                <?php
			 foreach($grades as $grade)
			 {
				 
				 $gradeid = $grade['dist_grade_id'];
				 $gradename = $grade['grade_name'];
				 ?>
                 <option value="<?php echo $gradeid;?>"><?php echo $gradename;?></option>
                 <?php
			 }
			  ?>
              			  
			  </select>
			  </td>
			  
			</tr>
   
			<tr>			
			<td>
			</td>
<td ><input title="Get Report" class="btnbig" type="button" name="submitform" value="School Report" onClick="checkfields();"></td>
			</tr>
	</table>
   </form>
		
<!-- Graph -->

<?Php

	$district_id=$this->session->userdata('district_id');
	//$sql="SELECT * FROM `assignments`  where `district_id`='".$district_id."'  order by id DESC limit 3";
	$sql = "SELECT id,`assignment_name` FROM assignments WHERE EXISTS (SELECT * FROM user_quizzes
           WHERE user_quizzes.assignment_id =assignments.id)  and assignments.district_id='".$district_id."' order by id desc limit 3";
	
	$aassm = mysql_query($sql);
	
	
	$assignmentname = array();
	$assignmentids = array();
	while($getres = mysql_fetch_assoc($aassm))
	{
	$assignmentname[] = $getres['assignment_name'];
	$assignmentids[] = $getres['id'];
	}
	
	if(!empty($assignmentname))
	{
		if(!empty($assignmentname[0]) && $assignmentname[0]!="")
		{
			$first = $assignmentname[0];
		}
		else
		{
			$first = '';
		}
		if(!empty($assignmentname[1]) && $assignmentname[1]!="")
		{
		$second = $assignmentname[1];
		}
		else
		{
			$second = '';
		}
		if(!empty($assignmentname[2]) && $assignmentname[0]!="")
		{
		$third = $assignmentname[2];
		}
		else
		{
			$third = '';
		}
		
		
	}





if(!empty($allschools))
{	
	
	
	$valarry =  array() ;		  // if empty  
	$schoolnamearr =  array();    // school name array
	$avgarr = array();            // for pie graph 
	
	?>
	<table>
		<tr>
			<td colspan="2" align="right">
				<h3> 
				<?php
				   		echo 'Comparison of All Schools by Grade -> &nbsp';
						echo $cid_resut_1 = $this->classroommodel->fetchGradeName(@$gradeid1); // Grade Name
						echo '&nbsp ->  &nbsp According To Current District School Type ';
				?>
				</h3>
			</td>
		</tr>
</table>
	<?php
	// first school
		$bb = array();
		$b = array();
		$pf = array();
		
		if(!empty($allschools[0]))
		{
				$allschools[0][0]['school_id'];				
				$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[0][0]['school_id']);
				
				$schoolTestAvg = array();
				
				foreach($allschools[0] as $key=>$val)
				{
					$UserID = $allschools[0][$key]['UserID'];					
					// 1 school student avr					
					//$schoolTestAvg[] = $this->classroommodel->schoolStdAvg($UserID);
					
					$schoolTestAvg[] = $this->classroommodel->schoolStdAvg1($UserID,$assignmentids);					
				}
				
			
	// First School Calculation starts here

$arryvalii = array();
for($i=0;$i<count($schoolTestAvg);$i++)
{

	for($j=0;$j<count($schoolTestAvg[$i]);$j++)
	{
		if(!empty($schoolTestAvg[$i][$j]) && $schoolTestAvg[$i][$j]!="")
		{
		$arryvalii[$j][] = $schoolTestAvg[$i][$j][0]['avg(pass_score_point)'];
		}
	}
	
}

if(!empty($arryvalii))
{

if(!empty($arryvalii[0]))
{
$sum1 = array_sum($arryvalii[0]);
$counttotal1 = count($arryvalii[0]);
$first1 =  $sum1/$counttotal1;
}
else
{
	$first1 =0;
}

if(!empty($arryvalii[1]))
{
$sum2 = array_sum($arryvalii[1]);
$counttotal2 = count($arryvalii[1]);
$first2 =  $sum2/$counttotal2;
}
else
{
	$first2=0;
}

if(!empty($arryvalii[2]))
{
$sum3 = array_sum($arryvalii[2]);
$counttotal3 = count($arryvalii[2]);
$first3 =  $sum3/$counttotal3;
}
else
{
$first3=0;
}

$sumschoolA = $first1 + $first2 + $first3;
$countSchoolA = count($assignmentids);

$finalSchool1Avg = round($sumschoolA/$countSchoolA);

}
else
{
	echo '<script>alert("No Records Available");</script>';
}

		}
		else
		{
			$first1=0;
			$first2=0;
			$first3=0;
			$finalSchool1Avg=0;
			$schoolnamearr[]='';
		}
		
	// 2 school
	
	
	
	
	
	
		if(!empty($allschools[1]))
		{
		
		
		
			$allschools[1][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[1][0]['school_id']);
			
		
			$schoolTestAvgsc2 = array();
				
				foreach($allschools[1] as $key=>$val)
				{
					$UserID = $allschools[1][$key]['UserID'];					
					// 1 school student avr					
						$schoolTestAvg2[] = $this->classroommodel->schoolStdAvg1($UserID,$assignmentids);		
				}
					
		
					
				// Second School Calculation starts here
				
		$arryvalii2 = array();
		for($i=0;$i<count($schoolTestAvg2);$i++)
		{
			for($j=0;$j<count($schoolTestAvg2[$i]);$j++)
			{
				if(!empty($schoolTestAvg2[$i][$j]) && $schoolTestAvg2[$i][$j]!="")
					{
					$arryvalii2[$j][] = $schoolTestAvg2[$i][$j][0]['avg(pass_score_point)'];
					}
			}		
		}
		
		if(!empty($arryvalii2))
		{
		
		if(!empty($arryvalii2[0]))
		{
			$sum4 = array_sum($arryvalii2[0]);
			$counttotal4 = count($arryvalii2[0]);
			$second1 =  $sum4/$counttotal4;
		}
		else
		{
			$second1=0;
		}
		
		if(!empty($arryvalii2[1]))
		{
		$sum5 = array_sum($arryvalii2[1]);
		$counttotal5 = count($arryvalii2[1]);
		$second2 =  $sum5/$counttotal5;
		}
		else
		{
			$second2=0;
		}
		
		if(!empty($arryvalii2[2]))
		{
		$sum6 = array_sum($arryvalii2[2]);
		$counttotal3 = count($arryvalii2[2]);
		$second3 =  $sum6/$counttotal3;
		}
		else
		{
		$second3=0;	
		}
		
		
		 $sumschoolB = $second1 + $second2 + $second3;
		$countSchoolB = count($assignmentids);
		
		 $finalSchool2Avg = round($sumschoolB/$countSchoolB);
		}
		
		
		// Second School Calculation ends here
				
	
		}
		else
		{
			$second1=0;
			$second2=0;
			$second3=0;	
			$finalSchool2Avg=0;
			$schoolnamearr[]='';
		}
		
	// 3 school
	
	if(!empty($allschools[2]))
		{
			$allschools[2][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[2][0]['school_id']);
			
				$schoolTestAvgsc3 = array();
				
				foreach($allschools[2] as $key=>$val)
				{
					$UserID = $allschools[2][$key]['UserID'];					
					// 1 school student avr					
					$schoolTestAvgsc3[] = $this->classroommodel->schoolStdAvg($UserID);	
					$schoolTestAvg3[] = $this->classroommodel->schoolStdAvg1($UserID,$assignmentids);						
				}
				
				
					// Third School Calculation starts here
				
		$arryvalii3 = array();
		for($i=0;$i<count($schoolTestAvg3);$i++)
		{
			
			for($j=0;$j<count($schoolTestAvg3[$i]);$j++)
			{
				if(!empty($schoolTestAvg3[$i][$j]) && $schoolTestAvg3[$i][$j]!="")
					{
					$arryvalii3[$j][] = $schoolTestAvg3[$i][$j][0]['avg(pass_score_point)'];
					}
			}
					
		}
		
		if(!empty($arryvalii3))
		{
		
		if(!empty($arryvalii3[0]))
		{
		$sum7 = array_sum($arryvalii3[0]);
		$counttotal7 = count($arryvalii3[0]);
		$third1 =  $sum7/$counttotal7;
		}
		else
		{
			$third1=0;
		}
		if(!empty($arryvalii3[1]))
		{
			$sum8 = array_sum($arryvalii3[1]);
			$counttotal8= count($arryvalii3[1]);
			$third2 =  $sum8/$counttotal8;
		}
		else
		{
			$third2=0;
		}
		if(!empty($arryvalii3[2]))
		{
			$sum9 = array_sum($arryvalii3[2]);
			$counttotal9 = count($arryvalii3[2]);
			$third3 =  $sum9/$counttotal9;
		}
		else
		{
			$third3=0;
		}
		
		
		 $sumschoolC = $third1 + $third2 + $third3;
		$countSchoolC = count($assignmentids);
		
		 $finalSchool3Avg = round($sumschoolC/$countSchoolC);
		 
		}
		
		// Third School Calculation ends here
		
		}
		else
		{
			$third1=0;
			$third2=0;
			$third3=0;
			$finalSchool3Avg=0;
			$schoolnamearr[]='';
		}
	// 4 school
	
		
		
	
		if(!empty($allschools[3]))
		{
			$allschools[3][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[3][0]['school_id']);
			
			$schoolTestAvgsc4 = array();
				
				foreach($allschools[3] as $key=>$val)
				{
					$UserID = $allschools[3][$key]['UserID'];					
					// 1 school student avr					
					//$schoolTestAvgsc4[] = $this->classroommodel->schoolStdAvg($UserID);									
					$schoolTestAvg4[] = $this->classroommodel->schoolStdAvg1($UserID,$assignmentids);
				}
				

				// Fourth School Calculation starts here
				
		$arryvalii4 = array();
		for($i=0;$i<count($schoolTestAvg4);$i++)
		{
			
						for($j=0;$j<count($schoolTestAvg4[$i]);$j++)
					{
				if(!empty($schoolTestAvg4[$i][$j]) && $schoolTestAvg4[$i][$j]!="")
					{
					$arryvalii4[$j][] = $schoolTestAvg4[$i][$j][0]['avg(pass_score_point)'];
					}
				}
					
		}
		
		
		if(!empty($arryvalii4))
		{
		
		if(!empty($arryvalii4[0]))
		{
		$sum10 = array_sum($arryvalii4[0]);
		$counttotal10 = count($arryvalii4[0]);
		$Fourth1 =  $sum10/$counttotal10;
		}
		else
		{
			$Fourth1=0;
		}
		if(!empty($arryvalii4[1]))
		{
		$sum11 = array_sum($arryvalii4[1]);
		$counttotal11= count($arryvalii4[1]);
		$Fourth2 =  $sum11/$counttotal11;
		}
		else
		{
		$Fourth2=0;
		}
		
		if(!empty($arryvalii4[2]))
		{
		$sum12 = array_sum($arryvalii4[2]);
		$counttotal12 = count($arryvalii4[2]);
		$Fourth3 =  $sum12/$counttotal12;
		}
		else
		{
			$Fourth3=0;
		}
		
		
		 $sumschoolD = $Fourth1 + $Fourth2 + $Fourth3;
		$countSchoolD = count($assignmentids);
		
		 $finalSchool4Avg = round($sumschoolD/$countSchoolD);
		
		}
		
		// Fourth School Calculation ends here
				
	
		}
		else
		{
			$Fourth1=0;
			$Fourth2=0;
			$Fourth3=0;
			 $finalSchool4Avg=0;
			 $schoolnamearr[]='';
		}
	// 5 school
		if(!empty($allschools[4]))
		{
			$allschools[4][0]['school_id'];
			$schoolnamearr[] = $this->classroommodel->schoolNameFun($allschools[4][0]['school_id']);
			
					
			$schoolTestAvgsc5 = array();
				
				foreach($allschools[4] as $key=>$val)
				{
					$UserID = $allschools[4][$key]['UserID'];					
					// 1 school student avr					
					//$schoolTestAvgsc5[] = $this->classroommodel->schoolStdAvg($UserID);									
					
					$schoolTestAvg5[] = $this->classroommodel->schoolStdAvg1($UserID,$assignmentids);
				}
				
				// Fifth School Calculation starts here
				
		$arryvalii5 = array();
		for($i=0;$i<count($schoolTestAvg5);$i++)
		{
			
						for($j=0;$j<count($schoolTestAvg5[$i]);$j++)
					{
				if(!empty($schoolTestAvg5[$i][$j]) && $schoolTestAvg5[$i][$j]!="")
					{
					$arryvalii5[$j][] = $schoolTestAvg5[$i][$j][0]['avg(pass_score_point)'];
					}
					}
					
		}
		
		
		if(!empty($arryvalii5))
		{
		
		if(!empty($arryvalii5[0]))
		{
		$sum13 = array_sum($arryvalii5[0]);
		$counttotal13 = count($arryvalii5[0]);
		$fifth1 =  $sum13/$counttotal13;
		}
		else
		{
			$fifth1=0;
		}
		
		if(!empty($arryvalii5[1]))
		{
		$sum14 = array_sum($arryvalii5[1]);
		$counttotal14= count($arryvalii5[1]);
		$fifth2 =  $sum14/$counttotal14;
		}
		else
		{
			$fifth2=0;
		}
		
		if(!empty($arryvalii5[2]))
		{
		$sum15 = array_sum($arryvalii5[2]);
		$counttotal15 = count($arryvalii5[2]);
		$fifth3 =  $sum15/$counttotal15;
		}
		else
		{
			$fifth3=0;
		}		
		
		
		 $sumschoolE = $fifth1 + $fifth2 + $fifth3;
		$countSchoolE = count($assignmentids);
		
		 $finalSchool5Avg = round($sumschoolE/$countSchoolE);
		
		}
		
		}
		else
		{
			$fifth1=0;
			$fifth2=0;
			$fifth3=0;
			$finalSchool5Avg=0;
			$schoolnamearr[]='';
		}

	
		$arrfltr = array_filter($schoolnamearr);
		$arrsumfltr = count($arrfltr);
		
		if(empty($arrsumfltr))
		{
			$arrsumfltr=1;
		}
	
// pie chart working

//1

	if(empty($first1))
	{
		$first1 = 0;
	}
	if(empty($second1))
	{
		$second1 = 0;
	}
	if(empty($third1))
	{
		$third1 = 0;
	}
	if(empty($Fourth1))
	{
		$Fourth1 = 0;
	}
	if(empty($fifth1))
	{
		$fifth1 = 0;
	}
//2
	
	if(empty($first2))
	{
		$first2 = 0;
	}
	if(empty($second2))
	{
		$second2 = 0;
	}
	if(empty($third2))
	{
		$third2 = 0;
	}
	if(empty($Fourth2))
	{
		$Fourth2 = 0;
	}
	if(empty($fifth2))
	{
		$fifth2 = 0;
	}
	
	//3
	if(empty($first3))
	{
		$first3 = 0;
	}
	if(empty($second3))
	{
		$second3 = 0;
	}
	if(empty($third3))
	{
		$third3 = 0;
	}
	if(empty($Fourth3))
	{
		$Fourth3 = 0;
	}
	if(empty($fifth3))
	{
		$fifth3 = 0;
	}
	$avgass1 = array();
	
	 $avgass1[] = ($first1 + $second1 + $third1 + $Fourth1 + $fifth1)/$arrsumfltr;
	 $avgass1[] = ($first2 + $second2 + $third2 + $Fourth2 + $fifth2)/$arrsumfltr;
	 $avgass1[] = ($first3 + $second3 + $third3 + $Fourth3 + $fifth3)/$arrsumfltr;
	
	//echo '<pre>';
	//print_r($avgass1);
	//exit;	

	foreach($assignmentids as $key222=>$val222)
	{
		$res = mysql_query('select * from proficiency where assessment_id="'.$assignmentids[$key222].'"');
		$row = mysql_fetch_assoc($res);
				
					$bbfrom = $row['bbasicfrom'];
					$bbto = $row['bbasicto'];
					$basicfrom = $row['basicfrom'];
					$basicto = $row['basicto'];
					$pro_from = $row['pro_from'];
					$pro_to = $row['pro_to'];
					
					$num = $avgass1[$key222];
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					$bb[] = $num;
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					  $b[] = $num;
				}
				else if($num>=$pro_from && $num<=$pro_to)
				{
					$pf[] = $num;
				}
	}
	
	

	
	
// end pie chart working
	
	

	if(count($valarry)<5)
	{
$numarr = array();
$namearr = array();
$uidarr = array();

// pie graph work 
	
	//print_r($avgarr);
	
	
			//$adv = array();
			
			
			
	/*for($i=0;$i<count($avgarr);$i++)
		{
				$num = $avgarr[$i];
				
					if($num>=$bbfrom && $num<$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$basicfrom && $num<$basicto)
				{
					 $basic[] = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro[] = $num;
				}*/
			/*	if($num>=0 && $num<40)
				{					
					$bbasic[] = $num;					
				}
				else if($num>=40 && $num<77)
				{
					 $basic[] = $num;
				}
				if($num>=77 && $num<100)
				{
					$pro[] = $num;
				}*/
				
		//}
				//1
			
		/* $ttarr = array_sum($bbasic);
		 
		  if($ttarr==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			  $avgbbbasic = $ttarr/$total;
			 }
		  
		 // 2
		  	 $ttarr1 = array_sum($basic);	
		  
		  	 if($ttarr1==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $ttarr1/$total1;
			 }
		 //3
		 
		  $ttarr2 = array_sum($pro);		 
		 if($ttarr2==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
				  $avgpro = $ttarr2/$total2;
			 }
	
	*/	
			if(empty($bb))
			{
				$finalbb=0;
			}
			else
			{
				$total1 = count($bb);
				$tt1 = array_sum($bb);
				$finalbb = $tt1/$total1;
			}
			
			if(empty($b))
			{
				$finalb=0;
			}
			else
			{
				$total2 = count($b);
				$tt2 = array_sum($b);
				$finalb = $tt2/$total2;	
			}
			
			if(empty($pf))
			{
			$finalpf=0;	
			}
			else
			{
				$total3 = count($pf);
				$tt3 = array_sum($pf);
				$finalpf = $tt3/$total3;
			}
			 $totalval = $finalbb + $finalb + $finalpf;
	
	if($finalbb!=0)
	{
		$frstval = ($finalbb/$totalval)*100;
	}
	else
	{
	$frstval=0;
	}
	if($finalb!=0)
	{
		$secval = ($finalb/$totalval)*100;
	}
	else
	{
	$secval=0;
	}
	if($finalpf!=0)
	{
		$thrdval = ($finalpf/$totalval)*100;
	}
	else
	{
	$thrdval=0;
	}
	
	 $finalbb=round($frstval);	
	 $finalb=round($secval);
	 $finalpf=round($thrdval);
		
			
		//print_r($numarr);
$ncat = implode("','",$schoolnamearr);

$ncat = "['','".$ncat."']";

/*
//1
echo '1=>';
echo ','.$sc1st1;
echo ','.$sc1st2;
echo ','.$sc1st3;
echo '2=>';
echo ','.$sc2st1;
echo ','.$sc2st2;
echo ','.$sc2st3;
echo '3=>';
echo ','.$sc3st1;
echo ','.$sc3st2;
echo ','.$sc3st3;
echo '4=>';
echo ','.$sc4st1;
echo ','.$sc4st2;
echo ','.$sc4st3;
echo '5=>';
echo ','.$sc5st1;
echo ','.$sc5st2;
echo ','.$sc5st3;
*/

/*
?>

<b><font color="#2f7ed8">Below Basic = <?Php echo @$finalbb; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#0d233a">Basic  =  <?Php echo @$finalb; ?>% </font>,&nbsp&nbsp</b>
<b><font color="#8bbc21">Proficient =<?Php echo @$finalpf; ?>% </font></b>
<?Php */ ?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: <?Php echo $ncat; ?>
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' %';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
			
          
            series: [
			{
				type: 'column',
				name: '',
				data: [,,,,],
			},
			{
                type: 'column',
               name: '<?php echo $first;?>',
                data: [0,<?Php echo $first1; ?>, <?Php echo $second1; ?>, <?Php echo $third1; ?>, <?Php echo $Fourth1; ?>, <?Php echo $fifth1; ?>]
            }, {
                type: 'column',
               name: '<?php echo $second;?>',
                 data: [0,<?Php echo $first2; ?>, <?Php echo $second2; ?>, <?Php echo $third2; ?>, <?Php echo $Fourth2; ?>, <?Php echo $fifth2; ?>]
            }, {
                type: 'column',
                name: '<?php echo $third;?>',
                 data: [0,<?Php echo $first3; ?>, <?Php echo $second3; ?>, <?Php echo $third3; ?>, <?Php echo $Fourth3; ?>, <?Php echo $fifth3; ?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [0,<?php echo $finalSchool1Avg; ?>, <?php echo $finalSchool2Avg; ?>, <?php echo $finalSchool3Avg; ?>, <?php echo $finalSchool4Avg; ?>, <?php echo $finalSchool5Avg; ?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            }, {
                type: 'pie',
                name: 'Total consumption',
                data: [{
                    name: 'Below Basic',
                    y: <?Php echo @$finalbb; ?>,
                    color: Highcharts.getOptions().colors[0] // Below Basic's color
                }, {
                    name: 'Basic Scores ',
                    y: <?Php echo @$finalb; ?>,
                    color: Highcharts.getOptions().colors[1] // Basic's color
                }, {
                    name: 'Proficient',
                    y: <?Php echo @$finalpf; ?>,
                    color: Highcharts.getOptions().colors[2] // Proficient's color
                }],
                 center: [25, 10],
                size: 100,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    });
    

		</script>
	
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>

<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
		<!-- End Graph -->			
		<?Php 
		}  // count if END
		else
		{		
			if(isset($allschools))
			{
		?>
			<script type="text/javascript">
			alert("No Record Found");
			</script>
		<?php
			}
			echo $this->session->flashdata('item');			
		
		}

	}  // Main if end 
	
	
		
		
		?>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
