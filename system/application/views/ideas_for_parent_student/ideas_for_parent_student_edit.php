﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District_id</td>
     <td>Ideas For Parent Student</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1 + ($page -1)*10;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->ideas_for_parent_student;?></td>
   <td><?php echo $data->status;?></td>
      <td>
      <button class="edit_ideas_parent_student btn btn-primary" type="button" name="<?php echo $data->id;?>" value="Edit" data-dismiss="modal" aria-hidden="true" id="edit"><i class="icon-pencil"></i></button>
      </td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
           <td>
         <button type="Submit" id="remove_ideas_parent_student" value="Remove" name="<?php echo $data->id;?>" data-dismiss="modal" class="remove_ideas_parent_student btn btn-danger"><i class="icon-trash"></i></button>
  </td>
  
  </tr>
  <?php  
  $con++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_ideas_parent_student ').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'ideas_for_parent_student/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#ideas_for_parent_student_id').val(result[0].id);
       $('#ideas_for_parent_student').val(result[0].ideas_for_parent_student);
       $('#status').val(result[0].status);
       console.log(result[0].behavior_location);
       $("#dialog").dialog({
      modal: true,
            height:200,
      width: 400
      });
       }
     });  
});

$(".remove_ideas_parent_student").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'ideas_for_parent_student/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed remove Ideas For Parent Student list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

