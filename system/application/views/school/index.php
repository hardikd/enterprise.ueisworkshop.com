<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::schools::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/school.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/school_type_dist.js" type="text/javascript"></script>
<script type="text/javascript">
var site_url='<?php echo SITEURLM;?>';
function inactive(type,id)
{
var t="'"+type+"'";
$.post("school/changestatus/inactive",{ school_id:id,type:type } ,function(data)
 {
    
	if(data==0)    
	{
	  alert('Failed Please Try Again');
	}
	else
	{
	 
	 $('#'+type+'_'+id).html('<img src="'+site_url+'/images/home_off.jpg" style="cursor:pointer;" title="Active" onclick="active('+t+','+id+')" />');
	
	}
 });


}


function active(type,id)
{
  var t="'"+type+"'";
$.post("school/changestatus/active",{ school_id:id,type:type } ,function(data)
 {
    
	if(data==0)    
	{
	  alert('Failed Please Try Again');
	}
	else
	{
	 
	  $('#'+type+'_'+id).html('<img src="'+site_url+'/images/home_on.jpg" style="cursor:pointer;" title="InActive" onclick="inactive('+t+','+id+')" />');
	
	}
 });



}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
        <table align="center" cellpadding="5" >
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { ?>
		<?php foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		Districts:
		</td>
		<td>
		<select class="combobox" name="district" id="district">
		<option value="all">All</option>
		<?php if(!empty($district)) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } else { ?>
		<option value="">No Districts Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getdistrict" id="getdistrict" value="Submit">
		</td>
		</tr>
		</table>
		<table>
		<tr>
		<td>
		Legend:
		</td>
		</tr>
		<tr>
		<td>
		LP:Lesson Plan
		</td>
		</tr>
		<tr>
		<td>
		NC:Notification
		</td>
		</tr>
		<tr>
		<td>
		TE:Teacher Evaluation
		</td>
		</tr>
		</table>
		<div id="schooldetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New" class="btnbig" type="button" name="school_add" id="school_add" value="Add New" >
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="School" style="display:none;"> 

<form name='schoolform' id='schoolform' method='post' onsubmit="return false" >
<table cellpadding="0" cellspacing="10" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>School Name:
				</td>
				<td valign="top">
				<input class='txtbox' type='text'  id='school_name' name='school_name'>
				</td>
			</tr>
			
			<tr>
				<td valign="top" >
				<font color="red">*</font>User Name:
				</td>
				<td valign="top">
				<input class='txtbox' type='text'  id='username' name='username'>
				</td>
			</tr>
			<tr>	
				<td valign="top" >
				<font color="red">*</font>Password:
				</td>
				<td>
				<input class='txtbox' type='password'  id='password' name='password'>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Country:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="country_id" id="country_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>State:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="state_id" id="state_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>	
				<td valign="top" >
				<font color="red">*</font>District:
				</td>
				<td>
				<select name="district_id" id="district_id">
				<option value="">-Select-</option>
				</select>
				<input  type='hidden'  id='school_id' name='school_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>School Type:
				</td>
				<td valign="top">
				<select class="combobox" name="school_type_id" id="school_type_id"  >
				<option value="">-Please Select-</option>		
				</select>
				</td>
			</tr>
			
						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New" class="btnbig" type='submit' name="submit" id='schooladd' value='Add' > <input title="Cancel" class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
