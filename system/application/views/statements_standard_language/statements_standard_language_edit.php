﻿    <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District_id</td>
     <td>Standard Language</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     <?php $con = 1; ?>
     <?php 
foreach($alldata as $data)
{?>
     </tr>
     <tr id="home_<?php echo $data->id;?>">
   <td><?php echo $con;?></td>
   <td><?php echo $data->district_id;?></td>
   <td><?php echo $data->standard_language;?></td>
   <td><?php echo $data->status;?></td>
      <td><button class="edit_standard_name btn btn-primary" data-dismiss="modal" type="button" name="<?php echo $data->id;?>" value="Edit" aria-hidden="true"><i class="icon-pencil"></i></button></td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
                <td><button data-dismiss="modal" type="Submit" value="Remove" name="<?php echo $data->id;?>" class="remove_standard_name btn btn-danger" id="remove_standard_name"><i class="icon-trash"></i></button></td>

    
  </tr>
  <?php $con ++;?>
  <?php  
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_standard_name').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'statements_standard_language/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#standard_id').val(result[0].id);
       $('#standard_language').val(result[0].standard_language);
       $('#status').val(result[0].status);
       console.log(result[0].behavior_location);
       $("#dialog").dialog({
      modal: true,
            height:300,
      width: 400
      });
       }
     });  
});

$(".remove_standard_name").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'statements_standard_language/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#home_"+id).css('display','none');
          alert('Successfully removed standard list!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

