<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<!--old script start -->


<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM.$view_path; ?>js/banktime.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/video.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo SITEURLM?>css/wall.css" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.oembed.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>js/wall.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
$(function() {

$(".more").click(function() {
   var msg= $("#last_msg").val();
   
   
$("#morebutton").html('<img src="<?php echo SITEURLM?>images/ajax-loader.gif" />');
	$.ajax({
	type: "POST",
  url: "banktime/more_updates",
   data: "lastmsg="+ msg,
  cache: false,
  success: function(html){
 $("#more_updates").append(html);
 $(".more").remove();
 
 
	
  }
});
	
		

    return false;
	});
	
	$('.more2').live('click', function() {
	
	
   
     var msg= $("#last_msg").val();
	$("#morebutton").html('<img src="<?php echo SITEURLM?>images/ajax-loader.gif" />');
	$.ajax({
	type: "POST",
  url: "banktime/more_updates",
   data: "lastmsg="+ msg,  
  cache: false,
  success: function(html){
  $(".more"+msg).remove(); 
 $("#more_updates").append(html);
 
 	
  }
});
	
		

    return false;
	});
	

	
	


});


</script>

 <style type="text/css">
#morebutton
{
background:#cccccc; 
color:#000000; 
font-size:14px; 
height:26px; 
font-weight:bold; 
width:860px; 
padding:4px; 
border:#000000 solid 2px; 
margin-top:10px; 
text-align:center;
 
}

.sttext {
    display: block;
    font-size: 16px;
   margin-left: 0px;
	padding-left:0px;
    min-height: 50px;
	overflow: hidden;
    padding: 5px;
    width: 90%;
    word-wrap: break-word;
    text-align:left;
}

.stimg {
    border: 1px solid #dedede;
    float: left;
    height: 50px;
    margin: 0 0 0 4px;
    padding: 3px;
    width: 50px;
    overflow: none;
}
</style>
<!--end old script -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
  <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/professional_development">Professional Development</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                             <?php 
                        if ($this->session->userdata('login_type') == 'teacher') {?>
                        		<a href="<?php echo base_url();?>banktime">Group </a>
                           <?php  }else if ($this->session->userdata('login_type') == 'observer') {?>
                               <a href="<?php echo base_url();?>banktime">Group Professional Development </a>
                           <?php }?>
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <?php 
                        if ($this->session->userdata('login_type') == 'teacher') {?>
                        		 <h4>Group</h4>
                           <?php  }else if ($this->session->userdata('login_type') == 'observer') {?>
                                <h4>Group Professional Development</h4>
                           <?php }?>
                         </div>
                         <div class="widget-body">
                         <div class="widget widget-tabs purple">
                        <div class="widget-title">
                            
                        </div>
                         <div class="widget-body">
                            <div class="tabbable ">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#widget_tab1" data-toggle="tab">Academic Talk</a></li>
                                    <li class=""><a href="#widget_tab2" data-toggle="tab">Video Library</a></li>
                                    <li class=""><a href="#widget_tab3" data-toggle="tab">Media Library</a></li>
                                    <li class=""><a href="#widget_tab4" data-toggle="tab">Website Library</a></li>
                                </ul>
                                </div>
                                
                                
                                <div class="tab-content">
                                <div class="tab-pane active" id="widget_tab1">
                                <div class="space20"></div>
                                
                                		 <div id="wall_container">
	
	 		<form name="search" method="post" action="banktime">
        
         <div class="form-horizontal">
        <div class="control-group">
                        <label class="control-label" style="width:127px;">School Name:</label>
                           <div class="controls" style="font-size:15px; margin-left:10px; padding-top:6px;">
            <?php echo $this->session->userdata("school_self_name");?>
			<input type="hidden" id="school_id" name="school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
			
                         </div>
                      </div>
                      
           <div class="control-group">
                        <label class="control-label" style="width:127px;">Select Subject:</label>
                           <div class="controls" style="margin-left:10px;">
            <select class="combobox1" name="subject_id" id="subject_id" style="width:250px;">
           <?php if($subjects!=false) { ?>
        <option value="">-Please Select Subject-</option> 		
		<?php foreach($subjects as $val)
		{
		?>
		<option value="<?php echo $val['subject_id'];?>"  
		<?php if($this->session->userdata('search_aca_subject_id') && $this->session->userdata('search_aca_subject_id')==$val['subject_id'] ) { ?> selected <?php } ?>
		><?php echo $val['subject_name'];?></option>
		<?php } } else { ?>
		<option value="">No Subjects Found</option>
		<?php } ?>
		</select>
           
                         </div>
                      </div>  
                      
                 <div class="control-group">
                        <label style="width:127px;" class="control-label">Select Grade:</label>
                           <div class="controls" style="margin-left:10px;" >
           <select class="combobox1" name="grade_id" id="grade_id" style="width:250px;" >
		<?php if($grades!=false) { ?>
        <option value="">-Please Select Grade-</option> 		
		<?php foreach($grades as $val)
		{
		?>
		<option value="<?php echo $val['grade_id'];?>"  
		<?php if($this->session->userdata('search_aca_grade_id') && $this->session->userdata('search_aca_grade_id')==$val['grade_id'] ) { ?> selected <?php } ?>
		><?php echo $val['grade_name'];?></option>
		<?php } } else { ?>
		<option value="">No Grades Found</option>
		<?php } ?>
		</select>
			
                         </div>
                      </div>     
                      
                       <div class="control-group">
                        <label class="control-label" style="width:127px;">Search Threads:</label>
                           <div class="controls" style="margin-left:10px;">
     <input type="textbox" name="alphasearch" id="alphasearch" value="<?php echo $this->session->userdata('search_aca_search_id');?>" style="width:250px;">   				
		<input  type="submit" class="btn btn-small btn-purple search" id="search" name="search" value="Search">
			
                         </div>
                      </div>          
            </div>
		</form>
		
	<div id="content" align="center">

	<?php include($view_path.'inc/load_messages.php'); ?>

	</div>



	</div>
                                
                                
                              
                                </div> 
                               
                                
                               
                                <div class="tab-pane" id="widget_tab2">
                              
                              	<form id="videoForm" name="videoForm" method="post" >
		
        <div class="form-horizontal">
        <div class="control-group">
                        <label class="control-label" style="width:127px;">School Name:</label>
                           <div class="controls" style="font-size:15px; margin-left:10px; padding-top:6px;">
            <?php echo $this->session->userdata("school_self_name");?>
			<input type="hidden" id="video_school_id" name="video_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
			
                         </div>
                      </div>
        <div class="control-group">
                        <label class="control-label" style="width:127px;">Add Link:</label>
                           <div class="controls" style="font-size:22px; margin-left:10px;">
        <textarea name="videofileToUpload" id="videofileToUpload" rows="4" cols="30" style="height: 67px;width: 433px"></textarea>	
                         </div>
                      </div>
        <div class="control-group">
                        <label class="control-label"></label>
                           <div class="controls" style="font-size:22px;">
       	<p id="videomessage"></p>
     <input type="Submit" class="btn btn-small btn-purple video_update_button" value="Submit"  id="video_update_button" />
                         </div>
                      </div>
        
	        <br />
			<div style="margin-left:200px;"><b>Or</b></div>
			<br />
            
            <div class="control-group">
                        <label class="control-label" style="width:127px;">Search Video Library:</label>
                           <div class="controls" style="font-size:22px; margin-left:10px;">
       	<p id="videomessage"></p>
<input type="textbox" name="videosearchvalue" id="videosearchvalue" value="<?php echo $this->session->userdata('search_videosearch_school_id');?>">   		
		<input type="button"  id="videosearch" onclick="videoload()" class="btn btn-small btn-purple search" name="search" value="Search">                         </div>
                      </div> 
        
        </div>  
	
	
 	</form>
	<div id='videoflashmessage'>
	<div id="videoflash" align="left"  ></div>
	</div> 	
	
	<!-- video  Upload End -->
	<table>
	<tr>
	<td align="center">
	<div id="videodetails" style="display:none;">
		<input type="hidden" id="videopageid" value="">
		<input type="hidden" id="video_school_id" name="video_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
		<div id="videomsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>

</div>

                              
                              <div class="tab-pane" id="widget_tab3">
                                 <div class="space20"></div>
                              
                              	<form id="mediaForm" name="mediaForm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="MEDIA_MAX_FILE_SIZE" value="2097152" />
			
			 <div class="form-horizontal">
        <div class="control-group">
                        <label class="control-label" style="width:127px;">School Name:</label>
                           <div class="controls" style="font-size:15px; margin-left:10px; padding-top:6px;">
            <?php echo $this->session->userdata("school_self_name");?>
			<input type="hidden" id="media_school_id" name="media_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
			
                         </div>
                      </div>

        <div class="control-group">
                        <label class="control-label" style="width:127px;">Add Media Description:</label>
                           <div class="controls" style="font-size:15px; margin-left:10px;">
            <textarea name="mediadesc" id="mediadesc" style="height: 67px;width: 433px"></textarea>
                         </div>
                      </div>

        <div class="control-group">
                        <label class="control-label" style="width:127px;">Add a File:</label>
                           <div class="controls" style="font-size:15px; margin-left:10px;">
						<input type="file" name="mediafileToUpload" id="mediafileToUpload" size="38" />
                         </div>
                      </div>
<p id="mediamessage"></p>

    <div class="control-group">
                        <label class="control-label"></label>
                           <div class="controls" style="font-size:22px;">
	 <input type="Submit"  value="Submit" class="btn btn-small btn-purple media_update_button" id="media_update_button" />
                         </div>
                      </div>
               <div style="margin-left:200px;">       
    		
			<b>Or</b>
			<br />
            <br />
     </div>
         <div class="control-group">
                        <label class="control-label" style="width:127px;">Search Publication:</label>
                           <div class="controls" style="font-size:22px; margin-left:10px;">
<input type="textbox" name="mediasearchvalue" id="mediasearchvalue" value="<?php echo $this->session->userdata('search_mediasearch_school_id');?>">   		
                         </div>
                      </div>
         <div class="control-group">
                        <label class="control-label"><b></b></label>
                           <div class="controls" style="font-size:22px;">
		<input type="button" id="mediasearch" onclick="mediaload()" class="btn btn-small btn-purple search" name="search" value="Search">
                         </div>
                      </div>

     </div>
                      
                     
 	</form>
	<div id='mediaflashmessage'>
	<div id="mediaflash" align="left"  ></div>
	</div> 	
	<!-- Media End -->
	<table>
	<tr>
	<td align="center">
	<div id="mediadetails" style="display:none;">
		<input type="hidden" id="mediapageid" value="">
		<input type="hidden" id="media_school_id" name="media_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
		<div id="mediamsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
                              
                           
                              </div>
                              
                              <div class="tab-pane" id="widget_tab4">
                                 <div class="space20"></div>
                              	<form id="websiteForm" name="websiteForm" method="post" >
			 <div class="form-horizontal">
        <div class="control-group">
                        <label class="control-label" style="width:127px;">School Name:</label>
                           <div class="controls" style="font-size:15px; margin-left:10px; padding-top:6px;">
            <?php echo $this->session->userdata("school_self_name");?>
			<input type="hidden" id="website_school_id" name="website_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
			
                         </div>
                      </div>
               <div class="control-group">
                        <label class="control-label" style="width:127px;">Add Link:</label>
                           <div class="controls" style="font-size:22px; margin-left:10px;">
<textarea name="websitefileToUpload" id="websitefileToUpload" rows="4" cols="30" style="height: 67px; width: 433px"></textarea>			
                         </div>
                      </div>         
                   
                   
               <div class="control-group">
                        <label class="control-label" style="width:127px;">Description:</label>
                           <div class="controls" style="font-size:22px; margin-left:10px;">
<textarea name="websitedesc" id="websitedesc" style="height: 67px;width: 433px"></textarea>
                         </div>
                      </div>            
        
        	<p id="websitemessage"></p>
               <div class="control-group">
                        <label class="control-label"></label>
                           <div class="controls" style="font-size:22px;">
	 <input type="Submit"  value="Submit"  id="website_update_button"  class="btn btn-small btn-purple website_update_button"/>
                         </div>
                      </div>            
			 <div style="margin-left:200px;">       
            <br />
			<b>Or</b>
			
			<br />
            <br />
            </div>
    
               <div class="control-group">
                        <label class="control-label" style="width:127px;">Search Websites:</label>
                           <div class="controls" style="font-size:22px; margin-left:10px;">
<input type="textbox" name="websitesearchvalue" id="websitesearchvalue" value="<?php if($this->session->userdata('search_websitesearch_school_id')) { echo $this->session->userdata('search_websitesearch_school_id');} ?>">   		
		<input type="button" id="websitesearch" onclick="websiteload()" class="btn btn-small btn-purple search" name="search" value="Search">			
                         </div>
                      </div>            
                      </div>
		
		
            
	   

	
	
 	</form>
	<!-- website end -->
	<table>
	<tr>
	<td align="center">
	<div id="websitedetails" style="display:none;">
		<input type="hidden" id="websitepageid" value="">
		<input type="hidden" id="website_school_id" name="website_school_id" value="<?php echo $this->session->userdata("school_summ_id");?>">
		<div id="websitemsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
                              </div>
                                
                                
            
                                       
                                
                                  
                                                       
                                
                                
                                </div>
                           
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
               </div>
             </div>  </div>
             </div>
            
             
             
             
             
             
               <div id="myModal-delete-student" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
                                </div>
                                <div class="modal-body">
                                     Please select "Yes" to remove this file.
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
                                    <button data-dismiss="modal" class="btn btn-success"><i class="icon-check"></i> Yes</button>
                                </div>
                            </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>   
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
 <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
 
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
 
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
 

   <!-- END JAVASCRIPTS --> 
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
      <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>


    


   
   
     <script>
 var elem = document.getElementById("select_1");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script>

    <script>
 var elem = document.getElementById("select_2");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script>



 <script>
 var elem = document.getElementById("select_3");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script>

    <script>
 var elem = document.getElementById("select_4");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script>

 <script>
function show(ele)    {      
    var links = ['upload1','upload2'];
    var srcElement = document.getElementById(ele);      
    var doShow = true;
    if(srcElement != null && srcElement.style.display == "block")
        doShow = false;
    for( var i = 0; i < links.length; ++i )    {
        var otherElement = document.getElementById(links[i]);      
        if( otherElement != null )
            otherElement.style.display = 'none';
    }
    if( doShow )
        srcElement.style.display='block';         
    return false;
  }
</script>




 <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
   
   <script>
   
   var button = document.getElementById('reportDiv2_btn');

button.onclick = function() {
    var div = document.getElementById('reportDiv2');
    if (div.style.display !== 'block') {
        div.style.display = 'block';
    }
    else {
        div.style.display = 'none';
    }
};

</script>
   
   
   
   
   
   
 
</body>
<!-- END BODY -->
</html>