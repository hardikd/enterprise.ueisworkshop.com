<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Lickert Elements::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/lickertcopy.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/countries3.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
		               <table align="center" cellpadding="5" >
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { 
		foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		District:
		</td>
		<td>
		<select class="combobox" name="district" id="district" onchange="district_change(this.value)">
		<?php if(!empty($district)) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		
		
		<td>
		Proficiency Standards:
		</td>
		<td>
		<select class="combobox" name="lickertgroup" id="lickertgroup">
		<option value="all">All</option>
		<?php if(!empty($lickertgroup)) { 
		foreach($lickertgroup as $val)
		{
		?>
		<option value="<?php echo $val['group_id'];?>" <?php if(isset($group_id) && $group_id==$val['group_id'] ) {?> selected <?php  } ?> ><?php echo $val['group_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getlickertgroup" id="getlickertgroup" value="Submit">
		</td>
		</tr>
		</table>
		<div id="lickertpointdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<table align="center" cellpadding="5" >
		<tr>
		<td align="center">
		Copy To:
		</td>
		<td>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td >
		Countries:
		</td>
		<td >
		<select class="combobox" name="countries1" id="countries1" onchange="states1_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states1" id="states1" onchange="district1_all(this.value)" >
		<?php if($states!=false) { 
		foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td colspan="3">
		Select District:<select class="combobox" name="districtcopy" id="districtcopy" onchange="districts1_change(this.value)">
		<?php if(!empty($district)) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		Select Proficiency Standard:<select class="combobox" name="group_id" id="group_id">
		<option value="">-Select-</option>
		<?php if(!empty($lickertgroup)) { 
		foreach($lickertgroup as $val)
		{
		?>
		<option value="<?php echo $val['group_id'];?>" <?php if(isset($group_id) && $group_id==$val['group_id'] ) {?> selected <?php  } ?> ><?php echo $val['group_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		</tr>
		<tr>
		<td align="center">
		<input type="button" class="btnsmall" title="Submit" name="copysubmit" id="copysubmit" value="Submit">
		</td>
		</tr>
		</table>
		</div>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
</body>
</html>
