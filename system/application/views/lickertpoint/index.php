<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Lickert Elements::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/addnew.js" type="text/javascript"></script>
<!--<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>-->
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/lickertpoint.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/countries3.js" type="text/javascript"></script>
<script type="text/javascript">
var dist_user_district_id= 0;
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
        <table align="center" cellpadding="5" >
		<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { 
		foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
		<tr>
		<td>
		District:
		</td>
		<td>
		<select class="combobox" name="district" id="district" onchange="district_change(this.value)">
		<?php if(!empty($district)) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		Proficiency Standards:
		</td>
		<td>
		<select class="combobox" name="lickertgroup" id="lickertgroup">
		<?php if(!empty($lickertgroup)) { ?>
		<option value="all">All</option>
		<?php foreach($lickertgroup as $val)
		{
		?>
		<option value="<?php echo $val['group_id'];?>" <?php if(isset($group_id) && $group_id==$val['group_id'] ) {?> selected <?php  } ?> ><?php echo $val['group_name'];?></option>
		<?php } }  else { ?>
		<option value="0">No Proficiency Standards Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		<input type="button" class="btnsmall" title="Submit" name="getlickertgroup" id="getlickertgroup" value="Submit">
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<a href="lickertpoint/copy">Copy Lickert Elements</a>
		</td>
		</tr>
		</table>
		<div id="lickertpointdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        <div>
		<input title="Add New" class="btnbig" type="button" name="lickertpoint_add" id="lickertpoint_add" value="Add New">
		</div>		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Lickert Elements" style="display:none;"> 

<form name='lickertpointform' id='lickertpointform' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Country:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="country_id" id="country_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>State:
				</td>
				<td valign="top"  style="height:40px;">
				<select class="combobox" name="state_id" id="state_id"  >
				<option value="">-Select-</option>
				</select>
				</td>
				
			</tr>
			<tr>	
				<td valign="top" class="style1">
				<font color="red">*</font>District:
				</td>
				<td>
				<select class="combobox" name="district_id" id="district_id">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Proficiency Standard:</td>
				<td valign="top" style="height:40px;">
				<select class="combobox" name="group_id" id="group_id">
				<option value="">-select</option>
				</select>
				<input  type='hidden'  id='point_id' name='point_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				Question :
				</td>
				<td valign="top" style="height:40px;">
				<textarea class='txtarea' type='text'  id='question' name='question'></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Question Type:
				</td>
				<td valign="top" style="height:40px;">
				<select class="combobox" name="ques_type_id" id="ques_type_id">
				<option value="">-select</option>
				</select>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" >
				<font color="red">*</font>Group Type:
				</td>
				<td valign="top" style="height:40px;">
				<select class="combobox" name="group_type_id" id="group_type_id">
				<option value="">-select</option>
				</select>
				
				</td>
				
			</tr>
			<tr>
			<td colspan="2">
			<table id="myTable" width="100%" style="display:none;">
			<tr>
			<td>
			<input type="hidden" name="countergroup" id="countergroup" value="">
			</td>
			</tr>
			<tr id="TextBoxDiv1">
			<td valign="top" >
			Subgroup Name: 
		</td>
		<td valign="top" style="height:40px;">
		<input class="txtbox" type='text' id='textbox1' name="textbox1"><input class="btnbig" title="Remove" type='button' value='Remove Button' id='removeButton' onclick="remove(1);">
		</td>
	    </tr>
		</table>
		</td>
       </tr>
<tr id="addsub" style="display:none">
<td>
<input class="btnsmall_long" title="Add SubGroup" type='button' value='Add SubGroup' id='addButton'>
</td>
</tr>
<!--<input type='button' value='Remove Button' id='removeButton'>-->


						
</tr><tr><td valign="top"></td><td valign="top"><input title="Add New" class="btnbig" type='submit' name="submit" id='lickertpointadd' value='Add' > <input title="Cancel" class="btnbig" type='button' name='cancel' id='cancel' value='Cancel'></td></tr></table></form>
</div>
</body>
</html>
