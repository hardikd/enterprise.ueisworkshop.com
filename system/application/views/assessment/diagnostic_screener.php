<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
  <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
                 <h3 class="page-title">
                      <i class="icon-bar-chart"></i>&nbsp; Assessment Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>assessment">Assessment Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>assessment/assessments/<?php echo $cat_id;?>/<?php echo $cat_name;?>"><?php echo $cat_name;?></a>
                          
                       </li>
                                              
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget orange">
                         <div class="widget-title">
                             <h4><?php echo $cat_name;?></h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-orange">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     
                                 </ul>
                                 <div class="progress progress-success-orange progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                         
                                <div class="control-group">
                                    <label class="control-label">Select Student</label>
                                    <div class="controls">
                                        <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" id="student_id">
                                        <option value=""></option>
                                        <?php foreach($students as $student):?>
                                            <option value="<?php echo $student['UserID'];?>"><?php echo $student['Name'].' '.$student['Surname'];?></option>
                                        <?php endforeach;?>
                                       
                                    </select>
                                    </div>
                                </div>
                                
                                        
                                         
                                        
                                     </div>
                                     
                                  <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                          <div class="control-group">
                                            <label class="control-label">Select Action</label>
                                            <div class="controls">
                                                <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" id="assessmenttype">
                                                <option value=""></option>
                                                <option value="current">Current Assessments</option>
                                                <option value="previous">Previous Assessments</option>

                                            </select>
                                            </div>
                                </div>
                                         
                                         <div class="control-group" style="display: none;" id="assignmentcontrol">
                                             
                                           
                                        </div>
                                        
                                     </div>
                                  
                                  
                                  <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                                                                  
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first orange"><a href="javascript:;">First</a></li>
                                         <li class="previous orange"><a href="javascript:;">Previous</a></li>
                                         <li class="next last orange"><a href="javascript:;">Last</a></li>
                                         <li class="next orange"><a  href="javascript:;" onclick="nextscreen();" >Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   
    <!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   
   
   <!-- BEGIN FOOTER -->
   <div id="footer">
       UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>
   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <!--<script src="<?php echo SITEURLM?>js/form-wizard.js"></script>-->
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   
   <script>
       var Script = function () {

    $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        
        //return false;
        if(index==2){
            return false;
        }
        console.log(index);
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(tab);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
    }});

}();
       
       
       $('#student_id').change(function(){
           $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>assessment/diagnostic_screener_session",
                data: { student_id: $('#student_id').val() }
              })
                .done(function( msg ) {
//                  alert( "Data Saved: " + msg );
                });
       });
       
       $('#assessmenttype').change(function(){
       if ($('#assessmenttype').val()=='current'){
           $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>assessment/getassignments",
                data: { student_id: $('#student_id').val(),cat_id:<?php echo $cat_id;?> }
              })
                .done(function( msg ) {
                    $.ajax({
                type: "POST",
                url: "<?php echo SITEURLM?>estrellita/index.php?module=start_quiz&id='+quiz_id+'&assessment_name=<?php echo $cat_name;?>"
              })
                      .done(function(msg1){
                          $('#assignmentcontrol').html(msg);
                        $('#assignmentcontrol').show();
                      });
                    
                    //console.log(msg);
//                  alert( "Data Saved: " + msg );
                    

                });
       }
       });
       
            function nextscreen (){
                if($('#assessmenttype').val()=='previous' && $('#student_id').val()!=''){
                    window.location = '<?php echo SITEURLM?>estrellita/index.php?module=old_assignments&mid=8&assessment_name=<?php echo $cat_name;?>';
                }
            }
            
            function start_test(quiz_id){
                window.location = '<?php echo SITEURLM?>estrellita/index.php?module=start_quiz&id='+quiz_id+'&assessment_name=<?php echo $cat_name;?>';
            }
            
            function reset_assignment(user_quiz_id){
                $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>assessment/resetassignment",
                data: { user_quizzes_id: user_quiz_id,studentId:$('#student_id').val()}
              })
                .done(function( msg ) {
                    if(msg=='DONE'){
                        $('#successbtn').modal('show');
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>assessment/getassignments",
                            data: { student_id: $('#student_id').val(),cat_id:<?php echo $cat_id;?> }
                          })
                            .done(function( msg ) {
                                $('#assignmentcontrol').html(msg);
                                $('#assignmentcontrol').show();
                                //console.log(msg);
            //                  alert( "Data Saved: " + msg );
                            });
                            }else 
                        $('#errorbtn').modal('show');
                });
            } 
        $(document).ready(function(){
   
   if(window.location.hash.substr(1)=='pills-tab2') {
      // # exists in URL
//      alert('test');
      $("#pills").bootstrapWizard("show",1)
    } 
    
});
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>