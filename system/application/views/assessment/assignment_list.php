<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */?>

<table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th >Quiz Name</th>
                                <th >Status</th>
                                <th class="no-sorting">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($assignments as $key=>$val){?>
                                <tr class="odd gradeX" id="row_<?php echo $val->id;?>">                                
                                    
                                    <td id="tdfirstname"><?php echo $val->quiz_name;?></td>
                                    <td id="tdfirstname"><?php if ($val->status>=2) echo "Completed"; else echo 'Pending';?></td>
                                    <!--<a href="javascript:void(0);" onclick="nextscreen(<?php //echo $val->assid;?>);">start</a>-->
                                <td class="hidden-phone">
                                <?php if ($val->status>=2){?>
                                    <a href="javascript:void(0);" role="button" class="btn btn-primary" onclick="$('#errorbtn').modal('show');"><i class="icon-pencil"></i></a>
                                <?php } else {?>
                                    <a href="javascript:void(0);" role="button" class="btn btn-primary" onclick="start_test(<?php echo $val->assid;?>);"><i class="icon-pencil"></i></a>
                                <?php }?>
                                    
                                    <a href="javascript:void(0);" role="button" class="btn btn-danger" onclick="reset_assignment(<?php echo $val->user_quizzes_id;?>);" ><i class="icon-refresh"></i></a>
                            </tr>
                            <?php }?>   
                            
                            </tbody>
                        </table>

