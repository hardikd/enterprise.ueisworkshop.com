<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Assessment Tests::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/school.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/countries1.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
        <table align="center" cellpadding="5" >
		<tr><td colspan="5" align="left"> <b> Assessment Data </b> </td></tr>
		<tr class="tchead">
		<td>Assignment Name</td> <td>Added Date</td><td>User</td><td>Quiz Name</td><td>Quiz Desc</td><td>Pass Score</td></td>
		</tr>
		<?Php
		//echo '<pre>';
		//print_r($getAllAssData);
		foreach($getAllAssData as $key =>$val)
		{
		?>
		<tr class="tcrow2">
            <td><?Php echo $getAllAssData[$key]['assignment_name'];?></td>
            <td><?Php echo $getAllAssData[$key]['added_date']; ?></td>
            <td><?Php echo $getAllAssData[$key]['Name'];?></td>
            <td><?Php echo $getAllAssData[$key]['quiz_name'];?></td>
            <td><?Php echo $getAllAssData[$key]['quiz_desc'];?></td>
            <td><?Php echo $getAllAssData[$key]['pass_score'];?></td>
            
		
		</tr>
			<?Php
		}
		?>
		</table>
		
        
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
