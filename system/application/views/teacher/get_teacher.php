<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::schools::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script>
    
    function states_select(id)
{
	var g_url='countries/getStates/'+id;
    $.getJSON(g_url,function(result)
	{
//	var str='<select class="combobox" name="states" id="states" onchange="district_all(this.value)">';
        var str='';
	if(result.states!=false)
	{
//        str+='<option value="0">Select State</option>';
	$.each(result.states, function(index, value) {
	str+='<option value="'+value['state_id']+'">'+value['name']+'</option>';
	
	if(index==0)
	{
		district_all(value['state_id']);
		
	}
	});
//	str+='</select>';
	
	
    }
	else
	{
     var str='';
	 str+='<option value="0">No States Found</option>';
//     var sstr='';
//	 sstr+='<option value="empty">No Districts Found</option>';
//	  $('#district').html(sstr); 
//	   var sstr='';
//	 sstr+='<option value="empty">No Schools Found</option>';
//	  $('#school').html(sstr); 
	}
     $('#states').html(str); 
     sstr='<option value="empty">Select District</option>';
      $('#district').html(sstr);
      sstr='<option value="empty">Select School</option>';
	  $('#school').html(sstr); 
	 
	 
	 
	 
	 });

}

function district_all(id)
{

	var s_url='district/getDistrictsByStateId/'+id;
    $.getJSON(s_url,function(sresult)
	{
//		var sstr='<select class="combobox" name="district" id="district" onchange="school_type(this.value)">';
                var sstr= '';
                var sstr1 = '';
		if(sresult.district!=false)
	{
	//sstr+='<option value="all">All</option>';
	$.each(sresult.district, function(index, value) {
	sstr+='<option value="'+value['district_id']+'">'+value['districts_name']+'</option>';
	if(index==0)
	{
		school_type(value['district_id']);
		
	}
	});
//	sstr+='</select>';
	
	}
	else
	{
     sstr+='<option value="empty">No Districts Found</option></select>';
//	  var sstr1='<select class="combobox" name="school" id="school">';
	 sstr1+='<option value="empty">No Schools Found</option>';
	  $('#school').html(sstr1); 
	  
	}
//        alert(sstr);
     $('#district').html(sstr); 
	});


}

function school_type(id)
{

	var s_url='school/getschoolbydistrict/'+id;
    $.getJSON(s_url,function(sresult)
	{
		var sstr='<option>Select School</option>';
		if(sresult.school!=false)
	{
	$.each(sresult.school, function(index, value) {
	sstr+='<option value="'+value['school_id']+'">'+value['school_name']+'</option>';
	
	});
//	sstr+='</select>';
	
	}
	else
	{
          sstr+='<option value="">No School found</option>';
//     sstr+='</select>';
	}
     $('#school').html(sstr); 
	});



}

</script>
	  
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/pleftmenu.php'); ?>
        <div class="content">
         <form method="post" action="<?php echo base_url().'teacher/retrieve_teacher';?>" id="teacher_get_form" name="teacher_get_form">
        <table align="center" >
			<tr>
		<td >
		Countries:
		</td>
		<td>
		<select class="combobox" name="countries" id="countries" onchange="states_select(this.value)" >
		<?php if(!empty($countries)) { 
		foreach($countries as $val)
		{
		?>
		<option value="<?php echo $val['id'];?>"  ><?php echo $val['country'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td >
		States:
		</td>
		<td>
		<select class="combobox" name="states" id="states" onchange="district_all(this.value)" >
		<?php if($states!=false) { ?>
		<?php foreach($states as $val)
		{
		?>
		<option value="<?php echo $val['state_id'];?>"  ><?php echo $val['name'];?></option>
		<?php } } else { ?>
		<option value="0">No States Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		</td>
		</tr>
        
		<tr>
		<td>
		Districts:
		</td>
		<td>
		<select class="combobox" name="district" id="district" onchange="school_type(this.value)">
		
		<?php if($district!=false) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['district_id'];?>" <?php if(isset($district_id) && $district_id==$val['district_id'] ) {?> selected <?php  } ?> ><?php echo $val['districts_name'];?></option>
		<?php } } else { ?>
		<option value="">No Districts Found</option>
		<?php } ?>
		</select>
		</td>
		<td>
		School:
		</td>
		<td>
  <select class="combobox" name="school_id" id="school" >
		<option value="">Select School</option>
		<?php if(!empty($school)) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>" ><?php echo $val['school_name'];?></option>
		<?php } } ?>
		</select>
		</td>
		<td>
		
		</td>
		</tr>
        <tr>
		<td>
		</td>
		<td>
			<input type="submit" class="btnsmall" name="submit"  value="submit" onclick="this.form.target='_blank';return true;">
		</td>
		</tr>
		</table>
		</form>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
