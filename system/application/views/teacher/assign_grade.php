<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style>
.icon-reorder {display:none !important;}
.sidebar-toggle-box {background:#5e3364 !important;}
.popoverinfo {font-size:18px !important;}
.infoicon {  position: absolute;
  margin-left: 205px;
  margin-top: 10px;
width:60%;}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid sidebar-closed">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll" style="display:none;">
        <div id="sidebar" class="nav-collapse collapse">

         
       <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                     <i class="icon-book"></i>&nbsp; Create Workshop Account
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <form class="form-horizontal" action="<?php echo base_url();?>teacherplan/assign_grades" name="createplanstep1" id="createplanstep1" method="post">
                         <input type="hidden" name="plan_type" value="<?php echo $type;?>" />
                     <div class="widget green">
                         <div class="widget-title">
                             <h4><?php echo $type;?> Assign Grades & Class Scheduling</h4>
                          
                         </div>
                         <div class="widget-body">
<!--                            <form class="form-horizontal" action="#">-->
                                <div id="pills" class="custom-wizard-pills-green4">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab" id="pillsstep2">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab" id="pillsstep3">Step 3</a></li>
                                 </ul>
                                 <div class="progress progress-success progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                  
                                  
                                     <div class="tab-pane" id="pills-tab1">
                                      
                                      <div class="infoicon"><a id="popoverData" class="popoverinfo" href="#" data-content="To begin using Workshop you will need to choose grades create schedules." rel="popover" data-placement="bottom" data-original-title="Student Registration" data-trigger="hover"><i class="icon-info-sign"></i></a></div>
                                         <h3 style="color:#000000;">Assign Grade(s)</h3>
                                                   <div class="control-group">
                                     </div>
    
                                         <div class="control-group" id="subject_name_group">
                                            <label class="control-label">Select Grade(s)</label>

                                             <div class="controls">
                                             	<select required class="span6 chzn-select" id='grade' name='grade[]' data-placeholder="--Please Select--" tabindex="1" style="width: 450px;" multiple>
												<option value=''>-Please Select-</option>
													<?php if($grades!=false)
													{
													foreach($grades as $gradeval)
													{
													?>
												<option value='<?php echo $gradeval['grade_id'];?>'><?php echo $gradeval['grade_name'];?></option>
													<?php
													}
													}
												?>
												</select>
                                            </div>
                                        </div>
                                        <div style="text-align:center;">
                                            <!-- <button class="btn btn-large btn-green" type="submit" name="add_another" value="add_another"><i class="icon-save icon-white"></i> Save & Add Another</button>-->
                                             <button class="btn btn-large btn-green" type="submit" name="continue" value="continue"><i class="icon-save icon-white"></i> Save & Continue</button>
                                        </div>
                                     </div>

                                     </div>
                                     </div>
                                    
                                     <!--<ul class="pager wizard">
                                         <li class="previous first green"><a href="javascript:;">First</a></li>
                                         <li class="previous green"><a href="javascript:;">Previous</a></li>
                                         <li class="next last green"><a href="javascript:;">Last</a></li>
                                         <li class="next green"><a  href="javascript:void(0);" onClick="$('#createplanstep1').submit();">Next</a></li>
                                     </ul>-->
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
<!--      </div>
       END PAGE   
   </div>-->
<!--notification -->
   <div id="successbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-ok-circle"></i> &nbsp;&nbsp; Successfully Updated.</h3>
                                </div>
                              
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-success">OK</button>
                                </div>
                                
                                 <!-- END POP UP CODE-->
                            </div>
   
   <!-- BEGIN POP UP CODE -->
                                            
                                            <div id="errorbtn" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                <div class="modal-header" style="background:#DE577B; color:#FFFFFF;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel2"><i class="icon-warning-sign"></i> &nbsp;&nbsp; Error. Please Try Again.</h3>
                                </div>
                                                <div id="errmsg"></div>
                                <div class="modal-footer" style="text-align:center;">
                                    <button data-dismiss="modal" class="btn btn-red">OK</button>
                                </div>
                                
                                 
                            </div>
                            <!-- END POP UP CODE-->
   <!-- notification ends -->
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   <script type="text/javascript" src="<?php echo SITEURLM?>assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.validate.js"></script>

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>

   <!-- END JAVASCRIPTS --> 
    <!--start old script -->
   <script src="<?php echo SITEURLM.$view_path; ?>js/report.js" type="text/javascript"></script>
   
<script type="text/javascript">
$("#createplanstep1").validate();
$('#popoverData').popover({
   placement: 'right'
});
$('#popoverData1').popover({
   placement: 'right'
});

function school_change(id)
{
	doit();
	var g_url='teacher/getTeachersBySchool/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="teacher_id" id="teacher_id" onchange="doit()" ><option value="">-Select-</option>';
	if(result.teacher!=false)
	{
	$.each(result.teacher, function(index, value) {
	str+='<option value="'+value['teacher_id']+'">'+value['firstname']+' '+value['lastname']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Teachers Found</option></select>';
	  
	}
     $('#teacher_id').replaceWith(str); 
	 
	 
	 
	 
	 });
	 
	 var g_url='observer/getobserverByschoolId/'+id;
    $.getJSON(g_url,function(result)
	{
	var str='<select class="combobox1" name="observer_id" id="observer_id" ><option value="">-Select-</option>';
	if(result.observer!=false)
	{
	$.each(result.observer, function(index, value) {
	str+='<option value="'+value['observer_id']+'">'+value['observer_name']+'</option>';
	
	
	});
	str+='</select>';
	
	
    }
	else
	{
      str+='<option value="">No Observers Found</option></select>';
	  
	}
     $('#observer_id').replaceWith(str); 
	 
	 
	 
	 
	 });

}
</script>
   <!--end old script -->
   
   <script>
   $(document).ready( function() { 
       
        $('#pillsstep2').click(function(){
           if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#timepicker1').val()==''){
               alert('Please enter date');
               return false;
           }
           $('#createplanstep1').submit();
       });
         $(document).ready( function() { 
        
//        alert('<?php echo $this->session->flashdata('message');?>');
        
        <?php if ($this->session->flashdata('error')){?>
            $('#errmsg').html('<?php echo $this->session->flashdata('error');?>');
            $('#errorbtn').modal('show');
            <?php }?>
        <?php if ($this->session->flashdata('message')){?>
            $('#successbtn').modal('show');
            <?php }?>
        <?php if($this->session->flashdata('link')){?>
            window.open("<?php echo $this->session->flashdata('link');?>");
        <?php }?>
    });
        $('#pillsstep3').click(function(){
           if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#timepicker1').val()==''){
               alert('Please enter date');
               return false;
           }
           $('#createplanstep1').submit();
       });
       
        $('#pillsstep3').click(function(){
        if($('#dp1').val()==''){
               alert('Please enter date');
               return false;
           } 
           if ($('#timepicker1').val()==''){
               alert('Please enter date');
               return false;
           }   
        $('#createplanstep1').submit();
       });
       
        $("select").change(function(){
            var divId = $(this).find(":selected").attr("data-div-id");
            $(".standard-topic").hide();
            $("#" + divId).show();
			
        });
        
        <?php if($this->session->userdata('login_type')=='observer'){?>
        $('#dp1').on('changeDate',function(){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_lesson_plan",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val()},
              success: function(result)
              {
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
        });
        <?php }else { ?>
        $('#dp1').on('changeDate',function(){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_lesson_plan",
              data: "cdate="+$('#dp1').val(),
              success: function(result)
              {
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
        });
        <?php }?>
    
    $('#dp1').on('changeDate',function(){
//        console.log('in changeDate');
        if($('#dp1').val()!='' && $('#timepicker101').val()!=''){
           $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_subject",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val(),starttime:$('#timepicker101').val()},
              success: function(result)
              {
                  console.log(result);
                  if(result!=''){
                      $('#subject_name').html('');
                      $('#subject_name_group').show();
                      $('#subject_name').html(result);
                  }
//                alert('done');  
//                $('#lesson_plans').html(result);
                //do something
              }
         });  
         }
        });
    
    
    });
    
    
    $(function(){
    $('.timepicker101').timepicker().change(function(){
        $('.timepicker101').val($(this).val());
        if($('#dp1').val()!='' && $('#timepicker101').val()!=''){
          
        $.ajax
        ({
              type: "Post",
              url: "<?php echo base_url();?>planningmanager/check_subject",
              data: {cdate:$('#dp1').val(),teacher_id: $('#teacher_id').val(),starttime:$(this).val()},
              success: function(result)
              {
                  if(result!=''){
                      $('#subject_name').html(' ');
                      $('#subject_name_group').show();
                      $('#subject_name').html(result);
                  } else {
                      $('#subject_name').html(' ');
                      $('#subject_name_group').show();
                      $('#subject_name').html('Not Assigned');
                  }
              }
         });
         }
});

});
    
	</script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
</body>
<!-- END BODY -->
</html>