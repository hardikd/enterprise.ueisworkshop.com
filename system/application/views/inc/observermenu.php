<script type="text/javascript">
function abcob()
{
	$("ul #submenuob > li").hide();
	$("ul #submenuob > #performanceassessob").hide();
	$("ul #submenuob ").hide();
	
}
function showassessob()
{

	$("#submenuob > li").show();
	$("ul #submenuob > #performanceassessob").show();
	$("ul #submenuob ").show();
}	

function demoob()
{
	$("ul #submenuob > li").hide();
	$("ul #submenuob > #performanceassessob").hide();
	$("ul #submenuob ").hide();
}
function showassessobb()
{
	$("#submenuobb > li").show();
	$("ul #submenuobb > #performanceassessobb").show();
	$("ul #submenuobb ").show();
}
function abcob2()
{
	$("ul #submenuob2 > li").hide();
	$("ul #submenuob2 > #performanceassessob2").hide();
	$("ul #submenuob2 ").hide();
	
}
function showassessob2()
{
	$("#submenuob2 > li").show();
	$("ul #submenuob2 > #performanceassessob2").show();
	$("ul #submenuob2 ").show();
}	
function demoob2()
{
	$("ul #submenuob2 > li").hide();
	$("ul #submenuob2 > #performanceassessob2").hide();
	$("ul #submenuob2 ").hide();
}
</script>
<div class="lft">
  <div class="leftmtop_main"></div>
  <div class="leftmenu"  >
    <div id="navigation">
      <ul>
        <li>
          <div class="hdtxtblank_c"><a>Implementation</a></div>
          <ul>
            <div class="leftmtop"></div>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/index/','Conduct Teacher Observation')" >Conduct Teacher Observation</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/leadershipcreateself/','Conduct Leadership Self Assessment')" >Conduct Leadership Self Assessment</a></div>
            </li>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/summativereport/','Conduct Summative Report')" >Conduct Summative Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/observer_feature','Behavior & Learning Running Record')" >Behavior & Learning Running Record</a></div>
            </li>
            <li>
              <div class="leftmbot"></div>
            </li>
          </ul>
        </li>
        <div class="imgImpl"></div>
      </ul>
    </div>
    <div id="navigation">
      <ul>
        <li style="width:87px !important;">
          <div class="hdtxtblank_c" style="width:156px !important;"><a onmouseover="demoob()" onclick="abcob();" >Data Analysis</a></div>
          <ul>
            <div class="leftmtop"></div>
            <?php if($this->session->userdata('login_type')=='user') { ?>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/dashboard/','Dashboard')"   >Dashboard</a></div>
            </li>
            <?php } ?>
            <li>
              <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/teacherreport/','Teacher Report')"   >Teacher Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a     style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/observerreport/','Observer Report')"  >Observer Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/Schoolreport/','School Report')" >School Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/gradereport/','Grade Report')"    >Grade Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/leadershipreport/','Leadership Report')"    >Leadership Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/summative/','Summative Report')"    >Summative Report</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_observation/','Observation Count')"   <?php  } else { ?>       style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_observation/','Observation Count')"          <?php  }  ?>   >Observation Count</a></div>
            </li>
            <?php if($this->session->userdata('login_type')=='observer') { ?>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/selfreport/','Teacher Self Reflection')"    >Teacher Self Reflection</a></div>
            </li>
            <?php } ?>
            <li>
              <div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_notification/','Notification Activity Count')"   <?php  } else { ?>      style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_notification/','Notification Activity Count')"          <?php  }  ?>   >Notification Activity Count</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_professional/','Professional Development Activity')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_professional/','Professional Development Activity')"          <?php  }  ?>   >Professional Development Activity</a></div>
            </li>
            <?php if($this->session->userdata('login_type')=='observer') { ?>
            <li>
              <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getstudentreport/','Behavior & Learning Running Record')"    >Behavior & Learning Running Record</a></div>
            </li>
            <li>
              <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getclassworkstudentreport/','Classwork Performance')"    >Classwork Performance</a></div>
            </li>
            <li>
              <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/gethomeworkstudentreport/','Homework')"    >Homework</a></div>
            </li>
            <li>
              <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/geteldstudentreport/','English Language Development')"    >English Language Development</a></div>
            </li>
            
              
            
             <li>
              <div class="lnktxt"><a onmouseover="showassessob();"   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>      >Performance Assessments</a></div>
            </li>
            
            
            <ul id="submenuob" style="left: 167px; top: 292px !important;">
          <div class="leftmtop" id="performanceassessob"></div>
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','allschoolgraph','Performance Assessments >> All School Comparisons')" >Compare All Schools
   </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','allschoolbygrade','Performance Assessments >> Comparison of All Schools by Grade')" >Compare All Schools by Grade Level
 </a></div>
          </li>-->
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','graphcontrollindividualschool','Performance Assessments >> Individual School')" >Compare A Grade Level by School 
</a></div>
          </li>-->
		  <!--<li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports by Standard')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Performance Assessments >> Single Assessment Reports by Standard
')"          <?php  }  ?>   >Single Assessment Reports by Standard</a></div>
          </li>
        <li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports by Standard')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Performance Assessments >>Multiple Assessment Reports by Standard
')"          <?php  }  ?>   >Multiple Assessment Reports by Standard
</a></div>
          </li>-->
          <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictreport','Performance Assessments >> Grade Level Report')" style="cursor:pointer;">Grade Level Report</a> </div>
          </li>
		  <li>  
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','classroomgrowthgraph','Performance Assessments >>View a Classrooms Growth')" style="cursor:pointer;">View a Classrooms Growth
</a></div>
          </li>	
		  
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','graphcontroll','Performance Assessments >> Classroom Graph')" >Compare A Classrooms Growth 
 </a></div>
          </li>-->  
 <!--<li>
            <div class="lnktxt">
			
			<a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','classroomteacherreport','Performance Assessments >> Assessment Completion Roster')" >Assessment Completion Roster</a> 
			
			</div>
          </li>-->
         	  
        
         <?php   if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		 ?>
          <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/index.php/schooldistrictreport" >Performance Score by Standard</a> </div>
          </li>
          <?php 
		  } else { ?>
          
           <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictreport','Performance Assessments >> Performance Score by Standard')" style="cursor:pointer;">Performance Score by Standard</a> </div>
          </li>	
          <?php }         
           if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		 ?>
          <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/index.php/schooldistrictgraph" >Performance Graphs by standard</a> </div>
          </li>
         	 
          <?php }else {?>
         <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictgraph','Performance Assessments >> Performance Graphs by Standard')" style="cursor:pointer;">Performance Graphs by Standard</a> </div>
          </li>	
          <?php 
		  } ?>	
			 	<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Performance Assessments >> Individual Student Performance Scores')" >Individual Student Performance Scores</a></div>
          </li>	
 <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','selectedstudentreport','Performance Assessments >> Individual Student Performance by Standard')" >Individual Student Performance by Standard</a> </div>
          </li>			  
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
            
            <?php } ?>
            <li>
              <div class="leftmbot"></div>
            </li>
          </ul>
        </li>
        <div class="imgReport"></div>
      </ul>
    </div>
    <?php if($this->session->userdata('login_type')=='observer') { ?>
    
    <!--Report splash page starts here--> 
    
    <!--<div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Data Analysis Resources</a></div>
        <ul>
          <div class="leftmtop"></div>
      
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assesstest','Assessment Documents')" >Assessment Documents </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assessscoring','Assessment Scoring Guides')" >Assessment Scoring Guides</a></div>
          </li>
		   <li>
    
		   <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assessmentcalendar','Instructional Calendar')" >Instructional Calendars
  </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','trainingmaterial','Training Materials Manager')" >Training Materials Manager</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgReport"></div>
    </ul>
  </div>--> 
    
    <!--Report splash page ends here-->
    
    <?php 
		} 
		?>
    <div id="navigation">
      <ul>
        <li>
          <div class="hdtxtblank_c"><a>Planning</a></div>
          <ul>
            <div class="leftmtop"></div>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','observerview/schedule','Calendared Activities')" >Calendared Activities</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','observerview/comments','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div>
            </li>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer; <?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','observerview/lessoncomments','Lesson Plan Book')" >Lesson Plan Book </a> </div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','index/observerindex','Notifications')" >Notifications</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer; <?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','observerview/answer','Observation Plans')" >Observation Plans </a> </div>
            </li>
            <li>
              <div class="leftmbot"></div>
            </li>
          </ul>
        </li>
        <div class="imgPlan"></div>
      </ul>
    </div>
    <div id="navigation">
      <ul>
        <li>
          <div class="hdtxtblank_c"><a>Communications</a></div>
          <ul>
            <div class="leftmtop"></div>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/memorandums','Create Memorandum')" >Create Memorandum</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/assign','Memorandums in Progress')" >Memorandums in Progress</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/assigned','Memorandum Records')" >Memorandum Records</a></div>
            </li>
            <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/index','Staff Communication')" >Staff Communication</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/staff_report','Staff Communication Records')" >Staff Communication Records</a></div></li>
            <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/parents','Parent Notification')" >Parent Notification</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/parent_report','Parent Notification Records')" >Parent Notification Records</a></div></li>
            <li>
              <div class="leftmbot"></div>
            </li>
          </ul>
        </li>
        <div class="imgComm"></div>
      </ul>
    </div>
    <div id="navigation">
      <ul>
        <li>
          <div class="hdtxtblank_c"><a>Capacity Building</a></div>
          <ul>
            <div class="leftmtop"></div>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','observerview/profdev','Individualized Professional Development')" >Individualized Professional Development</a></div>
            </li>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div>
            </li>
            <li>
              <div class="leftmbot"></div>
            </li>
          </ul>
        </li>
        <div class="imgCapBld"></div>
      </ul>
    </div>
    
    <!-- Testing for Navigation starts here -->
    <?php include(MEDIA_PATH.'inc/videomenu.php'); ?>
    <!-- Testing for Navigation ends here --> 
    
    <!-- Testing for Navigation starts here --> 
    <!--<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_o"><a>Off-Line</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt">GUI</div></li>
                    <li><div class="lnktxt">Navigation</div></li>
                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li><div class="imgOffline"></div>
            </ul>
   			</div>--> 
    <!-- Testing for Navigation ends here --> 
    
    <!-- Testing for Navigation starts here --> 
    <!--<div id="navigation" style="margin-bottom:-10px;">
	    	<ul>
            <li><div class="hdtxtblank_o"><a>On-Line</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt">GUI</div></li>
                    <li><div class="lnktxt">Demo</div></li>
                                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li>
            <div class="imgOnline"></div>
            </ul></div>-->
    <div class="leftmbot_main" style="height:5px;" id="navigation"></div>
    
    <!-- Testing for Navigation ends here --> 
    
  </div>
</div>