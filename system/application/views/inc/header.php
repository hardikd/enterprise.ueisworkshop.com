   <!-- BEGIN HEADER -->
   <div id="header" class="navbar navbar-inverse navbar-fixed-top">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class="navbar-inner">
           <div class="container-fluid">
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class="sidebar-toggle-box hidden-phone">
                   <div class="icon-reorder"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class="brand" href="<?php echo base_url();?>">
                   <img src="<?php echo SITEURLM?>img/logo.png" alt="UEIS Workshop">
               </a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="arrow"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               <div id="top_menu" class="nav notify-row">
                   <!-- BEGIN NOTIFICATION -->
                   <ul class="nav top-menu">
                   </ul></div>  
               <div class="top-nav ">
                   <ul class="nav pull-right top-menu">
                      <li><a href="#myModal-help-library" data-toggle="modal">Help Library</a></li>
                      <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </li>
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                               <i class="icon-user"></i> &nbsp;
                               <span class="username">
                               
                               		<!--Welcome, <b>John Doe</b> | <b>Level:</b> Classroom-->
                                    <?php if($this->session->userdata("login_type")=='user') {
		   echo "Welcome, <b>".$this->session->userdata("username").'</b>';
		   echo "&nbsp; | <b>Level:</b> District";

		   
		   if($this->session->userdata('product_login_type'))
		   {
		   echo "&nbsp; | <b>Level:</b> ".$this->session->userdata('product_login_type');
		   
		   }

		
		}
		else if($this->session->userdata("login_type")=='teacher') {
		   echo "Welcome, <b>".$this->session->userdata("teacher_name").'</b>';
		   echo "&nbsp; | <b>Level:</b> Classroom";

		if($this->session->userdata('product_login_type'))
		   {
		   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Login As: </b>".$this->session->userdata('product_login_type');
		   
		   }

		}
		else if($this->session->userdata("login_type")=='observer') {
		   echo "<b>Name: </b>".$this->session->userdata("observer_name");
		   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Type: </b>Campus";

		if($this->session->userdata('product_login_type'))
		   {
		   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Login As: </b>".$this->session->userdata('product_login_type');
		   
		   }

		}
		else if($this->session->userdata("login_type")=='parent') {
		   echo "<b>Name: </b>".$this->session->userdata("username");
		   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Type: </b>Parent";
		
		}
		else if($this->session->userdata("login_type")=='admin') {
		   echo "<b>Name: </b>admin";
		   echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Type: </b>Admin";
		
		}
		?>
                               
                               </span>
                               <b class="caret"></b>
                           </a>
                           <ul class="dropdown-menu extended logout">
                               <li><a href="<?php echo base_url(); ?>changepassword" ><i class="icon-key"></i> Change Password</a></li>
                               <li>		
							   
							   <?php if($this->session->userdata("loginas")=='parents')
		{
		?>
		<a href="<?php echo base_url(); ?>parentlogin/logout/"><i class="icon-unlock"></i>Logout</a>
		<?php
		}
		else if($this->session->userdata("login_type")=='admin')
		{
		?>
    	<a href="<?php echo base_url(); ?>admin/admin_logout/"><i class="icon-unlock"></i>Logout</a>
            
            <?php
		}
		else 
		{
		?>
		<a href="<?php echo base_url(); ?>admin/logout/"><i class="icon-unlock"></i>Logout</a>
		<?php
		}
		?>
        
        </a>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->

      <div id="myModal-help-library" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:60%;left:598px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Workshop Help Library</h3>
                                </div>
    <div class="help-model-tab">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs help-tab" role="tablist">
      <li class="active">
          <a href="#Planning" role="tab" data-toggle="tab">
              <i class="icon-book"></i> Planning
          </a>
      </li>
      <li><a href="#Attendance" role="tab" data-toggle="tab">
          <i class="icon-calendar"></i> Attendance
          </a>
      </li>
      <li>
          <a href="#Implementation" role="tab" data-toggle="tab">
              <i class="icon-pencil"></i> Implementation
          </a>
      </li>
      <li>
          <a href="#Classroom" role="tab" data-toggle="tab">
              <i class="icon-group"></i> Classroom
          </a>
      </li>
      <li>
          <a href="#Assessment" role="tab" data-toggle="tab">
              <i class="icon-bar-chart"></i> Assessment
          </a>
      </li>
      <li>
          <a href="#tools" role="tab" data-toggle="tab">
              <i class="icon-wrench"></i> Data Tracker
          </a>
      </li>
    </ul>
    <div style="font-size:16px;text-align:center;">
      Click on the menu options above to view helpful resources and then click the desire icon below
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade active in" id="Planning">
  <table class="helptable">
    <thead>
        <th>Module</th>
        <th>Feature</th>
        <th colspan="2">Video Tutorials</th>
        <th colspan="2">Tip Sheets</th>
        <th>Reports</th>
    </thead>
    <tr class="subheading">
        <td></td>
        <td></td>
        <td>Create</td>
        <td>Retrieve</td>
        <td>Create</td>
        <td>Retrieve</td>
        <td></td>
    </tr>
    <tr>
        <td>Planning</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Lesson Plan</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Lesson PlanningConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Lesson PlanningConverted/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/PLANNING MANAGER/Lesson Plan Module/Plan A Lesson/Create A Lesson/Create a Lesson Plan - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/PLANNING MANAGER/Lesson Plan Module/Retrieve A Lesson/Retrieve A Lesson/Retrieve a Lesson Plan - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Planning/Language Arts Lesson Plan 1st Grade.pdf" ><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>SMART Goals</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/PLANNING MANAGER/SMART Goal Module/Create SMART Goal/Create SMART Goal.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/PLANNING MANAGER/SMART Goal Module/Retrieve SMART Goal/Retrieve SMART Goal.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Planning/SMART Goal.pdf" ><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Calendar</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a>--></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/PLANNING MANAGER/Calendar Module/Create Calendar/Calendar Events .ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/PLANNING MANAGER/Calendar Module/Retrieve Calendar/Retrieve Calendar Event Details.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
</table>
      </div>
      <div class="tab-pane fade" id="Attendance">
          <table class="helptable">
    <thead>
        <th>Module</th>
        <th>Feature</th>
        <th colspan="2">Video Tutorials</th>
        <th colspan="2">Tip Sheets</th>
        <th>Reports</th>
    </thead>
    <tr class="subheading">
        <td></td>
        <td></td>
        <td>Create</td>
        <td>Retrieve</td>
        <td>Create</td>
        <td>Retrieve</td>
        <td></td>
    </tr>
    <tr>
        <td>Attendance</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Daily Attendance</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Taking AttendanceConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Retrieving attendanceConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/ATTENDANCE MANAGER/Attendance Manager/Take Attendance/Taking Attendance.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/ATTENDANCE MANAGER/Attendance Manager/Take Attendance/Taking Attendance.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Attendance/Detailed Report.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Student Enrollment</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/wX9jyBPXX2A/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/wX9jyBPXX2A/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/ATTENDANCE MANAGER/Attendance Manager/Enroll A Student/Enroll Student - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/ATTENDANCE MANAGER/Attendance Manager/Enroll A Student/Enroll Student - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Student Roster</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/-ZW33lBK44E/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/-ZW33lBK44E/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/ATTENDANCE MANAGER/Attendance Manager/Adding Students to Roster - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/ATTENDANCE MANAGER/Attendance Manager/Adding Students to Roster - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Create Teachers</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/WGt_FNyUYsQ/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/WGt_FNyUYsQ/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Create a Classroom</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Assign Teachers to Grades</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a>--></td>
    </tr>
    <tr>
        <td></td>
        <td>Create School Periods</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/KATQ_zyLjKc/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/KATQ_zyLjKc/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a>--></td>
    </tr>
    <tr>
        <td></td>
        <td>Master Scheduling</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/ya6A7ILdguQ/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/ya6A7ILdguQ/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a>--></td>
    </tr>
    <tr>
        <td></td>
        <td>Individual Students</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
</table>
      </div>
      <div class="tab-pane fade" id="Implementation">
          <table class="helptable">
    <thead>
        <th>Module</th>
        <th>Feature</th>
        <th colspan="2">Video Tutorials</th>
        <th colspan="2">Tip Sheets</th>
        <th>Reports</th>
    </thead>
    <tr class="subheading">
        <td></td>
        <td></td>
        <td>Create</td>
        <td>Retrieve</td>
        <td>Create</td>
        <td>Retrieve</td>
        <td></td>
    </tr>
    <tr>
        <td>Implementation</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Grade Tracker</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Grade TrackerConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Grade TrackerConverted/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/Grade Tracker/Grade Tracker Entry/Entering Grades Job Aide.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/Grade Tracker Retrieval/Retrieving Grades Job Aide.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Grade Tracker_ Academic.pdf" ><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Homework</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/dtfor0doGMI/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/dtfor0doGMI/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/Entering Homework Scores/Entering Homework Scores Advanced Feature.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/Retrieving Homework Scores/Homework Retrieval Job Aide.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Homework.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>IEP Tracker</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
    <tr>
        <td></td>
        <td>ELD Tracker</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/ELD Implementation/Entering ELD Scores/Entering ELD Scores - 111814.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/ELD Implementation/Retrieving ELD Scores/Retrieving ELD Scores Job Aide.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Implementation/ELD.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Instructional Efficacy</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/3ZYjhWoqvws/youtube"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/3ZYjhWoqvws/youtube"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/Instructional Efficacy/Creating An Instructional Efficacy Report/Creating an Instructional Efficacy Report .pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/IMPLEMENTATION MANAGER/Instructional Efficacy/Retrieving An Instructional Efficacy Report/Retrieving An Instructional Efficacy Report Job Aide.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
</table>
      </div>
      <div class="tab-pane fade" id="Classroom">
          <table class="helptable">
    <thead>
        <th>Module</th>
        <th>Feature</th>
        <th colspan="2">Video Tutorials</th>
        <th colspan="2">Tip Sheets</th>
        <th>Reports</th>
    </thead>
    <tr class="subheading">
        <td></td>
        <td></td>
        <td>Create</td>
        <td>Retrieve</td>
        <td>Create</td>
        <td>Retrieve</td>
        <td></td>
    </tr>
    <tr>
        <td>Classroom</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Progress Manager</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/>--></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Student Success Team</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/SST Video DemonstrationConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/SST Video DemonstrationConverted/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/SST Manager/Create an Student Success Team (SST) Report/Create a Student Success Team Report.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/SST Manager/Retrieve an Student Success Team (SST) Report/Retrieve a List of SST Reports.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Classroom_Management/SST Report.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Parent/ Teacher Conf.</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Parent Teacher ConferenceConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Parent Teacher ConferenceConverted/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/Parent Teacher Conference/Create a Parent Teacher Conference Report/Create a Parent Teacher Conference.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/Parent Teacher Conference/Retrieve A Parent Teacher Conference Report/Retrieve a Parent Teacher Conference Report.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/parent_teacher.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Student/ Teacher Conf.</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/Teacher Student Conference/Document a Teacher Student Conference/Documenting a Teacher Student Conference.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/Teacher Student Conference/Retrieve a Student Teacher Conference/Retrieve a Teacher Student Conference Job Aide.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Classroom_Management/Teacher Student Conference.pdf" ><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Behavior Running Record</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Behavior Running RecordConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Behavior Running RecordConverted/"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/Behavior Running Record/Create a Behavior Report/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/CLASSROOM MANAGER/Behavior Running Record/Create a Behavior Report/Behavior by Student/Behavior Retrieval by Student .ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Classroom_Management/Behavior Running Record_Incident Report.pdf" ><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
</table>
      </div>
      <div class="tab-pane fade" id="Assessment">
          <table class="helptable">
    <thead>
        <th>Module</th>
        <th>Feature</th>
        <th colspan="2">Video Tutorials</th>
        <th colspan="2">Tip Sheets</th>
        <th>Reports</th>
    </thead>
    <tr class="subheading">
        <td></td>
        <td></td>
        <td>Create</td>
        <td>Retrieve</td>
        <td>Create</td>
        <td>Retrieve</td>
        <td></td>
    </tr>
    <tr>
        <td>Assessment</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Assessment - Administration</td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Estrellita AssessmentConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Class Growth</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Assessment/View a classrooms growth.png"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Class Performance PDF</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Class Assessment SummaryConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Assessment/Class Performance Report.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Classroom Performance Graph</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Assessment/Class Performance Graph.png"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Individual Student Summary</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Student Summary ReportConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Assessment/Student Summary Report.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Individual Student Performance</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Student Summary ReportConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Assessment/Student Performance Exit.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Student Progress Report</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo base_url();?>index/video_player/Assessment Progress ReportConverted/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><!--<img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/>--></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>reports/Assessment/Progress Report Exit Assessment.pdf"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
</table>
      </div>
      <div class="tab-pane fade" id="tools">
          <table class="helptable">
    <thead>
        <th>Module</th>
        <th>Feature</th>
        <th colspan="2">Video Tutorials</th>
        <th colspan="2">Tip Sheets</th>
        <th>Reports</th>
    </thead>
    <tr class="subheading">
        <td></td>
        <td></td>
        <td>Create</td>
        <td>Retrieve</td>
        <td>Create</td>
        <td>Retrieve</td>
        <td></td>
    </tr>
    <tr>
        <td>Data Tracker</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
 
    <tr>
        <td></td>
        <td>Planning</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/TOOLS & RESOURCES/Data Tracker/Planning Manager/Planning Manager/Retrieve Lesson Planning Report.ppt"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Attendance/ Classroom</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="<?php echo SITEURLM;?>Classroom Job Aides/TOOLS & RESOURCES/Data Tracker/Attendance Manager/Attendance Report/"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Attendance/ Student</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Implementation</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    <tr>
        <td></td>
        <td>Classroom</td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/video.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/document.png" style="width: 23px;height: 30px;"/></a></td>
        <td><a target="_blank" href="#"><img src="<?php echo SITEURLM;?>system/application/views/inc/logo/report.png" style="width: 23px;height: 30px;"/></a></td>
    </tr>
    
</table>
      </div>
    </div>
</div>
</div>



