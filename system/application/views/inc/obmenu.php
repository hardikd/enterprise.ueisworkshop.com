<script type="text/javascript">
$(document).ready(function(e) {
    $("#navigation ul li ul li div a.performancetest").mouseout(function(){
	
	$("#submenu > li").slideDown(100);
	$("ul #submenu > #performanceassess").slideDown(100);
	$("ul #submenu ").slideDown(100);
	
		});
 
	
});
</script>
<script type="text/javascript">
function abc()
{
	$("ul #submenu > li").hide();
	$("ul #submenu > #performanceassess").hide();
	$("ul #submenu ").hide();
	
}
function showassess()
{
	$("#submenu > li").slideDown(100);
	$("ul #submenu > #performanceassess").slideDown(100);
	$("ul #submenu ").slideDown(100);
}	
function demo()
{
	$("ul #submenu > li").hide();
	$("ul #submenu > #performanceassess").hide();
	$("ul #submenu ").hide();
}
/*function abcnew()
{
	$("ul #submenunew > li").hide();
	$("ul #submenunew > #performanceassessnew").hide();
	$("ul #submenunew ").hide();
	
}
function showassessnew()
{
	$("#submenunew > li").show();
	$("ul #submenunew > #performanceassessnew").show();
	$("ul #submenunew ").show();
}	
function demonew()
{
	$("ul #submenut > li").hide();
	$("ul #submenut > #performanceassesst").hide();
	$("ul #submenut ").hide();
}*/
function abct()
{
	$("ul #submenut > li").hide();
	$("ul #submenut > #performanceassesst").hide();
	$("ul #submenut ").hide();
	
}
function showassesst()
{
	$("#submenut > li").show();
	$("ul #submenut > #performanceassesst").show();
	$("ul #submenut ").show();
}	
function demot()
{
	$("ul #submenut > li").hide();
	$("ul #submenut > #performanceassesst").hide();
	$("ul #submenut ").hide();
}

</script>
<div class="lft">
  <div class="leftmtop_main"></div>
  <div class="leftmenu"  >
    <?php if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer') { ?>
    <div id="navigation">
      <ul>
        <li>
          <div class="hdtxtblank_c"><a>Implementation</a></div>
          <ul>
            <div class="leftmtop"></div>
            <li>
              <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/index/','Conduct Teacher Observation')" >Conduct Teacher Observation</a></div>
            </li>
            <?php } ?>
            <?php if($this->session->userdata('login_type')=='user') { ?>
            <li>
              <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Implementation','report/leadershipcreate','Conduct Leadership Assessment')" >Conduct Leadership Assessment</a></div>
              <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/SummativeReport','Conduct Summative Report')" >Conduct Summative Report</a></div>
              
         <!-- <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','addproficiency/index','Create Proficiency Assessment Ranges')" >Create Proficiency Assessment Ranges</a></div>-->
          
            </li>
            <li>
              <div class="leftmbot"></div>
            </li>
          </ul>
        </li>
        <div class="imgImpl"></div>
      </ul>
    </div>
    <?php } ?>
    <?php if($this->session->userdata('login_type')=='observer') { ?>
    <li>
      <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/leadershipcreateself/','Conduct Leadership Self Assessment')" >Conduct Leadership Self Assessment</a></div>
    </li>
    <li>
      <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/summativereport/','Conduct Summative Report')" >Conduct Summative Report</a></div>
    </li>
    <li>
      <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/observer_feature','Behavior & Learning Running Record')" >Behavior & Learning Running Record</a></div>
    </li>
    <li>
      <div class="leftmbot"></div>
    </li>
    </ul>
    </li>
    <div class="imgImpl"></div>
    </ul>
  </div>
  <?php } ?>
  <?php if($this->session->userdata('login_type')=='teacher') { ?>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Implementation</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/index/','Conduct Self Reflection')" >Conduct Self Reflection</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/value_feature/','Behavior & Learning  Running Record')" >Behavior & Learning  Running Record</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/value_homework/','Homework')" >Homework</a></div>
          </li>
          <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb2('Implementation','estrellita/index.php?module=active_teacher_assignment','Administer Assessment')" >Administer Assessment</a></div>
	     </li>
           <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/value_eld/','English Language Development')" >English Language Development</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgImpl"></div>
    </ul>
  </div>
  <?php } ?>
  <?php if(!empty($groups)) 
		{
		 ?>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c">Sections</div>
        <ul>
          <div class="leftmtop"></div>
          <?php
		foreach($groups as $val)
		{
		?>
          <?php if($this->session->userdata('login_type')=='teacher') { ?>
          <li>
            <div class="lnktxt"><a href="teacherself/observationpoints/<?php echo $val['group_id'];?>"><?php echo $val['group_name'];?></a></div>
          </li>
          <?php }  else { ?>
          <li>
            <div class="lnktxt"><a href="report/observationpoints/<?php echo $val['group_id'];?>"><?php echo $val['group_name'];?></a></div>
          </li>
          <?php } } 
		?>
          <?php if($this->session->userdata('login_type')=='teacher') { ?>
          <li>
            <div class="lnktxt"><a href="teacherself/finish/">Save Report</a></div>
          </li>
          <?php } else { ?>
          <li>
            <div class="lnktxt"><a href="report/finish/">Save Report</a></div>
          </li>
          <?php } ?>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgSections"></div>
    </ul>
  </div>
  <?php
		
		} ?>
        
  <?php if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer') { ?>
  <!-- Testing for Navigation starts here -->
  <div id="navigation">
    <ul>
      <li style="width:87px !important;">
        <div class="hdtxtblank_c" style="width:156px !important;"><a onmouseover="demo()" onclick="abc();">Data Analysis</a></div>
        <ul>
          <div class="leftmtop"></div>
          <?php if($this->session->userdata('login_type')=='user') { ?>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/dashboard/','Dashboard')" >Dashboard</a></div>
          </li>
          <?php } ?>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/teacherreport/','Teacher Report')"   >Teacher Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a     style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/observerreport/','Observer Report')"  >Observer Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/Schoolreport/','School Report')" >School Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/gradereport/','Grade Report')"    >Grade Report</a></div>
          </li>
          
          <?php if($this->session->userdata('login_type')=='observer') { ?>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/selfreport/','Teacher Self Reflection')"    >Teacher Self Reflection</a></div>
          </li>
          <?php } ?>
          <li>
            <div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_notification/','Notification Activity Count')"   <?php  } else { ?>      style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_notification/','Notification Activity Count')"          <?php  }  ?>   >Notification Activity Count</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_professional/','Professional Development Activity')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_professional/','Professional Development Activity')"          <?php  }  ?>   >Professional Development Activity</a></div>
                      </li>
                      
              <?php if($this->session->userdata('login_type')=='user')
			  {
				  ?>       
            
              <li>
            <div class="lnktxt"><a  class="performancetest" onmouseover="showassess();" style="cursor:pointer;">Performance Assessments</a></div>
            </li>
            
            <ul id="submenu" style="margin-left:2px; top:153px !important;">
          <div class="leftmtop" id="performanceassess"></div>
          <!--<li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','allschoolgraph',' Performance Assessments >> All School Comparisons')" style="cursor:pointer;">Compare All Schools</a></div>
          </li>
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','allschoolbygrade','Performance Assessments >> Comparison of All Schools by Grade')" style="cursor:pointer;">Compare All Schools by Grade Level
 </a></div>
          </li>-->
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','districtwidegradelevelgraph','Performance Assessments >>Districtwide Performance by Grade Level')" style="cursor:pointer;">Districtwide Performance by Grade Level
</a></div>
          </li>
           <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','comparegradeinschoolgraph','Performance Assessments >>Grade Level Performance by School')" style="cursor:pointer;">Grade Level Performance by School 
</a></div>
          </li>
           <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','classroomgrowthgraph','Performance Assessments >>View a Classrooms Growth')" style="cursor:pointer;">View a Classrooms Growth
</a></div>
          </li>
		  <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictreport','Performance Assessments >> District Performance Scores by Standard')" style="cursor:pointer;">District Performance Scores by Standard</a> </div>
          </li>	
                 
         
           <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictgraph','Performance Assessments >> District Performance Graphs by Standard')" style="cursor:pointer;">District Performance Graphs by Standard</a> </div>
          </li>
		  <!--<li>
            <div class="lnktxt"> <a onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Performance Assessments >> Single Assessment Reports by Standard')" style="cursor:pointer;">Single Assessment Reports by Standard</a></div>
          </li>
        <li>
            <div class="lnktxt"> <a onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Performance Assessments >> Multiple Assessment Reports by Standard')" style="cursor:pointer;">Multiple Assessment Reports by Standard</a></div>
          </li>-->
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Performance Assessments >> Individual Student Performance Summary')" style="cursor:pointer;">Individual Student Performance Summary</a></div>
          </li>
         <!-- <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','graphcontroll','Performance Assessments >> Classroom Graph')" style="cursor:pointer;">Compare A Classrooms Growth 
 </a></div>
          </li>-->  
 <!--<li>
            <div class="lnktxt">
			
			<a onclick="javascript:changebreadcrumb('Data Analysis','classroomteacherreport','Performance Assessments >> Assessment Completion Roster')" style="cursor:pointer;">Assessment Completion Roster</a> 
			
			</div>
          </li>-->
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','selectedstudentreport','Performance Assessments >> Individual Student Performance by Standard')" style="cursor:pointer;">Individual Student Performance by Standard</a> </div>
          </li>		  
         	<!-- <li>  
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','studentarchivedata','Performance Assessments >> Archive student data')" style="cursor:pointer;">Archive student data</a> </div>
          </li> -->
                   		  
        
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
            
            
                    
              <?php
			  }
			  ?>
        
  
          <?php if($this->session->userdata('login_type')=='observer') { ?>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getstudentreport/','Behavior & Learning Running Record')"    >Behavior & Learning Running Record</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getclassworkstudentreport/','Classwork Performance')"    >Classwork Performance</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/gethomeworkstudentreport/','Homework')"    >Homework</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/geteldstudentreport/','English Language Development')"    >English Language Development</a></div>
          </li>
          
          
          
          <li>
            <div class="lnktxt"><a  class="performancetest" onmouseover="showassess();" style="cursor:pointer;">Performance Assessments</a></div>
            </li>
            
            <ul id="submenu" style="margin-left:2px; top:232px !important;">
          <div class="leftmtop" id="performanceassess"></div>
           <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','allschoolgraph','Performance Assessments >> All School Comparisons')" >Compare All Schools
   </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','allschoolbygrade','Performance Assessments >> Comparison of All Schools by Grade')" >Compare All Schools by Grade Level
 </a></div>
          </li>-->
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','graphcontrollindividualschool','Performance Assessments >> Individual School')" >Compare A Grade Level by School 
</a></div>
          </li>-->
		  <!--<li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports by Standard')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Performance Assessments >> Single Assessment Reports by Standard
')"          <?php  }  ?>   >Single Assessment Reports by Standard</a></div>
          </li>
        <li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports by Standard')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Performance Assessments >>Multiple Assessment Reports by Standard
')"          <?php  }  ?>   >Multiple Assessment Reports by Standard
</a></div>
          </li>-->
          		  
		  <li>  
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','classroomgrowthgraph','Performance Assessments >>View a Classrooms Growth')" style="cursor:pointer;">View a Classrooms Growth
</a></div>
          </li>	
			 
		 
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','graphcontroll','Performance Assessments >> Classroom Graph')" >Compare A Classrooms Growth 
 </a></div>
          </li>-->  
<!-- <li>
            <div class="lnktxt">
			
			<a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','classroomteacherreport','Performance Assessments >> Assessment Completion Roster')" >Assessment Completion Roster</a> 
			
			</div>
          </li>-->
         	  
        
         <?php   if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		 ?>
          <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/index.php/schooldistrictreport" >Performance Score by Standard</a> </div>
          </li>
          <?php 
		  } else { ?>
          
           <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictreport','Performance Assessments >> Performance Score by Standard')" style="cursor:pointer;">Performance Score by Standard</a> </div>
          </li>	
          <?php }         
           if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		 ?>
          <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/index.php/schooldistrictgraph" >Performance Graphs by standard</a> </div>
          </li>
         	 
          <?php }else {?>
         <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','schooldistrictgraph','Performance Assessments >> Performance Graphs by Standard')" style="cursor:pointer;">Performance Graphs by Standard</a> </div>
          </li>	
          <?php 
		  } ?>	
		   <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Performance Assessments >> Individual Student Performance Summary')" >Individual Student Performance Summary</a></div>
          </li>
		   <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','selectedstudentreport','Performance Assessments >> Individual Student Performance by Standard')" >Individual Student Performance by Standard</a> </div>
          </li>	
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentprogress','Performance Assessments >> Student Progress Report')" >Student Progress Report </a> </div>
          </li>
           
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
            
          
          <?php } ?>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgReport"></div>
    </ul>
  </div>
  <!-- Testing for Navigation ends here -->
  
  <?php } ?>
  <?php if($this->session->userdata('login_type')=='teacher') { ?>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c" style="width:156px !important;" ><a onmouseover="demot()" onclick="abct();" >Data Analysis</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport','Teacher Report')"   >Teacher Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport/observer','Observer Report')"  >Observer Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport/grade','Grade Report')"    >Grade Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport/summative','Summative Report')"    >Summative Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_observation','Observation Count')"    >Observation Count</a></div>
          </li>
          <li>
            <div class="lnktxt"><a     style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/selfreport','Self Reflection Report')"    >Self Reflection Report</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_notification','Notification Activity Count')"    >Notification Activity Count</a></div>
          </li>
          <!--<li><div class="lnktxt"><a  href="teacherself/getteacher_professional"   >Professional Development Activity</a></div></li>-->
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_consolidated','P.D. & Consolidated Activity Reports')"    >P.D. & Consolidated Activity Reports</a></div>
          </li>
          <li>
            <div class="lnktxt"><a style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getstudentreport','Behavior & Learning Running Record')"    >Behavior & Learning Running Record</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getclassworkstudentreport','Classwork Performance')"    >Classwork Performance</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/gethomeworkstudentreport','Homework')"    >Homework</a></div>
          </li>
          <li>
            <div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/geteldstudentreport','English Language Development')"    >English Language Development</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','parents/partnership','Partnership Report')" >Partnership Report</a></div>
          </li>
          
          
              <li>
            <div class="lnktxt"><a onmouseover="showassesst();" style="cursor:pointer;" style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>   >Performance Assessments</a></div>
          </li>
          
          	<ul id="submenut" style="left: 167px; !important; top: 271px !important;">
          <div class="leftmtop" id="performanceassesst"></div>
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','allschoolgraph','Performance Assessments >> All School Comparisons')" >Compare All Schools
   </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','allschoolbygrade','Performance Assessments >> Comparison of All Schools by Grade')" >Compare All Schools by Grade Level
 </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','graphcontrollindividualschool','Performance Assessments >> Individual School')" >Compare A Grade Level by School 
</a></div>
          </li>-->
		  <!--<li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports by Standard')"   <?php  } else { ?> style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Performance Assessments >> Single Assessment Reports by Standard')"          <?php  }  ?>   >Single1 Assessment Reports by Standard</a></div>
          </li>
        <li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports by Standard')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Performance Assessments >> Multiple Assessment Reports by Standard')"          <?php  }  ?>   >Multiple Assessment Reports by Standard</a></div>
          </li>-->
          <li>  
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','classroomgrowthgraph','Performance Assessments >>View a Classrooms Growth')" style="cursor:pointer;">View a Classrooms Growth
</a></div>
          </li>
		 
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','graphcontroll','Performance Assessments >> Classroom Graph')" >Compare A Classrooms Growth 
 </a></div>
          </li>-->  
 <!--<li>
            <div class="lnktxt">
			
			<a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','classroomteacherreport','Performance Assessments >> Assessment Completion Roster')" >Assessment Completion Roster</a> 
			
			</div>
          </li>-->
          
           
        
          <?php if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		 ?>
          <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/index.php/teacherclusterreport" >Performance Score by Standard</a> </div>
          </li>
          <?php 
		  } else{?>
          
            <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','teacherclusterreport','Performance Assessments >> Class Performance Score by Standard')" style="cursor:pointer;">Class Performance Score by Standard</a> </div>
          </li>	
           
          <?php }?>
           
         
           <?php if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		 ?>
             <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/index.php/teacherclustergraph" >Performance Graphs by standard</a> </div>
          </li>
          <?php 
		  } else{?>	  
          <li> <!-- schooldistrictreport -->
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Data Analysis','teacherclustergraph','Performance Assessments >> Class Performance Graphs by Standard')" style="cursor:pointer;">Class Performance Graphs by Standard</a> </div>
          </li>	
          <?php } ?>
		   <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Performance Assessments >> Individual Student Performance Summary')" >Individual Student Performance Summary</a></div>
          </li>
		   <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','selectedstudentreport','Performance Assessments >> Individual Student Performance by Standard')" >Individual Student Performance by Standard</a> </div>
          </li>
          
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentprogress','Performance Assessments >> Student Progress Report')" >Student Progress Report </a> </div>
          </li>
          	
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
          
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgReport"></div>
    </ul>
  </div>
  <!--Report splash page starts here-->
  
  <?php /*?><div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Data Analysis Resources</a></div>
        <ul>
          <div class="leftmtop"></div>
         <!-- <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','quickGuides','Reference Guides')" >Reference Guides </a></div>
          </li>-->
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assesstest','Assessment Documents')" >Assessment Documents </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assessscoring','Assessment Scoring Guides')" >Assessment Scoring Guides</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assessmentcalendar','Instructional Calendar')" >Instructional Calendars
  </a></div>
          </li>
        	     <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','trainingmaterial','Training Materials Manager')" >Training Materials Manager</a></div>
          </li>
		  <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','addkinder1','Manual Test Score Entry')" >Manual Test Score Entry</a></div>
          </li>
		<!--  <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','teacher','Training Materials Viewer')" >Training Materials Viewer</a></div>
          </li>-->
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgReport"></div>
    </ul>
  </div><?php */?>
  
  <!--Report splash page ends here-->
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Planning</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','teacherplan/schedule','Calendared Activities')" >Calendared Activities</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','teacherreport/goalplan','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','teacherplan','Lesson Plan Book')" >Lesson Plan Book</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','teacherreport/observationplan','Observation Plans')" >Observation Plans</a></div>
          </li>
          <li style="height:5px;">
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgPlan"></div>
    </ul>
  </div>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Communications</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','teacherreport/memorandums','Correspondences')" >Correspondences</a></div>
          </li>
          <li style="height:5px;">
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgComm"></div>
    </ul>
  </div>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Capacity Building</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','teacherplan/videos','Individualized Professional Development')" >Individualized Professional Development</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div>
          </li>
          <li style="height:5px;">
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgCapBld"></div>
    </ul>
  </div>
  
  
  
  <?php } ?>
  <?php if($this->session->userdata('login_type')=='observer') { ?>
  
  <!--Report splash page starts here-->
  
  <?php /*?><div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Data Analysis Resources</a></div>
        <ul>
          <div class="leftmtop"></div>
         <!-- <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','quickGuides','Reference Guides')" >Reference Guides </a></div>
          </li>-->
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assesstest','Assessment Documents')" >Assessment Documents </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','assessscoring','Assessment Scoring Guides')" >Assessment Scoring Guides</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','teacher','Assessment Calendars')" >Assessment Calendars </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis Resources','teacher','Training Materials Manager')" >Training Materials Manager</a></div>
          </li>
          
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','teacher','Training Materials Viewer')" >Training Materials Viewer</a></div>
          </li>-->
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgReport"></div>
    </ul>
  </div><?php */?>
  
  <!--Report splash page ends here-->
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Planning</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','observerview/schedule','Calendared Activities')" >Calendared Activities</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','observerview/comments','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div>
          </li>
          <li>
            <div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','observerview/lessoncomments','Lesson Plan Book')" >Lesson Plan Book</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','observerview/answer','Observation Plans')" >Observation Plans</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','index/observerindex','Dashboards')" >Dashboards</a></div>
          </li>
          <li style="height:5px;">
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgPlan"></div>
    </ul>
  </div>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Communications</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/memorandums','Create Memorandum')" >Create Memorandum</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/assign','Memorandums in Progress')" >Memorandums in Progress</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/assigned','Memorandum Records')" >Memorandum Records</a></div>
          </li>
          <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/index','Staff Communication')" >Staff Communication</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/staff_report','Staff Communication Records')" >Staff Communication Records</a></div></li>
          <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/parents','Parent Notification')" >Parent Notification</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/parent_report','Parent Notification Records')" >Parent Notification Records</a></div></li>
          <li style="height:5px;">
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgComm"></div>
    </ul>
  </div>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Capacity Building</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','observerview/profdev','Individualized Professional Development')" >Individualized Professional Development</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div>
          </li>
          <li style="height:5px;">
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgCapBld"></div>
    </ul>
  </div>
  <?php } ?>
  <?php if($this->session->userdata('login_type')=='user') { ?>

  
  
  <!-- Testing for Navigation starts here -->
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Planning</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','goalplan/comments','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','distplans','Observation Plans')" >Observation Plans</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','index/userindex','Dashboards')" >Dashboards</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgPlan"></div>
    </ul>
  </div>
  <!-- Testing for Navigation ends here --> 
  
  <!-- Testing for Navigation starts here -->
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Communications</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','memorandum/memorandumsall','Documents in Progress')" >Documents in Progress</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','memorandum/savememorandumsall','Saved Documents')" >Saved Documents</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','memorandum/assigned','Records')" >Records</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgComm"></div>
    </ul>
  </div>
  <!-- Testing for Navigation ends here --> 
  
  <!-- Testing for Navigation starts here -->
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Capacity Building</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgCapBld"></div>
    </ul>
  </div>
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c" style="width:156px !important;"><a onclick="abcnew();" onmouseover="demonew()">Development Tools</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school','Schools')" >Schools</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observer','Observers')" >Observers</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','teacher','Teachers')" >Teachers</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school_type','School Type')" >School Type</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observer/profileimage','Profile Image')" >Profile Image</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observationgroup','Checklist Standards')" >Checklist Standards</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observationpoint','Checklist Elements')" >Checklist Elements</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubricscale','Rubric Content')" >Rubric Content</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubricscalesub','Rubric Scales')" >Rubric Scales</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','proficiencygroup','Proficiency Rubric Standards')" >Proficiency Rubric Standards</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','proficiencypoint','Proficiency Elements and Scales')" >Proficiency Elements and Scales</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lickertgroup','Likert Standards')" >Likert Standards</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lickertpoint','Likert Elements')" >Likert Elements</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lubricgroup','Leadership Rubric')" >Leadership Rubric</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lubricpoint','Leadership Elements')" >Leadership Elements</a></div>
          </li>
          <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','goalplan','Goal Plans')" >Goal Plans</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lesson_plan_material','Lesson Plan Material')" >Lesson Plan Material</a></div></li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','standard','Instructional Standards')" >Instructional Standards</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','valueplan','Behavior & Learning Running Records')" >Behavior & Learning Running Records</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','classwork_proficiency','Classwork Proficiency')" >Classwork Proficiency</a></div>
          </li>
          <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','homework_proficiency','Homework Proficiency')" >Homework Proficiency</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','custom_differentiated','Lesson Plan Customizer')" >Lesson Plan Customizer</a></div></li>
          <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubric_data','ELD Proficiency Marks')" >ELD Proficiency Marks</a></div></li>
		  <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','eldrubric','ELD Rubrics')" >ELD Rubrics</a></div>
          </li>
           <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','report_disclaimer','Data Reports Disclaimer')" >Data Reports Disclaimer</a></div>
          </li>
           <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','greetings_text','Assessment Letter')" >Assessment Letter</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','memorandum','Communications')" >Communications</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','status','Teacher H.R. Status')" >Teacher H.R. Status</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','score','Teacher Performance Rating')" >Teacher Performance Rating</a></div>
          </li>
         <!-- <li>
            <div class="lnktxt"> <a href="http://<?php //echo $_SERVER['HTTP_HOST'];?>/testbank/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>-->
         <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>
           <li>
            <div class="lnktxt"> <a href="http://www.nanowebtech.com/testbank/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
          <?
		 }
		 else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
		 ?>
         
          <li>
            <div class="lnktxt"> <a href="https://enterprise.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php 
		 }
		 else if($_SERVER["HTTP_HOST"]=="estrellita.ueisworkshop.com"){
		 ?>
         
          <li>
            <div class="lnktxt"> <a href="https://dev.ueisworkshop.com/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php 
		 }
		 else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
			?>
             <li>
            <div class="lnktxt"> <a href="http://district.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
            
            <?php
		 }
		  else if($_SERVER["HTTP_HOST"]=="localhost"){
		 ?>
               <li>
            <div class="lnktxt"> <a href="http://localhost/WAPTrunk/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php
		  }
		   else if($_SERVER["HTTP_HOST"]=="estrellita.ueisworkshop.com"){
		 ?>
               <li>
            <div class="lnktxt"> <a href="http://localhost/WAPTrunk/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php
		  }
		  
          else if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		  ?>
             <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
          <?php 
		  } ?>
          
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','studentdetail','Student')" >Student</a></div>
          </li>-->
          
          
             <li >
<div class="lnktxt"><a onmouseover="showassessnew();" style="cursor:pointer;">Data Analysis Resources</a></div>
            </li>
            
            <ul id="submenunew" style="top:553px; left:158px;">
          <div class="leftmtop" id="performanceassessnew"></div>
          <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','quickGuides','Reference Guides')" >Reference Guides </a></div>
          </li>-->
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tool','assesstest','Data Analysis Resources >> Assessment Documents')" style="cursor:pointer;">Assessment Documents </a></div>
          </li>
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','assessscoring','Data Analysis Resources >> Assessment Scoring Guides')" style="cursor:pointer;">Assessment Scoring Guides</a></div>
          </li>
          <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','assessmentcalendar','Data Analysis Resources >> Instructional Calendar')" style="cursor:pointer;">Instructional Calendars
 </a></div>
          </li>
         <li>
            <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','trainingmaterial','Data Analysis Resources >> Training Materials Manager')" style="cursor:pointer;">Training Materials Manager</a></div>
          </li>
          
      <li>
        <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','addproficiency','Data Analysis Resources >> Create Proficiency Assessment Ranges')" style="cursor:pointer;">Create Proficiency Assessment Ranges </a></div>
          </li>   
          
          
		   <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','addkinder','Add Kindergarten details')" >Adding The Student Scores</a></div>
          </li> -->
          
           <!-- <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','addgradeteacher','Add First grade teachers detail')" >Add First grade teachers detail</a></div>
          </li>-->
          
          
          
	  <!--<li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','teacher','Training Materials Viewer')" >Training Materials Viewer</a></div>
          </li>-->
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
          
          
          
          
          
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgDevTools"></div>
    </ul>
  </div>
  
 
  
    <!--  Start Graphs --> 
  
  <!--<div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Performance Assessments</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','allschoolgraph','All School Comparisons')" >Compare All Schools
   </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','allschoolbygrade','Comparison of All Schools by Grade')" >Compare All Schools by Grade Level
 </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','graphcontrollindividualschool','Individual School')" >Compare A Grade Level by School 
</a></div>
          </li>
		  <li>
            <div class="lnktxt"> <a  <?php //if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports')"   <?php // } else { ?>      style="cursor:pointer;<?php //if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php //} else { ?>"<?php // } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports')"          <?php // }  ?>   >Single Assessment Reports</a></div>
          </li>
        <li>
            <div class="lnktxt"> <a  <?php //if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports')"   <?php  //} else { ?>      style="cursor:pointer;<?php //if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php //} else { ?>"<?php //} ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports')"          <?php  //}  ?>   >Multiple Assessment Reports</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Individual Student Performance Scores')" >Individual Student Performance Scores</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','graphcontroll','Classroom Graph')" >Compare A Classrooms Growth 
 </a></div>
          </li>  
 <li>
            <div class="lnktxt">
			
			<a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','classroomteacherreport','Teacher Detailed Report')" >Teacher Detailed Report</a> 
			
			</div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb(' Performance Assessments','selectedstudentreport','Individual Student Performance by Standard')" >Individual Student Performance by Standard</a> </div>
          </li>		  
        
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgDevTools"></div>
    </ul>
  </div>--> 
 
  
  <!-- End Graphs -->
  
  <!--  Start Graphs --> 
  <!--
  <div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a> Reports </a></div>
        
        <ul>
          <div class="leftmtop"></div>
         <li>
            <div class="lnktxt">
			
			<a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Reports','classroomteacherreport','District: Grade and Teacher Report')" >District: Grade and Teacher Report   </a> 
			
			</div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb(' Reports','selectedstudentreport','Selected Student Report')" >Selected Student Report </a></div>
          </li>
        
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgDevTools"></div>
    </ul>
  </div> 
 -->
  
  <!-- End Graphs -->
  
  <!-- Testing for Navigation ends here --> 


  
  
  
  <!-- Testing for Navigation ends here -->
  
  <?php } ?>
  
  <!-- Testing for Navigation starts here -->
  
  <?php include(MEDIA_PATH.'inc/videomenu.php'); ?>
  
  <!--<div id="navigation">
            
            
          
                <ul>
                    <li><div class="hdtxtblank_c"><a href="http://<?php //echo $_SERVER['HTTP_HOST'];?>/testbank/workshop/Quiz/">Assessments</a></div>
                    </li>
                <div class="imgComm"></div>
                </ul>
            </div>--> 
  
  <!-- Testing for Navigation ends here --> 
  
  <!-- Testing for Navigation starts here --> 
  <!--<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_o"><a>Off-Line</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt">GUI</div></li>
                    <li><div class="lnktxt">Navigation</div></li>
                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li><div class="imgOffline"></div>
            </ul>
   			</div>--> 
  <!-- Testing for Navigation ends here --> 
  
  <!-- Testing for Navigation starts here --> 
  <!--<div id="navigation" style="margin-bottom:-10px;">
	    	<ul>
            <li><div class="hdtxtblank_o"><a>On-Line</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt">GUI</div></li>
                    <li><div class="lnktxt">Demo</div></li>
                                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li>
            <div class="imgOnline"></div>
            </ul></div>-->
  
  <div class="leftmbot_main" style="height:5px;" id="navigation"></div>
  
  <!-- Testing for Navigation ends here --> 
  
</div>
</div>