<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
       <ul class="sidebar-menu">
                <li class="sub-menu">
                  <a class="" href="<?php echo base_url();?>">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
                <!-- FIRST LEVEL MENU Planning Manager-->
              <li class="sub-menu" id="<?php echo @$idname=='lesson'? $idname:''; ?>">
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Planning Manager</a></li>
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>custom_differentiated">Lesson Plan Customer</a></li></ul>
                    
                     <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>lesson_plan_material">Lesson Plan Materials</a></li></ul>
           
           
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>standard">Instructional Standards</a></li>
                </ul>
                                    
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>goalplan">Goals Plans</a></li>
                </ul>
                
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
              <li class="sub-menu" id="<?php echo @$idname=='attendance'? $idname:''; ?>" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Personnel Set-up</span>
                      <span class="arrow"></span>
                  </a>
                 <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                 <!-- SECOND LEVEL MENU -->
					
              <!-- THIRD LEVEL MENU -->
              <ul class="sub">
					  <li><a class="" href="<?php echo base_url();?>attendance/personnel_set_up">Personnel Set-up</a></li></ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>school">Schools</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>observer">Observers</a></li>
                </ul>   
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacher">Teachers</a></li>
                </ul>    
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>school_type">School Type</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>observer/profileimage">Profile Image</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>dist_grade">Grades</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>dist_subject">Subjects</a></li>
                </ul>
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu" id="<?php echo @$idname=='implementation'? $idname:''; ?>" >
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Instructional Efficacy</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                        <li><a class="" href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>observationgroup">Checklist Standards</a></li>
                </ul > 
           
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>observationpoint">Checklist Elements</a></li>
                </ul > 
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>rubricscale">Rubric Content</a></li>
                </ul > 
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>rubricscalesub">Rubric Scales</a></li>
                </ul > 
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>proficiencygroup">Proficiency Rubric Standards</a></li>
                </ul > 
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>proficiencypoint">Proficiency Elements & Scales</a></li>
                </ul > 
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>lickertgroup">Likert Standards</a></li>
                </ul > 
                     <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>lickertpoint">Likert Elements</a></li>
                </ul > 
           
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>lubricgroup">Leadership Rubric</a></li>
                </ul > 
           
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>lubricpoint">Leadership Elements</a></li>
                </ul > 
           
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>status">Teacher HR Status</a></li>
                </ul > 
           
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>score">Teacher Performance Rating</a></li>
                </ul > 
           
                
                  </ul>
              </li>
              
              
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
             <li class="sub-menu" id="<?php echo @$idname=='classroom'? $idname:''; ?>">
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Implementation</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                   <!--    <li><a class="" href="<?php echo base_url();?>classroom">Implementation</a></li>-->
                      <!-- THIRD LEVEL MENU -->
                   <!-- start implimantation-->
                   <li><a class="" href="<?php echo base_url();?>assessment/implementation">Implementation</a></li>                              
                       <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/iep_tracker">IEP Tracker</a></li>
                		</ul> 
                        
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>dates">Dates</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>meeting_type">Meeting type</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>attendance_page">Attendance Page</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>eligibility">eligibility</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>external_impact">External Impact Factors</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>eligibility_status">Eligibility Status</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>indicate_parent_consent">Indicate Parent Consent</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>categorize_issues">categorize issues</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>academic_achievement">Academic Achievement</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>english_langauge_development">English Langauge Development</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>performance_area">Performance Area</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>assessment_monitoring">Assessment Monitoring Process Used</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>state_district_assessment">State/District Assessment Results</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>responsible_person">Responsible person administration</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>services_grid">Services Grid</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>student_assessments">Student Assessments</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>ethnic_code">Ethnic Code</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>language">Language</a></li>
                		</ul> 
                        
                        
                        <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/iep_Plan">IEP Plan</a></li>
                		</ul> 
                        
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>plan_eligibility">Plan Eligibility</a></li>
                		</ul> 
                        
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>plan_curriculum">Plan Curriculum</a></li>
                		</ul> 
                        
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>plan_placement">Placement</a></li>
                		</ul> 
                        
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>instructionl_setting">Instructionl Setting</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>additional_factors">Additional Factors</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>accommodation_modifications">Accommodation Modifications</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>preparation">Preparation</a></li>
                		</ul> 
                   
                   <!--end implementation  -->   
                      
                       <li><a class="" href="<?php echo base_url();?>classroom">Implementation</a></li>
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>valueplan">Behavior & Learning Running Rec. </a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classwork_proficiency">Classwork Proficiency</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>homework_proficiency">Homework Proficiency</a></li>
                </ul>   
               
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>rubric_data">ELD Proficiency Marks</a></li>
                </ul>   
               
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>eldrubric">ELD Rubrics</a></li>
                </ul>   
               
                <!-- SECOND LEVEL MENU -->  
                   </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
                   <li class="sub-menu"  id="<?php echo @$idname=='assessment'? $idname:''; ?>">
<!--                  <a href="<?php echo base_url();?>assessment/" class="assessment">-->
                  <a href="javascript:;" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Class Setup</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                  <li><a class="" href="<?php echo base_url();?>assessment/class_setup">Class Setup</a></li>                      
                   
                        

                       <li><a class="" href="<?php echo base_url();?>assessment/classroom_management">Classroom Management</a></li>               
                       <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>report_standard">Progress Report Standards</a></li>
                		</ul>   
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>report_comment">Progress Report Comments </a></li>
                		</ul>  
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>report_grade">Progress Report Grades</a></li>
                		</ul>  
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>report_effort">Progress Report Efforts</a></li>
                		</ul>
                    <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>work_habits">Work Habits</a></li>
                    </ul>  
                    <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>learning_skills">Learning & Social Skills</a></li>
                    </ul>  
                        <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>instructional_program">Instructional Program</a></li>
                    </ul>
                      <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/student_success_team ">Student Success Team </a></li>
                		</ul>
                        
                        <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/needs">Needs</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>intervention_strategies_home">Needs Home</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>intervention_strategies_school">Needs School</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>intervention_strategies_medical">Needs Medical</a></li>
                		</ul>
                       
                        <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/action">Action Plan</a></li>
                		</ul>   
                        	 <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>actions_home">Home</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>actions_school">School</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>actions_medical">Medical</a></li>
                		</ul>
                        
                        
                      <ul class="sub">
							<li><a class="" href="#">Behavior Running Record Set-up</a></li>
                		</ul>  
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>behavior_location">Location</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>problem_behaviour">Behavior Type/Action Form</a></li>
                		</ul>
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>possible_motivation">possible motivation</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>interventions_prior">Interventions Prior to Office Referral </a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>student_has_multiple_referrals">Referrals Suggested</a></li>
                		</ul> 

                    <ul class="sub">
              <li><a class="" href="#">Incident Running Record Set-up</a></li>
                    </ul>  
                         <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>incident_location">Incident Location</a></li>
                    </ul>
                         <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>problem_incident">Incident Type/Action Form</a></li>
                    </ul>
                         <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>incident_possible_motivation">Incident possible motivation</a></li>
                    </ul> 
                        <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>incident_interventions_prior">Incident interventions Prior to Office Referral </a></li>
                    </ul> 
                        <ul class="sub" id="fourth">
              <li><a class="" href="<?php echo base_url();?>incident_student_has_multiple_referrals">Incident Referrals Suggested</a></li>
                    </ul> 
                        
                      <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/parent_teacher_conference">Parent/Teacher Conference</a></li>
                		</ul>  
                        
                           <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>strengths">Strengths</a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>concerns">concerns </a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>ideas_for_parent_student">Strategy</a></li>
                		</ul>  
                      <ul class="sub">
							<li><a class="" href="<?php echo base_url();?>assessment/student_teacher_conference">Student/ Teacher Conference</a></li>
                		</ul>   
                         <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>history">History </a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>behavior_teacher_student">Behavior</a></li>
                		</ul> 
						
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>strategy">Strategy</a></li>
                		</ul> 
                        
                 	</ul>
               </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
                <li class="sub-menu" id="<?php echo @$idname=='tools'? $idname:''; ?>">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Data Analysis Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  	  <li><a class="" href="<?php echo base_url();?>tools/data_tracker">Data Analysis Resources
</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                       <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>
                         <li><a href="http://www.nanowebtech.com/testbank/workshop/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a></li>
                      <? }else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){?>
                 		<li><a href="https://dev.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a></li>
                     <?php }else if($_SERVER["HTTP_HOST"]=="estrellita.ueisworkshop.com"){?>
                        <li><a href="https://dev.ueisworkshop.com/estrellita/login.php?msg=56" target="_blank">Student Assessment Platform</a></li>
                        <?php }else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){ ?> 
						<li> <a href="https://login.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a> </li>
                         <?php }else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){?>
                        <li> <a href="https://demo.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a></li>
                         <?php } else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){?>
                        <li> <a href="https://demo.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a> </li>
                         <?php } else if($_SERVER["HTTP_HOST"]=="localhost"){?>
                        <li> <a href="http://localhost/WAPTrunk/estrellita/login.php?msg=56" target="_blank">Student Assessment Platform</a> </li>
                         <?php } else if($_SERVER["HTTP_HOST"]=="estrellita.ueisworkshop.com"){ ?>
                        <li> <a href="http://localhost/WAPTrunk/estrellita/login.php?msg=56" target="_blank">Student Assessment Platform</a> </li>
						<?php }else if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){ ?>  
                      <li> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a> </li>
                      <?php }  else if($_SERVER["HTTP_HOST"]=="workshop2.ueisworkshop.com"){?>
						<li> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/Quiz/login.php?msg=56" target="_blank">Student Assessment Platform</a></li>
					<?php } ?> 
                </ul> 
                
                


                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/setup_manual_assessment_entry">Set-up Manual Assessment Entry</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>tools/manual_assessment">Manual Assessment Entry</a></li>
                		</ul> 
                <ul class="sub" id="fourth">
							<li><a class="" href="<?php echo base_url();?>tools/enter_assessment_score">Enter Assessment Score</a></li>
                		</ul> 
                
 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>addproficiency">Assessment Proficiency Ranges</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>assesstest">Assessment Documents</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>assessmentcalendar">Assessment Calendar</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>assessscoring">Assessment Scoring guides</a></li>
                </ul>
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>trainingmaterial">Training Materials</a></li>
                </ul>    
                
                  </ul>
      
              </li>
              
              <li class="sub-menu"  id="<?php echo @$idname=='assessment'? $idname:''; ?>">
<!--                  <a href="<?php echo base_url();?>assessment/" class="assessment">-->
                  <a href="javascript:;" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Communication</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>assessment/forms_reports_set_up">Communicaton</a></li>                      
                       <ul class="sub">
							<li><a class="" href="<?php // echo base_url();?>assessment/communications">Forms & Reports set-up</a></li>
                		</ul>   
                      
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php // echo base_url();?>greetings_text">Assessment Letters </a></li>
		                </ul>   
               
        		        <ul class="sub" id="fourth">
							<li><a class="" href="<?php // echo base_url();?>memorandum">Memorandums</a></li>
                		</ul>
                        <ul class="sub" id="fourth">
							<li><a class="" href="<?php  echo base_url();?>report_disclaimer">Report Disclaimer Records </a></li>
                		</ul> 
                        <ul class="sub" id="fourth">
              <li><a class="" href="<?php  echo base_url();?>report_description">Report Description Records </a></li>
                    </ul> 
                      
                        
                 	</ul>
               </li>
              
            
          </ul>
     
   