<div class="lft">
<div class="leftmtop_main"></div>
<div class="leftmenu"  >
        		<div class="lnktxt1"><a href="district">Districts</a></div>
				<div class="lnktxt1"><a href="school">Schools</a></div>
				<div class="lnktxt1"><a href="school_type">School Type</a></div>
                <div class="lnktxt1"><a href="observer">Observers</a></div>
				<div class="lnktxt1"><a href="teacher">Teachers</a></div>
				<div class="lnktxt1"><a href="prod_spec">Product Specialists</a></div>				
                
				<div class="lnktxt1"><a href="observationgroup">Checklist Standards</a></div>
				<div class="lnktxt1"><a href="observationpoint">Checklist Elements</a></div>
				
				<div class="lnktxt1"><a href="rubricscale">Rubric Content</a></div>
				<div class="lnktxt1"><a href="rubricscalesub">Rubric Scales</a></div>
				
				
				<div class="lnktxt1"><a href="proficiencygroup">Proficiency Rubric Standards</a></div>
				<div class="lnktxt1"><a href="proficiencypoint">Proficiency Rubric Elements</a></div>				
				<div class="lnktxt1"><a href="lickertgroup">Likert Standards</a></div>
				<div class="lnktxt1"><a href="lickertpoint">Likert Elements</a></div>
				<div class="lnktxt1"><a href="lubricgroup">Leadership Rubric</a></div>
				<div class="lnktxt1"><a href="lubricpoint">Leadership Elements</a></div>
				<div class="lnktxt1"><a href="observationplan">Observation Plan</a></div>
				<div class="lnktxt1"><a href="memorandum">Communications</a></div>
				<div class="lnktxt1"><a href="goalplan">GoalPlans</a></div>
				<div class="lnktxt1"><a href="standard">Instructional Standards</a></div>
				
				<div class="lnktxt1"><a href="valueplan">Behavior & Learning Running Records</a></div>
				<div class="lnktxt1"><a href="classwork_proficiency">Classwork Proficiency</a></div>
				<div class="lnktxt1"><a href="homework_proficiency">Homework Proficiency</a></div>
				<div class="lnktxt1"><a href="goalplan/comments">Teacher GoalPlans</a></div>
				<div class="lnktxt1"><a href="scheduletask">Schedule Tasks</a></div>
				<div class="lnktxt1"><a href="lessonplan">Lesson Plan Tab</a></div>
				<div class="lnktxt1"><a href="lessonplansub">Lesson Plan SubTab</a></div>
				<div class="lnktxt1"><a href="observationgroup/allvideos">List Of Videos</a></div>
				<div class="lnktxt1"><a href="observationgroup/allarticles">List Of Articles</a></div>
				<div class="lnktxt1"><a href="option">Options</a></div>
                <div class="lnktxt1"><a href="copy_assessments">Student Assessment</a></div>
                <div class="lnktxt1"><a href="transfer_student">Transfer Student</a></div>
                <div class="lnktxt1"><a href="teacher/get_teacher_data">Teahcer List</a></div>
               

				<!--<div class="lnktxt1"><a href="admin/logout">Logout</a></div>  -->             
</div>
<div class="leftmbot_main"></div>        
</div>