<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
        <?php if($this->session->userdata('login_type')=='teacher') { ?>

         <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
         <div class="navbar-inverse">
            <form class="navbar-search visible-phone">
               <input type="text" class="search-query" placeholder="Search">
            </form>
         </div>
         <!-- END RESPONSIVE QUICK SEARCH FORM -->
         <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu" <?php echo @$menustyle;?>>
              <li class="sub-menu">
                  <a class="" href="<?php echo base_url();?>">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
               <!-- FIRST LEVEL MENU Planning Manager-->
            <li class="sub-menu" id="<?php echo @$idname=='lesson'? $idname:''; ?>">
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Creator</a></li>
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="third">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/create_plan">Create Lesson Plan</a></li></ul>
                    
<!--
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/create_plan">Add Plan</a></li></ul>
                    
                  -->  
              <!--   
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/upload_plan">Upload Plan</a></li>
                </ul>
                -->
                <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/edit_plan">Edit Lesson Plan</a></li>
                </ul>
                                    
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/completed_plan">Retrieve Lesson Plan</a></li>
                </ul>
                
                   <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals">SMART Goals Creator</a></li>
                     
                      <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/create_goal">Create Goal</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/edit_goal">Edit Goal</a></li>
                </ul>
                <!-- <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_report">Retrieve Goal</a></li>
                </ul>
                -->
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_smart_goals_retrieve">Retrieve Goal</a></li>
                </ul>
                       <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_retrieve_goal">View Smart Goals Review</a></li>
                </ul>
                                    
              
                     <li><a class="" href="<?php echo base_url();?>planningmanager/pdf_smart_goal">PDF Smart Goals Review</a></li>
                
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_year">Smart Goals Review PDF by Year</a></li>
                </ul>
                
                   <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_subject">Smart Goals Review PDF by Subject </a></li>
                </ul>
                
                
                
                
                
                <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="<?php echo base_url();?>planningmanager/calendar">Calendar Creator</a></li>
                     
                         <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/new_event">Create New Event</a></li>
                </ul>
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/update_event">Edit Event</a></li>
                </ul>
                
                <!--                    
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/retrieve_event">Retrieve Event</a></li>
                </ul>
                -->
                
				<ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/view_class_events">Retrieve Event</a></li>
                </ul>
                
                
                
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu" id="<?php echo @$idname=='attendance'? $idname:''; ?>" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Attendance Manager</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/daily_attendance">Daily Attendance</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/take_roll">Take Roll</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/edit_roll">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="<?php echo base_url();?>attendance/new_student_enrollment">New Student Enrollment</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/modify_enrollment">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/enroll_student">Enroll Student</a></li>
                </ul>  
                  <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="<?php echo base_url();?>attendance/roster">Student Roster</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/create_roster">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/edit_roster">Edit</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
					  <li><a class="" href="<?php echo base_url();?>attendance/map_data">Add-Map Data</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/add_parent_info">Add Parent Information</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/map_parent">Map Parent/Student Data</a></li>
                </ul>  
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu" id="<?php echo @$idname=='implementation'? $idname:''; ?>" >
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Implementation Manager</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>implementation/grade_tracker">Grade Tracker</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_feature">Enter Grade</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/get_retrieve_grade">Retrieve Grade</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       <li><a class="" href="<?php echo base_url();?>implementation/homework_tracker">Homework Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_homework">Enter Homework Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/get_retrieve_homework_record">Retrieve Homework Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                        <li><a class="" href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/iep_tracker">Create IEP Record</a></li>
                </ul>  
                  <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/basic_student_meeting_information">Basic Student & Meeting Information </a></li>
                </ul>  
                <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/basic_student_information">Basic Student Information</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/meeting_attendees">Meeting Attendees</a></li>
                </ul>
				                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/iep_eligibility">IEP Eligibility</a></li>
                </ul>
                
                
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/develop_iep">Develop IEP</a></li>
                </ul>  
                <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/summary_of_academic_achievement">Summary of Academic Achievement</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>mplementation/comprehensive_academic_achievement">Comprehensive Academic Achievement</a></li>
                </ul>
				<ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/english_language_development">Create English Language Development Goals (ELD/ESL)</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/create_student_goals">Create Student Goals (Academic Standards)</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/determine_student_assessments">Determine Student Assessments</a></li>
                </ul>
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>/implementation/iep_plan_summary">IEP Plan Summary</a></li>
                </ul>  
                <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/summary_of_iep_plan">Summary of IEP Plan</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/description_of_iep_services">Description of IEP Services</a></li>
                </ul>
				                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/iep_agreement">IEP Agreement</a></li>
                </ul>
                
                
                                                                          
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/upload_iep_tracker">Edit IEP Record</a></li>
                </ul>  
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/retrieve_iep_record">Retrieve IEP Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="<?php echo base_url();?>implementation/eld_tracker">ELD Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_eld">Enter ELD Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/retrieve_eld_record">Retrieve ELD Record</a></li>
                </ul>  
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/observation">Lesson Observation Feedback</a></li>
                </ul>  
                
               
                
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherreport/observer_full_report">Retrieve Full Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherreport/observer_sectional_report">Retrieve Sectional Data Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/self_reflection">Self-Reflection</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/index">Generate/Edit Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/selfreport">Retrieve Report</a></li>
                </ul>
                
                
                  </ul>
              </li>
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
              <li class="sub-menu" id="<?php echo @$idname=='classroom'? $idname:''; ?>">
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Classroom Management</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>classroom/progress_report">Student Progress Report</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_progress_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_progress_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_progress_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                          <li><a class="" href="<?php echo base_url();?>classroom/success_team">Student Success Team</a></li>
                                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_success_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_success_report">Upload</a></li>
                </ul> 
                <ul class="sub">
<!--                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_success_report">Retrieve</a></li>-->
                     
                       <li><a class="" href="<?php echo base_url();?>classroom/sst_retrieve_report">Retrieve</a></li>
                </ul>
                        
                <!-- SECOND LEVEL MENU -->  
                              <li><a class="" href="<?php echo base_url();?>classroom/parent_teacher">Parent/Teacher Conference</a></li>
                              <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_parent_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_parent_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_parent_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="<?php echo base_url();?>classroom/teacher_student">Teacher/Student Conference</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_student_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_student_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_student_report">Retrieve</a></li>
                </ul>   
                
                  <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="<?php echo base_url();?>classroom/behavior_record">Behavior Running Record</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_behavior">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/edit_behavior">Edit</a></li>
                </ul> 
               <!-- <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_behavior">Upload</a></li>
                </ul> -->
                  
                    <li><a class="" href="<?php echo base_url();?>classroom/report_list">Retrieve Record</a></li> 
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/incident_report">Number of Behavior Entrys by incident type  </a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/students_behavior">Number of Behavior Entrys by student </a></li>
                    </ul>  
                    
                
                  </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
              <li class="sub-menu"  id="<?php echo @$idname=='assessment'? $idname:''; ?>">
                  <a href="<?php echo base_url();?>assessment/" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Assesment Manager</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
<!--                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>assessment/diagnostic_screener">Diagonstic Screener</a></li>
                      <li><a class="" href="<?php echo base_url();?>assessment/progress">Progress Monitoring</a></li>
                      <li><a class="" href="<?php echo base_url();?>assessment/benchmark_assessment">Benchmark Assesment</a></li>
 </ul>-->
              </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
              <li class="sub-menu" id="<?php echo @$idname=='tools'? $idname:''; ?>">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Tools &amp; Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  <li><a class="" href="<?php echo base_url();?>tools">Tools & Resources</a></li>

                       <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
					<li><a class="" href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a></li>
                </ul>       
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_notification">Planning Manager</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroomgrowthgraph">View a Classroom's Growth</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherclusterreport">Class Performance Score by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherclustergraph">Class Performance Graphs by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>studentdetail">Individual Student Performance Summary</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>selectedstudentreport">Individual Student Performance by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>studentprogress">Student Progress Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/attendance_manager">Attendance Manager</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a></li>
                </ul>   
                
                  <!-- FOURTH LEVEL MENU-->                       
              <!--  <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentreport">Grade Tracker</a>
                   </li>
                   </ul>-->
              
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/data_tracker_report">Grade Tracker</a></li>
                </ul> 
              
                 <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentreport">Retrieve Report</a></li>
                </ul> 
                 <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentgraph">Retrieve Graph</a></li>
                </ul> 
				 <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getstudentreport">Behavior Learning & Running Record Report</a></li>
                </ul>               
              
                 
                <ul class="sub">
                    <li><a class="" href="<?php echo base_url();?>teacherself/gethomeworkstudentreport">Homework Tracker</a></li>
                </ul>
                
                <ul class="sub">
                    <li><a class="" href="<?php echo base_url();?>tools/iep_tracker">IEP Tracker</a></li>
                </ul>
                
                <ul class="sub">
                    <li><a class="" href="<?php echo base_url();?>teacherself/geteldstudentreport">ELD Tracker</a></li>
                </ul>
                
                <ul class="sub">
                    <li><a class="" href="<?php echo base_url();?>tools/observation">Lesson Observations</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>teacherreport/observer">Observation by Viewer</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>teacherreport/grade">Observation by Grade</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_observation">Observation Count</a></li>
                </ul>
                  
                  <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_consolidated">Professional Development Activity</a></li>
                </ul> 
                
                
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/classroom_management">Classroom Management</a></li>
                </ul> 
                
               
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/student_progress_report">Student Progress Report</a></li>
                </ul>
                
                   <ul class="sub" id="fourth">
						<li><a class="" href="<?php echo base_url();?>tools/success_team">Student Success Team Meeting Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report">Student Success Team by Student Report</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report_need">Student Success Team by Needs Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report_action">Student Success Team by Actions Report</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/parent_teacher">Parent/Teacher Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/teacher_student">Teacher/Student Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/report_list">Behavior Running Record Report</a></li>
                </ul>
                <ul class="sub" id="fourth"  style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/incident_report">Behavior Entries By Incident Type</a></li>
                </ul>
                <ul class="sub" id="fourth"  style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/students_behavior">Behavior Entries by Student </a></li>
                </ul>
                
                
                <!-- SECOND LEVEL MENU -->  
                      <li><a class="" href="<?php echo base_url();?>tools/professional_development">Professional Development </a></li>
                         <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherplan/videos">Individual</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>banktime">Group</a></li>
                </ul> 
                  <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_consolidated">Professional Development Reports</a></li>
                </ul> 
                <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="<?php echo base_url();?>tools/parent_bridge">Parent Bridge</a></li> 
                     <!-- THIRD LEVEL MENU -->
                      <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>communication/parents">Parent Notification</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>communication/parent_report">Parent Notification Records</a></li>
                </ul>                  
                      <li><a class="" href="<?php echo base_url();?>tools/guide">User/Training Guide</a></li>
                      
                  </ul><BR><BR>
              </li>
             
          </ul>
         <!-- END SIDEBAR MENU -->
         
 <?php } else if($this->session->userdata('login_type')=='observer') {?>
	 
       <ul class="sidebar-menu" <?php echo @$menustyle;?>>
                <li class="sub-menu">
                  <a class="" href="<?php echo base_url();?>">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
                <!-- FIRST LEVEL MENU Planning Manager-->
              <li class="sub-menu" id="<?php echo @$idname=='lesson'? $idname:''; ?>">
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Review</a></li>
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="third">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Create Review</a></li></ul>
                    
                     <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/create_plan">Add Review</a></li></ul>
            <!--     
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/upload_plan">Upload Review</a></li>
                </ul>
                -->
                <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/edit_plan">Edit Review</a></li>
                </ul>
                                    
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/completed_plan">Retrieve Review</a></li>
                </ul>
                
                    <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals">SMART Goals Review</a></li>
                     
                      <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?> planningmanager/create_review">Create Review</a></li>
                </ul>
               
                <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/create_goal">Add Review</a></li></ul>
                 <!--
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>planningmanager/upload_goals">Upload Review</a></li>
                </ul>
                -->
                         <!-- THIRD LEVEL MENU-->           
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/edit_goal">Edit Goal</a></li>
                </ul>
                         
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_smart_goals_retrieve">Retrieve Goal</a></li>
                </ul>
                       <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_retrieve_goal">View Smart Goals Review</a></li>
                </ul>
                                    
              
                     <li><a class="" href="<?php echo base_url();?>planningmanager/pdf_smart_goal">PDF Smart Goals Review</a></li>
                
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_year">Smart Goals Review PDF by Year</a></li>
                </ul>
                
                   <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_subject">Smart Goals Review PDF by Subject </a></li>
                </ul>
                
                
                <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="<?php echo base_url();?>planningmanager/calendar">Calendar Creator</a></li>
                             
                         <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/new_event">Create New Event</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/view_your_events">View Your Events</a></li>
                </ul>
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/view_class_events">View Class Events</a></li>
                </ul>
                
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
              <li class="sub-menu" id="<?php echo @$idname=='attendance'? $idname:''; ?>" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Attendance Manager</span>
                      <span class="arrow"></span>
                  </a>
                 <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/assessment">Assessment</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/daily_attendance">Daily Attendance</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/take_roll">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/edit_roll">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/new_student_enrollment">New Student Enrollment</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/modify_enrollment">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/enroll_student">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->

                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/roster">Student Roster</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="<?php echo base_url();?>attendance/create_roster">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/edit_roster">Edit</a></li>
                </ul>  
                 <!-- SECOND LEVEL MENU -->
					<?php /*?>  <li><a class="" href="<?php echo base_url();?>">Add-Map Data</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/add_parent_info">Add Parent Information</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>attendance/map_parent">Map Parent/Student Data</a></li>
                </ul>  
<?php */?>
              <!-- THIRD LEVEL MENU -->
              <ul class="sub">
					  <li><a class="" href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-up</a></li></ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacher">Create Teachers</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>school_type">School Type</a></li>
                </ul>   
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>class_room">Homeroom/Classroom</a></li>
                </ul>    
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>grade_subject">Grade/Subject Association</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacher_grade_subject">Teacher/Grade Association</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>period">School Period Scheduling</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>time_table">Timetable Assignments</a></li>
                </ul>
                
                 <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/add_parent_info">Parent Information</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/individual_students">Individual Students</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/map_parent">Parent/ Student Association</a></li>
                </ul>  
                
                 <!-- SECOND LEVEL MENU -->
               <?php /*?>
                      <li><a class="" href="<?php echo base_url();?>../attendance/assessment.html">Assessment</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>../attendance/daily-attendance.html">Daily Attendance</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/take-roll.html">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/retrieve-roll.html">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>../attendance/new-student-enrollment.html">New Student Enrollment</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/modify-enrollment.html">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/enroll-student-enrollment.html">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>../attendance/roster.html">Student Roster</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="<?php echo base_url();?>../attendance/create-roster.html">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/edit-roster.html">Edit</a></li>
                </ul>  
              
              
              <!-- THIRD LEVEL MENU -->
              <ul class="sub">
					  <li><a class="" href="<?php echo base_url();?>../attendance/set-up.html">Attendance Manger Set-up</a></li></ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/create-teachers.html">Create Teachers</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/school-type.html">School Type</a></li>
                </ul>   
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/homeroom-classroom.html">Homeroom/Classroom</a></li>
                </ul>    
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/grade-subject.html">Grade/Subject Association</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/teacher-grade.html">Teacher/Grade Association</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/school-period.html">School Period Scheduling</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/timetable.html">Timetable Assignments</a></li>
                </ul>
                
                 <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/parent-student.html">Parent Student Association</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/individual-students.html">Individual Students</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>../attendance/student-mapping.html">Student Mapping</a></li>
                </ul>  <?php */?>
                
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu" id="<?php echo @$idname=='implementation'? $idname:''; ?>" >
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Implementation Manager</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>implementation/grade_tracker">Grade Tracker</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/observer_feature">Enter Grade</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/get_retrieve_grade">Retrieve Grade</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       <li><a class="" href="<?php echo base_url();?>implementation/homework_tracker">Homework Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_homework">Enter Homework Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/get_retrieve_homework_record">Retrieve Homework Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       
                <li><a class="" href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/iep_tracker">Create IEP Record</a></li>
                </ul>  
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/upload_iep_tracker">Edit IEP Record</a></li>
                </ul>  
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/retrieve_iep_record">Retrieve IEP Record</a></li>
                </ul> 
                
                
                
                
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="<?php echo base_url();?>implementation/eld_tracker">ELD Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_eld">Enter ELD Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/retrieve_eld_record">Retrieve ELD Record</a></li>
                </ul>  
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a></li>
                        
                        
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>implementation/observation">Lesson Observation</a></li>
                </ul > 
                
                <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>report/index">Generate Report</a></li> 
                    
                    </ul>
                
<!--          
                <br />
             
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/observation_edit">Edit Report in Progress</a></li>
                    </ul>
    -->
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/observation_retrieve">Retrieve Report</a></li>
                    </ul>
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fifth">
                    <li><a class="" href="<?php echo base_url();?>report/teacherreport/">Full Report</a></li></ul>
                 
                <ul class="sub" id="fifth">
                    <li><a class="" href="<?php echo base_url();?>teacherreport/observer_sectional_report">Sectional Data Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/summative">Summative Report</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>report/summativereport">Generate Report</a></li></ul>
                 
               <!-- <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/summative_edit">Edit Report</a></li>
                </ul>-->
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/summative_retrieve">Retrieve Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/self_assessment">Self-Assessment</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/index">Create/Edit Report</a></li></ul>
                 
            
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/selfreport">Retrieve Report</a></li>
                </ul>
                
                
                
                  </ul>
              </li>
              
              
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
             <li class="sub-menu" id="<?php echo @$idname=='classroom'? $idname:''; ?>">
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Classroom Management</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>classroom/progress_report">Student Progress Report</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_progress_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_progress_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_progress_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                          <li><a class="" href="<?php echo base_url();?>classroom/success_team">Student Success Team</a></li>
                           <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_success_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_success_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/sst_retrieve_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                              <li><a class="" href="<?php echo base_url();?>classroom/parent_teacher">Parent/Teacher Conference</a></li>
                              <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_parent_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_parent_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_parent_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="<?php echo base_url();?>classroom/teacher_student">Teacher/Student Conference</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_student_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_student_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_student_report">Retrieve</a></li>
                </ul>   
                
                  <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="<?php echo base_url();?>classroom/behavior_record">Behavior Running Record</a></li>
                                     <!-- THIRD LEVEL MENU -->
                
                  <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_behavior">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/edit_behavior">Edit</a></li>
                </ul> <!--
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_behavior">Upload</a></li>
                </ul>   -->
                  
                 
                    <li><a class="" href="<?php echo base_url();?>classroom/report_list">Retrieve Reports</a></li> 
                 
                <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/location_incident_report">Location of incidents</a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/time_incident_report">Time of Incident </a></li>
                    </ul>
              
                <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/number_of_behavior_staff_report">Number of Behavior Entrys by staff </a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/incident_report">Number of Behavior Entrys by incident type  </a></li>
                    </ul>
                      <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/month_report">Number of Behavior Entrys by month </a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/students_behavior">Number of Behavior Entrys by student </a></li>
                    </ul>
                
               
              
                
                  </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
                   <li class="sub-menu"  id="<?php echo @$idname=='assessment'? $idname:''; ?>">
                  <a href="<?php echo base_url();?>assessment/" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Assesment Manager</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
<!--                  <ul class="sub">
                      <li><a class="" href="../assessment/diagnostic.html">Diagonstic Screener</a></li>
                      <li><a class="" href="../assessment/progress.html">Progress Monitoring</a></li>
                      <li><a class="" href="../assessment/benchmark.html">Benchmark Assesment</a></li>
 </ul>-->
              </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
                <li class="sub-menu" id="<?php echo @$idname=='tools'? $idname:''; ?>">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Tools &amp; Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  	  <li><a class="" href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_notification">Planning Manager</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroomgrowthgraph">View a Classroom's Growth</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>schooldistrictreport">Class Performance Score by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>schooldistrictgraph">Class Performance Graphs by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>studentdetail">Individual Student Performance Summary</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>selectedstudentreport">Individual Student Performance by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>studentprogress">Student Progress Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/attendance_manager">Attendance Manager</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a></li>
                </ul>   
                
                  <!-- FOURTH LEVEL MENU-->                       
                <!--<ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentreport/">Grade Tracker</a></li></ul>
                    
                   -->
                    
                     <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/data_tracker_report">Grade Tracker</a></li></ul>
              
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentreport">Grade Tracker Retrieve Report</a></li>
                </ul> 
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentgraph">Grade Tracker Graph Report</a></li>
                </ul>   
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/gethomeworkstudentreport/">Homework Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/iep_tracker">IEP Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/geteldstudentreport/">ELD Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/observation">Lesson Observations</a></li>
                </ul>

<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>report/observerreport">Observation by Viewer</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>report/gradereport">Observation by Grade</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_observation">Observation Count</a></li>
                </ul>
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/dist_observation_graph">Activity Graph </a></li>
                </ul>

                
                
                
                    <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/classroom_management">Classroom Management</a></li>
                     
                </ul> 
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/student_progress_report">Student Progress Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/success_team">Student Success Team Meeting Report</a></li>
                </ul>
                
				<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report">Student Success Team by Student Report</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report_need">Student Success Team by Needs Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report_action">Student Success Team by Actions Report</a></li>
                </ul>

                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/parent_teacher">Parent/Teacher Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/teacher_student">Teacher/Student Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getstudentreport/">Behavior Learning & Running Record Report</a></li>
                </ul>
                
                            <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/report_list">Behavior Running Record Report</a></li>
                </ul>
                
				<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/incident_report">Behavior Entries By Incident Type</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/students_behavior">Behavior Entries by Student</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/location_incident_report">Behavior Entries by Location Of Incident</a></li>
                </ul>
                
                      <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/time_incident_report">Behavior Entries by Time Of Incident</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/number_of_behavior_staff_report">Number of Behavior Entrys by staff</a></li>
                </ul>
                 <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/month_report">Behavior Entries by Month</a></li>
                </ul>
                
                
                
                <!-- SECOND LEVEL MENU -->  
                      <li><a class="" href="<?php echo base_url();?>tools/professional_development">Professional Development</a></li>
                         <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>observerview/profdev">Individual Professional Development </a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>banktime">Group Professional Development</a></li>
                </ul> 
				<ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_professional">Professional Development Report</a></li>
                </ul> 
                
                <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="<?php echo base_url();?>tools/parent_bridge">Parent Bridge</a></li> 
                     <!-- THIRD LEVEL MENU -->
                      <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>communication/parents">Parent Notification</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>communication/parent_report">Parent Notification Records</a></li>
                </ul>                  
                      <li><a class="" href="<?php echo base_url();?>tools/guide">User/Training Guide</a></li>
                      
                       <li><a class="" href="<?php echo base_url();?>lesson_plan_material">Curriculum Upload</a></li>
                       
                    <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_professional">Professional Development Activity</a></li>  
                  </ul>
  
               
                    
           
    
                 
                  
                  <BR><BR>
              </li>
             
          </ul>
     
     <?php } else if($this->session->userdata('login_type')=='user' ) {?>
	 
               <ul class="sidebar-menu" <?php echo @$menustyle;?>>
              <li class="sub-menu">
                  <a class="" href="<?php echo base_url();?>">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
                <!-- FIRST LEVEL MENU Planning Manager-->
            <li class="sub-menu" id="<?php echo @$idname=='lesson'? $idname:''; ?>">
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="<?php echo base_url();?>planningmanager/lesson_plan_creator">Lesson Plan Library</a></li>
                    
                                    
                <ul class="sub">
<!--                     <li><a class="" href="<?php echo base_url();?>planningmanager/completed_plan">Create Lesson Plan Library</a></li>-->
                      <li><a class="" href="<?php echo base_url();?>planningmanager/lesson_Plan_library">Create Lesson Plan Library</a></li>
                </ul>
                
                   
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/edit_lesson_Plan_library">Edit Lesson Plan Library</a></li>
                </ul>
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/retrieve_lesson_Plan_library">Lesson Plan Rieve Report</a></li>
                </ul>
                
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/lesson_plan_graph">Lesson Plan Graph</a></li>
                </ul>
                
                
                
                   <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals">SMART Goals Review</a></li>
                     
                    
                         
                <!--<ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_retrieve_goal">Retrieve Goal</a></li>
                </ul>-->
                
                  <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_smart_goals_retrieve">Retrieve Goal</a></li>
                </ul>
                       <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/dist_retrieve_goal">View Smart Goals Review</a></li>
                </ul>
                                    
              
                     <li><a class="" href="<?php echo base_url();?>planningmanager/pdf_smart_goal">PDF Smart Goals Review</a></li>
                
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_year">Smart Goals Review PDF by Year</a></li>
                </ul>
                
                   <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/smart_goals_retrieve_subject">Smart Goals Review PDF by Subject </a></li>
                </ul>
                
                <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="<?php echo base_url();?>planningmanager/calendar">Calendar Creator</a></li>
                             
                         <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/new_event">Create New Event</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/view_your_events">View Your Events</a></li>
                </ul>
                
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>planningmanager/view_class_events">View School Events</a></li>
                </ul>
                
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu" id="<?php echo @$idname=='attendance'? $idname:''; ?>">
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Attendance Manager</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                     
                 <!-- SECOND LEVEL MENU -->
               
                      <li><a class="" href="<?php echo base_url();?>attendance/assessment">Assessment</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/daily_attendance">Daily Attendance</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/take_roll">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/edit_roll">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/new_student_enrollment">New Student Enrollment</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/modify_enrollment">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/enroll_student">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>attendance/roster">Student Roster</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="<?php echo base_url();?>attendance/create_roster">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/edit_roster">Edit</a></li>
                </ul>  
              
              
              <!-- THIRD LEVEL MENU -->
              <ul class="sub">
					  <li><a class="" href="<?php echo base_url();?>attendance/set_up">Attendance Manger Set-up</a></li></ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacher">Create Teachers</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>school_type">School Type</a></li>
                </ul>   
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>class_room">Homeroom/Classroom</a></li>
                </ul>    
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>grade_subject">Grade/Subject Association</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>teacher_grade_subject">Teacher/Grade Association</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>period">School Period Scheduling</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>time_table">Timetable Assignments</a></li>
                </ul>
                
                 <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/add_parent_info">Parent Student Association</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/individual_students">Individual Students</a></li>
                </ul>  
                
                <ul class="sub" id="fourth">
                     <li><a class="" href="<?php echo base_url();?>attendance/map_parent">Student Mapping</a></li>
                </ul>  
                
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu" id="<?php echo @$idname=='implementation'? $idname:''; ?>">
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Implementation Manager</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>implementation/grade_tracker">Grade Tracker</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/observer_feature">Enter Grade</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/get_retrieve_grade">Retrieve Grade</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       <li><a class="" href="<?php echo base_url();?>implementation/homework_tracker">Homework Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_homework">Enter Homework Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/get_retrieve_homework_record">Retrieve Homework Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
     	       <li><a class="" href="<?php echo base_url();?>implementation/iep_tracker_main_page">IEP Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/iep_tracker">Create IEP Record</a></li>
                </ul>  
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/upload_iep_tracker">Edit IEP Record</a></li>
                </ul>  
                
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/retrieve_iep_record">Retrieve IEP Record</a></li>
                </ul> 
        
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="<?php echo base_url();?>implementation/eld_tracker">ELD Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/value_eld">Enter ELD Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/retrieve_eld_record">Retrieve ELD Record</a></li>
                </ul>  
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="<?php echo base_url();?>implementation/instructional_efficacy">Instructional Efficacy</a></li>
                        
                        
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub" >
                     <li><a class="" href="<?php echo base_url();?>implementation/observation">Lesson Observation</a></li>
                </ul > 
                
                <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>report/index">Generate Report</a></li> 
                    
                    </ul>
                
                <!--
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/observation_edit">Edit Report in Progress</a></li>
                    </ul>
                    -->
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/observation_retrieve">Retrieve Report</a></li>
                    </ul>
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fifth">
                    <li><a class="" href="<?php echo base_url();?>report/teacherreport/">Full Report</a></li></ul>
                 
                <ul class="sub" id="fifth">
                    <li><a class="" href="<?php echo base_url();?>teacherreport/observer_sectional_report">Sectional Data Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/summative">Summative Report</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>report/SummativeReport">Generate Report</a></li></ul>
               <!--  
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/summative_edit">Edit Report</a></li>
                </ul>
                -->
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>implementation/summative_retrieve">Retrieve Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>implementation/self_assessment">Self-Assessment</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/index">Create/Edit Report</a></li></ul>
                 
            
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/selfreport">Retrieve Report</a></li>
                </ul>
                
                
                
                  </ul>
              </li>
              
              
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
              <li class="sub-menu" id="<?php echo @$idname=='classroom'? $idname:''; ?>">
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Classroom Management</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>classroom/progress_report">Student Progress Report</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_progress_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_progress_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_progress_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                          <li><a class="" href="<?php echo base_url();?>classroom/success_team">Student Success Team</a></li>
                           <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_success_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_success_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_success_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                              <li><a class="" href="<?php echo base_url();?>classroom/parent_teacher">Parent/Teacher Conference</a></li>
                              <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_parent_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_parent_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_parent_report">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="<?php echo base_url();?>classroom/teacher_student">Teacher/Student Conference</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_student_report">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/upload_student_report">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_student_report">Retrieve</a></li>
                </ul>   
                
                  <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="<?php echo base_url();?>classroom/behavior_record">Behavior Running Record</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/create_behavior">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/edit_behavior">Edit</a></li>
                </ul> 
                <!--
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>classroom/retrieve_behavior">Upload</a></li>
                </ul>   
                -->
                
                 <li><a class="" href="<?php echo base_url();?>classroom/report_list">Retrieve Reports</a></li> 
                 
                <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/location_incident_report">Location of incidents</a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/time_incident_report">Time of Incident </a></li>
                    </ul>
              
                <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/number_of_behavior_staff_report">Number of Behavior Entrys by staff </a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/incident_report">Number of Behavior Entrys by incident type  </a></li>
                    </ul>
                      <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/month_report">Number of Behavior Entrys by month </a></li>
                    </ul>
                    
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroom/students_behavior">Number of Behavior Entrys by student </a></li>
                    </ul>
                
                
                  </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
              <li class="sub-menu" id="<?php echo @$idname=='assessment'? $idname:''; ?>">
                  <a href="<?php echo base_url();?>assessment/" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Assesment Manager</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
<!--                  <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>assessment/diagnostic.html">Diagonstic Screener</a></li>
                      <li><a class="" href="<?php echo base_url();?>assessment/progress.html">Progress Monitoring</a></li>
                      <li><a class="" href="<?php echo base_url();?>assessment/benchmark.html">Benchmark Assesment</a></li>
 </ul>-->
              </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
              <li class="sub-menu" id="<?php echo @$idname=='tools'? $idname:''; ?>">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Tools &amp; Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  	  <li><a class="" href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>teacherself/getteacher_notification">Planning Manager</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>classroomgrowthgraph">View a Classroom's Growth</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>schooldistrictreport">District Performance Score by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>schooldistrictgraph">District Performance Graphs by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>studentdetail">Individual Student Performance Summary</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>selectedstudentreport">Individual Student Performance by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>districtwidegradelevelgraph">District Performance by Grade Level </a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/attendance_manager">Attendance Manager</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/implementation_manager">Implementation Manager</a></li>
                </ul>   
                
                  <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentreport">Grade Tracker</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/gethomeworkstudentreport">Homework Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/iep_tracker">IEP Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/geteldstudentreport">ELD Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/observation.html">Lesson Observations</a></li>
                </ul>

<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>report/observerreport">Observation by Viewer</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>report/gradereport">Observation by Grade</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>report/getteacher_notification/">Observation Count</a></li>
                </ul>
                 
                 <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/dist_observation_graph/">Activity Graph</a></li>
                </ul>

  <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>report/getteacher_professional">Professional Development Activity</a></li>
                </ul>
                
                
                
                    <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/classroom_management">Classroom Management</a></li>
                     
                </ul> 
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/student_progress_report">Student Progress Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/success_team">Student Success Team Meeting Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report">Student Success Team by Student Report</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report_need">Student Success Team by Needs Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report_action">Student Success Team by Actions Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/parent_teacher">Parent/Teacher Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/teacher_student">Teacher/Student Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>teacherself/getstudentreport/">Behavior Learning Record Report</a></li>
                </ul>
                
                    <ul class="sub" id="fourth">
                    <li><a class="" href="<?php echo base_url();?>tools/report_list">Behavior Running Record Report</a></li>
                </ul>
                
				<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/incident_report">Behavior Entries By Incident Type</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/students_behavior">Behavior Entries by Student</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/location_incident_report">Behavior Entries by Location Of Incident</a></li>
                </ul>
                
                      <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/time_incident_report">Behavior Entries by Time Of Incident</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/number_of_behavior_staff_report">Number of Behavior Entrys by staff</a></li>
                </ul>
                 <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="<?php echo base_url();?>tools/month_report">Behavior Entries by Month</a></li>
                </ul>
                
                
                <!-- SECOND LEVEL MENU -->  
                      <li><a class="" href="<?php echo base_url();?>tools/professional_development">Professional Development</a></li>
                         <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>observerview/profdev">Individual</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>banktime">Group</a></li>
                </ul> 
                 <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>tools/dist_prof_dev_ind"> Professional Development Activity Graph</a></li>
                </ul> 
                <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="<?php echo base_url();?>tools/parent_bridge">Parent Bridge</a></li> 
                     <!-- THIRD LEVEL MENU -->
                      <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>communication/parents">Parent Notification</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="<?php echo base_url();?>communication/parent_report">Parent Notification Records</a></li>
                </ul>                  
                      <li><a class="" href="<?php echo base_url();?>tools/guide">User/Training Guide</a></li>
                      
                       <li><a class="" href="<?php echo base_url();?>lesson_plan_material">Curriculum Upload</a></li>
                      
                  </ul><BR><BR>
              </li>
             
          </ul>
          
          <?php } else if($this->session->userdata('login_type')=='parent'){?>
              <?php require_once($view_path.'inc/parentmenu_new.php');?>
          <?php }?>