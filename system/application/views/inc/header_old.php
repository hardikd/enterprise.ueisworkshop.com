<!-- BEGIN HEADER -->
   <div id="header" class="navbar navbar-inverse navbar-fixed-top">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class="navbar-inner">
           <div class="container-fluid">
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class="sidebar-toggle-box hidden-phone">
                   <div class="icon-reorder"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class="brand" href="index.html">
                   <img src="<?php echo SITEURLM?>img/logo.png" alt="UEIS Workshop">
               </a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="arrow"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               <div id="top_menu" class="nav notify-row">
                   <!-- BEGIN NOTIFICATION -->
                   <ul class="nav top-menu">
                   </ul></div>  
               <div class="top-nav ">
                   <ul class="nav pull-right top-menu">
                      
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                               <i class="icon-user"></i> &nbsp;
                               <span class="username">
                                   <?php if($this->session->userdata("login_type")=='user') {
		   echo "Welcome, ".$this->session->userdata("username");
		   echo "&nbsp; | <b>Level:</b> District";

		   
		   if($this->session->userdata('product_login_type'))
		   {
		   echo "&nbsp; | <b>Level:</b> ".$this->session->userdata('product_login_type');
		   
		   }

		
		}
		else if($this->session->userdata("login_type")=='teacher') {
		   echo "Welcome, ".$this->session->userdata("teacher_name");
		   echo "&nbsp; | <b>Level:</b> Classroom";

		if($this->session->userdata('product_login_type'))
		   {
		   echo "&nbsp; | <b>Level:</b> ".$this->session->userdata('product_login_type');
		   
		   }

		}
		else if($this->session->userdata("login_type")=='observer') {
		   echo "Welcome, ".$this->session->userdata("observer_name");
		   echo "&nbsp; | <b>Level:</b> Campus";

		if($this->session->userdata('product_login_type'))
		   {
		   echo "&nbsp; | <b>Level:</b> ".$this->session->userdata('product_login_type');
		   
		   }

		}
		else if($this->session->userdata("login_type")=='parent') {
		   echo "Welcome, ".$this->session->userdata("username");
		   echo "&nbsp; | <b>Level:</b> Parent";
		
		}
		else if($this->session->userdata("login_type")=='admin') {
		   echo "Welcome, admin";
		   echo "&nbsp; | <b>Level:</b> Admin";
		
		}
		?>
                               </span>
                               <b class="caret"></b>
                           </a>
                           <ul class="dropdown-menu extended logout">
                               <?php if($this->session->userdata("login_type")!='admin' && $this->session->userdata("login_type")=='parent'){?>
                               
                               <li><a href="javascript:void(0);" onclick="javascript:changebreadcrumb('Options','changepassword','Change password')"><i class="icon-key"></i> Change Password</a></li>
                               <?php } else if($this->session->userdata("login_type")!='admin' && $this->session->userdata("login_type")=='parent') { ?>
                               <li><a href="<?php echo base_url(); ?>index/parentchangepasswrd/" ><i class="icon-key"></i> Change Password</a></li>
                               <?php }?>
                               <?php if($this->session->userdata("loginas")=='parents') {?>
                                                       <li><a href="parentlogin/logout/"><i class="icon-unlock"></i> Log Out</a></li>
                                <?php }	else if($this->session->userdata("login_type")=='admin') {?>
                                    <li><a href="admin/admin_logout/"><i class="icon-unlock"></i> Log Out</a></li>
                                <?php } else { ?>
                                    <li><a href="admin/logout/"><i class="icon-unlock"></i> Log Out</a></li>
                                <?php } ?>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                   <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->