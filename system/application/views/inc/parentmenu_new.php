<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
         <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
         <div class="navbar-inverse">
            <form class="navbar-search visible-phone">
               <input type="text" class="search-query" placeholder="Search">
            </form>
         </div>
         <!-- END RESPONSIVE QUICK SEARCH FORM -->
         <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu" <?php echo @$menustyle;?>>
              <li class="sub-menu">
                  <a class="" href="<?php echo base_url();?>">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
               <!-- FIRST LEVEL MENU Planning Manager-->
            <li class="sub-menu" id="<?php echo @$idname=='lesson'? $idname:''; ?>">
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Student Performance</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="<?php echo base_url();?>teacherself/getclassworkstudentreport">Grade Tracker Report</a></li>
                  <li><a class="" href="<?php echo base_url();?>teacherself/getstudentreport">Academic Effort Results</a></li>
                  <li><a class="" href="<?php echo base_url();?>teacherself/gethomeworkstudentreport">Homework Report</a></li>
                  <li><a class="" href="<?php echo base_url();?>tools/iep_tracker">IEP Report</a></li>
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu" id="<?php echo @$idname=='attendance'? $idname:''; ?>" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>School-to-Home</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>parentlogin/partnership">School-to-Home</a></li>
      				    </ul>
              </li>
                <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu" id="<?php echo @$idname=='attendance'? $idname:''; ?>" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Classroom</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="<?php echo base_url();?>tools/attendance_manager">Attendance Report</a></li>

                      <li><a class="" href="<?php echo base_url();?>tools/retrieve_success_report">SST Report</a></li>
                  
                      <li><a class="" href="<?php echo base_url();?>tools/teacher_student">Teacher/Student Conference</a></li>
                  
                      <li><a class="" href="<?php echo base_url();?>tools/students_behavior">Behavior Running Record</a></li>
                  </ul>
              </li>
        </ul>
         <!-- END SIDEBAR MENU -->
         
 