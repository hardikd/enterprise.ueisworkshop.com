<style>
    .lnktxt{border: none !important;color:#999;}
    .lnktxt a:hover{border: none !important;color:#000;}
    #navigation ul li{height:43px;}
    .content{min-height: 1345px !important;}
    .lnktxt a {color:#999; font-size: 14px;}
    .mm {font-size: 14px;}
	
    
</style>
<script>

    function showassessnew()
{
	if($("#behavior_records > li").css('display')=='block'){
		$("#behavior_records > li").toggle();
		$("ul #behavior_records > #performance_behavior").toggle();
		$("ul #behavior_records ").toggle();
	}
	$("#submenunew > li").toggle();
	$("ul #submenunew > #performanceassessnew").toggle();
	$("ul #submenunew ").toggle();
}
  function behavior_records()
{

if($("#submenunew > li").css('display')=='block'){
	$("#submenunew > li").toggle();
	$("ul #submenunew > #performanceassessnew").toggle();
	$("ul #submenunew ").toggle();
}
	
	$("#behavior_records > li").toggle();
	$("ul #behavior_records > #performance_behavior").toggle();
	$("ul #behavior_records ").toggle();
}

	
$(document).ready(function(){
  $("#intervention_records").click(function(){
    $("#intervention_record").show();
	$("action_records_in").hide();
  });
  $("#action_records_ins").click(function(){
    $("action_records_in").show();
	$("#intervention_record").hide();
  });
});


function showassesstt()
{
	$("#submenutt > li").show();
	$("ul #submenutt > #performanceassesstt").show();
	$("ul #submenutt ").show();
	
	$("#submenutt1 > li").hide();
	$("ul #submenutt1 > #performanceassesstt1").hide();
	$("ul #submenutt1 ").hide();	
}

function showassesstt1()
{
	$("#submenutt1 > li").show();
	$("ul #submenutt1 > #performanceassesstt1").show();
	$("ul #submenutt1 ").show();
	
	$("#submenutt > li").hide();
	$("ul #submenutt > #performanceassesstt").hide();
	$("ul #submenutt ").hide();
	
}

    </script>
    
   
<div class="lft">
    <div class="leftmtop_main" style="background:none;"></div>
  <div class="leftmenu" style="background:none;" >
<div id="navigation">
<ul>
<!--start new menu-->
  
  <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">SCHOOL/ PERSONNEL SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -110px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school','Schools')" >Schools</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	<div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observer','Observers')" >Observers(school leadership)</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','teacher','Teachers')" >Teachers</a></div>
            
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
     <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school_type','School Type')" >School Type</a></div>
            
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
    <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observer/profileimage','Profile Image')" >Profile Image</a></div>
            
          </li>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>                 
            
        <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">PERSONNEL EVALUATION SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -250px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observationgroup','Checklist Standards')" >Checklist Standards</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observationpoint','Checklist Elements')" >Checklist Elements</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubricscale','Rubric Content')" >Rubric Content</a></div>
            
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
      <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubricscalesub','Rubric Scales')" >Rubric Scales</a></div>
            
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
    <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','proficiencygroup','Proficiency Rubric Standards')" >Proficiency Rubric Standards</a></div>
      </li>
      
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
   <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','proficiencypoint','Proficiency Elements and Scales')" >Proficiency Elements and Scales</a></div>
      </li>
      
    <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
 <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lickertgroup','Likert Standards')" >Likert Standards</a></div>
      </li> 
       <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
  <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lickertpoint','Likert Elements')" >Likert Elements</a></div>
      </li> 
      
        <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
  <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lubricgroup','Leadership Rubric')" >Leadership Rubric</a></div>
      </li>  
      
       <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
  <div class="lnktxt">
         <a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lubricpoint','Leadership Elements')" >Leadership Elements</a>					       </div>
      </li>
       <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
  <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','status','Teacher H.R. Status')" >Teacher H.R. Status</a></div>
      </li>
      
       <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
<div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','score','Teacher Performance Rating')" >Teacher Performance Rating</a></div>
      </li>
      
      
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>  
            
            
           <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">INSTRUCTIONAL IMPLEMENTATION</a></div>
          <ul id="action_records" style="position:relative;margin-top: -100px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','classwork_proficiency','Classwork Proficiency')" >Classwork Proficiency</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	<div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','valueplan','Behavior & Learning Running Records')" >Behavior & Learning Running Record "Classwork Effort"</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','homework_proficiency','Homework Proficiency')" >Homework Proficiency</a></div>
            
          </li>
       
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>    

<li style="height:25px;">
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">PLANNING SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -50px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','goalplan','Goal Plans')" >Goal Plans "SMART Goals"</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	<div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','custom_differentiated','Lesson Plan Customizer')" >Lesson Plan Customizer</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>
            
       <li style="height:25px;">
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">MASTER SCHEDULER</a></div>
          <ul id="action_records" style="position:relative;margin-top: -150px;">
          <div class="leftmtop" id="performance_behavior"></div>
     
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
         <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','dist_subject','Course Subjects')" >Course Subjects</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
      <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','dist_grade','Course Grades')" >Course Grades</a></div>
          </li>
          
           <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','class_room','Homeroom/ Classroom Number')" >Homeroom/ Classroom Number</a></div>
          </li> 
             <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
         <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','grade_subject','Grade/ Subject Association')" >Grade/ Subject Association</a></div>
          </li> 
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','teacher_grade_subject','Teacher/ Grade/ Subject Association')" >Teacher/ Grade/ Subject Association</a></div>
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','period','School Period Scheduling')" >School Period Scheduling</a></div>
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
       <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','time_table','Timetable Assignments')" >Timetable Assignments</a></div>
          </li>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>     
            
            
            <li style="height:25px;">
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">CURRICULUM SETUP </a></div>
          <ul id="action_records" style="position:relative;margin-top: -140px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lesson_plan_material','Lesson Plan Material')" >Lesson Plan Material</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','standard','Instructional Standards')" >Instructional Standards</a></div>
          </li>
          
           <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubric_data','ELD Proficiency Marks')" >ELD Proficiency Marks</a></div>
          </li> 
             <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
       <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','eldrubric','ELD Rubrics')" >ELD Rubric Elements "ELD Standards & Proficiency Association"</a></div>
          </li> 
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>
            
          <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">FORMS & REPORTS SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -50px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','report_disclaimer','Data Reports Disclaimer')" >Data Reports Disclaimer (footer)</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	  <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','memorandum','Communications')" >Communications</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>  
            
                  <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">EXAM/ASSESSMENT SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -150px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tool','assesstest','Data Analysis Resources >> Assessment Documents')" style="cursor:pointer;">Assessment Documents </a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	<div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','assessscoring','Data Analysis Resources >> Assessment Scoring Guides')" style="cursor:pointer;">Assessment Scoring Guides</a></div>
          </li>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	    <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','assessmentcalendar','Data Analysis Resources >> Instructional Calendar')" style="cursor:pointer;">Instructional Calendars
 </a></div>
          </li>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	<div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','trainingmaterial','Data Analysis Resources >> Training Materials Manager')" style="cursor:pointer;">Training Materials Manager</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
                <div class="lnktxt"><a onclick="javascript:changebreadcrumb('Development Tools','addproficiency','Data Analysis Resources >> Create Proficiency Assessment Ranges')" style="cursor:pointer;">Create Proficiency Assessment Ranges </a></div>
          </li>
         
           <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
         <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','greetings_text','Assessment Letter')" >Assessment Letter</a></div>
          </li>
          
          
          
            <!-- <li>
            <div class="lnktxt"> <a href="http://<?php //echo $_SERVER['HTTP_HOST'];?>/testbank/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>-->
         <?php  if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
			?>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"> <a href="http://district.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
            
            <?php
		 } else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
      ?>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"> <a href="http://enterprise.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
            
            <?php
     }
		  else if($_SERVER["HTTP_HOST"]=="localhost"){
		 ?>
                <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"> <a href="http://localhost/client/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php
		  }
		   ?>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>  
             
            
                   <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">PROGRESS REPORT SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -70px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a href="<?php echo base_url();?>statements_score">Progress Report Marks Summary</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>standard_name ">Progress Report Marks by Standard</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>statements_standard_language">Standards & Marks Association</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>  
            
       
   
            
            <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">STUDENT SUCCESS PLANNING SETUP</a></div>
          <ul id="action_records" style="position:relative;margin-top: -50px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
          <div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onmouseover="showassesstt();" style="cursor:pointer;">Needs
</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onmouseover="showassesstt1();" style="cursor:pointer;">Action plan</a></div>
          </li>
              <li >
			<ul id="submenutt" style="position:relative;margin-top: -80px;">

          <div class="leftmtop" id="performanceassesstt"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>intervention_strategies_home ">Needs Home </a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>intervention_strategies_school">Needs School</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>intervention_strategies_medical">Needs Medical
 </a></div>
          </li>
        <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>   
            
          <li >
	      <ul id="submenutt1" style="position:relative;margin-top: -80px;">

          <div class="leftmtop" id="performanceassesstt1"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>actions_home ">Home </a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>actions_school">School</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>actions_medical">Medical
 </a></div>
          </li>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li> 
            
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>
            
            <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="behavior_records();" style="cursor:pointer;">BEHAVIOR RECORDS SETUP</a></div>
          <ul id="behavior_records" style="position:relative;margin-top: -120px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a href="<?php echo base_url();?>behavior_location">Location</a></div>
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>problem_behaviour">Behavior Type/Action Form </a></div>
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
			<div class="lnktxt"><a href="<?php echo base_url();?>possible_motivation">Possible Motivation</a></div>
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
		  <div class="lnktxt"><a href="<?php echo base_url();?>interventions_prior">Interventions Prior to Office Referral
 </a></div>
          </li>
          
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
		   <div class="lnktxt"><a href="<?php echo base_url();?>student_has_multiple_referrals">Referrals Suggested</a></div>
          </li>
         
             <li>
            <div class="leftmbot"></div>
          </li>
          
        </ul> 
            </li>
            
         <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">PARENT/ TEACHER CONFERENCE
</a></div>
          <ul id="action_records" style="position:relative;margin-top: -70px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a href="<?php echo base_url();?>strengths">Strengths</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>concerns ">Concerns</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>ideas_for_parent_student">Strategies</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>
            
            <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">STUDENT/ TEACHER CONFERENCE
</a></div>
          <ul id="action_records" style="position:relative;margin-top: -70px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a href="<?php echo base_url();?>history">History</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>behavior_teacher_student ">Behavior</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>strategy">Strategy</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>  
            
      <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">IEP Tracker
</a></div>
          <ul id="action_records" style="position:relative;margin-top: -365px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a href="<?php echo base_url();?>dates">Dates</a></div>
          </li>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>meeting_type ">Meeting Type</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>attendance_page">Attendance Page</a></div>
          </li>
             <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>eligibility">Eligibility</a></div>
          </li>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>external_impact">External Impact Factors</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>eligibility_status">Eligibility Status</a></div>
          </li>
         <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>indicate_parent_consent">Indicate Parent Consent</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>categorize_issues">Categorize Issues</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>academic_achievement">Academic Achievement</a></div>
          </li>
		   <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>english_langauge_development">English Language Development</a></div>
          </li>
		  
		   <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>performance_area">Performance Area</a></div>
          </li>
		  
		   <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>assessment_monitoring">Assessment monitoring process used</a></div>
          </li>
		   <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>state_district_assessment">State/District Assessment Results</a></div>
          </li>
		   <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>responsible_person">Responsible person administration</a></div>
          </li>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>services_grid">Services Grid</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>student_assessments">Student Assessments</a></div>
          </li>
           <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>ethnic_code">Ethnic Code</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>language">Language</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>
        <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid; margin:-15px 0px 0px 0px;"><a onclick="action_records();" style="cursor:pointer;">PLAN
</a></div>
          <ul id="action_records" style="position:relative;margin-top: -150px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
           <div class="lnktxt"><a href="<?php echo base_url();?>plan_eligibility">Eligibility</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	   <div class="lnktxt"><a href="<?php echo base_url();?>plan_curriculum ">Curriculum</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>plan_placement">Placement</a></div>
          </li>
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>instructionl_setting">Instructionl Setting</a></div>
          </li>
             <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>additional_factors">Additional Factors</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>accommodation_modifications">Accommodation Modifications</a></div>
          </li>
          
            <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
        	 <div class="lnktxt"><a href="<?php echo base_url();?>preparation">Preparation</a></div>
          </li>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li> 
            
            
             
<!--end menu-->
 
         <!-- <li>
            <div class="lnktxt"> <a href="http://<?php //echo $_SERVER['HTTP_HOST'];?>/testbank/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>-->
   <?php /*?>      <?php  if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){?>
           <li>
            <div class="lnktxt"> <a href="http://www.nanowebtech.com/testbank/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
          <?
		 }
		 else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
		 ?>
         
          <li>
            <div class="lnktxt"> <a href="https://dev.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php 
		 }
		 else if($_SERVER["HTTP_HOST"]=="estrellita.ueisworkshop.com"){
		 ?>
         
          <li>
            <div class="lnktxt"> <a href="https://dev.ueisworkshop.com/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php 
		 }
		 else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
			?>
             <li>
            <div class="lnktxt"> <a href="https://login.ueisworkshop.com/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
            
            <?php
		 }
		  else if($_SERVER["HTTP_HOST"]=="localhost"){
		 ?>
               <li>
            <div class="lnktxt"> <a href="http://localhost/WAPTrunk/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php
		  }
		   else if($_SERVER["HTTP_HOST"]=="estrellita.ueisworkshop.com"){
		 ?>
               <li>
            <div class="lnktxt"> <a href="http://localhost/WAPTrunk/estrellita/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
         <?php
		  }
		  
          else if($_SERVER["HTTP_HOST"]=="www.maven-infotech.com"){
		  ?>
             <li>
            <div class="lnktxt"> <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/demo/workshop/Quiz/login.php?msg=56" target="_blank">Workshop Student Assessment Platform</a> </div>
          </li>
          <?php 
		  } ?><?php */?>
   
               
             
          <?php /*?>  <li >
<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="intervention_records();" style="cursor:pointer;">Intervention Strategies</a></div>
          <ul id="intervention_records" style="position:relative;margin-top: -120px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>intervention_strategies_home ">Home </a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>intervention_strategies_school">School</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>intervention_strategies_medical">Medical
 </a></div>
          </li>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li>   
            
          <li >
	<div class="lnktxt" style="border-right:#e8b9b0 1px solid;"><a onclick="action_records();" style="cursor:pointer;">Action</a></div>
          <ul id="action_records" style="position:relative;margin-top: -120px;">
          <div class="leftmtop" id="performance_behavior"></div>
          
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>actions_home ">Home </a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>actions_school">School</a></div>
          </li>
          <li style="border-right:1px solid #E8B9B0;border-left:1px solid #E8B9B0;width:313px;">
            <div class="lnktxt"><a href="<?php echo base_url();?>actions_medical">Medical
 </a></div>
          </li>
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul> 
            </li> <?php */?> 
          
            
         
            

        </ul>
</div>
      <div class="leftmbot_main" style="height:5px;background:none;" id="navigation"></div>
      </div></div>
