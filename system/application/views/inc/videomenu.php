<!--<script type="text/javascript" src="<?php //echo SITEURLM?>js/jquery.js"></script>-->
<script type="text/javascript">
/*function abc()
{
	$("ul #submenu > li").hide();
	$("ul #submenu > #performanceassess").hide();
	$("ul #submenu ").hide();
	
}
function showassess()
{
	$("#submenu > li").show();
	$("ul #submenu > #performanceassess").show();
	$("ul #submenu ").show();
}	
function demo()
{
	$("ul #submenu > li").hide();
	$("ul #submenu > #performanceassess").hide();
	$("ul #submenu ").hide();
}*/

function abcnew()
{
	$("ul #submenunew > li").hide();
	$("ul #submenunew > #performanceassessnew").hide();
	$("ul #submenunew ").hide();
	
}
function showassessnew()
{
	$("#submenunew > li").show();
	$("ul #submenunew > #performanceassessnew").show();
	$("ul #submenunew ").show();
}	
function demonew()
{
	$("ul #submenunew > li").hide();
	$("ul #submenunew > #performanceassessnew").hide();
	$("ul #submenunew ").hide();
}
function abctt()
{
	$("ul #submenutt > li").hide();
	$("ul #submenutt > #performanceassesstt").hide();
	$("ul #submenutt ").hide();
	
}
function showassesstt()
{
	$("#submenutt > li").show();
	$("ul #submenutt > #performanceassesstt").show();
	$("ul #submenutt ").show();
}	
function demott()
{
	$("ul #submenutt > li").hide();
	$("ul #submenutt > #performanceassesstt").hide();
	$("ul #submenutt ").hide();
}
</script>
<?php
if($this->session->userdata('login_type')=='user') 
					{ 
					?>

<div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c"><a>S.I.S. Timetable</a></div>
      <ul>
        <div class="leftmtop"></div>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','dist_subject','Course Subjects')" >Course Subjects</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','dist_grade','Course Grades')" >Course Grades</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','class_room','Homeroom/ Classroom Number')" >Homeroom/ Classroom Number</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','grade_subject','Grade/ Subject Association')" >Grade/ Subject Association</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','teacher_grade_subject','Teacher/ Grade/ Subject Association')" >Teacher/ Grade/ Subject Association</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','period','School Period Scheduling')" >School Period Scheduling</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','time_table','Timetable Assignments')" >Timetable Assignments</a></div>
        </li>
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgDevTools"></div>
  </ul>
</div>
<?php } 
					if($this->session->userdata('login_type')=='observer') 
					{ 
					?>
<div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c" style="width:156px !important;"><a onclick="abcob2();" onmouseover="demoob2()">Development Tools</a></div>
      <ul>
        <div class="leftmtop"></div>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','teacher','Teachers')" >Teachers</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school_type','School Type')" >School Type</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observerview/profileimage','Profile Image')" >Profile Image</a></div>
        </li>
        <li>
          <div class="lnktxt"><a onmouseover="showassessnew();" style="cursor:pointer;" style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Development Tools','observerview/goalplans','Periodic Goal Setting')"  >Periodic Goal Setting</a></div>
        </li>
		<li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lesson_plan_material','Lesson Plan Material')" >Lesson Plan Material</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  onmouseover="showassessob2();" style="cursor:pointer;"  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"    <?php } else { ?>"<?php } ?>   >Data Analysis Resources</a></div>
        </li>
        <ul id="submenuob2" style="left: 158px; top: 92px !important;">
          <div class="leftmtop" id="performanceassessob2"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','assesstest','Data Analysis Resources >> Assessment Documents')" >Assessment Documents </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','assessscoring','Data Analysis Resources >> Assessment Scoring Guides')" >Assessment Scoring Guides</a></div>
          </li>
          <li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','assessmentcalendar','Data Analysis Resources >> Instructional Calendar')" >Instructional Calendars </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','trainingmaterial','Data Analysis Resources >> Training Materials Manager')" >Training Materials Manager</a></div>
          </li>
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgDevTools"></div>
  </ul>
</div>
<!--<div id="navigation">
    <ul>
      <li>
        <div class="hdtxtblank_c"><a>Performance Assessments</a></div>
        <ul>
          <div class="leftmtop"></div>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','allschoolgraph','All School Comparisons')" >Compare All Schools
   </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','allschoolbygrade','Comparison of All Schools by Grade')" >Compare All Schools by Grade Level
 </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','graphcontrollindividualschool','Individual School')" >Compare A Grade Level by School 
</a></div>
          </li>
		  <li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports')"          <?php  }  ?>   >Single Assessment Reports</a></div>
          </li>
        <li>
            <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports')"          <?php  }  ?>   >Multiple Assessment Reports</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Student Detail Reports')" >Student Detail Reports</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','graphcontroll','Classroom Graph')" >Compare A Classrooms Growth 
 </a></div>
          </li>  
 <li>
            <div class="lnktxt">
			
			<a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','classroomteacherreport','Teacher Detailed Report')" >Teacher Detailed Report</a> 
			
			</div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb(' Performance Assessments','selectedstudentreport','Student Detailed Report')" >Student Detailed Report</a> </div>
          </li>		  
        
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
      </li>
      <div class="imgDevTools"></div>
    </ul>
  </div>-->
<div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c"><a>S.I.S. Timetable</a></div>
      <ul>
        <div class="leftmtop"></div>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','class_room','Homeroom/ Classroom Number')" >Homeroom/ Classroom Number</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','grade_subject','Grade/ Subject Association')" >Grade/ Subject Association</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','teacher_grade_subject','Teacher/ Grade/ Subject Association')" >Teacher/ Grade/ Subject Association</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','period','School Period Scheduling')" >School Period Scheduling</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','time_table','Timetable Assignments')" >Timetable Assignments</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('NC')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','parents','Parent Student Association')" >Parent Student Association</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','parents/students','Students')" >Individual Students</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','parents/student_mapping','Student Mapping')" >Student Mapping</a></div>
        </li>
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgDevTools"></div>
  </ul>
</div>
<?php } 
			if($this->session->userdata('login_type')=='teacher') 
					{ 
					?>
<div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c" style="width:156px !important;"><a onclick="abctt();" onmouseover="demott()">Development Tools</a></div>
      <ul>
        <div class="leftmtop"></div>
        <li>
          <div class="lnktxt"><a style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Development Tools','students','Student Rosters')"  >Student Rosters</a></div>
        </li>
        <li>
          <div class="lnktxt"><a style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','teacherreport/profileimage','Profile Image')"  >Profile Image</a></div>
        </li>
        <li>
          <div class="lnktxt"><a style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','teacherreport/emailnotify','System E-mail Preferences')"  >System E-mail Preferences</a></div>
        </li>
        
         <li>
          <div class="lnktxt"><a style="cursor:pointer;" onmouseover="showassesstt();" >Data Analysis Resources</a></div>
        </li>
      
      
        <ul id="submenutt" style=" left: 157px; top: 72px !important;">
          <div class="leftmtop" id="performanceassesstt"></div>
            <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','assesstest','Data Analysis Resources >> Assessment Documents')" >Assessment Documents </a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','assessscoring','Data Analysis Resources >> Assessment Scoring Guides')" >Assessment Scoring Guides</a></div>
          </li>
          <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','assessmentcalendar','Data Analysis Resources >> Instructional Calendar')" >Instructional Calendars
  </a></div>
          </li>
        	     <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','trainingmaterial','Data Analysis Resources >> Training Materials Manager')" >Training Materials Manager</a></div>
          </li>
		  <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','addkinder1','Data Analysis Resources >> Manual Test Score Entry')" >Manual Test Score Entry</a></div>
          </li>
		<!--  <li>
            <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Splash Page','teacher','Training Materials Viewer')" >Training Materials Viewer</a></div>
          </li>-->
          
          <li>
            <div class="leftmbot"></div>
          </li>
        </ul>
        
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgDevTools"></div>
  </ul>
</div>
<div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c"><a>S.I.S. Timetable</a></div>
      <ul>
        <div class="leftmtop"></div>
        
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('NC')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('S.I.S. Timetable','parents','Parent Student Association')" >Parent Student Association</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','parents/students','Students')" >Individual Students</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('S.I.S. Timetable','parents/student_mapping','Student Mapping')" >Student Mapping</a></div>
        </li>
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgDevTools"></div>
  </ul>
</div>
<?php /*?><div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c"><a>Performance Assessments</a></div>
      <ul>
        <div class="leftmtop"></div>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','allschoolgraph','All School Comparisons')" >Compare All Schools </a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','allschoolbygrade','Comparison of All Schools by Grade')" >Compare All Schools by Grade Level </a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','graphcontrollindividualschool','Individual School')" >Compare A Grade Level by School </a></div>
        </li>
        <li>
          <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/single_school_report/','Single Assessment Reports')"          <?php  }  ?>   >Single Assessment Reports</a></div>
        </li>
        <li>
          <div class="lnktxt"> <a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/multiple_school_report/','Multiple Assessment Reports')"          <?php  }  ?>   >Multiple Assessment Reports</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','studentdetail','Student Detail Reports')" >Student Detail Reports</a></div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','graphcontroll','Classroom Graph')" >Compare A Classrooms Growth </a></div>
        </li>
        <li>
          <div class="lnktxt"> <a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Performance Assessments','classroomteacherreport','Teacher Detailed Report')" >Teacher Detailed Report</a> </div>
        </li>
        <li>
          <div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb(' Performance Assessments','selectedstudentreport','Student Detailed Report')" >Student Detailed Report</a> </div>
        </li>
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgDevTools"></div>
  </ul>
</div><?php */?>
<?php }
			?>
<div id="navigation">
  <ul>
    <li>
      <div class="hdtxtblank_c"><a>Video Tutorials</a></div>
      <ul>
        <div class="leftmtop"></div>
        <?php 
					if($this->session->userdata('login_type')=='user') 
					{ 
					?>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Administrator Evaluating Teacher">Administrator Evaluating Teacher</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Calendar Features">Calendar Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Communication Features">Communication Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Creating Standards Checklist">Creating Standards Checklist</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Creating Standards Likert">Creating Standards Likert</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Creating Standards Proficiency Rubric">Creating Standards Proficiency Rubric</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Creating Users">Creating Users</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Data Analysis Features">Data Analysis Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Lesson Planner">Lesson Planner</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Observation Plan Features">Observation Plan Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Periodic Goal Planner">Periodic Goal Planner</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Professional Development Features">Professional Development Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Summative Evaluation Report">Summative Evaluation Report</a></div>
        </li>
        <?php } ?>
        <?php 
					if($this->session->userdata('login_type')=='observer') 
					{ 
					?>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Calendar Features">Calendar Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Communication Features">Communication Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Data Analysis Features">Data Analysis Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Lesson Planner">Lesson Planner</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Observation Plan Features">Observation Plan Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Observation Plan Scheduling Feature">Observation Plan Scheduling Feature</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Periodic Goal Planner">Periodic Goal Planner</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Professional Development Features">Professional Development Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Summative Evaluation Report">Summative Evaluation Report</a></div>
        </li>
        <?php } ?>
        <?php 
					if($this->session->userdata('login_type')=='teacher') 
					{ 
					?>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Data Analysis Features">Data Analysis Features</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Lesson Planner">Lesson Planner</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Periodic Goal Planner">Periodic Goal Planner</a></div>
        </li>
        <li>
          <div class="lnktxt"><a href="videomenu/index/TOR - Professional Development Features">Professional Development Features</a></div>
        </li>
        <?php } ?>
        <li>
          <div class="leftmbot"></div>
        </li>
      </ul>
    </li>
    <div class="imgVideo"></div>
  </ul>
</div>