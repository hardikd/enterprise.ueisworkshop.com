<div class="lft">
<div class="leftmtop_main"></div>
<div class="leftmenu"  >
        	<?php if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer') { ?>
			
            <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Implementation</a></div>
            	<ul>
                	<div class="leftmtop"></div>
					<div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/index/','Conduct Teacher Observation')" >Conduct Teacher Observation</a></div>	
			<?php } ?>
			<?php if($this->session->userdata('login_type')=='user') { ?>
					<div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Implementation','report/leadershipcreate','Conduct Leadership Assessment')" >Conduct Leadership Assessment</a></div><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/summativereport','Conduct Summative Report')" >Conduct Summative Report</a></div>
			
                <li><div class="leftmbot"></div></li>                    
                </ul>
            </li><div class="imgImpl"></div>
            </ul>
   			</div>
			<?php } ?>
			<?php if($this->session->userdata('login_type')=='observer') { ?>
			
					<div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/leadershipcreateself/','Conduct Leadership Self Assessment')" >Conduct Leadership Self Assessment</a></div><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','report/summativereport','Conduct Summative Report')" >Conduct Summative Report</a></div><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/observer_feature','Behavior & Learning Running Record')" >Behavior & Learning Running Record</a></div>
                <li><div class="leftmbot"></div></li>                    
                </ul>
            </li><div class="imgImpl"></div>
            </ul>
   			</div>
			<?php } ?>

		
		<?php if(!empty($groups)) 
		{
		 ?>
		 <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Sections</a></div>
            	<ul>
                	<div class="leftmtop"></div>
					<?php
		foreach($groups as $val)
		{
		?>
		<li><div class="lnktxt">
		<?php if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer') { ?>
		<a href="report/proficiencypoints/<?php echo $val['group_id'];?>"><?php echo $val['group_name'];?></a>
		<?php }  ?>
		
		</div></li>
        
		<?php } 
		?>
		<li><div class="lnktxt"><a href="report/proficiencyfinish/">Save Report</a></div></li>
		<li><div class="leftmbot"></div></li>                    
                </ul>
            </li><div class="imgSections"></div>
            </ul>
   			</div>
		<?php 
		} ?>
		
		
		<?php if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer') { ?>
        	<!-- Testing for Navigation starts here -->
            <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Data Analysis</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	 <?php if($this->session->userdata('login_type')=='user') { ?> <li><div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/dashboard/','Dashboard')"   >Dashboard</a></div></li>  <?php } ?> <li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/teacherreport/','Teacher Report')"   >Teacher Report</a></div></li>
                    <li><div class="lnktxt"><a     style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/observerreport/','Observer Report')"  >Observer Report</a></div></li>                                        
                    <li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/Schoolreport/','School Report')" >School Report</a></div></li>
                    <li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/gradereport/','Grade Report')"    >Grade Report</a></div></li>
                    <li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/leadershipreport/','Leadership Report')"    >Leadership Report</a></div></li><li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','report/summative/','Summative Report')"    >Summative Report</a></div></li><li><div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_observation/','Observation Count')"   <?php  } else { ?>       style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_observation/','Observation Count')"          <?php  }  ?>   >Observation Count</a></div></li> <?php if($this->session->userdata('login_type')=='observer') { ?>  <li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/selfreport/','Teacher Self Reflection')"    >Teacher Self Reflection</a></div></li> <?php } ?><li><div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_notification/','Notification Activity Count')"   <?php  } else { ?>      style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_notification/','Notification Activity Count')"          <?php  }  ?>   >Notification Activity Count</a></div></li><li><div class="lnktxt"><a  <?php if($this->session->userdata('login_type')=='user') { ?>  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','report/getteacher_professional/','Professional Development Activity')"   <?php  } else { ?>      style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_professional/','Professional Development Activity')"          <?php  }  ?>   >Professional Development Activity</a></div></li>  <?php if($this->session->userdata('login_type')=='observer') { ?>  <li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getstudentreport/','Behavior & Learning Running Record')"    >Behavior & Learning Running Record</a></div></li><li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getclassworkstudentreport/','Classwork Performance')"    >Classwork Performance</a></div></li><li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/gethomeworkstudentreport/','Homework')"    >Homework</a></div></li><li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/geteldstudentreport/','English Language Development')"    >English Language Development</a></div></li> <?php } ?> 
                    <li><div class="leftmbot"></div></li>                    
                </ul>
            </li>
            <div class="imgReport"></div>
            </ul>
   			</div>
            <!-- Testing for Navigation ends here -->
            <?php } ?>		 
		 
		
		<?php if($this->session->userdata('login_type')=='teacher') { ?>
		<div id="navigation">
		<ul>
            <li><div class="hdtxtblank_c"><a>Implementation</a></div>
            	<ul>
                	<div class="leftmtop"></div>
					<li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/index/','Conduct Self Reflection')" >Conduct Self Reflection</a></div>	</li><li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/value_feature/','Behavior & Learning  Running Record')" >Behavior & Learning  Running Record</a></div>	</li><li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/value_homework/','Homework')" >Homework</a></div>	</li><li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Implementation','teacherself/value_eld/','English Language Development')" >English Language Development</a></div>	</li>
			        <li><div class="leftmbot"></div></li> 
               	</ul>
             </li>
             <div class="imgImpl"></div>	
             </ul>
			</div> 
			 
		<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Data Analysis</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport','Teacher Report')"   >Teacher Report</a></div></li> <li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport/observer','Observer Report')"  >Observer Report</a></div></li>		<li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport/grade','Grade Report')"    >Grade Report</a></div></li>		<li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherreport/summative','Summative Report')"    >Summative Report</a></div></li><li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_observation','Observation Count')"    >Observation Count</a></div></li><li><div class="lnktxt"><a     style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/selfreport','Self Reflection Report')"    >Self Reflection Report</a></div></li><li><div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_notification','Notification Activity Count')"    >Notification Activity Count</a></div></li><!--<li><div class="lnktxt"><a  href="teacherself/getteacher_professional"   >Professional Development Activity</a></div></li>--><li><div class="lnktxt"><a   style="cursor:pointer;" onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getteacher_consolidated','P.D. & Consolidated Activity Reports')"    >P.D. & Consolidated Activity Reports</a></div></li>		<li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getstudentreport','Behavior & Learning Running Record')"    >Behavior & Learning Running Record</a></div></li><li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/getclassworkstudentreport','Classwork Performance')"    >Classwork Performance</a></div></li><li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/gethomeworkstudentreport','Homework')"    >Homework</a></div></li><li><div class="lnktxt"><a    style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','teacherself/geteldstudentreport','English Language Development')"    >English Language Development</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Data Analysis','parents/partnership','Partnership Report')" >Partnership Report</a></div></li>
		<li><div class="leftmbot"></div></li> 
		</ul>
		</li>
        <div class="imgReport"></div>
		</ul>
		</div>
		
		<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Planning</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','teacherplan/schedule','Calendared Activities')" >Calendared Activities</a></div></li>
	<li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','teacherreport/goalplan','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div></li>
	<li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','teacherplan','Lesson Plan Book')" >Lesson Plan Book</a></div></li>
	<li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','teacherreport/observationplan','Observation Plans')" >Observation Plans</a></div></li>
	<li><div class="leftmbot"></div></li>
	</ul>
		</li>
        <div class="imgPlan"></div>
		</ul>
		</div> 	
	
	
	<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Communications</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','teacherreport/memorandums','Correspondences')" >Correspondences</a></div></li>
		<li><div class="leftmbot"></div></li>
	</ul>
		</li>
        <div class="imgComm"></div>
		</ul>
		</div> 	
	
	<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Capacity Building</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','teacherplan/videos','Individualized Professional Development')" >Individualized Professional Development</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div></li>
		<li><div class="leftmbot"></div></li>
	</ul>
		</li><div class="imgCapBld"></div>
		</ul>
		</div> 	
		
		<?php } ?>
		
           <?php if($this->session->userdata('login_type')=='observer') { ?>
		   
		   <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Planning</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','observerview/schedule','Calendared Activities')" >Calendared Activities</a></div></li>
	<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','observerview/comments','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div></li>
	<li><div class="lnktxt"><a   style="cursor:pointer;<?php if( $this->session->userdata('LP')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','observerview/lessoncomments','Lesson Plan Book')" >Lesson Plan Book</a></div></li>
	<li><div class="lnktxt"><a  style="cursor:pointer;<?php if( $this->session->userdata('TE')==0) { ?>color:#cccccc;"  onclick="javascript:void(0)"  <?php } else { ?>"<?php } ?>  onclick="javascript:changebreadcrumb('Planning','observerview/answer','Observation Plans')" >Observation Plans</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','index/observerindex','Dashboards')" >Dashboards</a></div></li>
	<li><div class="leftmbot"></div></li>
	</ul>
		</li><div class="imgPlan"></div>
		</ul>
		</div>
		
		<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Communications</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/memorandums','Create Memorandum')" >Create Memorandum</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/savedmemorandums','Saved Documents')" >Saved Documents</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/assign','Memorandums in Progress')" >Memorandums in Progress</a></div></li>
				<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','observerview/assigned','Memorandum Records')" >Memorandum Records</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/index','Staff Communication')" >Staff Communication</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/staff_report','Staff Communication Records')" >Staff Communication Records</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/parents','Parent Notification')" >Parent Notification</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','communication/parent_report','Parent Notification Records')" >Parent Notification Records</a></div></li>
				<li><div class="leftmbot"></div></li>
	</ul>
		</li><div class="imgComm"></div>
		</ul>
		</div>
				<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Capacity Building</a></div>
            	<ul>
                	<div class="leftmtop"></div> 
		<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','observerview/profdev','Individualized Professional Development')" >Individualized Professional Development</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div></li>
		   <li><div class="leftmbot"></div></li>
	</ul>
		</li><div class="imgCapBld"></div>
		</ul>
		</div>
		   
		   <?php } ?>
		   
		   <?php if($this->session->userdata('login_type')=='user') { ?>
		           	<!-- Testing for Navigation starts here -->
            <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Planning</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','goalplan/comments','Periodic Goal-Setting')" >Periodic Goal-Setting</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','distplans','Observation Plans')" >Observation Plans</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','index/userindex','Notifications')" >Notifications</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Planning','index/userindex','Dashboards')" >Dashboards</a></div></li>                                        
                    <li><div class="leftmbot"></div></li>                    
                </ul>
            </li><div class="imgPlan"></div>
            </ul>
   			</div>
            <!-- Testing for Navigation ends here -->

			<!-- Testing for Navigation starts here -->
            <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Communications</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','memorandum/memorandumsall','Documents in Progress')" >Documents in Progress</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','memorandum/savememorandumsall','Saved Documents')" >Saved Documents</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Communications','memorandum/assigned','Records')" >Records</a></div></li>                                        
                    <li><div class="leftmbot"></div></li>                    
                </ul>
            </li><div class="imgComm"></div>
            </ul>
   			</div>
            <!-- Testing for Navigation ends here -->

			<!-- Testing for Navigation starts here -->
            <div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_c"><a>Capacity Building</a></div>       	<ul>  	<div class="leftmtop"></div> <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Capacity Building','banktime','Professional Learning Community Discussions')" >Professional Learning Community Discussions</a></div></li>   <li><div class="leftmbot"></div></li>	</ul>		</li><div class="imgCapBld"></div> 	</ul>	</div> <div id="navigation">   	<ul> <li><div class="hdtxtblank_c"><a>Development Tools</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school','Schools')" >Schools</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observer','Observers')" >Observers</a></div></li>  
					<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','teacher','Teachers')" >Teachers</a></div></li> <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','school_type','School Type')" >School Type</a></div></li>
					<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observer/profileimage','Profile Image')" >Profile Image</a></div></li>															
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observationgroup','Checklist Standards')" >Checklist Standards</a></div></li>				  
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','observationpoint','Checklist Elements')" >Checklist Elements</a></div></li>	
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubricscale','Rubric Content')" >Rubric Content</a></div></li>			 
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubricscalesub','Rubric Scales')" >Rubric Scales</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','proficiencygroup','Proficiency Rubric Standards')" >Proficiency Rubric Standards</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','proficiencypoint','Proficiency Elements and Scales')" >Proficiency Elements and Scales</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lickertgroup','Likert Standards')" >Likert Standards</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lickertpoint','Likert Elements')" >Likert Elements</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lubricgroup','Leadership Rubric')" >Leadership Rubric</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lubricpoint','Leadership Elements')" >Leadership Elements</a></div></li>
					<li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','goalplan','Goal Plans')" >Goal Plans</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','lesson_plan_material','Lesson Plan Material')" >Lesson Plan Material</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','standard','Instructional Standards')" >Instructional Standards</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','valueplan','Behavior & Learning Running Records')" >Behavior & Learning Running Records</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','classwork_proficiency','Classwork Proficiency')" >Classwork Proficiency</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','homework_proficiency','Homework Proficiency')" >Homework Proficiency</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','custom_differentiated','Lesson Plan Customizer')" >Lesson Plan Customizer</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','rubric_data','ELD Proficiency Marks')" >ELD Proficiency Marks</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','eldrubric','ELD Rubrics')" >ELD Rubrics</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','report_disclaimer','Data Reports Disclaimer')" >Data Reports Disclaimer</a></div></li>
                    <li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','memorandum','Communications')" >Communications</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','status','Teacher H.R. Status')" >Teacher H.R. Status</a></div></li><li><div class="lnktxt"><a  style="cursor:pointer;" onclick="javascript:changebreadcrumb('Development Tools','score','Teacher Performance Rating')" >Teacher Performance Rating</a></div></li>
                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li>
            <div class="imgDevTools"></div>
            </ul>
   			</div>
            <!-- Testing for Navigation ends here --> 		   
		   <?php } ?>
	
    		<!-- Testing for Navigation starts here -->
            <?php include(MEDIA_PATH.'inc/videomenu.php'); ?>
            <!-- Testing for Navigation ends here --> 
	
    		<!-- Testing for Navigation starts here -->
            <!--<div id="navigation">
	    	<ul>
            <li><div class="hdtxtblank_o"><a>Off-Line</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt">GUI</div></li>
                    <li><div class="lnktxt">Navigation</div></li>
                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li><div class="imgOffline"></div>
            </ul>
   			</div>-->
            <!-- Testing for Navigation ends here --> 
            
            <!-- Testing for Navigation starts here -->
            <!--<div id="navigation" style="margin-bottom:-10px;">
	    	<ul>
            <li><div class="hdtxtblank_o"><a>On-Line</a></div>
            	<ul>
                	<div class="leftmtop"></div>                    				
                	<li><div class="lnktxt">GUI</div></li>
                    <li><div class="lnktxt">Demo</div></li>
                                    <li><div class="leftmbot"></div></li>                
                </ul>
            </li>
            <div class="imgOnline"></div>
            </ul></div>-->
            <div class="leftmbot_main" style="height:5px;" id="navigation"></div>
            <!-- Testing for Navigation ends here --> 
            
</div>
        
</div>