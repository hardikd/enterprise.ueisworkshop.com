﻿  <table class='table table-striped table-bordered' id='editable-sample'>
     <tr>
     <td>Id</td>
     <td>District_id</td>
    <td>Score</td>
     <td>Status</td>
     <td>Edit</td>
     <td>Remove</td>
     
     <?php 
   $con=1+($page -1)*10;
   $cntr = 1;
foreach($alldata as $data)
{?>
     </tr>
     <tr id="score_<?php echo $data->id;?>">
   <td><?php echo $cntr;?></td>
   <td><?php echo $data->district_id;?></td>
  <td><?php echo $data->grade;?></td>
   <td><?php echo $data->status;?></td>
      <td>  <button type="button" class="btn btn-primary edit_statements_score" data-dismiss="modal" value="Edit" name="<?php echo $data->id;?>" aria-hidden="true" ><i class="icon-pencil"></i></button></td>
             <input  type="hidden" name="prob_behaviour_id" id="prob_behaviour_id" value="<?php echo $data->id;?>" />
                <td><button data-dismiss="modal" type="Submit" value="Remove" name="<?php echo $data->id;?>" class="btn btn-danger remove_statements_score" id="remove_statements_score"><i class="icon-trash"></i></button></td>
     
  </tr>
  <?php  
  $con++;
  $cntr++;
  }
  ?>
     
     
     </table>
     <?php print $pagination;?>
   <script>
     $('.edit_statements_score').click(function(){
  var id = $(this).attr('name');
  $.ajax({
       type: "POST",
       url: "<?php echo base_url().'report_grade/edit';?>/"+id,
       success: function(data)
       {
       var result = JSON.parse(data);
       $('#statements_score_id').val(result[0].id);
       $('#score').val(result[0].grade);
       $('#status').val(result[0].status);
       console.log(result[0].score);
       $("#dialog").dialog({
      modal: true,
            height:300,
      width: 400
      });
       }
     });  
});

$(".remove_statements_score").click(function(){
var id = $(this).attr("name");
    $(".dialog").dialog({
      buttons : {
        "Confirm" : function() {
         $.ajax({
      type: "POST",
      url: "<?php echo base_url().'report_grade/delete';?>",
      data: { 'id': id},
      success: function(msg){
        console.log(msg);
        if(msg=='DONE'){
          $("#score_"+id).css('display','none');
          alert('Successfully removed!!');
        }
        
        }
      });
       $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $(".dialog").dialog(function(){
      
    });
  
  
    return false;
    
    
  });

</script>

