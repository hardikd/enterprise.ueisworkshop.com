<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/adapters/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/sample.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/sample.css" type=text/css rel=stylesheet>
<script type="text/javascript">
$(function()
{	
	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.ckeditor1').ckeditor();

	});
function check1()
{
if($('#subject').val()=='')
		   {
		     alert('Please Enter Subject');
			 return false;
		   
		   }
if($('#text').val()=='')
		   {
		     alert('Please Enter Memo');
			 return false;
		   
		   }
		   
		  

           return true;
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
	<?php 
		
		if($this->session->userdata("login_type")=='observer')
		{
			require_once($view_path.'inc/observermenu.php');
		}
		else
		{
			require_once($view_path.'inc/developmentmenu.php'); 
		
		}
		?>
    	
        <div class="content">
		
			<form action="memorandum/memorandum_save" method="post">
		
		
		<table>
		<?php if(!empty($message)) { ?>
		<tr>
		<td>
		<font ><?php echo $message;?></font>
		</td>
		</tr>
		<?php } ?>
		
		<tr>
		<td>Subject:&nbsp;<?php echo $subject ;?>
		<input type="hidden" name="subject" id="subject" value="<?php echo $subject;?>">
		<input type="hidden" name="saved" id="saved" value="saved">
		</td>
		</tr>
		<tr>
		<td>
		Memo&nbsp;&nbsp;:&nbsp;<b><?php echo $warning ;?></b></td>
		</tr>
		<tr>
		<td valign="top" style="height:40px;">
				<textarea class="ckeditor1"   id='text' name='text'><?php echo $memo;?></textarea>
				<input type="hidden" name="approve_id" id="approve_id" value="<?php echo $approve_id;?>">
				</td>
		</tr>
		
		
		<tr>
		<td >Select Status:
		<?php if($status=='NOTAPPROVED') { ?>
		<select class="combobox1" name="status" id="status"  >
		<option value="NOTAPPROVED">NOT APPROVED</option>
		<option value="APPROVED">APPROVED</option>
		</select>
		<?php } else { 
		echo $status;
		?>
		 
		<?php } ?>
		</td>
		
		</tr>
		<?php if($status=='NOTAPPROVED') { ?>
		<tr>
		<td align="center" >
		
		<input  class="btnsmall" type="submit" name="submit" id="submit" value="Submit" onclick="return check1()">
		
		</td>		
		</tr>
		<?php } ?>
		</table>
		</form>
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
