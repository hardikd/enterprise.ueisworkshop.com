<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/assign.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
		<table align="center" cellpadding="5">
		<tr>
		
		<td >Select Teacher:</td>
		<td>
		<input type="hidden" name="school" id="school" value="<?php echo $this->session->userdata("school_id");?>" >
		<select class="combobox1" name="teacher" id="teacher"  >
		<?php if($teacher!=false) { ?>
		<option value="all">All</option>
		<?php foreach($teacher as $val)
		{
		?>
		<option value="<?php echo $val['teacher_id'];?>"  ><?php echo $val['firstname'].' '.$val['lastname'];?></option>
		<?php } } else { ?>
		<option value="0">No Teachers Found</option>
		<?php } ?>
		</select>
		</td>
		
		</tr>
		<tr>
		<td colspan="4" align="center">
		<input  class="btnsmall" type="button" name="getmemorandumsasigned" id="getmemorandumsasigned" value="Submit">
		</td>
		</tr>
		</table>
		<div id="memorandumdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
		<table width="100%" ><tr><td><div style="float:right;"><a href="http://get.adobe.com/reader/" target="_blank" style="color:#ffffff;text-decoration:none;"><img src="<?php echo SITEURLM?>images/get_adobe_reader.png"></a></div></td></tr></table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
