<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/saveapproveobserver.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/developmentmenu.php'); ?>
        <div class="content">
		<table align="center" cellpadding="5">
		<tr>
		<td >Select School:</td>
		<td>
		<select class="combobox1" name="school" id="school" onchange="school1_change(this.value)" >
		<?php if($school!=false) { 
		foreach($school as $val)
		{
		?>
		<option value="<?php echo $val['school_id'];?>"  ><?php echo $val['school_name'];?></option>
		<?php } } else { ?>
		<option value="">No Schools Found</option>
		<?php } ?>
		</select>
		</td>
		<td >Select observer:</td>
		<td>
		<select class="combobox1" name="observer" id="observer"  >
		<?php if($observer!=false) { ?>
		<option value="0">-Please Select-</option>
		<?php foreach($observer as $val)
		{
		?>
		<option value="<?php echo $val['observer_id'];?>"  ><?php echo $val['observer_name'];?></option>
		<?php } } else { ?>
		<option value="0">No Observers Found</option>
		<?php } ?>
		</select>
		</td>
		
		</tr>
		<tr>
		<td colspan="4" align="center">
		<input  class="btnsmall" type="button" name="getmemorandumsasigned" id="getmemorandumsasigned" value="Submit">
		</td>
		</tr>
		</table>
		<div id="memorandumdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
		
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
