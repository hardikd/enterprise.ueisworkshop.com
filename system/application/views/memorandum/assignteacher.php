<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/adapters/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/sample.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/sample.css" type=text/css rel=stylesheet>
<script type="text/javascript">
$(function()
{	
	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('.ckeditor1').ckeditor();

	});
function check1()
{
if($('#subject').val()=='')
		   {
		     alert('Please Enter Subject');
			 return false;
		   
		   }
if($('#text').val()=='')
		   {
		     alert('Please Enter Memo');
			 return false;
		   
		   }
		   if($('#district').val()=='')
		   {
		     alert('Please Select District User');
			 return false;
		   
		   }
		  

           return true;
}
function check2()
{
if($('#subject').val()=='')
		   {
		     alert('Please Enter Subject');
			 return false;
		   
		   }
if($('#text').val()=='')
		   {
		     alert('Please Enter Memo');
			 return false;
		   
		   }
		   if($('#teacher').val()=='')
		   {
		     alert('Please Select teacher');
			 return false;
		   
		   }
		  

           return true;
}
function check3()
{
if($('#subject').val()=='')
		   {
		     alert('Please Enter Subject');
			 return false;
		   
		   }
if($('#text').val()=='')
		   {
		     alert('Please Enter Memo');
			 return false;
		   
		   }
		   
		  

           return true;
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
	<?php 
		
		if($this->session->userdata("login_type")=='observer')
		{
			require_once($view_path.'inc/observermenu.php');
		}
		else
		{
			require_once($view_path.'inc/developmentmenu.php'); 
		
		}
		?>
    	
        <div class="content">
		
			<form action="observerview/memorandum_approve" method="post">
		
		
		<table>
		<?php if(!empty($message)) { ?>
		<tr>
		<td>
		<font color="red"><?php echo $message;?></font>
		</td>
		</tr>
		<?php } ?>
		
		<tr>
		<td>Subject:&nbsp;<input type="textbox" class="txtbox" name="subject" id="subject" value=""></td>
		</tr>
		<tr>
		<td>
		Memo&nbsp;&nbsp;:&nbsp;<b><?php echo $subject ;?></b></td>
		</tr>
		<tr>		
		<td valign="top" style="height:40px;" colspan="2"><textarea class="ckeditor1"   id='text' name='text'><?php echo $memo;?></textarea>
		<input type="hidden" name="memorandum" id="memorandum" value="<?php echo $memo_id;?>">
		</td>
		</tr>		
		 <tr>		 
		 <td>
		 <b>Save Drafts</b>&nbsp;&nbsp;&nbsp;
		 <input  class="btnsmall" type="submit" name="submit" id="save" value="Save" onclick="return check3()">
		 </td>
		 </tr>
		 <tr>
		 <td>
		 &nbsp;
		 </td>
		 </tr>
		 <tr>
		 <td>
		 <b>Forward Document</b>
		 </td>
		 </tr>
		 
		<tr>
		<td>
		<table>
		
		<tr>
		<td >Select District User:
		<select class="combobox1" name="district" id="district"  >
		<option value="">-Select-</option>
		<?php if($district!=false) { 
		foreach($district as $val)
		{
		?>
		<option value="<?php echo $val['dist_user_id'];?>"  ><?php echo $val['username'];?></option>
		<?php } } else { ?>
		<option value="">No District users Found</option>
		<?php } ?>
		</select>
		</td>
		
		</tr>
		<tr>
		<td align="center">
		<input  class="btnsmall" type="submit" name="submit" id="submit" value="Review" onclick="return check1()">
		</td>
		</tr>
		</table>
		</td>
		<td>
		<table>
		<tr>
		<td >Select Teacher:</td>
		<td>
		<select class="combobox1" name="teacher" id="teacher"  >
		<option value="">-Select-</option>
		<?php if($teacher!=false) { 
		foreach($teacher as $val)
		{
		?>
		<option value="<?php echo $val['teacher_id'];?>"  ><?php echo $val['firstname'].' '.$val['lastname'];?></option>
		<?php } } else { ?>
		<option value="">No Teachers Found</option>
		<?php } ?>
		</select>
		</td>
		</tr>
		<tr>
		<td align="center" colspan="2">
		<input  class="btnsmall" type="submit" name="submit" id="submit" value="Submit" onclick="return check2()">
		</td>
		</tr>
		</table>
		</td>
		</tr>
		 
		</table>
		</form>
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
