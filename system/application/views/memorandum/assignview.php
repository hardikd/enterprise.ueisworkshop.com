<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php 
		
		if($this->session->userdata("login_type")=='observer')
		{
			require_once($view_path.'inc/observermenu.php');
		}
		else
		{
			require_once($view_path.'inc/developmentmenu.php'); 
		
		}
		?>
        <div class="content">
		<table width="100px" align="right">
			<tr>
			<td>
			<a href="comments/responsepdf/<?php echo $assign_id;?>" style="text-decoration:none;color:#FFFFFF;" target="_blank"><img src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
			</td>
			</tr>
			</table>
		<table>
		<tr>
		<td>
		<b>Teacher Name:</b><?php echo $teachername;?>
		</td>
		</tr>
		<tr>
		<td valign="top" style="height:40px;">
				<b>Subject :</b><?php echo $subject;?>
				
				</td>
		</tr>
		<tr>
		<td valign="top" style="height:40px;">
				<b>Memo :</b><?php echo $memo;?>
				
				</td>
		</tr>
		<tr>		
		<td>
		<b>Response:</b><?php if(!empty($response)) { echo $response; } else { echo ' No Response Found' ;}?>
		</td>		
		</tr>
		</table>
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
