<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM.$view_path; ?>js/assign.js" type="text/javascript"></script>
<script type="text/javascript">
function check()
{


if($('#teacher').val()=='')
		   {
		     alert('Please Select Teacher');
			 return false;
		   
		   }
		   if($('#memorandum').val()=='')
		   {
		     alert('Please Select Memorandum');
			 return false;
		   
		   }

           return true;
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
		<form action="observerview/assignedmemoteacher" method="post">
		<table align="center" cellpadding="5">
		<tr>
		<td>
		<?php if(isset($message)) { echo $message; } ?>
		</td>
		</tr>
		<?php /* <tr>
		<td >Select Teacher:</td>
		<td>
		<select class="combobox1" name="teacher" id="teacher"  >
		<option value="">-Select-</option>
		<?php if($teacher!=false) { 
		foreach($teacher as $val)
		{
		?>
		<option value="<?php echo $val['teacher_id'];?>"  ><?php echo $val['firstname'].' '.$val['lastname'];?></option>
		<?php } } else { ?>
		<option value="">No Teachers Found</option>
		<?php } ?>
		</select>
		</td>
		
		</tr> */ ?>
		<tr>
		<td  align="right">Select Memorandum:</td>
		<td colspan="2" align="left">
		<select class="combobox1" name="memorandum" id="memorandum"  >
		<option value="">-Select-</option>
		<?php if($memorandum!=false) { 
		foreach($memorandum as $val)
		{
		?>
		<option value="<?php echo $val['approve_id'];?>"  ><?php echo $val['subject'];?></option>
		<?php } } else { ?>
		<option value="">No Memorandums Found</option>
		<?php } ?>
		</select>
		</td>
		
		</tr>
		<tr>
		<td colspan="4" align="center">
		<input  class="btnsmall" type="submit" name="submit" id="submit" value="Submit" onclick="return check()">
		</td>
		</tr>
		</table>
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
