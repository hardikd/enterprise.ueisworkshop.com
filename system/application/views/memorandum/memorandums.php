<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Memorandum::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript">
function check()
{		   
			if($('#memorandum').val()=='')
		   {
		     alert('Please Select Memorandum');
			 return false;
		   
		   }
		   

           return true;
}
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/headerv1.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
		<form action="observerview/memorandum_edit" method="post">
		<table align="center" cellpadding="5">
		<tr>
		<td>
		<?php if(isset($message)) { echo $message; } ?>
		</td>
		</tr>
		<tr>
		<td  align="right">Select Document:</td>
		<td colspan="2" align="left">
		<select class="combobox1" name="memorandum" id="memorandum"  style="width:350px;">
		<option value="">-Select-</option>
		<?php if($memorandum!=false) { 
		foreach($memorandum as $val)
		{
		?>
		<option value="<?php echo $val['memorandum_id'];?>"  ><?php echo $val['subject'];?></option>
		<?php } } else { ?>
		<option value="">No Documents Found</option>
		<?php } ?>
		</select>
		</td>
		
		</tr>
		
		<tr>
		<td colspan="4" align="center">
		<input  class="btnsmall" type="submit" name="submit" id="submit" value="Submit" onclick="return check()">
		</td>
		</tr>
		</table>
		
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
