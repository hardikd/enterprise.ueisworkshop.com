<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<style type="text/css">
.button_next {
    background-color: #5e3364;
    border: 0 none;
    color: #FFFFFF;
    cursor: default;
	 float: right;
    margin-left: 5px;
	 border-radius: 15px;
    display: inline-block;
    padding: 5px 14px;
	 text-shadow: none !important;
	 border-radius: 0 !important;
	 outline: 0 none;
	 text-decoration: none;
	  line-height: 20px;
	  list-style: none outside none;
    text-align: center;
}
</style>


</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
   <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
     <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-group"></i>&nbsp; Classroom Management
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools/classroom_management">Classroom Management</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/parent_teacher">Parent/Teacher Conference</a>
                            
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Retrieve Report</h4>
                          
                         </div>
                         <div class="widget-body">

                                <div id="pills" class="custom-wizard-pills-purple2">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab" id="step2">Step 2</a></li>
                                    
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-purple progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <div class="form-horizontal" >
               <form method="post" action="<?php echo base_url().'tools/retrieve_parent_teacher';?>" name="retrieve_parent" id="retrieve_parent">
                                 
                                        
                                 
                                 
                                
                     <?php if($this->session->userdata("login_type")=='user' ) { ?>                     
                                        
                                       <?php if(isset($school_type)) $countst = count($school_type);
								if(isset($school_type) && $countst >0){ ?>   
                                             <div class="control-group">
                                  
                                             <label class="control-label">Select School Type :</label>
                                             <div class="controls">
<select class="span12 chzn-select" name="schools_type" id="schools_type" tabindex="1" style="width: 300px;" onchange="updateschool(this.value);">
      									<option value=""  selected="selected">Please Select</optgroup>
									       <?php  foreach($school_type as $k => $v)
	  										{
											?>
								           <option value="<?php echo $v['school_type_id']; ?>"><?php echo $v['tab']; ?></option>
						            	  <?php } ?>
                        			    </select>
                                             </div>
                                         </div> 
                                         
                                          <div class="control-group">
                                             <label class="control-label">Schools :</label>
                                             <div class="controls">
				<select class="span12 chzn-select" name="school_id" id="schools" tabindex="1" style="width: 300px;" onchange="updateclass(this.value)">
                 	<option value=""  selected="selected">Please Select</option>
              						</select>
                                             </div>
                                         </div> 
                                         
               
        <?php }?>      
                                        
                                        <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
		 <select class="span12 chzn-select" name="grade_id" id="grades" onchange="updateclassroom(this.value)" tabindex="1" style="width: 300px;">
             
                   <option value=""  selected="selected">Please Select</option>
                    <?php
					if(isset($school_grade)) $countst = count($school_grade["grades"]);
					if(isset($school_grade) && $countst >0){
						
					foreach($school_grade["grades"] as $k=> $v)
					{
					?>
					 <option value="<?php echo $v["dist_grade_id"];?>"> <?php echo $v["grade_name"]; ?></option>
					<?php } }?>
            	   </select>
                  </div>
                  <?php if(isset($school_grade) && $countst >0){?>
			         <input type="hidden" name="schools" id="schools" value="<?php echo $school_grade["school_id"][0]["school_id"];?>" />
         		<?php }?>
                </div>  
                                         
                                               <div class="control-group">
                        <label class="control-label">Teachers :</label>
                           <div class="controls">
                   		 <select class="combobox span12 chzn-select" name="teacher_id" id="classrooms" style="width: 300px;" >
               <option value=""  selected="selected">Please Select</option>
          </select>
                         </div>
                      </div> 
                      
                    <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select name="student_id" id="students" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <option value='all'>All</option>
 		
		      </select> 
              </div>
      </div> 
                                           
                       <?php } elseif($this->session->userdata("login_type")=='teacher') {?>
                      
<!--                                                    <div class="control-group">
        <label class="control-label">Select Student</label>
        <div class="controls">
          <select name="student_id" id="student" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
            <option value=''></option>
            <option value='all'>All</option>
 			<?php if(!empty($students)) { 
			foreach($students as $studentsvalue)
			{?>
			  <option value="<?php echo $studentsvalue['student_id'];?>"><?php echo $studentsvalue['firstname'];?> <?php echo $studentsvalue['lastname'];?></option>
			  <?php } } ?>			   
		      </select> 
              </div>
      </div> -->
                   <div class="control-group">
                    <label class="control-label">Select Student</label>
                    <div class="controls">
                        <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;" autocomplete="off"  />
                        <input type="hidden" name="student_id" id="student_id"  />
                    </div>
                </div>
                                                     
                                                        <div class="control-group">
                                                            <label class="control-label">Select Grade</label>
                                                            <div class="controls">
                                                                <select class="span12 chzn-select" name="grade_id" id="grade_id" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    if (!empty($grades)) {
                                                                        foreach ($grades as $val) {
                                                                            ?>
                                                                            <option value="<?php echo $val['grade_id']; ?>" ><?php echo $val['grade_name']; ?>  </option>
                                                                        <?php }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                         
                      <?php } else if($this->session->userdata("login_type")=='observer'){?>
                        <div class="control-group">
                        <label class="control-label">Teachers :</label>
                           <div class="controls">
                   		 <select class="combobox span12 chzn-select" name="teacher_id" id="classrooms" style="width: 300px;" >
                                    <option value=""  selected="selected">Please Select</option>
                                    <?php if(!empty($teachers)) { 
                                        foreach($teachers as $teachervalue)
                                         {?>
               <option value="<?php echo $teachervalue['teacher_id'];?>"><?php echo $teachervalue['firstname'];?> <?php echo $teachervalue['lastname'];?></option>
                                         <?php }?>
                                          <?php } ?>
                               </select>
                            </div>
                         </div> 

<!--                            <div class="control-group">
                             <label class="control-label">Select Student</label>
                             <div class="controls">
                               <select name="student_id" id="students" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                 <option value=''></option>
                                 <option value='all'>All</option>

                                           </select> 
                                   </div>
                           </div> -->
<div class="control-group">
                    <label class="control-label">Select Student</label>
                    <div class="controls">
                        <input type="text" class="span6 " name="studentlist" id="studentlist" style="width:300px;" autocomplete="off"  />
                        <input type="hidden" name="student_id" id="student_id"  />
                    </div>
                </div>
                      <?php }?>
                              <!--  
                                  <div class="control-group">
                                    <label class="control-label">Select Date</label>
                                    <div class="controls">
                                        <input id="dp1" type="text" name="date" value="" size="16" class="m-ctrl-medium"></input>
                                    </div>
                                </div>  -->  
                                
                                <div class="control-group">
                                                        <label class="control-label">Select Year</label>
                                                        <div class="controls">
                            <input id="dpYears1" name="years" type="text" value="" size="16" class="m-ctrl-medium" style="width: 285px;" />
                                                        </div>
                                                    </div>   
                                                    
                                   <div class="control-group">
        <label class="control-label">Select Month</label>
        <div class="controls">
          <select class="span12 chzn-select" name="month" id="month" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
             <option value="all">-All-</option>
			  <option value="1"  >January</option>
			  <option value="2"  >February</option>
			  <option value="3"  >March</option>
			  <option value="4"  >April</option>
			  <option value="5"  >May</option>
			  <option value="6"  >June</option>
			  <option value="7"  >July</option>
			  <option value="8"  >August</option>
			  <option value="9"  >September</option>
			  <option value="10" >October</option>
			  <option value="11" >November</option>
			  <option value="12" >December</option>
			  </select>
        </div>
      </div>                  
                                  

                                     <br /><br /><br /><br />
                                       <ul class="pager wizard">
                                         <li class="previous first purple"><a href="javascript:;">First</a></li>
                                         <li class="previous purple"><a href="javascript:;">Previous</a></li>
                                         <li class="next last purple"><a href="javascript:;">Last</a></li>
                                        <button type="submit" name="submit" id="retrieve_parent" value="submit"  class="button_next">Next</button> 
                                     </ul>
                                     </div>
                                     </div>
                                     </form>
                                       <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                              <div style="max-width:100%; overflow-x:scroll ; overflow-y: visible; padding-bottom:20px; ">            
  						<div class="space15"></div>
                                 <table class="table table-striped table-hover table-bordered" id="editable-sample" style="min-width:3000px;">
                                     <thead>
                                     <tr>
										 <th>ID</th>	                                     
                                         <th>Student Name</th>
                                         <th>PDF</th>
                                         <th>Grade Name</th>
                                         <th>period name</th>
                                         <th>subject name</th>
                                         <th>comment</th>
                                         <th>strengths</th>
                                         <th>concerns</th>
                                         <th>ideas_for_parent_student </th>
                                        
        <?php
		if(isset($retrieve_reports) && count($retrieve_reports) > 0) {
    foreach($retrieve_reports as $retrieve_report){?>
    </tr>
                                     </thead>
                                     <tbody>
                                     <tr class="">
                                     	<td class="name"><?php echo $retrieve_report->id;?></td>
                                   		 <td class="name"> <?php echo ($retrieve_report->firstname . ' ' . $retrieve_report->lastname . ''); ?></td>
   <td><a class="edit" target="_blank" href="<?php echo base_url().'classroom/parent_conferencepdf/'.$retrieve_report->student_id.'/'.$retrieve_report->grade_id.'/'.$retrieve_report->date.'/'.$retrieve_report->id?>">PDF</a></td>
                                         <td><?php echo $retrieve_report->grade_name;?> </td>
                       					 <td><?php echo $retrieve_report->start_time.'-'.$retrieve_report->end_time;?> </td>
                                         <td><?php echo $retrieve_report->subject_name;?> </td>
                                         <td><?php echo $retrieve_report->comment;?></td>
                                         <td><?php echo $retrieve_report->strengths;?> </td>
                                         <td><?php echo $retrieve_report->concerns;?> </td>
                                         <td><?php echo $retrieve_report->ideas_for_parent_student;?> </td>
                                        
                                     </tr>
   
   <?php }
}
else
{
	?>
		<tr class="odd">
        <td colspan="6" align="center"><strong>No items in database</strong></td>
        </tr>
<?php }
?>                               
                                     </tbody>
                                 </table>
                                 
                                 </div>
                                         
                                     
                                     
                                    <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         
                                         
                                      
                                             
                                       
                                    </div><BR><BR>
                                           
                                           <div class="span12">
                      
                        <!--<center><button class="btn btn-large btn-purple"><i class="icon-save icon-white"></i> Save File</button> <button class="btn btn-large btn-purple"><i class="icon-Print icon-white"></i> Print</button> <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Parent</button> <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>-->
                    </div>
                                         
                                         
                                         
                                         
                                         
                                         
                                     <ul class="pager wizard">
                                         <li class="previous first purple"><a href="javascript:;">First</a></li>
                                         <li class="previous purple"><a href="javascript:;">Previous</a></li>
                                         <li class="next last purple"><a href="javascript:;">Last</a></li>
                                         <li class="next purple"><a  href="javascript:;">Next</a></li>
                                     </ul>  
                                         </div>
                                         
                                     </div>
                                     </div>
                                     
                                    
                                     
                                 </div>
                             </div>
                           
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
   
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   
   <script src="<?php echo SITEURLM?>js/autocomplete.js" type="text/javascript"></script>
   <!-- END JAVASCRIPTS --> 
   
  <script>
         $(document).ready(function() {
				
            <?php if($this->session->userdata('login_type')=='teacher'):?>				
        $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
            width: 260,
            matchContains: true,
            selectFirst: false
        });
<?php else:?>	
        $("#studentlist").autocomplete("<?php echo base_url();?>teacherself/getstudents_by_teacher", {
            width: 260,
            matchContains: true,
            selectFirst: false,
            extraParams:{teacher_id:function(){
      return $('#teacher_id').val();
    }}
        });
<?php endif;?>

             $("#studentlist").result(function(event, data, formatted) {
            $("#student_id").val(data[1]);
        });
    });
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
       <?php if($this->session->userdata('submitbutton') || $this->input->post('submit')):?>
 $("#pills").bootstrapWizard("show",1);
<?php endif;
$this->session->unset_userdata('submitbutton');
$this->session->unset_userdata('submit');
?>

   </script>
  <script type="text/javascript">
function updateschool(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#schools").html(result);
			$("#schools").trigger("liszt:updated");
		}
		});
	
}
</script>
<script type="text/javascript">
function updateclass(tag)
{
		
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getgrades?schoolid='+tag,
		success:function(result)
		{
			$("#grades").html(result);
			$("#grades").trigger("liszt:updated");
		}
		});
	
}
</script>
<script type="text/javascript">
function checkvalid()
{
	
	var grade = document.getElementById('grades').value;
	var classrooms = document.getElementById('classrooms').value;
	<?php if(isset($school_type)) {?>
	var schooltype=document.getElementById('schools_type').value;
	var school = document.getElementById('schools').value;

	if(schooltype=="")
	{
		alert('Please select school type');
		return false;
	}
	
	if(school=="")
	{
		alert('Please select school');
		return false;
	}
		 <?php }?>	

	if(grade=="")
	{
		alert('Please select grade');
		return false;
	}
	
if(classrooms=="")
	{
		alert('Please select classroom');
		return false;
	}


document.searchstudent.submit();
	
}
</script>
<script type="text/javascript">
function updateclassroom(gradeid)
{
	var schoolid = $("#schools").val();
	$.ajax({
		url:'<?php echo base_url();?>/selectedstudentreport/getteachers?grade_id='+gradeid+"&schid="+schoolid,
		success:function(result)
		{
			
			$("#classrooms").html(result);
			$("#classrooms").trigger("liszt:updated");
		}
		});
}
</script>   
 <script type="text/javascript">
$('#step2').click(function(){$('.button_next').trigger('click');});
 </script> 
 <script>
$('#classrooms').change(function(){
	$('#students').html('');
	$.ajax({
	  type: "POST",
	  url: "<?php echo base_url();?>classroom/get_student_by_teacher",
	  data: { teacher_id: $('#classrooms').val() }
	})
	  .done(function( msg ) {
		  $('#students').html('');
		  var result = jQuery.parseJSON( msg )
		  $('#students').append('<option value="all">All</option>');
		 $(result.students).each(function(index, Element){ 
		 
		 console.log(Element);
			
		  $('#students').append('<option value="'+Element.student_id+'">'+Element.firstname+' '+Element.lastname+'</option>');
		 
		});  
		  $("#students").trigger("liszt:updated");
		  
	  });
  	
});

   </script>
</body>
<!-- END BODY -->
</html>