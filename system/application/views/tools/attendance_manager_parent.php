<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
    <link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.12/video.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
           <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                      <!--<a href="#jobaidesModal" data-toggle="modal" ><span>Job Aides</span>&nbsp;<i class="icon-folder-open"></i></a>-->
                      <a href="#myModal-video" data-toggle="modal" style="margin-right:5px;"><span>Video Tutorial</span>&nbsp;<i class="icon-play-circle"></i></a>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>tools/attendance_manager">Attendance Manager</a>
                           
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Attendance Manager</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" name="attendance_manager" id="attendance_manager" action="#">
                                <div id="pills" class="custom-wizard-pills-purple3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                    
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-purple progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         
                                          <?php if($this->session->userdata('login_type')=='observer' || $this->session->userdata('login_type')=='user'){
											 ?>
                                 <div class="control-group">
                                  <label class="control-label">Select Teacher</label>
                                             <div class="controls">
                         			 <select name="teacher_id" id="teacher_id" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                           <option value=""></option>
                                           <option value="all">All</option>
			   								 <?php if(!empty($teachers)) { 
											foreach($teachers as $val)
											{

											?>
			    						<option value="<?php echo $val['teacher_id'];?>" 
										<?php if(isset($teacher_id) && $teacher_id==$val['teacher_id']) {?> selected <?php } ?>
										><?php echo $val['firstname'].' '.$val['lastname'];?>  </option>
			    						<?php } } ?>
                                    </select>
                                    	
			                                 </div>
                                         </div> 
                                         <?php } else {?>
                                         <input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $this->session->userdata('teacher_id');?>" />
                                         <?php }?>
                                         
                                          <div class="control-group">
                                    <label class="control-label">Select Date Range</label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                            <input id="reservation" type="text" class=" m-ctrl-medium" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                             <label class="control-label">Select Period</label>
                                             <div class="controls">
                                                 <select id="periods" class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <?php foreach($periods as $value){?>
                                                <option value="<?php echo $value['period_id'];?>"><?php echo $value['start_time'].'-'.$value['end_time'];?></option>
                                            <?php }?>
                                        
                                    </select>
                                             </div>
                                         </div> 
                                 <div class="control-group">
                                             <label class="control-label">Select Student</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" id="student">
                                         <?php
                                                                if (!empty($students)) {
                                                                    foreach ($students as $studentsvalue) {
                                                                        ?>
                                                                        <option value="<?php echo $studentsvalue['student_id']; ?>"><?php echo $studentsvalue['firstname']; ?> <?php echo $studentsvalue['lastname']; ?></option>
    <?php }
} ?>               
                                         
                                    </select>
                                             </div>
                                         </div>
                                
                                          <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div>
                                 
                                   
                                        
                                       
                                     </div>
                                  <!-- BEGIN STEP 2-->
                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Subject</label>
                                             <div class="controls" id="subject">
                                                 
                                             </div>
                                         </div>
                                         
                                      <div class="control-group">
                                             <label class="control-label">Grade</label>
                                             <div class="controls" id="grade">
                                                 
                                             </div>
                                         </div> 
                                         
                                         <div class="control-group" style="display:none;">
                                             <label class="control-label">Select View</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" name="view" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" id="selectview">
                                        <option value=""></option>
                                        <option value="day">Day</option>
                                        <option value="week">Week</option>
                                        <option value="month" selected="selected">Month</option>
                                      <?php if($this->session->userdata('login_type')=='observer'){?>  
                                       <option value="year">Year</option>
                                       <?php }?>
                                       
                                    </select>
                                             </div>
                                         </div>
                                          <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div> <div class="space20"></div>
                                                 
                                         
                                        
                                     </div>
                                  <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                         
                                          <div class="control-group">
                                             <label class="control-label">Select Report Type</label>
                                             <div class="controls">
                                             <select class="span12 chzn-select" name="report_type" id="report_type" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option value="graph" >Graph</option>
                                        <option value="pdf">PDF Report</option>                                       
                                    </select>
                                             </div>
                                         </div>
                                        
                                       <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
							<input type="button" class="btn btn-small btn-purple" name="answer" value="Retrieve Report" onClick="showDiv()" style=" padding: 5px; " />                         	</div>
                                </div>
                                             
                                                 <div class="space20"></div>
                                
                                <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Attendance Manager Cumulative Report</h4>
                      </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  
                                 <div id="container1"></div>
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center>
                        <button class="btn btn-large btn-purple"><i class="icon-print icon-white"></i> Print</button> 
                        <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Colleague</button>
                        </center>
                          
                                </div>  
                                      
                                     </div>
                                         
                                       <ul class="pager wizard">
                                         <li class="previous first purple"><a href="javascript:;">First</a></li>
                                         <li class="previous purple"><a href="javascript:;">Previous</a></li>
                                         <li class="next last purple"><a href="javascript:;">Last</a></li>
                                         <li class="next purple"><a  href="javascript:;">Next</a></li>
                                     </ul> 
                                           
                                         </div>
                                     </div>
                                     
                                
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->
   <div id="myModal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:700px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Attendance Manager</h3>
                                </div>
                              
                                <div class="modal-body">
                                        
                                    <video id="example_video_1" class="video-js vjs-default-skin"
  controls preload="auto" width="640" height="360"
 
  data-setup='{"example_option":true}'>
 <source src="<?php echo SITEURLM;?>convertedVideos/Retrieving attendanceConverted.mp4" type='video/mp4' />
 
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
                                </div>
                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<!--<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>-->
                                </div>
                    
                            </div>
   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>	
   <script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script> 
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>
   <!--script for this page only-->
  

   
    
   <script>
   function showDiv() {
	   
       
        var selectdate = $('#reservation').val();
        console.log(selectdate);
        var rangearr = selectdate.split('-');
        var startdate = $.trim(rangearr[0]);
        var enddate = $.trim(rangearr[1]);
	
	var date = new Date($.trim(rangearr[0]));
	var startfrom = date.toString('yyyy-MM-dd');

	var enddateTo = new Date($.trim(rangearr[1]));
	var endto = enddateTo.toString('yyyy-MM-dd');


       <?php if($this->session->userdata('login_type')=='teacher' || $this->session->userdata('login_type')=='observer' || $this->session->userdata('login_type')=='parent'):?>
	   if($('#report_type').val()=='pdf') {
window.open('<?php echo base_url();?>attendance/attendance_managerpdf/'+ startfrom + '/' +endto + '/' + $('#student').val() + '/' + $('#selectview').val() + '/' + $('#periods').val()+ '/' + $('#teacher_id').val());   
			return false;
		 }
			   $.ajax({
				type: "POST",
				url: "<?php echo base_url();?>attendance/createattendancegraph/",
			data: {period_id: $('#periods').val(),report_type: $('#report_type').val(), student_id:$('#student').val(),start: startdate,end: enddate,view: $('#selectview').val()}
			  })
		
      <?php else:?>
	  
	  if($('#report_type').val()=='pdf') {
window.open('<?php echo base_url();?>attendance/attendance_manager_data_pdf/'+ startfrom + '/' +endto + '/' + $('#student').val() + '/' + $('#selectview').val() + '/' + $('#periods').val()+ '/' + $('#teacher_id').val());   
			return false;
		 }
		
	  
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>attendance/createattendancegraph/",
        data: {period_id: $('#periods').val(),report_type: $('#report_type').val(), student_id:$('#student').val(),start: startdate,end: enddate,view: $('#selectview').val(),teacher_id:$('#teacher_id').val()}
      })
      <?php endif;?>
        .done(function( msg ) {
            eval(msg);
//            $('#reportDiv').html(eval(msg));
        });
       
   $('#reportDiv').show();
}

</script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });
    $('#periods').change(function(){
    if($('#reservation').val()!='')
    {
//        var selectdate = $('#reservation').val();
//        console.log(selectdate);
//        var rangearr = selectdate.split('-');
//        var startdate = $.trim(rangearr[0]);
//        var enddate $.trim(rangearr[1]);
//        var selectdatearr = selectdate.split('-');
//        var newselectdate = selectdatearr[2]+'-'+selectdatearr[0]+'-'+selectdatearr[1];

//        dateformat = new Date(newselectdate);
//        daynumber = dateformat.getDay();
//        newdaynumber = daynumber-1;
       /*get class details using ajax*/ 
     <?php if($this->session->userdata('login_type')=='parent'){?>
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>attendance/getstudentdetails_parent/"+$('#periods').val()+"/"+$('#student').val()
      })

	 <?php } else {?>
	   $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>attendance/getstudentdetails/"+$('#periods').val(),
		data: {teacher_id: $('#teacher_id').val()}
      })
	  <?php }?>
	  
        .done(function( msg ) {
            result = jQuery.parseJSON(msg);
//            console.log(result);
$('#teacher_id').val(result.teacher_id);
            $('#grade').html(result.status[0].grade);
            $('#subject').html(result.status[0].subject_name);
			var studentarr = result.status;
            $.each(studentarr, function() { console.log(this);
                $('#student').append('<option value="'+this.student_id+'">'+this.firstname+' '+this.lastname+'</option>').trigger("liszt:updated");
            });
            
           
//            if(result.status!=false){
//                console.log(result.status[0].grade);
//                $('#class_grade').html("<strong>"+result.status[0].grade+"</strong>");
//                $('#subject').html("<strong>"+result.status[0].subject_name+"</strong>");
//            }
        });

    }
    
});

    $('#student').change(function(){
    if($('#reservation').val()!='')
    {
//        var selectdate = $('#reservation').val();
//        console.log(selectdate);
//        var rangearr = selectdate.split('-');
//        var startdate = $.trim(rangearr[0]);
//        var enddate $.trim(rangearr[1]);
//        var selectdatearr = selectdate.split('-');
//        var newselectdate = selectdatearr[2]+'-'+selectdatearr[0]+'-'+selectdatearr[1];

//        dateformat = new Date(newselectdate);
//        daynumber = dateformat.getDay();
//        newdaynumber = daynumber-1;
       /*get class details using ajax*/ 
     <?php if($this->session->userdata('login_type')=='parent'){?>
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>attendance/getstudentdetails_parent/"+$('#periods').val()+"/"+$('#student').val()
      })

   <?php } else {?>
     $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>attendance/getstudentdetails/"+$('#periods').val(),
    data: {teacher_id: $('#teacher_id').val()}
      })
    <?php }?>
    
        .done(function( msg ) {
            result = jQuery.parseJSON(msg);
//            console.log(result);
            //alert(result.teacher_id);
            $('#teacher_id').val(result.teacher_id);
            $('#grade').html(result.status[0].grade);
            $('#subject').html(result.status[0].subject_name);
      var studentarr = result.status;
            $.each(studentarr, function() { console.log(this);
                $('#student').append('<option value="'+this.student_id+'">'+this.firstname+' '+this.lastname+'</option>').trigger("liszt:updated");
            });
            
           
//            if(result.status!=false){
//                console.log(result.status[0].grade);
//                $('#class_grade').html("<strong>"+result.status[0].grade+"</strong>");
//                $('#subject').html("<strong>"+result.status[0].subject_name+"</strong>");
//            }
        });

    }
    
});

   </script> 
     <script>
     /*  $('#myModal-video').on('hidden.bs.modal', function (e) {
                var myPlayer = videojs('example_video_1');
                myPlayer.pause();
              });
             $('#myModal-video').modal('show');
          var myPlayer = videojs('example_video_1');
        myPlayer.play();
  */      </script>

</body>
<!-- END BODY -->
</html>