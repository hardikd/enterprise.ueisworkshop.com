<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
        <base href="<?php echo base_url(); ?>"/>

   <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
    <?php require_once($view_path . 'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
           <?php require_once($view_path . 'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                            <span class="divider">></span> 
                       </li>
                       
                         <li>
                            <a href="<?php echo base_url();?>tools/classroom_management">Classroom Management Assistant</a>
                           <span class="divider">></span> 
                       </li>
                       
                        <li>
                            <a href="<?php echo base_url();?>tools/teacher_student">Teacher/Student Conference Report</a>
                          
                       </li>
                       
                       
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Teacher/Student Conference Report</h4>
                          
                         </div>
                         <div class="widget-body">
                            <form class="form-horizontal" action="#">
                                <div id="pills" class="custom-wizard-pills-purple3">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab">Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                    
                                    
                                     
                                     
                                 </ul>
                                 <div class="progress progress-success-purple progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
                                         <form class="form-horizontal" action="#">
                                         
                                          <div class="control-group">
                                    <label class="control-label">Select Date Range</label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                            <input id="reservation" type="text" class=" m-ctrl-medium" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                             <label class="control-label">Select Teacher(s)</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                         <option >Select All</option>
                                        <option>Teacher 1</option>
                                        <option>Teacher 2</option>
                                        <option>Teacher 3</option>
                                    </select>
                                             </div>
                                         </div>
                                
                                <div class="control-group">
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option >Pre-K</option>
                                        <option >Kindergarten</option>
                                        <option >1st Grade</option>
                                        <option>2nd Grade</option>
                                        <option >3rd Grade</option>
                                        <option >4th Grade</option>
                                        <option >5th Grade</option>
                                        <option >6th Grade</option>
                                        <option >7th Grade</option>
                                        <option >8th Grade</option>
                                        <option >9th Grade</option>
                                        <option >10th Grade</option>
                                        <option >11th Grade</option>
                                        <option >12th Grade</option>
                                    </select>
                                             </div>
                                         </div> 
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Period(s)</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option >Select All</option>
                                        <option >Period 1</option>
                                        <option >Period 2</option>
                                        <option >Period 3</option>
                                        <option >Period 4</option>
                                        <option >Period 5</option>
                                        <option >Period 6</option>
                                        <option >Period 7</option>
                                        
                                    </select>
                                             </div>
                                         </div> 
                                
                               
                                 
                                 
                                        
                                       
                                     </div>
                                     
                                      <!-- BEGIN STEP 2-->
                                      <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Subject(s)</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option >Select All</option>
                                        <option>Math</option>
                                        <option>English</option>
                                        <option>History</option>
                                    </select>
                                             </div>
                                         </div>
                                         
                                     <div class="control-group">
                                             <label class="control-label">Select Student(s)</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                        <option >Select All</option>
                                        <option >Student 1</option>
                                        <option >Student 2</option>
                                        <option >Student 3</option>
                                        
                                    </select>
                                             </div>
                                         </div> 
                                         
                                         
                                         
                                         </div> 
                                         
                                         
                                        
                                     
                                     
                                     
                                                   <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                         
                                         <div class="control-group">
                                             <label class="control-label">Select Graph Type</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                         <option >Bar</option>
                                        <option>Line</option>                                       
                                    </select>
                                             </div>
                                         </div>
                                         
                                          <div class="control-group">
                                             <label class="control-label">Select View</label>
                                             <div class="controls">
                                                 <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">
                                        <option value=""></option>
                                         <option >Day</option>
                                        <option>Week</option>
                                        <option>Month</option>
                                        <option>Semester</option>
                                        <option>Year</option>
                                    </select>
                                             </div>
                                         </div>
                                      
                                       
                                        
                                       <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> <input type="button" class="btn btn-small btn-purple" name="answer" value="Retrieve Report" onClick="showDiv()" style=" padding: 5px; " />                         </div>    </div>
                                             
                                             
                                             
                                                 <div class="space20"></div>
                                
                                <div id="reportDiv"  style="display:none;" class="answer_list" >
                                   <div class="widget purple">
                         <div class="widget-title">
                             <h4>Teacher/Student Conference Cumulative Report</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                  <h3 style="text-align:center;">Report Appears Here</h3>
                                 
                                  </div>
                                
                                </div> 
                                
                       <div class="space20"></div> <div class="space20"></div><div class="space20"></div>
                                
                        <center><button class="btn btn-large btn-purple"><i class="icon-print icon-white"></i> Print</button> <button class="btn btn-large btn-purple"><i class="icon-envelope icon-white"></i> Send to Colleague</button></center>
                          
                                </div>  
                                
                                
                                  
                                      
                                     
                                       
                                         
                                         
                                         
                                           
                                         </div>
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first purple"><a href="javascript:;">First</a></li>
                                         <li class="previous purple"><a href="javascript:;">Previous</a></li>
                                         <li class="next last purple"><a href="javascript:;">Last</a></li>
                                         <li class="next purple"><a  href="javascript:;">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
   
   
   <script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>
   <script src="<?php echo SITEURLM ?>js/form-component.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/DT_bootstrap.js"></script>
   
   <!--script for this page only-->
  

    <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>
   
    <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
 
</body>
<!-- END BODY -->
</html>