﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
     <?php require_once($view_path.'inc/teacher_menu.php'); ?>
  
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                       <a href="#jobaidesModal" data-toggle="modal" ><i class="icon-folder-open"></i><span>Job Aides</span></a>
                   </h3>
           <div id="jobaidesModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-header" style="background:#74B749; color:#FFFFFF;">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="myModalLabel2"><i class="icon-wrench"></i> Job Aides</h3>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-success">OK</button>
            </div>
          </div>
                <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/data_tracker">Data Tracker</a>
                             <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/classroom_management">Classroom Management Assistant</a>
                             
                       </li>
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                  
                     <!--BEGIN METRO STATES-->
                
           <div class="metro-nav">
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>tools/student_progress_report" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-paste"></i><br><br><br>
                              Student Progress Report
                            </span>
                           
                        </a>
                    </div>
                    
                <div class="metro-nav">
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>tools/success_team" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-trophy"></i><i class="icon-group"></i><br><br><br>
                              Student Success Team Meeting Report
                            </span>
                           
                        </a>
                    </div>
                    
                          <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>tools/parent_teacher" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-group"></i><br><br><br>
                              Parent/Teacher Conference Report
                            </span>
                            
                        </a>
                    </div>
                    
                    
                          <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>tools/teacher_student" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-comments"></i><br><br><br>
                                Teacher/Student Conference Report
                            </span>
                            
                        </a>
                    </div>
                    
                       <div class="metro-nav metro-fix-view">
                    <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>teacherself/getstudentreport" class="text-center" data-original-title="">
                            <span class="value">
                                <i class="icon-smile"></i><br><br><br>
                             Behavior Learning Record Report
                            </span>
                            
                        </a>
                    </div>
                    
                        <div class="metro-nav-block nav-block-purple long-dash">
                        <a href="<?php echo base_url();?>tools/report_list" class="text-center" data-original-title="">
                            <span class="value">
									<i class="icon-smile"></i><br><br><br>
                                Behavior Running Record Report
                            </span>
                            
                        </a>
                    </div>      
                    
                    
                       
                </div>
              
                <div class="space10"></div>
                <!--END METRO STATES-->
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
<script src="<?php echo SITEURLM?>js/jquery.easing.js" type="text/javascript"></script>
    <script src="<?php echo SITEURLM?>js/jqueryFileTree.js" type="text/javascript"></script>
    <link href="<?php echo SITEURLM?>css_new/jqueryFileTree.css" rel="stylesheet" type="text/css" media="screen" />
   <script>
   
   $(document).ready( function() {
    $('.modal-body').fileTree({
        root: '<?php echo $_SERVER['DOCUMENT_ROOT'];?>/Classroom Job Aides/TOOLS & RESOURCES/Data Tracker/Classroom Management Assistant/',
        script: '<?php echo base_url();?>index/foldertree/',
        expandSpeed: 1000,
        collapseSpeed: 1000,
        multiFolder: false
    }, function(file) {
//        alert(file);
        
        window.open(file);
    });
});
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>