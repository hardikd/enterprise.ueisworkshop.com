<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<!--start old script -->

<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
/*
function viewpdf(studentid)
{
	var url = "<?php echo base_url()?>studentdetail/pdf/"+studentid; 
	window.location.href=""+url;
} */
</script>

<!--end old script -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
      <?php require_once($view_path.'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid"> 
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          
          <h3 class="page-title"> <i class="icon-wrench"></i>&nbsp; Tools & Resources </h3>
          <ul class="breadcrumb" >
            <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
            <li> <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools">Tools & Resources</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>studentdetail">Individual Student Performance Summary</a> </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN BLANK PAGE PORTLET-->
          <div id="reportDiv"  style="display:block;" class="answer_list" >
            <div class="widget purple">
              <div class="widget-title">
                <h4>Student Details</h4>
              </div>
              <div class="widget-body" style="min-height: 150px;">
              
                  <fieldset>
                    <fieldset>
                      <legend>Student infomation</legend>
                      <table cellpadding="5" cellspacing="0" width="100%">
                        <tr>
                          <td>First Name</td>
                          <td><?php echo @$studentrecord[0]['Name'];?></td>
                        </tr>
                        <tr>
                          <td>Last Name</td>
                          <td><?php echo @$studentrecord[0]['Surname'];?></td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td><?php echo @$studentrecord[0]['email'];?></td>
                        </tr>
                        <!--<tr>
              <td>Address</td>
              <td><?php //echo @$studentrecord[0]['address'];?></td>
            </tr>
            <tr>
              <td>Phone</td>
              <td><?php //echo @$studentrecord[0]['phone'];?></td>
            </tr>-->
                        <tr>
                          <td>District</td>
                          <td><?php echo @$districtname[0]['districts_name'];?></td>
                        </tr>
                        <tr>
                          <td>School Name</td>
                          <td><?php echo @$schoolname[0]['school_name'];?></td>
                        </tr>
                        <tr>
                          <td>Grade</td>
                          <td><?php echo @$studentrecord[0]['grade_name'];?></td>
                        </tr>
                        <!-- <tr>
              <td>Country</td>
              <td><?php //echo @$countryname[0]['country_name'];?></td>
            </tr>-->
                        <tr>
                          <td>State</td>
                          <td><?php echo @$statename[0]['name'];?></td>
                        </tr>
                          </tr>
                        
                      </table>
                    </fieldset>
                    
                    <!-- Student Performance-->
                    
                    <fieldset>
                      <legend>Student Performance</legend>
                      <table cellpadding="5" cellspacing="0" width="100%">
                        <tr>
                          <th align="justify">Assessment Name</th>
                          <th align="justify">Total Points Scored</th>
                          <th align="justify">Percentage Correct</th>
                        </tr>
                        <?php
	  
	  if(!empty($assessmentname))
	  {
		 $assessmentsids=array();
		  foreach($assessmentname as $kk => $vv)
		  {	
			@$assessmentsids[]=$vv[0]['id'];
				?>
                        <tr>
                          <td><?php echo @$vv[0]['assignment_name'];?></td>
                          <td><?php echo @$studentperformance[$kk]['pass_score_point'];?></td>
                          <td><?php echo @$studentperformance[$kk]['pass_score_perc'];?></td>
                        </tr>
                        <?php
		  }
		   echo '<tr><td colspan="3" align="right"> <a href="'.base_url().'studentdetail/pdf/'.$studentrecord[0]['UserID'].'" target="_blank"> 
		   <input type="button" class="btn btn-small btn-purple" name="pdfreport" id="pdfreport" value="PDF Report"/></a>  </td> </tr>';	
	  }
	 
	  ?>
                      </table>
                    </fieldset>
                    
                    <!--    <iframe src="<?php // echo $path;?>" width="600px" height="200px"></iframe>  --> 
                    
                    <!-- <object height="500" data="<?php //echo $path;?>" type="application/pdf"  width="100%" > </object> -->
                    
                    <fieldset>
                      <legend>Student Performance Chart</legend>
                      <?php
  $testnames=array();
  
  if(!empty($assessmentname))
  {
  
  foreach($assessmentname as $key => $val)
  {
	  
	  $testnames[] = @$val[0]['assignment_name'];
  }
  
 
  @$testnames = implode("','",$testnames);

	$testnames = "['','".$testnames."']";
  $marks=array();
foreach($studentperformance as $kkk=>$vvv)
{
	
	$marks[] = $studentperformance[$kkk]['pass_score_perc'];
}
  $dd = implode(',',$marks);


// For pie graph working		
	
			
			
	// Dynamic proficiency  
      
   
	  $temp=array();
	 for($k=0;$k<count($assessmentsids);$k++)
	 {
		$res = mysql_query('select * from proficiency where assessment_id="'.$assessmentsids[$k].'"');
		$temp[] = mysql_fetch_assoc($res);
	 }
			
			
			
			$pro = array();
			$basic = array();
			$bbasic = array();
			
	for($i=0;$i<count($studentperformance);$i++)
		{
				$num = round($studentperformance[$i]['pass_score_point']);
				
			$bbfrom = $temp[$i]['bbasicfrom'];
			$bbto = $temp[$i]['bbasicto'];
			$basicfrom = $temp[$i]['basicfrom'];
			$basicto = $temp[$i]['basicto'];
			$pro_from = $temp[$i]['pro_from'];
			$pro_to = $temp[$i]['pro_to'];
				
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					

				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					 $basic[] = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro[] = $num;
				}
				
				
				
		}
		
	
	$t1 = array_sum($bbasic);
		 
		  if($t1==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			   $avgbbbasic = $t1/$total;
			 }
		  
		 // 2
		  	 $t2 = array_sum($basic);	
		  
		  	 if($t2==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $t2/$total1;
			 }
		 //3
		
		   $t3 = array_sum($pro);		 
		 if($t3==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
			   $avgpro = $t3/$total2;
			 }

  }
  $totalval = $avgbbbasic + $avgbasic + $avgpro;
	
	
	
	if($avgbbbasic!=0)
	{
		$frstval = ($avgbbbasic/$totalval)*100;
	}
	else
	{
	$frstval=0;
	}
	if($avgbasic!=0)
	{
		$secval = ($avgbasic/$totalval)*100;
	}
	else
	{
	$secval=0;
	}
	if($avgpro!=0)
	{
		$thrdval = ($avgpro/$totalval)*100;
	}
	else
	{
		$thrdval=0;
	}
	
	 $avgbbbasic=round($frstval);	
	 $avgbasic=round($secval);
	 $avgpro=round($thrdval);
	 
  ?>
                      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
                      <script type="text/javascript">
$(function () {
        $('#map-container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: <?php echo $testnames;?>
            },
	        tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' %';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
				/*	 yAxis: {
                min: 0,
				max:100,
                },
             labels: {
                items: [{
                    html: 'Student Proficiency Score(s)',
                    style: {
                        left: '180px',
                        top: '0px',
                        color: 'black'
                    }
                }]
            },  */
            series: [ 
			{
               type: 'column',
			   
			   
				name: '',
				data: [,,,,],
				
            },

			{
                type: 'column',
                name: '<?php echo $studentrecord[0]['Name'];?>',
                data: [0,<?php echo $dd;?>],
				
            },
			 {
                type: 'spline',
                name: 'Average',
                data: [0,<?php echo $dd;?>],
				
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            },
			/* {
                type: 'pie',
               name: 'Student Proficiency',
                data: [
                    ['Below Basic',   <?php //   echo $avgbbbasic; ?>],
                    ['Basic', <?php  // echo $avgbasic;?>],
                    {
                        name: 'Proficient',
                        y: <?php // echo $avgpro;?>,
                        sliced: true,
                        selected: true
                    },
                                       
                ],
                 center: [25, 10],
                size: 80,
                showInLegend: false,
                dataLabels: {
                    enabled: false   
                } 
            } */]
        });
    });
    

</script> 
                      <script src="<?php echo SITEURLM?>js/highcharts.js"></script> 
                      <script src="<?php echo SITEURLM?>js/exporting.js"></script>
                      <div id="map-container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                    </fieldset>
                  </fieldset>
               
              </div>
            </div>
          </div>
          <!-- END BLANK PAGE PORTLET--> 
        </div>
      </div>
      
      <!-- END PAGE CONTENT--> 
    </div>
    <!-- END PAGE CONTAINER--> 
  </div>
  <!-- END PAGE --> 
</div>
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 

<script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script> 

<!--common script for all pages--> 
<script src="<?php echo SITEURLM?>js/common-scripts.js"></script> 
<!--script for this page--> 
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script> 
<script src="<?php echo SITEURLM?>js/form-component.js"></script> 
<script src="<?php echo SITEURLM?>js/dynamic-table.js"></script> 
<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script> 
<!--script for this page--> 
<script src="<?php echo SITEURLM?>js/dynamic-table.js"></script> 
<script src="<?php echo SITEURLM?>js/editable-table.js"></script> 
<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script> 
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script> 
<script src="<?php echo SITEURLM?>js/form-component.js"></script> 

<!-- END JAVASCRIPTS --> 

<script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script> 
<script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script> 
<script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>
</body>
<!-- END BODY -->
</html>