
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Teacher Performance Rating::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>

<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
</script>


</head>

<body>
<div class="wrapper">
  <?php require_once($view_path.'inc/header.php'); ?>
  <div class="mbody">
    <?php require_once($view_path.'inc/obmenu.php'); ?>
    <div class="content">
	      
      <fieldset>
      <legend>Student Detail</legend>
      <fieldset>
      <legend>Student infomation</legend>
      <table cellpadding="5" cellspacing="0" width="100%">
      <tr>
      <td>First Name</td><td><?php echo @$studentrecord[0]['Name'];?></td>
      </tr>
       <tr>
      <td>Last Name</td><td><?php echo @$studentrecord[0]['Surname'];?></td>
      </tr>
       <tr>
      <td>Email</td><td><?php echo @$studentrecord[0]['email'];?></td>
      </tr>
      <!-- <tr>
      <td>Address</td><td><?php //echo @$studentrecord[0]['address'];?></td>
      </tr>
       <tr>
      <td>Phone</td><td><?php //echo @$studentrecord[0]['phone'];?></td>
      </tr>
         -->
          <tr>
      <td>District</td><td><?php echo @$districtname[0]['districts_name'];?></td>
      </tr>
         <tr>
      <td>School Name</td><td><?php echo @$schoolname[0]['school_name'];?></td>
      </tr>
       <tr>
      <td>Grade</td><td><?php echo @$studentrecord[0]['grade_name'];?></td>
      </tr>
      <!--<tr>
      <td>Country</td><td><?php //echo @$countryname[0]['country_name'];?></td>
      </tr>-->
      <tr>
      <td>State</td><td><?php echo @$statename[0]['name'];?></td>
      </tr>
      </tr>
      
      </table>
      </fieldset>
      
     <!-- Student Performance-->
     
      <fieldset>
      <legend>Student Performance</legend>
      
    
      
      <table cellpadding="5" cellspacing="0" width="100%">
      <tr>
      <th align="justify">Assessment Name</th>
      <th align="justify">Total Points Scored</th>
      <th align="justify">Percentage Correct</th>
      </tr>
     
      <?php
	   $assessmentsids=array();
	   
	  foreach($assessmentname as $kk => $vv)
	  {

		  @$assessmentsids[]=$vv[0]['id'];
		  
		  ?>
      <tr>
      <td><?php echo @$vv[0]['assignment_name'];?></td>
       <td><?php echo @$studentperformance[$kk]['pass_score_point'];?></td>
       <td><?php echo @$studentperformance[$kk]['pass_score_perc'];?></td>
     </tr> 
   <?php
	  }
	  ?>
    
      </table>
      </fieldset>
     
      
      
      <fieldset>
      <legend>Student Performance Chart</legend>
      
  <?php
  $testnames=array();
  foreach($assessmentname as $key => $val)
  {
	  
	  $testnames[] = @$val[0]['assignment_name'];
  }
  
 
  $testnames = implode("','",$testnames);

	$testnames = "[	'".$testnames."']";
	 // echo $testnames;  exit;
  $marks=array();
foreach($studentperformance as $kkk=>$vvv)
{
	
	$marks[] = @$studentperformance[$kkk]['pass_score_perc'];
}
  $dd = implode(',',$marks);
//echo $dd = ','','.$dd;

// For pie graph working		
// get proficiency from database table * proficiency *
		
	
			 
    // Dynamic proficiency  
      
   
	  $temp=array();
	 for($k=0;$k<count($assessmentsids);$k++)
	 {
		$res = mysql_query('select * from proficiency where assessment_id="'.$assessmentsids[$k].'"');
		$temp[] = mysql_fetch_assoc($res);
	 }
		
		
		
		
		
			$pro = array();
			$basic = array();
			$bbasic = array();
			
	for($i=0;$i<count($studentperformance);$i++)
		{
			$num = round($studentperformance[$i]['pass_score_point']);
					
			$bbfrom = $temp[$i]['bbasicfrom'];
			$bbto = $temp[$i]['bbasicto'];
			$basicfrom = $temp[$i]['basicfrom'];
			$basicto = $temp[$i]['basicto'];
			$pro_from = $temp[$i]['pro_from'];
			$pro_to = $temp[$i]['pro_to'];
			
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					 $basic[] = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro[] = $num;
				}
				
				
				
		}
		
	
	$t1 = array_sum($bbasic);
		 
		  if($t1==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			   $avgbbbasic = $t1/$total;
			 }
		  
		 // 2
		  	 $t2 = array_sum($basic);	
		  
		  	 if($t2==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $t2/$total1;
			 }
		 //3
		
		   $t3 = array_sum($pro);		 
		 if($t3==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
			   $avgpro = $t3/$total2;
			 }

			 $totalval = $avgbbbasic + $avgbasic + $avgpro;
			 
			 
			 
			 if($avgbbbasic!=0)
			 {
			 	$frstval = ($avgbbbasic/$totalval)*100;
			 }
			 else
			 {
			 	$frstval=0;
			 }
			 if($avgbasic!=0)
			 {
			 	$secval = ($avgbasic/$totalval)*100;
			 }
			 else
			 {
			 	$secval=0;
			 }
			 if($avgpro!=0)
			 {
			 	$thrdval = ($avgpro/$totalval)*100;
			 }
			 else
			 {
			 	$thrdval=0;
			 }
			 
			 $avgbbbasic=round($frstval);
			 $avgbasic=round($secval);
			 $avgpro=round($thrdval);

  ?>
  	<script type="text/javascript">
	
$(function () {
	<?php // if($studentrecord[0]['Name']!='') { ?>
        $('#container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: <?php echo $testnames;?>
            },
			yAxis: {
                min: 0,
				max:100,
                title: {
                    text: 'Percentage'
                }
            },
            tooltip: {
                formatter: function() {	
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' %';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
           /* labels: {
                items: [{
                    html: 'Student Proficiency Score(s)',
                    style: {
                        left: '180px',
                        top: '0px',
                        color: 'black'
                    }
                }]
            }, */
            series: [ 
			//{
              // type: 'column',
			//	name: '',
			//	data: [,,,,],
				
           // },

			{
                type: 'column',
				color:['#0d233a'],
				
                name: '<?php echo $studentrecord[0]['Name'];?>',
                data: [<?php echo $dd;?>],
				
            }, {
                type: 'spline',
                name: 'Average',
                data: [<?php echo $dd;?>],
				lineColor: Highcharts.getOptions().colors[2],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: 'white'
                }
            } /*, {
                type: 'pie',
                name: 'Student Proficiency',
                data: [
                    ['Below Basic',   <?php //echo $avgbbbasic; ?>],
                    ['Basic', <?php //echo $avgbasic;?>],
                    {
                        name: 'Proficient',
                        y: <?php //echo $avgpro;?>,
                        sliced: true,
                        selected: true
                    },
                                       
                ],   
                 center: [25, 10],
                size: 80,
                showInLegend: false,
                dataLabels: {
                    enabled: false
                } 
            } */]
        }); <?php // }?>
    });
    
</script>
	
<script src="<?php echo SITEURLM?>js/highcharts.js"></script>
<script src="<?php echo SITEURLM?>js/exporting.js"></script>

 <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
 
 
 
      </fieldset>
      
      
      </fieldset>
      
    </div>
  </div>
  <?php require_once($view_path.'inc/footer.php'); ?>
</div>

</body>
</html>
