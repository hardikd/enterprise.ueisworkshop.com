<style type="text/css">
	 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
font-size:9px;
}

</style>

<page backtop="7mm" backbottom="17mm" backleft="1mm" backright="10mm"> 
        <page_header> 
              <div style="padding-left:610px;position:relative;top:-30px;">
                 <img alt="Logo"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/logo150.png" style="top:-10px;position: relative;text-align:right;float:right;"/>
		<div style="font-size:24px;color:#000000;position:absolute;width: 600px">
                    <b><img alt="pencil" width="12px"  src="<?php echo SITEURLM.$view_path; ?>inc/logo/pencil1.png"/>&nbsp;Student Performance Summary</b></div>
		</div>
        </page_header> 
        <page_footer> 
                <div style="font-family:arial; verticle-align:bottom; margin-left:10px;width:745px;font-size:7px;color:#<?php echo $fontcolor;?>;"><?php echo $dis;?></div>
		<br />
                <table style="margin-left:10px;">
                    <tr>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:30px;width:345px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            &copy; Copyright U.E.I.S. Corp. All Rights Reserved
                        </td>
                        <td style="font-family:arial; verticle-align:bottom; margin-left:10px;width:320px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                           Page [[page_cu]] of [[page_nb]] 
                        </td>
                        <td style="margin-righ:10px;test-align:righ;font-family:arial; verticle-align:bottom; margin-left:10px;width:145px;font-size:7px;color:#<?php echo $fontcolor;?>;">
                            Printed on: <?php echo date('F d,Y');?>
                        </td>
                    </tr>
                </table>
        </page_footer> 


        <table cellspacing="0" style="width:750px;border:2px #939393 solid;">
            <tr style="background:#939393;font-size: 18px;color:#FFF;height:36px;">
                <td colspan="3" style="padding-left:5px;padding-top:2px;height:36px;" >
                    <b> <?php echo ucfirst($this->session->userdata('district_name'));?> | <?php echo $schoolname[0]['school_name'];?></b>
                </td>
                <td style="text-align:right;padding-right:10px;"></td>
            </tr>
            <tr >
                <td colspan="2" >&nbsp;</td>
            </tr>
            <tr style="margin-top:20px;">
                <td style="padding-left:10px;width:350px;color:#939393; vertical-align: top;">
                    <b>Student:</b> <?php echo $studentrecord[0]['Name'].' '.$studentrecord[0]['Surname']; ?>   
                    <br />
                    <br />
                    <b>Teacher:</b> <?php echo $teachername[0]['firstname'].' '.$teachername[0]['lastname']; ?>   
                    <br />
                    <br />
                    <b>Room No:</b> <?php echo $room; ?>   
                    <br />
                    <br />
                    <b>Grade:</b> <?php echo $studentrecord[0]['grade_name'];?>
<!--                    <br />
                    <br />
                    <b>Assessment Period:</b> <?php echo date('F d, Y',strtotime($fdate)).' to '.date('F d, Y',strtotime($tdate));?>
-->
        </td>
                <td style="padding-left:10px;width:350px;color:#939393;height:50px;">
                    <table style="border:1px solid #939393;height:50px;width:350px;">
                        <tr>
                            <td style="width:350px;height:130px;vertical-align: top;">
                                <b>Description:</b> <?php echo $description;?>
                            </td>
                        </tr>
                    </table>
                </td>
        </tr>
        
                <tr >
                        <td colspan="2" >
                       &nbsp;
                        
                    </td>
                </tr>
        </table>
		
		<br/><br/>
		    <?php echo $htmlTable;?>
                <br/><br/>
                <table class="gridtable" style="width:750px;">
                    <tr>
                        <td>

                            <img width="740" src="<?php echo SITEURLM.$view_path; ?>inc/logo/inrep_<?php echo $studentid;?>.png"/>
                        </td>
                    </tr>
                    
                </table>
                
        
                  <!-- <br/><br/>
                  <table class="gridtable"><tr> <td height="40" style="width:700px;"> <span style="font-size:12"> Using This Report : </span> </td></tr></table>
              -->
   </page>
