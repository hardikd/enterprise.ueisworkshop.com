<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <base href="<?php echo base_url();?>"/>
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
   <link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
   
   <link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />
 
    <!--start old script -->
 
 	<style type="text/css" media="screen">
	
	#paginationstudent  
	{
		padding-top:10px;
		font-size: 12px;
    font-weight: bolder;	
	}
	#paginationstudent a, #paginationstudent strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #FFFFFF;
	font-size: 13px;


	}

	#paginationstudent strong, #paginationstudent a:hover {
	 font-weight: normal;
	 background: #5e3364;
	}		
	</style>
<style>
.black_overlay {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 158%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content {
	display: none;
	position: absolute;
	top: 25%;
	left: 31%;
	width: 459px;
	padding: 16px;
	border: 1px solid black;
	background-color: white;
	z-index:1002;
	overflow: auto;
}
.black_overlay1 {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 158%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content1 {
	display: none;
	position: fixed;
	top: 25%;
	left: 31%;
	width: 513px;
	padding: 16px;
	border: 1px solid black;
	background-color: white;
	z-index:1002;
	overflow: auto;
}
.black_overlay2 {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 158%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
.white_content2 {
	background-color: white;
	border: 1px solid black;
	height: 500px;
	left: 30%;
	overflow: auto;
	padding: 16px;
	position: absolute;
	top: 12%;
	width: 443px;
	display: none;
	z-index: 1002;
}
a.button {
    text-decoration: none;
}
.btnperformance
{
	 display: block;
   height:17px;
    width: 130px;
    background: #5e3364;
    border: 2px solid rgba(33, 68, 72, 0.59);
     
    /*Step 3: Text Styles*/
    color: #FFFFFF;
    text-align: center;
	
    /*font: bold 3.2em/100px "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;*/
     
    /*Step 4: Fancy CSS3 Styles*/
    background: -webkit-linear-gradient(top, #5e3364, #5e3364);
    background: -moz-linear-gradient(top, #5e3364, #5e3364);
    background: -o-linear-gradient(top, #5e3364, #5e3364);
    background: -ms-linear-gradient(top, #5e3364, #5e3364);
    background: linear-gradient(top, #5e3364, #5e3364);
     
    -webkit-border-radius: 50px;
    -khtml-border-radius: 50px;
    -moz-border-radius: 50px;
    border-radius: 3px;
     
 
}

a.btnperformance {
    text-decoration: none;
}
 
/*Step 5: Hover Styles*/
a.btnperformance:hover {
    background: #3d7a80;
    background: -webkit-linear-gradient(top, #3d7a80, #2f5f63);
    background: -moz-linear-gradient(top, #3d7a80, #2f5f63);
    background: -o-linear-gradient(top, #3d7a80, #2f5f63);
    background: -ms-linear-gradient(top, #3d7a80, #2f5f63);
    background: linear-gradient(top, #3d7a80, #2f5f63);
}
</style>
<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.12/video.js"></script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
   <!-- BEGIN HEADER -->
 <?php require_once($view_path.'inc/header.php'); ?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <?php require_once($view_path.'inc/teacher_menu.php'); ?>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
           <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <h3 class="page-title">
                      <i class="icon-wrench"></i>&nbsp; Tools & Resources
                      <!--<a href="#jobaidesModal" data-toggle="modal" ><span>Job Aides</span>&nbsp;<i class="icon-folder-open"></i></a>-->
                      <a href="#myModal-video" data-toggle="modal" style="margin-right:5px;"><span>Video Tutorial</span>&nbsp;<i class="icon-play-circle"></i></a>
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                       <li>
                           <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools">Tools & Resources</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>tools/assessment_manager">Assessment Manager</a>
                            <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo base_url();?>studentdetail">Individual Student Performance Summary</a>
                           
                       </li>
                       
                       
                       
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget purple">
                         <div class="widget-title">
                             <h4>Individual Student Performance Summary</h4>
                          
                         </div>
                         <div class="widget-body" style="min-height: 150px;">
                            <div class="space20"></div>
                                   
                                   <div class="space15"></div>
                                  <table>
                                   <tr>
                                   <td>
                                    <form action="studentdetail/getstudents" class="form-horizontal" method="post" name="searchstudent" >
                                   <div class="control-group">
                                    <?php if(isset($school_type)) $countst = count($school_type);
										if(isset($school_type) && $countst >0){ ?>
                                             <label class="control-label">Select Grade</label>
                                             <div class="controls">
		<select tabindex="1" style="width: 300px;" class="span12 chzn-select" name="schools_type" id="schools_type" onchange="updateschool(this.value);">
     										 <option value=""  selected="selected">Please Select</option>
												<?php
                                                if(!empty($school_type)){
                                               foreach($school_type as $k => $v){?>
                                               <option value="<?php echo $v['school_type_id']; ?>"><?php echo $v['tab']; ?></option>
                                                      <?php }} else {}?>
                                                    </select>
											</div>
                                         </div>   
                                          <div class="control-group">
                                              <label class="control-label">Schools</label>
                                             <div class="controls">
											<select name="schools" class="span12 chzn-select" id="schools" onchange="updateclass(this.value)">
                  							<option value=""  selected="selected">Please Select</optgroup>
             							 </select>
                                         
											</div>
                                         </div> 
                                          <?php }?> 
                                           <div class="control-group">
                                              <label class="control-label">Grades</label>
                                             <div class="controls">
									<select class="span12 chzn-select" name="grades" id="grades">
                                          <option value=""  selected="selected">Please Select</optgroup>
											  <?php
                                            if(isset($school_grade)) $countst = count($school_grade["grades"]);
                                            if(isset($school_grade) && $countst >0){
                                                
                                            foreach($school_grade["grades"] as $k=> $v)
                                            {
                                            ?>
                                             <option value="<?php echo $v["dist_grade_id"];?>"> <?php echo $v["grade_name"]; ?></option>
                                            
                                            <?php } }?>
                                               </select>
											</div>
                                         </div> 
                                          <?php if(isset($school_grade) && $countst >0){?>
        						<input type="hidden" name="schools" id="schools" value="<?php echo $school_grade["school_id"][0]["school_id"];?>" />
        						 <?php }?>
                           	              <div class="control-group">
                                         <label class="control-label"></label>
                                             <div class="controls"> 
    <input type="button" class="btn btn-small btn-purple" style=" padding: 5px; " value="Search" onclick="checkvalid()"  />
                             </div>    
            </div> 
             </form>   
                                </td></tr> 
                                </table>

                                <div class="space20"></div>
                                
                                <div id="reportDiv"  style="display:block;" class="answer_list" >
                                   <div class="widget purple">
                        	 <div class="widget-title">
                             <h4>Student Details</h4>
                          
                         </div> 
                                  <div class="widget-body" style="min-height: 150px;">
                                  
                                  
                                   
                                   <div class="space20"></div>
                                   
                          <!--        <table class="table table-striped table-bordered" id="editable-sample">
                            <thead>
                            <tr>
                                
                                <th >First Name</th>
                                <th class="sorting">Last Name</th>
                                <th class="no-sorting">Email</th>
                                <th class="no-sorting">School</th>
                                <th class="no-sorting">View Performance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">                                
                                <td>Carl</td>
                                <td class="hidden-phone">Batch</td>
                                <td class="hidden-phone">cb123@email.com</td>
                                <td class="center hidden-phone">School Name</td>
                                <td class="center hidden-phone"></td>                              </tr>
                            <tr class="odd gradeX">                                
                                <td>Able</td>
                                <td class="hidden-phone">Johnson</td>
                                <td class="hidden-phone">aj123@email.com</td>
                                <td class="center hidden-phone">School Name</td>
                                <td class="center hidden-phone"></td>                              </tr>
                                        <tr class="odd gradeX">                                
                                <td>Phyllis</td>
                                <td class="hidden-phone">Thomspson</td>
                                <td class="hidden-phone">pt123@email.com</td>
                                <td class="center hidden-phone">School Name</td>
                                <td class="center hidden-phone"></td>                              </tr>
                                </tbody>
                                </table>-->
       <div id="scoredetails" style="display:none;">
        <input type="hidden" id="pageid" value="">
        <div id="msgContainer"> </div>
      </div>
      <div class="content" style="margin-left:0;">
        <table class="table table-striped table-bordered" id="editable-sample">
          <tbody>
            <tr>
              <td align="left" colspan="5"><b><u> Student Details</u> </b></td>
            </tr>
            <tr class="tchead">
              <th>First Name</th>
              <th  class="sorting">Last Name</th>
              <th  class="sorting">Email</th>
              <th  class="sorting">School</th>
              <th  class="sorting">View Performance</th>
              <?php if($this->session->userdata('login_type')=='user')
				{ 
					?>
              <th  class="sorting">Action</th>
              <?php
				}
				?>
            </tr>
            <?php
			
			if(!empty($records))
			{
			foreach($records as $record)
			{
			
				?>
          
            <tr class="tcrow2">
              <td  class="hidden-phone" align="center"><?php echo $record['Name'];?></td>
              <td  class="hidden-phone" align="center"><?php echo $record['Surname'];?></td>
              <td  class="hidden-phone" align="center"><?php echo $record['email'];?></td>
              <?php
				 $schools = $this->Studentdetailmodel->getcschools($record['school_id']);
			
			
				?>
              <td class="center hidden-phone" align="center"><?php echo @$schools[0]['school_name'];?></td>
              <td><!--<input type="button" title="Edit" value="Student Performance" name="Edit" onclick="getperformance(<?php //echo $record['UserID'] ?>)">-->
                
                <!--  <input type="button" title="Edit" value="View As PDF" name="Edit" onclick="viewpdf(<?php echo $record['UserID'] ?>)"> 
            
                <a  target="_blank" class="btnperformance" 
 href="<?php echo base_url();?>studentdetail/studentreportaspdf/<?php echo $record['UserID']?>" >View as PDF</a> <br/>
                <?php // echo anchor('studentdetail/studentreportaspdf', 'View As PDF',array('target' => '_blank'));?>
               <!-- <input type="button" title="Edit" value="view As Graph" name="Edit" onclick="viewgraph(<?php //echo $record['UserID'] ?>)">-->
            
<a  class="btn btn-small btn-purple" href="<?php echo base_url();?>studentdetail/getsinglestudentdetail/<?php echo $record['UserID']?>" > Assessment Report</a>               
            <!--View as Graph-->
               
                
                <!--  <a id="<?php //echo $record['UserID']; ?>" href ="javascript:void(0)" onclick = "document.getElementById('light1').style.display='block';document.getElementById('fade1').style.display='block'">Student performance</a>--></td>
              <?php if($this->session->userdata('login_type')=='user')
				{
					?>
              <td class="center hidden-phone" align="center">
              <input type="button" title="Edit" value="Edit" name="Edit" onclick="editstudent(<?php echo $record['UserID'] ?>)" class="btn btn-small btn-purple">
                <input type="button" title="Delete" value="Delete" name="Delete" 
onclick="delete_stu(<?php echo $record['UserID'] ?>)" class="btn btn-small btn-purple"></td>
              <?php } ?>
            </tr>
            	
            <?php
			}
			}
			else
			{
				?>
                <tr class="tcrow2"><td colspan="6">No Record Available</td></tr> 
                <?php
			}
		?>
                                              
                        
          </tbody>
          
        </table>
       
        <div id="paginationstudent">
        <?php echo $this->paginationnew->create_links(); ?>
        </div>

      </div></div>
                                  </div>
                                
                                </div>   
                                </div>                    
                           
                           </div>       
                      
                                     
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
        UEIS © Copyright 2012. All Rights Reserved.
   </div>
   <!-- END FOOTER -->
   <div id="dialog" title="Student Details" style="display:none;"> Student Details </div>

<!-- LIGHT BOX STARTS HERE-->
<div id="light" class="white_content">
  <div style=" background-color:#5e3364; margin: -16px 0 0 -16px; padding: 5px; width: 481px;">
   <span style=" color: white; font-family: Tahoma;">Edit Student Detail</span>
    <span style=" float: right;   margin-right: 5px;"> 
    <a class="ui-icon ui-icon-closethick" href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></a> </span> </div>
  <form name="update_student"  method="post" action="studentdetail/updatestudent" >
    <table cellpadding="5" align="center">
      <tbody>
        <tr>
          <td>First Name</td>
          <td><input type="text" name="student_name" id="student_name" value="" /></td>
        </tr>
        <tr>
          <td>Last Name</td>
          <td><input type="text" name="student_surname" id="student_surname" value="" /></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><input type="text" name="student_email"  id="student_email" value=""/></td>
        </tr>
      <input type="hidden" name="userid" value="" id="userid" />
      <tr>
        <td></td>
        <td><input type="submit" class="btn btn-small btn-purple" name="btn_submit" value="update" /></td>
      </tr>
        </tbody>
      
    </table>
  </form>
</div>
<div id="fade" class="black_overlay"></div>

<div id="myModal-video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="width:700px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    
                                    <h3 id="myModalLabel4">Individual Student Performance Summary</h3>
                                </div>
                              
                                <div class="modal-body">
                                        
                                    <video id="example_video_1" class="video-js vjs-default-skin"
  controls preload="auto" width="640" height="360"
 
  data-setup='{"example_option":true}'>
 <source src="<?php echo SITEURLM;?>convertedVideos/Student Summary ReportConverted.mp4" type='video/mp4' />
 
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
                                </div>
                                <div class="modal-footer">
                  <button class="btn btn-danger" type='button' name='cancel' id='cancel' value='Cancel' data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
<!--<button type='button' name="button" id='saveoption' value='Add' class="btn btn-success"><i class="icon-plus"></i> Ok</button>-->
                                </div>
                    
                            </div>

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
   <script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script>
   <script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
 
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
     <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
   <script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>

   

   <!--common script for all pages-->
   <script src="<?php echo SITEURLM?>js/common-scripts.js"></script>
  <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/form-wizard.js"></script>
   
   <script src="<?php echo SITEURLM?>js/dynamic-table.js"></script>
   <script src="<?php echo SITEURLM?>js/form-validation-script.js"></script>
   <!--script for this page-->
   <script src="<?php echo SITEURLM?>js/editable-table.js"></script>
   <!-- END JAVASCRIPTS --> 
   <script>
       $('#myModal-video').on('hidden.bs.modal', function (e) {
                var myPlayer = videojs('example_video_1');
                myPlayer.pause();
              });
              
          $('#myModal-video').modal('show');
          var myPlayer = videojs('example_video_1');
        myPlayer.play();
       </script>
 <!--   <script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script>  
   
   <script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script>

 
   
    <script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script>-->
   
   <script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script type="text/javascript">
	
function delete_stu(userid)
{
	var con =confirm("Are you sure want to delete user");
	if(con)
	{
		$.ajax({
			url:'<?php echo base_url(); ?>studentdetail/delete_student?stuid='+userid,
			success:function(result)
			{
				window.location.reload();
				
				}
			
			});
	}
	else
	{
		return false;
	}
}
</script>
<script type="text/javascript">
function viewgraph(tag)
{
	document.getElementById('light2').style.display='block';
	document.getElementById('fade2').style.display='block';
	
	$.ajax({
	
			url:'<?php echo base_url(); ?>studentdetail/getstudentgraphdetail?stuid='+tag,
			success:function(result)
			{
				var split_data1=result.split("|");
				var getscored = split_data1[0];
				var pass_score = split_data1[1];
				var subj = split_data1[2];
				var status = split_data1[3];
				if(status==1)
				{
					var finalstatus = "passed";
				}
				else
				{
					var finalstatus = "failed";
				}
						
		    $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: false,
                plotShadow: false
            },
            title: {
                text: 'Pie Chart'+'<br/>'+subj+' '+'Student Score Detail'+' '+'Status'+' '+finalstatus
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Student Score',
                data: [
                    ['Pass Score',  eval(pass_score) ],
					['Student Score',   eval(getscored) ]
                ]
            }]
        });
		
		
        var colors = Highcharts.getOptions().colors,
            categories = ['Passed Score', 'Student Score '],
            name = 'Student Detail',
            data = [{
                    y: eval(pass_score),
                    color: colors[0],
                    drilldown: {
                        name: 'Passed Score',
                      
                        data: [eval(pass_score)],
                        color: colors[0]
                    }
                }, {
                    y: eval(getscored),
                    color: colors[1],
                    drilldown: {
                        name: 'Student Score',
                      
                        data: [eval(getscored)],
                        color: colors[1]
                    }
                }];
    
        function setChart(name, categories, data, color) {
			chart.xAxis[0].setCategories(categories, false);
			chart.series[0].remove(false);
			chart.addSeries({
				name: name,
				data: data,
				color: color || 'white'
			}, false);
			chart.redraw();
        }
    
        var chart = $('#containerbar').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Bar Chart'
            },
            /*subtitle: {
                text: 'Click the columns to view versions. Click again to view brands.'
            },*/
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Student Detail'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +'%';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {/*
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +'% market share</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view '+ point.category +' versions';
                    } else {
                        s += 'Click to return to browser brands';
                    }
                    return s;
                */}
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        })
        .highcharts(); // return chart
   
		
			}
		});
	
}
</script>
<script type="text/javascript">
function getperformance(userid)
{

	
	document.getElementById('light1').style.display='block';
	document.getElementById('fade1').style.display='block';
	$.ajax({
			url:'<?php echo base_url(); ?>studentdetail/getstudentperformance?userid='+userid,
			success:function(result)
			{ 
			
				if(result=="norecfound")
				{
				//$("#per td.date").html('');
				$("#per td.pass_score").html('');
				$("#per td.scorepercen").html('');
				$("#per td.assignment").html('');
			$("#per td.error").css('display','block').html("No Record Found").css('text-decoration','underline');
					
				}
				else
				{
				var split_data=result.split("|");
			
			
			$("#per td.assignment").html(split_data[3]);
			$("#per td.scorepercen").html(split_data[2]);
			//	$("#per td.date").html(split_data[0]);
				$("#per td.pass_score").html(split_data[1]);
				
				
				$("#per td.error").css('display','none').html("");
				}
			}
		});
	
}
</script>
<script type="text/javascript">
function editstudent(userid)
{
	
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block';
	$.ajax({
			url:'<?php echo base_url(); ?>studentdetail/getstudentDetail?stuid='+userid,
			success:function(result)
			{
				
				var split_data=result.split("|");
				$("#userid").val(split_data[0]);
				$("#student_name").val(split_data[1]);
				$("#student_surname").val(split_data[2]);
				$("#student_email").val(split_data[3]);
			}
		});
	
}
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light').style.display='none';
	 document.getElementById('fade').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light2').style.display='none';
	 document.getElementById('fade2').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light1').style.display='none';
	 document.getElementById('fade1').style.display='none';
	   } 
});
</script>
<script type="text/javascript">
$(document).keyup(function(e) {

  if (e.keyCode == 27)
   {
	 document.getElementById('light3').style.display='none';
	 document.getElementById('fade3').style.display='none';
	   } 
});
</script>

<script type="text/javascript">
function updateschool(tag)
{

	$.ajax({
		url:'<?php echo base_url();?>/studentdetail/getschools?school_type_id='+tag,
		success:function(result)
		{
			$("#schools").html(result);
			$("#schools").trigger("liszt:updated");
		}
		});
	
}
</script>
<script type="text/javascript">
function updateclass(tag)
{
	$.ajax({
		url:'<?php echo base_url();?>/studentdetail/getgrades?schoolid='+tag,
		success:function(result)
		{
			$("#grades").html(result);
			$("#grades").trigger("liszt:updated");
		}
		});
	
}
</script>
<script type="text/javascript">
function checkvalid()
{
	var school = document.getElementById('schools').value;
	var grade = document.getElementById('grades').value;
	<?php if(isset($school_type)) {?>
	var schooltype=document.getElementById('schools_type').value;

	if(schooltype=="")
	{
		alert('Please select school type');
		return false;
	}
	 <?php }?>
	 
	if(school=="")
	{
		alert('Please select school');
		return false;
	}
	if(grade=="")
	{
		alert('Please select grade');
		return false;
	}

document.searchstudent.submit();
	
}
</script>


    <!--end old script -->  
   
   
 
</body>
<!-- END BODY -->
</html>