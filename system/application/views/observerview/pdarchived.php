<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>UEIS Workshop</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-responsive.css" rel="stylesheet" />
<link href="<?php echo SITEURLM?>css_new/style-purple.css" rel="stylesheet" id="style_color" />
<link href="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.css" />

<!-- START OLD SCRIPT-->

<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM.$view_path; ?>js/videos.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/photos.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
var SITEURLM1 = '<?php echo SITEURLM; ?>';
</script>	
<script src="<?php echo SITEURLM?>js/jquery.lightbox.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<LINK href="<?php echo SITEURLM?>css/video.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
function openpdf(d)
{

var gradestr='';

gradestr+='<td id="fileview"><object  data="'+d+'" type="application/pdf"  width="610" height="500"></object> </td>';    
	$('#fileview').replaceWith(gradestr); 

}

function openfiles(d)
{
var gradestr='';

gradestr+='<td id="fileview" align="center" width="600px"><a href="'+d+'">DOWNLOAD</a></td>';    
	$('#fileview').replaceWith(gradestr); 

}
</script>


<!-- END OLD SCRIPT -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
<?php require_once($view_path.'inc/header.php'); ?>
<!-- END HEADER --> 
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid"> 
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse"> 
      
      <!-- BEGIN SIDEBAR MENU -->
 <?php require_once($view_path.'inc/teacher_menu.php'); ?>
      <!-- END SIDEBAR MENU --> 
    </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE -->
  <div id="main-content"> 
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid"> 
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          
          <h3 class="page-title"> <i class="icon-wrench"></i>&nbsp; Tools & Resources </h3>
          <ul class="breadcrumb" >
            <li> UEIS Workshop <span class="divider">&nbsp; | &nbsp;</span> </li>
            <li> <i class="icon-home"></i> <a href="<?php echo base_url();?>">Home</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools">Tools & Resources</a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>tools/professional_development">Professional Development Reports </a> <span class="divider">></span> </li>
            <li> <a href="<?php echo base_url();?>observerview/profdev">Individual </a> </li>
          </ul>
          <!-- END PAGE TITLE & BREADCRUMB--> 
        </div>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <div class="row-fluid">
        <div class="span12"> 
          <!-- BEGIN BLANK PAGE PORTLET-->
          
            <div class="widget-body">
              <div class="widget widget-tabs purple">
                <div class="widget-title"> </div>
                <div class="widget-body">
        
              <!-- END TAB 5 ARTIFACTS-->
              
              
                <div class="space20"></div>
                <h4 style="font-weight: bold;">
                Teacher Name:<?php echo $teacher_name;?>

                </h3>


		
		<table class="table table-striped table-bordered"  cellspacing="0" cellpadding="0" id="conf">
         <thead>
		<tr>
		<th>Date</th>
		<th>Action</th>
		</tr></thead>
		<?php if($archived!=false)
		{
		foreach($archived as $val)
		{
		?>
		<tr class="odd gradeX">
		<td class="hidden-phone"><?php echo $val;?></td>
		<td><a class="btn btn-purple" href="comments/pdarchived/<?php echo $val;?>"><i class="icon-eye"></i>View</a></td>
		</tr>
		<?php } } else { ?>
		<tr>
		<td>
		No Archived Found
		</td>
		</tr>
		<?php } ?>
		</table>	
			<!--<table><tr><td><div id="pagination"><?php// echo $this->pagination->create_links(); ?></div></td></tr></table>	-->
        </div>      
               
              </div>
            </div>

          <!-- END BLANK PAGE PORTLET--> 

      </div>
    </div>
  </div>
</div>
</div>
<div id="myModal-delete-student" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
  </div>
  <div class="modal-body"> Please select "Yes" to remove this file. </div>
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
    <button data-dismiss="modal" class="btn btn-success"><i class="icon-check"></i> Yes</button>
  </div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER --> 

<!-- BEGIN FOOTER -->
<div id="footer"> UEIS © Copyright 2012. All Rights Reserved. </div>
<!-- END FOOTER --> 

<!-- BEGIN JAVASCRIPTS --> 
<!-- Load javascripts at bottom, this will reduce page load time --> 
<script src="<?php echo SITEURLM?>js/jquery-1.8.3.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.nicescroll.js" type="text/javascript"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap/js/bootstrap-fileupload.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/clockface/js/clockface.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/date.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script> 
<script src="<?php echo SITEURLM?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script> 
<script src="<?php echo SITEURLM?>js/jquery.blockui.js"></script> 
<!-- ie8 fixes --> 
<!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]--> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/uniform/jquery.uniform.min.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo SITEURLM?>assets/data-tables/DT_bootstrap.js"></script> 
<script src="<?php echo SITEURLM?>assets/fancybox/source/jquery.fancybox.pack.js"></script> 

<!--common script for all pages--> 
<script src="<?php echo SITEURLM?>js/common-scripts.js"></script> 
<!--script for this page--> 
<script src="<?php echo SITEURLM?>js/dynamic-table.js"></script> 
<script src="<?php echo SITEURLM?>js/form-validation-script.js"></script> 
<script src="<?php echo SITEURLM?>js/form-wizard.js"></script> 
<script src="<?php echo SITEURLM?>js/form-component.js"></script> 

<!-- END JAVASCRIPTS --> 

<script>
       $(function () {
           $(" input[type=radio], input[type=checkbox]").uniform();
       });



   </script> 
<script>
   function showDiv() {
   document.getElementById('reportDiv').style.display = "block";
}

</script> 
<script>
 var elem = document.getElementById("select_1");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script> 
<script>
 var elem = document.getElementById("select_2");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script> 
<script>
 var elem = document.getElementById("select_3");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none";
};

</script> 
<script>
 var elem = document.getElementById("select_4");
elem.onchange = function(){
    var hiddenDiv = document.getElementById("journaling_article2");
    hiddenDiv.style.display = (this.value == "") ? "none":"block";
    var hiddenDiv = document.getElementById("journaling_video2");
    hiddenDiv.style.display = (this.value == "") ? "block":"none"
};

</script> 
<script>
function show(ele)    {      
    var links = ['upload1','upload2'];
    var srcElement = document.getElementById(ele);      
    var doShow = true;
    if(srcElement != null && srcElement.style.display == "block")
        doShow = false;
    for( var i = 0; i < links.length; ++i )    {
        var otherElement = document.getElementById(links[i]);      
        if( otherElement != null )
            otherElement.style.display = 'none';
    }
    if( doShow )
        srcElement.style.display='block';         
    return false;
  }
</script> 
<script>
       jQuery(document).ready(function() {
           EditableTable.init();
       });
   </script> 
<script>
   
   var button = document.getElementById('reportDiv2_btn');

button.onclick = function() {
    var div = document.getElementById('reportDiv2');
    if (div.style.display !== 'block') {
        div.style.display = 'block';
    }
    else {
        div.style.display = 'none';
    }
};

</script>
</body>
<!-- END BODY -->
</html>