<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>UEIS Workshop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <base href="<?php echo base_url(); ?>"/>
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo SITEURLM ?>css_new/style-purple.css" rel="stylesheet" id="style_color" />

        <link href="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/uniform/css/uniform.default.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.css" />

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php require_once($view_path . 'inc/header.php'); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <div class="sidebar-scroll">
                <div id="sidebar" class="nav-collapse collapse">


                    <!-- BEGIN SIDEBAR MENU -->
                    <?php require_once($view_path . 'inc/teacher_menu.php'); ?>

                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->  
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->   
                    <div class="row-fluid">
                        <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->

                            <h3 class="page-title">
                                <i class="icon-wrench"></i>&nbsp; Tools & Resources
                            </h3>
                            <ul class="breadcrumb" >

                                <li>
                                    UEIS Workshop
                                    <span class="divider">&nbsp; | &nbsp;</span>
                                </li>

                                <li>
                                    <i class="icon-home"></i> <a href="<?php echo base_url(); ?>">Home</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>tools">Tools & Resources</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>tools/professional_development">Professional Development</a>
                                    <span class="divider">></span>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>observerview/profdev">Individual Professional Development </a>

                                </li>




                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN BLANK PAGE PORTLET-->
                            <div class="widget purple">
                                <div class="widget-title">
                                    <h4>Individual Professional Development</h4>

                                </div>
                                <div class="widget-body">
                                    <div class="widget widget-tabs purple">
                                        <div class="widget-title">

                                        </div>
                                        <div class="widget-body">
                                            <div class="tabbable ">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#widget_tab1" data-toggle="tab">Videos</a></li>
                                                    <li class=""><a href="#widget_tab2" data-toggle="tab">Articles</a></li>
                                                    <li class=""><a href="#widget_tab3" data-toggle="tab">Journaling</a></li>
                                                    <li class=""><a href="#widget_tab4" data-toggle="tab">Journaling Response</a></li>
                                                    <li class=""><a href="#widget_tab5" data-toggle="tab">Artifacts</a></li>
                                                    <li class=""><a href="#widget_tab6" data-toggle="tab">Archive</a></li>

                                                </ul>
                                            </div>


                                            <div class="tab-content">
                                                <div class="tab-pane active" id="widget_tab1">
                                                    <div class="space20"></div>
                                                    <div id="tab1" class="tab_content" >
                                                        <?php if ($prof_groups != false) { ?>
                                                            <table align="center" class="table table-striped table-bordered" id="conf">
                                                                <tr>
                                                                    <th>
                                                                        Standard Name
                                                                    </th>
                                                                    <th>
                                                                        Video Name
                                                                    </th>
                                                                    <th>
                                                                        Video
                                                                    </th>
                                                                    <th>
                                                                        Actions
                                                                    </th>
                                                                </tr>
                                                                <?php
                                                                $i = 1;
                                                                foreach ($prof_groups as $val) {

                                                                    if ($i % 2 == 0) {
                                                                        $c = 'tcrow2';
                                                                    } else {
                                                                        $c = 'tcrow1';
                                                                    }
                                                                    ?>
                                                                    <tr class="<?php echo $c; ?>">
                                                                        <td>
                                                                            <?php echo $val['group_name']; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $val['name']; ?>
                                                                        </td>

  <td onClick="show(<?php echo $val['prof_dev_id']; ?>)">
  	<a href="javascript:void(0)" style="cursor:pointer;">Watch</a>
  </td>
   <td width="120">
<a href="javascript:void(0)" style="cursor:pointer;" name="assign" id="assign" onClick="assign(<?php echo $val['prof_dev_id']; ?>)"  > Assign </a>/
  <a href="javascript:void(0)" style="cursor:pointer;" name="assigned" id="assigned"  onclick="assigned(<?php echo $val['prof_dev_id']; ?>)" > Assigned </a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php $i++;
                                                                }
                                                                ?>
                                                            </table>
                                                        <?php } else { ?>
                                                            No Videos Found
<?php } ?>
                                                    </div>
                                                </div> 


                                                <div class="tab-pane" id="widget_tab2">
                                                    <div class="space20"></div>
                                                    <?php if ($article_groups != false) { ?>
                                          <table class="table table-striped table-bordered" >
                                               <thead>
                                               <tr>
                                                   <th >Standard Name</th>
                                                   <th >Article Name</th> 
                                                   <th >Link</th>    
                                                   <th >View</th>    
                                                   <th>Actions</th>                        
                                                   </tr>
                                               </thead>
                                               
                                               
                                               <tbody>
                                             <?php
                                                            $i = 1;
                                                            foreach ($article_groups as $val) {

                                                                if ($i % 2 == 0) {
                                                                    $c = 'tcrow2';
                                                                } else {
                                                                    $c = 'tcrow1';
                                                                }
                                                                ?>  
                                               
                                               <tr class="<?php echo $c; ?>">                               
                                                   <td class="hidden-phone"> <?php echo $val['group_name']; ?></td>
                                                   <td><?php echo $val['name']; ?></td>
                                                   <td><a class="btn btn-purple" href="<?php echo WORKSHOP_DISPLAY_FILES; ?>articles/<?php echo $val['article_dev_id'] . '.' . $val['link']; ?>"  target="_blank" style="cursor:pointer;">Link</a></td>
                                                  <td>
        <?php
        if ($val['link'] == 'pdf') {
            ?>
                                                                            <a class="btn btn-purple"  style="cursor:pointer;" onClick="javascript:openpdf('<?php echo WORKSHOP_DISPLAY_FILES; ?>articles/<?php echo $val['article_dev_id'] . '.' . $val['link']; ?>')">View</a>
                                                                        <?php
                                                                        } else {
                                                                            ?>
                                                                            <a class="btn btn-purple"  style="cursor:pointer;" onClick="javascript:openfiles('<?php echo WORKSHOP_DISPLAY_FILES; ?>articles/<?php echo $val['article_dev_id'] . '.' . $val['link']; ?>')">View</a>
                                                                        <?php } ?>
                                                                    </td>
                                                <td width="120">
                                                                        <a href="javascript:void(0)" style="cursor:pointer;" name="assign" id="assign" onClick="articlesassign(<?php echo $val['article_dev_id']; ?>)"  > Assign </a><br/><br/><a href="javascript:void(0)" style="cursor:pointer;" name="assigned" id="assigned"  onclick="articleassigned(<?php echo $val['article_dev_id']; ?>)" > Assigned </a>

                                                                    </td> 
                                                                    
								        <?php $i++;}} ?>   
                                         </tbody>
                                                   </table>
                                                   
                                            <div class="space20"></div>

                                                        <div id="reportDiv"  style="display:blick;" class="answer_list" >
                                                            <div class="widget purple">
                                                                <div class="widget-title">
                                                                    <h4>Article Title</h4>
                                                                </div> 
                                                                <div class="widget-body" style="min-height: 150px;">
                                                                    <br />
                                                                    <br />
                                                                      <?php if ($article_groups != false) { ?>
                                                                    <table>
                                                                        <tr>
                                                                            <td id="fileview">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
																	<?php } else { ?>
                                                                    No Articles Found
                                                                <?php } ?>
                                                            </div></div></div>         
                                                   
                                                   
                                                   
                                                   
                                                   
                                                    <!-- start new -->


                                                </div>

                               <div class="tab-pane" id="widget_tab3">
                                 <div class="space20"></div>
                                 
                                
                            
                                         
  
    
    
       <div class="form-horizontal" >  
                             	<div class="control-group">
                                             <label class="control-label">Select Teacher</label>
                                             <div class="controls">
                    <select  name="teachervalue" id="teachervalue" onChange="getall(this.value)"  style="width: 300px;">

                                       <option value="">-Please Select-</option>
											<?php if(!empty($teachers)) { 
												foreach($teachers as $teacherval) {
											?>
	<option value="<?php echo $teacherval['teacher_id'];?>"><?php echo $teacherval['firstname'].' '.$teacherval['lastname'];?></option>
	<?php } } ?>
	</select>
                                             </div>
                                         </div>
                                       
    
    						<div class="control-group">
                                             <label class="control-label">Select Video</label>
                                             <div class="controls">
					<select name="video" id="video" onChange="change()" style="width: 300px;">
                                     <option value="">--Please Select--</option>
                                        </select>
                                             </div>
                                         </div>
    
   
										<h4 style="margin:0px 0px 19px 313px; color:#000">(or)</h4>
	
    									<div class="control-group">
                                             <label class="control-label">Select Article</label>
                                             <div class="controls">
							<select  name="article" id="article" onChange="change()"  style="width: 300px;">
                        	             <option value="">--Please Select--</option>
                                        </select>
                                             </div>
                                         </div>
    

    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
<input class="btn btn-purple" type="submit" name="journal" id="journal" value="submit" onClick="getthis()">
                                             </div>
                                         </div>

    
    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
													<div id="comments"></div>
                                             </div>
                                         </div>
                                         </div>
    
    
    
                                         
                                         <div class="space20"></div>
                                          <div class="space20"></div>



                                          <!-- <div id="journaling_video">
                                                                             <div class="widget purple">
                                                                   <div class="widget-title">
                                                                       <h4>Video Title Here</h4>
                                                                    
                                                                   </div> 
                                                                            <div class="widget-body" style="min-height: 150px;">
                                                                            <h3 style="text-align:center;">Video Appears Here</h3>
                                                                            </div>
                                                                          
                                                                          </div>   
                                                                          </div>  
                                           <div id="journaling_article">
                                                                             <div class="widget purple">
                                                                   <div class="widget-title">
                                                                       <h4>Article Title Here</h4>
                                                                    
                                                                   </div>  
                                                                            <div class="widget-body" style="min-height: 150px;">
                                                                             <form action="#" class="form-horizontal">
                                                                             <div class="control-group">
                                                                          <label class="control-label">Article Comments</label>
                                                                          <div class="controls">
                                                                              <textarea class="span6 " rows="3"></textarea>
                                                                          </div>
                                                                      </div>
                                                                      <div class="form-actions">
                                                                          <button type="submit" class="btn btn-purple">Add</button>
                                                                          <button type="button" class="btn">Cancel</button>
                                                                      </div>
                                                                      </form>
                                                                            </div>
                                                                          
                                                                          </div>   
                                                                          </div>               -->
                                         
                                </div>
                                                
                                



								 <div class="tab-pane " id="widget_tab4">
                                 <div class="space20"></div>
                                <div class="form-horizontal" >  
                             	<div class="control-group">
                                             <label class="control-label">Select Teacher</label>
                                             <div class="controls">
                    <select name="teachervalue" id="teachervalue" onChange="getrefall(this.value)"  style="width: 300px;">

                                        <option value="">--Please Select--</option>
                                        <?php if(!empty($teachers)) { 
											foreach($teachers as $teacherval) {
										?>
						<option value="<?php echo $teacherval['teacher_id'];?>"><?php echo $teacherval['firstname'].' '.$teacherval['lastname'];?></option>
									<?php } } ?>
                                        </select>
                                             </div>
                                         </div>
                                       
    
    						<div class="control-group">
                                             <label class="control-label">Select Video</label>
                                             <div class="controls">
					<select name="refvideo" id="refvideo" onChange="refchange()"  style="width: 300px;">
                                     <option value="">--Please Select--</option>
                                        </select>
                                             </div>
                                         </div>
    
   
										<h4 style="margin:0px 0px 19px 313px; color:#000">(or)</h4>
	
    									<div class="control-group">
                                             <label class="control-label">Select Article</label>
                                             <div class="controls">
							<select name="refarticle" id="refarticle" onChange="change()"  style="width: 300px;">
                        	             <option value="">--Please Select--</option>
                                        </select>
                                             </div>
                                         </div>
    

    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
<input class="btn btn-purple" type="submit" name="journal" id="journal" value="submit" onClick="getrefthis()">
                                             </div>
                                         </div>

    
    									<div class="control-group">
                                             <label class="control-label"></label>
                                             <div class="controls">
													<div id="refcomments"></div>
                                             </div>
                                         </div>
                                         </div>
	
                                         <div class="space20"></div>
                                          <div class="space20"></div>
                                         
                                       

                        
                                  </div>
                                
                               <div class="tab-pane" id="widget_tab5">
                                 <div class="space20"></div>
                                         <div class="form-horizontal" >  
                                         <div class="control-group">
									<label class="control-label">Select Teacher</label>
                                             <div class="controls">
<select class="span12 chzn-select" style="width:150px;" name="articraftsteacher" id="articraftsteacher" onChange="articraftschange()">
							<option value="">-Please Select-</option>
									<?php if(!empty($teachers)) { 
											foreach($teachers as $teacherval) {
									?>
						<option value="<?php echo $teacherval['teacher_id'];?>"><?php echo $teacherval['firstname'].' '.$teacherval['lastname'];?></option>
									<?php } } ?>
						</select>
                    </div>
                 </div>
                 
                		     <div class="control-group">
								<label class="control-label"></label>
                             <div class="controls">
							<input type="submit" class="btn btn-purple" name="articrafts" id="articrafts" value="Get" onClick="articrafts()">
                    </div>
                 </div> 
      </div>
    
    
    
    
    
    
    
    
	<table width="100%" cellpadding=0 cellspacing=0 style="display:none;margin-left:3px;" id="articraftsdisplay">
		<tr><td>
		<ul class="subtabs">
   
	<li><a href="#tab7" >Notes</a></li>
	<li><a href="#tab8" >Links</a></li>
	<li><a href="#tab9" >Photos</a></li>
	<li><a href="#tab10" >Videos</a></li>
	<li><a href="#tab11" >Files</a></li>
	
	
	</ul>
	</td>
	</tr>
	
	
	<tr class="sub_tab_container">
	<td>
	<div id="tab7" class="sub_tab_content" >
	<br />
	<table style="width:600px;">
	<tr>
	<td width="50px;">
	Notes:
	</td>
	<td id="notesdata">
	
	</td>
	</tr>
	
	</table>
	</div>
	<div id="tab8" class="sub_tab_content" >
	<table >
	<tr>
	<td align="center">
	<div id="linkdetails" style="display:none;">
		<input type="hidden" id="linkpageid" value="">
		<div id="linkmsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
	</div>
	<div id="tab9" class="sub_tab_content" >
	<table>
	<tr>
	<td align="center">
	<div id="photodetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
	
	</div>
	<div id="tab10" class="sub_tab_content" >
	<table >
	<tr>
	<td align="center">
	<div id="videodetails" style="display:none;">
		<input type="hidden" id="videopageid" value="">
		<div id="videomsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
	</div>
	<div id="tab11" class="sub_tab_content" >
	<table >
	<tr>
	<td align="center">
	<div id="filedetails" style="display:none;">
		<input type="hidden" id="filepageid" value="">
		<div id="filemsgContainer">
			</div>
		</div>
	</td>
	</tr>
	</table>
	</div>
	</td>
	</tr>
	</table>           
       <!-- END WIDGET BODY FIRST-->

                                </div><!-- END TAB 5 ARTIFACTS-->
                               <div class="tab-pane" id="widget_tab6">
                                <div class="space20"></div>
								
                                <!--<table class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th >Date</th>
                                <th >Action</th>                              
                                </tr>
                            </thead>
                            <tbody>
                            <tr class="odd gradeX">                               
                                <td class="hidden-phone">10-22-12</td>
                                <td><a href="pd-archive.html" role="button" class="btn btn-purple"><i class="icon-eye"></i> View</a></td>  
                               </tr>
                            </tbody>
                                </table>  -->
<table align="center" width="100%">
	<tr>
	<td align="center" >
	Select Teacher:<select class="span12 chzn-select" style="width:150px;" name="archiveteacher" id="archiveteacher" >
	<option value="">-Please Select-</option>
	<?php if(!empty($teachers)) { 
	foreach($teachers as $teacherval) {
	?>
	<option value="<?php echo $teacherval['teacher_id'];?>"><?php echo $teacherval['firstname'].' '.$teacherval['lastname'];?></option>
	<?php } } ?>
	</select>
	</td>
	<td width="200px" align="center">
	<a href="observerview/pdarchive">Archived data</a>
	</td>
	</tr>
	<tr>
	<td align="center" >
	<input type="submit" class="btn btn-purple" name="archive" id="archive" value="Archive" onClick="archive()">
	</td>
	</tr>
    </table> 
      
    
    
    
                                   
                                </div>  



                                            </div>

                                        </div>
                                        <!-- END BLANK PAGE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>  </div>
                </div>

                <div id="assignteacher" title="Assign To Teacher" style="display:none;"> 


                    <table cellpadding="0" cellspacing="0" border=0 class="jqform1" >
                        <tr><td class='style1'></td><td>
                                <span style="color: Red;display:none" id="message"></span>
                            </td>
                        </tr>
                        <tr id="video_name">
                            <td colspan="2" align="center">
                                Standard Name:  Video Name:
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input type="hidden" name="prof_dev_id" id="prof_dev_id" value="">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" >
                                <font color="red">*</font>Select Teacher:
                            </td>
                            <td  >
                                <select name="teacher" id="teacher">
                                    <option value="">-Please Select-</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td  align="left"><input class="btnsmall" type="submit" name="submit" id="assignsubmit" onClick="assignsubmit()" value="Submit"><input class="btnsmall" type="submit" name="cancel" id="cancel" value="Cancel"></td>
                        </tr>




                        </tr></table>
                </div>
                <div id="assignedteacher" title="Assigned To Teacher" style="display:none;"> 


                    <table cellpadding="0" cellspacing="0" border=0 class="jqform1">
                        <tr><td class='style1'></td><td>
                                <span style="color: Red;display:none" id="message"></span>
                            </td>
                        </tr>
                        <tr id="video_namea">
                            <td colspan="2" align="center">
                                Standard Name:  Video Name:
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <br />
                            </td>
                        </tr>
                        <tr >
                            <td colspan="2" align="center">
                                <b>Assigned Teachers List</b>
                                <table id="teacherdata">
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td  align="center"><input class="btnsmall" type="submit" name="cancel" id="cancela" value="Cancel"></td>
                        </tr>




                        </tr></table>
                </div>
                <div id="watch" title="watch a video" style="display:none;">
                    <table cellpadding="0" cellspacing="0" border=0 class="jqform">
                        <tr>
                            <td id="alink">
                                Link:
                            </td>
                        </tr>
                        <tr>
                            <td id="avideo">
                            </td>
                        </tr>
                    </table>
                </div>



<?php /* ?>    
  <div id="myModal-delete-student" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h3 id="myModalLabel4">Are you sure you want to delete?</h3>
  </div>
  <div class="modal-body">
  Please select "Yes" to remove this file.
  </div>
  <div class="modal-footer">
  <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel</button>
  <button data-dismiss="modal" class="btn btn-success"><i class="icon-check"></i> Yes</button>
  </div>
  </div>
  <?php */ ?>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  

        <!-- END CONTAINER -->
	
        <!-- BEGIN FOOTER -->
        <div id="footer">
            UEIS © Copyright 2012. All Rights Reserved.
        </div>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS -->
        <!-- Load javascripts at bottom, this will reduce page load time -->
        <script src="<?php echo SITEURLM ?>js/jquery-1.8.3.min.js"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>   
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/date.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/jquery-validation-1.11.1/dist/additional-methods.min.js"></script>


        <script src="<?php echo SITEURLM ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.blockui.js"></script>
        <!-- ie8 fixes -->
        <!--[if lt IE 9]>
        <script src="js/excanvas.js"></script>
        <script src="js/respond.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/jquery.dataTables.js"></script>

        <script type="text/javascript" src="<?php echo SITEURLM ?>assets/data-tables/DT_bootstrap.js"></script>
        <script src="<?php echo SITEURLM ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>


        <!--common script for all pages-->
        <script src="<?php echo SITEURLM ?>js/common-scripts.js"></script>
        <!--script for this page-->
        <script src="<?php echo SITEURLM ?>js/dynamic-table.js"></script>

        <script src="<?php echo SITEURLM ?>js/form-validation-script.js"></script>
        <script src="<?php echo SITEURLM ?>js/form-wizard.js"></script>
        <script src="<?php echo SITEURLM ?>js/form-component.js"></script>
        <!--start old scirpt --> 
<?php /* ?><link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" /><?php */ ?>
        <script src="<?php echo SITEURLM ?>js/jquery.js" type="text/javascript"></script>
        <script src="<?php echo SITEURLM ?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <LINK href="<?php echo SITEURLM ?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
        <script src="<?php echo SITEURLM . $view_path; ?>js/assign_prof.js" type="text/javascript"></script>
        <script src="<?php echo SITEURLM . $view_path; ?>js/articrafts.js" type="text/javascript"></script>
        <script src="<?php echo SITEURLM ?>js/jquery.form.js" type="text/javascript"></script>
        <script type="text/javascript">
                                var SITEURLM1 = '<?php echo SITEURLM; ?>';
        </script>	
        <script src="<?php echo SITEURLM ?>js/jquery.lightbox.min.js" type="text/javascript"></script>
        <LINK href="<?php echo SITEURLM ?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
        <link href="<?php echo SITEURLM ?>css/video.css"  rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            <?php if($this->session->userdata('school_id')):?>
                var school_id =<?php echo $this->session->userdata('school_id') ?>;
            <?php endif;?>
        </script>
        <script type="text/javascript">
            function openpdf(d)
            {

                var gradestr = '';

                gradestr += '<td id="fileview"><object  data="' + d + '" type="application/pdf"  width="610" height="500"></object> </td>';
                $('#fileview').replaceWith(gradestr);

            }

            function openfiles(d)
            {
                var gradestr = '';

                gradestr += '<td id="fileview" align="center" width="600px"><a href="' + d + '">DOWNLOAD</a></td>';
                $('#fileview').replaceWith(gradestr);

            }
        </script>

        <!--end old script -->

        <!-- END JAVASCRIPTS --> 

        <script>
            $(function() {
                $(" input[type=radio], input[type=checkbox]").uniform();
            });



        </script>  

        <script>
            function showDiv() {
                document.getElementById('reportDiv').style.display = "block";
            }

        </script>







        <script>
            var elem = document.getElementById("select_1");
            elem.onchange = function() {
                var hiddenDiv = document.getElementById("journaling_video");
                hiddenDiv.style.display = (this.value == "") ? "none" : "block";
                var hiddenDiv = document.getElementById("journaling_article");
                hiddenDiv.style.display = (this.value == "") ? "block" : "none";
            };

        </script>

        <script>
            var elem = document.getElementById("select_2");
            elem.onchange = function() {
                var hiddenDiv = document.getElementById("journaling_article");
                hiddenDiv.style.display = (this.value == "") ? "none" : "block";
                var hiddenDiv = document.getElementById("journaling_video");
                hiddenDiv.style.display = (this.value == "") ? "block" : "none"
            };

        </script>



        <script>
            var elem = document.getElementById("select_3");
            elem.onchange = function() {
                var hiddenDiv = document.getElementById("journaling_video2");
                hiddenDiv.style.display = (this.value == "") ? "none" : "block";
                var hiddenDiv = document.getElementById("journaling_article2");
                hiddenDiv.style.display = (this.value == "") ? "block" : "none";
            };

        </script>

        <script>
            var elem = document.getElementById("select_4");
            elem.onchange = function() {
                var hiddenDiv = document.getElementById("journaling_article2");
                hiddenDiv.style.display = (this.value == "") ? "none" : "block";
                var hiddenDiv = document.getElementById("journaling_video2");
                hiddenDiv.style.display = (this.value == "") ? "block" : "none"
            };

        </script>
    </body>
    <!-- END BODY -->
</html>