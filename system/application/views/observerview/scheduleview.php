<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Schedule::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/calebderical.js" type="text/javascript"></script>

<script src="<?php echo SITEURLM.$view_path; ?>js/schedule.js" type="text/javascript"></script>
<script type="text/javascript">
var base_url = "<?php echo SITEURLM;?>";
$(document).ready(function() {
$('a.title').cluetip({splitTitle: '|',activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop'
});
});

</script>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/obmenu.php'); ?>
        <div class="content">
		<form method="post" action="observerview/scheduleview">
		<?php 
		 if(isset($selectdate)) { 
		 $selectdate=$selectdate;
		 } else { 
		 $selectdate= date('m-d-Y');
		 }
		?>
		<table align="center">
		<tr>
		<td>
		Select Week:
		</td>
		<td>
		<input type="text" name="selectdate" id="selectdate" readonly value="<?php echo $selectdate;?>">
		</td>
		<td>
		<input type="submit" name="go" value="go" id="go">
		</td>
		</tr>
		</table>
		</form>
		<table >
		<tr>
		<td>
		<b>Teacher Name:</b><?php echo $teacher_name;?>
		</td>
		</tr>
		</table>
		<table id="datetable" align="center" style="width:650px;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
    <tr>
  <td colspan="5" align="left" bgcolor="#657455" style="color:#FFF" >
  <b><img style="float:left;margin:3px;" src="<?php echo SITEURLM?>/images/task.jpg" height="20">&nbsp;&nbsp;&nbsp;&nbsp;Scheduler</b>&nbsp;&nbsp;&nbsp;&nbsp; <b>From: <?php echo $fromdate;?> To:<?php echo $todate;?></b>
  </td>
  </tr>
  <tr align="center" style="color:#333;">
  <td width="120px" bgcolor="#CCCCCC">Date</td>
  <td width="120px" bgcolor="#CCCCCC">Time</td>
  <td width="120px" bgcolor="#CCCCCC">Task</td>
  <td width="120px" bgcolor="#CCCCCC">Actions</td>
  <td width="120px" bgcolor="#CCCCCC">Details</td>
  </tr>
	<?php if($getscheduleplans!=false)
	{
	$le=0;
	  $je=0;
	  
	  foreach($getscheduleplans as $getplan)
	  {
	  $le=$getplan['schedule_week_plan_id'];
		$lecomments=$getplan['comments'];
	  ?>
		<tr align="center">
	     <td width="120px"><?php echo $getplan['date']?></td>
		 <td width="120px">
		<?php 
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		echo $start2." to ".$end2 ?>
		</td><td width="120px"><?php echo $getplan['task']?></td>
		<?php if($getplan['date']>=date('m-d-Y'))
		 {
		 ?>
		  <td width="150px"><input style="float:left;" type="button" name="Edit" id="Edit" value="Edit" onclick='edit(<?php echo $le;?>)' ><input  type="button" name="Delete" id="Delete" value="Delete" onclick="deleteweek(<?php echo $le;?>)"></td><td width="120px"><a class="title" href="#" title="Details|<?php if($lecomments!=''){ echo $lecomments; } else { echo "No Details Found. "; }  ?> ">View</a></td></tr><tr align='center'>
		 
		 <?php
		 }
		 else
		 {?>
		 <td width="150px">&nbsp;</td><td width="120px"><a class="title" href="#" title="Details|<?php if($lecomments!=''){ echo $lecomments; } else { echo "No Details Found. "; }  ?> ">View</a></td></tr><tr align="center">
		 
		 <?php }?>
	
		 
		 
	 </tr> <?php $le=$getplan['schedule_week_plan_id'];
		$lecomments=$getplan['comments'];  } }  else {?>
	<tr>
	<td colspan="5" align="center">
	No Task Found
	</td>
	</tr>
	
	<?php } ?>
	
		</table>	
			<table>
			
	<tr>
  <td align="right" colspan="5"><input type="button" name="Add" id="Add" value="Add Tasks" onclick="addplan('<?php echo $selectdate;?>')"></td>
  </tr>
		
			</table>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Task" style="display:none;"> 

<form name='schedule' id='schedule' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td colspan="5">
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date1' name='date1' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start1' name='start1' readonly >
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end1' name='end1' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task1:
				</td>
				<td valign="top" >
				<select name="task1" id="task1">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments1' name='comments1'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date2' name='date2' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start2' name='start2' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end2' name='end2' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task2:
				</td>
				<td valign="top" >
				<select name="task2" id="task2">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments2' name='comments2'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date3' name='date3' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start3' name='start3' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end3' name='end3' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task3:
				</td>
				<td valign="top" >
				<select name="task3" id="task3">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments3' name='comments3'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date4' name='date4' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start4' name='start4' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end4' name='end4' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task4:
				</td>
				<td valign="top" >
				<select name="task4" id="task4">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments4' name='comments4'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date5' name='date5' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start5' name='start5' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end5' name='end5' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task5:
				</td>
				<td valign="top" >
				<select name="task5" id="task5">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments5' name='comments5'  ></textarea>
				
				</td>
				
			</tr>
		
			
			
			
						
<tr><td valign="top"></td><td valign="top" colspan="10"><input class="btnbig" type='submit' name="submit" id='teacheradd' value='Add' title="Add New" style="float:left;margin-right:10px;margin-left:230px;"><input class="btnbig" type='button' name='cancel' id='cancel' value='Cancel' title="Cancel" style="float:left;"></td></tr></table></form>
</div>
<div id="updatedialog" title="Task" style="display:none;"> 

<form name='scheduleupdate' id='scheduleupdate' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="messageupdate"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text' readonly  id='date' name='date' value=''>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox'  type='text'  id='start' name='start' readonly >
				<input  type='hidden'  id='schedule_week_plan_id' name='schedule_week_plan_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox"  type='text'  id='end' name='end' readonly >
				
				</td>
				
			</tr>
			
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Task:
				</td>
				<td valign="top" >
				<select name="task" id="task">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:250px" type='text'  id='comments' name='comments'  ></textarea>
				
				</td>
				
			</tr>
		
			
			
			
						
<tr><td valign="top"></td><td valign="top"><input class="btnbig" type='submit' name="submit" id='teacheradd' value='Edit' title="Edit" > <input class="btnbig" type='button' name='cancel1' id='cancel1' value='Cancel' title="Cancel"></td></tr></table></form>
</div>
</body>
</html>