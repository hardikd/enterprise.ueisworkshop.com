<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::LessonPlan::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM?>js/atooltip.min.jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/cluetip.css" rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script type="text/javascript">
$(document).ready(function() {
$('a.title').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false
});
$('a.standard').cluetip({activation: 'click', closePosition: 'title',
  closeText: 'close',sticky: true,positionBy: 'bottomTop',ajaxCache: false,width:'600px'
});
$("#selectdate").datepicker({ dateFormat: 'mm-dd-yy',changeYear: true
		 });
		 
		 $('#save').click(function() {
            pdata={};
			pdata.plan_id=$('#comment_id').val();
			pdata.comments=$('#commentsvalue').val();
			
		   $.post('observerview/addcomments',{'pdata':pdata},function(data) {
		  
		  
		  if(data.status==1)
		  {
            
			 

		 }
		 else
		 {
		 
			 alert('Failed Please Try Again ');
		 
		 }
		 $('#comments').dialog('close');

},'json');	

});
	
	 
});
function comments(id)
{
 
 var p_url='observerview/getcomments/'+id;

    $.getJSON(p_url,function(result)
	{
	  if(status!='')
	  {
	  $('#commentsvalue').val(result.status);
	  }
	  else
	  {
	    $('#commentsvalue').val('');
	  }
	
	
	});
 $("#comment_id").val(id);
 
 $("#comments").dialog({
			modal: true,
           	height: 300,
			width: 400
			});
				
}
</script>
</head>
<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
		<form method="post" action="observerview/lessoncommentsview">
		<table align="center">
		<tr>
		<td style="padding-right:100px;">		
		<input onclick="document.location.href='<?php echo REDIRECTURL;?>observerview/dayview/<?php echo $teacher_id;?>'" type="button" name="Day View" id="Day View" value="Day View" >
		</td>
		<td>
		Select Week:
		</td>
		<td>
		<input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo $teacher_id;?>">
		<input type="text" name="selectdate" id="selectdate" readonly value="<?php if(isset($selectdate)) { echo $selectdate;} else { echo date('m-d-Y');}?>">
		</td>
		<td>
		<input type="submit" name="go" value="go" id="go">
		</td>
		<td style="padding-left:100px;">
		
		<input onclick="window.open('<?php echo REDIRECTURL;?>observerview/printweekview/<?php echo $teacher_id ?>/<?php if(isset($selectdate)) { echo $selectdate;} else { echo date('m-d-Y');}?>');" type="button" name="Print View" id="Print View" value="Print View" >
		</td>
		</tr>
		</table>
		</form>
		
		<table >
		<tr>
		<td>
		<b>Teacher Name:</b><?php echo $teacher_name;?>
		</td>
		</tr>
		</table>
		<?php 
		//print_r($dates);
		
		foreach($dates as $val)
		
		{
		$k=0;
		?>
		<table align="center" style="width:650px;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="<?php echo count($lessonplans)+4?>" align="center" bgcolor="#657455" style="color:#FFF"><b>Date:<?php echo $val['date'];?> &nbsp;&nbsp;&nbsp;Week:<?php echo $val['week'];?></b></td>
  </tr>
  <tr align="center" style="color:#333;">
    <td width="100px" bgcolor="#CCCCCC">Time</td>
	<td width="100px" bgcolor="#CCCCCC">Subject</td>
    <?php 
	$j=3;
	if($lessonplans!=false){
	 foreach($lessonplans as $plan)
	 {
	 $j++;
	 ?>
	<td width="100px" bgcolor="#CCCCCC"><?php echo $plan['tab'];?></td>
    <?php } ?>
	<td width="40px" bgcolor="#CCCCCC">Details</td>
	<td width="100px" bgcolor="#CCCCCC">Comments</td>
	
	
	<?php } ?>
  </tr>
    <tr align="center"  >
	<td colspan="<?php echo $j+1;?>">
	<table   id="<?php echo $val['date'];?>" >
    <?php if($getlessonplans!=false)
	{
	
	$le=0;
	  $je=0;
	 // echo "<pre>";
	  //print_r($getlessonplans);
	  foreach($getlessonplans as $getplan)
	  {
      if( $val['date']==$getplan['date'])
	{	  
	  $je++;
	  if($le!=$getplan['lesson_week_plan_id'])
		{	
		$les=0;
	     if($je==1)
		 { 
		    ?>
			<tr align="center">
		 
		 <?php }
		 else
		 {
		 if($val['date']>=date('m-d-Y'))
		 {
		 ?>
		  <td width="40px"><a class="standard" href="#" title="Other" rel="observerview/getother/<?php echo $le;?>">View</a></td>
		  <td width="100px"><a href='javascript:;' title="Add"  onclick='comments(<?php echo $le;?>)' >Add&nbsp;&nbsp;</a><a class="title" href="#" title="Comments" rel="observerview/getcommentsbyid/<?php echo $le;?>">View</a></td>
		  
		  </tr><tr align='center'>
		 
		 <?php
		 }
		 else
		 {?>
		 <td width="40px"><a class="standard" href="#" title="Other" rel="observerview/getother/<?php echo $le;?>">View</a></td>
		 <td width="100px"><a class="title" href="#" title="Comments" rel="observerview/getcommentsbyid/<?php echo $le;?>">View</a></td>
		 
		 </tr><tr align="center">
		 
		 <?php }
		 }
		 ?>
		
		<td width="100px"><?php 
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		 
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		 if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		echo $start2." to ".$end2 ?></td>
		<td width="100px"><?php echo $getplan['subject_name']?></td>
		<td width="100px"><?php echo $getplan['subtab']?></td>
		
		<?php 
		}
		else
		{
		$les++;
		?>
		  <td width="100px"><?php echo $getplan['subtab']?></td>
		
		<?php
		}
		
		$le=$getplan['lesson_week_plan_id'];
		$lecomments[$le]=$getplan['comments'];
	  
	  }
	  
	  }
	  if($je==0)
	  {
	  ?>
	    <tr align="center">
		<td colspan="<?php echo $j;?>">No Plans Found </td>
		
	<?php } if($le!=0) {
	/*if(($les+1)>=count($lessonplans))
	{
	
	
	}
	else
	{
	 ?>
	 <td width="<?php echo (count($lessonplans)-$les-1)*100;?>px" ></td>
	<?php } */
	if($val['date']>=date('m-d-Y'))
		 {
	?>	
	<td width="40px"><a class="standard" href="#" title="Other" rel="observerview/getother/<?php echo $le;?>">View</a></td>
	<td width="100px"><a href='javascript:;' title="Add"  onclick='comments(<?php echo $le;?>)' >Add&nbsp;&nbsp;</a><a class="title" href="#" title="Comments" rel="observerview/getcommentsbyid/<?php echo $le;?>">View</a></td>
	<?php } else { ?>
	
	<td width="40px"><a class="standard" href="#" title="Other" rel="observerview/getother/<?php echo $le;?>">View</a></td>
	<td width="100px"><a class="title" href="#" title="Comments" rel="observerview/getcommentsbyid/<?php echo $le;?>">View</a></td>
	
	<?php } } ?> </tr></table>
	
	


	<?php } else { ?>
	<tr><td colspan="<?php echo $j;?>">No Plans Found  </td></tr></table></td></tr>
	<?php } ?>
	<?php
	if($val['date']>=date('m-d-Y'))
		 {
		 ?>
	
  <?php } ?>
	</table>
	</td>
  </tr>
 
  
  
</table>
<br />
		<?php } ?>

		
			
			
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Lesson Plan" style="display:none;"> 

<form name='teacherplan' id='teacherplan' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td>
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox" type='text' readonly  id='date' name='date' value=''>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox'  type='text'  id='start' name='start' readonly >
				<input  type='hidden'  id='lesson_week_plan_id' name='lesson_week_plan_id'>
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox"  type='text'  id='end' name='end' readonly >
				
				</td>
				
			</tr>
		<?php	if($lessonplans!=false){
	 foreach($lessonplans as $all)
	 {
	 
	 ?>
	 <tr>
				<td valign="top" class="style1">
				<font color="red">*</font><?php echo $all['tab']?>:
				</td>
				<td valign="top" >
				<select name="<?php echo $all['lesson_plan_id']?>" id="<?php echo $all['lesson_plan_id']?>">
				<?php 
				foreach($lessonplansub as $sub)
				{
				if($all['lesson_plan_id']==$sub['lesson_plan_id']) {?>
				<option value="<?php echo $sub['lesson_plan_sub_id']?>"><?php echo $sub['subtab']?></option>
				<?php } } ?>
				
				</td>
				
			</tr>
	 
	 
	 <?php  } }?>
			
			
			
						
</tr><tr><td valign="top"></td><td valign="top"><input class="btnbig" type='submit' name="submit" id='teacheradd' value='Add' title="Add New" > <input class="btnbig" type='button' name='cancel' id='cancel' value='Cancel' title="Cancel"></td></tr></table></form>
</div>
<div id="comments" title="comments" style="display:none;">
<table>
<tr>
<td>
Comments:
</td>
<td>
<textarea style="width:200px;" name="commentsvalue" id="commentsvalue"></textarea>
<input type="hidden" name="comment_id" id="comment_id" value="">
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input type="button" name="save" id="save" value="save" >
</td>
</tr>
</table>


</div>
</body>
</html>