<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Archived Observation Plan::</title>
<base href="<?php echo base_url();?>"/>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />

</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php 
		if($this->session->userdata('login_type')=='observer')
		{
			require_once($view_path.'inc/observermenu.php'); 
		}
		else if($this->session->userdata('login_type')=='teacher')
		{
           require_once($view_path.'inc/obmenu.php'); 
		}	
		
		?>
        <div class="content">
		<div class="htitle">Archived Observation Plans</div>
		Teacher Name:<?php echo $teacher_name;?>
		<table class="tabcontent" cellspacing="0" cellpadding="0" id="conf">
		<tr class="tchead">
		<th>
		Date
		</th>
		<th>
		Action
		</th>
		<th>
		PDF
		</th>
		</tr>
		<?php if($archived!=false)
		{
		foreach($archived as $val)
		{
		?>
		<tr>
		<td>
		<?php 
		echo $val['archived'];?>
		</td>
		<td>
		<a href="comments/archived/<?php echo $val['archived'];?>">View</a>
		</td>
		<td>
		<a href="comments/createobservationplanpdf/<?php echo $val['archived'];?>" target="_blank"><img style="margin-top:3px;" src="<?php echo SITEURLM?>images/pdf_icon.gif"></a>
		</td>
		</tr>
		<?php } } else { ?>
		<tr>
		<td>
		No Archived Found
		</td>
		</tr>
		<?php } ?>
		</table>	
			<table><tr><td><div id="pagination"><?php echo $this->pagination->create_links(); ?></div></td></tr></table>	
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>

</body>
</html>
