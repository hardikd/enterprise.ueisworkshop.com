<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Workshop::Task::</title>
<base href="<?php echo base_url();?>"/>
<script src="<?php echo SITEURLM?>js/jquery.js" type="text/javascript"></script>
<link href="<?php echo SITEURLM?>css/style.css"  rel="stylesheet" type="text/css" />
<script src="<?php echo SITEURLM?>js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITEURLM?>js/jquery.form.js"></script>
<script src="<?php echo SITEURLM?>js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<LINK href="<?php echo SITEURLM?>css/jquery-ui-1.8.10.custom.css" type=text/css rel=stylesheet>
<script src="<?php echo SITEURLM?>js/calebderical.js" type="text/javascript"></script>
<script src="<?php echo SITEURLM.$view_path; ?>js/schedulecomments.js" type="text/javascript"></script>
<script type="text/javascript">
var base_url = "<?php echo SITEURLM;?>";
var redirect_url = "<?php echo REDIRECTURL;?>";
</script>
</head>

<body>

<div class="wrapper">

	<?php require_once($view_path.'inc/header.php'); ?>
    <div class="mbody">
    	<?php require_once($view_path.'inc/observermenu.php'); ?>
        <div class="content">
		<table align="center" cellpadding="5">
		<tr>
		
		<td>
		
		<?php if(!empty($school)) { 
		foreach($school as $val)
		{
		?>
		<input type="hidden" name="school" id="school" value="<?php echo $val['school_id'];?>">
		<?php } }  else {?>
		<input type="hidden" name="school" id="school" value="0">
		
		<?php } ?>
		
		</td>
		<?php /*<td >Select Teacher:</td>
		<td>
		<select class="combobox1" name="teacher" id="teacher"  >
		<?php if($teacher!=false) { ?>
		<option value="all">All</option>
		<?php foreach($teacher as $val)
		{
		?>
		<option value="<?php echo $val['teacher_id'];?>"  ><?php echo $val['firstname'].' '.$val['lastname'];?></option>
		<?php } } else { ?>
		<option value="0">No Teachers Found</option>
		<?php } ?>
		</select>
		</td>*/?>
		
		</tr>
		
		</table>
		<div id="commentsdetails" style="display:none;">
		<input type="hidden" id="pageid" value="">
		<div id="msgContainer">
			</div>
		</div>
        </div>
    </div>
    <?php require_once($view_path.'inc/footer.php'); ?>

</div>
<div id="dialog" title="Task" style="display:none;"> 

<form name='schedule' id='schedule' method='post' onsubmit="return false">
<table cellpadding="0" cellspacing="5" border=0 class="jqform">
<tr><td class='style1'></td><td colspan="6">
<span style="color: Red;display:none" id="message"></span>
				</td>
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date1' name='date1' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start1' name='start1' readonly >
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end1' name='end1' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Teacher:
				</td>
				<td valign="top" >
				<select name="teacher1" id="teacher1">
				<option value="">-Select-</option>
				</select>
				
				</td>
				<td valign="top" class="style1">
				<font color="red">*</font>Task1:
				</td>
				<td valign="top" >
				<select name="task1" id="task1">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments1' name='comments1'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date2' name='date2' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start2' name='start2' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end2' name='end2' readonly >
				
				</td>
				
			    <td valign="top" class="style1">
				<font color="red">*</font>Teacher:
				</td>
				<td valign="top" >
				<select name="teacher2" id="teacher2">
				<option value="">-Select-</option>
				</select>
				
				</td>
				<td valign="top" class="style1">
				<font color="red">*</font>Task2:
				</td>
				<td valign="top" >
				<select name="task2" id="task2">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments2' name='comments2'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date3' name='date3' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start3' name='start3' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end3' name='end3' readonly >
				
				</td>
				
			<td valign="top" class="style1">
				<font color="red">*</font>Teacher:
				</td>
				<td valign="top" >
				<select name="teacher3" id="teacher3">
				<option value="">-Select-</option>
				</select>
				
				</td>
				<td valign="top" class="style1">
				<font color="red">*</font>Task3:
				</td>
				<td valign="top" >
				<select name="task3" id="task3">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments3' name='comments3'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date4' name='date4' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start4' name='start4' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end4' name='end4' readonly >
				
				</td>
				<td valign="top" class="style1">
				<font color="red">*</font>Teacher:
				</td>
				<td valign="top" >
				<select name="teacher4" id="teacher4">
				<option value="">-Select-</option>
				</select>
				
				</td>
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task4:
				</td>
				<td valign="top" >
				<select name="task4" id="task4">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments4' name='comments4'  ></textarea>
				
				</td>
				
			</tr>
			<tr>
				<td valign="top" class="style1">
				<font color="red">*</font>Date:
				</td>
				<td valign="top" >
				<input class="txtbox1" type='text' readonly  id='date5' name='date5' value=''>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Start Time:
				</td>
				<td valign="top">
				<input class='txtbox1'  type='text'  id='start5' name='start5' readonly >
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>End Time:
				</td>
				<td valign="top" >
				<input class="txtbox1"  type='text'  id='end5' name='end5' readonly >
				
				</td>
				<td valign="top" class="style1">
				<font color="red">*</font>Teacher:
				</td>
				<td valign="top" >
				<select name="teacher5" id="teacher5">
				<option value="">-Select-</option>
				</select>
				
				</td>
			
				<td valign="top" class="style1">
				<font color="red">*</font>Task5:
				</td>
				<td valign="top" >
				<select name="task5" id="task5">
				<option value="">-Select-</option>
				</select>
				
				</td>
				
			
				<td valign="top" class="style1">
				<font color="red">*</font>Details:
				</td>
				<td valign="top" >
				<textarea   style="width:150px" type='text'  id='comments5' name='comments5'  ></textarea>
				
				</td>
				
			</tr>
		
			
			
			
						
<tr><td valign="top"></td><td valign="top" colspan="10"><input class="btnbig" type='submit' name="submit" id='teacheradd' value='Add' title="Add New" style="float:left;margin-right:10px;margin-left:230px;"><input class="btnbig" type='button' name='cancel' id='cancel' value='Cancel' title="Cancel" style="float:left;"></td></tr></table></form>
</div>
</body>
</html>
