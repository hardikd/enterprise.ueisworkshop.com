<?php 
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
/**
 * Studentdetail Controller.
 *
 */

class Schooldistrictgraph extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false ){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	
	{ 
		$data['idname']='tools';	
	 error_reporting(0);
	 	 
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('schooldistrictgraphmodel');
			    $district_id = $this->session->userdata('district_id');
				$data['records'] = $this->schooldistrictgraphmodel->getallschools($district_id);
	 			$this->load->view('schooldistrictgraph/index',$data);
	 	 }
		
		if($this->session->userdata("login_type")=='observer')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('schooldistrictgraphmodel');
			    $observer_id = $this->session->userdata('observer_id');
				$data['records'] = $this->schooldistrictgraphmodel->getobservergrade($observer_id);
				$this->load->view('schooldistrictgraph/index',$data);
	   
	 	 }
	}
	
	function getassessmentHtml()
	{
		error_reporting(0);
		 $school_id = $_REQUEST['school_id'];
 	 	 $district_id = $this->session->userdata('district_id'); 
		 $data = array();
		 $this->load->model('schooldistrictgraphmodel');
		 $data= $this->schooldistrictgraphmodel->getassessment($school_id,$district_id);
		  $strClass ='';
		 $strClass .= '<div class="control-group">  <label class="control-label">Select Assessment</label>
	   <div class="controls"><select class="span12 chzn-select" name="assessment" id="assessment" onchange="updateassessment(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>';
		
		if(count($data[0]['id'])>0)
		{
	 	   $strClass .= '<option value="0" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
				 $strClass .='<option value="'.$value['id'].'">'.$value['assignment_name'].'</option>';
		 	}
         $strClass .= '</select>   </div>
                                         </div> ';
 		 echo $strClass;
	}
	
	function getassessmentHtmlObserver()
	{
		error_reporting(0);
		 $grade_id = $_REQUEST['grade_id']; 
 	 	 $data = array();
 	    $observerid = $this->session->userdata('observer_id'); 
	   $data = array();
		 $this->load->model('schooldistrictgraphmodel');
 		$data['school'] = $this->schooldistrictgraphmodel->getobserverschool($observerid);
		 $school_id = $data['school'][0]['school_id']; 
	
		 $data= $this->schooldistrictgraphmodel->getassessmentofObserver($school_id,$grade_id);
		  $strClass ='';
		 $strClass .= '<div class="control-group"><label class="control-label">Select Assessment</label>
		  <div class="controls"><select class="span12 chzn-select" style="width: 300px;" name="assessment" id="assessment" onchange="updateassessmentOb(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>' ;
		
		
		if(count($data[0]['id'])>0)
		{
	 	   $strClass .= '<option value="0" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
				 $strClass .='<option value="'.$value['id'].'">'.$value['assignment_name'].'</option>';
		 	}
         $strClass .= '</select> </div></div>';
 		 echo $strClass;
	}

	
	function getReportHtml()
	{

		$school_id= $_REQUEST['school_id'];
		$district_id = $this->session->userdata('district_id');
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		
		$this->load->model('schooldistrictgraphmodel');
		$data= $this->schooldistrictgraphmodel->getTestItem($school_id,$district_id,$fDate,$tDate,$ass_id);
		
		
		$IntRow = count($data);
		$correctClounmstring  = "[";
		$incorrectClounmstring = "[";
		$strClusternams = "[";
		for($i=0;$i<$IntRow;$i++)
		{
			$strCluster = $data[$i]['test_cluster'];
			if($strCluster==''){$strCluster='No cluster';}
			$intCorrectPercentage = $data[$i]['per'];
			if($intCorrectPercentage==''){$intCorrectPercentage=0;}
			$intIncorrectPercentage = 100 - $intCorrectPercentage;
			//$correctClounmstring .="{  y: ".$intCorrectPercentage.", label: '".$strCluster."'},";
			//$incorrectClounmstring .="{  y: ".$intIncorrectPercentage.", label: '".$strCluster."'},";
			
			$correctClounmstring .= $intCorrectPercentage.",";
			$incorrectClounmstring .= $intIncorrectPercentage.",";
			$strClusternams .= "'".$strCluster."'".",";
		}
		$correctClounmstring = substr($correctClounmstring, 0, -1);
		$incorrectClounmstring = substr($incorrectClounmstring, 0, -1);
		$strClusternams = substr($strClusternams, 0, -1);
		
		$correctClounmstring .="]";
		$incorrectClounmstring .="]";
		$strClusternams .="]";
		$result = $correctClounmstring."*".$incorrectClounmstring."*".$strClusternams;
	echo $result;
	}

	function getgradeReportHtml()
	{
		$observerid= $_REQUEST['observerid'];
		$gradeid= $_REQUEST['grade_id'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		
		if($gradeid != -1)
		{
			$this->load->model('schooldistrictgraphmodel');
			$data['school'] = $this->schooldistrictgraphmodel->getobserverschool($observerid);
			$school_id = $data['school'][0]['school_id'];
			$district_id = $data['school'][0]['district_id'];
			$data= $this->schooldistrictgraphmodel->getgradeTestItem($school_id,$gradeid,$district_id,$fDate,$tDate,$ass_id);
			$IntRow = count($data);
			$correctClounmstring  = "[";
			$incorrectClounmstring = "[";
			$strClusternams = "[";
			
			for($i=0;$i<$IntRow;$i++)
			{
				$strCluster = $data[$i]['test_cluster'];
				if($strCluster==''){$strCluster='None Estrellita Skills';}
				$intCorrectPercentage = $data[$i]['per'];
				if($intCorrectPercentage==''){$intCorrectPercentage=0;}
				$intIncorrectPercentage = 100 - $intCorrectPercentage;
				$correctClounmstring .= $intCorrectPercentage.",";
				$incorrectClounmstring .= $intIncorrectPercentage.",";
				$strClusternams .= "'".$strCluster."'".",";
			}
			$correctClounmstring = substr($correctClounmstring, 0, -1);
			$incorrectClounmstring = substr($incorrectClounmstring, 0, -1);
			$strClusternams = substr($strClusternams, 0, -1);
			
			$correctClounmstring .="]";
			$incorrectClounmstring .="]";
			$strClusternams .="]";
			$result = $correctClounmstring."*".$incorrectClounmstring."*".$strClusternams;
	
		}
		
		echo $result;
	}
	
	
	


}?>