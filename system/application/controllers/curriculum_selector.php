<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Curriculum_selector extends MY_Auth {

    function __Construct() {
        parent::Controller();
        if($this->session->userdata('login_type')=='' && $this->session->userdata('product_login_type')==''){
            $this->session->sess_destroy();
            redirect(base_url());
            exit;
        }
    }

    function index() {
//        print_r($_POST);exit;
        $this->load->Model('curriculummodel');
      //  print_r($this->session->all_userdata());exit;
        $curriculum = $this->curriculummodel->checkcurriculum();
//        print_r($curriculum);exit;
        if ($this->input->post('submit') == 'submit') {

            if ($curriculum && $this->input->post('curriculum')!='Select Curriculum Type') {
                $this->curriculummodel->updatecurriculum();
            } else if($this->input->post('curriculum')!='Select Curriculum Type'){
                $this->curriculummodel->savecurriculum();
            }
            redirect(base_url().'curriculum_selector');
        }
        $this->load->Model('standardmodel');
            $data['standards'] = $this->standardmodel->standardByDistrict();
            //echo $this->db->last_query();
        
        
        $data['curriculum'] = $curriculum;

        $data['idname'] = 'tools';
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('curriculum_selector/index', $data);
    }

}
