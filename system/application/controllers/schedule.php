<?php

class Schedule extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_teacher()==false && $this->is_observer()==false){
			//These functions are available only to teachers - So redirect to the login page
			redirect("index");
		}
		
	}

	
	function add_plan()
	{
	
	  
		
		$this->load->Model('schedulemodel');
		$pstatus=$this->schedulemodel->check_plan_exists();
		
		//$pstatus=1;
	if($pstatus==1)
		 {
		$status=$this->schedulemodel->add_plan();
		if($status!=0){
		       $data['message']="plan added Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date1') ;
	            // sending email start
				for($i=1;$i<=5;$i++)
		{
		if($this->input->post("date$i") && $this->input->post("start$i") && $this->input->post("end$i") && $this->input->post("task$i") && $this->input->post("comments$i")   ) 
		{
		$teacher_id=$this->session->userdata("teacher_observer_id");
		$task=$this->input->post("task$i");
		$this->load->Model('teachermodel');
		$this->load->Model('scheduletaskmodel');
		$teacherdata=$this->teachermodel->getteacherById($teacher_id);
		$taskdata=$this->scheduletaskmodel->getplanById($task);
		
		
	   $dates=$this->input->post("date$i");
	  $date1=explode('-',$dates);
	  $date=$date1[0].'/'.$date1[1].'/'.$date1[2];
		$start=$this->input->post("start$i");
	  
	   $end=$this->input->post("end$i");
	  
	  $comments=$this->input->post("comments$i");
      if($this->valid_email($teacherdata[0]['email']))
			{
			 $email=$teacherdata[0]['email'];
			$teachername=$teacherdata[0]['firstname'];
			$taskd=$taskdata[0]['task'];
			$observer_name=$this->session->userdata('observer_name');
			$observer_email=$this->session->userdata('observer_email');
			
	   /*  sending mail to user */
$subject ="Tor-Events For $date ";
	$message = "<html>
					<head>
					  <title>Tor-Events For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $teachername,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  <tr>
					   <td width='30px'><b>Assigned By:</b>
					   </td>
					   <td>
					   $observer_name </td>
					   </tr>
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>
					   <tr><td width='30px'><b>Start Time:</b></td>
					   <td>$start</td></tr>
					   <tr><td width='30px'><b>End Time:</b></td>
					   <td>$end</td></tr>
					   <tr><td width='30px'><b>Comments:</b></td>
					   <td>$comments</td></tr>					  
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>					  
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  TOR 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";

	//Additional headers
	/*if($observer_email!='')
	{
	  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	  $headers .= "Reply-To: TOR Observer <$observer_email>".PHP_EOL;
	}
	else
	{*/
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	/*  End of Sennding Email To user*/
	  
		
		
		}
		}
				
				
				// sending email stop
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		
		foreach($pstatus as $key=>$error)
		{
		if($error==0)
		{
			$data["message"]="Scheduling Conflict on Task$key" ;
		    $data['status']=0 ;
		}
		else if($error==2)
		{
			$data["message"]="End Time Can not be Greater/Equal Than Start Time on Task$key" ;
		    $data['status']=0 ;
			
		
		}
		else if($error==3)
		{
			$data["message"]="Scheduleing Past Dates Not Available on Task$key" ;
		    $data['status']=0 ;
			
		
		}
		else if($error==4)
		{
			$data["message"]="Already Scheduled Complete Periodic Goal Planner on  Task$key (For Every Teacher Once a Year)" ;
		    $data['status']=0 ;
			
		
		}
		else if($error==5)
		{
			$data["message"]="Please Schedule  Complete Periodic Goal Planner  For Current Year Only on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		else if($error==6)
		{
			$data["message"]=" Schedule  Complete Periodic Goal Planner Twice  For Same Teacher is not possible on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		else if($error==7)
		{
			$data["message"]=" Please Archive Observation Plan Before Schedule  Complete Observation Plan For This Teacher on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		else if($error==8)
		{
			$data["message"]=" Schedule  Complete Observation Plan Twice  For Same Teacher is not possible on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		}
		
		}
		
		$this->schedulemodel->delete_temp_plan();
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function add_plan_all()
	{
	
	  
		
		$this->load->Model('schedulemodel');
		$pstatus=$this->schedulemodel->check_plan_exists_all();
		
		//$pstatus=1;
	if($pstatus==1)
		 {
		 
		$status=$this->schedulemodel->add_plan_all();
		if($status!=0){
		       $data['message']="plan added Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date1') ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
				// sending email start
				for($i=1;$i<=5;$i++)
		{
		if($this->input->post("date$i") && $this->input->post("start$i") && $this->input->post("end$i") && $this->input->post("task$i") && $this->input->post("comments$i") && $this->input->post("teacher$i")  ) 
		{
		$teacher_id=$this->input->post("teacher$i");
		$task=$this->input->post("task$i");
		$this->load->Model('teachermodel');
		$this->load->Model('scheduletaskmodel');
		$teacherdata=$this->teachermodel->getteacherById($teacher_id);
		$taskdata=$this->scheduletaskmodel->getplanById($task);
		
		
	   $dates=$this->input->post("date$i");
	  $date1=explode('-',$dates);
	  $date=$date1[0].'/'.$date1[1].'/'.$date1[2];
		$start=$this->input->post("start$i");
	  
	   $end=$this->input->post("end$i");
	  
	  $comments=$this->input->post("comments$i");
      if($this->valid_email($teacherdata[0]['email']))
			{
			 $email=$teacherdata[0]['email'];
			$teachername=$teacherdata[0]['firstname'];
			$taskd=$taskdata[0]['task'];
			$observer_name=$this->session->userdata('observer_name');
			$observer_email=$this->session->userdata('observer_email');
			
	   /*  sending mail to user */
$subject ="Tor-Events For $date ";
	$message = "<html>
					<head>
					  <title>Tor-Events For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $teachername,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  <tr>
					   <td width='30px'><b>Assigned By:</b>
					   </td>
					   <td>
					   $observer_name </td>
					   </tr>
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>
					   <tr><td width='30px'><b>Start Time:</b></td>
					   <td>$start</td></tr>
					   <tr><td width='30px'><b>End Time:</b></td>
					   <td>$end</td></tr>
					   <tr><td width='30px'><b>Comments:</b></td>
					   <td>$comments</td></tr>
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>					  
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  TOR 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
	//Additional headers
	/*if($observer_email!='')
	{
	  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	  $headers .= "Reply-To: TOR Observer <$observer_email>".PHP_EOL;
	}
	else
	{*/
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	/*  End of Sennding Email To user*/
	  
		
		
		}
		}
				
				
				// sending email stop
		}
		else
		{
		
		foreach($pstatus as $key=>$error)
		{
		if($error==0)
		{
			$data["message"]="Scheduling Conflict on Task$key" ;
		    $data['status']=0 ;
		}
		else if($error==2)
		{
			$data["message"]="End Time Can not be Greater/Equal Than Start Time on Task$key" ;
		    $data['status']=0 ;
			
		
		}
		else if($error==3)
		{
			$data["message"]="Scheduleing Past Dates Not Available on Task$key" ;
		    $data['status']=0 ;
			
		
		}
		else if($error==4)
		{
			$data["message"]="Already Scheduled Complete Periodic Goal Planner on  Task$key (For Every Teacher Once a Year)" ;
		    $data['status']=0 ;
			
		
		}
		else if($error==5)
		{
			$data["message"]="Please Schedule  Complete Periodic Goal Planner  For Current Year Only on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		else if($error==6)
		{
			$data["message"]=" Schedule  Complete Periodic Goal Planner Twice  For Same Teacher is not possible on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		else if($error==7)
		{
			$data["message"]=" Please Archive Observation Plan Before Schedule  Complete Observation Plan For This Teacher on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		else if($error==8)
		{
			$data["message"]=" Schedule  Complete Observation Plan Twice  For Same Teacher is not possible on  Task$key " ;
		    $data['status']=0 ;
			
		
		}
		}
		
		}
		
		$this->schedulemodel->delete_temp_plan();
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	
	function update_plan()
	{
	    $task=$this->input->post('task');
		if($task==1)
		{
		  $data["message"]=" Update To Lesson Plan Book is Restricted  " ;
		    $data['status']=0 ;
		 echo json_encode($data);
		exit;	
		
		
		}
	    
		$this->load->Model('schedulemodel');
		$pstatus=$this->schedulemodel->check_plan_update_exists();
		//$pstatus=1;
	if($pstatus==1)
		 {
		$status=$this->schedulemodel->update_plan();
		if($status!=0){
		       $data['message']="plan update Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date') ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Scheduling Conflict" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==3)
		{
			$data['message']="Scheduleing Past Dates Not Available" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==4)
		{
			$data['message']="Already Scheduled Complete Periodic Goal Planner (For Every Teacher Once a Year)" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==5)
		{
			$data["message"]="Please Schedule  Complete Periodic Goal Planner  For Current Year Only " ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==6)
		{
			$data["message"]="Please Archive  Complete Observation Plan  For This Teacher  " ;
		    $data['status']=0 ;
			
		
		}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function deleteplan($schedule_week_plan_id)
	{
	  $this->load->Model('schedulemodel');
		$result = $this->schedulemodel->deleteplan($schedule_week_plan_id);
		if($result!=false){
			$data['status']=1;
			//print_r($result);
			//exit;
			$data['date']=$result[0]['date'];
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
	
	
	
	}
	function getallscheduleplans($date)
	{
	  $this->load->Model('schedulemodel');
	  $datevar=explode('-',$date);
		 $sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	  $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	 // print_r($data['dates']);
	 // exit;
	  $getscheduleplans=$this->schedulemodel->getallscheduleplans($data['dates'][0]['date'],$data['dates'][6]['date']);
	   $siteurlm=SITEURLM;
	   
	  $data['html']='<table id="datetable" align="center" style="width:650px;margin-left:10px;border:1px #999 solid; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
    <tr>
  <td colspan="5" align="left" bgcolor="#657455" style="color:#FFF" >
  <b><img style="float:left;margin:3px;" src="'.$siteurlm.'/images/task.jpg" height="20">&nbsp;&nbsp;&nbsp;&nbsp;Scheduler</b>&nbsp;&nbsp;&nbsp;&nbsp; <b>From:'.$data['fromdate'].' To:'.$data['todate'].'</b>
  </td>
  </tr>
  <tr align="center" style="color:#333;">
  <td width="120px" bgcolor="#CCCCCC">Date</td>
  <td width="120px" bgcolor="#CCCCCC">Time</td>
  <td width="120px" bgcolor="#CCCCCC">Task</td>
  <td width="150px" bgcolor="#CCCCCC">Actions</td>
  <td width="120px" bgcolor="#CCCCCC">Details</td>
  </tr>';
	 if($getscheduleplans!=false)
	{
	
	  foreach($getscheduleplans as $getplan)
	  {
	  $le=$getplan['schedule_week_plan_id'];
		$lecomments=$getplan['comments'];
	  
		$data['html'].="<tr align='center'>
	     <td width='120px'>".$getplan['date']."</td>
		 <td width='120px'>";
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		$data['html'].="".$start2." to ".$end2."</td><td width='120px'>".$getplan['task']."</td>";
	 if($getplan['date']>=date('m-d-Y'))
		 {
		 
		  $data['html'].="<td width='150px'><input style='float:left;' type='button' name='Edit' id='Edit' value='Edit' onclick='edit($le)' ><input  type='button' name='Delete' id='Delete' value='Delete' onclick='deleteweek($le)'></td><td width='120px'><a class='title' href='#' title='Details|" ;if($lecomments!=''){ $data['html'].="".$lecomments; } else { $data['html'].="No Details Found. "; }$data['html'].="'>View</a></td></tr><tr align='center'>";
		 
		 
		 }
		 else
		 {
		 $data['html'].="<td width='150px'>&nbsp;</td><td width='120px'><a class='title' href='#' title='Details|" ;if($lecomments!=''){ $data['html'].="".$lecomments; } else { $data['html'].="No Details Found. "; }$data['html'].="'>View</a></td></tr><tr align='center'>";
		 
		  }
	      $data['html'].="</tr>";  $le=$getplan['schedule_week_plan_id'];
		$lecomments=$getplan['comments'];  } }  else {
	$data['html'].="<tr>
	<td colspan='5' align='center'>
	No Task Found
	</td>
	</tr>";
	
	 } 
	  
		$data['html'].="</table>";	
	  
	  
	  
	  echo json_encode($data);
	  exit;
	
	}
	
	function getscheduleplaninfo($schedule_week_plan_id)
	{
	  $this->load->Model('schedulemodel');
	  $pdata['schedule_week']=$this->schedulemodel->getscheduleplaninfo($schedule_week_plan_id);
	  echo json_encode($pdata);
	  exit;
	
	}
	function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}
function getallscheduletasks()
	{
		$this->load->Model('scheduletaskmodel');
		$data['scheduletask']=$this->scheduletaskmodel->getallscheduletasks();
		echo json_encode($data);
		exit;
	
	
	}	
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
}

?>	