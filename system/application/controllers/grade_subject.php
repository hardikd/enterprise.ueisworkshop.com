<?php
/**
 * grade_subject Controller.
 *
 */
class Grade_subject extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
		 $data['idname']='attendance';
	   if($this->session->userdata("login_type")=='user')
	  {
		  if($this->session->userdata('login_special') == !'district_management'){
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
		  }else{
		$this->load->Model('schoolmodel');
		$data['grades']=$this->schoolmodel->getallgrades();
		$this->load->Model('grade_subjectmodel');
		
		$data['subject'][] = $this->grade_subjectmodel->getgrade_subjects(false, false,'all');
//                print_r($data['subject']);exit;
		
		$this->load->Model('dist_subjectmodel');
		$data['allsubject']=$this->dist_subjectmodel->getdist_subjectsById();
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('grade_subject/all',$data);	
		  }
	  
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
		$this->load->Model('schoolmodel');
		$grades = $data['grades']=$this->schoolmodel->getallgrades();
		$this->load->Model('grade_subjectmodel');
		foreach($grades as $grade){
			$data['subject'][] = $this->grade_subjectmodel->getgrade_subjects(false, false,$grade['grade_id']);
		}
		

		$this->load->Model('dist_subjectmodel');
		$data['allsubject']=$this->dist_subjectmodel->getdist_subjectsById();
		//print_r($data['allsubject']);exit;

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('grade_subject/all',$data);	
	  
	  
	  }
	  
	
	}
	
	function getgrade_subjects($page,$grade_id=false)
	{
		if($grade_id==false)
		{
		  $grade_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('grade_subjectmodel');
		$total_records = $this->grade_subjectmodel->getgrade_subjectCount($grade_id);
		
			$status = $this->grade_subjectmodel->getgrade_subjects($page, $per_page,$grade_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<table class='table table-striped table-bordered' id='editable-sample' >
		<thead>
        <tr>
        	<td class='no-sorting'>Grade</td>
			<td class='no-sorting'>Subject Name</td>
			<td class='no-sorting'>Actions</td>
			</tr>
			</thead>";
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tbody><tr id="'.$val['grade_subject_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['grade_name'].'</td>
				<td class="hidden-phone">'.$val['subject_name'].'</td>
				
				<td class="hidden-phone" nowrap>
						<button data-dismiss="modal" title="Edit" type="button"  value="Edit" name="Edit" onclick="grade_subjectedit('.$val['grade_subject_id'].')" class="btn btn-primary"><i class="icon-pencil"></i></button>

		<button title="Delete" type="button"  name="Delete" value="Delete" onclick="grade_subjectdelete('.$val['grade_subject_id'].')"  class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-trash"></i></button>
                                    
		
								      </td>	
		
			</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'grade_subjects');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' align='center'><td >Grade</td><td>Subject Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Grade Subjects Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'grade_subjects');
						print $pagination;	
		}
		
		
	}
	
	function getgrade_subjectinfo($grade_subject_id)
	{
		if(!empty($grade_subject_id))
	  {
		$this->load->Model('grade_subjectmodel');
		
		$data['grade_subject']=$this->grade_subjectmodel->getgrade_subjectById($grade_subject_id);
		$data['grade_subject']=$data['grade_subject'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	function getgrade_subjectall($grade_id)
	{		
		$this->load->Model('grade_subjectmodel');
		
		$data['grade_subject']=$this->grade_subjectmodel->getgrade_subjectall($grade_id);
		
		echo json_encode($data);
		exit;
	  
	
	}
	
	function add_grade_subject()
	{
	
	
		$this->load->Model('grade_subjectmodel');
		$pstatus=$this->grade_subjectmodel->check_grade_subject_exists();
	if($pstatus==1)
		 {
		$status=$this->grade_subjectmodel->add_grade_subject();
		if($status!=0){
		       $data['message']="Grade Subject Added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Grade With Same Subject  Already Exists" ;
		    $data['status']=0 ;
		}
		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_grade_subject()
	{
		$this->load->Model('grade_subjectmodel');
	    $pstatus=$this->grade_subjectmodel->check_grade_subject_update();
		if($pstatus==1)
		 {

		$status=$this->grade_subjectmodel->update_grade_subject();
		if($status==true){
		       $data['message']="Grade Subject Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Grade With Same Subject  Already Exists" ;
		    $data['status']=0 ;
		}
			
		echo json_encode($data);
		exit;		
	}
	
	function delete($grade_subject_id)
	{
		
		$this->load->Model('grade_subjectmodel');
		$result = $this->grade_subjectmodel->deletegrade_subject($grade_subject_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
	
}	