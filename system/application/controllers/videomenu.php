<?php
/**
 * (Admin Or Home Or Startup) page Controller.
 *
 */
class Videomenu extends	Controller {

	function __construct()
	{
		parent::Controller();
	}

	function index($video)
	{
		 $data['idname']='tools';
		 $this->session->set_userdata('breadcrumb','');
		$data['video']=$video;
		$data['view_path']=$this->config->item('view_path');
		$siteurlm=SITEURLM ;
		if($this->session->userdata('login_type')=='user') 
		{ 
		$path=$siteurlm.$data['view_path'].'inc/districtmedia/';
		
		}
		if($this->session->userdata('login_type')=='observer') 
		{ 
		 $path=$siteurlm.$data['view_path'].'inc/observermedia/';
		}
		if($this->session->userdata('login_type')=='teacher') 
		{ 
		  $path=$siteurlm.$data['view_path'].'inc/teachermedia/';
		}
		$data['path']=$path;
		$this->load->view('video/index',$data);
	}
function lesson_palnner($video)
	{
		 $data['idname']='tools';
		 $this->session->set_userdata('breadcrumb','');
		$data['video']=$video;
		$data['view_path']=$this->config->item('view_path');
		$siteurlm=SITEURLM ;
		if($this->session->userdata('login_type')=='user') 
		{ 
		$path=$siteurlm.$data['view_path'].'inc/districtmedia/';
		
		}
		if($this->session->userdata('login_type')=='observer') 
		{ 
		 $path=$siteurlm.$data['view_path'].'inc/observermedia/';
		}
		if($this->session->userdata('login_type')=='teacher') 
		{ 
		  $path=$siteurlm.$data['view_path'].'inc/teachermedia/';
		}
		$data['path']=$path;
		$this->load->view('video/lesson_planner_tutorial',$data);
	}
function goal_planner_tutorial($video)
	{
		 $data['idname']='tools';
		 $this->session->set_userdata('breadcrumb','');
		$data['video']=$video;
		$data['view_path']=$this->config->item('view_path');
		$siteurlm=SITEURLM ;
		if($this->session->userdata('login_type')=='user') 
		{ 
		$path=$siteurlm.$data['view_path'].'inc/districtmedia/';
		
		}
		if($this->session->userdata('login_type')=='observer') 
		{ 
		 $path=$siteurlm.$data['view_path'].'inc/observermedia/';
		}
		if($this->session->userdata('login_type')=='teacher') 
		{ 
		  $path=$siteurlm.$data['view_path'].'inc/teachermedia/';
		}
		$data['path']=$path;
		$this->load->view('video/goal_planner_tutorial',$data);
	}		
function development_tutorial($video)
	{
		 $data['idname']='tools';
		 $this->session->set_userdata('breadcrumb','');
		$data['video']=$video;
		$data['view_path']=$this->config->item('view_path');
		$siteurlm=SITEURLM ;
		if($this->session->userdata('login_type')=='user') 
		{ 
		$path=$siteurlm.$data['view_path'].'inc/districtmedia/';
		
		}
		if($this->session->userdata('login_type')=='observer') 
		{ 
		 $path=$siteurlm.$data['view_path'].'inc/observermedia/';
		}
		if($this->session->userdata('login_type')=='teacher') 
		{ 
		  $path=$siteurlm.$data['view_path'].'inc/teachermedia/';
		}
		$data['path']=$path;
		$this->load->view('video/development_tutorial',$data);
	}
	
}
