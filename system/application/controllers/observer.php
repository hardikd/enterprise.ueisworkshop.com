<?php
/**
 * observer Controller.
 *
 */
class Observer extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observer/index',$data);
	  }
	  else if($this->session->userdata("login_type")=='user')
	  {
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('observer/all',$data);
	  
	  }
	
	}
	function getobservers($page,$state_id,$country_id,$district_id=false)
	{
		if($district_id==false)
		{
		  $district_id='all';
		}
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('observermodel');
		$total_records = $this->observermodel->getobserverCount($state_id,$country_id,$district_id);
		
			$status = $this->observermodel->getobservers($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Observers</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >Observer Name</td><td >School Name</td><td>District</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['observer_id'].'" class="'.$c.'" >';
			
				print '<td >'.$val['observer_name'].'</td>
					   <td >'.$val['school_name'].'</td>
					   <td >'.$val['districts_name'].'</td>
				
				<td  nowrap><input class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="observeredit('.$val['observer_id'].')"><input  class="btnsmall" title="Delete" type="button" name="Delete" value="Delete" onclick="observerdelete('.$val['observer_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'observers');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td >Observer Name</td><td >School Name</td><td>District</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No observers Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'observers');
						print $pagination;	
		}
		
		
	}
	
	function getobserverinfo($observer_id)
	{
		if(!empty($observer_id))
	  {
		$this->load->Model('observermodel');
		
		$data['observer']=$this->observermodel->getobserverById($observer_id);
		$data['observer']=$data['observer'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_observer()
	{
	
	
		$this->load->Model('observermodel');
	
		if($this->observermodel->check_observer_exists()==true)
		 {
		$status=$this->observermodel->add_observer();
		if($status!=0){
		       $data['message']="observer added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		  $data['message']="Observer With Same User Name   Already Exists" ;
		  $data['status']=0 ;

		}		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_observer()
	{
	$this->load->Model('observermodel');
	    if($this->observermodel->check_observer_update()==true)
		 {
		$status=$this->observermodel->update_observer();
		if($status==true){
		       $data['message']="observer Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		  $data['message']="Observer With Same User Name   Already Exists" ;
		  $data['status']=0 ;

		}			
		echo json_encode($data);
		exit;		
	}
	
	function delete($observer_id)
	{
		
		$this->load->Model('observermodel');
		$result = $this->observermodel->deleteobserver($observer_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getobserverByschoolId($school_id)
	{
	  $this->load->Model('observermodel');
		$data['observer']= $this->observermodel->getobserverByschoolId($school_id);
	   echo json_encode($data);
		exit;
	}
	
	function profileimage($msg=false)
	{
	  
	  if($msg==false)
	  {
	     $data['message']='';
	  
	  
	  }
	  else
	  {
	  
	    $data['message']=$msg;
	  
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observer/profileimage',$data);
	
	
	
	
	}
	
	function change()
	{
	 if($this->session->userdata('login_type')=='user')
	  {
	     $this->load->Model('observermodel');
		$status= $this->observermodel->changeDistImage();
	  
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
         $this->load->Model('observermodel');
		$status= $this->observermodel->changeObserverImage();
	  
	  }
	  if($status==true)
	  {
        $msg='Changed Sucessfully';
	    $this->session->set_userdata('avatar_id',$this->input->post('avatar'));
	  }
	  else
	  {
	    $msg='Failed Please Try Again';
	  
	  }
	   $this->profileimage($msg);
	
	
	}
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
					*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	