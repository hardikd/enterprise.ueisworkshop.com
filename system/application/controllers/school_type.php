<?php
/**
 * School_type Controller.
 *
 */
class School_type extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
		$data['idname']='attendance';
		
		if($this->session->userdata("login_type")=='user')
	  {
		  if($this->session->userdata('login_special') == !'district_management'){
		 $this->session->set_flashdata ('permission','Additional Permissions Required');
         redirect(base_url().'attendance/assessment');
		  }
	  }
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
          
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('school_type/index',$data);
	  
	  }else if($this->session->userdata("login_type")=='user' || $this->session->userdata("login_type")=='observer')
	  {
              $this->load->Model('school_typemodel');
              $country_id = $this->session->userdata('dis_country_id');
              $district_id = $this->session->userdata('district_id');
              $state_id = $this->session->userdata('dis_state_id');
		
			$data['status'] = $this->school_typemodel->getschool_types(false, false,$state_id,$country_id,$district_id);
                        
	     $data['view_path']=$this->config->item('view_path');
	     $this->load->view('school_type/all',$data);
	  }
	}
	
	
	function getschool_types($page,$state_id,$country_id,$district_id=false)
	{
	
	    if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('school_typemodel');
		$total_records = $this->school_typemodel->getschool_typescount($state_id,$country_id,$district_id);
		
			$status = $this->school_typemodel->getschool_types($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print"<table class='table table-striped table-bordered' id='editable-sample' >
			<thead>
		<tr>
		<td class='no-sorting'>Tabs</td>
		<td class='no-sorting'>State</td>
		<td class='no-sorting'>District Name</td>
		<td class='no-sorting'>Actions</td>
		</tr></thead>";
		
		$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tbody><tr id="'.$val['school_type_id'].'" class="'.$c.'" >';
			
				print '<td class="hidden-phone">'.$val['tab'].'</td>
				<td class="center hidden-phone">'.$val['name'].'</td>
				<td class="hidden-phone">'.$val['districtsname'].'</td>
				
				<td class="hidden-phone" nowrap>
				<button title="Edit" type="button" value="Edit" name="Edit" onclick="planedit('.$val['school_type_id'].')" class="btn btn-primary"><i class="icon-pencil"></i></button>
				 <button title="Delete" type="button" name="Delete" value="Delete" onclick="plandelete('.$val['school_type_id'].')" class="btn btn-danger"><i class="icon-trash"></i></button>
				
				
				</td></tr></tbody>';
			
				 
				 
					$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'school_type');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='table table-striped table-bordered' id='editable-sample' >
			<thead>
		<tr>
		<td class='no-sorting'>Tabs</td>
		<td class='no-sorting'>State</td>
		<td class='no-sorting'>District Name</td>
		<td class='no-sorting'>Actions</td>
		</tr>
			<tr><td valign='top' colspan='10'>No School Type Records Found.</td></tr></thead></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'school_type');
						print $pagination;	
		}
		
	
	
	
	
	}
	function add_plan()
	{
	
	
		$this->load->Model('school_typemodel');
			 
			$status=$this->school_typemodel->add_plan();
			if($status!=0){
				   $data['message']="Tab added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_plan()
	{
	$this->load->Model('school_typemodel');
			
	   $status=$this->school_typemodel->update_plan();
		if($status==true){
		       $data['message']="Tab Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		
		echo json_encode($data);
		exit;		
	}
	
	function delete($plan_id)
	{
		
		$this->load->Model('school_typemodel');
		$result = $this->school_typemodel->deleteplan($plan_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getplaninfo($plan_id)
	{
		if(!empty($plan_id))
	  {
		$this->load->Model('school_typemodel');
		$data['school_type']=$this->school_typemodel->getplanById($plan_id);
		$data['school_type']=$data['school_type'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)

	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = false;
			$last_btn = false;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='dataTables_paginate paging_bootstrap pagination'><ul>";


      // FOR ENABLING THE FIRST BUTTON
      if ($first_btn && $cur_page > 1) {
       $string.= "<li p='1' class='active'>First</li>";
      } else if ($first_btn) {
       $string.= "<li p='1' class='inactive'>First</li>";
      }


						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'><a href='javascript:void(0);'>← Prev</a></li>";
						} else if ($previous_btn) {
							$string.= "<li class='prev disabled'><a href='javascript:void(0);'>← Prev</a></li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {



							if ($cur_page == $i)
								$string.= "<li p='$i'  class='active'><a href='javascript:void(0);'>{$i}</a></li>";
							else
								$string.= "<li p='$i' class='active'><a href='javascript:void(0);'>{$i}</a></li>";
						}



						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'><a href='javascript:void(0);'>Next →</a></li>";
						} else if ($next_btn) {
							$string.= "<li class='next disabled'><a href='javascript:void(0);'>Next →</a> </li>";
						}



      // TO ENABLE THE END BUTTON
      if ($last_btn && $cur_page < $no_of_paginations) {
       $string.="<li p='$no_of_paginations' class='active'>Last</li>";
      } else if ($last_btn) {
       $string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
      }
      //$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
      $goto ='';
      $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
//      $count,$per_page
                                                        
                                                $string.= "</ul>" . $goto .  "</div><br /><br />";  // Content for pagination
 
 
     return $string;
 
 
 
 
 }
 
 function getschool_types_admin($page,$state_id,$country_id,$district_id=false)
	{
	
	    if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('school_typemodel');
		$total_records = $this->school_typemodel->getschool_typescount($state_id,$country_id,$district_id);
		
			$status = $this->school_typemodel->getschool_types($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>School Type Records</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>State</td><td >District Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['school_type_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['tab'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districtsname'].'</td>
				
				<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="planedit('.$val['school_type_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="plandelete('.$val['school_type_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination_admin($total_records,$per_page,$page,'school_type');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>State</td><td >District Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No School Type Records Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination_admin($total_records,$per_page,$page,'school_type');
						print $pagination;	
		}
		
	
	
	
	
	}
        
        function do_pagination_admin($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
 
	}
	?>