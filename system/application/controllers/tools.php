<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tools extends MY_Auth {
function __Construct()
	{
		parent::Controller();
	}

    public function index(){
	 if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/index',$data);
	} else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/index_data_trecker',$data);
} else if($this->session->userdata('login_type')=='user') {
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_index_data_trecker',$data);
    }
}
    
    public function data_tracker(){

	  if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/data_tracker',$data);
	} else if($this->session->userdata('login_type')=='observer') {
		 $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_data_tracker',$data);
	} else if($this->session->userdata('login_type')=='user') {

		 $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_data_trecker',$data);	
    }
}

public function access_content(){
    $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/access_content',$data);
}
    public function data_tracker_report(){

	 // if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/data_tracker_report',$data);
	/*} else if($this->session->userdata('login_type')=='observer') {
		 $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_data_tracker',$data);
	} else if($this->session->userdata('login_type')=='user') {

		 $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_data_trecker',$data);	
    }*/
}


    public function professional_development(){
	 if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/professional_development',$data);
	} else if($this->session->userdata('login_type')=='observer') {	
		
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_professional_development',$data);
	} else if($this->session->userdata('login_type')=='user') {	
		
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_professional_development',$data);
    }
}
    public function parent_bridge(){
		if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/parent_bridge',$data);
		
	} else if($this->session->userdata('login_type')=='observer') {	
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_parent_bridge',$data);
	} else if($this->session->userdata('login_type')=='user') {	
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_parent_bridge',$data);

		
    	}
	}
    public function guide(){
	if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/guide',$data);
	} else if($this->session->userdata('login_type')=='observer') {	
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_guide',$data);
		} else if($this->session->userdata('login_type')=='user') {	
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_guide',$data);
    }
}
    public function assessment_manager(){
        if($this->session->userdata('login_type')=='teacher') { 
	    $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/assessment_manager',$data);
		} else if($this->session->userdata('login_type')=='observer') {
		  $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_assessment_manager',$data);
		} else if($this->session->userdata('login_type')=='user') {
		  $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_assessment_manager',$data);
	}
}
    
    public function implementation_manager(){
   if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/implementation_manager',$data);
	} else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_implementation_manager',$data);
	} else if($this->session->userdata('login_type')=='user') {
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/dist_implementation_manager',$data);
    }
}
    public function observation(){
		if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/observation',$data);
		
		} else if($this->session->userdata('login_type')=='observer') {
		
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_observation',$data);
    } else if($this->session->userdata('login_type')=='user') {
		
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_observation',$data);
    }
}

   public function report_list() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'tools';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/teacher_report_list', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'tools';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/observer_report_list', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'tools';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/observer_report_list', $data);
        }
    }
	
    public function incident_report() {
        $data['idname'] = 'tools';
	
		$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	    //if($this->session->userdata('login_type')=='teacher'){
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/behavior_report_incident', $data);
        //}
    }
	
	    public function location_incident_report() {
        $data['idname'] = 'tools';

        $this->load->Model('schoolmodel');
        $data['school'] = $this->schoolmodel->getschoolbydistrict();


        //print_r($data['school']);exit;
        $this->load->Model('behavior_location_model');
        $data['behavior_location'] = $this->behavior_location_model->get_all_behavior_location_data();
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/location_incident_report', $data);
    }

    public function time_incident_report() {
        $data['idname'] = 'tools';
$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
		   
 $this->load->Model('periodmodel');
			$periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
	
//			$this->load->Model('parentmodel');
//            $data['students'] = $this->parentmodel->get_students_all();
//			
//           $this->load->model('problem_behaviour_model');
//	   $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/time_incident_report', $data);
    }

    public function number_of_behavior_staff_report() {
        $data['idname'] = 'tools';

        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
	
		$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	
        $this->load->Model('teachermodel');
        $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/number_of_behavior_staff_report', $data);
    }		
		
    public function students_behavior() {
        $data['idname'] = 'tools';
        // if($this->session->userdata('login_type')=='teacher'){

	$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
		   
	
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
		//print_r($data['students']);exit;

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/students_behavior', $data);
        //}
    }
	    public function month_report() {
        $data['idname'] = 'tools';

        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/month_report', $data);
    }
	
    public function classroom_management(){
		
		if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/classroom_management',$data);
		} else if($this->session->userdata('login_type')=='observer') {
			
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_classroom_management',$data);
	} else if($this->session->userdata('login_type')=='user') {
		$data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
       // $this->load->view('tools/dist_classroom_management',$data);
	    $this->load->view('tools/cam_classroom_management',$data);
    }
}

 public function iep_tracker(){
		if($this->session->userdata('login_type')=='teacher') { 
            $data['idname']='tools';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('tools/iep_tracker',$data);
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));

            $data['students'] = $this->parentmodel->get_students_all();
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('tools/cam_iep_tracker',$data);
        } else {
	    $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_iep_tracker',$data);
	}
}

 public function student_progress_report(){
		if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/student_progress_report',$data);
			} else {
	    $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_student_progress_report',$data);
	}
}

 public function success_team(){
		//if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/success_team',$data);
		/*}else if($this->session->userdata('login_type')=='observer') { 
        $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/success_team',$data);
		}else{
	    $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/cam_success_team',$data);
	}
	*/
}

    public function retrieve_success_report() {
       
            $data['idname'] = 'tools';
            $data['view_path'] = $this->config->item('view_path');

		 $this->load->model('Studentdetailmodel');
        $district_id = $this->session->userdata('district_id');
            $data['school_type'] = $this->Studentdetailmodel->getschooltype($district_id);
            
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
		
		
       // if ($this->session->userdata('login_type') == 'teacher') {
             $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
//            print_r($data['need_homes']);exit;
            
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();

            if($this->session->userdata('login_type') == 'parent') {
                $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
            $this->load->view('tools/retrieve_success_report_parent', $data);
            } else {
            $this->load->view('tools/retrieve_success_report', $data);
            }
       /* } else if ($this->session->userdata('login_type') == 'observer') {
             $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
//            print_r($data['need_homes']);exit;
            
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();
            
            $this->load->view('tools/retrieve_success_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $this->load->model('Studentdetailmodel');
            
            $district_id = $this->session->userdata('district_id');
            $data['school_type'] = $this->Studentdetailmodel->getschooltype($district_id);
            
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->view('tools/dist_retrieve_success_report', $data);
        }*/
    }
	
 public function retrieve_success_report_need() {
       
            $data['idname'] = 'tools';
    
        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
//            print_r($data['need_homes']);exit;
            
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_success_report_need', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'tools';
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
//            print_r($data['need_homes']);exit;
            
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_success_report_need', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'tools';
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_success_report_need', $data);
        }
    }
	
	
	public function retrieve_success_report_action() {
       
            $data['idname'] = 'tools';

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();
        
        if ($this->session->userdata('login_type') == 'teacher') {

            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_success_report_action', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'tools';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_success_report_action', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'tools';
            
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_success_report_action', $data);
        }
    }

public function retrieve_parent_report() {
       // if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
			
		/*	 $this->load->Model('classroommodel');
            $data['update_report'] = $this->classroommodel->get_parent_conference();*/
			
			 if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}
			
            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
            //$data['ideas_for_parent_student'] = $this->classroommodel->ideas_for_parent_student();
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/retrieve_parent_report', $data);
        /*} else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_retrieve_parent_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_retrieve_parent_report', $data);
        }*/
    }
	
	
 public function parent_teacher(){
	  $data['idname']='tools';
	 
	if($this->session->userdata("login_type")=='user' )
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}
                if($this->session->userdata("login_type")=='observer')	{
                    $this->load->Model('teachermodel');
                    $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
                }
            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
            //$data['ideas_for_parent_student'] = $this->classroommodel->ideas_for_parent_student();
//		print_r($data);exit;	
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('tools/parent_teacher', $data);
}

public function retrieve_parent_teacher() {
         $data['idname']='tools';
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
			$student_id = $this->input->post('student_id');
			
		
		
		
		if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}


            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
            $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
            //print_r($date);exit;
            $this->retrieve_parent_data($date, $grade_id, $student_id,$school_id);

//			$link = base_url().'classroom/retrieve_success_data/'.$grade_id.'/'.$student_id.'/'.$date.'';
            //redirect(base_url().'classroom/retrieve_success_report');	
        }
    }
	

    public function retrieve_parent_data($date, $grade_id, $student_id,$school_id) {
        $data['idname']='tools';


        error_reporting(1);
		
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
								
	}

        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
		

        $data['retrieve_reports'] = $this->classroommodel->parent_conference_report_pdf($date, $grade_id, $student_id,$school_id);

        //print_r($data['retrieve_report']);exit;
    	  if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			
			  if($this->session->userdata('login_type') == 'teacher'){
				 $this->load->Model('teachermodel');
				$data['teachername'] = $this->teachermodel->getteacherById($user_id);
			   
			}else {
				$this->load->Model('observermodel');
				$data['teachername'] = $this->observermodel->getobserverById($user_id);
			}

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/parent_teacher', $data);
    }	

public function retrieve_student_teacher() {
	    $data['idname'] = 'tools';
		
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
			$student_id = $this->input->post('student_id');


	if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}

            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
            $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
            //print_r($student_id);exit;
          $link = $this->retrieve_student_data($student_id, $grade_id, $date,$school_id);

//			$link = base_url().'classroom/retrieve_success_data/'.$grade_id.'/'.$student_id.'/'.$date.'';
            //redirect(base_url().'classroom/retrieve_success_report');	
        }
    }

    public function retrieve_student_data($student_id, $grade_id, $date,$school_id) {
        $data['idname'] = 'tools';

        error_reporting(1);

	 if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}
        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');
		 $this->load->Model('schoolmodel');
	
        $data['retrieve_reports'] = $this->classroommodel->student_conference_report_pdf($student_id, $grade_id, $date,$school_id);
		//print_r($data['retrieve_reports']);exit;
		 $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
			
            $data['ideas_for_parent_student'] = $this->classroommodel->ideas_for_parent_student();


        //print_r($data['retrieve_report']);exit;
       if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			   
	 if($this->session->userdata('login_type') == 'teacher'){
				 $this->load->Model('teachermodel');
				$data['teachername'] = $this->teachermodel->getteacherById($user_id);
		}else if($this->session->userdata('login_type') == 'observer'){
				$this->load->Model('observermodel');
				$data['teachername'] = $this->observermodel->getobserverById($user_id);
			} else {
                $this->load->Model('parentmodel');
                $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));
                $data['students'] = $this->parentmodel->get_students_all();
            }
			
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('tools/teacher_student', $data);
    }

 public function teacher_student(){
		
        $data['idname']='tools';
		
		
        //if ($this->session->userdata('login_type') == 'teacher') {
            
			  if($this->session->userdata("login_type")=='user' )
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}
                if($this->session->userdata("login_type")=='observer')	{
                    $this->load->Model('teachermodel');
                    $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
                }
			
						$this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
			
            $data['ideas_for_parent_student'] = $this->classroommodel->ideas_for_parent_student();

			
           /*} else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_retrieve_student_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_retrieve_student_report', $data);
        }*/
    
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('tools/teacher_student',$data);
		
}


public function attendance_manager(){


    $data['idname']='tools';
    $this->load->Model('periodmodel');
    $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));	

		$this->load->Model('teachermodel');
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        $this->load->Model('videooptionmodel');
        $data['video_option'] = $this->videooptionmodel->get_option();

	$data['view_path']=$this->config->item('view_path');
    if($this->session->userdata("login_type")!='parent'){
        $this->load->view('tools/attendance_manager',$data);
    } else {
        $this->load->Model('teachermodel');
        $this->load->Model('periodmodel');
        $data['periods'] = $this->periodmodel->getperiods(false,false,$this->session->userdata('school_id'));
        $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
        
        $this->load->view('tools/attendance_manager_parent',$data);
    }
}

public function district_planning(){
    if($this->session->userdata('login_type')=='user')
	  {
		 
	     /*$data['view_path']=$this->config->item('view_path');
	  
	  $this->load->Model('schoolmodel');
	  
	  $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	  
	  $this->load->view('district/home',$data);*/
	  
	  $data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');		
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
			  
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
		$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		}
		$this->load->view('district/dashboard',$data);
	  
	  }
}

public function dist_prof_dev_ind(){
    
      $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
    
    $login_required=$this->session->userdata('login_required'); 
	if($login_required=='')
		{
			header("Location: index");  
		}
		else
		{				
	 if($this->session->userdata('login_type')=='user')
	  {
		 
	     /*$data['view_path']=$this->config->item('view_path');
	  
	  $this->load->Model('schoolmodel');
	  
	  $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	  
	  $this->load->view('district/home',$data);*/
	  
	  $data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');		
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		if($this->input->post('submit'))
		{	
			$data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($this->input->post('school_type'));
			$data['school_type'] = $this->input->post('school_type');			
		}
		else
		{
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
		}	
         
			  
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
		$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		}
         
            $data['view_path']=$this->config->item('view_path');
            
                $this->load->view('tools/dist_prof_dev_ind',$data);		
		
	  
	  }
		}}
		
public function dist_prof_dev_ind_secondary(){
    
      $data['idname']='tools';
        $data['view_path']=$this->config->item('view_path');
    
    $login_required=$this->session->userdata('login_required'); 
	if($login_required=='')
		{
			header("Location: index");  
		}
		else
		{				
	 if($this->session->userdata('login_type')=='user')
	  {
		 
	     /*$data['view_path']=$this->config->item('view_path');
	  
	  $this->load->Model('schoolmodel');
	  
	  $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	  
	  $this->load->view('district/home',$data);*/
	  
	  $data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');		
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		if($this->input->post('submit'))
		{	
			$data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($this->input->post('school_type'));
			$data['school_type'] = $this->input->post('school_type');			
		}
		else
		{
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
		}	
         
			  
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
		$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		}
         
            $data['view_path']=$this->config->item('view_path');
            
                $this->load->view('tools/dist_prof_dev_ind_secondary',$data);		
		
	  }
		}}		

public function dist_observation_graph(){
		$data['idname']='tools';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		if($this->input->post('submit'))
		{	
			$data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($this->input->post('school_type'));
			$data['school_type'] = $this->input->post('school_type');			
		}
		else
		{
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
		}		
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
			$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		
		}
		 $this->load->view('tools/dist_prof_dev_ind',$data);
	
	
	}

}