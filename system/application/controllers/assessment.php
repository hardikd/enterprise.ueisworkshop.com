<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Assessment extends MY_Auth {
function __Construct()
	{
		parent::Controller();
	}

    public function index(){
        session_start();
        $data['idname']='assessment';
        $this->load->model('assessmentmodel','assessment');
        $district_id = $this->session->userdata('district_id');
        $_SESSION['district_id']=$district_id;
        $data['categorylist'] = $this->assessment->getAllAssignmentCats($district_id);
        $this->load->Model('videooptionmodel');
        $data['video_option'] = $this->videooptionmodel->get_option();
        $data['view_path']=$this->config->item('view_path');
        
        $this->load->view('assessment/assessment',$data);
    }
    
    public function assessments($cat_id,$cat_name){
        $data['idname']='assessment';
		
			if($this->session->userdata("login_type")=='user')
		{		
		  	$this->session->set_flashdata ('permission','Additional Permissions Required');
          redirect(base_url().'assessment');
		}

        $teacher_id = $this->session->userdata('teacher_id');
        $school_id=$this->session->userdata('school_id');
        $this->load->model('assessmentmodel','assessment');
	
        $data['students'] = $this->assessment->GetStudentByTeacherId($teacher_id,$school_id);

        $data['view_path']=$this->config->item('view_path');
        $data['cat_id'] = $cat_id;
        $data['cat_name'] = $cat_name;
        $this->load->view('assessment/diagnostic_screener',$data);
    }
    
    public function diagnostic_screener(){
        $data['idname']='assessment';
        $teacher_id = $this->session->userdata('teacher_id');
        $school_id=$this->session->userdata('school_summ_id');
        $this->load->model('assessmentmodel','assessment');
        $data['students'] = $this->assessment->GetStudentByTeacherId($teacher_id,$school_id);
        $data['view_path']=$this->config->item('view_path');
        
        
        $this->load->view('assessment/diagnostic_screener',$data);
    }
    
    public function diagnostic_screener_session(){
        
        
	session_start();
        $this->load->model('assessmentmodel','assessment');
        $studentdetails = $this->assessment->getStudentDetails($this->input->post('student_id'));
        
//        print_r($studentdetails);exit;
        
	$_SESSION['user_id'] = $studentdetails[0]['UserID'];
	$_SESSION['user_type'] = $studentdetails[0]['user_type'];
	$_SESSION['UserName'] = $studentdetails[0]['UserName'];
        $_SESSION['assessment_name'] = 'diagnostic_screener';
        $_SESSION['district_id'] = $studentdetails[0]['district_id'];
        $sessionarr = array('user_id'=>$studentdetails[0]['UserID'],'user_type'=>$studentdetails[0]['user_type'],'UserName'=>$studentdetails[0]['UserName'],'assessment_name'=>'diagnostic_screener');
        $this->session->set_userdata($sessionarr);
        
//        print_r($this->session->all_userdata());
//        $this->session->set_userdata('student_is',$this->input->post('student_id'));
//        $this->session->set_userdata(array('assessment_name'=>'diagnostic_screener'));
        
        return true;
    }
    
    public function getassignments(){
        $cat_id = $this->input->post('cat_id');
        $student_id = $this->input->post('student_id');
        $this->load->model('assessmentmodel','assessment');
        $data['assignments'] = $this->assessment->getAssignmentByStudent($cat_id,$student_id);
        
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('assessment/assignment_list',$data);
    }
    
    public function resetassignment (){
        $this->load->model('assessmentmodel','assessment');
        $status = $this->assessment->resetAssignmentByStudent($this->input->post('user_quizzes_id'),$this->input->post('studentId'));
        if($status){
                echo 'DONE';
            } else {
                echo 'ERROR';
            }
        
    }
    
    public function progress(){
        $data['idname']='assessment';
        $teacher_id = $this->session->userdata('teacher_id');
        $school_id=$this->session->userdata('school_summ_id');
        $this->load->model('assessmentmodel','assessment');
        $data['students'] = $this->assessment->GetStudentByTeacherId($teacher_id,$school_id);
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('assessment/progress',$data);
    }
    
    public function benchmark_assessment(){
        $data['idname']='assessment';
        $teacher_id = $this->session->userdata('teacher_id');
        $school_id=$this->session->userdata('school_summ_id');
        $this->load->model('assessmentmodel','assessment');
        $data['students'] = $this->assessment->GetStudentByTeacherId($teacher_id,$school_id);
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('assessment/benchmark_assessment',$data);
    }
    
}
