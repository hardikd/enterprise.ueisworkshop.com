<?php
/**
 * Countries Controller.
 *
 */
class Actions_school extends	MY_Auth {
function __Construct()
	{
			parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		$this->load->library('user_agent');
	}

	function index($need_school_child_id='')
	{	

		$data['view_path']=$this->config->item('view_path');
		$this->load->model('needs_for_school_child_model');
		$data['needs_school_child'] = $this->needs_for_school_child_model->get_all_school_child_data();
		$data['need_school_child_id']= $need_school_child_id;
		
		$this->load->view('actions/actions_school',$data);
	}
	
	function actions_school_list($page,$need_school_child_id=''){
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();

		$this->load->model('actions_school_model');
		
		$total_records = $this->actions_school_model->get_all_dataCount($need_school_child_id);
		if($need_school_child_id!=''){
		$data['alldata'] = $status = $this->actions_school_model->get_all_data($page,$per_page,$need_school_child_id);
		}else{
			$data['alldata'] = $status = $this->actions_school_model->get_all_school_data($page,$per_page);
		}
		//print_r($data['alldata']);exit;
		
		$data['pagination'] = $this->do_pagination($total_records,$per_page,$page,'actions_school');
						

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('actions/actions_school_edit',$data);
	}
	
	function actions_school_insert()
	{
		if($this->input->post('action_school_id')){
			$action_school_id = $this->input->post('action_school_id');
			$actions_school=$this->input->post('actions_school');
			$needs_school_child_id=$this->input->post('needs_school_child_id');
			$status=$this->input->post('status');
			$data=array('needs_school_child_id'=>$needs_school_child_id,'actions_school'=>$actions_school,'status'=>$status);
			$this->load->model('actions_school_model');
			$update = $this->actions_school_model->update('actions_school',$data,array('action_school_id'=>$action_school_id));
			if($update){
				echo "DONE";	
			} else {
				echo "ERROR";
			}

		} else{
			$district_id=$this->input->post('district_id');
			$is_delete=$this->input->post('is_delete');
			$actions_school=$this->input->post('actions_school');
			$needs_school_child_id=$this->input->post('needs_school_child_id');            
			$status=$this->input->post('status');
			$action_data=array('district_id'=>$district_id,'is_delete'=>$is_delete,'actions_school'=>$actions_school,'status'=>$status,'needs_school_child_id'=>$needs_school_child_id);
			$this->load->model('actions_school_model','actions_school');
			$insert = $this->actions_school->insert('actions_school',$action_data);
			if($insert){
				echo "DONE";	
			} else {
				echo "ERROR";
			}
			
		}
	}
 function edit($action_school_id)
	{
		$this->load->model('actions_school_model');
		$data['all']=$this->actions_school_model->get_actions_schoolById(array('action_school_id'=>$action_school_id));
		print_r(json_encode($data['all']));exit;
	}

	public function delete()
	{
		$action_data_remove =array('is_delete'=>'1');
		$this->load->model('actions_school_model');
		$schedule_id = $this->actions_school_model->delete_actions_school('actions_school',$action_data_remove);
		echo 'DONE';
	}	
	
function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active current'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}	
	
	
}
  
