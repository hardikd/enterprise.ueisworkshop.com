<?php
/**
 * report_description Controller.
 *
 */
class Report_description extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
		//$data['idname']='assessment';
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('report_description/index',$data);
	  
	  }else if($this->session->userdata("login_type")=='user')
	  {
	     $data['view_path']=$this->config->item('view_path');
	     $this->load->view('report_description/all',$data);
	  }
	  
	  
	
	}
	
	
	function getreport_descriptions($page,$state_id,$country_id,$district_id=false)
	{
	
	    if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('report_descriptionmodel');
		$total_records = $this->report_descriptionmodel->getreport_descriptionscount($state_id,$country_id,$district_id);
		
			$status = $this->report_descriptionmodel->getreport_descriptions($page, $per_page,$state_id,$country_id,$district_id);
		
		// echo $this->db->last_query();
		// print_r($status);exit;
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'></div><table class='table table-striped table-bordered' id='editable-sample'><tr><td>Tabs</td><td>Report</td><td>State</td><td >District Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['report_description_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['tab'].'</td>';
				if($val['report']==1)
				{
					$report='B & L Report';
				}
				if($val['report']==2)
				{
					$report='Classwork Performance';
				}
				if($val['report']==3)
				{
					$report='ELD Report';
				}
				if($val['report']==4)
				{
					$report='Observation Count';
				}
				if($val['report']==5)
				{
					$report='Notification Report';
				}
				if($val['report']==6)
				{
					$report='P.D. Activity Report';
				}
				if($val['report']==7)
				{
					$report='HomeWork Log';
				}
				if($val['report']==8)
				{
					$report='Student Progress Report';
				}
				if($val['report']==9)
				{
					$report='Student Performance Report';
				}
				if($val['report']==10)
				{
					$report='Student Performance Summary';
				}
				if($val['report']==11)
				{
					$report='Lesson Plan Report';
				}
				if($val['report']==12)
				{
					$report='Smart Goal';
				}
				if($val['report']==13)
				{
					$report='Student Success Team';
				}
				if($val['report']==14)
				{
					$report='Class Performance report';
				}
				if($val['report']==15)
				{
					$report='Student Attendance';
				}
				if($val['report']==16)
				{
					$report='Class Attendance';
				}
				if($val['report']==17)
				{
					$report='IEP';
				}
				if($val['report']==18)
				{
					$report='Instructional Efficacy';
				}
				if($val['report']==19)
				{
					$report='ehavior Running Record';
				}
				if($val['report']==20)
				{
					$report='Parent/Teacher Conference';
				}
				if($val['report']==21)
				{
					$report='Student/ Teacher Conference';
				}
				if($val['report']==22)
				{
					$report='Class Attendance';
				}
				if($val['report']==23)
				{
					$report='SST Summary Report';
				}
				if($val['report']==24)
				{
					$report='Student Success Team(Landscape) ';
				}
				print '<td>'.$report.'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districtsname'].'</td>
				
				<td nowrap>
				<button class="btn btn-primary" data-dismiss="modal" value="Edit" name="Edit" aria-hidden="true" onclick="planedit('.$val['report_description_id'].')"><i class="icon-pencil"></i></button>
  <button data-dismiss="modal" value="Delete" name="Delete" class="btn btn-danger" onclick="plandelete('.$val['report_description_id'].')"><i class="icon-trash"></i></button>
		</td>
			</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'report_description');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='table table-striped table-bordered' id='editable-sample'><tr><td>Tabs</td><td>State</td><td >District Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Report Description Records Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'report_description');
						print $pagination;	
		}
		
	
	
	
	
	}
	function add_plan()
	{
	
	
		$this->load->Model('report_descriptionmodel');
			 
			$cstatus=$this->report_descriptionmodel->check_plan();
			if($cstatus==0)
			{
			$status=$this->report_descriptionmodel->add_plan();
			if($status!=0){
				   $data['message']="Tab added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		  }
		  else
		  {
						$data['message']="Description  Already Added To The Distirct. " ;
					  $data['status']=0 ;
		  
		  
		  }
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_plan()
	{
	$this->load->Model('report_descriptionmodel');
	
	
			
		$cstatus=$this->report_descriptionmodel->update_check_plan();
			if($cstatus==0)
			{
	  $status=$this->report_descriptionmodel->update_plan();
		if($status==true){
		       $data['message']="Tab Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
				
			}	
			else
		  {
						$data['message']="Description  Already Added To The Distirct. " ;
					  $data['status']=0 ;
		  
		  
		  }
		
		echo json_encode($data);
		exit;		
	}
	
	function delete($plan_id)
	{
		
		$this->load->Model('report_descriptionmodel');
		$result = $this->report_descriptionmodel->deleteplan($plan_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getplaninfo($plan_id)
	{
		if(!empty($plan_id))
	  {
		$this->load->Model('report_descriptionmodel');
		$data['report_description']=$this->report_descriptionmodel->getplanById($plan_id);
		$data['report_description']=$data['report_description'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
				/*		if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						*/
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	}
	?>