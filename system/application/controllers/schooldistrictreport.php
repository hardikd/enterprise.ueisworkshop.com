<?php   
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
 

class Schooldistrictreport extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{ 
	$data['idname']='tools';
	
	 	 error_reporting(0);
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('schooldistrictreportmodel');
			    $district_id = $this->session->userdata('district_id');
				$data['records'] = $this->schooldistrictreportmodel->getallschools($district_id);
				
	 			$this->load->view('schooldistrictreport/index',$data);
	   
	 	 }
		
		 
		 if($this->session->userdata("login_type")=='observer')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('schooldistrictreportmodel');
			    $observer_id = $this->session->userdata('observer_id');
				//$data['records'] = $this->schooldistrictreportmodel->getteacherschool($teacher_id);
				$data['records'] = $this->schooldistrictreportmodel->getobservergrade($observer_id);
				$this->load->view('schooldistrictreport/index',$data);
	   
	 	 }
	
	}
	
	function getassessmentHtml()
	{
		error_reporting(0);
		 $school_id = $_REQUEST['school_id']; 
 	 	 $district_id = $this->session->userdata('district_id'); 
		 $data = array();
		 $this->load->model('schooldistrictreportmodel');
		 $data= $this->schooldistrictreportmodel->getassessment($school_id,$district_id);
		  $strClass ='';
		 $strClass .= '<select class="span12 chzn-select" tabindex="1" style="width: 300px;" name="assessment" id="assessment" onchange="updateassessment(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>' ;
		
		if(count($data[0]['id'])>0)
		{
	 	   $strClass .= '<option value="0" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
				 $strClass .='<option value="'.$value['id'].'">'.$value['assignment_name'].'</option>';
		 	}
         $strClass .= '</select>';
 		 echo $strClass;
	}
	
	function getassessmentHtmlObserver()
	{
		error_reporting(0);
		 $grade_id = $_REQUEST['grade_id']; 
 	 	 $data = array();
 	    $observerid = $this->session->userdata('observer_id'); 
	   $data = array();
		 $this->load->model('schooldistrictreportmodel');
 		$data['school'] = $this->schooldistrictreportmodel->getobserverschool($observerid);
		 $school_id = $data['school'][0]['school_id']; 
	
		 $data= $this->schooldistrictreportmodel->getassessmentofObserver($school_id,$grade_id);
		  $strClass ='';
		 $strClass .= '<td ><select class="span12 chzn-select" name="assessment" id="assessment" onchange="updateassessmentOb(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>' ;
		
		if(count($data[0]['id'])>0)
		{
	 	   $strClass .= '<option value="0" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
				 $strClass .='<option value="'.$value['id'].'">'.$value['assignment_name'].'</option>';
		 	}
         $strClass .= '</select>';
 		 echo $strClass;
	}

	
	function getReportHtml()
	{
		error_reporting(0);
		$school_id= $_REQUEST['school_id'];
		$district_id = $this->session->userdata('district_id'); 
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		
		if($school_id != -1)
		{
			$this->load->model('schooldistrictreportmodel');
			$data['gradename'] = $this->schooldistrictreportmodel->getgrades();
			//	echo "<pre>";print_r($data['gradename']);
			
			$htmlTable = "  <table class='table table-striped table-hover table-bordered' id='editable-sample' style='min-width:100%;'>
			 <tr>
			 <td style='white-space:nowrap;'> Estrellita Skills</td>
			 <td>All</td>";
			foreach($data['gradename'] as $key => $value)
			{
				if(preg_match("/Kindergarten/i",$value['grade_name']))
				{
					$grade_name = substr($value['grade_name'],0,6);
		 			$htmlTable .= "<td id='".$value['dist_grade_id']."'>".$grade_name."</td>";
				}
				else
				{
					$htmlTable .= "<td id='".$value['dist_grade_id']."'>".$value['grade_name']."</td>";
				}
				
			}
			$htmlTable .="</tr>";
			
		 	$data['Testitem']=$this->schooldistrictreportmodel->getTestItem($school_id,$district_id,$ass_id);
			$rows =  count($data['Testitem']);
			$row =  count($data['Testitem'][0]);
			$tablebody = '';
	 		if($row > 0)
			{
		 		$tablerow ='';
			 	for($i = 0 ;$i<$rows; $i++)
				{
					
					$tablerow .= "<tr bgcolor='#EEF9E3'> <td>".utf8_decode($data['Testitem'][$i]['test_cluster'])." </td>"; //utf8_decode(
					
					$test_cluster = $data['Testitem'][$i]['test_cluster'];
					$AllPer =$this->schooldistrictreportmodel->getPercentageAllByTestCluster($school_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id);	
					$tablerow .= "<td>".$AllPer."%</td>";
					
					foreach($data['gradename'] as $key => $value)
					{
					  $grade_id = $value['dist_grade_id'];
					   $gradewisePer =$this->schooldistrictreportmodel->getPercentageByGradeAndTestCluster($school_id,$test_cluster,$grade_id,$district_id,$fDate,$tDate,$ass_id);
 					 $tablerow .= "<td>".$gradewisePer."%</td>";						 
						
					}
				
				 	$tablerow .="</tr>";
			 	}
				 $tablebody .= $tablerow;
			}
			else
			{
				$tablebody = '<tr><td></td><td colspan="10" align="center"> No Record Found.</td></tr>';
			}
			
			$htmlTable .= $tablebody;
			$htmlTable .= "</table>";
	 	}
		else
		{
			$htmlTable = "Please select School.";
		}
		  echo $htmlTable;
	}
	
	
	function getgradeReportHtml()
	{
	 	$observerid= $_REQUEST['observerid'];
		$gradeid= $_REQUEST['grade_id'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 

	if($gradeid != -1)
	{	 
		$this->load->model('schooldistrictreportmodel');
		
		$data['school'] = $this->schooldistrictreportmodel->getobserverschool($observerid);
		$school_id = $data['school'][0]['school_id'];
		if($gradeid == 0) //for all
		{
			$data['gradename'] = $this->schooldistrictreportmodel->getobservergradeheader($observerid);
			$htmlTable = " <table class='table table-striped table-hover table-bordered' id='editable-sample' style='min-width:100%;'> 
			<tr>
			<td style='white-space:nowrap;'> Estrellita Skills</td>
			<td>All</td>";
			//echo count($data['gradename']);
			foreach($data['gradename'] as $key => $value)
			{
		 		if(preg_match("/Kindergarten/i",$value['grade_name']))
				{
					$grade_name = substr($value['grade_name'],0,6);
		 			$htmlTable .= "<td id='".$value['dist_grade_id']."'>".$grade_name."</td>";
				}
				else
				{
					$htmlTable .= "<td id='".$value['dist_grade_id']."'>".$value['grade_name']."</td>";
				}
	 		}
			$htmlTable .="</tr>";
			 $district_id = $data['gradename'][0]['district_id']; 
			$data['Testitem']=$this->schooldistrictreportmodel->getTestItem($school_id,$district_id,$ass_id);
			$row =  count($data['Testitem'][0]);
			$rows =  count($data['Testitem']);
			$tablebody = '';
	 		if($row > 0)
			{
			 	$tablerow ='';
			 	for($i = 0 ;$i<$rows; $i++)
				{
				 	$tablerow .= "<tr  bgcolor='#EEF9E3'> <td>".$data['Testitem'][$i]['test_cluster']." </td>"; //utf8_decode(
					
					$test_cluster = $data['Testitem'][$i]['test_cluster'];
					$AllPer = $this->schooldistrictreportmodel->getPercentageAllByTestCluster($school_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id);	
					$tablerow .= "<td>".$AllPer."%</td>";
					
					foreach($data['gradename'] as $key => $value)
					{
					  $grade_id = $value['dist_grade_id'];
					   $gradewisePer =$this->schooldistrictreportmodel->getPercentageByGradeAndTestCluster($school_id,$test_cluster,$grade_id,$district_id,$fDate,$tDate,$ass_id);
 					 $tablerow .= "<td>".$gradewisePer."%</td>";						 
						
					}
				
				 	$tablerow .="</tr>";
			 	}
	 		}
			else{ $tablerow = "<tr> <td></td><td colspan='3' align='center'> No Record Found.</td></tr>"; }
			
			 $tablebody .= $tablerow;
						 
	 	}
		else  //for selected school
		{
			$data['grade'] = $this->schooldistrictreportmodel->getobservergradeheader($observerid);
			
			$data['gradename'] = $this->schooldistrictreportmodel->getgradenamebyId($gradeid);
			$district_id = $data['grade'][0]['district_id'];
			
			$htmlTable = "<table   width='100%'> <tr class='tchead'><td width='130'>Standard / Cluster Item</td>";
			//$htmlTable .= "<td>". $data['gradename'][0]['grade_name']."</td> </tr>";
			$htmlTable .= "<td> Score </td> </tr>";
			$data['Testitem']=$this->schooldistrictreportmodel->getTestItem($school_id,$district_id,$ass_id);
			 $rows =  count($data['Testitem']);
			$tablebody = '';
			$row =  count($data['Testitem'][0]);
	 		if($row > 0)
			{
				$tablerow ='';
			 	for($i = 0 ;$i<$rows; $i++)
				{
				 	$tablerow .= "<tr  bgcolor='#EEF9E3'> <td >".utf8_decode($data['Testitem'][$i]['test_cluster'])."</td>";
					$test_cluster = $data['Testitem'][$i]['test_cluster'];
				 $gradewisePer =$this->schooldistrictreportmodel->getPercentageByGradeAndTestCluster($school_id,$test_cluster,$gradeid,$district_id,$fDate,$tDate,$ass_id);
 					 $tablerow .= "<td>".$gradewisePer."%</td></tr>";						 
			 	}
			}
			else { $tablerow = "<tr> <td></td><td> No Record Found.</td></tr>";  } 
			
			 $tablebody .= $tablerow;
		
		}
		$htmlTable .= $tablebody;
		$htmlTable .= "</table>";
	}
	 else
	 {
	 	$htmlTable = "Please select Grade.";
	 }
		  echo $htmlTable;
	}
	
	function getCsv()
	{
		$school_id= $_REQUEST['school_id'];
		$district_id = $this->session->userdata('district_id');
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
 		$ass_id = $_REQUEST['assignment_id']; 

		if($school_id != -1)
		{
		 	
			$this->load->model('schooldistrictreportmodel');
			$data['gradename'] = $this->schooldistrictreportmodel->getgrades();
			$columnTitle = array();
			$columnTitle[0] = 'Test Cluster';
			$columnTitle[1] = 'All';
			foreach($data['gradename'] as $key => $value)
			{
				$columnTitle[] = $value['grade_name'];
			}	 
			
			if($_SERVER['HTTP_HOST'] == 'localhost')//if($_SERVER['HTTP_HOST'] == 'www.maven-infotech.com')
			{
				$path = $_SERVER['DOCUMENT_ROOT']."/trunk/system/application/views/schooldistrictreport/";
				//$path = $_SERVER['DOCUMENT_ROOT']."/demo/workshop/system/application/views/schooldistrictreport/";
		 	}
			else
			{
				$path = $_SERVER['DOCUMENT_ROOT']."/system/application/views/schooldistrictreport/";
			}
			$filename = $path."SchoolLevelTestClusterReport.csv";
			 chmod($filename,0755);
			$handle = fopen($filename, 'w+');
			
			$school_name ='';
			if($school_id == 0)
			{
				$school_name = "All School ";
			}
			else
			{		
				$school_name = "School : ";		
				$data['records'] = $this->schooldistrictreportmodel->getcschools($school_id);
				$school_name .= $data['records'][0]['school_name']; 
			}
			
			$header =  "Result Report Per Test Item Of ".$school_name;
			fputcsv($handle,array('','','','','',$header));
			fputcsv($handle,array(''));
			fputcsv($handle,$columnTitle);
			
			$data['Testitem']=$this->schooldistrictreportmodel->getTestItem($school_id,$district_id,$ass_id);
			$rows =  count($data['Testitem']);
			
			if($rows > 0)
			{	
			 	for($i = 0 ;$i<$rows; $i++)
				{
					$dataArray = array();
					
				//	echo $cluster =$data['Testitem'][$i]['test_cluster'];echo"<br>";
			//	echo 	$cluster =  html_entity_decode($data['Testitem'][$i]['test_cluster'],ENT_QUOTES); echo"<br>";
			//	echo	$cluster =  htmlentities($data['Testitem'][$i]['test_cluster'],ENT_QUOTES); echo"<br>";
				  	$dataArray[0] = utf8_decode($data['Testitem'][$i]['test_cluster']);  
			 
			//	echo	$dataArray[0] =mb_convert_encoding($data['Testitem'][$i]['test_cluster'],'','utf-8');
					$test_cluster = $data['Testitem'][$i]['test_cluster'];
					$AllPer =$this->schooldistrictreportmodel->getPercentageAllByTestCluster($school_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id);
					$dataArray[1] = $AllPer; 
					foreach($data['gradename'] as $key => $value)
					{
					  	$grade_id = $value['dist_grade_id'];
					   $gradewisePer =$this->schooldistrictreportmodel->getPercentageByGradeAndTestCluster($school_id,$test_cluster,$grade_id,$district_id,$fDate,$tDate,$ass_id);				
					   $dataArray[] = $gradewisePer;
 		 			}
					
					fputcsv($handle,$dataArray);
				}
			}
			
			fclose($handle);
		 	$base =  base_url();
			$b = explode('index.php',$base);
			$baseUrl = $b[0];
		 	echo" <a href='".$baseUrl."system/application/views/schooldistrictreport/SchoolLevelTestClusterReport.csv'> <input type='button' id='csvbutton' class='btn btn-purple dropdown-toggle' value='Download CSV File' /> </a>";
			
			//echo" <a href='".$_SERVER['SERVER_NAME']."/workshop/quizreportscsv/SchoolLevelTestClusterReport.csv'> Download </a>";
		}
		else
		{
			echo "Please select School.";
		}

 		 exit;
	}
	
	function getgradeCsv()
	{
		$observerid= $_REQUEST['observerid'];
		$gradeid= $_REQUEST['grade_id'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
	
	 if($gradeid != -1)
	 {
		$this->load->model('schooldistrictreportmodel');
		$data['school'] = $this->schooldistrictreportmodel->getobserverschool($observerid);
		$school_id = $data['school'][0]['school_id'];
		$district_id = $data['school'][0]['district_id'];
		$school_name = $data['school'][0]['school_name'];
		if($_SERVER['HTTP_HOST'] == 'localhost')
		{
			$path = $_SERVER['DOCUMENT_ROOT']."/trunk/system/application/views/schooldistrictreport/";
		}else if($_SERVER['HTTP_HOST'] == 'localhost')
                {
                    $path = $_SERVER['DOCUMENT_ROOT']."/waptrunk/system/application/views/schooldistrictreport/";
                    
                }
                    else
		{
			$path = $_SERVER['DOCUMENT_ROOT']."/system/application/views/schooldistrictreport/";
		}
		$filename = $path."gradeLevelTestClusterReport.csv";
		 chmod($filename,0755);
		$handle = fopen($filename, 'w+');
			
		if($gradeid == 0) //for all
		{
	 		$data['gradename'] = $this->schooldistrictreportmodel->getobservergradeheader($observerid);
			
			$columnTitle = array();
			$columnTitle[0] = 'Test Cluster';
			$columnTitle[1] = 'All';
			foreach($data['gradename'] as $key => $value)
			{
				$columnTitle[] = $value['grade_name'];
			}	 
			
			$grade_name = "All Grades ";
		 	$header =  "Result Report Per Test Item Of ".$grade_name." of school : ". $school_name;
			fputcsv($handle,array(''));
			fputcsv($handle,array('','','','','',$header));
			fputcsv($handle,array(''));
			fputcsv($handle,$columnTitle);
			$data['Testitem']=$this->schooldistrictreportmodel->getTestItem($school_id,$district_id,$ass_id);
			$rows =  count($data['Testitem']);
			
			if($rows > 0)
			{	
			 	for($i = 0 ;$i<$rows; $i++)
				{
				 	$dataArray = array();
					  $cluster =$data['Testitem'][$i]['test_cluster'];
	 			 	$dataArray[0] = utf8_decode($cluster); 
					$test_cluster = $data['Testitem'][$i]['test_cluster'];
					$AllPer =$this->schooldistrictreportmodel->getPercentageAllByTestCluster($school_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id);	
					$dataArray[1] = $AllPer; 
					foreach($data['gradename'] as $key => $value)
					{
					  	$grade_id = $value['dist_grade_id'];
					    $gradewisePer =$this->schooldistrictreportmodel->getPercentageByGradeAndTestCluster($school_id,$test_cluster,$grade_id,$district_id,$fDate,$tDate,$ass_id);
					   $dataArray[] = $gradewisePer;
 		 			}
					 
					fputcsv($handle,$dataArray);
				}
			}
		}	
		else
		{
			$grade_name = "Grade : ";		
			$data['records'] = $this->schooldistrictreportmodel->getgradenamebyId($gradeid);
			$grade_name .= $data['records'][0]['grade_name'];
			$header =  "Result Report Per Test Item Of ".$grade_name." of school : ". $school_name;
			
			$columnTitle = array();
			$columnTitle[0] = '';
			$data['gradename'] = $this->schooldistrictreportmodel->getgradenamebyId($gradeid);
			$columnTitle[1] = $data['gradename'][0]['grade_name'];
			fputcsv($handle,array(''));
			fputcsv($handle,array('',$header));
			fputcsv($handle,array(''));
			fputcsv($handle,$columnTitle);
			
			$data['Testitem']=$this->schooldistrictreportmodel->getTestItem($school_id,$district_id,$ass_id);
			 $rows =  count($data['Testitem']);
			if($rows > 0)
			{
			 	for($i = 0 ;$i<$rows; $i++)
				{
					$dataArray = array();
				  	$dataArray[0] = utf8_encode($data['Testitem'][$i]['test_cluster']);
					$test_cluster = $data['Testitem'][$i]['test_cluster'];
				 $gradewisePer =$this->schooldistrictreportmodel->getPercentageByGradeAndTestCluster($school_id,$test_cluster,$gradeid,$district_id,$fDate,$tDate,$ass_id);
					$dataArray[1] = $gradewisePer; 
						
					fputcsv($handle,$dataArray);
				}
			}
		}
		
			fclose($handle);
		 	$base =  base_url();
			$b = explode('index.php',$base);
			$baseUrl = $b[0];
		 	echo" <a href='".$baseUrl."system/application/views/schooldistrictreport/gradeLevelTestClusterReport.csv'> <input type='button' id='csvbutton' value='Download CSV File' /> </a>";
			
			//echo" <a href='".$_SERVER['SERVER_NAME']."/workshop/quizreportscsv/SchoolLevelTestClusterReport.csv'> Download </a>";
	 }	
	 else
	 {
	 	$htmlTable = "Please select Grade.";
	 }
 		 exit;
	}
	
	
	 
	
}	
