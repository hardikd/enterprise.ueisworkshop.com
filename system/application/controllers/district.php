<?php
/**
 * district Controller.
 *
 */
class District extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('district/index',$data);
	
	}
	function getDistricts($page,$state_id,$country_id)
	{
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('districtmodel');
		$total_records = $this->districtmodel->getDistrictCount($state_id,$country_id);
		
			$status = $this->districtmodel->getDistricts($page, $per_page,$state_id,$country_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Districts</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td>Country</td><td>State</td><td>District</td><td>Users</td><td>Product Specialist</td><td >Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['district_id'].'" class="'.$c.'" >';
			
				print '
				<td >'.$val['country'].'</td>
				<td >'.$val['name'].'</td>
				<td >'.$val['districts_name'].'</td>
				<td ><a href="district/add_user/'.$val['district_id'].'">Add/View</a></td>	
				<td ><a href="prod_spec/add_prod_spec/'.$val['district_id'].'">Add/View</a></td>								
				<td  nowrap><input class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="distedit('.$val['district_id'].')"><input class="btnsmall" title="Delete" type="button" name="Delete" value="Delete" onclick="distdelete('.$val['district_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'districts');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td>Country</td><td>State</td><td>District</td><td >Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Districts Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'districts');
						print $pagination;	
		}
		
		
	}
	
	function getdistrictinfo($dist_id)
	{
		if(!empty($dist_id))
	  {
		$this->load->Model('districtmodel');
		
		$data['district']=$this->districtmodel->getDistrictById($dist_id);
		$data['district']=$data['district'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_district()
	{


		$this->load->Model('districtmodel');
	if($this->districtmodel->check_district_exists()==true){
		
		
		
		if($this->input->post('copystate'))
		{
		 $copystatus=$this->districtmodel->check_copystate();
		 if($copystatus>=1)
		 {
		     $data['message']="Already One District in Copy State" ;
		     $data['status']=0 ;
		
		
		
		echo json_encode($data);
		exit;	
		 
		 
		 }
		
		
		}
		$status=$this->districtmodel->add_district();
		//print_r($status);exit;

		if($status!=0){
		       $data['message']="District added Sucessfully" ;
			   $data['status']=1 ;
						
						
                           if($this->input->post('copymasterdata')){
                               $this->districtmodel->copymasterdata($status,47);
                           }
						    if($this->input->post('checklist_personnel_evalution_setup')){
								$this->districtmodel->checklist_standards_data($status,47);
								$this->districtmodel->checklist_rubricscale_data($status,47);
						   		$this->districtmodel->checklist_rubricscale_sub_data($status,47);
								$this->districtmodel->proficiency_rubric_standards($status,47);
								$this->districtmodel->likert_standards($status,47);
								$this->districtmodel->leadership_rubric_standards($status,47);
							   $this->districtmodel->teacher_status($status,47);
								$this->districtmodel->teacher_performance_rating($status,47);
								$this->districtmodel->sst_meeting_type($status,248);
								$this->districtmodel->sst_program_change($status,248);
								$this->districtmodel->sst_progress_monitoring($status,248);
								$this->districtmodel->sst_referral_reason($status,248);
								$this->districtmodel->intervention_strategies_home($status,248);
								$this->districtmodel->intervention_strategies_medical($status,248);
								$this->districtmodel->intervention_strategies_school($status,248);
								$this->districtmodel->interventions_prior($status,248);
								$this->districtmodel->behavior_teacher_student($status,248);
								$this->districtmodel->indicate_parent_consent($status,248);
								$this->districtmodel->ideas_for_parent_student($status,248);
								$this->districtmodel->behavior_location($status,248);
								//$this->districtmodel->behavior_teacher_student($status,248);
								$this->districtmodel->dist_prob_behaviour($status,248);

								$this->districtmodel->concerns($status,248);
								$this->districtmodel->strengths($status,248);
								$this->districtmodel->history($status,248);
								$this->districtmodel->strategy($status,248);
								

						   }
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		  $data['message']="District With Same Name Already Exists" ;
		  $data['status']=0 ;
		
		}
		//print_r($data);exit;
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_district()
	{
	$this->load->Model('districtmodel');
       if($this->districtmodel->check_district_update()==true){
		if($this->input->post('copystate'))
		{
		 
		$copystatus=$this->districtmodel->check_copystate_update();
		
		 
		 if($copystatus>=1)
		 {
		     $data['message']="Already One District in Copy State" ;
		     $data['status']=0 ;
		
		
		
		echo json_encode($data);
		exit;	
		 
		 
		 }
		
		
		}
	   $status=$this->districtmodel->update_district();
		if($status==true){
		       $data['message']="District Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		  $data['message']="District With Same Name Already Exists" ;
		  $data['status']=0 ;

		}	
		echo json_encode($data);
		exit;		
	}
	
	function delete($dist_id)
	{
		
		$this->load->Model('districtmodel');
		$result = $this->districtmodel->deleteDistrict($dist_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	function getAlldistricts()
	{
	  $this->load->Model('districtmodel');
	  $data['district']=$this->districtmodel->getalldistricts();
		
		echo json_encode($data);
		exit;
	
	}
	
	function getDistrictsByStateId($state_id)
	{
		$this->load->Model('districtmodel');
	  $data['district']=$this->districtmodel->getDistrictsByStateId($state_id);
		
		echo json_encode($data);
		exit;
	
	
	
	}
	function getDistrictsByschooltype($dis_id)
	{
		$this->load->Model('school_typemodel');
	  $data['school_type']=$this->school_typemodel->getallplans($dis_id);
		
		echo json_encode($data);
		exit;
	
	
	
	}
	
	
	// Add district user into district database table
	
	function add_user($district_id)
	{
	 
		$this->session->set_userdata("user_dist_id",$district_id);
		
	
	  
	 
	    $this->load->Model('districtmodel');
		$data['district']=$this->districtmodel->getDistrictById($district_id);
		if($data['district']!=false)
		{
		   $data['districts_name']=$data['district'][0]['districts_name'];
		  $this->session->set_userdata("user_dist_name",$data['district'][0]['districts_name']);
		
		}
		
		else
		{ 
		 $data['districts_name']='';
		
		}
	
	    $data['view_path']=$this->config->item('view_path');
	  $this->load->view('district/users',$data);
	
	}
	
	function getdist_users($page)
	{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('districtmodel');
		$total_records = $this->districtmodel->getdist_userCount();
		
			$status = $this->districtmodel->getdist_users($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>District Users</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >User Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['dist_user_id'].'" class="'.$c.'" >';
			
				print '<td >'.$val['username'].'</td>
					   
				
				<td  nowrap><input class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="dist_useredit('.$val['dist_user_id'].')"><input  class="btnsmall" title="Delete" type="button" name="Delete" value="Delete" onclick="dist_userdelete('.$val['dist_user_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'dist_users');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td >User Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No district Users Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'dist_users');
						print $pagination;	
		}
		
		
	}
	
	function getdist_userinfo($dist_user_id)
	{
		if(!empty($dist_user_id))
	  {
		$this->load->Model('districtmodel');
		
		$data['dist_user']=$this->districtmodel->getdist_userById($dist_user_id);
		$data['dist_user']=$data['dist_user'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	//Add district user
	
	function add_dist_user()
	{
	
	
		$this->load->Model('districtmodel');
	
		if($this->districtmodel->check_dist_user_exists()==true)
		 {
		$status=$this->districtmodel->add_dist_user();
		$adduser['dist_user_detail'] = $this->districtmodel->getdistusers($status);
		$dist_user_id=$adduser['dist_user_detail'][0]['dist_user_id'];
		$dist_username=$adduser['dist_user_detail'][0]['username'];
		$dist_userpassword=$adduser['dist_user_detail'][0]['password'];
		$dist_user_email=$adduser['dist_user_detail'][0]['email'];
		$dist_id=$adduser['dist_user_detail'][0]['district_id'];
		
		$data['final']=$this->districtmodel->adddistrictuser($dist_user_id,$dist_username,$dist_userpassword, $dist_user_email,$dist_id);
		
			
		if($status!=0){
		       $data['message']="dist_user added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		  $data['message']="dist_user With Same User Name   Already Exists" ;
		  $data['status']=0 ;

		}		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_dist_user()
	{
	$this->load->Model('districtmodel');
	    if($this->districtmodel->check_dist_user_update()==true)
		 {
		$status=$this->districtmodel->update_dist_user();
		if($status==true){
		       $data['message']="dist_user Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else
		{
		  $data['message']="dist_user With Same User Name   Already Exists" ;
		  $data['status']=0 ;

		}			
		echo json_encode($data);
		exit;		
	}
	
	function delete_user($dist_user_id)
	{
		
		$this->load->Model('districtmodel');
		$result = $this->districtmodel->deletedist_user($dist_user_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	