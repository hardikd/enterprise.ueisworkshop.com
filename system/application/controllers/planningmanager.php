<?php
ob_start();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$view_path="system/application/views/";
require_once($view_path.'inc/html2pdf/html2pdf.class.php');
class Planningmanager extends MY_Auth {
    
        function __Construct(){
        	parent::Controller();
            if($this->session->userdata('login_type')==''){
                redirect(base_url());
            }
                
	}
	
        public function index(){
        
        
       
	      if($this->session->userdata('login_type')=='teacher') { 
	 
	    $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/index',$data);
     
	 } else if($this->session->userdata('login_type')=='user') {
	 
	    $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/dist_index',$data);
    }else if($this->session->userdata('login_type')=='observer') {
	 
	    $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/index',$data);
    }
	}
    
        public function lesson_plan_creator(){
		
	if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='lesson';
        $this->load->Model('videooptionmodel');
            $data['video_option'] = $this->videooptionmodel->get_option();
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/lesson_plan_creator',$data);
 	} else if($this->session->userdata('login_type')=='user') {
		$data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/dist_lesson_plan_creator',$data);
	}else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/lesson_plan_creator',$data);
		}
		
	  }
    
        public function lesson_plan(){
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/lesson_plan',$data);
    }
    
	public function view_your_events(){
        $data['idname']='lesson';
		$this->load->Model('classroommodel');		
		$data['update_report'] = $this->classroommodel->event_list();
		//print_r($data['update_report']);exit;
		$data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/view_your_events',$data);
    }
	
	public function view_class_events(){

        $data['idname']='lesson';
		
		$this->load->Model('teachermodel');
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	    $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/view_class_events',$data);
    }
	
//        public function view_class_events_calendar(){
//        $data['idname']='lesson';
//        $data['view_path']=$this->config->item('view_path');
//        $this->load->view('planningmanager/view_class_events_calendar',$data);
//    }
    
    public function view_class_events_calendar() {
	
        $data['idname'] = 'lesson';
        if ($this->input->post('submit')) {
		   $teacher_id = $this->input->post('teacher_id');

            $this->teacher_by_event_data($teacher_id);

        }
    }
	

    public function teacher_by_event_data($teacher_id) {
        $data['idname'] = 'lesson';


        error_reporting(1);
		
	
       $this->load->Model('classroommodel');
		$data['get_teacher'] = $this->classroommodel->get_event_by_teacher($teacher_id);
//                print_r($data);exit;
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/view_class_events_calendar',$data);
    }
	
        public function upload_goals(){
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/upload_goals',$data);
    }
		
        public function create_plan(){
        //print_r($this->session->all_userdata());exit;
        if($this->session->userdata('login_type')=='user'){
            redirect(base_url());
        }
        $data['type']='Create';
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        if($this->session->userdata('login_type')=='observer')
        {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            
        }
        
    if($this->session->userdata('login_type')=='user')
      {
        
      $this->load->Model('schoolmodel');
            $data['school']=$this->schoolmodel->getschoolbydistrict();
    
        }
        
        $this->load->view('planningmanager/create_plan',$data);
    }
    
        public function check_lesson_plan(){
        $this->load->Model('lessonplanmodel');
         $this->load->Model('time_tablemodel');
        $cdataarr = explode('-',$this->input->post('cdate'));
        
        $cdate = $cdataarr[2].'-'.$cdataarr[0].'-'.$cdataarr[1];    //date("Y-m-d",strtotime($this->input->post('cdate')));
        $chkdate = $cdataarr[1].'-'.$cdataarr[0].'-'.$cdataarr[2];
        $data['dates']=$this->week_from_monday($chkdate);
        foreach($data['dates'] as $key=>$val)
	  {
	    $data['dates'][$key]['week']=$val['week'];
		$cdate=$val['date'];		
		$cdate1=explode('-',$cdate);
		$data['dates'][$key]['date']=$cdate1[1].'-'.$cdate1[2].'-'.$cdate1[0];
		$lessonplans=$this->time_tablemodel->gettimetablebyteacher($val['day']);
		if($lessonplans!=false && (strtotime($cdate)>=strtotime(date('Y-m-d'))))
		{
			
			foreach($lessonplans as $lessonval)
			{
				$lessonexists=$this->lessonplanmodel->check_lessonplan_exists($cdate,$lessonval['start_time'],$lessonval['end_time']);
				if($lessonexists==false)
				{					
					$this->lessonplanmodel->add_planbytimetable($lessonval['start_time'],$lessonval['end_time'],$cdate,$lessonval['grade_id'],$lessonval['subject_id'],$lessonval['period_id']);
				}
			}
		
		}
		
		
		
	  }
          $lessonplans=$this->time_tablemodel->gettimetablebyteacher($val['day']);
          
          
          $html = '<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">';
          
          foreach($lessonplans as $lessonplansval){
              $html .= '<option value="'.$lessonplansval['start_time'].'">'.$lessonplansval['start_time'].'-'.$lessonplansval['end_time'].'</option>';
          }
                                        
        
          $html .='</select>';
          echo $html;

    }
    
        public function create_plan_step1(){
//        print_r($_POST);exit;
        $data['type'] = $this->input->post('plan_type');
        $data['idname']='lesson';
        $this->load->Model('lessonplanmodel');
        $cdataarr = explode('-',$this->input->post('cdate'));
        
        $cdate = $cdataarr[2].'-'.$cdataarr[0].'-'.$cdataarr[1];    //date("Y-m-d",strtotime($this->input->post('cdate')));
        $chkdate = $cdataarr[1].'-'.$cdataarr[0].'-'.$cdataarr[2];
        $data['dates']=$this->week_from_monday($chkdate);
        
      $this->load->Model('time_tablemodel');
       $this->load->Model('custom_differentiatedmodel');
        $this->load->Model('periodmodel');
       
        $ctimearr = explode(' ',$this->input->post('ctime'));
        if ($ctimearr[1]=='AM'){
            $ctime = date('h:i:s',strtotime($ctimearr[0]));
        } else {
            $ctime = date('H:i:s',strtotime($ctimearr[0])+43200);
        }
        
        if($this->session->userdata("teacher_id")){
            $teacher_id = $this->session->userdata("teacher_id");
        } else {
            $data['teacher_id'] = $teacher_id = $this->input->post('teacher_id');   
        }
               
        $teacher_lesson_week_plan = $this->lessonplanmodel->lessonplansbyteacherbytimetable($teacher_id,$cdate,$cdate,$ctime);
//        print_r($teacher_lesson_week_plan);exit;
//        echo $this->db->last_query();exit;
        if ($teacher_lesson_week_plan==false){
            $this->session->set_flashdata('error','No lesson at mentioned date & time');
            redirect(base_url().'planningmanager/create_plan');
        } else {
            $this->load->Model('time_tablemodel');
            $this->load->Model('custom_differentiatedmodel');
            $this->load->Model('standarddatamodel');
        
            
             $lesson_week=$this->lessonplanmodel->getteacherplaninfo($teacher_lesson_week_plan[0]['lesson_week_plan_id']);
             //echo $this->db->last_query();
             //print_r($lesson_week);exit;
//              echo $this->db->last_query();exit;
             $this->load->Model('lesson_plan_materialmodel');
            $data['materials'] = $this->lesson_plan_materialmodel->getmaterial($teacher_lesson_week_plan[0]['subject_id'],$lesson_week[0]['grade'],$school_id);     
            
            $this->load->Model('curriculummodel');
            $data['curriculum'] = $this->curriculummodel->checkcurriculum();
            if($data['curriculum']!='materials' && $data['curriculum']!='textbook' && $data['curriculum']!='othermaterials'){
                
                $data['grades']=$this->standarddatamodel->getActiveGrades($lesson_week[0]["subject_id"],$lesson_week[0]["grade"]);
//                print_r($data['grades']);exit;
    //            echo $this->db->last_query();exit;
                if($data['grades']==''){
                    $this->session->set_flashdata('error','Entered time & date does not have proper lesson plan.');
    //                redirect(base_url().'planningmanager/create_plan');
    //              exit;  
                }
            } 
//            print_r($data['grades']);exit;
            //print_r($lesson_week[0]["subject_id"]);exit;
            $lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
//            print_r($lessonplans);exit;
            $lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
//           print_r($lessonplansub);exit;
             foreach($lessonplans as $lessonplansval){
             foreach($lessonplansub as $lessonplanssubval){
               $data['lessontype'][$lessonplansval['tab']]["lesson_plan_id"] = $lessonplansval['lesson_plan_id']; 
                    //if($lessonplansval)
                    if($lessonplansval['lesson_plan_id']==$lessonplanssubval['lesson_plan_id']){
                        $data['lessontype'][$lessonplansval['tab']][$lessonplanssubval['lesson_plan_sub_id']] =  $lessonplanssubval['subtab'];
                    }
                }
            }
         //   print_r($lesson_week);exit;
            $this->load->Model('standarddatamodel');
            if($data['curriculum']!='materials' && $data['curriculum']!='textbook' && $data['curriculum']!='othermaterials'){
            $grades = $data['grades']=$this->standarddatamodel->getActiveGrades($lesson_week[0]['subject_id'],$lesson_week[0]['grade']);
           // echo $this->db->last_query();
            //print_r($grades);exit;
            $data['strands']=$this->standarddatamodel->getstrand($grades[0]['grade_id'],$grades[0]['standard_id']);
            } else {
                $data['grades'][0]['grade_id'] = $lesson_week[0]['grade'];
            } 
           // print_r($data['grades'][0]['grade_id']);exit;
            $data['custom_diff']=$this->custom_differentiatedmodel->getallplans();
//            print_r($data['custom_diff']);exit;
            $data['subject'] = $teacher_lesson_week_plan[0]['subject_name'];
            $data['subject_id'] = $teacher_lesson_week_plan[0]['subject_id'];
            $data['period'] = date('h:i A',strtotime($teacher_lesson_week_plan[0]['starttime'])).'-'.date('h:i A',strtotime($teacher_lesson_week_plan[0]['endtime']));
            $data['cdate'] = $this->input->post('cdate');
            $data['endtime'] = date('h:i A',strtotime($teacher_lesson_week_plan[0]['endtime']));
            $data['starttime'] = date('h:i A',strtotime($teacher_lesson_week_plan[0]['starttime']));
            $data['lesson_week_plan_id'] = $teacher_lesson_week_plan[0]['lesson_week_plan_id'];
            $data['period_id'] = $lesson_week[0]['period_id'];
            
            //This is to get material on based of subjects selected from step 1
            $school_id = $this->session->userdata('school_id');
            
            
            
//            echo $this->db->last_query();
//      print_r($data['materials']);exit;
            
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('planningmanager/create_plan_step2',$data);
        }
        
        
    }
    
        public function create_plan_step2(){
        $data['idname']='lesson';
        $data['standardeledesc']= $standardeledesc = $this->input->post('standardeledesc');
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/create_plan_step3',$data);
        
    }
    
        public function upload_plan(){
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/upload_plan',$data);
    }
    
        public function edit_plan(){
        $data['type']='Edit';
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
			if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
		}
		
        $this->load->view('planningmanager/create_plan',$data);
    }
    
        public function completed_plan(){
        
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        if($this->session->userdata('login_type')=='observer')
        {
                $this->load->Model('teachermodel');
                $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        } else if($this->session->userdata('login_type')=='user'){
            $this->load->Model('schoolmodel');
    $data['schools']=$this->schoolmodel->getallschools();
    
    if($data['schools']!=false)	
	   {
		$this->load->model('teachermodel');
	    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['schools'][0]['school_id']);
	   }
	   else
	   {
	     $data['teachers']=false;
	   }
        }
        $this->load->view('planningmanager/completed_plan',$data);
    }
    
        public function smart_goals(){
		if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/smart_goals',$data);
		} else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/cam_smart_goals',$data);
  } else if($this->session->userdata('login_type')=='user') {
	  $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/dist_smart_goals',$data);
}
	}
        
        public function create_review(){
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/cam_create_review',$data);
    }
        
        public function create_goal(){
        $data['idname']='lesson';
        $this->load->Model('dist_grademodel');
        $this->load->Model('goalplanmodel');
        $data['goalplans']=$this->goalplanmodel->getallplans();
		//print_r($data['goalplans']);exit;
        if($this->session->userdata('login_type')=='observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        }
//        print_r($data['teachers']);exit;
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/create_goal',$data);
    }
    
        public function getgradesubject(){
        $gradeid = $this->input->post('gradeid');
         $this->load->Model('grade_subjectmodel','gsmodel');
         $data["subjects"] = $this->gsmodel->getgrade_subjectall($gradeid);
         $data['view_path']=$this->config->item('view_path');
         $this->load->view('planningmanager/subjectbygrade',$data);
         
    }
    
    public function getgradesubjectsmart(){
        $gradeid = $this->input->post('gradeid');
         $this->load->Model('grade_subjectmodel','gsmodel');
         $data["subjects"] = $this->gsmodel->getgrade_subjectall($gradeid);
         $data['view_path']=$this->config->item('view_path');
         $this->load->view('planningmanager/subjectbygradesmart',$data);
         
    }
        
        public function edit_goal(){

		$this->load->Model('goalplanmodel');
		$data['smartgoal']=$this->goalplanmodel->getsmartgoals();
//        print_r($data['smartgoal']);exit;
//        echo $this->db->last_query();exit;
        $this->load->Model('dist_grademodel');
        $this->load->Model('goalplanmodel');
        if($this->session->userdata('login_type')=='observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        }
		
        $data['goalplans']=$this->goalplanmodel->getallplans();
//        print_r($data['goalplans']);exit;
	
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();
//        print_r($data);exit;
        
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/edit_goal',$data);
    }
	
        public function smart_goals_retrieve_report(){

		$this->load->Model('goalplanmodel');
		$data['smartgoal']=$this->goalplanmodel->getsmartgoals();
//        print_r($data['smartgoal']);exit;
//        echo $this->db->last_query();exit;
        $this->load->Model('dist_grademodel');
        $this->load->Model('goalplanmodel');
		
        $data['goalplans']=$this->goalplanmodel->getallplans();
	
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();
        
        
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/smart_goals_retrieve_report',$data);
    }	
    
        public function edit_goal2(){
        if($this->input->post('smartgoalname')){
            $this->load->Model('goalplanmodel');
            $data['smartgoal']=$this->goalplanmodel->getsmartgoals();
    //        print_r($data['smartgoal']);exit;
    //        echo $this->db->last_query();exit;
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('planningmanager/edit_goal',$data);
        } else 
        {
            $this->session->set_flashdata('error','Please select goal name!!');
            redirect(base_url().'planningmanager/edit_goal');
        }
    }
    
        function getsmargoaldetails(){
        $smartgoalname = $this->input->post('smartgoalname');
        $this->load->Model('goalplanmodel');
        $data['smartgoaldetails']=$this->goalplanmodel->getsmartgoalsbyid($smartgoalname);
		
		
//        echo $this->db->last_query();
        print_r(json_encode($data['smartgoaldetails']));exit;
        
    }
    
    function getsmargoaldetails_teacher(){
        $teacher_id = $this->input->post('teacher_id');
        $this->load->Model('goalplanmodel');
        $data['smartgoaldetails']=$this->goalplanmodel->getsmartgoalsbyteacherid($teacher_id);
		
		
//        echo $this->db->last_query();
        print_r(json_encode($data['smartgoaldetails']));exit;
        
    }
    
        public function savegoals(){
       // print_r($this->input->post('smartgoalname'));exit;
        if (!$this->input->post('smartgoalname')) 
        {
           
            $school_id = $this->session->userdata('school_id');
            $view_path="system/application/views/";
            require_once($view_path.'inc/html2pdf/html2pdf.class.php');
            include(MEDIA_PATH.'inc/foto_upload_script.php');
            $this->load->Model('goalplanmodel');
            $data['goalplans']=$this->goalplanmodel->getallplans();
    //        print_r($data['goalplans']);
            //print_r($_FILES['file_'.$data['goalplans'][0]['goal_plan_id']]['size']);exit;
            $goalname = $this->input->post('goalname');

            $goaldatearr = explode("-",$this->input->post('goaldate'));
            $goaldate = $goaldatearr[2].'-'.$goaldatearr[0].'-'.$goaldatearr[1];
            $grade = $this->input->post('grade');
            $subject = $this->input->post('subject');
            $name_id = $this->goalplanmodel->addsmartgoalname($goalname,$goaldate,$grade,$subject);
			
            foreach($data['goalplans'] as $goalplanval){

                if($_FILES['file_'.$goalplanval['goal_plan_id']]['size']>0)
                     {
                     $foto_upload = new Foto_upload;	

                     $json['size'] = '';

                    $json['img'] = '';
//                    echo BANK_TIME.WORKSHOP_FILES."goalmedialibrary/";exit;

                    $foto_upload->upload_dir = '/var/www/html/workshop_files/goalmedialibrary/';

                    $foto_upload->foto_folder = '/var/www/html/workshop_files/goalmedialibrary/';
                    $foto_upload->extensions = array(".jpg", ".gif", ".png",".pdf",".xls",".xlsx",".jpeg",".ppt",".pptx",".doc",".docx");
                    $foto_upload->language = "en";
                    $foto_upload->x_max_size = 4800;
                    $foto_upload->y_max_size = 3600;
                    $foto_upload->x_max_thumb_size = 1200;
                    $foto_upload->y_max_thumb_size = 1200;

                    $foto_upload->the_temp_file = $_FILES['file_'.$goalplanval['goal_plan_id']]['tmp_name'];

                    $foto_upload->the_file = $_FILES['file_'.$goalplanval['goal_plan_id']]['name'];
                    $foto_upload->http_error = $_FILES['file_'.$goalplanval['goal_plan_id']]['error'];
                    $foto_upload->rename_file = true; 

                    if ($foto_upload->upload()) {
                            //$foto_upload->process_image(false, true, true, 80);
                            $json['img'] = $foto_upload->file_copy;
                    } 

                    $json['error'] = strip_tags($foto_upload->show_error_string());
//                    echo json_encode($json);
                    }
                    else
                    {
                     $json['img']='NoFile';
                     $json['error']='';

                    }
//print_r($_SERVER);
//                    print_r($json);exit;
                    $goalplanid = $goalplanval['goal_plan_id'];
                    $comment = $this->input->post('plan_'.$goalplanval['goal_plan_id']);
                    $file_upload = $json['img'];
                    $result = $this->goalplanmodel->savesmartgoal($comment,$file_upload,$school_id,$name_id,$goalplanid);
                    
                //echo $this->input->post('plan_'.$goalplanval['goal_plan_id']);

            }
			
//echo "test";exit;
           $date = $data['date'] = $this->input->post('goaldate');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $goaldate = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
		
//        if ($result)
//	
//            $link = base_url() . 'planningmanager/savegoals_pdf/' . $subject . '/' . $grade . '/' . $goaldate . '/' . $name_id;
//
//        $this->session->set_flashdata('link', $link);
	   
         //  echo $result;exit;
            if($result)
                    {
					  $this->session->set_flashdata('message','Smart goal created successfully!!');
                        redirect(base_url().'planningmanager/create_goal');

                    } else {
                        $this->session->set_flashdata('error','Some error occur while creating smart goal!!');
                        redirect(base_url().'planningmanager/create_goal');
                    }
//            print_r($_REQUEST);exit;
//            print_r($this->input->post());
            exit;
        } else 
        {
            
            $school_id = $this->session->userdata('school_id');
            $view_path="system/application/views/";
            require_once($view_path.'inc/html2pdf/html2pdf.class.php');
            include(MEDIA_PATH.'inc/foto_upload_script.php');
            $this->load->Model('goalplanmodel');
            $data['goalplans']=$this->goalplanmodel->getallplans();
            
            //print_r($data['goalplans']);
            //print_r($_FILES['file_'.$data['goalplans'][0]['goal_plan_id']]['size']);exit;
            
            $smartgoalname = $this->input->post('smartgoalname');

            $goaldatearr = explode("-",$this->input->post('goaldate'));
            $goaldate = $goaldatearr[2].'-'.$goaldatearr[0].'-'.$goaldatearr[1];
            $grade = $this->input->post('grade');
            $subject = $this->input->post('subject');
            $name_id = $this->goalplanmodel->editsmartgoalname($smartgoalname,$goaldate,$grade,$subject);

            foreach($data['goalplans'] as $goalplanval){

                if($_FILES['file_'.$goalplanval['goal_plan_id']]['size']>0)
                     {
                     $foto_upload = new Foto_upload;	

                     $json['size'] = '';

                    $json['img'] = '';

                    $foto_upload->upload_dir = '/var/www/html/workshop_files/goalmedialibrary/';

                    $foto_upload->foto_folder = '/var/www/html/workshop_files/goalmedialibrary/';
                    $foto_upload->extensions = array(".jpg", ".gif", ".png",".pdf",".xls",".xlsx",".jpeg",".ppt",".pptx",".doc",".docx");
                    $foto_upload->language = "en";
                    $foto_upload->x_max_size = 4800;
                    $foto_upload->y_max_size = 3600;
                    $foto_upload->x_max_thumb_size = 1200;
                    $foto_upload->y_max_thumb_size = 1200;

                    $foto_upload->the_temp_file = $_FILES['file_'.$goalplanval['goal_plan_id']]['tmp_name'];

                    $foto_upload->the_file = $_FILES['file_'.$goalplanval['goal_plan_id']]['name'];
                    $foto_upload->http_error = $_FILES['file_'.$goalplanval['goal_plan_id']]['error'];
                    $foto_upload->rename_file = true; 

                    if ($foto_upload->upload()) {
                            //$foto_upload->process_image(false, true, true, 80);
                            $json['img'] = $foto_upload->file_copy;
                    } 

                    $json['error'] = strip_tags($foto_upload->show_error_string());
                    //echo json_encode($json);
                    }
                    else
                    {
                     $json['img']='NoFile';
                     $json['error']='';

                    }

                    //print_r($json);exit;
                    $goalplanid = $goalplanval['goal_plan_id'];
                    
                    $comment = $this->input->post('plan_'.$goalplanval['goal_plan_id']);
                    $file_upload = $json['img'];
                    $result = $this->goalplanmodel->updatesmartgoal($comment,$file_upload,$school_id,$name_id,$goalplanid);
//                    echo $this->db->last_query();exit;

                //echo $this->input->post('plan_'.$goalplanval['goal_plan_id']);

            }
//			  if ($result)
//	
//            $link = base_url() . 'planningmanager/savegoals_pdf/' . $subject . '/' . $grade . '/' . $goaldate . '/' . $name_id;
//
//        $this->session->set_flashdata('link', $link);
	   

         //   echo $result;exit;
            if($result)
                    {
                        $this->session->set_flashdata('message','Smart goal created successfully!!');
                        redirect(base_url().'planningmanager/create_goal');

                    } else {
                        $this->session->set_flashdata('error','Some error occur while creating smart goal!!');
                        redirect(base_url().'planningmanager/create_goal');
                    }
//            print_r($_REQUEST);exit;
//            print_r($this->input->post());
            exit;
        }
    }
    
        public function calendar(){
	 if($this->session->userdata('login_type')=='teacher') {			
        $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/calendar',$data);
	} else if($this->session->userdata('login_type')=='observer') {		
		 $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/cam_calendar',$data);
    }else if($this->session->userdata('login_type')=='user') {
		 $data['idname']='lesson';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/dist_calendar',$data);
				
	
}
}
    
        public function new_event(){
       $data['idname']='lesson';
		
				if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}	
		

		$this->load->Model('teachermodel');
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/new_event',$data);
    }
   
        public function retrieve_event(){
        $data['idname']='lesson';
		$this->load->Model('classroommodel');
		$data['event'] = $this->classroommodel->get_event_titel();
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/retrieve_event',$data);
    }
    
    function update_plan()
    {
       error_reporting(1);
        $this->load->Model('lessonplanmodel');
       $this->load->Model('custom_differentiatedmodel');
        $jstatus=0;

        $data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
                
                
        if($this->input->post("grade")=='')
        {
//          echo 'here';
            $jstatus=1;
        
        
        }
        foreach($data['lessonplansub'] as $val)
        {
          $l=$val['lesson_plan_id'];

          if($this->input->post("$l")=='' && $l!=7)
          {      
//                      echo 'here';
            $jstatus=1;
          
          }
        
        }
        //echo 'check123';exit;

//      $custom_diff=$this->custom_differentiatedmodel->getallplans();
//                print_r($custom_diff);exit;
                
//      if($custom_diff!=false)
//      {
//          foreach($custom_diff as $val)
//      {
//        $c=$val['custom_differentiated_id'];
//        if($this->input->post("c_".$c)=='')
//        {         echo 'here';
//          $jstatus=1;
//        
//        }
//      
//      }
//      echo 'here';
//      
//      }
        
        if($this->input->post("standard_id")!='')
        {
            
            if($this->input->post("grade")=='')
        {
            
            $jstatus=1;
        
        
        }
        if($this->input->post("strand")=='' && $this->input->post('selectedmaterial')=='')
        {
            
            //$jstatus=1;
//      echo 'test';exit;
        
        }
        if($this->input->post("standarddata")=='' && $this->input->post('selectedmaterial')=='')
        {
            
            //$jstatus=1;
        
        
        }   
        
        }
        else if($this->input->post("standard")=='' )
        {
        
        
        //$jstatus=1;
        
        }
                //removed condition $this->input->post("diff_instruction")=='' ||
        if( $this->input->post("subject_id")=='' || $this->input->post("period_id")=='')
        {
            
            
            
            $jstatus=1;
    //  echo 'test';exit;
        
        }
             //   echo $jstatus;exit;
//                 print_r($custom_diff);exit;
        if($jstatus==0)
        {
    
        $pstatus=$this->lessonplanmodel->check_plan_update_exists();

               // print_r($pstatus);exit;
    if($pstatus==1)
         {
        $status=$this->lessonplanmodel->update_plan_teacher();
             //   echo $status;exit;
        if($status!=0){
               $data['message']="plan update Sucessfully" ;
               $data['status']=1 ;
               $date = $data['date']=$this->input->post('date') ;
                           $datearr = explode('-',$date);
                           $date = $datearr[2].'-'.$datearr[0].'-'.$datearr[1];
                           
                           if($this->input->post('btnaction')=='print')
                           {
                            if($this->input->post('teacher_id'))
                                $link = base_url().'teacherplan/createlessonplanpdf/'.$this->input->post('teacher_id').'/'.$this->input->post('subject_id').'/'.$date;
                            else
                                $link = base_url().'teacherplan/createlessonplanpdf/'.$this->session->userdata('teacher_id').'/'.$this->input->post('subject_id').'/'.$date;
                            
                            $this->session->set_flashdata('link',$link);
                           } else 
                           {
//                              if($this->input->post('teacher_id'))
//                                  $link = base_url().'teacherplan/printdayview/'.$data['date'].'/'.$this->input->post('teacher_id');
//                             else
//                                 $link = base_url().'teacherplan/printdayview/'.$data['date'];
                               
                               $this->session->set_flashdata('link',$link);
                           }
                           $this->session->set_flashdata('message','plan update Sucessfully');
                           if(strtolower($this->input->post('plan_type'))=='create')
                                redirect(base_url().'planningmanager/create_plan');
                           else 
                               redirect(base_url().'planningmanager/edit_plan');
                }
                else
                {
                  $data['message']="Contact Technical Support Update Failed" ;
                  $data['status']=0 ;
                                  $this->session->set_flashdata('error','Contact Technical Support Update Failed');
                                 redirect(base_url().'planningmanager/create_plan');
                }
        }
        else if($pstatus==0)
        {
            $data['message']="Scheduling Conflict" ;
                        $data['status']=0 ;
                        $this->session->set_flashdata('error','Scheduling Conflict');
                        redirect(base_url().'planningmanager/create_plan');
        }
        else if($pstatus==2)
        {
            $data['message']="End Time Can not be Greater/Equal Than Start Time" ;
                        $data['status']=0 ;
                        $this->session->set_flashdata('error','End Time Can not be Greater/Equal Than Start Time');
                        redirect(base_url().'planningmanager/create_plan');
        }
        else if($pstatus==3)
        {
            $data['message']="Data Already Exists. " ;
                        $data['status']=0 ;
                         $this->session->set_flashdata('error','Data Already Exists.');
                        redirect(base_url().'planningmanager/create_plan');
        }
        }
        
    
    
    
    
    }
        
        function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		$dates[$i]['day'] = $i;
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
			
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}
        
        function completelessonplanreport(){
//    echo $this->session->userdata('teacher_id');exit;
     $this->load->Model('lessonplanmodel');           
    $cdataarr = explode('-',$this->input->post('cdate'));
    $cdate = $cdataarr[2].'-'.$cdataarr[0].'-'.$cdataarr[1];    //date("Y-m-d",strtotime($this->input->post('cdate')));
    $chkdate = $cdataarr[1].'-'.$cdataarr[0].'-'.$cdataarr[2];
    $ctimearr = explode(' ',$this->input->post('ctime'));
    if ($ctimearr[1]=='AM'){
        $ctime = date('h:i:s',strtotime($ctimearr[0]));
    } else {
     $ctime = date('H:i:s',strtotime($ctimearr[0])+43200);
    }
    $teacher_lesson_week_plan = $this->lessonplanmodel->lessonplansbyteacherbytimetable(false,$cdate,$cdate,$ctime);
    if($teacher_lesson_week_plan!=false){
    $teacher_id = $teacher_lesson_week_plan[0]['teacher_id'];
    $subject = $teacher_lesson_week_plan[0]['subject_id'];
    $date = $cdate;
    
    $getlessonplans=$this->lessonplanmodel->getlessonplansbysubjectandteacher($teacher_id,$date,$date,$subject);
//    print_r($getlessonplans);exit;
    if($getlessonplans!=False)
    {
        $data['teacher_id'] = $teacher_id;
        $data['subject'] = $subject;
        $data['date'] = $date;
    } else 
    {
        $data['err'] = True;
        $data['msg'] = "No lesson plan has been created for mentioned date & time";
    }
    } else {
        $data['err'] = True;
        $data['msg'] = "No lesson plan has been created for mentioned date & time";
    }
    echo json_encode($data);
}

function createlessonplanpdf($teacher_id,$subject,$date)
{
    $data['date'] = $date;
    $this->load->Model('lessonplanmodel');
    $this->load->Model('dist_grademodel');
    $this->load->Model('teachermodel');
    $data['lessonplans'] = $lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
    $lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
    $data['getlessonplans'] = $getlessonplans=$this->lessonplanmodel->getlessonplansbysubjectandteacher($teacher_id,$date,$date,$subject);
    $data['gradename'] = $gradename=$this->dist_grademodel->getdist_gradeById($getlessonplans[0]['grade']);
    $data['teachername'] = $teachername=$this->teachermodel->getteacherById($teacher_id);
    $day = date('l', strtotime($date));
    if($day=='Monday') {
        $dayn=0;
    }
    if($day=='Tuesday') {
        $dayn=1;
    }
    if($day=='Wednesday') {
        $dayn=2;
    }
    if($day=='Thursday') {
        $dayn=3;
    }
    if($day=='Friday') {
        $dayn=4;
    }
    if($day=='Saturday') {
        $dayn=5;
    }
    if($day=='Sunday') {
        $dayn=6;
    }
    $this->load->Model('studentmodel');
    $data['total_students'] = $total_students = $this->studentmodel->getstudentCount($dayn,$getlessonplans[0]['period_id'],$teacher_id);
    $le=$getlessonplans[0]['lesson_week_plan_id'];
    $start1=explode(':',$getlessonplans[0]['starttime']);
    $end1=explode(':',$getlessonplans[0]['endtime']);
    if($start1[0]>=12) {
        if($start1[0]==12) {
            $start2=($start1[0]).':'.$start1[1].' pm';
        } else {
            $start2=($start1[0]-12).':'.$start1[1].' pm';
        } 
        
    } else if($start1[0]==0) {
        $start2=($start1[0]+12).':'.$start1[1].' am';
    } else {
        $start2=($start1[0]).':'.$start1[1].' am';
    }
    if($end1[0]>=12) {
        if($end1[0]==12) {
            $end2=($end1[0]).':'.$end1[1].' pm';
        } else {
            $end2=($end1[0]-12).':'.$end1[1].' pm';
        } 
        
    } else if($end1[0]==0) {
        $end2=($end1[0]+12).':'.$end1[1].' am';
    } else {
        $end2=($end1[0]).':'.$end1[1].' am';
    }
    $data['start2'] = $start2;
    $data['end2'] = $end2;
    $sh=0;
    $k=0;
    $view_path=$this->config->item('view_path');            
    $this->load->Model('curriculummodel');
    $data['curriculum'] = $this->curriculummodel->checkcurriculum();
    $this->load->Model('custom_differentiatedmodel');       
    $observationplan_ans=$this->custom_differentiatedmodel->getcustomdiff($le);
    $data['status']=0;
    if($observationplan_ans!=false) {
        $observationplan=$this->custom_differentiatedmodel->getallplans();
        if($observationplan!=false) {
            if($observationplan_ans!=false) {
                foreach($observationplan as $key=>$plan) {
                    foreach($observationplan_ans as $ans) {
                        if($plan['custom_differentiated_id']==$ans['custom_differentiated_id']  ) {
                            $observationplan[$key]['answer']=$ans['answer'];
                        }
                    }
                }   
            }
        }
        $data['observationplan'] = $observationplan;
    }
        
    $this->load->Model('report_disclaimermodel');
    $reportdis = $this->report_disclaimermodel->getallplans(11);
    $dis = '';
    $fontsize = 0;
    $fontcolor = 'cccccc';
    if ($reportdis != false) {
        $dis = $reportdis[0]['tab'];
        $data['fontsize'] = $reportdis[0]['size'];
        $data['fontcolor'] = $reportdis[0]['color'];
    }
    $data['dis'] = utf8_decode($dis);
    $this->output->enable_profiler(false);
    $this->load->library('parser');
    $str = $this->parser->parse('planningmanager/lesson_plan_pdf', $data, TRUE);
    $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
    $content = ob_get_clean();
    $html2pdf->WriteHTML($str);
    $html2pdf->Output();
}

        public function dist_retrieve_goal(){
//    echo 'test'.$this->session->userdata('login_type');exit;
            if($this->session->userdata('login_type')==''){
                redirect(base_url());
            }
	
    $data['idname']='lesson';
    $this->load->Model('teachermodel');
    if($this->session->userdata('login_type')=='user'){
    	$this->load->Model('schoolmodel');
    	$data['schools']=$this->schoolmodel->getallschools();
    
    if($data['schools']!=false)	
	   {
	$data['teachers']=$this->teachermodel->getTeachersBySchool($data['schools'][0]['school_id']);
	   }
	   else
	   {
	     $data['teachers']=false;
	   }
    } else if($this->session->userdata('login_type')=='observer'){
        $data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
    } else if($this->session->userdata('login_type')=='teacher'){
		 $this->load->model('goalplanmodel');
		 $status = $this->goalplanmodel->getcommentssasigned(false, false,$this->session->userdata('school_id'),$this->session->userdata('teacher_id'));
         $data['goals'] = $status;
	}
        $data['smartgoal']=$this->goalplanmodel->getsmartgoals();
//    print_r($data);exit;
    $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/dist_retrieve_goal',$data);
}

function get_smartgoals_year(){
    $this->load->model('goalplanmodel');
     $data['smartgoal']=$this->goalplanmodel->getsmartgoalsarray();
//     $subject_id,$grade_id,$teacher_id,$school_id,$date,$id
//     print_r($data['smartgoal']);exit;
     $str='<table class="table table-striped table-bordered" id="editable-sample">
                                            <thead>
                                            <tr>
                                                
                                                <th class="sorting">Goal Name</th>
                                                <th class="sorting">Subject</th>
                                                <th class="sorting">Subject</th>
                                                <th class="sorting">Date</th>
                                                <th class="no-sorting">Review Plan</th>
                                               
                                            </tr>
                                            </thead>
                                            <tbody>';
                                            foreach($data['smartgoal'] as $goal){
                                                 $str.='<tr class="odd gradeX">                                
                                                <td>'.$goal['smart_goal_name'].'</td>
                                                <td>'.$goal['subject_name'].'</td>
                                                <td>'.$goal['grade_name'].'</td>    
                                                <td class="hidden-phone">'.$goal['goal_date'].'</td>
                                                <td class="hidden-phone"><a href="javascript:;" role="button" class="btn btn-primary" data-toggle="modal" onclick="commentsviews('.$goal['user_id'].','.$goal['grade_id'].','.$goal['subject_id'].','.$goal['school_id'].',\''.$goal['goal_date'].'\')">View</a>
                                                
                                                </td>
                                                
                                                </tr>';
                                                
                                            }
                                            $str.='</tbody>
                                        </table>';
     echo $str;exit;
}

        public function dist_retrieve_goal2(){
        $this->load->model('goalplanmodel');
        if($this->input->post('school_id') && $this->input->post('school_id')!=''){
             $school_id = $this->input->post('school_id');
        } else {
           $school_id = $this->session->userdata('school_id');
        }

			$teacher_id = $this->input->post('teacher_id');	
		

        $status = $this->goalplanmodel->getcommentssasigned(false, false,$school_id,$teacher_id);
        $data['goals'] = $status;

        $data['view_path']=$this->config->item('view_path');    
        $this->load->view('planningmanager/dist_retrieve_goal_ajax',$data);
}


        public function dist_retrieve_goal3(){
//            $subject_id,$grade_id,$teacher_id,$school_id,$date,$id
    if($this->input->post('year'))
    {
        $year=$this->input->post('year');
    }
    else
    {
        $year=date('Y');
    }
    $teacher_id = $this->input->post('teacher_id');
    $data['year']=$year;
            $this->load->Model('teachermodel');
            $this->load->Model('goalplanmodel');
//            $data['teacher']=$this->teachermodel->getteacherById($teacher_id);
//            $data['observer_desc']=$this->goalplanmodel->getallplansdescbyteacher($year,$teacher_id);
//            if($data['teacher']!=false)
//            {
//              $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
//
//
//            }
//            else
//            { 
//             $data['teacher_name']='';
//
//            }
            $this->load->Model('goalplanmodel');
            $this->load->Model('schoolmodel');
//            $data['school']=$this->schoolmodel->getschoolById($data['teacher'][0]['school_id']);
            if($this->session->userdata("login_type")=='admin')
            {

             $this->session->set_userdata('district_goal_tabs',$data['school'][0]['district_id']);  
            }
            $data['goalplans']=$this->goalplanmodel->getallplans();
            if ($this->input->post('teacher_id')){
                $teacher_id = $this->session->userdata('teacher_id');
            } else {
                $teacher_id = $this->input->post('teacher_id');
            }
            $grade_id = $this->input->post('grade_id');
            $subject_id = $this->input->post('subject_id');
            $school_id = $this->input->post('school_id');
            $date = $this->input->post('goal_date');
            
            $data['smartgoals'] = $this->goalplanmodel->get_details_by_subject($subject_id,$grade_id,$teacher_id,$school_id,$date);
//            echo $this->db->last_query();
//             print_r($data['smartgoals']);exit;
            $this->load->model('teachermodel');
             $data['teacher'] =  $this->teachermodel->getteacherById($teacher_id);
//             print_r($data);exit;

//            $data['teacherplans']=$this->goalplanmodel->getteacherplanid($data['goalplans'][0]['goal_plan_id'],$teacher_id,$year);
//            if($data['teacherplans']!=false)
//            {
//
//            $data['comments']=$this->goalplanmodel->getcommentsbyplanid($data['teacherplans'][0]['teacher_plan_id']);
//            }
//            else
//            {
//                    $data['comments']=false;
//            }
//            print_r($data);exit;
            $data['teacher_id']=$teacher_id;
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('planningmanager/dist_retriev_goal_view',$data);
            
//            print_r($data);exit;
                
}

        public function lesson_plan_graph(){
		$data['idname']='lesson';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		if($this->input->post('submit'))
		{	
			$data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($this->input->post('school_type'));
			$data['school_type'] = $this->input->post('school_type');			
		}
		else
		{
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
		}		
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
			$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		
		}
		$this->load->view('planningmanager/lesson_plan_graph',$data);
	
	
	}

        function savegoals_pdf($subject, $grade, $goaldate, $name_id) {
        error_reporting(1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('teachermodel');
		$this->load->Model('goalplanmodel');

        $data['parent_conference'] = $this->goalplanmodel->savegoals_pdf($subject, $grade, $goaldate, $name_id);
	//	print_r($data['parent_conference']);exit;

          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
		
		  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }

        $data['view_path'] = $this->config->item('view_path');
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('planningmanager/savegoals_pdf', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }
	
	public function savegoals_pdf_retriev($user_id) {
        error_reporting(1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('teachermodel');
		$this->load->Model('goalplanmodel');

        $data['parent_conference'] = $this->goalplanmodel->savegoals_pdf($user_id);
	//	print_r($data['parent_conference']);exit;

          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
		
		  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }

        $data['view_path'] = $this->config->item('view_path');
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('planningmanager/savegoals_pdf', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }
        
        function getmedia($page,$school,$teacher){
	   $this->load->Model('goalplanmodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_goalmedia_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_goalmedia_school_id',$school);
		
		}
		
		
		$per_page = 12;
	   $total_records = $this->goalplanmodel->getgoalmediaCount();
		
			$status = $this->goalplanmodel->getgoalmedia($page, $per_page);
//                        echo $this->db->last_query();exit;
//                        print_r($status);exit;
		
		print("<input type='hidden' name='selectedteacher' id='selectedteacher' value='$teacher'>");
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Media</div><table class='tabcontent' cellspacing='0' border='0' cellpadding='0' id='gallery' style='width:1060px;' >";
					
		
		
			$i=0;
			foreach($status as $key=>$val){
//					print_r($val);exit;
                                        
					$school_name=$val['school_name'];
					$username=$val['username'];
					$type=$val['type'];					
					$description=substr($val['description'],0,100);
					
					$text='<br /><b>School:'.$school_name.'<br />    Name:'.$username.'(Type:'.$type.')</b>';
					
					
					if($i%3==0 || $i==0)
					{
					 print '<tr>';
	
					}
				
			    $siteurl=SITEURLM;
				if($val['file_path']=='NoFile')
				{
				}
				else
				{
				$filename=explode('.',$val['file_path']);
				$file=$filename[1];
				}
				
			if($val['file_path']=='NoFile')
				{

				}
				else	if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmedialibrary/'.$val['file_path'].' height="75px" width="100px">';
			}
			else
			{
			  if (file_exists(WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg')) {
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmediathumb/'.$filename[0].'.jpeg >';
				} else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'goalmedialibrary/'.$val['file_path'].'[0] '.WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					else
					{
					 $commandtext='sudo -u unoconv /usr/bin/unoconv -f pdf -o '.WORKSHOP_FILES.'goalmediagenerated '.WORKSHOP_FILES.'goalmedialibrary/'.$val['file_path'];
					 exec($commandtext);
					 
					  $commandtexta='convert -thumbnail x100 '.WORKSHOP_FILES.'goalmediagenerated/'.$filename[0].'.pdf[0] '.WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg ';
					exec($commandtexta);
					 
					}
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmediathumb/'.$filename[0].'.jpeg >';
				}
			  
			  
			
			}
			if($val['file_path']=='NoFile')
				{
					$newvar=$text;
				}
				else
				{
			$file_path=WORKSHOP_DISPLAY_FILES.'goalmedialibrary/'.$val['file_path'];
            $newvar='<a style="color:#ffffff;" target="_blank" href='.$file_path.'>'.$fileas.'</a>'.$text;
			}
			
				print '<td style="width: 30%; height: 100px;padding-right:25px;">'.$newvar.'<br /><b>Description:</b>'.substr($description,0, 40).'</td>';
				
				
				$i++;
				if($i%3==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:1060px;' ><tr ><td>No  Media Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
		}
	
	}
        
          function getmediasmart($page,$school,$teacher){
	   $this->load->Model('goalplanmodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_goalmedia_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_goalmedia_school_id',$school);
		
		}
		
		
		$per_page = 12;
	   $total_records = $this->goalplanmodel->getgoalmediaCountSmart();
		
			$status = $this->goalplanmodel->getgoalmediaSmart($page, $per_page);
//                        echo $this->db->last_query();exit;
//                        print_r($status);exit;
		
		print("<input type='hidden' name='selectedteacher' id='selectedteacher' value='$teacher'>");
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Media</div><table class='tabcontent' cellspacing='0' border='0' cellpadding='0' id='gallery' style='width:1060px;' >";
					
		
		
			$i=0;
			foreach($status as $key=>$val){
//					print_r($val);exit;
                                        
					$school_name=$val['school_name'];
					$username=$val['username'];
					$type=$val['type'];					
					$description=substr($val['description'],0,100);
					
					$text='<br /><b>School:'.$school_name.'<br />    Name:'.$username.'(Type:'.$type.')</b>';
					
					
					if($i%3==0 || $i==0)
					{
					 print '<tr>';
	
					}
				
			    $siteurl=SITEURLM;
				if($val['file_path']=='NoFile')
				{
				}
				else
				{
				$filename=explode('.',$val['file_path']);
				$file=$filename[1];
				}
				
			if($val['file_path']=='NoFile')
				{
                                    $fileas= 'No File';
				}
				else	if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmedialibrary/'.$val['file_path'].' height="75px" width="100px">';
			}
			else
			{
			  if (file_exists(WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg')) {
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmediathumb/'.$filename[0].'.jpeg >';
				} else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'goalmedialibrary/'.$val['file_path'].'[0] '.WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					else
					{
					 $commandtext='sudo -u unoconv /usr/bin/unoconv -f pdf -o '.WORKSHOP_FILES.'goalmediagenerated '.WORKSHOP_FILES.'goalmedialibrary/'.$val['file_path'];
					 exec($commandtext);
					 
					  $commandtexta='convert -thumbnail x100 '.WORKSHOP_FILES.'goalmediagenerated/'.$filename[0].'.pdf[0] '.WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg ';
					exec($commandtexta);
					 
					}
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmediathumb/'.$filename[0].'.jpeg >';
				}
			  
			  
			
			}
			if($val['file_path']=='NoFile')
				{
					$newvar='<b>No File</b>'.$text;
				}
				else
				{
			$file_path=WORKSHOP_DISPLAY_FILES.'goalmedialibrary/'.$val['file_path'];
            $newvar='<a style="color:#ffffff;" target="_blank" href='.$file_path.'>'.$fileas.'</a>'.$text;
			}
			
				print '<td style="width: 30%; height: 100px;padding-right:25px;">'.$newvar.'<br /><b>Description:</b>'.substr($description,0, 40).'</td>';
				
				
				$i++;
				if($i%3==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:1060px;' ><tr ><td>No  Media Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
		}
	
	}

        function do_pagination($count,$per_page,$cur_page,$paginationdetails){
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
        
        function dist_smart_goals_retrieve(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('planningmanager/dist_smart_goals_retrieve',$data);
        }
        
        function pdf_smart_goal(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('planningmanager/pdf_smart_goal',$data);    
        }
        
        function smart_goals_retrieve_subject(){
             if($this->session->userdata('login_type')==''){
                redirect(base_url());
            }
	
            $data['idname']='lesson';
            $this->load->Model('teachermodel');
            if($this->session->userdata('login_type')=='user'){
            $this->load->Model('schoolmodel');
            $data['schools']=$this->schoolmodel->getallschools();

            if($data['schools']!=false)	
                   {

                    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['schools'][0]['school_id']);
                   }
                   else
                   {
                     $data['teachers']=false;
                   }
            } else if($this->session->userdata('login_type')=='observer'){
                $data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            }
            
             $this->load->Model('dist_grademodel');
            $this->load->Model('goalplanmodel');
            $data['goalplans']=$this->goalplanmodel->getallplans();
                    //print_r($data['goalplans']);exit;
        
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();

            $data['view_path']=$this->config->item('view_path');
                $this->load->view('planningmanager/dist_smart_goals_retrieve_subject',$data);
        }
        
        function smart_goals_retrieve_year(){
            
             if($this->session->userdata('login_type')==''){
                redirect(base_url());
            }
	
            $data['idname']='lesson';
            $this->load->Model('teachermodel');
            if($this->session->userdata('login_type')=='user'){
            $this->load->Model('schoolmodel');
            $data['schools']=$this->schoolmodel->getallschools();

            if($data['schools']!=false)	
                   {

                    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['schools'][0]['school_id']);
                   }
                   else
                   {
                     $data['teachers']=false;
                   }
            } else if($this->session->userdata('login_type')=='observer'){
                $data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            }
            
             $this->load->Model('dist_grademodel');
            $this->load->Model('goalplanmodel');
            $data['goalplans']=$this->goalplanmodel->getallplans();
                    //print_r($data['goalplans']);exit;
        
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();

            $data['view_path']=$this->config->item('view_path');
                $this->load->view('planningmanager/dist_smart_goals_retrieve_year',$data);
        }
        
    function generate_pdf_goal($subject_id,$grade_id,$teacher_id,$school_id,$date,$id){
        $this->load->model('goalplanmodel');
        $data['smartgoals'] = $this->goalplanmodel->get_details_by_subject($subject_id,$grade_id,$teacher_id,$school_id,$date,$id);
        $this->load->model('teachermodel');
        $data['teacher'] =  $this->teachermodel->getteacherById($data['smartgoals'][$id]['teacher_id']);
        $this->load->model('schoolmodel');
        $data['subject'] =  $this->schoolmodel->getsubjects_by_id($data['smartgoals'][$id]['subject_id']);
        $data['grade'] =  $this->schoolmodel->getGradeById($data['smartgoals'][$id]['grade_id']);
        $data['row_id'] = $id;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(2);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['dis'] = utf8_decode($dis);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('planningmanager/generate_pdf_goal', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }
        
        function generate_pdf_goal_list(){
             error_reporting(1);
             $subject_id = $this->input->post('subject_id');
             $grade_id = $this->input->post('grade_id');
             $teacher_id = $this->input->post('teacher_id');
             $school_id = $this->input->post('school_id');
             $date = $this->input->post('year');
             
             $this->load->model('goalplanmodel');
             $data['smartgoals'] = $this->goalplanmodel->get_details_by_subject($subject_id,$grade_id,$teacher_id,$school_id,$date);
//             print_r($data['smartgoals']);exit;
             $data['type']='goal_by_subject';

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('planningmanager/smart_goal_pdf_list',$data);
        
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
//        $this->output->enable_profiler(false);
//        $this->load->library('parser');
//        echo $str = $this->parser->parse('planningmanager/generate_pdf_goal', $data, TRUE);
//
//
//        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
//        $content = ob_get_clean();
//        $html2pdf->WriteHTML($str);
//        $html2pdf->Output();
        }
        
    function generate_pdf_goal_year($teacher_id,$school_id,$date,$id){
        error_reporting(1);
        $this->load->model('goalplanmodel');
        $data['smartgoals'] = $this->goalplanmodel->get_details_by_subject('','',$teacher_id,$school_id,$date,$id);
        $this->load->model('teachermodel');
        $data['teacher'] =  $this->teachermodel->getteacherById($data['smartgoals'][$id]['teacher_id']);
        $this->load->model('schoolmodel');
        $data['subject'] =  $this->schoolmodel->getsubjects_by_id($data['smartgoals'][$id]['subject_id']);
        $data['grade'] =  $this->schoolmodel->getGradeById($data['smartgoals'][$id]['grade_id']);
        $data['row_id'] = $id;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(12);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {
            $dis = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['dis'] = $dis;
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('planningmanager/generate_pdf_goal', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }        
        function generate_list_goal_year(){
             error_reporting(1);
             $this->load->model('goalplanmodel');
             $teacher_id = $this->input->post('teacher_id');
             $school_id = $this->input->post('school_id');
             $date = $this->input->post('year');
             $data['smartgoals'] = $this->goalplanmodel->get_details_by_subject('','',$teacher_id,$school_id,$date);
//             print_r($data['smartgoals']);exit;
             $data['type']='goal_by_year';

//             print_r($data);exit;
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('planningmanager/smart_goal_pdf_list',$data);
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
//        $this->output->enable_profiler(false);
//        $this->load->library('parser');
//        echo $str = $this->parser->parse('planningmanager/generate_pdf_goal', $data, TRUE);
//
//
//        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
//        $content = ob_get_clean();
//        $html2pdf->WriteHTML($str);
//        $html2pdf->Output();
        }
        
        function event_calender_ajax_list(){
		$this->load->Model('classroommodel');		
		
		$data['update_report'] = $this->classroommodel->event_list();
		
		
//		print_r($data);exit;

		echo json_encode($data);
	}
                
        public function create_new_event() {
           $data['idname']='lesson';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
            $titel = $this->input->post('titel');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $repeat_event = $this->input->post('repeat_event');
            $show_as = $this->input->post('show_as');
           
  if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			
if($this->session->userdata('login_type')=='teacher'){
                $teacher_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $teacher_id = $this->input->post('teacher_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $teacher_id = $this->input->post('teacher_id');
			}
			
					
if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
			


            $data = array('user_id' =>$user_id,'teacher_id'=>$teacher_id, 'school_id' =>$school_id, 'district_id' => $this->session->userdata('district_id'), 'login_type' => $this->session->userdata('login_type'), 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'titel' => $titel, 'start_time' => $start_time, 'end_time' =>$end_time,'repeat_event' => $repeat_event, 'show_as' => $show_as);

            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('create_event', $data);
        }
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'planningmanager/calendar');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }   

        public function edit_event_data() {
           $data['idname']='lesson';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
			$event_id = $this->input->post('event_id');
			$titel = $this->input->post('titel');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $repeat_event = $this->input->post('repeat_event');
            $show_as = $this->input->post('show_as');
           
  if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}

if($this->session->userdata('login_type')=='teacher'){
                $teacher_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $teacher_id = $this->input->post('teacher_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $teacher_id = $this->input->post('teacher_id');
			}


            $data = array('user_id' =>$user_id,'teacher_id'=>$teacher_id,'event_id'=>$event_id, 'school_id' => $this->session->userdata('school_id'),'district_id' => $this->session->userdata('district_id'), 'login_type' => $this->session->userdata('login_type'),'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))),'titel' => $titel, 'start_time' => $start_time, 'end_time' =>$end_time,'repeat_event' => $repeat_event, 'show_as' => $show_as);


            $this->load->model('classroommodel');
            $result = $this->classroommodel->edit_date_event('create_event',$data,$data['event_id']);
        }
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'planningmanager/calendar');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }  

        public function retrieve_event_list() {
	
        $data['idname'] = 'lesson';
        if ($this->input->post('submit')) {
		    $date = $this->input->post('date');
           // $titel = $this->input->post('titel');
		
		if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
$this->session->set_userdata('submitbutton', $this->input->post('submit'));
			
			echo $date = $data['date'] = $this->input->post('date');
            $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
            $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
//            print_r($date);exit;
            $this->retrieve_event_data($date,$school_id);

//			$link = base_url().'classroom/retrieve_success_data/'.$grade_id.'/'.$student_id.'/'.$date.'';
            //redirect(base_url().'classroom/retrieve_success_report');	
        }
    }
	
        public function retrieve_event_data($date,$school_id) {
        $data['idname'] = 'lesson';


        error_reporting(1);
		
	/*	 if($this->session->userdata("login_type")=='user')
		  	{
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
			
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
								
	}*/

        $this->load->Model('classroommodel');
        $data['retrieve_reports'] = $this->classroommodel->get_event_data($date,$school_id);

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('planningmanager/retrieve_event', $data);
    }
	
        function event_pdf($date,$school_id,$event_id) {
        error_reporting(1);
        $this->load->Model('classroommodel');

        $data['event_data'] = $this->classroommodel->event_pdf($date,$school_id,$event_id);

      if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
		
		  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
   
   
        $data['view_path'] = $this->config->item('view_path');
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('planningmanager/event_pdf', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }	
        
        public function edit_event($event_id){
        $data['idname']='lesson';
		
		$this->load->Model('classroommodel');
		  $data['update_report'] = $this->classroommodel->event_pdf($event_id);
		 // print_r($data['update_report']);exit;
		$data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/edit_event',$data);
    }

        public function update_event(){
        $data['idname']='lesson';
		$this->load->Model('classroommodel');
		   $data['update_report'] = $this->classroommodel->event_list();
		   
		   
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/update_event',$data);
    }

     

        public function event($event_id){
        $data['idname']='lesson';
 		$this->load->Model('classroommodel');
		$data['event_data'] = $this->classroommodel->event_pdf($event_id);		

        $data['view_path']=$this->config->item('view_path');
        $this->load->view('planningmanager/event',$data);
    }
    
        /* start Lesson Plan Library */

        function lesson_Plan_library (){
	 $data['type'] = $this->input->post('plan_type');
             $data['idname']='lesson';

        $data['type']='Create';
    $this->load->Model('lessonplanmodel');     
	$this->load->Model('custom_differentiatedmodel');     
        $data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
		}
	if($this->session->userdata('login_type')=='user')
	  	{
	$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
		}
		
	  $lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
            
            $lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
           
             foreach($lessonplans as $lessonplansval){
             foreach($lessonplansub as $lessonplanssubval){
               $data['lessontype'][$lessonplansval['tab']]["lesson_plan_id"] = $lessonplansval['lesson_plan_id']; 
                    //if($lessonplansval)
                    if($lessonplansval['lesson_plan_id']==$lessonplanssubval['lesson_plan_id']){
                        $data['lessontype'][$lessonplansval['tab']][$lessonplanssubval['lesson_plan_sub_id']] =  $lessonplanssubval['subtab'];
                    }
                }
			 }
		  $data['custom_diff']=$this->custom_differentiatedmodel->getallplans();		
		
		$this->load->Model('iep_tracker_model');
		$data['subjects'] = $this->iep_tracker_model->get_subject_in_standard();

		 $this->load->Model('schoolmodel');
		 $data['grades'] = $this->schoolmodel->getallgrades();

	
		$data['id']= $iep_id;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('planningmanager/lesson_Plan_library',$data);
	   }

        function get_grade_by_strand(){
		$this->load->Model('iep_tracker_model');
		$this->load->Model('standarddatamodel');
		$get_all_standard = $this->iep_tracker_model->get_grade_By_standards($this->input->post("grade_id"));
	

	$grades = $data['grades']=$this->standarddatamodel->getActiveGrades($get_all_standard[0]['subject_id'],$get_all_standard[0]['grade']);

	$data['strands']=$this->standarddatamodel->getstrand($grades[0]['grade_id'],$grades[0]['standard_id']);

		echo json_encode($data);
	}

        public function lesson_Plan_library_insert() {

        //print_r($this->input->post('action_home_responsible'));exit;
        $data['idname'] = 'lesson';

        if ($this->input->post('submit')) {
			
			$lesson_title = $this->input->post('lesson_title');
			$grade_id = $this->input->post('grade_id');
			$standard_id = $this->input->post('standard_id');
            $standarddata_id = $this->input->post('standarddata_id');
			
            $strand = $this->input->post('strand');
			$standarddata = $this->input->post('standarddata');
          	$standarddisplay = $this->input->post('standarddisplay');
       	    $diff_instruction = $this->input->post('diff_instruction');
       
          // $meeting = implode(",", $meeting_type);
			
            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            } else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}

		
 
            $data = array('user_type' => $this->session->userdata('login_type'),'user_id' => $user_id,'lesson_title'=>$lesson_title,'district_id' => $this->session->userdata('district_id'),'grade_id'=>$grade_id,'standard_id'=>$standard_id,'standarddata_id'=>$standarddata_id,'strand'=>$strand,'standarddata'=>$standarddata,'standarddisplay'=>$standarddisplay,'diff_instruction'=>$diff_instruction);

//print_r($data);	
    	       $this->load->model('classroommodel');
	           $result = $this->classroommodel->strengths_insert('lesson_plan_library_data', $data);

        }
   
   if ($result)
	  $result_id = $this->db->insert_id();
	 // print_r($user_id);exit;
	  
   
      $lesson_plan_id = $this->input->post('lesson_plan_id');
		//$dates_description=array();
foreach($lesson_plan_id as $lesson_plan){
		$lesson_plan_data[$lesson_plan][0]= $this->input->post('lesson_plan_data_'.$lesson_plan);		
		}
	
foreach($lesson_plan_data as $key=>$value_data)
		{
	$dates_data = array('lesson_plan_id'=>$key,'lesson_plan_data'=>$value_data[0],'lesson_plan_library_id'=>$result_id);
	//print_r($dates_data);
	$this->load->model('classroommodel');			
    $this->classroommodel->strengths_insert('lesson_plan_library_sub',$dates_data);
}
$custom_diff = $this->input->post('custom_diff');

		//$dates_description=array();
foreach($custom_diff as $custom_diff_val){
		
		$custom[$custom_diff_val][0]= $this->input->post('custom_'.$custom_diff_val);		
		
		}
	
foreach($custom as $key=>$value)
		{
	$custom_data = array('custom_diff'=>$key,'custom'=>$value[0],'lesson_plan_library_id'=>$result_id);
	//print_r($custom_data);
	$this->load->model('classroommodel');			
    $result_data= $this->classroommodel->strengths_insert('lesson_plan_library_custom',$custom_data);
}
 if ($result_data) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'planningmanager/lesson_plan_creator');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
           
    }
	
	public function lesson_Plan_library_edit() {

        //print_r($this->input->post('action_home_responsible'));exit;
        $data['idname'] = 'lesson';

        if ($this->input->post('submit')) {
			
			
			$lesson_plan_library_id = $this->input->post('lesson_plan_library_id');
			$lesson_title = $this->input->post('lesson_title');
			$grade_id = $this->input->post('grade_id');
			$standard_id = $this->input->post('standard_id');
            $standarddata_id = $this->input->post('standarddata_id');
			
            $strand = $this->input->post('strand');
			$standarddata = $this->input->post('standarddata');
          	$standarddisplay = $this->input->post('standarddisplay');
       	    $diff_instruction = $this->input->post('diff_instruction');
       
          // $meeting = implode(",", $meeting_type);
			
            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            } else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
 
            $data = array('user_type' => $this->session->userdata('login_type'),'lesson_plan_library_id'=>$lesson_plan_library_id,'user_id' => $user_id,'lesson_title'=>$lesson_title,'district_id' => $this->session->userdata('district_id'),'grade_id'=>$grade_id,'standard_id'=>$standard_id,'standarddata_id'=>$standarddata_id,'strand'=>$strand,'standarddata'=>$standarddata,'standarddisplay'=>$standarddisplay,'diff_instruction'=>$diff_instruction);

//print_r($data);	exit;
		   
			    $this->load->model('iep_tracker_model');
			  $result = $this->iep_tracker_model->update_lesson_plan_library('lesson_plan_library_data',$data,$data['lesson_plan_library_id']);

        }
   
   if ($result)
	  
   $this->load->Model('iep_tracker_model');
	$remove_data = $this->iep_tracker_model->delete_data('lesson_plan_library_sub',array('lesson_plan_library_id'=>$lesson_plan_library_id));
	if($remove_data){
      $lesson_plan_id = $this->input->post('lesson_plan_id');
		//$dates_description=array();
foreach($lesson_plan_id as $lesson_plan){
		$lesson_plan_data[$lesson_plan][0]= $this->input->post('lesson_plan_data_'.$lesson_plan);		
		}
	
foreach($lesson_plan_data as $key=>$value_data)
		{
	$dates_data = array('lesson_plan_id'=>$key,'lesson_plan_data'=>$value_data[0],'lesson_plan_library_id'=>$lesson_plan_library_id);
	//print_r($dates_data);
	$this->load->model('classroommodel');			
    $this->classroommodel->strengths_insert('lesson_plan_library_sub',$dates_data);
}
	}
	
	 $this->load->Model('iep_tracker_model');
	$remove_custom_data = $this->iep_tracker_model->delete_data('lesson_plan_library_custom',array('lesson_plan_library_id'=>$lesson_plan_library_id));
if($remove_custom_data){	
$custom_diff = $this->input->post('custom_diff');

		//$dates_description=array();
foreach($custom_diff as $custom_diff_val){
		
		$custom[$custom_diff_val][0]= $this->input->post('custom_'.$custom_diff_val);		
		
		}
	
foreach($custom as $key=>$value)
		{
	$custom_data = array('custom_diff'=>$key,'custom'=>$value[0],'lesson_plan_library_id'=>$lesson_plan_library_id);
	//print_r($custom_data);
	$this->load->model('classroommodel');			
    $result_data= $this->classroommodel->strengths_insert('lesson_plan_library_custom',$custom_data);
}
	}
 if ($result_data) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'planningmanager/lesson_plan_creator');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
           
    }

        public function edit_lesson_Plan_library() {
        //if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'lesson';
			
			 $this->load->Model('classroommodel');
            $data['lesson_Plan_library'] = $this->classroommodel->get_lesson_Plan_library();
			
			
		//print_r($data['update_report']);exit;	
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('planningmanager/edit_lesson_Plan_library', $data);
     
    }
	
	function edit_lesson_Plan_library_data ($lesson_plan_library_id){

	 $data['type'] = $this->input->post('plan_type');
             $data['idname']='lesson';

        $data['type']='Create';
    $this->load->Model('lessonplanmodel');     
	$this->load->Model('custom_differentiatedmodel');     
        $data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
		}
	if($this->session->userdata('login_type')=='user')
	  	{
	$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
		}
		
	  $lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
            
            $lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
           
             foreach($lessonplans as $lessonplansval){
             foreach($lessonplansub as $lessonplanssubval){
               $data['lessontype'][$lessonplansval['tab']]["lesson_plan_id"] = $lessonplansval['lesson_plan_id']; 
                    //if($lessonplansval)
                    if($lessonplansval['lesson_plan_id']==$lessonplanssubval['lesson_plan_id']){
                        $data['lessontype'][$lessonplansval['tab']][$lessonplanssubval['lesson_plan_sub_id']] =  $lessonplanssubval['subtab'];
                    }
                }
			 }
		  $data['custom_diff']=$this->custom_differentiatedmodel->getallplans();		
		
		$this->load->Model('iep_tracker_model');
		$data['subjects'] = $this->iep_tracker_model->get_subject_in_standard();

		 $this->load->Model('schoolmodel');
		 $data['grades'] = $this->schoolmodel->getallgrades();

		if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}	

			 $this->load->Model('classroommodel');	
		 $data['lesson_plan_library'] = $this->classroommodel->get_lesson_Plan_library_by_id($lesson_plan_library_id);
	//print_r($data['lesson_Plan_library']);exit;
	
		$data['id']= $iep_id;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('planningmanager/edit_lesson_Plan_library_data',$data);
	   }

        function get_grade_by_strand_info(){
		$this->load->Model('iep_tracker_model');
		$this->load->Model('standarddatamodel');

		$get_all_standard = $this->iep_tracker_model->get_grade_By_standards($this->input->post("grade_id"));
	$grades = $data['grades']=$this->standarddatamodel->getActiveGrades($get_all_standard[0]['subject_id'],$get_all_standard[0]['grade']);

	$data['strands']=$this->standarddatamodel->getstrand($grades[0]['grade_id'],$grades[0]['standard_id']);


		echo json_encode($data);
	}

	function get_lesson_info($lesson_plan_library_id){
	
		 $this->load->Model('classroommodel');	
		 $data['lesson_plan_library'] = $this->classroommodel->get_lesson_Plan_library_by($lesson_plan_library_id);
	  echo json_encode($data);
	  exit;
	
	
	
	}
 
        public function retrieve_lesson_Plan_library() {
            $data['idname'] = 'lesson';
			
		    $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('planningmanager/retrieve_lesson_plan_library_report', $data);
       
    }

        public function retrieve_lesson_Plan_library_data() {
        $data['idname'] = 'lesson';
        if ($this->input->post('submit')) {
	
	  $grade_id = $this->input->post('grade_id');
	  
	        $this->session->set_userdata('submitbutton', $this->input->post('submit'));
      
//            print_r($date);exit;
           $this->retrieve_lesson_data($grade_id);
        }
    }
	
        public function retrieve_lesson_data($grade_id) {
        $data['idname'] = 'lesson';

 $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();

		$this->load->Model('classroommodel');
        $data['retrieve_reports'] = $this->classroommodel->get_lesson_Plan_library_by_grade_id($grade_id);
		
		//print_r($data['retrieve_reports']);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('planningmanager/retrieve_lesson_plan_library_report', $data);
    }	
	
        function lesson_Plan_library_pdf($grade_id,$lesson_plan_library_id) {
        error_reporting(1);
        $this->load->Model('classroommodel');
        $data['lesson_Plan_library'] = $this->classroommodel->get_lesson_Plan_library_by_grade_pdf($grade_id,$lesson_plan_library_id);
		
		 $data['lesson_plan_library_sub'] = $this->classroommodel->get_lesson_plan_library_sub($lesson_plan_library_id);
		//print_r($data['lesson_plan_library_sub']);exit;
		$data['lesson_plan_library_custom'] = $this->classroommodel->get_lesson_plan_library_custom($lesson_plan_library_id);



        $data['view_path'] = $this->config->item('view_path');
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('planningmanager/lesson_plan_library_report_pdf', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    function check_subject(){
        $this->load->Model('lessonplanmodel');
        $cdataarr = explode('-',$this->input->post('cdate'));
        
        $fromdate = $todate = $cdate = $cdataarr[2].'-'.$cdataarr[0].'-'.$cdataarr[1];
        if($this->input->post('teacher_id')){
        $teacher_id = $this->input->post('teacher_id');
        } else {
            $teacher_id = $this->session->userdata('teacher_id');
        }
        $ctimearr = explode(' ',$this->input->post('starttime'));
        if ($ctimearr[1]=='AM'){
            $ctime = date('h:i:s',strtotime($ctimearr[0]));
        } else {
            $ctime = date('H:i:s',strtotime($ctimearr[0])+43200);
        }
        
        $starttime = $ctime;
        

        $subject = $this->lessonplanmodel->lessonplansbyteacherbytimetable($teacher_id,$fromdate,$todate,$starttime);
        echo $subject[0]['subject_name'];
        //$this->db->last_query();
        //print_r($subject[0]['subject_name']);exit;
        /**/
        exit;
    }
        
}