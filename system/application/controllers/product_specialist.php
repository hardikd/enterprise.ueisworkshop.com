<?php
/**
 * (Product_specialist Or Home Or Startup) page Controller.
 *
 */
class Product_specialist  extends	Controller {

	function __construct()
	{
		parent::Controller();
		$this->no_cache();
	}

protected function no_cache()
	{
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	} 
	function index($error_code=0)
	{
		
		if($error_code!=0){
			//Get Error description
			$data['error_msg'] = $this->getErrorDesc($error_code);
			//Get the posted data to repopulate the form
			$data['username']=$this->input->post('username');
			$data['password']=$this->input->post('password');
			}
		$data['view_path']=$this->config->item('view_path');
		
		$this->load->view('prod_spec/view',$data);
	}	
	
	
	function getErrorDesc($error_code){
		switch($error_code){
			case "1":
				return "username or password are incorrect";
				break;
			
		}
	}
	
}
