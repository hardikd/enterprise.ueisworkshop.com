<?php 
$view_path="system/application/views/";
include ($view_path.'inc/PDFMerger.php');

class Pdf extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false && $this->is_teacher()==false){
			//These functions are available only to schools - So redirect to the login page
			redirect("index");
		}
		
	}
	
 
function index()
{
$pdata=$this->input->post('pdata');
if(!empty($pdata))
{
$view_path=$this->config->item('view_path');
$login_type=$this->session->userdata('login_type');
             if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}			
  
			
			$filesave=md5(uniqid(rand(), true)).time();

			
$pdf = new PDFMerger;
$pdf->addPDF(WORKSHOP_FILES.'general/'.$login_type.$login_id.'.pdf', 'all')
    ->addPDF(WORKSHOP_FILES.'pdf/'.$login_type.$login_id.'.pdf', 'all')
	->addPDF(WORKSHOP_FILES.'uploads/'.$login_type.$login_id.'.pdf', 'all')	
	->merge('file', WORKSHOP_FILES.'reportpdf/'.$filesave.'.pdf');
	$this->load->Model('teachermodel');
	$this->teachermodel->pdfreportsave($filesave,$pdata['status'],$pdata['score'],$pdata['dates'],$pdata['teacher_id'],$pdata['comments']);
	$data['status']='Report Created Successfully';
	echo json_encode($data);
	exit;
}	
	
}
function reportpdf($filepath)
	{
	 $view_path=$this->config->item('view_path');
	 
	$pdf = new PDFMerger;
$pdf->addPDF(WORKSHOP_FILES.'reportpdf/'.$filepath.'.pdf', 'all')
	->merge('', WORKSHOP_FILES.'reportpdf/'.$filepath.'.pdf');
	
	
	}
	
	function viewpdf($type,$id,$report_id)
{

$pdf = new PDFMerger;
$pdf->addPDF(WORKSHOP_FILES.'observationplans/view_'.$type.'_'.$id.'.pdf', 'all')
    ->addPDF(WORKSHOP_FILES.'observationplans/'.$report_id.'.pdf', 'all')	
	->merge('', WORKSHOP_FILES.'observationplans/save_'.$type.'_'.$id.'.pdf');



}
}


?>