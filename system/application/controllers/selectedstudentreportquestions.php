<?php 
 /**
 * Studentdetail Controller.
 *
 */
class Selectedstudentreportquestions extends	MY_Auth {
function __Construct()
	{
	
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false  && $this->is_teacher()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}

	}
	function index()
	{  error_reporting(0);
	
	 	 
		$data['view_path']=$this->config->item('view_path');
		$data['dataRecord'] = $this->session->userdata('que_result');
		
	}
	function displayQuestion()
	{
			error_reporting(0);
			$this->load->model('selectedstudentreportmodel');
			
			$aid = $_REQUEST['aid'];
			$studu_id = $_REQUEST['uid'];
			//echo $_REQUEST['cluster_name']; exit;
	 		//$cluster = html_entity_decode($_REQUEST['cluster_name'],ENT_QUOTES);
			$cluster = str_replace('*','"',$_REQUEST['cluster_name']);
			$questions = array();
			$questions = $this->selectedstudentreportmodel->getquestionsdetail($aid,$cluster);
			//echo $totques = count($questions);
			//echo"<pre>"; print_r($questions); exit;
			$quetr = '';
			$totques ='';
			$trueQuecount = 0;
			$ParttrueQuecount = 0;
			$falseQuecount = 0;
			foreach($questions as $val)
			{
				$totques++;
				$ans = '';
				$quetr .= '<tr><td align="center" width="5%">'.$totques.'</td>  <td width="85%">';
				$quetr .= $val['question_text']."</td>";
				$qid = $val['id'];
				//echo $val['question_type_id'];
				if($val['question_type_id']!= 6)
				{	
					 $queresult = $this->selectedstudentreportmodel->getquestionresult($aid,$qid,$studu_id);
				 	 if($queresult[0]['is_correct_ans'] == 1)
						 $ans = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
					 	$ans = 0;
				}
	 			else 
				{
					 $queresult = $this->selectedstudentreportmodel->getquestionresult6($aid,$qid,$studu_id);
					 $tot = count($queresult); $cc ='';
					 if($queresult[0]['is_correct_ans'] == 1)
							 $cc = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
							$cc = 0;
					  $rc = 0; //echo"<pre>"; print_r($queresult);
					  if($tot >1)
					  {
						foreach($queresult as $qr)
						{
							if($qr['is_radio_ans_correct'] == 1)
							 $rc++;
						}
					  }
						if($cc == 1 && $rc == $tot)
							 $ans = 1;
						else if($cc == 1 && $rc < $tot)
				 			 $ans = 2;
						else if($cc == 0 && $rc <= $tot && $rc >0)
				 			 $ans = 2;
						if($cc != 1 && $rc != $tot)
							 $ans = 0;	 
				}
				 	$quetr .='<td align="center">';
					if($ans == 1) $quetr .= '<font color="#009900"> Correct </font>';
					if($ans == 0) $quetr .= '<font color="#FF0000"> Incorrect </font>';
					if($ans == 2) $quetr .= '<font color="#0000CC"> Partially correct </font>';
		 		 $quetr .= "</td></tr>";
				 if($ans == 1)
					 $trueQuecount++;
				 if($ans == 0)
					 $falseQuecount++;
				  if($ans == 2)
					 $ParttrueQuecount++;
	  		}
				$disp ='';
				if($ParttrueQuecount >0)
				{
					$disptrueQuecount = $trueQuecount + $ParttrueQuecount;
					$disp = '<font color="#FF9900">'.$disptrueQuecount.'</font>';
				}
				else
				{
					$disp = '<font color="#009900">'.$trueQuecount.'</font>';
				}
					
				 $Quetab ='';
				 $Quetab .=' <table width="90%" border="0"><tr><td><table border="1" width="100%"> <tr  height="40"> <td> Cluster:  '. utf8_decode($cluster).'</td> <td> Total Questions:  '.$totques.'</td><td> Correct Answer: '.$disp.' </td><td> Incorrect Answers:  <font color="#FF0000">'.$falseQuecount.'</font></td></tr></table></td></tr>' ;
				 $Quetab .='<tr><td><table border="1" width="100%" cellpadding="5">';
				$Quetab .= $quetr;
				$Quetab .='</table></td></tr></table>';
				 
				 $data['resultTable'] =$Quetab;
		
 				$data['view_path']=$this->config->item('view_path');
				$this->load->view('selectedstudentreportquestions/index',$data);
 	
	
		/*if($Quetab != '')
		{
			//echo $Quetab;
			$this->session->set_userdata('que_result',$Quetab);
			//$this->session->set_userdata('graph_json',json_encode($data));								
		 	exit;					
		}	
		else
		{
			$this->session->set_userdata('que_result',"");						
		 	exit;
		}
		*/
		
 
	

	}
	
 	
 
}?>
