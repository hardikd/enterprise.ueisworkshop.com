<?php
 $path_file =  'libchart/classes/libchart.php';
 include_once($path_file);
/**
 * Studentdetail Controller.
 *
 */
class Addproficiency extends	MY_Auth {
function __Construct()
	{
	
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false  && $this->is_teacher()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->no_cache();
	}
	
	function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
		
	function index()
	{
		
			
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/enterprise/index.php/";</script>';
			}
			else {
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
	
	   if($this->session->userdata("login_type")=='user')
		  {
			  	if($this->session->userdata('login_special') == 'district_management'){
		$data['idname']='tools';
				}
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('proficiencymodel');
								
				$data['assessments']=$this->proficiencymodel->getassessments();
								
			//	$data['records'] = $this->proficiencymodel->getprodetail();
						
				$this->load->view('proficiency/addproficiency',$data);
	   
	 	 }
	
		}
	
	}
	
	function createprof()
	{
			
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/enterprise/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		if(isset($_REQUEST['assessmentid']) && $_REQUEST['assessmentid'] !="")
		{
			$assessment_id = $_REQUEST['assessmentid'];
			$belowbasicfrom = $_REQUEST['bbasicfrom'];
			$belowbasicto = $_REQUEST['bbasicto'];
			$basicfrom = $_REQUEST['basicfrom'];
			$basicto = $_REQUEST['basicto'];
			$proficientfrom = $_REQUEST['proficientfrom'];
			$proficientto = $_REQUEST['proficientto'];
			$this->load->model('proficiencymodel');
			$this->proficiencymodel->updateproficiency($belowbasicfrom,$belowbasicto,$basicfrom,$basicto,$proficientfrom,$proficientto,$assessment_id);
				$this->session->set_flashdata('success','Proficiency has been successfully Updated. ');
					redirect('addproficiency/index');

			
		}
		else
			{
					if(isset($_REQUEST['bbasicfrom']) && $_REQUEST['bbasicfrom'] !='')
					{
					
					$belowbasicfrom = $_REQUEST['bbasicfrom'];
					$belowbasicto = $_REQUEST['bbasicto'];
					$basicfrom = $_REQUEST['basicfrom'];
					$basicto = $_REQUEST['basicto'];
					$proficientfrom = $_REQUEST['proficientfrom'];
					$proficientto = $_REQUEST['proficientto'];
					$assessment_id = $_REQUEST['assessment'];
					
			$this->load->model('proficiencymodel');
			$this->proficiencymodel->setproficiency($belowbasicfrom,$belowbasicto,$basicfrom,$basicto,$proficientfrom,$proficientto,$proficient_id,$assessment_id);
				$this->session->set_flashdata('success','Proficiency has been successfully submitted. ');
					redirect('addproficiency/index');
				}
			}
		}
	}
	
	function getproficiency()
	{
		$assid = $_REQUEST['assessment_id'];
		$this->load->model('proficiencymodel');
		$data['alldata']=$this->proficiencymodel->getdata($assid);
		if(!empty($data['alldata']))
		{
		foreach($data['alldata'] as $kk => $value)
		{
		echo $value['assessment_id'].'|';
		echo $value['bbasicfrom'].'|';
		echo $value['bbasicto'].'|';
		echo $value['basicfrom'].'|';
		echo $value['basicto'].'|';
		echo $value['pro_from'].'|';
		echo $value['pro_to'].'|';
		}
		}
		else
		{
			echo "emp";
		}
	}
	

	
}	