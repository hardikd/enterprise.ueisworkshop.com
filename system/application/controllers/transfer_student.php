<?php 

class Transfer_student extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
                $this->load->Model('transferstudentmodel');
		
	}
	
	function index()
	{ 
            
             if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('schoolmodel');
	   $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  if($data['district']!=false)
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict($data['district'][0]['district_id']);
	  }
	  else
	  {
	    $data['school']='';
	  }
	  
	  
	  }
	 	// error_reporting(0);
		$data['view_path']=$this->config->item('view_path');
//		print_r($data);exit;
		$this->load->view('transfer_student/index',$data);
 		 
	}
	
	 public function getStudentsBySchool_1(){
            $students = $this->transferstudentmodel->getStudentBySchool($this->input->post('school_id'));
            
            echo json_encode($students);
        }
	
        public function getStudentsBySchool(){

			$students = $this->transferstudentmodel->getStudentBySchoolName($this->input->get('school_id'),$this->input->get('q'));
                        //echo $this->db->last_query();exit;
			
	if($students!=false)
		{	
		foreach($students as $students_val)
			{

			$sname = $students_val->firstname.' '.$students_val->lastname.' - '.$students_val->student_number;			
			$student_number = $students_val->student_number;
			$lastname = $students_val->lastname;
			$firstname = $students_val->firstname;
			$UserID = $students_val->UserID;
			$student_id = $students_val->student_id;
			
			echo "$sname|$student_number|$lastname|$firstname|$UserID|$student_id\n";	
			}
		}
		else
		{
			echo 'No Students Found.|';
		}
		
		}
           
        
        public function getSchoolGrades(){
            $grades = $this->transferstudentmodel->getSchoolGrades($this->input->post('district_id'));
            echo json_encode($grades);
        }
        
        public function transfer(){
            $userid = $this->input->post('userID');
            $school_id = $this->input->post('toschool');
            $grade = $this->input->post('grade');
            $district_id = $this->input->post('todistrict');
            $grade_name = $this->input->post('gradeName');
            $transfer = $this->transferstudentmodel->transfer_student($userid,$school_id,$grade,$district_id,$grade_name);
            if($transfer){
                echo "Successfully transferred student.";
            } else {
                echo "Error while transferring student.";
            }
        }

	
	
}	//class end