<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ob_start();
$view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');

class Implementation extends MY_Auth {
    function __Construct()
	{
		parent::Controller();
	}
    public function index(){
	if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/index',$data);
	}else if($this->session->userdata('login_type')=='observer') { 
	    $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/index',$data);
	
	}else if($this->session->userdata('login_type')=='user') { 
	    $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_index',$data);
	
	
	}
	
    }
    
    public function grade_tracker(){

	
	if($this->session->userdata('login_type')=='teacher') { 
	
        $data['idname']='implementation';
        $this->load->Model('videooptionmodel');
        		$data['video_option'] = $this->videooptionmodel->get_option();
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/grade_tracker',$data);
	}elseif($this->session->userdata('login_type')=='observer') { 
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/grade_tracker',$data);
	}else if($this->session->userdata('login_type')=='user') { 
	
	$login_required=$this->session->userdata('login_required'); 
	if($login_required=='')
		{
			header("Location: index");  
		}
		else
		{				
	 if($this->session->userdata('login_type')=='user')
	  {
		 
	     /*$data['view_path']=$this->config->item('view_path');
	  
	  $this->load->Model('schoolmodel');
	  
	  $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	  
	  $this->load->view('district/home',$data);*/
	  
	  $data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');		
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
			  
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
		$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		}
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_grade_tracker',$data);
	
	  
	  }
		}
		
    }
	}
function grade_tracker_secondary()
	{
		   $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		if($this->input->post('submit'))
		{	
			$data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($this->input->post('school_type'));
			$data['school_type'] = $this->input->post('school_type');			
		}
		else
		{
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
		}		
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
			$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		
		}
		$this->load->view('implementation/grade_tracker_secondary',$data);
	
	
	}


    function schoolhome($id=false)
	{
		
	$this->load->Model('districtmodel');
		
		
		if($id!=false && $this->session->userdata('product_login_type')=='Product Specialist')
		{
		$status = $this->districtmodel->is_valid_login($id);
		}
		else
		{
		$status = $this->districtmodel->is_valid_login();
		
		}
		
		
		
		
		
		
		
		if($status!=false){
			//set the session
			 $today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$status[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			if($status[0]['type']=='demo' && $status[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($status[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$status[0]['district_id']);
			  }	
			
			
			}
			
			if($status[0]['type']=='demo' && $status[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['username']);
			 $this->session->set_userdata("demodist_user_id",$status[0]['dist_user_id']);
			 $this->session->set_userdata("demodistrict_id",$status[0]['district_id']);
			  $this->session->set_userdata("demodistrictname",$status[0]['districts_name']);
			 $this->load->Model('districtmodel');
		      $email = $this->districtmodel->getemail($status[0]['districts_name']);
			
			  $this->session->set_userdata("demoemail",$email[0]['email']);
			 $this->userdemo(30,'district');
			}
			else if($status[0]['type']=='demo'  && $days>=90 && $status[0]['username']!='bhasker73'   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['username']);
			$this->session->set_userdata("demodist_user_id",$status[0]['dist_user_id']);
			 $this->session->set_userdata("demodistrict_id",$status[0]['district_id']);
			  $this->session->set_userdata("demodistrictname",$status[0]['districts_name']);
			 $this->load->Model('districtmodel');
		      $email = $this->districtmodel->getemail($status[0]['districts_name']);
			
			  $this->session->set_userdata("demoemail",$email[0]['email']);
			 
			 if($status[0]['feedback']==0)
			 {
			    $this->userdemo(30,'district');
			 
			 }
			 else
			 {
			 	 
				$this->userdemo(90,'district');
			  }
			 
			}
			else
			{
				
			$this->session->set_userdata("login_type",'user');
			$this->session->set_userdata("username",$status[0]['username']);
			$this->session->set_userdata("dist_user_id",$status[0]['dist_user_id']);
			$this->session->set_userdata("district_id",$status[0]['district_id']);
			$this->session->set_userdata("district_name",$status[0]['districts_name']);			
			$this->session->set_userdata("dis_country_id",$status[0]['country_id']);
			$this->session->set_userdata("dis_state_id",$status[0]['state_id']);
			$this->session->set_userdata("avatar_id",$status[0]['avatar']);
			$this->session->set_userdata("LP",1);
			$this->session->set_userdata("NC",1);
			$this->session->set_userdata("TE",1);
			$this->session->set_userdata("login_required",'yes');
	
			$this->userindex();
			
			}
		}else{
			
			// give appropriate message.
			
			$this->index(1);
			return;
		}
	
	
	}
    public function homework_tracker(){
		if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/homework_tracker',$data);
}else if($this->session->userdata('login_type')=='observer') { 
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/homework_tracker',$data);
}else if($this->session->userdata('login_type')=='user') { 

$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'implementation');
				
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_homework_tracker',$data);
    }
}
	
    
    public function iep_tracker(){
	if($this->session->userdata('login_type')=='teacher'){
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/iep_tracker',$data);
	}else if($this->session->userdata('login_type')=='observer') {
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/iep_tracker',$data);
	}else if($this->session->userdata('login_type')=='user') {
		
		//$this->session->set_flashdata ('permission','Additional Permissions Required');
          //      redirect(base_url().'implementation');
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
		   $this->load->view('implementation/iep_tracker',$data);
        $this->load->view('implementation/dist_iep_tracker',$data);
    }
}
    public function eld_tracker(){
	if($this->session->userdata('login_type')=='teacher') {
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/eld_tracker',$data);
	}else if($this->session->userdata('login_type')=='observer') {	
		 $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/eld_tracker',$data);
    
	}else if($this->session->userdata('login_type')=='user') {	
	
	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'implementation');
	
		 $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_eld_tracker',$data);
    }
}
    
    public function instructional_efficacy(){
         if($this->session->userdata('login_type')=='teacher') { 
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/instructional_efficacy',$data);
    }
	else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/instructional_efficacy1',$data);
	}else if($this->session->userdata('login_type')=='user') {
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_instructional_efficacy',$data);
	}	
		
	}
     public function observation(){
		 if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/observation',$data);
    }else if($this->session->userdata('login_type')=='observer') {
    	$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/lesson_observation',$data);
	}else if($this->session->userdata('login_type')=='user') {
    	$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_lesson_observation',$data);
	}
	}
public function observation_retrieve(){
	 if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/observation',$data);
	  }else if($this->session->userdata('login_type')=='observer') {
	$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/observation',$data);
	  }else if($this->session->userdata('login_type')=='user') {
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_observation',$data);
	  }
    }
public function summative(){
	 if($this->session->userdata('login_type')=='teacher') { 
     $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/summative',$data);
	}else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/summative',$data);
	 }else if($this->session->userdata('login_type')=='user') {	
	 	$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_summative',$data);
	
	}
  }
public function self_assessment(){
		 if($this->session->userdata('login_type')=='teacher') { 
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/self_assessment',$data);
		}else if($this->session->userdata('login_type')=='observer') {
		$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/self_assessment',$data);
		}else if($this->session->userdata('login_type')=='user') {
			$data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
        $this->load->view('implementation/dist_self_assessment',$data);
		
    }
}


	function iep_tracker_main_page($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/iep_tracker_main_page',$data);
	  
	}

	    public function upload_iep_tracker() {
            $data['idname'] = 'implementation';


	   $this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');			
	if($this->session->userdata('login_type')=='user')
		{
	$this->load->Model('schoolmodel');
	$data['school']=$this->schoolmodel->getschoolbydistrict();
	if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   	}
	   else
	   {
	    $data['teachers']=false; 
	   }
	  }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
			
				if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}	
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('implementation/edit_iep', $data);
     
    }


public function edit_iep_data(){
        $data['idname'] = 'implementation';
        if ($this->input->post('submit')) {

            $student_id = $this->input->post('student_id');
			$teacher_id = $this->input->post('teacher_id');
			

			//$date = date('Y/m/d', strtotime(str_replace('-', '/', $this->input->post('date'))));

		 $this->upload_iep_tracker_2($student_id,$teacher_id);
        }
    }
	
	    public function upload_iep_tracker_2($student_id,$teacher_id) {
            $data['idname'] = 'implementation';
			
		/*	 $this->load->Model('iep_tracker_model');
            $data['update_report'] = $this->iep_tracker_model->get_iep_tracker_data();
			
		*/
			$this->load->Model('iep_tracker_model');
	 if ($this->session->userdata('login_type') == 'teacher') {          
            $data['update_report'] = $this->iep_tracker_model->get_iep_tracker_data_1($student_id);

	 }else{
			$data['update_report'] = $this->iep_tracker_model->get_iep_tracker_data_by_teacher($student_id,$teacher_id);
	 }

            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('implementation/upload_iep_tracker_report', $data);
     
    }

  public function edit_iep_tracker_report($id) {
            $data['idname'] = 'implementation';
			$data['id']=$id;

            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('implementation/iep_edit_block', $data);
     
    }
	
	function edit_basic_student_meeting_information($id){
            	 $data['idname']='implementation';
		$data['id'] = $id;
$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/edit_basic_student_information',$data);
	  
		}	
		
	
	
	 public function edit_iep_tracker($id) {
        $data['idname'] = 'implementation';
	/* satrt dates*/
$this->load->Model('dates_model');
            $data['dates'] = $this->dates_model->get_all_dates_data();

$this->load->Model('language_model');
            $data['language'] = $this->language_model->get_all_language_data();	

 $this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
	if($this->session->userdata('login_type')=='user')
		{
	$this->load->Model('schoolmodel');
	$data['school']=$this->schoolmodel->getschoolbydistrict();
	if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   	}
	   else
	   {
	    $data['teachers']=false; 
	   }
	  }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
	
		if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}		
			 $this->load->Model('iep_tracker_model');
            $data['dates_description'] = $this->iep_tracker_model->get_dates_data($id);
//			print_r($data['meeting_attendees']);exit;
			foreach($data['dates'] as $dates){
				
					foreach($data['dates_description'] as $dates_description){
					if($dates_description->dates_id==$dates->id){
						$updateatt[$dates->id]['dates_description'] = $dates_description->dates_description;
						$updateatt[$dates->id]['dates'] = $dates->dates;
						$updateatt[$dates->id]['field_type'] = $dates->field_type;
					}
				
					}
				}
			$data['dates_data'] = $updateatt;
			
$this->load->Model('meeting_type_model');
            $data['meeting_type'] = $this->meeting_type_model->get_all_meeting_type_data();

$this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();	
				
	$this->load->Model('iep_tracker_model');
            $data['iep_data'] = $this->iep_tracker_model->get_upload_iep_data($id);
//print_r($data['iep_data']);exit;


			$data['get_upload_iep'] = $this->iep_tracker_model->get_upload_iep_data_data($id);

$this->load->Model('ethnic_code_model');
            $data['ethnic_code'] = $this->ethnic_code_model->get_all_ethnic_code_data();	
			
$data['view_path'] = $this->config->item('view_path');
        $this->load->view('implementation/edit_iep_tracker', $data);
    }

 public function update_iep_report() {
        $data['idname'] = 'implementation';
  if ($this->input->post('submit')) {
	  		$id = $this->input->post('id');
            $schools_type = $this->input->post('schools_type');
			$student_id = $this->input->post('student_id');
            $grade_id = $this->input->post('grade_id');
			$date_birth = $this->input->post('birth_date');
            $gender = $this->input->post('gender');
			$limited_english_proficiency = $this->input->post('limited_english_proficiency');
			$ethnic_code = $this->input->post('ethnic_code');
			//step2
			$date = $this->input->post('date');
          	//$dates = $this->input->post('dates');
            $meeting_type = $this->input->post('meeting_type');
           
            //step3
           $home_language = $this->input->post('home_language');
            $student_language = $this->input->post('student_language');
            $alternate_mode_of_communication = $this->input->post('alternate_mode_of_communication');
			$address = $this->input->post('address');
			$city = $this->input->post('city');
			$zip_code = $this->input->post('zip_code');
            //step 4
            $home_telephone = $this->input->post('home_telephone');
			$daytime_telephone = $this->input->post('daytime_telephone');
			$emergency_telephone = $this->input->post('emergency_telephone');
			$school_of_attendance = $this->input->post('school_of_attendance');
			$school_of_residence = $this->input->post('school_of_residence');
			$location_coad = $this->input->post('location_coad');


            $meeting = implode(",", $meeting_type);
			
            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            } else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
		
	if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	
		}
 
            $data = array('user_type' => $this->session->userdata('login_type'),'user_id' => $user_id,'teacher_id'=>$teacher_id,'schools_type'=>$schools_type,'school_id' => $school_id,'id'=>$id,'district_id' => $this->session->userdata('district_id'),'student_id' => $student_id, 'grade_id'=>$grade_id,'birth_date' => date('Y-m-d', strtotime(str_replace('-', '/', $date_birth))),'gender'=>$gender,'limited_english_proficiency'=>$limited_english_proficiency,'ethnic_code'=>$ethnic_code,'date'=>date('Y-m-d', strtotime(str_replace('-', '/', $date))),'home_language'=>$home_language,'student_language'=>$student_language,'student_language'=>$student_language,'alternate_mode_of_communication'=>$alternate_mode_of_communication,'address'=>$address,'city'=>$city,'zip_code'=>$zip_code,'home_telephone'=>$home_telephone,'daytime_telephone'=>$daytime_telephone,'emergency_telephone'=>$emergency_telephone,'school_of_attendance'=>$school_of_attendance,'school_of_residence'=>$school_of_residence,'location_coad'=>$location_coad,'meeting_type' =>$meeting);

            $this->load->model('iep_tracker_model');
			  $result = $this->iep_tracker_model->update_data('basic_student_information_form', $data,$data['id']);
        }
		
	$this->load->Model('iep_tracker_model');
	$remove_data = $this->iep_tracker_model->delete_data('dates_description',array('iep_id'=>$id));
	if($remove_data){
	 $dates_id = $this->input->post('dates_id');
		$dates_description=array();
foreach($dates_id as $dates){
		$dates_description[$dates][0]= $this->input->post('dates_description_'.$dates);	
		}
	
foreach($dates_description as $key=>$value_data)
		{
	$dates_data = array('dates_id'=>$key,'dates_description'=>$value_data[0],'iep_id'=>$id);
	//print_r($dates_data);
	$this->load->model('classroommodel');			
    $this->classroommodel->strengths_insert('dates_description',$dates_data);
} 
	}
		
        
	  if ($result)
	        $link = redirect(base_url() . 'implementation/update_meeting_attendees/' . $id);
	
    }
function observation_edit($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/observation_edit',$data);
	  
	}

function summative_edit($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/summative_edit',$data);
	  
	}
function summative_retrieve($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/summative_retrieve',$data);
	  
	}

		

	function retrieve_grade($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/retrieve_grade',$data);
	  
	}
        
        function retrieve_homework_record($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/retrieve_homework_record',$data);
	  
	}
        
        function retrieve_eld_record($msg=false)
	{		
		 if($this->session->userdata('login_type')=='user'){
                $this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'implementation');
            }
            
            if ($this->session->userdata('LP') == 0) {
                redirect("index");
            }
            if ($this->session->userdata('login_type') == 'teacher') {
                $this->load->Model('parentmodel');
                $data['students'] = $this->parentmodel->get_students_all();
            } else if ($this->session->userdata('login_type') == 'parent') {
                $this->load->Model('parentmodel');
                $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));

                $data['students'] = $this->parentmodel->get_students_all();
            } else {
                $this->load->Model('teachermodel');
                $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
                $this->load->Model('parentmodel');
                $data['students'] = $this->parentmodel->get_students_all();
            }
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/retrieve_eld_record',$data);
	  
	}
        
        function self_reflection($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/self_reflection',$data);
	  
	}
        
        function retrieve_report($msg=false)
	{		
		 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/retrieve_report',$data);
	  
	}
        
       /*
	   function enter_iep_record(){
            	 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/enter_iep_record',$data);
	  
		}
		*/
		
function basic_student_information(){
       $data['idname']='implementation';
		$this->load->Model('dates_model');
            $data['dates'] = $this->dates_model->get_all_dates_data();
			
	  $this->load->Model('meeting_type_model');
            $data['meeting_type'] = $this->meeting_type_model->get_all_meeting_type_data();
	 $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();		
	
	 $this->load->Model('ethnic_code_model');
            $data['ethnic_code'] = $this->ethnic_code_model->get_all_ethnic_code_data();

	$this->load->Model('language_model');
            $data['language'] = $this->language_model->get_all_language_data();	
			
	
	   $this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
	if($this->session->userdata('login_type')=='user')
		{
	$this->load->Model('schoolmodel');
	$data['school']=$this->schoolmodel->getschoolbydistrict();
	if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   	}
	   else
	   {
	    $data['teachers']=false; 
	   }
	  }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
	
		if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}
				
		
		
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/enter_iep_record',$data);
	  
		}
      public function basic_student_information_insert() {

        //print_r($this->input->post('action_home_responsible'));exit;
        $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {
			
			$schools_type = $this->input->post('schools_type');
            $student_id = $this->input->post('student_id');
            $grade_id = $this->input->post('grade_id');
			$date_birth = $this->input->post('birth_date');
            $gender = $this->input->post('gender');
			$limited_english_proficiency = $this->input->post('limited_english_proficiency');
			$ethnic_code = $this->input->post('ethnic_code');
			//step2
			$date = $this->input->post('date');
          	//$dates = $this->input->post('dates');
            $meeting_type = $this->input->post('meeting_type');
           
            //step3
           $home_language = $this->input->post('home_language');
            $student_language = $this->input->post('student_language');
            $alternate_mode_of_communication = $this->input->post('alternate_mode_of_communication');
			$address = $this->input->post('address');
			$city = $this->input->post('city');
			$zip_code = $this->input->post('zip_code');
            //step 4
            $home_telephone = $this->input->post('home_telephone');
			$daytime_telephone = $this->input->post('daytime_telephone');
			$emergency_telephone = $this->input->post('emergency_telephone');
			$school_of_attendance = $this->input->post('school_of_attendance');
			$school_of_residence = $this->input->post('school_of_residence');
			$location_coad = $this->input->post('location_coad');


            $meeting = implode(",", $meeting_type);
			
            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            } else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
		
	if($this->session->userdata("$teacher_id")){
			$teacher_id = $this->session->userdata("$teacher_id");
		} else {
			 $teacher_id = $this->input->post('teacher_id');	
		}
			
		
 
            $data = array('user_type' => $this->session->userdata('login_type'),'user_id' => $user_id,'teacher_id'=>$teacher_id,'schools_type'=>$schools_type,'school_id' => $school_id,'district_id' => $this->session->userdata('district_id'),'student_id' => $student_id, 'grade_id'=>$grade_id,'birth_date' => date('Y-m-d', strtotime(str_replace('-', '/', $date_birth))),'gender'=>$gender,'limited_english_proficiency'=>$limited_english_proficiency,'ethnic_code'=>$ethnic_code,'date'=>date('Y-m-d', strtotime(str_replace('-', '/', $date))),'home_language'=>$home_language,'student_language'=>$student_language,'student_language'=>$student_language,'alternate_mode_of_communication'=>$alternate_mode_of_communication,'address'=>$address,'city'=>$city,'zip_code'=>$zip_code,'home_telephone'=>$home_telephone,'daytime_telephone'=>$daytime_telephone,'emergency_telephone'=>$emergency_telephone,'school_of_attendance'=>$school_of_attendance,'school_of_residence'=>$school_of_residence,'location_coad'=>$location_coad,'meeting_type' =>$meeting);

//print_r($data);exit;	
            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('basic_student_information_form', $data);

        }
   
   if ($result)
	  $user_id = $this->db->insert_id();
	 // print_r($user_id);exit;
	  
   
      $dates_id = $this->input->post('dates_id');
		$dates_description=array();
foreach($dates_id as $dates){
		$dates_description[$dates][0]= $this->input->post('dates_description_'.$dates);	
		}
	
foreach($dates_description as $key=>$value_data)
		{
	$dates_data = array('dates_id'=>$key,'dates_description'=>$value_data[0],'iep_id'=>$user_id);
	//print_r($dates_data);
	$this->load->model('classroommodel');			
    $this->classroommodel->strengths_insert('dates_description',$dates_data);
} 
	    
	  $iep_id=array('iep_id'=>$user_id);
	  $ses=$this->session->set_userdata($iep_id);
	   
	  
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/meeting_attendees/' . $user_id);
	  
    }		
				
function get_student_by_grade(){
	
		$this->load->Model('classroommodel');
		$data['grade'] = $this->classroommodel->get_students_By_grade($this->input->post("student_id"));
		
		echo json_encode($data);
	}
	
	
function getstudentinfo()
	{
		$this->load->Model('classroommodel');
		$data['student'] = $this->classroommodel->getstudentById($this->input->post("student_id"));
		//print_r($data);exit;
		echo json_encode($data);
		
	  }
function get_subject_to_objective_info()
	{
		$this->load->Model('classroommodel');
		$data['objective_data'] = $this->classroommodel->get_subject_By_objective($this->input->post("subject_id"));
		//print_r($data);exit;
		echo json_encode($data);
		
	  }
	
		
		
	function basic_student_meeting_information (){
            	 $data['idname']='implementation';

		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/eligibility_determination',$data);
	  
		}
		
		function develop_iep(){
            	 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/iep_development',$data);
	  
		}	
	
	function edit_develop_iep($id){
            	 $data['idname']='implementation';
		$data['id']=$id;		 
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/edit_iep_development',$data);
	  
		}

	function edit_iep_plan_summary($id){
            	 $data['idname']='implementation';
		$data['id']=$id;		 
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/edit_iep_plan_summary',$data);
	  
		}

	
		function iep_plan_summary(){
            	 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/placement',$data);
	  
		}
		
	function description_of_iep_services($iep_id=''){
            	 $data['idname']='implementation';

				 $data['id'] = $iep_id;
			$this->load->Model('services_grid_model');
            $data['services_grid'] = $this->services_grid_model->get_all_services_grid_data();	 
				 
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/services_grid',$data);
	  
		}
	function update_description_of_iep_services($iep_id=''){
            	 $data['idname']='implementation';

				 $data['id'] = $iep_id;
			$this->load->Model('services_grid_model');
            $data['services_grid'] = $this->services_grid_model->get_all_services_grid_data();

			$this->load->Model('iep_tracker_model');
			$data['description']=$this->iep_tracker_model->get_description_of_iep_services($iep_id);
		 		 
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_description_of_iep_services',$data);
	  
		}	
		
function description_of_iep_services_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $services = $this->input->post('services_grid');
		 $services_grid = implode(",", $services);	
		
            $data = array('iep_id'=>$iep_id,'services_grid'=>serialize($services_grid));

            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('description_of_iep_services',$data);

        }
        
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/iep_agreement/' . $iep_id);
	}
function edit_description_of_iep_services(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $services = $this->input->post('services_grid');
		 $services_grid = implode(",", $services);	
		
            $data = array('iep_id'=>$iep_id,'services_grid'=>serialize($services_grid));

			$this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('description_of_iep_services',$data,$data['iep_id']);

        }
       if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/update_iep_agreement/' . $iep_id);
	  	
		
	}			
		
function determine_student_assessments($iep_id=''){
        $data['idname']='implementation';
		
		$this->load->Model('student_assessments_model');
			$data['assessments']=$this->student_assessments_model->get_all_student_assessments_data();
		
		 $data['id'] = $iep_id;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/testing',$data);
	  
		}
function update_determine_student_assessments($iep_id=''){
        $data['idname']='implementation';
		
		 $data['id'] = $iep_id;
		 
		 $this->load->Model('student_assessments_model');
			$data['assessments']=$this->student_assessments_model->get_all_student_assessments_data();

		 
			$this->load->Model('iep_tracker_model');
			$data['get_determine']=$this->iep_tracker_model->get_determine_student_data($iep_id);

		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_determine_student_assessments',$data);
	  
		}		
		
function determine_student_assessments_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $determine_student = $this->input->post('determine_student_assessments');
		 $determine_student_assessments = implode(",", $determine_student);	
		
            $data = array('iep_id'=>$iep_id,'determine_student_assessments'=>serialize($determine_student_assessments));
			
//print_r($data);exit;	

            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('determine_student_assessments',$data);

        }
        
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/summary_of_iep_plan/' . $iep_id);
	  	
		
	}
	
function edit_determine_student_assessments(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $determine_student = $this->input->post('determine_student_assessments');
		 $determine_student_assessments = implode(",", $determine_student);	
		
            $data = array('iep_id'=>$iep_id,'determine_student_assessments'=>serialize($determine_student_assessments));
			

   $this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('determine_student_assessments',$data,$data['iep_id']);

        }
        
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/update_summary_of_iep_plan/' . $iep_id);
	  	
		
	}	
	
	   public function summary_of_iep_plan_insert() {
		$data['idname'] = 'implementation';
        //   $this->load->model('classroommodel');
       // $result = $this->classroommodel->student_progress_report($login_id);
	    if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
	$effective_date = $this->input->post('effective_date');
    $future_changes_date = $this->input->post('future_changes_date');
	$time_outside = $this->input->post('time_outside');
	$team_exceeds = $this->input->post('team_exceeds');
	
	

    $summary = array( 'effective_date' => date('Y-m-d', strtotime(str_replace('-', '/', $effective_date))),
								'future_changes_date' => date('Y-m-d', strtotime(str_replace('-', '/', $future_changes_date))),
                                'iep_id'=>$iep_id,
                                'time_outside'=>$time_outside,
                                'team_exceeds'=>$team_exceeds,
								
                            );
							
	$this->load->model('classroommodel');			
    $result = $this->classroommodel->strengths_insert('summary_of_iep_plan',$summary);

	    if($result){
			 if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
/* start plan_eligibility */
	$plan_eligibility_id = $this->input->post('plan_eligibility_id');
    $eligibility_effective = $this->input->post('eligibility_effective');
	$eligibility_future_changes = $this->input->post('eligibility_future_changes');
	
	
	$eligibility = array('plan_eligibility_id'=>$plan_eligibility_id,
	'eligibility_effective'=>$eligibility_effective,
	'eligibility_future_changes'=>$eligibility_future_changes,
	'iep_id'=>$iep_id
	);
	 
	
		$this->load->model('classroommodel');			
		$this->classroommodel->strengths_insert('plan_eligibility_data',$eligibility);
	

/* end plan_eligibility */
/* start plan_curriculum */

$plan_curriculum_id = $this->input->post('plan_curriculum_id');
    $curriculum_effective = $this->input->post('curriculum_effective');
	$curriculum_future_changes = $this->input->post('curriculum_future_changes');
	

	$curriculum = array('plan_curriculum_id'=>$plan_curriculum_id,
	'curriculum_effective'=>$curriculum_effective,
	'curriculum_future_changes'=>$curriculum_future_changes,
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$this->classroommodel->strengths_insert('plan_curriculum_data',$curriculum);
	


/* end plan_curriculum */
/* start plan_placement */
	$plan_placement_id = $this->input->post('plan_placement_id');
    $placement_effective = $this->input->post('placement_effective');
	$placement_future_changes = $this->input->post('placement_future_changes');


	$effective = array('plan_placement_id'=>$plan_placement_id,
	'placement_effective'=>$placement_effective,
	'placement_future_changes'=>$placement_future_changes,
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$this->classroommodel->strengths_insert('plan_placement_data',$effective);
	


/* end plan_placement */
/* start instructionl_setting */
$instructionl_setting_id = $this->input->post('instructionl_setting_id');
		$instructionl_setting_effective=array();
		$instructionl_setting_future_changes=array();
		
foreach($instructionl_setting_id as $setting_id){
		
			$instructionl_setting_effective[$setting_id][0]= $this->input->post('instructionl_setting_effective_'.$setting_id);	
			$instructionl_setting_future_changes[$setting_id][0]= $this->input->post('instructionl_setting_future_changes_'.$setting_id);	
		}

foreach($instructionl_setting_effective as $key=>$value)
		{
	$setting_effective = array('instructionl_setting_id'=>$key,
	'instructionl_setting_effective'=>$value[0],
	'instructionl_setting_future_changes'=>$instructionl_setting_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('instructionl_setting_data',$setting_effective);
	
}
/* end instructionl_setting */
/* start additional_factors */
$additional_factors_id = $this->input->post('additional_factors_id');
		$additional_effective=array();
		$additional_future_changes=array();
		
foreach($additional_factors_id as $factors_id){
		
			$additional_effective[$factors_id][0]= $this->input->post('additional_effective_'.$factors_id);	
			$additional_future_changes[$factors_id][0]= $this->input->post('additional_future_changes_'.$factors_id);	
		}

foreach($additional_effective as $key=>$value)
		{
	$effective_data = array('additional_factors_id'=>$key,
	'additional_effective'=>$value[0],
	'additional_future_changes'=>$additional_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('additional_factors_data',$effective_data);
	
}

/* end additional_factors */
/* end accommodation_modifications */


$accommodation_modifications_id = $this->input->post('accommodation_modifications_id');
		$accommodation_modifications_effective=array();
		$accommodation_modifications_future_changes=array();
		
foreach($accommodation_modifications_id as $modifications_id){
		
	$accommodation_modifications_effective[$modifications_id][0]= $this->input->post('accommodation_modifications_effective_'.$modifications_id);	
	$accommodation_modifications_future_changes[$modifications_id][0]= $this->input->post('accommodation_modifications_future_changes_'.$modifications_id);	
		}

foreach($accommodation_modifications_effective as $key=>$value)
	{
	$effective_data = array('accommodation_modifications_id'=>$key,
	'accommodation_modifications_effective'=>$value[0],
	'accommodation_modifications_future_changes'=>$accommodation_modifications_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('accommodation_modifications_data',$effective_data);
	
}
/* end accommodation_modifications */

/* start preparation */
$preparation_id = $this->input->post('preparation_id');
		$preparation_effective=array();
		$preparation_future_changes=array();
		
foreach($preparation_id as $prepa_id){
		
	$preparation_effective[$prepa_id][0]= $this->input->post('preparation_effective_'.$prepa_id);	
	$preparation_future_changes[$prepa_id][0]= $this->input->post('preparation_future_changes_'.$prepa_id);	
		}

foreach($preparation_effective as $key=>$value)
	{
	$effective_data = array('preparation_id'=>$key,
	'preparation_effective'=>$value[0],
	'preparation_future_changes'=>$preparation_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('preparation_data',$effective_data);
	
}
/* end preparation */

	}
	  if ($result)
	        $link = redirect(base_url() . 'implementation/description_of_iep_services/' . $iep_id);
	 }
		
	function summary_of_iep_plan($iep_id=''){
         $data['idname']='implementation';
		 
		  $data['id'] = $iep_id;
		 $this->load->Model('plan_eligibility_model');
            $data['plan_eligibility'] = $this->plan_eligibility_model->get_all_plan_eligibility_data();
			
		 $this->load->Model('plan_curriculum_model');
            $data['curriculum'] = $this->plan_curriculum_model->get_all_plan_curriculum_data();
			
		 $this->load->Model('plan_placement_model');
            $data['placement'] = $this->plan_placement_model->get_all_plan_placement_data();
		
		$this->load->Model('instructionl_setting_model');
            $data['instructionl_setting'] = $this->instructionl_setting_model->get_all_instructionl_setting_data();
		
		$this->load->Model('additional_factors_model');
            $data['additional_factors'] = $this->additional_factors_model->get_all_additional_factors_data();	
		
		$this->load->Model('accommodation_modifications_model');
            $data['accommodation'] = $this->accommodation_modifications_model->get_all_accommodation_modifications_data();	
			
			$this->load->Model('preparation_model');
            $data['preparation'] = $this->preparation_model->get_all_preparation_data();
			
			
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/plan',$data);
	  
		}
function update_summary_of_iep_plan($iep_id=''){
         $data['idname']='implementation';
		 
		  $data['id'] = $iep_id;
		  /*start eligibility */
		 $this->load->Model('plan_eligibility_model');
            $data['plan_eligibility'] = $this->plan_eligibility_model->get_all_plan_eligibility_data();
			
		$this->load->model('iep_tracker_model');
            $data['eligibility'] = $this->iep_tracker_model->get_plan_eligibility($iep_id);	
			
		
			//print_r($data['eligibility']);exit;
/*end eligibility */
/*start curriculum */				
		$this->load->Model('plan_curriculum_model');
            $data['curriculum'] = $this->plan_curriculum_model->get_all_plan_curriculum_data();
		
		$this->load->model('iep_tracker_model');
            $data['plan_curriculum'] = $this->iep_tracker_model->get_plan_curriculum($iep_id);	

				//print_r($data['updateatt_curriculum']);exit;
/*end curriculum */
/*start placement */														
		 $this->load->Model('plan_placement_model');
            $data['placement'] = $this->plan_placement_model->get_all_plan_placement_data();

		$this->load->model('iep_tracker_model');
            $data['plan_placement'] = $this->iep_tracker_model->get_plan_placement($iep_id);	
			
/*end placement */																
/*start instructionl */														
		$this->load->Model('instructionl_setting_model');
            $data['instructionl_setting'] = $this->instructionl_setting_model->get_all_instructionl_setting_data();
	
	$this->load->model('iep_tracker_model');
            $data['plan_instructionl'] = $this->iep_tracker_model->get_instructionl_setting($iep_id);	
foreach($data['instructionl_setting'] as $instructionl_setting){
					foreach($data['plan_instructionl'] as $plan_instructionl){
					if($plan_instructionl->instructionl_setting_id==$instructionl_setting->id){
				$updateatt_inst[$instructionl_setting->id]['instructionl_setting_effective'] = $plan_instructionl->instructionl_setting_effective;
				$updateatt_inst[$instructionl_setting->id]['instructionl_setting_future_changes'] = $plan_instructionl->instructionl_setting_future_changes;
				$updateatt_inst[$instructionl_setting->id]['instructionl_setting'] = $instructionl_setting->instructionl_setting;
					}
				
					}
				}
			$data['updateatt_instructionl'] = $updateatt_inst;
			//print_r($data['updateatt_instructionl']);exit;		
/*end instructionl */		
/*start additional_factors */
		$this->load->Model('additional_factors_model');
            $data['additional_factors'] = $this->additional_factors_model->get_all_additional_factors_data();	

		$this->load->model('iep_tracker_model');
            $data['plan_additional'] = $this->iep_tracker_model->get_additional_factors($iep_id);	
		
		foreach($data['additional_factors'] as $additional_factors){
					foreach($data['plan_additional'] as $plan_additional){
					if($plan_additional->additional_factors_id==$additional_factors->id){
						$updateatt_add[$additional_factors->id]['additional_effective'] = $plan_additional->additional_effective;
						$updateatt_add[$additional_factors->id]['additional_future_changes'] = $plan_additional->additional_future_changes;
						$updateatt_add[$additional_factors->id]['additional_factors'] = $additional_factors->additional_factors;
					}
				
					}
				}
			$data['updateatt_additional'] = $updateatt_add;
		
/*end additional_factors */
/*start accommodation */		
		$this->load->Model('accommodation_modifications_model');
            $data['accommodation'] = $this->accommodation_modifications_model->get_all_accommodation_modifications_data();	

		$this->load->model('iep_tracker_model');
            $data['plan_accommodation'] = $this->iep_tracker_model->get_accommodation_modifications($iep_id);	

		foreach($data['accommodation'] as $accommodation){
					foreach($data['plan_accommodation'] as $plan_accommodation){
					if($plan_accommodation->accommodation_modifications_id==$accommodation->id){
		$updateatt_accom[$accommodation->id]['accommodation_modifications_effective'] = $plan_accommodation->accommodation_modifications_effective;
	$updateatt_accom[$accommodation->id]['accommodation_modifications_future_changes'] = $plan_accommodation->accommodation_modifications_future_changes;
	$updateatt_accom[$accommodation->id]['accommodation_modifications'] = $accommodation->accommodation_modifications;
					}
				
					}
				}
			$data['accommodation_data'] = $updateatt_accom;
		//print_r($data['accommodation']);exit;
/*end accommodation */
/*start preparation */	
			$this->load->Model('preparation_model');
            $data['preparation'] = $this->preparation_model->get_all_preparation_data();

		$this->load->model('iep_tracker_model');
            $data['plan_preparation'] = $this->iep_tracker_model->get_preparation($iep_id);	
		foreach($data['preparation'] as $preparation){
					foreach($data['plan_preparation'] as $plan_preparation){
					if($plan_preparation->preparation_id==$preparation->id){
						$updateatt_prep[$preparation->id]['preparation_effective'] = $plan_preparation->preparation_effective;
						$updateatt_prep[$preparation->id]['preparation_future_changes'] = $plan_preparation->preparation_future_changes;
						$updateatt_prep[$preparation->id]['preparation'] = $preparation->preparation;
					}
				
					}
				}
			$data['preparation_data'] = $updateatt_prep;

/*end preparation */	
/*start summary_of_iep_plan */				
			   $this->load->model('iep_tracker_model');
            $data['summary_of_iep'] = $this->iep_tracker_model->get_summary_of_iep_plan($iep_id);
/*end summary_of_iep_plan */				
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_summary_of_iep_plan',$data);
	  		}	

public function edit_summary_of_iep_plan() {
		$data['idname'] = 'implementation';
        //   $this->load->model('classroommodel');
       // $result = $this->classroommodel->student_progress_report($login_id);
	    if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
	$effective_date = $this->input->post('effective_date');
    $future_changes_date = $this->input->post('future_changes_date');
	$time_outside = $this->input->post('time_outside');
	$team_exceeds = $this->input->post('team_exceeds');
	
	

    $summary = array( 'effective_date' => date('Y-m-d', strtotime(str_replace('-', '/', $effective_date))),
								'future_changes_date' => date('Y-m-d', strtotime(str_replace('-', '/', $future_changes_date))),
                                'iep_id'=>$iep_id,
                                'time_outside'=>$time_outside,
                                'team_exceeds'=>$team_exceeds,
								
                            );
							
	$this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('summary_of_iep_plan',$summary,$summary['iep_id']);

	    if($result){
			 if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
		
		
/* start plan_eligibility */

	$plan_eligibility_id = $this->input->post('plan_eligibility_id');
    $eligibility_effective = $this->input->post('eligibility_effective');
	$eligibility_future_changes = $this->input->post('eligibility_future_changes');


	$eligibility = array('plan_eligibility_id'=>$plan_eligibility_id,
	'eligibility_effective'=>$eligibility_effective,
	'eligibility_future_changes'=>$eligibility_future_changes,
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
	if($this->iep_tracker_model->check_eligibility($iep_id) > 0) {
	$this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('plan_eligibility_data',$eligibility,$eligibility['iep_id']);
	}else{
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('plan_eligibility_data',$eligibility);		
	}
/* end plan_eligibility */
/* start plan_curriculum */
	$plan_curriculum_id = $this->input->post('plan_curriculum_id');
    $curriculum_effective = $this->input->post('curriculum_effective');
	$curriculum_future_changes = $this->input->post('curriculum_future_changes');


	$curriculum = array('plan_curriculum_id'=>$plan_curriculum_id,
	'curriculum_effective'=>$curriculum_effective,
	'curriculum_future_changes'=>$curriculum_future_changes,
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
	
if($this->iep_tracker_model->check_curriculum($iep_id) > 0) {
	$this->load->model('iep_tracker_model');
           $result = $this->iep_tracker_model->iep_update_data('plan_curriculum_data',$curriculum,$curriculum['iep_id']);
	}else{
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('plan_curriculum_data',$curriculum);		
	}



/* end plan_curriculum */
/* start plan_placement */

	$plan_placement_id = $this->input->post('plan_placement_id');
    $placement_effective = $this->input->post('placement_effective');
	$placement_future_changes = $this->input->post('placement_future_changes');


	$effective = array('plan_placement_id'=>$plan_placement_id,
	'placement_effective'=>$placement_effective,
	'placement_future_changes'=>$placement_future_changes,
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
			
	if($this->iep_tracker_model->check_placement($iep_id) > 0) {
	$this->load->model('iep_tracker_model');
          $result = $this->iep_tracker_model->iep_update_data('plan_placement_data',$effective,$effective['iep_id']);
	}else{
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('plan_placement_data',$effective);		
	}		

/* end plan_placement */
/* start instructionl_setting */
$this->load->Model('iep_tracker_model');
	$remove_instructionl = $this->iep_tracker_model->delete_data('instructionl_setting_data',array('iep_id'=>$iep_id));	
if($remove_instructionl){
$instructionl_setting_id = $this->input->post('instructionl_setting_id');
		$instructionl_setting_effective=array();
		$instructionl_setting_future_changes=array();
		
foreach($instructionl_setting_id as $setting_id){
		
			$instructionl_setting_effective[$setting_id][0]= $this->input->post('instructionl_setting_effective_'.$setting_id);	
			$instructionl_setting_future_changes[$setting_id][0]= $this->input->post('instructionl_setting_future_changes_'.$setting_id);	
		}

foreach($instructionl_setting_effective as $key=>$value)
		{
	$setting_effective = array('instructionl_setting_id'=>$key,
	'instructionl_setting_effective'=>$value[0],
	'instructionl_setting_future_changes'=>$instructionl_setting_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('instructionl_setting_data',$setting_effective);
	
}}
/* end instructionl_setting */
/* start additional_factors */
$this->load->Model('iep_tracker_model');
	$remove_additional = $this->iep_tracker_model->delete_data('additional_factors_data',array('iep_id'=>$iep_id));	
	if($remove_additional){
$additional_factors_id = $this->input->post('additional_factors_id');
		$additional_effective=array();
		$additional_future_changes=array();
		
foreach($additional_factors_id as $factors_id){
		
			$additional_effective[$factors_id][0]= $this->input->post('additional_effective_'.$factors_id);	
			$additional_future_changes[$factors_id][0]= $this->input->post('additional_future_changes_'.$factors_id);	
		}

foreach($additional_effective as $key=>$value)
		{
	$effective_data = array('additional_factors_id'=>$key,
	'additional_effective'=>$value[0],
	'additional_future_changes'=>$additional_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('additional_factors_data',$effective_data);
	
}
	}
/* end additional_factors */
/* end accommodation_modifications */
$this->load->Model('iep_tracker_model');
	$remove_accommodation = $this->iep_tracker_model->delete_data('accommodation_modifications_data',array('iep_id'=>$iep_id));	
if($remove_accommodation){
$accommodation_modifications_id = $this->input->post('accommodation_modifications_id');
		$accommodation_modifications_effective=array();
		$accommodation_modifications_future_changes=array();
		
foreach($accommodation_modifications_id as $modifications_id){
		
	$accommodation_modifications_effective[$modifications_id][0]= $this->input->post('accommodation_modifications_effective_'.$modifications_id);	
	$accommodation_modifications_future_changes[$modifications_id][0]= $this->input->post('accommodation_modifications_future_changes_'.$modifications_id);	
		}

foreach($accommodation_modifications_effective as $key=>$value)
	{
	$effective_data = array('accommodation_modifications_id'=>$key,
	'accommodation_modifications_effective'=>$value[0],
	'accommodation_modifications_future_changes'=>$accommodation_modifications_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('accommodation_modifications_data',$effective_data);
	
}}
/* end accommodation_modifications */

/* start preparation */
$this->load->Model('iep_tracker_model');
	$remove_preparation = $this->iep_tracker_model->delete_data('preparation_data',array('iep_id'=>$iep_id));	
	if($remove_preparation){
$preparation_id = $this->input->post('preparation_id');
		$preparation_effective=array();
		$preparation_future_changes=array();
		
foreach($preparation_id as $prepa_id){
		
	$preparation_effective[$prepa_id][0]= $this->input->post('preparation_effective_'.$prepa_id);	
	$preparation_future_changes[$prepa_id][0]= $this->input->post('preparation_future_changes_'.$prepa_id);	
		}

foreach($preparation_effective as $key=>$value)
	{
	$effective_data = array('preparation_id'=>$key,
	'preparation_effective'=>$value[0],
	'preparation_future_changes'=>$preparation_future_changes[$key][0],
	'iep_id'=>$iep_id
	);
	 
	// print_r($eligibility);
		$this->load->model('classroommodel');			
		$result = $this->classroommodel->strengths_insert('preparation_data',$effective_data);
	
}}
/* end preparation */

	}
	  if ($result)
	        $link = redirect(base_url() . 'implementation/update_description_of_iep_services/' . $iep_id);
	 }
	
function iep_eligibility($iep_id=''){
       $data['idname']='implementation';
	   
	   $data['id']	=$iep_id;
	   
	$this->load->Model('eligibility_model');
            $data['eligibility_name'] = $this->eligibility_model->get_all_eligibility_data();
	
		$this->load->Model('external_impact_model');
            $data['external_impact'] = $this->external_impact_model->get_all_external_impact_data();

		$this->load->Model('eligibility_status_model');
            $data['eligibility_status'] = $this->eligibility_status_model->get_all_eligibility_status_data();	
			

		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/eligibility',$data);
	  
		}
	function iep_eligibility_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $eligibility = $this->input->post('eligibility');
			$external_impact_factors = $this->input->post('external_impact_factors');
            $eligibility_status = $this->input->post('eligibility_status');
		
		
            $data = array('iep_id'=>$iep_id,'eligibility'=>$eligibility,'external_impact_factors'=>$external_impact_factors,'eligibility_status'=>$eligibility_status);
			
//print_r($data);exit;	

            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('iep_eligibility', $data);

        }
        
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/summary_of_academic_achievement/' . $iep_id);
		
	}
	    function update_eligibility($iep_id=''){
       $data['idname']='implementation';
	   
	   $data['id']	=$iep_id;
	   
	$this->load->Model('eligibility_model');
            $data['eligibility_name'] = $this->eligibility_model->get_all_eligibility_data();
	
		$this->load->Model('external_impact_model');
            $data['external_impact'] = $this->external_impact_model->get_all_external_impact_data();
			
		$this->load->Model('eligibility_status_model');
            $data['eligibility_status'] = $this->eligibility_status_model->get_all_eligibility_status_data();
			
				
		$this->load->Model('iep_tracker_model');
           $data['retrieve_reports'] = $this->iep_tracker_model->get_all_eligibility_data($iep_id);
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_eligibility',$data);
	  
		}	
	function edit_eligibility_data(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $eligibility = $this->input->post('eligibility');
			$external_impact_factors = $this->input->post('external_impact_factors');
            $eligibility_status = $this->input->post('eligibility_status');
	
	        $data = array('iep_id'=>$iep_id,'eligibility'=>$eligibility,'external_impact_factors'=>$external_impact_factors,'eligibility_status'=>$eligibility_status);
	

            $this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('iep_eligibility',$data,$data['iep_id']);
        }
if ($result)
// print_r($iep_record_id);exit;
            //$link = redirect(base_url() . 'implementation/update_summary_of_academic_achievement/' . $result);
			
			  $link = redirect(base_url() . 'implementation/update_comprehensive_academic_achievement/' . $iep_id);
	}
		
		
	function summary_of_academic_achievement($iep_id=''){
            	 $data['idname']='implementation';
				 
				$data['id']= $result;
				 $this->load->Model('schoolmodel');
			$data['subjects'] = $this->schoolmodel->getallsubjects();
			
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/academic_achievement',$data);
	  
        }
function comprehensive_academic_achievement($iep_id=''){
	
            	 $data['idname']='implementation';
				 
				 $data['id']= $iep_id;
				 
				 $this->load->Model('performance_area_model');
			$data['performance'] = $this->performance_area_model->get_all_performance_area_data(); 
			
		 $this->load->Model('assessment_monitoring_model');
			$data['assessment'] = $this->assessment_monitoring_model->get_all_assessment_monitoring_data();
			
		$this->load->Model('state_district_assessment_model');
			$data['state_district'] = $this->state_district_assessment_model->get_all_state_district_data();	 	
		
		
			

		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/description_of_academic_achievement',$data);
	  
        }
function update_comprehensive_academic_achievement($iep_id=''){
	
            	 $data['idname']='implementation';
				 
				 $data['id']= $iep_id;
				 
				 $this->load->Model('performance_area_model');
			$data['performance'] = $this->performance_area_model->get_all_performance_area_data(); 
			
		 $this->load->Model('assessment_monitoring_model');
			$data['assessment'] = $this->assessment_monitoring_model->get_all_assessment_monitoring_data();
			
		$this->load->Model('state_district_assessment_model');
			$data['state_district'] = $this->state_district_assessment_model->get_all_state_district_data();
			
		$this->load->Model('iep_tracker_model');
            $data['retrieve_reports'] = $this->iep_tracker_model->get_all_comprehensive_academic_achievement_data($iep_id);	
		

			
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_comprehensive_academic_achievement',$data);
	  
        }		
function comprehensive_academic_achievement_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
       
		$comprehensive_academic_id = $this->input->post('comprehensive_academic_id');
		$performance_area=array();
		$assessment_monitoring=array();
		$state_district_assessment=array();
		$current_performance=array();
		
foreach($comprehensive_academic_id as $comprehensive_academic){
	
			$performance_area[$comprehensive_academic][0]= $this->input->post('performance_area_'.$comprehensive_academic);	
			$assessment_monitoring[$comprehensive_academic][0]= $this->input->post('assessment_monitoring_'.$comprehensive_academic);	
			$state_district_assessment[$comprehensive_academic][0]= $this->input->post('state_district_assessment_'.$comprehensive_academic);	
			$current_performance[$comprehensive_academic][0]= $this->input->post('current_performance_'.$comprehensive_academic);	
			}
	
foreach($performance_area as $key=>$value_data)
		{
	$academic_data = array('comprehensive_academic_id'=>$key,'performance_area'=>$value_data[0],'assessment_monitoring'=>$assessment_monitoring[$key][0],'state_district_assessment'=>$state_district_assessment[$key][0],'current_performance'=>$current_performance[$key][0],'iep_id'=>$iep_id);
	
		
	$this->load->model('classroommodel');			
    $result = $this->classroommodel->strengths_insert('comprehensive_academic_achievement',$academic_data);
		}
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/english_language_development/' . $iep_id);
	  	
		}
	}
function edit_comprehensive_academic_achievement(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		$this->load->Model('iep_tracker_model');
	$remove_data = $this->iep_tracker_model->delete_data('comprehensive_academic_achievement',array('iep_id'=>$iep_id));
	
		
  	$comprehensive_academic_id = $this->input->post('comprehensive_academic_id');
		$performance_area=array();
		$assessment_monitoring=array();
		$state_district_assessment=array();
		$current_performance=array();
		
foreach($comprehensive_academic_id as $comprehensive_academic){
	
			$performance_area[$comprehensive_academic][0]= $this->input->post('performance_area_'.$comprehensive_academic);	
			$assessment_monitoring[$comprehensive_academic][0]= $this->input->post('assessment_monitoring_'.$comprehensive_academic);	
			$state_district_assessment[$comprehensive_academic][0]= $this->input->post('state_district_assessment_'.$comprehensive_academic);	
			$current_performance[$comprehensive_academic][0]= $this->input->post('current_performance_'.$comprehensive_academic);	
			}
	
foreach($performance_area as $key=>$value_data)
		{
	$academic_data = array('comprehensive_academic_id'=>$key,'performance_area'=>$value_data[0],'assessment_monitoring'=>$assessment_monitoring[$key][0],'state_district_assessment'=>$state_district_assessment[$key][0],'current_performance'=>$current_performance[$key][0],'iep_id'=>$iep_id);
		
			
			$this->load->model('classroommodel');			
    $result = $this->classroommodel->strengths_insert('comprehensive_academic_achievement',$academic_data);

    }
	    
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/update_english_language_development/' . $iep_id);
	  	
		
	}			
}
		
	function create_student_goals ($iep_id=''){
            	 $data['idname']='implementation';
				 
	/*if($this->session->userdata('login_type')=='user'){
	redirect(base_url());
        }*/
        $data['type']='Create';
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
		}
	if($this->session->userdata('login_type')=='user')
	  	{
	$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
		}
		
	$this->load->Model('iep_tracker_model');
		$data['subjects'] = $this->iep_tracker_model->get_subject_in_standard();

	
		$data['id']= $iep_id;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/new_goals',$data);
	   }
	   
function get_subject_by_strand(){
		$this->load->Model('iep_tracker_model');
		$this->load->Model('standarddatamodel');
		$get_all_standard = $this->iep_tracker_model->get_subject_By_standards($this->input->post("subject_id"));

	$grades = $data['grades']=$this->standarddatamodel->getActiveGrades($get_all_standard[0]['subject_id'],$get_all_standard[0]['grade']);

	$data['strands']=$this->standarddatamodel->getstrand($grades[0]['grade_id'],$grades[0]['standard_id']);

		echo json_encode($data);
	}

function update_student_goals ($iep_id=''){
            	 $data['idname']='implementation';
/*if($this->session->userdata('login_type')=='user'){
	redirect(base_url());
        }
*/		
        $data['type']='Create';
        $data['idname']='implementation';
        $data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
		}
	if($this->session->userdata('login_type')=='user')
	  	{
	$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
	}
	$this->load->Model('iep_tracker_model');
		$data['student_data']=$this->iep_tracker_model->get_all_student_goals_data($iep_id);
//print_r($data['student_data']);exit;
	$this->load->Model('iep_tracker_model');
		$data['subjects'] = $this->iep_tracker_model->get_subject_in_standard();

		$data['id']= $iep_id;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_student_goals',$data);
	}	   
		
    public function create_new_goals_step1(){
        $data['type'] = $this->input->post('plan_type');
        $data['idname']='implementation';
        $this->load->Model('iep_tracker_model');
     	
		if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$data['teacher_id'] = $teacher_id = $this->input->post('teacher_id');	
		}
		if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
               $subject_id = $this->input->post('subject_id');	
			   
        $get_all_standard = $this->iep_tracker_model->get_all_standard_data($subject_id);
		//print_r($get_all_standard);exit;
        if ($get_all_standard==false){
            $this->session->set_flashdata('error','No lesson at mentioned date & time');
            redirect(base_url().'implementation/create_student_goals');
        } else {
			
            $this->load->Model('time_tablemodel');
            $this->load->Model('custom_differentiatedmodel');
            $this->load->Model('standarddatamodel');
            $data['grades']=$this->standarddatamodel->getActiveGrades($get_all_standard[0]->subject_id,$get_all_standard[0]->grade);
            if($data['grades']==''){
                $this->session->set_flashdata('error','Entered time & date does not have proper lesson plan.');
                redirect(base_url().'implementation/new_goals');
              exit;  
            }
		
            //print_r($lesson_week[0]["subject_id"]);exit;
            $this->load->Model('standarddatamodel');
            
            $grades = $data['grades']=$this->standarddatamodel->getActiveGrades($get_all_standard[0]->subject_id,$get_all_standard[0]->grade);

            $data['strands']=$this->standarddatamodel->getstrand($grades[0]['grade_id'],$grades[0]['standard_id']);

			$data['custom_diff']=$this->custom_differentiatedmodel->getallplans();

            
            $data['subject'] = $get_all_standard[0]->subject;
            $data['subject_id'] = $get_all_standard[0]->subject_id;

           $data['id']= $iep_id;
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('implementation/create_new_goal_step2',$data);
        }
        
        
    }	
	
	
	public function update_new_goals_step1(){
		$data['type'] = $this->input->post('plan_type');
        $data['idname']='implementation';
        $this->load->Model('lessonplanmodel');
        $cdataarr = explode('-',$this->input->post('cdate'));
        
        $cdate = $cdataarr[2].'-'.$cdataarr[0].'-'.$cdataarr[1];    //date("Y-m-d",strtotime($this->input->post('cdate')));
        $chkdate = $cdataarr[1].'-'.$cdataarr[0].'-'.$cdataarr[2];
        $data['dates']=$this->week_from_monday_day($chkdate);
        
      $this->load->Model('time_tablemodel');
	   $this->load->Model('custom_differentiatedmodel');
	    $this->load->Model('periodmodel');
       
        $ctimearr = explode(' ',$this->input->post('ctime'));
        if ($ctimearr[1]=='AM'){
            $ctime = date('h:i:s',strtotime($ctimearr[0]));
        } else {
            $ctime = date('H:i:s',strtotime($ctimearr[0])+43200);
        }
		
		if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$data['teacher_id'] = $teacher_id = $this->input->post('teacher_id');	
		}
		if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
               
        $teacher_lesson_week_plan = $this->lessonplanmodel->lessonplansbyteacherbytimetable($teacher_id,$cdate,$cdate,$ctime);
        
        if ($teacher_lesson_week_plan==false){
            $this->session->set_flashdata('error','No lesson at mentioned date & time');
            redirect(base_url().'implementation/create_student_goals');
        } else {
            $this->load->Model('time_tablemodel');
            $this->load->Model('custom_differentiatedmodel');
            $this->load->Model('standarddatamodel');
		
            
             $lesson_week=$this->lessonplanmodel->getteacherplaninfo($teacher_lesson_week_plan[0]['lesson_week_plan_id']);
             
             
            $data['grades']=$this->standarddatamodel->getActiveGrades($lesson_week[0]["subject_id"],$lesson_week[0]["grade"]);
            if($data['grades']==''){
                $this->session->set_flashdata('error','Entered time & date does not have proper lesson plan.');
                redirect(base_url().'implementation/new_goals');
              exit;  
            }
            //print_r($lesson_week[0]["subject_id"]);exit;
            $lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
            
            $lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
           
             foreach($lessonplans as $lessonplansval){
             foreach($lessonplansub as $lessonplanssubval){
               $data['lessontype'][$lessonplansval['tab']]["lesson_plan_id"] = $lessonplansval['lesson_plan_id']; 
                    //if($lessonplansval)
                    if($lessonplansval['lesson_plan_id']==$lessonplanssubval['lesson_plan_id']){
                        $data['lessontype'][$lessonplansval['tab']][$lessonplanssubval['lesson_plan_sub_id']] =  $lessonplanssubval['subtab'];
                    }
                }
            }
            $this->load->Model('standarddatamodel');
            
            $grades = $data['grades']=$this->standarddatamodel->getActiveGrades($lesson_week[0]['subject_id'],$lesson_week[0]['grade']);
            $data['strands']=$this->standarddatamodel->getstrand($grades[0]['grade_id'],$grades[0]['standard_id']);
            $data['custom_diff']=$this->custom_differentiatedmodel->getallplans();
            
            $data['subject'] = $teacher_lesson_week_plan[0]['subject_name'];
            $data['subject_id'] = $teacher_lesson_week_plan[0]['subject_id'];
            $data['period'] = date('h:i A',strtotime($teacher_lesson_week_plan[0]['starttime'])).'-'.date('h:i A',strtotime($teacher_lesson_week_plan[0]['endtime']));
            $data['cdate'] = $this->input->post('cdate');
            $data['endtime'] = date('h:i A',strtotime($teacher_lesson_week_plan[0]['endtime']));
            $data['starttime'] = date('h:i A',strtotime($teacher_lesson_week_plan[0]['starttime']));
            $data['lesson_week_plan_id'] = $teacher_lesson_week_plan[0]['lesson_week_plan_id'];
            $data['period_id'] = $lesson_week[0]['period_id'];
           $data['id']= $iep_id;
		   $this->load->Model('iep_tracker_model');
			$data['student_data']=$this->iep_tracker_model->get_all_student_goals_data($iep_id);
	
	
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('implementation/update_new_goal_step2',$data);
        }
        
        
    }
	
function create_student_goals_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
			$subject_id = $this->input->post('subject_id');
           	$grade = $this->input->post('grade');
			$standard_id = $this->input->post('standard_id');
			$standarddata_id = $this->input->post('standarddata_id');
			$strand = $this->input->post('strand');
			$standarddata = $this->input->post('standarddata');
			$incremental_date_1 = $this->input->post('incremental_date_1');
			$standarddisplay = $this->input->post('standarddisplay');
			$incremental_date_2 = $this->input->post('incremental_date_2');
			$gol_standard = $this->input->post('gol_standard');
			
			
		    $data = array('iep_id'=>$iep_id,'subject_id'=>$subject_id,'grade'=>$grade,'standard_id'=>$standard_id,'standarddata_id'=>$standarddata_id,'strand'=>$strand,'standarddata'=>$standarddata,'incremental_date_1'=>date('Y-m-d', strtotime(str_replace('-', '/', $incremental_date_1))),'standarddisplay'=>$standarddisplay,'incremental_date_2'=> date('Y-m-d', strtotime(str_replace('-', '/', $incremental_date_2))),'gol_standard'=>$gol_standard);

		$this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('create_student_goals', $data);
		
	if ($result)
	 		// print_r($iep_record_id);exit;
	        $link = redirect(base_url() . 'implementation/determine_student_assessments/' . $iep_id);
	 }else if ($this->input->post('add')) {
	 
	       if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
			$subject_id = $this->input->post('subject_id');
           	$grade = $this->input->post('grade');
			$standard_id = $this->input->post('standard_id');
			$standarddata_id = $this->input->post('standarddata_id');
			$strand = $this->input->post('strand');
			$standarddata = $this->input->post('standarddata');
			$incremental_date_1 = $this->input->post('incremental_date_1');
			$standarddisplay = $this->input->post('standarddisplay');
			$incremental_date_2 = $this->input->post('incremental_date_2');
			
		    $data = array('iep_id'=>$iep_id,'subject_id'=>$subject_id,'grade'=>$grade,'standard_id'=>$standard_id,'standarddata_id'=>$standarddata_id,'strand'=>$strand,'standarddata'=>$standarddata,'incremental_date_1'=>date('Y-m-d', strtotime(str_replace('-', '/', $incremental_date_1))),'standarddisplay'=>$standarddisplay,'incremental_date_2'=> date('Y-m-d', strtotime(str_replace('-', '/', $incremental_date_2))));
	
		$this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('create_student_goals', $data);
		
	if ($result)
	 		// print_r($iep_record_id);exit;
	        $link = redirect(base_url() . 'implementation/create_student_goals/' . $iep_id);
	 
}
	}

function edit_student_goals(){
	   $data['idname'] = 'implementation';

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		$student_goals_id = $this->input->post('student_goals_id');
          	$subject_id = $this->input->post('subject_id');
           	$grade = $this->input->post('grade');
			$standard_id = $this->input->post('standard_id');
			$standarddata_id = $this->input->post('standarddata_id');
			$strand = $this->input->post('strand');
			$standarddata = $this->input->post('standarddata');
			$incremental_date_1 = $this->input->post('incremental_date_1');
			$standarddisplay = $this->input->post('standarddisplay');
			$incremental_date_2 = $this->input->post('incremental_date_2');
			$gol_standard = $this->input->post('gol_standard');
			
		
		
            $data = array('student_goals_id'=>$student_goals_id,'iep_id'=>$iep_id,'subject_id'=>$subject_id,'grade'=>$grade,'standard_id'=>$standard_id,'standarddata_id'=>$standarddata_id,'strand'=>$strand,'standarddata'=>$standarddata,'incremental_date_1'=>date('Y-m-d', strtotime(str_replace('-', '/', $incremental_date_1))),'standarddisplay'=>$standarddisplay,'incremental_date_2'=> date('Y-m-d', strtotime(str_replace('-', '/', $incremental_date_2))),'gol_standard'=>$gol_standard);

	//print_r($data);exit;	
			  $this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_goal_update_data('create_student_goals',$data,$data['student_goals_id']);
        
		echo json_encode($data);
		exit;	
		
	/*  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/update_determine_student_assessments/' . $iep_id);
	  	
		*/
	}
			
	
        function week_from_monday_day($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		$dates[$i]['day'] = $i;
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
			
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}		
function iep_agreement($iep_id=''){
            	 $data['idname']='implementation';
				 $data['id']=$iep_id;
				 $this->load->Model('indicate_parent_consent_model');
			$data['indicate_parent']=$this->indicate_parent_consent_model->get_all_indicate_parent_data();
			
				$this->load->Model('categorize_issues_model');
			$data['categorize_issues']=$this->categorize_issues_model->get_all_categorize_issues_data();

			
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/consent',$data);
	  
        }	
function update_iep_agreement($iep_id=''){
            	 $data['idname']='implementation';
				 $data['id']=$iep_id;
				 $this->load->Model('indicate_parent_consent_model');
			$data['indicate_parent']=$this->indicate_parent_consent_model->get_all_indicate_parent_data();
			
				$this->load->Model('categorize_issues_model');
			$data['categorize_issues']=$this->categorize_issues_model->get_all_categorize_issues_data();

 $this->load->Model('iep_tracker_model');
            $data['agreement'] = $this->iep_tracker_model->get_agreement($iep_id);

			
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_iep_agreement',$data);
	  
        }	
		
function iep_agreement_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $indicate_parent_consent = $this->input->post('indicate_parent_consent');
			$categorize = $this->input->post('categorize_issues');
			$summarize = $this->input->post('summarize');
			
		 $categorize_issues = implode(",", $categorize);	
		
            $data = array('iep_id'=>$iep_id,'indicate_parent_consent'=>$indicate_parent_consent,'categorize_issues'=>serialize($categorize_issues),'summarize'=>$summarize);

            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('iep_agreement',$data);

        }
        
	   if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'implementation/iep_tracker_main_page');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }  	
		
	}	


function edit_iep_agreement(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $indicate_parent_consent = $this->input->post('indicate_parent_consent');
			$categorize = $this->input->post('categorize_issues');
			$summarize = $this->input->post('summarize');
			
		 $categorize_issues = implode(",", $categorize);	
		
            $data = array('iep_id'=>$iep_id,'indicate_parent_consent'=>$indicate_parent_consent,'categorize_issues'=>serialize($categorize_issues),'summarize'=>$summarize);
			
			  $this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('iep_agreement',$data,$data['iep_id']);

        }
        
	   if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'implementation/iep_tracker_main_page');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }  	
		
	}	
		
		function meeting_attendees($iep_id=''){
            	 $data['idname']='implementation';
			$data['id']	=$iep_id;
			$this->load->Model('attendance_page_model');
            $data['attendance'] = $this->attendance_page_model->get_all_attendance_data();
			
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/attendance_page',$data);
	  
        }
	function update_meeting_attendees($id=''){
            	 $data['idname']='implementation';
			$data['id']	=$id;
			$this->load->Model('attendance_page_model');
            $data['attendance'] = $this->attendance_page_model->get_all_attendance_data();
//print_r($data['attendance']);exit;
			 $this->load->Model('iep_tracker_model');
            $data['meeting_attendees'] = $this->iep_tracker_model->get_meeting_attendees($id);
			
			//print_r($data['meeting_attendees']);exit;
			foreach($data['attendance'] as $attendance){
				
					foreach($data['meeting_attendees'] as $meeting_attendance){
					if($meeting_attendance->team_member_id==$attendance->id){
						$updateatt[$attendance->id]['print_name'] = $meeting_attendance->print_name;
						$updateatt[$attendance->id]['signature'] = $meeting_attendance->signature;
						$updateatt[$attendance->id]['team_member_name'] = $attendance->team_member_name;
					}
				
					}
				}
			$data['updateatt'] = $updateatt;
			//
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_attendance',$data);
	  
        }	
function meeting_attendees_update(){
		if ($this->input->post('submit')) {
		
		if($this->input->post('iep_id')!=0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
	$this->load->Model('iep_tracker_model');
	$remove_data = $this->iep_tracker_model->delete_data('iep_meeting_attendees',array('iep_id'=>$iep_id));
		
	if ($remove_data)
	{	
		$team_member_id = $this->input->post('team_member_id');
		$print_name=array();
		$signature=array();
		
foreach($team_member_id as $team_member){
	
			$print_name[$team_member][0]= $this->input->post('print_name_'.$team_member);	
			$signature[$team_member][0]= $this->input->post('signature_'.$team_member);	
			}
	
foreach($print_name as $key=>$value_data)
		{
	$team_data = array('team_member_id'=>$key,'print_name'=>$value_data[0],'signature'=>$signature[$key][0],'iep_id'=>$iep_id);
	//print_r($team_data);
	$this->load->model('classroommodel');			
    $result = $this->classroommodel->strengths_insert('iep_meeting_attendees',$team_data);
	
	
}	
 if ($result)
            $link = redirect(base_url() . 'implementation/update_eligibility/' . $iep_id);
	}	
		}
	}
			
	function meeting_attendees_insert(){
		if ($this->input->post('submit')) {
		
		if($this->input->post('iep_id')!=0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
		$team_member_id = $this->input->post('team_member_id');
		$print_name=array();
		$signature=array();
		
foreach($team_member_id as $team_member){
	
			$print_name[$team_member][0]= $this->input->post('print_name_'.$team_member);	
			$signature[$team_member][0]= $this->input->post('signature_'.$team_member);	
			}
	
foreach($print_name as $key=>$value_data)
		{
	$team_data = array('team_member_id'=>$key,'print_name'=>$value_data[0],'signature'=>$signature[$key][0],'iep_id'=>$iep_id);
	//print_r($team_data);
		
	$this->load->model('classroommodel');			
    $result = $this->classroommodel->strengths_insert('iep_meeting_attendees',$team_data);
}	
 if ($result)
            $link = redirect(base_url() . 'implementation/iep_eligibility/' . $iep_id);
	}	

		
		}
	
		
		
	function responsible_person(){
            	 $data['idname']='implementation';
			$this->load->model('responsible_person_model');
			$data['responsible']=$this->responsible_person_model->get_all_responsible_person_data();	
				 
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/responsible_person',$data);
	  
        }
		function english_language_development($iep_id=''){
            	 $data['idname']='implementation';
			$data['id']	=$iep_id;	 
			$this->load->model('english_langauge_development_model');
			$data['listening']=$this->english_langauge_development_model->get_all_english_langauge_listening();	
			$data['speaking']=$this->english_langauge_development_model->get_all_english_langauge_speaking();	 
			$data['readnig']=$this->english_langauge_development_model->get_all_english_langauge_readnig();	
			$data['writing']=$this->english_langauge_development_model->get_all_english_langauge_writing();	 
				
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/english_language_development',$data);
	  
        }
	function update_english_language_development($iep_id=''){
            	 $data['idname']='implementation';
			$data['id']	=$iep_id;	 
			$this->load->model('english_langauge_development_model');
			$data['listening']=$this->english_langauge_development_model->get_all_english_langauge_listening();	
			$data['speaking']=$this->english_langauge_development_model->get_all_english_langauge_speaking();	 
			$data['readnig']=$this->english_langauge_development_model->get_all_english_langauge_readnig();	
			$data['writing']=$this->english_langauge_development_model->get_all_english_langauge_writing();	
			
			$this->load->Model('iep_tracker_model');
            $data['retrieve_reports'] = $this->iep_tracker_model->get_all_english_language_development_data($iep_id);
				
				//print_r($data['retrieve_reports']);exit;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_english_language_development',$data);
	  
        }		
function english_language_development_insert(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $listen = $this->input->post('listening');
			$speak = $this->input->post('speaking');
            $read = $this->input->post('readnig');
			$writ = $this->input->post('writing');
			$english_language = $this->input->post('english_language_development_goals');
			
			
			 $listening = implode(",", $listen);
			 $speaking = implode(",", $speak);
			 $readnig = implode(",", $read);
			 $writing = implode(",", $writ);
		
		
        $data = array('iep_id'=>$iep_id,'listening'=>$listening,'speaking'=>$speaking,'readnig'=>$readnig,'writing'=>$writing,'english_language_development_goals'=>$english_language);

//print_r($data);exit;	

			$this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('english_language_development', $data);
    }
        
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/create_student_goals/' . $iep_id);
		
	}
function edit_english_language_development(){
	   $data['idname'] = 'implementation';

        if ($this->input->post('submit')) {

        if($this->input->post('iep_id')!= 0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		
            $listen = $this->input->post('listening');
			$speak = $this->input->post('speaking');
            $read = $this->input->post('readnig');
			$writ = $this->input->post('writing');
			$english_language = $this->input->post('english_language_development_goals');
			
			 $listening = implode(",", $listen);
			 $speaking = implode(",", $speak);
			 $readnig = implode(",", $read);
			 $writing = implode(",", $writ);
		
		
        $data = array('iep_id'=>$iep_id,'listening'=>$listening,'speaking'=>$speaking,'readnig'=>$readnig,'writing'=>$writing,'english_language_development_goals'=>$english_language);

//print_r($data);exit;	
			
			   $this->load->model('iep_tracker_model');
            $result = $this->iep_tracker_model->iep_update_data('english_language_development',$data,$data['iep_id']);
    }
        
	  if ($result)
	
	 // print_r($iep_record_id);exit;
            $link = redirect(base_url() . 'implementation/update_student_goals/' . $iep_id);
	  	
		
	}


	function skill_area(){
            	 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/skill_area',$data);
	  
        }
		
		
        
        function observation_full_report(){
            	 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/observation_full_report',$data);
	  
        }
        
        function observation_sectional_report(){
            	 $data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/observation_sectional_report',$data);
	  
        }
        
       function get_student_eld_report() {
		   
        $data['idname']='implementation';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');
        
         

        $data['studentname'] = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select	

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select	

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);		 		 	
        $this->load->Model('lessonplanmodel');
        $this->load->Model('eldrubricmodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }
        $studentresult = $data['studentresult'] = $this->lessonplanmodel->geteldweekdata($month, $year, $student_id, $ndays);
        $valueplans = $data['valueplans'] = $this->eldrubricmodel->getAllsubgroupsbydistrict();
        $data['student_id'] = $student_id;
         for($pj=0;$pj<count($weeks);$pj++)
		{
		  if(!empty($studentresult))
		  {
		  foreach($valueplans as $key=>$valupl)
			{
			$exist=0;
		  foreach($studentresult as $studenvalue)
		  {
		    
			
			
			if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab'])
			{
				
				$notify[$key][$pj]=intval($studenvalue['count']);
				$exist=1;
			}
			
			
		   }
		   if($exist==0)
			{
			  $notify[$key][$pj]=0;
			}
		  }
		  
		  }
		  else
		  {
		  foreach($valueplans as $key=>$valupl)
			{
		    $notify[$key][$pj]=0;
			}
		  }
		 $weeksdata[]=$weeks[$pj]['week']	;
		}
		
			
	  
		foreach($valueplans as $key=>$valupl)
			{
			
		
		//$MyData->addPoints($notify[$key],$valupl['tab']);
//		$$chart->series[] = array('name' => $valupl['tab'],'data' => $notify[$key]);	
		$high['series'][] = array('name' => $valupl['tab'],'data' => $notify[$key]);
		
		}
                $high['xAxis']['categories'] = $weeksdata;
                $high['chart']['type'] = "column";			
			$high['title']['text'] = "MONTHLY CLASSWORK PERFORMANCE";
//                        $high['xAxis']['labels']['rotation'] = -50;	
                        $high['yAxis']['min'] = 0;
			$high['yAxis']['allowDecimals'] =false;
                        $high['yAxis']['title']['text'] = "";
                        $high['yAxis']['stackLabels']['enabled'] = 1;
                        $high['yAxis']['stackLabels']['style']['fontWeight'] = "bold";
                        $high['legend']['layout'] = 'horizontal';
                        $high['legend']['align'] = 'top';
                        $high['legend']['verticalAlign'] = 'top';
                        $high['legend']['x'] = 20;
                        $high['legend']['y'] = 15;
                        $high['credits']['enabled'] = false;
                        $high['legend']['itemStyle'] = array("fontWeight"=>"normal");
                         $high['xAxis']['labels']['autoRotation'] = array(-70);
//                        echo json_encode($high);exit;
                        
                        $myfile = fopen(WORKSHOP_FILES."mediagenerated/newfile_$student_id.json", "w") or die("Unable to open file!");
                        $txt = json_encode($high);
                        fwrite($myfile, $txt);
                        fclose($myfile);
                        
                        unlink("/var/www/html/system/application/views/inc/logo/eld_$student_id.png");
                        
                        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/newfile_$student_id.json -outfile /var/www/html/system/application/views/inc/logo/eld_$student_id.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
                         exec($command,$output);
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('implementation/value_eld_report', $data);
        // $this->load->view('teacherself/value_eld_view',$data);
    }
    
    function week_from_monday($date, $days, $sk = false) {
        // Assuming $date is in format DD-MM-YYYY
        list($day, $month, $year) = explode("-", $date);

        // Get the weekday of the given date
        $wkday = date('l', mktime('0', '0', '0', $month, $day, $year));


        switch ($wkday) {
            case 'Monday': $numDaysToMon = 0;
                break;
            case 'Tuesday': $numDaysToMon = 1;
                break;
            case 'Wednesday': $numDaysToMon = 2;
                break;
            case 'Thursday': $numDaysToMon = 3;
                break;


            case 'Friday': $numDaysToMon = 4;
                break;
            case 'Saturday': $numDaysToMon = 5;
                break;
            case 'Sunday': $numDaysToMon = 6;
                break;
        }

        // Timestamp of the monday for that week
        $monday = mktime('0', '0', '0', $month, $day, $year);

        $seconds_in_a_day = 86400;

        // Get date for 7 days from Monday (inclusive)

        $j = $numDaysToMon;

        for ($i = 0; $i < $days; $i++) {




            if ($j > 6) {
                $j = 0;
            }

            $dates[$i]['date'] = date('m/d/Y', $monday + ($seconds_in_a_day * $i));

            if ($j == 0) {
                $dates[$i]['week'] = 'Monday';
            }
            if ($j == 1) {
                $dates[$i]['week'] = 'Tuesday';
            }
            if ($j == 2) {
                $dates[$i]['week'] = 'Wednesday';
            }
            if ($j == 3) {
                $dates[$i]['week'] = 'Thursday';
            }
            if ($j == 4) {
                $dates[$i]['week'] = 'Friday';
            }
            if ($j == 5) {
                $dates[$i]['week'] = 'Saturday';
            }
            if ($j == 6) {
                $dates[$i]['week'] = 'Sunday';
            }
            $j++;
        }

        if ($sk == true && $days != 28) {
            $days++;
            for ($i = $days; $i <= 35; $i++) {

                $dates[$i]['week'] = '';
                $dates[$i]['date'] = '';
            }
        }
        return $dates;
    }
 public function check_lesson_plan(){
        
		 $this->load->Model('iep_tracker_model');
        $cdataarr = explode('-',$this->input->post('cdate'));
        
        $cdate = $cdataarr[2].'-'.$cdataarr[0].'-'.$cdataarr[1];    //date("Y-m-d",strtotime($this->input->post('cdate')));
        $chkdate = $cdataarr[1].'-'.$cdataarr[0].'-'.$cdataarr[2];
        $data['dates']=$this->week_from_monday_day($chkdate);
        foreach($data['dates'] as $key=>$val)
	  {
	    $data['dates'][$key]['week']=$val['week'];
		$cdate=$val['date'];
		$cdate1=explode('-',$cdate);
		$data['dates'][$key]['date']=$cdate1[1].'-'.$cdate1[2].'-'.$cdate1[0];
		$lessonplans=$this->iep_tracker_model->gettimetablebyteacher($val['day']);
		if($lessonplans!=false && (strtotime($cdate)>=strtotime(date('Y-m-d'))))
		{
			
			foreach($lessonplans as $lessonval)
			{
				$lessonexists=$this->iep_tracker_model->check_lessonplan_exists($cdate,$lessonval['start'],$lessonval['end']);
				if($lessonexists==false)
				{					
					$this->iep_tracker_model->add_planbytimetable($lessonval['start'],$lessonval['end'],$cdate,$lessonval['grade_id']);
				}
			}
		}
	}
          $lessonplans=$this->iep_tracker_model->gettimetablebyteacher($val['day']);
          $html = '<select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;">';
          foreach($lessonplans as $lessonplansval){
              $html .= '<option value="'.$lessonplansval['start'].'">'.$lessonplansval['start'].'-'.$lessonplansval['end'].'</option>';
          }
     $html .='</select>';
          echo $html;

    }  
	
	
function retrieve_iep_record()
	{	
	
		$data['idname']='implementation';
		 
		 $this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');			
	if($this->session->userdata('login_type')=='user')
		{
	$this->load->Model('schoolmodel');
	$data['school']=$this->schoolmodel->getschoolbydistrict();
	if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	    $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   	}
	   else
	   {
	    $data['teachers']=false; 
	   }
	  }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}	
	
			if($this->session->userdata("login_type")=='user')
		  {
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}		
				
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/retrieve_iep',$data);
	  
	}	
	
public function retrieve_iep_data(){
        $data['idname'] = 'implementation';
        if ($this->input->post('submit')) {

            $student_id = $this->input->post('student_id');
			$teacher_id = $this->input->post('teacher_id');

			//$date = date('Y/m/d', strtotime(str_replace('-', '/', $this->input->post('date'))));

		 $this->retrieve_iep_record_step_2($student_id,$teacher_id);
        }
    }
		 

function retrieve_iep_record_step_2($student_id,$teacher_id)
	{	
		$data['idname']='implementation';
			$this->load->Model('iep_tracker_model');
	if ($this->session->userdata('login_type') == 'teacher') {          
            $data['update_report'] = $this->iep_tracker_model->get_iep_tracker_data_1($student_id);
	 }else{
			$data['update_report'] = $this->iep_tracker_model->get_iep_tracker_data_by_teacher($student_id,$teacher_id);
	 }
			 
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/retrieve_iep_record',$data);
	  
	}

function iep_pdf_record($iep_id,$user_id) {
	
	
 $this->load->Model('iep_tracker_model');
            $data['success_data'] = $this->iep_tracker_model->get_iep_tracker_pdf($iep_id);
			
		$this->load->Model('iep_tracker_model');
            $data['retrieve_reports'] = $this->iep_tracker_model->get_all_comprehensive_academic_achievement_data($iep_id);		
			
			$data['goal_data'] = $this->iep_tracker_model->get_all_iep_goal_data($iep_id);		

//			print_r($data['success_data']);exit;
			$this->load->model('english_langauge_development_model');
		$data['listening']=$this->english_langauge_development_model->get_all_english_langauge_listening();	
			$data['speaking']=$this->english_langauge_development_model->get_all_english_langauge_speaking();	 
			$data['readnig']=$this->english_langauge_development_model->get_all_english_langauge_readnig();	
			$data['writing']=$this->english_langauge_development_model->get_all_english_langauge_writing();	
			
	//$this->load->Model('iep_tracker_model');
      //      $data['agreement'] = $this->iep_tracker_model->get_agreement_data($iep_id);

			$this->load->Model('iep_tracker_model');
            $data['agreement'] = $this->iep_tracker_model->get_agreement($iep_id);

	  /*start eligibility */
		 $this->load->Model('plan_eligibility_model');
            $data['plan_eligibility'] = $this->plan_eligibility_model->get_all_plan_eligibility_data();
			
		$this->load->model('iep_tracker_model');
            $data['eligibility'] = $this->iep_tracker_model->get_plan_eligibility($iep_id);	
		
			foreach($data['plan_eligibility'] as $plan_eligibility){
					foreach($data['eligibility'] as $eligibility){
					if($eligibility->plan_eligibility_id==$plan_eligibility->id){
						$updateatt[$plan_eligibility->id]['eligibility_effective'] = $eligibility->eligibility_effective;
						$updateatt[$plan_eligibility->id]['eligibility_future_changes'] = $eligibility->eligibility_future_changes;
						$updateatt[$plan_eligibility->id]['plan_eligibility'] = $plan_eligibility->plan_eligibility;
					}
				
					}
				}
			$data['updateatt'] = $updateatt;
			//print_r($data['updateatt']);exit;
/*end eligibility */
/*start curriculum */				
		$this->load->Model('plan_curriculum_model');
            $data['curriculum'] = $this->plan_curriculum_model->get_all_plan_curriculum_data();
		$this->load->model('iep_tracker_model');
            $data['plan_curriculum'] = $this->iep_tracker_model->get_plan_curriculum($iep_id);	

				foreach($data['curriculum'] as $curriculum){
					foreach($data['plan_curriculum'] as $plan_curriculum){
					if($plan_curriculum->plan_curriculum_id==$curriculum->id){
						$updateatt_curr[$curriculum->id]['curriculum_effective'] = $plan_curriculum->curriculum_effective;
						$updateatt_curr[$curriculum->id]['curriculum_future_changes'] = $plan_curriculum->curriculum_future_changes;
						$updateatt_curr[$curriculum->id]['plan_curriculum'] = $curriculum->plan_curriculum;
					}
				
					}
				}
			$data['updateatt_curriculum'] = $updateatt_curr;
		//print_r($data['updateatt_curriculum']);exit;
/*end curriculum */
/*start placement */														
		 $this->load->Model('plan_placement_model');
            $data['placement'] = $this->plan_placement_model->get_all_plan_placement_data();
		$this->load->model('iep_tracker_model');
            $data['plan_placement'] = $this->iep_tracker_model->get_plan_placement($iep_id);	
foreach($data['placement'] as $placement){
					foreach($data['plan_placement'] as $plan_placement){
					if($plan_placement->plan_placement_id==$placement->id){
						$updateatt_plac[$placement->id]['placement_effective'] = $plan_placement->placement_effective;
						$updateatt_plac[$placement->id]['placement_future_changes'] = $plan_placement->placement_future_changes;
						$updateatt_plac[$placement->id]['plan_placement'] = $placement->plan_placement;
					}
				
					}
				}
			$data['updateatt_placement'] = $updateatt_plac;
			
/*end placement */																
/*start instructionl */														
		$this->load->Model('instructionl_setting_model');
            $data['instructionl_setting'] = $this->instructionl_setting_model->get_all_instructionl_setting_data();
	
	$this->load->model('iep_tracker_model');
            $data['plan_instructionl'] = $this->iep_tracker_model->get_instructionl_setting($iep_id);	
foreach($data['instructionl_setting'] as $instructionl_setting){
					foreach($data['plan_instructionl'] as $plan_instructionl){
					if($plan_instructionl->instructionl_setting_id==$instructionl_setting->id){
				$updateatt_inst[$instructionl_setting->id]['instructionl_setting_effective'] = $plan_instructionl->instructionl_setting_effective;
				$updateatt_inst[$instructionl_setting->id]['instructionl_setting_future_changes'] = $plan_instructionl->instructionl_setting_future_changes;
				$updateatt_inst[$instructionl_setting->id]['instructionl_setting'] = $instructionl_setting->instructionl_setting;
					}
				
					}
				}
			$data['updateatt_instructionl'] = $updateatt_inst;
			//print_r($data['updateatt_instructionl']);exit;		
/*end instructionl */		
/*start additional_factors */
		$this->load->Model('additional_factors_model');
            $data['additional_factors'] = $this->additional_factors_model->get_all_additional_factors_data();	

		$this->load->model('iep_tracker_model');
            $data['plan_additional'] = $this->iep_tracker_model->get_additional_factors($iep_id);	
		
		foreach($data['additional_factors'] as $additional_factors){
					foreach($data['plan_additional'] as $plan_additional){
					if($plan_additional->additional_factors_id==$additional_factors->id){
						$updateatt_add[$additional_factors->id]['additional_effective'] = $plan_additional->additional_effective;
						$updateatt_add[$additional_factors->id]['additional_future_changes'] = $plan_additional->additional_future_changes;
						$updateatt_add[$additional_factors->id]['additional_factors'] = $additional_factors->additional_factors;
					}
				
					}
				}
			$data['updateatt_additional'] = $updateatt_add;
		
/*end additional_factors */
/*start accommodation */		
		$this->load->Model('accommodation_modifications_model');
            $data['accommodation'] = $this->accommodation_modifications_model->get_all_accommodation_modifications_data();	

		$this->load->model('iep_tracker_model');
            $data['plan_accommodation'] = $this->iep_tracker_model->get_accommodation_modifications($iep_id);	

		foreach($data['accommodation'] as $accommodation){
					foreach($data['plan_accommodation'] as $plan_accommodation){
					if($plan_accommodation->accommodation_modifications_id==$accommodation->id){
		$updateatt_accom[$accommodation->id]['accommodation_modifications_effective'] = $plan_accommodation->accommodation_modifications_effective;
	$updateatt_accom[$accommodation->id]['accommodation_modifications_future_changes'] = $plan_accommodation->accommodation_modifications_future_changes;
	$updateatt_accom[$accommodation->id]['accommodation_modifications'] = $accommodation->accommodation_modifications;
					}
				
					}
				}
			$data['accommodation_data'] = $updateatt_accom;
		//print_r($data['accommodation']);exit;
/*end accommodation */
/*start preparation */	
			$this->load->Model('preparation_model');
            $data['preparation'] = $this->preparation_model->get_all_preparation_data();

		$this->load->model('iep_tracker_model');
            $data['plan_preparation'] = $this->iep_tracker_model->get_preparation($iep_id);	
		foreach($data['preparation'] as $preparation){
					foreach($data['plan_preparation'] as $plan_preparation){
					if($plan_preparation->preparation_id==$preparation->id){
						$updateatt_prep[$preparation->id]['preparation_effective'] = $plan_preparation->preparation_effective;
						$updateatt_prep[$preparation->id]['preparation_future_changes'] = $plan_preparation->preparation_future_changes;
						$updateatt_prep[$preparation->id]['preparation'] = $preparation->preparation;
					}
				
					}
				}
			$data['preparation_data'] = $updateatt_prep;

/*end preparation */	
/*start summary_of_iep_plan */				
			   $this->load->model('iep_tracker_model');
            $data['summary_of_iep'] = $this->iep_tracker_model->get_summary_of_iep_plan($iep_id);
/*end summary_of_iep_plan */
    
		$this->load->Model('attendance_page_model');
            $data['attendance'] = $this->attendance_page_model->get_all_attendance_data();


			 $this->load->Model('iep_tracker_model');
            $data['meeting_attendees'] = $this->iep_tracker_model->get_meeting_attendees($iep_id);
			//print_r($data['meeting_attendees']);exit;
			foreach($data['attendance'] as $attendance){
				
					foreach($data['meeting_attendees'] as $meeting_attendance){
					if($meeting_attendance->team_member_id==$attendance->id){
						$updateatt[$attendance->id]['print_name'] = $meeting_attendance->print_name;
						$updateatt[$attendance->id]['signature'] = $meeting_attendance->signature;
						$updateatt[$attendance->id]['team_member_name'] = $attendance->team_member_name;
					}
				
					}
				}
			$data['updateatt'] = $updateatt;
	/*start dates*/	
	$this->load->Model('dates_model');
            $data['dates'] = $this->dates_model->get_all_dates_data();
			
				 $this->load->Model('indicate_parent_consent_model');
			$data['indicate_parent']=$this->indicate_parent_consent_model->get_all_indicate_parent_data();
			
				$this->load->Model('categorize_issues_model');
			$data['categorize_issues']=$this->categorize_issues_model->get_all_categorize_issues_data();

			$this->load->Model('meeting_type_model');
            $data['meeting_type'] = $this->meeting_type_model->get_all_meeting_type_data();
			
			 $this->load->Model('iep_tracker_model');
            $data['dates_description'] = $this->iep_tracker_model->get_dates_data($iep_id);
//			print_r($data['meeting_attendees']);exit;
			foreach($data['dates'] as $dates){
				
					foreach($data['dates_description'] as $dates_description){
					if($dates_description->dates_id==$dates->id){
						$dates_name[$dates->id]['dates_description'] = $dates_description->dates_description;
						$dates_name[$dates->id]['dates'] = $dates->dates;
		
					}
				
					}
				}
			$data['dates_data'] = $dates_name;
			//print_r($data['dates_data']);exit;

	/*end dates*/
	  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
		
		$this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(25);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(25);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }

       
        $data['view_path'] = $this->config->item('view_path');
   // echo $str = $this->load->view('implementation/iep_pdf_record',$data,true);exit;
	
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('implementation/iep_pdf_record', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }
function academic_achievement_insert(){

		if ($this->input->post('submit')) {
		
		if($this->input->post('iep_id')!=0){
			$iep_id = $this->input->post('iep_id');	
		} else {
			$iep_id = $this->session->userdata("iep_id");
		}
		$subject = $this->input->post('subject_id');
		 $subject_id = implode(",", $subject);
		
		$objective = $this->input->post('objective');
		$objective_id = $this->input->post('objective_id');
		$objective_report=array();


foreach($objective_id as $objective_data){
		$elementarr = explode('|',$objective_data);
			$objective_name[$objective_data][0]= $this->input->post('objective_report_'.$elementarr[0]);	
		}

foreach($objective_name as $key=>$value_data){
	
			$elementarr = explode('|',$key);
			$achievement_data = array('iep_id'=>$iep_id,'subject_id'=>$elementarr[1],'objective'=>$elementarr[0],'objective_report'=>$value_data[0]);

		
	$this->load->model('classroommodel');			
    $result = $this->classroommodel->strengths_insert('summary_of_academic_achievement',$achievement_data);
}//	exit;

            $link = redirect(base_url() . 'implementation/comprehensive_academic_achievement/' . $iep_id);
	}	

		
		}

	function update_summary_of_academic_achievement($result=''){
            	 $data['idname']='implementation';
				 
				$data['id']= $result;
				 	$this->load->Model('classroommodel');
				 $this->load->Model('schoolmodel');
			$data['subjects'] = $this->schoolmodel->getallsubjects();
	
	$this->load->Model('iep_tracker_model');
            $data['achievement'] = $this->iep_tracker_model->get_summary_of_academic_achievement($result);
//print_r($data['achievement']);exit;
			foreach($data['subjects'] as $subjects){
					foreach($data['achievement'] as $achievement){
						
				if($achievement->subject_id == $subjects->subject_id){
						$academic_achievement[$subjects->subject_id]['objective'] = $achievement->objective;
						$academic_achievement[$subjects->subject_id]['objective_report'] = $achievement->objective_report;
						$academic_achievement[$subjects->subject_id]['subject_name'] = $subjects->subject_name;
					}
			}
		}
		
		$data['academic'] = $academic_achievement;
			//print_r($data['academic']);exit;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('implementation/update_summary_of_academic_achievement',$data);
	  
        }
function get_student_number_by_grade(){

		$this->load->Model('classroommodel');
		$data['grade'] = $this->classroommodel->get_students_number_By_grade($this->input->post("student_number"),$this->input->post("lastname"),$this->input->post("firstname"));
		echo json_encode($data);
	}	

  public function getStudentsByTeacher(){

 	$this->load->Model('iep_tracker_model');
			$students = $this->iep_tracker_model->getStudentByTeacherName($this->input->get('teacher_id'),$this->input->get('q'));
                        //echo $this->db->last_query();exit;
			
	if($students!=false)
		{	
		foreach($students as $students_val)
			{

			$sname = $students_val->firstname.' '.$students_val->lastname.' - '.$students_val->student_number;			
			$student_id = $students_val->student_id;
			
			echo "$sname|$student_id\n";	
			}
		}
		else
		{
			echo 'No Students Found.|';
		}
		
		}

function getparentinfo($student_goals_id)
	{
	if(!empty($student_goals_id))
		  {
		$this->load->Model('iep_tracker_model');
		$data['student_goal']=$this->iep_tracker_model->getparentById($student_goals_id);
		$data['student_goal']=$data['student_goal'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
		
}