<?php
class Cron extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		
		
	}
	
function dailyjob()
{
   $dates=$this->week_from_monday(date('d-m-Y'));
   $fromdate=$dates[0]['date'];
   $todate=$dates[6]['date'];
   $this->load->Model('reportmodel');
   $this->load->Model('goalplanmodel');
   $this->load->Model('scheduletaskmodel');
   
   $tasks=$this->reportmodel->getalltotaltask($fromdate,$todate);
   $teacheremail=array();
	 
   if($tasks!=false)
   {
     $teacheremail=array();
	 $i=0;
	 foreach($tasks as $taskvalue)
	 {
	    if($taskvalue['task']==3)
		{
		  $goal=$this->goalplanmodel->goalstatus($taskvalue['schedule_week_plan_id']);
		  if($goal==false)
		  {
		    $teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];
			$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
		  $teacheremail[$i]['endtime']=$end2;
		  }
		
		}
		if($taskvalue['task']==2)
		{
		  $observerstatus=$this->goalplanmodel->observationplanstatus($taskvalue['schedule_week_plan_id']);
		  if($observerstatus==false)
		  {
		    $teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];
			$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
		  $teacheremail[$i]['endtime']=$end2;
		  
		  }
		
		}
		if($taskvalue['task']==1)
		{
            $lesson=$this->goalplanmodel->lessonplanstatus($taskvalue['schedule_week_plan_id']);
			if($lesson[0]['status']=='')
			{
				$teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];
			$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
		  $teacheremail[$i]['endtime']=$end2;
			
			}
		
		}
		
		  
	 
       $i++;
	 }
   
   
   }
   
   if($teacheremail!='')
   {
     foreach($teacheremail as $teachervalue)
	 {
	   if($this->valid_email($teachervalue['email']))
			{
			 $email=$teachervalue['email'];
			$teacher_name=$teachervalue['name'];
			$taskd=$teachervalue['task'];
			$date=$teachervalue['date'];
			$starttime=$teachervalue['starttime'];
			$endtime=$teachervalue['endtime'];
			
			
	   /*  sending mail to Teacher */
	   
	   $subject ="Workshop-Events Pending For $date ";
	$message = "<html>
					<head>
					  <title>Workshop-Events Pending For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $teacher_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>
<tr><td width='30px'><b>Start Time:</b></td>
					   <td> $starttime</td></tr>					   
<tr><td width='30px'><b>End Time:</b></td>
					   <td> $endtime</td></tr>					   					   
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Workshop 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
					
					
	//Additional headers
	
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	 
	 
	 
	 
	 
	 }
   
   
   }
   
   $lessonplantasks=$this->reportmodel->getalllassonplantask();
   $lessonteacheremail=array();
   if($lessonplantasks!=false)
   {
     $lessonteacheremail=array();
	 $i=0;
	 $lessonweek_plan_id=0;
	 foreach($lessonplantasks as $taskvalue)
	 {
	    if($taskvalue['emailnotifylesson']==1)
		{
		$teacher_lesson_id=$taskvalue['teacher_id'];
		
            
			$lessonteacheremail[$teacher_lesson_id]['email']=$taskvalue['email'];
			$lessonteacheremail[$teacher_lesson_id]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$lessonteacheremail[$teacher_lesson_id]['date']=$taskvalue['date'];
			//$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $lessonteacheremail[$teacher_lesson_id]['task']='Lesson Plan Book';
			if($lessonweek_plan_id!=$taskvalue['lesson_week_plan_id'])
			{
			$i++;
			$start1=explode(':',$taskvalue['starttime']);
			$end1=explode(':',$taskvalue['endtime']);
		
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $lessonteacheremail[$teacher_lesson_id]['time'][$i]['starttime']=$start2;
		  $lessonteacheremail[$teacher_lesson_id]['time'][$i]['endtime']=$end2;
		   $this->load->Model('dist_subjectmodel');
		   $subjectdata=$this->dist_subjectmodel->getdist_subjectById($taskvalue['subject_id']);
		   
		   $lessonteacheremail[$teacher_lesson_id]['time'][$i]['Subject']=$subjectdata[0]['subject_name'];
		
		}	
		$lessonteacheremail[$teacher_lesson_id]['time'][$i][$taskvalue['tab']]=$taskvalue['subtab'];
		 
				
				
		$lessonweek_plan_id=$taskvalue['lesson_week_plan_id'];
		
	 }
       
	 }
   
   
   }
   
   if($lessonteacheremail!='')
   {
     foreach($lessonteacheremail as $teachervalue)
	 {
	   if($this->valid_email($teachervalue['email']))
			{
			 $email=$teachervalue['email'];
			$teacher_name=$teachervalue['name'];
			$taskd=$teachervalue['task'];
			$date=$teachervalue['date'];
			
			
	   /*  sending mail to Teacher */
	   
	   $subject ="Workshop-Lesson Plan For  $date ";
	$message = "<html>
					<head>
					  <title>Workshop-Lesson Plan For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Lesson Plan</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $teacher_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>			  
					  
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>					   
					  <tr>
					  <td colspan='2'>
					  <b>Lesson Plan Details</b>
					  </td>
					  </tr>
					  ";
					  $j=1;
					  foreach($teachervalue['time'] as $value)
					  {
					  $message.="<tr>
					  <td colspan='2'>
					  <b>Lesson Plan $j :</b>
					  </td>					  
					  </tr>				  
					  ";
					  
					  foreach($value as $keyvalul=>$eachlesson)
					  {
					    if($keyvalul=='starttime')
						{
						  $keyvalul='Start Time';
						}
						if($keyvalul=='endtime')
						{
						  $keyvalul='End Time';
						}
						$message.="<tr><td width='30px'><b>$keyvalul:</b></td>
					   <td>$eachlesson</td></tr>";
					  
					  
					  }
					  $message.="<tr><td colspan='2'></td></tr>";
					  
					  $j++;
					  }
					  $message.="<tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Workshop 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
					
					
	//Additional headers
	
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	
	mail($email,$subject,$message,$headers);
	}
	 
	 
	 
	 
	 
	 }
   
   
   }

}

function hourlyjob()
{
   
   if(date('H')!=23)
   {
    $starttime=(date('H')+1).':00';
    $endtime=(date('H')+1).':59';
   }
   else
   {
   $starttime=date('H').':00';
    $endtime=(date('H')+1).':59';
   
   }
   
   
   
   $this->load->Model('reportmodel');
   $this->load->Model('goalplanmodel');
   $this->load->Model('scheduletaskmodel');
   
   $tasks=$this->reportmodel->getalltotaltaskhourly($starttime,$endtime);
   $teacheremail=array();
	 $observeremail=array();
   if($tasks!=false)
   {
     
	 $i=0;
	 foreach($tasks as $taskvalue)
	 {
	    if($taskvalue['task']==3)
		{
		  $goal=$this->goalplanmodel->goalstatus($taskvalue['schedule_week_plan_id']);
		  if($goal==false)
		  {
		    $teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];
			$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
		
		  }
		
		}
		if($taskvalue['task']==2)
		{
		  $observerstatus=$this->goalplanmodel->observationplanstatus($taskvalue['schedule_week_plan_id']);
		  if($observerstatus==false)
		  {
		    $teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];
			$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
		  
		  }
		
		}
		if($taskvalue['task']==1)
		{
            $lesson=$this->goalplanmodel->lessonplanstatus($taskvalue['schedule_week_plan_id']);
			if($lesson[0]['status']=='')
			{
				$teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
			
			}
		
		}if($taskvalue['task']==5)
		{
		  
		  //if($observerstatus==false)
		  {
		    $observerdata=$this->observermodel->getobserverById($taskvalue['role_id']);
			
			$observeremail[$i]['email']=$observerdata[0]['email'];
			$observeremail[$i]['name']=$observerdata[0]['observer_name'];
			
			
			
			$teacheremail[$i]['email']=$taskvalue['email'];
			$teacheremail[$i]['name']=$taskvalue['firstname'].' '.$taskvalue['lastname'];
			$teacheremail[$i]['date']=$taskvalue['date'];
			$taskdata=$this->scheduletaskmodel->getplanById($taskvalue['task']);
		    $teacheremail[$i]['task']=$taskdata[0]['task'];
			$start1=explode(':',$taskvalue['starttime']);
		$end1=explode(':',$taskvalue['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		
		  $teacheremail[$i]['starttime']=$start2;
		  
		  }
		
		}
	 
       $i++;
	 }
   
   
   }
   
   
   if($teacheremail!='')
   {
     foreach($teacheremail as $teachervalue)
	 {
	   if($this->valid_email($teachervalue['email']))
			{
			 $email=$teachervalue['email'];
			$teacher_name=$teachervalue['name'];
			$taskd=$teachervalue['task'];
			$date=$teachervalue['date'];
			$starttime=$teachervalue['starttime'];
			
			
	   /*  sending mail to Teacher */
	    $subject ="Workshop-Events Pending For $date ";
	$message = "<html>
					<head>
					  <title>Workshop-Events Pending For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $teacher_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>
<tr><td width='30px'><b>Start Time:</b></td>
					   <td> $starttime</td></tr>					   					   
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Workshop 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
	//Additional headers
	
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	 
	 
	 
	 
	 
	 }
   
   
   }
   if($observeremail!='')
   {
     foreach($observeremail as $key=>$observervalue)
	 {
	   if($this->valid_email($observervalue['email']))
			{
			 $email=$observervalue['email'];
			$observername=$observervalue['name'];
			$taskd=$teacheremail[$key]['task'];
			$date=$teacheremail[$key]['date'];
			$starttime=$teacheremail[$key]['starttime'];
			$teachername=$teacheremail[$key]['name'];
			
			
	   /*  sending mail to Teacher */
	    $subject ="Workshop-Events Pending For $date ";
	$message = "<html>
					<head>
					  <title>Workshop-Events Pending For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $observer_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  <tr>
					   <td width='30px'><b>Assigned To:</b>
					   </td>
					   <td>
					   $teachername </td>
					   </tr>
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>	
<tr><td width='30px'><b>Start Time:</b></td>
					   <td> $starttime</td></tr>					   					   
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='https://login.ueisworkshop.com'>https://login.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Workshop 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
	  
	//Additional headers
	
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	 
	 
	 
	 
	 
	 }
   
   
   }

}

function consolidated()
{
  $d=date('Y-m-d');
  $this->load->Model('parentmodel');
  $parentdata = $this->parentmodel->getparentdata($d);
  if($parentdata!=false)
  {
  
  foreach($parentdata as $parentvalue)
  {
  
  $taskdata='';
	$sendemail=0;
	$email=$parentvalue['email'];
	  $emailsub=$parentvalue['emailsub'];
	  $name=$parentvalue['parentsname'];
				$namesub=$parentvalue['parentsnamesub'];
	$teacherdata = $this->parentmodel->getteacherdata($d,$parentvalue['parents_id']);
	if($teacherdata!=false)
	{
	
	foreach($teacherdata as $teachervalue)
	{
	
	$taskdata.='<tr><td>&nbsp;</td></tr>';
	$taskdata.='<tr><td>Teacher Name:</td><td>'.$teachervalue['firstname'].' '.$teachervalue['lastname'].'</td></tr>';
	
	
	$pdata=$this->parentmodel->get_students_all_parent($teachervalue['teacher_id'],$parentvalue['parents_id']);
  foreach($pdata as $pvalue)
  {
	 $classwork='';
	  $homework='';
	
	$this->load->Model('lessonplanmodel');
	$this->load->Model('lessonplansubmodel');
	$result = $this->lessonplanmodel->getstudentdata($pvalue['teacher_student_id']);
	$classresult = $this->lessonplanmodel->getstudentclassworkdata($pvalue['teacher_student_id']);
	$homeworkresult=$this->lessonplanmodel->getstudenthomeworkdata($pvalue['teacher_student_id']);
	
	
	$date=date('Y-m-d');
	if($result!=false)
	{
	//echo '<pre>';
	
	//print_r($result);
	//exit;
	foreach($result as $resultvalue)
	{
	 
	  
	  $task=explode(',',$resultvalue['task']);
	  $time_interval=explode(',',$resultvalue['time_interval']);
	  
	 $date=$resultvalue['current_date'];
				
				$studentname=$resultvalue['studentname'];
				$standard=$resultvalue['standard'];
				$taskdata.='<tr><td>&nbsp;</td></tr>';
				$taskdata.="<tr><td>Student Name:</td><td> $studentname </td></tr>";
				$taskdata.="<tr><td>Standard:</td><td> $standard </td></tr>";
				$subjectname=$resultvalue['subtab'];
        if($classresult!=false)
		{
			foreach($classresult as $classvalue) {
			if($classvalue['lesson_plan_sub_id']==$resultvalue['lesson_plan_sub_id'])
			{
			  $classwork=$classvalue['tab'];
			
			
			
			
			}
			
			
			}
		}
		if($homeworkresult!=false)
		{
			foreach($homeworkresult as $homevalue) {
			if($homevalue['lesson_plan_sub_id']==$resultvalue['lesson_plan_sub_id'])
			{
			   $homework=$homevalue['tab'];
			
			
			
			
			}
			
			
			}
		}
		
	  foreach($task as $taskvalue)
	  {
	    /*if($taskvalue!='Not')
		{
		 $sendemail=1;
		
		}*/
		$taskd[]=$taskvalue;
	  
	  }
	  $taskdata.="<tr><td>Subject:</td><td>Timings:</td><td>B&L</td><td>Classwork Proficiency</td><td>Homework</td></tr>"; 
				$subco=1;
				$taskcount=count($time_interval);
				foreach($time_interval as $keyt=>$timevalue) { 
				
				if($subco==1)
				{
				$taskdata.="<tr><td rowspan=".$taskcount."> $subjectname </td><td>$timevalue</td><td>".$task[$keyt]."</td><td rowspan=".$taskcount.">$classwork</td><td rowspan=".$taskcount.">$homework</td></tr>"; 
				}else{
				
				$taskdata.="<tr><td>$timevalue</td><td>".$task[$keyt]."</td></tr>"; 
				}
				$subco++;
				
				
				}  
				
				
				
	  
	}
	
	}
	else
	{
	
	if($homeworkresult!=false)
		{
			 
			//$homeworkresult=$this->lessonplanmodel->getstudenthomeworkdataall($pvalue['teacher_student_id']);
			$homeworkresult=$this->lessonplanmodel->getstudenthomeworkdata($pvalue['teacher_student_id']);
			
			//$studentresulthead = $this->lessonplansubmodel->getlessonplansubById(1);
			if($homeworkresult!=false)
			{
			$jkk=1;
			//foreach($studentresulthead as $studentresultheadvalue)
			{
			$exists=0;
			foreach($homeworkresult as $homevalue) {
			 if($jkk==1)
			 {
			 $standard=$homevalue['standard'];
			 $taskdata.='<tr><td>&nbsp;</td></tr>';
			 $taskdata.="<tr><td>Student Name:</td><td> ".$homevalue['firstname']." ".$homevalue['lastname']." </td></tr>";
			 $taskdata.="<tr><td>Standard:</td><td> $standard </td></tr>";
			 $taskdata.="<tr><td>Subject:</td><td>Homework</td></tr>"; 
			 
			}

			 //if($homevalue['subtab']==$studentresultheadvalue['subtab'])
			 {
			 			$exists=1;
						$taskdata.="<tr><td > ".$homevalue['subtab']."</td><td>".$homevalue['tab']."</td></tr>"; 
			}
			
			$jkk++;
			
			
			}
			
			if($exists==0)
			{
			
			
			
			//$taskdata.="<tr><td > ".$studentresultheadvalue['subtab']."</td><td>&nbsp;</td></tr>"; 
			
			}
		
	       }
	        }
	
	
	
	
	}
	
	
	
	}
	  
	 // if($sendemail==1)
	  { 
	    
		  
	  
	  }
	
	
      
  }
  }
  
  }
  
  if($this->valid_email($email))
			{
				
				$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;	
				$headers .= 'MIME-Version: 1.0'.PHP_EOL;
				$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
				$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
				$subject ="Behavior & Learning Details For $date";
				$message = "<html><head><title>Behavior & Learning Details For $date</title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> Details </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' border='0' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> Greetings $name,</td></tr>
				  $taskdata
				<tr> <td colspan='2'> Powered By, </td></tr><tr><td colspan='2'> Workshop </td></tr></table></body></html>";
	            
			 mail($email,$subject,$message,$headers);
			   
	  
	        }  
			
			if($this->valid_email($emailsub))
			{
				
				$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;	
				$headers .= 'MIME-Version: 1.0'.PHP_EOL;
				$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
				$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
				$subject =" Behavior & Learning Details For $date";
			    $message = "<html><head><title>Behavior & Learning Details For $date</title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> Details </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> Greetings $namesub,</td></tr>
				  $taskdata
				<tr> <td colspan='2'> Powered By, </td></tr><tr><td colspan='2'> Workshop </td></tr></table></body></html>";
	           
			   mail($emailsub,$subject,$message,$headers);
			   
	  
	        }
			//echo $message;
			//exit;
  }
  
  }
    
	


}

function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}

function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}

function printviewmail()
	{
	
	
	          $se=date('m-d-Y');
			  $data['selectdate']=$se;
	   
	   
	    $this->load->Model('lessonplanmodel');
		$lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$lessonplansub=$this->lessonplanmodel->getalllessonplanssub();
		
		$datevar=explode('-',$data['selectdate']);
		$fromdate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
		$todate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
	  
	    $data['fromdate']=$data['selectdate'];	  
	    $data['todate']=$data['selectdate'];
	  
	    
		$weekday = date('l', strtotime($fromdate));
		
	    $data['dates'][$data['selectdate']]['week']=$weekday;		
		$data['dates'][$data['selectdate']]['date']=$data['selectdate'];
	     
	    $teachers=$this->lessonplanmodel->getlessonplansallbydate($fromdate,$todate);
		//print_r($teachers);
		//exit;
	   if($teachers!=false)
	   {
	     foreach($teachers as $teachervalue)
		 {
			
			$getlessonplans=$this->getlessonplansbyteacher($teachervalue['teacher_id'],$fromdate,$todate);
			if($this->valid_email($teachervalue['email']))
			{
			 $email=$teachervalue['email'];
			$teachername=$teachervalue['firstname'];
			
			
			
	   /*  sending mail to Teacher */
	    $subject ="Workshop-Events  For $fromdate ";
	$message = "<html>
					<head>
					  <title>Workshop-Events  For $fromdate </title>
					</head>
					<body><table >
					  <tr>
					  <td >
					  Greetings $teachername,					  
					  </td>
					  </tr></table>";
                    
		//print_r($dates);
		
		foreach($data['dates'] as $val)
		
		{
		$show=array();
		$sh=0;
		$k=0;
	   $widthp=(count($lessonplans)+3)*100; 
	   $widthp.='px';
		$message.="<table align='center' style='width:$widthp;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;' cellpadding='0' cellspacing='0'>
  <tr>
    <td colspan=";
	 $clses=count($lessonplans)+6;  
	$message.="$clses align='center' bgcolor='#999999' style='color:#FFF'><b>Date:".$val['date']."&nbsp;&nbsp;&nbsp;Week:".$val['week']."</b></td>
  </tr>
  <tr align='center' style='color:#333;'>
    <td width='100px' bgcolor='#CCCCCC'>Time</td>
	 <td width='100px' bgcolor='#CCCCCC'>Subject</td>
	";
    
	$j=5;
	if($lessonplans!=false){
	 foreach($lessonplans as $plan)
	 {
	 $j++;
	
	$message.="<td width='100px' bgcolor='#CCCCCC'>".$plan['tab']."</td>";
    } 
	//$message.="<td width='100px' bgcolor='#CCCCCC'></td>";
	
	
	 } 
	 $message.='<td width="100px" bgcolor="#CCCCCC">Standard Deatails</td>
    <td width="100px" bgcolor="#CCCCCC">Differentiated Instruction</td>';
 $message.=" </tr>
    <tr align='center'  >
	<td colspan=".$j.">
	<table   border='1' id=".$val['date']." >";
     if($getlessonplans!=false)
	{
	
	$le=0;
	  $je=0;
	 // echo "<pre>";
	  //print_r($getlessonplans);
	  foreach($getlessonplans as $key=>$getplan)
	  {
      if( $val['date']==$getplan['date'])
	{	  
	  
	  
	  $je++;
	  if($le!=$getplan['lesson_week_plan_id'])
		{	
		$les=0;
	     if($je==1)
		 { 
			 $lehd=$key;
			$message.="<tr align='center'>";
		 
		 }
		 else
		 {
		 
		 if($lehd==0)
		  {
		    $lehd=0;
		  
		  }
		 if($val['date']>=date('m-d-Y'))
		 {
		
		  
		  //$message.="<td width='100px'></td>";
		  
		  $message.="<td  width='100px' align='left' >".$getlessonplans[$lehd]['standard']."</td><td  width='100px' align='left' >".$getlessonplans[$lehd]['diff_instruction']."</td></tr><tr align='center'>";
		 
		 
		 }
		 else
		 {
		 
		 //$message.="<td width='100px'></td>";
		 
		 $message.="</tr><tr align='center'>";
		 
		  }
		   $lehd=$key;
		 }
		 
		
		$message.="<td width='100px'>";
		$start1=explode(':',$getplan['starttime']);
		$end1=explode(':',$getplan['endtime']);
		if($start1[0]>=12)
		{
		 
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		 if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		$message.="$start2 to $end2 </td><td width='100px'>".$getplan['subject_name']."</td><td width='100px'>".$getplan['subtab']."</td>";
		
		
		
		if($getplan['lesson_plan_id']==1 && $getplan['standard']!='' && $getplan['diff_instruction']!='' )
		{
		$sh++;
		
		$show[$sh][$start2." to ".$end2]=$getplan['subtab'];
		$show[$sh]['Standard']=$getplan['standard'];
		$show[$sh]['Differentiated Instruction']=$getplan['diff_instruction'];
		
		}
		}
		else
		{
		$les++;
		
		  $message.="<td width='100px'>".$getplan['subtab']."</td>";
		
		
		}
		
		$le=$getplan['lesson_week_plan_id'];
		$lecomments[$le]=$getplan['comments'];
	  
	  }
	  
	  }
	  if($je==0)
	  {
	 
	    $message.="<tr align='center'>
		<td colspan=".$j.">No Plans Found </td>";
		
	 } if($le!=0) {
	/*if(($les+1)>=count($lessonplans))
	{
	
	
	}
	else
	{
	 ?>
	 <td width="<?php echo (count($lessonplans)-$les-1)*100;?>px" ></td>
	<?php } */
	if($val['date']>=date('m-d-Y'))
		 {
		//$message.="<td width='100px'></td>";
	 } else { 
	
	//$message.="<td width='100px'></td>";
	
	 } }  $message.="<td  width='100px' align='left' >".$getplan['standard']."</td><td  width='100px' align='left' >".$getplan['diff_instruction']."</td></tr></table>";
	
	


	 } else { 
	$message.="<tr><td colspan=".$j.">No Plans Found  </td></tr></table></td></tr>";
	} 
	
	if($val['date']>=date('m-d-Y'))
		 {
		 
	
   } 
	$message.="</table>
	</td>
  </tr>  
  
</table>";
 if(!empty($show))
{


$message.="<table align='center' style='width:$widthp;margin-left:10px;border:1px #999 solid; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:12px; color:#666;'>";
 foreach($show as $key=>$showvalue) { 
foreach($showvalue as $keys=>$showvalues) { 



$message.="<tr>
<td style='border-bottom:1px solid #999;'><b>".$keys.":</b>".$showvalues."

</td>
</tr>";

 }  } 
$message.="</table>";

 }
$message.="<br />";
		 } 
					  
					$message.="</body>
					</html>";
	
	//Additional headers
	
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	//echo $message;
	//exit;
	mail($email,$subject,$message,$headers);
	}
		 
		 
		 
		 }
	   
	   }
	   
	   
	
	
	
	
	
	
	}
	function getlessonplansbyteacher($teacher_id=false,$fromdate,$todate)
	{
	
	   
	   $this->load->Model('lessonplanmodel');
	  $data['lessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	    
		
		return $data['lessonplans'];
	
	
	
	
	
	}
	
}
?>