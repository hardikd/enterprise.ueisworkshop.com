<?php
/**
 * GoalPlan Controller.
 *
 */
class Goalplan extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('goalplan/index',$data);
	  
	  }else if($this->session->userdata("login_type")=='user')
	  {
	     $data['view_path']=$this->config->item('view_path');
	     $this->load->view('goalplan/all',$data);
	  
	  
	  
	  }
	  
	  
	
	}
	
	
	function getgoalplans($page,$state_id,$country_id,$district_id=false)
	{
	
	    if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('goalplanmodel');
		$total_records = $this->goalplanmodel->getgoalplanscount($state_id,$country_id,$district_id);
		
			$status = $this->goalplanmodel->getgoalplans($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Goal Plan</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>State</td><td >District Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['goal_plan_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['tab'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districtsname'].'</td>
				
				<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="planedit('.$val['goal_plan_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="plandelete('.$val['goal_plan_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'goal_plan');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>State</td><td >District Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No goal Plans Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'goal_plan');
						print $pagination;	
		}
		
	
	
	
	
	}
	function add_plan()
	{
	
	
		$this->load->Model('goalplanmodel');
			 
			$status=$this->goalplanmodel->add_plan();
			if($status!=0){
				   $data['message']="Tab added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_plan()
	{
	$this->load->Model('goalplanmodel');
			
	   $status=$this->goalplanmodel->update_plan();
		if($status==true){
		       $data['message']="Tab Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		
		echo json_encode($data);
		exit;		
	}
	
	function delete($plan_id)
	{
		
		$this->load->Model('goalplanmodel');
		$result = $this->goalplanmodel->deleteplan($plan_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getplaninfo($plan_id)
	{
		if(!empty($plan_id))
	  {
		$this->load->Model('goalplanmodel');
		$data['goalplan']=$this->goalplanmodel->getplanById($plan_id);
		$data['goalplan']=$data['goalplan'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	
	function comments()
	{

	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('schoolmodel');
	  

	  $data['school']=$this->schoolmodel->getallschools();
	  if($data['school']!=false)	
	   {
		$this->load->Model('teachermodel');
	    $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
		
	   }
	   else
	   {
	     $data['teacher']=false;
		 
	   
	   }
	   
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('goalplan/comments',$data);
	  }
	  else if($this->session->userdata("login_type")=='user')
	  {
	     $this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['teacher']=false; 
	   
	   }
	     $data['view_path']=$this->config->item('view_path');
	    $this->load->view('goalplan/comments',$data);
	  }
	
	}
	
	function getcommentssasigned($page,$school_id,$teacher_id)
	{
	
	  $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('goalplanmodel');
		$total_records = $this->goalplanmodel->getcommentssasignedcount($school_id,$teacher_id);
		
			$status = $this->goalplanmodel->getcommentssasigned($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Goal Plans</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Review Plans</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr  class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				
				<td nowrap><a href="goalplan/commentsview/'.$val['teacher_id'].'">View</a></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Review Plans</td></tr>
			<tr><td valign='top' colspan='10'>No  Teacher Goal Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
		}
		
	
	
	
	
	
	}
	
	function commentsview($teacher_id)
	{
	
	    if($this->input->post('year'))
		{
		
			$year=$this->input->post('year');
		}
		else
		{
		  $year=date('Y');
		
		}
		$data['year']=$year;
		$this->load->Model('teachermodel');
		$this->load->Model('goalplanmodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		$data['observer_desc']=$this->goalplanmodel->getallplansdescbyteacher($year,$teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		$this->load->Model('goalplanmodel');
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolById($data['teacher'][0]['school_id']);
		if($this->session->userdata("login_type")=='admin')
		{
	       
		 $this->session->set_userdata('district_goal_tabs',$data['school'][0]['district_id']);  
		}
		
		
			$data['goalplans']=$this->goalplanmodel->getallplans();
		/*echo '<pre>';	
		//print_r($data['goalplans']);
		$data['teacherplans']=$this->goalplanmodel->getallplansbyteacher($year,$teacher_id);
		print_r($data['teacherplans']);*/
		
		$data['teacherplans']=$this->goalplanmodel->getteacherplanid($data['goalplans'][0]['goal_plan_id'],$teacher_id,$year);
		//print_r($data['teacherplans']);
		//exit;
		if($data['teacherplans']!=false)
		{
		
		$data['comments']=$this->goalplanmodel->getcommentsbyplanid($data['teacherplans'][0]['teacher_plan_id']);
		//print_r($data['comments']);
		}
		else
		{
			$data['comments']=false;
		
		}
		$data['teacher_id']=$teacher_id;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('goalplan/commentsview',$data);
	
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
}	