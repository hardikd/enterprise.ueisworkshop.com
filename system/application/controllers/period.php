<?php
/**
 * period Controller.
 *
 */
class Period extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
		$data['idname']='attendance';
	  if($this->session->userdata("login_type")=='user')
	  {
		  if($this->session->userdata('login_special') == !'district_management'){
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
		  }else{
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		$this->load->Model('periodmodel');
			$data['status'][] = $this->periodmodel->getperiods(false,false,'all');
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('period/all',$data);	
	  
		  }
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			}
		$this->load->Model('schoolmodel');
		$period=$data['school']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
		
		$this->load->Model('periodmodel');
		foreach($period as $peri){
		$data['status'][] = $this->periodmodel->getperiods(false,false,$peri['school_id']);
		}
		
//		print_r($data['status']);exit;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('period/all',$data);	
	  
	  
	  }
	  
	
	}
	function getperiods($page,$school_id=false)
	{
		if($school_id==false)
		{
		  $school_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('periodmodel');
		$total_records = $this->periodmodel->getperiodCount($school_id);
		
			$status = $this->periodmodel->getperiods($page, $per_page,$school_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<table class='table table-striped table-bordered' id='editable-sample' >
			<thead>
			<tr>
			<td class='no-sorting'>Start Time</td>
			<td class='no-sorting'>End Time</td>
			<td class='no-sorting'>School Name</td>
			<td class='no-sorting'>Actions</td></tr></thead> ";
				
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print ' <tbody><tr id="'.$val['period_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['start_time'].'</td>
					  <td>'.$val['end_time'].'</td>
					   <td class="hidden-phone">'.$val['school_name'].'</td>
				<td class="hidden-phone" nowrap>
								 <button class="btn btn-primary" title="Edit" type="button"  value="Edit" name="Edit" onclick="periodedit('.$val['period_id'].')"><i class="icon-pencil"></i></button>
				 <button class="btn btn-danger" title="Delete" type="button"  name="Delete" value="Delete" onclick="perioddelete('.$val['period_id'].')"><i class="icon-trash"></i></button>
			
			</td>
				</tr>
				</tbody>';
				 
				
				
				
				
				
				
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'periods');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' align='center'><td>Start Time</td><td>End Time</td><td>School Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No periods Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'periods');
						print $pagination;	
		}
		
		
	}
	
	function getperiodinfo($period_id)
	{
		if(!empty($period_id))
	  {
		$this->load->Model('periodmodel');
		
		$data['period']=$this->periodmodel->getperiodById($period_id);
		$data['period']=$data['period'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_period()
	{
	
	
		$this->load->Model('periodmodel');
		$pstatus=$this->periodmodel->check_period_exists();
		//$pstatus=1;
	if($pstatus==1)
		 {
		$status=$this->periodmodel->add_period();
		if($status!=0){
		       $data['message']="Period Added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Scheduling Conflict" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==3)
		{
			$data['message']=" Already Added 10 Periods." ;
		    $data['status']=0 ;
			
		
		}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_period()
	{
		$this->load->Model('periodmodel');
	    $pstatus=$this->periodmodel->check_period_update();
		//$pstatus=1;
		if($pstatus==1)
		 {

		$status=$this->periodmodel->update_period();
		if($status==true){
		       $data['message']="period Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Scheduling Conflict" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
		    $data['status']=0 ;
			
		
		}	
		echo json_encode($data);
		exit;		
	}
	
	function delete($period_id)
	{
		
		$this->load->Model('periodmodel');
		$result = $this->periodmodel->deleteperiod($period_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	

	
}	