<?php 
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);


class Teacherclustergraph extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_teacher()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{  $data['idname']='tools';
	 	 error_reporting(0);
		 
		 if($this->session->userdata("login_type")=='teacher')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('teacherclustergraphmodel');
			    $teacher_id = $this->session->userdata('teacher_id');
				
				/*$data['school'] = $this->teacherclustergraphmodel->getteacherschool($teacher_id);
				$school_id = $data['school'][0]['school_id'];
				$district_id = $data['school'][0]['district_id'];
				*/
				$data['records'] = $this->teacherclustergraphmodel->getAssessment($teacher_id);
				$this->load->view('teacherclustergraph/index',$data);
	   
	 	 }
 		 
	}
	
	function getTestclusterhtml()
	{
		error_reporting(0);
		$ass_id = $_REQUEST['assignment_id']; 
		
 	 	 $teacher_id = $this->session->userdata('teacher_id'); 
		 $data = array();
		 $this->load->model('teacherclustergraphmodel');
		 $data= $this->teacherclustergraphmodel->getTestItem($teacher_id,$ass_id);
//                 echo $this->db->last_query();exit;
		  $strClass ='';
		 $strClass .= '<div id="testclusterdrp" class="control-group">
                                             <label class="control-label">Select Skill</label>
		<div class="controls">
<select class="span12 chzn-select" name="test_cluster" id="test_cluster" tabindex="1" onchange="updateTestcluster(this.value)" style="width: 300px;" >
        <option value="-1"  selected="selected">-Please Select-</option>' ;
		
		if(count($data[0]['test_cluster'])>0)
		{
	 	   $strClass .= '<option value="all" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
				 $strClass .='<option value="'.htmlspecialchars($value['test_cluster']).'">'.utf8_decode($value['test_cluster']).'</option>';
		 	}
         $strClass .= '</select> </div></div>';
 		 echo $strClass;
	}
	
	function getReportHtml()
	{
 		$teacherid = $this->session->userdata('teacher_id');
		$test_item= $_REQUEST['test_cluster'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 

		if($test_item != -1)
		{	
			$this->load->model('teacherclustergraphmodel');
				
			$data['school'] = $this->teacherclustergraphmodel->getteacherschool($teacherid);
			$district_id = $data['school'][0]['district_id'];
			
			$data= $this->teacherclustergraphmodel->getGraphData($test_item,$teacherid,$district_id,$fDate,$tDate,$ass_id);
			$IntRow = count($data);
			$correctClounmstring  = "[";
			$incorrectClounmstring = "[";
			$strClusternams = "[";
			for($i=0;$i<$IntRow;$i++)
			{
				$strCluster = $data[$i]['student_name'];
				if($strCluster==''){$strCluster='No student';}
				$intCorrectPercentage = $data[$i]['per'];
				if($intCorrectPercentage==-1)
				{
					$intIncorrectPercentage=0;
				}
				else
				{
					$intIncorrectPercentage = 100 - $intCorrectPercentage;
					//s$intIncorrectPercentage = $intCorrectPercentage;
				}
				$correctClounmstring .= $intCorrectPercentage.",";
				$incorrectClounmstring .= $intIncorrectPercentage.",";
				$strClusternams .= "'".$strCluster."'".",";
			}
			$correctClounmstring = substr($correctClounmstring, 0, -1);
			$incorrectClounmstring = substr($incorrectClounmstring, 0, -1);
			$strClusternams = substr($strClusternams, 0, -1);
			
			$correctClounmstring .="]";
			$incorrectClounmstring .="]";
			$strClusternams .="]";
			$result = $correctClounmstring."*".$incorrectClounmstring."*".$strClusternams;
		}
	echo $result;
	}
	
 	 
	
}	//class end