<?php   
$path_file =  'libchart/classes/libchart.php';
include_once($path_file);
 

class Importdata extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{ 
	 $data['idname']='tools';
	 	 error_reporting(0);
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('importdatamodel');
				$data['records'] = $this->importdatamodel->getallschools();
	 			$this->load->view('importdata/index',$data);
 	 	 }
  	}
	
	function getassessmentHtml()
	{
		error_reporting(0);
		 $school_id = $_REQUEST['school_id']; 
		 $data = array();
		 $this->load->model('importdatamodel');
		 $data= $this->importdatamodel->getassessment($school_id);
		  $strClass ='';
 		 $strClass .= '<select class="combobox" name="assessment" id="assessment" onchange="updateassessment(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option> ';
		
		if(count($data[0]['id'])>0)
		{
	 	   $strClass .= '<option value="0" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
 	 			 $strClass .='<option value="'.$value['assessment_name'].'">'.$value['assessment_name'].'</option>';
		 	}
         $strClass .= '</select>';
 		 echo $strClass;
	}
	
	
	function getReportHtml()
	{
		error_reporting(0);
		$school_id= $_REQUEST['school_id'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		
		if($school_id != -1)
		{
			$this->load->model('importdatamodel');
			$data['grades'] = $this->importdatamodel->getgrades($school_id);
			//	echo "<pre>";print_r($data['grades']); 
			 
			$data['Testitem']=$this->importdatamodel->getTestItem($school_id,$ass_id,$fDate,$tDate);
			$htmlTable = '<div class="outer">  <div class="inner"><table id="innertab">  
       			 <tr class="tchead"><th width="180" align="left"  bgcolor="#8ca275" valign="top" style="color:#fff;font-size:13px;" >  Estrellita Skills </th>';
			foreach($data['grades'] as $value)	
			{
				/*$gradedisplay ='';
				if($value['grade'] == 'K' || $value['grade'] == 'Kindergarten' || $value['grade'] == 'kindergarten' || $value['grade'] == 'k')
					$gradedisplay = 'Kindergarten';
				else if($value['grade'] == '1')
					$gradedisplay = '1st Grade';
				else if($value['grade'] == '2')
					$gradedisplay = '2nd Grade';
				else if($value['grade'] == '3')
					$gradedisplay = '3rd Grade';	
				else if(in_array($value['grade'],array('4','5','6','7','8','9','10','11','12')))
					$gradedisplay = $value['grade'].'th Grade';
					else */
					$gradedisplay = $value['grade'];
				$htmlTable .= "<td style='white-space:nowrap;' valign='top'> &nbsp;&nbsp;".$gradedisplay."&nbsp;&nbsp;</td>";
			}
			$htmlTable .="</tr>";
			
		//	 echo "<pre>";print_r($data['Testitem']);
			$rw =  count($data['grades'][0]);
			if($rw >= 1)
			{		 
	 	 
			$rows =  count($data['Testitem']);
			$row =  count($data['Testitem'][0]);
			
			
			$tablebody = '';
	 		if($row > 0)
			{
		 		$tablerow ='';
			 	for($i = 0 ;$i<$rows; $i++)
				{
					//.preg_replace('/[^(\x20-\x7F)]*/','',$data['Testitem'][$i]['cluster_item'])
					 $tablerow .= "<tr bgcolor='#EEF9E3'> <th width='180' align='left'>".utf8_decode($data['Testitem'][$i]['cluster_item'])."</th>";
			 		$cluster = $data['Testitem'][$i]['cluster_item'];
					
				 	foreach($data['grades'] as $key => $value)
					{
					  $grade = $value['grade'];
					   $gradewisePer =$this->importdatamodel->getPercentageByGradeAndTestCluster($school_id,$grade,$cluster,$fDate,$tDate,$ass_id);		$resper ='';
					   	if($gradewisePer[0]['marks'] !='')
						  $resper =  number_format($gradewisePer[0]['marks'],2);
						  else
						  $resper =number_format(0,2);
						  
					    $tablerow .= "<td> &nbsp;&nbsp;".$resper."%</td>";						 
						
					}
				
				 	$tablerow .="</tr>";
			 	}
				 $tablebody .= $tablerow;
			}
			else
			{
				$cols =  count($data['Testitem'])+1;
				$tablebody = '<tr><td colspan="'.$cols.'" align="center"> No Record Found.</td></tr>';
			}
			}
			else
			{
				$cols =  count($data['Testitem'])+1;
				$tablebody = '<tr><td colspan="'.$cols.'" align="center"> No Record Found.</td></tr>';
			}
			
			$htmlTable .= $tablebody;
			$htmlTable .= "</table>";
	 	}
	   echo $htmlTable;
	}
	
 	 
	
}	
