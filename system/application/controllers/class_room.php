<?php
/**
 * class_room Controller.
 *
 */
class Class_room extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
		 $data['idname']='attendance';
	   if($this->session->userdata("login_type")=='user')
	  {
		 if($this->session->userdata('login_special') == !'district_management'){
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
		 }else{
		$this->load->Model('schoolmodel');
		$data['schools']=$this->schoolmodel->getschoolbydistrict();
//                print_r($data['school']);exit;
		$this->load->Model('class_roommodel');
		$data["class_rooms"] = $this->class_roommodel->getclass_rooms(false, false,'all');
                $this->load->Model('schoolmodel');
                $data['grades']=$this->schoolmodel->getallgrades();
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('class_room/all',$data);	
	  
		 }
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
		$this->load->Model('schoolmodel');
		$data['schools']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
                
                
                $this->load->Model('class_roommodel');
		$data["class_rooms"] = $this->class_roommodel->getclass_rooms(false, false,$this->session->userdata('school_id'));
//                print_r($data["class_rooms"]);exit;
                $this->load->Model('schoolmodel');
                $data['grades']=$this->schoolmodel->getallgrades();
                
                
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('class_room/all',$data);	
	  
	  
	  }
	  
	
	}
	function getallgrades()
	{
	  $this->load->Model('schoolmodel');
	  $data['grade']=$this->schoolmodel->getallgrades();
		
		echo json_encode($data);
		exit;
	
	}
	function getsubjectbydistrict()
	{
	  $this->load->Model('dist_subjectmodel');
	  $data['subject']=$this->dist_subjectmodel->getdist_subjectsById();
		
		echo json_encode($data);
		exit;
	
	}
	function getclass_rooms($page,$school_id=false)
	{
		if($school_id==false)
		{
		  $school_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('class_roommodel');
		$total_records = $this->class_roommodel->getclass_roomCount($school_id);
		
			$status = $this->class_roommodel->getclass_rooms($page, $per_page,$school_id);
			if($status!=FALSE){
			
			print"<table class='table table-striped table-bordered' id='editable-sample'><thead><tr><th class='no-sorting'>Class Name</th><th class='no-sorting'>School Name</th><th class='no-sorting'>Grade</th><th class='no-sorting'>Actions</th></tr></thead>";
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
				print ' <tbody><tr id="'.$val['class_room_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['name'].'</td>
					  <td class="hidden-phone">'.$val['school_name'].'</td>
					  <td class="center hidden-phone">'.$val['grade_name'].'</td>
				<td nowrap class="hidden-phone">
<button data-dismiss="modal" class="btn btn-primary"  title="Edit"  type="button"  value="Edit" name="Edit" onclick="class_roomedit('.$val['class_room_id'].')"><i class="icon-pencil"></i></button>
	<button class="btn btn-danger"  title="Delete" type="button"  name="Delete" value="Delete" onclick="class_roomdelete('.$val['class_room_id'].')" data-dismiss="modal" aria-hidden="true"><i class="icon-trash"></i></button>
		</td>
		</tr>';
		
				
				$i++;
				}
				print '</tbody></table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'class_rooms');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='table table-striped table-bordered' id='editable-sample'>
			<tr align='center'>
			<th class='no-sorting'>Class Name</th>
              <th class='no-sorting'>School Name</th>
              <th class='no-sorting'>Grade</th>
              <th class='no-sorting'>Actions</th>
              </tr>
			  <tr>
			<td valign='top' colspan='10'>No Class Rooms Found.</td></tr></table>
			";
	
			
			
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'class_rooms');
						print $pagination;	
		}
		
		
	}
	
	function getclass_roominfo($class_room_id)
	{
		if(!empty($class_room_id))
	  {
		$this->load->Model('class_roommodel');
		
		$data['class_room']=$this->class_roommodel->getclass_roomById($class_room_id);
		$data['class_room']=$data['class_room'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_class_room()
	{
	
	
		$this->load->Model('class_roommodel');
		$pstatus=$this->class_roommodel->check_class_room_exists();
	if($pstatus==1)
		 {
		$status=$this->class_roommodel->add_class_room();
		if($status!=0){
		       $data['message']="Class Room Added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Class Room With Same Name In this School  Already Exists" ;
		    $data['status']=0 ;
		}
		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_class_room()
	{
		$this->load->Model('class_roommodel');
	    $pstatus=$this->class_roommodel->check_class_room_update();
		if($pstatus==1)
		 {

		$status=$this->class_roommodel->update_class_room();
		if($status==true){
		       $data['message']="Class Room Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Class Rroom With Same Name In this School  Already Exists" ;
		    $data['status']=0 ;
		}
			
		echo json_encode($data);
		exit;		
	}
	
	function delete($class_room_id)
	{
		
		$this->load->Model('class_roommodel');
		$result = $this->class_roommodel->deleteclass_room($class_room_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	function getclassroom($school_id,$grade_id)
	{
		
		$this->load->Model('class_roommodel');
		$data['class_room'] = $this->class_roommodel->getclassroom($school_id,$grade_id);
		echo json_encode($data);
		exit;
		
	}
	
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
	
}	