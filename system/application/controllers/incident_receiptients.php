<?php
/**
 * Countries Controller.
 *
 */
class Incident_receiptients extends	MY_Auth {
function __Construct()
	{
			parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		$this->load->library('user_agent');
	}

	function index()
	{	

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('incident_receipients/incident_receipients',$data);
	}
	
	function incident_receipients_list($page){
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();

		$this->load->model('incident_receipients_model');
		
		$total_records = $this->incident_receipients_model->get_all_dataCount();
		
		$data['alldata'] = $status = $this->incident_receipients_model->get_all_data($page,$per_page);
		// echo $this->db->last_query();exit;
		
		$data['pagination'] = $this->do_pagination($total_records,$per_page,$page,'incident_possible_motivation');
						

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('incident_receipients/incident_receipients_edit',$data);
	}
	
	function incident_receipients_insert()
	{
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$firstname=$this->input->post('firstname');
			$lastname=$this->input->post('lastname');
			$email=$this->input->post('email');
			$status=$this->input->post('status');
			$data=array('firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email);
			$this->load->model('incident_receipients_model');
			$update = $this->incident_receipients_model->update('incident_colleague',$data,array('colleague_id'=>$id));
			if($update){
				echo "DONE";	
			} else {
				echo "ERROR";
			}

		} else{
			$district_id=$this->input->post('district_id');
			$is_delete=0;
			$firstname=$this->input->post('firstname');
			$lastname=$this->input->post('lastname');
			$email=$this->input->post('email');
			$incident_possible_motivation_value=array('is_delete'=>$is_delete,'district_id'=>$district_id,'firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email);
			$this->load->model('incident_receipients_model');
			$insert = $this->incident_receipients_model->insert('incident_colleague',$incident_possible_motivation_value);
			if($insert){
				echo "DONE";	
			} else {
				echo "ERROR";
			}
			
		}
	}
 function edit($id)
	{
		$this->load->model('incident_receipients_model');
		$data['all']=$this->incident_receipients_model->get_incident_colleagueById(array('colleague_id'=>$id));
		print_r(json_encode($data['all']));exit;
	}

	public function delete()
	{
		$motivation_remove =array('is_delete'=>'1');
		$this->load->model('incident_receipients_model');
		$schedule_id = $this->incident_receipients_model->delete_incident_possible_motivation('incident_possible_motivation',$motivation_remove);
		echo 'DONE';
	}	
	
function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active current'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}	
	
	
}
  
