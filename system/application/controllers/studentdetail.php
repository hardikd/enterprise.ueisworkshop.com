<?php
 $path_file =  'libchart/classes/libchart.php';
 include_once($path_file);
/**
 * Studentdetail Controller.
 *
 */
class Studentdetail extends	MY_Auth {
function __Construct()
	{
	
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false  && $this->is_teacher()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->load->library('paginationnew');
		$this->load->helper('url');
		$this->no_cache();

		
	}
	
   function no_cache()
	{
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	
	
	function index()
	{
		
	 $data['idname']='tools';
		$login_required = $this->session->userdata('login_required');
		
		if(empty($login_required) && $login_required =='')
		{
			
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			
		}
		else
		{
	 
	   if($this->session->userdata("login_type")=='user')
		  {
			
			 
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentdetailmodel');
				//$data['records'] = $this->Studentdetailmodel->getstuddetail();
				$district_id = $this->session->userdata('district_id');
				$data['school_type'] = $this->Studentdetailmodel->getschooltype($district_id);
			
				$config = array();
				$config["base_url"] = base_url() . "studentdetail/index";
				$config["total_rows"] = $this->Studentdetailmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentdetailmodel->getstuddetail($config["per_page"], $page);
								
				$this->load->view('studentdetail/studetail',$data);
	   
	   
	   
	   
	 	 }
		
		else if($this->session->userdata("login_type")=='teacher')
		  {
			
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentdetailmodel');
				//$data['records'] = $this->Studentdetailmodel->getstuddetail();
				$district_id = $this->session->userdata('district_id');
			//	$data['school_type'] = $this->Studentdetailmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
				$teacher_id = $this->session->userdata('teacher_id');
				$data['school_grade'] = $this->Studentdetailmodel->getteacher_schoolandgrade($teacher_id,$district_id);
				
				$config = array();
				$config["base_url"] = base_url() . "studentdetail/index";
				$config["total_rows"] = $this->Studentdetailmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentdetailmodel->getstuddetail($config["per_page"], $page);
				$this->load->Model('videooptionmodel');
	            $data['video_option'] = $this->videooptionmodel->get_option();
				
				$this->load->view('studentdetail/studetail',$data);
	   
	 	 }
		 
		 else if($this->session->userdata("login_type")=='observer')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentdetailmodel');
				//$data['records'] = $this->Studentdetailmodel->getstuddetail();
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
				$district_id = $this->session->userdata('district_id');
			//	$data['school_type'] = $this->Studentdetailmodel->getschooltype($district_id);
				
				$observer_id = $this->session->userdata('observer_id');
				$data['school_grade'] = $this->Studentdetailmodel->getobserver_schoolandgrade($observer_id,$district_id);

                $config = array();
				$config["base_url"] = base_url() . "studentdetail/index";
				$config["total_rows"] = $this->Studentdetailmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentdetailmodel->getstuddetail($config["per_page"], $page);
				
				
				
				$this->load->view('studentdetail/studetail',$data);
	   
	 	 }
	  }
	
	}
	
	function delete_student()
	{
			$userid = $_REQUEST['stuid'];
			$this->load->model('Studentdetailmodel');
			$this->Studentdetailmodel->student_delete($userid);
	}
	
	function getstudentDetail()
	{
		$userid = $_REQUEST['stuid'];
		$this->load->model('Studentdetailmodel');
		$data['records']=$this->Studentdetailmodel->getrecord_student($userid);
		
		foreach($data['records'] as $key => $value)
		{
			
			echo $value['UserID']. "|";
			echo $value['Name']. "|";
			echo $value['Surname']. "|";
			echo $value['email'];
			
		
		
		}
		
	}
	
	function updatestudent()
	{
		

		$this->load->model('Studentdetailmodel');
		$data['records']=$this->Studentdetailmodel->student_update($_REQUEST);
		
		$this->index();
		
	}
	
	function getstudentperformance()
	{
	 
	 $userid = $_REQUEST['userid'];
		
		$this->load->model('Studentdetailmodel');
		$data['records']=$this->Studentdetailmodel->getstuperformance($userid);
		if(!empty($data['records'][0]['assignment_id']) && $data['records'][0]['assignment_id']!="")
		{
		$assessment_id = @$data['records'][0]['assignment_id'];
		}
		$data['assessment']=$this->Studentdetailmodel->getassessment($assessment_id);
			
			if(count($data['records'])>0)
			{
				if(!empty($data['records']) && $data['records']!="")
					{
		
						echo @$data['records'][0]['added_date']. "|";
						echo @$data['records'][0]['pass_score_point']. "|";
						echo @$data['records'][0]['pass_score_perc']. "|";
						echo @$data['assessment'][0]['assignment_name'];
						
					}
			}
			else
			{
				echo "norecfound";
			}
		
		
		
		
	}
	
	/* Student graph detail*/
		
		function getstudentgraphdetail()
		{
			
			$stud_id =  $_REQUEST['stuid'];
			$this->load->model('Studentdetailmodel');
			$data['records']=$this->Studentdetailmodel->getstuperformance($stud_id);
			if(!empty($data['records'][0]['assignment_id']) && $data['records'][0]['assignment_id']!="")
			{
			$assessment_id = $data['records'][0]['assignment_id'];
			}
			$data['assessment']=$this->Studentdetailmodel->getassessment($assessment_id);
			if(!empty($data['assessment'][0]['subject_id']) && $data['assessment'][0]['subject_id']!="")
			{
			$subj_id = $data['assessment'][0]['subject_id'];
			}
			
			$data['subject'] = $this->Studentdetailmodel->getsubdetail($subj_id);

			echo $data['records'][0]['pass_score_point'].'|';
			echo $data['assessment'][0]['pass_score'].'|';
			echo $data['subject'][0]['subject_name'].'|';
			echo $data['records'][0]['success'].'|';
			
		}
	

	function studentreportaspdf($stuid)
	{
		
		
		$newdata = array('studentid'  => $stuid,       
               );

		$this->session->set_userdata($newdata);
		
		$this->load->model('studentdetailmodel');
		//get student according to userid
		
			
		
		@$data['studentrecord']=$this->studentdetailmodel->getrecord_student($stuid);
		if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
		{
		@$data['schoolname']=$this->studentdetailmodel->getschoolname($data['studentrecord'][0]['school_id']);
		}
		if(!empty($data['studentrecord'][0]['country_id']) && $data['studentrecord'][0]['country_id']!="")
		{
		@$data['countryname']=$this->studentdetailmodel->getcountry($data['studentrecord'][0]['country_id']);
		}
		if(!empty($data['studentrecord'][0]['district_id']) && $data['studentrecord'][0]['district_id']!="")
		{
		@$data['districtname']=$this->studentdetailmodel->getdistrict($data['studentrecord'][0]['district_id']);
		}
		if(!empty($data['districtname'][0]['state_id']) && $data['districtname'][0]['state_id']!="")
		{
		$data['statename']=$this->studentdetailmodel->getstate($data['districtname'][0]['state_id']);
		}
		//get student performance detail according to their student id
		@$data['studentperformance'] = $this->studentdetailmodel->getstuperformance($stuid);
		if(empty($data['studentperformance']))
		{
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('studentdetail/notfound',$data);	
		}
		else
		{
			
		$assessname =array();
		foreach($data['studentperformance'] as $k => $v)
		{
			$assessmentid = $v['assignment_id'];
			// get assessment name
			@$assessname[] = $this->studentdetailmodel->getassessment($assessmentid);
		}
		@$data['assessmentname'] = $assessname;
		
	
  $testnames=array();
  foreach($data['assessmentname'] as $key => $val)  {
	    
	  @$testnames[] = $val[0]['assignment_name'];
  }
  
 
  $testnames = implode("','",$testnames);

 @$testnames = "['".$testnames."']";
 
  $marks=array();
foreach($data['studentperformance'] as $kkk=>$vvv)
{
	
	$marks[] = $data['studentperformance'][$kkk]['pass_score_perc'];
}

 @$dd = implode(',',$marks);



// For pie graph working		
			
			$pro = array();
			$basic = array();
			$bbasic = array();
			
	for($i=0;$i<count($data['studentperformance']);$i++)
		{
			$num = round($data['studentperformance'][$i]['pass_score_perc']);
				
			
				
				if($num>=0 && $num<40)
				{
					
					@$bbasic[] = $num;
					
				}
				else if($num>=40 && $num<77)
				{
					 @$basic[] = $num;
				}
				if($num>=77 && $num<=100)
				{
					@$pro[] = $num;
				}
				
				
				
		}
		


		
		
	$t1 = array_sum($bbasic);
		 
		  if($t1==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			   @$avgbbbasic = $t1/$total;
			 }
		  
		 // 2
		  	 $t2 = array_sum($basic);	
		  
		  	 if($t2==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  @$avgbasic = $t2/$total1;
			 }
		 //3
		
		   $t3 = array_sum($pro);		 
		 if($t3==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
			   @$avgpro = $t3/$total2;
			 }	
			 

echo '<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>';
echo '<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script>'; 
echo '<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>';
echo '<script src="/testbank/workshop/js/highcharts.js"></script>';
echo '<script src="/testbank/workshop/js/exporting.js"></script>';
echo '<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>';
echo '<script src="/testbank/workshop/js/base64.js"></script>';
echo '<script src="/testbank/workshop/js/canvas2image.js"></script>';	
echo '<script type="text/javascript">
$(function () {
        $("#piechart").highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: "Pie Chart"
            },
            tooltip: {
        	    pointFormat: "{series.name}: <b>{point.percentage}%</b>",
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: "pointer",
                    dataLabels: {
                        enabled: true,
                        color: "#000000",
                        connectorColor: "#000000",
                        formatter: function() {
                            return "<b>"+ this.point.name +"</b>: "+ this.percentage +" %";
                        }
                    }
                }
            },
            series: [{
                type: "pie",
                name: "Student Score",
                data: [
                    ["Below Basic",'.$avgbbbasic.'],
                    ["Basic", '.$avgbasic.'],
                    {
                        name: "Proficient",
                        y: '.$avgpro.',
                        sliced: true,
                        selected: true
                    },
                   
                    
                ]
            }]
        });
    });
    

		</script>';
		
echo '<script type="text/javascript">
$(function () {
        $("#barchart").highcharts({
            chart: {
            },
            title: {
                text: "Bar chart"
            },
            xAxis: {
                categories: '.$testnames.'
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ""+
                            this.point.name +": "+ this.y +" fruits";
                    } else {
                        s = ""+
                            this.x  +": "+ this.y;
                    }
                    return s;
                }
            },
            labels: {
                items: [{
                    html: "Student Performance bar chart",
                    style: {
                        left: "40px",
                        top: "8px",
                        color: "black"
                    }
                }]
            },
            series: [{
                type: "column",
                name: "hh",
                data: ['.$dd.']
            },{
                type: "spline",
                name: "Average",
                data: ['.$dd.'],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[3],
                	fillColor: "white"
                }
            }]
        });
   
var chart=$("#piechart").highcharts();
var chart1=$("#barchart").highcharts();
var svg=chart.getSVG();
var svg1=chart1.getSVG();
canvg(document.getElementById("canvas"),svg);  
canvg(document.getElementById("canvas1"),svg1);       
canvas = document.getElementById("canvas");
canvas1=document.getElementById("canvas1");
img_PNG = Canvas2Image.saveAsPNG(canvas, true);
img_PNG1 = Canvas2Image.saveAsPNG(canvas1, true);
var img_src=img_PNG.src;

var img_src1=img_PNG1.src;
$.ajax({
	data:{s:img_src,s1:img_src1},
 	 type: "POST",
  url: "/testbank/workshop/ajax_files/ajax_page.php",
  context: document.body,
  success:function(str)
  {
	
	
  }
});
}); 
</script>';

echo '<div id="piechart" style="display:none; min-width: 400px; height: 400px; margin: 0 auto"></div>';		
echo '<div id="barchart" style="display:none; min-width: 400px; height: 400px; margin: 0 auto"></div>';
echo '<canvas id="canvas" width="1000px" height="600px;" style="display:none;"></canvas>';
echo '<canvas id="canvas1" width="1000px" height="600px;" style="display:none;"></canvas>';
echo '<div id="a"></div>';
$path = base_url().'studentdetail/pdf';
echo '<script>document.location.href="'.$path.'"</script>';

	}

	
}
	
	function pdf($studentid)
	{
		//$studentid = $this->session->userdata('studentid');
		$this->load->model('studentdetailmodel');
		//get student according to userid
		$data['studentrecord']=$this->studentdetailmodel->getrecord_student($studentid);
		
		if(!empty($data['studentrecord'][0]['Name']) && $data['studentrecord'][0]['Name']!="")
		{
		@$studentname = $data['studentrecord'][0]['Name'];
		}
		if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
		{		
		@$data['schoolname']=$this->studentdetailmodel->getschoolname($data['studentrecord'][0]['school_id']);
		}
		if(!empty($data['studentrecord'][0]['country_id']) && $data['studentrecord'][0]['country_id']!="")
		{	
		@$data['countryname']=$this->studentdetailmodel->getcountry($data['studentrecord'][0]['country_id']);
		}
		if(!empty($data['studentrecord'][0]['district_id']) && $data['studentrecord'][0]['district_id']!="")
		{	
		@$data['districtname']=$this->studentdetailmodel->getdistrict($data['studentrecord'][0]['district_id']);
		}
		if(!empty($data['districtname'][0]['state_id']) && $data['districtname'][0]['state_id']!="")
		{
		@$data['statename']=$this->studentdetailmodel->getstate($data['districtname'][0]['state_id']);
		}
		//get student performance detail according to their student id
		@$data['studentperformance'] = $this->studentdetailmodel->getstuperformance($studentid);
		// echo $this->db->last_query();

		// print_r($data['studentperformance']);
		// exit;
	if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
		{
	
	@$data['teachername']=$this->studentdetailmodel->getteachername($data['studentrecord'][0]['school_id']);
		}
		$assessname =array();
		
		
			foreach($data['studentperformance'] as $k => $v)
			{
				$assessmentid = $v['assignment_id'];
				// get assessment name
				$assessname[] = $this->studentdetailmodel->getassessment($assessmentid);
			}
		@$data['assessmentname'] = $assessname;
                
                $this->load->model('studentdetailmodel');
		//get student according to userid
		$studentrecord = $data['studentrecord']=$this->studentdetailmodel->getrecord_student($studentid);
				
		$schoolname = $data['schoolname']=$this->studentdetailmodel->getschoolname($data['studentrecord'][0]['school_id']);
		$countryname = $data['countryname']=$this->studentdetailmodel->getcountry($data['studentrecord'][0]['country_id']);
		
		$districtname = $data['districtname']=$this->studentdetailmodel->getdistrict($data['studentrecord'][0]['district_id']);
		$statename = $data['statename']=$this->studentdetailmodel->getstate($data['districtname'][0]['state_id']);
		//get student performance detail according to their student id
		$studentperformance = $data['studentperformance'] = $this->studentdetailmodel->getstuperformance($studentid);
                $teacherid = $this->session->userdata('teacher_id');
//                $data['teachername'] = $this->teacherclusterreportmodel->getteachername($teacherid);
//		$data['teacher_name'] = $data['teachername'][0]['firstname'].' '.$data['teachername'][0]['lastname'];
                $this->load->Model('teacherclusterreportmodel');
                 $classroom = $this->teacherclusterreportmodel->getRoomByTeacherId($teacherid);
		 $data['room'] = $classroom[0]['name']; 
		if(empty($data['studentperformance']))
		{
			$data['view_path']=$this->config->item('view_path');
//			$this->load->view('studentdetail/recordnotfound',$data);	
		}
		else
		{
			
		$assessname =array();
		$alreadyadded = array();
		foreach($data['studentperformance'] as $k => $v)
		{
			$assessmentid = $v['assignment_id'];
			// get assessment name
			$assessmentdet = $this->studentdetailmodel->getassessment($assessmentid);
			// echo $this->db->last_query();
			// print_r($assessmentdet);
			if(!in_array($assessmentdet[0][assignment_name],$alreadyadded)){
				$assessname[] = $assessmentdet;
			}
			$alreadyadded[] = $assessmentdet[0][assignment_name];
		}
		// exit;
		$assessmentname = $data['assessmentname'] = $assessname;
//		$data['view_path']=$this->config->item('view_path');
//		$this->load->view('studentdetail/individualstudentchart',$data);
		
		
		}
//                print_r($assessmentname);exit;
                 if(!empty($assessmentname))
  {
  
  foreach($assessmentname as $key => $val)
  {
	  
	  $testnames[] = @$val[0]['assignment_name'];
	  $testids[] = @$val[0]['id'];
  }
  
 
//  @$testnames = implode("','",$testnames);
//
//	$testnames = "['','".$testnames."']";
  $marks=array();
  // $marks[] = 0;
foreach($studentperformance as $kkk=>$vvv)
{
	
	if(in_array($vvv['assignment_id'],$testids) && $studentperformance[$kkk]['pass_score_perc']!='') {
		$marks[] = (double)$studentperformance[$kkk]['pass_score_perc'];
	}
}
  $dd = $marks;//implode(',',$marks);


// For pie graph working		
	
			
			
	// Dynamic proficiency  
      
   
	  $temp=array();
	 for($k=0;$k<count($assessmentsids);$k++)
	 {
		$res = mysql_query('select * from proficiency where assessment_id="'.$assessmentsids[$k].'"');
		$temp[] = mysql_fetch_assoc($res);
	 }
			
			
			
			$pro = array();
			$basic = array();
			$bbasic = array();
			
	for($i=0;$i<count($studentperformance);$i++)
		{
				$num = round($studentperformance[$i]['pass_score_point']);
				
			$bbfrom = $temp[$i]['bbasicfrom'];
			$bbto = $temp[$i]['bbasicto'];
			$basicfrom = $temp[$i]['basicfrom'];
			$basicto = $temp[$i]['basicto'];
			$pro_from = $temp[$i]['pro_from'];
			$pro_to = $temp[$i]['pro_to'];
				
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					

				}
				else if($num>=$basicfrom && $num<=$basicto)
				{
					 $basic[] = $num;
				}
				if($num>=$pro_from && $num<=$pro_to)
				{
					$pro[] = $num;
				}
				
				
				
		}
		
	
	$t1 = array_sum($bbasic);
		 
		  if($t1==0)
			 {
				$avgbbbasic=0; 
			 }
			 else
			 {		 
			 	$total =  count($bbasic);			
			   $avgbbbasic = $t1/$total;
			 }
		  
		 // 2
		  	 $t2 = array_sum($basic);	
		  
		  	 if($t2==0)
			 {
				$avgbasic=0; 
			 }
			 else
			 {			 
				 $total1 =  count($basic);			
			  $avgbasic = $t2/$total1;
			 }
		 //3
		
		   $t3 = array_sum($pro);		 
		 if($t3==0)
			 {
				$avgpro=0; 
			 }
			 else
			 {
		 	      $total2 =  count($pro);			
			   $avgpro = $t3/$total2;
			 }

  }
//                print_r($dd);exit;
                $high['title']['text'] = "Student Performance Report";
                $high['xAxis']['categories'] = $testnames;
                $high['yAxis']['title']['text'] = $studentrecord[0]['Name'].' '.$studentrecord[0]['Surname'];
                $high['series'][] = array('name' => 'Estrellita Assessments','data' => $dd,'type'=>'column');
                $high['series'][] = array('name' => 'Assessments Average','data' => $dd,'type'=>'spline');
                
                
               // echo json_encode($high);exit;
                
                $myfile = fopen(WORKSHOP_FILES."mediagenerated/inrep_$studentid.json", "w") or die("Unable to open file!");
                        $txt = json_encode($high);
                        fwrite($myfile, $txt);
                        fclose($myfile);
                        
                        unlink("/var/www/html/system/application/views/inc/logo/inrep_$studentid.png");
                        
                        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/inrep_$studentid.json -outfile /var/www/html/system/application/views/inc/logo/inrep_$studentid.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
                         exec($command,$output);
                         //Code for disclaimer added by Hardik
//                         print_r($data['teachername']);exit;
	    $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(10);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(10);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdes != false) {

            $data['description'] = $reportdes[0]['tab'];
            $data['description_fontsize'] = $reportdes[0]['size'];
            $data['description_fontcolor'] = $reportdes[0]['color'];
        }
        
        $html='

<table class="gridtable" style="width:750px;">
<tr>
<th style="width:210px;"><b>Assignment Name</b></th> <th style="width:135px;"><b>Number Correct</b></th><th style="width:135px;"><b>Percentage Correct</b></th><th style="width:135px;"><b>Assessment Pass Score</b></th>
</tr>';
if(!empty($data["assessmentname"]))
{
for($i=0;$i<count($data["assessmentname"]);$i++)
{
	
	@$test = $data["assessmentname"][$i][0]["assignment_name"];
	@$point =  $data["studentperformance"][$i]['pass_score_point'];
@$percen =  $data["studentperformance"][$i]['pass_score_perc'];
@$totalscoreofassessment =  $data["assessmentname"][$i][0]['pass_score'];
$html .=
'<tr>
 <td style="width:210px;">'.$test.'</td>
 <td style="width:135px;text-align:center;">'.$point.'</td>
 <td style="width:135px;text-align:center;">'.$percen.'</td>
 <td style="width:135px;text-align:center;">'.$totalscoreofassessment.'</td>

</tr>';
}
}
else
{
$html .=
'<tr>
 <td></td>
 <td></td>
 <td>The Student has not attempted any assessment yet.</td>
 <td></td>
</tr>';	

}

$html.='</table>';

$data['htmlTable'] = $html;
                 $data['studentid'] = $studentid;
	$data['view_path'] = $this->config->item('view_path');	
            $this->output->enable_profiler(false);
            $this->load->library('parser');
    //        print_r($data['teachername']);exit;
            $ostr = $this->parser->parse('studentdetail/studentdetailpdf', $data, TRUE);
            $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
            $content = ob_get_clean();




            $html2pdf->WriteHTML($ostr);
            $html2pdf->Output();

		
		
		}		
	



	function getschools()
	{
		@$schooltypeid= $_REQUEST['school_type_id'];
		$this->load->model('studentdetailmodel');
		$data['result']=$this->studentdetailmodel->getallschools($schooltypeid);
		echo '<option value="">Please Select</option>';
		foreach($data['result'] as $key => $value)
		{
			
			echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
		}
		
	}
	
	
	function getgrades()
	{
		$schoolid = $_REQUEST['schoolid'];
		$this->load->model('studentdetailmodel');
		$data['grades']=$this->studentdetailmodel->getgrades();
		echo '<option value="">Please Select</option>';
		foreach($data['grades'] as $key => $value)
		{
			
		echo '<option value="'.$value['dist_grade_id'].'">'.@$value['grade_name'].'</option>';
		}
	}
	
	function getstudents()
	{ $data['idname']='tools';
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		
		@$schooltype = $_REQUEST['schools_type'];
		@$schoolid = $_REQUEST['schools'];
		@$gradeid = $_REQUEST['grades'];
	
		$this->load->model('studentdetailmodel');
		
		//get all student according to selected school
		
		$data['students']=$this->studentdetailmodel->getstud($schoolid,$gradeid);
		
		
		if(!empty($data['students']))
		{
		foreach($data['students'] as $k=>$v)
		{
			
		$data['schoolname']=$this->studentdetailmodel->getschoolname($schoolid);
	
		}
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('studentdetail/searchstudent',$data);
		
		}
		else
		{
				$data['view_path']=$this->config->item('view_path');
				$this->load->view('studentdetail/notfound',$data);	
		}
		
		}
		
	
	}
	
	function getindividual()
	{
		
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		if(!empty($_REQUEST['userid']) && $_REQUEST['userid']!="")
		{
		$userid = $_REQUEST['userid'];
		}
		$this->load->model('studentdetailmodel');
		//get student according to userid
		$data['studentrecord']=$this->studentdetailmodel->getrecord_student($userid);
		if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
		{
		$data['schoolname']=$this->studentdetailmodel->getschoolname($data['studentrecord'][0]['school_id']);
		}
		if(!empty($data['studentrecord'][0]['country_id']) && $data['studentrecord'][0]['country_id']!="")
		{
		$data['countryname']=$this->studentdetailmodel->getcountry($data['studentrecord'][0]['country_id']);
		}
		if(!empty($data['studentrecord'][0]['district_id']) && $data['studentrecord'][0]['district_id']!="")
		{
		$data['districtname']=$this->studentdetailmodel->getdistrict($data['studentrecord'][0]['district_id']);
		}
		if(!empty($data['districtname'][0]['state_id']) && $data['districtname'][0]['state_id']!="")
		{
		$data['statename']=$this->studentdetailmodel->getstate($data['districtname'][0]['state_id']);
		}
		//get student performance detail according to their student id
		$data['studentperformance'] = $this->studentdetailmodel->getstuperformance($userid);
		if(empty($data['studentperformance']))
		{
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('studentdetail/notfound',$data);	
		}
		else
		{
			
		$assessname =array();
		foreach($data['studentperformance'] as $k => $v)
		{
			$assessmentid = $v['assignment_id'];
			// get assessment name
			$assessname[]  = $this->studentdetailmodel->getassessment($assessmentid);
			//echo "<pre>";print_r($assessname);
		}
		//echo "<pre>";print_r($assessname[0]);
		$data['assessmentname'] = $assessname;
		
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('studentdetail/individualstudentdetail',$data);
		
		
		}
	}
			
	}
	
	function getsinglestudentdetail($userid)
	{
		 $data['idname']='tools';
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
	
		$this->load->model('studentdetailmodel');
		//get student according to userid
		$data['studentrecord']=$this->studentdetailmodel->getrecord_student($userid);
				
		$data['schoolname']=$this->studentdetailmodel->getschoolname($data['studentrecord'][0]['school_id']);
		$data['countryname']=$this->studentdetailmodel->getcountry($data['studentrecord'][0]['country_id']);
		
		$data['districtname']=$this->studentdetailmodel->getdistrict($data['studentrecord'][0]['district_id']);
		$data['statename']=$this->studentdetailmodel->getstate($data['districtname'][0]['state_id']);
		//get student performance detail according to their student id
		$data['studentperformance'] = $this->studentdetailmodel->getstuperformance($userid);
		if(empty($data['studentperformance']))
		{
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('studentdetail/recordnotfound',$data);	
		}
		else
		{
			
		$assessname =array();
		foreach($data['studentperformance'] as $k => $v)
		{
			$assessmentid = $v['assignment_id'];
			// get assessment name
			$assessname[] = $this->studentdetailmodel->getassessment($assessmentid);
		}
		$data['assessmentname'] = $assessname;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('studentdetail/individualstudentchart',$data);
		
		
		}
		
		}
	
	}
	
	
	
}	