<?php
/**
 * school Controller.
 *
 */
class QuickGuides extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  	
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('school/index',$data);
	  }
	  else if($this->session->userdata("login_type")=='user')
	  {
	      $this->load->Model('school_typemodel');
		$data['school_type']=$this->school_typemodel->getallplans();		
		$data['view_path']=$this->config->item('view_path');
	     $this->load->view('quickGuides/all',$data);
	  
	  
	  
	  }
	
	}
	function getschools($page,$state_id,$country_id,$district_id=false)
	{
		if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('schoolmodel');
		$total_records = $this->schoolmodel->getschoolCount($state_id,$country_id,$district_id);
		
			$status = $this->schoolmodel->getschools($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Schools</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td> School Name</td><td> User Name</td><td>State</td><td >District Name</td><td >LP</td><td >NC</td><td >TE</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['school_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['username'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districtsname'].'</td>';
				 if($val['LP']==1) { 
						print '<td id="LP_'.$val['school_id'].'"><img src="'.SITEURLM.'/images/home_on.jpg" style="cursor:pointer;" title="Click to InActivate" onclick="inactive(\'LP\','.$val['school_id'].')" /> </td>';
					 } else  {  
						print '<td id="LP_'.$val['school_id'].'"><img src="'.SITEURLM.'/images/home_off.jpg" style="cursor:pointer;" title="Click to Activate" onclick="active(\'LP\','.$val['school_id'].')" /> </td>';
					 } 
					  if($val['NC']==1) { 
						print '<td id="NC_'.$val['school_id'].'"><img src="'.SITEURLM.'/images/home_on.jpg" style="cursor:pointer;" title="Click to InActivate" onclick="inactive(\'NC\','.$val['school_id'].')" /> </td>';
					 } else  {  
						print '<td id="NC_'.$val['school_id'].'"><img src="'.SITEURLM.'/images/home_off.jpg" style="cursor:pointer;" title="Click to Activate" onclick="active(\'NC\','.$val['school_id'].')" /> </td>';
					 } 
					  if($val['TE']==1) { 
						print '<td id="TE_'.$val['school_id'].'"><img src="'.SITEURLM.'/images/home_on.jpg" style="cursor:pointer;" title="Click to InActivate" onclick="inactive(\'TE\','.$val['school_id'].')" /> </td>';
					 } else  {  
						print '<td id="TE_'.$val['school_id'].'"><img src="'.SITEURLM.'/images/home_off.jpg" style="cursor:pointer;" title="Click to Activate" onclick="active(\'TE\','.$val['school_id'].')" /> </td>';
					 } 
					 
				
				
				
				print '<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="schooledit('.$val['school_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="schooldelete('.$val['school_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'schools');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>school Name</td><td>User Name</td><td>State</td><td >District Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No schools Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'schools');
						print $pagination;	
		}
		
		
	}
	
	function getschoolinfo($school_id)
	{
		if(!empty($school_id))
	  {
		$this->load->Model('schoolmodel');
		$this->load->Model('districtmodel');
		
		
		$data['school']=$this->schoolmodel->getschoolById($school_id);
		$data['district']=$this->districtmodel->getalldistricts();
		$data['school']=$data['school'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	 function changestatus($types)
	{		
		
		$this->load->Model('schoolmodel');
	  $school_id=$this->input->post('school_id');
	 
	  $type=$this->input->post('type');
	  
	  
	 
	  if($types=='inactive')
	  {
	    
		echo $status = $this->schoolmodel->changestatus(0,$type,$school_id);
	  
	  
	  }
	  else
      {
        
		echo $status = $this->schoolmodel->changestatus(1,$type,$school_id);

       }	  
	  
	
	}
	function add_school()
	{
	
	
		$this->load->Model('schoolmodel');
		$pstatus=$this->schoolmodel->check_school_exists();
	
		 if($pstatus==1)
		 {	 
			$status=$this->schoolmodel->add_school();
			if($status!=0){
				   $data['message']="school added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		}
		else if($pstatus==0)
		{
			$data['message']="School With Same Name   Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="School With User Name   Already Exists" ;
		    $data['status']=0 ;
		}		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_school()
	{
	$this->load->Model('schoolmodel');
	$pstatus=$this->schoolmodel->check_school_update();
	if($pstatus==1)
		 {		
	   $status=$this->schoolmodel->update_school();
		if($status==true){
		       $data['message']="school Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="School With Same Name   Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="School With User Name   Already Exists" ;
		    $data['status']=0 ;
		}	
		echo json_encode($data);
		exit;		
	}
	
	function delete($school_id)
	{
		
		$this->load->Model('schoolmodel');
		$result = $this->schoolmodel->deleteschool($school_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getAllschools()
	{
	  $this->load->Model('schoolmodel');
	  $data['school']=$this->schoolmodel->getallschools();
		
		echo json_encode($data);
		exit;
	
	}
	function getschoolbydistrict($id=false)
	{
	  $this->load->Model('schoolmodel');
	  $data['school']=$this->schoolmodel->getschoolbydistrict($id);
		
		echo json_encode($data);
		exit;
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	