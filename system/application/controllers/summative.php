<?php
error_reporting(0);
$view_path="system/application/views/";
include($view_path.'inc/class/pData.class.php');
include($view_path.'inc/class/pDraw.class.php');
include($view_path.'inc/class/pImage.class.php');
	
	require_once($view_path.'inc/libchart/classes/libchart.php'); 
	require_once($view_path.'inc/class.htmlgraph.php');
 require_once($view_path.'inc/html2pdf/html2pdf.class.php');	

class Summative extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false){
			//These functions are available only to schools - So redirect to the login page
			redirect("index");
		}
		
	}	
function getwindow1($school_id,$teacher_id,$report)
{

$view_path=$this->config->item('view_path');
$this->load->Model('observationpointmodel');
$this->load->Model('observationgroupmodel');
$this->load->Model('lickertgroupmodel');
$this->load->Model('lickertpointmodel');
$this->load->Model('proficiencypointmodel');
	
	  $this->session->set_userdata('school_id',$school_id);
	  $this->session->set_userdata('reportform',$report);
	  $this->session->set_userdata('report_criteria_id',$teacher_id);
	  $this->session->unset_userdata('reportmonth');
	  $this->session->unset_userdata('reportyear');
	  $this->session->unset_userdata('report_query_subject');
	  if($report=='forma')
		{
			
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($report=='formc')
		{
			
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();

		}
        else if($report=='formb')
		{
          $this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));

        }
		else if($report=='formp')
		{
			$this->load->Model('proficiencygroupmodel');
			$data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
			
		}			
        
		if(!empty($data['groups']))
		{
		 if($report!='formp')
			{
		 $data['reports']=$this->observationpointmodel->getAllReport('teacher');
		  }
		  else
		  {
			
			$data['reports']=$this->proficiencypointmodel->getAllReportpro('teacher');
		  }	
		 if($data['reports']!=false)
			{
				$countreport=count($data['reports']);
			
			}
			else
			{
			  $countreport=0;
			}
		if($report=='formb')
		{
		 foreach($data['groups'] as $groupval)
		  {
		  $sub_id=0;
		  $sub_scale_name='';
		 $group_id=$groupval['scale_id'];
		 $graph_id=$group_id;
		if(!empty($groupval['sub_scale_id']))
				{
				 $sub_id=$groupval['sub_scale_id'];
				
				}
				$group_name=$groupval['scale_name'];
			if($sub_id!=0)
		   {
            $graph_id=$sub_id;
			$sub_scale_name=$groupval['sub_scale_name'];
           
		  }
		
		$data['sectional']=$this->rubricscalemodel->getAllsectional($group_id,$sub_id,'teacher');	
		 print '<table width="100%" border="0">
  <tr>
    <td class="htitle">'.$group_name;
			 if(isset($sub_scale_name) && !empty($sub_scale_name)) { print'--->'.$sub_scale_name; } print '</td>
  </tr>
  <tr>
    <td style="font-size:14px;">Sectional Data</td>
  </tr>';
		 print '<tr>
    <td>
    
	<table  class="questab">
	
  <tr>
    <td  class="hlft">Report Number</td>
    <td class="rc1">';
    
    
	$rcc=1;
	if($data['reports']!=false) {
		$rc=0;	
			$showreport=1;
			print'<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			
			print '<td class="hbox">'.$showreport.'
			</td>';
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print '</td>
			<td class=rc'.$rcc.' style="display:none">';*/
			print'</tr><tr>';
			
			}
			$showreport++;
			 } 
			 print'</tr></table>';
			 } else { 
			
			print'<div>
			No Reports Found
			</div>';
			 } 
			print '<input type="hidden" id="rc" name="rc" value='.$rcc.'>
    </td>
  </tr>
  <tr>
    <td  class="hlft">Strengths:</td>
    <td class="rcf1">';
			$count=0;
			$str=0;
			if($data['reports']!=false && $data['sectional']!=false) {
			$rc=0;
			$rcc=1;			
			print'<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && !empty($secval['strengths']) )
			{
			 $str++;
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			 }
			else
			{
			print'<td class="hbox">&nbsp;</td>';
			
			 }
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print'</tr><tr>';
			
			}
			
			} 
			print'</tr></table>';
			} else { 
			
			 } print'</td>
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
  </tr>
  <tr>
    <td class="hlft">Concerns:</td>
    <td class="rcf1">';
			$count=0;
			$con=0;
			if($data['reports']!=false && $data['sectional']!=false) { 
			$rc=0;
			$rcc=1;	
			print'<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && !empty($secval['concerns']) )
			{
			 $con++;
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			 }
			else
			{
			print'<td class="hbox">&nbsp;</td>';
			
			 }
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print'</tr><tr>';
			
			}
			
			} 
			print'</tr></table>';
			} else { 
			
			 } 
            
            print'</td>
			
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
  </tr>
  <tr>
  <td class="hlft">Score:</td>
    <td class="rcf1">';
			$count=0;
			$scr=0;
			if($data['reports']!=false && $data['sectional']!=false) {
			$rc=0;
			$rcc=1;				
			print'<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && !empty($secval['score']) )
			{
			 $scr++;
			 $count++;
			 $check=1;
			 $score=$secval['score'];
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">'.$score.' </td>';
			
			 }
			else
			{
			print'<td class="hbox">&nbsp;</td>';
			
			 }
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print'</tr><tr>';
			
			}
			
			} 
			
			print'</tr></table>';
			} else { 
			
			 } 
            
            print'</td>
  </tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   
	
    ';
	
	if($data['reports']!=false)
  {
  
	print'<tr>';
  
  
  $login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('school_id');

			}
  $chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	$dataSet->addPoint(new Point("Scaled Level 1", $str));
	$dataSet->addPoint(new Point("Scaled Level 2", $con));
	$dataSet->addPoint(new Point("Scaled Level 3", $scr));
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$graph_id.".png");
	
	print'<td>
	<img alt="Bar chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$graph_id.'.png" style="border: 1px solid gray;"/>
	</td>
	
  </tr>';
 } 
   print' </table>';
	print'
	 </td>
  </tr>';
		print '</table>'; 
		}
		
		}
		if($report=='forma' || $report=='formc' || $report=='formp')
		{
		//$k=0;
		foreach($data['groups'] as $groupval)
		  {
            //if($k==0 || $k==1)
			{
           // $k++;			
			print'<div class="htitle">'.$groupval['group_name'].'</div>';
			 
             if($report=='forma')
		{
			
			$data['points']=$this->observationpointmodel->getAllPoints($groupval['group_id'],$this->session->userdata('district_id'));
		}
		else  if($report=='formc')
		{
			
			$data['points']=$this->lickertpointmodel->getAllPoints($groupval['group_id'],$this->session->userdata('district_id'));
		}
		else  if($report=='formp')
		{
			
			$data['points']=$this->proficiencypointmodel->getAllPoints($groupval['group_id'],$this->session->userdata('district_id'));
		}
          
		 
			if($report!='formp')
			{
				$data['sectional']=$this->observationpointmodel->getAllsectional($groupval['group_id'],'teacher');
			}
            else
            {
			   $data['sectional']=$this->proficiencypointmodel->getAllsectionalpro($groupval['group_id'],'teacher');

             }			
		  if($data['sectional']!=false)
			{
				$countsectional=count($data['sectional']);
			
			}
			else
			{
			  $countsectional=0;
			}
			if($data['reports']!=false)
			{
				$countreport=count($data['reports']);
			
			}
			else
			{
			  $countreport=0;
			}
		  if(isset($data['points']))
		{ 
		  $group_set=0;
		  
		  if($data['points']!='')
		  {
		  foreach($data['points'] as $pointval)
		  {
		    if($group_set!=1)
			{
			if($pointval['group_type_id']==1 || $pointval['group_type_id']==2 )
			{
			 $group_set=1;
			 $point_set=$pointval['point_id'];
			 break;
			}
            }		  
		  }
		  }
		  
		  
		
		
		}
		if(!isset($data['points']))
		{
			 $data['points']=false;
		}
		
		 if($report=='formc' || $report=='formp')
		 {
		  
		if($data['points']!=false) {
		$login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}
			
			  
			 
			  
			  
			  
			  
			  if($group_set==1)
			{ 
			print '<table>
			<tr>
						  <td colspan="2">
						  <font color="#08A5D6"><b></b></font>
						  </td>
						  </tr>
						  <tr>
  <td colspan="2">
  <img alt="Pie chart"  src='.WORKSHOP_DISPLAY_FILES.'stacked/'.$login_type.'_'.$login_id.'_'.$point_set.'.png style="border-right: 2px solid gray;"/>
  </td>
</tr>
</table>';
 } 
     
			$point_again=0;
			$group_id=0;
			$allpiec=0;
			$allpiedata=array();
			$allpiecdata=array();
			$previousnext=0;
			$subgroup=2;
			foreach($data['points'] as $val)
			{
			$previousnext++;
			//if($this->session->userdata('reportform')=='forma')
			{
			if($point_again!=$val['point_id'] && $group_id==2)
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			
			  print'<tr>
  <td colspan="2">';
 
	$chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	
	print '<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png" style="border: 1px solid gray;"/>
  </td>
  </tr>';
			
			 } } 
			if($previousnext==1)
			{
			
			
			/*print '<table width="100%">
			<tr>
			<td>
			<div style="float:right; "><input type="button" id="previous" name="previous" value="<<" style="display:none">
			<input type="button" id="next" name="next" value=">>" style="display:none"></div>
			</td>
			</tr>
			</table>';*/
			
			 } }
			if($val['group_type_id']!=2)
			{
			if($subgroup==1)
			{
			
			 print '</table>';
			}
			$subgroup=0;
			
                
                print '<table class="questab" >
  
  <tr>
    <td colspan="2"><div class="ques1">'.$val['question'].'</div></td>
    </tr>
  <tr>
    <td class="hlft">Report Number:</td>
    <td class="rc1">';
			$rcc=1;
			if($data['reports']!=false) { 
			$rc=0;
			$showreport=1;
			print'<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			
			print '<td class="hbox">'.$showreport.'</td>';
			
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rc'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			$showreport++;
			} 
			print'</tr></table>';
			} else { 
			print '<div>
			No Reports Found
			</div>';
			 } 
			print '
			<input type="hidden" id="rc" name="rc" value='.$rcc.'>
			</td>
		 
  </tr>
  <tr>
    <td><div class="hlft">Frequency:</div></td>
    <td class="rcf1">';
			$count=0;
			
			if($data['reports']!=false && $data['sectional']!=false) { 
			$rc=0;
			$rcc=1;
			print'<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			
			 }
			else
			{
			
	print'<td class="hbox">&nbsp;</td>';
			
			 }
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			
			}
			print'</tr></table>';
			} else { 
			
			 } 
			 print'</td>
			</tr>
			
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.'observations</div>
	
			</td>
 
  </tr>
  <tr>';
			
			{ 
  print'<td colspan="2">
  <div>';
  
 
	$chart = new VerticalBarChart(500, 250);  
  $dataSet = new XYDataSet();
	//$dataSet->addPoint(new Point("score", $countreport));
	$dataSet->addPoint(new Point("report",$count ));
	
	
	
	$chart->setDataSet($dataSet);
	
	$chart->setTitle("Bar Chart");
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$val['point_id'].".png");
	
	print'<img alt="Bar chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$val['point_id'].'.png" style="border: 1px solid gray;"/>
	</div>
  </td>';
   } 
  print'</tr>';
  
			{ 
			
			$allpiec++;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiecdata[$allpiec][]=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];


 
 } 
print'</table>';	
			}
			else if($val['group_type_id']==2) {
			$subgroup=1;
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			if($point_again!=0)
			{
			 print '</table>';
			}
			print'<table class="questab">
  <tr>
    <td colspan="3"><div class="ques1">'.$val['question'].'</div></td>
    </tr>
  <tr>
    <td><div class="hlft">Report Number:</div></td>
    <td class="rc1">';	
			$rcc=1;
			if($data['reports']!=false) { 
			$rc=0;
			$showreport=1;
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			
			print'<td class="hbox">'.$showreport.'</td>';
			
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rc'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			$showreport++;
			} 
			print '</tr></table>';
			} else { 
			print'<div>
			No Reports Found
			</div>';
			 } 
			print'<input type="hidden" id="rc" name="rc" value='.$rcc.'>
			</td>
  
  </tr>
  <tr>
    <td><div class="hlft">'.$val['sub_group_name'].'</div></td>
    <td class="rcf1">';	
			$count=0;
			if($data['reports']!=false && $data['sectional']!=false) { 
			$rc=0;
			$rcc=1;
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			
			
			 }
			else
			{
			
			print'<td class="hbox">&nbsp;</td>';
			
			
			 }
			 if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			
			} 
			print '</tr></table>';
			} else { 
	
			 } print'</td>';
   
		
			{ 
  print'<td rowspan="2">
  <div>
  </div>
  </td>';
   } 
			
  print'</tr>  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
  
  </tr>';

$piec=0;
$piedata[$piec]['name']=$val['sub_group_name'];
$piedata[$piec]['count']=$count;
$piedata[$piec]['point_id']=$val['point_id'];
			$allpiec++;
			$allpiecsub=0;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiedata[$allpiec]['subgroup']=1;
			$allpiedata[$allpiec][$allpiecsub]=$val['sub_group_name'];
			$allpiecdata[$allpiec][]=$count;
			//$allpiedata[$allpiec][$allpiecsub]['count']=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];
} 
			else {
			$piec++;
			$allpiecsub++;
			

  print'<tr>
    <td ><div class="hlft">'.$val['sub_group_name'].'</div></td>
    <td class="rcf1">';			
			$count=0;
			if($data['reports']!=false && $data['sectional']!=false) {
			$rc=0;
			$rcc=1;			
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			
			
			 }
			else
			{
			print'<td class="hbox">&nbsp;</td>';
			
			
			 }
			 if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			
			}
			 print '</tr></table>';
			
			 } else {  } print'</td>';
			 
			{
  print'<td rowspan="2">
  <div>';
  
	print'</div>
  </td>';
   } 
    
  print'</tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
 
  </tr>';
               		
        	      
				  
				  $piedata[$piec]['name']=$val['sub_group_name'];
				  $piedata[$piec]['count']=$count;
				  $piedata[$piec]['point_id']=$val['point_id'];
				  
				  
			
			
			$allpiedata[$allpiec][$allpiecsub]=$val['sub_group_name'];
			//$allpiedata[$allpiec][$allpiecsub]['count']=$count;
			$allpiecdata[$allpiec][]=$count;
			
				  } 
			
			
			 }
			
			$point_again=$val['point_id'];
			$group_id=$val['group_type_id'];
			
			}
			//if($this->session->userdata('reportform')=='forma')
			{
			if(!isset($plotteddata[$point_again]) )
			{
			if(isset($piedata[0]['point_id']))
			{
			if($point_again==$piedata[0]['point_id'])
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			
			  print'<tr>
  <td colspan="2">';
 
 $chart = new VerticalBarChart(500, 250);  
 //$chart = new PieChart(600, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	//$dataSet->addPoint(new Point('Score', $countreport));
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Bar Chart");
	
	$chart->render(WORKSHOP_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	
	print'<img alt="Bar chart"  src="'.WORKSHOP_DISPLAY_FILES.'bar/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png" style="border: 1px solid gray;"/>
  </td>
  </tr>';
			
			 
			
			
			}
			}
			}
			}
			}
			
			} 
			else
			{
			
			
	
			print'<div>No Observation Points Found</div>';
	
			 } //print_r($getreportpoints); 
			  if(isset($allpiec) && $allpiec!=0)
			{ 
	$MyData = new pData(); 
	
  
  $pdc=0;
  for($pj=1;$pj<=$allpiec;$pj++)
	{
	   
	   $cd=count($allpiecdata[$pj]);
       if($cd>$pdc)
	   {
	     $pdc=$cd;
	   
	   }
	
	}
	
	//echo '<pre>';
	//print_r($allpiecdata);
	$newallpiecdata=array();
	for($kj=0;$kj<$pdc;$kj++)
	{
	  
	  
	foreach($allpiecdata as $keyall=>$val)
	{
	  	if(isset($val[$kj]))
		{
		 $newallpiecdata[$kj][]=$val[$kj];
		
		}
		else
		{
		 $newallpiecdata[$kj][]=0;
		
		}
	}
	
	
	}
	//echo '<pre>';
	//print_r($newallpiecdata);
	$drawgraph=array();
	for($dr=1;$dr<=$pdc;$dr++)
	{
	$jhk=$dr-1;
	if(isset($allpiedata[1][$jhk]))
	{
		$drawgraph[$jhk]=$allpiedata[1][$jhk];
		
		$MyData->addPoints($newallpiecdata[$jhk],$allpiedata[1][$jhk]);
		
		
		
      $MyData->setSerieDescription($allpiedata[1][$jhk],$allpiedata[1][$jhk]); 		
	}
	else
	{
       $drawgraph[$jhk]='Element'.$dr;
	   
	   $MyData->addPoints($newallpiecdata[$jhk],'Element'.$dr);
		
		
		
      $MyData->setSerieDescription('Element'.$dr,'Element'.$dr); 		
	}		
	}
	$namelimit=array();
  for($pj=1;$pj<=count($allpiedata);$pj++)
	{
		
	    
		//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $allpiedata[$pj]['name']=preg_replace($sPattern, $sReplace, $allpiedata[$pj]['name']);
		
		if(strlen($allpiedata[$pj]['name'])>30)
		{
		   
		   $limitname=substr($allpiedata[$pj]['name'],0,30);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$allpiedata[$pj]['name'];
		
		}
		
		$namelimit[]=$limitname;
		
	}
	
	$MyData->addPoints($namelimit,'Element');
	
	$MyData->setAbscissa("Element");  
  
  

 /* Create the pChart object */
 $myPicture = new pImage(600,350,$MyData);

 /* Draw the background */
 $Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>0, "DashR"=>255, "DashG"=>255, "DashB"=>255);
 $myPicture->drawFilledRectangle(0,0,700,350,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>255, "EndG"=>255, "EndB"=>255, "Alpha"=>50);
 $myPicture->drawGradientArea(0,0,700,350,DIRECTION_VERTICAL,$Settings);
 //$myPicture->drawGradientArea(0,0,700,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,699,349,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/Silkscreen.ttf","FontSize"=>6));
 //$myPicture->drawText(10,13,"drawStackedBarChart() - draw a stacked bar chart",array("R"=>255,"G"=>255,"B"=>255));

 /* Write the chart title */ 
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/verdana.ttf","FontSize"=>7));
 $myPicture->drawText(250,55,"Summative Bar Graph",array("FontSize"=>20,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

 /* Draw the scale and the 1st chart */
 if(count($allpiedata)>6)
 {
 $myPicture->setGraphArea(80,60,450,220);
 }
 else
 {
    $myPicture->setGraphArea(80,60,250,220);
 
 }
 //$myPicture->drawFilledRectangle(60,60,450,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"Mode"=>SCALE_MODE_ADDALL,"LabelRotation"=>50));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
 $myPicture->setFontProperties(array("FontName"=>"$view_path/inc/Fonts/pf_arma_five.ttf","FontSize"=>8));
  $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"Surrounding"=>30));
  
 /*$Config = array("DisplayValues"=>1);
$myPicture->drawStackedBarChart($Config);*/

// $myPicture->drawStackedBarChart(array("DisplayValues"=>TRUE,"DisplayColor"=>DISPLAY_AUTO,"Rounded"=>TRUE,"Surrounding"=>60));
 $myPicture->setShadow(FALSE);
 


 /* Write the chart legend */
 $myPicture->drawLegend(510,5,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));
 
 /* Render the picture (choose the best way) */
 // $myPicture->autoOutput("pictures/example.drawStackedBarChart.png");
 $myPicture->Render(WORKSHOP_FILES."stacked/".$login_type.'_'.$login_id.'_'.$allpiedata[1]['point_id'].".png");
	
 } 
if($group_id==2) { 
 print'</table>';
 }
		}// end of formc
		
		else if($report=='forma')
		{
		
		 if($data['points']!=false) {
			$login_type=$this->session->userdata('login_type');
            if($login_type=='teacher')
			{
				$login_id=$this->session->userdata('teacher_id');

			}
            else if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}	
			if($group_set==1)
			{ 
			print '<table>
			<tr>
						  <td colspan="2">
						  <font color="#08A5D6"><b>All Elements Pie chart</b></font>
						  </td>
						  </tr>
						  <tr>
  <td colspan="2">
  <img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$point_set.'.png" style="border: 1px solid gray;"/>
  </td>
</tr>
</table>';
 } 
				
			
			
			$point_again=0;
			$group_id=0;
			$allpiec=0;
			$allpiedata=array();
			$allpiecdata=array();
			$previousnext=0;
			$subgroup=2;
			foreach($data['points'] as $val)
			{
			$previousnext++;
			
			{
			if($point_again!=$val['point_id'] && $group_id==2)
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			
			  print'<tr>
  <td colspan="2">';
  $chart = new PieChart(600, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	
	print'<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png" style="border: 1px solid gray;"/>
  </td>
  </tr>';
			
			 } } 
			if($previousnext==1)
			{
			
			
			/*print'<table width="100%">
			<tr>
			<td>
			<div style="float:right; "><input type="button" id="previous" name="previous" value="<<" style="display:none">
			<input type="button" id="next" name="next" value=">>" style="display:none"></div>
			</td>
			</tr>
			</table>';*/
			
			 } }
			if($val['group_type_id']!=2)
			{
			if($subgroup==1)
			{
			  print '</table>';
			}
               $subgroup=0; 
                print'<table class="questab" >
  
  <tr>
    <td colspan="2"><div class="ques1">'.$val['question'].'</div></td>
    </tr>
  <tr>
    <td class="hlft">Report Number:</td>
    <td class="rc1">';
	       
			$rcc=1;
			if($data['reports']!=false) { 
			$rc=0;
			$showreport=1;
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			
			print'<td class="hbox">'.$showreport.'</td>';
			
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rc'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			$showreport++;
			} 
			print '</tr></table>';
			} else { 
			print'<div>
			No Reports Found
			</div>';
			 } 
			print'
			<input type="hidden" id="rc" name="rc" value='.$rcc.'>
			</td>
		 
  </tr>
  <tr>
    <td><div class="hlft">Frequency:</div></td>
    <td class="rcf1">';
			$count=0;
			
			if($data['reports']!=false && $data['sectional']!=false) { 
			$rc=0;
			$rcc=1;
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			
			print'<td class="hbox">X</td>';
			
			
			
			 }
			else
			{
			
	print'<td class="hbox">&nbsp;</td>';
			
			 }
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			
			} 
			
			print '</tr></table>';
			} else { 
			
			 } print'</td>';
			// if($this->session->userdata('reportform')=='forma')
			{ 
  print'<td rowspan="2">
  <div style="padding-left:10px; position: relative; top:-80px;">';
  
  $gr = new HTMLGraph("37", "100", " ");
    
  
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();
	
	print'</div>
  </td>';
   } 
  print'</tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div>
	
			</td>
 
  </tr>';
  
			{ 
			
			$allpiec++;
			$allpiedata[$allpiec]['name']=$val['question'];
			$allpiedata[$allpiec]['count']=$count;
			$allpiedata[$allpiec]['point_id']=$val['point_id'];

			
  
   } 
print'</table>';
			}
			else if($val['group_type_id']==2) {
			$subgroup=1;
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			if($point_again!=0)
			{
			 print '</table>';
			}
			print'<table class="questab">
  <tr>
    <td colspan="3"><div class="ques1">'.$val['question'].'</div></td>
    </tr>
  <tr>
    <td><div class="hlft">Report Number:</div></td>
    <td class="rc1">';	
			$rcc=1;
			if($data['reports']!=false) { 
			$rc=0;
			$showreport=1;
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			
			print'<td class="hbox">'.$showreport.'</td>';
			
			if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rc'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			$showreport++;
			} 
			
			print '</tr></table>';
			} else { 
			print'<div>
			No Reports Found
			</div>';
			 } 
			print'<input type="hidden" id="rc" name="rc" value='.$rcc.'>
			</td>
  
  </tr>
  <tr>
    <td><div class="hlft">'.$val['sub_group_name'].'</div></td>
    <td class="rcf1">';	
			$count=0;
			if($data['reports']!=false && $data['sectional']!=false) { 
			$rc=0;
			$rcc=1;
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			
			
			 }
			else
			{
			
			print'<td class="hbox">&nbsp;</td>';
			
			
			 }
			 if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			
			} 
			print '</tr></table>';
			} else { 
	
			 } print'</td>';
   
		
			{ 
  print'<td rowspan="2">
  <div style="padding-left:10px;>';
  
  $gr = new HTMLGraph("37", "100", " ");
    
   
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();
	
	print'</div>
  </td>';
   } 
			
  print'</tr>  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
  
  </tr>';

$piec=0;
$piedata[$piec]['name']=$val['sub_group_name'];
$piedata[$piec]['count']=$count;
$piedata[$piec]['point_id']=$val['point_id'];
} 
			else {
			$piec++;
			

  print'<tr>
    <td ><div class="hlft">'.$val['sub_group_name'].'</div></td>
    <td class="rcf1">';		
			$count=0;
			if($data['reports']!=false && $data['sectional']!=false) {
			$rc=0;
			$rcc=1;			
			print '<table><tr>';
			foreach($data['reports'] as $reportval)
			{
			$rc++;
			$check=0;
			
			foreach($data['sectional'] as $secval)
			{
			if($reportval['report_id']==$secval['report_id'] && $secval['point_id']==$val['point_id'] && $secval['response']==$val['sub_group_id'] )
			{
			 $count++;
			 $check=1;
			 } 
			
			
			} 
			if($check==1)
			{
			
			print'<td class="hbox">X</td>';
			
			
			
			 }
			else
			{
				print'<td class="hbox">&nbsp;</td>';
			
			
			 }
			 if($rc>12)
			{
			$rcc++;
			$rc=0;
			
			/*print'</td>
			<td class=rcf'.$rcc.' style="display:none">';*/
			print '</tr><tr>';
			
			}
			
			}
			print '</tr></table>';
			 } else { 

			 } print'</td>';
			 
			{ 
  print'<td rowspan="2">
  <div style="padding-left:10px;>';
 
  $gr = new HTMLGraph("37", "100", " ");
    
  
    
    // now create a BarPlot object (this is the canvas for bars)
    $plot = new HTMLGraph_BarPlot();
  $values = array($count, $countreport);
    
    // add the values to the graph
    $colors = array("#58728D", "#AD5257");$plot->add($values,false,$colors);
    
    // add the plot to the graph
    $gr->add($plot);

    // add a footnote to the graph
    
    
    // output the graph
    $gr->render();
	
	print'</div>
  </td>';
   } 
    
  print'</tr>
  <tr>
    <td>Element Observed:</td>
    <td><div class="subobs">'.$count.' of '.$countreport.' observations</div></td>
 
  </tr>';
               		
        	      
				  
				  $piedata[$piec]['name']=$val['sub_group_name'];
				  $piedata[$piec]['count']=$count;
				  $piedata[$piec]['point_id']=$val['point_id'];
				  } 
			
			
			 }
			
			$point_again=$val['point_id'];
			$group_id=$val['group_type_id'];
			
			}
			if($this->session->userdata('reportform')=='forma')
			{
			if(!isset($plotteddata[$point_again]) )
			{
			if(isset($piedata[0]['point_id']))
			{
			if($point_again==$piedata[0]['point_id'])
			{
			if(isset($piec) && $piec!=0  )
			{
			$plotteddata[$piedata[0]['point_id']]=1;
			
			  print'<tr>
  <td colspan="2">';
  $chart = new PieChart(600, 300);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=0;$pj<=$piec;$pj++)
	{
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $piedata[$pj]['name']=preg_replace($sPattern, $sReplace, $piedata[$pj]['name']);
		if(strlen($piedata[$pj]['name'])>35)
		{
		   $limitname=substr($piedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($piedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$piedata[$pj]['name'];
		
		}
		//print_r($piedata[$pj]['name']);
		$dataSet->addPoint(new Point(trim($limitname).'('.$piedata[$pj]['count'].')', $piedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].".png");
	
	print'<img alt="Pie chart"  src="'.WORKSHOP_DISPLAY_FILES.'generated/'.$login_type.'_'.$login_id.'_'.$piedata[0]['point_id'].'.png" style="border: 1px solid gray;"/>
  </td>
  </tr>';
			
			 
			
		
			}
			}
			}
			}
			}
			
			} 
			else
			{
			
			
	
			print'<div>No Observation Points Found</div>';
	
			 } 
			  if(isset($allpiec) && $allpiec!=0)
			{ 
						  
  $chart = new PieChart(650, 350);

	$dataSet = new XYDataSet();
	//print_r($piedata);
	for($pj=1;$pj<=$allpiec;$pj++)
	{
		//print_r($piedata[$pj]['name']);
		$sPattern = '!\s+!'; 
		$sReplace = ' ';
        $allpiedata[$pj]['name']=preg_replace($sPattern, $sReplace, $allpiedata[$pj]['name']);
		
		if(strlen($allpiedata[$pj]['name'])>35)
		{
		   
		   $limitname=substr($allpiedata[$pj]['name'],0,35);
		   $last=strripos($limitname,' ');
		   $limitname=substr($allpiedata[$pj]['name'],0,$last);
		   $limitname.='...';
		
		}
		else
		{
		  $limitname=$allpiedata[$pj]['name'];
		
		}
		$dataSet->addPoint(new Point(trim($limitname).'('.$allpiedata[$pj]['count'].')', $allpiedata[$pj]['count']));
	}	
	
	$chart->setDataSet($dataSet);
$chart->setTitle("Pie Chart");
	
	$chart->render(WORKSHOP_FILES."generated/".$login_type.'_'.$login_id.'_'.$allpiedata[1]['point_id'].".png");
	
	
 }
if($val['group_type_id']==2) { 
 print'</table>';
 }
		
		
		}// end of forma

		}
		 } // end of foreach
		
		}
		
		}
		 else { 
			print'No Groups Found';
			} 

  
}

function createpdf()
{
//set_time_limit(3600);

 $data['status']=0;
 $pdata=$this->input->post('pdata');
$view_path=$this->config->item('view_path');
   ob_start();
   $healthy = array(SITEURLM);
$yummy   = array("");

   $newphrase =str_replace($healthy, $yummy, $pdata['msg']);
  
  
  $str = '<style type="text/css">
  @charset "utf-8";
/* CSS Document */

body{ background:#f5f5f5; margin:0px; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;}
a { text-decoration:none;}
input,select{font-size:12px;  color:#02aad2; margin:5px 5px 5px 0px; background-color:#f6ffff; }

textarea{font-size:12px;  color:#02aad2;margin:0px 0px 5px 0px; width:650px; border:1px #70b8ba solid; height:70px; background-color:#f6ffff;}



.wrapper{margin-left:auto; margin-right:auto; width:1000px; min-height:900px; background:url(../images/body_bg_30.jpg);}
.wrapper1{margin-left:auto; margin-right:auto; width:1000px; min-height:900px; background:url(../images/body_bg_30.jpg);}
.wrapper2{margin-left:auto; margin-right:auto; width:1000px; min-height:900px; background:url(../images/body_bg_30.jpg);}
.wrapper3{margin-left:auto; margin-right:auto; width:1000px; min-height:900px; background:url(../images/body_bg_30.jpg);}
.wrapperhome{margin-left:auto; margin-right:auto; width:1000px; min-height:550px; background:url(../images/body_bg_30.jpg);}
.header{ width:905px; background:url(../images/Header_bg_02.jpg); height:152px; margin-left:48px; }
.logo{ width:411px; height:44px; background:url(../images/logo_05.png) no-repeat 0 0; margin-top:14px; margin-left:20px; position:absolute;}
.book_img{ width:138px; height:152px; background:url(../images/apple_img_03.jpg) no-repeat 0 60px; position:absolute;}
.header_img{ width:355px; height:64px; background:url(../images/header_Image_02.png) no-repeat 0 0; float:right; margin-right:20px;}
.mbody{ width:865px;margin-left:58px; margin-top:10px; padding-bottom:15px;  position:relative;}

.leftmtop_main{ width:180px; height:5px; background:url(../images/leftmenu_10.jpg) no-repeat 0 0;}
.leftmtop{ width:314px; height:5px; background:url(../images/sm_body_new_03.png) no-repeat 0 0;}

.leftmenu{width:200px; height:auto; background:url(../images/leftmenu_12.jpg); padding-top:10px;padding-bottom:10px;float:left;}


.lnktxt_main{ background:url(../images/arrow.png) no-repeat 5px 0; height:auto; line-height:20px; padding-left:20px; font-size:12px; font-weight:bold; color:#02aad2;  width:160px;}
.lnktxt_main:hover{color:#b03e2e; cursor:pointer;}

.HTMLGraph_fullSize{vertical-align:bottom;
width:100%;
height:100%;
}



.lnktxt{ background:#fff url(../images/arrow.png) no-repeat 5px 0; height:auto; line-height:20px; padding-left:20px; font-size:12px; font-weight:bold; color:#02aad2; padding-right:35px; border-left:#e8b9b0 1px solid; border-right:#e8b9b0 1px solid;}
.lnktxt:hover{}
.lnktxt a{ font-size:12px; font-weight:bold; color:#02aad2; }
.lnktxt a:hover{color:#b03e2e; }

.slnktxt{ background:#fff ; height:auto; line-height:20px; padding-left:30px; font-size:12px; font-weight:bold; color:#02aad2; padding-right:35px; border-left:#e8b9b0 1px solid; border-right:#e8b9b0 1px solid;}
.slnktxt:hover{}
.slnktxt a{ font-size:12px; font-weight:bold; color:#02aad2; }
.slnktxt a:hover{color:#b03e2e; }

.lnktxt1{ background:#fff url(../images/arrow.png) no-repeat 5px 0; height:auto; line-height:20px; padding-left:20px; font-size:12px; font-weight:bold; color:#02aad2;  border-left:#e8b9b0 1px solid; border-right:#e8b9b0 1px solid;width:158px;}
.lnktxt1:hover{}
.lnktxt1 a{ font-size:12px; font-weight:bold; color:#02aad2; }
.lnktxt1 a:hover{color:#b03e2e; }

.smalllnktxt{ height:20px; line-height:20px; padding-left:30px; font-size:12px;  color:#02aad2;padding-right:35px;}
.smalllnktxt a{ font-size:12px;  color:#02aad2; }
.smalllnktxt a:hover{color:#b03e2e; }

#pagination{ font-size:12px; font-weight:bolder;}
#pagination a{ color:#02aad2;}
#pagination a:hover{ color:#b03e2e;}


.hdtxtReport{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px;  padding-left:15px; background:url(../images/leftmenu_15.jpg) no-repeat 161px 0;}
/*.hdtxtReport:hover{ color:#900; cursor:pointer;}*/
.hdtxtReport a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtReport a:hover{color:#b03e2e; cursor:pointer; }

.hdtxtVideoTutorial{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px; padding-left:15px; background:url(../images/leftmenu_17.jpg) no-repeat 161px 0;}
/*.hdtxtVideoTutorial:hover{ color:#900; cursor:pointer;}*/
.hdtxtVideoTutorial a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtVideoTutorial a:hover{color:#b03e2e;  cursor:pointer;}


.hdtxtblank{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px; padding-left:15px;}
/*.hdtxtVideoTutorial:hover{ color:#900; cursor:pointer;}*/
.hdtxtblank a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtblank a:hover{color:#b03e2e;  cursor:pointer;}
.hdtxtblank a:active{ color:#9C6;}


.hdtxtblank_c{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px;width:145px; margin-left:35px;}
/*.hdtxtblank:hover{ color:#900; cursor:pointer;}*/
.hdtxtblank_c a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtblank_c a:hover{color:#b03e2e;  cursor:pointer;}

.hdtxtblank_o{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px; margin-left:65px; width:115px;}
/*.hdtxtblank:hover{ color:#900; cursor:pointer;}*/
.hdtxtblank_o a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtblank_o a:hover{color:#b03e2e;  cursor:pointer;}

.logout{ width:67px; height:23px; line-height:20px; background:url(../images/logoutNew_08.png) no-repeat 0 0; float:right; padding-left:25px;font-family:Verdana, Geneva, sans-serif; font-size:10px; font-weight:bold; color:#fff;}
.logout:hover{ background:url(../images/logoutNew_06.png) no-repeat 0 0;}
.logout a{ color:#FFF;}

.user{float: right; margin-top: 88px; margin-right: -375px; width: 750px; color:#a68100; font-size:13px; height:23px; line-height:23px;}

.imgReport{ width:50px; height:50px; background:url(../images/leftmenu_15_40.jpg) no-repeat 4px 4px;float:left; margin-top:-50px; margin-left:-18px;}
.imgVideo{ width:50px; height:50px; background:url(../images/leftmenu_17_40.jpg) no-repeat 4px 4px;float:left; margin-top:-50px; margin-left:-18px;}
.imgOnline{ width:50px; height:50px; background:url(../images/leftmenu_21_401.jpg) no-repeat 4px 4px;float:left; margin-top:-50px; margin-left:18px;}
.imgOffline{ width:50px; height:50px; background:url(../images/leftmenu_19_401.jpg) no-repeat 4px 4px;float:left; margin-top:-50px; margin-left:18px;}
.imgPlan{ width:50px; height:50px; background:url(../images/planning_03_40.jpg) no-repeat 4px 4px;float:right;float:left; margin-top:-50px; margin-left:-18px;}
.imgComm{ width:50px; height:50px; background:url(../images/communication_03_40.jpg) no-repeat 4px 4px;float:right;float:left; margin-top:-50px; margin-left:-18px;}
.imgCapBld{ width:50px; height:50px; background:url(../images/capacityBuilding_03_40.jpg) no-repeat 4px 4px;float:right;float:left; margin-top:-50px; margin-left:-18px;}
.imgSections{ width:50px; height:50px; background:url(../images/sections_03_40.jpg) no-repeat 4px 4px;float:right;float:left; margin-top:-50px; margin-left:-18px;}
.imgDevTools{ width:50px; height:50px; background:url(../images/devtools_03_40.jpg) no-repeat 4px 4px;float:right;float:left; margin-top:-50px; margin-left:-18px;}
.imgImpl{ width:50px; height:50px; background:url(../images/implementation_03_40.jpg) no-repeat 4px 4px;float:right;float:left; margin-top:-50px; margin-left:-18px;}

.hdtxtOffline{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px;  padding-left:15px; background:url(../images/leftmenu_19.jpg) no-repeat 161px 0;}
/*.hdtxtOffline:hover{ color:#900; cursor:pointer;}*/
.hdtxtOffline a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtOffline a:hover{color:#b03e2e;  cursor:pointer;}

.hdtxtOnline{ font-size:14px; font-weight:bold; color:#657455; height:50px; line-height:50px;  padding-left:15px; background:url(../images/leftmenu_21.jpg) no-repeat 161px 0;}
/*.hdtxtOnline:hover{ color:#900; cursor:pointer;}*/
.hdtxtOnline a{ text-decoration:none; font-size:14px; font-weight:bold; color:#657455; }
.hdtxtOnline a:hover{color:#b03e2e; cursor:pointer; }

.leftmbot_main{width:180px; height:5px; background:url(../images/leftmenu_23.jpg) no-repeat 0 0;}
.leftmbot{width:314px; height:7px; background:url(../images/sm_body_new_06.png) no-repeat 0 0px;}

.wrapperfoot{ margin-left:auto; margin-right:auto; width:1000px; height:17px; background:url(../images/body_bg_31.jpg) no-repeat 0 0;}
.foottop{ clear:both;margin-left:auto; margin-right:auto; width:1000px; height:14px; background:url(../images/Footer_bg_32.jpg) no-repeat 0 0;}
.footmid{ margin-left:auto; margin-right:auto; width:1000px;height:30px; line-height:30px; background:url(../images/Footer_bg_33.jpg);}
.foottxt{ margin-left:68px; font-size:10px; color:#FFF;}
.footbot{ margin-left:auto; margin-right:auto; width:1000px;height:20px; background:url(../images/Footer_bg_35.jpg) no-repeat 0 0;}

.content{ max-width:646px; font-size:12px; color:#657455; margin-left:215px; height:auto;}
.content1{width:615px; font-size:14px; color:#657455;}
.hometab{ border:#94a87e 1px solid; padding:20px; margin-left:260px; margin-top:100px;}

.combobox{  width:213px; height:23px; border:1px #70b8ba solid; font-size:14px;}
.combobox1{  width:180px; height:23px; border:1px #70b8ba solid; font-size:14px;}
.combobox2{  width:100px; height:23px; border:1px #70b8ba solid; font-size:14px;}
.txtbox{  width:206px; height:18px; border:1px #70b8ba solid; font-size:14px;}
.txtbox1{  width:100px; height:18px; border:1px #70b8ba solid; font-size:14px;}
.comboboxsml{  width:82px; height:23px; border:1px #70b8ba solid; font-size:14px;}
.txtarea{  width:206px; height:68px; border:1px #70b8ba solid; font-size:14px;}

.f14{ font-size:14px;}

.lft{ width:150px; height:auto; position:relative; float:left; margin-left:15px; }

.tdlink a{ text-decoration:none; font-weight:bold;  color:#02aad2; }
.tdlink a:hover{color:#b03e2e; }

.jqform{ width:370px; font-size:13px; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; color:#657455; margin-left:auto; margin-right:auto;}
.jqform1{ width:670px; font-size:13px; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; color:#657455; margin-left:auto; margin-right:auto;}
#myTable{  width:370px; border:1px #e6deb7 solid; padding:2px;}

.tabcontent{ width:635px; border:#a2ba88 1px solid;}
.tabcontent TD{ padding:0px 10px 0px 10px; text-align:center; height:30px;}

.tabcontent1{ width:667px; border:#a2ba88 1px solid;padding:0px;}
.tabcontent1 TD{vertical-align:top;padding:5px 0px 5px 5px; text-align:left;background-color:#FFF;}
.bl{ color:#048dc4;}
.tchead{ font-weight:bold; background:#8ca275; color:#FFF;}

.tabreport{ margin-left:150px;}

.tcrow2{ background-color:#eef9e3;}

.btnsmall{ text-transform:capitalize; background:#76BCBE url(../images/btn_tor_hover_06.jpg) no-repeat 0 0; width:67px; height:23px; color:#FFF; font-weight:bold; border:none; font-size:10px; font-family:Verdana, Geneva, sans-serif; margin-left:5px; margin-right:5px; padding-bottom:3px;}
.btnsmall:hover{ background:#8EB566 url(../images/btn_tor_03.jpg) no-repeat 0 0; cursor:pointer;}
.btnconf93{  text-transform:capitalize; background:#76BCBE url(../images/btn_tor_big_hover_11.jpg) no-repeat 0 0; width:105px; height:30px; color:#fff; font-weight:bold;font-size:11px; font-family:Verdana, Geneva, sans-serif; border:none; padding-bottom:4px;}
.btnconf93:hover{ background:#8EB566 url(../images/btn_tor_big_08.jpg) no-repeat 0 0; cursor:pointer;}

.btnsmall_long{  text-transform:capitalize;background:#76BCBE url(../images/btnsmall_long_05.jpg) no-repeat 0 0; width:119px; height:23px; color:#FFF; font-weight:bold; border:none; font-size:10px; font-family:Verdana, Geneva, sans-serif; margin-left:5px; margin-right:5px; padding-bottom:3px;}
.btnsmall_long:hover{ background:#8EB566 url(../images/btnsmall_long_03.jpg) no-repeat 0 0; cursor:pointer;}

.btnsmall_long2{  text-transform:capitalize;background:#76BCBE url(../images/btn_small_long2_22.jpg) no-repeat 0 0; width:175px; height:23px; color:#FFF; font-weight:bold; border:none; font-size:10px; font-family:Verdana, Geneva, sans-serif; margin-left:5px; margin-right:5px; padding-bottom:3px;}
.btnsmall_long2:hover{ background:#8EB566 url(../images/btn_small_long2_19.jpg) no-repeat 0 0; cursor:pointer;}

.btnbig{  text-transform:capitalize; background:#76BCBE url(../images/btn_tor_big_hover_11.jpg) no-repeat 0 0; width:105px; height:30px; color:#fff; font-weight:bold;font-size:11px; font-family:Verdana, Geneva, sans-serif; border:none; padding-bottom:4px;}
.btnbig:hover{ background:#8EB566 url(../images/btn_tor_big_08.jpg) no-repeat 0 0; cursor:pointer;}

.htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
.desc{  color:#0090b2;  font-size:11px;}
.queshd{  color:#0090b2;  font-size:12px; padding-left:30px;}
.ques{ color:#333; font-weight:bold; font-size:11px; margin-top:5px;}

.ques1{color:#b97533; font-weight:bold; font-size:12px; font-weight:bold; margin-top:5px; width:620px; }
.hlft{ width:220px; float:left;  font-size:10;color:#333; font-weight:bold;}
.hlftsub{ width:100px; float:left; height:20px; font-size:10;color:#333;}
.flft{ width:465px; float:left;  height:20px;}
.hlftcb{width:150px; float:left;}

.subobs{margin-right:13px;}

.bx{ border:1px #70b8ba solid; margin:10px 0px 10px 0px;}
.questab{background-color:#f4ffff; border:1px #d8ebeb solid; padding:5px; margin-top:3px; width:400px;}

.hbox{color:#0090b2;  font-size:11px; width:20px;float:left; background-color:#FFF; text-align:center; margin-right:2px; border:1px #e3f3f3 solid; height:16px; margin-bottom:2px;}
.hboxlong{color:#0090b2;  font-size:11px;width:400px; }



.error{ color:#900; font-size:12px;}



#msgContainer #paginationall ul li.inactive,  #msgContainer #paginationall ul li.inactive:hover {
	color:#859970;
	cursor: default;
	font-size:12px;
}
#msgContainer .data ul li {
	list-style: none;
	font-family: verdana;
	margin: 5px 0 5px 0;
	color: #000;
	font-size: 13px;
}
#msgContainer #paginationall {
	
	width: 615px;
	height: 25px;
	
}
#msgContainer #paginationall ul{ margin-left:-50px;	}
#msgContainer #paginationall ul li {
	list-style: none;
	float: left;
	padding: 2px 6px 2px 6px;
	margin: 0 3px 0 3px;
	font-family: arial;
	font-size: 12px;
	color: #0798b7;
	font-weight: bold;
	}
#msgContainer #paginationall ul li:hover {
	
	cursor: pointer;
}

.total {
	float:right;
	font-family:arial;
	color:#999;
}


#navigation {width:200px; height:auto; float:left;}

/*Main Menu*/
#navigation ul {margin:0px; padding:0px;}

#navigation ul li {
height:50px;
line-height:50px; 
list-style:none;

width:180px;

}

#navigation ul li:first-child { border:none;}

#navigation ul li a {}
#navigation ul li ul li a {color:#02AAD2;}
#navigation ul li ul li a:hover {color:#900; }

#navigation ul li.blank:hover{ background:#FFF;}
#navigation ul li:hover { position:relative;}

#navigation ul ul {display:none; position:absolute; left:165px; top:1px; border-width:1px;font-size:10px; font-weight:bold; width:314px; }
#navigation ul li:hover ul {display:block;}



#navigation ul ul li {  width:314px; height:auto;float:left; display:inline; border:none; overflow:hidden;}
#navigation ul ul li:hover { border:none;}
 
#navigation li:hover ul li ul {display:none;}

#navigation ul ul li ul {left:200px; background-color:#a8a7a7;font-size:10px;}
#navigation ul ul li:hover ul {display:block;}
.HTMLGraph_graph
{
    border: 1px solid #666666;
    background-color: #ffffff;
}
.HTMLGraph_fullSize
{
    position: relative;
    width: 10px;
    height: 10px;
}
.HTMLGraph_title
{
    font-family:   arial;
    font-size: 14px;
    font-weight: bold;
    color: #000000;
    text-align: center;
    padding: 5px;
}
.HTMLGraph_xAxis
{
    border-bottom: 1px solid #666666;
}
.HTMLGraph_barVertical
{
    background-color: #aabbcc;
    border-right: 1px solid #000000;
    border-top: 1px solid #dedede;
    border-left: 1px solid #dedede;
}
.HTMLGraph_valueTopMaring
{
    font-family: arial;
    font-size: 11px;
    background-color: transparent;
}
.HTMLGraph_value
{
    position: absolute;
    bottom: 0px;
    font-family:  arial;
    font-size: 11px;
}
.HTMLGraph_label
{
    font-family: arial;
    font-size: 11px;
    width: 10px;
    text-align: center;
    overflow: hidden;
}
.HTMLGraph_plotLabel
{
    font-family:arial;
    font-size: 11px;
    font-weight: bold;
    width: 10px;
    text-align: center;
    overflow: hidden;
}
.HTMLGraph_footnote
{
    font-family:  arial;
    font-size: 10px;
    font-weight: normal;
    color: #000000;
    text-align: right;
}
.HTMLGraph_xAxisTick
{
    width: 1px;
    height: 5px;
    background-color: #666666;
}

  </style>
  
<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              
        </page_footer> 
		<HTML>
        <BODY style="font-size:10px;padding:10px;">
		<b><div style="font-size:14px;padding-left:300px;">Document One:</div></b><br/>
		'.$newphrase.'
		<b><div style="font-size:14px;padding-left:250px;">Additional Documents Continued on Next Page...</div></b><br/>        
		</BODY>
		</HTML>
   </page> 
';

   $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output(WORKSHOP_FILES.'pdf/'.$pdata['logintype'].$pdata['login_id'].'.pdf','F');
    $data['status']=1;
	echo json_encode($data);
	exit;



}

function upload()
{

$data['status']=0;
if($_POST['window2']=='file')
{
if($_FILES['upload']['size']>0 )
		{
			 $login_type=$this->session->userdata('login_type');
             if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}
			
			 $view_path=$this->config->item('view_path');
			$filename=explode('.',$_FILES['upload']['name']);
			print_r($filename);exit;
			$target_path = WORKSHOP_FILES."/uploads/";
			echo $target_path = $target_path.$login_type.$login_id.'.pdf';exit;
			if($filename[1]=='pdf') 
			{
			
			

			if(move_uploaded_file($_FILES['upload']['tmp_name'], $target_path))
			{
			  
			 $data['status']=1;
			 echo json_encode($data);
			 exit;
			}
			else
			{
			  $data['mesage']='Failed To Upload Please Try Again.';
			 $data['status']=0;
			 echo json_encode($data);
			 exit;
			
			}
			
			}
			else
			{
			
			$data['mesage']='Please Upload A PDF File';
			 $data['status']=0;
			 echo json_encode($data);
			 exit;
			
			}
			
			}
			else
			{
			 $data['mesage']='Please Upload File';
			 $data['status']=0;
			 echo json_encode($data);
			 exit;
			
			}
        }
		else
		{
		   
		  $data['status']=1;
		  
		  $data['mesage']=$_POST['url'];
		  
		  
		  echo json_encode($data);
		  exit;
		
		
		}



}
function general()
{
$pdata=$this->input->post('pdata');
$view_path=$this->config->item('view_path');
$login_type=$this->session->userdata('login_type');
$comments=$pdata['comments'];
$status=$pdata['status'];
$score=$pdata['score'];
$dates=$pdata['dates'];
             if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}			
   ob_start();
   $str = ' 
<table  align="center"><tr><td style="color:#C28100;">SUMMATIVE REPORT</td></tr></table>
<table rowspan="4" width="100%" style="width:667px;padding:5px;font-size:12px;">
<tr>
<td style="height:30px;">
&nbsp;
</td>
</tr>
<tr>
<td><b>Dates:</b>'.$dates.' </td>
</tr>
<tr>
<td style="height:30px;">
&nbsp;
</td>
</tr>
<tr>
<td><b>Status:</b>'.$status.' </td>
</tr>
<tr>
<td style="height:30px;">
&nbsp;
</td>
</tr>
<tr>
<td><b>Score:</b>'.$score.' </td>
</tr>
<tr>
<td style="height:30px;">
&nbsp;
</td>
</tr>
<tr>
<td ><b>Comments:</b></td>
<td style="border:1px solid #70B8BA;height:400px;width:500px;vertical-align:text-top;" > '.$comments.'</td>
</tr>
</table>

';
    $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
    
	$html2pdf->WriteHTML($str);
    $html2pdf->Output(WORKSHOP_FILES.'general/'.$login_type.$login_id.'.pdf','F');
	 
	 $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	$stra='<div style="font-size:14px;padding-left:300px;"><b>Document Two Continued:</b></div>';
	$html2pdf->WriteHTML($stra);
    $html2pdf->Output(WORKSHOP_FILES.'general/'.$login_type.$login_id.'_doc.pdf','F');
	

}
function pdfreport($school_id,$teacher_id,$reporttype)
{
  $this->load->Model('reportmodel');
  $data['reports'] = $this->reportmodel->getReportByTeacherpdf($school_id,$teacher_id,$reporttype);
  echo json_encode($data['reports']);
  exit;
}

function getreport($report_id,$report_type)
{
 $report=explode('-',$report_id);
 $report_id=$report[0];
 $sno=$report[1];
 $str='';	
 if($report_type=='forma' || $report_type=='formc' )
 {
        
		$this->load->Model('reportmodel');
		$this->load->Model('lessonplanmodel');
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$standarddata=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
			
		if($report_type=='forma')
		{
			$this->load->Model('observationpointmodel');
			$data['points']=$this->observationpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->observationpointmodel->getReportPoints($report_id);
		}
		else if($report_type=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->lickertpointmodel->getReportPoints($report_id);
		}
		if($data['getpoints']!=false)
		{
		  $data['getreportpoint']=$data['getpoints'];
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			
			}
		  
		  }
		  }
		
		}
		else
		{
		 $data['getreportpoints'][0]='';
		}
		
	   
       if(!empty($data['reportdata'])) {
			$str.=' 
<table  align="center"><tr><td style="color:#C28100;">Report Data</td></tr></table>
			<table width="100%" style="width:667px; border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td align="left" >
			<b>Report Number:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$sno.'</a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['report_date'].'</a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['observer_name'].'</a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['teacher_name'].'</a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['school_name'].'</a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['subject_name'].'</a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['grade_name'].'</a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['period_lesson'].'</a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['students'].'</a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['paraprofessionals'].'</a></td>
			<td align="left">
			<b>Lesson Correlation:</b></td><td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['lesson_correlation'].'</a></td>
			</tr>
			</table><br />';
			 } 
				if($data['reportdata']['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { 
			$str.='<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['standard'].'</td></tr>
			</table>
			<br />
			<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['diff_instruction'].'
			</td></tr>
			</table><br />';
			
			 } } 
			 
			 if($data['points']!=false) {	
			
			
			$str.='<table  style="padding:5px;border:#a2ba88 1px solid;">';
			$point_again=0;
			$group_again=0;
			foreach($data['points'] as $val)
			{
			
			$gcheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			$gcounter=0;
			
			}
			else
			{
				$gcounter=1;
			
			}
			if($gcounter==0)
			{
			$str.='<tr>
			<td style="color:#048DC4; font-size: 16px; margin-bottom: 10px;">'.$val['group_name'].'
			</td>
			</tr>
			<tr>
			<td style="color: #0090B2;font-size: 11px;">
			'.$val['description'].'
			</td>
			</tr>';
			
			
			 }
			
			
			if($val['group_type_id']!=2)
			{
			
			$str.='<tr><td >';			
			
			
			
 if(isset($data['getreportpoints'][$val['point_id']]['response']) && $data['getreportpoints'][$val['point_id']]['response']==1 ) { 
$str.='<img style="float:left" src='.SITEURLM.'images/checked.jpg>';  }else{  $str.='<img style="float:left" src='.SITEURLM.'images/unchecked.jpg>';  } $str.=''.wordwrap($val['question'], 150, "<br />\n").'</td></tr>';
			
			 
			}
			else if($val['group_type_id']==2) {
			$check=0;
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			$counter=0;
			$point_sub=$val['sub_group_name'];
			}
			else
			{
				$counter=1;
			
			}
			if($counter==0)
			{
			
			
			$str.='<tr>
			<td style="color: #333333;font-size: 11px;font-weight: bold;margin-top: 5px;">
			'.wordwrap($val['question'], 150, "<br />\n").'
			</td>
			</tr>
			<tr>
			<td style="padding-left:34px;">';
			
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  
			
			 
			if($val['ques_type']=='radio' && !isset($data['getreportpoints'][$point_id]['response'])) {
			
			//$check=1;
			}
			if(isset($data['getreportpoints'][$point_id]['response']) && $data['getreportpoints'][$point_id]['response']==$val['sub_group_id'] && $val['ques_type']!='checkbox' ) { $str.='<img style="float:left" src='.SITEURLM.'images/checked.jpg>';  } else  
			if(isset($data['getreportpoints'][$point_id][$val['sub_group_id']])) {
			 
			$str.='<img style="float:left" src='.SITEURLM.'images/checked.jpg>';
			 } else if ($check!=1){ $str.='<img style="float:left" src='.SITEURLM.'images/unchecked.jpg>';  }
			 if($check==1) { $str.='<img style="float:left" src='.SITEURLM.'images/checked.jpg>';  } 
			  $str.='<div style="" >'.wordwrap($val['sub_group_name'], 150, "<br />\n").'</div>
			
			</td>
			</tr>';
			
			 }
			else {
			
			
			$str.='<tr>
			<td style="padding-left:34px;">';
			
			
			 
			if($val['ques_type']=='checkbox')
			{
			$name=$point_id.'_'.$val['sub_group_id'];
			 } else {
			 
			 $name=$point_id;
			  } 
			  
			
			 if(isset($data['getreportpoints'][$point_id]['response']) && $data['getreportpoints'][$point_id]['response']==$val['sub_group_id']  && $val['ques_type']!='checkbox' ) {  $str.='<img style="float:left" src='.SITEURLM.'images/checked.jpg>';  } else 
			if(isset($data['getreportpoints'][$point_id][$val['sub_group_id']])) {
			 
			$str.='<img style="float:left" src='.SITEURLM.'images/checked.jpg>';
			 }  else { $str.='<img style="float:left" src='.SITEURLM.'images/unchecked.jpg>';  } 
			
			 $str.='<div style=""> '.wordwrap($val['sub_group_name'], 150, "<br />\n").'</div>
			</td>
			</tr>';
			
			}
			
			}
			$point_again=$val['point_id'];
			$group_again=$val['group_id'];
			}
			} 
			else
			{
			
			
			$str.='<tr>
			<td>
			No Observation Points Found
			</td>
			</tr>';
			 } 
			$str.='</table>'; 
           
 
 
 }
 else if($report_type=='formb')
 {
 
	$this->load->Model('reportmodel');
	$this->load->Model('lessonplanmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$standarddata=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
			
		
			$this->load->Model('rubricscalemodel');
			$data['points']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			$data['getpoints']=$this->rubricscalemodel->getallrubricsubscalesformpoints($report_id);
			//echo '<pre>';
			//print_r($data['getpoints']);
			//exit;
			if($data['getpoints']!=false)
			{
			foreach($data['getpoints'] as $gval)
			{
			foreach($data['points'] as $key=>$pval)
			{
			if(!empty($gval['point_id']))
			{
			  if($gval['point_id']==$pval['sub_scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}
			else
			{
				if($gval['group_id']==$pval['scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}

			}	
			
			}
			}
			
			if(!empty($data['reportdata'])) {
			$str.=' 
<table  align="center"><tr><td style="color:#C28100;">Report Data</td></tr></table>
			<table width="100%" style="width:667px; border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td align="left" >
			<b>Report Number:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$sno.'</a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['report_date'].'</a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['observer_name'].'</a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['teacher_name'].'</a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['school_name'].'</a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['subject_name'].'</a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['grade_name'].'</a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['period_lesson'].'</a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['students'].'</a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['paraprofessionals'].'</a></td>
			<td align="left"><b>Lesson Correlation:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['lesson_correlation'].'</a></td>
			</tr>
			</table><br />';
			 } 

			if($data['reportdata']['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { 
			$str.='<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['standard'].'</td></tr>
			</table>
			<br />
			<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['diff_instruction'].'
			</td></tr>
			</table><br />';
			
			 } } 
			
			
			$str.='<table cellspacing="0" cellpadding="5" style="width:300px;padding:5px;border:#a2ba88 1px solid;">';
			
			 if($data['points']!=false) {	
			
			//echo "<pre>";
			//print_r($data['points']);
		    $i=1;
			foreach($data['points'] as $val)
			{
			$i++;
			//if($i%2==0)
			{			
            
			 $str.='<tr><td >'; 
			  } 
			$str.='<div style="margin-top:10px;">
			<table style="padding:5px;border:1px #cccccc solid;width:300px;" cellspacing="0" cellpadding="3">
			<tr style="background-color:#cccccc;color:#000000;font-weight:bold;">
			<td colspan=2>'.$val['scale_name'];
			if(!empty($val['sub_scale_name'])) { 
 			$str.='-->'.$val['sub_scale_name'] ;  
			}
			$str.='</td>			
			</tr>
			<tr>
			<td>
			Strengths:
			</td>
			<td><div style="width:455px;">';
			if(isset($val['strengths'])) { 
			$str.=$val['strengths']; 
			} 
			$str.='</div></td>
			</tr>
			<tr>
			<td>
			Concerns:
			</td>
			<td><div style="width:455px;">';
			if(isset($val['concerns'])) { 
			$str.=$val['concerns'] ; 
			
			} 
			$str.='</div></td>
			</tr>
			</table></div>
			';
			
			//if($i%2!=0)
			{
			
            
			 $str.='</td></tr>';
			 } 
			 } 			
			} 
			else
			{
			$str.='	<tr>
			<td>
			No  Points Found
			</td>
			</tr>';
			 } 
			
			$str.='</table>'; 
 }
 else if($report_type=='formp')
 {
	$this->load->Model('reportmodel');
			
			
		
		$this->load->Model('lessonplanmodel');
		
		$data['reportdata']=$this->reportmodel->getproficiencyReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$standarddata=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
			
		
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllGroupPoints();
			$data['getpoints']=$this->proficiencypointmodel->getReportPoints($report_id);
			//echo '<pre>';
			//print_r($data['points']);
			
		if($data['getpoints']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  }
		  $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		   
		}
		else
		{
		 $data['getreportpoints'][0]='';
		 $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		}
		
		if(!empty($data['reportdata'])) {
			$str.=' 
<table  align="center"><tr><td style="color:#C28100;">Report Data</td></tr></table>
			<table width="100%" style="width:667px; border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td align="left" >
			<b>Report Number:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$sno.'</a>
			</td>
			<td align="left">
			<b>Report Date:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['report_date'].'</a>
			</td>
			<td align="left">
			<b>Observer Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['observer_name'].'</a>
			</td>
            
			</tr>
			<tr>
			<td align="left">
			<b>Teacher Name:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['teacher_name'].'</a></td>
			<td  align="left"><b>School Name:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['school_name'].'</a></td><td align="left"><b>Subject:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['subject_name'].'</a></td>
			
			
			</tr>
			<tr>
			<td align="left"><b>Grade:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['grade_name'].'</a></td>
			<td align="left">
			<b>Type of Observation:</b></td><td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['period_lesson'].'</a></td>
			<td  align="left"><b>Students Present:</b></td>
			<td align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['students'].'</a></td>
			
			</tr>
			<tr>
			<td align="left"><b>Paraprofessionals:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['paraprofessionals'].'</a></td>
			<td align="left"><b>Lesson Correlation:</b></td>
			<td  align="left"><a style="color:#048DC4;text-decoration:none">'.$data['reportdata']['lesson_correlation'].'</a></td>
			</tr>
			</table><br />';
			 } 
           
			if($data['reportdata']['lesson_correlation_id']==2)
			{
			if($standarddata!=false) { 
			$str.='<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Instructional Standard:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['standard'].'</td></tr>
			</table>
			<br />
			<table style="border:#a2ba88 1px solid;padding:5px;font-size:12px;">
			<tr>
			<td style="color:#048DC4;width:690px;">
			<b>Differentiated Instrucion:</b>
			</td>
			</tr>
			<tr>
			<td>'.$standarddata[0]['diff_instruction'].'
			</td></tr>
			</table><br />';
			
			 } } 
			
			
			//$str.='<table>';
			$group_again=0;
			$c=0;
			if($data['alldata']!=false) {	
			foreach($data['alldata'] as $key=> $val)
			{
			$check=0;
			$scheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			if($c>0)
			{
            $str.='</table>';
			
			}
			$str.='<br />
			<table border="0" cellpadding="5" cellspacing="0" style="border:1px #a2ba88 solid; width:100%;">
			<tr>
    <td style="font-size:13px;font-weight:bold;color:#444444;" colspan="5">'.$val['group_name'].'</td>
  </tr>
  <tr>
    <td style="font-size:13px;font-weight:bold;color:#444444;" colspan="5">'.$val['description'].'</td>
  </tr>';
			} 
			$c++;
			$str.='<tr>
			<td  style="border:1px #a2ba88 solid; background:#f0f5eb; ">
			Element
			</td>';
			foreach($val['names'] as $names)
			{
			$str.='<td style="border-top:1px #a2ba88 solid;border-bottom:1px #a2ba88 solid; border-right:1px #a2ba88 solid; background:#f0f5eb;">
			'.$names.'
			</td>';
			 } 
			$str.='</tr>
			<tr>
			<td style="vertical-align: top;width:130px;border-bottom:1px #a2ba88 solid;border-left:1px #a2ba88 solid;border-right:1px #a2ba88 solid;">
			'.$val['question'].'</td>';
			foreach($val['text'] as $text)
			{
			$str.='<td style="vertical-align: top;width:130px;border-bottom:1px #a2ba88 solid; border-right:1px #a2ba88 solid;">
			'.$text.'
			</td>';
			 }
			$str.='</tr>';
			if($this->session->userdata('login_type')=='user')
			{
			$str.='<tr>
			<td style="border-bottom:1px #a2ba88 solid;border-left:1px #a2ba88 solid;border-right:1px #a2ba88 solid;">
			</td>';
			 foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			
			$str.='<td style="border-bottom:1px #a2ba88 solid;border-right:1px #a2ba88 solid;">';
			 
			if($val['ques_type']=='radio' && !isset($data['getreportpoints'][$key]['response'])) {
			
			//$check=1;
			}
			if(isset($data['getreportpoints'][$key]['response']) && $data['getreportpoints'][$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { 
			$str.='<img src='.SITEURLM.'images/checked.jpg>';  } else  
			if(isset($data['getreportpoints'][$key][$subgroupid])) {
			
			$str.='<img src='.SITEURLM.'images/checked.jpg>';
			} else  if($check==1 && $key1==0) { $str.='<img src='.SITEURLM.'images/checked.jpg>';  } else { 
			$str.='<img src='.SITEURLM.'images/unchecked.jpg>';
			 } 
			 $str.='Assessment
			</td>';
			 } 
			
			$str.='</tr>';
		 
			
			} 
			else
			{
			$str.='
			<tr>
			<td style="border-bottom:1px #a2ba88 solid;border-left:1px #a2ba88 solid;border-right:1px #a2ba88 solid;">
			</td>';
			 foreach($val['sub_group_id'] as $key1=> $subgroupid)
			{
			if($val['ques_type']=='checkbox')
			{
			$name=$key.'_'.$subgroupid;
			 } else {
			 
			 $name=$key;
			  } 
			
			
			$str.='<td style="border-bottom:1px #a2ba88 solid;border-right:1px #a2ba88 solid;">';
			 
			if($val['ques_type']=='radio' && !isset($data['getreportpoints'][$key]['response'])) {
			
			//$check=1;
			}
			if(isset($data['getreportpoints'][$key]['response']) && $data['getreportpoints'][$key]['response']==$subgroupid && $val['ques_type']!='checkbox' ) { 
			$str.='<img src='.SITEURLM.'images/checked.jpg>'; } else  
			if(isset($data['getreportpoints'][$key][$subgroupid])) {
			
			$str.='<img src='.SITEURLM.'images/checked.jpg>';
			 } else  if($check==1 && $key1==0) {
			 $str.='<img src='.SITEURLM.'images/checked.jpg>';  } else { 
			$str.='<img src='.SITEURLM.'images/unchecked.jpg>';
			 } 
			 $str.='Assessment
			</td>';
			 } 
			
			$str.='</tr>';
			
			
			}
			
			$group_again=$val['group_id'];
			}
		  
		    $str.='</table>';
			
			} 
			else
			{
			
			$str.='<table>
			<tr>
			<td>
			No Observation Points Found
			</td>
			</tr>
			</table>';
			} 
			$str.=''; 
 
 }
  
 print $str;





}
	
}
?>	