<?php
/**
 * Countries Controller.
 *
 */
class Needs_for_medical_child extends	MY_Auth {
function __Construct()
	{
			parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		$this->load->library('user_agent');
	}

	function index($parent='')
	{	
		$data['view_path']=$this->config->item('view_path');
		
		$this->load->Model('intervention_strategies_medical_model');
			$data['needs'] = $this->intervention_strategies_medical_model->get_all_intervention_medical($parent);
			$data['parent'] = $parent;
		$this->load->view('needs_for_medical_child/needs_for_medical_child',$data);
	}
	
	function medical_child_list($parent,$page){
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();

		$this->load->model('intervention_strategies_medical_model');
		
		$total_records = $this->intervention_strategies_medical_model->get_all_dataCount($parent);
		
		if($parent!=''){
			$this->load->model('needs_for_medical_child_model');
			$data['alldata'] = $status = $this->needs_for_medical_child_model->get_all_data($page,$per_page,$parent);
		}else{
			$data['alldata'] = $status = $this->intervention_strategies_medical_model->get_all_data($page,$per_page,$parent);
		}
		
		

		
		$data['pagination'] = $this->do_pagination($total_records,$per_page,$page,'medical_child');
						

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('needs_for_medical_child/needs_for_medical_child_edit',$data);
	}
	
	function medical_child_insert()
	{
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$needs_medical_id = $this->input->post('needs_medical_id');
			$needs_for_medical_child = $this->input->post('needs_for_medical_child');
			$status=$this->input->post('status');
			$data=array('parent_id'=>$needs_medical_id,'intervention_strategies_medical'=>$needs_for_medical_child,'status'=>$status);
			$this->load->model('needs_for_medical_child_model');
			$update = $this->needs_for_medical_child_model->update('intervention_strategies_medical',$data,array('id'=>$id));
			if($update){
				echo "DONE";	
			} else {
				echo "ERROR";
			}

		} else{
			$district_id=$this->input->post('district_id');
			$is_delete=$this->input->post('is_delete');
			$needs_medical_id = $this->input->post('needs_medical_id');
			$needs_for_medical_child = $this->input->post('needs_for_medical_child'); 
			$status=$this->input->post('status');
			$data=array('district_id'=>$district_id,'is_delete'=>$is_delete,'parent_id'=>$needs_medical_id,'intervention_strategies_medical'=>$needs_for_medical_child,'status'=>$status);
			$this->load->model('needs_for_medical_child_model','needs_for_medical_child');
			$insert = $this->needs_for_medical_child->insert('intervention_strategies_medical',$data);
			if($insert){
				echo "DONE";	
			} else {
				echo "ERROR";
			}
			
		}
	}
 function edit($id)
	{
		$this->load->model('needs_for_medical_child_model');
		$data['all']=$this->needs_for_medical_child_model->get_medical_childById(array('id'=>$id));
		print_r(json_encode($data['all']));exit;
	}

	public function delete()
	{
		$medical_child_remove =array('is_delete'=>'1');
		$this->load->model('needs_for_medical_child_model');
		$schedule_id = $this->needs_for_medical_child_model->delete_medical_child('intervention_strategies_medical',$medical_child_remove);
		echo 'DONE';
	}	
	
function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active current'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}	
	
	
}
  
