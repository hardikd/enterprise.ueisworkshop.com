<?php
class Teacherreport extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_teacher()==false){
			//These functions are available only to teachers - So redirect to the login page
			redirect("index");
		}
		
	}

	function index()
	{
	  if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
	  $data['view_path']=$this->config->item('view_path');
      	  $this->load->Model('schoolmodel');
	  $data['subjects']=$this->schoolmodel->getallsubjects();
	  $this->load->view('teacherreport/index',$data);
	
	}
	function getteacherreport()
	{
	    
		
		if($this->input->post('teacher_id'))
		{
			
		$this->session->set_userdata('report_teacher_id',$this->input->post('teacher_id'));
		}
		$this->load->Model('teacherreportmodel');
		if($this->session->userdata('report_teacher_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('month'))
		{
		$this->session->set_userdata('reportmonth',$this->input->post('month'));
		}
		if($this->input->post('year'))
		{
		$this->session->set_userdata('reportyear',$this->input->post('year'));
		}
		if($this->input->post('subject'))
		{
		$this->session->set_userdata('report_query_subject',$this->input->post('subject'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}
		
		if($this->session->userdata('submitbutton')=='Get Reports')
		{
		
		
			//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/teacherreport/getteacherreport/';
		$config['total_rows'] = $this->teacherreportmodel->getReportTeacherCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 
		$data['reports'] = $this->teacherreportmodel->getReportByTeacher($start,$config['per_page']);
		$data['teacher_id']=$this->session->userdata('report_teacher_id');
		$data['view_path']=$this->config->item('view_path');
		//$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		$this->load->Model('schoolmodel');
		$data['subjects']=$this->schoolmodel->getallsubjects();
		$this->load->view('teacherreport/index',$data);
		
		}
		else if($this->input->post('submit')=='Get Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('teacher_id'));
		  $this->session->set_userdata('report_criteria','teacher');
		  $this->load->Model('teachermodel');
		  $teacher_name=$this->teachermodel->getteacherById($this->input->post('teacher_id'));
		  $name=$teacher_name[0]['firstname'].' '.$teacher_name[0]['lastname'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  if($this->session->userdata('reportform')!='formb')
		  {
			$this->qualitativereport(false,'teacher');
		  }
		  else
		  {
			$this->qualitativereportform(false,false,'teacher');
		  }	
		}
		else if($this->input->post('submit')=='Get Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('teacher_id'));
		  $this->session->set_userdata('report_criteria','teacher');
		  $this->load->Model('teachermodel');
		  $teacher_name=$this->teachermodel->getteacherById($this->input->post('teacher_id'));
		  $name=$teacher_name[0]['firstname'].' '.$teacher_name[0]['lastname'];
		  $this->session->set_userdata('report_name',$name);
		  
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->sectionalreport(false,'teacher');
		  }
		  else
		  {
			$this->sectionalform(false,false,'teacher');
		  }	
		 
		   
		
		}
		}
		else
		{
          $this->index();
		  

		}
		
		
		
		
		
		
		
		
		
	
	
	}
	
	function getobserverreport()
	{
		if($this->input->post('observer_id'))
		{
			
		$this->session->set_userdata('report_observer_id',$this->input->post('observer_id'));
		}
	   $this->load->Model('teacherreportmodel');
		
		if($this->session->userdata('report_observer_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Get Reports')
		{
			
			//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/teacherreport/getobserverreport/';
		$config['total_rows'] = $this->teacherreportmodel->getReportobserverCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('observermodel');
		$data['reports'] = $this->teacherreportmodel->getReportByobserver($start,$config['per_page']);
		$data['observer_id']=$this->session->userdata('report_observer_id');
		$data['view_path']=$this->config->item('view_path');
		$data['observers'] = $this->observermodel->getobserverByTeacherId($this->session->userdata("teacher_id"));
		$this->load->view('teacherreport/observerreport',$data);
		
		}
		else if($this->input->post('submit')=='Get Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  
		  if($this->session->userdata('reportform')!='formb')
		  {
			$this->qualitativereport(false,'observer');
		  }
		  else
		  {
			$this->qualitativereportform(false,false,'observer');
		  }	
		  
		}
		else if($this->input->post('submit')=='Get Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->sectionalreport(false,'observer');
		  }
		  else
		  {
			$this->sectionalform(false,false,'observer');
		  }	
		  
		   
		
		}
		}
		else
		{
           $this->observerreport();

        }		
		
		
	}
	
	function observer()
	{
		if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
		$this->load->Model('observermodel');
		$data['observers'] = $this->observermodel->getobserverByTeacherId($this->session->userdata("teacher_id"));
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacherreport/observerreport',$data);
	
	}
	
	function grade()
	{
		 if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
		 $this->load->Model('schoolmodel');
		$data['grades'] = $this->schoolmodel->getallgrades();
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacherreport/gradereport',$data);
	
	}
	
	function getgradereport()
	{
	    
		if($this->input->post('grade_id'))
		{
			
		$this->session->set_userdata('report_grade_id',$this->input->post('grade_id'));
		}
	      $this->load->Model('teacherreportmodel');
		
		if($this->session->userdata('report_grade_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Get Reports')
		{
		
		
		
			
			
			
		
		//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/teacherreport/getgradereport/';
		$config['total_rows'] = $this->teacherreportmodel->getReportgradeCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('schoolmodel');
		$data['reports'] = $this->teacherreportmodel->getReportBygrade($start,$config['per_page']);
		$data['grade_id']=$this->session->userdata('report_grade_id');
		$data['view_path']=$this->config->item('view_path');
		
		$data['grades'] = $this->schoolmodel->getallgrades();
		$this->load->view('teacherreport/gradereport',$data);
		}
		else if($this->input->post('submit')=='Get Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('grade_id'));
		  $this->session->set_userdata('report_criteria','grade');
		  $this->load->Model('schoolmodel');
		  $grade_name=$this->schoolmodel->getGradeById($this->input->post('grade_id'));
		  $name=$grade_name[0]['grade_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->qualitativereport(false,'grade');
		  }
		  else
		  {
			$this->qualitativereportform(false,false,'grade');
		  }	
		  
		 
		}
		else if($this->input->post('submit')=='Get Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('grade_id'));
		  $this->session->set_userdata('report_criteria','grade');
		  $this->load->Model('schoolmodel');
		  $grade_name=$this->schoolmodel->getGradeById($this->input->post('grade_id'));
		  $name=$grade_name[0]['grade_name'];
		  $this->session->set_userdata('report_name',$name);
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->sectionalreport(false,'grade');
		  }
		  else
		  {
			$this->sectionalform(false,false,'grade');
		  }
		   
		
		}
		}
		else
		{
		  $this->gradereport();
		
		}
	}
	
	function qualitativereport($group_id=false,$report)
	{
	  
		$this->load->Model('teacherreportmodel');
		$data['view_path']=$this->config->item('view_path');
	  if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();

		}	
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
			$data['qualitative']=$this->teacherreportmodel->getAllqualitative($group_id,$report);
		}
		else
		{
		   $group_id=0;
		   
		
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('report/qualitative',$data);
	
	
	}
	function qualitativereportform($group_id=false,$sub_id=false,$report)
	{
	
		
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('rubricscalemodel');
			$this->load->Model('teacherreportmodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
            
			
		if(!empty($data['groups']))
		{
			
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['scale_id'];
				
				if($sub_id)
				{
				
				}
				else
				{
				
				if(!empty($data['groups'][0]['sub_scale_id']))
				{
				 $sub_id=$data['groups'][0]['sub_scale_id'];
				
				}
				
				}
			}
					
			$data['group_name']='';
			$data['sub_scale_name']='';
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			   
			  if($data['group_name']=='')
			  {
				 
				 $data['group_name']=$val['scale_name'];
				
			  }
			
			
			
		   }
		   
		   if($sub_id!=0)
		   {
		     if($val['sub_scale_id']==$sub_id)
			{
				if($data['sub_scale_name']=='')
			  {
				$data['sub_scale_name']=$val['sub_scale_name'];
			}	
			
		   }
		   
			
			 
		  }
		  
			
		}
		$data['qualitative']=$this->teacherreportmodel->getAllqualitativerubiscale($group_id,$sub_id,$report);
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('teacherreport/qualitativeform',$data);
	
	
	}
	
	function viewreport($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			$this->load->Model('lessonplanmodel');
			
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
			if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationpointmodel');
			$data['points']=$this->observationpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->observationpointmodel->getReportPoints($report_id);
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->lickertpointmodel->getReportPoints($report_id);
		}
			
		if($data['getpoints']!=false)
		{
		  $data['getreportpoint']=$data['getpoints'];
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			
			}
		  
		  }
		  
		  }
		  
		
		}
		else
		{
		 $data['getreportpoints'][0]='';
		}
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewreport',$data);
	   }
	
	
	
	}
	function viewreportform($report_id,$sno)
	{
		
		$data['sno']=$sno;
		
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			
		
			$this->load->Model('rubricscalemodel');
			$data['points']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			$data['getpoints']=$this->rubricscalemodel->getallrubricsubscalesformpoints($report_id);
			//echo '<pre>';
			//print_r($data['getpoints']);
			//exit;
			if($data['getpoints']!=false)
			{
			foreach($data['getpoints'] as $gval)
			{
			foreach($data['points'] as $key=>$pval)
			{
			if(!empty($gval['point_id']))
			{
			  if($gval['point_id']==$pval['sub_scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}
			else
			{
				if($gval['group_id']==$pval['scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}

			}	
			
			}
			}
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewreportform',$data);
	   }
	   }
	   
	   function sectionalreport($group_id=false,$report)
	{
		$this->load->Model('teacherreportmodel');
		$data['view_path']=$this->config->item('view_path');
	  if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();

		}
		else if($this->session->userdata('reportform')=='formp')
		{
			$this->load->Model('proficiencygroupmodel');
			$data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
			
		}			
		
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			
		   }
		  }
			
			
			 if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationpointmodel');
			$data['points']=$this->observationpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		}
		else  if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		}
			else  if($this->session->userdata('reportform')=='formp')
		{
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllPoints($group_id,$this->session->userdata('district_id'));
		}
			if($this->session->userdata('reportform')!='formp')
			{
			$data['reports']=$this->teacherreportmodel->getAllReport($report);
			$data['sectional']=$this->teacherreportmodel->getAllsectional($group_id,$report);
			
			}
			else
			{
			  $data['reports']=$this->proficiencypointmodel->getAllReportproteacher($report);
			  $data['sectional']=$this->proficiencypointmodel->getAllsectionalproteacher($group_id,$report);
			}
			if($data['sectional']!=false)
			{
				$data['countsectional']=count($data['sectional']);
			
			}
			else
			{
			  $data['countsectional']=0;
			}
			if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
			
			
		}
		if(isset($data['points']))
		{ 
		  $data['group_set']=0;
		  
		  if($data['points']!='')
		  {
		  foreach($data['points'] as $val)
		  {
		    if($data['group_set']!=1)
			{
			if($val['group_type_id']==1 || $val['group_type_id']==2)
			{
			 $data['group_set']=1;
			 $data['point_set']=$val['point_id'];
			
			}
            }		  
		  }
		  }
		
		
		}
		if(!isset($data['points']))
		{
			 $data['points']=false;
		}
		$this->load->view('report/sectional',$data);
	
	
	
	}
	function sectionalform($group_id=false,$sub_id=false,$report)
	{
	
		
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			
		
		if(!empty($data['groups']))
		{
			
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['scale_id'];
				
				if($sub_id)
				{
				
				}
				else
				{
				
				if(!empty($data['groups'][0]['sub_scale_id']))
				{
				 $sub_id=$data['groups'][0]['sub_scale_id'];
				
				}
				
				}
			}
					
			$data['group_name']='';
			$data['sub_scale_name']='';
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			   
			  if($data['group_name']=='')
			  {
				 
				 $data['group_name']=$val['scale_name'];
				
			  }
			
			
			
		   }
		   
		   if($sub_id!=0)
		   {
		     if($val['sub_scale_id']==$sub_id)
			{
				if($data['sub_scale_name']=='')
			  {
				$data['sub_scale_name']=$val['sub_scale_name'];
			}	
			
		   }
		   
			
			 
		  }
		  
			
		}
		$this->load->Model('teacherreportmodel');
		$data['reports']=$this->teacherreportmodel->getAllReport($report);
		if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
		$data['sectional']=$this->teacherreportmodel->getAllsectionalrubiscale($group_id,$sub_id,$report);
		}
		
		if(!isset($data['sectional']))
		{
			 $data['sectional']=false;
		}
		$this->load->view('teacherreport/sectionalform',$data);
	
	
	
	
	
	}
	
	function memorandums()
	{
		$this->load->Model('memorandummodel');
		$data['memorandums']=$this->memorandummodel->getallmemorandumsByteacher();
	    $data['view_path']=$this->config->item('view_path');
		$this->load->view('teacherreport/memorandums',$data);
	
	
	
	
	}
	
	function memoview($memo_id)
	{
		$this->load->Model('memorandummodel');
		$data['memorandums']=$this->memorandummodel->getmemoview($memo_id);
	    $data['view_path']=$this->config->item('view_path');
		$this->load->view('teacherreport/memoview',$data);
	
	
	
	}
	
	function memosave()
	{
	 $this->load->Model('memorandummodel');
	 $status=$this->memorandummodel->memosave();
	 $data['memorandums']=$this->memorandummodel->getallmemorandumsByteacher();
	 $data['view_path']=$this->config->item('view_path');
	 $this->load->view('teacherreport/memorandums',$data);
	
	
	}
	
	function plansave()
	{
	
	 $this->load->Model('observationplanmodel');
	 $status=$this->observationplanmodel->plansave();
	 /* start of send Email to Observer*/
	 $teacher_id=$this->session->userdata("teacher_id");
	 $this->load->Model('schedulemodel');
	 $this->load->Model('observermodel');
	 $this->load->Model('scheduletaskmodel');
	 $this->load->Model('teachermodel');
		$teacherdata=$this->teachermodel->getteacherById($teacher_id);
		$teachername=$teacherdata[0]['firstname'].' '.$teacherdata[0]['lastname'];
		$teacheremail=$teacherdata[0]['email'];
		$schduledata=$this->schedulemodel->getscheduleplaninfo($this->input->post('schedule_week_plan_id'));
		$observerdata=$this->observermodel->getobserverById($schduledata[0]['role_id']);
		
		$taskdata=$this->scheduletaskmodel->getplanById($schduledata[0]['task']);
		$dates=$schduledata[0]['date'];
	  $date1=explode('-',$dates);
	  
	  $date=$date1[0].'/'.$date1[1].'/'.$date1[2];
	  
      if($this->valid_email($observerdata[0]['email']))
			{
			 $email=$observerdata[0]['email'];
			$observer_name=$observerdata[0]['observer_name'];
			$taskd=$taskdata[0]['task'];
			
			
	   /*  sending mail to observer */
$subject ="Tor-Events Completed For $date ";
	$message = "<html>
					<head>
					  <title>Tor-Events Completed For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $observer_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  <tr>
					   <td width='30px'><b>Assigned To:</b>
					   </td>
					   <td>
					   $teachername </td>
					   </tr>
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>					   
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  TOR 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
	//Additional headers
	/*if($teacheremail!='')
	{
	  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	  $headers .= "Reply-To: TOR Teacher <info@ueisworkshop.com>".PHP_EOL;
	}
	else
	{*/
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	/*  End of Sennding Email To Observer*/
	
	
	 $this->observationplan();
	
	
	
	}
	
	 function observationplan()
	{
		
	    if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
		$this->load->Model('observationplanmodel');
		$data['observationplan']=$this->observationplanmodel->getallplans();
		$data['observationplan_ans']=$this->observationplanmodel->getallanswerplans();
		if($data['observationplan']!=false)
		{
		  $this->load->Model('goalplanmodel');
		$data['schedule_week_plan_id']=$this->goalplanmodel->getscheduleobservationplans();
		
		
		
		  if($data['observationplan_ans']!=false)
		  {
		    foreach($data['observationplan'] as $key=>$plan)
			{
            foreach($data['observationplan_ans'] as $ans)
			{
			  if($plan['observation_plan_id']==$ans['observation_plan_id'] && $ans['archived']=='')
			  {
			    $data['observationplan'][$key]['answer']=$ans['answer']; 
				$data['observationplan'][$key]['comment']=$ans['comment']; 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}
		$data['view_path']=$this->config->item('view_path');
	 $this->load->view('teacherreport/observationplan',$data);
		
	
	
	}
	
	function goalplan()
	{
	//print_r($this->input->post());exit;
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
		$data['schedule_week_plan_id']=0;
		if($this->input->post('year'))
		{
		
			$year=$this->input->post('year');
		}
		else
		{
		  $year=date('Y');
		  
		  	
		
		}
		
		 $data['year']=$year;
		
		$this->load->Model('goalplanmodel');
		$data['teacher_schedule']=$this->goalplanmodel->getscheduleteacherplans($year);
		if($data['teacher_schedule']!=false)
		{
		 $data['schedule_week_plan_id']=$data['teacher_schedule'][0]['schedule_week_plan_id'];
		
		
		}
		$data['goalplans']=$this->goalplanmodel->getallplans();
		
		//print_r($data['goalplans']);
		//$data['teacherplans']=$this->goalplanmodel->getallplansbyteacher($year);
		//$data['observer_desc']=$this->goalplanmodel->getallplansdescbyteacher($year);
		//print_r($data['teacherplans']);
		$data['teacherplans']=false;
		if($data['goalplans']!=false)
		{
		$data['teacherplans']=$this->goalplanmodel->getteacherplanid($data['goalplans'][0]['goal_plan_id'],false,$year);
		}
		if($data['teacherplans']!=false)
		{
		
		$data['comments']=$this->goalplanmodel->getcommentsbyplanid($data['teacherplans'][0]['teacher_plan_id']);
		//print_r($data['comments']);
		}
		else
		{
			$data['comments']=false;
		
		}
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacherreport/create-goal',$data);
	
	
	
	}
	
	function archived()
	{
	  $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
	  $this->load->Model('observationplanmodel');
	  //Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/teacherreport/archived/';
		$config['total_rows'] = $this->observationplanmodel->getarchivedCount();
		
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		}
		//Pagination Code End

	  
	 
	  $data['archived']=$this->observationplanmodel->getarchivedprojects($start,$config['per_page']);
	  //print_r($data['archived']);
	  //exit;
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/archived',$data);
	
	
	}
	function viewproficiency($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getproficiencyReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			
		
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllGroupPoints();
			$data['getpoints']=$this->proficiencypointmodel->getReportPoints($report_id);
			//echo '<pre>';
			//print_r($data['points']);
			
		if($data['getpoints']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  }
		  $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		   
		}
		else
		{
		 $data['getreportpoints'][0]='';
		 $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		}
			
			
		
			
			//print_r($data['alldata']);
		   //exit;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewproficiency',$data);
	   }
	
	
	
	}
	function summative()
	{
	    if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
		$this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
		$data['statuses']=$this->statusmodel->getstatusbydistrict();
		$data['scores']=$this->scoremodel->getscorebydistrict();
	    
		 if($this->session->userdata("login_type")=='teacher')
		{
		$data['teachers'] = $this->teachermodel->getteacherById($this->session->userdata("teacher_id"));
		}
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/summative',$data);
	}
	
	function getsummativereport()
	{
	  $this->load->Model('reportmodel');
	  if($this->input->post('submit')=='Get Report')
		{
	  if($this->input->post('teacher_id'))
		{
			
		$this->session->set_userdata('summative_teacher_id',$this->input->post('teacher_id'));
		}
		if($this->input->post('status'))
		{
			
		$this->session->set_userdata('summative_status',$this->input->post('status'));
		}
		else if($this->input->post('status')=='')
		{
		  $this->session->set_userdata('summative_status','');
		
		}
		if($this->input->post('score'))
		{
			
		$this->session->set_userdata('summative_score',$this->input->post('score'));
		}
		else if($this->input->post('score')=='')
		{
		  $this->session->set_userdata('summative_score','');
		
		}
		if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('summative_school_id',$this->input->post('school'));
		}
		
		
		}
		}
		//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/teacherreport/getsummativereport/';
		$config['total_rows'] = $this->reportmodel->getsummativeReportCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
			$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('teachermodel');
		$data['reports'] = $this->reportmodel->getsummativeReportByTeacher($start,$config['per_page']);
		$data['teacher_id']=$this->session->userdata('summative_teacher_id');
		$data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("summative_school_id");
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		
		$data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata("summative_school_id"));
	   }
	   else
	   {
	    $data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		else if($this->session->userdata("login_type")=='teacher')
		{
		$data['teachers'] = $this->teachermodel->getteacherById($this->session->userdata("teacher_id"));
		}
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
		$data['statuses']=$this->statusmodel->getstatusbydistrict();
		$data['scores']=$this->scoremodel->getscorebydistrict();
		$data['status']=$this->session->userdata('summative_status');
		$data['score']=$this->session->userdata('summative_score');
		$this->load->view('report/summative',$data);
		
	
	
	
	}
	function profileimage($msg=false)
	{
	  
	  if($msg==false)
	  {
	     $data['message']='';
	  
	  
	  }
	  else
	  {
	  
	    $data['message']=$msg;
	  
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observer/profileimage',$data);
	
	
	
	
	}
	
	function change()
	{
	 if($this->session->userdata('login_type')=='user')
	  {
	     $this->load->Model('observermodel');
		$status= $this->observermodel->changeDistImage();
	  
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
         $this->load->Model('observermodel');
		$status= $this->observermodel->changeObserverImage();
	  
	  }
	  else if($this->session->userdata('login_type')=='teacher')
	  {
         $this->load->Model('observermodel');
		$status= $this->observermodel->changeTeacherImage();
	  
	  }
	  if($status==true)
	  {
        $msg='Changed Sucessfully';
	    $this->session->set_userdata('avatar_id',$this->input->post('avatar'));
	  }
	  else
	  {
	    $msg='Failed Please Try Again';
	  
	  }
	   $this->profileimage($msg);
	
	
	}
	function emailnotify($msg=false)
	{
	  
	  if($msg==false)
	  {
	     $data['message']='';
	  
	  
	  }
	  else
	  {
	  
	    $data['message']=$msg;
	  
	  }
	  $this->load->Model('teachermodel');
	 $data['emailnotify']= $this->teachermodel->getemailnotify();
	  $data['emailnotify']=$data['emailnotify'][0]['emailnotifylesson'];
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('teacher/emailnotify',$data);
	
	
	
	
	}
	
	function emailnotifychange()
	{
	  if($this->session->userdata('login_type')=='teacher')
	  {
         $this->load->Model('teachermodel');
		$status= $this->teachermodel->changeTeacherEmail();
	  
	  }
	  if($status==true)
	  {
        $msg='Changed Sucessfully';
	    
	  }
	  else
	  {
	    $msg='Failed Please Try Again';
	  
	  }
	   $this->emailnotify($msg);
	
	
	}
	function teacherreport()
	{
	    $this->load->Model('teachermodel');
		if($this->session->userdata('login_type')=='user')
		{
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/teacherreport',$data);
	}
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
	
}	