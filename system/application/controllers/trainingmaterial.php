<?php
/**
 * teacher Controller.
 *
 */
class Trainingmaterial extends MY_Auth {
	
	function __Construct()
	{
		
		parent::__construct();
if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false && $this->is_teacher()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->load->library('paginationnew');
		$this->load->helper('url');
		$this->no_cache();
		
	}
	
	
	function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
		
		
		
	public function index()
	{
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
		if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else {
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
			
		if($this->session->userdata("login_type")=="user")
			{
			 if($this->session->userdata('login_special') == 'district_management'){
		$data['idname']='tools';
			}
				$userid = $this->session->userdata('dist_user_id');
				if($userid == "")
				{
				redirect('report/index');	
				}
				else
				{	
								
				$this->load->model('trainingmaterialmodel');
							
				$config = array();
				$config["base_url"] = base_url() . "trainingmaterial/index";
				$config["total_rows"] = $this->trainingmaterialmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['docs'] = $this->trainingmaterialmodel->gettrainingmaterial($config["per_page"], $page);
			
					
				$data['view_path']=$this->config->item('view_path');
				$this->load->view('trainingmaterialdetail/trainingmaterial',$data);
				
				
				}
			}
			else if($this->session->userdata("login_type")=="observer")
			{
			
				$observerid = $this->session->userdata('observer_id');
				if($observerid == "")
				{
				redirect('report/index');	
				}
				else
				{	
				
			
				$this->load->model('trainingmaterialmodel');
				
				$config = array();
				$config["base_url"] = base_url() . "trainingmaterial/index";
				$config["total_rows"] = $this->trainingmaterialmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['docs'] = $this->trainingmaterialmodel->gettrainingmaterial($config["per_page"], $page);
				
				
				
				$data['view_path']=$this->config->item('view_path');
				$this->load->view('trainingmaterialdetail/trainingmaterial',$data);
				
				
				}
			}
				else if($this->session->userdata("login_type")=="teacher")
				{
			
				$teacherid = $this->session->userdata('teacher_id');
				if($teacherid == "")
				{
				redirect('report/index');	
				}
				else
				{	
						
				$this->load->model('trainingmaterialmodel');
				
				$config = array();
				$config["base_url"] = base_url() . "trainingmaterial/index";
				$config["total_rows"] = $this->trainingmaterialmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['docs'] = $this->trainingmaterialmodel->gettrainingmaterial($config["per_page"], $page);
				
				$data['view_path']=$this->config->item('view_path');
				$this->load->view('trainingmaterialdetail/trainingmaterial',$data);
				
				
				}
			}
		}
	}
	
	function uploadtrainingmaterial()
	{
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
				if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else {
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		$title = $this->input->post('title');
		 $desc = $this->input->post('description');
		  $filename = $_FILES['file']['name'];
		  $doctype = end(explode(".", $_FILES["file"]["name"]));
		
	/*if($_SERVER["HTTP_HOST"]=="localhost"){
			 //$path = $_SERVER['DOCUMENT_ROOT'].'/testbank/workshop/docfile/trainingmaterial/'.$_FILES['file']['name'];
			 $path = 'docfile/trainingmaterial/'.$_FILES['file']['name'];
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
	 //$path = $_SERVER['DOCUMENT_ROOT'].'/testbank/workshop/docfile/trainingmaterial/'.$_FILES['file']['name'];
			
			$path = 'docfile/trainingmaterial/'.$_FILES['file']['name'];
			
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
			 //$path = $_SERVER['DOCUMENT_ROOT'].'/docfile/trainingmaterial/'.$_FILES['file']['name'];
			 	
				$path = 'docfile/trainingmaterial/'.$_FILES['file']['name'];
				
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
		// $path = $_SERVER['DOCUMENT_ROOT'].'/docfile/trainingmaterial/'.$_FILES['file']['name'];
		
			$path = 'docfile/trainingmaterial/'.$_FILES['file']['name'];
		
			}
			else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){
		// $path = $_SERVER['DOCUMENT_ROOT'].'/docfile/trainingmaterial/'.$_FILES['file']['name'];
		
			$path = 'docfile/trainingmaterial/'.$_FILES['file']['name'];
		
			}
			*/
			
		$path = 'docfile/trainingmaterial/'.$_FILES['file']['name'];
		 
 	   	move_uploaded_file($_FILES['file']['tmp_name'],$path);
		
		$this->load->model('trainingmaterialmodel');
		$this->trainingmaterialmodel->addtraingmaterial($title,$desc,$filename,$doctype);
		redirect('trainingmaterial/index/');
		}
		
	}
	
	function deletetrainingdoc()
	{
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
		if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else {
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
			
		 $docid = $_REQUEST['docsid'];
	 	$docname = $_REQUEST['name'];
		
		if($_SERVER["HTTP_HOST"]=="localhost"){
			$path = $_SERVER['DOCUMENT_ROOT'].'/testbank/workshop/docfile/trainingmaterial/'.$docname;
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				$path = $_SERVER['DOCUMENT_ROOT'].'/testbank/workshop/docfile/trainingmaterial/'.$docname;
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				$path = $_SERVER['DOCUMENT_ROOT'].'/docfile/trainingmaterial/'.$docname;
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				$path = $_SERVER['DOCUMENT_ROOT'].'/docfile/trainingmaterial/'.$docname;
			}
			else {
				$path = $_SERVER['DOCUMENT_ROOT'].'/docfile/trainingmaterial/'.$docname;
			}

		if(unlink($path))
		{
		$this->load->model('trainingmaterialmodel');
		$this->trainingmaterialmodel->deletetrainingmaterialdoc($docid);
		
		}
		else
		{
			$this->session->set_flashdata('deletedocfailed','Not deleted the document.');
			redirect('trainingmaterial/index/');
		}
		
	}
}

	
	
	
}	