<?php
/**
 * memorandum Controller.
 *
 */
class Memorandum extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/index',$data);
	  }
	  else if($this->session->userdata("login_type")=='user')
	  {
		$data['view_path']=$this->config->item('view_path');
	    $this->load->view('memorandum/all',$data);
	  
	  }
	
	}
	
	
	function memorandumsall()
	{
		 $this->load->Model('memorandummodel');
	  $this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('observermodel');
	   $data['observer']=$this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['observer']=false; 
	   
	   }
	   
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/approveobservert',$data);
	
	
	
	
	
	}
	
	function savememorandumsall()
	{
		 $this->load->Model('memorandummodel');
	  $this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('observermodel');
	   $data['observer']=$this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['observer']=false; 
	   
	   }
	   
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/saveapproveobservert',$data);
	
	
	
	
	
	}
	
	function memorandums($page,$observer_id)
	{
	 if($observer_id==false)
		{
		  print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>State</td<td>District</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No  Documents Found.</td></tr></table>
			";
			
			
		}
		else
		{
        
	    $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('memorandummodel');
		$total_records = $this->memorandummodel->getmemorandumsallcount($observer_id);
		
			$status = $this->memorandummodel->getmemorandumsall($page,$per_page,$observer_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Documents Approved</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>Observer Name</td><td>Status</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['memorandum_id'].'" class="'.$c.'" >';
			
				print '<td><a href=memorandum/memo_edit/'.$val['approve_id'].'>'.$val['subject'].'</a></td>
				<td>'.$val['observer_name'].'</td>';
				if($val['status']=='NOTAPPROVED')
				{
				print '<td>Waiting</td>';
				}
				else
				{
				  print '<td>'.$val['status'].'</td>';
				 } 
					print '</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>State</td<td>District</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No  Documents Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
		}
	
	    }
	
	}
	function savememorandums($page,$observer_id)
	{
	 if($observer_id==false)
		{
		  print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>State</td<td>District</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No  Saved Documents Found.</td></tr></table>
			";
			
			
		}
		else
		{
        
	    $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('memorandummodel');
		$total_records = $this->memorandummodel->getsavememorandumsallcount($observer_id);
		
			$status = $this->memorandummodel->getsavememorandumsall($page,$per_page,$observer_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Saved Documents</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>Observer Name</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['memorandum_id'].'" class="'.$c.'" >';
			
				print '<td><a href=memorandum/save_memo_edit/'.$val['memo_approve_save_id'].'>'.$val['subject'].'</a></td>
				<td>'.$val['observer_name'].'</td>';
				 
					print '</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>State</td<td>District</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No  Documents Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
		}
	
	    }
	
	}
	
	function memo_edit($approve_id,$message=false)
	{
	  if($message!=false)
	  {
	    $data['message']=$message;
	  
	  }
	  $this->load->Model('memorandummodel');
	  $data['memorandum']=$this->memorandummodel->getapprovedmemobyid($approve_id);
	  
	  $data['subject']=$data['memorandum'][0]['subject'];
	  $data['warning']=$data['memorandum'][0]['warning'];
	  $data['memo']=$data['memorandum'][0]['text'];
	  $data['approve_id']=$data['memorandum'][0]['approve_id'];
	  $data['status']=$data['memorandum'][0]['status'];
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/memo_edit',$data);
	  
	
	
	}
	function save_memo_edit($approve_id,$message=false)
	{
	  if($message!=false)
	  {
	    $data['message']=$message;
	  
	  }
	  $this->load->Model('memorandummodel');
	  $data['memorandum']=$this->memorandummodel->getsavedapprovedmemobyid($approve_id);
	  
	  $data['subject']=$data['memorandum'][0]['subject'];
	  $data['warning']=$data['memorandum'][0]['warning'];
	  $data['memo']=$data['memorandum'][0]['text'];
	  $data['approve_id']=$data['memorandum'][0]['memo_approve_save_id'];
	  $data['status']=$data['memorandum'][0]['status'];
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/save_memo_edit',$data);
	  
	
	
	}
	function memorandum_save()
	{
	
		
	   $status=false;
	   
		$approve_id=$this->input->post('approve_id');
		$text=$this->input->post('text');
		$subject=$this->input->post('subject');
		$status=$this->input->post('status');
	  
	   if(!empty($approve_id) && !empty($status) && !empty($text) && !empty($subject) )
	   {
	     
		 if(isset($approve_id) && isset($status) && isset($text)  && isset($subject))
		 {
		    
		     
			$this->load->Model('memorandummodel');
			
			if($this->input->post('saved')=='saved')
			{
			
			$approve_id=$this->memorandummodel->saveagainmemoradum();
			if($approve_id!=0)
			{
			$status=true;
			
			}
			}
			else
			{
			$status=$this->memorandummodel->savememoradum();
			}
			
		 }
	   }
	   
	   if($status==true)
	   {
	      $this->memo_edit($approve_id,'Updated Successfully');
	  }
	  else
	  { 
	     $this->memo_edit($approve_id,'Failed Please Try Again');
	  }
	
	
	
	
	}
	
	function getmemorandums($page,$state_id,$country_id,$district_id=false)
	{
		if($district_id==false)
		{
		  $district_id='all';
		}	
	    $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('memorandummodel');
		$total_records = $this->memorandummodel->getmemorandumscount($state_id,$country_id,$district_id);
		
			$status = $this->memorandummodel->getmemorandums($page,$per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Communications</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>State</td><td>District</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['memorandum_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['subject'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districtsname'].'</td>
				
				<td ><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="memorandumedit('.$val['memorandum_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="memorandumdelete('.$val['memorandum_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>State</td<td>District</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No  Memorandums Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
		}
		
	
	
	
	
	}
	
	function add_memorandum()
	{
	
	
		$this->load->Model('memorandummodel');
			 
			$status=$this->memorandummodel->add_memorandum();
			if($status!=0){
				   $data['message']="Memorandum added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_memorandum()
	{
	$this->load->Model('memorandummodel');
			
	   $status=$this->memorandummodel->update_memorandum();
		if($status==true){
		       $data['message']="Memorandum Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		
		echo json_encode($data);
		exit;		
	}
	
	function delete($memorandum_id)
	{
		
		$this->load->Model('memorandummodel');
		$result = $this->memorandummodel->deletememorandum($memorandum_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getmemoranduminfo($memorandum_id)
	{
		if(!empty($memorandum_id))
	  {
		$this->load->Model('memorandummodel');
		$data['memorandum']=$this->memorandummodel->getmemorandumById($memorandum_id);
		$data['memorandum']=$data['memorandum'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	
	/*function assign()
	{
	  
	  $this->load->Model('memorandummodel');
	  $this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['teacher']=false; 
	   
	   }
	   $data['memorandum']=$this->memorandummodel->getallmemorandums($this->session->userdata("district_id"));
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assigndist',$data);
	
	}*/
	function assignteacher($teacher_id=false,$memorandum_id=false)
	{
	 
	    if($teacher_id==false)
		{			
		$teacher_id=$this->input->post('teacher');
		$memorandum_id=$this->input->post('memorandum');
		$data['message']='';
		}
		else
		{
		 $data['message']='Failed Please Try Again';
		}
	  
	   if(!empty($teacher_id) && !empty($memorandum_id) )
	   {
	     
		 if(isset($teacher_id) && isset($memorandum_id)  )
		 {
		    
			$this->load->Model('teachermodel');
			$teacher=$this->teachermodel->getteacherById($teacher_id);
			$data['teacher_id']=$teacher[0]['teacher_id'];
			$data['teachername']=$teacher[0]['firstname'].' '.$teacher[0]['lastname'];
			$this->load->Model('memorandummodel');
		    $memo=$this->memorandummodel->getmemorandumById($memorandum_id);
			$data['memo']=$memo[0]['text'];
			$data['memo_id']=$memo[0]['memorandum_id'];
			
		 }
	   }
	   
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assignteacher',$data);

	
	}
	function assignedteacher()
	{
	
	
	   $status=false;
	   $teacher_id=$this->input->post('teacher_id');
		$memorandum_id=$this->input->post('memo_id');
		$text=$this->input->post('text');
	  
	   if(!empty($teacher_id) && !empty($memorandum_id) && !empty($text) )
	   {
	     
		 if(isset($teacher_id) && isset($memorandum_id) && isset($text) )
		 {
		    
			
			$this->load->Model('memorandummodel');
		    $status=$this->memorandummodel->assignteacher();
			
			
		 }
	   }
	   
	   if($status==true)
	   {
	      $this->assigned();
	  }
	  else
	  { 
	     $this->assignteacher($teacher_id,$memorandum_id);
	  }
	
	
	
	
	}
	
	function assigned()
	{
	  
	  $this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['teacher']=false; 
	   
	   }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assigneddist',$data);
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
				}
	
}	