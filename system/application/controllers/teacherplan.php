<?php
ob_start();
$view_path="system/application/views/";
require_once($view_path.'inc/html2pdf/html2pdf.class.php');
class Teacherplan extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_teacher()==false && $this->is_observer()==false  && $this->is_user()==false){
			//These functions are available only to teachers - So redirect to the login page
			redirect("index");
		}
		
	}
        
        function index() {
            $data['menustyle'] = 'style="display: none;"';
            $data['idname']='';
            
            //This is to check last login for teacher
            $this->load->model('instructormodel');
			$lastLogin = $this->instructormodel->getinstructor();

			/*if($lastLogin[0]['step1']==0){
				//$this->instructormodel->updateinstructor();
				$this->load->Model('dist_grademodel');
				$data['grades'] = $this->dist_grademodel->getdist_gradesById();
				$data['menustyle'] = 'style="display:none;"';
				$data['view_path']=$this->config->item('view_path');
			 	$this->load->view('teacher/assign_grade',$data);
			} else if($lastLogin[0]['step1']==1 && $lastLogin[0]['step2']==0) {
					redirect('teacherplan/createClassrooms');
				} else if($lastLogin[0]['step1']==1 && $lastLogin[0]['step2']==1 && $lastLogin[0]['step3']==0){
					redirect('teacherplan/add_schedule');
			} else {
				$data['show_option'] = true;
				$this->load->Model('videooptionmodel');
	            $video = $this->videooptionmodel->insert();
	            if($video[0]->popup_cntr>6){
	                $data['show_option'] = false;
	            } else if (!$this->session->userdata('popup')){
	                $data['show_option'] = true;
	                $this->session->set_userdata('popup',true);
	                
	            }
//			            echo $video[0]->popup_cntr;exit;
	            if(!isset($video[0]->popup_cntr) || $video[0]->popup_cntr<1){
	            	$data['popupcntr'] = 1;
	            }
	            $data['view_path']=$this->config->item('view_path');
	            $this->load->view('teacherplan/index',$data);
        	}*/
        	$data['view_path']=$this->config->item('view_path');
	            $this->load->view('teacherplan/index',$data);
        }
        function workshopoption($option,$showpopup){
			$this->load->Model('videooptionmodel');
             $this->videooptionmodel->update($option,$showpopup);
             echo true;             
        }
        
        function planning_manager(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
             $this->load->view('teacherplan/planning-manager',$data);
        }
        
        function lesson_plan_creator(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('teacherplan/lesson-plan-creator',$data);
        }
        function lesson_plan(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('teacherplan/lesson-plan',$data);
        }
        function smart_goals(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('teacherplan/smart-goals',$data);
        }
        function create_goal(){
            $data['idname']='lesson';
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('teacherplan/create-goals',$data);
        }
        
	function teacherplan1()
	{
	

	   if($this->session->userdata('LP')==0)
			{
				 $data['view_path']=$this->config->item('view_path');
				$this->load->view('teacherplan/lpplan',$data);
			
			}
			else
		{	
	   if($this->input->post('selectdate'))
	   {
	      $data['selectdate']=$this->input->post('selectdate');
	   
	   }
	   else
	   {
	          $data['selectdate']=date('m-d-Y');
	   
	   }
	   $this->load->Model('lessonplanmodel');
	   $this->load->Model('time_tablemodel');
	   $this->load->Model('custom_differentiatedmodel');
	    $this->load->Model('periodmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
//                print_r($data['lessonplans']);exit;
		$data['custom_diff']=$this->custom_differentiatedmodel->getallplans();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssub();
		//echo '<pre>';
//		print_r($data['lessonplansub']);
//		exit;
	 $datevar=explode('-',$data['selectdate']);
		$sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	  $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	  //echo '<pre>';
	 // print_r($data['dates']);
	  //exit;
	  foreach($data['dates'] as $key=>$val)
	  {
	    $data['dates'][$key]['week']=$val['week'];
		$cdate=$val['date'];		
		$cdate1=explode('-',$cdate);
		$data['dates'][$key]['date']=$cdate1[1].'-'.$cdate1[2].'-'.$cdate1[0];
		$lessonplans=$this->time_tablemodel->gettimetablebyteacher($val['day']);
                //echo $this->db->last_query();exit;
		//print_r($lessonplans);
		//exit;
		
		if($lessonplans!=false && (strtotime($cdate)>=strtotime(date('Y-m-d'))))
		{
			
			foreach($lessonplans as $lessonval)
			{
				$lessonexists=$this->lessonplanmodel->check_lessonplan_exists($cdate,$lessonval['start_time'],$lessonval['end_time']);
				if($lessonexists==false)
				{					
					$this->lessonplanmodel->add_planbytimetable($lessonval['start_time'],$lessonval['end_time'],$cdate,$lessonval['grade_id'],$lessonval['subject_id'],$lessonval['period_id']);
				}
			}
		
		}
		
		
		
	  }
	  $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacherbytimetable(false,$fromdate,$todate);
//echo 'test'.$this->db->last_query();exit;	  
//echo '<pre>';
	  //print_r($data['getlessonplans']);
	 
	  if($data['getlessonplans']!=false)
	  {
	  foreach($data['getlessonplans'] as $key=>$val)
	  {
	    if($val['lesson_plan_sub_id']!='')
		{
			$subtab=$this->lessonplanmodel->getsubtab($val['lesson_plan_sub_id']);		
			$data['getlessonplans'][$key]['subtab']=$subtab;
		}
		else
		{
		$data['getlessonplans'][$key]['subtab']='';
		
		}
	  
	  }
	  }
	  //print_r($data['getlessonplans']);
	   //exit;
	  $data['pending']=$this->pending(false,$fromdate,$todate);
	  $data['value_feature']=$this->lessonplanmodel->value_feature();
//          echo 'test';
//	  print_r($data['value_feature']);exit;
	 $data['view_path']=$this->config->item('view_path');
	  
	  $data['periods']=$this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
	  $this->load->Model('schoolmodel');
	  $data['grades']=$this->schoolmodel->getallgrades();
	  
	  $this->load->view('teacherplan/lessonplan',$data);
	
	}
	
	}
	function getsubjects($period_id,$date)
	{
		
		list($mn,$dt,$yr)    =    split('-',$date);    // separate year, month and date
		$timeStamp            =    mktime(0,0,0,$mn,$dt,$yr); 
		$dayinfo=getdate($timeStamp);		
		$day=$dayinfo['weekday'];
		if($day=='Monday')
		{
			$dayn=0;
		}
		if($day=='Tuesday')
		{
			$dayn=1;
		}
		if($day=='Wednesday')
		{
			$dayn=2;
		}
		if($day=='Thursday')
		{
			$dayn=3;
		}
		if($day=='Friday')
		{
			$dayn=4;
		}
		if($day=='Saturday')
		{
			$dayn=5;
		}
		if($day=='Sunday')
		{
			$dayn=6;
		}
		
		
		$this->load->Model('time_tablemodel');
		$data['subjects']=$this->time_tablemodel->getsubjects($period_id,$dayn);
	    echo json_encode($data);
		exit;
	
	}
	function getActiveGrades($subject_id,$grade_id)
	{
		$this->load->Model('standarddatamodel');
		$data['grades']=$this->standarddatamodel->getActiveGrades($subject_id,$grade_id);
	    echo json_encode($data);
		exit;
	
	}
	function getGrades()
	{
		$this->load->Model('schoolmodel');
		$data['grades']=$this->schoolmodel->getallgrades();
	    echo json_encode($data);
		exit;
	
	}
	function getstrand($grade,$standard_id)
	{
		$this->load->Model('standarddatamodel');
		$data['strands']=$this->standarddatamodel->getstrand($grade,$standard_id);
	    echo json_encode($data);
		exit;
	
	}
	function getstandard()
	{
		$pdata=$this->input->post('pdata');
		$strand=$pdata['strand'];
		$grade=$pdata['grade'];
		$standard_id=$pdata['standard_id'];
		$this->load->Model('standarddatamodel');
		$data['standard']=$this->standarddatamodel->getstandard($strand,$grade,$standard_id);
        
	    echo json_encode($data);
		exit;
	
	}
	function getstandardajax()
	{
		$strand=$this->input->post('strand');
		$grade=$this->input->post('grade');
		$standard_id=$this->input->post('standard_id');
		$this->load->Model('standarddatamodel');
		$data['standard']=$this->standarddatamodel->getstandard($strand,$grade,$standard_id);
        
	    echo json_encode($data);
		exit;
	
	}
	function printweekview($se)
	{
	   
	          $data['selectdate']=$se;
	   
	   
	   $this->load->Model('lessonplanmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		//echo '<pre>';
		//print_r($data['getlessonplans']);
		//exit;
	 $datevar=explode('-',$data['selectdate']);
		$sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	  $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	  foreach($data['dates'] as $key=>$val)
	  {
	    $data['dates'][$key]['week']=$val['week'];
		$cdate=$val['date'];
		$cdate1=explode('-',$cdate);
		$data['dates'][$key]['date']=$cdate1[1].'-'.$cdate1[2].'-'.$cdate1[0];
	  
	  }
	  $data['getlessonplans']=$this->getlessonplansbyteacher(false,$fromdate,$todate);
	  
	  
	 $data['view_path']=$this->config->item('view_path');
	  $data['teacher_name']=$this->session->userdata('teacher_name');
	   $this->load->view('observerview/printdayview',$data);
	
	
	
	}
	
	function dayview()
	{
	
	 if($this->input->post('selectdate'))
	   {
	      $data['selectdate']=$this->input->post('selectdate');
	   
	   }
	   else
	   {
	          $data['selectdate']=date('m-d-Y');
	   
	   }
	    $this->load->Model('lessonplanmodel');
		$this->load->Model('periodmodel');
		$this->load->Model('custom_differentiatedmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		$data['custom_diff']=$this->custom_differentiatedmodel->getallplans();
		
		$datevar=explode('-',$data['selectdate']);
		$fromdate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
		$todate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
	  
	    $data['fromdate']=$data['selectdate'];	  
	    $data['todate']=$data['selectdate'];
	  
	    
		$weekday = date('l', strtotime($fromdate));
		
	    $data['dates'][$data['selectdate']]['week']=$weekday;		
		$data['dates'][$data['selectdate']]['date']=$data['selectdate'];
	  
	  
	  // $data['getlessonplans']=$this->getlessonplansbyteacher(false,$fromdate,$todate);
	    $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacherbytimetable(false,$fromdate,$todate);
	  //echo '<pre>';
	  //print_r($data['getlessonplans']);
	 
	  if($data['getlessonplans']!=false)
	  {
	  foreach($data['getlessonplans'] as $key=>$val)
	  {
	    if($val['lesson_plan_sub_id']!='')
		{
			$subtab=$this->lessonplanmodel->getsubtab($val['lesson_plan_sub_id']);		
			$data['getlessonplans'][$key]['subtab']=$subtab;
		}
		else
		{
		$data['getlessonplans'][$key]['subtab']='';
		
		}
	  
	  }
	  }
		$data['value_feature']=$this->lessonplanmodel->value_feature($fromdate);	  
	  $data['pending']=$this->pending(false,$fromdate,$todate);
	   $data['view_path']=$this->config->item('view_path');
	    $data['periods']=$this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
	   $this->load->view('teacherplan/dayplan',$data);
	
	
	
	
	
	
	}
	function printdayview($se,$teacher_id=false)
	{
	
	
	          $data['selectdate']=$se;
	   
	    $this->load->Model('lessonplanmodel');
		$this->load->Model('teachermodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		
		$datevar=explode('-',$data['selectdate']);
		$fromdate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
		$todate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
	  
	    $data['fromdate']=$data['selectdate'];	  
	    $data['todate']=$data['selectdate'];
	  
	    
		$weekday = date('l', strtotime($fromdate));
		
	    $data['dates'][$data['selectdate']]['week']=$weekday;		
		$data['dates'][$data['selectdate']]['date']=$data['selectdate'];
	  
	  if($teacher_id)
	  	$data['getlessonplans']=$this->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	  else
	   $data['getlessonplans']=$this->getlessonplansbyteacher(false,$fromdate,$todate);
	   
	   $data['view_path']=$this->config->item('view_path');
	   if($this->session->userdata('teacher_name'))
		   $data['teacher_name']=$this->session->userdata('teacher_name');
		else {
			$teacherdetail = $this->teachermodel->getteacherById($teacher_id);
			$data['teacher_name'] = ucfirst($teacherdetail[0]['firstname']).' '.ucfirst($teacherdetail[0]['lastname']);
		}

	   $this->load->view('observerview/printdayview',$data);
	
	
	
	
	
	
	}
	
	function schedule()
	{
	   if($this->input->post('selectdate'))
	   {
	      $data['selectdate']=$this->input->post('selectdate');
	   
	   }
	   else
	   {
	          $data['selectdate']=date('m-d-Y');
	   
	   }
	   $this->load->Model('schedulemodel');
	  
		$datevar=explode('-',$data['selectdate']);
		$sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	  $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	   $data['getscheduleplans']=$this->schedulemodel->getschedulesbyteacher(false,$fromdate,$todate);
	  
	  if($data['getscheduleplans']!=false)
	  {
	     $data['getscheduleplans1']=$data['getscheduleplans'];
		 $data['getscheduleplansstatus']='';
		
		foreach($data['getscheduleplans1'] as $value)
		{
		 if($value['task_id']==3)
		 {
		 $this->load->Model('goalplanmodel');
		$data['getscheduleplansstatus']=$this->goalplanmodel->goalstatus($value['schedule_week_plan_id']);
		
		
		if($data['getscheduleplansstatus']!=false)
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=1;
		}
		else
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=0;
		
		}
		}
		//task 2
		if($value['task_id']==2)
		 {
		 $this->load->Model('goalplanmodel');
		$data['getscheduleplansstatus']=$this->goalplanmodel->observationplanstatus($value['schedule_week_plan_id']);
		
		
		if($data['getscheduleplansstatus']!=false)
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=1;
		}
		else
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=0;
		
		}
		}
		
		// task 2
		//task 1
		if($value['task_id']==1)
		 {
		 $this->load->Model('goalplanmodel');
		$data['getlessonplansstatus']=$this->goalplanmodel->lessonplanstatus($value['schedule_week_plan_id']);
		
		if($data['getlessonplansstatus']!=false)
		{
		if($data['getlessonplansstatus'][0]['status']=='Completed')
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=1;
		}
		else
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=0;
		
		}
		}
		else
		{
		 $data['allstatus'][$value['schedule_week_plan_id']]=1;
		
		
		}
		
		}
		
		// task 1
		}
	  
	  
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('teacherplan/schedule',$data);
	
	
	
	}
	
	function getalllessonplans()
	{
		$this->load->Model('lessonplanmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplans();
		echo json_encode($data);
		exit;	
		
	}
	function getcustomdiff($id)
	{
		$this->load->Model('custom_differentiatedmodel');
		$data['custom']=$this->custom_differentiatedmodel->getcustomdiff($id);
		echo json_encode($data);
		exit;	
		
	}	
	function getlessonplansbyteacher($teacher_id=false,$fromdate,$todate)
	{
	
	   
	   $this->load->Model('lessonplanmodel');
	  $data['lessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	    
		
		return $data['lessonplans'];
	
	
	
	
	
	}
	
	function pending($teacher_id=false,$fromdate,$todate)
	{
	
	  $this->load->Model('lessonplanmodel');
	  $data['lessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	  $data['pendinglessonplans']=$this->lessonplanmodel->getpendinglessonplansbyteacher($teacher_id,$fromdate,$todate);
	  if($data['pendinglessonplans']!=false && $data['lessonplans']!=false)
	  {
	  $data['pendinglessonplansnew']=false;
	  foreach($data['pendinglessonplans'] as $pendingvalue)
	  {
	  $exists=0;
	  foreach($data['lessonplans'] as $levalue)
	  {
	    if($levalue['lesson_week_plan_id']==$pendingvalue['lesson_week_plan_id'])
		{
		  $exists=1;
		 
		
		
		}
		
	  
	  
	  }
	  
	  if($exists==0)
	  {
	    $data['pendinglessonplansnew'][]=$pendingvalue;
	  
	  }
	    
	  
	  
	  
	  }
	  $data['pendinglessonplans']=$data['pendinglessonplansnew'];
	  
	  }
	  
	  
		
		return $data['pendinglessonplans'];
	
	
	
	}
	
	function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		$dates[$i]['day'] = $i;
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
			
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}


function add_plan()
	{
	
	    
		$this->load->Model('lessonplanmodel');
		$jstatus=0;
		if($this->input->post("grade")=='')
		{
			$jstatus=1;
		
		
		}
		if($this->input->post("standard_id")!='')
		{
			if($this->input->post("grade")=='')
		{
			$jstatus=1;
		
		
		}
		if($this->input->post("strand")=='')
		{
			$jstatus=1;
		
		
		}
		if($this->input->post("standarddata")=='')
		{
			$jstatus=1;
		
		
		}
			
			 $standard=$this->input->post("standarddata");
		
		}
		else
		{
		
		$standard=$this->input->post("standard");
		
		}
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		foreach($data['lessonplansub'] as $val)
		{
		  $l=$val['lesson_plan_id'];
		  
		  if($this->input->post('lesson_week_plan_id_pending'))
		  {
		    if($this->input->post("pending".$l)=='')
		  {
		    $jstatus=1;
		  
		  }
		  
		  }
		  else
		  {
		    if($this->input->post("$l")=='')
		  {
		    $jstatus=1;
		  
		  }
		  
		  }
		
		}
		if($this->input->post('lesson_week_plan_id_pending'))
		  {
		if($this->input->post("pending_standard")=='' || $this->input->post("pending_diff_instruction")=='')
		{
			$jstatus=1;
		
		
		}
		}
		else
		{
		
		if($standard=='' || $this->input->post("diff_instruction")=='' || $this->input->post("subject_id")=='' || $this->input->post("period_id")=='')
		{
			$jstatus=1;
		
		
		}
		
		
		}
		if($jstatus==0)
		{
		
		$pstatus=$this->lessonplanmodel->check_plan_exists();
		//$pstatus=1;
	
	if($pstatus==1)
		 {
		$status=$this->lessonplanmodel->add_plan_teacher();
		if($status!=0){
		       $data['message']="Plan added Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date') ;
				if($this->input->post('lesson_week_plan_id_pending'))
		  {
			
		  
		  
		  
		  
		  
		  
		  
		  
		  }
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Scheduling Conflict" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==3)
		{
			$data['message']="The Same Period Multiple Times In a Day Not Allowed. " ;
		    $data['status']=0 ;
			
		
		}
		}
		else
		{
		   $data['message']="Please Select All Values" ;
		    $data['status']=0 ;
		
		}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	
	function add_plan_pending()
	{
	
	    
		$this->load->Model('lessonplanmodel');
		$this->load->Model('custom_differentiatedmodel');
		$jstatus=0;
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		if($this->input->post("pendinggrade")=='')
		{
			$jstatus=1;
		
		
		}
		foreach($data['lessonplansub'] as $val)
		{
		  $l=$val['lesson_plan_id'];
		  
		  
		    if($this->input->post("pending".$l)=='')
		  {
		    $jstatus=1;
		  
		  }
		  
		  
		  
		
		}
		$custom_diff=$this->custom_differentiatedmodel->getallplans();
		if($custom_diff!=false)
		{
			foreach($custom_diff as $val)
		{
		  $c=$val['custom_differentiated_id'];
		  if($this->input->post("p_c_".$c)=='')
		  {		    
			$jstatus=1;
		  
		  }
		
		}
		
		
		}
		if($this->input->post("pendingstandard_id")!='')
		{
			if($this->input->post("pendinggrade")=='')
		{
			$jstatus=1;
		
		
		}
		if($this->input->post("pendingstrand")=='')
		{
			$jstatus=1;
		
		
		}
		if($this->input->post("pendingstandarddata")=='')
		{
			$jstatus=1;
		
		
		}
			
			 $standard=$this->input->post("pendingstandarddata");
		
		}
		else
		{
		
		$standard=$this->input->post("pending_standard");
		
		}
		if($standard=='' || $this->input->post("pending_diff_instruction")=='' || $this->input->post("pendingsubject_id")=='' || $this->input->post("pendingperiod_id")=='')
		{
			$jstatus=1;
		
		
		}
		
		if($jstatus==0)
		{
		
		$pstatus=$this->lessonplanmodel->check_plan_pending();
		
	
	if($pstatus==1)
		 {
		$status=$this->lessonplanmodel->add_plan_teacher();
		if($status!=0){
		       $data['message']="plan added Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date') ;
			   $data['lesson_week_plan_id']=$this->input->post('lesson_week_plan_id_pending') ;
			   /* start of send Email to Observer*/
	 $teacher_id=$this->session->userdata("teacher_id");
	 $this->load->Model('schedulemodel');
	 $this->load->Model('observermodel');
	 $this->load->Model('scheduletaskmodel');
	 $this->load->Model('teachermodel');
		$teacherdata=$this->teachermodel->getteacherById($teacher_id);
		$teachername=$teacherdata[0]['firstname'].' '.$teacherdata[0]['lastname'];
		$teacheremail=$teacherdata[0]['email'];
		$schedulestatusid=$this->lessonplanmodel->getcommentsbyid($this->input->post('lesson_week_plan_id_pending'));
		$schduledata=$this->schedulemodel->getscheduleplaninfo($schedulestatusid[0]['schedule_week_plan_id']);
		$observerdata=$this->observermodel->getobserverById($schduledata[0]['role_id']);
		
		$taskdata=$this->scheduletaskmodel->getplanById($schduledata[0]['task']);
		$dates=$schduledata[0]['date'];
	  $date1=explode('-',$dates);
	  
	  $date=$date1[0].'/'.$date1[1].'/'.$date1[2];
	  $pending_standard=$this->input->post("pending_standard");
	  $pending_diff_instruction=$this->input->post("pending_diff_instruction");
	  
      if($this->valid_email($observerdata[0]['email']))
			{
			 $email=$observerdata[0]['email'];
			$observer_name=$observerdata[0]['observer_name'];
			$taskd=$taskdata[0]['task'];
			
			
	   /*  sending mail to observer */
$subject ="Workshop-Events Completed For $date ";
	$message = "<html>
					<head>
					  <title>Workshop-Events Completed For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $observer_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Standard:</b>
					  </td>
					  <td>
					  $pending_standard
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Differentiated Instruction:</b>
					  </td>
					  <td>
					  $pending_diff_instruction
					  </td>
					  </tr>
					  <tr>
					   <td width='30px'><b>Assigned To:</b>
					   </td>
					   <td>
					   $teachername </td>
					   </tr>
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>					   
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Workshop 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
	//Additional headers
	/*if($teacheremail!='')
	{
	  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	  $headers .= "Reply-To: Workshop Teacher <info@ueisworkshop.com>".PHP_EOL;
	}
	else
	{*/
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	/*  End of Sennding Email To Observer*/
		  
		  
		  
		  
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==3)
		{
			$data['message']="The Same Period Multiple Times In a Day Not Allowed. " ;
		    $data['status']=0 ;
			
		
		}
		}
		else
		{
		   
		   $data['message']="Please Complete All Tabs" ;
		    $data['status']=0 ;
		
		}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_plan()
	{
	   $this->load->Model('lessonplanmodel');
	   $this->load->Model('custom_differentiatedmodel');
		$jstatus=0;
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
                
		if($this->input->post("grade")=='')
		{
			echo 'here';
			$jstatus=1;
		
		
		}
		foreach($data['lessonplansub'] as $val)
		{
		  $l=$val['lesson_plan_id'];
		  if($this->input->post("$l")=='')
		  {		    
			$jstatus=1;
		  
		  }
		
		}
		
		$custom_diff=$this->custom_differentiatedmodel->getallplans();
		if($custom_diff!=false)
		{
			foreach($custom_diff as $val)
		{
		  $c=$val['custom_differentiated_id'];
		  if($this->input->post("c_".$c)=='')
		  {		    
			$jstatus=1;
		  
		  }
		
		}
		
		
		}
		
		if($this->input->post("standard_id")!='')
		{
			
			if($this->input->post("grade")=='')
		{
			
			$jstatus=1;
		
		
		}
		if($this->input->post("strand")=='')
		{
			
			$jstatus=1;
		
		
		}
		if($this->input->post("standarddata")=='')
		{
			
			$jstatus=1;
		
		
		}	
		
		}
		else if($this->input->post("standard")=='')
		{
		
		
		$jstatus=1;
		
		}
		if($this->input->post("diff_instruction")=='' || $this->input->post("subject_id")=='' || $this->input->post("period_id")=='')
		{
			
			
			
			$jstatus=1;
		
		
		}
		if($jstatus==0)
		{
	
		
		$pstatus=$this->lessonplanmodel->check_plan_update_exists();
		//$pstatus=1;
	if($pstatus==1)
		 {
		$status=$this->lessonplanmodel->update_plan_teacher();
		if($status!=0){
		       $data['message']="plan update Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date') ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Scheduling Conflict" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==3)
		{
			$data['message']="The Same Period Multiple Times In a Day Not Allowed. " ;
		    $data['status']=0 ;
			
		
		}
		
		}
		else
		{
		   $data['message']="Please Complete All Tabs" ;
		    $data['status']=0 ;
		
		}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function deleteplan($lesson_week_plan_id)
	{
	  $this->load->Model('lessonplanmodel');
		$result = $this->lessonplanmodel->delete_plan($lesson_week_plan_id);
		if($result!=false){
			$data['status']=1;
			$data['date']=$result[0]['date'];
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
	
	
	
	}
	function getallteacherplans($date)
	{
	  $this->load->Model('lessonplanmodel');
	  $date1=explode('-',$date);
	  $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
	  list($month,$day,$year) = explode("-", $date);
	$ct=$year.'-'.$month.'-'.$day;
	
	  //$pdata=$this->lessonplanmodel->getallteacherplans($date2);
	  $pdata=$this->lessonplanmodel->getlessonplansbyteacherbytimetable(false,$date2,$date2);
	  if($pdata!=false)
	  {
	  foreach($pdata as $key=>$val)
	  {
	    if($val['lesson_plan_sub_id']!='')
		{
			$subtab=$this->lessonplanmodel->getsubtab($val['lesson_plan_sub_id']);		
			$pdata[$key]['subtab']=$subtab;
		}
		else
		{
		$pdata[$key]['subtab']='';
		
		}
	  
	  }
	  }
	   
	   $ldata= $this->lessonplanmodel->getalllessonplansnotsubject();
	    $value_feature=$this->lessonplanmodel->value_feature();
	  $c=count($ldata);
	  
	   
	  if($pdata!=false)
	  {
	    $data['html']='<table id="'.$date.'" style="width:650px" >';
	  $le=0;
	  $je=0;
	  
	  foreach($pdata as $val)
	  {  
	  $je++;
	  if($le!=$val['lesson_week_plan_id'])
		{	
	     if($je==1)
		 {
		    $data['html'].='<tr align="center">'; 
		 
		 }
		 else
		 {
		     if($date>=date('m-d-Y') )
			 {
			 if($date==date('m-d-Y') )
			 {
			 if(!isset($value_feature[$le]))
			 {
				$data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td><td width='80px'><input style='border:1px #cccccc solid;background-color:#eeeeee;float:center;width:40px;font-size:9px;' type='button' name='Edit' id='Edit' value='Edit' onclick='edit($le)'><input type='button' style='border:1px #cccccc solid;background-color:#eeeeee;float:center;width:40px;font-size:9px;' name='Delete' id='Delete' value='Delete' onclick='deleteweek($le)'></td><td width='80px'><a href='javascript:;' title='Add'  onclick='comments($le)' >Add&nbsp;&nbsp;</a><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td></tr><tr align='center'>";
			 }
			 else
			 {
				$data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td><td width='80px'>&nbsp;</td><td width='80px'><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td></tr><tr align='center'>";
			 
			 }
			 }
			 else
			 {
				$data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td><td width='80px'><input style='border:1px #cccccc solid;background-color:#eeeeee;float:center;width:40px;font-size:9px;' type='button' name='Edit' id='Edit' value='Edit' onclick='edit($le)'><input type='button' style='border:1px #cccccc solid;background-color:#eeeeee;float:center;width:40px;font-size:9px;' name='Delete' id='Delete' value='Delete' onclick='deleteweek($le)'></td><td width='80px'><a href='javascript:;' title='Add'  onclick='comments($le)' >Add&nbsp;&nbsp;</a><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td></tr><tr align='center'>";
		     }
			 }
			 else
			 {
			    $data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td><td width='80px'>&nbsp;</td><td width='80px'><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td></tr><tr align='center'>";
			 }
		 }
		 
		$start1=explode(':',$val['starttime']);
		$end1=explode(':',$val['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
			$start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
		  
		  $start2=($start1[0]-12).':'.$start1[1].' pm';
		  }
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
		    $end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		$data['html'].="<td width='80px'>".$start2." to ".$end2."</td><td width='80px'>".$val['subject_name']."</td><td width='80px'>".$val['subtab']."</td>";
		}
		else
		{
		   $data['html'].="<td width='80px'>".$val['subtab']."</td>";
		
		}
		
		$le=$val['lesson_week_plan_id'];
		$le_id[$le]=$val['lesson_plan_sub_id'];
		$subject_le_id[$le]=$val['subject_id'];
			$lecomments[$le]=$val['comments'];
				
	  
	  }
	  if($le!=0)
	  {
	    if($date>=date('m-d-Y')  )
			 {
			 if($date==date('m-d-Y'))
			 {
			   if(!isset($value_feature[$le]))
			   {
			     $data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td><td width='80px' ><input style='border:1px #cccccc solid;background-color:#eeeeee;float:center;width:40px;font-size:9px;'  type='button' name='Edit' id='Edit' value='Edit' onclick='edit($le)'><input type='button' style='border:1px #cccccc solid;float:center;background-color:#eeeeee;width:40px;font-size:9px;' name='Delete' id='Delete' value='Delete' onclick='deleteweek($le)'></td><td width='80px'><a href='javascript:;' title='Add'  onclick='comments($le)' >Add&nbsp;&nbsp;</a><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td>";
			   }
			   else
			   {
				$data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td> <td width='80px'></td><td width='80px'><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td>";
			   
			   }
			 }
			 else
			 {
	   $data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td><td width='80px' ><input style='border:1px #cccccc solid;background-color:#eeeeee;float:center;width:40px;font-size:9px;'  type='button' name='Edit' id='Edit' value='Edit' onclick='edit($le)'><input type='button' style='border:1px #cccccc solid;float:center;background-color:#eeeeee;width:40px;font-size:9px;' name='Delete' id='Delete' value='Delete' onclick='deleteweek($le)'></td><td width='80px'><a href='javascript:;' title='Add'  onclick='comments($le)' >Add&nbsp;&nbsp;</a><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td>";
				
				}
				}
				else
				{ 
				  $data['html'].="<td width='40px'><a class='standard' href='#' title='Other' rel='teacherplan/getother/$le'>View</a></td> <td width='80px'></td><td width='80px'><a class='title' href='#' title='Comments' rel='teacherplan/getcommentsbyid/$le'>View</a></td><td>"; if($le_id[$le]!='') { $data['html'].="<a href='teacherplan/createlessonplanpdf/".$val['teacher_id']."/".$subject_le_id[$le]."/".$ct."' style='text-decoration:none;color:#FFFFFF;' target='_blank'><img style='float:left;margin-left:10px;margin-top:3px;' src='".SITEURLM."images/pdf_icon.gif'></a>" ;} $data['html'].="</td>";
				}
	  }
	    $data['html'].="</tr></table>";
	  }
	  else
	  {
	  
	    $data['html']="<table id='$date'><tr align='center'><td colspan='$c+2'>No Plans Found</td></tr></table>";
	  
	  }
	  
	  echo json_encode($data);
	  exit;
	
	
	}
	
	function getteacherplaninfo($lesson_week_plan_id)
	{
	
	  $this->load->Model('lessonplanmodel');
	  $pdata['lesson_week']=$this->lessonplanmodel->getteacherplaninfo($lesson_week_plan_id);
	  echo json_encode($pdata);
	  exit;
	
	
	
	}
	
	function videos()
	{
            $data['idname']='tools';
	  $this->load->Model('observationgroupmodel');
	  $data['videos']=$this->observationgroupmodel->getvideosByTeacher();
	  $data['articles']=$this->observationgroupmodel->getarticlesbyTeacher();
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('teacherplan/videos',$data);
	
	
	
	}
	function pdarchived($teacher_id)
	{
		 $data['idname']='tools';
	  $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
	  $this->load->Model('observationgroupmodel');
	  //Pagination Code Start
		/*$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/observerview/pdarchived/';
		$config['total_rows'] = $this->observationgroupmodel->getpdarchivedCount();
		
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		}
		*/
		//Pagination Code End

	  
	 
	  $data['archived']=$this->observationgroupmodel->getpdarchivedprojects();
	//print_r($data['archived']);
	//exit;
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/pdarchived',$data);
	
	
	}
	
	function uploadphoto()
	{
	
		if($_FILES['photo']['size']>0 && $this->input->post('name')!='')
	   {
	      $type=explode('/',$_FILES['photo']['type']);
		  if($type[0]=='image')
		  {
		  $this->load->Model('articraftsmodel');
		  $status=$this->articraftsmodel->add_photo();
		if($status!=0){
		  $target_path = WORKSHOP_FILES."photos/";
			$filename=explode('.',$_FILES['photo']['name']); 
			preg_match("/\.([^\.]+)$/", $_FILES['photo']['name'], $matches);
			$ext = $matches[1];
			
			//$filename=explode('.',$_FILES['file']['name']); 
			$target_path = $target_path . $status.'.'.$ext;
			
			
			
            
			move_uploaded_file($_FILES['photo']['tmp_name'], $target_path);
			 echo "uploaded Sucefully";
			}
          }
			else
          {
            echo "Please Upload A Image";

		  }			  
	    
	   }
	   
	   else
	   {
	   
	    echo "Please Enter Name and  Upload a File";
	   
	   
	   }
	
	}
	function uploadlink()
	{
	     
		if($this->input->post('link')!='' && $this->input->post('linkname')!='')
	   {
	      $this->load->Model('articraftsmodel');
		  $status=$this->articraftsmodel->add_link();
		if($status!=0){
		  
			 echo "Uploaded Sucefully";
			} 
	    
	   }
	   else
	   {
	   
	    echo "Please Enter Link Name and  Link";
	   
	   
	   }
	
	}
	function uploadvideo()
	{
	    
		
		if($_FILES['video']['size']!=0)
		{	
		
		if($_FILES['video']['size']>0 && $this->input->post('videoname')!='')
	   {
	      $type=explode('/',$_FILES['video']['type']);
		  if($type[0]=='video')
		  {
		  $this->load->Model('articraftsmodel');
		  $status=$this->articraftsmodel->add_video();
		if($status!=0){
		  $target_path = "videos/";
			$filename=explode('.',$_FILES['video']['name']); 
			preg_match("/\.([^\.]+)$/", $_FILES['video']['name'], $matches);
			$ext = $matches[1];
			
			//$filename=explode('.',$_FILES['file']['name']); 
			$target_path = $target_path . $status.'.'.$ext;
			
			
			
            
			move_uploaded_file($_FILES['video']['tmp_name'], $target_path);
			 echo "uploaded Sucefully";
			} 
		  }
		  else
          {
            echo "Please Upload A video File";

		  }	
	    
	   }
	   
	   else
	   {
	   
	    echo "Please Enter Video Name and  Upload a File";
	   
	   
	   }
	   }
	   else
	   {
	      $s=ini_get('upload_max_filesize');
		  echo " Please Enter A File or Max File Size is ";
		  echo $s;
	   
	   }
	}
	
	function uploadfile()
	{
	    
		if($_FILES['file']['size']!=0)
		{
		  
		if($_FILES['file']['size']>0 && $this->input->post('filename')!='')
	   {
	      $this->load->Model('articraftsmodel');
		  $status=$this->articraftsmodel->add_file();
		if($status!=0){
		  $target_path = WORKSHOP_FILES."files/";
			preg_match("/\.([^\.]+)$/", $_FILES['file']['name'], $matches);
			$ext = $matches[1];
			
			//$filename=explode('.',$_FILES['file']['name']); 
			$target_path = $target_path . $status.'.'.$ext;
			
			
			
            
			move_uploaded_file($_FILES['file']['tmp_name'], $target_path);
			 echo "uploaded Sucefully";
			} 
	    
	   }
	   
	   else
	   {
	   
	    echo "Please Enter file Name and  Upload a File";
	   
	   
	   }
	   }
	   else
	   {
	      $s=ini_get('upload_max_filesize');
		  echo " Please Enter A File or Max File Size is ";
		  echo $s;
	   
	   }
	
	}
	function addnotes($comments)
	{
	
	 $this->load->Model('articraftsmodel');
	     
		
          if($comments!='')
		 { 
		  $status=$this->articraftsmodel->addnotes($comments);
           if($status==true){
		
		    print 'Sucefully Commented';
		}
		else
		{
			print 'Failed Please Try Again';
		
		}
		}
		
		
			
		 
	
	
	
	}
	function createlessonplanpdf($teacher_id,$subject,$date)
{


		 
		 $this->load->Model('lessonplanmodel');
		 $this->load->Model('dist_grademodel');
		 $this->load->Model('teachermodel');
		$lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
		

		$lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		

		
		$getlessonplans=$this->lessonplanmodel->getlessonplansbysubjectandteacher($teacher_id,$date,$date,$subject);
		
         $gradename=$this->dist_grademodel->getdist_gradeById($getlessonplans[0]['grade']);
		
		$teachername=$this->teachermodel->getteacherById($teacher_id);
		$day = date('l', strtotime($date));
		if($day=='Monday')
		{
			$dayn=0;
		}
		if($day=='Tuesday')
		{
			$dayn=1;
		}
		if($day=='Wednesday')
		{
			$dayn=2;
		}
		if($day=='Thursday')
		{
			$dayn=3;
		}
		if($day=='Friday')
		{
			$dayn=4;
		}
		if($day=='Saturday')
		{
			$dayn=5;
		}
		if($day=='Sunday')
		{
			$dayn=6;
		}
		$this->load->Model('studentmodel');
		$total_students = $this->studentmodel->getstudentCount($dayn,$getlessonplans[0]['period_id'],$teacher_id);
		
		
		
		
		$le=$getlessonplans[0]['lesson_week_plan_id'];
		$start1=explode(':',$getlessonplans[0]['starttime']);
		$end1=explode(':',$getlessonplans[0]['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		 
		
		$sh=0;
		$k=0;
		
		$view_path=$this->config->item('view_path');
		//$weekday = date('l', strtotime($date));
   
		$newphrase='<br /><table style="width:650px;border:2px #7FA54E solid;">
		<tr>
		<td style="width:175px;color:#CCCCCC;">
		<b>Teacher:</b>'.$teachername[0]['firstname'].' '.$teachername[0]['lastname'].'
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Period:</b>'.$start2.' to '.$end2.'
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Subject:</b>'.$getlessonplans[0]['subject_name'].'
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Grade:</b>'.$gradename[0]['grade_name'].'
		</td>
		</tr>
		<tr>
		<td>
		<br />
		</td>
		</tr>
		<tr>
		<td style="width:175px;color:#CCCCCC;">
		<b>Class Size:</b>
		'.$total_students.'</td>';
		$jkk=0;
		foreach($lessonplans as $val)
		{
		
		if($jkk==3)
		{
		$newphrase.='</tr><tr><td><br /></td></tr><tr>';
		
		}
		$newphrase.='<td style="width:175px;color:#CCCCCC;">
		<b>'.$val['tab'].':</b>
		'.$getlessonplans[$jkk]['subtab'].'</td>';
		$jkk++;
		
		}
		$newphrase.='</tr>
		</table>
		
		
		';
	
	$newphrase.='<br /><br />';
	$newphrase.='<table style="border:2px #7FA54E solid;">
		<tr><td style="width:718px;color:#CCCCCC;">Standard:'.$getlessonplans[0]['standard'].'</td></tr></table>';
	$newphrase.='<br /><br />';
		
		
		$this->load->Model('custom_differentiatedmodel');		
		$observationplan_ans=$this->custom_differentiatedmodel->getcustomdiff($le);
		$data['status']=0;
		if($observationplan_ans!=false)
		{
		$observationplan=$this->custom_differentiatedmodel->getallplans();
		if($observationplan!=false)
		{
		  if($observationplan_ans!=false)
		  {
		    foreach($observationplan as $key=>$plan)
			{
            foreach($observationplan_ans as $ans)
			{
			  if($plan['custom_differentiated_id']==$ans['custom_differentiated_id']  )
			  {
			    $observationplan[$key]['answer']=$ans['answer']; 				 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}

 
 

 

   /*$newphrase.='<table align="center" style="width:725px;">
				<tr>
		<td class="htitle">
		<font size="4px">Custom Differentiated Instructions</font>
		</td>
		</tr>
		</table>
		';*/
		 if($observationplan!=false) { 
		
		
		$newphrase.="<br /><table  cellspacing='0' cellpadding='0' border='0' id='conf' style='width:725px;' >";
		
		
		$an=0;
		foreach($observationplan as $plan) { 
		$newphrase.='<tr >
		<td >
       <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b>'.$plan['tab'].'</b>
        </div>
		</td>
		</tr>';
		 if(isset($plan['answer'])) { 
		$newphrase.='<tr>
		<td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		'.$plan['answer'].'</div>
		</td></tr>';

		
		 } 
		
		 }  
		
		$newphrase.='</table>';
		
		} else { 	
		$newphrase.='<table>
		<tr>
		<td>
		No Data Found
		</td>
		</tr>
		</table>';
		 } 
			$newphrase.='';
		}
		
   $str='<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
   td{
   font-size:8px;
   }
   </style>';
   
  $str.='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              
        </page_footer>
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b>		
		<br /><b>'.ucfirst($teachername[0]['school_name']).'</b>;
		<br /><br /><b>Lesson Plan</b>';
		
		
		
		
	
		
		$str.='</div>
		</div>
		<br />				
		'.$newphrase.'				
   </page> 
';

   $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output();
    



}
		
	function addcomments()
	{
	    $data['status']=0;
	   $pdata=$this->input->post('pdata');
	   //print_r($pdata);
	  // exit;
	   if(!empty($pdata))
	   {
	    if(isset($pdata['plan_id']) && isset($pdata['comments'])  )
		 {
		$this->load->Model('lessonplanmodel');
		$status = $this->lessonplanmodel->addcommentsteacher($pdata['plan_id'],$pdata['comments']);
		if($status==true)
		{
		  $data['status']=1;
		
		}
		}
		}
		echo json_encode($data);
		exit;
		
	
	
	
	}
	function getcomments($id)
	{
	
	
	  $this->load->Model('lessonplanmodel');
	 $status=$this->lessonplanmodel->getcomments($id);
	 $data['status']=$status[0]['teacher_comments'];
	  echo json_encode($data);
		exit;	
	
	
	}
	function getcommentsbyid($id)
	{
	
	
	  $this->load->Model('lessonplanmodel');
	 $status=$this->lessonplanmodel->getcomments($id);
	  if($status[0]['comments']=='')
	  {
	    echo "<b>Observer:</b> No Comments Found.";
	  }
	  else
	  {
		echo '<b>Observer:</b> '.$status[0]['comments'];
	  }
	  if($status[0]['teacher_comments']=='')
	  {
	    echo "<br /><b>Teacher:</b> No Comments Found.";
	  }
	  else
	  {
		echo '<br /><b>Teacher:</b> '.$status[0]['teacher_comments'];
	  }	  
	  
	
	
	}
	function getother($id)
	{
	
	
	  $this->load->Model('lessonplanmodel');
	 $status=$this->lessonplanmodel->getother($id);
	  
		echo '<b>Standard:</b> '.$status[0]['standard'];
		echo '<br/>';
	  	echo '<b>Differentiated Instruction:</b> '.$status[0]['diff_instruction'];  
	  
	
	
	}
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}

	function getMaterial ($lesson_week_plan_id){
            $this->load->Model('lessonplanmodel');
	  $pdata['lesson_week']=$this->lessonplanmodel->getteacherplanmaterial($lesson_week_plan_id);
	  echo json_encode($pdata);
	  exit;
        }

    function assign_grades (){
    	//print_r($this->input->post('grade'));
    	$selectedGrades = $this->input->post('grade');
    	if(is_array($selectedGrades)){
    			$this->load->Model('grade_subjectmodel');
    			$this->load->Model('teacher_grade_subjectmodel');

    		foreach($selectedGrades as $grade){
    			$subjects = $this->grade_subjectmodel->getgrade_subjectall($grade);
    			//print_r($subjects);
    			foreach($subjects as $subject){
    				$this->teacher_grade_subjectmodel->teacher_grade_subject_add($subject['grade_subject_id']);
    				
    			}
    		}
    		
    	}
    	$this->load->Model('instructormodel');
			$update = array('lastLogin'=>$now,'step1'=>1);
			$this->instructormodel->updatesteps($update);
    	redirect('teacherplan/createClassrooms');
    	
    }

    function createClassrooms(){
    	$this->load->Model('dist_grademodel');
		$data['grades'] = $this->dist_grademodel->getGradeByTeacher();
		if($this->input->post('create_classrooms')=='create'){
			$this->dist_grademodel->create_classrooms();
			//exit;
			$this->load->Model('instructormodel');
			$update = array('lastLogin'=>$now,'step2'=>1);
			$this->instructormodel->updatesteps($update);
			redirect('teacherplan/add_schedule');
		}
		$data['menustyle'] = 'style="display:none;"';
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('period/create_classrooms',$data);	
    	
    }

    	function add_schedule(){
    		//print_r($this->session->all_userdata());exit;
		$school_id = $this->session->userdata('school_id');
		$this->load->Model('schoolmodel');
		$period=$data['school']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
		//print_r($period);exit;
		$this->load->Model('dist_grademodel');
		$data['grades'] = $this->dist_grademodel->getGradeByTeacher();
		//$data['grades'] = $this->dist_grademodel->getdist_gradesById();

		$this->load->Model('periodmodel');
		
		if($this->input->post('save_schedule')=='save'){
			for($periods=1;$periods<=10;$periods++){
				if($this->input->post('start_time_p'.$periods)!='' && $this->input->post('end_time_p'.$periods)!=''){
					$pstatus=$this->periodmodel->check_period_exists($this->input->post('start_time_p'.$periods),$this->input->post('end_time_p'.$periods));
					if($pstatus==1) {
						$status=$this->periodmodel->add_period($this->input->post('start_time_p'.$periods),$this->input->post('end_time_p'.$periods));
						if($status!=0){
							$data['message']="Period Added Sucessfully" ;
							$data['status']=1 ;
						} else {
							$data['message']="Contact Technical Support Update Failed" ;
							$data['status']=0 ;
						}
					} else if($pstatus==0) {
						$data['message']="Scheduling Conflict" ;
						$data['status']=0 ;
					} else if($pstatus==2) {
						$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
						$data['status']=0 ;
					} else if($pstatus==3) {
						$data['message']=" Already Added 10 Periods." ;
						$data['status']=0 ;
					}
				}
			}
		} else if($this->input->post('continue')=='continue'){
			for($periods=1;$periods<=10;$periods++){
				if($this->input->post('start_time_p'.$periods)!='' && $this->input->post('end_time_p'.$periods)!=''){
					$pstatus=$this->periodmodel->check_period_exists($this->input->post('start_time_p'.$periods),$this->input->post('end_time_p'.$periods));
					if($pstatus==1) {
						$status=$this->periodmodel->add_period($this->input->post('start_time_p'.$periods),$this->input->post('end_time_p'.$periods),$this->input->post('grade'.$periods),$this->input->post('subject'.$periods));
						if($status!=0){
							$data['message']="Period Added Sucessfully" ;
							$data['status']=1 ;
						} else {
							$data['message']="Contact Technical Support Update Failed" ;
							$data['status']=0 ;
						}
					} else if($pstatus==0) {
						$data['message']="Scheduling Conflict" ;
						$data['status']=0 ;
					} else if($pstatus==2) {
						$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
						$data['status']=0 ;
					} else if($pstatus==3) {
						$data['message']=" Already Added 10 Periods." ;
						$data['status']=0 ;
					}
				}
			}
			$this->load->Model('instructormodel');
			$update = array('lastLogin'=>$now,'step3'=>1);
			$this->instructormodel->updatesteps($update);
			redirect('teacherplan');
		}
		$data['menustyle'] = 'style="display:none;"';
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('period/add_period',$data);	
	}
	
	function get_subject($grade_id){
		$this->load->Model('grade_subjectmodel');
		$subjects = $this->grade_subjectmodel->getgrade_subjects(false,false,$grade_id);
		echo json_encode($subjects);
		exit;
	}
}

?>