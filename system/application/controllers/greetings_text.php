<?php
/**
 * greetings_text Controller.
 *
 */
class Greetings_text extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  if($this->session->userdata("login_type")=='user')
	  {
	     $data['view_path']=$this->config->item('view_path');
		//$district_id = $this->session->userdata("district_id");
		//$this->load->Model('greetings_textmodel');
		//$total_records = $this->greetings_textmodel->getgreetings_textscount($district_id);
		 $this->load->view('greetings_text/all',$data);
 	  }
 	}
 	
	function getgreetings_texts($page)
	{
 	  	$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('greetings_textmodel');
		$district_id = $this->session->userdata("district_id");
		$total_records = $this->greetings_textmodel->getgreetings_textscount($district_id);
		
		$status = $this->greetings_textmodel->getgreetings_texts($page, $per_page,$district_id);
 		
		if($status!=FALSE){
 		print "<div class='htitle'>Assessment Letters            </div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Assessment Letter</td><td>Report</td><td>Actions</td></tr>";
 			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['greeting_text_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['greeting_text'].'</td>';
				/*if($val['report']==1)
				{
					$report='B & L Report';
				}
				if($val['report']==2)
				{
					$report='Classwork Performance';
				}
				if($val['report']==3)
				{
					$report='ELD Report';
				}
				if($val['report']==4)
				{
					$report='Observation Count';
				}
				if($val['report']==5)
				{
					$report='Notification Report';
				}
				if($val['report']==6)
				{
					$report='P.D. Activity Report';
				}
				if($val['report']==7)
				{
					$report='HomeWork Log';
				} */
				if($val['report']==1)
				{
					$report='Student Progress Report';
				}
				print '<td>'.$report.'</td>
				
				<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="planedit('.$val['greeting_text_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="plandelete('.$val['greeting_text_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
                   $pagination=$this->do_pagination($total_records,$per_page,$page,'greetings_text');
					print $pagination;	
						
                        
		}else{
			
			print "<div class='htitle'>Assessment Letters            </div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Assessment Letter</td><td>Report</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='3'>No Assessment Letters Records Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'greetings_text');
						print $pagination;	
		}
		
 	}
	function add_plan()
	{
 		$this->load->Model('greetings_textmodel');
			 
			$cstatus=$this->greetings_textmodel->check_plan();
			 $data['message'] ='';
			if($cstatus==0)
			{
			$status=$this->greetings_textmodel->add_plan();
			if($status!=0){
				   $data['message']="Assessment Letter added Sucessfully." ;
				   $data['status']=1 ;
	 				}
					else
					{
					  $data['message']="Contact Technical Support Update Failed." ;
					  $data['status']=0 ;
	 				}
			  }
			  else
			  {
					$data['message']="Assessment Letter Already Added For This Report To This Distirct. " ;
					$data['status']=0 ;
			  }
			
		echo json_encode($data);
		exit;	
 	}
	function update_plan()
	{
	$this->load->Model('greetings_textmodel');
 		$cstatus=$this->greetings_textmodel->update_check_plan();
			if($cstatus==0)
			{
	  $status=$this->greetings_textmodel->update_plan();
		if($status==true){
		       $data['message']="Assessment Letter Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
 				}
				
			}	
			else
			  {
					$data['message']="Assessment Letter Already Existing For This Report To This Distirct. " ;
				  $data['status']=0 ;
			  }
		
		echo json_encode($data);
		exit;		
	}
	
	function delete($plan_id)
	{
		
		$this->load->Model('greetings_textmodel');
		$result = $this->greetings_textmodel->deleteplan($plan_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getplaninfo($plan_id)
	{
		if(!empty($plan_id))
	  {
		$this->load->Model('greetings_textmodel');
		$data['greetings_text']=$this->greetings_textmodel->getplanById($plan_id);
		$data['greetings_text']=$data['greetings_text'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	 		$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";


						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
				}
	}
	?>