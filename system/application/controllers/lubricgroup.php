<?php
/**
 * lubricgroup Controller.
 *
 */
class Lubricgroup extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	   if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('lubricgroup/all',$data);
	 }
	  else if($this->session->userdata("login_type")=='user')
	  {
	  
	   $data['view_path']=$this->config->item('view_path');
	   $this->load->view('lubricgroup/index',$data);
	  } 
	  
	}
	function copy()
	{
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('lubricgroup/copy',$data);
	
	}
	function pointcopy()
	{
		$data['status']=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    $this->load->Model('lubricgroupmodel');
		    $status=$this->lubricgroupmodel->addpoints();
			$data['status']=$status;
		 }
	   }
	   echo json_encode($data);
		exit;
	
	}
	function getlubricgroups($page,$state_id,$country_id,$district_id=false)
	{
		if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('lubricgroupmodel');
		if($page!='all')
		{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		
		$total_records = $this->lubricgroupmodel->getlubricgroupCount($state_id,$country_id,$district_id);
		
			$status = $this->lubricgroupmodel->getlubricgroups($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Leadership Rubric Standards</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td  >Group Name</td><td>State</td><td>District</td><td>Description</td><td >Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['group_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['group_name'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districts_name'].'</td>';
				if(strlen($val['description'])>60)
				{
				print '<td>'.substr($val['description'], 0, 60).'...</td>';
				}
				else
				{
					print '<td>'.$val['description'].'</td>';
				}				
				
				print '<td nowrap><input class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="distedit('.$val['group_id'].')"><input class="btnsmall" title="Delete" type="button" name="Delete" value="Delete" onclick="distdelete('.$val['group_id'].')" ></td> 		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'lubricgroups');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td  >Group Name</td><td>State</td><td>District</td><td>Description</td><td >Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Leadership Rubrics Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'lubricgroups');
						print $pagination;	
		}
		
		}
		else
		{
			$status = $this->lubricgroupmodel->getlubricgroups($page, 'all',$state_id,$country_id,$district_id);
		
			if($status!=FALSE){
			
			$chk="'chk_'";
			print '<div class="htitle">Leadership Rubric Standards</div><table class="tabcontent" cellspacing="0" cellpadding="0" id="conf" ><tr class="tchead" ><td><input id="chk"	name="chk_grp" type="checkbox"	value="1" onclick= "toggleCheckboxes('.$chk.',this.checked)" /></td><td  >Group Name</td><td>State</td><td>District</td><td>Description</td></tr>';
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['group_id'].'" class="'.$c.'" >';
			
				print '
				<td><input type="checkbox" id="chk_'.$val['group_id'].'" name="point_id[]" value="'.$val['group_id'].'"></td>
				<td>'.$val['group_name'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districts_name'].'</td>
				<td>'.$val['description'].'</td>
				
					</tr>';
				$i++;
				}
				print '</table>';
               
				
                        
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td  >Group Name</td><td>State</td><td>District</td><td>Description</td></tr>
			<tr><td valign='top' colspan='10'>No Leadership Rubric Standards Found.</td></tr></table>
			";
			
			
		}
		
		
		}
		
	}
	
	function getlubricgroupinfo($dist_id)
	{
		if(!empty($dist_id))
	  {
		$this->load->Model('lubricgroupmodel');
		
		$data['lubricgroup']=$this->lubricgroupmodel->getlubricgroupById($dist_id);
		$data['lubricgroup']=$data['lubricgroup'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_lubricgroup()
	{
	
	
		$this->load->Model('lubricgroupmodel');
	
		$status=$this->lubricgroupmodel->add_lubricgroup();
		if($status!=0){
		       $data['message']="lubricgroup added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_lubricgroup()
	{
	$this->load->Model('lubricgroupmodel');
	    $status=$this->lubricgroupmodel->update_lubricgroup();
		if($status==true){
		       $data['message']="lubricgroup Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;		
	}
	
	function delete($dist_id)
	{
		
		$this->load->Model('lubricgroupmodel');
		$result = $this->lubricgroupmodel->deletelubricgroup($dist_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
		
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	