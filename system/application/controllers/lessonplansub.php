<?php
/**
 * lessonplansub Controller.
 *
 */
class Lessonplansub extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  
	  $this->load->Model('lessonplanmodel');
	  $data['lessonplan']=$this->lessonplanmodel->getalllessonplans();
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('lessonplansub/index',$data);
	
	}
	
	function getlessonplansubs($page,$group_id=false)
	{
		if($group_id==false)
		{
		  $group_id='all';
		}
		
		
		$this->load->Model('lessonplansubmodel');
		//if($page!='all')
		{
			$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
			$total_records = $this->lessonplansubmodel->getlessonplansubCount($group_id);
		
			$status = $this->lessonplansubmodel->getlessonplansubs($page, $per_page,$group_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Lesson Plan SubTab</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Lesson Plan Tab</td><td>Sub Tab</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['lesson_plan_id'].'" class="'.$c.'" >';
			
				print '
				<td>'.$val['tab'].'</td>
				<td>'.$val['subtab'].'</td>
				
				
				<td nowrap><input  class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="pointedit('.$val['lesson_plan_id'].')"><input class="btnsmall" title="Delete"  type="button" name="Delete" value="Delete" onclick="pointdelete('.$val['lesson_plan_sub_id'].')" ></td>		</tr>';
				$i++;}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'lessonplansubs');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Lesson Plan Tab</td><td>Sub Tab</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No lessonplansubs Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'lessonplansubs');
						print $pagination;	
		}
		
		}
		
	}
	
	function getlessonplansubinfo($point_id)
	{
		if(!empty($point_id))
	  {
		$this->load->Model('lessonplansubmodel');
		
		$data['lessonplansub']=$this->lessonplansubmodel->getlessonplansubById($point_id);
		$data['lessonplansub']=$data['lessonplansub'];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	
	
	function add_lessonplansub()
	{
	
	
		$this->load->Model('lessonplansubmodel');
	
		$status=$this->lessonplansubmodel->add_lessonplansub();
		if($status!=0){
		       $data['message']="lessonplansub added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_lessonplansub()
	{
	$this->load->Model('lessonplansubmodel');
	    $status=$this->lessonplansubmodel->update_lessonplansub();
		if($status==true){
		       $data['message']="lessonplansub Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;		
	}
	
	function delete($dist_id)
	{
		
		$this->load->Model('lessonplansubmodel');
		$result = $this->lessonplansubmodel->deletelessonplansub($dist_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	