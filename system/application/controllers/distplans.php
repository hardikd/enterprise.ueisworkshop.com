<?php
/**
 * observer Controller.
 *
 */
class Distplans extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index");
		}
		
	}


function index()
	{
	
	 $this->load->Model('schoolmodel');
	
	 $data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['teacher']=false; 
	   
	   }
	 $data['view_path']=$this->config->item('view_path');
	 $this->load->view('displans/answer',$data);
	
	
	
	
	}
	function answerview($teacher_id,$message=false)
	{
	
		$data['teacher_id']=$teacher_id;
		
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		if($message!=false)
		{
			$data['message']=$message;
		
		}
		else
		{
		 $data['message']='';
		
		}
	
		$this->load->Model('observationplanmodel');
		$data['observationplan']=$this->observationplanmodel->getallplans();
		$data['observationplan_ans']=$this->observationplanmodel->getallanswerplans($teacher_id);
		if($data['observationplan']!=false)
		{
		  if($data['observationplan_ans']!=false)
		  {
		    foreach($data['observationplan'] as $key=>$plan)
			{
            foreach($data['observationplan_ans'] as $ans)
			{
			  if($plan['observation_plan_id']==$ans['observation_plan_id'] && $ans['archived']=='' )
			  {
			    $data['observationplan'][$key]['answer']=$ans['answer']; 
				 $data['observationplan'][$key]['observation_ans_id']=$ans['observation_ans_id']; 
				  $data['observationplan'][$key]['comment']=$ans['comment']; 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}
		//print_r($data['observationplan']);
		$data['view_path']=$this->config->item('view_path');
	 $this->load->view('displans/answerview',$data);
		
	
	
	
	
	
	}
	function commentssave()
	{
	 $teacher_id=$this->input->post('teacher_id');
	 if($this->input->post('submit')=='Submit')
	 {
	 $this->load->Model('observationplanmodel');
	 $status=$this->observationplanmodel->commentssave();
	 $message="Comment Saved Sucessfully";
	 }
	 else if($this->input->post('submit')=='Archive')
	 {
	   if($this->input->post('an')==0)
		{   
		$this->load->Model('observationplanmodel');
	 $status=$this->observationplanmodel->archived();
	 if($status==true)
	 {
		$message="Archived Sucessfully";
	  }
	  else
	  {
         $message="Archived Twice a Day For same Teacher Is Not Possible";
	  }
		}
		else
		{

         $message="Please Ask Teacher To Answer";

		}		
	 
	 }
	 $this->answerview($teacher_id,$message);
	
	
	
	}
	function getarchived($teacher_id)
	{
	   $this->session->set_userdata('teacher_archive',$teacher_id);
	
	   redirect('distplans/archived');
	
	}
	
	function archived()
	{
	  $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($this->session->userdata('teacher_archive'));
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
	  $this->load->Model('observationplanmodel');
	  //Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/distplans/archived/';
		$config['total_rows'] = $this->observationplanmodel->getarchivedCount();
		
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		}
		//Pagination Code End

	  
	 
	  $data['archived']=$this->observationplanmodel->getarchivedprojects($start,$config['per_page']);
	  //print_r($data['archived']);
	  //exit;
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('displans/archived',$data);
	
	
	}
}
?>	