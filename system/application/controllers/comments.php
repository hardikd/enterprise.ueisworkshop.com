<?php
ob_start();
$view_path="system/application/views/";
require_once($view_path.'inc/html2pdf/html2pdf.class.php');
include(MEDIA_PATH.'inc/foto_upload_script.php');
class Comments extends	Controller {

	function __construct()
	{
		parent::Controller();
	}

	function index()
	{
	
	
	}
	function getteacherobservationreportdata($year,$school_id)
{

	$page = isset($_POST['page'])?$_POST['page']:1; // get the requested page
	$limit = isset($_POST['rows'])?$_POST['rows']:10; // get how many rows we want to have into the grid
	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'name'; // get index row - i.e. user click to sort
	$sord = isset($_POST['sord'])?$_POST['sord']:''; // get the direction
	
			 
 
	if(!$sidx) $sidx =1;
	
	$this->load->Model('teachermodel');	  
	 
		$teachers = $this->teachermodel->getTeachersBySchool($school_id);


	
	 
	 $count = count($teachers);
 
	if( $count > 0 ) {
		$total_pages = ceil($count/$limit);    //calculating total number of pages
	} else {
		$total_pages = 0;
	}
	if ($page > $total_pages) $page=$total_pages;
 
	$start = $limit*$page - $limit; // do not put $limit*($page - 1)
	$start = ($start<0)?0:$start;  // make sure that $start is not a negative value
 
	
	//$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction
 
	if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
		header("Content-type: application/xhtml+xml;charset=utf-8");
	} else {
		header("Content-type: text/xml;charset=utf-8");
	}
	$et = ">";
 
	echo "<?xml version='1.0' encoding='utf-8'?$et\n";
	echo "<rows>";
	echo "<page>".$page."</page>";
	echo "<total>".$total_pages."</total>";
	echo "<records>".$count."</records>";
	// be sure to put text data in CDATA
	
	 
	   
	 
	if($teachers!=false)
	{
	
	foreach($teachers as $key=>$teachervalue)
	{
	 $reportdata=$this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'],$year);	  	 
	  
	echo "<row id='".$key."'>";
	echo "<cell><![CDATA[".$teachervalue['firstname']."  ".$teachervalue['lastname']."]]></cell>";	
	if($reportdata!=false)
	{
	for($m=1;$m<=12;$m++)
	
	{
	$forma=0;
	$formaformal=0;
	$formainformal=0;
	$formb=0;
	$formbformal=0;
	$formbinformal=0;
	$formc=0;
	$formcformal=0;
	$formcinformal=0;
	$formp=0;
	$formpformal=0;
	$formpinformal=0;
	foreach($reportdata as $reportvalue)
	{
	
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='forma')
	 {
	    $forma=1;
		if($reportvalue['period_lesson']=='Formal')
		{
		 $formaformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formainformal=$reportvalue['count'];
		}
	   
	 }
	 
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='formb')
	 {
	  $formb=1;
	  if($reportvalue['period_lesson']=='Formal')
		{
		 $formbformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formbinformal=$reportvalue['count'];
		}
	 }
	 
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='formc')
	 {
	  $formc=1;
	  if($reportvalue['period_lesson']=='Formal')
		{
		 $formcformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formcinformal=$reportvalue['count'];
		}
	 }
	 
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='formp')
	 {
	  $formp=1;
	  if($reportvalue['period_lesson']=='Formal')
		{
		 $formpformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formpinformal=$reportvalue['count'];
		}
	 }
	  
	 
	
	}
	if($forma==1)
	  {
	    echo "<cell><![CDATA[<font color='#5987D3'>".$formaformal."</font>,&nbsp;<font color='#DD6435'>".$formainformal."</font>]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[]]></cell>";	
	  }
	  if($formb==1)
	  {
	    echo "<cell><![CDATA[<font color='#5987D3'>".$formbformal."</font>,&nbsp;<font color='#DD6435'>".$formbinformal."</font>]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[]]></cell>";	
	  }
	  
	  if($formp==1)
	  {
	    echo "<cell><![CDATA[<font color='#5987D3'>".$formpformal."</font>,&nbsp;<font color='#DD6435'>".$formpinformal."</font>]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[]]></cell>";	
	  }
	  if($formc==1)
	  {
	    echo "<cell><![CDATA[<font color='#5987D3'>".$formcformal."</font>,&nbsp;<font color='#DD6435'>".$formcinformal."</font>]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[]]></cell>";	
	  }
	}	
	
	}
	else
	{
	for($m=1;$m<=12;$m++)	
	{
	  echo "<cell><![CDATA[]]></cell>";	
	  echo "<cell><![CDATA[]]></cell>";	
	  echo "<cell><![CDATA[]]></cell>";	
	  echo "<cell><![CDATA[]]></cell>";	
	}
	
	}
	echo "</row>";
	}
	}
	else
	{
	 echo "<row id='no'>";
	echo "<cell><![CDATA[No Teachers Found]]></cell>";	
	echo "</row>";
	
	}
	
	
	echo "</rows>";

}
function getteachernotificationreportdata($year,$school_id)
{

	$page = isset($_POST['page'])?$_POST['page']:1; // get the requested page
	$limit = isset($_POST['rows'])?$_POST['rows']:10; // get how many rows we want to have into the grid
	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'name'; // get index row - i.e. user click to sort
	$sord = isset($_POST['sord'])?$_POST['sord']:''; // get the direction
	
			 
 
	if(!$sidx) $sidx =1;
	
	$this->load->Model('teachermodel');	  
	 
		$teachers = $this->teachermodel->getTeachersBySchool($school_id);


	
	 
	 $count = count($teachers);
 
	if( $count > 0 ) {
		$total_pages = ceil($count/$limit);    //calculating total number of pages
	} else {
		$total_pages = 0;
	}
	if ($page > $total_pages) $page=$total_pages;
 
	$start = $limit*$page - $limit; // do not put $limit*($page - 1)
	$start = ($start<0)?0:$start;  // make sure that $start is not a negative value
 
	
	//$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction
 
	if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
		header("Content-type: application/xhtml+xml;charset=utf-8");
	} else {
		header("Content-type: text/xml;charset=utf-8");
	}
	$et = ">";
 
	echo "<?xml version='1.0' encoding='utf-8'?$et\n";
	echo "<rows>";
	echo "<page>".$page."</page>";
	echo "<total>".$total_pages."</total>";
	echo "<records>".$count."</records>";
	// be sure to put text data in CDATA
	
	 
	   
	 
	if($teachers!=false)
	{
	
	foreach($teachers as $key=>$teachervalue)
	{
	 $this->load->Model('lessonplanmodel');	  
	 $lessondata=$this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'],$year);
$homeworkdata=$this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'],$year);	  	 
$behaviordata=$this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'],$year);	  	 	 
	  
	echo "<row id='".$key."'>";
	echo "<cell><![CDATA[".$teachervalue['firstname']."  ".$teachervalue['lastname']."]]></cell>";	
	
	for($m=1;$m<=12;$m++)
	
	{
	$lp=0;
	$hn=0;	
	$bl=0;
	$lpvalue=0;	
	$hnvalue=0;	
	$blvalue=0;		
	
	
	if($lessondata!=false)
	{
	foreach($lessondata as $lessonvalue)
	{	
	 if($lessonvalue['month']==$m)
	 {
	    $lp=1;
		
		$lpvalue=$lessonvalue['count'];
		
	   
	 }	
	 }
	 }
	 if($homeworkdata!=false)
	{
	foreach($homeworkdata as $homevalue)
	{	
	 if($homevalue['month']==$m)
	 {
	    $hn=1;
		
		$hnvalue=$homevalue['count'];
		
	   
	 }	
	 }
	 }
	 if($behaviordata!=false)
	{
	foreach($behaviordata as $bevalue)
	{	
	 if($bevalue['month']==$m)
	 {
	    $bl=1;
		
		$blvalue=$bevalue['count'];
		
	   
	 }	
	 }
	 }
	if($lp==1)
	  {
	    echo "<cell><![CDATA[".$lpvalue."]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[0]]></cell>";	
	  }
	  if($hn==1)
	  {
	    echo "<cell><![CDATA[".$hnvalue."]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[0]]></cell>";	
	  }
	  if($bl==1)
	  {
	    echo "<cell><![CDATA[".$blvalue."]]></cell>";	
	  }
	  else
	  {
	  echo "<cell><![CDATA[0]]></cell>";	
	  }
	  
	  
	}	
	
	
	
	echo "</row>";
	}
	}
	else
	{
	 echo "<row id='no'>";
	echo "<cell><![CDATA[No Teachers Found]]></cell>";	
	echo "</row>";
	
	}
	
	
	echo "</rows>";

}
function getteacherprofessionalreportdata($year,$school_id)
{

	$page = isset($_POST['page'])?$_POST['page']:1; // get the requested page
	$limit = isset($_POST['rows'])?$_POST['rows']:10; // get how many rows we want to have into the grid
	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'name'; // get index row - i.e. user click to sort
	$sord = isset($_POST['sord'])?$_POST['sord']:''; // get the direction
	
			 
 
	if(!$sidx) $sidx =1;
	
	$this->load->Model('teachermodel');	  
	
		$teachers = $this->teachermodel->getTeachersBySchool($school_id);
		


	
	 
	 $count = count($teachers);
 
	if( $count > 0 ) {
		$total_pages = ceil($count/$limit);    //calculating total number of pages
	} else {
		$total_pages = 0;
	}
	if ($page > $total_pages) $page=$total_pages;
 
	$start = $limit*$page - $limit; // do not put $limit*($page - 1)
	$start = ($start<0)?0:$start;  // make sure that $start is not a negative value
 
	
	//$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction
 
	if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
		header("Content-type: application/xhtml+xml;charset=utf-8");
	} else {
		header("Content-type: text/xml;charset=utf-8");
	}
	$et = ">";
 
	echo "<?xml version='1.0' encoding='utf-8'?$et\n";
	echo "<rows>";
	echo "<page>".$page."</page>";
	echo "<total>".$total_pages."</total>";
	echo "<records>".$count."</records>";
	// be sure to put text data in CDATA
	
	 
	   
	 
	if($teachers!=false)
	{
	
	foreach($teachers as $key=>$teachervalue)
	{
	 $this->load->Model('banktimemodel');	  
	 $plctotal=$this->banktimemodel->getschoolplcall($school_id,$year);
	 $plcteacher=$this->banktimemodel->getteacherplcall($school_id,$teachervalue['teacher_id'],$year);
	  $articletotalassign=$this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'],$year);
	   $articleteacherjournal=$this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'],$year);
	   $videototalassign=$this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'],$year);
	   $videoteacherjournal=$this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'],$year);
	   $artifacts=$this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'],$year);

	  
	echo "<row id='".$key."'>";
	echo "<cell><![CDATA[".$teachervalue['firstname']."  ".$teachervalue['lastname']."]]></cell>";	
	
	
	
	
	   echo "<cell><![CDATA[".$plcteacher." of ".$plctotal."]]></cell>";	
		 echo "<cell><![CDATA[".$articleteacherjournal." of ".$articletotalassign."]]></cell>";	
		 echo "<cell><![CDATA[".$videoteacherjournal." of ".$videototalassign."]]></cell>";	
		 echo "<cell><![CDATA[".$artifacts."]]></cell>";
	
	  
	  
	
	
	
	
	echo "</row>";
	}
	}
	else
	{
	 echo "<row id='no'>";
	echo "<cell><![CDATA[No Teachers Found]]></cell>";	
	echo "</row>";
	
	}
	
	
	echo "</rows>";

}
	 function addcomment()
	{
	  
	  $data['status']=0;
	  
	  
	  
	  
	    $pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	    if(isset($pdata['id']) && isset($pdata['comments'])  )
		 {
		    $this->load->Model('goalplanmodel');
		    $status=$this->goalplanmodel->addcomment();
			$data['status']=$status;
		 
		 }
	    
	   
	   }
	  
	    
	  
	 echo json_encode($data);
		exit;
	}
	
	 function getcomments($id)
	{
	  $this->load->Model('goalplanmodel');
	   $status = $this->goalplanmodel->getcommentsbyplanid($id);
		
		
		
		if($status!=FALSE){
		
		  foreach($status as $val)
		  {
		    print '<div style="padding:4px 0px 4px 0px;border-bottom:1px #cccccc solid;">';
			print '<div class="stcommentimg">
<img src="'.SITEURLM.'images/'.$val['avatar'].'.gif" class="small_face"/>
</div> 
		<div class="stcommenttext">';
			print '<b>'.$val['role'].':</b> '.$val['comments'].' <b>('.$val['created'].')</b>';
			print '</div></div>';
		  
		  }
		}
		
		else
		{
		
		 print 'No Comments Found';
		
		}
	
	
	}
	
	 function getgoalcomments($id,$teacher_id=false,$year=false)
	{
	    $this->load->Model('goalplanmodel');
	     if($teacher_id=='false')
		{

			$teacher_id=false;

		}
		 $desc=$this->goalplanmodel->getplanById($id);
		 
		 $observer_desc=$desc[0]['school_description'];	
		 $desc=$desc[0]['description'];
		
		 $pid=$this->goalplanmodel->getteacherplanid($id,$teacher_id,$year);
		 //$observer_desc=$this->goalplanmodel->getplansdescbyteacher($id,$year,$teacher_id);
		 
		 
		 
		 if($this->session->userdata('login_type')=='observer')
		 {
		    $observer_desc_dis='<div id="observer_description">School Level Task:'.$observer_desc.'
			<!--<div id="observer_desc_submit'.$id.'">'.$observer_desc.'</div><div><textarea style="height: 67px;width: 433px" name="observer_desc'.$id.'" id="observer_desc'.$id.'" ></textarea>
		<input type="button" name="descsubmit" onclick="desc_submit('.$id.')" id="descsubmit" value="Submit">
		</div>-->
		</div>';
		 
		 }
		 else
		 {
		  $observer_desc_dis='<div id="observer_description">School Level Task:'.$observer_desc.'</div>';
		 
		 
		 
		 
		 }
		 if($pid!=false && $teacher_id==false)
		 {
			$status = $this->goalplanmodel->getcommentsbyplanid($pid[0]['teacher_plan_id']);
		
		
		
		if($status!=FALSE){
		
		  foreach($status as $val)
		  {
		    print '<div style="padding:4px 0px 4px 0px;border-bottom:1px #cccccc solid;">';
			print '<div class="stcommentimg">
<img src="'.SITEURLM.'images/'.$val['avatar'].'.gif" class="small_face"/>
</div> 
		<div class="stcommenttext">';
		
			print '<b>'.$val['role'].':</b> '.$val['comments'].'<b>('.$val['created'].')</b>';
			print '</div></div>';
		  
		  }
		}
		
		else
		{
		
		 print 'No Comments Found';
		
		}
	
	   }
	   else if($pid!=false && $teacher_id!=false)
	   {
	     if($year>=date('Y'))
		{
		print '<div style="font-size:11px;color:#666666;">
		Add Comments:<textarea style="height: 57px;width:600px;"  name="comments" id="comment'.$pid[0]['teacher_plan_id'].'"></textarea>
		<input type="submit" class="btnsmall" onclick="btnsmall('.$pid[0]['teacher_plan_id'].')" id="'.$pid[0]['teacher_plan_id'].'" name="submit" value="submit">
		</div>';
	    }
		
		 print '
		 <div id="description">District Task:'.$desc.'</div>
		 '.$observer_desc_dis.'
		 <div style="color:#02AAD2;" id="disgoal">
		Goal:'.$pid[0]['text'].'('.$pid[0]['added'].')</div><div style="color:#02AAD2;" id="discomments">
		Comments:</div>';
		 $status = $this->goalplanmodel->getcommentsbyplanid($pid[0]['teacher_plan_id']);
		if($status!=FALSE){
			print '<div id="comments">';	
		  foreach($status as $val)
		  {
		    print '<div style="padding:4px 0px 4px 0px;border-bottom:1px #cccccc solid;">';
			print '<div class="stcommentimg">
<img src="'.SITEURLM.'images/'.$val['avatar'].'.gif" class="small_face"/>
</div> 
		<div class="stcommenttext">';
			print '<b>'.$val['role'].':</b> '.$val['comments'].'<b>('.$val['created'].')</b>';
			print '</div></div>';
		  
		  }
		  print '</div>';
		}
		
		else
		{
		
		 print '<div id="comments">No Comments Found</div>';
		
		}
		
		
		 
	   
	   
	   
	   }
	   else if($teacher_id!=false){
	   
	     print '<div id="nogoal">  </div>
		 <div id="description">District Task:'.$desc.'</div>
		 '.$observer_desc_dis.'
		 ';
	   
	   }
	}
	function getprofcomments($id,$type)
	{
	  
	  
	    $this->load->Model('observationgroupmodel');
	     
		if($type=='video')
		{
          $status=$this->observationgroupmodel->getvideocomments($id);
           if($status!=FALSE){
		
		  foreach($status as $val)
		  {
		    if($this->session->userdata('login_type')=='teacher')
			{
			print '<table><tr><td>Video Comments:</td><td><textarea name="teachercomments" id="teachercomments" style="width:400px;">'.$val['teacher_comments'].'</textarea></td></tr>
					<tr><td colspan="2" align="center"><input type="submit" name="submit" value="Add" onclick="addcomments('.$val['prof_dev_assign_id'].')"></td></tr></table>';
			}
			else if($this->session->userdata('login_type')=='observer')
			{
			  if($val['teacher_comments']=='')
				{
				  print '<table><tr><td>Video Comments:</td><td> No Comments Found</td></tr></table>';
				}
				else
				{
			  print '<table><tr><td>Video Comments:</td><td>'.$val['teacher_comments'].'</td></tr>
					</table>';
			    }
			}
		  
		  }
		}
		
		
		}
		else
		{
             $status=$this->observationgroupmodel->getarticlecomments($id);
           if($status!=FALSE){
		
		  foreach($status as $val)
		  {
		    if($this->session->userdata('login_type')=='teacher')
			{
				print '<table><tr><td>Article Comments:</td><td><textarea name="teachercomments" id="teachercomments" style="width:400px;">'.$val['teacher_comments'].'</textarea></td></tr>
					<tr><td colspan="2" align="center"><input type="submit" name="submit" value="Add" onclick="addcomments('.$val['article_dev_assign_id'].')"></td></tr></table>';
			}
		    else if($this->session->userdata('login_type')=='observer')
			{
				if($val['teacher_comments']=='')
				{
				  print '<table><tr><td>Article Comments:</td><td> No Comments Found</td></tr></table>';
				}
				else
				{
					print '<table><tr><td>Article Comments:</td><td>'.$val['teacher_comments'].'</td></tr></table>';
					
				}	
			
			}
		  }
		}


		}	
		 
	
	   
	}
	function getrefcomments($id,$type)
	{
	  
	  
	    $this->load->Model('observationgroupmodel');
	     
		if($type=='video')
		{
          $status=$this->observationgroupmodel->getvideocomments($id);
           if($status!=FALSE){
		
		  foreach($status as $val)
		  {
		    if($this->session->userdata('login_type')=='teacher')
			{
			if($val['observer_comments']=='')
				{
				  print '<table><tr><td>Video Comments:</td><td> No Comments Found</td></tr></table>';
				}
				else
				{
			  print '<table><tr><td>Video Comments:</td><td>'.$val['observer_comments'].'</td></tr>
					</table>';
			    }
			}
			else if($this->session->userdata('login_type')=='observer')
			{
			  
			  print '<table><tr><td>Video Comments:</td><td><textarea name="observercomments" id="observercomments" style="width:400px;">'.$val['observer_comments'].'</textarea></td></tr>
					<tr><td colspan="2" align="center"><input type="submit" name="submit" value="Add" onclick="addrefcomments('.$val['prof_dev_assign_id'].')"></td></tr></table>';
			  
			  
			  
			}
		  
		  }
		}
		
		
		}
		else
		{
             $status=$this->observationgroupmodel->getarticlecomments($id);
           if($status!=FALSE){
		
		  foreach($status as $val)
		  {
		    if($this->session->userdata('login_type')=='teacher')
			{
				if($val['observer_comments']=='')
				{
				  print '<table><tr><td>Article Comments:</td><td> No Comments Found</td></tr></table>';
				}
				else
				{
					print '<table><tr><td>Article Comments:</td><td>'.$val['observer_comments'].'</td></tr></table>';
					
				}	
			}
		    else if($this->session->userdata('login_type')=='observer')
			{
				print '<table><tr><td>Article Comments:</td><td><textarea name="observercomments" id="observercomments" style="width:400px;">'.$val['observer_comments'].'</textarea></td></tr>
					<tr><td colspan="2" align="center"><input type="submit" name="submit" value="Add" onclick="addrefcomments('.$val['article_dev_assign_id'].')"></td></tr></table>';
					
			
			}
		  }
		}


		}	
		 
	
	   
	}
	
	function addprofcomments($id,$type,$comments)
	{
	
	 $this->load->Model('observationgroupmodel');
	     
		if($type=='video')
		{
          if($comments!='')
		 { 
		  $status=$this->observationgroupmodel->addteachervideocomments($id,$comments);
           if($status==true){
		
		    print 'Succefully Commented';
		}
		else
		{
			print 'Failed Please Try Again';
		
		}
		}
		
		
		}
		else
		{
               if($comments!='')
		 { 
		  $status=$this->observationgroupmodel->addteacherarticlecomments($id,$comments);
           if($status==true){
		
		    print 'Succefully Commented';
		}
		else
		{
			print 'Failed Please Try Again';
		
		}
		}


		}	
		 
	
	
	
	}
	function addrefcomments($id,$type,$comments)
	{
	
	 $this->load->Model('observationgroupmodel');
	     
		if($type=='video')
		{
          if($comments!='')
		 { 
		  $status=$this->observationgroupmodel->addobservervideocomments($id,$comments);
           if($status==true){
		
		    print 'Succefully Commented';
		}
		else
		{
			print 'Failed Please Try Again';
		
		}
		}
		
		
		}
		else
		{
               if($comments!='')
		 { 
		  $status=$this->observationgroupmodel->addobserverarticlecomments($id,$comments);
           if($status==true){
		
		    print 'Succefully Commented';
		}
		else
		{
			print 'Failed Please Try Again';
		
		}
		}


		}	
		 
	
	
	
	}
	 function  addgoal()
	{
	   $pdata=$this->input->post('pdata');
	   $id=$pdata['id'];
	   $text=$pdata['text'];
	   $year=$pdata['year'];
	   $schedule_week_plan=$pdata['schedule_week_plan'];
	   $this->load->Model('goalplanmodel');
	     $mailstatus=$this->goalplanmodel->goalstatus($schedule_week_plan);
		 $status=$this->goalplanmodel->addgoal($id,$text,$year,$schedule_week_plan);
		 
		 if($status!=false)
		 {  
		    $result=$this->goalplanmodel->getteacherplanid($id,false,$year);
			$sresult=$this->goalplanmodel->getplanById($id);
			if($mailstatus==false)
			{
				/* start of send Email to Observer*/
	 $teacher_id=$this->session->userdata("teacher_id");
	 $this->load->Model('schedulemodel');
	 $this->load->Model('observermodel');
	 $this->load->Model('scheduletaskmodel');
	 $this->load->Model('teachermodel');
		$teacherdata=$this->teachermodel->getteacherById($teacher_id);
		$teachername=$teacherdata[0]['firstname'].' '.$teacherdata[0]['lastname'];
		$teacheremail=$teacherdata[0]['email'];
		$schduledata=$this->schedulemodel->getscheduleplaninfo($schedule_week_plan);
		$observerdata=$this->observermodel->getobserverById($schduledata[0]['role_id']);
		
		$taskdata=$this->scheduletaskmodel->getplanById($schduledata[0]['task']);
		$dates=$schduledata[0]['date'];
	  $date1=explode('-',$dates);
	  
	  $date=$date1[0].'/'.$date1[1].'/'.$date1[2];
	  
      if($this->valid_email($observerdata[0]['email']))
			{
			 $email=$observerdata[0]['email'];
			$observer_name=$observerdata[0]['observer_name'];
			$taskd=$taskdata[0]['task'];
			
			
	   /*  sending mail to observer */

	   $subject ="Tor-Events Completed For $date ";
	$message = "<html>
					<head>
					  <title>Tor-Events Completed For $date </title>
					</head>
					<body>
                     <table height='40px'  width='100%' style='background-color:#ccc'>
					 <tr>
					 <td >
					 <font color='white' size='5px'>Events</font>
					 </td>
					 </tr>
					 </table>
					 <table height='10px'  width='100%' >
					 <tr>
					 <td>					 
					 </td>
					 </tr>
					 </table>
					 <table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
					  <tr>
					  <td colspan='2'>
					  Greetings $observer_name,					  
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  <b>Task Details ($date)</b>
					  </td>
					  </tr>
					  <tr height='10%'>
					  <td>
					  </td>
					  </tr>
					  <tr>
					  <td width='30px'>					  					   
					   <b>Task:</b>
					  </td>
					  <td>
					  $taskd
					  </td>
					  </tr>
					  <tr>
					   <td width='30px'><b>Assigned To:</b>
					   </td>
					   <td>
					   $teachername </td>
					   </tr>
					   <tr><td width='30px'><b>Date:</b></td>
					   <td> $date</td></tr>					   
					  <tr>
					  <td width='30px'>					  
					  <b>Login Url:</b></td>
					  <td><a href='http://enterprise.ueisworkshop.com'>http://enterprise.ueisworkshop.com</a>
					  </td>
					  </tr>
					  <tr height='30%'>
					  <td colspan='2'>
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  Powered By,
					  </td>
					  </tr>
					  <tr>
					  <td colspan='2'>
					  TOR 
					  </td>
					  </tr>
					  </table>
					  
					</body>
					</html>";
	   
	//Additional headers
	/*if($teacheremail!='')
	{
	  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	  $headers .= "Reply-To: TOR Teacher <info@ueisworkshop.com>".PHP_EOL;
	}
	else
	{*/
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail($email,$subject,$message,$headers);
	}
	/*  End of Sennding Email To Observer*/
	
			
			
			
			
			
			
			
			
			}
			
		    print '<div style="font-size:11px;color:#666666;">
		Add Comments:<textarea style="height:57px;width:600px;" name="comments" id="comment'.$status.'"></textarea>
		<input type="submit" class="btnsmall" onclick="btnsmall('.$status.')" id="'.$status.'" name="submit" value="submit">
		</div>
			<div id="description">District Task:'.$sresult[0]['description'].'</div>
			<div id="observer_description">School Level Task:'.$sresult[0]['school_description'].'</div>
			<div style="color:#02AAD2;" id="disgoal">
		Goal:'.$text.'('.$result[0]['added'].')</div><div style="color:#02AAD2;" id="discomments">
		Comments:</div><div id="comments">No Comments  Found</div><br />';
		 
		 
		 
		 
		 }
		    

	
	
	
	}
	
	function getTeachersBySchool($school_id)
	{
	
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getTeachersBySchool($school_id);
		echo json_encode($data);
		exit;
	
	
	
	}
	function getvideoinfo($dist_id)
	{
		if(!empty($dist_id))
	  {
		$this->load->Model('observationgroupmodel');
		
		$data['video']=$this->observationgroupmodel->getvideoinfobyid($dist_id);
		
		$data['video']=$data['video'][0];
		
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function getTeachersassigned($id)
	{
	
	if(!empty($id))
	  {
		$this->load->Model('observationgroupmodel');
		
		$data['teacher']=$this->observationgroupmodel->getTeachersassigned($id);
		
		echo json_encode($data);
		exit;
	  }
	
	
	
	
	}
	function getarticleassigned($id)
	{
	
	if(!empty($id))
	  {
		$this->load->Model('observationgroupmodel');
		
		$data['teacher']=$this->observationgroupmodel->getarticleassigned($id);
		
		echo json_encode($data);
		exit;
	  }
	
	
	
	
	}
	function archived($date)
	{
	   
	   if($this->session->userdata('login_type')=='teacher')
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	}
	else if($this->session->userdata('login_type')=='observer' || $this->session->userdata('login_type')=='user' )
	{
		$teacher_id=$this->session->userdata('teacher_archive');
	
	}
	   $this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		$data['date']=$date;
	  if(!empty($date))
	  {
		$this->load->Model('observationplanmodel');
		
		$data['ardata']=$this->observationplanmodel->getArchiveddata($date);
		 $data['view_path']=$this->config->item('view_path');
	     $this->load->view('archived/view',$data);
			
		
		
	  }
	
	
	}
	function createobservationplanpdf($date)
{

		 
		
		$newphrase='';
		$view_path=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='teacher')
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	}
	else if($this->session->userdata('login_type')=='observer' || $this->session->userdata('login_type')=='user' )
	{
		$teacher_id=$this->session->userdata('teacher_archive');
	
	}
	   $this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $teacher_name=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		  $school_name=$data['teacher'][0]['school_name'];
		
		
		}
		else
		{ 
		 $teacher_name='';
		 $school_name='';
		
		}
		$data['date']=$date;
	  if(!empty($date))
	  {
		$this->load->Model('observationplanmodel');
		
		$ardata=$this->observationplanmodel->getArchiveddata($date);
		 
		
		
		
		$newphrase.='<table  cellspacing="0" cellpadding="0" border="0" id="conf" style="width:725px;">
		<tr>
		<td class="htitle" align="center">
		Archived Observation Plans 
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;
		</td>
		</tr>
		<tr >
		<td align="left" style="font-size:13px;font-weight:bold;">
		<b>Teacher Name:'.$teacher_name.'</b>
		</td>
		</tr>
		<tr>
		<td align="left" style="font-size:12px;font-weight:bold;">
		<b>Date:</b>'.$date.'</td>
		</tr>';
		 if($ardata!=false)
		{
		foreach($ardata as $plan) { 
		$newphrase.='<tr>
		<td >
		<div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b>'.$plan['question'].'</b>
		</div>
		</td>
		</tr>
		<tr>';
		if(isset($plan['answer'])) {
		$newphrase.='<td>
		<div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		'.$plan['answer'].'
		</div>
		</td>';
			
		} 
		$newphrase.='</tr>';
		 if(isset($plan['comment'])) { 
		$newphrase.='<tr><td>
		<div  style="border-left:1px #dce6d0 solid; background:#fdfffa;border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px;color:#466229;">
		<b>Comments:</b>'.$plan['comment'].'
		</div>
		</td></tr>';
			
		 } 
		
		 }   } else { 
		$newphrase.='<tr>
		<td>
		No Data Found
		</td>
		</tr>';
		} 
		$newphrase.='</table>	
		';
		
		
	  }

	  
	 
		 
		
   $str='<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
   td{
   font-size:15px;
   }
   </style>';
   
  $str.='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              
        </page_footer>
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b>		
		<br /><b>'.ucfirst($school_name).'</b>';
		
		
		
		
	
		
		$str.='</div>
		</div>
		<br />				
		'.$newphrase.'				
   </page> 
';
   
   $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output();
    



}
	function createobservationplan_pdf($teacher_id)
{

		 
		
		$newphrase='';
		$view_path=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='teacher')
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	}
	
	   
		
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $teacher_name=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		  $school_name=$data['teacher'][0]['school_name'];
		
		
		}
		else
		{ 
		 $teacher_name='';
		 $school_name='';
		
		}
		
	
		$this->load->Model('observationplanmodel');
		$data['observationplan']=$this->observationplanmodel->getallplans();
		$data['observationplan_ans']=$this->observationplanmodel->getallanswerplans($teacher_id);
		if($data['observationplan']!=false)
		{
		  if($data['observationplan_ans']!=false)
		  {
		    foreach($data['observationplan'] as $key=>$plan)
			{
            foreach($data['observationplan_ans'] as $ans)
			{
			  if($plan['observation_plan_id']==$ans['observation_plan_id'] && $ans['archived']=='' )
			  {
			    $data['observationplan'][$key]['answer']=$ans['answer']; 
				 $data['observationplan'][$key]['observation_ans_id']=$ans['observation_ans_id']; 
				  $data['observationplan'][$key]['comment']=$ans['comment']; 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}
		
		
	  if(!empty($data['observationplan']))
	  {
		
		 
		
		
		
		$newphrase.='<table  cellspacing="0" cellpadding="0" border="0" id="conf" style="width:725px;">
		<tr>
		<td class="htitle" align="center">
		Observation Plan
		</td>
		</tr>
		<tr>
		<td>
		&nbsp;
		</td>
		</tr>
		<tr >
		<td align="left" style="font-size:13px;font-weight:bold;">
		<b>Teacher Name:'.$teacher_name.'</b>
		</td>
		</tr>
		';
		 if($data['observationplan']!=false)
		{
		foreach($data['observationplan'] as $plan) { 
		$newphrase.='<tr>
		<td >
		<div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b>'.$plan['question'].'</b>
		</div>
		</td>
		</tr>
		<tr>';
		if(isset($plan['answer'])) {
		$newphrase.='<td>
		<div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		'.$plan['answer'].'
		</div>
		</td>';
			
		}else
		{
$newphrase.='<td>
		<div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		No Answer
		</div>
		</td>';

		}		
		$newphrase.='</tr>';
		 if(isset($plan['comment'])) { 
		$newphrase.='<tr><td>
		<div  style="border-left:1px #dce6d0 solid; background:#fdfffa;border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px;color:#466229;">
		<b>Comments:</b>'.$plan['comment'].'
		</div>
		</td></tr>';
			
		 } 
		
		 }   } else { 
		$newphrase.='<tr>
		<td>
		No Data Found
		</td>
		</tr>';
		} 
		$newphrase.='</table>	
		';
		
		
	  }

	  
	 
		 
		
   $str='<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
   td{
   font-size:15px;
   }
   </style>';
   
  $str.='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              
        </page_footer>
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b>		
		<br /><b>'.ucfirst($school_name).'</b>';
		
		
		
		
	
		
		$str.='</div>
		</div>
		<br />				
		'.$newphrase.'				
   </page> 
';
   
   $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output();
    



}
	function pdarchived($date)
	{
		 $data['idname']='tools';
	   
	   if($this->session->userdata('login_type')=='teacher')
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	}
	else if($this->session->userdata('login_type')=='observer')
	{
		$teacher_id=$this->session->userdata('teacher_archive');
	
	}
	   $this->load->Model('teachermodel');
	   $data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		$data['date']=$date;
	  if(!empty($date))
	  {
		$this->load->Model('observationgroupmodel');
		
		$data['prof_groups']=$this->observationgroupmodel->getvideosdata($date,$teacher_id);
		$data['article_groups']=$this->observationgroupmodel->getarticlesdata($date,$teacher_id);
		 $data['view_path']=$this->config->item('view_path');
	     $this->load->view('archived/pdview',$data);
			
		
		
	  }
	
	
	}
	function getwatchvideo($dist_id)
	{
		if(!empty($dist_id))
	  {
		$this->load->Model('observationgroupmodel');
		
		$data['video']=$this->observationgroupmodel->getvideoinfo($dist_id);
		$data['video']=$data['video'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	function getarticleinfo($id)
	{
	
	
	if(!empty($id))
	  {
		$this->load->Model('observationgroupmodel');
		
		$data['article']=$this->observationgroupmodel->getarticleinfo($id);
		$data['article']=$data['article'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	
	
	}
	function getphotos($page,$date=false)
	{
	   $this->load->Model('articraftsmodel');
	   
		$per_page = 12;
	   $total_records = $this->articraftsmodel->getphotosCount($date);
		
			$status = $this->articraftsmodel->getphotos($page, $per_page,$date);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Photos</div>
			<table class='table table-striped table-bordered'>
			   <thead>
                      <tr>
                                <th>File Preview</th>
                                <th>Photo Name</th> 
                                <th>Actions</th>                             
                                
                            </tr>
                            </thead>
					";
		
		
			$i=0;
			foreach($status as $val){
					
					if($i%3==0 || $i==0)
					{
					 print '<tr>';
	
					}
					
				
			    $siteurl=SITEURLM;
				$WORKSHOP_DISPLAY_FILES=WORKSHOP_DISPLAY_FILES;
				print '<tbody>
				<tr class="odd gradeX"> 
			<td>
<a class="lightbox" style="color:#ffffff;" title="'.$val['name'].'" href="'.$WORKSHOP_DISPLAY_FILES.'photos/'.$val['artifact_id'].'.'.$val['file_type'].'">
<img src="'.$WORKSHOP_DISPLAY_FILES.'photos/'.$val['artifact_id'].'.'.$val['file_type'].'" height="30px" width="30px"></a></td>  
				<td class="hidden-phone">"'.$val['name'].'"</td>	
				 <td><a href="#myModal-delete-student" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a></td></tbody>';
				
				
				
				
				$i++;
				if($i%3==0)
					{
					 print '</tr>';
	
					}
				}
						
					print '</table>';
					
               			$pagination=$this->do_pagination($total_records,$per_page,$page,'photos');
						print $pagination;				
						
		}else{
		print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:535px;' >
				<tr >
				<td>No  Photos Found.</td>
				</tr>
				</table>";
				
			$pagination=$this->do_pagination($total_records,$per_page,$page,'photos');
						print $pagination;	
		}
	
	}
	
	function getnotes($id,$date=false)
	{
	
		$this->load->Model('articraftsmodel');
		$data['articrafts']=$this->articraftsmodel->getnotes($date);
		if($data['articrafts']!=false)
		{
			
$data['articrafts']=$data['articrafts'][0];
		}
		else
		{
         $data['articrafts']['notes']=false;

		}	
		echo json_encode($data);
		exit;
	
	
	
	}
	function getlinks($page,$date=false)
	{
	   $this->load->Model('articraftsmodel');
	   
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
	   $total_records = $this->articraftsmodel->getlinksCount($date);
		
			$status = $this->articraftsmodel->getlinks($page, $per_page,$date);
		
		
		
		if($status!=FALSE){
			
			
			print "
			<table class='table table-striped table-bordered'>
			<thead>
			<tr>
			<th> Link Name</th>
			<th> Link</th>
			</tr>
			</thead>";
			
			$i=0;
			foreach($status as $val){
					
					print '<tr>';
					print'<td style="text-align:left;font-size:12px;vertical-align:top;">'.$val['name'].'</td>';
					print'<td style="font-size:12px;vertical-align:top;"><a href="'.$val['link'].'" target="_blank">Click Here</a></td>';
					print'</tr>';
				
			  
					
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'links');
						print $pagination;	
						
                        
		}else{
			
		print "<table class='table table-striped table-bordered'>
		<thead>
		<tr>
		<th> Link Name</th>
		<th> Link</th>
		</tr>
		<tr >
		<td>No  links Found.</td>
		</tr>
		</table>";
			$pagination=$this->do_pagination($total_records,$per_page,$page,'links');
						print $pagination;	
		}
	
	}
	
	function getvideos($page,$date=false)
	{
	   $this->load->Model('articraftsmodel');
	   
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
	   $total_records = $this->articraftsmodel->getvideosCount($date);
		
			$status = $this->articraftsmodel->getvideos($page, $per_page,$date);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Videos</div>
			<table class='table table-striped table-bordered'>
			
			<tr>
			<th>Video Name</th>
			<th>Actions</th>
			</tr>";
			$i=0;
			foreach($status as $val){
				 
				$siteurl=SITEURLM;
				print '<tr class="odd gradeX">
				<td class="hidden-phone">'.$val['name'].'</td>
				<td><a href="#myModal-delete-student" role="button" class="btn btn-danger" data-toggle="modal"><i class="icon-trash"></i></a></td>
				</tr>';
				
				$i++;
				
					
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'videos');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:535px;' >
			<tr>
			<td>Video Name</td>
			<td>Actions</td>
			</tr>
			<tr>
			<td>No  Videos Found.</td>
			</tr>
			</table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'videos');
						print $pagination;	
		}
	
	}
	
	function getfiles($page,$date=false)
	{
	   $this->load->Model('articraftsmodel');
	   
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
	   $total_records = $this->articraftsmodel->getfilesCount($date);
		
			$status = $this->articraftsmodel->getfiles($page, $per_page,$date);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Files</div>
			<table class='table table-striped table-bordered' >
			<thead>
			<tr>
			<th>File Name</td>
			<th>Actions</td>
			</thead>
			</tr>";
			
			$i=0;
			foreach($status as $val){
					
					
					
				
			    $siteurl=SITEURLM;
				$WORKSHOP_DISPLAY_FILES=WORKSHOP_DISPLAY_FILES;
				print '<tbody>
				<tr class="odd gradeX">
				<td class="hidden-phone">'.$val['name'].'</td>
				<td><a class="btn btn-purple" href="'.$WORKSHOP_DISPLAY_FILES.'files/'.$val['artifact_id'].'.'.$val['file_type'].'"  target="_blank">Download</a></td>
				</td>
				</tr></tbody>';
				
				$i++;
		

				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'files');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:535px;' ><tr class='tchead'><td>File Name</td><td>Actions</td></tr><tr ><td>No  Files Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'files');
						print $pagination;	
		}
	
	}
	
	function addteacherid($teacher_id)
	{
	 $this->session->set_userdata('arti_teacher_id',$teacher_id);
	
	
	}
	function getmemorandumsasigned($page,$school_id,$teacher_id)
	{
	
	    $siteurl=SITEURLM;
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('memorandummodel');
		$total_records = $this->memorandummodel->getmemorandumsasignedscount($school_id,$teacher_id);
		
			$status = $this->memorandummodel->getmemorandumsasigned($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Memorandums</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Subject</td><td>PDF Copy</td><td>Created</td><td>Response</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['memorandum_id'].'" class="'.$c.'" >';
			
				print '
				<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				<td><a href="comments/response/'.$val['assign_id'].'">'.$val['subject'].'</a></td><td><a href="comments/responsepdf/'.$val['assign_id'].'" target="_blank" style="text-decoration:none;color:#FFFFFF;"><img src="'.$siteurl.'images/pdf_icon.gif" ></a></td>
				<td>'.$val['created'].'</td>
				';
				if(empty($val['response']))
				{
				print '<td> Response Incomplete </td>';
				}
				else
				{
				  print '<td><a href="comments/response/'.$val['assign_id'].'">View</a></td>';
				}
				
						print '</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Subject</td><td>Response</td></tr>
			<tr><td valign='top' colspan='10'>No  Memorandums Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
		}
		
	
	
	
	
	}
	
	function getstaffreport($page,$type,$value)
	{
	
	    //$d=explode('-',$date);
		//$date=$d[2].'-'.$d[0].'-'.$d[1];
		
		$siteurl=SITEURLM;
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('communicationmodel');
		$total_records = $this->communicationmodel->getstaffreportcount($type,$value);
		
			$status = $this->communicationmodel->getstaffreport($page,$per_page,$type,$value);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Teacher</td><td>Communication</td><td>Sent</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['staff_communication_id'].'" class="'.$c.'" >';
			
				print '				
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				<td><a class="communicationview" href="#" title="Communication" rel="comments/getother/staff/'.$val['staff_communication_id'].'">View</a></td>
				<td>'.$val['created'].'</td>
				';
				
				
						print '</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Teacher</td><td>Communication</td><td>Sent</td></tr>
			<tr><td valign='top' colspan='10'>No  Records Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
						print $pagination;	
		}
		
	
	
	
	
	}
	function getparentreport($page,$type,$value)
	{
	
	    //$d=explode('-',$date);
		//$date=$d[2].'-'.$d[0].'-'.$d[1];
		
		$siteurl=SITEURLM;
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('communicationmodel');
		$total_records = $this->communicationmodel->getparentreportcount($type,$value);
		
			$status = $this->communicationmodel->getparentreport($page,$per_page,$type,$value);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<table class= table table-striped table-bordered' id='conf' >
		<thead>
		<tr>
		<td>Parent</td>
		<td>Communication</td>
		<td>Sent</td>
		</tr>
		</thead>
		";
					
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tbody><tr id="'.$val['parent_communication_id'].'" class="'.$c.'" >';
			
				print '				
				<td class="hidden-phone">'.$val['firstname'].' '.$val['lastname'].'</td>
				<td><a class="communicationview" href="#" title="Communication" rel="comments/getother/parent/'.$val['parent_communication_id'].'">View</a></td>
				<td>'.$val['created'].'</td>
				';
				
			               
				
				
						print '</tr>';
				$i++;
				}
				print '</body></table>';
               
			if($total_records>0){	
                            $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
                            print $pagination;	
                        }
                        
		}else{
			
			print "<table class= table table-striped table-bordered' id='conf' >
			
			<tr>
			<td>Parent</td>
			<td>Communication</td>
			<td>Sent</td>
			</tr>
			
			<tr><td  valign='top' colspan='3'><p align='center'>No  Records Found.</p></td></tr></table>
			";
			
//			if($total_records>0){	
                            $pagination=$this->do_pagination($total_records,$per_page,$page,'memorandum');
                            print $pagination;	
//                        }
		}
		
	
	
	
	
	}
	
	function getother($type,$id)
	{
	
	
	  $this->load->Model('communicationmodel');
	  if($type=='parent')
	  {
		$status=$this->communicationmodel->getparentcomm($id);
	  }
	  else
	  {
	  $status=$this->communicationmodel->getstaffcomm($id);
	  }
		echo '<b>Communication:</b> '.$status[0]['communication'];
		
	  
	
	
	}
	
	function response($id)
	{
	 
	 if(isset($id))
		 {
		    
			
			$this->load->Model('memorandummodel');
		    $memo=$this->memorandummodel->getassignById($id);
			$data['memo']=$memo[0]['text'];
			$data['assign_id']=$id;
			$data['response']=$memo[0]['response'];
			$data['subject']=$memo[0]['subject'];
			$this->load->Model('teachermodel');
			$teacher=$this->teachermodel->getteacherById($memo[0]['teacher_id']);
			$data['teachername']=$teacher[0]['firstname'].' '.$teacher[0]['lastname'];
			
		 
	   }
	   
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assignview',$data);

	
	
	}
	function responsepdf($id)
	{
	 
	 if(isset($id))
		 {
		    
			
			$this->load->Model('memorandummodel');
		    $memo=$this->memorandummodel->getassignById($id);
			$data['memo']=$memo[0]['text'];
			$data['response']=$memo[0]['response'];
			$data['subject']=$memo[0]['subject'];
			$this->load->Model('teachermodel');
			$teacher=$this->teachermodel->getteacherById($memo[0]['teacher_id']);
			$data['teachername']=$teacher[0]['firstname'].' '.$teacher[0]['lastname'];
			
		 
	   }
	   
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assignviewpdf',$data);

	
	
	}
	function getanswer($page,$school_id,$teacher_id)
	{
	
	 $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('observationplanmodel');
		$total_records = $this->observationplanmodel->getAnswerscount($school_id,$teacher_id);
		
			$status = $this->observationplanmodel->getAnswers($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Observation Plan</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Actions</td><td>PDF</td><td>Archived</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr  class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>';
				
				if($this->session->userdata('login_type')=='observer')
				{
				
				print '<td nowrap><a href="observerview/answerview/'.$val['teacher_id'].'">Add/View</a></td>
				<td nowrap><a target="_blank" href="comments/createobservationplan_pdf/'.$val['teacher_id'].'"><img src="'.SITEURLM.'images/pdf_icon.gif" /></a></td>
				<td nowrap><a href="observerview/getarchived/'.$val['teacher_id'].'">View</a></td>';
				}
				else if($this->session->userdata('login_type')=='user')
				{
				  print '<td nowrap><a href="distplans/answerview/'.$val['teacher_id'].'">Add/View</a></td>
				  <td nowrap><a target="_blank" href="comments/createobservationplan_pdf/'.$val['teacher_id'].'"><img src="'.SITEURLM.'images/pdf_icon.gif" /></a></td>
				<td nowrap><a href="distplans/getarchived/'.$val['teacher_id'].'">View</a></td>';
				}
				print '</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'answer');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Actions</td><td>Archived</td></tr>
			<tr><td valign='top' colspan='10'>No Data Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'answer');
						print $pagination;	
		}
		
	
	
	
	}
	function viewreportpdf($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			$this->load->Model('lessonplanmodel');
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationpointmodel');
			$data['points']=$this->observationpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->observationpointmodel->getReportPoints($report_id);
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->lickertpointmodel->getReportPoints($report_id);
		}
		if($data['getpoints']!=false)
		{
		  $data['getreportpoint']=$data['getpoints'];
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			
			}
		  
		  }
		  }
		
		}
		else
		{
		 $data['getreportpoints'][0]='';
		}

		$this->load->Model('report_disclaimermodel');
        $reportdes = $this->report_disclaimermodel->getallplans(4);
        if ($reportdes != false) {

            $data['dis'] = $reportdes[0]['tab'];
            $data['fontsize'] = $reportdes[0]['size'];
            $data['fontcolor'] = $reportdes[0]['color'];
        }

			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewreportpdf',$data);
	   }
	
	
	
	}
	function viewselfreportpdf($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('teacherselfmodel');
			
			
		
		$data['reportdata']=$this->teacherselfmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			
		if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationpointmodel');
			$data['points']=$this->observationpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->teacherselfmodel->obgetReportPoints($report_id);
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->teacherselfmodel->ligetReportPoints($report_id);
		}
		if($data['getpoints']!=false)
		{
		  $data['getreportpoint']=$data['getpoints'];
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			
			}
		  
		  }
		  }
		
		}
		else
		{
		 $data['getreportpoints'][0]='';
		}
		$this->load->Model('report_disclaimermodel');
                $reportdis = $this->report_disclaimermodel->getallplans(3);
                $dis = '';
                $fontsize = '';
                $fontcolor = '';
                if ($reportdis != false) {

                    $data['dis'] = $reportdis[0]['tab'];
                    $data['fontsize'] = $reportdis[0]['size'];
                    $data['fontcolor'] = $reportdis[0]['color'];
                }
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('teacherself/viewreportpdf',$data);
	   }
	
	
	
	}
	function viewreportformpdf($report_id,$sno)
	{
		
		$data['sno']=$sno;
	
		
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		
		$this->load->Model('lessonplanmodel');
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
			
		
			$this->load->Model('rubricscalemodel');
			$data['points']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			$data['getpoints']=$this->rubricscalemodel->getallrubricsubscalesformpoints($report_id);
			//echo '<pre>';
			//print_r($data['getpoints']);
			//exit;
			if($data['getpoints']!=false)
			{
			foreach($data['getpoints'] as $gval)
			{
			foreach($data['points'] as $key=>$pval)
			{
			if(!empty($gval['point_id']))
			{
			  if($gval['point_id']==$pval['sub_scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}
			else
			{
				if($gval['group_id']==$pval['scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}

			}	
			
			}
			}
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewreportformpdf',$data);
	   }
	
	
	
	}
	function viewproficiencypdf($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		
		
		
		$this->load->Model('lessonplanmodel');
		
		$data['reportdata']=$this->reportmodel->getproficiencyReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
			
		
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllGroupPoints();
			$data['getpoints']=$this->proficiencypointmodel->getReportPoints($report_id);
			//echo '<pre>';
			//print_r($data['points']);
			
		if($data['getpoints']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  }
		  $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		   
		}
		else
		{
		 $data['getreportpoints'][0]='';
		 $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		}
			
			
		
			
			//print_r($data['alldata']);
		   //exit;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewproficiencypdf',$data);
	   }
	
	
	
	}
	function memopdf($memo_id)
	{
		$this->load->Model('memorandummodel');
		$data['memorandums']=$this->memorandummodel->getmemoview($memo_id);
	    $data['memorandums']=$data['memorandums'][0];
		$data['memorandums']['teacher_name']=$this->session->userdata("teacher_name");
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacherreport/memopdf',$data);
	
	
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
//						$string.= "<br /><br />";
						//$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";
                                                $string.= "<div class='row-fluid'><div class='span12'><div  class='dataTables_paginate paging_bootstrap pagination'><ul>";
						// FOR ENABLING THE FIRST BUTTON
//						if ($first_btn && $cur_page > 1) {
//							$string.= "<li p='1' class='active'>First</li>";
//						} else if ($first_btn) {
//							$string.= "<li p='1' class='prev disabled'>First</li>";
//						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'><a href='javascript:void(0);'>Previous</a></li>";
						} else if ($previous_btn) {
							$string.= "<li class='prev disabled'><a href='javascript:void(0);'>Previous</a></li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' class='active '><a href='javascript:void(0);'>{$i}</a></li>";
							else
								$string.= "<li p='$i' class='active'><a href='javascript:void(0);'>{$i}</a></li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'><a href='javascript:void(0);'>Next</a></li>";
						} else if ($next_btn) {
							$string.= "<li class='next disabled'><a href='javascript:void(0);'>Next</a></li>";
						}

						// TO ENABLE THE END BUTTON
//						if ($last_btn && $cur_page < $no_of_paginations) {
//							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
//						} else if ($last_btn) {
//							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
//						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
//						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
                                                $string.= "</ul>" . "</div></div></div>";  // Content for pagination
	
					return $string;
	
	
	
	
	}
	
	function send_feedback()
	{
	  
	  $pdata=$_POST['selected'];
	  $remail=$pdata['email'];
	  $feedback=$pdata['feedback'];
	  $dist_id=$pdata['dist_id'];
	  $this->load->Model('districtmodel');
	  $status = $this->districtmodel->savefeedback();
	  if($status==true)
	  {
       $distdetails = $this->districtmodel->getdemodistrict($dist_id);
	   $cname=$distdetails[0]['country'];
	   $sname=$distdetails[0]['name'];
	   $distname=$this->session->userdata('demodistrictname');
	  /*  sending mail to user */
$subject ="Feedback From District:$distname";
	$message = "<html>
					<head>
					  <title>Feedback</title>
					</head>
					<body>
					  <p>Hi,</p>
					  <p>Feedback</p>
					  <p>
					  
					   Below Are The Login Details <br />
					   Country:$cname  <br />
					   State:$sname <br />
					   District name:$distname<br />
					   Feedback:$feedback <br />
					   
					  </p>
					  <p>Thank you,</p>
					  <p>TOR Team</p>
					</body>
					</html>";

	//Additional headers
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	$headers .= "Reply-To: $remail".PHP_EOL;
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	mail('info@ueisworkshop.com',$subject,$message,$headers);
	$data['status']=1;
	echo json_encode($data);
	exit;
	/*  End of Sennding Email To user*/
	  
	}
	else
	{
	
	$data['status']=0;
	echo json_encode($data);
	exit;
	
	}
	
	}
	
	function mediaupload()
	{
		 
		 
		 if($_FILES['mediafileToUpload']['size']>0)
		 {
		 $foto_upload = new Foto_upload;	

		 $json['size'] = $_POST['MEDIA_MAX_FILE_SIZE'];
		
		$json['img'] = '';
        
		$foto_upload->upload_dir = BANK_TIME.WORKSHOP_FILES."goalmedialibrary/";
		
		$foto_upload->foto_folder = BANK_TIME.WORKSHOP_FILES."goalmedialibrary/";
		$foto_upload->extensions = array(".jpg", ".gif", ".png",".pdf",".xls",".xlsx",".jpeg",".ppt",".pptx",".doc",".docx");
		$foto_upload->language = "en";
		$foto_upload->x_max_size = 480;
		$foto_upload->y_max_size = 360;
		$foto_upload->x_max_thumb_size = 120;
		$foto_upload->y_max_thumb_size = 120;

		$foto_upload->the_temp_file = $_FILES['mediafileToUpload']['tmp_name'];
		
		$foto_upload->the_file = $_FILES['mediafileToUpload']['name'];
		$foto_upload->http_error = $_FILES['mediafileToUpload']['error'];
		$foto_upload->rename_file = true; 

		if ($foto_upload->upload()) {
			//$foto_upload->process_image(false, true, true, 80);
			$json['img'] = $foto_upload->file_copy;
		} 

		$json['error'] = strip_tags($foto_upload->show_error_string());
		echo json_encode($json);
		}
		else if($_FILES['mediafileToUpload']['error']==2)
		{
		  $json['img']='';
		  $json['error']='Max File Size is 2 MB';
		 echo json_encode($json);
		}
		else
		{
		 $json['img']='NoFile';
		 $json['error']='';
		 echo json_encode($json);
		}
	
	}
	
	function media_upload()
	{
	
		if(isSet($_POST['school_id']))
		{			
		$school_id=$_POST['school_id'];
		$file_path=$_POST['fileupload'];
		$description=$_POST['descriptionmedia'];
		
		
		$this->load->Model('goalplanmodel');
		$data=$this->goalplanmodel->Insert_Media($school_id,$file_path,$description);           
		}
	
	}
	function getmedia($page,$school)
	{
	   $this->load->Model('goalplanmodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_goalmedia_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_goalmedia_school_id',$school);
		
		}
		
		
		$per_page = 12;
	   $total_records = $this->goalplanmodel->getgoalmediaCount();
		
			$status = $this->goalplanmodel->getgoalmedia($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Media</div><table class='tabcontent' cellspacing='0' border='0' cellpadding='0' id='gallery' style='width:535px;' >";
					
		
		
			$i=0;
			foreach($status as $val){
					
					$school_name=$val['school_name'];
					$username=$val['username'];
					$type=$val['type'];					
					$description=$val['description'];
					
					$text='<br /><b>School:'.$school_name.'<br />    Name:'.$username.'(Type:'.$type.')</b>';
					
					
					if($i%3==0 || $i==0)
					{
					 print '<tr>';
	
					}
				
			    $siteurl=SITEURLM;
				if($val['file_path']=='NoFile')
				{
				}
				else
				{
				$filename=explode('.',$val['file_path']);
				$file=$filename[1];
				}
				
			if($val['file_path']=='NoFile')
				{

				}
				else	if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmedialibrary/'.$val['file_path'].' height="75px" width="100px">';
			}
			else
			{
			  if (file_exists(WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg')) {
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmediathumb/'.$filename[0].'.jpeg >';
				} else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'goalmedialibrary/'.$val['file_path'].'[0] '.WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					else
					{
					 $commandtext='sudo -u unoconv /usr/bin/unoconv -f pdf -o '.WORKSHOP_FILES.'goalmediagenerated '.WORKSHOP_FILES.'goalmedialibrary/'.$val['file_path'];
					 exec($commandtext);
					 
					  $commandtexta='convert -thumbnail x100 '.WORKSHOP_FILES.'goalmediagenerated/'.$filename[0].'.pdf[0] '.WORKSHOP_FILES.'goalmediathumb/'.$filename[0].'.jpeg ';
					exec($commandtexta);
					 
					}
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'goalmediathumb/'.$filename[0].'.jpeg >';
				}
			  
			  
			
			}
			if($val['file_path']=='NoFile')
				{
					$newvar=$text;
				}
				else
				{
			$file_path=WORKSHOP_DISPLAY_FILES.'goalmedialibrary/'.$val['file_path'];
            $newvar='<a style="color:#ffffff;" target="_blank" href='.$file_path.'>'.$fileas.'</a>'.$text;
			}
			
				print '<td style="width: 120px; height: 100px;">'.$newvar.'<br /><b>Description:</b>'.$description.'</td>';
				
				
				$i++;
				if($i%3==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:535px;' ><tr ><td>No  Media Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
		}
	
	}
	function homework($verifycode=false,$message=false,$type=false)
	{
	  if($verifycode!=false)
	  {
	    if($message!=false)
		{
		
		$data['message']=$message;
		$data['type']=$type;
		}
		else
		{
		$data['message']='';
		
		}
		$this->load->Model('parentmodel');
	    $answerdata=$this->parentmodel->get_answer_homework($verifycode);
		if($answerdata==false)
		{
			echo 'Not a Valid User.';
		
		
		
		
		}
		else if($answerdata[0]['answer']!='')
		{
			 $homeworkdata=$this->parentmodel->get_homeworkbyid($answerdata[0]['homework_id']);
			 if($message==false)
			 {
				$data['message']='Already Submitted';
				$data['type']='green';
			 
			 }
			 $data['homeworkdata']=$homeworkdata[0];
			 $data['answerdata']=$answerdata;
			 $data['verifycode']=$verifycode;
			 $data['questions']=$this->parentmodel->get_questions();
		     $data['view_path']=$this->config->item('view_path');
		     $this->load->view('parent/answer',$data);		  	
		 
		
		}
		else
		{
		  $homeworkdata=$this->parentmodel->get_homeworkbyid($answerdata[0]['homework_id']);
		  $data['verifycode']=$verifycode;
		  $data['homeworkdata']=$homeworkdata[0];
		  $data['questions']=$this->parentmodel->get_questions();
		  $data['view_path']=$this->config->item('view_path');
		  $this->load->view('parent/answer',$data);		  	
		  
		}
	     
	  
	  }
	  else
	  {
	  
	    echo 'Not a Valid User.';
	  
	  }	
	
	}
	
	function answer_save()
	{
	 $this->load->Model('parentmodel');
	 $error=0;
	 $data['questions']=$this->parentmodel->get_questions();
	 foreach($data['questions'] as $question)
	 {
	    if($this->input->post($question['question_id'])=='')
		{
		  $error=1;
		  $message="All Fields Are Required.";	
		  $type="red";
		
		}
		 	
	 
	 
	 }
	 if($error==0)
		{
		   $status=$this->parentmodel->update_answer();
		   $message="Successfully Saved.";
		   $type="green";
		
		}
		
		
		  $this->homework($this->input->post('verifycode'),$message,$type);
		
		
	
	
	}
	
	function setbreadcrumb()
	{
	  $pdata=$this->input->post('pdata');	  
	  $this->session->set_userdata('breadcrumb',$pdata['breadcrumb']);
	  
	  $data['url']=$pdata['url'];
	 echo json_encode($data);
		exit;
	}
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
	
}	