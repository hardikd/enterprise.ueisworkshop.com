<?php
error_reporting(0);
$view_path="system/application/views/";
require_once($view_path.'inc/html2pdf/html2pdf.class.php');
class Report extends	MY_Auth {

function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false && $this->is_teacher()==false){
			//These functions are available only to schools - So redirect to the login page
			redirect("index");
		}
		if($this->session->userdata('TE')==0)
		{
			redirect("index");
		}
		
		$this->no_cache();

	}


	function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
		
	function index()
	{
	
	$data['idname']='implementation';
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->Model('teachermodel');
	  $this->load->Model('schoolmodel');
	  $this->load->Model('observermodel');
	  
	  $data['subjects']=$this->schoolmodel->getallsubjects();
	  $data['grades']=$this->schoolmodel->getallgrades();
	  if($this->session->userdata('login_type')=='user')
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict();

		if($data['school']!=false)
		{
			$this->load->Model('teachermodel');
			$data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
			$data['observers']=$this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
					

	   }
	   else
	   {
	    $data['teacher']=false;
		$data['observers']=false;		
	   }
				
		
	  }
	else	  
	{
	  $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	  $data['observers']=$this->observermodel->getobserverById($this->session->userdata("observer_id"));
	
	}
	  
	  $this->load->view('report/index',$data);
	
	}
	
	function create()
	{
		$data['idname']='implementation';
	  $this->load->Model('reportmodel');
	  //echo $this->input->post('form');exit;
	  if($this->input->post('form')=='formp')
		{
			$report_id=$this->reportmodel->createproficiency();
		}
		else
		{
			$report_id=$this->reportmodel->createReport();
		}	
	  if($report_id!=0)
	  {
	    $report_number=$this->reportmodel->getreportnumber();
		$this->session->set_userdata('report_number_new',$report_number);
		
		
		$this->session->set_userdata('newform',$this->input->post('form'));
		
		if($this->input->post('form')=='forma' || $this->input->post('form')=='formc')
		{
		
		$data['view_path']=$this->config->item('view_path');
		$this->session->set_userdata('new_report_id',$report_id);
	    if($this->input->post('form')=='forma')
		{
				$data['reportdata']=$this->reportmodel->getReportData($report_id);
				$data['reportdata']=$data['reportdata'][0];
				$this->load->Model('observationgroupmodel');
				$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->input->post('form')=='formc')
		{
			$data['reportdata']=$this->reportmodel->getReportData($report_id);
		    $data['reportdata']=$data['reportdata'][0];
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();
		}
        	
		if(!empty($data['groups']))
		{
			$group_id=$data['groups'][0]['group_id'];
			if(isset($data['groups'][1]['group_id']))
			{
				$next_group_id=$data['groups'][1]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}			
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
		if($this->input->post('form')=='forma')
		{
   $this->load->Model('observationpointmodel');
   $data['points']=$this->observationpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		}
		else if($this->input->post('form')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		}
		}
		else
		{
		   $group_id=0;
		   $next_group_id=0;
		
		}
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
	
		$this->load->view('report/observationpoints',$data);
		
	    }
		else if($this->input->post('form')=='formb')
		{
			$this->session->set_userdata('new_report_id',$report_id);
			$this->rubricscale($report_id);
			
			
		}
		else if($this->input->post('form')=='formp')
		{
			
			$this->session->set_userdata('new_report_id',$report_id);
			$this->createproficiency($report_id);
			
			
		}		
	  }
	  else
	  {

	    $this->index(); 
	  
	  }
	  
	
	
	}
	
	function rubricscale($report_id)
	{
		$data['idname']='implementation';
			$data['view_path']=$this->config->item('view_path');
			$this->load->Model('reportmodel');
	        $data['reportdata']=$this->reportmodel->getReportData($report_id);
	 	    $data['reportdata']=$data['reportdata'][0];
			$this->load->Model('rubricscalemodel');
		$data['groups']=$this->rubricscalemodel->getallrubricsubscales($this->session->userdata("district_id"));
			if(!empty($data['groups']))
			{
			$group_id=$data['groups'][0]['scale_id'];
			
			if(isset($data['groups'][1]['scale_id']))
			{
				$next_group_id=$data['groups'][1]['scale_id'];
				if($data['groups'][1]['sub_scale_id']!=null)
				{
					$next_sub_scale_id=$data['groups'][1]['sub_scale_id'];
				}
				else
				{
					$next_sub_scale_id=0;
				}	
			}
			else
			{
				$next_group_id=0;
				$next_sub_scale_id=0;

			}			
			$sub_scale_name='';
			$sub_scale_id=0;
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			if($val['sub_scale_name']!=null)
			{
				if($sub_scale_name=='')
				{
					$sub_scale_name=$val['sub_scale_name'];
					$data['sub_scale_name']=$val['sub_scale_name'];
				}
				if($sub_scale_id==0)
				{
					$sub_scale_id=$val['sub_scale_id'];
					$data['sub_scale_id']=$val['sub_scale_id'];
				}				
			}
			else
			{
			  $data['sub_scale_name']='';
			  $data['sub_scale_id']=0;
			}
			$data['scale_name']=$val['scale_name'];
			$data['description']=$val['description'];
		   }
		  }
		
			
		
		}
		else
		{
		   $group_id=0;
		   $next_group_id=0;
		   $next_sub_scale_id=0;
		
		}
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		$data['next_sub_scale_id']=$next_sub_scale_id;
		
		$this->load->view('report/rubricscalepoints',$data);
	
	}
	
	function rubricscalepoints($group_id,$sub_scale_id)
	{
	
	if($this->session->userdata('new_report_id'))
		{
		
		
		
		$this->load->Model('reportmodel');
		
		$data['reportdata']=$this->reportmodel->getReportData($this->session->userdata('new_report_id'));
		$data['reportdata']=$data['reportdata'][0];
		$data['view_path']=$this->config->item('view_path');
		$this->load->Model('rubricscalemodel');
		$data['groups']=$this->rubricscalemodel->getallrubricsubscales($this->session->userdata("district_id"));
			if(!empty($data['groups']))
			 {
		
			foreach($data['groups'] as $key=> $val)
		      {
		   // print_r($val);
			if($sub_scale_id!=0)
			{
			 if($val['sub_scale_id']==$sub_scale_id)
			 {
			    $data['sub_scale_name']=$val['sub_scale_name'];
				$data['scale_name']=$val['scale_name'];
			    $data['description']=$val['description'];
				$key++;
				if(isset($data['groups'][$key]['scale_id']))
			{
				$next_group_id=$data['groups'][$key]['scale_id'];
				if($data['groups'][$key]['sub_scale_id']!=null)
				{
					$next_sub_scale_id=$data['groups'][$key]['sub_scale_id'];
				}
				else				
				{
				 $next_sub_scale_id=0;
				}
			}
			else
			{
				$next_group_id=0;
				$next_sub_scale_id=0;
			}		
				
			 
			 
			 }
			
			
			
			}
			else
			{
				if($val['scale_id']==$group_id)
			 {
			    $data['scale_name']=$val['scale_name'];
			    $data['description']=$val['description'];
				$key++;
				if(isset($data['groups'][$key]['scale_id']))
			{
				$next_group_id=$data['groups'][$key]['scale_id'];
				if($data['groups'][$key]['sub_scale_id']!=null)
				{
					$next_sub_scale_id=$data['groups'][$key]['sub_scale_id'];
				}
				else				
				{
				 $next_sub_scale_id=0;
				}
			}
			else
			{
				$next_group_id=0;
				$next_sub_scale_id=0;
			}	
			 
			 
			 }
			
			
			}
		  }
		
			
		
		}
		else
		{
		   $group_id=0;
		   $next_group_id=0;
		   $next_sub_scale_id=0;
		   $sub_scale_id=0;
		
		}
		
		$data['points']=$this->rubricscalemodel->getallpoints($group_id,$sub_scale_id);
		
		//print_r($data['points']);
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		$data['next_sub_scale_id']=$next_sub_scale_id;
		$data['sub_scale_id']=$sub_scale_id;
		
		$this->load->view('report/rubricscalepoints',$data);
	
		
	
	
	 }
	  else
	  {
	    $this->index(); 
	  
	  }
	
	
	
	
	
	
	
	
	
	
	
	
	}
	function observationpoints($group_id)
	{
			$data['idname']='implementation';
		if($this->session->userdata('new_report_id'))
		{
		
		
		
		$this->load->Model('reportmodel');
		
		$data['reportdata']=$this->reportmodel->getReportData($this->session->userdata('new_report_id'));
		$data['reportdata']=$data['reportdata'][0];
		$data['view_path'] = $this->config->item('view_path');
		if($this->session->userdata('newform')=='forma')
		{
		 $this->load->Model('observationgroupmodel');
		 $data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('newform')=='formc')
		{		
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();
		
		}
		if($data['groups']!=false)
		{
		  foreach($data['groups'] as $key=>$val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
			$key++;
			if(isset($data['groups'][$key]['group_id']))
			{
				$next_group_id=$data['groups'][$key]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}	
			
		   }
		  }
		
		}
		if($this->session->userdata('newform')=='forma')
		{
			$this->load->Model('observationpointmodel');
		$data['points']=$this->observationpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		$data['getpoints']=$this->observationpointmodel->getReportPoints($this->session->userdata('new_report_id'));
		}
		else if($this->session->userdata('newform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
	$data['points']=$this->lickertpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		$data['getpoints']=$this->lickertpointmodel->getReportPoints($this->session->userdata('new_report_id'));
		
		}
		//echo "<pre>";
		//print_r($data['getpoints']);
		//exit;
		if($data['getpoints']!=false && $data['points']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
				
				$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
				
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getreportpoints'][0]='';
		}
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/observationpoints',$data);
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	
	}
	
	function save()
	{
	$data['idname']='implementation';
		
	  if($this->session->userdata('new_report_id'))
		{
	
	  $group_id=$this->input->post('group_id');
	  if(isset($group_id))
	  {
	  $next_group_id=$this->input->post('next_group_id');
	  if($this->session->userdata('newform')=='forma')
		{
		  $this->load->Model('observationpointmodel');
		  $status=$this->observationpointmodel->savereport();
	  }
	  else if($this->session->userdata('newform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
		  $status=$this->lickertpointmodel->savereport();
		}
	  
	  if($status==true)
	  {
	    if($next_group_id==0)
		{
		 $this->finish();
		}
		else
		{
			//echo 'test';exit;
			$this->observationpoints($next_group_id);
		}	
	  
	  }
	  else
	  {
	     
		 $this->observationpoints($group_id);
	  
	  
	  }
	
	 }
	 }
	 else
	  {
	    $this->index(); 
	  
	  }
	  
	}
	function rubricsave()
	{
	  if($this->session->userdata('new_report_id'))
		{
	
	   $group_id=$this->input->post('group_id');
	  
	  $sub_scale_id=$this->input->post('sub_scale_id');
	  if(isset($group_id))
	  {
	  $next_group_id=$this->input->post('next_group_id');
	  $next_sub_scale_id=$this->input->post('next_sub_scale_id');
	  
		  $this->load->Model('rubricscalemodel');
		  $status=$this->rubricscalemodel->savereport();
	  
	  
	  if($status==true)
	  {
	    if($next_group_id==0)
		{
		 $this->finish();
		}
		else
		{
			$this->rubricscalepoints($next_group_id,$next_sub_scale_id);
		}	
	  
	  }
	  else
	  {
	     
		 $this->rubricscalepoints($group_id,$sub_scale_id);
	  
	  
	  }
	
	 }
	 }
	 else
	  {
	    $this->index(); 
	  
	  }
	  
	}
	function summative()
	{
		$data['idname']='implementation';
	    $this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
		$data['statuses']=$this->statusmodel->getstatusbydistrict();
		$data['scores']=$this->scoremodel->getscorebydistrict();
	    
		if($this->session->userdata('login_type')=='user')
		{
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
	    $data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/summative',$data);
	}
	
	function getsummativereport()
	{
		    $data['idname']='implementation';
	  $this->load->Model('reportmodel');
	    if($this->input->post('submit')=='Get Report')
		{
		if($this->input->post('teacher_id'))
		{
			
		$this->session->set_userdata('summative_teacher_id',$this->input->post('teacher_id'));
		}
		if($this->input->post('status'))
		{
			
		$this->session->set_userdata('summative_status',$this->input->post('status'));
		}
		else if($this->input->post('status')=='')
		{
		  $this->session->set_userdata('summative_status','');
		
		}
		
		if($this->input->post('score'))
		{
			
		$this->session->set_userdata('summative_score',$this->input->post('score'));
		}
		else if($this->input->post('score')=='')
		{
		  $this->session->set_userdata('summative_score','');
		
		}
		if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('summative_school_id',$this->input->post('school'));
		}
		
		
		}
		}
		//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getsummativereport/';
		$config['total_rows'] = $this->reportmodel->getsummativeReportCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
			$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		$this->load->Model('teachermodel');
		$data['reports'] = $this->reportmodel->getsummativeReportByTeacher($start,$config['per_page']);
		$data['teacher_id']=$this->session->userdata('summative_teacher_id');
		$data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("summative_school_id");
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		
		$data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata("summative_school_id"));
	   }
	   else
	   {
	    $data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
		$data['statuses']=$this->statusmodel->getstatusbydistrict();
		$data['scores']=$this->scoremodel->getscorebydistrict();
		$data['status']=$this->session->userdata('summative_status');
		$data['score']=$this->session->userdata('summative_score');
		$this->load->view('report/summative',$data);
		
	
	
	
	}
	function teacherreport()
	{
		 $data['idname']='implementation';
	    $this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		if($this->session->userdata('login_type')=='user')
		{
		
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		$this->load->Model('teachermodel');
	   $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   }
	   else
	   {
		   
	    $data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		$data['subjects']=$this->schoolmodel->getallsubjects();
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/teacherreport',$data);
	}
	
	function getteacherreport()
	{
		 $data['idname']='implementation';
	    if($this->input->post('teacher_id'))
		{
			
		$this->session->set_userdata('report_teacher_id',$this->input->post('teacher_id'));
		}
		if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('school_id',$this->input->post('school'));
		}
		
		
		}
		$this->load->Model('reportmodel');
		if($this->session->userdata('report_teacher_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('month'))
		{
		$this->session->set_userdata('reportmonth',$this->input->post('month'));
		}
		if($this->input->post('year'))
		{
		$this->session->set_userdata('reportyear',$this->input->post('year'));
		}
		if($this->input->post('subject'))
		{
		$this->session->set_userdata('report_query_subject',$this->input->post('subject'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}
		
		if($this->session->userdata('submitbutton')=='Retrieve Full Report')
		{
		
		
			//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getteacherreport/';
		$config['total_rows'] = $this->reportmodel->getReportTeacherCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
			$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		$this->load->Model('teachermodel');
		$data['reports'] = $this->reportmodel->getReportByTeacher($start,$config['per_page']);
                $data['fetchreports'] = true;
		$data['teacher_id']=$this->session->userdata('report_teacher_id');
		$data['view_path']=$this->config->item('view_path');
		$this->load->Model('schoolmodel');
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("school_id");
		
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		
		$data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		
	   }
	   else
	   {
	    
		$data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		$data['subjects']=$this->schoolmodel->getallsubjects();
		$this->load->view('report/teacherreport',$data);
		
		}
		else if($this->input->post('submit')=='Retrieve Qualitative Data Report')
		{
                    
                    $data['fetchreports'] = true;
		  $this->session->set_userdata('report_criteria_id',$this->input->post('teacher_id'));
		  $this->session->set_userdata('report_criteria','teacher');
		  $this->load->Model('teachermodel');
		  $teacher_name=$this->teachermodel->getteacherById($this->input->post('teacher_id'));
		  $name=$teacher_name[0]['firstname'].' '.$teacher_name[0]['lastname'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
                  //echo $this->session->userdata('reportform');exit;
		  if($this->session->userdata('reportform')!='formb')
		  {
			$this->teacherreport_qualitative(false,'teacher');
		  }
		  else
		  {
			$this->teacherreport_qualitativeform(false,false,'teacher');
		  }	
		}
		else if($this->input->post('submit')=='Retrieve Sectional Report')
		{
                    $data['fetchreports'] = true;
		  $this->session->set_userdata('report_criteria_id',$this->input->post('teacher_id'));
		  $this->session->set_userdata('report_criteria','teacher');
		  $this->load->Model('teachermodel');
		  $teacher_name=$this->teachermodel->getteacherById($this->input->post('teacher_id'));
		  $name=$teacher_name[0]['firstname'].' '.$teacher_name[0]['lastname'];
		  $this->session->set_userdata('report_name',$name);
		  
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->teacherreport_sectional(false,'teacher');
		  }
		  else
		  {
			$this->teacherreport_sectionalform(false,false,'teacher');
		  }	
		 
		   
		
		}
		}
		else
		{
          $this->teacherreport();
		  

		}	
		
		
	}
	function qualitativereport($group_id=false,$report)
	{
	   $data['idname']='tools';

		$this->load->Model('observationpointmodel');
		$data['view_path']=$this->config->item('view_path');
	  if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();

		}	
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
		foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
			$data['qualitative']=$this->observationpointmodel->getAllqualitative($group_id,$report);
		}
		else
		{
		   
		   $group_id=0;
		   
		
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('report/qualitative',$data);
	
	
	}
	
	
	function teacherreport_qualitative($group_id=false,$report)
	{
	  $data['idname']='implementation';
		$this->load->Model('observationpointmodel');
		$data['view_path']=$this->config->item('view_path');
	  if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();

		}	
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
		foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
			$data['qualitative']=$this->observationpointmodel->getAllqualitative($group_id,$report);
		}
		else
		{
		   
		   $group_id=0;
		   
		
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('report/teacherreport_qualitative',$data);
	
	
	}
	
	
	
	
	
	
	function qualitativeleadershipreport($group_id=false,$report)
	{
	  
		
		$this->load->Model('lubricpointmodel');
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('lubricgroupmodel');

$data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
			
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
			$data['qualitative']=$this->lubricpointmodel->getAllqualitative($group_id,$report);
		}
		else
		{
		   $group_id=0;
		   
		
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('report/leadershipqualitative',$data);
	
	
	}
	
	function sectionalform($group_id=false,$sub_id=false,$report)
	{
	
		$data['idname']='tools';
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			
		
		if(!empty($data['groups']))
		{
			
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['scale_id'];
				
				if($sub_id)
				{
				
				}
				else
				{
				
				if(!empty($data['groups'][0]['sub_scale_id']))
				{
				 $sub_id=$data['groups'][0]['sub_scale_id'];
				
				}
				
				}
			}
					
			$data['group_name']='';
			$data['sub_scale_name']='';
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			   
			  if($data['group_name']=='')
			  {
				 
				 $data['group_name']=$val['scale_name'];
				
			  }
			
			
			
		   }
		   
		   if($sub_id!=0)
		   {
		     if($val['sub_scale_id']==$sub_id)
			{
				if($data['sub_scale_name']=='')
			  {
				$data['sub_scale_name']=$val['sub_scale_name'];
			}	
			
		   }
		   
			
			 
		  }
		  
			
		}
		$this->load->Model('observationpointmodel');
		$data['reports']=$this->observationpointmodel->getAllReport($report);
		if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
		$data['sectional']=$this->rubricscalemodel->getAllsectional($group_id,$sub_id,$report);
		}
		if(!isset($data['sectional']))
		{
			 $data['sectional']=false;
		}
		
		
		$this->load->view('report/sectionalform',$data);
	
	}

function teacherreport_sectionalform($group_id=false,$sub_id=false,$report)
	{

		$data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			
		
		if(!empty($data['groups']))
		{
			
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['scale_id'];
				
				if($sub_id)
				{
				
				}
				else
				{
				
				if(!empty($data['groups'][0]['sub_scale_id']))
				{
				 $sub_id=$data['groups'][0]['sub_scale_id'];
				
				}
				
				}
			}
					
			$data['group_name']='';
			$data['sub_scale_name']='';
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			   
			  if($data['group_name']=='')
			  {
				 
				 $data['group_name']=$val['scale_name'];
				
			  }
			
			
			
		   }
		   
		   if($sub_id!=0)
		   {
		     if($val['sub_scale_id']==$sub_id)
			{
				if($data['sub_scale_name']=='')
			  {
				$data['sub_scale_name']=$val['sub_scale_name'];
			}	
			
		   }
		   
			
			 
		  }
		  
			
		}
		$this->load->Model('observationpointmodel');
		$data['reports']=$this->observationpointmodel->getAllReport($report);
		if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
		$data['sectional']=$this->rubricscalemodel->getAllsectional($group_id,$sub_id,$report);
		}
		if(!isset($data['sectional']))
		{
			 $data['sectional']=false;
		}
		
		
		$this->load->view('report/teacherreport_sectionalform',$data);
	
	}



	
	function sectionalreport($group_id=false,$report)
	{
		$data['idname']='tools';
		$this->load->Model('observationpointmodel');
		$data['view_path']=$this->config->item('view_path');
	    if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('reportform')=='formc')
		{		
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();
			

		}
		else if($this->session->userdata('reportform')=='formp')
		{
		$this->load->Model('proficiencygroupmodel');
		$data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
			
		}	
		
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			
		   }
		  }
			
			
			 if($this->session->userdata('reportform')=='forma')
				{
					$this->load->Model('observationpointmodel');
					$data['points']=$this->observationpointmodel->getAllPoints($group_id,$this->session->userdata                    ('district_id'));
				}
		else  if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllPoints($group_id,$this->session->userdata('district_id'));
		}
		else  if($this->session->userdata('reportform')=='formp')
		{
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllPoints($group_id,$this->session->userdata('district_id'));
		}
			if($this->session->userdata('reportform')!='formp')
			{
				$data['reports']=$this->observationpointmodel->getAllReport($report);
				$data['sectional']=$this->observationpointmodel->getAllsectional($group_id,$report);
			}
			else
			{
			  $data['reports']=$this->proficiencypointmodel->getAllReportpro($report);
			  $data['sectional']=$this->proficiencypointmodel->getAllsectionalpro($group_id,$report);
			}
			
			if($data['sectional']!=false)
			{
				$data['countsectional']=count($data['sectional']);
			
			}
			else
			{
			  $data['countsectional']=0;
			}
			if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
			
			
		}
		
		//print_r($data);
		if(isset($data['points']))
		{ 
		  $data['group_set']=0;
		  
		  if($data['points']!='')
		  {
		  foreach($data['points'] as $val)
		  {
		    if($data['group_set']!=1)
			{
			if($val['group_type_id']==1 || $val['group_type_id']==2 )
			{
			 $data['group_set']=1;
			 $data['point_set']=$val['point_id'];
			
			}
            }		  
		  }
		  }
		  
		  
		
		
		}
		if(!isset($data['points']))
		{
			 $data['points']=false;
		}
		
		
		//echo '<pre>';
		//print_r($data);
		//exit;
		$this->load->view('report/sectional',$data);
	
	
	
	}
	
	
		
	function teacherreport_sectional($group_id=false,$report)
	{

		$data['idname']='implementation';
		$this->load->Model('observationpointmodel');
		$data['view_path']=$this->config->item('view_path');
	    if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('reportform')=='formc')
		{		
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();
			

		}
		else if($this->session->userdata('reportform')=='formp')
		{
		$this->load->Model('proficiencygroupmodel');
		$data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
			
		}	
		
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			
		   }
		  }
			
			
			 if($this->session->userdata('reportform')=='forma')
				{
					$this->load->Model('observationpointmodel');
					$data['points']=$this->observationpointmodel->getAllPoints($group_id,$this->session->userdata                    ('district_id'));
				}
		else  if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllPoints($group_id,$this->session->userdata('district_id'));
		}
		else  if($this->session->userdata('reportform')=='formp')
		{
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllPoints($group_id,$this->session->userdata('district_id'));
		}
			if($this->session->userdata('reportform')!='formp')
			{
				$data['reports']=$this->observationpointmodel->getAllReport($report);
				$data['sectional']=$this->observationpointmodel->getAllsectional($group_id,$report);
			}
			else
			{
			  $data['reports']=$this->proficiencypointmodel->getAllReportpro($report);
			  $data['sectional']=$this->proficiencypointmodel->getAllsectionalpro($group_id,$report);
			}
			
			if($data['sectional']!=false)
			{
				$data['countsectional']=count($data['sectional']);
			
			}
			else
			{
			  $data['countsectional']=0;
			}
			if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
			
			
		}
		
		//print_r($data);
		if(isset($data['points']))
		{ 
		  $data['group_set']=0;
		  
		  if($data['points']!='')
		  {
		  foreach($data['points'] as $val)
		  {
		    if($data['group_set']!=1)
			{
			if($val['group_type_id']==1 || $val['group_type_id']==2 )
			{
			 $data['group_set']=1;
			 $data['point_set']=$val['point_id'];
			
			}
            }		  
		  }
		  }
		  
		  
		
		
		}
		if(!isset($data['points']))
		{
			 $data['points']=false;
		}
		
		
		//echo '<pre>';
		//print_r($data);
		//exit;
		$this->load->view('report/teacherreport_sectional',$data);
	
	
	
	}
	
	
	
	
	
	function sectionalleadershipreport($group_id=false,$report)
	{
		$this->load->Model('lubricpointmodel');
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('lubricgroupmodel');
			$data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
			
		
		if(!empty($data['groups']))
		{
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['group_id'];
				
			}
					
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			
		   }
		  }
			
		 
			$this->load->Model('lubricpointmodel');
$data['points']=$this->lubricpointmodel->getAllPoints($group_id,$this->session->userdata('district_id'));
		
			$data['reports']=$this->lubricpointmodel->getAllReport($report);
			$data['sectional']=$this->lubricpointmodel->getAllsectional($group_id,$report);
			if($data['sectional']!=false)
			{
				$data['countsectional']=count($data['sectional']);
			
			}
			else
			{
			  $data['countsectional']=0;
			}
			if($data['reports']!=false)
			{
				$data['countreport']=count($data['reports']);
			
			}
			else
			{
			  $data['countreport']=0;
			}
			
			
		}
		
		//print_r($data);
		if(isset($data['points']))
		{ 
		  $data['group_set']=0;
		  
		  if($data['points']!='')
		  {
		  foreach($data['points'] as $val)
		  {
		    if($data['group_set']!=1)
			{
			if($val['group_type_id']==1)
			{
			 $data['group_set']=1;
			 $data['point_set']=$val['point_id'];
			
			}
            }		  
		  }
		  }
		  
		  
		
		
		}
		if(!isset($data['points']))
		{
			 $data['points']=false;
		}
		
		
		
		$this->load->view('report/leadershipsectional',$data);
	
	
	
	}
	
	function qualitativereportform($group_id=false,$sub_id=false,$report)
	{
	
		$data['idname']='tools';
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
            
			
		if(!empty($data['groups']))
		{
			
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['scale_id'];
				
				if($sub_id)
				{
				
				}
				else
				{
				
				if(!empty($data['groups'][0]['sub_scale_id']))
				{
				 $sub_id=$data['groups'][0]['sub_scale_id'];
				
				}
				
				}
			}
					
			$data['group_name']='';
			$data['sub_scale_name']='';
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			   
			  if($data['group_name']=='')
			  {
				 
				 $data['group_name']=$val['scale_name'];
				
			  }
			
			
			
		   }
		   
		   if($sub_id!=0)
		   {
		     if($val['sub_scale_id']==$sub_id)
			{
				if($data['sub_scale_name']=='')
			  {
				$data['sub_scale_name']=$val['sub_scale_name'];
			}	
			
		   }
		   
			
			 
		  }
		  
			
		}
		$data['qualitative']=$this->rubricscalemodel->getAllqualitative($group_id,$sub_id,$report);
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('report/qualitativeform',$data);
	
	
	}
function teacherreport_qualitativeform($group_id=false,$sub_id=false,$report)
	{
	
		$data['idname']='implementation';
		$data['view_path']=$this->config->item('view_path');
	  
			$this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
            
			
		if(!empty($data['groups']))
		{
			
			if($group_id)
			{
				
			}
            else
			{
				$group_id=$data['groups'][0]['scale_id'];
				
				if($sub_id)
				{
				
				}
				else
				{
				
				if(!empty($data['groups'][0]['sub_scale_id']))
				{
				 $sub_id=$data['groups'][0]['sub_scale_id'];
				
				}
				
				}
			}
					
			$data['group_name']='';
			$data['sub_scale_name']='';
			foreach($data['groups'] as $val)
		  {
		    if($val['scale_id']==$group_id)
			{
			   
			  if($data['group_name']=='')
			  {
				 
				 $data['group_name']=$val['scale_name'];
				
			  }
			
			
			
		   }
		   
		   if($sub_id!=0)
		   {
		     if($val['sub_scale_id']==$sub_id)
			{
				if($data['sub_scale_name']=='')
			  {
				$data['sub_scale_name']=$val['sub_scale_name'];
			}	
			
		   }
		   
			
			 
		  }
		  
			
		}
		$data['qualitative']=$this->rubricscalemodel->getAllqualitative($group_id,$sub_id,$report);
		}
		$data['group_id']=$group_id;
		
		if(!isset($data['qualitative']))
		{
			 $data['qualitative']=false;
		}
		$this->load->view('report/teacherreport_qualitativeform',$data);
	
	
	}	
	
	
	
	function observerreport()
	{	    				
		$data['idname']='tools';
		$this->load->Model('observermodel');
		if($this->session->userdata('login_type')=='user')
		{
			$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
			$data['observers']=$this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
		}
		else
	   {
	    $data['observers']=false; 
	   
	   }		
		}
		else	  
		{
			$data['observers']=$this->observermodel->getobserverById($this->session->userdata("observer_id"));
	
		}
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/observerreport',$data);
	}
	
	function getobserverreport()
	{
	 $data['idname']='tools';	
		if($this->input->post('observer_id'))
		{
			
		$this->session->set_userdata('report_observer_id',$this->input->post('observer_id'));
		}
		if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('school_id',$this->input->post('school'));
		}
		
		
		}
	   $this->load->Model('reportmodel');
		
		if($this->session->userdata('report_observer_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Retrieve Full Report')
		{
			
			//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getobserverreport/';
		$config['total_rows'] = $this->reportmodel->getReportobserverCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('observermodel');
		$data['reports'] = $this->reportmodel->getReportByobserver($start,$config['per_page']);
		$data['observer_id']=$this->session->userdata('report_observer_id');
		$data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("school_id");
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		
		$data['observers']=$this->observermodel->getobserverByschoolId($this->session->userdata("school_id"));
	   }
	   else
	   {
	    $data['observers']=false; 
	   
	   }
	   }
	   else
	   {
		$data['observers'] = $this->observermodel->getobserverByschoolId($this->session->userdata("school_id"));
	   }	
		$this->load->view('report/observerreport',$data);
		
		}
		else if($this->input->post('submit')=='Retrieve Qualitative Data Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  
		  if($this->session->userdata('reportform')!='formb')
		  {
			$this->qualitativereport(false,'observer');
		  }
		  else
		  {
			$this->qualitativereportform(false,false,'observer');
		  }	
		  
		}
		else if($this->input->post('submit')=='Retrieve Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->sectionalreport(false,'observer');
		  }
		  else
		  {
			$this->sectionalform(false,false,'observer');
		  }	
		  
		   
		
		}
		}
		else
		{
           $this->observerreport();

        }		
		
		
	}
	
	function leadershipreport()
	{	    				
		
		$this->load->Model('observermodel');
		if($this->session->userdata('login_type')=='user')
		{
			$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
			$data['observers']=$this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
		}
		else
	   {
	    $data['observers']=false; 
	   
	   }		
		}
		else	  
		{
			$data['observers']=$this->observermodel->getobserverById($this->session->userdata("observer_id"));
	
		}
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/leadershipreport',$data);
	}
	
	function getleadershipreport()
	{
		$this->session->unset_userdata('reportform');
		if($this->input->post('observer_id'))
		{
			
		$this->session->set_userdata('report_observer_id',$this->input->post('observer_id'));
		}
		if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('school_id',$this->input->post('school'));
		}
		
		
		}
	   $this->load->Model('reportmodel');
		
		if($this->session->userdata('report_observer_id'))
		{
		
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Get Reports')
		{
			
			//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getleadershipreport/';
		$config['total_rows'] = $this->reportmodel->getleadershipReportobserverCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('observermodel');
		$data['reports'] = $this->reportmodel->getleadershipReportByobserver($start,$config['per_page']);
		$data['observer_id']=$this->session->userdata('report_observer_id');
		$data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("school_id");
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		
		$data['observers']=$this->observermodel->getobserverByschoolId($this->session->userdata("school_id"));
	   }
	   else
	   {
	    $data['observers']=false; 
	   
	   }
	   }
	   else
	   {
		$data['observers'] = $this->observermodel->getobserverByschoolId($this->session->userdata("school_id"));
	   }	
		$this->load->view('report/leadershipreport',$data);
		
		}
		else if($this->input->post('submit')=='Get Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  
		  
			$this->qualitativeleadershipreport(false,'observer');
		  	
		  
		}
		else if($this->input->post('submit')=='Get Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  
			 $this->sectionalleadershipreport(false,'observer');
		  	
		  
		   
		
		}
		}
		else
		{
           $this->leadershipreport();

        }		
		
		
	}
	
	function getproficiencyreport()
	{
		$this->session->unset_userdata('reportform');
		if($this->input->post('observer_id'))
		{
			
		$this->session->set_userdata('report_observer_id',$this->input->post('observer_id'));
		}
		if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('school_id',$this->input->post('school'));
		}
		
		
		}
	   $this->load->Model('reportmodel');
		
		if($this->session->userdata('report_observer_id'))
		{
		
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Get Reports')
		{
			
			//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getproficiencyreport/';
		$config['total_rows'] = $this->reportmodel->getproficiencyReportobserverCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('observermodel');
		$data['reports'] = $this->reportmodel->getproficiencyReportByobserver($start,$config['per_page']);
		$data['observer_id']=$this->session->userdata('report_observer_id');
		$data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("school_id");
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		
		$data['observers']=$this->observermodel->getobserverByschoolId($this->session->userdata("school_id"));
	   }
	   else
	   {
	    $data['observers']=false; 
	   
	   }
	   }
	   else
	   {
		$data['observers'] = $this->observermodel->getobserverByschoolId($this->session->userdata("school_id"));
	   }	
		$this->load->view('report/proficiencyreport',$data);
		
		}
		else if($this->input->post('submit')=='Get Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  
		  
			$this->qualitativeleadershipreport(false,'observer');
		  	
		  
		}
		else if($this->input->post('submit')=='Get Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('observer_id'));
		  $this->session->set_userdata('report_criteria','observer');
		  $this->load->Model('observermodel');
		  $observer_name=$this->observermodel->getobserverById($this->input->post('observer_id'));
		  $name=$observer_name[0]['observer_name'];
		  $this->session->set_userdata('report_name',$name);
		  
			 $this->sectionalleadershipreport(false,'observer');
		  	
		  
		   
		
		}
		}
		else
		{
           $this->leadershipreport();

        }		
		
		
	}
	function gradereport()
	{
		 
	    $data['idname']='tools';
		if($this->session->userdata('login_type')=='user')
		{
			$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
			
		}
		
		
		$this->load->Model('schoolmodel');
		$data['grades'] = $this->schoolmodel->getallgrades();
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/gradereport',$data);
	}
	
	function getgradereport()
	{
	     $data['idname']='tools';
		if($this->input->post('grade_id'))
		{
			
		$this->session->set_userdata('report_grade_id',$this->input->post('grade_id'));
		}
	      
		  if($this->session->userdata('login_type')=='user')
		{
			if($this->input->post('school'))
		{
			
		$this->session->set_userdata('school_id',$this->input->post('school'));
		}
		
		
		}
		  $this->load->Model('reportmodel');
		
		if($this->session->userdata('report_grade_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Retrieve Full Report')
		{
		
		
		
		//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getgradereport/';
		$config['total_rows'] = $this->reportmodel->getReportgradeCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		$this->load->Model('schoolmodel');
		$data['reports'] = $this->reportmodel->getReportBygrade($start,$config['per_page']);
		$data['grade_id']=$this->session->userdata('report_grade_id');
		$data['view_path']=$this->config->item('view_path');
		
		if($this->session->userdata('login_type')=='user')
		{
		$data['school_id']=$this->session->userdata("school_id");
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		
	   }
		$data['grades'] = $this->schoolmodel->getallgrades();
		$this->load->view('report/gradereport',$data);
		}
		else if($this->input->post('submit')=='Retrieve Qualitative Data Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('grade_id'));
		  $this->session->set_userdata('report_criteria','grade');
		  $this->load->Model('schoolmodel');
		  $grade_name=$this->schoolmodel->getGradeById($this->input->post('grade_id'));
		  $name=$grade_name[0]['grade_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->qualitativereport(false,'grade');
		  }
		  else
		  {
			$this->qualitativereportform(false,false,'grade');
		  }	
		  
		 
		}
		else if($this->input->post('submit')=='Retrieve Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('grade_id'));
		  $this->session->set_userdata('report_criteria','grade');
		  $this->load->Model('schoolmodel');
		  $grade_name=$this->schoolmodel->getGradeById($this->input->post('grade_id'));
		  $name=$grade_name[0]['grade_name'];
		  $this->session->set_userdata('report_name',$name);
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->sectionalreport(false,'grade');
		  }
		  else
		  {
			$this->sectionalform(false,false,'grade');
		  }
		   
		
		}
		}
		else
		{
		  $this->gradereport();
		
		}
	}
	
	function schoolreport()
	{
	    
		if($this->session->userdata('login_type')=='user')
		{
			$this->load->Model('schoolmodel');
			$data['schools']=$this->schoolmodel->getschoolbydistrict();
			
		}
		else
		{
			$this->load->Model('schoolmodel');
			$data['schools'] = $this->schoolmodel->getschoolById($this->session->userdata("school_id"));
        }		
		
		
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/schoolreport',$data);
	}
	
	function getschoolreport()
	{
	    if($this->input->post('school_id'))
		{
			
		 $this->session->set_userdata('report_school_id',$this->input->post('school_id'));
		 $this->session->set_userdata('school_id',$this->input->post('school_id'));
		}
	      $this->load->Model('reportmodel');
		
		if($this->session->userdata('report_school_id'))
		{
		if($this->input->post('form'))
		{
		$this->session->set_userdata('reportform',$this->input->post('form'));
		}
		if($this->input->post('formtype'))
		{
		$this->session->set_userdata('formtype',$this->input->post('formtype'));
		}
		if($this->input->post('submit'))
		{
			$this->session->set_userdata('submitbutton',$this->input->post('submit'));
		}	
		if($this->session->userdata('submitbutton')=='Get Reports')
		{
		
			
		
		//Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/report/getschoolreport/';
		$config['total_rows'] = $this->reportmodel->getReportschoolCount();
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		$data['sno']=1;
		}
		else
		{
		  $data['sno']=$start+1;
		}
		//Pagination Code End
		
		
		 $this->load->Model('schoolmodel');
		$data['reports'] = $this->reportmodel->getReportByschool($start,$config['per_page']);
		$data['school_id']=$this->session->userdata('report_school_id');
		$data['view_path']=$this->config->item('view_path');
		if($this->session->userdata('login_type')=='user')
		{
			
			$data['schools']=$this->schoolmodel->getschoolbydistrict();
			
		}
		else
		{
		$data['schools'] = $this->schoolmodel->getschoolById($this->session->userdata("school_id"));
		}
		$this->load->view('report/schoolreport',$data);
		}
		else if($this->input->post('submit')=='Get Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('school_id'));
		  $this->session->set_userdata('report_criteria','school');
		  $this->load->Model('schoolmodel');
		  $school_name=$this->schoolmodel->getschoolById($this->input->post('school_id'));
		  $name=$school_name[0]['school_name'];
		  $this->session->set_userdata('report_name',$name);
		  $this->session->set_userdata('report_from',$this->input->post('from'));
		  $this->session->set_userdata('report_to',$this->input->post('to'));
		  if($this->session->userdata('reportform')!='formb')
		  {
			 $this->qualitativereport(false,'school');
		  }
		  else
		  {
			$this->qualitativereportform(false,false,'school');
		  }
		 
		}
		else if($this->input->post('submit')=='Get Sectional Report')
		{
		  $this->session->set_userdata('report_criteria_id',$this->input->post('school_id'));
		  $this->session->set_userdata('report_criteria','school');
		  $this->load->Model('schoolmodel');
		  $school_name=$this->schoolmodel->getschoolById($this->input->post('school_id'));
		  $name=$school_name[0]['school_name'];
		  $this->session->set_userdata('report_name',$name);
		   if($this->session->userdata('reportform')!='formb')
		  {
			 $this->sectionalreport(false,'school');
		  }
		  else
		  {
			$this->sectionalform(false,false,'school');
		  }
		   
		
		}
		
		}
		else
		{
			$this->schoolreport();
		}
	}
	function viewreport($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			$this->load->Model('lessonplanmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
		
			
		if($this->session->userdata('reportform')=='forma')
		{
			$this->load->Model('observationpointmodel');
			$data['points']=$this->observationpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->observationpointmodel->getReportPoints($report_id);
		}
		else if($this->session->userdata('reportform')=='formc')
		{
			$this->load->Model('lickertpointmodel');
			$data['points']=$this->lickertpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->lickertpointmodel->getReportPoints($report_id);
		}
		if($data['getpoints']!=false)
		{
		  $data['getreportpoint']=$data['getpoints'];
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
			$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			
			}
		  
		  }
		  }
		
		}
		else
		{
		 $data['getreportpoints'][0]='';
		}
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewreport',$data);
	   }
	
	
	
	}
	
	function viewleadership($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getleadershipReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			
		
			$this->load->Model('lubricpointmodel');
			$data['points']=$this->lubricpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->lubricpointmodel->getReportPoints($report_id);
			//echo '<pre>';
			//print_r($data['points']);
			
		if($data['getpoints']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
				
				$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
				
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    
				$data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    
				$data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
						  
			  }
			}
		  
		  }
		  }
		  $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		   
		}
		else
		{
		 $data['getreportpoints'][0]='';
		 $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		}
			
			
			//self points
		
		$data['getselfpoints']=$this->lubricpointmodel->getReportselfPointsbyobserverid($data['reportdata']['observer_id']);
		
		
		if($data['getselfpoints']!=false && $data['points']!=false )
		{
		  $data['getselfreportpoint']=$data['getselfpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getselfreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
		$data['getselfreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getselfreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getselfreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getselfreportpoints'][0]='';
		}
			
			//print_r($data['alldata']);
		   //exit;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewleadership',$data);
	   }
	
	
	
	}
	function viewproficiency($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		$this->load->Model('lessonplanmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getproficiencyReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
		
		
		
			
		
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllGroupPoints();
			$data['getpoints']=$this->proficiencypointmodel->getReportPoints($report_id);
			//echo '<pre>';
			//print_r($data['points']);
			
		if($data['getpoints']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  }
		  $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		   
		}
		else
		{
		 $data['getreportpoints'][0]='';
		 $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		}
			
			
		
			
			//print_r($data['alldata']);
		   //exit;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewproficiency',$data);
	   }
	
	
	
	}
	
	function viewleadershipself($report_id,$sno)
	{
		
		$data['sno']=$sno;
		if(!empty($report_id))
		{
			$this->load->Model('reportmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getleadershipselfReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		
			
		
			$this->load->Model('lubricpointmodel');
			$data['points']=$this->lubricpointmodel->getAllGroupPoints();
			$data['getpoints']=$this->lubricpointmodel->getReportselfPoints($report_id);
		
		if($data['getpoints']!=false)
		{
		  $data['getreportpoint']=$data['getpoints'];
		  if($data['points']!=false)
		  {
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			
			}
		  
		  }
		  }
		  
		  $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		
		}
		else
		{
		 $data['getreportpoints'][0]='';
		 $point_again=0;
		  if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			  $data['alldata'][$point_id]['group_id']=$val['group_id'];
			  $data['alldata'][$point_id]['group_name']=$val['group_name'];
			  $data['alldata'][$point_id]['description']=$val['description'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		}
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewleadership',$data);
	   }
	
	
	
	}
	
	function viewreportform($report_id,$sno)
	{
		
		$data['sno']=$sno;
	
		
		if(!empty($report_id))
		{
			
			
			$this->load->Model('reportmodel');
			
			
		
		$this->load->Model('lessonplanmodel');
			
			
		
		$data['reportdata']=$this->reportmodel->getReportData($report_id);
		$data['reportdata']=$data['reportdata'][0];
		if($data['reportdata']['lesson_correlation_id']==2)
		{
		$data['standarddata']=$this->lessonplanmodel->getstandard($data['reportdata']['report_date'],$data['reportdata']['teacher_id'],$data['reportdata']['subject_id']);
		}
		
		
		
		
			
		
			$this->load->Model('rubricscalemodel');
			$data['points']=$this->rubricscalemodel->getallrubricsubscalesform($this->session->userdata("district_id"));
			$data['getpoints']=$this->rubricscalemodel->getallrubricsubscalesformpoints($report_id);
			//echo '<pre>';
			//print_r($data['getpoints']);
			//exit;
			if($data['getpoints']!=false)
			{
			foreach($data['getpoints'] as $gval)
			{
			foreach($data['points'] as $key=>$pval)
			{
			if(!empty($gval['point_id']))
			{
			  if($gval['point_id']==$pval['sub_scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}
			else
			{
				if($gval['group_id']==$pval['scale_id'])
			  {
			    $data['points'][$key]['strengths']=$gval['strengths'];
				 $data['points'][$key]['concerns']=$gval['concerns'];
			  
			  
			  }
			
			
			}

			}	
			
			}
			}
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('report/viewreportform',$data);
	   }
	
	
	
	}
	function finish()
	{
		$data['idname']='implementation';
	  if($this->session->userdata('new_report_id'))
		{
		  $report_id=$this->session->userdata('new_report_id');
		  $this->load->Model('reportmodel');
		  $data['reportdata']=$this->reportmodel->getReportData($report_id);
		  $data['reportdata']=$data['reportdata'][0];
		  if($this->session->userdata('newform')=='forma')
		{
			$this->load->Model('observationgroupmodel');
			$data['groups']=$this->observationgroupmodel->getallobservationgroups();
		}
		else if($this->session->userdata('newform')=='formc')
		{
			$this->load->Model('lickertgroupmodel');
			$data['groups']=$this->lickertgroupmodel->getalllickertgroups();
		}
		else if($this->session->userdata('newform')=='formb')
		{
			$this->load->Model('rubricscalemodel');
			$data['groups']=$this->rubricscalemodel->getallrubricsubscales($this->session->userdata("district_id"));
		}
		 $data['view_path']=$this->config->item('view_path');
		  $this->load->view('report/finish',$data);
		  
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	  
	
	}
	function leadershipfinish()
	{
	  if($this->session->userdata('new_report_id'))
		{
		  $report_id=$this->session->userdata('new_report_id');
		  $this->load->Model('reportmodel');
		  if($this->session->userdata('login_type')=='user')
		  {
			$data['reportdata']=$this->reportmodel->getleadershipReportData($report_id);
		  }
          else
          {
            $data['reportdata']=$this->reportmodel->getleadershipselfReportData($report_id);

		  }	
		  $data['reportdata']=$data['reportdata'][0];
		  
			$this->load->Model('lubricgroupmodel');
			$data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
		
		 $data['view_path']=$this->config->item('view_path');
		  $this->load->view('report/leadershipfinish',$data);
		  
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	  
	
	}
	
	function proficiencyfinish()
	{
		$data['idname']='implementation';
	  if($this->session->userdata('new_report_id'))
		{
		  $report_id=$this->session->userdata('new_report_id');
		  $this->load->Model('reportmodel');
		  if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer' )
		  {
			$data['reportdata']=$this->reportmodel->getproficiencyReportData($report_id);
		  }
          
		  $data['reportdata']=$data['reportdata'][0];
		  
			$this->load->Model('proficiencygroupmodel');
			$data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
		
		 $data['view_path']=$this->config->item('view_path');
		  $this->load->view('report/proficiencyfinish',$data);
		  
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	  
	
	}
	
	function finishall()
	{
	   $this->session->unset_userdata('new_report_id');
	   $this->session->unset_userdata('newform');
		$this->index(); 
	}
	function leadershipfinishall()
	{
	   $this->session->unset_userdata('new_report_id');
	   $this->session->unset_userdata('leadershipreport');
	   if($this->session->userdata('login_type')=='user')
		  {
			$this->leadershipcreate(); 
		  }
		  else
          {
            $this->leadershipcreateself(); 

		   }		
	}
	function proficiencyfinishall()
	{
	   $this->session->unset_userdata('new_report_id');
	   $this->session->unset_userdata('newform');
	   
	   if($this->session->userdata('login_type')=='user' || $this->session->userdata('login_type')=='observer')
		  {
			$this->index(); 
		  }
		  		
	}
	
	function leadershipfinishselfall()
	{
	    if($this->session->userdata('login_type')=='observer')
		  {
			$this->load->Model('reportmodel');
			$this->reportmodel->finishself();
			$this->leadershipcreateself(); 
		  }
		  
	}
	
	function leadershipcreate()
	{
	
		if($this->session->userdata('login_type')!='user')
	  {
	    redirect('index');
	  
	  }
		$data['view_path']=$this->config->item('view_path');
	 
		$this->load->Model('schoolmodel');
	  $data['school']=$this->schoolmodel->getschoolbydistrict();
		if($data['school']!=false)
		{
		 $this->load->Model('observermodel');
	   
	   $data['observers']=$this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
	   }
	   else
	   {
	   
		$data['observers']=false;		
	   
	   }
	 
	  
	  $this->load->view('report/leadership',$data);
	
	}
	
	
	function leadershipcreateself()
	{
		if($this->session->userdata('login_type')!='observer')
	  {
	    redirect('index');
	  
	  }
		$this->load->Model('reportmodel');
		$datareport=$this->reportmodel->check_self();
		
		
		if($datareport==false)
		{
		$data['view_path']=$this->config->item('view_path');
	 
		$this->load->Model('observermodel');
	  
	   
	   $data['observers']=$this->observermodel->getobserverById($this->session->userdata('observer_id'));
	   
	 
	  
	  $this->load->view('report/leadershipself',$data);
	  }
	  else if($datareport[0]['completed']==0)
	  {
	    $this->createleadershipself($datareport[0]['report_id']);
	  
	  
	  }
	  else if($datareport[0]['completed']==1)
	  { $data['view_path']=$this->config->item('view_path');
	    $data['reports']=$datareport;
		$this->load->view('report/leadershipselfall',$data);
	  }
	
	}
	function createleadership()
	{
		
	  if($this->session->userdata('login_type')!='user')
	  {
	    redirect('index');
	  
	  }
	  $this->load->Model('reportmodel');
	  $report_id=$this->reportmodel->createleadership();
	  if($report_id!=0)
	  {
	   	$report_number=$this->reportmodel->getreportnumber('leader');
		$this->session->set_userdata('report_number_new',$report_number);
		
		$data['view_path']=$this->config->item('view_path');
		$this->session->set_userdata('new_report_id',$report_id);
	    
			$data['reportdata']=$this->reportmodel->getleadershipReportData($report_id);
		   $data['reportdata']=$data['reportdata'][0];
			$this->load->Model('lubricgroupmodel');
			$data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
		
		
        	
		if(!empty($data['groups']))
		{
			$group_id=$data['groups'][0]['group_id'];
			if(isset($data['groups'][1]['group_id']))
			{
				$next_group_id=$data['groups'][1]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}			
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
		
			$this->load->Model('lubricpointmodel');
			$data['points']=$this->lubricpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
			$point_again=0;
			if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
			
			
			//self points
		
		$data['getselfpoints']=$this->lubricpointmodel->getReportselfPointsbyobserverid($data['reportdata']['observer_id']);
		
		
		if($data['getselfpoints']!=false && $data['points']!=false )
		{
		  $data['getselfreportpoint']=$data['getselfpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getselfreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getselfreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getselfreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getselfreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getselfreportpoints'][0]='';
		}
		
		}
		else
		{
		   $group_id=0;
		   $next_group_id=0;
		
		}
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/leadershippoints',$data);
	   
		
	  }
	  else
	  {
	    $this->leadershipcreate(); 
	  
	  }
	  
	
	
	}
	
	function createproficiency($report_id)
	{
		$data['idname']='implementation';
		
	  if($this->session->userdata('login_type')!='user' && $this->session->userdata('login_type')!='observer')
	  {
	    redirect('index');
	  
	  }
	  $this->load->Model('reportmodel');
	  //$report_id=$this->reportmodel->createproficiency();
	  if($report_id!=0)
	  {
	   	
		
		$data['view_path']=$this->config->item('view_path');
		$this->session->set_userdata('new_report_id',$report_id);
	    
			$data['reportdata']=$this->reportmodel->getproficiencyReportData($report_id);
		   $data['reportdata']=$data['reportdata'][0];
			$this->load->Model('proficiencygroupmodel');
			$data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
		
		
        	
		if(!empty($data['groups']))
		{
			$data['idname']='implementation';
			$group_id=$data['groups'][0]['group_id'];
			if(isset($data['groups'][1]['group_id']))
			{
				$next_group_id=$data['groups'][1]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}			
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
		
			$this->load->Model('proficiencypointmodel');
			$data['points']=$this->proficiencypointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
			$point_again=0;
			if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
			
			
			
		
		}
		else
		{
		   $group_id=0;
		   $next_group_id=0;
		
		}
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/proficiencypoints',$data);
	   
		
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	
	
	}
	
	function createleadershipself($areport_id=false)
	{
	  if($this->session->userdata('login_type')!='observer')
	  {
	    redirect('index');
	  
	  }
	  $this->load->Model('reportmodel');
	  if($areport_id!=false)
	  {
	    $report_id=$areport_id;
	  
	  }
	  else
	  {
	    $report_id=false;
	  
	  }
	  if($report_id==false)
	  {
		$report_id=$this->reportmodel->createleadershipself();
	  }	
	  if($report_id!=0)
	  {
	   	
		
		$data['view_path']=$this->config->item('view_path');
		$this->session->set_userdata('new_report_id',$report_id);
		$report_number=$this->reportmodel->getreportnumber('leader');
		$this->session->set_userdata('report_number_new',$report_number);
	    
			$data['reportdata']=$this->reportmodel->getleadershipselfReportData($report_id);
		   $data['reportdata']=$data['reportdata'][0];
			$this->load->Model('lubricgroupmodel');
			$data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
		
		
        	
		if(!empty($data['groups']))
		{
			$group_id=$data['groups'][0]['group_id'];
			if($areport_id!=false)
			{
               redirect('report/leadershipselfpoints/'.$group_id);
			
			}
			if(isset($data['groups'][1]['group_id']))
			{
				$next_group_id=$data['groups'][1]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}			
			foreach($data['groups'] as $val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
		   }
		  }
		
			$this->load->Model('lubricpointmodel');
			$data['points']=$this->lubricpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
			$point_again=0;
			if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		
		}
		else
		{
		   $group_id=0;
		   $next_group_id=0;
		
		}
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/leadershipselfpoints',$data);
	   
		
	  }
	  else
	  {
	    $this->leadershipcreate(); 
	  
	  }
	  
	
	
	}
	
	function leadershipsave()
	{
	  if($this->session->userdata('new_report_id'))
		{
	
	  $group_id=$this->input->post('group_id');
	  if(isset($group_id))
	  {
	  $next_group_id=$this->input->post('next_group_id');
	  
		  $this->load->Model('lubricpointmodel');
		  $status=$this->lubricpointmodel->savereport();
	  
	  
	  if($status==true)
	  {
	    if($next_group_id==0)
		{
		 $this->leadershipfinish();
		}
		else
		{
			$this->leadershippoints($next_group_id);
		}	
	  
	  }
	  else
	  {
	     
		 $this->leadershippoints($group_id);
	  
	  
	  }
	
	 }
	 }
	 else
	  {
	    $this->index(); 
	  
	  }
	  
	}
	function proficiencysave()
	{
	  if($this->session->userdata('new_report_id'))
		{
	
	  $group_id=$this->input->post('group_id');
	  if(isset($group_id))
	  {
	  $next_group_id=$this->input->post('next_group_id');
	  
		  $this->load->Model('proficiencypointmodel');
		  $status=$this->proficiencypointmodel->savereport();
	  
	  
	  if($status==true)
	  {
	    if($next_group_id==0)
		{
		 $this->proficiencyfinish();
		}
		else
		{
			$this->proficiencypoints($next_group_id);
		}	
	  
	  }
	  else
	  {
	     
		 $this->proficiencypoints($group_id);
	  
	  
	  }
	
	 }
	 }
	 else
	  {
	    $this->index(); 
	  
	  }
	  
	}
	function leadershipselfsave()
	{
	  if($this->session->userdata('new_report_id'))
		{
	
	  $group_id=$this->input->post('group_id');
	  if(isset($group_id))
	  {
	  $next_group_id=$this->input->post('next_group_id');
	  
		  $this->load->Model('lubricpointmodel');
		  $status=$this->lubricpointmodel->selfsavereport();
	  
	  
	  if($status==true)
	  {
	    if($next_group_id==0)
		{
		 $this->leadershipfinish();
		}
		else
		{
			$this->leadershipselfpoints($next_group_id);
		}	
	  
	  }
	  else
	  {
	     
		 $this->leadershipselfpoints($group_id);
	  
	  
	  }
	
	 }
	 }
	 else
	  {
	    $this->index(); 
	  
	  }
	  
	}
	function leadershippoints($group_id)
	{
		if($this->session->userdata('new_report_id'))
		{
		
		
		
		$this->load->Model('reportmodel');
		
		$data['reportdata']=$this->reportmodel->getleadershipReportData($this->session->userdata('new_report_id'));
		$data['reportdata']=$data['reportdata'][0];
	$data['view_path']=$this->config->item('view_path');
		
		 $this->load->Model('lubricgroupmodel');
		 $data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
		
		if($data['groups']!=false)
		{
		  foreach($data['groups'] as $key=>$val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
			$key++;
			if(isset($data['groups'][$key]['group_id']))
			{
				$next_group_id=$data['groups'][$key]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}	
			
		   }
		  }
		
		}
		
		$this->load->Model('lubricpointmodel');
		$data['points']=$this->lubricpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		$point_again=0;
			if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		$data['getpoints']=$this->lubricpointmodel->getReportPoints($this->session->userdata('new_report_id'));
		
		//echo "<pre>";
		//print_r($data['getpoints']);
		//exit;
		if($data['getpoints']!=false && $data['points']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getreportpoints'][0]='';
		}
		
		
		//self points
		
		$data['getselfpoints']=$this->lubricpointmodel->getReportselfPointsbyobserverid($data['reportdata']['observer_id']);
		
		
		if($data['getselfpoints']!=false && $data['points']!=false )
		{
		  $data['getselfreportpoint']=$data['getselfpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getselfreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getselfreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getselfreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getselfreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getselfreportpoints'][0]='';
		}
		
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/leadershippoints',$data);
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	
	}
	function proficiencypoints($group_id)
	{
		$data['idname']='implementation';
		if($this->session->userdata('new_report_id'))
		{
		
		
		
		$this->load->Model('reportmodel');
		
		$data['reportdata']=$this->reportmodel->getproficiencyReportData($this->session->userdata('new_report_id'));
		$data['reportdata']=$data['reportdata'][0];
	$data['view_path']=$this->config->item('view_path');
		
		 $this->load->Model('proficiencygroupmodel');
		 $data['groups']=$this->proficiencygroupmodel->getallproficiencygroupsbyDistrictID($this->session->userdata('district_id'));
		
		if($data['groups']!=false)
		{
		  foreach($data['groups'] as $key=>$val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
			$key++;
			if(isset($data['groups'][$key]['group_id']))
			{
				$next_group_id=$data['groups'][$key]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}	
			
		   }
		  }
		
		}
		
		$this->load->Model('proficiencypointmodel');
		$data['points']=$this->proficiencypointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		$point_again=0;
			if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		$data['getpoints']=$this->proficiencypointmodel->getReportPoints($this->session->userdata('new_report_id'));
		
		//echo "<pre>";
		//print_r($data['getpoints']);
		//exit;
		if($data['getpoints']!=false && $data['points']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getreportpoints'][0]='';
		}
		
		
		
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/proficiencypoints',$data);
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	
	}
	function leadershipselfpoints($group_id)
	{
		if($this->session->userdata('new_report_id'))
		{
		
		
		
		$this->load->Model('reportmodel');
		
		$data['reportdata']=$this->reportmodel->getleadershipselfReportData($this->session->userdata('new_report_id'));
		$data['reportdata']=$data['reportdata'][0];
	$data['view_path']=$this->config->item('view_path');
		
		 $this->load->Model('lubricgroupmodel');
		 $data['groups']=$this->lubricgroupmodel->getalllubricgroupsbyDistrictID($this->session->userdata('district_id'));
		
		if($data['groups']!=false)
		{
		  foreach($data['groups'] as $key=>$val)
		  {
		    if($val['group_id']==$group_id)
			{
			$data['group_name']=$val['group_name'];
			$data['description']=$val['description'];
			$key++;
			if(isset($data['groups'][$key]['group_id']))
			{
				$next_group_id=$data['groups'][$key]['group_id'];
			}
			else
			{
				$next_group_id=0;

			}	
			
		   }
		  }
		
		}
		
		$this->load->Model('lubricpointmodel');
		$data['points']=$this->lubricpointmodel->getAllPoints($group_id,$this->session->userdata("district_id"));
		$point_again=0;
			if($data['points']!=false)
			{
			foreach($data['points'] as $val)
			{
			$point_id=$val['point_id'];
			if($point_again!=$point_id)
			{
			  $data['alldata'][$point_id]['names'][]=$val['sub_group_name'];
			  $data['alldata'][$point_id]['question']=$val['question'];
			  $data['alldata'][$point_id]['text'][]=$val['sub_group_text'];
			  $data['alldata'][$point_id]['sub_group_id'][]=$val['sub_group_id'];
			  $data['alldata'][$point_id]['ques_type']=$val['ques_type'];
			
			
			}
			}
			}
			else
			{
			$data['alldata']=false;
			
			}
		$data['getpoints']=$this->lubricpointmodel->getReportselfPoints($this->session->userdata('new_report_id'));
		
		//echo "<pre>";
		//print_r($data['getpoints']);
		//exit;
		if($data['getpoints']!=false && $data['points']!=false )
		{
		  $data['getreportpoint']=$data['getpoints'];
		  foreach($data['points'] as $pointval)
		  {
			foreach($data['getreportpoint'] as $reportval)
			{
			  if($pointval['ques_type']=='checkbox' && $pointval['group_type_id']==2 )
			  {
			    if($pointval['point_id']==$reportval['point_id'])
				{
					$data['getreportpoints'][$reportval['point_id']][$reportval['response']]=$reportval['response'];
			    }
			  }
			  else  if($pointval['point_id']==$reportval['point_id'])
			  {
			    $data['getreportpoints'][$reportval['point_id']]['response']=$reportval['response'];
			  
			  }
			  if($reportval['group_id'])
			  {
			    $data['getreportpoints'][$reportval['group_id']]['response-text']=$reportval['responsetext'];
				
			  
			  
			  }
			}
		  
		  }
		  
		
		}
		else
		{
		$data['getreportpoints'][0]='';
		}
		
		$data['group_id']=$group_id;
		$data['next_group_id']=$next_group_id;
		
		$this->load->view('report/leadershipselfpoints',$data);
	  }
	  else
	  {
	    $this->index(); 
	  
	  }
	  
	
	}
	
	function home()
	{
	
	 
	}
	
	function schoolhome()
	{
	  
	 if($this->input->post('school'))
	 {
	    
		$data['set']=1;
		$school_id=$this->input->post('school');
	    $data['school_id']=$school_id;
	   $this->load->Model('reportmodel');
	 $this->load->Model('teachermodel');
	 $this->load->Model('schoolmodel');
	 $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	 $data['view_path']=$this->config->item('view_path');
	
	if($this->session->userdata('login_type')=='user')
	  {
		
		//start perodic goal planner
		$data['noteachers']=$this->reportmodel->getNumberofteachers($school_id);
		
		$data['totalsubmitted']=$this->reportmodel->gettotalsubmittedteachers($school_id);
		$data['teachers']=$this->reportmodel->gettotalsubmittedteachernames($school_id);
		
		$data['suteachers']=$this->reportmodel->getsubmittedteachernamescount($school_id);
		$data['suteachernames']=$this->reportmodel->getsubmittedteachernames($school_id);
		
		//$data['teachers']=$this->teachermodel->getTeachersBySchool($school_id);
		$data['teachernames']='';
		if($data['teachers']!='' && $data['suteachernames']!=false)
		{
		  foreach($data['teachers'] as $val)
		  {
		    $k=1;
			foreach($data['suteachernames'] as $name)
			{
			  if($name['teacher_id']==$val['teacher_id'])
			  {
			    $k=0;
			  
			  }
			
			}
			
			if($k==1)
			{
			
			$data['teachernames'][]=$val;
			
			
			}
		  
		  
		  
		  }
		
		
		
		
		}
		else
		{
		$data['teachernames']=$data['teachers'];
		
		
		}
		
		// end of periodic goal planner
		
		// start of lesson plans
		
		$dates=$this->week_from_monday(date('d-m-Y'));
		$fdate=explode('-',$dates[0]['date']);
		$tdate=explode('-',$dates[6]['date']);
		$data['fromdate']=$dates[0]['date'];
	  $data['todate']=$dates[6]['date'];
		
	  
		$data['totallesson']=$this->reportmodel->gettotalsubmittedteacherslesson($school_id,$data['fromdate'],$data['todate']);
		$data['totallessonnames']=$this->reportmodel->gettotalsubmittedteacherslessonnames($school_id,$data['fromdate'],$data['todate']);
		
		
		$data['suteacherslesson']=$this->reportmodel->getsubmittedteacherslesson($school_id,$data['fromdate'],$data['todate']);
		$data['suteachernameslesson']=$this->reportmodel->getsubmittedteachernameslesson($school_id,$data['fromdate'],$data['todate']);
		
		$data['teachernameslesson']='';
		if($data['totallessonnames']!='' && $data['suteachernameslesson']!=false)
		{
		  foreach($data['totallessonnames'] as $val)
		  {
		    $k=1;
			foreach($data['suteachernameslesson'] as $name)
			{
			  if($name['teacher_id']==$val['teacher_id'])
			  {
			    $k=0;
			  
			  }
			
			}
			
			if($k==1)
			{
			
			$data['teachernameslesson'][]=$val;
			
			
			}
		  
		  
		  
		  }
		
		
		
		
		}
		else
		{
		$data['teachernameslesson']=$data['totallessonnames'];
		
		
		}
		// end of lesson plans
		
		// start of Observation plans
		
		
		$data['totalobservation']=$this->reportmodel->gettotalsubmittedteachersobservation($school_id,$data['fromdate'],$data['todate']);
		$data['totalteachernamesobservation']=$this->reportmodel->gettotalsubmittedteachernamesobservation($school_id,$data['fromdate'],$data['todate']);
		
		
		$data['suteachersobservation']=$this->reportmodel->getsubmittedteachersobservation($school_id,$data['fromdate'],$data['todate']);
		$data['suteachernamesobservation']=$this->reportmodel->getsubmittedteachernamesobservation($school_id,$data['fromdate'],$data['todate']);
		
		$data['teachernamesobservation']='';
		if($data['totalteachernamesobservation']!='' && $data['suteachernamesobservation']!=false)
		{
		  foreach($data['totalteachernamesobservation'] as $val)
		  {
		    $k=1;
			foreach($data['suteachernamesobservation'] as $name)
			{
			  if($name['teacher_id']==$val['teacher_id'])
			  {
			    $k=0;
			  
			  }
			
			}
			
			if($k==1)
			{
			
			$data['teachernamesobservation'][]=$val;
			
			
			}
		  
		  
		  
		  }
		
		
		
		
		}
		else
		{
		$data['teachernamesobservation']=$data['totalteachernamesobservation'];
		
		
		}
		// end of Observation plans
		
		// start of Observations
		
		
	  
		$data['suteachersobservations']=$this->reportmodel->getsubmittedteachersobservations($school_id,$data['fromdate'],$data['todate']);
		$data['suteachernamesobservations']=$this->reportmodel->getsubmittedteachernamesobservations($school_id,$data['fromdate'],$data['todate']);
		
		$data['teachernamesobservations']='';
		
		if($data['suteachernamesobservations']!=false)
		{
		foreach($data['suteachernamesobservations'] as $valueas)
		{
		  $data['tea'][$valueas['observer_id']][]= $valueas['firstname'].' '.$valueas['lastname'].' ('.$valueas['created'].')<br />Start Time: '.$valueas['starttime'].' - End Time: '.$valueas['endtime'].''; 
		
		
		}
		
		
		}
		//echo '<pre>';
		//print_r($data['tea']);
		//exit;
		//$data['totalteachers']=$this->teachermodel->getTeachersBySchool($school_id);
		
		/*if($data['totalteachers']!='' && $data['suteachernamesobservations']!=false)
		{
		  foreach($data['totalteachers'] as $val)
		  {
		    $k=1;
			foreach($data['suteachernamesobservations'] as $name)
			{
			  if($name['teacher_id']==$val['teacher_id'])
			  {
			    $k=0;
			  
			  }
			
			}
			
			if($k==1)
			{
			
			$data['teachernamesobservations'][]=$val;
			
			
			}
		  
		  
		  
		  }
		
		
		
		
		}
		else
		{
		$data['teachernamesobservations']=$data['totalteachers'];
		
		
		}*/
		// end of Observations
		
		$data['fromdate']=$fdate[1].'/'.$fdate[2].'/'.$fdate[0];
	  $data['todate']=$tdate[1].'/'.$tdate[2].'/'.$tdate[0];
		
		}
	
		
	
	 $this->load->view('district/home',$data);
	 
	 
	 
	 
	 
	 
	 
	 }
	 else
	 {
	     $this->load->Model('schoolmodel');
	 $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	 $data['view_path']=$this->config->item('view_path');
		 $this->load->view('district/home',$data);
	 
	 }
	
	
	
	
	
	
	
	
	}
	
	function SummativeReport()
	{
            error_reporting(0);
		$data['idname']='implementation';
	    $this->load->Model('teachermodel');
		$this->load->Model('statusmodel');
		$this->load->Model('scoremodel');
		$data['statuses']=$this->statusmodel->getstatusbydistrict();
		$data['scores']=$this->scoremodel->getscorebydistrict();
		
		if($this->session->userdata('login_type')=='user')
		{
		
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolbydistrict();
		
		
		if($data['school']!=false)
		{
			
		$this->load->Model('teachermodel');
	   $data['teachers']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
	   
	 
	   }
	   else
	   {
	    $data['teachers']=false; 
	   
	   }
	   }
	   else if($this->session->userdata("login_type")=='observer')
		{
			
			
		$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
		}
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('report/summativereport',$data);
	}
	
	function getteacher_observation()
	{
	
	  $data['view_path']=$this->config->item('view_path');	  
	  $this->load->Model('schoolmodel');	  
	  
	  
	  if($this->session->userdata('login_type')=='user')
	  {
			$data['school']=$this->schoolmodel->getschoolbydistrict();
			$this->load->view('report/teacher_observation',$data);			
		
	  }
	
	}
	function getteacher_notification()
	{
	$data['idname']='tools';
	  $data['view_path']=$this->config->item('view_path');	  
	  $this->load->Model('schoolmodel');	  
	  
	  
	  if($this->session->userdata('login_type')=='user')
	  {
			$data['school']=$this->schoolmodel->getschoolbydistrict();
			$this->load->view('report/teacher_notification',$data);			
		
	  }
	
	}
	function get_teacher_observation()
	{
	  $year=$this->input->post('year');
	  $school_id=$this->input->post('school');
	  $data['year']=$year;
	  $data['school_id']=$school_id;
	  $cols=''; 	  
	  $headers='';
	  $form=''; 
		  for($i=1;$i<=12;$i++)
		  {
			 $j=1;
			 $head = array();
			 $head["startColumnName"] = "value_".$i.'_'.$j;
			 $head["numberOfColumns"] =4 ;
			 $head["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
			 $head=json_encode($head);
			 $headers.=$head.',';
			
			for($j=1;$j<=4;$j++)
			{	
			$col = array();
			$col["name"] = "value_".$i.'_'.$j;
			$col["index"] = "time_".$i.'_'.$j;
			$col["width"] = 60;
			$col["align"] = "center"; // render as select	
			if($j==1)
			{
			$form.="'Checklist',";
			}
			if($j==2)
			{
			$form.="'Scale',";
			
			}
			if($j==3)
			{
			$form.="'Proficiency',";
			}
			if($j==4)
			{
			$form.="'Likert',";
			
			}
			$newas=json_encode($col);
			$cols.=$newas.',';	
			}
			 
		  
		  }
		  	
		  $data['cols']=substr($cols,0,-1);		 
		  
		$data['headers']=substr($headers,0,-1);	
		$data['form']=substr($form,0,-1);			
	  
	  $data['view_path']=$this->config->item('view_path');	  
	  $this->load->view('report/teacher_observation_report',$data);



	}
	function get_teacher_notification()
	{
		$data['idname']='tools';
	  $year=$this->input->post('year');
	  $school_id=$this->input->post('school');
	  $data['year']=$year;
	  $data['school_id']=$school_id;
	  $cols=''; 	  
	  $headers='';
	  $form=''; 
		  for($i=1;$i<=12;$i++)
		  {
			 $j=1;
			 $head = array();
			 $head["startColumnName"] = "value_".$i.'_'.$j;
			 $head["numberOfColumns"] =3 ;
			 $head["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
			 $head=json_encode($head);
			 $headers.=$head.',';
			
			for($j=1;$j<=3;$j++)
			{	
			$col = array();
			$col["name"] = "value_".$i.'_'.$j;
			$col["index"] = "time_".$i.'_'.$j;
			$col["width"] = 60;
			$col["align"] = "center"; // render as select	
			if($j==1)
			{
			$form.="'LP',";
			}
			if($j==2)
			{
			$form.="'HN',";
			}
			if($j==3)
			{
			$form.="'BL',";
			}
			
			$newas=json_encode($col);
			$cols.=$newas.',';	
			}
			 
		  
		  }
		  	
		  $data['cols']=substr($cols,0,-1);		 
		  
		$data['headers']=substr($headers,0,-1);	
		$data['form']=substr($form,0,-1);			
	  
	  $data['view_path']=$this->config->item('view_path');	  
	  $this->load->view('report/teacher_notification_report',$data);



	}
	function getteacher_professional()
	{
		 $data['idname']='tools';
		 $data['view_path']=$this->config->item('view_path');	  
	  $this->load->Model('schoolmodel');	  
	  
	  
	  if($this->session->userdata('login_type')=='user')
	  {
			$data['school']=$this->schoolmodel->getschoolbydistrict();
			$this->load->view('report/teacher_professional',$data);			
		
	  }
	  
		
	
	
	}
	function get_teacher_professional()
	{
		 $data['idname']='tools';
	  $year=$this->input->post('year');
	  $data['year']=$year;
	  $school_id=$this->input->post('school');
	  $data['school_id']=$school_id;
	  $cols=''; 	  
	  $headers='';
	  $form=''; 
		 
			
			for($j=1;$j<=4;$j++)
			{	
			$col = array();
			$col["name"] = "value_".$j;
			$col["index"] = "time_".$j;
			$col["width"] = 180;
			$col["align"] = "center"; // render as select	
			if($j==1)
			{
			$form.="'PLC Discussions',";
			}
			if($j==2)
			{
			$form.="'Individual Articles',";
			}
			if($j==3)
			{
			$form.="'Individual Videos',";
			}
			if($j==4)
			{
			$form.="'Individual Artifacts',";
			}
			$newas=json_encode($col);
			$cols.=$newas.',';	
			}
			 
		  
		 
		  	
		  $data['cols']=substr($cols,0,-1);		 
		  
		
		$data['form']=substr($form,0,-1);			
	  
	  $data['view_path']=$this->config->item('view_path');	  
	  $this->load->view('report/teacher_professional_report',$data);



	}
	function teacher_professional_pdf($year,$school_id)
{
$view_path=$this->config->item('view_path');
$header='';
$style='';
$message='';
$orgmessage='';
$this->load->Model('teachermodel');
 
		$teachers = $this->teachermodel->getTeachersBySchool($school_id);
		


$style.='<style type="text/css">
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:95.2px;}
</style>
';

if($teachers!=false)
	{
    
	$orgmessage.="<table class='gridtable'><tr><td>Teacher Name</td><td>SLC Discussion&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Article Reviews&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Video Reviews&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Artifacts&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
	$k=0;
	
	foreach($teachers as $key=>$teachervalue)
	{
	 $this->load->Model('banktimemodel');	  
	 $plctotal=$this->banktimemodel->getschoolplcall($school_id,$year);
	 $plcteacher=$this->banktimemodel->getteacherplcall($school_id,$teachervalue['teacher_id'],$year);
	  $articletotalassign=$this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'],$year);
	   $articleteacherjournal=$this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'],$year);
	   $videototalassign=$this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'],$year);
	   $videoteacherjournal=$this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'],$year);
	   $artifacts=$this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'],$year);
	$orgmessage.="<tr><td>".$teachervalue['firstname']."  ".$teachervalue['lastname']."</td>";
	
	  $orgmessage.="<td>".$plcteacher." of ".$plctotal."</td>";	
		 $orgmessage.="<td>".$articleteacherjournal." of ".$articletotalassign."</td>";	
		 $orgmessage.="<td>".$videoteacherjournal." of ".$videototalassign."</td>";	
		 $orgmessage.="<td>".$artifacts."</td>";
	$orgmessage.="</tr>";
	}
	$orgmessage.="</table>";
	}
	else
	{
	 $orgmessage.='<table border="1"><tr>';
	$orgmessage.= "<td>No Teachers Found</td>";	
   $orgmessage.='</tr></table>'	;	
	
	}
$this->load->Model('report_disclaimermodel');
	$reportdis = $this->report_disclaimermodel->getallplans(6);  
	$dis='';
	$fontsize='';
	$fontcolor='';
	if($reportdis!=false)
	{
	
		$dis=$reportdis[0]['tab'];
		$fontsize=$reportdis[0]['size'];
		$fontcolor=$reportdis[0]['color'];
	}

	  
$str='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              <div style="width:650px;font-size:'.$fontsize.'px;color:#'.$fontcolor.';">'.$dis.'</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b> 		
		<br />';
		$this->load->Model('schoolmodel');
	$schools = $this->schoolmodel->getschoolById($school_id); 
	
		
			$str.='<b>'.$schools[0]['school_name'].'</b>';
		
		
		
	
		
		$str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />
		<b><div style="font-size:14px;">Professional Development Activity:</div></b><br/><br/><br/>		
		'.$style.$orgmessage.'
		<br />
		<br />
   </page> ';
   

$html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(18, 20, 20, 2));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output();

}
	
function teacher_observation_pdf($year,$school_id)
{
$view_path=$this->config->item('view_path');
$header='';
$style='';
$message='';
$orgmessage='';
$this->load->Model('teachermodel');
 
		$teachers = $this->teachermodel->getTeachersBySchool($school_id);


$style.='<style type="text/css">
table.gridtable {	
	font-size:9px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
</style>
';

if($teachers!=false)
	{
    
	
	
	$k=0;
	
	foreach($teachers as $key=>$teachervalue)
	{
	//for($j=1;$j<=7;$j++)
	{
	if($k==0)
	{
	$header.='<table border="1" style="font-size:10px;"><tr>';	
	$header.='<td><table><tr><td><b>Month Name</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	$monthname=date( 'F', mktime(0, 0, 0, $m) );
	 $header.='<tr><td>'.$monthname.'</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	
	}
	$header.='</table></td>';
	$header.='<td><table><tr><td><b>Forms</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	 $header.='<tr><td>Checklist</td></tr>';
	 $header.='<tr><td>Scale</td></tr>';
	 $header.='<tr><td>Proficiency</td></tr>';
	 $header.='<tr><td>Likert</td></tr>';
	 
	 
	
	}
	$header.='</table></td>';
	}
	if($k%6==0 && $k!=0)
	{
	$orgmessage.=$header.$message.'</tr></table>';
	$header='';
	$message='';
	$header.='<table border="1" style="font-size:10px;"><tr>';	
	$header.='<td><table><tr><td><b>Month Name</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	$monthname=date( 'F', mktime(0, 0, 0, $m) );
	 $header.='<tr><td>'.$monthname.'</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	
	}
	$header.='</table></td>';
	$header.='<td><table><tr><td><b>Forms</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	 $header.='<tr><td>Checklist</td></tr>';
	 $header.='<tr><td>Likert</td></tr>';
	 $header.='<tr><td>Scale</td></tr>';
	 $header.='<tr><td>Proficiency</td></tr>';
	
	}
	$header.='</table></td>';
	
	
	
	}
	 $reportdata=$this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'],$year);	  	 
	  
	$message.= "<td><table>";
	$message.= "<tr>";
	$message.= "<td><b>".$teachervalue['firstname']."  ".$teachervalue['lastname']."</b></td>";
	$message.= "</tr>";	
	if($reportdata!=false)
	{
	for($m=1;$m<=12;$m++)
	
	{
	$forma=0;
	$formaformal=0;
	$formainformal=0;
	$formb=0;
	$formbformal=0;
	$formbinformal=0;
	$formc=0;
	$formcformal=0;
	$formcinformal=0;
	$formp=0;
	$formpformal=0;
	$formpinformal=0;
	foreach($reportdata as $reportvalue)
	{
	
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='forma')
	 {
	    $forma=1;
		if($reportvalue['period_lesson']=='Formal')
		{
		 $formaformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formainformal=$reportvalue['count'];
		}
	   
	 }
	 
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='formb')
	 {
	  $formb=1;
	  if($reportvalue['period_lesson']=='Formal')
		{
		 $formbformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formbinformal=$reportvalue['count'];
		}
	 }
	 
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='formc')
	 {
	  $formc=1;
	  if($reportvalue['period_lesson']=='Formal')
		{
		 $formcformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formcinformal=$reportvalue['count'];
		}
	 }
	 
	 if($reportvalue['month']==$m && $reportvalue['report_form']=='formp')
	 {
	  $formp=1;
	  if($reportvalue['period_lesson']=='Formal')
		{
		 $formpformal=$reportvalue['count'];
		}
		if($reportvalue['period_lesson']=='Informal')
		{
		 $formpinformal=$reportvalue['count'];
		}
	 }
	  
	 
	
	}
	if($forma==1)
	  {
	    $message.= "<tr>";
		$message.="<td><span class='blue'>".$formaformal."</span>,&nbsp;<span class='yellow'>".$formainformal."</span></td>";	
		$message.= "</tr>";
	  }
	  else
	  {
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	  }
	  if($formb==1)
	  {
	    $message.= "<tr>";
		$message.="<td><span class='blue'>".$formbformal."</span>,&nbsp;<span class='yellow'>".$formbinformal."</span></td>";	
		$message.= "</tr>";
	  }
	  else
	  {
	  $message.= "<tr>";
	 $message.= "<td>&nbsp;</td>";	
	 $message.= "</tr>";
	  }
	  
	  if($formp==1)
	  {
	  $message.= "<tr>";
	    $message.="<td><span class='blue'>".$formpformal."</span>,&nbsp;<span class='yellow'>".$formpinformal."</span></td>";	
		$message.= "</tr>";
	  }
	  else
	  {
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	  }
	  if($formc==1)
	  {
	    $message.= "<tr>";
		$message.="<td><span class='blue'>".$formcformal."</span>,&nbsp;<span class='yellow'>".$formcinformal."</span></td>";	
		$message.= "</tr>";
	  }
	  else
	  {
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	  }
	}	
	
	}
	else
	{
	for($m=1;$m<=12;$m++)	
	{
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	  $message.= "<tr>";
	  $message.= "<td>&nbsp;</td>";	
	  $message.= "</tr>";
	}
	
	}
	$message.= "</table></td>";
	$k++;
	}
	}
	$orgmessage.=$header.$message.'</tr></table>'	;
	}
	else
	{
	 $orgmessage.='<table border="1"><tr>';
	$orgmessage.= "<td>No Teachers Found</td>";	
   $orgmessage.='</tr></table>'	;	
	
	}

		$this->load->Model('report_disclaimermodel');
	$reportdis = $this->report_disclaimermodel->getallplans(4);  
	$dis='';
	$fontsize='';
	$fontcolor='';
	if($reportdis!=false)
	{
	
		$dis=$reportdis[0]['tab'];
		$fontsize=$reportdis[0]['size'];
		$fontcolor=$reportdis[0]['color'];
	}
	

	  
$str='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              <div style="width:650px;font-size:'.$fontsize.'px;color:#'.$fontcolor.';">'.$dis.'</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b> 		
		<br />';
		$this->load->Model('schoolmodel');
	$schools = $this->schoolmodel->getschoolById($school_id); 
	
		
			$str.='<b>'.$schools[0]['school_name'].'</b>';
		
		
		
		
		
		$str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />
		<b><div style="font-size:14px;">Observation Count:</div></b><br/>
		<b><div style="font-size:12px;">Year:&nbsp;'.$year.'&nbsp;Legend:</div>
		<div style="font-size:12px;">Blue:&nbsp;&nbsp;Formal &nbsp; Red:&nbsp;&nbsp;Informal  </div></b><br/>
		'.$style.$orgmessage.'
		<br />
		<br />
   </page> ';

$html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(18, 20, 20, 2));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output();

}
function teacher_notification_pdf($year,$school_id)
{
$view_path=$this->config->item('view_path');
$header='';
$style='';
$message='';
$orgmessage='';
$this->load->Model('teachermodel');
 
		$teachers = $this->teachermodel->getTeachersBySchool($school_id);


$style.='<style type="text/css">
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
</style>
';

if($teachers!=false)
	{
    
	
	
	$k=0;
	
	foreach($teachers as $key=>$teachervalue)
	{
	//for($j=1;$j<=7;$j++)
	{
	if($k==0)
	{
	$header.='<table border="1"><tr>';	
	$header.='<td><table><tr><td><b>Month Name</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	$monthname=date( 'F', mktime(0, 0, 0, $m) );
	 $header.='<tr><td>'.$monthname.'</td></tr>';	 
	 $header.='<tr><td>&nbsp;</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	
	}
	$header.='</table></td>';
	$header.='<td><table><tr><td><b>Tasks</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	 $header.='<tr><td>LP</td></tr>';
	 $header.='<tr><td>HN</td></tr>';
	 $header.='<tr><td>BL</td></tr>';
	 
	
	}
	$header.='</table></td>';
	}
	if($k%5==0 && $k!=0)
	{
	$orgmessage.=$header.$message.'</tr></table>';
	$header='';
	$message='';
	$header.='<table border="1"><tr>';	
	$header.='<td><table><tr><td><b>Month Name</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	$monthname=date( 'F', mktime(0, 0, 0, $m) );
	 $header.='<tr><td>'.$monthname.'</td></tr>';	 
	 $header.='<tr><td>&nbsp;</td></tr>';
	 $header.='<tr><td>&nbsp;</td></tr>';
	
	}
	$header.='</table></td>';
	$header.='<td><table><tr><td><b>Tasks</b></td></tr>';
	for($m=1;$m<=12;$m++)
	
	{
	 $header.='<tr><td>LP</td></tr>';
	 $header.='<tr><td>HN</td></tr>';
	 $header.='<tr><td>BL</td></tr>';
	
	}
	$header.='</table></td>';
	
	
	
	}
	  $this->load->Model('lessonplanmodel');	  
	 $lessondata=$this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'],$year);
$homeworkdata=$this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'],$year);	  	 
$behaviordata=$this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'],$year);	  	 	 
	  
	$message.= "<td><table>";
	$message.= "<tr>";
	$message.= "<td><b>".$teachervalue['firstname']."  ".$teachervalue['lastname']."</b></td>";
	$message.= "</tr>";	
	
	for($m=1;$m<=12;$m++)
	
	{
	$lp=0;
	$hn=0;	
	$bl=0;
	$lpvalue=0;	
	$hnvalue=0;	
	$blvalue=0;		
	
	
	if($lessondata!=false)
	{
	foreach($lessondata as $lessonvalue)
	{	
	 if($lessonvalue['month']==$m)
	 {
	    $lp=1;
		
		$lpvalue=$lessonvalue['count'];
		
	   
	 }	
	 }
	 }
	 if($homeworkdata!=false)
	{
	foreach($homeworkdata as $homevalue)
	{	
	 if($homevalue['month']==$m)
	 {
	    $hn=1;
		
		$hnvalue=$homevalue['count'];
		
	   
	 }	
	 }
	 }
	 if($behaviordata!=false)
	{
	foreach($behaviordata as $bevalue)
	{	
	 if($bevalue['month']==$m)
	 {
	    $bl=1;
		
		$blvalue=$bevalue['count'];
		
	   
	 }	
	 }
	 }
	if($lp==1)
	  {
	    $message.= "<tr><td>".$lpvalue."</td></tr>";	
	  }
	  else
	  {
	  $message.= "<tr><td>0</td></tr>";	
	  }
	  if($hn==1)
	  {
	   $message.= "<tr><td>".$hnvalue."</td></tr>";	
	  }
	  else
	  {
	   $message.= "<tr><td>0</td></tr>";	
	  }
	  if($bl==1)
	  {
	   $message.= "<tr><td>".$blvalue."</td></tr>";	
	  }
	  else
	  {
	   $message.= "<tr><td>0</td></tr>";	
	  }
	  
	  
	}	
	
	
	$message.= "</table></td>";
	$k++;
	}
	}
	$orgmessage.=$header.$message.'</tr></table>'	;
	}
	else
	{
	 $orgmessage.='<table border="1"><tr>';
	$orgmessage.= "<td>No Teachers Found</td>";	
   $orgmessage.='</tr></table>'	;	
	
	}

$this->load->Model('report_disclaimermodel');
	$reportdis = $this->report_disclaimermodel->getallplans(5);  
	$dis='';
	$fontsize='';
	$fontcolor='';
	if($reportdis!=false)
	{
	
		$dis=$reportdis[0]['tab'];
		$fontsize=$reportdis[0]['size'];
		$fontcolor=$reportdis[0]['color'];
	}
	  
$str='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
            <div style="width:650px;font-size:'.$fontsize.'px;color:#'.$fontcolor.';">'.$dis.'</div>
        </page_footer> 
		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b> 		
		<br />';
		$this->load->Model('schoolmodel');
	$schools = $this->schoolmodel->getschoolById($school_id); 
	
		
			$str.='<b>'.$schools[0]['school_name'].'</b>';
		
		
		
	
		
		$str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />
		<b><div style="font-size:14px;">Notification Activity Count:</div></b><br/><br/><br/>
		<b><div style="font-size:12px;">Year:&nbsp;'.$year.'&nbsp;Legend:</div>
		<div>LP:&nbsp;Lesson Plan &nbsp; HN:&nbsp;Home Notification &nbsp;BL:&nbsp;Behavior & Learning  </div></b><br/>		
		'.$style.$orgmessage.'
			<br />
		<br />
		
		
   </page> ';

$html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(18, 20, 20, 2));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output();

}
	
	function dashboard()
	{
		
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		if($this->input->post('submit'))
		{	
			$data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($this->input->post('school_type'));
			$data['school_type'] = $this->input->post('school_type');			
		}
		else
		{
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
		}		
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
			$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		
		}
		$this->load->view('district/dashboard',$data);
	
	
	}
	function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}

 function gettestnames()
	{
		if(!empty($_REQUEST['cid']) && $_REQUEST['cid']!='')
		{
			$categoryid = $_REQUEST['cid'];
			$series_from = $_REQUEST['seriesfrom'];
			$series_to = $_REQUEST['seriesto'];
			
			
	$this->load->model('Reportschooltestmodel');
$data['testname'] = $this->Reportschooltestmodel->gettestnamebycat($categoryid,$series_from,$series_to);
			
					
			echo '<option value="" selected="selected">Please Select</option>';
			if(!empty($data['testname']))
			{
				foreach($data['testname'] as $key => $value)
				{
					echo '<option value="'.$value['id'].'">'.$value['assignment_name'].'</option>';
				}
			}

		}

	}
	
		// Single_school_report function to display the single test report page

	function single_school_report()
	{
		
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			
		}
		else
		{
		$data['view_path']=$this->config->item('view_path');
		// if the login user type is 'USER' that means user is district user
			
		if($this->session->userdata('login_type')=='user')
		{
			//get the district id

			$district_id = $this->session->userdata('district_id');

			// get the district id from session which tells us which user is currently logged into site

			$district_userid = $this->session->userdata('dist_user_id');
			$this->load->Model('Reportschooltestmodel');

			// get all schools by DISTRICT ID

			//$data['testitemcluster'] = $this->Reportschooltestmodel->getitemcluster($district_id);



			$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);

			// get all grades by DISTRICT ID

			$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
		$data['getCategory'] = $this->Reportschooltestmodel->getCategory();
			// get all assessment name (testname) from assignments table

			//$data['testname'] = $this->Reportschooltestmodel->gettestname();

			//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);

			// Get the school type by DISTRICT ID

			$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);

			//$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);

			//Load the SINGLE TEST PAGE

			$this->load->view('report/single_school_test',$data);

		}
		else if($this->session->userdata('login_type')=='teacher')
		{
			//get the district id

			$district_id = $this->session->userdata('district_id');

			// get the district id from session which tells us which user is currently logged into site

			$district_userid = $this->session->userdata('dist_user_id');
			$this->load->Model('Reportschooltestmodel');

			// get all schools by DISTRICT ID

			$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);

			// get all grades by DISTRICT ID

			$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);



			// Get the school type by DISTRICT ID

			$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);

			//$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);

			//Load the SINGLE TEST PAGE


			$data['getCategory'] = $this->Reportschooltestmodel->getCategory();


			$this->load->view('report/single_school_test',$data);


		}
		else if($this->session->userdata('login_type')=='observer')
		{

			//get the district id

			$district_id = $this->session->userdata('district_id');

			// get the district id from session which tells us which user is currently logged into site

			$district_userid = $this->session->userdata('dist_user_id');
			$this->load->Model('Reportschooltestmodel');

			// get all schools by DISTRICT ID

			$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);

			// get all grades by DISTRICT ID
$data['getCategory'] = $this->Reportschooltestmodel->getCategory();
			$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);

			// get all assessment name (testname) from assignments table

			$data['testname'] = $this->Reportschooltestmodel->gettestname();

			//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);

			// Get the school type by DISTRICT ID

			$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);

			//$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);

			//Load the SINGLE TEST PAGE

			$this->load->view('report/single_school_test',$data);


		}
		}

	}
	
	
		// get the single test report

	function get_single_school_test()
	{
		
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		$testcluster = $this->input->post('testcluster');
		$data['testcluster'] = $testcluster;
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$schooltypeid = $this->input->post('school_type');
		$testname = $this->input->post('testname');
		
		$schools = $this->input->post('schools');
		$grade = $this->input->post('grade');
		
		$series_from = $this->input->post('seriesfrom');
		$series_to = $this->input->post('seriesto');
		
		

		if($this->session->userdata('login_type')=='user')
		{
			
		$this->load->model('Reportschooltestmodel');

			//get all schools by SCHOOL TYPE ID

			$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($schooltypeid);
		
			// if we not found any school regarding school type id then it will load notfound page for display the 	         error message otherwise run the next statements

			if(empty($data['schools']))
			{
					
				$data['view_path'] = $this->config->item('view_path');
				$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
				$this->load->view('report/notfound',$data);
					
			}
			else
			{
					
				// get school id's form $data['schools'] array

				$schoolid = array();
				if(is_array($data['schools']))
				{
					foreach($data['schools'] as $key => $val)
					{
						$schoolid[]=$val['school_id'];
					}
				}
				
				

				//get users by school id and grade id

				$data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusers($schoolid,$grade);



				// if we not found users related to school id and grade then error page will display otherwise the execute the next working.

				if(empty($data['getuseracctogradeandschool']))
				{

					$data['view_path'] = $this->config->item('view_path');
					$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
					$this->load->view('report/notfound',$data);
				}
				else
				{


					$userid=array();
					$sidarr = array();
					foreach($data['getuseracctogradeandschool'] as $key => $getall)
					{
						// $userid[] = $getall['UserID'];
						foreach($getall as  $key => $value)
						{


							$getall[$key]['school_id'];   // school id

							$userid = $getall[$key]['UserID'];    // user id

							$sidarr[]=$value['school_id'];
							
							//$userid[]=$value['UserID'];
						}

							
					}



					//array_unique($sidarr);    // this is a school uniq id



					// get school average by school id , testname(assessment), 'from' to 'to'

foreach(array_unique($sidarr) as $key =>$val)
{
/*	$sdata[] =$this->Reportschooltestmodel->getfinaldata($sidarr[$key],$testname,$from,$to);*/
	$sdata[] =$this->Reportschooltestmodel->getfinaldata($sidarr[$key],$testname,$from,$to,$testcluster);
}





					// pass the final score to view, $sdata array contains school average

					$data['view_path'] = $this->config->item('view_path');
					$data['finalscore'] = $sdata;

						$data['assessmentid'] = $testname;

					//exit;
					$this->load->view('report/singletestreport',$data);

				}

			}
		}

		else if($this->session->userdata('login_type')=='observer')
		{

			$this->load->model('Reportschooltestmodel');


			$observerid =$this->session->userdata('observer_id');


			$data['observerdetail'] =$this->Reportschooltestmodel->getobdetail($observerid);
			$schoolid = $data['observerdetail'][0]['school_id'];
			$obsid = $data['observerdetail'][0]['observer_id'];


			$data['schools'] =$this->Reportschooltestmodel->checkschools($schooltypeid,$schoolid);
		 $sch_id = $data['schools'][0]['school_id'];



		 // if we not found any school regarding school type id then it will load notfound page for display the 	         error message otherwise run the next statements

		 if(empty($data['schools']))
		 {
		 	$data['view_path'] = $this->config->item('view_path');
		 	$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
		 	$this->load->view('report/notfound',$data);
		 	 
		 }
		 else
		 {
		 	 
		 	// get school id's form $data['schools'] array
		 	//get users by school id and grade id

		 	$data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusersob($sch_id,$grade);



		 	// if we not found users related to school id and grade then error page will display otherwise the execute the next working.


		 	if(empty($data['getuseracctogradeandschool']))
		 	{

		 		$data['view_path'] = $this->config->item('view_path');
		 		$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
		 		$this->load->view('report/notfound',$data);
		 	}
		 	else
		 	{

		 		$userid=array();
		 		$sidarr = array();
		 		foreach($data['getuseracctogradeandschool'] as $key => $getall)
		 		{

		 			$sidarr[]=$getall['school_id'];
		 				
		 		}
		 		$schid = $sidarr[0];
		 		$sdata =$this->Reportschooltestmodel->getfinaldataob($schid,$testname,$from,$to);
		 		$data['view_path'] = $this->config->item('view_path');
		 		$data['finalscore'] = $sdata;
				$data['assessmentid'] = $testname;
		 		$this->load->view('report/singletestreportob',$data);

		 	}




		 }

		}


		else if($this->session->userdata('login_type')=='teacher')
		{

			$this->load->model('Reportschooltestmodel');


			$teacherid =$this->session->userdata('teacher_id');

			$data['schoolid'] = $this->Reportschooltestmodel->getschoolbyteacher($teacherid);
			$schid = $data['schoolid'][0]['school_id'];


			$data['schools'] =$this->Reportschooltestmodel->checkschools($schooltypeid,$schid);

			$sch_id = $data['schools'][0]['school_id'];





			// if we not found any school regarding school type id then it will load notfound page for display the 	         error message otherwise run the next statements

			if(empty($data['schools']))
			{
					
				$data['view_path'] = $this->config->item('view_path');
				$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
				$this->load->view('report/notfound',$data);
					
			}
			else
			{
					
				// get school id's form $data['schools'] array

				//get users by school id and grade id

				$data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusersob($sch_id,$grade);



				// if we not found users related to school id and grade then error page will display otherwise the execute the next working.


				if(empty($data['getuseracctogradeandschool']))
				{

					$data['view_path'] = $this->config->item('view_path');
					$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
					$this->load->view('report/notfound',$data);
				}
				else
				{

					$userid=array();
					$sidarr = array();
					foreach($data['getuseracctogradeandschool'] as $key => $getall)
					{

						$sidarr[]=$getall['school_id'];
							
					}


					$schid = $sidarr[0];

					$sdata =$this->Reportschooltestmodel->getfinaldataob($schid,$testname,$from,$to);
	//echo '<pre>';
		//		print_r($sdata);

					$data['view_path'] = $this->config->item('view_path');
					$data['finalscore'] = $sdata;

			$data['assessmentid'] = $testname;

					$this->load->view('report/singletestreportteacher',$data);

				}




			}

		}
		}
			
	}
	
	
function multiple_school_report()
	{


		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		$data['view_path']=$this->config->item('view_path');
	 if($this->session->userdata('login_type')=='user')
	 {


	 	$district_id = $this->session->userdata('district_id');
	 	$district_userid = $this->session->userdata('dist_user_id');
	 	$this->load->Model('Reportschooltestmodel');
	 	$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);

	 	$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
	 	$data['testname'] = $this->Reportschooltestmodel->gettestname();
		$data['getCategory'] = $this->Reportschooltestmodel->getCategory();
	 	//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
	 	$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
	 	//		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);



	 	$this->load->view('report/multiple_school_report',$data);
	 }
	 else  if($this->session->userdata('login_type')=='teacher')
	 {


	 	$district_id = $this->session->userdata('district_id');
	 	$district_userid = $this->session->userdata('dist_user_id');
	 	$this->load->Model('Reportschooltestmodel');
	 	$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);

	 	$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
	 	//$data['testname'] = $this->Reportschooltestmodel->gettestname();
	 	$data['getCategory'] = $this->Reportschooltestmodel->getCategory();

	 	//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
	 	$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
	 	//		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);



	 	$this->load->view('report/multiple_school_report',$data);
	 }
	 else  if($this->session->userdata('login_type')=='observer')
	 {


	 	$district_id = $this->session->userdata('district_id');
	 	$district_userid = $this->session->userdata('dist_user_id');
	 	$this->load->Model('Reportschooltestmodel');
	 	$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);
		$data['getCategory'] = $this->Reportschooltestmodel->getCategory();
	 	$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
	 	$data['testname'] = $this->Reportschooltestmodel->gettestname();

	 	//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
	 	$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
	 	//		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);



	 	$this->load->view('report/multiple_school_report',$data);
	 }
		}
	}
	
function get_multiple_school_test()
	{


		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="demo.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		$from = $this->input->post('from');
		$to = $this->input->post('to');
			
		$schooltypeid = $this->input->post('school_type');
		$testcluster = $this->input->post('testcluster');
		$testname = $this->input->post('testname');
		
		
		
		$schools = $this->input->post('schools');
		$grade = $this->input->post('grade');

		if($this->session->userdata('login_type')=='user')
		{

			$this->load->model('Reportschooltestmodel');
			//get all schools

			$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($schooltypeid);
			

			if(empty($data['schools']))
			{
					
				$data['view_path'] = $this->config->item('view_path');
				$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
				$this->load->view('report/notfoundmultiple',$data);
					
			}
			else
			{


				$schoolid = array();
				if(is_array($data['schools']))
				{
					foreach($data['schools'] as $key => $val)
					{
						$schoolid[]=$val['school_id'];
						
					}
				}


				//get users by school id and grade id

				$data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusers($schoolid,$grade);

		
		
	

				if(empty($data['getuseracctogradeandschool']))
				{

					$data['view_path'] = $this->config->item('view_path');
					$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
					$this->load->view('report/notfoundmultiple',$data);
				}
				else
				{

					$userid=array();
					$sidarr = array();
					
				
					foreach($data['getuseracctogradeandschool'] as $key => $getall)
					{
						
						foreach($getall as  $key => $value)
						{	
							$userid = $getall[$key]['UserID'];    // user id

							$sidarr[]=$value['school_id'];
					
						}

							
					}


					foreach(array_unique($sidarr) as $key =>$val)
					{
						$sdata = array();

						foreach($testname as $key1 => $val1)
						{
							
		$sdata[] =$this->Reportschooltestmodel->getfinaldataMultiple($sidarr[$key],$testname[$key1],$from,$to);

						}


						$finalallarr[] = $sdata;

					}	
					
					
//					echo '<pre>';
	//				print_r($finalallarr);
					

		//			exit;
					
					$alldata=array();
					$avgbbbasic=array();
					$avgbasic=array();
					$avgpro=array();
					$finalbar=array();
					$schoolnames=array();
						$schoolavgs=array();
						$assessmentids=array();
						$schoolavgsschool_id =  array();
					foreach($finalallarr as $k => $j)
					{
						
						foreach($j as $jj => $bb)
						{

							
							$assessmentids[]=$j[$jj][0][0]['assignment_id'];
							$schoolavgs[]=$j[$jj][0][0]['schoolaverage'];
							$schoolavgsschool_id[]=$j[$jj][2][0]['school_id'];
							$schoolnames[]=$j[$jj][2][0]['school_name'];
							

						}
					

		// cut area
					}
						
		// paste work
								
	$barchart=array();
					
	$getdata =array();
	
	$bbasic =array();
	$basic=array();
	$proficient=array();
	// 16 july 2013
	
		$sum = array();
		
	//print_r($schoolavgs);
		//print_r($assessmentids);
		//print_r($schoolavgsschool_id);
		
		
		$ffarr =  array();
		foreach($assessmentids as $key22=>$val22)
		{
		
		
			 $ffarr[$schoolavgsschool_id[$key22]][] =  $schoolavgs[$key22];
			 //$ffarr[$schoolavgsschool_id[$key22]][] =  $schoolnames[$key22];
			
		}
		
	//	print_r($ffarr);
		
		foreach($ffarr as $key222=>$val222)
		{
			$finalbar[] = array_sum($ffarr[$key222])/count($ffarr[$key222]); 
		}
		

	//print_r($sum);
				
		/*	if(!empty($sum))
			{
				$total = count($schoolavgs);
				$finalbar[] = array_sum($sum)/$total;
				}
				else
				{
				$finalbar[]=0;
				}
			*/
	
		//echo '<pre>';	
		$assessmentids = array_unique($assessmentids);
	//print_r($finalbar);

		
	// 16 july 2013

		for($i=0;$i<count($assessmentids);$i++)
		{
			$data = $this->Reportschooltestmodel->getproficiency($assessmentids[$i]);
			
					$bbfrom = $data[0]['bbasicfrom'];	
					$bbto = $data[0]['bbasicto'];
					$bf = $data[0]['basicfrom'];	
					$bt = $data[0]['basicto'];
					$pf = $data[0]['pro_from'];
					$pt = $data[0]['pro_to'];
					
										
					 $num = round($finalbar[$i]);
					
				/*	$sum = round(@$schoolavgs[0]+@$schoolavgs[1]);
				   $total = count($schoolavgs);
				
					  $finalbar[] = $sum/$total;
					*/ 
						
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$bf && $num<=$bt)
				{
					$basic[] = $num;
				}
				if($num>=$pf && $num<=$pt)
				{
					$proficient[] = $num;
				}
				
	
	}					
	

			
if(empty($bbasic))
	{
		$avgbbbasic[]=0;
	}
	 else
	 {
		 $ttarr = array_sum($bbasic);
			   
		 $total =  count($bbasic);			
		 $avgbbbasic[] = round($ttarr/$total);		
		
	 }


				if(empty($basic))
				{
					$avgbasic[]=0;
				}
				 else
				 {
		
					 $ttarr1 = array_sum($basic);	
					$total1 =  count($basic);			
					$avgbasic[] = round($ttarr1/$total1);
			
				 }//3
				
		
				 
		if(empty($proficient))
			{
				$avgpro[]=0;
			}
			 else
			 {
		
				   $ttarr2 = array_sum($proficient);		 
				 
				
						  $total2 =  count($proficient);			
					 	$avgpro[] = round($ttarr2/$total2);
					 
					 
			
			 }
						
						
			
									
						
						
					$schname[]=array_unique($schoolnames);
		
		// end paste work				
						
					if(!empty($avgbbbasic))
					{
						$cnt = count($avgbbbasic);
						$sumavg = array_sum($avgbbbasic);
						
						$avgbbbasic = $sumavg/$cnt;
						
					}
					else
					{
						$avgbbbasic=0;
					}
					
						if(!empty($avgbasic))
					{
						$cnt1 = count($avgbasic);
						$sumavg1 = array_sum($avgbasic);
						
						$avgbasic = $sumavg1/$cnt1;
						
					}
					else
					{
						$avgbasic=0;
					}
						
						
					if(!empty($avgpro))
					{
						$cnt2 = count($avgpro);
						$sumavg2 = array_sum($avgpro);
						
						$avgpro = $sumavg2/$cnt2;
						
					}
					else
					{
						$avgpro=0;
					}
						
										
					$data['finalbbasic'] = $avgbbbasic;
					$data['finalbasic'] = $avgbasic;
					$data['finalproficient'] = $avgpro;					
					

					
			
					
					$unique_bar_values = array_unique($finalbar);
					 $finalbargraph = array_values(array_filter($unique_bar_values));
					 
					$data['bargrph']=$finalbargraph;
					
					//$data['schavg'] = $schavg;
					$data['schname']= $schname;

					
					//$data['assessmentsids'] = $testname;
					
					$data['cluster'] = $testcluster;
					$data['view_path']= $this->config->item('view_path');

					$this->load->view('report/multipletestreport',$data);


				}
			}
		}

		else if($this->session->userdata('login_type')=='observer')
		{

			$this->load->model('Reportschooltestmodel');

			//get all schools

			$observerid =$this->session->userdata('observer_id');
			$data['observerdetail'] =$this->Reportschooltestmodel->getobdetail($observerid);
		
			$schoolid = $data['observerdetail'][0]['school_id'];
			$obsid = $data['observerdetail'][0]['observer_id'];
			$data['schools'] =$this->Reportschooltestmodel->checkschools($schooltypeid,$schoolid);
			
		
			if(empty($data['schools']))
			{
					
				$data['view_path'] = $this->config->item('view_path');
				$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
				$this->load->view('report/notfoundmultiple',$data);
					
			}
			else
			{
			   $sch_id = $data['schools'][0]['school_id'];
			 
			 $data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusersob($sch_id,$grade);
			 
			
		
			 if(empty($data['getuseracctogradeandschool']))
			 {

			 	$data['view_path'] = $this->config->item('view_path');
			 	$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
			 	$this->load->view('report/notfoundmultiple',$data);
			 }
			 else
			 {

			 	$userid=array();
			 	$sidarr = array();
			 	foreach($data['getuseracctogradeandschool'] as $key => $getall)
			 	{
			 		 
					$sidarr[] = $getall['school_id'];
			 		 		
			 		 
			 		 
			 	}
				
			
				$sdata = array();
				
			 	foreach($testname as $key1 => $val1)
			 	{
			 	
		$sdata[] =$this->Reportschooltestmodel->getfinaldataMultipleob($sidarr[0],$testname[$key1],$from,$to);
			 		 $finalallarr[]=$sdata;
			 	}

				
				$alldata=array();
					$avgbbbasic=array();
					$avgbasic=array();
					$avgpro=array();
					$finalbar=array();
					$schoolnames=array();
						$schoolavgs=array();
						$assessmentids=array();
						$schoolavgsschool_id =  array();
					foreach($sdata as $k => $j)
					{
						
						
					$assessmentids[]=$sdata[$k][0][0]['assignment_id'];
					$schoolavgs[]=$sdata[$k][0][0]['schoolaverage'];
					$schoolavgsschool_id[]=$sdata[$k][2][0]['school_id'];
					$schoolnames[]=$sdata[$k][2][0]['school_name'];
				
					}
				

					
		// paste work
								
	$barchart=array();
					
	$getdata =array();
	
	$bbasic =array();
	$basic=array();
	$proficient=array();
	// 16 july 2013
	
		$sum = array();
		
	
		
		
		$ffarr =  array();
		foreach($assessmentids as $key22=>$val22)
		{
		
		
			 $ffarr[$schoolavgsschool_id[$key22]][] =  $schoolavgs[$key22];
			 //$ffarr[$schoolavgsschool_id[$key22]][] =  $schoolnames[$key22];
			
		}
		
	//	print_r($ffarr);
		
		foreach($ffarr as $key222=>$val222)
		{
			$finalbar[] = array_sum($ffarr[$key222])/count($ffarr[$key222]); 
		}
		
		//print_r($finalbar);
		
			//		exit;	

$assessmentids = array_unique($assessmentids);

		for($i=0;$i<count($assessmentids);$i++)
		{
			$data = $this->Reportschooltestmodel->getproficiency($assessmentids[$i]);
					
					$bbfrom = $data[0]['bbasicfrom'];	
					$bbto = $data[0]['bbasicto'];
					$bf = $data[0]['basicfrom'];	
					$bt = $data[0]['basicto'];
					$pf = $data[0]['pro_from'];
					$pt = $data[0]['pro_to'];

				 $num = round($finalbar[0]);
					
				
						
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$bf && $num<=$bt)
				{
					$basic[] = $num;
				}
				if($num>=$pf && $num<=$pt)
				{
					$proficient[] = $num;
				}
				
	
	}					
		
//		print_r($proficient);
	//	exit;

			
if(empty($bbasic))
	{
		$avgbbbasic[]=0;
	}
	 else
	 {
		 $ttarr = array_sum($bbasic);
			   
		 $total =  count($bbasic);			
		 $avgbbbasic[] = round($ttarr/$total);		
		
	 }


				if(empty($basic))
				{
					$avgbasic[]=0;
				}
				 else
				 {
		
					 $ttarr1 = array_sum($basic);	
					$total1 =  count($basic);			
					$avgbasic[] = round($ttarr1/$total1);
			
				 }//3
				
		
				 
		if(empty($proficient))
			{
				$avgpro[]=0;
			}
			 else
			 {
		
				   $ttarr2 = array_sum($proficient);		 
				 
				
						  $total2 =  count($proficient);			
					 	$avgpro[] = round($ttarr2/$total2);
					 
					 
			
			 }
			 		
			$schname[]=array_unique($schoolnames);
		
	
		// end paste work				
						
					if(!empty($avgbbbasic))
					{
						$cnt = count($avgbbbasic);
						$sumavg = array_sum($avgbbbasic);
						
						$avgbbbasic = $sumavg/$cnt;
						
					}
					else
					{
						$avgbbbasic=0;
					}
					
						if(!empty($avgbasic))
					{
						$cnt1 = count($avgbasic);
						$sumavg1 = array_sum($avgbasic);
						
						$avgbasic = $sumavg1/$cnt1;
						
					}
					else
					{
						$avgbasic=0;
					}
						
						
					if(!empty($avgpro))
					{
						$cnt2 = count($avgpro);
						$sumavg2 = array_sum($avgpro);
						
						$avgpro = $sumavg2/$cnt2;
						
					}
					else
					{
						$avgpro=0;
					}
						
										
					$data['finalbbasic'] = $avgbbbasic;
					$data['finalbasic'] = $avgbasic;
					$data['finalproficient'] = $avgpro;					
					

					
			
					
					$unique_bar_values = array_unique($finalbar);
					 $finalbargraph = array_values(array_filter($unique_bar_values));
					
					$data['bargrph'] = $finalbar;
					$data['schname']= $schname;
					
	//				echo '<pre>';
//					print_r($data);
		//			exit;
					
				$data['cluster'] = $testcluster;
			 	$data['view_path']= $this->config->item('view_path');

			 	$this->load->view('report/multipletestreportob',$data);


			 }
			}

		}
		else if($this->session->userdata('login_type')=='teacher')
		{
				

			$this->load->model('Reportschooltestmodel');

			//get all schools

			
			$teacherid =$this->session->userdata('teacher_id');

			$data['schoolid'] = $this->Reportschooltestmodel->getschoolbyteacher($teacherid);
			$schid = $data['schoolid'][0]['school_id'];


			$data['schools'] =$this->Reportschooltestmodel->checkschools($schooltypeid,$schid);

				
			if(empty($data['schools']))
			{
					
				$data['view_path'] = $this->config->item('view_path');
				$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
				$this->load->view('report/notfoundmultiple',$data);
					
			}
			else
			{
			   $sch_id = $data['schools'][0]['school_id'];
			 
			 $data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusersob($sch_id,$grade);
			 
			
		
			 if(empty($data['getuseracctogradeandschool']))
			 {

			 	$data['view_path'] = $this->config->item('view_path');
			 	$this->session->set_flashdata('recnotfound','We have not found any record that matching your search creteria...');
			 	$this->load->view('report/notfoundmultiple',$data);
			 }
			 else
			 {

			 	$userid=array();
			 	$sidarr = array();
			 	foreach($data['getuseracctogradeandschool'] as $key => $getall)
			 	{
			 		 
					$sidarr[] = $getall['school_id'];
			 		 		
			 		 
			 		 
			 	}
				
			
				$sdata = array();
				
			 	foreach($testname as $key1 => $val1)
			 	{
			 	
		$sdata[] =$this->Reportschooltestmodel->getfinaldataMultipleob($sidarr[0],$testname[$key1],$from,$to);
			 		 $finalallarr[]=$sdata;
			 	}

				
				$alldata=array();
					$avgbbbasic=array();
					$avgbasic=array();
					$avgpro=array();
					$finalbar=array();
					$schoolnames=array();
						$schoolavgs=array();
						$assessmentids=array();
						$schoolavgsschool_id =  array();
					foreach($sdata as $k => $j)
					{
						
						
					$assessmentids[]=$sdata[$k][0][0]['assignment_id'];
					$schoolavgs[]=$sdata[$k][0][0]['schoolaverage'];
					$schoolavgsschool_id[]=$sdata[$k][2][0]['school_id'];
					$schoolnames[]=$sdata[$k][2][0]['school_name'];
				
					}
				

					
		// paste work
								
	$barchart=array();
					
	$getdata =array();
	
	$bbasic =array();
	$basic=array();
	$proficient=array();
	// 16 july 2013
	
		$sum = array();
		
	
		
		
		$ffarr =  array();
		foreach($assessmentids as $key22=>$val22)
		{
		
		
			 $ffarr[$schoolavgsschool_id[$key22]][] =  $schoolavgs[$key22];
			 //$ffarr[$schoolavgsschool_id[$key22]][] =  $schoolnames[$key22];
			
		}
		
	//	print_r($ffarr);
		
		foreach($ffarr as $key222=>$val222)
		{
			$finalbar[] = array_sum($ffarr[$key222])/count($ffarr[$key222]); 
		}
		
		//print_r($finalbar);
		
			//		exit;	

$assessmentids = array_unique($assessmentids);

		for($i=0;$i<count($assessmentids);$i++)
		{
			$data = $this->Reportschooltestmodel->getproficiency($assessmentids[$i]);
					
					$bbfrom = $data[0]['bbasicfrom'];	
					$bbto = $data[0]['bbasicto'];
					$bf = $data[0]['basicfrom'];	
					$bt = $data[0]['basicto'];
					$pf = $data[0]['pro_from'];
					$pt = $data[0]['pro_to'];

				 $num = round($finalbar[0]);
					
				
						
				if($num>=$bbfrom && $num<=$bbto)
				{
					
					$bbasic[] = $num;
					
				}
				else if($num>=$bf && $num<=$bt)
				{
					$basic[] = $num;
				}
				if($num>=$pf && $num<=$pt)
				{
					$proficient[] = $num;
				}
				
	
	}					
		
//		print_r($proficient);
	//	exit;

			
if(empty($bbasic))
	{
		$avgbbbasic[]=0;
	}
	 else
	 {
		 $ttarr = array_sum($bbasic);
			   
		 $total =  count($bbasic);			
		 $avgbbbasic[] = round($ttarr/$total);		
		
	 }


				if(empty($basic))
				{
					$avgbasic[]=0;
				}
				 else
				 {
		
					 $ttarr1 = array_sum($basic);	
					$total1 =  count($basic);			
					$avgbasic[] = round($ttarr1/$total1);
			
				 }//3
				
		
				 
		if(empty($proficient))
			{
				$avgpro[]=0;
			}
			 else
			 {
		
				   $ttarr2 = array_sum($proficient);		 
				 
				
						  $total2 =  count($proficient);			
					 	$avgpro[] = round($ttarr2/$total2);
					 
					 
			
			 }
			 		
			$schname[]=array_unique($schoolnames);
		
	
		// end paste work				
						
					if(!empty($avgbbbasic))
					{
						$cnt = count($avgbbbasic);
						$sumavg = array_sum($avgbbbasic);
						
						$avgbbbasic = $sumavg/$cnt;
						
					}
					else
					{
						$avgbbbasic=0;
					}
					
						if(!empty($avgbasic))
					{
						$cnt1 = count($avgbasic);
						$sumavg1 = array_sum($avgbasic);
						
						$avgbasic = $sumavg1/$cnt1;
						
					}
					else
					{
						$avgbasic=0;
					}
						
						
					if(!empty($avgpro))
					{
						$cnt2 = count($avgpro);
						$sumavg2 = array_sum($avgpro);
						
						$avgpro = $sumavg2/$cnt2;
						
					}
					else
					{
						$avgpro=0;
					}
						
										
					$data['finalbbasic'] = $avgbbbasic;
					$data['finalbasic'] = $avgbasic;
					$data['finalproficient'] = $avgpro;					
					

					$unique_bar_values = array_unique($finalbar);
					 $finalbargraph = array_values(array_filter($unique_bar_values));
					
					$data['bargrph'] = $finalbar;
					$data['schname']= $schname;
					

					
				$data['cluster'] = $testcluster;
			 	$data['view_path']= $this->config->item('view_path');

			 	$this->load->view('report/multipletestreportob',$data);


			 }
		
			
			}


		}
		}

		}

	
	
		public function getquestions()
	{
	 $this->load->model('Reportschooltestmodel');
		$data['rec'] = $this->Reportschooltestmodel->getclusterbytestid($_REQUEST['testid']);
		echo '<option value="">Please Select</option>';
		foreach($data['rec'] as $kkk => $vvv)
		{

		echo '<option value="'.$vvv['test_cluster'].'">'.substr($vvv['test_cluster'],0,25).'</option>';
		}
		
	}
	function getsubjectbydistrict()
	{
	  $this->load->Model('dist_subjectmodel');
	  $data['subject']=$this->dist_subjectmodel->getdist_subjectsById();
		
		echo json_encode($data);
		exit;
	
	}
	function getsubjectbyteacher($date,$teacher_id)
	{
	  $this->load->Model('dist_subjectmodel');
	  $data['subject']=$this->dist_subjectmodel->getsubjectbyteacher($date,$teacher_id);
		
		echo json_encode($data);
		exit;
	
	}
function createpdf($teacher_id,$login_type,$login_id)
{

		$this->load->Model('observationplanmodel');		
		$observationplan_ans=$this->observationplanmodel->getallcompletedanswerplans($teacher_id);
		$data['status']=0;
		if($observationplan_ans!=false)
		{
		$observationplan=$this->observationplanmodel->getallplans();
		if($observationplan!=false)
		{
		  if($observationplan_ans!=false)
		  {
		    foreach($observationplan as $key=>$plan)
			{
            foreach($observationplan_ans as $ans)
			{
			  if($plan['observation_plan_id']==$ans['observation_plan_id'] && $ans['archived']=='' )
			  {
			    $observationplan[$key]['answer']=$ans['answer']; 
				 $observationplan[$key]['observation_ans_id']=$ans['observation_ans_id']; 
				  $observationplan[$key]['comment']=$ans['comment']; 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}

 
 
$view_path=$this->config->item('view_path');
   ob_start();
 

   $newphrase ='<div style="border:1px #E6B7BF solid;width:650px; padding:10px;">
		<table align="center">
				<tr>
		<td class="htitle">
		<font size="4px">Observation Plan</font>
		</td>
		</tr>
		</table>
		';
		 if($observationplan!=false) { 
		
		
		$newphrase.="<table  cellspacing='0' cellpadding='0' border='0' id='conf' width='100%' >";
		
		
		$an=0;
		foreach($observationplan as $plan) { 
		$newphrase.='<tr >
		<td >
       <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b>'.$plan['question'].'</b>
        </div>
		</td>
		</tr>';
		 if(isset($plan['answer'])) { 
		$newphrase.='<tr>
		<td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		'.$plan['answer'].'</div>
		</td></tr>';

		
		 } 
		
		 }  
		
		$newphrase.='</table>';
		
		} else { 	
		$newphrase.='<table>
		<tr>
		<td>
		No Data Found
		</td>
		</tr>
		</table>';
		 } 
			$newphrase.='</div>	';
  
   $str='<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
   </style>';
  
  $str.='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              
        </page_footer> 
		<HTML>
        <BODY style="font-size:10px;padding:10px;">		
		'.$newphrase.'		
		</BODY>
		</HTML>
   </page> 
';

   $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output(WORKSHOP_FILES.'observationplans/temp_'.$login_type.'_'.$login_id.'.pdf','F');
    $data['status']=1;
	}
	else
	{
	$data['status']=2;
	
	}
	echo json_encode($data);
	exit;



}

function createlessonplanpdf($teacher_id,$login_type,$login_id,$subject,$date)
{

		 $this->load->Model('lessonplanmodel');
		 $this->load->Model('dist_grademodel');
		 $this->load->Model('teachermodel');
		$lessonplans=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$lessonplansub=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		
		$getlessonplans=$this->lessonplanmodel->getlessonplansbysubjectandteacher($teacher_id,$date,$date,$subject);
		$gradename=$this->dist_grademodel->getdist_gradeById($getlessonplans[0]['grade']);
		
		$teachername=$this->teachermodel->getteacherById($teacher_id);
		$day = date('l', strtotime($date));
		if($day=='Monday')
		{
			$dayn=0;
		}
		if($day=='Tuesday')
		{
			$dayn=1;
		}
		if($day=='Wednesday')
		{
			$dayn=2;
		}
		if($day=='Thursday')
		{
			$dayn=3;
		}
		if($day=='Friday')
		{
			$dayn=4;
		}
		if($day=='Saturday')
		{
			$dayn=5;
		}
		if($day=='Sunday')
		{
			$dayn=6;
		}
		$this->load->Model('studentmodel');
		$total_students = $this->studentmodel->getstudentCount($dayn,$getlessonplans[0]['period_id'],$teacher_id);
		
		
		
		
		$le=$getlessonplans[0]['lesson_week_plan_id'];
		$start1=explode(':',$getlessonplans[0]['starttime']);
		$end1=explode(':',$getlessonplans[0]['endtime']);
		if($start1[0]>=12)
		{
		  if($start1[0]==12)
		  {
		    $start2=($start1[0]).':'.$start1[1].' pm';
		  
		  }
		  else
		  {
			$start2=($start1[0]-12).':'.$start1[1].' pm';
		  }	
		
		}
		else if($start1[0]==0)
		{
		  $start2=($start1[0]+12).':'.$start1[1].' am';
		
		}
		else
		{
		  $start2=($start1[0]).':'.$start1[1].' am';
		
		}
		if($end1[0]>=12)
		{
		  if($end1[0]==12)
		  {
			$end2=($end1[0]).':'.$end1[1].' pm';
		  }
		  else
		  {
			$end2=($end1[0]-12).':'.$end1[1].' pm';
		  }	
		
		}
		else if($end1[0]==0)
		{
		  $end2=($end1[0]+12).':'.$end1[1].' am';
		
		}
		else
		{
		  $end2=($end1[0]).':'.$end1[1].' am';
		
		}
		 
		
		$sh=0;
		$k=0;
		
		$view_path=$this->config->item('view_path');
		//$weekday = date('l', strtotime($date));
   ob_start();
		$newphrase='<table style="width:650px;border:2px #7FA54E solid;">
		<tr>
		<td style="width:175px;color:#CCCCCC;">
		<b>Teacher:</b>'.$teachername[0]['firstname'].' '.$teachername[0]['lastname'].'
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Period:</b>'.$start2.' to '.$end2.'
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Subject:</b>'.$getlessonplans[0]['subject_name'].'
		</td>
		<td style="width:175px;color:#CCCCCC;">
		<b>Grade:</b>'.$gradename[0]['grade_name'].'
		</td>
		</tr>
		<tr>
		<td>
		<br />
		</td>
		</tr>
		<tr>
		<td style="width:175px;color:#CCCCCC;">
		<b>Class Size:</b>
		'.$total_students.'</td>';
		$jkk=0;
		foreach($lessonplans as $val)
		{
		
		if($jkk==3)
		{
		$newphrase.='</tr><tr><td><br /></td></tr><tr>';
		
		}
		$newphrase.='<td style="width:175px;color:#CCCCCC;">
		<b>'.$val['tab'].':</b>
		'.$getlessonplans[$jkk]['subtab'].'</td>';
		$jkk++;
		
		}
		$newphrase.='</tr>
		</table>
		
		
		';
	
	$newphrase.='<br /><br />';
	$newphrase.='<table style="border:2px #7FA54E solid;">
		<tr><td style="width:718px;color:#CCCCCC;">Standard:'.$getlessonplans[0]['standard'].'</td></tr></table>';
	$newphrase.='<br /><br />';
		
		
		$this->load->Model('custom_differentiatedmodel');		
		$observationplan_ans=$this->custom_differentiatedmodel->getcustomdiff($le);
		$data['status']=0;
		if($observationplan_ans!=false)
		{
		$observationplan=$this->custom_differentiatedmodel->getallplans();
		if($observationplan!=false)
		{
		  if($observationplan_ans!=false)
		  {
		    foreach($observationplan as $key=>$plan)
			{
            foreach($observationplan_ans as $ans)
			{
			  if($plan['custom_differentiated_id']==$ans['custom_differentiated_id']  )
			  {
			    $observationplan[$key]['answer']=$ans['answer']; 				 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}

 
 

 

   /*$newphrase.='<table align="center" style="width:725px;">
				<tr>
		<td class="htitle">
		<font size="4px">Custom Differentiated Instructions</font>
		</td>
		</tr>
		</table>
		';*/
		 if($observationplan!=false) { 
		
		
		$newphrase.="<table  cellspacing='0' cellpadding='0' border='0' id='conf' style='width:725px;' >";
		
		
		$an=0;
		foreach($observationplan as $plan) { 
		$newphrase.='<tr >
		<td >
       <div  style=" font-size:15px;background-color:#f1f5ec;border:1px #dce6d0 solid; padding:5px; margin-top:15px;color:#7FA54E;">
		<b>'.$plan['tab'].'</b>
        </div>
		</td>
		</tr>';
		 if(isset($plan['answer'])) { 
		$newphrase.='<tr>
		<td ><div  style="font-size:12px;border-left:1px #dce6d0 solid; border-right:1px #dce6d0 solid; border-bottom:1px #dce6d0 solid;padding:5px; color:#000;">
		'.$plan['answer'].'</div>
		</td></tr>';

		
		 } 
		
		 }  
		
		$newphrase.='</table>';
		
		} else { 	
		$newphrase.='<table>
		<tr>
		<td>
		No Data Found
		</td>
		</tr>
		</table>';
		 } 
			$newphrase.='';
		}
		
   $str='<style type="text/css">
   .htitle{ font-size:16px; color:#08a5ce; margin-bottom:10px;}
   td{
   font-size:8px;
   }
   </style>';
   
  $str.='<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              
        </page_footer>
<div style="padding-left:550px;position:relative;">
		<img alt="Logo"  src="'.$view_path.'inc/logo/logo150.png"/>
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>'.ucfirst($this->session->userdata('district_name')).'</b>		
		<br /><b>'.ucfirst($teachername[0]['school_name']).'</b>';
		
		
		
		
	
		
		$str.='</div>
		</div>
		<br />				
		'.$newphrase.'				
   </page> 
';

   $html2pdf = new HTML2PDF('P','A4','en',false, 'ISO-8859-15', array(4, 20, 20, 20));
    $content = ob_get_clean();
	
   
	
	
	$html2pdf->WriteHTML($str);
    $html2pdf->Output(WORKSHOP_FILES.'observation_lessonplan/temp_'.$login_type.'_'.$login_id.'.pdf','F');
    $data['status']=1;
	
	
	echo json_encode($data);
	exit;



}

function createcompdf($report_id,$login_type,$login_id){


if ( file_exists(WORKSHOP_FILES.'observationplans/'.$report_id.'.pdf') )
    { 
		$data['status']=1;
	}
	else
	{
	
		if ( file_exists(WORKSHOP_FILES.'observationplans/temp_'.$login_type.'_'.$login_id.'.pdf') )
    { 
		copy(WORKSHOP_FILES.'observationplans/temp_'.$login_type.'_'.$login_id.'.pdf', WORKSHOP_FILES.'observationplans/'.$report_id.'.pdf');
		$data['status']=1;
	}
	else
	{
	$data['status']=0;
	}
	
	
	}
	echo json_encode($data);
	exit;
	
}
function createlessoncompdf($report_id,$login_type,$login_id){


if ( file_exists(WORKSHOP_FILES.'observation_lessonplan/'.$report_id.'.pdf') )
    { 
		$data['status']=1;
	}
	else
	{
	
		if ( file_exists(WORKSHOP_FILES.'observation_lessonplan/temp_'.$login_type.'_'.$login_id.'.pdf') )
    { 
		copy(WORKSHOP_FILES.'observation_lessonplan/temp_'.$login_type.'_'.$login_id.'.pdf', WORKSHOP_FILES.'observation_lessonplan/'.$report_id.'.pdf');
		$data['status']=1;
	}
	else
	{
	$data['status']=0;
	}
	
	
	}
	echo json_encode($data);
	exit;
	
}
	
}
?>	
