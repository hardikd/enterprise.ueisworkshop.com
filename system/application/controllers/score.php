<?php
/**
 * score Controller.
 *
 */
class Score extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('score/all',$data);
	  
	  
	
	}
	function getscores($page)
	{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('scoremodel');
		$total_records = $this->scoremodel->getscoreCount();
		
			$score = $this->scoremodel->getscores($page, $per_page);
		
		
		
		if($score!=FALSE){
			
			
			print "<div class='htitle'>Teacher Performance Rating</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >Rating Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($score as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['score_id'].'" class="'.$c.'" >';
			
				print '<td >'.$val['score_name'].'</td>
					   
				
				<td  nowrap><input class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="scoreedit('.$val['score_id'].')"><input  class="btnsmall" title="Delete" type="button" name="Delete" value="Delete" onclick="scoredelete('.$val['score_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'scores');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td >Rating Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Teacher Performance Rating Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'scores');
						print $pagination;	
		}
		
		
	}
	
	function getscoreinfo($score_id)
	{
		if(!empty($score_id))
	  {
		$this->load->Model('scoremodel');
		
		$data['score']=$this->scoremodel->getscoreById($score_id);
		$data['score']=$data['score'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_score()
	{
	
	
		$this->load->Model('scoremodel');
	
		
		$score=$this->scoremodel->add_score();
		if($score!=0){
		       $data['message']="score added Sucessfully" ;
			   $data['score']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['score']=0 ;
				
				
				}
		
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_score()
	{
	$this->load->Model('scoremodel');
	   
		$score=$this->scoremodel->update_score();
		if($score==true){
		       $data['message']="score Updated Sucessfully" ;
			   $data['score']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['score']=0 ;
				
				
				}
				
		echo json_encode($data);
		exit;		
	}
	
	function delete($score_id)
	{
		
		$this->load->Model('scoremodel');
		$result = $this->scoremodel->deletescore($score_id);
		if($result==true){
			$data['score']=1;
		}else{
			$data['score']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	