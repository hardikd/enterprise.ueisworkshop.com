<?php
/**
 * (Index Or Home Or Startup) page Controller.
 *
 */
class Index extends	Controller {

	function __construct()
	{
	
		parent::__construct();
		$this->no_cache();
               // print_r($this->session->all_userdata());exit;
	
	}

	protected function no_cache()
	{
		
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	} 


	function index($error_code=0)
	{

		if($error_code!=0){
			//Get Error description
			$data['error_msg'] = $this->getErrorDesc($error_code);
			//Get the posted data to repopulate the form
			$data['username']=$this->input->post('username');
			$data['password']=$this->input->post('password');
			}
		$data['view_path']=$this->config->item('view_path');
		//Loading view (File in: views/home/index.php)
                //print_r($this->session->all_userdata());exit;
                if ($this->session->userdata('login_type')=='teacher' && $this->session->userdata('teacher_id')) {
                   redirect('/teacherplan');
                }
                if ($this->session->userdata('login_type')=='observer' && $this->session->userdata('observer_id')) {
                   redirect(base_url().'index/observerindex');
                }
				if ($this->session->userdata('login_type')=='user' && $this->session->userdata('district_id')) {
                   redirect(base_url().'index/userindex');
                }

		$this->load->view('home/index',$data);
	}

	function district_cms(){
		$this->session->set_userdata('login_special',true);
		$this->schoolmanagement();
	}
	
	function login()
	{	

//            session_start();
		$this->load->helper('url');
		//$this->session->sess_destroy();
		
                include("sess.php");

//                echo $this->input->post('loginas');exit;
		if($this->input->post('loginas')=="district"){
			$this->schoolhome();
                } else if($this->input->post('loginas')=="district_management"){
			$this->schoolmanagement();
		}else if($this->input->post('loginas')=="teacher"){
			$this->teacherhome();
		}else if($this->input->post('loginas')=="observer"){
			$this->observerhome();
		}else if($this->input->post('loginas')=="parent"){
		$parent_newdata = array(
                   'parentusername'  => $_REQUEST['username'],
                   'parentpass'     => $_REQUEST['password'],
                   'loginas' => $_REQUEST['loginas'],
               );
		$this->session->set_userdata($parent_newdata);
		$this->loginall();
		}
		else if($this->input->post('loginas')=="student"){
			$student_newdata = array(
                   'studentusername'  => $_REQUEST['username'],
                   'studentpass'     => $_REQUEST['password'],
                   'loginas' => $_REQUEST['loginas'],
               );
		$this->session->set_userdata($student_newdata);
				$this->loginall();
		}
		else if($this->input->post('loginas')=="elsphpwebquiz"){
		
		
		$username = $_REQUEST['username'];
		$password= $_REQUEST['password'];
		$loginas = $_REQUEST['loginas'];
			
		$this->load->model('wapmodel');
		$data['record'] = $this->wapmodel->checklogin($username,$password);
                $_SESSION['district_id'] = $data['record'][0]['district_id'];
//                print_r($data['record'][0]['district_id']);exit;
                
		
		if(!empty($data['record']))
		{
			$student_newdata = array(
                   'studentusername'  => $_REQUEST['username'],
                   'studentpass'     => $_REQUEST['password'],
                   'loginas' => $_REQUEST['loginas'],
            );
		$this->session->set_userdata($student_newdata);
		
                $this->loginall();
		}
		else
		{
			$this->session->set_flashdata('invalidwap','Invalid username and password');
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('home/index',$data);
		}
		
		}
		else if($this->input->post('loginas')=="product_specialist"){
			$this->product_specialist();
		}
}

public function schoolmanagement($id=false){
    
//echo $id;
	$this->load->Model('districtmodel');
		
		
		if(@$id!=false && $this->session->userdata('product_login_type')=='Product Specialist')
		{

		$status = $this->districtmodel->is_valid_login($id);
		} else if (@$id!=false && $this->session->userdata('access_contet')==true){
			$status = $this->districtmodel->is_valid_login($id);
		}
		else
		{

		$status = $this->districtmodel->is_valid_login();
		
		}
		
//		echo 'test';exit;
		
		
		
		
		
		if($status!=false){
			//set the session
			 $today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$status[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			if($status[0]['type']=='demo' && $status[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($status[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$status[0]['district_id']);
			  }	
			
			
			}
			
			if($status[0]['type']=='demo' && $status[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['username']);
			 $this->session->set_userdata("demodist_user_id",$status[0]['dist_user_id']);
			 $this->session->set_userdata("demodistrict_id",$status[0]['district_id']);
			  $this->session->set_userdata("demodistrictname",$status[0]['districts_name']);
			 $this->load->Model('districtmodel');
		      $email = $this->districtmodel->getemail($status[0]['districts_name']);
			
			  $this->session->set_userdata("demoemail",$email[0]['email']);
			 $this->userdemo(30,'district');
			}
			else if($status[0]['type']=='demo'  && $days>=90 && $status[0]['username']!='bhasker73'   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['username']);
			$this->session->set_userdata("demodist_user_id",$status[0]['dist_user_id']);
			 $this->session->set_userdata("demodistrict_id",$status[0]['district_id']);
			  $this->session->set_userdata("demodistrictname",$status[0]['districts_name']);
			 $this->load->Model('districtmodel');
		      $email = $this->districtmodel->getemail($status[0]['districts_name']);
			
			  $this->session->set_userdata("demoemail",$email[0]['email']);
			 
			 if($status[0]['feedback']==0)
			 {
			    $this->userdemo(30,'district');
			 
			 }
			 else
			 {
			 	 
				$this->userdemo(90,'district');
			  }
			 
			}
			else
			{
			$this->session->set_userdata('login_special','district_management');	
			$this->session->set_userdata("login_type",'user');
			$this->session->set_userdata("username",$status[0]['username']);
			$this->session->set_userdata("dist_user_id",$status[0]['dist_user_id']);
			$this->session->set_userdata("district_id",$status[0]['district_id']);
			$this->session->set_userdata("district_name",$status[0]['districts_name']);			
			$this->session->set_userdata("dis_country_id",$status[0]['country_id']);
			$this->session->set_userdata("dis_state_id",$status[0]['state_id']);
			$this->session->set_userdata("avatar_id",$status[0]['avatar']);
			$this->session->set_userdata("LP",1);
			$this->session->set_userdata("NC",1);
			$this->session->set_userdata("TE",1);
			$this->session->set_userdata("login_required",'yes');
	
			$this->userindex();
			
			}
		}else{
			
			// give appropriate message.
			
			$this->index(1);
			return;
		}
}
	
		function product_specialist()
	{
		$this->load->Model('prod_specmodel');
		$status = $this->prod_specmodel->is_valid_login();
		if($status!=false)
		{
			$this->session->set_userdata("product_login_type",'Product Specialist');
			$this->session->set_userdata("product_username",$status[0]['firstname'].' '.$status[0]['lastname']);
			
		$data['districts'] = $this->prod_specmodel->getalldistprod_specs($status[0]['prod_spec_id']);
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('prod_spec/login',$data);
		
		}
		else
		{
			// give appropriate message.
			
			echo '<script>window.location = "'.REDIRECTURL.'product_specialist/index/1"</script>';
			//$this->index(4);
			//return;
		}
	}
	
	
	function loginall()
	{
	
		$this->load->Model('parentmodel');
				
		if($this->session->userdata('loginas')=='student')
		{
			/*
			$this->session->all_userdata();
			$this->session->set_userdata($data);
			include('sess_student.php');
			*/

			
		//	$url = @$_SERVER['HTTP_URI'].'/testbank/workshop/Quiz/login.php?s_msg=67';
		
		if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com")
		{
     		echo '<script>window.location = "https://dev.ueisworkshop.com/Quiz/login.php?s_msg=67"</script>';
		}
		else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com")
		{
   	  		echo '<script>window.location = "http://nanowebtech.com/testbank/workshop/Quiz/login.php?s_msg=67"</script>';
		}
		else if($_SERVER["HTTP_HOST"]=="localhost")
		{
			$arrFolderName = explode("/",$_SERVER['PHP_SELF']);
			$strLocalPath = $_SERVER['HTTP_HOST']."/".$arrFolderName[1]."/";
		 	echo '<script>window.location ="'.$strLocalPath.'Quiz/login.php?s_msg=67"</script>';
		}	
		
	
		
			
		}
		
		else if($this->session->userdata('loginas')=='elsphpwebquiz')
		{
			/*$this->session->all_userdata();
			$this->session->set_userdata($data);
			include('sess_student.php');
			*/
		//$url = @$_SERVER['HTTP_URI'].'/testbank/workshop/Quiz/login.php?s_msg=67';
//		print_r($_SESSION);exit;
			
	if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com")
		{
     		echo '<script>window.location = "https://enterprise.ueisworkshop.com/Quiz/login.php?s_msg=67"</script>';
		}
		else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com")
		{
     		echo '<script>window.location = "http://district.ueisworkshop.com/Quiz/login.php?s_msg=67"</script>';
		}
		else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com")
		{
   	  		echo '<script>window.location = "http://nanowebtech.com/testbank/workshop/Quiz/login.php?s_msg=67"</script>';
		}
		else if($_SERVER["HTTP_HOST"]=="localhost")
		{
		 	echo '<script>window.location = "http://localhost/testbank/workshop/Quiz/login.php?s_msg=67"</script>';
		}
		else if($_SERVER["HTTP_HOST"]=="ueis")
		{
     		/*echo '<script>window.location = "http://ueis/WAPTrunk/login.php?s_msg=67"</script>';*/
                    redirect('http://ueis/waptrunk/Quiz/login.php?s_msg=67');
		}	
		
			
			
		}
		
		else if($this->session->userdata('loginas')=='parent')
		{
			
			$username = $this->session->userdata('parentusername');
			
			$password = $this->session->userdata('parentpass');
		
		$status = $this->parentmodel->is_valid_login($username,$password);
		//echo $this->db->last_query();exit;
		
			if($status!=false){			
			if($status[0]['NC']==1)
			{
			$this->session->set_userdata("login_type",'parent');
			$this->session->set_userdata("username",$status[0]['firstname'].' '.$status[0]['lastname']);
			$this->session->set_userdata("login_id",$status[0]['parents_id']);						
			$this->session->set_userdata("school_id",$status[0]['school_id']);
			$this->session->set_userdata("district_id",$status[0]['district_id']);
			$this->session->set_userdata("LP",$status[0]['LP']);
			$this->session->set_userdata("NC",$status[0]['NC']);
			$this->session->set_userdata("TE",$status[0]['TE']);
			$this->session->set_userdata("login_required",'yes');
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('parent/home',$data);
			}
			else
			{
			/*
			$this->index(2);
			echo "Invalid username and password!";
			die();
			return;
			*/
			
			}
		
			
		}else{
			// give appropriate message.
			//$this->index(1);
		//	return;
	
		$this->index(3);
		}
	
		}
	}

	
	function copydistrict($fromdist_id,$todist_id)
	{
	   //$fromdist_id=$this->config->item('dist_id');
	   $this->load->Model('observationgroupmodel');
	   $this->load->Model('rubricscalemodel');
	   $this->load->Model('lickertgroupmodel');
	    $this->load->Model('proficiencygroupmodel');
		$this->load->Model('districtmodel');
	   $this->observationgroupmodel->copyobservationgroup($fromdist_id,$todist_id);
	   $this->rubricscalemodel->copyrubricscale($fromdist_id,$todist_id);
	   $this->lickertgroupmodel->copylickertgroup($fromdist_id,$todist_id);
	   $this->proficiencygroupmodel->copyproficiencygroup($fromdist_id,$todist_id);
	   $this->districtmodel->savecopydistrict($todist_id);
	   
	   
	  
	 
	  
	
	}
	function copygoalplan($fromdist_id,$todist_id)
	{
	   //$fromdist_id=$this->config->item('dist_id');
	   $this->load->Model('goalplanmodel');
	   
	   $this->goalplanmodel->copygoal($fromdist_id,$todist_id);  
	
	}
	function copymemorandums($fromdist_id,$todist_id)
	{
	   //$fromdist_id=$this->config->item('dist_id');
	   $this->load->Model('memorandummodel');
	   
	   $this->memorandummodel->copymemorandum($fromdist_id,$todist_id);	
	}
	
	
	
	function prod_spec_login()
	{
//print_r($_POST);exit;
		if(isset($_POST['dist_login']) && $_POST['dist_login']=='Login')
		{
		 
		 $this->schoolhome($_POST['district_user']);
		
		}
                if(isset($_POST['distcms_login']) && $_POST['distcms_login']=='Login'){
                    $this->schoolmanagement($_POST['districtcms_user']);
                }
		if(isset($_POST['ob_login']) && $_POST['ob_login']=='Login')
		{
		   $this->observerhome($_POST['observer']);
		
		}
		if(isset($_POST['te_login']) && $_POST['te_login']=='Login')
		{
		   $this->teacherhome($_POST['teacher']);
		
		}
	
	}
	
	
	
	
	function schoolhome($id=false)
	{
		
	$this->load->Model('districtmodel');
		
		
		if($id!=false && $this->session->userdata('product_login_type')=='Product Specialist')
		{
		$status = $this->districtmodel->is_valid_login($id);
		}
		else
		{
		$status = $this->districtmodel->is_valid_login();
		
		}
		
		
		
		
		
		
		
		if($status!=false){
			//set the session
			 $today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$status[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			if($status[0]['type']=='demo' && $status[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($status[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$status[0]['district_id']);
			  }	
			
			
			}
			
			if($status[0]['type']=='demo' && $status[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['username']);
			 $this->session->set_userdata("demodist_user_id",$status[0]['dist_user_id']);
			 $this->session->set_userdata("demodistrict_id",$status[0]['district_id']);
			  $this->session->set_userdata("demodistrictname",$status[0]['districts_name']);
			 $this->load->Model('districtmodel');
		      $email = $this->districtmodel->getemail($status[0]['districts_name']);
			
			  $this->session->set_userdata("demoemail",$email[0]['email']);
			 $this->userdemo(30,'district');
			}
			else if($status[0]['type']=='demo'  && $days>=90 && $status[0]['username']!='bhasker73'   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['username']);
			$this->session->set_userdata("demodist_user_id",$status[0]['dist_user_id']);
			 $this->session->set_userdata("demodistrict_id",$status[0]['district_id']);
			  $this->session->set_userdata("demodistrictname",$status[0]['districts_name']);
			 $this->load->Model('districtmodel');
		      $email = $this->districtmodel->getemail($status[0]['districts_name']);
			
			  $this->session->set_userdata("demoemail",$email[0]['email']);
			 
			 if($status[0]['feedback']==0)
			 {
			    $this->userdemo(30,'district');
			 
			 }
			 else
			 {
			 	 
				$this->userdemo(90,'district');
			  }
			 
			}
			else
			{
				
			$this->session->set_userdata("login_type",'user');
			$this->session->set_userdata("username",$status[0]['username']);
			$this->session->set_userdata("dist_user_id",$status[0]['dist_user_id']);
			$this->session->set_userdata("district_id",$status[0]['district_id']);
			$this->session->set_userdata("district_name",$status[0]['districts_name']);			
			$this->session->set_userdata("dis_country_id",$status[0]['country_id']);
			$this->session->set_userdata("dis_state_id",$status[0]['state_id']);
			$this->session->set_userdata("avatar_id",$status[0]['avatar']);
			$this->session->set_userdata("LP",1);
			$this->session->set_userdata("NC",1);
			$this->session->set_userdata("TE",1);
			$this->session->set_userdata("login_required",'yes');
			$this->userindex();
			}
		}else{
			
			// give appropriate message.
			
			$this->index(1);
			return;
		}
	
	
	}
	
	function userdemo($days,$type)
	{
		$data['view_path']=$this->config->item('view_path');
	  
	    $data['days']=$days;
		if($type=='district')
		{
			$this->load->view('demo/user',$data);
		}
        else if($type=='teacher')
		{
			$this->load->view('demo/teacher',$data);
		}
		else if($type=='observer')
		{
			$this->load->view('demo/observer',$data);
		}		
	
	
	}
	function teacherhome($id=false)
	{
	
	$this->load->Model('teachermodel');
		//$status = $this->teachermodel->is_valid_login();
		
	if($id!=false && $this->session->userdata('product_login_type')=='Product Specialist')
		{
		$status = $this->teachermodel->is_valid_login($id);
		}
		else
		{
		$status = $this->teachermodel->is_valid_login();
		
		}
		
		if($status!=false){

			$this->load->Model('districtmodel');
			$dist=$this->districtmodel->getdistrictIDByteacherID($status[0]['teacher_id']);
			$distdetails=$this->districtmodel->getdemodistrict($dist[0]['district_id']);
			$today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$distdetails[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			
			if($distdetails[0]['type']=='demo' && $distdetails[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($distdetails[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$dist[0]['district_id']);
			  }	
			
			
			}
			
			if($distdetails[0]['type']=='demo' && $distdetails[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['firstname'].' '.$status[0]['lastname']);
			 $this->userdemo(30,'teacher');
			}
			else if($distdetails[0]['type']=='demo'  && $days>=90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['firstname'].' '.$status[0]['lastname']);
			 if($distdetails[0]['feedback']==0)
			 {
			    $this->userdemo(30,'teacher');
			 
			 }
			 else
			 {
			 	 
				$this->userdemo(90,'teacher');
			  }	
			}
			else
			{
			
			//set the session
			
			$this->session->set_userdata("login_type",'teacher');
			$this->session->set_userdata("teacher_id",$status[0]['teacher_id']);
			$this->session->set_userdata("school_summ_id",$status[0]['school_id']);
			$this->session->set_userdata("school_id",$status[0]['school_id']);
			$this->session->set_userdata("LP",$status[0]['LP']);
			$this->session->set_userdata("NC",$status[0]['NC']);
			$this->session->set_userdata("TE",$status[0]['TE']);
			$this->session->set_userdata("school_self_name",$status[0]['school_name']);
			$this->session->set_userdata("teacher_name",$status[0]['firstname'].' '.$status[0]['lastname']);
			$this->session->set_userdata("avatar_id",$status[0]['avatar']);
			$this->session->set_userdata("district_id",$dist[0]['district_id']);
			$this->session->set_userdata("district_name",$status[0]['districts_name']);
			$this->session->set_userdata("login_required",'yes');

			$this->load->Model('curriculummodel');
			if(!$this->curriculummodel->checkcurriculum()){
				$this->load->Model('standardmodel');
	            if( $this->standardmodel->standardByDistrict())
					$this->curriculummodel->savecurriculumatlogin('standards');
				else 
					$this->curriculummodel->savecurriculumatlogin('materials');
			}
			
			$this->teacherindex();
			
			}
		}else{
			//give appropriate message.
			$this->index(2);
			return;
		}
	
	
	}
	
	function observerhome($id=false)
	{
	
	$this->load->Model('observermodel');
		
	if($id!=false && ($this->session->userdata('product_login_type')=='Product Specialist' || $this->session->userdata('login_type')=='user'))
		{
		$status = $this->observermodel->is_valid_login($id);
		}
		else
		{
		$status = $this->observermodel->is_valid_login();
		
		}
		
		
		if($status!=false){
			
			$this->load->Model('districtmodel');
			
			$distdetails=$this->districtmodel->getdemodistrict($status[0]['district_id']);
			$today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$distdetails[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			
			if($distdetails[0]['type']=='demo' && $distdetails[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($distdetails[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$status[0]['district_id']);
				$this->copygoalplan($from_district_id,$status[0]['district_id']);
			   $this->copymemorandums($from_district_id,$status[0]['district_id']);
			  }	
			   
			
			}
			
			if($distdetails[0]['type']=='demo' && $distdetails[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['observer_name']);
			 $this->userdemo(30,'observer');
			}
			else if($distdetails[0]['type']=='demo'  && $days>=90   )
			{			
			 $this->session->set_userdata("demousername",$status[0]['observer_name']);
			 if($distdetails[0]['feedback']==0)
			 {
			    $this->userdemo(30,'observer');
			 
			 }
			 else
			 {
			 	 
				$this->userdemo(90,'observer');
			  }	
			}
			else
			{
			//set the session
			
			$this->session->set_userdata("login_type",'observer');
			$this->session->set_userdata("observer_id",$status[0]['observer_id']);
			$this->session->set_userdata("observer_name",$status[0]['observer_name']);
			$this->session->set_userdata("observer_email",$status[0]['email']);
			$this->session->set_userdata("school_id",$status[0]['school_id']);
			$this->session->set_userdata("LP",$status[0]['LP']);
			$this->session->set_userdata("NC",$status[0]['NC']);
			$this->session->set_userdata("TE",$status[0]['TE']);
			$this->session->set_userdata("school_name",$status[0]['school_name']);
			$this->session->set_userdata("district_id",$status[0]['district_id']);
			@$this->session->set_userdata("district_name",$status[0]['districts_name']);
			$this->session->set_userdata("dis_country_id",$status[0]['country_id']);
			$this->session->set_userdata("dis_state_id",$status[0]['state_id']);
			$this->session->set_userdata("avatar_id",$status[0]['avatar']);
			$this->session->set_userdata("login_required",'yes');
			//$this->observerindex();
                        redirect(base_url().'index/observerindex');
			}
		}else{
			// give appropriate message.
			$this->index(2);
			return;
		}
	
	
	}
	
	function userindex()
	{
		
	$login_required=$this->session->userdata('login_required'); 
	if($login_required=='')
		{
			header("Location: index");  
		}
		else
		{				
	 if($this->session->userdata('login_type')=='user')
	  {
		 
	     /*$data['view_path']=$this->config->item('view_path');
	  
	  $this->load->Model('schoolmodel');
	  
	  $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	  
	  $this->load->view('district/home',$data);*/
	  
	  $data['view_path']=$this->config->item('view_path');	  
		$this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$this->load->Model('reportmodel');		
		$this->load->Model('school_typemodel');	
		$data['school_types'] = $this->school_typemodel->getallplans();
		if($data['school_types']!=false)
		{
		
          $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);	
		  $data['school_type'] = $data['school_types'][0]['school_type_id'];	
         
			  
		if(!empty($data['schools']))
		{
		  foreach($data['schools'] as $key=>$val)
		  {
		    
			$data['schools'][$key]['countteachers'] = $this->teachermodel->getteacherCount($val['school_id']);	
			
			$data['schools'][$key]['countlessonplans'] = $this->reportmodel->getplanbymonth($val['school_id']);	
			
			$data['schools'][$key]['countpdlc'] = $this->reportmodel->getpdlcbymonth($val['school_id']);	
			
			$data['schools'][$key]['checklist'] = $this->reportmodel->getchecklistbymonth($val['school_id']);	
			$data['schools'][$key]['scaled'] = $this->reportmodel->getscaledbymonth($val['school_id']);	
			$data['schools'][$key]['proficiency'] = $this->reportmodel->getproficiencybymonth($val['school_id']);	
			$data['schools'][$key]['likert'] = $this->reportmodel->getlikertbymonth($val['school_id']);	
			
		$data['schools'][$key]['notification'] = $this->reportmodel->getnotificationbymonth($val['school_id']);	
			$data['schools'][$key]['behaviour'] = $this->reportmodel->getbehaviourbymonth($val['school_id']);	
			
		  
		  }
		
		}
		}
         $data['menustyle'] = 'style="display: none;"';
            $data['idname']='';
            $data['view_path']=$this->config->item('view_path');
//            print_r($data);exit;
            if($this->session->userdata('login_special'))
                $this->load->view('district/dashboard',$data);		
            else 
                $this->load->view('district/index1',$data);		
		
	  
	  }
		}
	
	}
	
	function teacherindex()
	{	
		if($this->session->userdata('login_type')=='teacher')
	  {
		  /*$data['view_path']=$this->config->item('view_path');
		  
		  $this->load->view('teacher/home',$data);*/
		  
		  header('Location: '.REDIRECTURL.'teacherplan');
	  
	  }	
	
	
	}
	
	function observerindex()
	{
//	echo 'test';exit;

	  if($this->session->userdata('login_type')=='observer')
	  {
	  	$this->load->model('instructormodel');
		$lastLogin = $this->instructormodel->getinstructor();
	  	$diff =  abs(strtotime($lastLogin[0]['registerDate']) - strtotime(date("Y-m-d")));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
 
		if($this->session->userdata('popup')!=true){
	  		$data['show_option'] = true;
			$this->load->Model('videooptionmodel');
			$video = $this->videooptionmodel->insert();
			if($video[0]->popup_cntr>6){
				$data['show_option'] = true;
			} else if (!$this->session->userdata('popup')){
				$data['show_option'] = true;
			    $this->session->set_userdata('popup',true);
			}
			if(!isset($video[0]->popup_cntr) || $video[0]->popup_cntr<1){
				$data['popupcntr'] = 1;
			    $this->session->set_userdata('popupcntr',1);
			}
		}//else {
			$data['menustyle'] = 'style="display:none;"';
			$data['view_path']=$this->config->item('view_path');
	 		$this->load->view('observer/new_home',$data);
		//}
//	  header('Location: '.REDIRECTURL.'observer');
	  }	
	
	
	
	}
	function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}
	function getErrorDesc($error_code){
		switch($error_code){
			case "1":
				return "School username  or password are incorrect";
				break;
			case "2":
				return "Teacher username  or password are incorrect";
				break;
				
			case "3":
				return "Parent username  or password are incorrect";
				break;
			case "4":
				return "Product Specialist username  or password are incorrect";
				break;
			
		}
	}


	function parentchangepasswrd()
	{

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('parent/changepassword',$data);
			
	}
	
	function updatepassword()
	{
		$this->load->library('form_validation');
	
	
		$this->form_validation->set_rules('oldpass', 'Old Password', 'required');
		$this->form_validation->set_rules('npass', 'New Password', 'required|alpha');
		$this->form_validation->set_rules('cpass', 'Confirm Password', 'required|matches[npass]|');


	
	if($this->input->post('submit'))
	{
		if($this->form_validation->run() ==FALSE)
		{
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('parent/changepassword',$data);
		}
		else
		{
			
		$this->load->model('parentmodel'); 
	 	$data['query']=$this->parentmodel->GetUserProfile();
		
		$data['sql']= $this->parentmodel->UpdatePass($data);
		
		$this->session->set_flashdata('parent_success_password', 'You have successfully changed password');
		
		
		redirect('/index/parentchangepasswrd/');
		
		
		}
		 
	 }
		
	}

        function foldertree($link=''){
            $_POST['dir'] = urldecode($_POST['dir']);

            if( file_exists($root . $_POST['dir']) ) {
                    $files = scandir($root . $_POST['dir']);
                    natcasesort($files);
                    
                    if( count($files) > 2 ) { /* The 2 accounts for . and .. */
                            echo "<ul class=\"jqueryFileTree\" style=\"display: none;\">";
                            // All dirs
                            foreach( $files as $file ) {
                                    if( file_exists($root . $_POST['dir'] . $file) && $file != '.' && $file != '..' && is_dir($root . $_POST['dir'] . $file) ) {
                                            echo "<li class=\"directory collapsed\"><a href=\"#\" rel=\"" . htmlentities($_POST['dir'] . $file) . "/\">" . htmlentities($file) . "</a></li>";
                                    }
                            }
                            
                            // All files
                            foreach( $files as $file ) {
                                    if( file_exists($root . $_POST['dir'] . $file) && $file != '.' && $file != '..' && !is_dir($root . $_POST['dir'] . $file) ) {
                                            $ext = preg_replace('/^.*\./', '', $file);
                                            if($link!=''){
                                            echo "<li class=\"file ext_$ext\"><a href=\"\" rel=\"" . htmlentities(str_replace($_SERVER['DOCUMENT_ROOT'].'/',SITEURLM,$_POST['dir']) . $file) . "\">" . htmlentities($file) . "</a></li>";
                                            } else {
                                                echo "<li class=\"file ext_$ext\"><a href=\"\" rel=\"" . htmlentities(str_replace($_SERVER['DOCUMENT_ROOT'].'/',SITEURLM,$_POST['dir']) . $file). "\">" . htmlentities($file) . "</a></li>";
                                            }
                                    }
                            }
                            echo "</ul>";	
                    }else {
                echo "No Job Aides found";
            }
            } else {
                echo "No Job Aides found";
            }
        }

        function access_campus_data($id=false){
            if(isset($_POST['submit']) && $_POST['submit']=='Login')
		{
                
		   $this->observerhome($_POST['observer_id']);
		
		}
            $this->load->Model('schoolmodel');
            $this->load->Model('school_typemodel');
            $data['school_types'] = $this->school_typemodel->getallplans();
            $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);
            $data['view_path']=$this->config->item('view_path');
            $this->load->view('prod_spec/access_campus_data',$data);
        }
	
	 function video_player($videoname,$videotype=''){
			$data['videoname'] = $videoname;
			$data['videotype'] = $videotype;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('observer/video_player',$data);
		
	}

	function monthly_plan(){
		$data['menustyle'] = 'style="display:none;"';
		$data['view_path']=$this->config->item('view_path');
		 	$this->load->view('observer/monthly_plan',$data);
	}

	function annual_plan(){
		$data['menustyle'] = 'style="display:none;"';
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('observer/annual_plan',$data);
	}

	function payment($duration,$plan){
		error_reporting(1);
		if($_SERVER['HTTP_HOST']=='localhost'){
			include($_SERVER['DOCUMENT_ROOT'].'homeschool/payment/lib/Stripe.php');
			include($_SERVER['DOCUMENT_ROOT'].'homeschool/xmlrpc-2.0/lib/xmlrpc.inc');
		} else {
			include('payment/lib/Stripe.php');
			include('xmlrpc-2.0/lib/xmlrpc.inc');
		}

		###Set our Infusionsoft application as the client###
		$client = new xmlrpc_client("https://sq216.infusionsoft.com/api/xmlrpc");
		
		###Return Raw PHP Types###
		$client->return_type = "phpvals";

		###Dont bother with certificate verification###
		$client->setSSLVerifyPeer(FALSE);

		###Our API Key###
		$key = "655ef2d25d355ca89bfdb3839fa4fc71";


		if($this->input->post('stripeToken'))
		{
			//echo 'test';
		Stripe::setApiKey("sk_test_hDp9GdVQgxW9ouI4TKvsgT4y");
		$additional_quantity = 0;
	
					switch ($plan) {
				case 'planning':
					$main_plan = strtolower($plan).'_'.strtolower($duration);
					$additional_quantity = $this->input->post('quantity');
					if($duration=='yearly'){
						$subscription_amount = $additional_quantity*60;
					} else {
						$subscription_amount = $additional_quantity*6;
					}
				
					break;
				case 'implementation':
					$main_plan = strtolower($plan).'_'.strtolower($duration);
					$additional_quantity = $this->input->post('quantity');
					if($duration=='yearly'){
						$subscription_amount = $additional_quantity*90;
					} else {
						$subscription_amount = $additional_quantity*9;
					}
					break;
				case 'classroom':
					$main_plan = strtolower($plan).'_'.strtolower($duration);
					$additional_quantity = $this->input->post('quantity');
					if($duration=='yearly'){
						$subscription_amount = $additional_quantity*90;
					} else {
						$subscription_amount = $additional_quantity*9;
					}
					break;
				
				case 'attendance':
					$main_plan = strtolower($plan).'_'.strtolower($duration);
					$additional_quantity = $this->input->post('quantity');
					if($duration=='yearly'){
						$subscription_amount = $additional_quantity*60;
					} else {
						$subscription_amount = $additional_quantity*6;
					}
					break;
				
				case 'resource':
					$main_plan = strtolower($plan).'_'.strtolower($duration);
					$additional_quantity = $this->input->post('quantity');
					if($duration=='yearly'){
						$subscription_amount = $additional_quantity*195;
					} else {
						$subscription_amount = $additional_quantity*18;
					}
					break;
				
				
				default:
					$main_plan = strtolower($plan).'_'.strtolower($duration);
					$additional_quantity = 1;
					if($duration=='yearly'){
						$subscription_amount = $additional_quantity*195;
					} else {
						$subscription_amount = $additional_quantity*18;
					}
					break;
			}
		try {
		$customer = Stripe_Customer::create(array(
			"card" => $this->input->post('stripeToken'),
			"plan" => $main_plan,
			"email" => $this->session->userdata('observer_email'),
			"quantity" => $this->input->post('quantity')
		));
		}
		catch (Exception $e) {
			$error .= $e->getMessage();
		}
		if(!$error){
		$paymentId = $customer->id;		
				if($additional_plan){
					$additiona_cust = Stripe_Customer::retrieve($paymentId = $customer->id);
					$additiona_cust->subscriptions->create(array("plan" => $additional_plan,"quantity" => $additional_quantity));
				}

		//echo "<pre>";		
		//print_r($customer);exit;

		$this->load->Model('instructormodel');
		$updateinstructor = $this->instructormodel->updatepayment($paymentId,$this->input->post('plan'),$subscription_amount,$additional_quantity);
		
		$instructordetails = $this->instructormodel->getinstructor();
		//print_r($instructordetails);
			$ch = curl_init();
			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "http://client.ueisworkshop.com/index.php/webservice/add_observer/".$this->session->userdata('observer_email')."/".$instructordetails[0]["password"]."/".$instructordetails[0]["firstname"]."/".$instructordetails[0]["lastname"]);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			
			// grab URL and pass it to the browser
			curl_exec($ch);

			// close cURL resource, and free up system resources
			curl_close($ch);

		$this->load->library('email');
		//$this->load->library('security');
		if($updateinstructor){
			$this->load->library('email');
							$config['protocol'] = "smtp";
							$config['smtp_host'] = "ssl://smtpout.secureserver.net";
							$config['smtp_port'] = "465";
							$config['smtp_user'] = "admin@ueiscorp.com"; 
							$config['smtp_pass'] = "harddisk";
							$config['charset'] = "utf-8";
							$config['mailtype'] = "html";
							$config['newline'] = "\r\n";

            $this->email->initialize($config);
            if ($_SERVER['HTTP_HOST'] == 'localhost') {
                $filename = $_SERVER['DOCUMENT_ROOT'] . "homeschool/email_templates/payment_email.txt";
            } else {
                $filename = "email_templates/payment_email.txt";
            }
            $handle = fopen($filename, "r");
            $contents = fread($handle, filesize($filename));

            fclose($handle);
            $instructordetail = $this->instructormodel->getinstructor();
            $data['payment_date'] = date('M d, Y H:i T');
            $instructor_name = $instructordetail[0]['firstname'].' '.$instructordetail[0]['lastname'];
            $contents = str_replace('[[observer_id]]', $this->session->userdata('observer_id'), $contents);
            $contents = str_replace('[[name]]', $instructor_name, $contents);
            $contents = str_replace('[[email]]', $this->session->userdata('observer_email'), $contents);
            $contents = str_replace('[[amount]]', $subscription_amount, $contents);
            $contents = str_replace('[[form_of_payment]]', $customer->sources->data[0]->brand, $contents);
            $contents = str_replace('[[customer_id]]', $customer->id, $contents);
            $contents = str_replace('[[payment_date]]', $data['payment_date'], $contents);
            $contents = str_replace('[[payment_status]]', 'Approved', $contents);

            $this->email->from('admin@ueisworkshop.com', 'ueisworkshop admin');
            $this->email->reply_to('admin@ueisworkshop.com');
            $this->email->to(trim($this->session->userdata('observer_email')), $contents);
            $this->email->subject('Thank you for your payment!');
            $this->email->message($contents);
            $this->email->send();

		}
		$data['firstname'] = $instructordetail[0]['firstname'];
		$data['lastname'] = $instructordetail[0]['lastname'];
		$data['payment_amount'] = $subscription_amount;
		$data['form_of_payment'] = $customer->sources->data[0]->brand;
		$data['customer_id'] = $customer->id;
		$data['payment_status'] = 'Approved';



		$email = $this->session->userdata('observer_email');
		$selectedFields = array();
		$callcontanct = new xmlrpcmsg("ContactService.findByEmail", array(
			php_xmlrpc_encode($key), 		#The encrypted API key
			php_xmlrpc_encode($email),		#The tag ID
			php_xmlrpc_encode($selectedFields),		#The contact ID
			
		));
		
		$contactId = $client->send($callcontanct);
		$CID = $contactId->val[0]['Id'];
		$TID = 109;
		###Set up the call to add to the group###
		$call = new xmlrpcmsg("ContactService.addToGroup", array(
			php_xmlrpc_encode($key), 		#The encrypted API key
			php_xmlrpc_encode($CID),		#The contact ID
			php_xmlrpc_encode($TID),		#The tag ID
		));
		###Send the call###
		$result = $client->send($call);

		$RTID = 107;
		$callr = new xmlrpcmsg("ContactService.removeFromGroup", array(
			php_xmlrpc_encode($key), 		#The encrypted API key
			php_xmlrpc_encode($CID),		#The contact ID
			php_xmlrpc_encode($RTID),		#The tag ID
		));
		###Send the call###
		$result = $client->send($callr);

			/*if(!$result->faultCode()) {
				print "Tag " . $TID . " added to contact.";
				print "<BR>";
			} else {
				print $result->faultCode() . "<BR>";
				print $result->faultString() . "<BR>";
			}*/
			
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('observer/payment_thank',$data);
		} else {
			$this->session->set_flashdata('error',$error);
		}//end error	
					
		} else {
			switch ($plan) {
				case 'planning':
					if($duration=='yearly') {
						$price = 60;
					} else {
						$price = 6;
					}
					break;
				case 'implementation':
					if($duration=='yearly') {
						$price = 90;
					} else {
						$price = 9;
					}
					break;
				case 'classroom':
					if($duration=='yearly') {
						$price = 90;
					} else {
						$price = 9;
					}
					break;
				
				case 'attendance':
					if($duration=='yearly') {
						$price = 60;
					} else {
						$price = 6;
					}
					break;
				
				case 'resource':
					if($duration=='yearly') {
						$price = 195;
					} else {
						$price = 18;
					}
					break;
				
				
				default:
					if($duration=='yearly') {
						$price = 195;
					} else {
						$price = 18;
					}
					break;
			}
			$data['duration'] = $duration;
			$data['price'] = $price;
			$data['plan'] = $plan;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('observer/payment',$data);
		}
	}

	function subscribe($plan){
			$data['menustyle'] = 'style="display:none;"';
			switch ($plan) {
				case 'planning':
					$data['monthly'] = 6;
					$data['yearly'] = 60;
					break;
				case 'attendance':
					$data['monthly'] = 6;
					$data['yearly'] = 60;
				break;
				case 'implementation':
					$data['monthly'] = 6;
					$data['yearly'] = 60;
				break;
				case 'classroom':
					$data['monthly'] = 9;
					$data['yearly'] = 90;
				break;
				case 'resource':
					$data['monthly'] = 18;
					$data['yearly'] = 195;
				break;
				
				default:
					# code...
					break;
			}
			$data['plan'] = $plan;
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('observer/subscription_plan',$data);
	}
}
