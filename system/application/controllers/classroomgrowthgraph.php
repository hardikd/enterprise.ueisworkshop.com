<?php 
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
/**
 * Studentdetail Controller.
 *
 */

class Classroomgrowthgraph extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false && $this->is_teacher()==false ){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{  
        $data['idname']='tools';
	 	 error_reporting(1);
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('classroomgrowthgraphmodel');
			    $district_id = $this->session->userdata('district_id');
				$data['records'] = $this->classroomgrowthgraphmodel->getcategoryandgrade($district_id);
				$this->load->view('classroomgrowthgraph/index',$data);
				//print_r($_SESSION); 
	 	 }
		  
		  if($this->session->userdata("login_type")=='observer')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('classroomgrowthgraphmodel');
			    $observer_id = $this->session->userdata('observer_id');
				$data['records'] = $this->classroomgrowthgraphmodel->getcategoryandgradeObserver($observer_id);
				$this->load->view('classroomgrowthgraph/index',$data);
	 	 }
		 if($this->session->userdata("login_type")=='teacher')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('classroomgrowthgraphmodel');
			    $teacher_id = $this->session->userdata('teacher_id');
				$data['records'] = $this->classroomgrowthgraphmodel->getcategoryandclassofTeacher($teacher_id);
                                
				$this->load->view('classroomgrowthgraph/index',$data);
	 	 }
	}
	
 	function getclassroomHtml()
	{	
		error_reporting(0);
		 $school_id = $_REQUEST['school_id']; 
		$observer_id = $this->session->userdata('observer_id');
		
		 if($observer_id !='')
		 {
		 	$data = array();
			 $this->load->model('classroomgrowthgraphmodel');
 			 $data= $this->classroomgrowthgraphmodel->getobserverschool($observer_id);
			 $school_id = $data[0]['school_id']; 
 		 }
		 $grade_id= $_REQUEST['grade_id'];
	 
		 $data = array();
		 $this->load->model('classroomgrowthgraphmodel');
		 $data= $this->classroomgrowthgraphmodel->getschoolteacher($grade_id,$school_id);
		  $strClass ='';
		 $strClass .= '<div class="control-group"> <label class="control-label">Select Teacher :</label>
							<div class="controls">
				 <select class="span12 chzn-select"  tabindex="1" style="width: 300px;" name="class" id="class" onchange="updateclass(this.value)">
       			 <option value="-1"  selected="selected">-Please Select-</option>';
        
		
		   foreach($data as $key => $value)
			{
				 $strClass .='<option value="'.$value['teacher_id'].'">'.$value['firstname']." ".$value['lastname'].'</option>';
		 	}
         $strClass .= '</select> </div></div> ';
 		 echo $strClass;
  	}
	
	function getReportHtml()
	{
		 $cat_id= $_REQUEST['cat_id']; 
		 $school_id = $_REQUEST['school_id']; 
		 $grade_id= $_REQUEST['grade_id']; 
		 $class_id= $_REQUEST['class_id'];
		 $fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
	
 		$district_id = $this->session->userdata('district_id');
		$this->load->model('classroomgrowthgraphmodel');
		$data= $this->classroomgrowthgraphmodel->getGraphData($cat_id,$grade_id,$school_id,$class_id,$fDate,$tDate);		
		$IntRow = count($data);		
	 
	    $strResult ='';
		
		if(is_array($data))
		{
			//$_SESSION['graphdata'] = json_encode($data);
			$this->session->set_userdata('graph_json',json_encode($data));						
		 	exit;					
		}	
		else
		{
			$this->session->set_userdata('graph_json',"");						
		 	exit;
		}
 	}
 
}?>
