<?php
/**
 * teacher Controller.
 *
 */
class Addkinder extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false && $this->is_teacher()==false)
		{
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}		
	}
	
	function index()
	{
	
	
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('schoolmodel');
	   $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  if($data['district']!=false)
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict($data['district'][0]['district_id']);
	  }
	  else
	  {
	    $data['school']='';
	  }
	  
	  //$data['school']=$this->schoolmodel->getallschools();
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('addkinder/index',$data);
	  }
	  else if($this->session->userdata("login_type")=='teacher')
	  {	  
	  
		$this->load->Model('king_model');
		$data['grades'] = $this->king_model->retrieve_students();
		
		
		$data['view_path']=$this->config->item('view_path');		
	 	$this->load->view('king/all',$data);	
	  
	  
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('addkinder/all',$data);	
	  
	  
	  }
	  
	
	}
	function getteachers($page)
	{		
	
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('king_model');
		 $total_records = $this->king_model->getteacherCountbydistrict();
		
		$status = $this->king_model->getteachersbydistrict($page, $per_page);
				
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Add Scores</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >Student Name</td><td>Last Name</td><td>School</td><td>
			Grade</td><td>Subject</td><td>Obtained Marks</td><td>Total Marks</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['kindergarten_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['firstname'].'</td>
					  <td>'.$val['lastname'].'</td>
					  <td>'.@$val['school_id'].'</td>
					   <td>'.@$val['grade'].'</td><td>'.@$val['subject_id'].'</td><td>'.@$val['obtained_marks'].'</td>
					   <td>'.@$val['total_marks'].'</td>
				
				<td nowrap><input class="btnsmall" title="Edit" type="button"  value="Edit" name="Edit" onclick="studentedit('.@$val['kindergarten_id'].')">
				<input class="btnsmall" title="delete" type="button"  value="Delete" name="delete" onclick="studentdelete('.@$val['kindergarten_id'].')">
				</td>		</tr>';
				$i++;
				}
				print '</table>';
               
	//<input class="btnsmall" title="Delete" type="button"  name="Delete" value="Delete" onclick="teacherdelete('.$val['id'].','.$val['UserID'].')" >
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'teachers');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >Student Name</td><td>Last Name</td><td>School</td><td>
			Grade</td><td>Subject</td><td>Obtained Marks</td><td>Total Marks</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Record Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'teachers');
						print $pagination;	
		}
		
		
	}
	
	function getstudentinfo($teacher_id)
	{		
		if(!empty($teacher_id))
	  {
		$this->load->Model('king_model');
		
		$data['teacher']=$this->king_model->getteacherById($teacher_id);
		
		$data['teacher']=$data['teacher'][0];	
		
	
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_teacher()
	{
	
		$this->load->Model('king_model');
		$pstatus=$this->king_model->check_add_stud_exists();
	if($pstatus==1)
		 {
		$status=$this->king_model->add_teacher();
		if($status!=0){
		       $data['message']="teacher added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Teacher With Same FirstName In this School  Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="Student With Student Number  Already Exists" ;
		    $data['status']=0 ;
		}
		echo json_encode($data);
		exit;	
	
	}
	function update_student()
	{
		$this->load->Model('king_model');	    

		$status=$this->king_model->update_teacher();		
		
		
		if($status==true){
		       $data['message']="Updated Sucessfully" ;
			   $data['status']=1 ;	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		
		
			
		echo json_encode($data);
		
		exit;		
	}
	
	function delete_student($studentuserid)
	{
		 $teacher_id;
		 $studentuserid;
	
		$this->load->Model('king_model');
		$result = $this->king_model->deleteteacher($studentuserid);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getTeachersBySchool($school_id)
	{
	
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getTeachersBySchool($school_id);
		echo json_encode($data);
		exit;
	
	
	
	}
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
	function bulkupload()
	{
	   if($this->session->userdata("login_type")=='admin')
	  {
		 $this->load->Model('schoolmodel');
	   $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  if($data['district']!=false)
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict($data['district'][0]['district_id']);
	  }
	  else
	  {
	    $data['school']='';
	  }
	  
		  $data['view_path']=$this->config->item('view_path');
		  $this->load->view('teacher/bulkupload',$data);
	  }
	   else if($this->session->userdata("login_type")=='user')
	  {
		   $this->load->Model('schoolmodel');
		   $data['school']=$this->schoolmodel->getschoolbydistrict();
		   $data['view_path']=$this->config->item('view_path');
		   $this->load->view('teacher/bulkuploadall',$data);
	  
	  
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacher/bulkuploadall',$data);	
	  
	  
	  }
	
	}
	function sendfile()
	{
	
	if($this->input->post('countries'))
	{
		if($this->input->post('school')=='all' || $this->input->post('school')=='empty' )
		{
			$pdata['status']=2;
			$pdata['msg']='Please Select School.';
			echo json_encode($pdata);
	  	  exit;
		
		}
	
	}
	
	$pdata['status']=0;
	if($_FILES['upload']['size']>0 )
		{
			if($this->input->post('countries'))
			{
			$school_id=$_POST['school'];
			}
			else
			{
				$school_id=$_POST['school_id'];
			}
			$this->load->Model('teachermodel');
			
			$filename=explode('.',$_FILES['upload']['name']);
			$target_path = WORKSHOP_FILES."file_uploads/";
			$target_path = $target_path . basename( $_FILES['upload']['name']);
			if($filename[1]=='xlsx')
			{
			 $pdata['status']=2;
			 $pdata['msg']='Please Upload Excel 97-2003 ';
			 echo json_encode($pdata);
			 exit;
			
			
			
			}
			else if($filename[1]=='xls') 
			{
			$this->load->library('Spreadsheet_Excel_Reader');
			

			if(move_uploaded_file($_FILES['upload']['tmp_name'], $target_path))
			{
				




				//$this->spreadsheet_excel_reader->setOutputEncoding('CP1251'); // Set output Encoding.
				$this->spreadsheet_excel_reader->read($target_path); // relative path to .xls that was uploaded earlier

				$rows = $this->spreadsheet_excel_reader->sheets[0]['cells'];
				$cell_count = count($this->spreadsheet_excel_reader->sheets[0]['cells']);
				
				$row_count = $this->spreadsheet_excel_reader->sheets[0]['numCols'];
				$data['rows']=$row_count;
				$data['cols']=$cell_count;
				
				//for ($j = 1; $j <= $row_count; $j++) {
	
				for ($i = 2; $i <= $cell_count; $i++)
				{
					for($j=1;$j<=$row_count;$j++)
					{
						if(isset($rows[$i][$j]))
						{
							$text = $rows[$i][$j];
					       $data[$i][$j]=trim($text);
							//$data[$i][$j]=trim($rows[$i][$j]);
						}	
					}	
					
					

				}
				
				//echo $b="asdlsad";
				//}
				
				//print_r($data);
				//exit;
				//echo json_encode($data);
	  	       // exit;
				
				
			}
			
			}
			else if($filename[1]=='csv')
			{
			
			move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);
			
			 $row = 1;
		$file = fopen($target_path, "r");
		while (($fdata = fgetcsv($file)) !== FALSE) {
		  $num = count($fdata);
		  if(!empty($fdata))
		  $sdata[]=$fdata;
		  
		
		$row++;
		/*for ($c=0; $c < $num; $c++) {
        //echo $fdata[$c] . "<br>";
		 $mobile.=trim($fdata[$c]);*/
		 
		 
		}
		$data['cols']=count($sdata);
		
		//echo "<pre>";
		//print_r($sdata);
		//exit;
		$m=1;
        for ($i = 1; $i <count($sdata); $i++)
				{
			 		
					$pdata=$sdata[$i];	
					$data['rows']=count($pdata);
					$n=1;
					for($j=0;$j<count($pdata);$j++)
					{
					   $text =$pdata[$j];
					   $data[$m][$n]=trim($text);
					
					 $n++;
					}
					$m++;
					
					

				}
                //print_r($data); 				
				//exit;
	        
		 fclose($file);

        
				//echo json_encode($data);
	  	       // exit;
			
			
			}
			//echo '<pre>';
			//print_r($data);
			//exit;
			//$i=1;
			foreach($data as $key=>$val)
			{
			 if($key>0)
			 {
			 if(isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4]) )
			 {
			 if(!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4]) )
			 {
			    $pstatus=$this->teachermodel->check_teacher_exists($val[1],$val[3],$school_id);
				if($pstatus==1)
					 {
					    if($val[5]>0)
						{
							$this->teachermodel->add_bulk_teacher($val[1],$val[2],$school_id,$val[3],$val[4],$val[5],$val[6]);
						}
						else
						{
							$this->teachermodel->add_bulk_teacher($val[1],$val[2],$school_id,$val[3],$val[4],0,$val[6]);
						}	
					 
					 }
			 
			 }
			 
			 }
			 }
			
			// $i++;
			}
			$pdata['status']=1;
			
		}
	     echo json_encode($pdata);
	  	  exit;
		 
    }
}	