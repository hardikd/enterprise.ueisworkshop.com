<?php
 $path_file =  'libchart/classes/libchart.php';
 include_once($path_file);
 $view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');
/**
 * Studentdetail Controller.
 *
 */
class Selectedstudentreport extends	MY_Auth {
function __Construct()
	{
	
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false  && $this->is_teacher()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		
		$this->no_cache();
		
	}
	
	  function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
	
	function index()
	{
		
	 	 $data['idname']='tools';
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
		}
		else
		{
	 
	   if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
								
				$this->load->view('selectedstudentreport/index',$data);
	   
	 	 }
		else if($this->session->userdata("login_type")=='teacher')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				//$data['records'] = $this->Studentdetailmodel->getstuddetail();
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
				
				$teacher_id = $this->session->userdata('teacher_id');
				 $district_id = $this->session->userdata('district_id');
				$data['school_grade'] = $this->selectedstudentreportmodel->getteacher_schoolandgrade($teacher_id,$district_id);
				$this->load->Model('videooptionmodel');
	            $data['video_option'] = $this->videooptionmodel->get_option();
				
				//$this->load->view('studentdetail/studetail',$data);
					$this->load->view('selectedstudentreport/index',$data);
	   
	 	 }
		 
		 else if($this->session->userdata("login_type")=='observer')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				//$data['records'] = $this->Studentdetailmodel->getstuddetail();
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
				
				
				//$this->load->view('studentdetail/studetail',$data);
				 $district_id = $this->session->userdata('district_id');
				$observer_id = $this->session->userdata('observer_id');
				$data['school_grade'] = $this->selectedstudentreportmodel->getobserver_schoolandgrade($observer_id,$district_id);
				
				//$this->load->view('studentdetail/studetail',$data);
					$this->load->view('selectedstudentreport/index',$data);
	 	 }
	  
		}
	}


	function getschools()
	{
		$schooltypeid= $_REQUEST['school_type_id'];
		$this->load->model('selectedstudentreportmodel');
		$data['result']=$this->selectedstudentreportmodel->getallschools($schooltypeid);
		echo '<option value="">Please Select</option>';
		foreach($data['result'] as $key => $value)
		{
			
			echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
		}
		
	}
	
	
	function getgrades()
	{
		$schoolid = $_REQUEST['schoolid'];
		$this->load->model('selectedstudentreportmodel');
		$data['grades']=$this->selectedstudentreportmodel->getgrades();
		echo '<option value="">Please Select</option>';
		foreach($data['grades'] as $key => $value)
		{
			if(!empty($value) && $value!="")
			{
					echo '<option value="'.$value['dist_grade_id'].'">'.@$value['grade_name'].'</option>';
			}
		}
	}
	
	function pdf()
	{
		error_reporting(1);

		$studentid = $_REQUEST['stud_id'];
		$assignment_id = $_REQUEST['a_id'];
		$this->load->model('selectedstudentreportmodel'); 


		//get student according to userid
		$data['studentrecord']=$this->selectedstudentreportmodel->getrecord_student($studentid);
		
	 	if(!empty($data['studentrecord'][0]['Name']))
		{
			$studentname = $data['studentrecord'][0]['Name'].' '.$data['studentrecord'][0]['Surname'];
		}

		$classroom = $this->selectedstudentreportmodel->get_roomByStudent($data['studentrecord'][0]['student_id']);
	 	//print_r($classroom);exit;
	 	$data['room'] = $classroom[0]['name']; 
		$data['assignment'] = $this->selectedstudentreportmodel->getassessment($assignment_id);

 		$assignment_name = $data['assignment'][0]['assignment_name'];		
	 	$quiz_id = $data['assignment'][0]['quiz_id'];

	 	$quizdetails = $this->selectedstudentreportmodel->getassessmentreportbyid($assignment_id,$studentid);
	 	// print_r($quizdetails);exit;

	 	if($quizdetails[0]['finish_date']!=''){
	 		$data['assessmentdate'] = date('F d, Y',strtotime($quizdetails[0]['finish_date']));
	 	} else {
	 		$data['assessmentdate'] = date('F d, Y',strtotime($quizdetails[0]['added_date']));
	 	}

		 // require_once('tcpdf/config/lang/eng.php');
		 // require_once('tcpdf/tcpdf.php');
	 			
			// 	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, false, '', false,true);
                                
			// 	$pdf->SetCreator(PDF_CREATOR);
			// 	$pdf->SetAuthor('Workshop');
			// 	$pdf->SetTitle('Student Assessment Performance Report');
			// 	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Student Assessment Performance Report", PDF_HEADER_STRING);
			// 	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			// 	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			// 	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			// //	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			// 	$pdf->SetMargins(7,PDF_MARGIN_TOP,7);
			// 	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			// 	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			// 	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			// 	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			// 	$pdf->setLanguageArray($l);
			// 	$pdf->SetFont('times', '', 12);
			
			// 	$pdf->AddPage();
				$currdate = date('Y-m-d');
		// $html='	<table width="700">';
		// <tr> <td><table width="50%">
 	// 	<tr><td>Student Name:</td><td>'.$studentname.'</td></tr>
		//  <tr><td>Assessment Name:</td><td>'.$assignment_name.'</td></tr>
		// </table></td></tr>';
		$html = '';
		$data['studentname'] = $studentname;
		$data['assignment_name'] = $assignment_name;
		
		// $html .= '<tr><td align="center">  </td></tr>'; 
		// $cls = "LIKE 'Segment%'";
		// $html .= '<tr><td>'.$this->display_cluster($studentid,$assignment_id,$cls,'Segments').'</td></tr>';
		// $html .= '<tr style="width:700px;"><td align="center">   </td></tr>'; 
		// $cls = "LIKE 'S%labas con%'";
		// $html .= '<tr><td>'.$this->display_cluster_Oraciones($studentid,$assignment_id,$cls,'Sílabas con "a-e"').'</td></tr>';
		// $html .= '<tr><td align="center">   </td></tr>';
	 // 	$cls = "LIKE 'Palabras con%'";
		// $html .= '<tr><td>'.$this->display_cluster_Oraciones($studentid,$assignment_id,$cls,'Palabras con').'</td></tr>';
		// $html .= '<tr><td align="center">   </td></tr>';
		// $cls = "LIKE 'Oraciones con%'";
		// $html .= '<tr><td>'.$this->display_cluster_Oraciones($studentid,$assignment_id,$cls,'Oraciones').'</td></tr>';
		// $html .= "</table>";  

		$cls = "LIKE 'Segment%'";
		$html .= $this->display_cluster($studentid,$assignment_id,$cls,'Segments');
		$html .= '<br /><br />'; 
		$cls = "LIKE 'S%labas con%'";
		$html .= $this->display_cluster_Oraciones($studentid,$assignment_id,$cls,'Sílabas con "a-e"');
		$cls = "LIKE 'Palabras con%'";
		$html .= $this->display_cluster_Oraciones($studentid,$assignment_id,$cls,'Palabras con');
		$cls = "LIKE 'Oraciones con%'";
		$html .= $this->display_cluster_Oraciones($studentid,$assignment_id,$cls,'Oraciones');
		

		$data['html'] = $html;

		$data['schoolname'] = $this->selectedstudentreportmodel->getcschools($data['studentrecord'][0]['school_id']);
		
		$this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($data['studentrecord'][0]['student_id'], 'student');
        //print_r($this->session->all_userdata());
		if($this->session->userdata('login_type')=='teacher'){
            $data['teachername'] = $this->session->userdata('teacher_name');
        } else {
            $this->load->model('teachermodel');
            $teacher_data = $this->teachermodel->getteacherById($studentresult[0]['teacher_id']);
//            print_r($teacher_data);exit;
            $data['teachername'] = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        }
        //print_r($data['teachername']);exit;

        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(9);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(9);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdes != false) {

            $data['description'] = $reportdes[0]['tab'];
            $data['description_fontsize'] = $reportdes[0]['size'];
            $data['description_fontcolor'] = $reportdes[0]['color'];
        }
		$data['view_path'] = $view_path = $this->config->item('view_path');
	// $pdf->writeHTML($html, true, false, true, false, '');

	// 	$pdf->lastPage();
	// 	$pdf->Output($studentname.'_'.'Assessment_Report_'.$currdate.'.pdf', 'I');
		
		$this->output->enable_profiler(false);
        $this->load->library('parser');
//        print_r($data['teachername']);exit;
        $ostr = $this->parser->parse('selectedstudentreport/selectedstudentreportpdf', $data, TRUE);
         //echo $ostr;exit;
        $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(4, 20, 20, 2));
        //$html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();




        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();

		}
		
		function display_cluster($studentid,$assignment_id,$cluster,$disp_name)
		{
			$clustrehtml = '';
			
 			if($disp_name =='Sílabas con "a-e"' || $disp_name =='Palabras con')
			{
//                            echo utf8_decode($disp_name);exit;
			  	$clustrehtml .= '<table width="99%" class="gridtable"> <tr bgcolor="#cfcfcf"><td colspan="8" align="center">'.utf8_decode($disp_name).'&nbsp;&nbsp;&nbsp; <font color="#00FF00">With ease </font> &nbsp;<font color="#009900">With some ease </font>&nbsp;<font color="#FF3399">With difficulty </font>&nbsp;<font color="#FF0000">Cannot decode </font></td></tr>';
			}else if($disp_name == 'Segments'){
				$clustrehtml .= '<table width="99%" class="gridtable">';
			} else {
			 	$clustrehtml .= '<table width="99%" class="gridtable"> <tr bgcolor="#cfcfcf"><td colspan="8" align="center" >'.utf8_decode($disp_name).' </td></tr>';
			}
			//echo"<pre>";
			//print_r($clusterData);
			if($disp_name =='Palabras con')
			{
				$clusterData = $this->selectedstudentreportmodel->getClustersByClusterGroup2($assignment_id,$cluster);
				
		 		foreach($clusterData as $clus)
				{
					  $clustrehtml .= '<tr><td width="35%" style="font-size:11px;">'.str_replace('<font>','',$clus['test_cluster']).' </td>';
					  $clustrehtml .= $this->displayQuestion($studentid,$assignment_id,$clus['test_cluster']);
					  $clustrehtml .= '</tr>';
					  
					  $clustrehtml .= '<tr><td width="35%" style="font-size:11px;">'.str_replace('<font>','',$clus['answer_text']).' </td>';
					  $clustrehtml .= $this->displayQuestion_yes_no($studentid,$assignment_id,$clus['test_cluster']);
					  $clustrehtml .= '</tr>';
				}
			}
			elseif($disp_name =='Sílabas con "a-e"' || $disp_name =='Segments' || $disp_name =='Sílabas con')
			{	
				$width = '';
				if($disp_name == 'Segments')
				 {
					 $width = 16;
				 }
				 else if($disp_name == 'Sílabas con'){
					 $width = 25;
				 }
				  else if($disp_name == 'Sílabas con "a-e"'){
					 $width = 25;
				 }
				$clusterData = $this->selectedstudentreportmodel->getClustersByClusterGroup($assignment_id,$cluster);
//                                if($disp_name =='Sílabas con "a-e"'){
//                                    echo  $this->db->last_query();exit;
//                                }
				foreach($clusterData as $clus)
				{
					  $clustrehtml .= '<tr><td width="'.$width.'%" style="background:#939393;">'.utf8_decode(str_replace('SÃ­labas','Sílabas',$clus['test_cluster'])).' </td>';
					  $clustrehtml .= str_replace('<font>','',$this->displayQuestion($studentid,$assignment_id,$clus['test_cluster']));
					  $clustrehtml .= '</tr>';
				}
			} 
			if($disp_name =='Segments'){

				$clusterData = $this->selectedstudentreportmodel->getClustersByClusterGroup($assignment_id," = 'Foreign Letters'");
//                                if($disp_name =='Sílabas con "a-e"'){
//                                    echo  $this->db->last_query();exit;
//                                }
				foreach($clusterData as $clus)
				{
					  $clustrehtml .= '<tr><td width="'.$width.'%" style="background:#939393;">'.utf8_decode(str_replace('SÃ­labas','Sílabas',$clus['test_cluster'])).' </td>';
					  $clustrehtml .= str_replace('<font>','',$this->displayQuestion($studentid,$assignment_id,$clus['test_cluster']));
					  $clustrehtml .= '</tr>';
				}
			}
			$clustrehtml .= "</table>";
                        
			return $clustrehtml;
		}
		
		function display_cluster_Oraciones($studentid,$assignment_id,$cluster,$disp_name)
		{
			$clustrehtml = '';
			//$clustrehtml .= '<br /><table width="99%" border="1"> <tr bgcolor="#cfcfcf"><td colspan="8" align="center">'.$disp_name.' </td></tr>';
	 		 
			$clusterData = $this->selectedstudentreportmodel->getClustersByClusterGroup($assignment_id,$cluster);
                       // echo $this->db->last_query();exit;
			
			foreach($clusterData as $clus)
			{
				$clustrehtml .= '<table width="750" class="gridtable"> <tr bgcolor="#939393"> <td style="width:180px;background:#939393;"><b>Test Item</b></td><td align="center" style="width:50px;background:#939393;"><b>Correct</b></td><td style="width:50px;background:#939393;" align="center"><b>Incorrect</b></td><td style="width:335px;background:#939393;" align="center"><b>Comments</b></td></tr>';
				//$clustrehtml .= '<tr bgcolor="#cfcfcf"><td align="center">'.$disp_name.' </td></tr>';
				$clustrehtml .= '<tr> <td style="width:180px;"><b>'.(str_replace('Ã­','í',$clus['test_cluster'])).'</b></td><td align="center"><b></b></td><td align="center"><b></b></td><td style="width:325px;" align="center"><b></b></td></tr>';
				$clustrehtml .= $this->displayQuestion_Oraciones($studentid,$assignment_id,$clus['test_cluster']);
				$clustrehtml .= "</table><br /><br />";	 
			}
			
			
			return $clustrehtml;
		}
		
		
		
	
	function displayQuestion($studu_id ,$aid,$cluster)
	{
//			error_reporting(0);
		$this->load->model('selectedstudentreportmodel');
		
		$questions = array();
		$questions = $this->selectedstudentreportmodel->getquestionsdetail($aid,$cluster);
		$question_html = '';
		$qcntr = 0;
		foreach($questions as $val)
		{
			$ans = '';
			$qid = $val['id'];
			$disp ='';
			if(preg_match("/Segment/i",$cluster))
			 {
				 $que_name = strip_tags( $val['question_text'],"<span>");
			 }
			 else{
				 $que_name = strip_tags( $val['question_text']);
			 }
			 $que_name = str_replace('font-family: avant garde,avantgarde,century gothic,centurygothic,applegothic,sans-serif;','',$que_name);
			 $que_name = str_replace('font-family:avant garde,avantgarde,century gothic,centurygothic,applegothic,sans-serif;','',$que_name);
			 $que_name = str_replace("font-family: 'avant garde', avantgarde, 'century gothic', centurygothic, applegothic, sans-serif;","",$que_name);
			 $que_name = str_replace("72px;","36px;",$que_name);
			 $que_name = str_replace("24px;","18px;",$que_name);
			if($val['question_type_id']!= 6)
			{	
				 $queresult = $this->selectedstudentreportmodel->getquestionresult($aid,$qid,$studu_id);
				
				$font = '<font>';
				if(!empty($queresult[0]))
				{
					 if($queresult[0]['is_correct_ans'] == 1)
						 $ans = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
						$ans = 0;
						
					if($ans == 0)
					{
						$font = '<td align="center" height="40px" valign="middle" style="width:65px;color:#FF0000;font-size:36px;">'.str_replace('<font>','',$que_name).'</td>';//'<font color="#FF0000">'; 
						$qcntr++;
					}
					elseif($ans == 1)
					{
						$font = '<td align="center" height="40px" valign="middle" style="width:65px;color:#009900;font-size:36px;">'.str_replace('<font>','',$que_name).'</td>';//'<font color="#009900">'; 
						$qcntr++;
					} else {
						$font = '<td align="center" height="40px" valign="middle" style="width:65px;color:#009900;">&nbsp;</td>';//'<font color="#009900">';
						$qcntr++;
					}
				}
				if(preg_match("/Segment/i",$cluster))
					 {
						 $width = 12;
					 }
					 else{
						 $width = 15;
					 }
				 $disp = $font;//'<td width="'.$width.'%" align="center" height="50px" valign="middle">'.$font.''.$que_name.'</font></td>';
			}
			else 
			{
				 $queresult = $this->selectedstudentreportmodel->getquestionresult6($aid,$qid,$studu_id);
			//	 	echo"<pre>";
			//	print_r($queresult);
				$font = '<font>';
				if(!empty($queresult[0]))
				{
					if($queresult[0]['cant_decode'] == 1)
					{
						$font = '<td width="'.$width.'%" align="center" valign="middle" style="color:#FF0000;">'.$que_name.'</td>';//'<font color="#FF0000">';	
						$qcntr++;
					}
					elseif($queresult[0]['with_difficulty'] == 1)
					{
						$font = '<td width="'.$width.'%" align="center" valign="middle" style="color:#FF3399;">'.$que_name.'</td>';//'<font color="#FF3399">';
					$qcntr++;
					}
					 elseif(preg_match("/with ease/i",$queresult[0]['answer_text']))
					 {
						 $font = '<td width="'.$width.'%" align="center" valign="middle" style="color:#00FF00;">'.$que_name.'</td>';//'<font color="#00FF00">';
					 $qcntr++;
					 }							
					 elseif(preg_match("/some ease/i",$queresult[0]['answer_text']))
					 {
						 $font = '<td width="'.$width.'%" align="center" valign="middle" style="color:#009900;">'.$que_name.'</td>';//'<font color="#009900">';
					 $qcntr++;
					 }
				}
				 if(preg_match("/Palabras/i",$cluster))
				 {
					 $width = 13;
				 }
				 else{
					 $width = 15;
				 }
			 $disp = $font;//'<td width="'.$width.'%" align="center" valign="middle">'.$font.''.$que_name.'</font></td>';
			}
			
			$question_html .=  $disp;
			
		}
		 
		for($qcntr;$qcntr<=6;$qcntr++){
			$question_html .= '<td align="center" valign="middle" style="color:#009900;width:45px;">&nbsp;</td>';//'<font color="#009900">';
		}
		$qcntr = 0;
		$data['questionresult'] = $queresult;

		return $question_html;
	}
	function displayQuestion_yes_no($studu_id ,$aid,$cluster)
	{
//			error_reporting(0);
		$this->load->model('selectedstudentreportmodel');
		
		$questions = array();
		$questions = $this->selectedstudentreportmodel->getquestionsdetail($aid,$cluster);
		$question_html = '';
			
		foreach($questions as $val)
		{
			$ans = '';
			$qid = $val['id'];
			$disp ='';
   		 	 $queresult = $this->selectedstudentreportmodel->getquestionresult6yes_no($aid,$qid,$studu_id);
			 $font = '<font>';
	 		if(!empty($queresult[0]))
			{
				 if($queresult[0]['user_answer_text'] == 1)
				 {
					 $ans = 'Yes';
				 }
				 elseif($queresult[0]['user_answer_text'] == 0)
				 {
					 $ans = 'No';
				 }
				 
	 			if($queresult[0]['is_radio_ans_correct'] == 1)
				{
					$font = '<font color="#00FF00">';	
				}
				elseif($queresult[0]['is_radio_ans_correct'] == 0)
				{
					$font = '<font color="#FF0000">';
				}
			}
	 		$disp = '<td width="13%" align="center" valign="middle">'.$font.''.$ans.'</font></td>';
			
			$question_html .=  $disp;
 		}
	 	return $question_html;
	}
	
	function displayQuestion_Oraciones($studu_id ,$aid,$cluster)
		{
//			error_reporting(0);
			$view_path = $this->config->item('view_path');
			$this->load->model('selectedstudentreportmodel');
 			
			$questions = array();
			$questions = $this->selectedstudentreportmodel->getquestionsdetail($aid,$cluster);

   			$question_html = '';
			
			 foreach($questions as $val)
			{
 				$qid = $val['id'];
				$disp ='';
				
				$que_name = strip_tags( $val['question_text']);
				$que_name = ($que_name);
				$queresult = $this->selectedstudentreportmodel->getquestionresult6_new($aid,$qid,$studu_id);
				$questionnote = $this->selectedstudentreportmodel->getquestionnote($qid);
				if($questionnote) {
					$note = $questionnote[0]['user_note_text'];
				} else {
					$note = '';
				}
				// if($cluster=='Palabras con "a"'){
				// echo $this->db->last_query();
    //                     print_r($queresult);exit;
    //                 }
//                                print_r($queresult);exit;
                               // echo $this->db->last_query();//exit;
	 			$font = '<font>';
				$CannotDecode ='';$WithDifficulty='';$Withease='';$Withsomeease='';
				$select_text = 'Selected';
				/*if(!empty($queresult[0]))
				{
					if($queresult[0]['cant_decode'] == 1)
					{
						$CannotDecode = $select_text;
					}
					elseif($queresult[0]['with_difficulty'] == 1)
					{
						$WithDifficulty = $select_text;
					}
					 elseif(preg_match("/with ease/i",$queresult[0]['answer_text']))
					 {
						 $Withease = $select_text;
					 }							
					 elseif(preg_match("/some ease/i",$queresult[0]['answer_text']))
					 {
						 $Withsomeease = $select_text;
					 }
				}*/
				$correct = ' ';
				$incorrect = ' ';

				if(!empty($queresult[0]))
				{
					if($queresult[0]['is_correct_ans'] == 1)
					{
						$correct = '<img alt="Logo"  src="'.SITEURLM.$view_path.'inc/logo/tick.png'."\" style='position: relative;text-align:center;'/>";
					}
					else
					{
						$incorrect = '<img alt="Logo"  src="'.SITEURLM.$view_path.'inc/logo/tick.png'."\" style='position: relative;text-align:center;'/>";
					}
					 
				}

                $disp .= '<tr><td >'.$que_name.'</td><td align="center">'.$correct.'</td><td align="center">'.$incorrect.'</td><td align="left" style="width:225px;">'.$note.'</td></tr>';
				//$disp .= '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
				$question_html .=  $disp;
			}
			return $question_html;
 		}
	
	function getstudentacctoschool()
	{
		 $data['idname']='tools';
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
		redirect(base_url());
			//echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
		}
		else
		{
		if(isset($_REQUEST['schools']) && $_REQUEST['schools']!="")
		{
		@$schooltype = $_REQUEST['schools_type'];
		@$schoolid = $_REQUEST['schools'];
		@$gradeid = $_REQUEST['grades'];
		@$classroomid = $_REQUEST['classrooms'];
		}
		$this->load->model('selectedstudentreportmodel');
		
		//get all student according to selected school
		
		//$data['studentsids']=$this->selectedstudentreportmodel->getstud($classroomid);
		
		$data['students']=$this->selectedstudentreportmodel->getstud($classroomid,$schoolid,$gradeid);
		
		//$data['students']=$this->selectedstudentreportmodel->getstuddetails($data['studentsids'],$schoolid,$gradeid);
		//echo"<pre>" ; print_r($data['students']); exit;
	
	if(empty($data['students']))
		{
				$data['view_path']=$this->config->item('view_path');
				$this->load->view('selectedstudentreport/notfound',$data);
		}
		else
		{
			//foreach($data['students'] as $k=>$v)
			//{
				 $this->load->Model('schoolmodel');
        		$data['grades'] = $this->schoolmodel->getallgrades();
				
				$data['schoolname']=$this->selectedstudentreportmodel->getschoolname($schoolid);
				$data['view_path']=$this->config->item('view_path');
			//}
			$this->load->view('selectedstudentreport/searchstudent',$data);
		}
		
	}
		
		
	
	}
	
	function getindividual()
	{
		 $data['idname']='tools';
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
			
		if(!empty($_REQUEST['assessmentname']) && $_REQUEST['assessmentname']!='')
		{
			if(!empty($_REQUEST['userid']) && $_REQUEST['userid']!='')
			{
				 $testid = $_REQUEST['assessmentname'];
				 $userid = $_REQUEST['userid'];
			}
		}
				
				
	
	
		$this->load->model('selectedstudentreportmodel');
		//get student according to userid
		$data['individualtestdetail']=$this->selectedstudentreportmodel->getassessmentreportbyid($testid,$userid);
		
		if(!empty($data['individualtestdetail']))
		{
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('selectedstudentreport/individualstudentdetail',$data);
		}
		else
		{
				$data['view_path']=$this->config->item('view_path');
				$this->load->view('selectedstudentreport/notfound',$data);
		}
		
		
		}
 			
	}
	
	
	function getsinglestudentdetail($userid)
	{
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
	
		$this->load->model('studentdetailmodel');
		//get student according to userid
		$data['studentrecord']=$this->studentdetailmodel->getrecord_student($userid);
		
		if(!empty($data['studentrecord']) && $data['studentrecord']!="")
		{
		$data['schoolname']=$this->studentdetailmodel->getschoolname($data['studentrecord'][0]['school_id']);
		$data['countryname']=$this->studentdetailmodel->getcountry($data['studentrecord'][0]['country_id']);
		$data['districtname']=$this->studentdetailmodel->getdistrict($data['studentrecord'][0]['district_id']);
		}
		if(!empty($data['districtname'][0]['state_id']) && $data['districtname'][0]['state_id']!="")
		{
		$data['statename']=$this->studentdetailmodel->getstate($data['districtname'][0]['state_id']);
		}
		//get student performance detail according to their student id
		$data['studentperformance'] = $this->studentdetailmodel->getstuperformance($userid);
		if(empty($data['studentperformance']))
		{
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('studentdetail/recordnotfound',$data);	
		}
		else
		{
			
		$assessname =array();
		foreach($data['studentperformance'] as $k => $v)
		{
			$assessmentid = $v['assignment_id'];
			// get assessment name
			$assessname[] = $this->studentdetailmodel->getassessment($assessmentid);
		}
		$data['assessmentname'] = $assessname;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('studentdetail/individualstudentchart',$data);
		
		
		}
		
		}
	
	}
	
	function getteachers()
	{
		if(!empty($_REQUEST['grade_id']) && $_REQUEST['grade_id']!="")
		{
		 $gradeid = $_REQUEST['grade_id'];
		}
		if(!empty($_REQUEST['schid']) && $_REQUEST['schid']!="")
		{
		 $schoolid = $_REQUEST['schid'];
		}
		$this->load->model('selectedstudentreportmodel');
		$data['classrooms']=$this->selectedstudentreportmodel->getschoolteacher($gradeid,$schoolid);
	
		echo '<option value="">Please Select</option>';
		foreach($data['classrooms'] as $key => $value)
		{
			if(!empty($value) && $value!="")
			{
				echo '<option value="'.$value['teacher_id'].'">'.@$value['firstname']." ".@$value['lastname'].'</option>';
			}
		}
 	}
	
	function getindividualpdf()
	{
		
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
		if(isset($_REQUEST['quizid']) && $_REQUEST['quizid']!='')
		{
		$quiz_id = $_REQUEST['quizid'];
		$school_id=$_REQUEST['schoolid'];
		$studentid = $_REQUEST['studentid'];
		$assessmentid = $_REQUEST['assignmentid'];
		
	$this->load->model('selectedstudentreportmodel');
	
	@$userdetail =$this->selectedstudentreportmodel->getrecord_student($studentid);
	@$schoolname =$this->selectedstudentreportmodel->getschoolname($school_id);
	@$testname =$this->selectedstudentreportmodel->getassessment($assessmentid);
	@$questions =$this->selectedstudentreportmodel->getquestionsbyquizid($quiz_id);
	
	
	
	@$individualtestdetail=$this->selectedstudentreportmodel->getassessmentreportbyid($assessmentid,$studentid); 

		$this->load->model('proficiencymodel');
		$records = $this->proficiencymodel->getdata($assessmentid);
		
		if(!empty($records) && $records!="")
		{
			$bbfrom = $records[0]['bbasicfrom'];
			$bbto = $records[0]['bbasicto'];
			$basicfrom = $records[0]['basicfrom'];
			$basicto = $records[0]['basicto'];
			$pro_from = $records[0]['pro_from'];
			$pro_to = $records[0]['pro_to'];
				
		}
			
	
			require_once('tcpdf/config/lang/eng.php');
				require_once('tcpdf/tcpdf.php');
 				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('Workshop');
				$pdf->SetTitle('Student Detailed Report');
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Student Detailed Report", PDF_HEADER_STRING);
				$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
				$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				$pdf->setLanguageArray($l);
				$pdf->SetFont('times', '', 12);
				$pdf->AddPage();
				$currdate = date('Y-m-d');
				
				if(!empty($userdetail[0]["Name"]) && $userdetail[0]["Name"]!="")
				{
					 $studentname = $userdetail[0]["Name"];
				}
				if(!empty($userdetail[0]["grade_name"]) && $userdetail[0]["grade_name"]!="")
				{
				$grade_name= $userdetail[0]["grade_name"];
				}
				if(!empty($schoolname[0]["school_name"]) && $schoolname[0]["school_name"]!="")
				{
					 $school_name = $schoolname[0]["school_name"];
				}
				if(!empty($testname[0]["assignment_name"]) && $testname[0]["assignment_name"]!="")
				{
				 	 $assessment_name= $testname[0]["assignment_name"];
				}
				if(!empty($individualtestdetail[0]["pass_score_point"]) && $individualtestdetail[0]["pass_score_point"]!="")
				{
				
				 $scoreinpoint = $individualtestdetail[0]["pass_score_point"];
				 $scoreinper = $individualtestdetail[0]["pass_score_perc"];
				 $status = $individualtestdetail[0]["success"];
				}
				
				 if($status =="1"){ $st='Passed';}else{$st='Failed';}
				
			
					// Report Template starts here
$html='<fieldset>
<legend><h1>Student Report</h1></legend>
<div id="leftpanel">
<table cellpadding="5" cellspacing="0" width="100%">
<tr>
<td><strong>Student</strong></td><td>'.$studentname.'</td><td></td>
</tr>
<tr>
<td><strong>School Name</strong></td><td>'.$school_name.'</td><td></td>
</tr>
<tr>
<td><strong>Grade</strong></td><td>'.$grade_name.'</td>
</tr>
<tr>
<td><strong>Assessment Name</strong></td> <td>'.$assessment_name.'</td><td></td>
</tr>
<tr>
<td><strong>Number Correct</strong></td> <td>'.$scoreinpoint.'</td><td></td>
</tr>
<tr>
<td><strong>Percentage Correct</strong></td><td>'.$scoreinper.'%</td><td></td>
</tr>
<tr>
<td><strong>Status</strong></td><td>'.$st.'</td><td></td>
</tr>
</table>
</div>
<div id="rightpanel">
<div style="background-color:#657455; width:auto; color:#FFFFFF; padding:2px;"> Report Key</div>  
<table border="1px" cellpadding="5" cellspacing="0">
<tr>
 <td>Proficient</td><td>('.$pro_from.'% - '.$pro_to.'%)</td>
</tr>
<tr>
 <td>Basic</td><td>('.$basicfrom.'% - '.$basicto.'%)</td>
</tr>
<tr>
 <td>Below Basic</td><td>('.$bbfrom.'% - '.$bbto.'%)</td>
</tr>
</table>
</div>
</fieldset>';
$html .='
<fieldset style="border:1px">
<legend><h2>Standards Assessed</h2></legend>
<table width="100%" cellpadding="5" cellspacing="0" >
<tr style="background:#657455;">
<th><b>Standards Assessed</b></th><th><b>Questions</b></th></tr>';
if(!empty($questions))
		{
		for($i=0;$i<count($questions);$i++)
			{
				
$questionids = $this->selectedstudentreportmodel->getrepeatclusterinfo($questions[$i]["test_cluster"],$quiz_id);
				 $question_name=$questions[$i]["test_cluster"];
				
				$testitemcluster =substr($questions[$i]["test_cluster"],0,6);
				
				$html .='<tr bgcolor="#f2f2f2"><td>'.strip_tags($question_name).'</td><td>';
				
				//print_r($questionids);
				foreach($questionids as $kkk => $vvv)
		 		 {
			 	// $html .=$vvv['id'].',';
				$tt[] =$vvv['id'];
		 		 }
				 
			 	 $html .=count($tt);
		 		 $html .='</td></tr>';
				
			}
			$html .='</table></fieldset>';
		
		}
		else
			{
				$html .='The Student has not attempted any assessment yet.';	
			}

				$pdf->writeHTML($html, true, false, true, false, '');
				$pdf->lastPage();
				$pdf->Output($studentname.'_'.'Performance_Report_'.$currdate.'.pdf', 'I');
		}

	}


	}

	
	
}	