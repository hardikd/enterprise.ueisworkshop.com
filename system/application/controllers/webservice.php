<?php
/**
 * school Controller.
 *
 */
$view_path="system/application/views/";
require_once($view_path.'inc/html2pdf/html2pdf.class.php');

class Webservice extends	MY_Auth {
function __Construct()
	{
		
		parent::__construct();
	}


	 function changestatus($types)
	{		
		
		$this->load->Model('schoolmodel');
	  $school_id=$this->input->post('school_id');
	 
	  $type=$this->input->post('type');
	  
	  
	 
	  if($types=='inactive')
	  {
	    
		echo $status = $this->schoolmodel->changestatus(0,$type,$school_id);
	  
	  
	  }
	  else
      {
        
		echo $status = $this->schoolmodel->changestatus(1,$type,$school_id);

       }	  
	  
	
	}
	function add_school($username,$password,$firstname,$lastname,$school)
	{

		
		$school = str_replace(' ', '', $school);

		$school_name = substr($school, 0,5);

		//$school_name = $username;
	
		$this->load->Model('webservicemodel');
		$pstatus=$this->webservicemodel->check_school_exists($school_name,$username);
	
		 if($pstatus==1)
		 {	 
			$status=$this->webservicemodel->add_school($school_name,$password);

			if($status!=0){
					$ostatus = $this->webservicemodel->check_observer_exists($username);
						if($ostatus){
							$addobserver = $this->webservicemodel->add_observer($username,$password,$status,$firstname,$lastname); 
							$data['teachers'] = $addteacher = $this->webservicemodel->add_dummy_teacher($username,$password,$status,$firstname,$lastname,$school_name);
							$addperiod = $this->webservicemodel->add_periods($status);

							$addtimetable = $this->webservicemodel->time_table($status);
							
							$data['view_path'] = "system/application/views/";
							$this->output->enable_profiler(false);
					        $this->load->library('parser');
					        
					        $str = $this->parser->parse('home/teacher_pdf', $data, TRUE);

					        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
					        $content = ob_get_clean();
					        $html2pdf->WriteHTML($str);
					        $html2pdf->Output($_SERVER['DOCUMENT_ROOT'].'/'.$data['view_path'].'inc/logo/'.$school_name.'_teachers.pdf', 'F');
					       // exit;

							
		
							$data['message']="school & observer added Sucessfully" ;
					  		$data['status']=1 ;

					  		$this->load->library('email');
							//$config['protocol'] = 'sendmail';
							//$config['mailtype'] = 'html';
					  		$config['protocol'] = "smtp";
							$config['smtp_host'] = "ssl://smtpout.secureserver.net";
							$config['smtp_port'] = "465";
							$config['smtp_user'] = "admin@ueiscorp.com"; 
							$config['smtp_pass'] = "harddisk";
							$config['charset'] = "utf-8";
							$config['mailtype'] = "html";
							$config['newline'] = "\r\n";

							$filename = $_SERVER['DOCUMENT_ROOT'].'/email_templates/register.txt';
						
							$handle = fopen($filename, "r");
							$contents = fread($handle, filesize($filename));
							fclose($handle);
							$contents  = str_replace('[[firstname]]',$firstname,$contents);
							$contents  = str_replace('[[username]]',$username,$contents);
							$contents  = str_replace('[[password]]',$password,$contents);
							$contents  = str_replace('[[level]]','Campus',$contents);


							$this->email->initialize($config);
							$this->email->from('admin@ueiscorp.com', 'ueisworkshop admin');
							$this->email->reply_to('admin@ueiscorp.com', 'ueisworkshop admin');
							$this->email->to($username);
							$this->email->subject('Welcome to ueisworkshop.com');
							$this->email->message($contents);
							$this->email->attach($_SERVER['DOCUMENT_ROOT'].'/'.$data['view_path'].'inc/logo/'.$school_name.'_teachers.pdf');
							$this->email->send();
						}  else {
							$data['message']="Observer already exist" ;
					   		$data['status']=0 ;	
						}
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		}
		else if($pstatus==0)
		{
			$data['message']="School With Same Name   Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="School With User Name   Already Exists" ;
		    $data['status']=0 ;
		}		
		//echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_school()
	{
	$this->load->Model('schoolmodel');
	$pstatus=$this->schoolmodel->check_school_update();
	if($pstatus==1)
		 {		
	   $status=$this->schoolmodel->update_school();
		if($status==true){
		       $data['message']="school Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="School With Same Name   Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="School With User Name   Already Exists" ;
		    $data['status']=0 ;
		}	
		echo json_encode($data);
		exit;		
	}
	
	function delete($school_id)
	{
		
		$this->load->Model('schoolmodel');
		$result = $this->schoolmodel->deleteschool($school_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getAllschools()
	{
	  $this->load->Model('schoolmodel');
	  $data['school']=$this->schoolmodel->getallschools();
		
		echo json_encode($data);
		exit;
	
	}
	function getschoolbydistrict($id=false)
	{
	  $this->load->Model('schoolmodel');
	  $data['school']=$this->schoolmodel->getschoolbydistrict($id);
		
		echo json_encode($data);
		exit;
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
			/*			if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
			*/			
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	