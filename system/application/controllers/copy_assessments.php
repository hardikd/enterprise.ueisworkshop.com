<?php 

class Copy_assessments extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{ 
	 	// error_reporting(0);
		$data['view_path']=$this->config->item('view_path');
		$this->load->model('copy_assessmentsmodel');
		$data['dist'] = $this->copy_assessmentsmodel->getDistricts();
		$this->load->view('copy_assessments/index',$data);
 		 
	}
	
	function getassessmentselectionHtml()
	{
		error_reporting(0);
		 $dist_id = $_REQUEST['dist_id'];
		  $data = array();
		 $this->load->model('copy_assessmentsmodel');
		 $data= $this->copy_assessmentsmodel->getDistAssessments($dist_id);
		  $strClass ='';
		 $strClass .= '<td align="right" >Select Assessment : &nbsp;  </td> <td ><select class="combobox" name="assessment" id="assessment" onchange="getRemainDistict(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>';
    
		  if(count($data[0]['id'])>0)
		{
		   foreach($data as $value)
			{
				 $strClass .='<option value="'.$value['id'].'">'.$value['quiz_name'].'</option>';
		 	}
         	$strClass .= '</select> </td>';
		 }	
 		 echo $strClass; 
	 		
	}
	
	function getToDistrictselectionHtml()
	{
		//error_reporting(0);
		 $dist_id = $_REQUEST['dist_id'];
		  $data = array();
		 $this->load->model('copy_assessmentsmodel');
		 $data= $this->copy_assessmentsmodel->getToDistricts($dist_id);
		  $strClass ='';
		 $strClass .= '<td align="right" >To District : &nbsp; </td> <td ><select class="combobox" name="todistrict" id="todistrict" onchange="removeMsg()">
        <option value="-1"  selected="selected">-Please Select-</option>';
    
		  if(count($data[0]['district_id'])>0)
		{
		   foreach($data as $value)
			{
				 $strClass .='<option value="'.$value['district_id'].'">'.$value['districts_name'].'</option>';
		 	}
         	$strClass .= '</select> </td>';
		 }	
 		 echo $strClass; 
	 		
	}
	
	function copyAssessment()
	{
		//error_reporting(0);
		  $dist_id = $_REQUEST['dist_id'];
		  $to_dist = $_REQUEST['to_dist'];
		  $a_id = $_REQUEST['a_id'];
		 $this->load->model('copy_assessmentsmodel');
		 $success= $this->copy_assessmentsmodel->copyAssessments($dist_id,$to_dist,$a_id);
		$msg  = $success;
		 echo $msg; 
	 		
	}
	
	
	
}	//class end