<?php

include(MEDIA_PATH.'inc/foto_upload_script.php');
class Lesson_plan_material extends	MY_Auth {
	function __Construct()
	{
		parent::Controller();
		if($this->is_observer()==false  &&  $this->is_teacher()==false &&  $this->is_user()==false  ){
			//These functions are available only to admins - So redirect to the login page
			redirect("index");
		}
		
	}
	
	function index()
	{

	   $data['idname']='tools';
		$this->load->Model('dist_subjectmodel');
		$this->load->Model('dist_grademodel');
		 $data['view_path']=$this->config->item('view_path');
		$data['subjects']=$this->dist_subjectmodel->getdist_subjectsById();
		$data['grades']=$this->dist_grademodel->getdist_gradesById();	  
		if($this->session->userdata('login_type')=='user')
		{
			if($this->session->userdata("login_special")=='district_management') {	
			$data['idname']='lesson';
			}	
			$this->load->Model('schoolmodel');
			$data['school']=$this->schoolmodel->getschoolbydistrict();
		}
	
		$this->load->view('lesson_plan_material/index',$data);
	 
	}
	function mediaupload()
	{


	 if($_FILES['mediafileToUpload']['size']>0)
		 {
		 $foto_upload = new Foto_upload;	

		 $json['size'] = $_POST['MEDIA_MAX_FILE_SIZE'];
		
		$json['img'] = '';
//                echo BANK_TIME.WORKSHOP_FILES;exit;
		
		$foto_upload->upload_dir = BANK_TIME.WORKSHOP_FILES."medialibrary/lesson_plan/";
		
		$foto_upload->foto_folder = BANK_TIME.WORKSHOP_FILES."medialibrary/lesson_plan/";
		$foto_upload->extensions = array(".pdf");
		$foto_upload->language = "en";
		$foto_upload->x_max_size = 4800;
		$foto_upload->y_max_size = 3600;
		$foto_upload->x_max_thumb_size = 1200;
		$foto_upload->y_max_thumb_size = 1200;

		$foto_upload->the_temp_file = $_FILES['mediafileToUpload']['tmp_name'];
		
		$foto_upload->the_file = $_FILES['mediafileToUpload']['name'];
		$foto_upload->http_error = $_FILES['mediafileToUpload']['error'];
		$foto_upload->rename_file = true; 

		if ($foto_upload->upload()) {
        exec ("find /var/www/html/workshop_files/medialibrary/lesson_plan/ -type f -exec chmod 0777 {} +");           
			$foto_upload->process_image(false, false, false, 80);
			$json['img'] = $foto_upload->file_copy;
		$json['name'] = $foto_upload->the_file;
		} 

		$json['error'] = strip_tags($foto_upload->show_error_string());
		echo json_encode($json);
		}
		else if($_FILES['mediafileToUpload']['error']==2)
		{
		  $json['img']='';
		  $json['error']='Max File Size is 2 MB';
		 echo json_encode($json);
		}
		else
		{
		 $json['img']='';
		 $json['error']='Please Select a File To Upload';
		 echo json_encode($json);
		}
	
	}
	
	function media_upload()
	{
	if(isSet($_POST['school_id']))
		{			
		$school_id=$_POST['school_id'];
		$subject_id=$_POST['subject_id'];
		$grade_id=$_POST['grade_id'];
		$name=$_POST['filename'];
		$name=explode('.pdf',$name);		
		$file_path=$_POST['fileupload'];
		
		
		
		$this->load->Model('lesson_plan_materialmodel');
		$data=$this->lesson_plan_materialmodel->Insert_Media($school_id,$file_path,$subject_id,$grade_id,$name[0]);           
		}
	
	}
	function getmedia($page,$school,$search)
	{
	
	   $this->load->Model('lesson_plan_materialmodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_media_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_media_school_id',$school);
		
		}
		
		if($search!='empty')
	   {
	   $this->session->set_userdata('search_mediasearch_school_id',$search);
		
		}
		else
		{
		 $this->session->unset_userdata('search_mediasearch_school_id',$search);
		
		}
		$per_page = 12;
	   $total_records = $this->lesson_plan_materialmodel->getmediaCount();
		
			$status = $this->lesson_plan_materialmodel->getmedia($page, $per_page);
		
			//print_r($status);
			//exit;
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Lesson Plan Material</div>
			<table class='tabcontent' cellspacing='0' border='0' cellpadding='0' id='gallery' style='width:80%;' >
			
				";	
		
		
			$i=0;
			foreach($status as $val){
					
					
					$name=$val['name'];
					$subject_name=$val['subject_name'];
					$grade_name=$val['grade_name'];
					$text='<br /><b>Name:</b>&nbsp;'.$name.'.pdf';
					$text.='<br /><b>Subject:</b>&nbsp;'.$subject_name;
					$text.='<br /><b>Grade:</b>&nbsp;'.$grade_name.'<br />';
					
					
					
					
					if($i%2==0 || $i==0)
					{
					 print '<tr>';
	
					}
					
				
			    $siteurl=SITEURLM;
				$filename=explode('.',$val['file_path']);
				$file=$filename[1];
				
			if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'medialibrary/lesson_plan/'.$val['file_path'].' height="75px" width="100px">';
			}
			else
			{
			  if (file_exists(WORKSHOP_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg')) {
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg >';
				} else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'medialibrary/lesson_plan/'.$val['file_path'].'[0] '.WORKSHOP_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'mediathumb/lesson_plan/'.$filename[0].'.jpeg >';
				}
			  
			  
			
			}
			$file_path=WORKSHOP_DISPLAY_FILES.'medialibrary/lesson_plan/'.$val['file_path'];
            $newvar='<a style="color:#ffffff;" target="_blank" href='.$file_path.'>'.$fileas.'</a>'.$text;
			
			
				print '<td style="width: 40%;">'.$newvar.'<input class="btn btn-large btn-purple" title="Delete" type="button"  name="Delete" value="Delete" onclick="mediadelete('.$val['lesson_plan_material_id'].')" ></td>';
				
				
				$i++;
				if($i%2==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:535px;' ><tr ><td>No  Media Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
		}
	
	}
	
	function delete($id)
	{
		
		$this->load->Model('lesson_plan_materialmodel');
		$file = $this->lesson_plan_materialmodel->getfile($id);
                
		$result = $this->lesson_plan_materialmodel->delete($id);
		if($result==true){
			$data['status']=1;
			unlink(WORKSHOP_FILES.'medialibrary/lesson_plan/'.$file);
			$thumb=explode('.pdf',$file);
			unlink(WORKSHOP_FILES.'mediathumb/lesson_plan/'.$thumb[0].'.jpeg');
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	function getmaterial($subject_id,$grade_id,$school_id=false)
	{
		
		$this->load->Model('lesson_plan_materialmodel');
		$data['materials'] = $this->lesson_plan_materialmodel->getmaterial($subject_id,$grade_id,$school_id);		
		echo json_encode($data);
		exit;
		
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>&nbsp;&nbsp;</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>&nbsp;&nbsp;</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
	}