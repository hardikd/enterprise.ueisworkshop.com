<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ob_start();
$view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');

class Classroom extends MY_Auth {

    function __Construct() {
        parent::Controller();
        if ($this->is_teacher() == false && $this->is_observer() == false && $this->is_user() == false) {
            //These functions are available only to teachers - So redirect to the login page
            redirect("index");
        }
        $this->load->library('user_agent');
    }

    public function index() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/index', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/index', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_index', $data);
        }
    }

    
    public function progress_report() {
        if ($this->session->userdata('login_type') == 'teacher') {
			
			//$this->session->set_flashdata ('permission','Additional Permissions Required');
              //  redirect(base_url().'classroom');
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/progress_report', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
			$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'classroom');
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/progress_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
			$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'classroom');
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_progress_report', $data);
        }
    }

    public function success_team() {
        //	print_r($this->session->userdata('teacher_id'));
        // print_r($this->session->userdata('district_id'));exit;	

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $this->load->Model('videooptionmodel');
            $data['video_option'] = $this->videooptionmodel->get_option();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/success_team', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/success_team', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/success_team', $data);
        }
    }

    public function parent_teacher() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $this->load->Model('videooptionmodel');
            $data['video_option'] = $this->videooptionmodel->get_option();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/parent_teacher', $data);
        } elseif ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/parent_teacher', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/parent_teacher', $data);
        }
    }

    public function teacher_student() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/teacher_student', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/teacher_student', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/teacher_student', $data);
        }
    }

    public function behavior_record() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $this->load->Model('videooptionmodel');
            $data['video_option'] = $this->videooptionmodel->get_option();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/behavior_record', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/behavior_record', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/behavior_record', $data);
        }
    }

    public function report_list() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/teacher_report_list', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/observer_report_list', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/observer_report_list', $data);
        }
    }



    public function create_progress_report_type(){
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/progress_report_type', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/progress_report_type', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/progress_report_type', $data);
        }
    }

    public function create_progress_report() {
        $data['idname'] = 'classroom';
        $this->load->Model('schoolmodel');
        $this->load->Model('periodmodel');
		$this->load->Model('classroommodel');
        $this->load->Model('statements_standard_language_model');
		$data['standard_language'] = $this->statements_standard_language_model->get_all_data();
        $this->load->Model('statements_score_model');
		$data['statements_score'] = $this->statements_score_model->get_all_data();
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
         // $data['subject_id']
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/create_progress_report', $data);
    }

    public function create_progress_report_grade() {
        $data['idname'] = 'classroom';
        $this->load->Model('periodmodel');
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/create_progress_report_grade', $data);
    }



    function get_subject_period(){

    }
function grade_by_subject(){
		//$this->load->Model('standard_name_model');
		$this->load->Model('classroommodel');
		$data['subjects'] = $this->classroommodel->getgradeBysubject($this->input->post("grade_id"));

		echo json_encode($data);
	}
	
function grade_by_students(){
		$this->load->Model('classroommodel');
		$data['students'] = $this->classroommodel->getgradeBystudents($this->input->post("grade_id"));

		echo json_encode($data);
	}	

function get_student_by_teacher(){
		$this->load->Model('classroommodel');
		$data['students'] = $this->classroommodel->getteacherBystudents($this->input->post("teacher_id"));
		
		echo json_encode($data);
	}

function get_need_home_by_action_home(){
		$this->load->Model('actions_home_model');
		$data['action_data'] = $this->actions_home_model->get_need_home_by_action_home($this->input->post("needs_action_home"));
		//print_r($data['action_data']);exit;
		echo json_encode($data);
	}	

function get_need_school_by_action_school(){
		$this->load->Model('actions_school_model');
		$data['action_data'] = $this->actions_school_model->get_need_school_by_action_school($this->input->post("needs_action_school"));
		//print_r($data['action_data']);exit;
		echo json_encode($data);
	}	

function get_need_medical_by_action_medical(){
		$this->load->Model('actions_medical_model');
		$data['action_data'] = $this->actions_medical_model->get_need_medical_by_action_medical($this->input->post("needs_action_medical"));
		//print_r($data['action_data']);exit;
		echo json_encode($data);
	}		
	
    public function upload_progress_report() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/upload_progress_report', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_upload_progress_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_upload_progress_report', $data);
        }
    }

    public function retrieve_progress_report() {
        $this->load->Model('schoolmodel');
        $this->load->Model('periodmodel');
        $this->load->Model('classroommodel');
        $this->load->Model('statements_standard_language_model');
        $data['standard_language'] = $this->statements_standard_language_model->get_all_data();
        $this->load->Model('statements_score_model');
        $data['statements_score'] = $this->statements_score_model->get_all_data();
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
         // $data['subject_id']
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
        
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_progress_report', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_retrieve_progress_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_retrieve_progress_report', $data);
        }
    }

    public function create_success_report() {
		
//	if($this->session->userdata('login_type')=='teacher') { 
        $data['idname'] = 'classroom';
	   if($this->session->userdata("login_type")=='user')
		  	{
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
								
	}


        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();



        $this->load->Model('schoolmodel');
        $data['grades'] = $this->schoolmodel->getallgrades();

        $this->load->model('intervention_strategies_home_model');
        $data['strategies_home'] = $this->intervention_strategies_home_model->get_all_intervention_home();
    
        $this->load->model('intervention_strategies_school_model');
        $data['strategies_school'] = $this->intervention_strategies_school_model->get_all_intervention_school();


        $this->load->model('intervention_strategies_medical_model');
        $data['strategies_medical'] = $this->intervention_strategies_medical_model->get_all_intervention_medical();

        $this->load->model('actions_home_model');
		$data['action_home'] = $this->actions_home_model->get_all_action_home();

        $this->load->model('actions_school_model');
        $data['action_school'] = $this->actions_school_model->get_all_action_school();

        $this->load->model('actions_medical_model');
        $data['action_medical'] = $this->actions_medical_model->get_all_action_medical();

        $data['meeting_type'] = $this->intervention_strategies_home_model->get_meeting_type();
        $data['referral_reasons'] = $this->intervention_strategies_home_model->get_referral_reason();
        $data['progress_monitorings'] = $this->intervention_strategies_home_model->get_progress_monitoring();
        $data['program_changes'] = $this->intervention_strategies_home_model->get_program_change();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/create_success_report', $data);
    }

    public function upload_success_report() {
    //    if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';

            $this->load->Model('classroommodel');
            $data['update_report'] = $this->classroommodel->update_success_report();
			
			//print_r($data['update_report']);exit;

            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/upload_success_report', $data);
        /*} else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_upload_success_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_upload_success_report', $data);
        }*/
    }
	
	public function sst_retrieve_report() { 
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->view('classroom/sst_retrieve_report', $data);
    } else if ($this->session->userdata('login_type') == 'observer') {
            $this->load->view('classroom/sst_retrieve_report', $data);
    } else if($this->session->userdata('login_type') == 'user'){
        $this->load->view('classroom/dist_sst_retrieve_report', $data);
    }
	}
	

    public function retrieve_success_report() {
       
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
			
			

           
        
        if ($this->session->userdata('login_type') == 'teacher') {
             $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
//            print_r($data['need_homes']);exit;
            
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();
            
            $this->load->view('classroom/retrieve_success_report', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
             $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
//            print_r($data['need_homes']);exit;
            
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();
            
            $this->load->view('classroom/retrieve_success_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $this->load->model('Studentdetailmodel');
            
            $district_id = $this->session->userdata('district_id');
            $data['school_type'] = $this->Studentdetailmodel->getschooltype($district_id);
            
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->view('classroom/dist_retrieve_success_report', $data);
        }
    }

    public function create_parent_report() {


      //  if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();

			$data['schools'] = $this->schoolmodel->getschoolbydistrict();
			
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
			$this->load->Model('strengths_model');
			$this->load->Model('concerns_model');
			$this->load->Model('ideas_for_parent_student_model');
			
			
            $data['strengths'] = $this->strengths_model->get_all_strengths_name_data();
			
            $data['concerns'] = $this->concerns_model->get_all_concerns_name_data();
			
            $data['ideas_for_parent_student'] = $this->ideas_for_parent_student_model->get_all_ideas_parent_student_data();
			
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/create_parent_report', $data);
     /*   } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_create_parent_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_create_parent_report', $data);
        }*/
    }

    public function upload_parent_report() {
       // if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
			
			
			 $this->load->Model('classroommodel');
            $data['update_report'] = $this->classroommodel->get_parent_conference();
			
			//print_r($data['update_report']);exit;
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/upload_parent_report', $data);
        /*} else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_upload_parent_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_upload_parent_report', $data);
        }*/
    }

    public function retrieve_parent_report() {
        $data['idname'] = 'classroom';
	if($this->session->userdata("login_type")=='user') {
            $this->load->model('selectedstudentreportmodel');
            $district_id = $this->session->userdata('district_id');
            $data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
	} else if ($this->session->userdata("login_type")=='observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
        }
			
            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_parent_report', $data);
    }

    public function create_student_report() {
        //if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            
			$this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
		
			$data['schools'] = $this->schoolmodel->getschoolbydistrict();
			
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
            $this->load->Model('parentmodel');
            
			$data['students'] = $this->parentmodel->get_students_all();
            $this->load->Model('classroommodel');
        
			$this->load->Model('history_model');
			$this->load->Model('behavior_teacher_student_model');
			$this->load->Model('strategy_model');
			
			
            $data['historys'] = $this->history_model->get_all_history();
		
			
            $data['behaviors'] = $this->behavior_teacher_student_model->get_all_behavior();
				
            $data['strategys'] = $this->strategy_model->get_all_strategy();
					


			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/create_student_report', $data);
        /*} else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_create_student_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_create_student_report', $data);
        }*/
    }

    public function upload_student_report() {
        //if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
			
			 $this->load->Model('classroommodel');
            $data['update_report'] = $this->classroommodel->get_student_teacher_conference();
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/upload_student_report', $data);
      /*  } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_upload_student_report', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_upload_student_report', $data);
        }*/
    }


public function edit_student_report($id) {
        $data['idname'] = 'classroom';


        error_reporting(1);

            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
			$data['schools'] = $this->schoolmodel->getschoolbydistrict(); 		
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
            $this->load->Model('classroommodel');
			
			$this->load->Model('history_model');
			$this->load->Model('behavior_teacher_student_model');
			$this->load->Model('strategy_model');
			
			
            $data['historys'] = $this->history_model->get_all_history();
		
			
            $data['behaviors'] = $this->behavior_teacher_student_model->get_all_behavior();
				
            $data['strategys'] = $this->strategy_model->get_all_strategy();
			
			
			

//  	$data['retrieve_reports'] = $this->classroommodel->get_student_teacher_data($id);
	if($this->session->userdata('login_type')=='user'){
	$data['retrieve_reports'] = $this->classroommodel->get_student_teacher_data_user($id);
		} else {
	$data['retrieve_reports'] = $this->classroommodel->get_student_teacher_data($id);
		}
		
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/edit_student_report', $data);
    }

 public function edit_student_educator_conference_report() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
			$id = $this->input->post('id');
            $grade_id = $this->input->post('grade_id');
            $period_id = $this->input->post('period_id');
            $subject_id = $this->input->post('subject_id');
            $student_id = $this->input->post('student_id');
            $comment = $this->input->post('comment');
            $strength = $this->input->post('history');
            $concern = $this->input->post('behavior');
            $ideas_for_parent_student = $this->input->post('strategy_consequences');

            $history = implode(",", $strength);
            $behavior = implode(",", $concern);
            $strategy_consequences = implode(",", $ideas_for_parent_student);


   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			
		if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}


$data = array('user_type' => $this->session->userdata('login_type'), 'user_id' => $user_id,'id'=>$id, 'school_id' => $school_id, 'district_id' => $this->session->userdata('district_id'), 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'grade_id' => $grade_id, 'period_id' => $period_id, 'subject_id' => $subject_id, 'student_id' => $student_id, 'comment' => $comment, 'history' => $history, 'behavior' => $behavior, 'strategy_consequences' => $strategy_consequences, 'login_type' => $this->session->userdata('login_type'));

//print_r($data);exit;
            $this->load->model('classroommodel');
            $result = $this->classroommodel->update_data('student_educator_conference', $data,$data['id']);
        }


        $date = $data['date'] = $this->input->post('date');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];

        if ($result)
            $link = base_url() . 'classroom/student_conferencepdf/' . $student_id . '/' . $grade_id . '/' . $date . '/' . $id;

        $this->session->set_flashdata('link', $link);

        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/teacher_student');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

    public function retrieve_student_report() {
        //if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
			
        if($this->session->userdata("login_type")=='user') {
            $this->load->model('selectedstudentreportmodel');
            /*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
            $district_id = $this->session->userdata('district_id');

            $data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
            //$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
        } else if ($this->session->userdata("login_type")=='observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
        }
            
        $this->load->Model('schoolmodel');
        $this->load->Model('periodmodel');
        $data['subjects'] = $this->schoolmodel->getallsubjects();
        $data['grades'] = $this->schoolmodel->getallgrades();
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
        if($this->session->userdata('login_type')=='teacher'){
            $this->load->Model('studentmodel');
            $data['students'] = $this->studentmodel->GetStudentByTeacherId($this->session->userdata('teacher_id'),$this->session->userdata('school_id'));
        }else {
                $this->load->Model('parentmodel');
                $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->Model('classroommodel');
        $data['strengths'] = $this->classroommodel->strengths();
        $data['concerns'] = $this->classroommodel->concerns();
	$data['ideas_for_parent_student'] = $this->classroommodel->ideas_for_parent_student();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/retrieve_student_report', $data);
    }

      public function strengths_create_insert() {

        //print_r($this->input->post('action_home_responsible'));exit;
        $data['idname'] = 'classroom';
     
        if ($this->input->post('submit')) {
//            print_r($this->input->post('date'));exit;
            $datearr = explode("-",$this->input->post('date'));
            $date = $datearr[2].'-'.$datearr[0].'-'.$datearr[1];
            $student_id = $this->input->post('student_id');
            $grade_id = $this->input->post('grade_id');
			$schools_type = $this->input->post('schools_type');
			$teacher_id = $this->input->post('teacher_id');
            $meeting_type = $this->input->post('meeting_type');
            $referral_reason = $this->input->post('referral_reason');
            $progress_monitoring = $this->input->post('progress_monitoring');

            //step2
            $parent = $this->input->post('strengths_parent');
            $teacher = $this->input->post('strengths_teacher');
            $familys = $this->input->post('information_family');
            $schools = $this->input->post('information_school');
            $healths = $this->input->post('information_health');
            //step3
            $need_home = $this->input->post('needs_home');
            $need_school = $this->input->post('needs_school');
            $need_medical = $this->input->post('needs_medical');
			
			
			$needs_home_child = $this->input->post('needs_home_child');
			$needs_school_child = $this->input->post('needs_school_child');
			$needs_medical_child = $this->input->post('needs_medical_child');
            //step 4
            
            $needs_action_home = $this->input->post('needs_action_home');
//            print_r($needs_action_home);exit;
            $needs_action_school = $this->input->post('needs_action_school');
            $needs_action_medical = $this->input->post('needs_action_medical');
			
			$actions_home = $this->input->post('action_home');
            $actions_school = $this->input->post('action_school');
            $actions_medical = $this->input->post('action_medical');
            
           
            $action_home_responsible = $this->input->post('action_home_responsible');
            $action_school_responsible = $this->input->post('action_school_responsible');
            $action_medical_responsible = $this->input->post('action_medical_responsible');

            $action_home_date = $this->input->post('action_home_date');
            $action_school_date = $this->input->post('action_school_date');
            $action_medical_date = $this->input->post('action_medical_date');


            $program_change = $this->input->post('program_change');

            $nextdatearr = explode("/",$this->input->post('next_date'));
            //print_r($nextdatearr);exit;
            $nextdate = $nextdatearr[2].'-'.$nextdatearr[0].'-'.$nextdatearr[1];

            //$next_date = $this->input->post('next_date');


            
            
            


            $pear = implode("|", $parent);
            $teach = implode("|", $teacher);
            $family = implode("|", $familys);
            $school = implode("|", $schools);
            $health = implode("|", $healths);
            //Step 3
            $needs_home = implode("|", $need_home);
            $needs_school = implode("|", $need_school);
            $needs_medical = implode("|", $need_medical);
			
			$need_home_child = implode("|", $needs_home_child);
			$need_school_child = implode("|", $needs_school_child);
			$need_medical_child = implode("|", $needs_medical_child);
            
            //Step 4
            
            $action_home_need = implode("|",$needs_action_home);
            $action_school_need = implode("|",$needs_action_school);
            $action_medical_need = implode("|",$needs_action_medical);
            
            $action_home = implode("|", $actions_home);
            $action_school = implode("|", $actions_school);
            $action_medical = implode("|", $actions_medical);

            $action_home_responsibles = implode("|", $action_home_responsible);
            $action_school_responsibles = implode("|", $action_school_responsible);
            $action_medical_responsibles = implode("|", $action_medical_responsible);

            $action_home_date = implode("|", $action_home_date);
            $action_school_date = implode("|", $action_school_date);
            $action_medical_date = implode("|", $action_medical_date);
            
            
            
            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            } else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
 
            $data = array('user_type' => $this->session->userdata('login_type'), 
                        'user_id' => $user_id, 
                        'school_id' => $school_id,
                        'schools_type'=>$schools_type,
                        'teacher_id'=>$teacher_id, 
                        'meeting_type'=>$meeting_type,
                        'referral_reason'=>$referral_reason,
                        'progress_monitoring'=>$progress_monitoring,
                        'district_id' => $this->session->userdata('district_id'), 
                        'date' => $date, 
                        'student_id' => $student_id,
                         'grade_id' => $grade_id, 
                         'strengths_parent' => serialize($pear),
                          'strengths_teacher' => serialize($teach), 
                          'information_family' => serialize($family), 
                          'information_school' => serialize($school), 
                          'information_health' => serialize($health), 
                          'needs_home' => serialize($needs_home), 
                          'needs_school' => serialize($needs_school), 
                          'needs_medical' => serialize($needs_medical), 
                          'action_home' => serialize($action_home), 
                          'action_school' => serialize($action_school), 
                          'action_medical' => serialize($action_medical), 
                          'action_home_responsible' => serialize($action_home_responsibles), 
                          'action_school_responsible' => serialize($action_school_responsibles), 
                          'action_medical_responsible' => serialize($action_medical_responsibles),
                          'action_home_need'=>$action_home_need,
                          'action_school_need'=>$action_school_need,
                          'action_medical_need'=>$action_medical_need,
                          'needs_home_child'=>$need_home_child,
                          'needs_school_child'=>$need_school_child,
                          'needs_medical_child'=>$need_medical_child, 
                          'action_home_date' => serialize($action_home_date), 
                          'action_school_date' => serialize($action_school_date), 
                          'action_medical_date' => serialize($action_medical_date),
                          'program_change'=>$program_change,
                          'next_date'=>$nextdate);

//            echo "<pre>";
//print_r($data);exit;

            $this->load->model('classroommodel');
            $result = $this->classroommodel->sst_insert('student_success_strengths', $data);
          
        }

//        $date = $data['date'] = $this->input->post('date');
//        $datearr = explode('-', date('Y-m-d', strtotime($date)));
//        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];

        if ($result)
            $link = base_url() . 'classroom/createsstpdf/' . $user_id . '/' . $student_id . '/' . $date . '/' . $result;

        $this->session->set_flashdata('link', $link);
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/success_team');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }


    public function retrieve_success() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
            $student_id = $this->input->post('student_id');

            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            if($this->input->post('report_type')=='list'){
                $this->retrieve_success_data($grade_id, $student_id, $date);
            } else {
                $this->retrieve_success_graph($grade_id, $student_id, $date);
            }

        }
    }

    public function create_parent_insert() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
            $period_id = $this->input->post('period_id');
            $subject_id = $this->input->post('subject_id');
            $student_id = $this->input->post('student_id');
            $comment = $this->input->post('comment');
            $strength = $this->input->post('strengths');
            $concern = $this->input->post('concerns');
            $ideas_for_parent_student = $this->input->post('ideas_for_parent_student');


            $strengths = implode(",", $strength);
            $concerns = implode(",", $concern);
            $ideas_for_parent_students = implode(",", $ideas_for_parent_student);

    	   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}


if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}

            $data = array('user_type' => $this->session->userdata('login_type'), 'user_id' => $user_id,'district_id' => $this->session->userdata('district_id'), 'school_id' => $school_id, 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'grade_id' => $grade_id, 'period_id' => $period_id, 'subject_id' => $subject_id, 'student_id' => $student_id, 'comment' => $comment, 'strengths' => $strengths, 'concerns' => $concerns, 'ideas_for_parent_student' => $ideas_for_parent_students);

//print_r($data);exit;

            $this->load->model('classroommodel');
            $result = $this->classroommodel->parent_teacher_insert('parent_teacher_conference', $data);
        }


        $date = $data['date'] = $this->input->post('date');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];

        if ($result)
	
            $link = base_url() . 'classroom/parent_conferencepdf/' . $student_id . '/' . $grade_id . '/' . $date . '/' . $result;

        $this->session->set_flashdata('link', $link);


        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/parent_teacher');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

   public function update_parent_report() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
			$id = $this->input->post('id');
            $grade_id = $this->input->post('grade_id');
            $period_id = $this->input->post('period_id');
            $subject_id = $this->input->post('subject_id');
            $student_id = $this->input->post('student_id');
            $comment = $this->input->post('comment');
            $strength = $this->input->post('strengths');
            $concern = $this->input->post('concerns');
            $ideas_for_parent_student = $this->input->post('ideas_for_parent_student');

            $strengths = implode(",", $strength);
            $concerns = implode(",", $concern);
            $ideas_for_parent_students = implode(",", $ideas_for_parent_student);

       if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}

if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}

            $data = array('user_type' => $this->session->userdata('login_type'), 'user_id' => $user_id,'id'=>$id,'district_id' => $this->session->userdata('district_id'), 'school_id' => $school_id, 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'grade_id' => $grade_id, 'period_id' => $period_id, 'subject_id' => $subject_id, 'student_id' => $student_id, 'comment' => $comment, 'strengths' => $strengths, 'concerns' => $concerns, 'ideas_for_parent_student' => $ideas_for_parent_students);


            $this->load->model('classroommodel');
            $result = $this->classroommodel->update_data('parent_teacher_conference', $data,$data['id']);
        }


        $date = $data['date'] = $this->input->post('date');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];

        if ($result)
            $link = base_url() . 'classroom/parent_conferencepdf/' . $student_id . '/' . $grade_id . '/' . $date . '/' . $id;

        $this->session->set_flashdata('link', $link);


        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/parent_teacher');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }


    


    public function create_behavior_records() {
        $data['idname'] = 'classroom';
        error_reporting(1);

		
		  if($this->session->userdata('login_type')=='teacher'){
                $login_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $login_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $login_id = $this->session->userdata('dist_user_id');
			}
		
		
		
        $this->load->model('classroommodel');
        $result = $this->classroommodel->create_behavior_records($login_id);
        
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/behavior_record');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

    public function update_behavior_records() {
        $data['idname'] = 'classroom';
        error_reporting(1);
     if($this->session->userdata('login_type')=='teacher'){
                $login_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $login_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $login_id = $this->session->userdata('dist_user_id');
			}
        $this->load->model('classroommodel');
        $result = $this->classroommodel->update_behavior_records($login_id);

        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/behavior_record');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }
    public function delete_behavior_records1() {
        $data['idname'] = 'classroom';
        error_reporting(1);
     
	    $this->load->model('classroommodel');
        $result = $this->classroommodel->delete_behavior_records();

echo "DONE";

    }
function delete_behavior_records()
	{
		 $data['idname'] = 'classroom';
        error_reporting(1);
	    $this->load->model('classroommodel');
		$behavior_id = $this->input->post('behaviorid');
        $result = $this->classroommodel->delete_behavior_records($behavior_id);
		if($result==true){
			echo "DONE";
			} else {
				echo "ERROR";
				}
	}	



    public function create_behavior_insert() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');

            $victim_ids = $this->input->post('victim_ids');
            $suspect_ids = $this->input->post('suspect_ids');

            $problem_behaviour = $this->input->post('problem_behaviour');

            $incident = $this->input->post('incident');

            $possible_motivation = $this->input->post('possible_motivation');

            $interventions_prior = $this->input->post('interventions_prior');

            $student_has_multiple_referrals = $this->input->post('student_has_multiple_referrals');


            $victim_id = implode(",", $victim_ids);
            $suspect_id = implode(",", $suspect_ids);
            $problem_behaviours = implode(",", $problem_behaviour);
            $possible_motivations = implode(",", $possible_motivation);
            $interventions_priors = implode(",", $interventions_prior);
            $student_has_multiple_referral = implode(",", $student_has_multiple_referrals);



            $data = array('teacher_id' => $this->session->userdata('teacher_id'), 'school_id' => $this->session->userdata('school_id'), 'district_id' => $this->session->userdata('district_id'), 'login_type' => $this->session->userdata('login_type'), 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'victim_ids' => $victim_id, 'suspect_ids' => $suspect_id, 'problem_behaviour' => $problem_behaviours, 'incident' => $incident, 'possible_motivation' => $possible_motivations, 'interventions_prior' => $interventions_priors, 'student_has_multiple_referrals' => $student_has_multiple_referral);


            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('create_behavior_running_record', $data);
        }
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/behavior_record');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

    public function campus_create_behavior_insert() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
            $victim_ids = $this->input->post('victim_ids');
            $suspect_ids = $this->input->post('suspect_ids');
            $problem_behaviour = $this->input->post('problem_behaviour');
            $incident = $this->input->post('incident');
            $possible_motivation = $this->input->post('possible_motivation');
            $interventions_prior = $this->input->post('interventions_prior');
            $student_has_multiple_referrals = $this->input->post('student_has_multiple_referrals');


            $victim_id = implode(",", $victim_ids);
            $suspect_id = implode(",", $suspect_ids);
            $problem_behaviours = implode(",", $problem_behaviour);
            $possible_motivations = implode(",", $possible_motivation);
            $interventions_priors = implode(",", $interventions_prior);
            $student_has_multiple_referral = implode(",", $student_has_multiple_referrals);



            $data = array('observer_id' => $this->session->userdata('observer_id'), 'school_id' => $this->session->userdata('school_id'), 'district_id' => $this->session->userdata('district_id'), 'login_type' => $this->session->userdata('login_type'), 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'victim_ids' => $victim_id, 'suspect_ids' => $suspect_id, 'problem_behaviour' => $problem_behaviours, 'incident' => $incident, 'possible_motivation' => $possible_motivations, 'interventions_prior' => $interventions_priors, 'student_has_multiple_referrals' => $student_has_multiple_referral);


            $this->load->model('classroommodel');
            $result = $this->classroommodel->strengths_insert('create_behavior_running_record', $data);
        }
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/behavior_record');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

    public function student_educator_conference_insert() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
            $period_id = $this->input->post('period_id');
            $subject_id = $this->input->post('subject_id');
            $student_id = $this->input->post('student_id');
            $comment = $this->input->post('comment');
            $strength = $this->input->post('history');
            $concern = $this->input->post('behavior');
            $ideas_for_parent_student = $this->input->post('strategy_consequences');

            $history = implode(",", $strength);
            $behavior = implode(",", $concern);
            $strategy_consequences = implode(",", $ideas_for_parent_student);

   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			
	if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}


            $data = array('user_type' => $this->session->userdata('login_type'), 'user_id' => $user_id, 'school_id' =>$school_id, 'district_id' => $this->session->userdata('district_id'), 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))), 'grade_id' => $grade_id, 'period_id' => $period_id, 'subject_id' => $subject_id, 'student_id' => $student_id, 'comment' => $comment, 'history' => $history, 'behavior' => $behavior, 'strategy_consequences' => $strategy_consequences, 'login_type' => $this->session->userdata('login_type'));

//print_r($data);exit;

            $this->load->model('classroommodel');
            $result = $this->classroommodel->sst_insert('student_educator_conference', $data);
        }


        $date = $data['date'] = $this->input->post('date');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];

        if ($result)
            $link = base_url() . 'classroom/student_conferencepdf/' . $student_id . '/' . $grade_id . '/' . $date . '/' . $result;
//print_r($link);exit;

        $this->session->set_flashdata('link', $link);



        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/teacher_student');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

    public function create_behavior() {
        ini_set('display_errors', 1);
error_reporting(1);
//		if($this->session->userdata('login_type')=='teacher') { 
        $data['idname'] = 'classroom';

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();

        $data['possible_motivation'] = $this->problem_behaviour_model->get_all_possible_motivation_data();

        $data['interventions_prior'] = $this->problem_behaviour_model->get_all_interventions_prior_data();

        $data['student_has_multiple_referrals'] = $this->problem_behaviour_model->get_all_student_has_multiple_referrals_data();

        $this->load->Model('behavior_location_model');
        $data['behavior_location'] = $this->behavior_location_model->get_all_behavior_location_data();

		$this->load->Model('schoolmodel');
		$data['schools'] = $this->schoolmodel->getschoolbydistrict();

        //print_r($data['student_has_multiple_referrals']);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/create_behavior', $data);
//	}else if($this->session->userdata('login_type')=='observer') { 
//	
//		//print_r($this->session->all_userdata());exit;
//        $data['idname']='classroom';
//		
//		$this->load->model('problem_behaviour_model');
//		$data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
//		
//		$data['possible_motivation'] = $this->problem_behaviour_model->get_all_possible_motivation_data();
//		
//		$data['interventions_prior'] = $this->problem_behaviour_model->get_all_interventions_prior_data();
//		
//		$data['student_has_multiple_referrals'] = $this->problem_behaviour_model->get_all_student_has_multiple_referrals_data();
//		
//		
//        $data['view_path']=$this->config->item('view_path');
//        $this->load->view('classroom/cam_create_behavior',$data);
//	}else if($this->session->userdata('login_type')=='user') { 
//		 $data['idname']='classroom';
//        $data['view_path']=$this->config->item('view_path');
//        $this->load->view('classroom/dist_create_behavior',$data);
//    }
    }

    function autocomplete() {

        $this->load->model('parentmodel');
        $student = $this->parentmodel->entries();
        foreach ($student as $row):
            echo "<li class='suggestions suggestions_odd' title='" . $row->firstname . "' id='" . $row->student_id . "' onclick='select_student(" . $row->student_id . ");'>" . $row->firstname . " " . $row->lastname . " - " . $row->student_number . "</li>";
        endforeach;
    }

    function autocompleteuser() {
        $this->load->model('parentmodel');
        $students = $this->parentmodel->get_user_autocomplete();
        $users = array();
        foreach ($students as $student) {
            $usersarr['text'] = $student->firstname . ' ' . $student->lastname;
            if ($student->student_number)
                $usersarr['text'] .= ' - ' . $student->student_number;
            $usersarr['value'] = 'student_' . $student->student_id;
            $users[] = $usersarr;
        }

        $teachers = $this->parentmodel->get_teachers_autocomplete();

        foreach ($teachers as $teacher) {
            $usersarr['text'] = $teacher->firstname . ' ' . $teacher->lastname;
            if ($teacher->emp_number)
                $usersarr['text'] .= ' - ' . $teacher->emp_number;
            $usersarr['value'] = 'teacher_' . $teacher->teacher_id;
            $users[] = $usersarr;
        }



        echo json_encode($users);
    }

    public function edit_behavior() {
        $data['idname'] = 'classroom';
    	$this->load->model('classroommodel');
        $data['incident_details'] = $this->classroommodel->get_list_by_behavior();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/edit_behavior', $data);
    }

    public function edit_incident() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        $data['incident_details'] = $this->classroommodel->get_incident_list();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/edit_behavior', $data);
    }

    public function retrieve_behavior() {
        redirect(base_url().'classroom');
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_behavior', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/cam_retrieve_behavior', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/dist_retrieve_behavior', $data);
        }
    }

    //incident report
    public function incident_report() {
        $data['idname'] = 'classroom';
        // echo $this->session->userdata('login_type');exit;
	
		$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	    //if($this->session->userdata('login_type')=='teacher'){
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_report_incident', $data);
        //}
    }

    public function students_behavior() {
        $data['idname'] = 'classroom';
        // if($this->session->userdata('login_type')=='teacher'){

	$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
		   
	
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
		//print_r($data['students']);exit;

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/students_behavior', $data);
        //}
    }

    public function incident_students_behavior() {
        $data['idname'] = 'classroom';
        // if($this->session->userdata('login_type')=='teacher'){

    $this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
           
    
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
        //print_r($data['students']);exit;

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_incident_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/incident_students_behavior', $data);
        //}
    }
	
	function getallstudents()
	{
		 parse_str($_SERVER['QUERY_STRING'], $_GET); 
		 
		$q = strtolower($_GET["q"]);
		
		if (!$q) return;		
	
	$this->load->Model('parentmodel');
        $students = $this->parentmodel->get_students_all($q);
		//print_r($students);exit;
		if($students!=false)
		{	
			foreach($students as $student)
			{
				$sname = $student['firstname'].' '.$student['lastname'];			
				$sid = $student['student_id'];				
				echo "$sname|$sid\n";	
				
			}	
		
		}
		else
		{
		
			echo 'No Parents Found.|';
		}
		
			
	
	}	

    public function location_incident_report() {
        $data['idname'] = 'classroom';

        $this->load->Model('schoolmodel');
        $data['school'] = $this->schoolmodel->getschoolbydistrict();


        //print_r($data['school']);exit;
        $this->load->Model('behavior_location_model');
        $data['behavior_location'] = $this->behavior_location_model->get_all_behavior_location_data();
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/location_incident_report', $data);
    }

    public function time_incident_report() {
        $data['idname'] = 'classroom';
$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
		   
 $this->load->Model('periodmodel');
			$periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
	
//			$this->load->Model('parentmodel');
//            $data['students'] = $this->parentmodel->get_students_all();
//			
//           $this->load->model('problem_behaviour_model');
//	   $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/time_incident_report', $data);
    }

    public function number_of_behavior_staff_report() {
        $data['idname'] = 'classroom';

        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
	
		$this->load->Model('schoolmodel');
           $data['schools'] = $this->schoolmodel->getschoolbydistrict();
	
        $this->load->Model('teachermodel');
        $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/number_of_behavior_staff_report', $data);
    }

    public function month_report() {
		 if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'classroom/behavior_record');
        }
		
		 if ($this->session->userdata('login_type') == 'observer') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'classroom/behavior_record');
        }
        $data['idname'] = 'classroom';

        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/month_report', $data);
    }

    //fetch list of incident happened
    public function list_by_incident() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        if($this->input->post('reportType')=='graph'){
            if ($this->session->userdata('login_type') == 'user' && $this->input->post('school_id')=='all') {
                // echo 'test';exit;
                $this->graph_by_incident_district();
            
            } else {
                $this->graph_by_incident();
            }
        } else {
            $data['incident_list'] = $this->classroommodel->get_list_by_incident();
    //        print_r($data['incident_list']);exit;
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/behavior_record_list', $data);
        }
    }

    public function list_by_student() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        if($this->input->post('reportType')=='graph'){
            
            $this->graph_by_student();
        } else {
        $data['incident_list'] = $this->classroommodel->get_list_by_student();
        //echo $this->db->last_query();exit;

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_list', $data);
        }
    }

    public function incident_list_by_student() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        if($this->input->post('reportType')=='graph'){
            
            $this->graph_by_student();
        } else {
        $data['incident_list'] = $this->classroommodel->get_list_by_student();
        //echo $this->db->last_query();exit;

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_list', $data);
        }
    }

    public function location_by_list() {

	//$behavior_location = $this->input->post('behavior_location');


        $data['idname'] = 'classroom';
        if($this->input->post('reportType')!='graph'){
        $this->load->model('classroommodel');
        $data['incident_list'] = $this->classroommodel->get_list_by_location();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_list', $data);
        } else {
            $this->location_by_graph();
        }
        
    }

    public function time_by_list() {

//	$behavior_location = $this->input->post('behavior_location');


        $data['idname'] = 'classroom';

        $this->load->model('classroommodel');
        $data['incident_list'] = $this->classroommodel->get_list_by_time();
//		print_r($data['incident_list']);exit;

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_list', $data);
    }
	
	
	 public function time_by_list_report() {

//	$behavior_location = $this->input->post('behavior_location');


        $data['idname'] = 'classroom';

        $this->load->model('classroommodel');
        $data['incident_list'] = $this->classroommodel->get_list_by_time_report();
//		print_r($data['incident_list']);exit;

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_list', $data);
    }
	

	

    public function behavior_staff_by_list() {

//	$behavior_location = $this->input->post('behavior_location');

        $data['idname'] = 'classroom';
        if($this->input->post('reportType')=='graph'){
            $this->staff_by_graph();
        }else {
            $this->load->model('classroommodel');
            $data['incident_list'] = $this->classroommodel->get_list_by_behavior_staff();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/behavior_record_list', $data);
        }
    }

    //new function from workshop2
   function createsstpdf($teacher_id, $student_id, $date, $id) {
   // print_r($this->session->all_userdata());exit;
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classroommodel');
        $data['lessonplans'] = $this->lessonplanmodel->getalllessonplansnotsubject();
        $data['success_data'] = $this->classroommodel->sst_report_pdf($teacher_id, $student_id, $date, $id);
      // print_r($data['success_data']['need_home']);exit;

       $data['studentname'] = $data['success_data']['details']->firstname.' '.$data['success_data']['details']->lastname;
        if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($teacher_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($teacher_id);
        }
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(13);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(13);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }
        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
       // print_r($data);exit;
      //  print_r($this->session->all_userdata());exit;
        $str = $this->parser->parse('classroom/createsstpdf', $data, TRUE);
      //  echo $str;exit;
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }
    
    
    function progress_report_pdf($student_id, $grade_id, $date, $id) {
        error_reporting(1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

			
        $data['progress_report'] = $this->classroommodel->progress_report_pdf($student_id, $grade_id, $date, $id);
		//	$data['subjects'] = $this->schoolmodel->getallsubjects();	
		print_r($data['progress_report']);exit;
//	$data['subject_list']= $this->classroommodel->progress_subject_report();
	$this->load->Model('schoolmodel');
	$data['subject_list']= $this->schoolmodel->getallsubjects();
		$subject_mas = $data['subject_list'];
		foreach($subject_mas as $master)
		{
			$data['master_data'][$master['subject_id']][] = $this->classroommodel->progress_subject_report_pdf($master['subject_id']);
			
		}
		print_r($data['master_data']);
		exit;


		
			

          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
		
		 
		 
		  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }

        $data['view_path'] = $this->config->item('view_path');
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('classroom/create_progress_report_pdf', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }



    function parent_conferencepdf($student_id, $grade_id, $date, $id) {
        error_reporting(1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

        $data['parent_conference'] = $this->classroommodel->parent_conference_pdf($student_id, $grade_id, $date, $id);
        //print_r($data['parent_conference']);exit;

          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
            }
        
          if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
//        print_r($data);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(20);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {

            $dis = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['dis'] = $dis;
        
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('classroom/parent_conference_pdf', $data, TRUE);

//  $str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

   function student_conferencepdf($student_id, $grade_id, $date, $id) {
        error_reporting(1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classroommodel');
        


        $data['student_conference'] = $this->classroommodel->student_conference_pdf($student_id, $grade_id, $date, $id);


            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
            }

        
          if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else if($this->session->userdata('login_type') == 'observer') {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
        $data['view_path'] = $this->config->item('view_path');
         $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(21);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {

            $dis = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['dis'] = $dis;
        
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('classroom/student_conference_pdf', $data, TRUE);

//  $str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 5));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    public function retrieve_success_data($grade_id, $student_id, $date) {
        $data['idname'] = 'classroom';


        error_reporting(1);

        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

        $data['retrieve_reports'] = $this->classroommodel->retrieve_success_report_pdf($grade_id, $student_id, $date);
        //print_r($data['retrieve_report']);exit;
        if ($this->session->userdata("teacher_id")) {
            $teacher_id = $this->session->userdata("teacher_id");
        } else {
            $teacher_id = $this->input->post('teacher_id');
        }
        $data['teachername'] = $this->teachermodel->getteacherById($teacher_id);

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/student_success_report_list_ajax', $data);
    }

public function retrieve_parent_teacher() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
			$student_id = $this->input->post('student_id');
		
		
		if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}


            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
            $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
//            print_r($date);exit;
            $this->retrieve_parent_data($date, $grade_id, $student_id,$school_id);

//			$link = base_url().'classroom/retrieve_success_data/'.$grade_id.'/'.$student_id.'/'.$date.'';
            //redirect(base_url().'classroom/retrieve_success_report');	
        }
    }
	

    public function retrieve_parent_data($date, $grade_id, $student_id,$school_id) {
        $data['idname'] = 'classroom';

//echo 'test';exit;
        error_reporting(1);
		
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
								
	} else if ($this->session->userdata("login_type")=='observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
        }

        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
		
//             echo $date;exit;
        $data['retrieve_reports'] = $this->classroommodel->parent_conference_report_pdf($date, $grade_id, $student_id,$school_id);

//        print_r($data['retrieve_reports']);exit;
    	  if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			
			  if($this->session->userdata('login_type') == 'teacher'){
				 $this->load->Model('teachermodel');
				$data['teachername'] = $this->teachermodel->getteacherById($user_id);
			   
			}else {
				$this->load->Model('observermodel');
				$data['teachername'] = $this->observermodel->getobserverById($user_id);
			}

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/retrieve_parent_report', $data);
    }	

public function retrieve_student_teacher() {
	    $data['idname'] = 'classroom';
		
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
			$student_id = $this->input->post('student_id');
                      

	if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}

            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
            $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
            //print_r($date);exit;
          $link = $this->retrieve_student_data($student_id, $grade_id, $date,$school_id);

//			$link = base_url().'classroom/retrieve_success_data/'.$grade_id.'/'.$student_id.'/'.$date.'';
            //redirect(base_url().'classroom/retrieve_success_report');	
        }
    }

    public function retrieve_student_data($student_id, $grade_id, $date,$school_id) {
        $data['idname'] = 'classroom';

        error_reporting(1);

        if($this->session->userdata("login_type")=='user') {
            $this->load->model('selectedstudentreportmodel');
            $district_id = $this->session->userdata('district_id');
            $data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
        } else if ($this->session->userdata("login_type")=='observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
        }
        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');
		 $this->load->Model('schoolmodel');
	
        $data['retrieve_reports'] = $this->classroommodel->student_conference_report_pdf($student_id, $grade_id, $date,$school_id);
		
		
//		print_r($data['retrieve_reports']);exit;
		 $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
            if($this->session->userdata('login_type')=='teacher'){
                $this->load->Model('studentmodel');
                $data['students'] = $this->studentmodel->GetStudentByTeacherId($this->session->userdata('teacher_id'),$this->session->userdata('school_id'));
            }else {
                $this->load->Model('parentmodel');
                $data['students'] = $this->parentmodel->get_students_all();
            }
            $this->load->Model('classroommodel');
            $data['strengths'] = $this->classroommodel->strengths();
            $data['concerns'] = $this->classroommodel->concerns();
			
            $data['ideas_for_parent_student'] = $this->classroommodel->ideas_for_parent_student();

//        print_r($data['retrieve_report']);exit;
       if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			   
	 if($this->session->userdata('login_type') == 'teacher'){
				 $this->load->Model('teachermodel');
				$data['teachername'] = $this->teachermodel->getteacherById($user_id);
		}else if($this->session->userdata('login_type') == 'observer'){
				$this->load->Model('observermodel');
				$data['teachername'] = $this->observermodel->getobserverById($user_id);
			}
			
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/retrieve_student_report', $data);
    }
    public function update_success_data($id) {
        $data['idname'] = 'classroom';


        error_reporting(1);
		
		
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
								
	}
	   $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');


		$data['home_child_data'] = $this->classroommodel->get_home_By_child_data();
//		print_r($data['home_child_data']);exit;
        $data['retrieve_reports'] = $this->classroommodel->update_success($id);

        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();

        $this->load->Model('schoolmodel');
        $data['grades'] = $this->schoolmodel->getallgrades();

        $this->load->model('intervention_strategies_home_model');
        $data['strategies_home'] = $this->intervention_strategies_home_model->get_all_intervention_home();

        $this->load->model('intervention_strategies_school_model');
        $data['strategies_school'] = $this->intervention_strategies_school_model->get_all_intervention_school();


        $this->load->model('intervention_strategies_medical_model');
        $data['strategies_medical'] = $this->intervention_strategies_medical_model->get_all_intervention_medical();

        $this->load->model('actions_home_model');
        $data['action_home'] = $this->actions_home_model->get_all_action_home();

        $this->load->model('actions_school_model');
        $data['action_school'] = $this->actions_school_model->get_all_action_school();

        $this->load->model('actions_medical_model');
        $data['action_medical'] = $this->actions_medical_model->get_all_action_medical();

        //print_r($data['retrieve_reports']);exit;
         if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			} 
			
			  if($this->session->userdata('login_type') == 'teacher'){
				 $this->load->Model('teachermodel');
				$data['teachername'] = $this->teachermodel->getteacherById($user_id);
			   
			}else if ($this->session->userdata('login_type') == 'observer') {
				$this->load->Model('observermodel');
				$data['teachername'] = $this->observermodel->getobserverById($user_id);
			}

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/update_success_report', $data);
    }


 public function edit_parent_report($id) {
        $data['idname'] = 'classroom';

        error_reporting(1);

			 $this->load->Model('teachermodel');

            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
            $data['subjects'] = $this->schoolmodel->getallsubjects();
            $data['grades'] = $this->schoolmodel->getallgrades();
            $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('classroommodel');
           
			$this->load->Model('strengths_model');
			$this->load->Model('concerns_model');
			$this->load->Model('ideas_for_parent_student_model');
			
			
            $data['strengths'] = $this->strengths_model->get_all_strengths_name_data();
			
            $data['concerns'] = $this->concerns_model->get_all_concerns_name_data();
			
            $data['ideas_for_parent_student'] = $this->ideas_for_parent_student_model->get_all_ideas_parent_student_data();
			
			
			
			
			$data['schools'] = $this->schoolmodel->getschoolbydistrict(); 
		  	if($this->session->userdata('login_type')=='user'){
	$data['retrieve_reports'] = $this->classroommodel->get_parent_conference_data_user($id);
		} else {
	$data['retrieve_reports'] = $this->classroommodel->get_parent_conference_data($id);
		}
				
			  
		//print_r($data['retrieve_reports']);exit;
     			   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
			
			  if($this->session->userdata('login_type') == 'teacher'){
				 $this->load->Model('teachermodel');
				$data['teachername'] = $this->teachermodel->getteacherById($user_id);
			   
			}else {
				$this->load->Model('observermodel');
				$data['teachername'] = $this->observermodel->getobserverById($user_id);
			}

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/edit_parent_report', $data);
    }



	
	

    function behavior_record_details() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        $id = $this->input->post('incident');
        $data['incident_details'] = $this->classroommodel->get_incident_by_id($id);
//        print_r($data['incident_details']);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_details', $data);
    }

    function incident_record_details() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        $id = $this->input->post('incident');
        $data['incident_details'] = $this->classroommodel->incident_record_by_id($id);
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/incident_record_details', $data);
    }

    function incident_record_details_print($id) {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        $data['incident_details'] = $this->classroommodel->incident_record_by_id($id);
        if($data['incident_details']['incident'][0]->login_type=='teacher'){
            $this->load->model('teachermodel');
            $data['teacher'] =  $this->teachermodel->getteacherById($data['incident_details']['incident'][0]->login_id);
        } else if($data['incident_details']['incident'][0]->login_type=='observer') {
            $this->load->model('observermodel');
            $data['observer'] =  $this->observermodel->getobserverById($data['incident_details']['incident'][0]->login_id);
        }
        $data['view_path'] = $this->config->item('view_path');
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(19);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {
            $dis = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['dis'] = $dis;
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('classroom/incident_record_print', $data, TRUE);
        // echo $str;exit;
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 5));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    function behavior_record_details_print($id) {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        $data['incident_details'] = $this->classroommodel->get_incident_by_id($id);
        if($data['incident_details']['incident'][0]->login_type=='teacher'){
            $this->load->model('teachermodel');
            $data['teacher'] =  $this->teachermodel->getteacherById($data['incident_details']['incident'][0]->login_id);
        } else if($data['incident_details']['incident'][0]->login_type=='observer') {
            $this->load->model('observermodel');
            $data['observer'] =  $this->observermodel->getobserverById($data['incident_details']['incident'][0]->login_id);
        }
        $data['view_path'] = $this->config->item('view_path');
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(19);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {
            $dis = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['dis'] = $dis;
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('classroom/behavior_record_print', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 5));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }
    
    public function graph_by_incident() {
        $data['idname'] = 'classroom';
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        $chart->chart->type = "column";
        if ($this->input->post('month')!='all'){
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));//date('F',strtotime($this->input->post('month')));
            $chart->title->text = "Behavior Record By Incident - $monthname, $year";
            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
            $beg = (int) date('W', strtotime($first))-1;
            $end = (int) date('W', strtotime($last))-1;
            $range = range($beg, $end);
            $cnt = 0;
            for($firstdate=1;$firstdate<=$day;$firstdate++){
               $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
            }
            foreach($arrday as $dateval){
                $arrcat[] = date('d',strtotime($dateval)); 
            }
            $chart->xAxis->categories = $arrcat;
            $incident_list = $this->classroommodel->get_graph_by_incident();
       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $weekcnt = array();
            foreach($value as $date){
                foreach($arrday as $key1=>$arday){
                    if($date==$arday){
                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
                    } else {
                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
                    }
                }
            }
            $chart->series[] = array('name' => $key,
                         'data' => $weekcnt);
        }
        
                    } else {
                        $chart->title->text = "Behavior Record By Incident";
            $chart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            
            $chart->plotOptions->column->stacking = "normal";
        
        $incident_list = $this->classroommodel->get_graph_by_incident();
        if($this->input->post('incident')==0){
                    foreach($incident_list as $incident){

                         $arr[$incident->behaviour_name][] = $incident->date;
                     }
              } else {
                  foreach($incident_list as $incident){
                      
                      if($incident->problem_id===$this->input->post('incident')){
                         $arr[$incident->behaviour_name][] = $incident->date;
                      }
                     }
              }
        foreach($arr as $key=>$value){
            $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0; $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
            foreach($value as $date){
                
                switch(date('m',strtotime($date))){
                    case 1:
                        $jan++;
                        break;
                    case 2: 
                        $feb++;
                        break;
                    case 3:
                        $mar++;
                        break;
                    case 4: 
                        $apr++;
                        break;
                    case 5:
                        $may++;
                        break;
                    case 6:
                        $jun++;
                        break;
                    case 7: 
                        $jul++;
                        break;
                    case 8:
                        $aug++;
                        break;
                    case 9: 
                        $sep++;
                        break;
                    case 10:
                        $oct++;
                        break;
                    case 11: 
                        $nov++;
                        break;
                    case 12:
                        $dec++;
                        break;
                }
                
            }
            $chart->series[] = array('name' => $key,
                         'data' => array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec));
        }
        }
        $html = $chart->render("chart1");
        echo $html;
    }

    function graph_by_incident_district(){
        $data['idname'] = 'classroom';
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        // $chart->chart->type = "column";
        $chart->chart->type = "bar";
        if ($this->input->post('month')!='all'){
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));//date('F',strtotime($this->input->post('month')));
            $chart->title->text = "Behavior Record By Incident - $monthname, $year";
            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
            $beg = (int) date('W', strtotime($first))-1;
            $end = (int) date('W', strtotime($last))-1;
            $range = range($beg, $end);
            $cnt = 0;
            for($firstdate=1;$firstdate<=$day;$firstdate++){
               $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
            }
            foreach($arrday as $dateval){
                $arrcat[] = date('d',strtotime($dateval)); 
            }
            $chart->xAxis->categories = $arrcat;
            $incident_list = $this->classroommodel->get_graph_by_incident();
            echo $this->db->last_query();exit;

       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $weekcnt = array();
            foreach($value as $date){
                foreach($arrday as $key1=>$arday){
                    if($date==$arday){
                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
                    } else {
                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
                    }
                }
            }
            $chart->series[] = array('name' => $key,
                         'data' => $weekcnt);
        }
        
                    } else {
                        $chart->title->text = "Behavior Record By Incident";
        $incident_list = $this->classroommodel->get_graph_by_incident();
        if($this->input->post('incident')==0){
                foreach($incident_list as $incident) {
                    if(!in_array($incident->school_name,$xAxisarr )){
                        $xAxisarr[] = $incident->school_name;
                    }
                }

                foreach($incident_list as $incident){
                    if(!in_array($incident->behaviour_name,$xAxisarr )){
                       $incident_name[] = $incident->behaviour_name;
                    }
                }
$chart->yAxis->allowDecimals = false;
            $chart->xAxis->categories = $xAxisarr;
            $chart->yAxis->min = 0;
            $chart->plotOptions->series->stacking = "normal";   
                    foreach($incident_list as $incident){
                       $arr[$incident->behaviour_name][$incident->school_name] = $arr[$incident->behaviour_name][$incident->school_name] + 1;
                     }
              } 

              foreach($arr as $arrk=>$arrv){
                $chart->series[] = array('name' => $arrk,
                         'data' => array_values($arrv));
              }

        //       print_r($arr);exit;

        // $chart->series[] = array('name' => 'Incidents',
        //                  'data' => array_values($arr));
        }
        $html = $chart->render("chart1");
        echo $html;
    }
    
    public function graph_by_student() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        
        $data['idname'] = 'classroom';
        
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        
        $chart->chart->type = "column";
        
        if($this->input->post('student_id')!=0){
            $chart->title->text = "Behavior Record ".$this->input->post('student_name');
        } else {
            $chart->title->text = "Behavior Record By Student";
        }
//        echo $this->input->post('month');exit;
        if($this->input->post('month')=='all') {
            $chart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            
              $incident_list = $this->classroommodel->get_graph_by_student();
        
//        print_r($incident_list);exit;
        
       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0; $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
            foreach($value as $date){
                
                switch(date('m',strtotime($date))){
                    case 1:
                        $jan++;
                        break;
                    case 2: 
                        $feb++;
                        break;
                    case 3:
                        $mar++;
                        break;
                    case 4: 
                        $apr++;
                        break;
                    case 5:
                        $may++;
                        break;
                    case 6:
                        $jun++;
                        break;
                    case 7: 
                        $jul++;
                        break;
                    case 8:
                        $aug++;
                        break;
                    case 9: 
                        $sep++;
                        break;
                    case 10:
                        $oct++;
                        break;
                    case 11: 
                        $nov++;
                        break;
                    case 12:
                        $dec++;
                        break;
                }
                
            }
//            print_r($value);
            $chart->series[] = array('name' => $key,
                         'data' => array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec),'stack' => 'male');
             $chart->plotOptions->column->stacking = "normal";
        }
        } else {
            
            $month = $this->input->post('month');
            $year = $this->input->post('year');

            $monthname = date('F', mktime(0, 0, 0, $month, 10));
            $chart->title->text = "Behavior Record By Incident - $monthname, $year";
            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            
            $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
            $beg = (int) date('W', strtotime($first))-1;
            $end = (int) date('W', strtotime($last))-1;
            
            $range = range($beg, $end);
           
            $cnt = 0;
//            foreach($range as $val){
////                echo 'va'.$val;
////                echo '<br />';
//                $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $val * 7 * 24 * 60 * 60 );
//                 $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
//                $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
//                $timestamp_for_sunday = $timestamp - 86400 * ( date( 'N', $timestamp ) -7 );
//                $date_for_sunday = date( 'Y-m-d', $timestamp_for_sunday );
//                if(date('n',strtotime($date_for_monday))<=$month){
//                    $arrcat[] = $date_for_monday.' - '.$date_for_sunday;
//                    $arrweek[$cnt]['start'] = $date_for_monday;
//                    $arrweek[$cnt]['end'] = $date_for_sunday;
//                    $weekcnt[$cnt] = 0;
//                    $cnt++;
//                }
//            }
            
            for($firstdate=1;$firstdate<=$day;$firstdate++){
               $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
            }
            
            foreach($arrday as $dateval){
                $arrcat[] = date('d',strtotime($dateval)); 
            }
            
//            print_r($arrweek);exit;
//            $category = implode();
            
            $chart->xAxis->categories = $arrcat;
            
              $incident_list = $this->classroommodel->get_graph_by_student();
        
//        print_r($incident_list);exit;
        
       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
//        print_r($arr);exit;
        $jun=0;
//        foreach($arr as $key=>$value){
//            $weekcnt = array();
//            foreach($value as $date){
//                foreach($arrweek as $key1=>$arweek){
//                    if($date >= $arweek['start'] && $date <=$arweek['end']){
//                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
//                    } else {
//                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
//                    }
//                }
//            }
////            print_r($weekcnt);exit;
//            $chart->series[] = array('name' => $key,
//                         'data' => $weekcnt);
//        }
        
        foreach($arr as $key=>$value){
            $weekcnt = array();
            foreach($value as $date){
//                foreach($arrweek as $key1=>$arweek){
//                    echo $date.'---'.$arweek['start'].'---'.$arweek['end'];
                foreach($arrday as $key1=>$arday){
//                    echo $data.'---'.$arday;
                    if($date==$arday){
                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
                    } else {
                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
                    }
                }
            }
//            print_r($weekcnt);exit;
            $chart->series[] = array('name' => $key,
                         'data' => $weekcnt);
        }
        
        
        }
       
        
      


        
        $html = $chart->render("chart1");

        echo $html;
        
    }


 public function success_edit_data() {

        //print_r($this->input->post('action_home_responsible'));exit;
        $data['idname'] = 'classroom';

        if ($this->input->post('submit')) {
            $date = $this->input->post('date');
			$id = $this->input->post('id');
			$student_id = $this->input->post('student_id');
            $grade_id = $this->input->post('grade_id');
            $parent = $this->input->post('strengths_parent');
            $teacher = $this->input->post('strengths_teacher');
            $familys = $this->input->post('information_family');
            $schools = $this->input->post('information_school');
            $healths = $this->input->post('information_health');
            $need_home = $this->input->post('needs_home');
            $need_school = $this->input->post('needs_school');
            $need_medical = $this->input->post('needs_medical');
            $actions_home = $this->input->post('action_home');
            $actions_school = $this->input->post('action_school');
            $actions_medical = $this->input->post('action_medical');
            
            $school_id = $this->session->userdata('school_id');
            $action_home_responsible = $this->input->post('action_home_responsible');
            $action_school_responsible = $this->input->post('action_school_responsible');
            $action_medical_responsible = $this->input->post('action_medical_responsible');


            $pear = implode(",", $parent);
            $teach = implode(",", $teacher);
            $family = implode(",", $familys);
            $school = implode(",", $schools);
            $health = implode(",", $healths);
            $needs_home = implode(",", $need_home);
            $needs_school = implode(",", $need_school);
            $needs_medical = implode(",", $need_medical);
            $action_home = implode(",", $actions_home);
            $action_school = implode(",", $actions_school);
            $action_medical = implode(",", $actions_medical);
            $action_home_responsibles = implode(",", $action_home_responsible);
            $action_school_responsibles = implode(",", $action_school_responsible);
            $action_medical_responsibles = implode(",", $action_medical_responsible);
            
            if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }



            $data = array('user_type' => $this->session->userdata('login_type'), 'user_id' => $user_id, 'school_id' => $school_id, 'district_id' => $this->session->userdata('district_id'), 'date' => date('Y-m-d', strtotime(str_replace('-', '/', $date))),'id' => $id, 'student_id' => $student_id, 'grade_id' => $grade_id, 'strengths_parent' => serialize($pear), 'strengths_teacher' => serialize($teach), 'information_family' => serialize($family), 'information_school' => serialize($school), 'information_health' => serialize($health), 'needs_home' => serialize($needs_home), 'needs_school' => serialize($needs_school), 'needs_medical' => serialize($needs_medical), 'action_home' => serialize($action_home), 'action_school' => serialize($action_school), 'action_medical' => serialize($action_medical), 'action_home_responsible' => serialize($action_home_responsibles), 'action_school_responsible' => serialize($action_school_responsibles), 'action_medical_responsible' => serialize($action_medical_responsibles));


//print_r($data);exit;	

            $this->load->model('classroommodel');
			$fetch=$this->classroommodel->update_data('student_success_strengths',$data,$data['id']);
        }

        $date = $data['date'] = $this->input->post('date');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];

        if ($result)
            $link = base_url() . 'classroom/createsstpdf/' . $user_id . '/' . $student_id . '/' . $date . '/' . $result;

        $this->session->set_flashdata('link', $link);
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/success_team');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }
 public function edit_behavior_report($id) {
        $data['idname'] = 'classroom';

        error_reporting(1);

			 $this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
			$data['schools'] = $this->schoolmodel->getschoolbydistrict();

        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_behaviour_data();

        $data['possible_motivation'] = $this->problem_behaviour_model->get_all_possible_motivation_data();

        $data['interventions_prior'] = $this->problem_behaviour_model->get_all_interventions_prior_data();

        $data['student_has_multiple_referrals'] = $this->problem_behaviour_model->get_all_student_has_multiple_referrals_data();

        $this->load->Model('behavior_location_model');
        $data['behavior_location'] = $this->behavior_location_model->get_all_behavior_location_data();
		
		$this->load->model('classroommodel');
        	$data['incident_details'] = $this->classroommodel->get_incident_by_id($id);
			
			//print_r($data['incident_details']);exit;
     
	
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/edit_behavior_report',$data);
    }
    
    function getWeeks($date, $rollover)
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $i = 1;
        $weeks = 1;

        for($i; $i<=$elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover))  $weeks ++;
        }

        return $weeks;
    }


function get_weeks($month,$year){
    $mm= $month;
$yy= $year;
$startdate=date($yy."-".$mm."-01") ;
//$current_date=date('Y-m-t');
$ld= cal_days_in_month(CAL_GREGORIAN, $mm, $yy);
$lastday=$yy.'-'.$mm.'-'.$ld;
$start_date = date('Y-m-d', strtotime($startdate));
$end_date = date('Y-m-d', strtotime($lastday));
$end_date1 = date('Y-m-d', strtotime($lastday." + 6 days"));
$count_week=0;
$week_array = array();

for($date = $start_date; $date <= $end_date1; $date = date('Y-m-d', strtotime($date. ' + 7 days')))
{
    $getarray=$this->getWeekDates($date, $start_date, $end_date);

$week_array[]=$getarray;
$count_week++;

}

// its give the number of week for the given month and year
//echo $count_week;
echo "<pre>";
print_r($week_array);
}

function getWeekDates($date, $start_date, $end_date)
{
    $week =  date('W', strtotime($date));
    $year =  date('Y', strtotime($date));
    $from = date("Y-m-d", strtotime("{$year}-W{$week}+1"));
    if($from < $start_date) $from = $start_date;

    $to = date("Y-m-d", strtotime("{$year}-W{$week}-6")); 
    if($to > $end_date) $to = $end_date;

$array1 = array(
        "ssdate" => $from,
        "eedate" => $to,
);

return $array1;
}

 public function location_by_graph() {

        $data['idname'] = 'classroom';
        
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        
        $chart->chart->type = "column";
        
        $chart->title->text = "Behavior Record By Location";
        
        $this->load->model('classroommodel');
        $incident_list = $this->classroommodel->get_graph_by_location1();
        
        foreach($incident_list as $incident){
            $arr[$incident->behavior_location][] = $incident->date;
        }
        
        if ($this->input->post('month')!='all'){
            
                $month = $this->input->post('month');
                $year = $this->input->post('year');
                
                $monthname = date('F', mktime(0, 0, 0, $month, 10));//date('F',strtotime($this->input->post('month')));
                $chart->title->text = "Behavior Record By Location of Incident - $monthname, $year";
                $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);

//                $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
//                $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
//                $beg = (int) date('W', strtotime($first));
//                $end = (int) date('W', strtotime($last));
//
//                $range = range($beg, $end);
//                $cnt = 0;
//                foreach($range as $val){
//                    $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $val * 7 * 24 * 60 * 60 );
//                    $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
//                    $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
//                    $timestamp_for_sunday = $timestamp - 86400 * ( date( 'N', $timestamp ) -7 );
//                    $date_for_sunday = date( 'Y-m-d', $timestamp_for_sunday );
//                    if(date('n',strtotime($date_for_monday))<=$month){
//                        $arrcat[] = $date_for_monday.' - '.$date_for_sunday;
//                        $arrweek[$cnt]['start'] = $date_for_monday;
//                        $arrweek[$cnt]['end'] = $date_for_sunday;
//                        $weekcnt[$cnt] = 0;
//                        $cnt++;
//                    }
//                }
//
//                $chart->xAxis->categories = $arrcat;
                
                $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
                $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
                $beg = (int) date('W', strtotime($first))-1;
                $end = (int) date('W', strtotime($last))-1;
                $range = range($beg, $end);
                $cnt = 0;
                for($firstdate=1;$firstdate<=$day;$firstdate++){
                   $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
                }
                foreach($arrday as $dateval){
                    $arrcat[] = date('d',strtotime($dateval)); 
                }
                $chart->xAxis->categories = $arrcat;

                foreach($arr as $key=>$value){
                    $weekcnt = array();
                    foreach($value as $date){
                        foreach($arrday as $key1=>$arday){
                            if($date==$arday){
                                $weekcnt[$key1] = $weekcnt[$key1] + 1;
                            } else {
                                $weekcnt[$key1] = $weekcnt[$key1] + 0;
                            }
                        }
                    }
                    $chart->series[] = array('name' => $key,
                                 'data' => $weekcnt);
                }
    
//            foreach($arr as $key=>$value){
//                $weekcnt = array();
//                
//                foreach($value as $date){
//                    foreach($arrweek as $key1=>$arweek){
//                        if($date >= $arweek['start'] && $date <=$arweek['end']){
//                            $weekcnt[$key1] = $weekcnt[$key1] + 1;
//                        } else {
//                            $weekcnt[$key1] = $weekcnt[$key1] + 0;
//                        }
//                    }
//                }
//                $chart->series[] = array('name' => $key,
//                             'data' => $weekcnt);
//            }
            
        } else {
            $chart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            //$chart->xAxis->categories =  $xcategory;//array('restroom','test1','test2','test3','test4','test5');//$xcategory;
            foreach($arr as $key=>$value){
            $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0; $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
            foreach($value as $date){
                
                switch(date('m',strtotime($date))){
                    case 1:
                        $jan++;
                        break;
                    case 2: 
                        $feb++;
                        break;
                    case 3:
                        $mar++;
                        break;
                    case 4: 
                        $apr++;
                        break;
                    case 5:
                        $may++;
                        break;
                    case 6:
                        $jun++;
                        break;
                    case 7: 
                        $jul++;
                        break;
                    case 8:
                        $aug++;
                        break;
                    case 9: 
                        $sep++;
                        break;
                    case 10:
                        $oct++;
                        break;
                    case 11: 
                        $nov++;
                        break;
                    case 12:
                        $dec++;
                        break;
                }
                
            }
            $chart->series[] = array('name' => $key,
                         'data' => array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec),'stack' => 'male');
             $chart->plotOptions->column->stacking = "normal";
        }
     }
        
        $chart->plotOptions->column->stacking = "normal";
        $html = $chart->render("chart1");

        echo $html;
        
    }
    
    public function staff_by_graph() {
    
        $data['idname'] = 'classroom';
        
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        
        $chart->chart->type = "column";
        
        $chart->title->text = "Behavior Record By Location";
        
        $this->load->Model('teachermodel');
        $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
         $this->load->model('classroommodel');
        $incident_list = $this->classroommodel->get_graph_by_staff();
        foreach($incident_list as $incident){
            $arr[$incident->firstname.' '.$incident->lastname][] = $incident->date;
        }
        if ($this->input->post('month')!='all'){
            $month = $this->input->post('month');
                $year = $this->input->post('year');

                $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
                $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
                $beg = (int) date('W', strtotime($first));
                $end = (int) date('W', strtotime($last));

                $range = range($beg, $end);
                $cnt = 0;
                foreach($range as $val){
                    $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $val * 7 * 24 * 60 * 60 );
                    $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
                    $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
                    $timestamp_for_sunday = $timestamp - 86400 * ( date( 'N', $timestamp ) -7 );
                    $date_for_sunday = date( 'Y-m-d', $timestamp_for_sunday );
                    if(date('n',strtotime($date_for_monday))<=$month){
                        $arrcat[] = $date_for_monday.' - '.$date_for_sunday;
                        $arrweek[$cnt]['start'] = $date_for_monday;
                        $arrweek[$cnt]['end'] = $date_for_sunday;
                        $weekcnt[$cnt] = 0;
                        $cnt++;
                    }
                }

                $chart->xAxis->categories = $arrcat;
            foreach($arr as $key=>$value){
                $weekcnt = array();
                
                foreach($value as $date){
                    foreach($arrweek as $key1=>$arweek){
                        if($date >= $arweek['start'] && $date <=$arweek['end']){
                            $weekcnt[$key1] = $weekcnt[$key1] + 1;
                        } else {
                            $weekcnt[$key1] = $weekcnt[$key1] + 0;
                        }
                    }
                }
                $chart->series[] = array('name' => $key,
                             'data' => $weekcnt);
            }
            
        } else {
            $chart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            foreach($arr as $key=>$value){
            $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0; $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
            foreach($value as $date){
                
                switch(date('m',strtotime($date))){
                    case 1:
                        $jan++;
                        break;
                    case 2: 
                        $feb++;
                        break;
                    case 3:
                        $mar++;
                        break;
                    case 4: 
                        $apr++;
                        break;
                    case 5:
                        $may++;
                        break;
                    case 6:
                        $jun++;
                        break;
                    case 7: 
                        $jul++;
                        break;
                    case 8:
                        $aug++;
                        break;
                    case 9: 
                        $sep++;
                        break;
                    case 10:
                        $oct++;
                        break;
                    case 11: 
                        $nov++;
                        break;
                    case 12:
                        $dec++;
                        break;
                }
                
            }
            $chart->series[] = array('name' => $key,
                         'data' => array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec),'stack' => 'male');
             $chart->plotOptions->column->stacking = "normal";
        }
        }
        
        $chart->plotOptions->column->stacking = "normal";
        
        $html = $chart->render("chart1");

        echo $html;
        
    }

    public function student_progress_report() {
		$data['idname'] = 'classroom';
        error_reporting(1);

        // print_r($this->input->post('work_habit'));exit;

        $standards = $this->input->post('standards');
        $grades = $this->input->post('grade1');
        $efforts = $this->input->post('grade2');
        $comments = $this->input->post('comment');
        $student_id = $this->input->post('student_id');
        $period_id = $this->input->post('period_id');
        $quarter_number = $this->input->post('quarter_number');
        $work_habit = $this->input->post('work_habit');
        $learningSkill = $this->input->post('learning');
        $instructional = $this->input->post('instructional');

        $this->load->model('classroommodel');
        $insert['quarter'] = $quarter_number;
        $insert['period'] = $period_id;
        $insert['student_id'] = $student_id;
        $insert['quarter'] = $quarter_number;
        $insert['date_added'] = date('Y-m-d H:i:s');

        foreach($standards as $subject_id=>$standard) {
            // print_r($standard);
            $insert['subject_id'] = $subject_id;
            $insert['standards'] = serialize($standard);
            $insert['comments'] = serialize($comments[$subject_id]);
            $insert['grade'] = $grades[$subject_id];
            $insert['effort'] = $efforts[$subject_id];
            $insert['work_habit'] = serialize($work_habit);
            $insert['learning_skill'] = serialize($learningSkill);
            $insert['instructional_prog'] = serialize($instructional);

            $result = $this->classroommodel->insert_report($insert);            
        }
       


	    
$date = $data['date'] = $this->input->post('date');
        $datearr = explode('-', date('Y-m-d', strtotime(str_replace('-', '/', $date))));
        $date = $datearr[0] . '-' . $datearr[1] . '-' . $datearr[2];
      
	    if ($result)
            $link = base_url() . 'classroom/progress_report_pdf1/' . $student_id . '/' . $result;
        $this->session->set_flashdata('link', $link);
        
		
if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            redirect(base_url() . 'classroom/progress_report');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }
    }

    
    public function retrieve_success_need() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
            $student_id = $this->input->post('student_id');

            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            if($this->input->post('report_type')=='list'){
                $this->retrieve_success_data_need();
            } else {
                $this->retrieve_success_graph_need($grade_id, $student_id, $date);
            }

        }
    }
    
          public function retrieve_success_graph_need() {
                $data['idname'] = 'classroom';

                error_reporting(1);
                include_once "highcharts/Highchart.php";
                $chart = new Highchart();
                $chart->chart->renderTo = "ajaxcontainer";

                $chart->chart->type = "column";

                $chart->title->text = "Student Success Team By Needs";
        
                $this->load->Model('classroommodel');
                
                $retrieve_reports = $this->classroommodel->retrieve_success_report_need();

                foreach($retrieve_reports as $retrieve_report){
                    
                    foreach($retrieve_report['need_home'] as $need_home_detail){
                        if(isset($arr['home'][$need_home_detail->intervention_strategies_home]))
                            $arr['home'][$need_home_detail->intervention_strategies_home] = $arr['home'][$need_home_detail->intervention_strategies_home] + 1;
                        else 
                            $arr['home'][$need_home_detail->intervention_strategies_home] = 1;
                    }
                    foreach($retrieve_report['need_school'] as $need_school_detail){
                        if(isset($arr['school'][$need_school_detail->intervention_strategies_school]))
                            $arr['school'][$need_school_detail->intervention_strategies_school] = $arr['school'][$need_school_detail->intervention_strategies_home] + 1;
                        else 
                            $arr['school'][$need_school_detail->intervention_strategies_school] = 1;
                    }
                    foreach($retrieve_report['need_medical'] as $need_medical_detail){
                        if(isset($arr['medical'][$need_medical_detail->intervention_strategies_medical]))
                            $arr['medical'][$need_medical_detail->intervention_strategies_medical] = $arr['medical'][$need_medical_detail->intervention_strategies_medical] + 1;
                        else 
                            $arr['medical'][$need_medical_detail->intervention_strategies_medical] = 1;
                    }
                    
                    
                    
                }
                foreach($arr as $key=>$value){
                    
                    if ($key=='home'){
                        foreach($value as $valkey=>$val){
                            
                            $chart->series[] = array('name' => $valkey,
                         'data' => array($val, 0, 0));
                        }
                    }
                    if ($key=='school'){
                        foreach($value as $valkey=>$val){
                            
                            $chart->series[] = array('name' => $valkey,
                         'data' => array(0, $val, 0));
                        }
                    }
                    if ($key=='medical'){
                        foreach($value as $valkey=>$val){
                            
                            $chart->series[] = array('name' => $valkey,
                         'data' => array(0, 0, $val));
                        }
                    }
                }

               $chart->xAxis->categories = array('Home Need', 'School Need', 'Medical Need');
                
               $chart->plotOptions->column->stacking = "normal";
               $chart->plotOptions->credits->enabled = 'false';
               //$chart->setConfigurations(array('credits' => array('enabled' => false)));

               $html = $chart->render("chart1");

               echo $html;

        }
    
      public function retrieve_success_data_need() {
        $data['idname'] = 'classroom';


        error_reporting(1);

        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

        $data['retrieve_reports'] = $this->classroommodel->retrieve_success_report_need();
		
//        echo $this->db->last_query();exit;
//        print_r($data['retrieve_reports']);
        if ($this->session->userdata("teacher_id")) {
            $teacher_id = $this->session->userdata("teacher_id");
        } else {
            $teacher_id = $this->input->post('teacher_id');
        }
//        $data['teachername'] = $this->teachermodel->getteacherById($teacher_id);

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/student_success_report_list_ajax', $data);
    }

    public function retrieve_success_report_need() {
       
        $data['idname'] = 'classroom';
        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_need', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            $this->load->Model('intervention_strategies_home_model','need_home_model');
            $data['need_homes'] = $this->need_home_model->get_all_intervention_home();
            $this->load->Model('intervention_strategies_school_model','need_school_model');
            $data['need_schools'] = $this->need_school_model->get_all_intervention_school();
            $this->load->Model('intervention_strategies_medical_model','need_medical_model');
            $data['need_medicals'] = $this->need_medical_model->get_all_intervention_medical();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_need', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_need', $data);
        }
    }
    
    public function retrieve_success_report_action() {
       
            $data['idname'] = 'classroom';

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();
        
        if ($this->session->userdata('login_type') == 'teacher') {

            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_action', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_action', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_action', $data);
        }
    }

    public function retrieve_success_action() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {

            $date = $this->input->post('date');
            $grade_id = $this->input->post('grade_id');
            $student_id = $this->input->post('student_id');

            $this->session->set_userdata('submitbutton', $this->input->post('submit'));

            $date = $data['date'] = $this->input->post('date');
            if($this->input->post('report_type')=='list'){
                $this->retrieve_success_data_action();
            } else {
                $this->retrieve_success_graph_action($grade_id, $student_id, $date);
            }

        }
    }
    
          public function retrieve_success_data_action() {
     
        error_reporting(1);

        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

        $data['retrieve_reports'] = $this->classroommodel->retrieve_success_report_need();
//        echo count($data['retrieve_reports']);
//        print_r($data['retrieve_reports']);exit;
        if ($this->session->userdata("teacher_id")) {
            $teacher_id = $this->session->userdata("teacher_id");
        } else {
            $teacher_id = $this->input->post('teacher_id');
        }
//        $data['teachername'] = $this->teachermodel->getteacherById($teacher_id);
//        print_r($data);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/student_success_report_list_ajax', $data);
    }
    
    public function retrieve_success_graph_action() {
               
                error_reporting(1);
                include_once "highcharts/Highchart.php";
                $chart = new Highchart();
                $chart->chart->renderTo = "ajaxcontainer";

                $chart->chart->type = "column";

                $chart->title->text = "Student Success Team By Actions";
        
                $this->load->Model('classroommodel');
                
                $retrieve_reports = $this->classroommodel->retrieve_success_report_need();
//                print_r($retrieve_reports);exit;
                foreach($retrieve_reports as $retrieve_report){
                    
                    foreach($retrieve_report['action_home'] as $action_home_detail){
                        if(isset($arr['home'][$action_home_detail->actions_home]) && $arr['home'][$action_home_detail->actions_home]!='' )
                            $arr['home'][$action_home_detail->actions_home] = $arr['home'][$action_home_detail->actions_home] + 1;
                        else 
                            $arr['home'][$need_home_detail->actions_home] = 1;
                    }
                    foreach($retrieve_report['action_school'] as $action_school_detail){
                        if(isset($arr['school'][$action_school_detail->actions_school]) && isset($arr['school'][$action_school_detail->actions_school])!='')
                            $arr['school'][$action_school_detail->actions_school] = $arr['school'][$action_school_detail->actions_school] + 1;
                        else 
                            $arr['school'][$action_school_detail->actions_school] = 1;
                    }
                    foreach($retrieve_report['action_medical'] as $action_medical_detail){
                        if(isset($arr['medical'][$action_medical_detail->actions_medical]) && isset($arr['medical'][$action_medical_detail->actions_medical])!='')
                            $arr['medical'][$action_medical_detail->actions_medical] = $arr['medical'][$action_medical_detail->actions_medical] + 1;
                        else 
                            $arr['medical'][$action_medical_detail->actions_medical] = 1;
                    }
                    
                    
                    
                }
                foreach($arr as $key=>$value){
                    
                    if ($key=='home'){
                        foreach($value as $valkey=>$val){
                            if($valkey!='') {
                            $chart->series[] = array('name' => $valkey,
                         'data' => array($val, 0, 0));
                        }
                        }
                    }
                    if ($key=='school'){
                        foreach($value as $valkey=>$val){
                            if($valkey!='') {
                            $chart->series[] = array('name' => $valkey,
                         'data' => array(0, $val, 0));
                        }
                        }
                    }
                    if ($key=='medical'){
                        foreach($value as $valkey=>$val){
                            if($valkey!='') {
                            $chart->series[] = array('name' => $valkey,
                         'data' => array(0, 0, $val));
                        }
                        }
                    }
                }

               $chart->xAxis->categories = array('Home Actions', 'School Actions', 'Medical Actions');
                
               $chart->plotOptions->column->stacking = "normal";
                $chart->legend->align = "center";
                $chart->legend->x =  200;
                // $chart->legend->width =  1000;
                // $chart->legend->floating = 1;
               $html = $chart->render("chart1");

               echo $html;

        }
        
        function getStudentsByTeacher(){
//            $teacher_id = $this->input->post('teacher_id');
            $school_id = $this->input->post('school_id');
            $this->load->model('classroommodel','assessment');
            $data['students'] = $this->assessment->GetStudentBySchoolId($school_id);
            echo json_encode($data['students']);
        }
function get_home_to_child_info()
	{
		$this->load->Model('classroommodel');
		$data['child_data'] = $this->classroommodel->get_home_By_child($this->input->post("needs_home"));
		//print_r($data['child_data']);exit;
		echo json_encode($data);
		
	  }
function get_school_to_child_info()
	{
		$this->load->Model('classroommodel');
		$data['school_child'] = $this->classroommodel->get_school_By_child($this->input->post("needs_school"));
		//print_r($data['school_child']);exit;
		echo json_encode($data);
		
	  } 


function get_medical_to_child_info()
	{
		$this->load->Model('classroommodel');
		$data['medical_child'] = $this->classroommodel->get_medical_By_child($this->input->post("needs_medical"));
		//print_r($data['school_child']);exit;
		echo json_encode($data);
		
	  } 

      function class_summary(){
         $this->load->model('intervention_strategies_home_model');
        $data['meetings'] = $this->intervention_strategies_home_model->get_meeting_type();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/class_room', $data);
      }

      function class_summary_report($year,$meeting_id){
        $this->load->Model('classroommodel');
        $data['class_summarys'] = $this->classroommodel->get_class_summary($year,$meeting_id);

        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(23);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(23);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }


        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('classroom/class_summary_report', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
      }

         function createsstlandscape($teacher_id, $student_id, $date, $id) {
   // print_r($this->session->all_userdata());exit;
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classroommodel');
        $data['lessonplans'] = $this->lessonplanmodel->getalllessonplansnotsubject();
        $data['success_data'] = $this->classroommodel->sst_report_pdf($teacher_id, $student_id, $date, $id);
      // print_r($data['success_data']['need_home']);exit;

       $data['studentname'] = $data['success_data']['details']->firstname.' '.$data['success_data']['details']->lastname;
        if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($teacher_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($teacher_id);
        }
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(24);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(24);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }
     //   print_r($data['success_data']);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $str = $this->parser->parse('classroom/createsstlandscape', $data, TRUE);
        $html2pdf = new HTML2PDF('L', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    function student_entry_page(){
//        print_r($this->session->all_userdata());exit;
      //  print_r($_REQUEST);exit;
        $data['student_id'] = $this->input->post('student_id');
        $data['period_id'] = $this->input->post('period_id');
        $quater = $this->input->post('select_quater');
        switch ($quater) {
            case 'first':
                $data['quater_no'] = 1;
                break;
            case 'second':
                $data['quater_no'] = 2;
                break;
            case 'third':
                $data['quater_no'] = 3;
                break;
            case 'fourth':
                $data['quater_no'] = 4;
                break;
            default:
                $data['quater_no'] = 1;
                break;
        }
        $student_id = $this->input->post('student_id');
        $this->load->model('studentmodel');
        $data['student_details'] = $this->studentmodel->getStudentDetails($student_id);
        // print_r($data['student_details']);exit;
        $school_id = $this->session->userdata('school_id');
        $this->load->model('schoolmodel');
        $data['school_details'] = $this->schoolmodel->getSchoolDetails($school_id);

        $this->load->model('observermodel');
        $data['observer_details'] = $this->observermodel->getobserverByschoolId($school_id);

        $this->load->model('grade_subjectmodel');
        $data['grade_subjects'] = $this->grade_subjectmodel->getgrade_subjects('','',$data['student_details'][0]->grade);
      //  print_r($data['grade_subjects']);exit;
        $this->load->model('standardmodel');
        $data['reportstandards'] = $this->standardmodel->reportstandards();
        $data['reportGrades'] = $this->standardmodel->reportGrades();
        $data['reportEfforts'] = $this->standardmodel->reportEfforts();

        $this->load->model('classroommodel');

        $data['getreports'] = $this->classroommodel->getreport($student_id,$data['quater_no']);
        // print_r($getreport);exit;

        $data['workinghabits'] =  $this->classroommodel->getWorkingHabits();
        $data['instProg'] =  $this->classroommodel->getInstProg();
        $data['learningSkills'] =  $this->classroommodel->getLearningSkills();
        // echo $this->db->last_query();
        // print_r($workinghabits);exit;

       // print_r($data['grade_subjects']);exit;
        $period_id = $this->input->post('period_id');

        $data['view_path'] = $this->config->item('view_path');
        //echo $data['view_path'];exit;
        $this->load->view('classroom/student_entry_page', $data);
        
        //print_r($this->input->post('select_quater'));exit;
    }

    function grade_period_students(){
        $period_id = $data['period_id'] = $this->input->post('period_id');
        $quater = $this->input->post('select_quater');
        switch ($quater) {
            case 'first':
                $data['quater_no'] = 1;
                break;
            case 'second':
                $data['quater_no'] = 2;
                break;
            case 'third':
                $data['quater_no'] = 3;
                break;
            case 'fourth':
                $data['quater_no'] = 4;
                break;
            default:
                $data['quater_no'] = 1;
                break;
        }
        // echo $data['quater_no'];exit;
        $this->load->Model('studentmodel');
        $data['students'] = $this->studentmodel->getStudentsByRange($period_id);
        // print_r($data['students']);exit;
        $this->load->model('standardmodel');
        $data['reportstandards'] = $this->standardmodel->reportstandards();
        $data['reportGrades'] = $this->standardmodel->reportGrades();
        $data['reportEfforts'] = $this->standardmodel->reportEfforts();
        $this->load->Model('lessonplanmodel');
         $lesson_plan_details = $this->lessonplanmodel->getlessonplansbyid($period_id);
         $data['subject_id'] = $lesson_plan_details[0]['subject_id'];
         //print_r($lesson_plan_details);exit;
        
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/class_entry_page', $data);
        //print_r($students);exit;
    }

    function getallcomments()
    {
         parse_str($_SERVER['QUERY_STRING'], $_GET); 
         
        $q = strtolower($_GET["q"]);
        
        if (!$q) return;        
    
        $this->load->Model('parentmodel');
        $comments = $this->parentmodel->get_comments_all($q);
        //print_r($students);exit;
        if($comments!=false)
        {   
            foreach($comments as $comment)
            {
                $sname = $comment['comments'];            
                $sid = $comment['id'];              
                echo "$sname|$sid\n";   
                
            }   
        
        }
        else
        {
        
            echo 'No Comments Found.|';
        }
    }

      function progress_report_pdf1($student_id) {
        error_reporting(1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classroommodel');
        $this->load->Model('teachermodel');

            
        $data['progress_report'] = $this->classroommodel->progress_report_pdf1($student_id);
        // echo $this->db->last_query();exit;
        $this->load->Model('schoolmodel');
        $data['subject_list']= $this->schoolmodel->getallsubjects();
        $subject_mas = $data['subject_list'];
        
        $this->load->model('studentmodel');
        $data['student_details'] = $this->studentmodel->getStudentDetails($student_id);
        // print_r($data['student_details']);exit;
        $school_id = $this->session->userdata('school_id');
        $this->load->model('schoolmodel');
        $data['school_details'] = $this->schoolmodel->getSchoolDetails($school_id);
         $this->load->model('grade_subjectmodel');
        $data['grade_subjects'] = $this->grade_subjectmodel->getgrade_subjects('','',$data['student_details'][0]->grade);

             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
           // print_r($data['progress_report']);exit;
            foreach($data['progress_report'] as $progressReport){
                // print_r($progressReport);exit;
                // $standardarr = unserialize($progressReport->standards);
                // echo key($standardarr);exit;
       $standards[$progressReport->subject_id][$progressReport->quarter] = unserialize($progressReport->standards);
       $comments[$progressReport->subject_id][$progressReport->quarter] = unserialize($progressReport->comments);
       $grades[$progressReport->subject_id][$progressReport->quarter] = ($progressReport->grade);
       $efforts[$progressReport->subject_id][$progressReport->quarter] = ($progressReport->effort);
       $work_habit[$progressReport->quarter] = unserialize($progressReport->work_habit);
       $instructional_prog[$progressReport->quarter] = unserialize($progressReport->instructional_prog);
       $learning_skill[$progressReport->quarter] = unserialize($progressReport->learning_skill);
       }
       // print_r($standards);exit;

       // print_r($work_habit);exit;
       $efforttemp = $gradetemp = $commenttemp = $standardtemp = array();

       foreach($standards as $standard){
          $standardtemp = array_merge($standardtemp,$standard);
       }
       $data['standards'] = $standards;
       foreach($comments as $comment){
        $commenttemp  = array_merge($commenttemp,$comment);
       }
       foreach($comments as $comment){
        $commenttemp  = array_merge($commenttemp,$comment);
       }
       foreach($grades as $grade){
        $gradetemp  = array_merge($gradetemp,$grade);
       }
       foreach($efforts as $effort){
        $efforttemp  = array_merge($efforttemp,$effort);
       }
       $data['efforts'] = $efforts;
       $data['grade'] = $grades;
       $data['comments'] = $comments;
       
       foreach($work_habit as $quarterNo=>$workhabits){
        foreach($workhabits as $key=>$workhabit){
            $whabit[$key][$quarterNo] =$workhabit; 
        }
       }

       foreach($instructional_prog as $quarterNo=>$instructional_progs){
        foreach($instructional_progs as $key=>$ipv){
            $instructionalp[$key][$quarterNo] =$ipv; 
        }
       }

       foreach($learning_skill as $quarterNo=>$learning_skills){
        foreach($learning_skills as $key=>$lsv){
            $learningskil[$key][$quarterNo] =$lsv; 
        }
       }
       // print_r($instructionalp);exit;
       $data['work_habit'] = $whabit;

       // print_r($data['work_habit']);exit;
       $data['instructional_prog'] = $instructionalp;
       $data['learning_skill'] = $learningskil;
       
       // print_r($data['learning_skill']);exit;
       $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(26);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(26);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }

       $this->load->model('standardmodel');
        $data['reportstandards'] = $this->standardmodel->reportstandards();
        $data['reportGrades'] = $this->standardmodel->reportGrades();
        $data['reportEfforts'] = $this->standardmodel->reportEfforts();
       // print_r($data['reportstandards'] );exit;
        $data['view_path'] = $this->config->item('view_path');
        // print_r($data);exit;
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('classroom/student_report_page_pdf', $data, TRUE);
// exit;
//  $str = $this->load->view('classroom/createsstpdf',$data,true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    public function class_progress_report() {
        $data['idname'] = 'classroom';
        // error_reporting(1);
        // print_r($_REQUEST);exit;

        $standards = $this->input->post('standards');

        $grades = $this->input->post('grade1');
        $efforts = $this->input->post('grade2');
        $comments = $this->input->post('comment');
        $this->load->model('classroommodel');
        $insert['quarter'] = $this->input->post('quarter_no');
        $insert['period'] = $this->input->post('period_id');
        $insert['subject_id'] = $this->input->post('subject_id');
        $insert['date_added'] = date('Y-m-d H:i:s');

        foreach($standards as $student_id=>$standard) {
            $insert['student_id'] = $student_id;
            $insert['standards'] = serialize($standard);
            $insert['grade'] = $grades[$student_id];
            $insert['effort'] = $efforts[$student_id];
            $insert['comments'] = serialize($comments[$student_id]);
            $this->classroommodel->insert_report($insert);
        }

        $this->load->Model('studentmodel');
        $data['students'] = $this->studentmodel->getStudentsByRange($insert['period']);

        $data['workinghabits'] =  $this->classroommodel->getWorkingHabits();
        $data['instProg'] =  $this->classroommodel->getInstProg();
        $data['learningSkills'] =  $this->classroommodel->getLearningSkills();

        $this->load->model('standardmodel');
        $data['reportstandards'] = $this->standardmodel->reportstandards();
        $data['reportGrades'] = $this->standardmodel->reportGrades();
        $data['reportEfforts'] = $this->standardmodel->reportEfforts();

        $data['quarter'] = $insert['quarter'];
        $data['period'] = $insert['period'];
        $data['subject_id'] = $insert['subject_id'];

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/class_entry_page_extra', $data);
        
        // echo 'done';
        // exit;
    }

     public function class_progress_report_extra() {
        $data['idname'] = 'classroom';
    
        // print_r($_REQUEST);exit;
        $work_habit = $this->input->post('work_habit');
        $learning = $this->input->post('learning');
        // print_r($learning);exit;
        $instructional = $this->input->post('instructional');
        // print_r($instructional);exit;
        $where['quarter'] = $this->input->post('quarter');
        $where['subject_id'] = $this->input->post('subject_id');
        $this->load->Model('classroommodel');
        foreach($work_habit as $key=>$workhb) {
            $where['student_id'] = $key;
            // print_r($workhb);exit;
            $update['work_habit'] = serialize($work_habit[$key]);
            // print_r($update['work_habit']);exit;
            $update['learning_skill'] = serialize($learning[$key]);
            $update['instructional_prog'] = serialize($instructional[$key]);
            $this->classroommodel->update_report($update,$where);
        }

       redirect(base_url().'classroom/create_progress_report_type');
    }

    //added for incident record

    public function incident_record() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $this->load->Model('videooptionmodel');
            $data['video_option'] = $this->videooptionmodel->get_option();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/incident_record', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/incident_record', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/incident_record', $data);
        }
    }

    public function create_incident() {

        $data['idname'] = 'classroom';
        $this->load->model('problem_behaviour_model');
        $data['incident_data'] = $this->problem_behaviour_model->get_all_incident_data();

        $data['possible_motivation'] = $this->problem_behaviour_model->get_incident_possible_motivation_data();

        $data['interventions_prior'] = $this->problem_behaviour_model->get_incident_interventions_prior_data();

        $data['student_has_multiple_referrals'] = $this->problem_behaviour_model->get_incident_student_has_multiple_referrals_data();

        $this->load->Model('incident_location_model');
        $data['incident_location'] = $this->incident_location_model->get_all_incident_location_data();

        $this->load->Model('schoolmodel');
        $data['schools'] = $this->schoolmodel->getschoolbydistrict();
        $data['view_path'] = $this->config->item('view_path');

        $this->load->view('classroom/create_incident', $data);
    }

    public function create_incident_records() {
        $data['idname'] = 'classroom';
        if($this->session->userdata('login_type')=='teacher'){
            $login_id = $this->session->userdata('teacher_id');
        } else if($this->session->userdata('login_type')=='observer'){
            $login_id = $this->session->userdata('observer_id');
        }  else if($this->session->userdata('login_type')=='user'){
            $login_id = $this->session->userdata('dist_user_id');
        }
        $this->load->model('classroommodel');
        
        $result = $this->classroommodel->create_incident_records($login_id);
        if ($result) {
            $this->session->set_flashdata('message', 'successfully!!');
            // print_r($_REQUEST);
            // echo $this->input->post('send_coll');exit;
            if($this->input->post('send_coll')=='send'){

                $colleagues = $this->classroommodel->get_colleagues();
                // print_r($colleagues);exit;

                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtpout.secureserver.net";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "admin@ueiscorp.com"; 
                $config['smtp_pass'] = "harddisk";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                $filename = $_SERVER['DOCUMENT_ROOT'].'/email_templates/register.txt';
            
                $handle = fopen($filename, "r");
                $contents = fread($handle, filesize($filename));
                fclose($handle);
                foreach($colleagues as $colleague){
                    $contents  = str_replace('[[firstname]]',$firstname,$contents);
                  
                    $this->email->initialize($config);
                    $this->email->from('admin@ueiscorp.com', 'ueisworkshop admin');
                    $this->email->reply_to('admin@ueiscorp.com', 'ueisworkshop admin');
                    $this->email->to($colleague['email']);
                    $this->email->subject('A New incident reported');
                    $this->email->message($contents);
                    $this->email->attach($_SERVER['DOCUMENT_ROOT'].'/'.$data['view_path'].'inc/logo/'.$school_name.'_teachers.pdf');
                    $this->email->send();
                }
            }

            redirect(base_url() . 'classroom/incident_record');
        } else {
            $this->session->set_flashdata('error', 'Some error occur while creating !!');
            redirect($this->agent->referrer());
        }


    }

    public function incident_report_list() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/incident_report_list', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/observer_incident_report_list', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/observer_incident_report_list', $data);
        }
    }

    public function incident_incident_report() {
        $data['idname'] = 'classroom';
        $this->load->Model('schoolmodel');
        $data['schools'] = $this->schoolmodel->getschoolbydistrict();
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_incident_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/district_summary', $data);
    }

    public function district_summary() {
        $data['idname'] = 'classroom';
        $this->load->Model('schoolmodel');
        $data['schools'] = $this->schoolmodel->getschoolbydistrict();
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_incident_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/incident_report_incident', $data);
    }

    public function list_incident_by_incident() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        if($this->input->post('reportType')=='graph'){
            // if ($this->session->userdata('login_type') == 'user' && $this->input->post('school_id')=='all') {
            // $this->graph_incident_by_incident_district();
            // } else {
            $this->graph_incident_by_incident();
            // }
            
        } else {
            $data['incident_list'] = $this->classroommodel->get_list_incident_by_incident();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/incident_record_list', $data);
        }
    }

        public function graph_incident_by_incident() {
        $data['idname'] = 'classroom';
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        $chart->chart->type = "column";
        if ($this->input->post('month')!='all'){
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));//date('F',strtotime($this->input->post('month')));
            $chart->title->text = "Incident Record By Incident - $monthname, $year";
            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
            $beg = (int) date('W', strtotime($first))-1;
            $end = (int) date('W', strtotime($last))-1;
            $range = range($beg, $end);
            $cnt = 0;
            for($firstdate=1;$firstdate<=$day;$firstdate++){
               $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
            }
            foreach($arrday as $dateval){
                $arrcat[] = date('d',strtotime($dateval)); 
            }
            $chart->xAxis->categories = $arrcat;
            $incident_list = $this->classroommodel->get_graph_incident_by_incident();
       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $weekcnt = array();
            foreach($value as $date){
                foreach($arrday as $key1=>$arday){
                    if($date==$arday){
                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
                    } else {
                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
                    }
                }
            }
            $chart->series[] = array('name' => $key,
                         'data' => $weekcnt);
        }
        
                    } else {
                        $chart->title->text = "Incident Record By Incident";
            $chart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            
            $chart->plotOptions->column->stacking = "normal";
        
        $incident_list = $this->classroommodel->get_graph_incident_by_incident();
        // print_r($incident_list);exit;
        if($this->input->post('incident')==0){
                    foreach($incident_list as $incident){

                         $arr[$incident->incident_name][] = $incident->date;
                     }
              } else {
                  foreach($incident_list as $incident){
                      
                      if($incident->problem_id===$this->input->post('incident')){
                         $arr[$incident->incident_name][] = $incident->date;
                      }
                     }
              }

        foreach($arr as $key=>$value){
            $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0; $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
            foreach($value as $date){
                
                switch(date('m',strtotime($date))){
                    case 1:
                        $jan++;
                        break;
                    case 2: 
                        $feb++;
                        break;
                    case 3:
                        $mar++;
                        break;
                    case 4: 
                        $apr++;
                        break;
                    case 5:
                        $may++;
                        break;
                    case 6:
                        $jun++;
                        break;
                    case 7: 
                        $jul++;
                        break;
                    case 8:
                        $aug++;
                        break;
                    case 9: 
                        $sep++;
                        break;
                    case 10:
                        $oct++;
                        break;
                    case 11: 
                        $nov++;
                        break;
                    case 12:
                        $dec++;
                        break;
                }
                
            }
            $chart->series[] = array('name' => $key,
                         'data' => array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec));
        }
        }
        $html = $chart->render("chart1");
        echo $html;
    }

    public function students_incident() {
        $data['idname'] = 'classroom';
        $this->load->Model('schoolmodel');
        $data['schools'] = $this->schoolmodel->getschoolbydistrict();
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_all();
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_incident_data();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/students_incident', $data);
    }

    public function list_incident_by_student() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        if($this->input->post('reportType')=='graph'){
            $this->graph_incident_by_student();
        } else {
        $data['incident_list'] = $this->classroommodel->get_list_incident_by_student();
        // print_r($data['incident_list']);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/incident_record_list', $data);
        }
    }

       public function graph_incident_by_student() {
        $data['idname'] = 'classroom';
        $this->load->model('classroommodel');
        $data['idname'] = 'classroom';
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        $chart->chart->type = "column";
        if($this->input->post('student_id')!=0){
            $chart->title->text = "Incident Record ".$this->input->post('student_name');
        } else {
            $chart->title->text = "Incident Record By Student";
        }
        if($this->input->post('month')=='all') {
            $chart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            $incident_list = $this->classroommodel->get_graph_incident_by_student();
       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0; $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0; $nov = 0; $dec = 0;
            foreach($value as $date){
                switch(date('m',strtotime($date))){
                    case 1:
                        $jan++;
                        break;
                    case 2: 
                        $feb++;
                        break;
                    case 3:
                        $mar++;
                        break;
                    case 4: 
                        $apr++;
                        break;
                    case 5:
                        $may++;
                        break;
                    case 6:
                        $jun++;
                        break;
                    case 7: 
                        $jul++;
                        break;
                    case 8:
                        $aug++;
                        break;
                    case 9: 
                        $sep++;
                        break;
                    case 10:
                        $oct++;
                        break;
                    case 11: 
                        $nov++;
                        break;
                    case 12:
                        $dec++;
                        break;
                }
                
            }
            $chart->series[] = array('name' => $key,
                         'data' => array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $oct, $nov, $dec),'stack' => 'male');
             $chart->plotOptions->column->stacking = "normal";
        }
        } else {
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));
            $chart->title->text = "Behavior Record By Incident - $monthname, $year";
            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
            $beg = (int) date('W', strtotime($first))-1;
            $end = (int) date('W', strtotime($last))-1;
            $range = range($beg, $end);
            $cnt = 0;
            for($firstdate=1;$firstdate<=$day;$firstdate++){
               $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
            }
            foreach($arrday as $dateval){
                $arrcat[] = date('d',strtotime($dateval)); 
            }
            
            $chart->xAxis->categories = $arrcat;
            
              $incident_list = $this->classroommodel->get_graph_incident_by_student();
       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $weekcnt = array();
            foreach($value as $date){
                foreach($arrday as $key1=>$arday){
                    if($date==$arday){
                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
                    } else {
                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
                    }
                }
            }
            $chart->series[] = array('name' => $key,
                         'data' => $weekcnt);
        }
        }
        $html = $chart->render("chart1");
        echo $html;
    }

       function graph_incident_by_incident_district(){
        $data['idname'] = 'classroom';
        error_reporting(0);
        include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "gridcontainer";
        // $chart->chart->type = "column";
        $chart->chart->type = "bar";
        if ($this->input->post('month')!='all'){
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $monthname = date('F', mktime(0, 0, 0, $month, 10));//date('F',strtotime($this->input->post('month')));
            $chart->title->text = "Behavior Record By Incident - $monthname, $year";
            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
            $last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
            $beg = (int) date('W', strtotime($first))-1;
            $end = (int) date('W', strtotime($last))-1;
            $range = range($beg, $end);
            $cnt = 0;
            for($firstdate=1;$firstdate<=$day;$firstdate++){
               $arrday[] =  date('Y-m-d', mktime(0, 0, 0, $month, $firstdate, $year));
            }
            foreach($arrday as $dateval){
                $arrcat[] = date('d',strtotime($dateval)); 
            }
            $chart->xAxis->categories = $arrcat;
            $incident_list = $this->classroommodel->get_graph_by_incident();
            echo $this->db->last_query();exit;

       foreach($incident_list as $incident){
            $arr[$incident->behaviour_name][] = $incident->date;
        }
        $jun=0;
        foreach($arr as $key=>$value){
            $weekcnt = array();
            foreach($value as $date){
                foreach($arrday as $key1=>$arday){
                    if($date==$arday){
                        $weekcnt[$key1] = $weekcnt[$key1] + 1;
                    } else {
                        $weekcnt[$key1] = $weekcnt[$key1] + 0;
                    }
                }
            }
            $chart->series[] = array('name' => $key,
                         'data' => $weekcnt);
        }
        
                    } else {
                        $chart->title->text = "Behavior Record By Incident";
        $incident_list = $this->classroommodel->get_graph_incident_by_incident();

        if($this->input->post('incident')==0){
                foreach($incident_list as $incident) {
                    if(!in_array($incident->school_name,$xAxisarr )){
                        $xAxisarr[] = $incident->school_name;
                    }
                }

                foreach($incident_list as $incident){
                    if(!in_array($incident->behaviour_name,$xAxisarr )){
                       $incident_name[] = $incident->incident_name;
                    }
                }
$chart->yAxis->allowDecimals = false;
            $chart->xAxis->categories = $xAxisarr;
            $chart->yAxis->min = 0;
            $chart->plotOptions->series->stacking = "normal";   
                    foreach($incident_list as $incident){
                       $arr[$incident->incident_name][$incident->school_name] = $arr[$incident->incident_name][$incident->school_name] + 1;
                     }
              } 

              foreach($arr as $arrk=>$arrv){
                $chart->series[] = array('name' => $arrk,
                         'data' => array_values($arrv));
              }

        //       print_r($arr);exit;

        // $chart->series[] = array('name' => 'Incidents',
        //                  'data' => array_values($arr));
        }
        $html = $chart->render("chart1");
        echo $html;
    }

    public function location_eincident_report() {
        $data['idname'] = 'classroom';

        $this->load->Model('schoolmodel');
        $data['school'] = $this->schoolmodel->getschoolbydistrict();


        //print_r($data['school']);exit;
        $this->load->Model('behavior_location_model');
        $data['behavior_location'] = $this->behavior_location_model->get_all_incident_location_data();
        $this->load->model('problem_behaviour_model');
        $data['behaviour_data'] = $this->problem_behaviour_model->get_all_eincident_data();

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/location_eincident_report', $data);
    }

    public function eincident_location_by_list() {

    //$behavior_location = $this->input->post('behavior_location');


        $data['idname'] = 'classroom';
        if($this->input->post('reportType')!='graph'){
        $this->load->model('classroommodel');
        $data['incident_list'] = $this->classroommodel->get_eincident_list_by_location();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/behavior_record_list', $data);
        } else {
            $this->location_by_graph();
        }
        
    }

    function export_incident(){
         $this->load->model('classroommodel');
         $incident_lists = $this->classroommodel->get_graph_incident_by_incident();
         foreach($incident_lists as $incident_list) {
            $incidents[$incident_list->school_name][$incident_list->incident_name] = $incidents[$incident_list->school_name][$incident_list->incident_name]+1;
            if(!in_array($incident_list->incident_name,$incident_names))
                $incident_names[] = trim($incident_list->incident_name);
         }

         // print_r($incidents);exit;
        date_default_timezone_set('Asia/calcutta');
        $this->load->library('excel');
         // echo 'test';exit;
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Incident Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Incident Report');
        $this->excel->getActiveSheet()->setCellValue('A2', 'School Name');
         // $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'test');
        $alpha = 66;
        $row = 2; 
        $col = 1;
        // print_r($incident_names);exit;
        

        for($cnt=0;$cnt<=count($incident_names);$cnt++){
            // $col = $col + $cnt;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$incident_names[$cnt]);

            $col++;
        }
        
         foreach($incidents as $key=>$inci){
            $row++;
            $col = 0;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$key);
            foreach($incident_names as $inci_nm){
                 $col++;
                 if(array_key_exists($inci_nm, $inci))
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$inci[$inci_nm]);
                else
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,0);
               
            }
        }
        // 
        // $this->excel->getActiveSheet()->setCellValue('B2', 'Address');
        // $this->excel->getActiveSheet()->setCellValue('C2', 'Gender');
        // $this->excel->getActiveSheet()->setCellValue('D2', 'Year of Passing');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
        // for($col = ord('A'); $col <= ord('D'); $col++){
        //         //set column dimension
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
             $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
              $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
               $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                 $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                  $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                   $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                     $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                      $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                       $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                        $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                         $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                          $this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                           $this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                            $this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                             $this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
        //  //change the font size
        //     $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
        //     $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // }
        //retrive contries table data
       
//        $rs = $this->db->get('countries');
        
                //Fill data 
        // $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A3');
        // $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $filename='Student_List1-'.date('d/m/y').'.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
                exit;

    }

    function export_sst_action(){
         $this->load->model('classroommodel');
        $retrieve_reports = $this->classroommodel->retrieve_success_report_need();
                 foreach($retrieve_reports as $retrieve_report){
                    foreach($retrieve_report['action_home'] as $action_home_detail){
                        if(isset($arr[$retrieve_report['details']->school_name][$action_home_detail->actions_home]) && $arr[$retrieve_report['details']->school_name][$action_home_detail->actions_home]!='' )
                            $arr[$retrieve_report['details']->school_name][$action_home_detail->actions_home] = $arr[$retrieve_report['details']->school_name][$action_home_detail->actions_home] + 1;
                        else 
                            $arr[$retrieve_report['details']->school_name][$need_home_detail->actions_home] = 1;
                    }
                    foreach($retrieve_report['action_school'] as $action_school_detail){
                        if(isset($arr[$retrieve_report['details']->school_name][$action_school_detail->actions_school]) && isset($arr[$retrieve_report['details']->school_name][$action_school_detail->actions_school])!='')
                            $arr[$retrieve_report['details']->school_name][$action_school_detail->actions_school] = $arr[$retrieve_report['details']->school_name][$action_school_detail->actions_school] + 1;
                        else 
                            $arr[$retrieve_report['details']->school_name][$action_school_detail->actions_school] = 1;
                    }
                    foreach($retrieve_report['action_medical'] as $action_medical_detail){
                        if(isset($arr[$retrieve_report['details']->school_name][$action_medical_detail->actions_medical]) && isset($arr[$retrieve_report['details']->school_name][$action_medical_detail->actions_medical])!='')
                            $arr[$retrieve_report['details']->school_name][$action_medical_detail->actions_medical] = $arr[$retrieve_report['details']->school_name][$action_medical_detail->actions_medical] + 1;
                        else 
                            $arr[$retrieve_report['details']->school_name][$action_medical_detail->actions_medical] = 1;
                    }
                }
                foreach($arr as $key=>$val){
                    foreach($val as $actkey=>$actions){
                        if($actions!='' && $actkey!='' )
                        $action[$actkey][$key] = $action[$actkey][$key] + 1; 
                         if($actions!='' && $actkey!='' )
                            $actionsval[$actkey] = 1;
                    }
                    
                }

                foreach($arr as $key=>$val){
                    foreach($val as $actkey=>$actions){
                       foreach($actionsval as $actfkey=>$actionsf){
                            if($actkey==$actfkey){
                                $schoolarr[$key][$actfkey] = $actions;
                            } else if(!isset($schoolarr[$key][$actfkey])) {
                                $schoolarr[$key][$actfkey] = 0;
                            }
                        } 
                    }
                }
                
                // print_r($schoolarr);exit;
        date_default_timezone_set('Asia/calcutta');
        $this->load->library('excel');
         // echo 'test';exit;
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('SST report By Action');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Action');
        $this->excel->getActiveSheet()->setCellValue('A2', 'School Name');

        $row = 2;
                $col = 1;
                foreach($actionsval as $actionfkey=>$actionfval){
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$actionfkey);
                    $col++;
                }

                $row = 3;
                $col = 0;
                foreach($schoolarr as $schoolname=>$schoolactar){
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$schoolname);
                    foreach($schoolactar as $schoolaction=>$action_val){
                        $col++;
                         $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$action_val);
                         
                    }
                    $col = 0;
                    $row++;
                }
       $filename='SST-Action-'.date('d/m/y').'.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
                exit;
    }

     function export_sst_need(){
         $this->load->model('classroommodel');
        $retrieve_reports = $this->classroommodel->retrieve_success_report_need();
                  foreach($retrieve_reports as $retrieve_report){
                    
                    foreach($retrieve_report['need_home'] as $need_home_detail){
                        if(isset($arr['home'][$need_home_detail->intervention_strategies_home]))
                            $arr['home'][$need_home_detail->intervention_strategies_home] = $arr['home'][$need_home_detail->intervention_strategies_home] + 1;
                        else 
                            $arr['home'][$need_home_detail->intervention_strategies_home] = 1;
                    }
                    foreach($retrieve_report['need_school'] as $need_school_detail){
                        if(isset($arr['school'][$need_school_detail->intervention_strategies_school]))
                            $arr['school'][$need_school_detail->intervention_strategies_school] = $arr['school'][$need_school_detail->intervention_strategies_home] + 1;
                        else 
                            $arr['school'][$need_school_detail->intervention_strategies_school] = 1;
                    }
                    foreach($retrieve_report['need_medical'] as $need_medical_detail){
                        if(isset($arr['medical'][$need_medical_detail->intervention_strategies_medical]))
                            $arr['medical'][$need_medical_detail->intervention_strategies_medical] = $arr['medical'][$need_medical_detail->intervention_strategies_medical] + 1;
                        else 
                            $arr['medical'][$need_medical_detail->intervention_strategies_medical] = 1;
                    }
                }
                foreach($arr as $key=>$val){
                    foreach($val as $actkey=>$actions){
                        if($actions!='' && $actkey!='' )
                        $action[$actkey][$key] = $action[$actkey][$key] + 1; 
                         if($actions!='' && $actkey!='' )
                            $actionsval[$actkey] = 1;
                    }
                    
                }

                foreach($arr as $key=>$val){
                    foreach($val as $actkey=>$actions){
                       foreach($actionsval as $actfkey=>$actionsf){
                            if($actkey==$actfkey){
                                $schoolarr[$key][$actfkey] = $actions;
                            } else if(!isset($schoolarr[$key][$actfkey])) {
                                $schoolarr[$key][$actfkey] = 0;
                            }
                        } 
                    }
                }
                
                // print_r($schoolarr);exit;
        date_default_timezone_set('Asia/calcutta');
        $this->load->library('excel');
         // echo 'test';exit;
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('SST report By Action');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Action');
        $this->excel->getActiveSheet()->setCellValue('A2', 'School Name');

        $row = 2;
                $col = 1;
                foreach($actionsval as $actionfkey=>$actionfval){
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$actionfkey);
                    $col++;
                }

                $row = 3;
                $col = 0;
                foreach($schoolarr as $schoolname=>$schoolactar){
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$schoolname);
                    foreach($schoolactar as $schoolaction=>$action_val){
                        $col++;
                         $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$action_val);
                         
                    }
                    $col = 0;
                    $row++;
                }
       $filename='SST-Needs-'.date('d/m/y').'.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
                exit;
    }

    function district_sst_summary(){
         $data['idname'] = 'classroom';

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $this->load->Model('actions_home_model');
            $data['action_homes'] = $this->actions_home_model->get_all_action_home();
            
            $this->load->Model('actions_school_model');
            $data['action_schools'] = $this->actions_school_model->get_all_action_school();
            
            $this->load->Model('actions_medical_model');
            $data['action_medicals'] = $this->actions_medical_model->get_all_action_medical();
        
        if ($this->session->userdata('login_type') == 'teacher') {

            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_action', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'classroom';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/retrieve_success_report_action', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'classroom';
            
            $this->load->Model('schoolmodel');
            $data['schools'] = $this->schoolmodel->getschoolbydistrict();
            
            $this->load->Model('schoolmodel');
            $data['grades'] = $this->schoolmodel->getallgrades();
            
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('classroom/district_summary_report_action', $data);
        }
    }

    function district_sst_summary_need(){
        $data['idname'] = 'classroom';
        $data['idname'] = 'classroom';
        $this->load->Model('schoolmodel');
        $data['schools'] = $this->schoolmodel->getschoolbydistrict();
        $this->load->Model('schoolmodel');
        $data['grades'] = $this->schoolmodel->getallgrades();
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('classroom/district_summary_report_need', $data);
    }

    function bahevior_baseline(){
        $this->load->model('problem_behaviour_model');
        $behaviour_data = $this->problem_behaviour_model->get_all_behaviour_data();

        // $this->load->model('studentmodel');
        $students = $this->problem_behaviour_model->getStudentBySchool();
        // print_r($students);exit;

        date_default_timezone_set('Asia/calcutta');
        $this->load->library('excel');
         // echo 'test';exit;
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Behavior Baseline Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Behavior Baseline Report');
        $this->excel->getActiveSheet()->setCellValue('A2', 'Student Names');
       
        $row = 2;
        $col = 1;
        foreach($behaviour_data as $key=>$value){
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$value->behaviour_name);
            $col++;
        }
         $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Total");
         $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Risk(Circle)\n L M H");



        $row = 3;
        $col = 0;
        foreach($students as $student){
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$student['firstname'].' '.$student['lastname']);
            $row++;
        }

        $filename='Behavior-Baseline-'.date('d/m/y').'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        exit;
    }

}
