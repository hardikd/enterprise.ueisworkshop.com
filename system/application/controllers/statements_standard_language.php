<?php
/**
 * Countries Controller.
 *
 */
class Statements_standard_language extends	MY_Auth {
function __Construct()
	{
			parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		$this->load->library('user_agent');
	}

	function index()
	{	
		$data['view_path']=$this->config->item('view_path');
		
		$this->load->Model('schoolmodel');
			$data['subjects'] = $this->schoolmodel->getallsubjects();
		
		$this->load->view('statements_standard_language/statements_standard_language',$data);
	}
	
	function standard_language_list($page){
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();

		$this->load->model('statements_standard_language_model');
		
		$total_records = $this->statements_standard_language_model->get_all_dataCount();
		
		$data['alldata'] = $status = $this->statements_standard_language_model->get_all_data($page,$per_page);
		
		$data['pagination'] = $this->do_pagination($total_records,$per_page,$page,'standard_language');
						

		$data['view_path']=$this->config->item('view_path');
		$this->load->view('statements_standard_language/statements_standard_language_edit',$data);
	}
	
	function standard_language_insert()
	{
		if($this->input->post('id')){
			$id = $this->input->post('id');
			$standard_language = $this->input->post('standard_language');
			$status=$this->input->post('status');
			$data=array('standard_language'=>$standard_language,'status'=>$status);
			$this->load->model('statements_standard_language_model');
			$update = $this->statements_standard_language_model->update('standard_language',$data,array('id'=>$id));
			if($update){
				echo "DONE";	
			} else {
				echo "ERROR";
			}

		} else{
			$district_id=$this->input->post('district_id');
			$is_delete=$this->input->post('is_delete');
			$standard_language=$this->input->post('standard_language');
			$status=$this->input->post('status');
			$standard_language_data=array('district_id'=>$district_id,'is_delete'=>$is_delete,'standard_language'=>$standard_language,'status'=>$status);
			$this->load->model('statements_standard_language_model','standard_language');
			$insert = $this->standard_language->insert('standard_language',$standard_language_data);
			if($insert){
				echo "DONE";	
			} else {
				echo "ERROR";
			}
			
		}
	}
 function edit($id)
	{
		$this->load->model('statements_standard_language_model');
		$data['all']=$this->statements_standard_language_model->get_standard_languageById(array('id'=>$id));
		print_r(json_encode($data['all']));exit;
	}

	public function delete()
	{
		$standard_remove =array('is_delete'=>'1');
		$this->load->model('statements_standard_language_model');
		$schedule_id = $this->statements_standard_language_model->delete_standard_language('standard_language',$standard_remove);
		echo 'DONE';
	}	
	
function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	 		$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";


						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
				}
	
	
}
  
