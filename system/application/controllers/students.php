<?php

/**
 * parent Controller.
 *
 */
class Students extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
//		if($this->is_teacher()==false || $this->is_observer()==false){
//			//These functions are available only to teachers - So redirect to the login page
//			redirect("/");
//		}
		
	}
	
function index()
{
	    
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			}
		$data['view_path']=$this->config->item('view_path');
		$this->load->Model('periodmodel');
		$data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));	
               // print_r($data['periods']);exit;
		$this->load->Model('dist_grademodel');
		$data['grades'] = $this->dist_grademodel->getdist_gradesById();
		$this->load->view('students/all',$data);	

}
function getGrades()
	{
		$this->load->Model('studentmodel');
		$data['grades']=$this->studentmodel->getGrades();
	    echo json_encode($data);
		exit;
	
	}
	function getstudents()
	{
		$this->load->Model('studentmodel');
		$data['student']=$this->studentmodel->getstudents();
//                echo $this->db->last_query();
	    echo json_encode($data);
		exit;
	
	}
	function getstudentnumber()
	{
		$this->load->Model('studentmodel');
		$data['student']=$this->studentmodel->getstudentnumber();
	    echo json_encode($data);
		exit;
	
	}
	function add_student()
	{
	 
	
		$this->load->Model('studentmodel');
		$pstatus=$this->studentmodel->check_student_exists();
                
	if($pstatus=='success')
		 {
		$status=$this->studentmodel->add_student();
		if($status!=0){
		       $data['message']="Student Added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else 
		{
			$data['message']=$pstatus ;
		    $data['status']=0 ;
		}
		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function getstudentsdata($day,$period,$page)
	{
		
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('studentmodel');
		$total_records = $this->studentmodel->getstudentCount($day,$period);
		
			$status = $this->studentmodel->getstudentsdata($day,$period,$page, $per_page);
		
		$class_room_all=$this->studentmodel->getallclassroom($day);
		
		if($status!=FALSE){
			print '<table cellspacing="0" cellpadding="0" width="635px"><tr><td>Class Room:'.$status[0]['class_name'].'</td></tr></table>';
			print '<table cellspacing="0" cellpadding="0" width="635px"><tr><td>Subject:'.$status[0]['subject_name'].'</td></tr></table>';
			
			
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>First Name</td><td>Last Name</td><td>Grade</td><td>Student Number</td><td>Email</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['student_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['firstname'].'</td>
					  <td>'.$val['lastname'].'</td>
					  <td>'.$val['grade'].'</td>					  
					  <td>'.$val['student_number'].'</td>					  
					  ';
					  	 if($val['email']==1) { 
						print '<td id="status_'.$val['student_id'].'"><img src="'.SITEURLM.'/images/home_on.jpg" style="cursor:pointer;" title="Click to InActivate" onclick="inactive('.$day.','.$val['student_id'].')" /> </td>';
					 } else  {  
						print '<td id="status_'.$val['student_id'].'"><img src="'.SITEURLM.'/images/home_off.jpg" style="cursor:pointer;" title="Click to Activate" onclick="active('.$day.','.$val['student_id'].')" /> </td>';
					 } 
					   
				
				print '<td nowrap><input  title="Delete" type="button"  name="Delete" value="Delete" onclick="studentdelete('.$day.','.$val['teacher_student_id'].')" ></td>		</tr>';
				$i++;
				$class_room_id=$val['class_room_id'];
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'student _'.$day.'_'.$period);
						print $pagination;	
						
                     print '<table cellspacing="0" cellpadding="0" width="635px"><tr><td>
		<input class="btnbig" type="button" name="student_add" id="student_add" onclick="add_student('.$day.','.$period.','.$class_room_id.')" title="Add Student" value="Add Student" >
		</td>';
if($class_room_all!=false)
			{
				
				print '<td>or &nbsp;Replicate Students Across Periods:&nbsp;Select:<select name="'.$day.'_'.$period.'_'.$status[0]['class_room_id'].'" id="'.$day.'_'.$period.'_'.$status[0]['class_room_id'].'"><option value="all">All Periods</option>';
				foreach($class_room_all as $class_room_all_val)
				{
					if($class_room_all_val['period_id']!=$period)
					{
					print '<option value='.$class_room_all_val['period_id'].'_'.$class_room_all_val['class_room_id'].'>'.$class_room_all_val['start_time'].' '.$class_room_all_val['end_time'].'</option>';
				   }
				}
				print '&nbsp;<input type="button" name="copy" id="copy" Value="Copy" onclick="copytoperiods('.$day.','.$period.','.$status[0]['class_room_id'].')"></td>';
			
			}	

			print '</tr></table>';			
		}else{
			  $class_room=$this->studentmodel->getclassroom($day,$period);
			if($class_room!=false)
			{
			print '<table cellspacing="0" cellpadding="0" width="635px"><tr><td>Class Room:'.$class_room[0]['class_name'].'</td></tr></table>';
			print '<table cellspacing="0" cellpadding="0" width="635px"><tr><td>Subject:'.$class_room[0]['subject_name'].'</td></tr></table>';			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>First Name</td><td>Last Name</td><td>Grade</td><td>Student Number</td><td>Email</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Students Found.</td></tr></table>";
			 print '<div style="padding-right:520px;">
		<input class="btnbig" type="button" name="student_add" id="student_add" onclick="add_student('.$day.','.$period.','.$class_room[0]['class_room_id'].')" title="Add Student" value="Add Student" >
		</div>'; 
			}
			else
			{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf'><tr><td>Not Assigned Any Subject For This Period</td></tr></table> ";
			
			}
			$pagination=$this->do_pagination($total_records,$per_page,$page,'student _'.$day.'_'.$period);
						print $pagination;	
		}
		
		
	}
	
	function copy()
	{
		$this->load->Model('studentmodel');
		$pdata=$this->input->post('pdata');	
		$result = $this->studentmodel->getalldaystudents($pdata['day'],$pdata['period']);		
		if($pdata['copy']=='all')
		{
			$class_room_all=$this->studentmodel->getallclassroom($pdata['day']);
			if($class_room_all!=false && $result!=false)
			{
				foreach($class_room_all as $class_room_all_val)
				{
					if($class_room_all_val['period_id']!=$pdata['period'])
					{
					foreach($result as $resultval)
			{
			  $sresult = $this->studentmodel->check_student_exists($pdata['day'],$class_room_all_val['period_id'],$resultval['student_id']);
			  if($sresult=='success')
			  {
				$this->studentmodel->add_student($pdata['day'],$class_room_all_val['period_id'],$class_room_all_val['class_room_id'],$resultval['student_id']);
			  }
			
			}
					
					
					}
				
				
				}
			
			}
		}
		else
		{
		 
		
		if($result!=false)
		{
			
			$p=explode('_',$pdata['copy']);			
			foreach($result as $resultval)
			{
			  $sresult = $this->studentmodel->check_student_exists($pdata['day'],$p[0],$resultval['student_id']);
			  if($sresult=='success')
			  {
				$this->studentmodel->add_student($pdata['day'],$p[0],$p[1],$resultval['student_id']);
			  }
			
			}
		
		}
		  
		 
		
		}
		$data['status']=1;
		echo json_encode($data);
		exit;
	
	}
	function copyall()
	{	
		$this->load->Model('time_tablemodel');
		$this->load->Model('studentmodel');
		
		$pdata=$this->input->post('pdata');	
                
		if($pdata['day']=='all')
		{
			for($i=0;$i<=6;$i++)
			{
				if($i!=$pdata['id'])
				{
				
				$dresult = $this->time_tablemodel->getallperiods($i);	
			if($dresult!=false)
			{
			foreach($dresult as $dvalue)
			{
				$result = $this->studentmodel->getalldaystudents($pdata['id'],$dvalue['period_id']);	
			if($result!=false)
			{			
				foreach($result as $resultval)
			{
			  $sresult = $this->studentmodel->check_student_exists($i,$dvalue['period_id'],$resultval['student_id']);
			  if($sresult=='success')
			  {
				$this->studentmodel->add_student($i,$dvalue['period_id'],$dvalue['class_room_id'],$resultval['student_id']);
			  }
			
			}
			
			}
			}
			}
		
				
				
				
				}
			
			
			
			}
		
		}
		else
		{
			$dresult = $this->time_tablemodel->getallperiods($pdata['day']);	
			if($dresult!=false)
			{
			foreach($dresult as $dvalue)
			{
				$result = $this->studentmodel->getalldaystudents($pdata['id'],$dvalue['period_id']);	
			if($result!=false)
			{			
				foreach($result as $resultval)
			{
			  $sresult = $this->studentmodel->check_student_exists($pdata['day'],$dvalue['period_id'],$resultval['student_id']);
			  if($sresult=='success')
			  {
				$this->studentmodel->add_student($pdata['day'],$dvalue['period_id'],$dvalue['class_room_id'],$resultval['student_id']);
			  }
			
			}
			
			}
			}
			}
		
		}
		//echo $this->db->last_query();exit;
		$data['status']=1;
		echo json_encode($data);
		exit;
	
	}
	function delete($student_id)
	{
		
		$this->load->Model('studentmodel');
		$result = $this->studentmodel->deletestudent($student_id);
//                echo $this->db->last_query();
		if($result==true){
			$data['status']=1;
                        $data['message'] = 'Student removed Successfully.';
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
                        $data['message'] = 'Student does not deleted. Please try again.';
		}
		echo json_encode($data);
		exit;
		
	}
	 function changestatus($type)
	{		
		
		$this->load->Model('studentmodel');
	  $user_id=$this->input->post('user_id');
	 
	  if($type=='inactive')
	  {
	    
		echo $status = $this->studentmodel->changestatus(0,$user_id);
	  
	  
	  }
	  else
      {
        
		echo $status = $this->studentmodel->changestatus(1,$user_id);

       }	  
	  
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	

	

}

?>