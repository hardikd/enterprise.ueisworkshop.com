<?php

class Communication extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_teacher()==false && $this->is_observer()==false && $this->is_user()==false){
			//These functions are available only to observers - So redirect to the login page
			redirect("index");
		}
		
	}

	function index()
	{
		$data['view_path']=$this->config->item('view_path');
	    $this->load->Model('schoolmodel');
		$this->load->Model('teachermodel');
		$this->load->Model('teacher_grade_subjectmodel');
		$data['grades']=$this->schoolmodel->getallgrades();
		$data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));	
		$data['gradeteachersall']=$this->teacher_grade_subjectmodel->staff_teacher_grade(false);	  		
		
		$this->load->view('communication/index',$data);
	
	
	}
	function staff_report()
	{
		$data['view_path']=$this->config->item('view_path');
	    $this->load->Model('schoolmodel');
		$this->load->Model('teachermodel');
		$this->load->Model('teacher_grade_subjectmodel');
		$data['grades']=$this->schoolmodel->getallgrades();
		$data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));	
		$data['gradeteachersall']=$this->teacher_grade_subjectmodel->staff_teacher_grade(false);	  		
		
		$this->load->view('communication/staff_report',$data);
	
	
	}
	function parent_report()
	{
		  if($this->session->userdata('login_type')=='user'){
                $this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'tools/parent_bridge');
            }
		 $data['idname']='tools';
		$data['view_path']=$this->config->item('view_path');
		$this->load->Model('schoolmodel');
		$this->load->Model('parentmodel');	
		
		$data['parents']=$this->parentmodel->get_parents_all();	
	
		$data['grades']=$this->schoolmodel->getallgrades();	
	
		$this->load->Model('teacher_grade_subjectmodel');
		$this->load->Model('class_roommodel');
		$this->load->Model('periodmodel');

		$data['gradeteachersall']=$this->teacher_grade_subjectmodel->staff_parent_grade(false);	  		
		
		$data['classrooms']=$this->class_roommodel->getclassroombyschool($this->session->userdata('school_id'));	  		
		
		$data['pariods']=$this->periodmodel->getperiodbyschoolid($this->session->userdata('school_id'));	  		
		
		$this->load->view('communication/parent_report',$data);
	
	
	}
	function parents()
	{
            //echo $this->session->userdata('login_type');exit;
            if($this->session->userdata('login_type')=='user'){
                $this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'tools/parent_bridge');
            }
		 $data['idname']='tools';
		$data['view_path']=$this->config->item('view_path');	    
		$this->load->Model('schoolmodel');
		$this->load->Model('parentmodel');		
		$data['parents']=$this->parentmodel->get_parents_all();	
		$data['grades']=$this->schoolmodel->getallgrades();		
		$this->load->Model('teacher_grade_subjectmodel');
		$this->load->Model('class_roommodel');
		$this->load->Model('periodmodel');
		$data['gradeteachersall']=$this->teacher_grade_subjectmodel->staff_parent_grade(false);	  		
		
		$data['classrooms']=$this->class_roommodel->getclassroombyschool($this->session->userdata('school_id'));
		//print_r($data['classrooms']);exit;	  		
		$data['pariods']=$this->periodmodel->getperiodbyschoolid($this->session->userdata('school_id'));	  		
		
		
		$this->load->view('communication/parents',$data);
	
	
	}
	function send_staff()
	{
	
	$pdata=$this->input->post('pdata');
	
	$d=$pdata['homedate'];
	$homeworktext=$pdata['homework'];
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	
	
	  $this->load->Model('teachermodel');
	  $this->load->Model('communicationmodel');
	  
	  
	  
	  foreach($pdata['teacher'] as $teacherval)
	  {
	  
	$teacher=$this->teachermodel->getteacherById($teacherval);
	
		if($teacher!=false)
		{
		
	
	
	
	if($this->valid_email($teacher[0]['email']))
			{
			 
			 $staff_save=$this->communicationmodel->save_staff($teacherval,$homeworktext,$pdata['type'],$pdata['value']);
			 $email=$teacher[0]['email'];
			$name=$teacher[0]['firstname'].' '.$teacher[0]['lastname'];
			$subject="Staff Communication For Date:$d";
			
			 $message="<html><head><title>Staff Communication For Date:$d </title></head><body>						
						
						<table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
						<tr><td><font  size='5px'> Hi $name, </font></td></tr>
						<tr><td><font  size='5px'> $homeworktext </font></td></tr>	
						<tr><td > Powered By, </td></tr><tr><td > Workshop </td></tr></table></body></html>";
	   
			mail($email,$subject,$message,$headers);
	}
	}	
	}		
			
				$data['status']=1 ;
	
	 echo json_encode($data);
	  	  exit;
		 
	
	
	}
	function parent_send_staff()
	{
	
	$pdata=$this->input->post('pdata');
	$d=$pdata['homedate'];
	$homeworktext=$pdata['homework'];
	
	$headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
	
	/*}*/
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0'.PHP_EOL;
	$headers .= 'Content-Type: text/html; charset=iso-8859-1'.PHP_EOL;
	$headers .= 'X-Mailer: PHP/' . phpversion().PHP_EOL;
	
	
	  $this->load->Model('parentmodel');
	  $this->load->Model('communicationmodel');
	  
	  
	  
	  foreach($pdata['teacher'] as $teacherval)
	  {
	  
	$teacher=$this->parentmodel->getparentById($teacherval);
	
		if($teacher!=false)
		{
		
	
	
	
	if($this->valid_email($teacher[0]['email']))
			{
			$parent_save=$this->communicationmodel->parent_staff($teacherval,$homeworktext,$pdata['type'],$pdata['value']);
			 $email=$teacher[0]['email'];
			$name=$teacher[0]['firstname'].' '.$teacher[0]['lastname'];
			$subject="Parent Notification For Date:$d";
			
			 $message="<html><head><title>Parent Notification For Date:$d </title></head><body>		
						
						<table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
						<tr><td><font  size='5px'> Hi $name, </font></td></tr>
						<tr><td><font  size='5px'> $homeworktext </font></td></tr>
						<tr><td > Powered By, </td></tr><tr><td > Workshop </td></tr></table></body></html>";
	   
			mail($email,$subject,$message,$headers);
	}
	if($this->valid_email($teacher[0]['emailsub']))
			{
			 $email=$teacher[0]['emailsub'];
			$name=$teacher[0]['firstnamesub'].' '.$teacher[0]['lastnamesub'];
			$subject='Parent Notification For Date:$d';
			
			 $message="<html><head><title>Parent Notification For Date:$d </title></head><body>						
						<table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'>
						<tr><td><font  size='5px'> Hi $name, </font></td></tr>
						<tr><td><font  size='5px'> $homeworktext </font></td></tr>
						<tr><td > Powered By, </td></tr><tr><td > Workshop </td></tr></table></body></html>";
	   
			mail($email,$subject,$message,$headers);
	}
	}	
	}		
			
				$data['status']=1 ;
	
	 echo json_encode($data);
	  	  exit;
		 
	
	
	}
	
	function chagegrade()
	{
		$pdata=$this->input->post('gdata');
		$this->load->Model('teacher_grade_subjectmodel');
		if($pdata['id']=='all')
		{
			 
			$teachers=$this->teacher_grade_subjectmodel->staff_teacher_grade();
		
		}
		else
		{
			$teachers=$this->teacher_grade_subjectmodel->staff_teacher_grade($pdata['id']);		
		
		}
		echo '<table style="padding-left:40px;" class="gradeteachers"><tr>';
		if($teachers!=false)
		{
		$i=0;
			
			foreach($teachers as $teachersval)
			{
			if($i%3==0 && $i!=0)
			{
			
			echo '</table>
			</td>
			<td >
				<table BORDER="0" >';

			} else if($i==0)	
			{

			echo '<td>    <table BORDER="0" >';
			}

			echo '<tr>
			<td>
			<input type="checkbox" checked=checked name="gradeallt[]" id="gradeall_'.$teachersval['teacher_id'].'" value="'.$teachersval['teacher_id'].'">'.$teachersval['firstname'].' '.$teachersval['lastname'].'</td>
			</tr>';
			$i++; 
			}
			
			echo '</table></td>';
		
		
		
		}
		else
		{
			echo '<td>No Teachers Found.</td>';
		
		}
	echo '</tr></table>';
		
	}
	function chageparentgrade()
	{
		$pdata=$this->input->post('gdata');
		$this->load->Model('teacher_grade_subjectmodel');
		if($pdata['id']=='all')
		{
			 
			$teachers=$this->teacher_grade_subjectmodel->staff_parent_grade();
		
		}
		else
		{
			$teachers=$this->teacher_grade_subjectmodel->staff_parent_grade($pdata['id']);		
		
		}
		echo '<table class="gradeteachers" style="padding-left:40px;"><tr>';
		if($teachers!=false)
		{
		$i=0;
			
			foreach($teachers as $teachersval)
			{
			if($i%3==0 && $i!=0)
			{
			
			echo '</table>
			</td>
			<td >
				<table BORDER="0" >';

			} else if($i==0)	
			{

			echo '<td>    <table BORDER="0" >';
			}

			echo '<tr>
			<td>
			<input type="checkbox" checked=checked name="gradeallt[]" id="gradeall_'.$teachersval['parents_id'].'" value="'.$teachersval['parents_id'].'">'.$teachersval['firstname'].' '.$teachersval['lastname'].'</td>
			</tr>';
			$i++; 
			}
			
			echo '</table></td>';
		
		
		
		}
		else
		{
			echo '<td>No Parents Found.</td>';
		
		}
	echo '</tr></table>';
		
	}
	function parent_get_staff()
	{
		
		$this->load->Model('teacher_grade_subjectmodel');
		
			$teachers=$this->teacher_grade_subjectmodel->staff_parent_class_grade();		
			
		
		echo '<table class="classroomteachers" style="padding-left:40px;"><tr>';
		if($teachers!=false)
		{
		$i=0;
			
			foreach($teachers as $teachersval)
			{
			if($i%3==0 && $i!=0)
			{
			
			echo '</table>
			</td>
			<td >
				<table BORDER="0" >';

			} else if($i==0)	
			{

			echo '<td>    <table BORDER="0" >';
			}

			echo '<tr>
			<td>
			<input type="checkbox" checked=checked name="classroomallt[]" id="classroomall_'.$teachersval['parents_id'].'" value="'.$teachersval['parents_id'].'">'.$teachersval['firstname'].' '.$teachersval['lastname'].'</td>
			</tr>';
			$i++; 
			}
			
			echo '</table></td>';
		
		
		
		}
		else
		{
			echo '<td>No Parents Found.</td>';
		
		}
	echo '</tr></table>';
		
	}
	function search_parent_get_staff()
	{
		
		$this->load->Model('teacher_grade_subjectmodel');
		
			$teachers=$this->teacher_grade_subjectmodel->search_staff_parent_class_grade();		
			
		
		echo '<table class="searchteachers" style="padding-left:40px;"><tr>';
		if($teachers!=false)
		{
		$i=0;
			
			foreach($teachers as $teachersval)
			{
			if($i%3==0 && $i!=0)
			{
			
			echo '</table>
			</td>
			<td >
				<table BORDER="0" >';

			} else if($i==0)	
			{

			echo '<td>    <table BORDER="0" >';
			}

			echo '<tr>
			<td>
			<input style="margin-top:-2px !important;" type="checkbox" checked=checked name="searchallt[]" id="searchall_'.$teachersval['parents_id'].'" value="'.$teachersval['parents_id'].'">&nbsp;&nbsp;&nbsp;'.$teachersval['firstname'].' '.$teachersval['lastname'].'&nbsp;&nbsp;&nbsp;&nbsp; <b>Student:&nbsp;&nbsp;</b>'.$teachersval['studentname'].'</td>
			</tr>';
			$i++; 
			}
			
			echo '</table></td>';
		
		
		
		}
		else
		{
			echo '<td>No Parents Found.</td>';
		
		}
	echo '</tr></table>';
		
	}
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
	
}	