<?php
/**
 * eldrubric Controller.
 *
 */
class Eldrubric extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{	   
	    
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('eldrubric/index',$data);
	  
	  
	}
	
	
	function geteldrubrics($page)
	{
		
		
		$this->load->Model('eldrubricmodel');
		$sub = $this->eldrubricmodel->geteldbydistrict();
		$district_id=$sub[0]['subject_id'];
		if($district_id==false)
		{
		$district_id=0;
		}
		if($page!='all')
		{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
	
		
		$total_records = $this->eldrubricmodel->geteldrubricCount($district_id);
		
		
			$status = $this->eldrubricmodel->geteldrubrics($page, $per_page,$district_id);
			
	
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>ELD Rubric Elements </div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>Grade</td><td>Strand</td><td>Standard</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['subject_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['subject'].'</a></td>';
				print '<td>'.$val['grade'].'</a></td>';
				print '<td>'.$val['strand'].'</a></td>';
				print '<td>'.$val['standard'].'</a></td>';
				
				
				
				
				print '<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="pointedit('.$val['subject_id'].')"></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'eldrubrics');
						print $pagination;	
						
                        
		
		/*$this->load->Model('eldrubricmodel');
		$total_records = $this->eldrubricmodel->geteldrubricCount();
		
		
			$status = $this->eldrubricmodel->geteldrubrics($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>ELD Rubric Elements</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['subject_id'].'" class="'.$c.'" >';
			
				print '
				<td>'.$val['subject_name'].'</td>				
				
				<td nowrap><input  class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="pointedit('.$val['subject_id'].')"></td>		</tr>';
				$i++;}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'eldrubrics');
						print $pagination;	*/
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Subject</td><td>Grade</td><td>Strand</td><td>Standard</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No ELD Rubric Elements Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'eldrubrics');
						print $pagination;	
		}
		
		}
		
	}
	
	
	
	function getAllsubgroups($point_id)
	{
		if(!empty($point_id))
	  {
		$this->load->Model('eldrubricmodel');
		
		$data['eldrubric']=$this->eldrubricmodel->getAllsubgroups($point_id);
		if($data['eldrubric']!=false)
		{
					
			$data['eldrubric']=$data['eldrubric'];
		}
		
		$this->load->Model('rubric_datamodel');
		
		$data['rubric_data']=$this->rubric_datamodel->getallplans();
		if($data['rubric_data']!=false)
		{
					
			$data['rubric_data']=$data['rubric_data'];
		}

		echo json_encode($data);
		exit;
	  }
	
	
	}
	
	
	function update_eldrubric()
	{
	$this->load->Model('eldrubricmodel');
	    $status=$this->eldrubricmodel->update_eldrubric();
		if($status==true){
		       $data['message']="eldrubric Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;		
	}
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
				}
}	