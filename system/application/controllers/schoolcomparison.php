<?php
 $path_file =  'libchart/classes/libchart.php';
 include_once($path_file);

$view_path="system/application/views/";
require_once($view_path.'inc/html2pdf/html2pdf.class.php');
class Schoolcomparison extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false){
		//These functions are available only to schools - So redirect to the login page
			redirect("index");
		}
		if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
		
	}

	function index()
	{
	  $data['view_path']=$this->config->item('view_path');
	  
	  if($this->session->userdata('login_type')=='user')
	  {
		$district_id = $this->session->userdata('district_id');
		$district_userid = $this->session->userdata('dist_user_id');
		$this->load->Model('Reportschooltestmodel');
		$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);
				
		$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
		$data['testname'] = $this->Reportschooltestmodel->gettestname();
	
		//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
		$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);
		$this->load->view('graphreports/schoolcomparison',$data);
	  }
	
	  
	
	
	}
	
	
	
	
	function single_school_report()
	{
		
		
	  $data['view_path']=$this->config->item('view_path');	  
	  
	  if($this->session->userdata('login_type')=='user')
	  {
		  
		$district_id = $this->session->userdata('district_id');
		$district_userid = $this->session->userdata('dist_user_id');
		$this->load->Model('Reportschooltestmodel');
		$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);
				
		$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
		$data['testname'] = $this->Reportschooltestmodel->gettestname();
	
		//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
		$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);
		
		$this->load->view('report/single_school_test',$data);			
		
	  }

	}
	

	function getschoolbyschooltype()
	{
		$school_type_id =  $_REQUEST['id'];
		$this->load->model('Reportschooltestmodel');
		$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($school_type_id);
		echo '<option value="all">All</option>';
		foreach($data['schools'] as $schools)
		{
			$schoolid = $schools['school_id'];
			$school_name = $schools['school_name'];
			
			echo '<option value="'.$schoolid.'">'.$school_name.'</option>';
		}
	} 

	
	
	function get_single_school_test()
	{
		
			
		 $from = $this->input->post('from');
		 $to = $this->input->post('to');
		
		$schooltypeid = $this->input->post('school_type');
		$testname = $this->input->post('testname');
		$schools = $this->input->post('schools');
		$grade = $this->input->post('grade');
		$this->load->model('Reportschooltestmodel');
		
		//get all schools
		
		$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($schooltypeid);
		
		

		$schoolid = array();
		if(is_array($data['schools']))
		{		
		foreach($data['schools'] as $key => $val)
		{
			$schoolid[]=$val['school_id']."<br/>";
		}
		}
		
		
		//get users by school id and grade id

		$data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusers($schoolid,$grade);
		
		
	
		
		
		$userid=array();
		$sidarr = array();
		foreach($data['getuseracctogradeandschool'] as $key => $getall)
		{
			// $userid[] = $getall['UserID'];
		foreach($getall as  $key => $value)
		{	
				
		
		$getall[$key]['school_id'];   // school id
		
		$userid = $getall[$key]['UserID'];    // user id
			
					$sidarr[]=$value['school_id'];
					//$userid[]=$value['UserID'];
		}
		
			
		}
		
	
	array_unique($sidarr);    // this is a school uniq id
	$sdata = array();
	
	
foreach(array_unique($sidarr) as $key =>$val)
{	
	$sdata[] =$this->Reportschooltestmodel->getfinaldata($sidarr[$key],$testname,$from,$to);	


}

/*echo count($sdata);
	echo "<pre>";
	print_r($sdata);
	die;*/


$data['view_path'] = $this->config->item('view_path');
$data['finalscore'] = $sdata;


$this->load->view('report/singletestreport',$data);
		
					
	}
	
	
	function multiple_school_report()
	{
		
	  $data['view_path']=$this->config->item('view_path');	  
	 if($this->session->userdata('login_type')=='user')
	  {
	/*	$district_id = $this->session->userdata('district_id');
		$district_userid = $this->session->userdata('dist_user_id');
		$this->load->Model('Reportschooltestmodel');
		$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);
		$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
		$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
		$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);*/
		
		$district_id = $this->session->userdata('district_id');
		$district_userid = $this->session->userdata('dist_user_id');
		$this->load->Model('Reportschooltestmodel');
		$data['schools'] = $this->Reportschooltestmodel->getschoolbydistrict($district_id);
				
		$data['grades'] = $this->Reportschooltestmodel->getschoolgrades($district_id);
		$data['testname'] = $this->Reportschooltestmodel->gettestname();
	
		//$data['testname'] = $this->Reportschooltestmodel->getschooltestname($district_id);
		$data['schools_type'] = $this->Reportschooltestmodel->getschooltype($district_id);
		$data['scoretype'] = $this->Reportschooltestmodel->getscoretype($district_id);
		
		
		
		$this->load->view('report/multiple_school_report',$data);
	  }

	}
	
	function get_multiple_school_test()
	{	
	
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		 
		$schooltypeid = $this->input->post('school_type');
		$testname = $this->input->post('testname');
		$schools = $this->input->post('schools');
		$grade = $this->input->post('grade');
		$this->load->model('Reportschooltestmodel');
		
	
		//get all schools
		
		$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($schooltypeid);
		
		$schoolid = array();
		if(is_array($data['schools']))
		{		
		foreach($data['schools'] as $key => $val)
		{
			$schoolid[]=$val['school_id']."<br/>";
		}
		}
		
		
		//get users by school id and grade id

		$data['getuseracctogradeandschool'] =$this->Reportschooltestmodel->getallusers($schoolid,$grade);
		
		
		
		
		
		$userid=array();
		$sidarr = array();
		foreach($data['getuseracctogradeandschool'] as $key => $getall)
		{
			// $userid[] = $getall['UserID'];
		foreach($getall as  $key => $value)
		{	
				
		
		$getall[$key]['school_id'];   // school id
		
		$userid = $getall[$key]['UserID'];    // user id
			
					$sidarr[]=$value['school_id'];
					//$userid[]=$value['UserID'];
		}
		
			
		}
		
	
	array_unique($sidarr);    // this is a school uniq id
	$sdata = array();
	
	
	$finalallarr = array();
	
	foreach(array_unique($sidarr) as $key =>$val)
	{		
	
	$singleschoolavg = array();
	$singleschoolname=array();
	
		foreach($testname as $key1 => $val1)
			{
				 $testname[$key1];
			
			$sdata[] =$this->Reportschooltestmodel->getfinaldataMultiple($sidarr[$key],$testname[$key1],$from,$to);

					
			}
			
		
		foreach($sdata as $key => $value)
		{
			
			$singleschoolavg[] = $sdata[$key][0][0]['schoolaverage'];
			$singleschoolname[] = $sdata[$key][2][0]['school_name'];
		}	
	
	
	$schoolnames = array_values(array_unique($singleschoolname));
	
	
	$allttavg = array_sum($singleschoolavg);
	
	if($allttavg==0)
	{
		$mainavg =0;
	}	
	else
	{		 
		$cot =  count($singleschoolavg);
		 $mainavg = $allttavg/$cot;
	}
	
		
		  $finalallarr[] = round($mainavg);
		//echo '<pre>';
		 //print_r($schoolnames);
		
		
			
	} // main
	
	
	//print_r($schoolnames);
	
	$finalallarrmultiple = array();
	foreach($schoolnames as $key =>$val)
	{
		
		$finalallarrmultiple[0][] =  $schoolnames[$key];
		$finalallarrmultiple[1][] =  $finalallarr[$key];
		

	}
	$data['view_path']= $this->config->item('view_path');
	$data['final'] = $finalallarrmultiple;
	$this->load->view('report/multipletestreport',$data);
	
								
}



}
?>	
