<?php
/**
 * teacher_grade_subject Controller.
 *
 */
class Teacher_grade_subject extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false && $this->is_observer()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
		$data['idname']='attendance';
	  if($this->session->userdata("login_type")=='user'){
	  if($this->session->userdata('login_special') == !'district_management'){
	  
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
	  }else{
		$this->load->Model('schoolmodel');
		$data['schools']=$this->schoolmodel->getschoolbydistrict();
		$data['grades']=$this->schoolmodel->getallgrades();
		$this->load->Model('teacher_grade_subjectmodel');
		$data['status'][] = $this->teacher_grade_subjectmodel->getteacher_grade_subjects(false,false,'all');
//                print_r($data['status']);exit;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacher_grade_subject/all',$data);	
	  }
	  
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
		$this->load->Model('schoolmodel');
		$schools=$data['schools']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
		 $data['grades']=$this->schoolmodel->getallgrades();
                 
//	print_r($data['grades']);exit;
		
		$this->load->Model('teacher_grade_subjectmodel');
		$this->load->model('teachermodel');
                foreach($schools as $school){
			$data['status'][] = $this->teacher_grade_subjectmodel->getteacher_grade_subjects(false,false,$school['school_id']);
                 $data['teachers'][] = $this->teachermodel->getTeachersBySchool($school['school_id']);
		}
                
                
//                print_r($data['status']);exit;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacher_grade_subject/all',$data);	
	  
	  
	  }
	  
	
	}
	function getteacher_grade_subjects($page,$school_id=false)
	{
		if($school_id==false)
		{
		  $school_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('teacher_grade_subjectmodel');
		$total_records = $this->teacher_grade_subjectmodel->getteacher_grade_subjectCount($school_id);
		
			$status = $this->teacher_grade_subjectmodel->getteacher_grade_subjects($page, $per_page,$school_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<table class='table table-striped table-bordered' id='editable-sample'>
			<thead>
			<tr>
			<td class='no-sorting'>First Name</td>
			<td class='no-sorting'>Last Name</td>
			<td class='no-sorting'>Grade Subject</td>
			<td class='no-sorting'>School Name</td>
			<td class='no-sorting'>Actions</td>
			</tr></thead>";
			
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tbody><tr id="'.$val['teacher_grade_subject_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['firstname'].'</td>
					  <td>'.$val['lastname'].'</td>
					  <td class="hidden-phone">'.$val['grade_name'].' '.$val['subject_name'].'</td>
					   <td>'.$val['school_name'].'</td>
					   
					    
				<td class="hidden-phone" nowrap>
				 <button title="Edit" type="button"  value="Edit" name="Edit" onclick="teacher_grade_subjectedit('.$val['teacher_grade_subject_id'].')" class="btn btn-primary"><i class="icon-pencil"></i></button>
				 
				 <button title="Delete" type="button"  name="Delete" value="Delete" onclick="teacher_grade_subjectdelete('.$val['teacher_grade_subject_id'].')" class="btn btn-danger"><i class="icon-trash"></i></button>';
				
				
				
				 
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'teacher_grade_subjects');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='table table-striped table-bordered' id='editable-sample'>
			<thead>
			<tr>
			<td class='no-sorting'>First Name</td>
			<td class='no-sorting'>Last Name</td>
			<td class='no-sorting'>Grade Subject</td>
			<td class='no-sorting'>School Name</td>
			<td class='no-sorting'>Actions</td>
			</tr>
			<tr>
			<td valign='top' colspan='10'>No Teacher Grade Subjects Found.</td>
			</tr>
			</thead></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'teacher_grade_subjects');
						print $pagination;	
		}
		
		
	}
	
	function getteacher_grade_subjectinfo($teacher_grade_subject_id)
	{
		if(!empty($teacher_grade_subject_id))
	  {
		$this->load->Model('teacher_grade_subjectmodel');
		
		$data['teacher_grade_subject']=$this->teacher_grade_subjectmodel->getteacher_grade_subjectById($teacher_grade_subject_id);
		$data['teacher_grade_subject']=$data['teacher_grade_subject'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_teacher_grade_subject()
	{
	
	
		$this->load->Model('teacher_grade_subjectmodel');
		$pstatus=$this->teacher_grade_subjectmodel->check_teacher_grade_subject_exists();
		//$pstatus=1;
	if($pstatus==1)
		 {
		$status=$this->teacher_grade_subjectmodel->add_teacher_grade_subject();
		if($status!=0){
		       $data['message']="Teacher Grade Subject added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Grade Subject With Same Teacher In this School  Already Exists" ;
		    $data['status']=0 ;
		}
		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_teacher_grade_subject()
	{
		$this->load->Model('teacher_grade_subjectmodel');
	    $pstatus=$this->teacher_grade_subjectmodel->check_teacher_grade_subject_update();
		//$pstatus=1;
		if($pstatus==1)
		 {

		$status=$this->teacher_grade_subjectmodel->update_teacher_grade_subject();
		if($status==true){
		       $data['message']="Teacher Grade Subject Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']=" Grade Subject With Same Teacher In this School  Already Exists" ;
		    $data['status']=0 ;
		}
			
		echo json_encode($data);
		exit;		
	}
	
	function delete($teacher_grade_subject_id)
	{
		
		$this->load->Model('teacher_grade_subjectmodel');
		$result = $this->teacher_grade_subjectmodel->deleteteacher_grade_subject($teacher_grade_subject_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	/*function getteacher_grade_subjectsBySchool($school_id)
	{
	
		$this->load->Model('teacher_grade_subjectmodel');
		$data['teacher_grade_subject']=$this->teacher_grade_subjectmodel->getteacher_grade_subjectsBySchool($school_id);
		echo json_encode($data);
		exit;
	
	
	
	}*/
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
	
}	