<?php
/**
 * (Admin Or Home Or Startup) page Controller.
 *
 */
class Admin extends	Controller {

	function __construct()
	{
		parent::Controller();
		$this->no_cache();
	}

protected function no_cache()
	{
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	} 
	function index($error_code=0)
	{
		
		if($error_code!=0){
			//Get Error description
			$data['error_msg'] = $this->getErrorDesc($error_code);
			//Get the posted data to repopulate the form
			$data['username']=$this->input->post('username');
			$data['password']=$this->input->post('password');
			}
		$data['view_path']=$this->config->item('view_path');
		
		$this->load->view('admin/index',$data);
	}
	
	function home()
	{
	
	$this->load->Model('accountsmodel');
		$status = $this->accountsmodel->is_valid_login();
		
		if($status!=false){
			//set the session
			
			$this->session->set_userdata("login_type",'admin');
			$this->session->set_userdata("login_id",$status[0]['admin_id']);
			$this->session->set_userdata("login_required",'yes');
			
			$this->schoolindex();
		}else{
			// give appropriate message.
			$this->index(1);
			return;
		}
	
	
	}
	
	function logout()
	{
		//$this->session->unset_userdata('login_required'); 
		$this->session->sess_destroy();
		redirect('/');
		//redirect('index/index', 'refresh');

		}
	
	function schoolindex()
	{
	
	  if($this->session->userdata('login_type')=='admin')
	  {
	    $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('district/index',$data);
	  
	  }
	  
	
	
	}
	
	function getErrorDesc($error_code){
		switch($error_code){
			case "1":
				return "username or password are incorrect";
				break;
			
		}
	}
	
   function admin_logout()
	{
		$this->session->sess_destroy();
		redirect('admin/index/');	
	}
	
}
