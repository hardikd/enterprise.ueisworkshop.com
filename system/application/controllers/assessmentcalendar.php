<?php
/**
 * teacher Controller.
 *
 */
class Assessmentcalendar extends MY_Auth {
	
	function __Construct()
	{
		
		parent::__construct();
if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false && $this->is_teacher()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->no_cache();
	}
	
	
	function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
		
	
	public function index($year = null ,$month = null)
	{
 	
	$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
		  if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/enterprise/workshop/index.php/";</script>';
			} else {
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}

		}
		else
		{	
	
	if($this->session->userdata("login_type")=="user")
	  {
	
			$userid = $this->session->userdata('dist_user_id');
			if($userid == "")
			{
				redirect('report/index');	
			}
			else
			{	
				$data['view_path']=$this->config->item('view_path');	
					
			if (!$year) {
				 $year = date('Y');
			}
			if (!$month) {
				 $month = date('m');
			}
			$this->load->model('assessmentcalendarmodel');
			$bidayDetail=$this->input->post('birday');
			if ( $day = $this->input->post('day')) {
			$this->assessmentcalendarmodel->add_calendar_data("$year-$month-$day",$this->input->post('data'),$userId);
			}
			$data['calendar']=$this->assessmentcalendarmodel->generate($year,$month);
			$this->load->view('assessmentcalendar/calendar',$data);
		}
	
	}
	else if($this->session->userdata("login_type")=="observer")
	  {
			$observerid = $this->session->userdata('observer_id');
			if($observerid == "")
			{
				redirect('report/index');	
			}
			else
			{	
				$data['view_path']=$this->config->item('view_path');	
					
			if (!$year) {
				 $year = date('Y');
			}
			if (!$month) {
				 $month = date('m');
			}
			$this->load->model('assessmentcalendarmodel');
			$bidayDetail=$this->input->post('birday');
			if ( $day = $this->input->post('day')) {
			$this->assessmentcalendarmodel->add_calendar_data("$year-$month-$day",$this->input->post('data'),$userId);
			}
			$data['calendar']=$this->assessmentcalendarmodel->generate($year,$month);
			$this->load->view('assessmentcalendar/calendar',$data);
		  }
	  }
	  
	  else if($this->session->userdata("login_type")=="teacher")
	  {
		
		  	$teacherid = $this->session->userdata('teacher_id');
		 
			if($teacherid == "")
			{
				redirect('report/index');	
			}
			else
			{	
			$data['view_path']=$this->config->item('view_path');	
					
			if (!$year) {
				 $year = date('Y');
			}
			if (!$month) {
				 $month = date('m');
			}
			$this->load->model('assessmentcalendarmodel');
			$bidayDetail=$this->input->post('birday');
			if ( $day = $this->input->post('day')) {
			$this->assessmentcalendarmodel->add_calendar_data("$year-$month-$day",$this->input->post('data'),$userId);
			}
			$data['calendar']=$this->assessmentcalendarmodel->generate($year,$month);
		
			$this->load->view('assessmentcalendar/calendar',$data);
		  }
	  }
	  
		}
	}
	
	
}	