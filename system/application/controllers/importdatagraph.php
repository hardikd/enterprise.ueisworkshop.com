<?php   
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
 

class Importdatagraph extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{ 
	 	 error_reporting(0);
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('importdatagraphmodel');
				$data['records'] = $this->importdatagraphmodel->getallschools();
	 			$this->load->view('importdatagraph/index',$data);
	   
	 	 }
		
 	}
	
	function getassessmentHtml()
	{
		error_reporting(0);
		 $school_id = $_REQUEST['school_id']; 
		 $data = array();
		 $this->load->model('importdatagraphmodel');
		 $data= $this->importdatagraphmodel->getassessment($school_id);
		  $strClass ='';
 		 $strClass .= '<td align="right" >Select Assessment :&nbsp; </td> <td ><select class="combobox" name="assessment" id="assessment" onchange="updateassessment(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option> 
		<option value="0" >All</option>' ;
		if(count($data[0]['id'])>0)
		{
	 	   $strClass .= '<option value="0" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
 	 			 $strClass .='<option value="'.$value['assessment_name'].'">'.$value['assessment_name'].'</option>';
		 	}
         $strClass .= '</select> </td>';
 		 echo $strClass;
	}
	
	
	function getReportHtml()
	{
		error_reporting(0);
		$school_id= $_REQUEST['school_id'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		
			
		$this->load->model('importdatagraphmodel');
		$data= $this->importdatagraphmodel->getTestItem($school_id,$fDate,$tDate,$ass_id);
		$IntRow = count($data);
		$correctClounmstring  = "[";
		$incorrectClounmstring = "[";
		$strClusternams = "[";
		for($i=0;$i<$IntRow;$i++)
		{
			$strCluster = $data[$i]['cluster_item'];
			if($strCluster==''){$strCluster='No cluster';}
			$intCorrectPercentage = $data[$i]['per'];
			if($intCorrectPercentage==''){$intCorrectPercentage=0;}
			$intIncorrectPercentage = 100 - $intCorrectPercentage;
			//$correctClounmstring .="{  y: ".$intCorrectPercentage.", label: '".$strCluster."'},";
			//$incorrectClounmstring .="{  y: ".$intIncorrectPercentage.", label: '".$strCluster."'},";
			
			$correctClounmstring .= $intCorrectPercentage.",";
			$incorrectClounmstring .= $intIncorrectPercentage.",";
			$strClusternams .= "'".$strCluster."'".",";
		}
		$correctClounmstring = substr($correctClounmstring, 0, -1);
		$incorrectClounmstring = substr($incorrectClounmstring, 0, -1);
		$strClusternams = substr($strClusternams, 0, -1);
		
		$correctClounmstring .="]";
		$incorrectClounmstring .="]";
		$strClusternams .="]";
		$result = $correctClounmstring."*".$incorrectClounmstring."*".$strClusternams;
	echo $result;
	}
	
 	 
	
}	
