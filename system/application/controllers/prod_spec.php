<?php
/**
 * prod_spec Controller.
 *
 */
class Prod_spec extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('prod_spec/index',$data);
	
	}
	
	
	function getprod_specs($page)
	{
	
	    $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('prod_specmodel');
		$total_records = $this->prod_specmodel->getprod_specscount();
		
			$status = $this->prod_specmodel->getprod_specs($page, $per_page);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Product Specialists</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Firstname</td><td>Lastname</td><td>Username</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['prod_spec_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['firstname'].'</td>
				<td>'.$val['lastname'].'</td>
				<td>'.$val['userid'].'</td>
				
				<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="planedit('.$val['prod_spec_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="plandelete('.$val['prod_spec_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'prod_spec');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Product Specialist Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'prod_spec');
						print $pagination;	
		}
		
	
	
	
	
	}
	function add_plan()
	{
	
	
		$this->load->Model('prod_specmodel');
				
				
				if($this->prod_specmodel->check_prod_spec_exists()==true)
		 {
			
			$status=$this->prod_specmodel->add_plan();
			if($status!=0){
				   $data['message']="Product Specialist added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		}
		else
		{
		  $data['message']="Product Specialist With Same User Name   Already Exists" ;
		  $data['status']=0 ;

		}	
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function dist_add_prod_spec()
	{
	
	
		$this->load->Model('prod_specmodel');
				
				
				if($this->prod_specmodel->check_dist_prod_spec_exists()==true)
		 {
			
			$status=$this->prod_specmodel->add_dist_prod_spec();
			if($status!=0){
				   $data['message']="Product Specialist added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
		}
		else
		{
		  $data['message']=" The Product Specialist Already Added To This District" ;
		  $data['status']=0 ;

		}	
			
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_plan()
	{
	$this->load->Model('prod_specmodel');
			
	     if($this->prod_specmodel->check_prod_spec_update()==true)
		 {
	   $status=$this->prod_specmodel->update_plan();
		if($status==true){
		       $data['message']="Product Specialist Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}else
		{
		  $data['message']="Product Specialist With Same User Name   Already Exists" ;
		  $data['status']=0 ;

		}	
		
		echo json_encode($data);
		exit;		
	}
	
	function delete($plan_id)
	{
		
		$this->load->Model('prod_specmodel');
		$result = $this->prod_specmodel->deleteplan($plan_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	function delete_dist_prod_spec($plan_id)
	{
		
		$this->load->Model('prod_specmodel');
		$result = $this->prod_specmodel->delete_dist_prod_spec($plan_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getplaninfo($plan_id)
	{
		if(!empty($plan_id))
	  {
		$this->load->Model('prod_specmodel');
		$data['prod_spec']=$this->prod_specmodel->getplanById($plan_id);
		$data['prod_spec']=$data['prod_spec'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	function getallplaninfo()
	{
		
		$this->load->Model('prod_specmodel');
		$data['prod_spec']=$this->prod_specmodel->getallplanById();		
		echo json_encode($data);
		exit;
	  
	
	
	}
	function add_prod_spec($district_id)
	{
	 
		$this->session->set_userdata("user_prod_spec_id",$district_id);
		
	
	  
	 
	    $this->load->Model('districtmodel');
		$data['district']=$this->districtmodel->getDistrictById($district_id);
		if($data['district']!=false)
		{
		   $data['districts_name']=$data['district'][0]['districts_name'];
		  $this->session->set_userdata("user_dist_name",$data['district'][0]['districts_name']);
		
		}
		
		else
		{ 
		 $data['districts_name']='';
		
		}
	
	    $data['view_path']=$this->config->item('view_path');
	  $this->load->view('prod_spec/prod_spec_users',$data);
	
	}
	
	function getprod_spec($page)
	{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('prod_specmodel');
		$total_records = $this->prod_specmodel->getprod_spec_userCount();
		
			$status = $this->prod_specmodel->getprod_spec_users($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>District Wise Product Specialists</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >First Name</td><td >Last Name</td><td >User Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['dist_prod_spec_id'].'" class="'.$c.'" >';
			
				print '<td >'.$val['firstname'].'</td>
				<td >'.$val['lastname'].'</td>
				<td >'.$val['userid'].'</td>
					   
				
				<td  nowrap><input  class="btnsmall" title="Delete" type="button" name="Delete" value="Delete" onclick="dist_userdelete('.$val['dist_prod_spec_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'dist_prod_users');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' ><td >First Name</td><td >Last Name</td><td >User Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Product Specialists Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'dist_prod_users');
						print $pagination;	
		}
		
		
	}
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
}	