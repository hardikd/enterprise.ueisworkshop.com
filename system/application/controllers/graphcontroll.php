<?php
/**
 * teacher Controller.
 *
 */
class Graphcontroll extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false
		&& $this->is_teacher()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->no_cache();
		
	}
	
	function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
		
	function index()
	{	  
	
	$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{	 
		$this->load->Model('classroommodel');
		//$data['getAllAssData']=$this->assessscoretypemodel->getAllassignmentsScore();
		$district_id = $this->session->userdata('district_id');
		$data['view_path']=$this->config->item('view_path');	
		
		$data['grades'] = $this->classroommodel->getschoolgrades($district_id);   // grade
		$data['schools_type'] = $this->classroommodel->getschooltype($district_id);
	    $this->load->view('classroom/all',$data); 
		}

	}
	
	function getschoolbyschooltype()
	{
		$school_type_id =  $_REQUEST['id'];
		$this->load->model('Reportschooltestmodel');
		
		$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($school_type_id);
		echo '<option value="all">All</option>';
		foreach($data['schools'] as $schools)
		{
			$schoolid = $schools['school_id'];
			$school_name = $schools['school_name'];
			
			echo '<option value="'.$schoolid.'">'.$school_name.'</option>';
		}
	} 
	function showclasses()
	{	
	$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{	 	
		 $schooltypeid = $this->input->post('school_type');		
		 $school_name_id = $this->input->post('school_name_id');
		 $grade = $this->input->post('grade');
		 $school_classroom_id = $this->input->post('school_classroom_id');
		
		  
				$data['gradeid1'] = 	$grade;  
				$data['school_name_id1'] = 	$school_name_id;
				$data['schooltypeid1'] = 	$schooltypeid;
				$data['school_classroom_id'] = 	$school_classroom_id;
		
		$this->load->model('classroommodel');	
		
		//get all schools
		
		$data['student'] =$this->classroommodel->getStudent($school_name_id,$grade);
		
		// class rooms
		
		//$data['classroom'] = $this->classroommodel->getClassroom($school_name_id,$grade);
		
		$data['classroom'] = $this->classroommodel->getClassroom2($school_name_id,$grade,$school_classroom_id);
		
		
		
		$classroomid = $data['classroom'][0]['class_room_id'];		
		$data['classroomstudent'] = $this->classroommodel->getClassroomStudent($classroomid);		
		
		//get users by school id and grade id

		$data['getuseracctogradeandschool'] = $this->classroommodel->getallusers($school_name_id,$grade);
		
		
		$userid=array();
		$userarr  = array();
		$namearr  = array();
		
		
		if(!empty($data['classroomstudent']))
		{
			foreach($data['classroomstudent'] as $key => $getall)
			{
				// $userid[] = $getall['UserID'];
				foreach($getall as  $key => $value)
				{
					//$getall[$key]['school_id'];   // school id
							
					$userid = $getall[$key]['UserID'];    // user id
					
					$username1 = $getall[$key]['Name']; 
					
					$userarr[] = $this->classroommodel->studentscore($userid,$username1,$userid);			
					  
				}	
				
			}
		}
		else
		{
			$this->session->set_flashdata('item', 'No Record Found..!!');
		}
		
		
		//print_r($userarr);
		
		$data['usernumrecord'] = $userarr;	
		
		
		$district_id = $this->session->userdata('district_id');
		$data['view_path']=$this->config->item('view_path');	
		
		$data['grades'] = $this->classroommodel->getschoolgrades($district_id);   // grade
		$data['schools_type'] = $this->classroommodel->getschooltype($district_id);
		
		$this->load->view('classroom/all',$data); 


	} 
	}
	
}

?>