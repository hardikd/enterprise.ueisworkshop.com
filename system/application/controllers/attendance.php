<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ob_start();
$view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');

class Attendance extends MY_Auth {

    function __Construct() {
        parent::Controller();
       // print_r($this->session->all_userdata());exit;
        if ($this->is_teacher() == false && $this->is_observer() == false && $this->is_user() == false) {
            //These functions are available only to teachers - So redirect to the login page
           // echo 'parent';exit;
           // redirect("/");
        }
    }

    public function index() {
        redirect(site_url('attendance/assessment'));
        $data['idname'] = 'attendance';
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/index', $data);
    }

    public function assessment() {

        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/assessment', $data);
        } else if ($this->session->userdata('login_type') == 'user') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_assessment', $data);
        } else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_assessment', $data);
        }
    }

    public function daily_attendance() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'attendance';
            $this->load->Model('videooptionmodel');
                $data['video_option'] = $this->videooptionmodel->get_option();
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/daily_attendance', $data);
		}else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/daily_attendance', $data);
        }else if ($this->session->userdata('login_type') == 'user') {
				if($this->session->userdata('login_special') == 'district_management'){
					$data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_daily_attendance', $data);
				}else{
			$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
				}
        }
    }

    public function new_student_enrollment() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/new_student_enrollment', $data);
		}elseif ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/new_student_enrollment', $data);
        	
        }else if ($this->session->userdata('login_type') == 'user') {
			if($this->session->userdata('login_special') == 'district_management'){
			$data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_new_student_enrollment', $data);
			}else{
$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
			}
		}
    }
	
	  public function individual_students() {
      if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'attendance';
			
		$data['enroll_breadcrumbs'] = 'Enroll Students';
        $data['enroll_breadcrumbs_link'] = 'attendance/enroll_student';
        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->Model('dist_grademodel');

            $this->load->Model('parentmodel');
            $status = $this->parentmodel->getallstudents();

//		print_r($status);
            $data['students'] = $status;
            $data['grades'] = $this->dist_grademodel->getdist_gradesById();

			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/individual_students', $data);
		}
        }else if ($this->session->userdata('login_type') == 'user') {
			
				if($this->session->userdata('login_special') == !'district_management'){			
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
				}else{
            $data['idname'] = 'attendance';
			 $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->getallstudentsByDistrict();
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/individual_students', $data);
			}
		}
    }
	
	

    public function roster() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/roster', $data);
		}elseif ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/roster', $data);
     	
        }else if ($this->session->userdata('login_type') == 'user') {
			if($this->session->userdata('login_special') == 'district_management'){
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_roster', $data);
			}else{
			$this->session->set_flashdata ('permission','Additional Permissions Required');
            redirect(base_url().'attendance/assessment');
		}
	
        }
    }

    public function map_data() {
        $data['idname'] = 'attendance';
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/map_data', $data);
    }

    public function create_roster() {
        
        $day = 0;
        $data['idname'] = 'attendance';


if ($this->session->userdata('login_type') == 'user') {
	if($this->session->userdata('login_special') == !'district_management'){
  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
}}

        $this->load->Model('periodmodel');
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));

        $this->load->Model('dist_grademodel');
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();
        if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
			
		}
//                print_r($data['teachers']);exit;
        
        $this->load->Model('studentmodel');
        foreach ($periods as $period) {
            $all_records[$period['period_id']] = $this->studentmodel->getallstudentsdata($day, $period['period_id']);
        }
//        print_r($all_records);exit;
      $day = 0;
        $class_room_all = $this->studentmodel->getallclassroom($day);
        $data['class_room_all'] = $class_room_all;
        $data['all_students'] = $all_records;
        $data['view_path'] = $this->config->item('view_path');
        
        $this->load->view('attendance/create_roster', $data);
    }

    public function edit_roster() {
        //set day as today

        $day = 0;
        $data['idname'] = 'attendance';
if ($this->session->userdata('login_type') == 'user') {
	if($this->session->userdata('login_special') == !'district_management'){
  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
	}}
        $this->load->Model('periodmodel');
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
//        print_r($periods);exit;

        $this->load->Model('dist_grademodel');
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();

if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
			
		}

        $this->load->Model('studentmodel');
        foreach ($periods as $period) {
            //print_r($period['period_id']);exit;
            $all_records[$period['period_id']] = $this->studentmodel->getallstudentsdata($day, $period['period_id']);
        }
//            print_r($total_records);exit;
//                $status = $this->studentmodel->getstudentsdata($day,$period,$page, $per_page);
//            print_r($periods);exit;
        $day = 0;
        $class_room_all = $this->studentmodel->getallclassroom($day);
//            print_r($class_room_all);exit;
        $data['class_room_all'] = $class_room_all;
        $data['all_students'] = $all_records;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/create_roster', $data);
    }

    function getstudentsdata1($day, $period,$teacher='') {
        error_reporting(0);
        if($teacher!=''){
            $data['teacher_id']=$teacher;
        } else {
            $data['teacher_id']=$this->session->userdata('teacher_id');
        }
        $this->load->Model('studentmodel');
        $this->load->Model('periodmodel');
        $this->load->Model('dist_grademodel');
        $status = $this->studentmodel->getallstudentsdata($day, $period,$teacher);
//        echo $this->db->last_query();
        $class_room_all = $this->studentmodel->getallclassroom($day);
        $period_data = $this->periodmodel->getperiodbyperiodid($period);
        $class_room=$this->studentmodel->getclassroom($day,$period,$teacher);
       // echo $this->db->last_query();
        $data['period_data'] = $period_data;
        $data['status'] = $status;
        $data['class_room_all'] = $class_room_all;
        $data['day'] = $day;
        $data['period'] = $period;
        $data['grades'] = $this->dist_grademodel->getdist_gradesById();
        $data['class_room'] = $class_room;

//        print_r($data['class_room']);
        $data['view_path'] = $this->config->item('view_path');
//		if($data['status'][0]['class_name']!='')
        
	        $this->load->view('attendance/create_roster_ajax', $data);
//		else 
//		exit;
    }




    function enroll_student() {
        $data['idname'] = 'attendance';
		
		if ($this->session->userdata('login_type') == 'user') {
			if($this->session->userdata('login_special') == !'district_management'){
  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
}}
		
        $data['enroll_breadcrumbs'] = 'Enroll Students';
        $data['enroll_breadcrumbs_link'] = 'attendance/enroll_student';
        if ($this->is_observer() == true || $this->is_teacher() == true || $this->is_user() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->Model('dist_grademodel');

		
            $this->load->Model('parentmodel');
            $status = $this->parentmodel->getallstudents();


//		print_r($status);
            $data['students'] = $status;
            $data['grades'] = $this->dist_grademodel->getdist_gradesById();

            $this->load->view('parent/students_all', $data);
        }
    }

    function modify_enrollment() {
        $data['idname'] = 'attendance';
		if ($this->session->userdata('login_type') == 'user') {
			if($this->session->userdata('login_special') == !'district_management'){
			 
  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
}}
		
        $data['enroll_breadcrumbs'] = 'Modify Enrollment';
        $data['enroll_breadcrumbs_link'] = 'attendance/modify_enrollment';
        if ($this->is_observer() == true || $this->is_teacher() == true || $this->is_user() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->Model('dist_grademodel');



            $this->load->Model('parentmodel');
            $status = $this->parentmodel->getallstudents();


//		print_r($status);
            $data['students'] = $status;
            $data['grades'] = $this->dist_grademodel->getdist_gradesById();
            $this->load->view('parent/students_all', $data);
        }
    }

    function studentsview($parent_id) {
        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->Model('parentmodel');
            $data['student'] = $this->parentmodel->getstudentsbyparentsId($parent_id);

            $this->load->view('parent/student_view', $data);
        }
    }

    function getstudents() {

        $this->load->Model('utilmodel');
        $per_page = $this->utilmodel->get_recperpage();

        $this->load->Model('parentmodel');
        $total_records = $this->parentmodel->getstudentCount();

        $status = $this->parentmodel->getallstudents();

//		print_r($status);
        $data['students'] = $status;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/modify_enrollment_ajax', $data);
//		if($status!=FALSE){
//			
//			
//			print "<table class='table table-striped table-bordered' id='editable-sample' >
//			   <tr>
//			<td>First Name</td>
//			<td class='sorting'>Last Name</td>
//			<td class='sorting'>Grade</td>
//			<td class='sorting'>Student Number</td>
//			<td class='sorting'>Actions</td>
//			</tr>";
//		
//		
//			$i=1;
//			
//			foreach($status as $val){
//					if($i%2==0)
//					{
//					 $c='tcrow2';
//					}
//					else
//					{
//					 $c='tcrow1';
//					
//					}
//					
//				print '<tr id="'.$val['student_id'].'" class="'.$c.'" >';
//			
//				print '<td>'.$val['firstname'].'</td>
//					  <td>'.$val['lastname'].'</td>
//					  <td>'.$val['grade'].'</td>
//					 <td>'.$val['student_number'].'</td>					 					  
//					  ';
//					  	
//					   
//				
//				print '<td nowrap>
//<a href="#myModal-edit-student" role="button" class="btn btn-primary" onClick="studentedit('.$val['student_id'].')" data-toggle="modal"><i class="icon-pencil"></i></a>
//<a href="#myModal-delete-student" role="button" class="btn btn-danger" data-toggle="modal" onClick="studentdelete('.$val['student_id'].')"> <i class="icon-trash"></i></a>
//				</td>	
//				</tr>';
//				$i++;
//				}
//				print '</table>';
//               
//				
//                        $pagination=$this->do_pagination($total_records,$per_page,$page,'student');
//						print $pagination;	
//						
//                        
//		}else{
//			
//			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>First Name</td><td>Last Name</td><td>Grade</td><td>Student Number</td><td>Actions</td></tr>
//			<tr><td valign='top' colspan='10'>No Students Found.</td></tr></table>
//			";
//			
//			$pagination=$this->do_pagination($total_records,$per_page,$page,'student');
//						print $pagination;	
//		}
    }

    function getstudentsmap($page, $fname) {
        if ($fname == 'empty') {
            $fname = '';
        }
        $this->load->Model('utilmodel');
        $per_page = $this->utilmodel->get_recperpage();

        $this->load->Model('parentmodel');
        $total_records = $this->parentmodel->getstudentmapCount($fname);

        $status = $this->parentmodel->getstudentsmap($page, $per_page, $fname);



        if ($status != FALSE) {
            $chk = "'chk_'";

            print '<table class="table table-striped table-bordered" id="editable-sample" >
			<tr>
			<td><input type="checkbox" name="chk_grp" class="group-checkable" data-set="#editable-sample input.checkboxes" onclick= "toggleCheckboxes(' . $chk . ',this.checked)" /></td>
			<td>First Name</td>
			<td class="sorting">Last Name</td>
			<td class="no-sorting">Grade</td>
			<td class="no-sorting">Student Number</td>
			</tr>';



            $i = 1;

            foreach ($status as $val) {
                if ($i % 2 == 0) {
                    $c = 'tcrow2';
                } else {
                    $c = 'tcrow1';
                }

                print '<tr id="' . $val['student_id'] . '" class="' . $c . '" >';

                print '
				<td><input type="checkbox" id="chk_' . $val['student_id'] . '" name="point_id[]" value="' . $val['student_id'] . '"></td>
				<td>' . $val['firstname'] . '</td>
					  <td class="hidden-phone">' . $val['lastname'] . '</td>
					  <td class="hidden-phone">' . $val['grade'] . '</td>
					 <td class="center hidden-phone">' . $val['student_number'] . '</td>					 					  
					  ';

                print '</tr>';
                $i++;
            }
            print '</table>';


            $pagination = $this->do_pagination($total_records, $per_page, $page, 'student');
            print $pagination;
        } else {

            print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>First Name</td><td>Last Name</td><td>Grade</td><td>Student Number</td></tr>
			<tr><td valign='top' colspan='10'>No Students Found.</td></tr></table>
			";
            $pagination = $this->do_pagination($total_records, $per_page, $page, 'student');
            print $pagination;
        }
    }

    function getparents($page) {

        $this->load->Model('utilmodel');
        $per_page = $this->utilmodel->get_recperpage();

        $this->load->Model('parentmodel');
        $total_records = $this->parentmodel->getparentCount();

        $status = $this->parentmodel->getparents($page, $per_page);



        if ($status != FALSE) {


            print "<div class='htitle'>Parent & Student Data Input Screen</div>
		    <table class='table table-striped table-bordered' id='editable-sample'>
                            <thead>
                            <tr>
                               <td>First Name</td>
                                <td class='sorting'>Last Name</td>
                                <td class='no-sorting'>Email</td>
                                <td class='no-sorting'>Lang</td>
                                <td class='no-sorting'>Actions</td>
                            </tr>
                            </thead>";



            $i = 1;
            $LANGUAGES = $this->config->item('LANGUAGES');
            foreach ($status as $val) {
                if ($i % 2 == 0) {
                    $c = 'tcrow2';
                } else {
                    $c = 'tcrow1';
                }

                print '<tr id="' . $val['parents_id'] . '" class="' . $c . '" >';

                print '<td class="hidden-phone"><a href="parents/studentsview/' . $val['parents_id'] . '">' . $val['firstname'] . '</a></td>
				<td class="hidden-phone">' . $val['lastname'] . '</td>
					  <td class="hidden-phone">' . $val['email'] . '</td>
					  <td class="hidden-phone">' . substr($LANGUAGES[$val['language']], 0, 3) . '</td>
					  ';





                print '<td nowrap>

		<a href="#myModal-edit-parent" role="button" class="btn btn-primary" onClick="parentedit(' . $val['parents_id'] . ')" data-toggle="modal"><i class="icon-pencil"></i></a>
		<a href="#myModal-delete-student" role="button" class="btn btn-danger" onClick="parentdelete(' . $val['parents_id'] . ')" data-toggle="modal"><i class="icon-trash"></i></a>

	</td>		
				</tr>';
                $i++;
            }
            print '</table>';


            $pagination = $this->do_pagination($total_records, $per_page, $page, 'parents');
            print $pagination;
        } else {

            print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' align='center'><td>Parent A Name</td><td>Parent A Email</td><td>Parent B Name</td><td>Parent B Email</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No parents Found.</td></tr></table>
			";

            $pagination = $this->do_pagination($total_records, $per_page, $page, 'parents');
            print $pagination;
        }
    }

    function student_mapping() {
        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $data['view_path'] = $this->config->item('view_path');

            $this->load->Model('parentmodel');
            $data['parents'] = $this->parentmodel->get_parents_all();
            $this->load->view('parent/student_mapping', $data);
        }
    }

    function getallparents() {
        parse_str($_SERVER['QUERY_STRING'], $_GET);
        if ($this->is_observer() == true || $this->is_teacher() == true) {

            $q = strtolower($_GET["q"]);

            if (!$q)
                return;

            $this->load->Model('parentmodel');
            $parents = $this->parentmodel->get_parents_all_auto($q);
            if ($parents != false) {
                foreach ($parents as $parentval) {
                    $pname = $parentval['firstname'] . ' ' . $parentval['lastname'];
                    $pid = $parentval['parents_id'];
                    echo "$pname|$pid\n";
                }
            } else {

                echo 'No Parents Found.|';
            }
        }
    }

    function mapstudents() {

        $data['status'] = 0;
        $pdata = $this->input->post('pdata');

        if (!empty($pdata)) {

            if (isset($pdata['point_id']) && isset($pdata['end'])) {
                $this->load->Model('parentmodel');
                $status = $this->parentmodel->mapstudents();
                $data['status'] = $status;
            }
        }
        echo json_encode($data);
        exit;
    }

    function getparentinfo($parent_id) {
        if (!empty($parent_id)) {
            $this->load->Model('parentmodel');

            $data['parent'] = $this->parentmodel->getparentById($parent_id);
            $data['parent'] = $data['parent'][0];
            echo json_encode($data);
            exit;
        }
    }

    function getstudentinfo($student_id) {

        if (!empty($student_id)) {
            $this->load->Model('parentmodel');

            $data['student'] = $this->parentmodel->getstudentById($student_id, 'student');
            $data['student'] = $data['student'][0];
            //print_r($data);exit;
            echo json_encode($data);
            exit;
        }
    }

    function add_parent() {

        if ($this->input->post('email') != '' && $this->input->post('emailsub') != '') {
            if ($this->input->post('email') == $this->input->post('emailsub')) {
                $data['message'] = 'Parent A Email And Parent B Email Should not be equal.';
                $data['status'] = 0;
                echo json_encode($data);
                exit;
            }
        }


        $this->load->Model('parentmodel');
        $pstatus = $this->parentmodel->check_parent_exists();
        if ($pstatus == 'success') {
            $usernamestatus = $this->parentmodel->check_parent_username();
            if ($usernamestatus == 'success') {
                $status = $this->parentmodel->add_parent();
                if ($status != 0) {
                    $data['message'] = "parent added Sucessfully";
                    $data['status'] = 1;
                } else {
                    $data['message'] = "Contact Technical Support Update Failed";
                    $data['status'] = 0;
                }
            } else {
                $data['message'] = "Username Already Exists Please Try Different Username.";
                $data['status'] = 0;
            }
        } else {
            $data['message'] = $pstatus;
            $data['status'] = 0;
        }

        echo json_encode($data);
        exit;
    }

    function update_parent() {
        if ($this->input->post('email') != '' && $this->input->post('emailsub') != '') {
            if ($this->input->post('email') == $this->input->post('emailsub')) {
                $data['message'] = 'Parent A Email And Parent B Email Should not be equal.';
                $data['status'] = 0;
                echo json_encode($data);
                exit;
            }
        }


        $this->load->Model('parentmodel');
        $pstatus = $this->parentmodel->check_parent_update();
        if ($pstatus == 'success') {
            $usernamestatus = $this->parentmodel->check_parent_username_update();
            if ($usernamestatus == 'success') {
                $status = $this->parentmodel->update_parent();

                if ($status == true) {
                    $data['message'] = "Parent Updated Sucessfully";
                    $data['status'] = 1;
                } else {
                    $data['message'] = "Contact Technical Support Update Failed";
                    $data['status'] = 0;
                }
            } else {
                $data['message'] = "Username Already Exists Please Try Different Username.";
                $data['status'] = 0;
            }
        } else {
            $data['message'] = $pstatus;
            $data['status'] = 0;
        }

        echo json_encode($data);
        exit;
    }

    function delete($parent_id) {

        $this->load->Model('parentmodel');
        $result = $this->parentmodel->deleteparent($parent_id);
        if ($result == true) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
            $date['error_msg'] = $result;
        }
        echo json_encode($data);
        exit;
    }

    function add_student() {

        $this->load->Model('parentmodel');

        $pstatus = $this->parentmodel->check_student_exists();

        $email = $this->input->post('emailaddress');

        if (isset($email) && $email != "") {
            $pstatus = $this->parentmodel->check_student_email_exists();
        }


        if ($pstatus == 'success') {
            $status = $this->parentmodel->add_student();
            $data['message'] = "Student Added Sucessfully";
            $data['status'] = 1;
        } else {

            $data['message'] = 'Student Already Exists';
            $data['status'] = 0;
        }

        echo json_encode($data);
        exit;
    }

    function update_student() {

        $this->load->Model('parentmodel');
        $pstatus = $this->parentmodel->check_student_update();
        if ($pstatus == 'success') {

            $status = $this->parentmodel->update_student();
            if ($status == true) {
                $data['message'] = "Student Updated Sucessfully";
                $data['status'] = 1;
            } else {
                $data['message'] = "Contact Technical Support Update Failed";
                $data['status'] = 0;
            }
        } else {
            $data['message'] = $pstatus;
            $data['status'] = 0;
        }

        echo json_encode($data);
        exit;
    }

    function delete_student($student_id) {

        $this->load->Model('parentmodel');
        $result = $this->parentmodel->deletestudent($student_id);
        if ($result == true) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
            $date['error_msg'] = $result;
        }
        echo json_encode($data);
        exit;
    }

    function bulkupload() {
        if ($this->session->userdata('NC') == 0) {
            redirect("index");
        }
        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('parent/bulkupload', $data);
        }
    }

    function parent_student_bulkupload() {
        if ($this->session->userdata('NC') == 0) {
            redirect("index");
        }
        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('parent/parent_student_bulkupload', $data);
        }
    }

    function student_upload() {
        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('parent/student_upload', $data);
        }
    }

    function student_only_upload() {
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('parent/student_only_upload', $data);
    }

    function sendfile() {

        $pdata['status'] = 0;
        if ($_FILES['upload']['size'] > 0) {

            $this->load->Model('parentmodel');

            $filename = explode('.', $_FILES['upload']['name']);
            $target_path = WORKSHOP_FILES . "parentuploads/";
            $target_path = $target_path . basename($_FILES['upload']['name']);
            if ($filename[1] == 'xlsx') {
                $pdata['status'] = 2;
                $pdata['msg'] = 'Please Upload Excel 97-2003 ';
                echo json_encode($pdata);
                exit;
            } else if ($filename[1] == 'xls') {
                $this->load->library('Spreadsheet_Excel_Reader');


                if (move_uploaded_file($_FILES['upload']['tmp_name'], $target_path)) {





                    //$this->spreadsheet_excel_reader->setOutputEncoding('CP1251'); // Set output Encoding.
                    $this->spreadsheet_excel_reader->read($target_path); // relative path to .xls that was uploaded earlier

                    $rows = $this->spreadsheet_excel_reader->sheets[0]['cells'];
                    $cell_count = count($this->spreadsheet_excel_reader->sheets[0]['cells']);

                    $row_count = $this->spreadsheet_excel_reader->sheets[0]['numCols'];
                    $data['rows'] = $row_count;
                    $data['cols'] = $cell_count;

                    //for ($j = 1; $j <= $row_count; $j++) {

                    for ($i = 2; $i <= $cell_count; $i++) {
                        for ($j = 1; $j <= $row_count; $j++) {
                            if (isset($rows[$i][$j])) {
                                $text = $rows[$i][$j];
                                $data[$i][$j] = trim($text);
                                //$data[$i][$j]=trim($rows[$i][$j]);
                            }
                        }
                    }

                    //echo $b="asdlsad";
                    //}
                    //print_r($data);
                    //exit;
                    //echo json_encode($data);
                    // exit;
                }
            } else if ($filename[1] == 'csv') {

                move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);

                $row = 1;
                $file = fopen($target_path, "r");
                while (($fdata = fgetcsv($file)) !== FALSE) {
                    $num = count($fdata);
                    if (!empty($fdata))
                        $sdata[] = $fdata;


                    $row++;
                    /* for ($c=0; $c < $num; $c++) {
                      //echo $fdata[$c] . "<br>";
                      $mobile.=trim($fdata[$c]); */
                }
                $data['cols'] = count($sdata);

                //echo "<pre>";
                //print_r($sdata);
                //exit;
                $m = 1;
                for ($i = 1; $i < count($sdata); $i++) {

                    $pdata = $sdata[$i];
                    $data['rows'] = count($pdata);
                    $n = 1;
                    for ($j = 0; $j < count($pdata); $j++) {
                        $text = $pdata[$j];
                        $data[$m][$n] = trim($text);

                        $n++;
                    }
                    $m++;
                }
                //print_r($data); 				
                //exit;

                fclose($file);


                //echo json_encode($data);
                // exit;
            }
            //echo '<pre>';
            //print_r($data);
            //exit;
            //$i=1;
            foreach ($data as $key => $val) {
                if ($key > 0) {
                    if (isset($val[1]) && isset($val[2]) && isset($val[5]) && isset($val[6])) {
                        if (!empty($val[1]) && !empty($val[2])) {
                            if ($val[4] == 'en' || $val[4] == 'zh-CN' || $val[4] == 'es') {
                                
                            } else {
                                $val[4] = 'en';
                            }

                            if (!empty($val[7]) && !empty($val[8])) {
                                if ($val[3] != '' && $val[9] != '') {
                                    if ($val[3] != $val[9]) {

                                        if ($val[10] == 'en' || $val[10] == 'zh-CN' || $val[10] == 'es') {
                                            
                                        } else {
                                            $val[10] = 'en';
                                        }
                                        $pstatus = $this->parentmodel->check_parent_exists($val[3], $val[9]);
                                        if ($pstatus == 'success') {
                                            $usernamestatus = $this->parentmodel->check_parent_username($val[5]);
                                            if ($usernamestatus == 'success') {
                                                $this->parentmodel->add_parent($val[1], $val[2], $val[3], $val[4], $val[5], $val[6], $val[7], $val[8], $val[9], $val[10]);
                                            }
                                        }
                                    }
                                } else {
                                    if ($val[10] == 'en' || $val[10] == 'zh-CN' || $val[10] == 'es') {
                                        
                                    } else {
                                        $val[10] = 'en';
                                    }
                                    $pstatus = $this->parentmodel->check_parent_exists($val[3], $val[9]);
                                    if ($pstatus == 'success') {
                                        $usernamestatus = $this->parentmodel->check_parent_username($val[5]);
                                        if ($usernamestatus == 'success') {
                                            $this->parentmodel->add_parent($val[1], $val[2], $val[3], $val[4], $val[5], $val[6], $val[7], $val[8], $val[9], $val[10]);
                                        }
                                    }
                                }
                            } else {
                                $pstatus = $this->parentmodel->check_parent_exists($val[3], '');

                                if ($pstatus == 'success') {
                                    $usernamestatus = $this->parentmodel->check_parent_username($val[5]);
                                    if ($usernamestatus == 'success') {
                                        $this->parentmodel->add_parent($val[1], $val[2], $val[3], $val[4], $val[5], $val[6], '', '', '', '');
                                    }
                                }
                            }
                        }
                    }
                }

                // $i++;
            }
            $pdata['status'] = 1;
        }
        echo json_encode($pdata);
        exit;
    }

    function parents_students_sendfile() {

        $pdata['status'] = 0;
        if ($_FILES['upload']['size'] > 0) {

            $this->load->Model('parentmodel');
            $this->load->Model('dist_grademodel');

            $filename = explode('.', $_FILES['upload']['name']);
            $target_path = WORKSHOP_FILES . "parentuploads/";
            $target_path = $target_path . basename($_FILES['upload']['name']);
            if ($filename[1] == 'xlsx') {
                $pdata['status'] = 2;
                $pdata['msg'] = 'Please Upload Excel 97-2003 ';
                echo json_encode($pdata);
                exit;
            } else if ($filename[1] == 'xls') {
                $this->load->library('Spreadsheet_Excel_Reader');


                if (move_uploaded_file($_FILES['upload']['tmp_name'], $target_path)) {


                    //$this->spreadsheet_excel_reader->setOutputEncoding('CP1251'); // Set output Encoding.
                    $this->spreadsheet_excel_reader->read($target_path); // relative path to .xls that was uploaded earlier

                    $rows = $this->spreadsheet_excel_reader->sheets[0]['cells'];
                    $cell_count = count($this->spreadsheet_excel_reader->sheets[0]['cells']);

                    $row_count = $this->spreadsheet_excel_reader->sheets[0]['numCols'];
                    $data['rows'] = $row_count;
                    $data['cols'] = $cell_count;

                    //for ($j = 1; $j <= $row_count; $j++) {

                    for ($i = 2; $i <= $cell_count; $i++) {
                        for ($j = 1; $j <= $row_count; $j++) {
                            if (isset($rows[$i][$j])) {
                                $text = $rows[$i][$j];
                                $data[$i][$j] = trim($text);
                                //$data[$i][$j]=trim($rows[$i][$j]);
                            }
                        }
                    }

                    //echo $b="asdlsad";
                    //}
                    //print_r($data);
                    //exit;
                    //echo json_encode($data);
                    // exit;
                }
            } else if ($filename[1] == 'csv') {

                move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);

                $row = 1;
                $file = fopen($target_path, "r");
                while (($fdata = fgetcsv($file)) !== FALSE) {
                    $num = count($fdata);
                    if (!empty($fdata))
                        $sdata[] = $fdata;


                    $row++;
                    /* for ($c=0; $c < $num; $c++) {
                      //echo $fdata[$c] . "<br>";
                      $mobile.=trim($fdata[$c]); */
                }
                $data['cols'] = count($sdata);

                //echo "<pre>";
                //print_r($sdata);
                //exit;
                $m = 1;
                for ($i = 1; $i < count($sdata); $i++) {

                    $pdata = $sdata[$i];
                    $data['rows'] = count($pdata);
                    $n = 1;
                    for ($j = 0; $j < count($pdata); $j++) {
                        $text = $pdata[$j];
                        $data[$m][$n] = trim($text);

                        $n++;
                    }
                    $m++;
                }
                //print_r($data); 				
                //exit;

                fclose($file);


                //echo json_encode($data);
                // exit;
            }
            //echo '<pre>';
            //print_r($data);
            //exit;
            //$i=1;
            foreach ($data as $key => $val) {
                if ($key > 0) {
                    if (isset($val[11]) && isset($val[12]) && isset($val[13]) && isset($val[14])) {
                        if (!empty($val[11]) && !empty($val[12]) && !empty($val[13]) && !empty($val[14])) {

                            $grade_exist = 1;

                            if (!empty($val[13])) {
                                $grade_exist = $this->dist_grademodel->check_dist_grade_exists_name($val[13]);
                            }


                            if ($grade_exist == 0) {
                                $rdata['status'] = 2;
                                $re = $key + 1;
                                $rdata['msg'] = "The Grade Name In Row '$re' Not Exists . Please Create Grade In district Panel. ";
                                echo json_encode($rdata);
                                exit;
                            } else {
                                $data[$key][13] = $grade_exist;
                            }
                        }
                    }
                }

                // $i++;
            }


            foreach ($data as $key => $val) {
                $parent_id = 0;
                if ($key > 0) {
                    if (isset($val[1]) && isset($val[2]) && isset($val[5]) && isset($val[6])) {
                        if (!empty($val[1]) && !empty($val[2])) {
                            if ($val[4] == 'en' || $val[4] == 'zh-CN' || $val[4] == 'es') {
                                
                            } else {
                                $val[4] = 'en';
                            }

                            if (!empty($val[7]) && !empty($val[8]) && !empty($val[9])) {
                                if ($val[3] != '' && $val[9] != '') {
                                    if ($val[3] != $val[9]) {
                                        if ($val[10] == 'en' || $val[10] == 'zh-CN' || $val[10] == 'es') {
                                            
                                        } else {
                                            $val[10] = 'en';
                                        }
                                        $pstatus = $this->parentmodel->check_parent_exists($val[3], $val[9]);
                                        if ($pstatus == 'success') {
                                            $usernamestatus = $this->parentmodel->check_parent_username($val[5]);
                                            if ($usernamestatus == 'success') {
                                                $parent_id = $this->parentmodel->add_parent($val[1], $val[2], $val[3], $val[4], $val[5], $val[6], $val[7], $val[8], $val[9], $val[10]);
                                            }
                                        }
                                    }
                                } else {

                                    if ($val[10] == 'en' || $val[10] == 'zh-CN' || $val[10] == 'es') {
                                        
                                    } else {
                                        $val[10] = 'en';
                                    }
                                    $pstatus = $this->parentmodel->check_parent_exists($val[3], $val[9]);
                                    if ($pstatus == 'success') {
                                        $usernamestatus = $this->parentmodel->check_parent_username($val[5]);
                                        if ($usernamestatus == 'success') {
                                            $parent_id = $this->parentmodel->add_parent($val[1], $val[2], $val[3], $val[4], $val[5], $val[6], $val[7], $val[8], $val[9], $val[10]);
                                        }
                                    }
                                }
                            } else {
                                $pstatus = $this->parentmodel->check_parent_exists($val[3], '');

                                if ($pstatus == 'success') {
                                    $usernamestatus = $this->parentmodel->check_parent_username($val[5]);
                                    if ($usernamestatus == 'success') {
                                        $parent_id = $this->parentmodel->add_parent($val[1], $val[2], $val[3], $val[4], $val[5], $val[6], '', '', '', '');
                                    }
                                }
                            }
                        }
                    }

                    if ($parent_id != 0) {

                        if (isset($val[11]) && isset($val[12]) && isset($val[13]) && isset($val[14])) {
                            if (!empty($val[11]) && !empty($val[12]) && !empty($val[13]) && !empty($val[14])) {

                                $insert = 1;

                                if (!empty($val[14])) {
                                    if (!$this->alpha_numeric($val[14]) || strlen($val[14]) > 15) {
                                        $insert = 0;
                                    }
                                } else {
                                    $val[14] = '';
                                }


                                if ($insert == 1) {
                                    $usernamestatus = $this->parentmodel->check_student_exists($val[11]);

                                    if ($usernamestatus == 'success') {
                                        $this->parentmodel->add_student($val[11], $val[12], $val[13], $val[14], $parent_id);
                                    }
                                }
                            }
                        }
                    }
                }

                // $i++;
            }
            $pdata['status'] = 1;
        }
        echo json_encode($pdata);
        exit;
    }

    function student_sendfile() {

        if ($this->is_observer() == true || $this->is_teacher() == true) {
            $pdata['status'] = 0;
            if ($_FILES['upload']['size'] > 0) {

                $this->load->Model('parentmodel');
                $this->load->Model('dist_grademodel');

                $filename = explode('.', $_FILES['upload']['name']);

                $target_path = WORKSHOP_FILES . "studentuploads/";
                $target_path = $target_path . basename($_FILES['upload']['name']);
                if ($filename[1] == 'xlsx') {
                    $pdata['status'] = 2;
                    $pdata['msg'] = 'Please Upload Excel 97-2003 ';
                    echo json_encode($pdata);
                    exit;
                } else if ($filename[1] == 'xls') {
                    $this->load->library('Spreadsheet_Excel_Reader');


                    if (move_uploaded_file($_FILES['upload']['tmp_name'], $target_path)) {

                        //$this->spreadsheet_excel_reader->setOutputEncoding('CP1251'); // Set output Encoding.
                        $this->spreadsheet_excel_reader->read($target_path); // relative path to .xls that was uploaded earlier

                        $rows = $this->spreadsheet_excel_reader->sheets[0]['cells'];
                        $cell_count = count($this->spreadsheet_excel_reader->sheets[0]['cells']);

                        $row_count = $this->spreadsheet_excel_reader->sheets[0]['numCols'];
                        $data['rows'] = $row_count;
                        $data['cols'] = $cell_count;

                        //for ($j = 1; $j <= $row_count; $j++) {

                        for ($i = 2; $i <= $cell_count; $i++) {
                            for ($j = 1; $j <= $row_count; $j++) {
                                if (isset($rows[$i][$j])) {
                                    $text = $rows[$i][$j];
                                    $data[$i][$j] = trim($text);
                                    //$data[$i][$j]=trim($rows[$i][$j]);
                                }
                            }
                        }

                        //echo $b="asdlsad";
                        //}

                        foreach ($data as $key => $val) {
                            if ($key > 0) {
                                if (isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4]) && isset($val[5])) {
                                    if (!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4]) && !empty($val[5])) {

                                        $grade_exist = 1;

                                        if (!empty($val[3])) {
                                            $grade_exist = $this->dist_grademodel->check_dist_grade_exists_name($val[3]);
                                        }


                                        if ($grade_exist == 0) {
                                            $rdata['status'] = 2;
                                            $re = $key + 1;
                                            $rdata['msg'] = "The Grade Name In Row '$re' Not Exists . Please Create Grade In district Panel. ";
                                            echo json_encode($rdata);
                                            exit;
                                        } else {
                                            $data[$key][3] = $grade_exist;
                                        }
                                    }
                                }
                            }

                            // $i++;
                        }

                        foreach ($data as $key => $val) {

                            if ($key > 0) {
                                if (isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4]) && isset($val[5])) {
                                    if (!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4]) && !empty($val[5])) {

                                        $insert = 1;

                                        if (!empty($val[4])) {
                                            if (!$this->alpha_numeric($val[4]) || strlen($val[4]) > 15) {
                                                $insert = 0;
                                            }
                                        } else {
                                            $val[4] = '';
                                        }

                                        if ($insert == 1) {
                                            $usernamestatus = $this->parentmodel->check_student_exists($val[1]);

                                            if ($usernamestatus == 'success') {

                                                //$this->parentmodel->add_student($val[1],$val[2],$val[3],$val[4],$val[5]);
                                                $this->parentmodel->add_studentfromcsv($val[1], $val[2], $val[3], $val[4], $val[5]);
                                            }
                                        }
                                    }
                                }
                            }

                            // $i++;
                        }
                        $pdata['status'] = 1;

                        //echo '<pre>';
                        //print_r($data);
                        //exit;
                        //echo json_encode($data);
                        //exit;
                    }
                } else if ($filename[1] == 'csv') {

                    move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);

                    $row = 1;
                    $file = fopen($target_path, "r");
                    while (($fdata = fgetcsv($file)) !== FALSE) {
                        $num = count($fdata);
                        if (!empty($fdata))
                            $sdata[] = $fdata;


                        $row++;
                        /* for ($c=0; $c < $num; $c++) {
                          //echo $fdata[$c] . "<br>";
                          $mobile.=trim($fdata[$c]); */
                    }
                    $data['cols'] = count($sdata);

                    //echo "<pre>";
                    //print_r($sdata);
                    //exit;
                    $m = 1;
                    for ($i = 1; $i < count($sdata); $i++) {

                        $pdata = $sdata[$i];
                        $data['rows'] = count($pdata);
                        $n = 1;
                        for ($j = 0; $j < count($pdata); $j++) {
                            $text = $pdata[$j];
                            $data[$m][$n] = trim($text);

                            $n++;
                        }
                        $m++;
                    }
                    //print_r($data); 				
                    //exit;

                    fclose($file);


                    //echo json_encode($data);
                    // exit;
                }
                //echo '<pre>';
                //print_r($data);
                //exit;
                //$i=1;

                foreach ($data as $key => $val) {
                    if ($key > 0) {
                        if (isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4]) && isset($val[5])) {
                            if (!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4]) && !empty($val[5])) {

                                $grade_exist = 1;

                                if (!empty($val[3])) {
                                    $grade_exist = $this->dist_grademodel->check_dist_grade_exists_name($val[3]);
                                }


                                if ($grade_exist == 0) {
                                    $rdata['status'] = 2;
                                    $re = $key + 1;
                                    $rdata['msg'] = "The Grade Name In Row '$re' Not Exists . Please Create Grade In district Panel. ";
                                    echo json_encode($rdata);
                                    exit;
                                } else {
                                    $data[$key][3] = $grade_exist;
                                }
                            }
                        }
                    }

                    // $i++;
                }
                //exit;
                foreach ($data as $key => $val) {
                    if ($key > 0) {
                        if (isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4]) && isset($val[5])) {
                            if (!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4]) && !empty($val[5])) {

                                $insert = 1;

                                if (!empty($val[4])) {
                                    if (!$this->alpha_numeric($val[4]) || strlen($val[4]) > 15) {
                                        $insert = 0;
                                    }
                                } else {
                                    $val[4] = '';
                                }

                                if ($insert == 1) {
                                    $usernamestatus = $this->parentmodel->check_student_exists($val[1]);
                                    if ($usernamestatus == 'success') {
                                        //$this->parentmodel->add_student($val[1],$val[2],$val[3],$val[4],$val[5]);
                                        $this->parentmodel->add_studentfromcsv($val[1], $val[2], $val[3], $val[4], $val[5]);
                                    }
                                }
                            }
                        }
                    }

                    // $i++;
                }
                $pdata['status'] = 1;
            }
            echo json_encode($pdata);
            exit;
        }
    }

    function student_only_sendfile() {

        $pdata['status'] = 0;
        if ($_FILES['upload']['size'] > 0) {

            $this->load->Model('parentmodel');

            $filename = explode('.', $_FILES['upload']['name']);
            $target_path = WORKSHOP_FILES . "studentuploads/";
            $target_path = $target_path . basename($_FILES['upload']['name']);
            if ($filename[1] == 'xlsx') {
                $pdata['status'] = 2;
                $pdata['msg'] = 'Please Upload Excel 97-2003 ';
                echo json_encode($pdata);
                exit;
            } else if ($filename[1] == 'xls') {
                $this->load->library('Spreadsheet_Excel_Reader');


                if (move_uploaded_file($_FILES['upload']['tmp_name'], $target_path)) {





                    //$this->spreadsheet_excel_reader->setOutputEncoding('CP1251'); // Set output Encoding.
                    $this->spreadsheet_excel_reader->read($target_path); // relative path to .xls that was uploaded earlier

                    $rows = $this->spreadsheet_excel_reader->sheets[0]['cells'];
                    $cell_count = count($this->spreadsheet_excel_reader->sheets[0]['cells']);

                    $row_count = $this->spreadsheet_excel_reader->sheets[0]['numCols'];
                    $data['rows'] = $row_count;
                    $data['cols'] = $cell_count;

                    //for ($j = 1; $j <= $row_count; $j++) {

                    for ($i = 2; $i <= $cell_count; $i++) {
                        for ($j = 1; $j <= $row_count; $j++) {
                            if (isset($rows[$i][$j])) {
                                $text = $rows[$i][$j];
                                $data[$i][$j] = trim($text);
                                //$data[$i][$j]=trim($rows[$i][$j]);
                            }
                        }
                    }

                    //echo $b="asdlsad";
                    //}
                    //print_r($data);
                    //exit;
                    //echo json_encode($data);
                    // exit;
                }
            } else if ($filename[1] == 'csv') {

                move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);

                $row = 1;
                $file = fopen($target_path, "r");
                while (($fdata = fgetcsv($file)) !== FALSE) {
                    $num = count($fdata);
                    if (!empty($fdata))
                        $sdata[] = $fdata;


                    $row++;
                    /* for ($c=0; $c < $num; $c++) {
                      //echo $fdata[$c] . "<br>";
                      $mobile.=trim($fdata[$c]); */
                }
                $data['cols'] = count($sdata);

                //echo "<pre>";
                //print_r($sdata);
                //exit;
                $m = 1;
                for ($i = 1; $i < count($sdata); $i++) {

                    $pdata = $sdata[$i];
                    $data['rows'] = count($pdata);
                    $n = 1;
                    for ($j = 0; $j < count($pdata); $j++) {
                        $text = $pdata[$j];
                        $data[$m][$n] = trim($text);

                        $n++;
                    }
                    $m++;
                }
                //print_r($data); 				
                //exit;

                fclose($file);


                //echo json_encode($data);
                // exit;
            }
            //echo '<pre>';
            //print_r($data);
            //exit;
            //$i=1;
            foreach ($data as $key => $val) {
                if ($key > 0) {
                    if (isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4])) {
                        if (!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4])) {

                            $insert = 1;

                            if (!empty($val[4])) {
                                if (!$this->alpha_numeric($val[4]) || strlen($val[4]) > 15) {
                                    $insert = 0;
                                }
                            } else {
                                $val[4] = '';
                            }

                            if ($insert == 1) {
                                $usernamestatus = $this->parentmodel->check_student_exists($val[1]);
                                if ($usernamestatus == 'success') {
                                    $usernameonlystatus = $this->parentmodel->check_student_only_exists($val[1]);
                                    if ($usernameonlystatus == 'success') {
                                        $this->parentmodel->add_only_student($val[1], $val[2], $val[3], $val[4]);
                                    }
                                }
                            }
                        }
                    }
                }

                // $i++;
            }
            $pdata['status'] = 1;
        }
        echo json_encode($pdata);
        exit;
    }

    function do_pagination($count, $per_page, $cur_page, $paginationdetails) {
        $string = '';


        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;


        $no_of_paginations = ceil($count / $per_page);
        /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }
        /* ----------------------------------------------------------------------------------------------------------- */
        $string.= "<br />";
        $string.= "<div id='paginationall' class='$paginationdetails'><ul>";

        // FOR ENABLING THE FIRST BUTTON
        if ($first_btn && $cur_page > 1) {
            $string.= "<li p='1' class='active'>First</li>";
        } else if ($first_btn) {
            $string.= "<li p='1' class='inactive'>First</li>";
        }

        // FOR ENABLING THE PREVIOUS BUTTON
        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $string.= "<li p='$pre' class='active'>Previous</li>";
        } else if ($previous_btn) {
            $string.= "<li class='inactive'>Previous</li>";
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i)
                $string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
            else
                $string.= "<li p='$i' class='active'>{$i}</li>";
        }

        // TO ENABLE THE NEXT BUTTON
        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $string.= "<li p='$nex' class='active'>Next</li>";
        } else if ($next_btn) {
            $string.= "<li class='inactive'>Next</li>";
        }

        // TO ENABLE THE END BUTTON
        if ($last_btn && $cur_page < $no_of_paginations) {
            $string.="<li p='$no_of_paginations' class='active'>Last</li>";
        } else if ($last_btn) {
            $string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
        }
        //$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
        $goto = '';
        $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
        $string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination


        return $string;
    }

    function add_homework() {
        $client = new apiClient();
        $client->setApplicationName('Google Translate PHP Starter Application');
        $client->setDeveloperKey('AIzaSyBdiO6T4yyCkmTBxGDtf_m7oeL8WrFXSHc');
        $service = new apiTranslateService($client);

        $data = array();
        $this->load->Model('parentmodel');
        if ($this->session->userdata('NC') == 0) {
            $data['homemessage'] = 'Notification To Parents is Deactivated For This School.';
            $data['status'] = 0;
        } else if ($this->input->post('homework') == '') {
            $data['homemessage'] = 'Please Enter Text To Send HomeWork';
            $data['status'] = 0;
        } else if ($this->input->post('homegrade_id') == '') {
            $data['homemessage'] = 'Please Select Grade';
            $data['status'] = 0;
        } else {

            $pstatus = $this->parentmodel->checkhomework();
            if ($pstatus == false) {
                $data['homemessage'] = 'Already Sent HomeWork For This Date';
                $data['status'] = 0;
            } else {
                $status = $this->parentmodel->add_homework();
                if ($status == 0) {
                    $data['homemessage'] = 'Failed Please Try Again ';
                    $data['status'] = 0;
                } else {
                    $parents = $this->parentmodel->get_parents_all();
                    $homedata = $this->parentmodel->get_homework();
                    if ($parents != false && $homedata != false) {
                        $chin = array();
                        $span = array();
                        foreach ($parents as $parent) {
                            $verifycode = $this->parentmodel->add_questions($parent['parents_id'], $status);

                            if ($this->valid_email($parent['email'])) {
                                $email = $parent['email'];
                                $name = $parent['firstname'] . ' ' . $parent['lastname'];
                                $lan = $parent['language'];

                                $parents_id = $parent['parents_id'];
                                $date = $homedata[0]['entered'];
                                $homework = $homedata[0]['homework'];
                                $SITE_URLM = SITEURLM;


                                /*  sending mail to observer */

                                //Additional headers
                                /* if($teacheremail!='')
                                  {
                                  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
                                  $headers .= "Reply-To: TOR Teacher <info@ueisworkshop.com>".PHP_EOL;
                                  }
                                  else
                                  { */

                                $headers = "From: Workshop  <info@ueisworkshop.com>" . PHP_EOL;

                                /* } */
                                // To send HTML mail, the Content-type header must be set
                                $headers .= 'MIME-Version: 1.0' . PHP_EOL;
                                $headers .= 'Content-Type: text/html; charset=iso-8859-1' . PHP_EOL;
                                $headers .= 'X-Mailer: PHP/' . phpversion() . PHP_EOL;
                                if ($lan == 'zh-CN' || $lan == 'es') {
                                    $subject = "Workshop-HomeWork For $date ";
                                    if ($lan == 'zh-CN') {
                                        if (empty($chin)) {
                                            $translations = $service->translations->listTranslations($homework, $lan);
                                            $translationssub = $service->translations->listTranslations($subject, $lan);
                                            $translationshometext = $service->translations->listTranslations('HomeWork', $lan);

                                            $translationsplease = $service->translations->listTranslations("Please click on the following link to <b>Answer</b> questions", $lan);
                                            $translationsclick = $service->translations->listTranslations("If clicking the above link does not work for you please copy and paste it in your browser address bar and then hit enter key", $lan);
                                            $translationspower = $service->translations->listTranslations("Powered By", $lan);
                                            $translationstor = $service->translations->listTranslations("Workshop", $lan);

                                            $homework = $translations['translations'][0]['translatedText'];
                                            $subject = $translationssub['translations'][0]['translatedText'];
                                            $homeworktext = $translationshometext['translations'][0]['translatedText'];
                                            $please = $translationsplease['translations'][0]['translatedText'];
                                            $click = $translationsclick['translations'][0]['translatedText'];
                                            $powered = $translationspower['translations'][0]['translatedText'];
                                            $tor = $translationstor['translations'][0]['translatedText'];

                                            $chin['homework'] = $homework;
                                            $chin['subject'] = $subject;
                                            $chin['homeworktext'] = $homeworktext;
                                            $chin['please'] = $please;
                                            $chin['click'] = $click;
                                            $chin['powered'] = $powered;
                                            $chin['tor'] = $tor;
                                        } else {
                                            $homework = $chin['homework'];
                                            $subject = $chin['subject'];
                                            $homeworktext = $chin['homeworktext'];
                                            $please = $chin['please'];
                                            $click = $chin['click'];
                                            $powered = $chin['powered'];
                                            $tor = $chin['tor'];
                                        }
                                    }
                                    if ($lan == 'es') {
                                        if (empty($span)) {
                                            $translations = $service->translations->listTranslations($homework, $lan);
                                            $translationssub = $service->translations->listTranslations($subject, $lan);
                                            $translationshometext = $service->translations->listTranslations('HomeWork', $lan);

                                            $translationsplease = $service->translations->listTranslations("Please click on the following link to <b>Answer</b> questions", $lan);
                                            $translationsclick = $service->translations->listTranslations("If clicking the above link does not work for you please copy and paste it in your browser address bar and then hit enter key", $lan);
                                            $translationspower = $service->translations->listTranslations("Powered By", $lan);
                                            $translationstor = $service->translations->listTranslations("Workshop", $lan);

                                            $homework = $translations['translations'][0]['translatedText'];
                                            $subject = $translationssub['translations'][0]['translatedText'];
                                            $homeworktext = $translationshometext['translations'][0]['translatedText'];
                                            $please = $translationsplease['translations'][0]['translatedText'];
                                            $click = $translationsclick['translations'][0]['translatedText'];
                                            $powered = $translationspower['translations'][0]['translatedText'];
                                            $tor = $translationstor['translations'][0]['translatedText'];

                                            $span['homework'] = $homework;
                                            $span['subject'] = $subject;
                                            $span['homeworktext'] = $homeworktext;
                                            $span['please'] = $please;
                                            $span['click'] = $click;
                                            $span['powered'] = $powered;
                                            $span['tor'] = $tor;
                                        } else {
                                            $homework = $span['homework'];
                                            $subject = $span['subject'];
                                            $homeworktext = $span['homeworktext'];
                                            $please = $span['please'];
                                            $click = $span['click'];
                                            $powered = $span['powered'];
                                            $tor = $span['tor'];
                                        }
                                    }



                                    $translationsgreetings = $service->translations->listTranslations("Greetings $name", $lan);




                                    $greetings = $translationsgreetings['translations'][0]['translatedText'];

                                    $message = "<html><head><title>$subject</title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> $homeworktext </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> $greetings,</td></tr><tr height='10%'><td colspan='2'></td></tr><tr><td> $homework </td></tr><tr><td> $please: </td></tr><tr><td><a href='" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "'>" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "</a></td></tr><tr><td> ($click).</td></tr><tr><td colspan='2'> $powered, </td></tr><tr><td colspan='2'> $tor </td></tr></table></body></html>";
                                } else {
                                    $subject = "Workshop-HomeWork For $date ";
                                    $message = "<html><head><title>Workshop-HomeWork  For $date </title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> HomeWork </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> Greetings $name,</td></tr><tr height='10%'><td colspan='2'></td></tr><tr><td> $homework </td></tr><tr><td> Please click on the following link to <b>Answer</b> questions : </td></tr><tr><td><a href='" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "'>" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "</a></td></tr><tr><td> (If clicking the above link does not work for you please copy and paste it in your browser address bar and then hit enter key).</td></tr><tr> <td colspan='2'> Powered By, </td></tr><tr><td colspan='2'> Workshop </td></tr></table></body></html>";
                                }

                                mail($email, $subject, $message, $headers);
                            }


                            if ($this->valid_email($parent['emailsub'])) {
                                $email = $parent['emailsub'];
                                $name = $parent['firstnamesub'] . ' ' . $parent['lastnamesub'];
                                $languagesub = $parent['languagesub'];
                                $parents_id = $parent['parents_id'];
                                $date = $homedata[0]['entered'];
                                $homework = $homedata[0]['homework'];
                                $SITE_URLM = SITEURLM;


                                /*  sending mail to observer */

                                //Additional headers
                                /* if($teacheremail!='')
                                  {
                                  $headers = "From: Workshop  <info@ueisworkshop.com>".PHP_EOL;
                                  $headers .= "Reply-To: TOR Teacher <info@ueisworkshop.com>".PHP_EOL;
                                  }
                                  else
                                  { */

                                $headers = "From: Workshop  <info@ueisworkshop.com>" . PHP_EOL;

                                /* } */
                                // To send HTML mail, the Content-type header must be set
                                $headers .= 'MIME-Version: 1.0' . PHP_EOL;
                                $headers .= 'Content-Type: text/html; charset=iso-8859-1' . PHP_EOL;
                                $headers .= 'X-Mailer: PHP/' . phpversion() . PHP_EOL;
                                if ($languagesub == 'zh-CN' || $languagesub == 'es') {
                                    $subject = "Workshop-HomeWork For $date ";

                                    if ($languagesub == 'zh-CN') {

                                        if (empty($chin)) {
                                            $translations = $service->translations->listTranslations($homework, $languagesub);
                                            $translationssub = $service->translations->listTranslations($subject, $languagesub);
                                            $translationshometext = $service->translations->listTranslations('HomeWork', $languagesub);

                                            $translationsplease = $service->translations->listTranslations("Please click on the following link to <b>Answer</b> questions", $languagesub);
                                            $translationsclick = $service->translations->listTranslations("If clicking the above link does not work for you please copy and paste it in your browser address bar and then hit enter key", $languagesub);
                                            $translationspower = $service->translations->listTranslations("Powered By", $languagesub);
                                            $translationstor = $service->translations->listTranslations("Workshop", $languagesub);

                                            $homework = $translations['translations'][0]['translatedText'];
                                            $subject = $translationssub['translations'][0]['translatedText'];
                                            $homeworktext = $translationshometext['translations'][0]['translatedText'];
                                            $please = $translationsplease['translations'][0]['translatedText'];
                                            $click = $translationsclick['translations'][0]['translatedText'];
                                            $powered = $translationspower['translations'][0]['translatedText'];
                                            $tor = $translationstor['translations'][0]['translatedText'];

                                            $chin['homework'] = $homework;
                                            $chin['subject'] = $subject;
                                            $chin['homeworktext'] = $homeworktext;
                                            $chin['please'] = $please;
                                            $chin['click'] = $click;
                                            $chin['powered'] = $powered;
                                            $chin['tor'] = $tor;
                                        } else {
                                            $homework = $chin['homework'];
                                            $subject = $chin['subject'];
                                            $homeworktext = $chin['homeworktext'];
                                            $please = $chin['please'];
                                            $click = $chin['click'];
                                            $powered = $chin['powered'];
                                            $tor = $chin['tor'];
                                        }
                                    }
                                    if ($languagesub == 'es') {

                                        if (empty($span)) {
                                            $translations = $service->translations->listTranslations($homework, $languagesub);
                                            $translationssub = $service->translations->listTranslations($subject, $languagesub);
                                            $translationshometext = $service->translations->listTranslations('HomeWork', $languagesub);

                                            $translationsplease = $service->translations->listTranslations("Please click on the following link to <b>Answer</b> questions", $languagesub);
                                            $translationsclick = $service->translations->listTranslations("If clicking the above link does not work for you please copy and paste it in your browser address bar and then hit enter key", $languagesub);
                                            $translationspower = $service->translations->listTranslations("Powered By", $languagesub);
                                            $translationstor = $service->translations->listTranslations("Workshop", $languagesub);

                                            $homework = $translations['translations'][0]['translatedText'];
                                            $subject = $translationssub['translations'][0]['translatedText'];
                                            $homeworktext = $translationshometext['translations'][0]['translatedText'];
                                            $please = $translationsplease['translations'][0]['translatedText'];
                                            $click = $translationsclick['translations'][0]['translatedText'];
                                            $powered = $translationspower['translations'][0]['translatedText'];
                                            $tor = $translationstor['translations'][0]['translatedText'];

                                            $span['homework'] = $homework;
                                            $span['subject'] = $subject;
                                            $span['homeworktext'] = $homeworktext;
                                            $span['please'] = $please;
                                            $span['click'] = $click;
                                            $span['powered'] = $powered;
                                            $span['tor'] = $tor;
                                        } else {
                                            $homework = $span['homework'];
                                            $subject = $span['subject'];
                                            $homeworktext = $span['homeworktext'];
                                            $please = $span['please'];
                                            $click = $span['click'];
                                            $powered = $span['powered'];
                                            $tor = $span['tor'];
                                        }
                                    }

                                    $translationsgreetings = $service->translations->listTranslations("Greetings $name", $languagesub);




                                    $greetings = $translationsgreetings['translations'][0]['translatedText'];
                                    $message = "<html><head><title>$subject</title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> $homeworktext </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> $greetings,</td></tr><tr height='10%'><td colspan='2'></td></tr><tr><td> $homework </td></tr><tr><td> $please: </td></tr><tr><td><a href='" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "'>" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "</a></td></tr><tr><td> ($click).</td></tr><tr><td colspan='2'> $powered, </td></tr><tr><td colspan='2'> $tor </td></tr></table></body></html>";
                                } else {
                                    $subject = "Workshop-HomeWork For $date ";
                                    $message = "<html><head><title>Workshop-HomeWork  For $date </title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> HomeWork </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> Greetings $name,</td></tr><tr height='10%'><td colspan='2'></td></tr><tr><td> $homework </td></tr><tr><td> Please click on the following link to <b>Answer</b> questions : </td></tr><tr><td><a href='" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "'>" . $SITE_URLM . "index.php/comments/homework/" . $verifycode . "</a></td></tr><tr><td> (If clicking the above link does not work for you please copy and paste it in your browser address bar and then hit enter key).</td></tr><tr> <td colspan='2'> Powered By, </td></tr><tr><td colspan='2'> Workshop </td></tr></table></body></html>";
                                }
                                mail($email, $subject, $message, $headers);
                            }
                        }
                    }
                    $data['status'] = 1;
                }
            }
        }

        echo json_encode($data);
        exit;
    }

    function partnership() {
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        if ($this->input->post('selectdate')) {
            $data['selectdate'] = $this->input->post('selectdate');
        } else {
            $data['selectdate'] = date('m-d-Y');
        }
        $this->load->Model('parentmodel');

        $datevar = explode('-', $data['selectdate']);
        $sel = $datevar[1] . '-' . $datevar[0] . '-' . $datevar[2];
        $data['dates'] = $this->week_from_monday($sel);
        $fromdate = $data['dates'][0]['date'];
        $todate = $data['dates'][6]['date'];
        $fromdate1 = explode('-', $fromdate);
        $data['fromdate'] = $fromdate1[1] . '-' . $fromdate1[2] . '-' . $fromdate1[0];
        $todate1 = explode('-', $todate);
        $data['todate'] = $todate1[1] . '-' . $todate1[2] . '-' . $todate1[0];
        foreach ($data['dates'] as $key => $val) {
            $data['dates'][$key]['week'] = $val['week'];
            $cdate = $val['date'];
            $cdate1 = explode('-', $cdate);
            $data['dates'][$key]['date'] = $cdate1[1] . '-' . $cdate1[2] . '-' . $cdate1[0];
        }

        $data['view_path'] = $this->config->item('view_path');
        $data['answers'] = $this->parentmodel->get_all_questions($fromdate, $todate);
        $data['questions'] = $this->parentmodel->get_questions();

        $this->load->view('parent/partnership', $data);
    }

    function week_from_monday($date) {
        // Assuming $date is in format DD-MM-YYYY
        list($day, $month, $year) = explode("-", $date);

        // Get the weekday of the given date
        $wkday = date('l', mktime('0', '0', '0', $month, $day, $year));

        switch ($wkday) {
            case 'Monday': $numDaysToMon = 0;
                break;
            case 'Tuesday': $numDaysToMon = 1;
                break;
            case 'Wednesday': $numDaysToMon = 2;
                break;
            case 'Thursday': $numDaysToMon = 3;
                break;
            case 'Friday': $numDaysToMon = 4;
                break;
            case 'Saturday': $numDaysToMon = 5;
                break;
            case 'Sunday': $numDaysToMon = 6;
                break;
        }

        // Timestamp of the monday for that week
        $monday = mktime('0', '0', '0', $month, $day - $numDaysToMon, $year);

        $seconds_in_a_day = 86400;

        // Get date for 7 days from Monday (inclusive)

        for ($i = 0; $i < 7; $i++) {
            $dates[$i]['date'] = date('Y-m-d', $monday + ($seconds_in_a_day * $i));
            if ($i == 0) {
                $dates[$i]['week'] = 'Monday';
            }
            if ($i == 1) {
                $dates[$i]['week'] = 'Tuesday';
            }
            if ($i == 2) {
                $dates[$i]['week'] = 'Wednesday';
            }
            if ($i == 3) {
                $dates[$i]['week'] = 'Thursday';
            }
            if ($i == 4) {
                $dates[$i]['week'] = 'Friday';
            }
            if ($i == 5) {
                $dates[$i]['week'] = 'Saturday';
            }
            if ($i == 6) {
                $dates[$i]['week'] = 'Sunday';
            }
        }

        return $dates;
    }

    function gethomework($date) {

        $this->load->Model('parentmodel');
        $data['homework'] = $this->parentmodel->get_homework($date);
        if ($data['homework'] != false) {
            $data['homework'] = $data['homework'][0]['homework'];
        } else {
            $data['homework'] = 'No Response';
        }
        echo json_encode($data);
        exit;
    }

    function alpha_numeric($str) {
        return (!preg_match("/^([a-z0-9])+$/i", $str)) ? FALSE : TRUE;
    }

    function valid_email($str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }

    function add_parent_info() {
        $data['idname'] = 'attendance';
		
		   if($this->session->userdata("login_type")=='user')
	  		{
				if($this->session->userdata('login_special') == !'district_management'){	
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
	  		}}
        $data['view_path'] = $this->config->item('view_path');
        if ($this->session->userdata('NC') == 0) {
            redirect("index");
        }
        if ($this->is_observer() == true || $this->is_teacher() == true || $this->is_user() == true) {
            $data['view_path'] = $this->config->item('view_path');
            $data['LANGUAGES'] = $this->config->item('LANGUAGES');
            $this->load->Model('parentmodel');

            $total_records = $this->parentmodel->getparentCount();


            $data['parents'] = $this->parentmodel->getallparents();
			
			
            $data['view_path'] = $this->config->item('view_path');
            $data['LANGUAGES'] = $this->config->item('LANGUAGES');
//         print_r($data['parents']);exit;
            $this->load->view('attendance/add_parent_info', $data);
        }
    }

    public function take_roll() {
        $data['idname'] = 'attendance';

if ($this->session->userdata('login_type') == 'user') {
  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
}


        $this->load->Model('periodmodel');
 
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
 //         print_r($periods);exit;
        if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
			
		}
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/take_roll', $data);
    }

    public function edit_roll() {
        $data['idname'] = 'attendance';

if ($this->session->userdata('login_type') == 'user') {
  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
}
        $this->load->Model('periodmodel');
        $periods = $data['periods'] = $this->periodmodel->getperiodbyschoolid($this->session->userdata('school_summ_id'));
        if($this->session->userdata('login_type')=='observer')
		{
			$this->load->Model('teachermodel');
			$data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
			
		}
//            print_r($periods);exit;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/edit_roll', $data);
    }

    function getclassdetails($day, $period,$teacher='') {
        $this->load->Model('studentmodel');
        $this->load->Model('periodmodel');
//        echo $this->input->post('teacher_id');exit;
        if($teacher=='' && $this->input->post('teacher_id')){
            $teacher = $this->input->post('teacher_id');
        }
        $status = $this->studentmodel->getallstudentsdata($day, $period,$teacher);
           // echo $this->db->last_query();exit;
        $class_room_all = $this->studentmodel->getallclassroom($day);
        $period_data = $this->periodmodel->getperiodbyperiodid($period);
        $data['period_data'] = $period_data;
        $data['status'] = $status;
        $data['class_room_all'] = $class_room_all;
        $data['day'] = $day;
        $data['period'] = $period;
        $data['view_path'] = $this->config->item('view_path');
        echo json_encode($data);
        //$this->load->view('attendance/create_roster_ajax',$data);           
    }

    function getstudentdetails($period) {
        $this->load->Model('studentmodel');
        $this->load->Model('periodmodel');
        
        $status = $this->studentmodel->getStudentsByRange($period);
		
        // $class_room_all=$this->studentmodel->getallclassroom();
        $period_data = $this->periodmodel->getperiodbyperiodid($period);
        //$students = $this->studentmodel->getallstudentsdata(false,$period);
        $data['period_data'] = $period_data;
        $data['status'] = $status;
        //$data['class_room_all'] = $class_room_all;
        // $data['day'] = $day;
        $data['period'] = $period;
        //$data['students'] = $students;
        // print_r($data);exit;
        $data['view_path'] = $this->config->item('view_path');
        echo json_encode($data);
        //$this->load->view('attendance/create_roster_ajax',$data);           
    }
    function getstudentdetails_parent($period,$student) {
        $this->load->Model('studentmodel');
        $this->load->Model('periodmodel');
        $teacher = $this->studentmodel->GetTeacherByStudentId($student,$period);
        $data['teacher_id'] = $teacher[0]['teacher_id'];
        $status = $this->studentmodel->getStudentsByRange($period,$data['teacher_id']);
        
        // $class_room_all=$this->studentmodel->getallclassroom();
        $period_data = $this->periodmodel->getperiodbyperiodid($period);
        //$students = $this->studentmodel->getallstudentsdata(false,$period);
        $data['period_data'] = $period_data;
        $data['status'] = $status;
        //$data['class_room_all'] = $class_room_all;
        // $data['day'] = $day;
        $data['period'] = $period;
        //$data['students'] = $students;
        // print_r($data);exit;
        $data['view_path'] = $this->config->item('view_path');
        echo json_encode($data);
        //$this->load->view('attendance/create_roster_ajax',$data);           
    }

    function getstudentsdetails($day, $period) {
        $this->load->Model('studentmodel');
        $status = $this->studentmodel->getallstudentsdata($day, $period);
        $data['status'] = $status;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/roster_student', $data);
    }

    function getstudentsrolldetails($day, $period,$teacher='') {
        $this->load->Model('studentmodel');
        $status = $this->studentmodel->getallstudentsdata($day, $period,$teacher);
       // echo $this->db->last_query();exit;
        $data['status'] = $status;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/roster_student_edit', $data);
    }

    function updateroll() {

        $student_id = $this->input->post('studentId');
        $teacher_student_id = $this->input->post('teacherStudentId');
        $datearr = explode("-", $this->input->post('date'));
        $date = $datearr[2] . '-' . $datearr[0] . '-' . $datearr[1];
        $period = $this->input->post('period');
        $rollstatus = $this->input->post('status');
        if($this->session->userdata('teacher_id')){
            $teacher_id = $this->session->userdata('teacher_id');
        } else { 
            $teacher_id = $this->input->post('teacher_id');
        }
        $this->load->Model('studentmodel');
        $status = $this->studentmodel->updatestudentroll($student_id, $teacher_student_id, $date, $period, $rollstatus, $teacher_id);
        $data['status'] = $status;
        $data['view_path'] = $this->config->item('view_path');
        //$this->load->view('attendance/roster_student',$data); 
    }

    function getroll() {

        $student_id = $this->input->post('studentId');
        $teacher_student_id = $this->input->post('teacherStudentId');
        $datearr = explode("-", $this->input->post('date'));
        $date = $datearr[2] . '-' . $datearr[0] . '-' . $datearr[1];
        $period = $this->input->post('period');
        $teacher_id = $this->input->post('teacher_id');
        $this->load->Model('studentmodel');
        $status = $this->studentmodel->getstundentroll($student_id, $teacher_student_id, $date, $period,$teacher_id);
        //echo $this->db->last_query();
        $data['status'] = $status;
        echo $status;
//            $data['view_path']=$this->config->item('view_path');
        //$this->load->view('attendance/roster_student',$data); 
    }

    public function retrieve_roll() {
        $data['idname'] = 'attendance';
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('attendance/retrieve_roll', $data);
    }

    public function map_parent() {
        $data['idname'] = 'attendance';
        $data['view_path'] = $this->config->item('view_path');
		
		if ($this->session->userdata('login_type') == 'user') {
			 if($this->session->userdata('login_special') == !'district_management'){	
		  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
			 }else{

            $data['idname'] = 'attendance';
			 $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->getallstudentsByDistrict();
			//print_r($data['students']);exit;
		}}else{
		
		 $this->load->Model('parentmodel');
        $status = $this->parentmodel->getallstudentsmap();
		  $data['students'] = $status;
		}
		
		
      
        $this->load->view('attendance/map_parent', $data);
    }

    //for observer
    public function set_up() {
        if ($this->session->userdata('login_type') == 'teacher') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/set_up', $data);
        }else if ($this->session->userdata('login_type') == 'user') {
					if($this->session->userdata('login_special') == 'district_management'){
					$data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_set_up', $data);
				}else{
			  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/dist_set_up', $data);
		}}else if ($this->session->userdata('login_type') == 'observer') {
            $data['idname'] = 'attendance';
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/set_up', $data);
        }
    }

    public function createattendancegraph() 
{
      //  error_reporting(0);

	    include_once "highcharts/Highchart.php";
        $chart = new Highchart();
        $chart->chart->renderTo = "container1";
        if ($this->input->post('view') == 'day') {
            $strDateFrom = $this->input->post('start');
            $strDateTo = $this->input->post('end');
            $dateArr = $this->getDates($strDateFrom, $strDateTo);
            $chart->chart->type = "column";

           foreach ($dateArr as $value) {
                $showdatecat[] = date('m-d', strtotime($value));
            }
            $chart->xAxis->categories = $showdatecat;

            $this->load->model('studentmodel');
            $studentsroll = $this->studentmodel->getstudentroll();
		
            $chart->tooltip->formatter = new HighchartJsExpr("function() {
                    return '' + this.series.name +': '+ this.y +'';}");
            $chart->credits->enabled = false;
            if ($this->input->post('student_id') == 'all') {
                $period = $this->input->post('period_id');
                $this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange($period);
//                print_r($students);
                foreach($students as $student){
                    
                   $student_details = $this->studentmodel->getStudentDetails($student['student_id']);
//                   print_r($student_details);exit;
                }
                
                
            } else {
                $student_details = $this->studentmodel->getStudentDetails($studentsroll[0]->student_id);
                
                $student_name = $student_details[0]->firstname . ' ' . $student_details[0]->lastname;
                $chart->title->text = "<h3 style='text-align:center;'>Attendance report of <b>$student_name</b></h3>";
                $presentArr = array();
                foreach ($dateArr as $date) {
                    $flag = false;
                    foreach ($studentsroll as $studentroll) {
                        if ($studentroll->date == $date) {
                            switch (strtolower($studentroll->status)) {
                                case 'present':
                                    $presentArr[] = 1;
                                    $absentArr[] = 0;
                                    $absentwreasonArr[] = 0;
                                    $tardyArr[] = 0;
                                    $tardywreasonArr[] = 0;
                                    $flag = true;
                                    break;
                                case 'absent':
                                    $presentArr[] = 0;
                                    $absentArr[] = 1;
                                    $absentwreasonArr[] = 0;
                                    $tardyArr[] = 0;
                                    $tardywreasonArr[] = 0;
                                    $flag = true;
                                    break;
                                case 'absent w/ excuse':
                                    $presentArr[] = 0;
                                    $absentArr[] = 0;
                                    $absentwreasonArr[] = 1;
                                    $tardyArr[] = 0;
                                    $tardywreasonArr[] = 0;
                                    $flag = true;
                                    break;
                                case 'tardy':
                                    $presentArr[] = 0;
                                    $absentArr[] = 0;
                                    $absentwreasonArr[] = 0;
                                    $tardyArr[] = 1;
                                    $tardywreasonArr[] = 0;
                                    $flag = true;
                                    break;
                                case 'tardy w/ excuse':
                                    $presentArr[] = 0;
                                    $absentArr[] = 0;
                                    $absentwreasonArr[] = 0;
                                    $tardyArr[] = 0;
                                    $tardywreasonArr[] = 1;
                                    $flag = true;
                                    break;
                            }
                        }
                    }
                    if (!$flag) {
                        $presentArr[] = 0;
                        $absentArr[] = 0;
                        $absentwreasonArr[] = 0;
                        $tardyArr[] = 0;
                        $tardywreasonArr[] = 0;
                    }
                }
            }
            $chart->series[] = array('name' => 'Present',
                'data' => $presentArr, 'color' => '#8bbc21');
            $chart->series[] = array('name' => 'Absent',
                'data' => $absentArr, 'color' => '#FF0000');
            $chart->series[] = array('name' => 'Absent w/ excuse',
                'data' => $absentwreasonArr, 'color' => '#A52A2A');
            $chart->series[] = array('name' => 'Tardy',
                'data' => $tardyArr, 'color' => '#2f7ed8');
            $chart->series[] = array('name' => 'Tardy w/ excuse',
                'data' => $tardywreasonArr, 'color' => '#0d233a');
        }

        if ($this->input->post('view') == 'week') {

            $strDateFrom = $this->input->post('start');
            $strDateTo = $this->input->post('end');
//                $weeksnumber = $this->datediff('ww',$strDateFrom, $strDateTo,false);
            $weeksnumber = $this->getWeeks($strDateFrom, $strDateTo);
//               print_r($weeksnumber);exit;

            $chart->chart->type = "column";


            $chart->plotOptions->series->stacking = "normal";

            $this->load->model('studentmodel');
            $studentsroll = $this->studentmodel->getstudentroll();
            $chart->tooltip->formatter = new HighchartJsExpr("function() {
                    return '' + this.series.name +': '+ this.y +'';}");
            $chart->credits->enabled = false;
            if ($this->input->post('student') == 'All') {
                
            } else {
                $student_details = $this->studentmodel->getStudentDetails($studentsroll[0]->student_id);
                $student_name = $student_details[0]->firstname . ' ' . $student_details[0]->lastname;
                $chart->title->text = "<h3 style='text-align:center;'>Weekly Attendance report of <b>$student_name</b></h3>";
                $presentArr = array();
                $absentArr = array();
                foreach ($weeksnumber as $weeknumber) {
                    $flag = false;
                    $wk_ts = strtotime('+' . $weeknumber['week'] . ' weeks', strtotime($weeknumber['year'] . '0101'));
                    for ($first = 1; $first <= 7; $first++) {
                        $day_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);
                        if ($first == 1) {
                            $categoryname = date('m-d', ($day_ts));
                        } else if ($first == 7) {
                            $categoryname .= ' To ' . date('m-d', ($day_ts));
                        }

//                            $presentArr[$wk_ts] = 0;
//                            $absentArr[$wk_ts] = 0;
//                            $absentwreasonArr[$wk_ts] = 0;
//                            $tardyArr[$wk_ts] = 0;
//                            $tardywreasonArr[$wk_ts] = 0;

                        foreach ($studentsroll as $studentroll) {
//                                    echo $studentroll->date.'=='.$day_ts;
                            if ($studentroll->date == date('Y-m-d', $day_ts)) {
                                switch (strtolower($studentroll->status)) {
                                    case 'present':
                                        $presentArr[$wk_ts] += 1;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'absent':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 1;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'absent w/ excuse':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 1;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'tardy':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 1;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'tardy w/ excuse':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 1;
                                        $flag = true;
                                        break;
                                }
                            }
                        }

                        if (!$flag) {
                            $presentArr[$wk_ts] += 0;
                            $absentArr[$wk_ts] += 0;
                            $absentwreasonArr[$wk_ts] += 0;
                            $tardyArr[$wk_ts] += 0;
                            $tardywreasonArr[$wk_ts] += 0;
                        }
                    }
                    $catArr[] = $categoryname;
                }
            }
//                print_r($presentArr);exit;
            foreach ($presentArr as $presentArr1) {
                $present[] = $presentArr1;
            }
            foreach ($absentArr as $absentArr1) {
                $absent[] = $absentArr1;
            }
            foreach ($absentwreasonArr as $absentwreasonArr1) {
                $absentwreason[] = $absentwreasonArr1;
            }
            foreach ($tardyArr as $tardyArr1) {
                $tardy[] = $tardyArr1;
            }
            foreach ($tardywreasonArr as $tardywreasonArr1) {
                $tardywreason[] = $tardywreasonArr1;
            }
            $chart->xAxis->categories = $catArr;
            $chart->series[] = array('name' => 'Present',
                'data' => $present, 'color' => '#8bbc21');
            $chart->series[] = array('name' => 'Absent',
                'data' => $absent, 'color' => '#FF0000');
            $chart->series[] = array('name' => 'Absent w/ excuse',
                'data' => $absentwreason, 'color' => '#A52A2A');
            $chart->series[] = array('name' => 'Tardy',
                'data' => $tardy, 'color' => '#2f7ed8');
            $chart->series[] = array('name' => 'Tardy w/ excuse',
                'data' => $tardywreason, 'color' => '#0d233a');
        }

        if ($this->input->post('view') == 'month') {

            $strDateFrom = $this->input->post('start');
            $strDateTo = $this->input->post('end');
//                $weeksnumber = $this->datediff('ww',$strDateFrom, $strDateTo,false);
            $monthsnumber = $this->getMonths($strDateFrom, $strDateTo);
//               print_r($monthsnumber);exit;

            $chart->chart->type = "column";


            $chart->plotOptions->series->stacking = "normal";

            $this->load->model('studentmodel');
            $studentsroll = $this->studentmodel->getstudentroll();
            
//            print_r($studentsroll);exit;
            $chart->tooltip->formatter = new HighchartJsExpr("function() {
                    return '' + this.series.name +': '+ this.y +'';}");
            $chart->credits->enabled = false;
            if ($this->input->post('student') == 'All') {
                
            } else {
                
                $student_details = $this->studentmodel->getStudentDetails($studentsroll[0]->student_id);
                
                $student_name = $student_details[0]->firstname . ' ' . $student_details[0]->lastname;
                $chart->title->text = "<h3 style='text-align:center;'>Monthly Attendance report of <b>$student_name</b></h3>";
                $presentArr = array();
                $absentArr = array();
                foreach ($monthsnumber as $monthnumber) {
                    
                    $flag = false;
//                    $wk_ts = strtotime('+' . $weeknumber['month'] . ' months', strtotime($weeknumber['year'] . '0101'));
                    $totalDaysofMonth = cal_days_in_month(CAL_GREGORIAN,$monthnumber['month'],$monthnumber['year']);
                    $firstdate = date('Y-m-d', mktime(0, 0, 0, $monthnumber['month'], 1, $monthnumber['year']));
                    for ($first = 1; $first <= $totalDaysofMonth; $first++) {
                        $day_ts = strtotime('+'.($first-1).' day', strtotime($firstdate));
                        if ($first == 1) {
                            $categoryname = date('m-d', strtotime($monthnumber['year'].'-'.$monthnumber['month'].'-01'));
                            
                        } else if ($first == $totalDaysofMonth) {
                            $categoryname .= ' To '.date('m-t', strtotime($monthnumber['year'].'-'.$monthnumber['month'].'-01'));
                        }
//                        echo date('m-d-Y',$day_ts);exit;
                       // echo $categoryname;exit;

//                            $presentArr[$wk_ts] = 0;
//                            $absentArr[$wk_ts] = 0;
//                            $absentwreasonArr[$wk_ts] = 0;
//                            $tardyArr[$wk_ts] = 0;
//                            $tardywreasonArr[$wk_ts] = 0;

                        foreach ($studentsroll as $studentroll) {
//                                    echo $studentroll->date.'=='.$day_ts;
                            if ($studentroll->date == date('Y-m-d', $day_ts)) {
                                switch (strtolower($studentroll->status)) {
                                    case 'present':
                                        $presentArr[$wk_ts] += 1;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'absent':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 1;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'absent w/ excuse':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 1;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'tardy':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 1;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'tardy w/ excuse':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 1;
                                        $flag = true;
                                        break;
                                }
                            }
                        }

                        if (!$flag) {
                            $presentArr[$wk_ts] += 0;
                            $absentArr[$wk_ts] += 0;
                            $absentwreasonArr[$wk_ts] += 0;
                            $tardyArr[$wk_ts] += 0;
                            $tardywreasonArr[$wk_ts] += 0;
                        }
                    }
                    $catArr[] = $categoryname;
                }
            }
//                print_r($presentArr);exit;
            foreach ($presentArr as $presentArr1) {
                $present[] = $presentArr1;
            }
            foreach ($absentArr as $absentArr1) {
                $absent[] = $absentArr1;
            }
            foreach ($absentwreasonArr as $absentwreasonArr1) {
                $absentwreason[] = $absentwreasonArr1;
            }
            foreach ($tardyArr as $tardyArr1) {
                $tardy[] = $tardyArr1;
            }
            foreach ($tardywreasonArr as $tardywreasonArr1) {
                $tardywreason[] = $tardywreasonArr1;
            }
            $chart->xAxis->categories = $catArr;
            $chart->series[] = array('name' => 'Present',
                'data' => $present, 'color' => '#8bbc21');
            $chart->series[] = array('name' => 'Absent',
                'data' => $absent, 'color' => '#FF0000');
            $chart->series[] = array('name' => 'Absent w/ excuse',
                'data' => $absentwreason, 'color' => '#A52A2A');
            $chart->series[] = array('name' => 'Tardy',
                'data' => $tardy, 'color' => '#2f7ed8');
            $chart->series[] = array('name' => 'Tardy w/ excuse',
                'data' => $tardywreason, 'color' => '#0d233a');
        }

        if ($this->input->post('view') == 'year') {

            $strDateFrom = $this->input->post('start');
            $strDateTo = $this->input->post('end');
//                $weeksnumber = $this->datediff('ww',$strDateFrom, $strDateTo,false);
            $yearsnumber = $this->getYears($strDateFrom, $strDateTo);
//               print_r($monthsnumber);exit;

            $chart->chart->type = "column";


            $chart->plotOptions->series->stacking = "normal";

            $this->load->model('studentmodel');
            $studentsroll = $this->studentmodel->getstudentroll();
            $chart->tooltip->formatter = new HighchartJsExpr("function() {
                    return '' + this.series.name +': '+ this.y +'';}");
            $chart->credits->enabled = false;
            if ($this->input->post('student') == 'All') {
                
            } else {
                
                $student_details = $this->studentmodel->getStudentDetails($studentsroll[0]->student_id);
                $student_name = $student_details[0]->firstname . ' ' . $student_details[0]->lastname;
                $chart->title->text = "<h3 style='text-align:center;'>Monthly Attendance report of <b>$student_name</b></h3>";
                $presentArr = array();
                $absentArr = array();
                foreach ($yearsnumber as $yearnumber) {
                    
                    $flag = false;
//                    $wk_ts = strtotime('+' . $weeknumber['month'] . ' months', strtotime($weeknumber['year'] . '0101'));
                    $totalDaysofYear = date("z", mktime(0,0,0,12,31,$yearnumber['year']));
                    
                    $firstdate = date('Y-m-d', mktime(0, 0, 0, 1, 1, $yearnumber['year']));
                    
                    for ($first = 0; $first <= $totalDaysofYear; $first++) {
                        $day_ts = strtotime('+'.($first).' day', strtotime($firstdate));
                        $categoryname = date('Y', strtotime($yearnumber['year'].'-01-01'));
                        
//                        echo date('m-d-Y',$day_ts);exit;
                       // echo $categoryname;exit;

//                            $presentArr[$wk_ts] = 0;
//                            $absentArr[$wk_ts] = 0;
//                            $absentwreasonArr[$wk_ts] = 0;
//                            $tardyArr[$wk_ts] = 0;
//                            $tardywreasonArr[$wk_ts] = 0;

                        foreach ($studentsroll as $studentroll) {
//                                    echo $studentroll->date.'=='.$day_ts;
                            if ($studentroll->date == date('Y-m-d', $day_ts)) {
                                switch (strtolower($studentroll->status)) {
                                    case 'present':
                                        $presentArr[$wk_ts] += 1;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'absent':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 1;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'absent w/ excuse':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 1;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'tardy':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 1;
                                        $tardywreasonArr[$wk_ts] += 0;
                                        $flag = true;
                                        break;
                                    case 'tardy w/ excuse':
                                        $presentArr[$wk_ts] += 0;
                                        $absentArr[$wk_ts] += 0;
                                        $absentwreasonArr[$wk_ts] += 0;
                                        $tardyArr[$wk_ts] += 0;
                                        $tardywreasonArr[$wk_ts] += 1;
                                        $flag = true;
                                        break;
                                }
                            }
                        }

                        if (!$flag) {
                            $presentArr[$wk_ts] += 0;
                            $absentArr[$wk_ts] += 0;
                            $absentwreasonArr[$wk_ts] += 0;
                            $tardyArr[$wk_ts] += 0;
                            $tardywreasonArr[$wk_ts] += 0;
                        }
                    }
                    $catArr[] = $categoryname;
                }
            }
//                print_r($presentArr);exit;
            foreach ($presentArr as $presentArr1) {
                $present[] = $presentArr1;
            }
            foreach ($absentArr as $absentArr1) {
                $absent[] = $absentArr1;
            }
            foreach ($absentwreasonArr as $absentwreasonArr1) {
                $absentwreason[] = $absentwreasonArr1;
            }
            foreach ($tardyArr as $tardyArr1) {
                $tardy[] = $tardyArr1;
            }
            foreach ($tardywreasonArr as $tardywreasonArr1) {
                $tardywreason[] = $tardywreasonArr1;
            }
            $chart->xAxis->categories = $catArr;
            $chart->series[] = array('name' => 'Present',
                'data' => $present, 'color' => '#8bbc21');
            $chart->series[] = array('name' => 'Absent',
                'data' => $absent, 'color' => '#FF0000');
            $chart->series[] = array('name' => 'Absent w/ excuse',
                'data' => $absentwreason, 'color' => '#A52A2A');
            $chart->series[] = array('name' => 'Tardy',
                'data' => $tardy, 'color' => '#2f7ed8');
            $chart->series[] = array('name' => 'Tardy w/ excuse',
                'data' => $tardywreason, 'color' => '#0d233a');
        }


        $html = $chart->render("chart1");

        echo $html;
	
    }

    function getDates($startTime, $endTime) {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);

        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day); // remove increment exit;

        $days = array();

        for ($i = 0; $i <= $numDays; $i++) { //change $i to 1
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    function getWeeks($startDate, $endDate) {
        $startDateUnix = strtotime($startDate);
        $endDateUnix = strtotime($endDate);

        $currentDateUnix = $startDateUnix;

        $weekNumbers = array();
        $cnt = 0;
        while ($currentDateUnix <= $endDateUnix) {
            if(date('w',$currentDateUnix)!=0){
            $weekNumbers[$cnt]['week'] = date('W', $currentDateUnix)-1;
            } else {
                $weekNumbers[$cnt]['week'] = date('W', $currentDateUnix);
            }
            $weekNumbers[$cnt]['year'] = date('Y', $currentDateUnix);
            $currentDateUnix = strtotime('+1 week', $currentDateUnix);
            $cnt++;
        }
        if(date('w',$endDateUnix)<=1){
            if(!$this->in_array_r(date('W', $currentDateUnix)-1,$weekNumbers)){
            $weekNumbers[$cnt]['week'] = date('W', $currentDateUnix)-1;
            $weekNumbers[$cnt]['year'] = date('Y', $currentDateUnix);
        }
        }
       // print_r($weekNumbers);exit;
        return $weekNumbers;
    }
    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }
    function getMonths($startDate, $endDate) {
        $startDateUnix = strtotime($startDate);
        $endDateUnix = strtotime($endDate);

        $currentDateUnix = $startDateUnix;

        $monthNumbers = array();
        $cnt = 0;
        while ($currentDateUnix < $endDateUnix) {
            $monthNumbers[$cnt]['month'] = date('m', $currentDateUnix);
            $monthNumbers[$cnt]['year'] = date('Y', $currentDateUnix);
            $currentDateUnix = strtotime('+1 month', $currentDateUnix);
            $cnt++;
        }

        return $monthNumbers;
    }
    
    function getYears($startDate, $endDate) {
        $startDateUnix = strtotime($startDate);
        $endDateUnix = strtotime($endDate);

        $currentDateUnix = $startDateUnix;

        $yearNumbers = array();
        $cnt = 0;
        while ($currentDateUnix < $endDateUnix) {
//            $monthNumbers[$cnt]['month'] = date('m', $currentDateUnix);
            $yearNumbers[$cnt]['year'] = date('Y', $currentDateUnix);
            $currentDateUnix = strtotime('+1 year', $currentDateUnix);
            $cnt++;
        }

        return $yearNumbers;
    }
    
    

    function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
        /*
          $interval can be:
          yyyy - Number of full years
          q - Number of full quarters
          m - Number of full months
          y - Difference between day numbers
          (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
          d - Number of full days
          w - Number of full weekdays
          ww - Number of full weeks
          h - Number of full hours
          n - Number of full minutes
          s - Number of full seconds (default)
         */

        if (!$using_timestamps) {
            $datefrom = strtotime($datefrom, 0);
            $dateto = strtotime($dateto, 0);
        }
        $difference = $dateto - $datefrom; // Difference in seconds

        switch ($interval) {

            case 'yyyy': // Number of full years
                $years_difference = floor($difference / 31536000);
                if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom) + $years_difference) > $dateto) {
                    $years_difference--;
                }
                if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto) - ($years_difference + 1)) > $datefrom) {
                    $years_difference++;
                }
                $datediff = $years_difference;
                break;
            case "q": // Number of full quarters
                $quarters_difference = floor($difference / 8035200);
                while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($quarters_difference * 3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                    $months_difference++;
                }
                $quarters_difference--;
                $datediff = $quarters_difference;
                break;
            case "m": // Number of full months
                $months_difference = floor($difference / 2678400);
                while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                    $months_difference++;
                }
                $months_difference--;
                $datediff = $months_difference;
                break;
            case 'y': // Difference between day numbers
                $datediff = date("z", $dateto) - date("z", $datefrom);
                break;
            case "d": // Number of full days
                $datediff = floor($difference / 86400);
                break;
            case "w": // Number of full weekdays
                $days_difference = floor($difference / 86400);
                $weeks_difference = floor($days_difference / 7); // Complete weeks
                $first_day = date("w", $datefrom);
                $days_remainder = floor($days_difference % 7);
                $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
                if ($odd_days > 7) { // Sunday
                    $days_remainder--;
                }
                if ($odd_days > 6) { // Saturday
                    $days_remainder--;
                }
                $datediff = ($weeks_difference * 5) + $days_remainder;
                break;
            case "ww": // Number of full weeks
                $datediff = floor($difference / 604800);
                break;
            case "h": // Number of full hours
                $datediff = floor($difference / 3600);
                break;
            case "n": // Number of full minutes
                $datediff = floor($difference / 60);
                break;
            default: // Number of full seconds (default)
                $datediff = $difference;
                break;
        }
        return $datediff;
    }
	
function attendance_managerpdf($start, $end, $student_id, $view, $periods,$teacher_id=''){
        //error_reporting(1);
    $this->load->Model('periodmodel');
        $preioddetails = $this->periodmodel->getperiodById($periods);
//        print_r($preioddetails);exit;
        $data['start2'] = $preioddetails[0]['start_time'];
        $data['end2'] = $preioddetails[0]['end_time'];
        $this->load->Model('studentmodel');
       
         $status = $this->studentmodel->getStudentsByRange($periods,$teacher_id);
  //print_r($status);exit;       
         $data['subject'] = $status[0]['subject_name'];
         $data['grade'] = $status[0]['grade'];
         
          $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(22);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(22);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }
         //print_r($reportdes);exit;
         
//        print_r($preioddetails);exit;
if($view =='day'){
    
    $data['start_date']=$start;
    $data['end_date']=$end;
        
        
    
    $this->load->Model('lessonplanmodel');
    //$data['studentresult'] = $this->lessonplanmodel->attendance_manager_data($start, $end, $student_id);
    
     $data['dateArr'] = $this->getDates($start, $end);
    $this->load->model('studentmodel');
    echo $teacher_id;
            $data['studentsroll'] = $this->studentmodel->getstudentroll_data($student_id,$periods,$teacher_id);
print_r($data['studentsroll']);exit;
    $data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
    //print_r($data['students_all_detail']);exit;
  if ($student_id == 'all') {
                
                $data['student_data'] =$student_id;
                $this->load->Model('studentmodel');
                
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//              print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
                }
                
                $data['student_details'] = $student_detail;
                //print_r($data['student_details']);exit;
                
    }else {
                $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
                //print_r($data['student_details']);exit;
            }
            
            
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
            }
        
          if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
        

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('attendance/attendance_manager_pdf_data', $data, TRUE);

        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

if($view =='week'){
    
    $data['start_date']=$start;
    $data['end_date']=$end;
    
    $this->load->Model('lessonplanmodel');
      $data['weeksnumber'] = $this->getWeeks($start,$end);
    //  print_r($data['weeksnumber']);exit;
    $this->load->model('studentmodel');
            $data['studentsroll'] = $this->studentmodel->getstudentroll_data($student_id,$periods,$teacher_id,$start,$end);
            $data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//print_r($data['students_all_detail']);exit;
          if ($student_id == 'all') {
              
                $data['student_data'] =$student_id;
                $this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//              print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
                }
                
                $data['student_details'] = $student_detail;
                //print_r($data['student_details']);exit;
            
                
            } else {
                 $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
                //print_r($data['student_details']);exit;            
            }
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
            }
        
          if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
        

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('attendance/attendance_manager_pdf_by_week', $data, TRUE);

        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }   
    
if($view =='month'){
    error_reporting(1);
    $this->load->Model('lessonplanmodel');
      $data['monthsnumber'] = $this->getMonths($start,$end);
          
          $data['yearnumber'] = $this->getYears($start,$end);
        $data['start_date']=$start;
        $data['end_date']=$end;

      $data['weeksnumber'] = $this->getWeeks($start,$end);
    //  print_r($data['weeksnumber']);exit;
    $this->load->model('studentmodel');
            $data['studentsroll'] = $this->studentmodel->getstudentroll_data($student_id,$periods,$teacher_id,$start,$end);
//                        echo $this->db->last_query();
//                        print_r($data['studentsroll']);exit;
            $data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//print_r($data['students_all_detail']);exit;
          if ($student_id == 'all') {
              
                $data['student_data'] =$student_id;
                $this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//              print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
                }
                
                $data['student_details'] = $student_detail;
                //print_r($data['student_details']);exit;
            
                
            } else {
                $data['student_data'] =$student_id;
                $this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
                 $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
                //print_r($data['student_details']);exit;            
            }
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
            } else if($teacher_id!='') {
                $user_id = $teacher_id;
            }
        
          if($this->session->userdata('login_type') == 'teacher' || $this->session->userdata('login_type') == 'parent'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
        $data['view_path'] = $this->config->item('view_path');
        
       
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        
       
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
//         $i = 0;
//            $j = 0;
//       foreach($data['monthsnumber'] as $months){
//           $month = $months['month'];
//           $year = $months['year'];
//        $ndays = date("t", strtotime($year . '-' . $month . '-01'));
//            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
//           //  print_r($weekdays);exit;
//            foreach ($weekdays as $key => $weekval) {
//                $ds = explode('/', $weekval['date']);
//                $dsa = $ds[0] . '/' . $ds[1];
//                $weeks[$key]['week'] = $dsa;
//                $weeks[$key]['dates'] = $weekval['date'];
//            }
//            
//            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
//            $dayss = $this->week_from_monday('01-' . $month . '-' . $year, $ndays, true);
//          
//            foreach ($dayss as $key => $dayssplits) {
//
//                if ($i % 7 == 0) {
//                    $j++;
//                }
//                $dayssplit[$j][] = $dayssplits;
//                $i++;
//            }
//       }
//            $data['dayssplit'] = $dayssplit;
           // print_r($dayssplit);exit;
            
//            print_r($data['student_details']);exit;
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////

    //  $this->load->view('attendance/attendance_manager_pdf_by_month', $data);
        /*exit;*/
//        print_r($data['monthsnumber']);exit;
       
         $str = $this->parser->parse('attendance/attendance_manager_pdf_by_month', $data, TRUE);
//         echo $str;exit;
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 10, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }       
    
if($view =='year'){
    $this->load->Model('lessonplanmodel');
       $data['yearsnumber'] = $this->getYears($start,$end);
//print_r($data['yearsnumber']);exit;
    $this->load->model('studentmodel');
            $data['studentsroll'] = $this->studentmodel->getstudentroll_data($student_id,$periods,$teacher_id);
            $data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//print_r($data['studentsroll']);exit;
          if ($student_id == 'all') {
                $data['student_data'] =$student_id;
                $this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//              print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
                }
                
                $data['student_details'] = $student_detail;
                //print_r($data['student_details']);exit;
            
                
            } else {
                 $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
                //print_r($data['student_details']);exit;            
            }
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
            }
        
          if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');

    //  $this->load->view('attendance/attendance_manager_pdf_by_year', $data);
        /*exit;*/
     echo $str = $this->parser->parse('attendance/attendance_manager_pdf_by_year', $data, TRUE);

        $html2pdf = new HTML2PDF('L', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }       
}

    	
public function promote_students() {
        
            $data['idname'] = 'attendance';
			
			    
            $this->load->Model('schoolmodel');
            $this->load->Model('periodmodel');
         
            $data['grades'] = $this->schoolmodel->getallgrades();

			$data['schools'] = $this->schoolmodel->getschoolbydistrict();
			
         
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
			
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('attendance/promote_students', $data);
		
    }
	
function get_grade_to_student_info()
	{
		
	$this->load->Model('classroommodel');
            $data['students'] = $this->classroommodel->getgradeBystudents_data($this->input->post("grade_id"));
	
		//print_r($data);exit;
		echo json_encode($data);
		
	  }
	

   public function update_promote_student() {
        $data['idname'] = 'classroom';
        if ($this->input->post('submit')) {
            $student = $this->input->post('student_id');
            $grade = $this->input->post('grade');

		foreach($student as $student_id){	
				
//                    $data = array('grade' => $grade);
                    
                    $this->load->model('classroommodel');
                    $result = $this->classroommodel->update_student_grade('students', $grade,$student_id);

                }
        }
        $this->session->set_flashdata('message','Student Promoted successfully.');
        redirect(base_url().'attendance/promote_students');
   }
   
   function attendance_manager_data_pdf($start, $end, $student_id, $view, $periods,$teacher_id=''){
        //error_reporting(1);
if($view =='day'){
	
	$data['start_date']=$start;
	$data['end_date']=$end;
	
	$this->load->Model('lessonplanmodel');
	//$data['studentresult'] = $this->lessonplanmodel->attendance_manager_data($start, $end, $student_id);
	
	 $data['dateArr'] = $this->getDates($start, $end);
	 
	 $this->load->model('studentmodel');
		$data['studentsroll'] = $this->studentmodel->getstudentrollBy_teacher_data($student_id,$periods,$teacher_id);
//print_r($data['studentsroll']);exit;
	
	$data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
	//print_r($data['students_all_detail']);exit;
	
  if ($student_id == 'all') {
	  			
				$data['student_data'] =$student_id;
				$this->load->Model('studentmodel');
				
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//				print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
				}
				
				$data['student_details'] = $student_detail;
				//print_r($data['student_details']);exit;
				
	}else {
	            $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
				//print_r($data['student_details']);exit;
            }
			
			
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			} else if($teacher_id!='') {
                $user_id = $teacher_id;
            }
		
		  if($this->session->userdata('login_type') == 'teacher' || $this->session->userdata('login_type') == 'parent'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
		

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('attendance/attendance_managerpdf_data', $data, TRUE);

        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

if($view =='week'){
	
	$data['start_date']=$start;
	$data['end_date']=$end;
	
	$this->load->Model('lessonplanmodel');
	  $data['weeksnumber'] = $this->getWeeks($start,$end);
	//  print_r($data['weeksnumber']);exit;
	 $this->load->model('studentmodel');
		$data['studentsroll'] = $this->studentmodel->getstudentrollBy_teacher_data($student_id,$periods,$teacher_id);	
	//print_r($data['studentsroll']);exit;
	$data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
	//print_r($data['students_all_detail']);exit;
			
//print_r($data['students_all_detail']);exit;
	      if ($student_id == 'all') {
			  
			  	$data['student_data'] =$student_id;
				$this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//				print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
				}
				
				$data['student_details'] = $student_detail;
				//print_r($data['student_details']);exit;
			
                
            } else {
                 $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
  				//print_r($data['student_details']);exit;            
			}
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
		
		  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }
		

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('attendance/attendance_managerpdf_data_by_week', $data, TRUE);

        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }	
	
if($view =='month'){
	$this->load->Model('lessonplanmodel');
	  $data['monthsnumber'] = $this->getMonths($start,$end);
          $data['yearnumber'] = $this->getYears($start,$end);
	$this->load->model('studentmodel');
				$data['studentsroll'] = $this->studentmodel->getstudentrollBy_teacher_data($student_id,$periods,$teacher_id);	
	//print_r($data['studentsroll']);exit;
	$data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
			
//print_r($data['students_all_detail']);exit;
	      if ($student_id == 'all') {
			  
			  	$data['student_data'] =$student_id;
				$this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//				print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
				}
				
				$data['student_details'] = $student_detail;
				//print_r($data['student_details']);exit;
			
                
            } else {
                 $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
  				//print_r($data['student_details']);exit;            
			}
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			} else if($teacher_id!='') {
                $user_id = $teacher_id;
            }
		
		  if($this->session->userdata('login_type') == 'teacher' || $this->session->userdata('login_type')=='parent'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');

	//	$this->load->view('attendance/attendance_manager_pdf_by_month', $data);
		/*exit;*/
     echo $str = $this->parser->parse('attendance/attendance_managerpdf_data_by_month', $data, TRUE);

        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }		
	
if($view =='year'){
	$this->load->Model('lessonplanmodel');
	   $data['yearsnumber'] = $this->getYears($start,$end);
//print_r($data['yearsnumber']);exit;
			$this->load->model('studentmodel');
				$data['studentsroll'] = $this->studentmodel->getstudentrollBy_teacher_data($student_id,$periods,$teacher_id);	
	//print_r($data['studentsroll']);exit;
	$data['students_all_detail'] = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
			
//print_r($data['studentsroll']);exit;
	      if ($student_id == 'all') {
			  	$data['student_data'] =$student_id;
				$this->load->Model('studentmodel');
                $students = $this->studentmodel->getStudentsByRange_data($periods,$teacher_id);
//				print_r($this->db->last_query());exit;
                //print_r($students);exit;
                foreach($students as $student){
                    $student_detail[$student['student_id']] = $this->studentmodel->getStudentDetails($student['student_id']);
				}
				
				$data['student_details'] = $student_detail;
				//print_r($data['student_details']);exit;
			
                
            } else {
                 $data['student_details'] = $this->studentmodel->getStudentDetails($data['studentsroll'][0]->student_id);
  				//print_r($data['student_details']);exit;            
			}
          if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
		
		  if($this->session->userdata('login_type') == 'teacher'){
             $this->load->Model('teachermodel');
            $data['teachername'] = $this->teachermodel->getteacherById($user_id);
           
        }else {
            $this->load->Model('observermodel');
            $data['teachername'] = $this->observermodel->getobserverById($user_id);
        }

        $data['view_path'] = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');

	//	$this->load->view('attendance/attendance_manager_pdf_by_year', $data);
		/*exit;*/
     echo $str = $this->parser->parse('attendance/attendance_managerpdf_data_by_year', $data, TRUE);

        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }		
}
   }

