<?php
/**
 * Countries Controller.
 *
 */
class Countries extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function getCountries()
	{
		$this->load->Model('countrymodel');
		$data['countries']=$this->countrymodel->getCountries();
	    echo json_encode($data);
		exit;
	
	}
	function getStates($id)
	{
		$this->load->Model('countrymodel');
		$data['states']=$this->countrymodel->getStates($id);
	    echo json_encode($data);
		exit;
	
	}
	
	
}	