<?php

class Time_table extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_observer()==false && $this->is_user()==false){
			//These functions are available only to schools - So redirect to the login page
			redirect("index");
		}
		
	}
	function index($msg=false)
	{
		$data['idname']='attendance';
		$this->load->Model('schoolmodel');	  
		$data['grades'] = $this->schoolmodel->getallgrades();	  	 
		if($this->session->userdata("login_type")=='user')
		{	
		  if($this->session->userdata('login_special') == !'district_management'){	
		  	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
		  }
			$data['schools']=$this->schoolmodel->getschoolbydistrict();	  
		}
		else if($this->session->userdata("login_type")=='observer')
		{		
			if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
			$data['schools']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));			  
		}
		$data['message']=$msg;
		$data['view_path']=$this->config->item('view_path');	  
		$this->load->view('time_table/index',$data);
	  
	
	}
	
	function createvalue()
	{
		 $data['idname']='attendance';
		if($this->input->post('submit') && $this->input->post('submit')=='Submit')
			{
			$this->session->set_userdata('time_table_class_work_id',$this->input->post('class_room_id'));
			$this->session->set_userdata('time_table_grade_id',$this->input->post('grade_id'));
			$this->session->set_userdata('time_table_school_id',$this->input->post('school_id'));
			}
		
		
		 $report_date=date('d-m-Y');
		
		
		
		
		$this->load->Model('periodmodel');		
		$periods=$this->periodmodel->getperiodbyschoolid();
		
		$this->load->Model('grade_subjectmodel');		
		$subjects=$this->grade_subjectmodel->getgrade_subjectall($this->input->post('grade_id'));
		
		
		
		if($periods==false)
		{			
		
			 $this->index('No Periods  Found For This School Please Add Periods   ');
		}
		else if($subjects==false)		
		{
			$this->index('No Subjects Found For This District Please Add Subjects In District Panel  ');

		}		
		
		else
		{ 
		  
			 
			 
			 $this->load->Model('time_tablemodel');
			 
			
			 
			 
			 
			 
			  
			  
			  $subjectstr=':;';
			 foreach($subjects as $subjectsvalue)
			 {
			     $keyvalue=$subjectsvalue['subject_id'];
				 $keytext=$subjectsvalue['subject_name'];
				$subjectstr.="$keyvalue:$keytext;";
				
			 
			 
			 }
			  $subjectstr=substr($subjectstr,0,-1);
			  $data['subjects']=$subjectstr;
			 // exit;
	         $timestr='';
		  
			$cols=''; 
			 $headers='';
		  foreach($periods as $key=>$value)
		  {		     
			
			    $dates=$this->week_from_monday($report_date);
				
				
			   foreach($dates as $dayvalue)
			   {
			      $checkresult = $this->time_tablemodel->check_time_feature($dayvalue['date'],$value['period_id'],$this->input->post('class_room_id'));
				  $new_id=$checkresult[0]['time_table_id'];
				  if($checkresult==false)
					{  
				    
					$new_id=$this->time_tablemodel->add_time_feature($dayvalue['date'],$value['period_id'],$this->input->post('class_room_id'));
					} 
			   }
			$head = array();
				$head["startColumnName"] = 'value_'.$key;
				$head["numberOfColumns"] =2 ;
				$head["titleText"] = "".$value['start_time']." ".$value['end_time']."";
				$head=json_encode($head);
				$headers.=$head.',';
				
				
			/*$col = array();
			$col["name"] = "value_".$key;
			$col["index"] = "time_".$key;
			$col["width"] = 75;
			$col["align"] = "left"; // render as select
			$col["editable"] = true;
			$col["edittype"] = "select"; // render as select			
			//$col["editoptions"] = array("value"=>'1:Off Task;2:On Task'); // with these values "key:value;key:value;key:value"
			
			$col["editoptions"] = array("value"=>$subjectstr); // with these values "key:value;key:value;key:value"
			$newas=json_encode($col);
			//print_r($newas);
			//exit;
			$cols.=$newas.',';
			
			$col = array();
			$col["name"] = "value_".$key."_".$key;
			$col["index"] = "time_".$key."_".$key;
			$col["width"] = 75;
			$col["align"] = "left"; // render as select
			$col["editable"] = true;
			$col["edittype"] = "select"; // render as select			
			//$col["editoptions"] = array("value"=>'1:Off Task;2:On Task'); // with these values "key:value;key:value;key:value"
			
			$col["editoptions"] = array("value"=>$subjectstr); // with these values "key:value;key:value;key:value"
			$newas=json_encode($col);
			$cols.=$newas.',';*/
			
			$timestr.="'Subject',";
			$timestr.="'Teacher',";
			
		  
		  }
		 
		  
		 
		  $timesstr=substr($timestr,0,-1);		  
		  $data['times']=$timesstr;
		  
		   $headers=substr($headers,0,-1);		  
		  $data['headers']=$headers;
		  
		   $cols=substr($cols,0,-1);		
		  $data['cols']=$cols;
		  $data['periods']=$periods;
//                  print_r($periods);exit;
		 
		  $data['view_path']=$this->config->item('view_path');	  
		  $this->load->view('time_table/time_create',$data);
		  
			
		}
	    
	
	}
	
	function get_teachers($subject_id)
	{
	  if($subject_id=='')
	  {
	     echo '<select class="editable" role="select" name="test"><option role="option" value="">No Teachers</option></select>';
	  
	  }
	  else
	  {
	  $this->load->Model('time_tablemodel');
	  $teachers=$this->time_tablemodel->getteachersbysubjectid($subject_id);
	   if($teachers!=false)
	   {
	    echo '<select class="editable" role="select" name="test"><option role="option" value=""></option>';
		foreach($teachers as $teachervalue)
		{
		 echo '<option role="option" value="'.$teachervalue['teacher_id'].'">'.$teachervalue['firstname'].'</option>';
		
		
		}
		echo '</select>';
	   }
	   else
	   {
	   echo '<select class="editable" role="select" name="test"><option role="option" value="">No Teachers</option></select>';
	   }
	  }
	
	}
	function get_allteachers($id)
	{
	   $ids=explode('_',$id);
	    $this->load->Model('time_tablemodel');
	   foreach($ids as $key=>$value)
	   {
		 
		 $time_table=$this->time_tablemodel->gettimetablebyid($value);
	     $time_table=$time_table[0];
		 if($time_table['subject_id']!='')
		 {
		 $teachers=$this->time_tablemodel->getteachersbysubjectid($time_table['subject_id']);
	   if($teachers!=false)
	   {
	   $data['res']['_value_'.$key.'_'.$key]='';
	    
		foreach($teachers as $teachervalue)
		{
		$data['res']['_value_'.$key.'_'.$key].='<option role="option" ';
		
		if($time_table['teacher_grade_subject_id']==$teachervalue['teacher_id'])
		{
		$data['res']['_value_'.$key.'_'.$key].=' selected ';
		
		}
		
		$data['res']['_value_'.$key.'_'.$key].=' value="'.$teachervalue['teacher_id'].'">'.$teachervalue['firstname'].'</option>';
		 
		
		}
		
	   }
		 
		 }
		 
	   
	   }
	   
	   echo json_encode($data);
	   exit;
	  
	
	}
function gettimetable()
{
	$page = isset($_POST['page'])?$_POST['page']:1; // get the requested page
	$limit = isset($_POST['rows'])?$_POST['rows']:10; // get how many rows we want to have into the grid
	$sidx = isset($_POST['sidx'])?$_POST['sidx']:'name'; // get index row - i.e. user click to sort
	$sord = isset($_POST['sord'])?$_POST['sord']:''; // get the direction
	//$this->load->Model('dist_subjectmodel');
	$this->load->Model('time_tablemodel');	
		$this->load->Model('grade_subjectmodel');		
		$subjects=$this->grade_subjectmodel->getgrade_subjectall($this->session->userdata('time_table_grade_id'));
//		print_r($subjects);exit;
		//$subjects=$this->dist_subjectmodel->getdist_subjectsById();
			 $valuetext['']='';
			 foreach($subjects as $subjectvalue)
			 {
			    $valuetext[$subjectvalue['subject_id']]=$subjectvalue['subject_name'];
			 
			 }
			 
			
 
	if(!$sidx) $sidx =1;
	
	$dates=$this->week_from_monday(date('d-m-Y'));
 
	$count = count($dates);
 
	if( $count > 0 ) {
		$total_pages = ceil($count/$limit);    //calculating total number of pages
	} else {
		$total_pages = 0;
	}
	if ($page > $total_pages) $page=$total_pages;
 
	$start = $limit*$page - $limit; // do not put $limit*($page - 1)
	$start = ($start<0)?0:$start;  // make sure that $start is not a negative value
 
	
	$result = $this->time_tablemodel->time_feature($dates[0]['date'],$dates[6]['date']);  //here DB_Func is a model handling DB interaction
   // print_r($result);exit;
	//exit;
	if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
		header("Content-type: application/xhtml+xml;charset=utf-8");
	} else {
		header("Content-type: text/xml;charset=utf-8");
	}
	$et = ">";
 
	echo "<?xml version='1.0' encoding='utf-8'?$et\n";
	echo "<rows>";
	echo "<page>".$page."</page>";
	echo "<total>".$total_pages."</total>";
	echo "<records>".$count."</records>";
	// be sure to put text data in CDATA
	
	
	
	
	if($dates!=false)
	{
	foreach($dates as $key=>$dayvalue)
	{
	
	$ids='';
	foreach($result as $row) {
	if($row['current_date']==$dayvalue['date'])	
	{
	if($row['subject_id']!='')
	{
	  $teachers=$this->time_tablemodel->getteachersbysubjectid($row['subject_id']);
	 
	 $teachertext['']='';
			 if($teachers!=false)
			 {
			 foreach($teachers as $teachervalue)
			 {
			    $teachertext[$teachervalue['teacher_id']]=$teachervalue['firstname'];
			 
			 }
			 }
	}
	$ids.=$row['time_table_id'].'_';
	}
	}
	
	 $ids=substr($ids,0,-1);		 
	echo "<row id='".$ids."'>";
	echo "<cell><![CDATA[".$dayvalue['week']."]]></cell>";	
	echo "<cell><![CDATA[act]]></cell>";
	foreach($result as $row) {
	if($row['current_date']==$dayvalue['date'])	
	{
	$s=0;
	$t=0;
	foreach ($subjects as $item) {
        if ($item['subject_id'] == $row['subject_id'])
           $s=1;
    }
	$teachers=false;
	if($row['subject_id']!='')
	{
	$teachers=$this->time_tablemodel->getteachersbysubjectid($row['subject_id']);
	}
	if($teachers!=false)
			 {
	foreach ($teachers as $teacheritem) {
        if ($teacheritem['teacher_id'] == $row['teacher_grade_subject_id'])
           $t=1;
    }
	}
	if($s==1 && $t==1)
	{
	echo "<cell><![CDATA[".$valuetext[$row['subject_id']]."]]></cell>";
	echo "<cell><![CDATA[".$teachertext[$row['teacher_grade_subject_id']]."]]></cell>";
	}
	else
	{
	echo "<cell><![CDATA[]]></cell>";
	echo "<cell><![CDATA[]]></cell>";
	}
	}
	
	
	}
	
	echo "</row>";
	/*foreach($result as $row) {
		echo "<row id='".$row['value_id']."'>";
		echo "<cell><![CDATA[".$studentvalue['firstname']."]]></cell>";
		echo "<cell><![CDATA[".$row['value_id']."]]></cell>";
		echo "<cell><![CDATA[".$row['task']."]]></cell>";		
		echo "<cell><![CDATA[act]]></cell>";
		
		echo "</row>";
	}*/
	}
	
	}
	echo "</rows>";
}
	function edit_test_save()
	{
		$this->load->Model('time_tablemodel');
		if(isset($_POST))
		{
			
			$ids=explode('_',$_POST['id']);
			$status=0;
			foreach($ids as $key=>$idvalue)
			{
			if(isset($_POST['value_'.$key.'_'.$key]) && $_POST['value_'.$key.'_'.$key]!='')
			{
			$time_table=$this->time_tablemodel->gettimetablebyid($idvalue);
			$time_table=$time_table[0];
			$status=$this->time_tablemodel->checkteacheralready($idvalue,$time_table['period_id'],$time_table['current_date'],$_POST['value_'.$key.'_'.$key]);
			}
			if($status==1)
			{
			
			break;
			}
			}
			if($status==0)
			{
			//model function updateRow writes and executes an UPDATE query with the data supplied
			$r = $this->time_tablemodel->updateRow('time_table',$_POST,array('id'=>$_POST['id'])); //$_POST contains all the editable data when we hit the save button in the grid
			echo $r;
			}
			else
			{
			  echo 'The Same Teacher Already Assinged To Another Class Period .';
			
			}
		}
		
	}
	function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
       // $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
	    $dates[$i]['date'] = $i;
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}
}	