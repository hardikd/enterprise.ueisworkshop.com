<?php 
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
/**
 * Studentdetail Controller.
 *
 */

class Comparegradeinschoolgraph extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false ){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{  error_reporting(0);
	 	 
		 if($this->session->userdata("login_type")=='user')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('comparegradeinschoolgraphmodel');
			    $district_id = $this->session->userdata('district_id');
				$data['records'] = $this->comparegradeinschoolgraphmodel->getcategoryandgrade($district_id);
	 			$this->load->view('comparegradeinschoolgraph/index',$data);
	 	 }
	}
	
 		
	function getReportHtml()
	{
		$cat_id ='';$school_id='';$grade_id=''; $fDate='';$tDate='';
		 $cat_id= $_REQUEST['cat_id']; 
		 $school_id = $_REQUEST['school_id']; 
		 $grade_id= $_REQUEST['grade_id'];
		 $fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		 	
		$district_id = $this->session->userdata('district_id');
		$this->load->model('comparegradeinschoolgraphmodel');
		$data= $this->comparegradeinschoolgraphmodel->getGraphData($district_id,$cat_id,$grade_id,$school_id,$fDate,$tDate);
		$IntRow = count($data);
		
			//	echo"<pre>"; print_r($data); exit;
		
	
		 $strResult ='';
		
		if(is_array($data))
		{
			echo json_encode($data);
			
			exit;
		
		}	
 	}
 
}?>