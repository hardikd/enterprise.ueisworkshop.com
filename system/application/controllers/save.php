<?php

class Save extends	Controller {

	function __construct()
	{
		parent::Controller();
	}
	function index()
	{
	
	
	
	
	
	
	
	 $this->load->view('api/save');
	
	}
	
	function savereport()
	{
	
	 $username=$this->input->post('username');
	 $password=$this->input->post('password');
	 $type=$this->input->post('type');
	 $reporttype=$this->input->post('reporttype');
	 $content=$this->input->post('content');
	 if($username && $password  && $type && $reporttype && $content)
	 {
	    if($reporttype=='checklist')
		{ 
		
		  $this->checklistreport($username,$password,$type,$content);
		
		}
		else  if($reporttype=='likert')
		{
			$this->lickertreport($username,$password,$type,$content);
		}
		else  if($reporttype=='rubric')
		{
			$this->proficiencyrubricreport($username,$password,$type,$content);
		}
		else  if($reporttype=='scale')
		{
			$this->proficiencyscalereport($username,$password,$type,$content);
		}
		
	 
	 }
	 else
	 {
	 
	    echo "error|Required All Fields";
	 
	 }
	
	
	
	}
	
	function checklistreport($username,$password,$type,$content)
	{
	  $data=$content;
	  //echo '<pre>';
	 
	 $arraydata=json_decode($data, true);
	 if(!empty($arraydata))
	 {
	    if(!empty($arraydata['info']))
		{
		  /*$username=$arraydata['info']['user_login'];
		  //$username='dist';
		  $password='12345678';
		  $type='district';*/
		  $status=$this->check_login($username,$password,$type);
	 if($status!=false){
	    $this->load->Model('reportmodel');
		$this->load->Model('observationpointmodel');
	    $district_id=$status[0]['district_id'];
	    $arraydata['info']['district_id']=$district_id;
		$date = new DateTime($arraydata['info']['date']);
		$arraydata['info']['date']=$date->format('Y-m-d');
		
		  $arraydata['info']['form_type']='forma';
		
		
		
		$reportdata = array('report_date' => $arraydata['info']['date'], 
					'school_id'=> $arraydata['info']['school_id'],
					'teacher_id'=> $arraydata['info']['teacher_id'],
					'subject_id'=> $arraydata['info']['subject_id'],
					'grade_id'=> $arraydata['info']['grade_id'],
					'students'=> $arraydata['info']['students_present'],
					'paraprofessionals'=> $arraydata['info']['paraprofessionals'],
					'observer_id'=> $arraydata['info']['observer_id'],
					'report_form'=> $arraydata['info']['form_type'],
					'period_lesson'=> $arraydata['info']['period_lesson'],
					'district_id'=>$arraydata['info']['district_id']
					
		
		
		);
	    
		 $report_id=$this->reportmodel->ApiCreateReport($reportdata);
		
		
		
			
	  if($report_id!=0)
	  {
	    if(!empty($arraydata['detail']))
		{
		   // print_r($arraydata['detail']);
			
			$group_again=0;
			foreach($arraydata['detail'] as $val)
			{
			$gcheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			$gcounter=0;
			
			}
			else
			{
				$gcounter=1;
			
			}
			if($gcounter==0)
			{
			  if($val['notes']!='')
			  {
			    $this->observationpointmodel->savenotes($report_id,$group_id,$val['note']);
			  
			  
			  }
			
			}
		
		    if($val['group_type_id']!=2)
			{
			    
				if($val['Checked']=='YES')
			  {
			    
				$this->observationpointmodel->savepoint($report_id,$val['point_id']);
			  
			  
			  }
			   
			}
			else if($val['group_type_id']==2) {
			
			 if($val['Checked']=='YES')
			  {
			    $this->observationpointmodel->savesubgroup($report_id,$val['point_id'],$val['sub_group_id']);
			  
			  
			  }
			
			
			}
		   $group_again=$val['group_id'];
		
		
		}
		echo "message|Checklist Success";
		exit;
		
		}
	  
	  }
	  else
	  {
	    echo "error|Failed Please Try Again";
		exit;
	  
	  
	  }
	  
	    }
		else
	 {
	 
	   echo "error|User Name or password are incorrect";
	   exit;
	 
	 }
		
		
		}
		else
		{
		
		echo 'error|Empty Data';
		exit;
		
		}
		
	 
	 
	 }
	 else
	 {
	 
	    echo 'error|Empty Data';
		exit;
	 
	 
	 
	 
	 }
	 
	 
	}
	
	function lickertreport($username,$password,$type,$content)
	{
	  $data=$content;
	  //echo '<pre>';
	 
	 $arraydata=json_decode($data, true);
	 if(!empty($arraydata))
	 {
	    if(!empty($arraydata['info']))
		{
		/*$username=$arraydata['info']['user_login'];
		  //$username='dist';
		  $password='12345678';
		  $type='district';*/
		  $status=$this->check_login($username,$password,$type);
	 if($status!=false){
	    $this->load->Model('reportmodel');
		$this->load->Model('lickertpointmodel');
	    $district_id=$status[0]['district_id'];
	    $arraydata['info']['district_id']=$district_id;
		$date = new DateTime($arraydata['info']['date']);
		$arraydata['info']['date']=$date->format('Y-m-d');
		
		  $arraydata['info']['form_type']='formc';
		
		
		
		$reportdata = array('report_date' => $arraydata['info']['date'], 
					'school_id'=> $arraydata['info']['school_id'],
					'teacher_id'=> $arraydata['info']['teacher_id'],
					'subject_id'=> $arraydata['info']['subject_id'],
					'grade_id'=> $arraydata['info']['grade_id'],
					'students'=> $arraydata['info']['students_present'],
					'paraprofessionals'=> $arraydata['info']['paraprofessionals'],
					'observer_id'=> $arraydata['info']['observer_id'],
					'report_form'=> $arraydata['info']['form_type'],
					'period_lesson'=> $arraydata['info']['period_lesson'],
					'district_id'=>$arraydata['info']['district_id']
					
		
		
		);
	    
		 $report_id=$this->reportmodel->ApiCreateReport($reportdata);
		
		
		
			
	  if($report_id!=0)
	  {
	    if(!empty($arraydata['detail']))
		{
		    //print_r($arraydata['detail']);
			
			$group_again=0;
			foreach($arraydata['detail'] as $val)
			{
			$gcheck=0;
			$group_id=$val['group_id'];
			if($group_again!=$group_id)
			{
			$gcounter=0;
			
			}
			else
			{
				$gcounter=1;
			
			}
			if($gcounter==0)
			{
			  if($val['notes']!='')
			  {
			    $this->lickertpointmodel->savenotes($report_id,$group_id,$val['note']);
			  
			  
			  }
			
			}
		
		    if($val['group_type_id']!=2)
			{
			    
				if($val['Checked']=='YES')
			  {
			    
				$this->lickertpointmodel->savepoint($report_id,$val['point_id']);
			  
			  
			  }
			   
			}
			else if($val['group_type_id']==2) {
			
			 if($val['Checked']=='YES')
			  {
			    $this->lickertpointmodel->savesubgroup($report_id,$val['point_id'],$val['sub_group_id']);
			  
			  
			  }
			
			
			}
		   $group_again=$val['group_id'];
		
		
		}
		echo "message|Likert Success";
		exit;
		
		}
	  
	  }
	  else
	  {
	    echo "error|Failed Please Try Again";
		exit;
	  
	  
	  }
	  
	    }
		else
	 {
	 
	   echo "error|User Name or password are incorrect";
	   exit;
	 
	 }
		
		
		}
		else
		{
		
		echo 'error|Empty Data';
		exit;
		
		}
		
	 
	 
	 }
	 else
	 {
	 
	    echo 'error|Empty Data';
		exit;
	 
	 
	 
	 
	 }
	 
	 
	}
	
	function proficiencyrubricreport($username,$password,$type,$content)
	{
	  $data=$content;
	  //echo '<pre>';
	 
	 $arraydata=json_decode($data, true);
	 if(!empty($arraydata))
	 {
	    if(!empty($arraydata['info']))
		{
		 /*$username=$arraydata['info']['user_login'];
		  //$username='dist';
		  $password='12345678';
		  $type='district';*/
		  $status=$this->check_login($username,$password,$type);
	 if($status!=false){
	    $this->load->Model('reportmodel');
		$this->load->Model('proficiencypointmodel');
	    $district_id=$status[0]['district_id'];
	    $arraydata['info']['district_id']=$district_id;
		$date = new DateTime($arraydata['info']['date']);
		$arraydata['info']['date']=$date->format('Y-m-d');
		
		  $arraydata['info']['form_type']='formp';
		
		
		
		$reportdata = array('report_date' => $arraydata['info']['date'], 
					'school_id'=> $arraydata['info']['school_id'],
					'teacher_id'=> $arraydata['info']['teacher_id'],
					'subject_id'=> $arraydata['info']['subject_id'],
					'grade_id'=> $arraydata['info']['grade_id'],
					'students'=> $arraydata['info']['students_present'],
					'paraprofessionals'=> $arraydata['info']['paraprofessionals'],
					'observer_id'=> $arraydata['info']['observer_id'],
					'report_form'=> $arraydata['info']['form_type'],
					'period_lesson'=> $arraydata['info']['period_lesson'],
					'district_id'=>$arraydata['info']['district_id']
					
		
		
		);
	    
		 $report_id=$this->reportmodel->ApiCreateproficiency($reportdata);
		
		
		
			
	  if($report_id!=0)
	  {
	    if(!empty($arraydata['detail']))
		{
		   // print_r($arraydata['detail']);
			
			
			
			foreach($arraydata['detail'] as $val)
			{
			
		
		    if($val['group_type_id']!=2)
			{
			    
				if($val['Checked']=='YES')
			  {
			    
				$this->proficiencypointmodel->savepoint($report_id,$val['point_id']);
			  
			  
			  }
			   
			}
			else if($val['group_type_id']==2) {
			
			 if($val['Checked']=='YES')
			  {
			    $this->proficiencypointmodel->savesubgroup($report_id,$val['point_id'],$val['sub_group_id']);
			  
			  
			  }
			
			
			}
		  
		
		
		}
		echo "message|Proficiency Rubric Success";
		exit;
		
		}
	  
	  }
	  else
	  {
	    echo "error|Failed Please Try Again";
		exit;
	  
	  
	  }
	  
	    }
		else
	 {
	 
	   echo "error|User Name or password are incorrect";
	   exit;
	 
	 }
		
		
		}
		else
		{
		
		echo 'error|Empty Data';
		exit;
		
		}
		
	 
	 
	 }
	 else
	 {
	 
	    echo 'error|Empty Data';
		exit;
	 
	 
	 
	 
	 }
	 
	 
	}
	
	function proficiencyscalereport($username,$password,$type,$content)
	{
	  $data=$content;
	  //echo '<pre>';
	 
	 $arraydata=json_decode($data, true);
	 if(!empty($arraydata))
	 {
	    if(!empty($arraydata['info']))
		{
		  /*$username=$arraydata['info']['user_login'];
		  //$username='dist';
		  $password='12345678';
		  $type='district';*/
		  $status=$this->check_login($username,$password,$type);
	 if($status!=false){
	    $this->load->Model('reportmodel');
		$this->load->Model('rubricscalesubmodel');
	    $district_id=$status[0]['district_id'];
	    $arraydata['info']['district_id']=$district_id;
		$date = new DateTime($arraydata['info']['date']);
		$arraydata['info']['date']=$date->format('Y-m-d');
		
		  $arraydata['info']['form_type']='formb';
		
		
		
		$reportdata = array('report_date' => $arraydata['info']['date'], 
					'school_id'=> $arraydata['info']['school_id'],
					'teacher_id'=> $arraydata['info']['teacher_id'],
					'subject_id'=> $arraydata['info']['subject_id'],
					'grade_id'=> $arraydata['info']['grade_id'],
					'students'=> $arraydata['info']['students_present'],
					'paraprofessionals'=> $arraydata['info']['paraprofessionals'],
					'observer_id'=> $arraydata['info']['observer_id'],
					'report_form'=> $arraydata['info']['form_type'],
					'period_lesson'=> $arraydata['info']['period_lesson'],
					'district_id'=>$arraydata['info']['district_id']
					
		
		
		);
	    
		 $report_id=$this->reportmodel->ApiCreateReport($reportdata);
		
		
		
			
	  if($report_id!=0)
	  {
	    if(!empty($arraydata['detail']))
		{
		    //print_r($arraydata['detail']);
			
			
			
			foreach($arraydata['detail'] as $val)
			{
			
		
		    if($val['sub_scale_id']=='')
			{
			    
				
			    
				$this->rubricscalesubmodel->savepoint($report_id,$val['scale_id'],$val['strengths'],$val['concerns'],$val['score']);
			  
			  
			  
			   
			}
			else {
			
			 
			    $this->rubricscalesubmodel->savesubgroup($report_id,$val['scale_id'],$val['sub_scale_id'],$val['strengths'],$val['concerns'],$val['score']);
			  
			  
			  
			
			
			}
		  
		
		
		}
		echo "message|Proficiency Scale Success";
		exit;
		
		}
	  
	  }
	  else
	  {
	    echo "error|Failed Please Try Again";
		exit;
	  
	  
	  }
	  
	    }
		else
	 {
	 
	   echo "error|User Name or password are incorrect";
	   exit;
	 
	 }
		
		
		}
		else
		{
		
		echo 'error|Empty Data';
		exit;
		
		}
		
	 
	 
	 }
	 else
	 {
	 
	    echo 'error|Empty Data';
		exit;
	 
	 
	 
	 
	 }
	 
	 
	}
	function check_login($username,$password,$type)
	{
	 $this->load->Model('ApiModel');
	 $status=$this->ApiModel->login($username,$password,$type);
	  return $status;
	
	
	}
	 
}	 