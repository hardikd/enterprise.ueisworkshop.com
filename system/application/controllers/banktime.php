<?php
include(MEDIA_PATH.'inc/foto_upload_script.php');
//error_reporting(0);
/**
 * district and observer Controller.
 *
 */
class Banktime extends	MY_Auth {
	function __Construct()
	{
		parent::Controller();
		if($this->is_observer()==false && $this->is_user()==false &&  $this->is_teacher()==false &&  $this->is_parent()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("index");
		}
		
	}
	
	function index()
	{
            $data['idname']='tools';
	 if($this->input->post('school_id'))
	 {
	    $this->session->set_userdata('search_aca_school_id',$this->input->post('school_id'));
	 }
	 else
	 {
	    $this->session->unset_userdata('search_aca_school_id');
	 
	 
	 }
	 if($this->input->post('subject_id'))
	 {
	    $this->session->set_userdata('search_aca_subject_id',$this->input->post('subject_id'));
	 }
	 else
	 {
	    $this->session->unset_userdata('search_aca_subject_id');
	 
	 
	 }
	 if($this->input->post('grade_id'))
	 {
	    $this->session->set_userdata('search_aca_grade_id',$this->input->post('grade_id'));
	 }
	 else
	 {
	    $this->session->unset_userdata('search_aca_grade_id');
	 
	 
	 }
	 if($this->input->post('alphasearch'))
	 {
	    $this->session->set_userdata('search_aca_search_id',$this->input->post('alphasearch'));
	 }
	 else
	 {
	    $this->session->unset_userdata('search_aca_search_id');
	 
	 
	 }
	$this->load->Model('dist_subjectmodel');
	$this->load->Model('dist_grademodel');
	 $data['subjects']=$this->dist_subjectmodel->getdist_subjectsById();
	  $data['grades']=$this->dist_grademodel->getdist_gradesById();
	  
	 $this->load->Model('banktimemodel');
	   $data['updatesarray']=$this->banktimemodel->Updates();
	 $data['view_path']=$this->config->item('view_path');
	 if($data['updatesarray'])
	 {
	    foreach($data['updatesarray'] as $upone )
		{
		  $data['commentsarray'][$upone['banking_time_id']]=$this->banktimemodel->Comments($upone['banking_time_id']);
		
		
		}
	 
	 }
	
	 $this->load->Model('schoolmodel');
	 $data['school']=$this->schoolmodel->getschoolbydistrict();
	 if($this->session->userdata('login_type')=='teacher')
	 {
		$this->load->view('banktime/teacher',$data);
	  }
	 else
	 {
        $this->load->view('banktime/index',$data);
     }	 
	
	}
	
	function update_message()
	{
	
				 if(isSet($_POST['update']) && isSet($_POST['school_id']) && isSet($_POST['subject_id']) && isSet($_POST['grade_id']) )
			{
			$update=$_POST['update'];
			$school_id=$_POST['school_id'];
			$subject_id=$_POST['subject_id'];
			$grade_id=$_POST['grade_id'];
			$file_path=$_POST['fileupload'];
			
			$this->load->Model('banktimemodel');
			$data=$this->banktimemodel->Insert_Update($update,$school_id,$subject_id,$grade_id,$file_path);
           

			if($data)
			{
			$msg_id=$data['banking_time_id'];
			$message=$this->tolink(htmlentities($data['message']));
			$time=$data['created'];
			$uid=$data['user_id'];
			$username=$data['username'];
			$avatar=$data['avatar'];
			$type=$data['type'];
			$school_name=$data['school_name'];
			$subject_name=$data['subject_name'];
			$grade_name=$data['grade_name'];
			if($data['file_path']!='')
			{
			$filename=explode('.',$data['file_path']);
			$file=$filename[1];
			if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'banktime/'.$data['file_path'].' height="100" width="100">';
			}
			else
			{
			  if (file_exists(WORKSHOP_FILES.'bankthumb/'.$filename[0].'.jpeg')) {
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'bankthumb/'.$filename[0].'.jpeg >';
				} else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'banktime/'.$data['file_path'].'[0] '.WORKSHOP_FILES.'bankthumb/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					else
					{
					 $commandtext='sudo -u unoconv /usr/bin/unoconv -f pdf -o '.WORKSHOP_FILES.'bankgenerated '.WORKSHOP_FILES.'banktime/'.$data['file_path'];
					 exec($commandtext);
					 
					  $commandtexta='convert -thumbnail x100 '.WORKSHOP_FILES.'bankgenerated/'.$filename[0].'.pdf[0] '.WORKSHOP_FILES.'bankthumb/'.$filename[0].'.jpeg ';
					exec($commandtexta);
					 
					}
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'bankthumb/'.$filename[0].'.jpeg >';
				}
			  
			  
			
			}
			$file_path=WORKSHOP_DISPLAY_FILES.'banktime/'.$data['file_path'];
            $newvar='<a target="_blank" href='.$file_path.'>'.$fileas.'</a>';
			}
			else
			{
			 $newvar='';
			
			}
			if($type=='user')
			{
			  $type='District';
			
			}
			echo '<script type="text/javascript"> 
$(document).ready(function(){$("#stexpand").oembed("'.$data['message'].'",{maxWidth: 400, maxHeight: 300});});
</script><div class="stbody" id="stbody'.$msg_id.'">
			<div class="stimg"><img src="'.SITEURLM.'images/'.$avatar.'.gif" class="big_face"/>
			</div> 
			<div class="sttext">

			<b>Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.$type.')&nbsp;&nbsp;&nbsp;&nbsp;<b>School:&nbsp;</b>'.$school_name.'    </b><br />
			<b>Subject:&nbsp;'.$subject_name.'&nbsp;&nbsp;&nbsp;&nbsp;<b>Grade:&nbsp;</b>'.$grade_name.'</b>
			<br /><b>Post:</b>'.$message.$newvar.' 
			<div class="sttime">'.$this->time_stamp($time).' | <a href="#" class="commentopen" id="'.$msg_id.'" title="Comment">Comment </a></div> 
			<div id="stexpandbox">
			<div id="stexpand"></div>
			</div>
			<div class="commentcontainer" id="commentload'.$msg_id.'">
			</div>
			<div class="commentupdate" style="display:none" id="commentbox'.$msg_id.'">
			<div class="stcommentimg">
			<img src="'.SITEURLM.'images/'.$this->session->userdata('avatar_id').'.gif" class="small_face"/>
			</div> 
			<div class="stcommenttext" >
			<form method="post" action="">
			<textarea name="comment" class="comment" maxlength="200"  id="ctextarea'.$msg_id.'"></textarea>
			<br />
			<input type="submit"  value=" Comment "  id="'.$msg_id.'" class="comment_button"/>
			</form>
			</div>
			</div>
			</div> 
			</div>';

			}
			}
				
	
	
	
	
	}
	
	function more_updates()
	{
	 if(isSet($_POST['lastmsg']) )
			{
			 $last_msg_id=$_POST['lastmsg'];
			 $this->load->Model('banktimemodel');
			$data=$this->banktimemodel->more_Update($last_msg_id);
            //echo '<pre>';
			//print_r($data)            ; 

			if($data)
			{
			foreach($data as $moredata)
			{
			  $commentsarray=$this->banktimemodel->Comments($moredata['banking_time_id']);
			  
			$msg_id=$moredata['banking_time_id'];
			$message=$this->tolink(htmlentities($moredata['message']));
			$time=$moredata['created'];
			$uid=$moredata['user_id'];
			$username=$moredata['username'];
			$type=$moredata['type'];
			$school_name=$moredata['school_name'];
			$subject_name=$moredata['subject_name'];
			$grade_name=$moredata['grade_name'];
			$avatar=$moredata['avatar'];
            if($moredata['file_path']!='')
			{
			$filename=explode('.',$moredata['file_path']);
			$file=$filename[1];
			if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'banktime/'.$moredata['file_path'].' height="100" width="100">';
			}
			else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'banktime/'.$moredata['file_path'].'[0] '.WORKSHOP_FILES.'bankthumb/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					else
					{
					 $commandtext='sudo -u unoconv /usr/bin/unoconv -f pdf -o '.WORKSHOP_FILES.'bankgenerated '.WORKSHOP_FILES.'banktime/'.$moredata['file_path'];
					 exec($commandtext);
					 
					  $commandtexta='convert -thumbnail x100 '.WORKSHOP_FILES.'bankgenerated/'.$filename[0].'.pdf[0] '.WORKSHOP_FILES.'bankthumb/'.$filename[0].'.jpeg ';
					exec($commandtexta);
					 
					}
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'bankthumb/'.$filename[0].'.jpeg >';
				}
			$file_path=WORKSHOP_DISPLAY_FILES.'banktime/'.$moredata['file_path'];
            $newvar='<a target="_blank" href='.$file_path.'>'.$fileas.'</a>';
			}
			else
			{
			 $newvar='';
			
			}
			if($type=='user')
			{
			  $type='District';
			
			}
			echo '<script type="text/javascript"> 
$(document).ready(function(){$("#stexpand'.$msg_id.'").oembed("'.$moredata['message'].'",{maxWidth: 400, maxHeight: 300});});
</script>
<div style="width:100%;" class="stbody" id="stbody'.$msg_id.'">
			<div class="stimg"><img src="'.SITEURLM.'images/'.$avatar.'.gif" class="big_face"/>
			</div> 
			<div class="sttext">

			<b>Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.$type.')<b/>&nbsp;&nbsp;&nbsp;<b>School:&nbsp;</b>'.$school_name.'    </b>
			<br /><b>Subject:&nbsp;'.$subject_name.'&nbsp;&nbsp;&nbspGrade:&nbsp;'.$grade_name.'</b>
			<br /><b>Post:</b>'.$message.$newvar.' 
			<div class="sttime">'.$this->time_stamp($time).' | <a href="#" class="commentopen" id="'.$msg_id.'" title="Comment">Comment </a></div> 
			<div id="stexpandbox">
			<div id="stexpand'.$msg_id.'"></div>
			</div>
			<div class="commentcontainer" id="commentload'.$msg_id.'">';
			
			if($commentsarray)
{
foreach($commentsarray as $cdata)
 {
 $com_id=$cdata['com_id'];
 $oricomment=$cdata['comment'];
 $comment=$this->tolink(htmlentities($cdata['comment'] ));
  $time=$cdata['created'];
   $username=$cdata['username'];
  $uid=$cdata['user_id'];
  $type=$cdata['type'];
  $avatar=$cdata['avatar'];
            if($type=='user')
			{
			  $type='District';
			
			}
   
 
echo '<script type="text/javascript"> 
$(document).ready(function(){$("#commentstexpand'.$com_id.'").oembed("'.$oricomment.'",{maxWidth: 400, maxHeight: 300});});
</script><div class="stcommentbody" id="stcommentbody'.$com_id.'">
<div class="stcommentimg">
<img src="'.SITEURLM.'images/'.$avatar.'.gif" class="small_face"/>
</div> 
<div class="stcommenttext">

<b>Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.$type.')</b>'.$comment.'
<div class="stcommenttime">'.$this->time_stamp($time).'</div> 
<div id="stexpandbox">
<div id="commentstexpand'.$com_id.'"></div>
</div>
</div>
</div>';

}
}


			echo '</div>
			<div class="commentupdate" style="display:none" id="commentbox'.$msg_id.'">
			<div class="stcommentimg">
			<img src="'.SITEURLM.'images/'.$this->session->userdata('avatar_id').'.gif" class="small_face"/>
			</div> 
			<div class="stcommenttext" >
			<form method="post" action="">
			<textarea name="comment" class="comment" maxlength="200"  id="ctextarea'.$msg_id.'"></textarea>
			<br />
			<input type="submit"  value=" Comment "  id="'.$msg_id.'" class="comment_button"/>
			</form>
			</div>
			</div>
			</div> 
			</div>';

			}
			
			
   
   
			echo '<div class="more'.$msg_id.'" id="morebutton" style="margin:0px 0px 0px 100px;" >
			<input type="hidden" id="last_msg" value="'.$msg_id.'">
  <a id="'.$msg_id.'" class="more2" title="Follow" href="#" style="color:#000">
			   More Posts
			   </a> </div>';
			   
			}
			else
			{
			
			  echo '<div style="text-align:center;">No More Post</div>';
			
			}
			}
	
	
	}
	
	function comment_message()
	{
	
			  if(isSet($_POST['comment']))
		{
		$comment=$_POST['comment'];
		$msg_id=$_POST['msg_id'];
		$this->load->Model('banktimemodel');
		$cdata=$this->banktimemodel->Insert_Comment($msg_id,$comment);
			
		
		if($cdata)
		{
		$com_id=$cdata['com_id'];
		$oricomment=$cdata['comment'];
		 $comment=$this->tolink(htmlentities($cdata['comment'] ));
		 $time=$cdata['created'];
		 $username=$cdata['username'];
		 $uid=$cdata['user_id'];
		 $type=$cdata['type'];
		 $avatar=$cdata['avatar'];
            if($type=='user')
			{
			  $type='District';
			
			}
		 
		echo '<script type="text/javascript"> 
$(document).ready(function(){$("#commentstexpand'.$com_id.'").oembed("'.$oricomment.'",{maxWidth: 400, maxHeight: 300});});
</script><div class="stcommentbody" id="stcommentbody'.$com_id.'">
		<div class="stcommentimg">
		<img src="'.SITEURLM.'images/'.$avatar.'.gif" class="small_face"/>
		</div> 
		<div class="stcommenttext">
		<b>Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.$type.')</b>'.$comment.'
		<div class="stcommenttime">'.$this->time_stamp($time).'</div> 
		<div id="stexpandbox">
<div id="commentstexpand'.$com_id.'"></div>
</div>
		</div>
		</div>';
		
		}
		}
	}
	
	function upload()
	{
		 
		 
		 if($_FILES['fileToUpload']['size']>0)
		 {
		 $foto_upload = new Foto_upload;	

		 $json['size'] = $_POST['MAX_FILE_SIZE'];
		
		$json['img'] = '';
        
		$foto_upload->upload_dir = BANK_TIME.WORKSHOP_FILES."banktime/";
		
		$foto_upload->foto_folder = BANK_TIME.WORKSHOP_FILES."banktime/";
		//$foto_upload->thumb_folder = $_SERVER['DOCUMENT_ROOT'].'/tor/'.MEDIA_PATH."inc/banktime/thumbs/";
		$foto_upload->extensions = array(".jpg", ".gif", ".png",".pdf",".xls",".xlsx",".jpeg",".ppt",".pptx",".doc",".docx");
		$foto_upload->language = "en";
		$foto_upload->x_max_size = 480;
		$foto_upload->y_max_size = 360;
		$foto_upload->x_max_thumb_size = 120;
		$foto_upload->y_max_thumb_size = 120;

		$foto_upload->the_temp_file = $_FILES['fileToUpload']['tmp_name'];
		
		$foto_upload->the_file = $_FILES['fileToUpload']['name'];
		$foto_upload->http_error = $_FILES['fileToUpload']['error'];
		$foto_upload->rename_file = true; 

		if ($foto_upload->upload()) {
			//$foto_upload->process_image(false, true, true, 80);
			$json['img'] = $foto_upload->file_copy;
		} 

		$json['error'] = strip_tags($foto_upload->show_error_string());
		echo json_encode($json);
		}
		else if($_FILES['fileToUpload']['error']==2)
		{
		  $json['img']='';
		  $json['error']='Max File Size is 2 MB';
		 echo json_encode($json);
		}
		else
		{
		 $json['img']='No_Image';
		 echo json_encode($json);
		}
	
	}
	
	function mediaupload()
	{
		 
		 
		 if($_FILES['mediafileToUpload']['size']>0)
		 {
		 $foto_upload = new Foto_upload;	

		 $json['size'] = $_POST['MEDIA_MAX_FILE_SIZE'];
		
		$json['img'] = '';
        
		$foto_upload->upload_dir = BANK_TIME.WORKSHOP_FILES."medialibrary/";
		
		$foto_upload->foto_folder = BANK_TIME.WORKSHOP_FILES."medialibrary/";
		$foto_upload->extensions = array(".jpg", ".gif", ".png",".pdf",".xls",".xlsx",".jpeg",".ppt",".pptx",".doc",".docx");
		$foto_upload->language = "en";
		$foto_upload->x_max_size = 480;
		$foto_upload->y_max_size = 360;
		$foto_upload->x_max_thumb_size = 120;
		$foto_upload->y_max_thumb_size = 120;

		$foto_upload->the_temp_file = $_FILES['mediafileToUpload']['tmp_name'];
		
		$foto_upload->the_file = $_FILES['mediafileToUpload']['name'];
		$foto_upload->http_error = $_FILES['mediafileToUpload']['error'];
		$foto_upload->rename_file = true; 

		if ($foto_upload->upload()) {
			//$foto_upload->process_image(false, true, true, 80);
			$json['img'] = $foto_upload->file_copy;
		} 

		$json['error'] = strip_tags($foto_upload->show_error_string());
		echo json_encode($json);
		}
		else if($_FILES['mediafileToUpload']['error']==2)
		{
		  $json['img']='';
		  $json['error']='Max File Size is 2 MB';
		 echo json_encode($json);
		}
		else
		{
		 $json['img']='';
		 $json['error']='Please Select a File To Upload';
		 echo json_encode($json);
		}
	
	}
	
	function media_upload()
	{
	
		if(isSet($_POST['school_id']))
		{			
		$school_id=$_POST['school_id'];
		$file_path=$_POST['fileupload'];
		$mediadesc=$_POST['mediadesc'];
		$parent=$_POST['parent'];
		
		$this->load->Model('banktimemodel');
		$data=$this->banktimemodel->Insert_Media($school_id,$file_path,$mediadesc,$parent);           
		}
	
	}
	function getmedia($page,$school,$search)
	{
	   $this->load->Model('banktimemodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_media_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_media_school_id',$school);
		
		}
		
		if($search!='empty')
	   {
	   $this->session->set_userdata('search_mediasearch_school_id',$search);
		
		}
		else
		{
		 $this->session->unset_userdata('search_mediasearch_school_id',$search);
		
		}
		$per_page = 12;
	   $total_records = $this->banktimemodel->getmediaCount();
		
			$status = $this->banktimemodel->getmedia($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print "<div style='margin-left:-130px;' class='htitle'>Media</div><div style='padding-left:100px;'><table class='tabcontent' cellspacing='0' border='0' cellpadding='0' id='gallery' style='width:935px;' >";
					
		
		
			$i=0;
			foreach($status as $val){
					
					$school_name=$val['school_name'];
					$username=$val['username'];
					$type=$val['type'];
					$description=$val['description'];
					if($type=='user')
					{
						$type='district';
					}
					
					$text='<br /><b>School:&nbsp;'.$school_name.'<br />    Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.ucfirst($type).')</b>';
					
					
					
					
					if($i%2==0 || $i==0)
					{
					 print '<tr>';
	
					}
					
				
			    $siteurl=SITEURLM;
				$filename=explode('.',$val['file_path']);
				$file=$filename[1];
				
			if($file=='jpg' || $file=='png' || $file=='gif' || $file=='jpeg')
			{
			  $fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'medialibrary/'.$val['file_path'].' height="75px" width="100px">';
			}
			else
			{
			  if (file_exists(WORKSHOP_FILES.'mediathumb/'.$filename[0].'.jpeg')) {
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'mediathumb/'.$filename[0].'.jpeg >';
				} else {
					if($file=='pdf')
					{
					 $commandtext='convert -thumbnail x100 '.WORKSHOP_FILES.'medialibrary/'.$val['file_path'].'[0] '.WORKSHOP_FILES.'mediathumb/'.$filename[0].'.jpeg';
					
					exec($commandtext);
					}
					else
					{
					 $commandtext='sudo -u unoconv /usr/bin/unoconv -f pdf -o '.WORKSHOP_FILES.'mediagenerated '.WORKSHOP_FILES.'medialibrary/'.$val['file_path'];
					 exec($commandtext);
					 
					  $commandtexta='convert -thumbnail x100 '.WORKSHOP_FILES.'mediagenerated/'.$filename[0].'.pdf[0] '.WORKSHOP_FILES.'mediathumb/'.$filename[0].'.jpeg ';
					exec($commandtexta);
					 
					}
					$fileas='<br /><img src='.WORKSHOP_DISPLAY_FILES.'mediathumb/'.$filename[0].'.jpeg >';
				}
			  
			  
			
			}
			$file_path=WORKSHOP_DISPLAY_FILES.'medialibrary/'.$val['file_path'];
            $newvar='<a style="color:#ffffff;" target="_blank" href='.$file_path.'>'.$fileas.'</a>'.$text;
			
			
				print '<td style="width: 500px; height: 100px;">'.$newvar.'<br />Description:'.$description.'</td>';
				
				
				$i++;
				if($i%2==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table></div>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:935px;' ><tr ><td>No  Media Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'media');
						print $pagination;	
		}
	
	}
	
	function websiteupload()
	{
		 
		 
		 if($_POST['websitefileToUpload']!='')
		 {
		
			$json['img'] = $_POST['websitefileToUpload'];		
			echo json_encode($json);
		}		
		else
		{
		 $json['img']='';
		 $json['error']='Please Enter a Link  To Upload';
		 echo json_encode($json);
		}
	
	}
	
	function website_upload()
	{
	
		if(isSet($_POST['school_id']))
		{			
		$school_id=$_POST['school_id'];
		$file_path=$_POST['fileupload'];
		$websitedesc=$_POST['websitedesc'];
		
		$this->load->Model('banktimemodel');
		$data=$this->banktimemodel->Insert_website($school_id,$file_path,$websitedesc);           
		}
	
	}
	function getwebsite($page,$school,$search)
	{
	   $this->load->Model('banktimemodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_website_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_website_school_id',$school);
		
		}
		
		if($search!='empty')
	   {
	   $this->session->set_userdata('search_websitesearch_school_id',$search);
		
		}
		else
		{
		 $this->session->unset_userdata('search_websitesearch_school_id',$search);
		
		}
		$per_page = 12;
	   $total_records = $this->banktimemodel->getwebsiteCount();
		
			$status = $this->banktimemodel->getwebsite($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print "<div style='margin-left:-130px;' class='htitle'>Website Library</div><div style='padding-left:100px;'><table class='tabcontent' cellspacing='0' border='0' cellpadding='0' id='gallery' style='width:935px;' >";
					
		
		
			$i=0;
			foreach($status as $val){
					
					$school_name=$val['school_name'];
					$username=$val['username'];
					$type=$val['type'];
					$description=$val['description'];
					
					if($type=='user')
					{
						$type='district';
					}
					
					$text='<br /><b>School:&nbsp;'.$school_name.'<br />    Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.ucfirst($type).')</b>';
					
					
					
					if($i%2==0 || $i==0)
					{
					 print '<tr>';
	
					}
					
				
			    
				$file=$val['link'];
			
			
            $newvar='<a style="color:#02AAD2;" target="_blank" href='.$file.'>'.$file.'</a>'.$text;
			
			
				print '<td style="width: 500px; height: 100px;">'.$newvar.'<br/>Description:'.$description.'</td>';
				
				
				$i++;
				if($i%2==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table></div>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'website');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:935px;' ><tr ><td>No  Website Link Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'website');
						print $pagination;	
		}
	
	}
	function videoupload()
	{
		 
		 
		 if($_POST['videofileToUpload']!='')
		 {
		
			$json['img'] = $_POST['videofileToUpload'];		
			echo json_encode($json);
		}		
		else
		{
		 $json['img']='';
		 $json['error']='Please Enter a Link  To Upload';
		 echo json_encode($json);
		}
	
	}
	
	function video_upload()
	{
	
		if(isSet($_POST['school_id']))
		{			
		$school_id=$_POST['school_id'];
		$file_path=$_POST['fileupload'];
		$parent=$_POST['parent'];
		
		$this->load->Model('banktimemodel');
		$data=$this->banktimemodel->Insert_video($school_id,$file_path,$parent);           
		}
	
	}
	function getvideo($page,$school,$search)
	{
	   $this->load->Model('banktimemodel');
	   if($school!='empty')
	   {
	   $this->session->set_userdata('search_video_school_id',$school);
		
		}
		else
		{
		 $this->session->unset_userdata('search_video_school_id',$school);
		
		}
		
		if($search!='empty')
	   {
	   $this->session->set_userdata('search_videosearch_school_id',$search);
		
		}
		else
		{
		 $this->session->unset_userdata('search_videosearch_school_id',$search);
		
		}
		
		$per_page = 12;
	   $total_records = $this->banktimemodel->getvideoCount();
		
			$status = $this->banktimemodel->getvideo($page, $per_page);
		
		
		
		if($status!=FALSE){
			
			
			print '<div  style="margin-left:-130px;" class="htitle">video Library</div><div style="padding-left:100px;"><table class="tabcontent" cellspacing="0" border="0" cellpadding="0" id="gallery" style="width:935px;" >';
					
		
		
			$i=0;
			foreach($status as $val){
					
					print '<script type="text/javascript"> 
$(document).ready(function(){$("#videoexpand'.$val['video_library_id'].'").oembed("'.$val['link'].'",{maxWidth: 100, maxHeight: 75});});
</script>';
					$school_name=$val['school_name'];
					$username=$val['username'];
					$type=$val['type'];
					if($type=='user')
					{
						$type='district';
					}
					
					$text='<br /><b>School:&nbsp;'.$school_name.'<br />    Name:&nbsp;'.$username.'&nbsp;(Type:&nbsp;'.ucfirst($type).')</b>';
					
					
					if($i%2==0 || $i==0)
					{
					 print '<tr>';
	
					}
					
				
			    
				$file=$val['link'];
			
			
            $newvar=' '.$file.' '.$text;
			
			
				print '<td style="width: 500px; height: 100px;"><div id="videoexpand'.$val['video_library_id'].'"></div><br />'.$newvar.'</td>';
				
				
				$i++;
				if($i%2==0)
					{
					 print '</tr>';
	
					}
					
				}
				print '</tr></table></div>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'video');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' style='width:935px;' ><tr ><td>No  video Link Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'video');
						print $pagination;	
		}
	
	}
	
	function time_stamp($session_time) 
{ 
 
$time_difference = time() - $session_time ; 
$seconds = $time_difference ; 
$minutes = round($time_difference / 60 );
$hours = round($time_difference / 3600 ); 
$days = round($time_difference / 86400 ); 
$weeks = round($time_difference / 604800 ); 
$months = round($time_difference / 2419200 ); 
$years = round($time_difference / 29030400 ); 

if($seconds <= 60)
{
echo"$seconds seconds ago"; 
}
else if($minutes <=60)
{
   if($minutes==1)
   {
     echo"one minute ago"; 
    }
   else
   {
   echo"$minutes minutes ago"; 
   }
}
else if($hours <=24)
{
   if($hours==1)
   {
   echo"one hour ago";
   }
  else
  {
  echo"$hours hours ago";
  }
}
else if($days <=7)
{
  if($days==1)
   {
   echo"one day ago";
   }
  else
  {
  echo"$days days ago";
  }


  
}
else if($weeks <=4)
{
  if($weeks==1)
   {
   echo"one week ago";
   }
  else
  {
  echo"$weeks weeks ago";
  }
 }
else if($months <=12)
{
   if($months==1)
   {
   echo"one month ago";
   }
  else
  {
  echo"$months months ago";
  }
 
   
}

else
{
if($years==1)
   {
   echo"one year ago";
   }
  else
  {
  echo"$years years ago";
  }


}
 


}

function tolink($text){
        $text = html_entity_decode($text);
        
        $text = " ".$text;
        
        $text = preg_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)','<a href="\\1">\\1</a>', $text);
        $text = preg_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)','<a href="\\1">\\1</a>', $text);
        //$text = preg_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)','\\1<a href="http://\\2">\\2</a>', $text);
        
        $text = preg_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})','<a href="mailto:\\1">\\1</a>', $text);
        
        return $text;
}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>&nbsp;&nbsp;</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>&nbsp;&nbsp;</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
}