<?php
/**
 * observer Controller.
 *
 */
class Observerview extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_observer()==false && $this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index");
		}
		
	}
	
	function addcomments()
	{
	    $data['status']=0;
	   $pdata=$this->input->post('pdata');
	   //print_r($pdata);
	  // exit;
	   if(!empty($pdata))
	   {
	    if(isset($pdata['plan_id']) && isset($pdata['comments'])  )
		 {
		$this->load->Model('lessonplanmodel');
		$status = $this->lessonplanmodel->addcomments($pdata['plan_id'],$pdata['comments']);
		if($status==true)
		{
		  $data['status']=1;
		
		}
		}
		}
		echo json_encode($data);
		exit;
		
	
	
	
	}
	
	function comments()
	{
	   $this->load->Model('schoolmodel');
	   $data['school']=$this->schoolmodel->getschoolbyobserver();
	  if($data['school']!=false)	
	   {
		$this->load->Model('teachermodel');
	    $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
		
	   }
	   else
	   {
	     $data['teacher']=false;
		 
	   
	   }
	   
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/comments',$data);
	
	}
	
	function getcommentssasigned($page,$school_id,$teacher_id)
	{
	
	  $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('goalplanmodel');
		$total_records = $this->goalplanmodel->getcommentssasignedcount($school_id,$teacher_id);
		
			$status = $this->goalplanmodel->getcommentssasigned($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Goal Plans</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Review Plans</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr  class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				
				<td nowrap><a href="observerview/commentsview/'.$val['teacher_id'].'">View</a></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Review Plans</td></tr>
			<tr><td valign='top' colspan='10'>No  Teacher Goal Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
		}
		
	
	
	
	
	
	}
	function commentsview($teacher_id)
	{
	
	    if($this->input->post('year'))
		{
		
			$year=$this->input->post('year');
		}
		else
		{
		  $year=date('Y');
		
		}
		$data['year']=$year;
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		$this->load->Model('goalplanmodel');
		$data['goalplans']=$this->goalplanmodel->getallplans();
		//print_r($data['goalplans']);
		//$data['teacherplans']=$this->goalplanmodel->getallplansbyteacher($year,$teacher_id);
		$data['teacherplans']=$this->goalplanmodel->getteacherplanid($data['goalplans'][0]['goal_plan_id'],$teacher_id,$year);
		$data['observer_desc']=$this->goalplanmodel->getallplansdescbyteacher($year,$teacher_id);
		//print_r($data['teacherplans']);
		if($data['teacherplans']!=false)
		{
		
		$data['comments']=$this->goalplanmodel->getcommentsbyplanid($data['teacherplans'][0]['teacher_plan_id']);
		//print_r($data['comments']);
		}
		else
		{
			$data['comments']=false;
		
		}
		$data['teacher_id']=$teacher_id;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('observerview/commentsview',$data);
	
	
	}
	function lessoncomments()
	{
	  if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
	  $this->load->Model('schoolmodel');
	   $data['school']=$this->schoolmodel->getschoolbyobserver();
	  if($data['school']!=false)	
	   {
		$this->load->Model('teachermodel');
	    $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
		
	   }
	   else
	   {
	     $data['teacher']=false;
		 
	   
	   }
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/lessoncomments',$data);
	
	}
	
	function schedule()
	{
	  $this->load->Model('schoolmodel');
	   $data['school']=$this->schoolmodel->getschoolbyobserver();
	  /*if($data['school']!=false)	
	   {
		$this->load->Model('teachermodel');
	    $data['teacher']=$this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
		
	   }
	   else
	   {
	     $data['teacher']=false;
		 
	   
	   }*/
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/schedule',$data);
	
	}
	
	function scheduleview($teacher_id=false)
	{
	  if($teacher_id!=false)
	  {
		$this->session->set_userdata("teacher_observer_id",$teacher_id);
	  }

     $teacher_id=$this->session->userdata("teacher_observer_id");	  
	 
	 
	    $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
	  if($this->input->post('selectdate'))
	   {
	      $data['selectdate']=$this->input->post('selectdate');
	   
	   }
	   else
	   {
	          $data['selectdate']=date('m-d-Y');
	   
	   }
	   $this->load->Model('schedulemodel');
	  
		
	 $datevar=explode('-',$data['selectdate']);
		$sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	   $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	   $data['getscheduleplans']=$this->schedulemodel->getschedulesbyteacher($teacher_id,$data['dates'][0]['date'],$data['dates'][6]['date']);
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/scheduleview',$data);
	
	
	
	
	
	
	}
	
	function lessoncommentssasigned($page,$school_id,$teacher_id)
	{
	
	  $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('lessonplanmodel');
		$total_records = $this->lessonplanmodel->getcommentssasignedcount($school_id,$teacher_id);
		
			$status = $this->lessonplanmodel->getcommentssasigned($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Lesson Plans</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Review Plans</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr  class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				
				<td nowrap><a href="observerview/lessoncommentsview/'.$val['teacher_id'].'">View</a></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Review Plans</td></tr>
			<tr><td valign='top' colspan='10'>No  Lesson Plans Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
		}
		
	
	
	
	
	
	}
	
	function schedulecommentssasigned($page,$school_id,$teacher_id)
	{
	
	  $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('schedulemodel');
		$total_records = $this->schedulemodel->getschedulesasignedcount($school_id,$teacher_id);
		
			$status = $this->schedulemodel->getschedulesasigned($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Tasks</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr  class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				
				<td nowrap><a href="observerview/scheduleview/'.$val['teacher_id'].'">View/Add</a></td>		</tr>';
				$i++;
				}
				print '</table><table>	<tr>
  <td align="right" colspan="5"><input type="button" name="Add" id="Add" value="Add Multiple Tasks" onclick="addplanall('.$school_id.')"></td>
  </tr>
</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No  Tasks Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'comments');
						print $pagination;	
		}
		
	
	
	
	
	
	}
	
	function lessoncommentsview($teacher_id=false)
	{
	     if($teacher_id!=false)
		 {
		    $data['teacher_id']=$teacher_id;
		 
		 }
		 else
		 {  
		     $teacher_id=$this->input->post('teacher_id');
		      $data['teacher_id']=$teacher_id;
		 }
	    $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		if($this->input->post('selectdate'))
	   {
	      $data['selectdate']=$this->input->post('selectdate');
	   
	   }
	   else
	   {
	          $data['selectdate']=date('m-d-Y');
	   
	   }
	    $this->load->Model('lessonplanmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		//echo '<pre>';
		//print_r($data['getlessonplans']);
		//exit;
	   $datevar=explode('-',$data['selectdate']);
		$sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	   $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	  
	  foreach($data['dates'] as $key=>$val)
	  {
	    $data['dates'][$key]['week']=$val['week'];
		$cdate=$val['date'];
		$cdate1=explode('-',$cdate);
		$data['dates'][$key]['date']=$cdate1[1].'-'.$cdate1[2].'-'.$cdate1[0];
	  
	  }
	  $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/lessoncommentsview',$data);
	
	}
	function printweekview($teacher_id,$sedate)
	{
	     
	    $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		
	          $data['selectdate']=$sedate;
	   
	   
	    $this->load->Model('lessonplanmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		//echo '<pre>';
		//print_r($data['getlessonplans']);
		//exit;
	   $datevar=explode('-',$data['selectdate']);
		$sel=$datevar[1].'-'.$datevar[0].'-'.$datevar[2];
	  $data['dates']=$this->week_from_monday($sel);
	   $fromdate=$data['dates'][0]['date'];
	  $todate=$data['dates'][6]['date'];
	  $fromdate1=explode('-',$fromdate);
	  $data['fromdate']=$fromdate1[1].'-'.$fromdate1[2].'-'.$fromdate1[0];
	  $todate1=explode('-',$todate);
	  $data['todate']=$todate1[1].'-'.$todate1[2].'-'.$todate1[0];
	  
	  foreach($data['dates'] as $key=>$val)
	  {
	    $data['dates'][$key]['week']=$val['week'];
		$cdate=$val['date'];
		$cdate1=explode('-',$cdate);
		$data['dates'][$key]['date']=$cdate1[1].'-'.$cdate1[2].'-'.$cdate1[0];
	  
	  }
	  $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/printdayview',$data);
	
	}
	
	function dayview($teacher_id=false)
	{
	     if($teacher_id!=false)
		 {
		    $data['teacher_id']=$teacher_id;
		 
		 }
		 else
		 {  
		     $teacher_id=$this->input->post('teacher_id');
		      $data['teacher_id']=$teacher_id;
		 }
	    $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		if($this->input->post('selectdate'))
	   {
	      $data['selectdate']=$this->input->post('selectdate');
	   
	   }
	   else
	   {
	          $data['selectdate']=date('m-d-Y');
	   
	   }
	    $this->load->Model('lessonplanmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		//echo '<pre>';
		//print_r($data['getlessonplans']);
		//exit;
	   $datevar=explode('-',$data['selectdate']);
		$fromdate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
		$todate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
	  
	    $data['fromdate']=$data['selectdate'];	  
	    $data['todate']=$data['selectdate'];
	  
	    
		$weekday = date('l', strtotime($fromdate));
		
	    $data['dates'][$data['selectdate']]['week']=$weekday;		
		$data['dates'][$data['selectdate']]['date']=$data['selectdate'];
	  $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/dayview',$data);
	
	}
	function printdayview($teacher_id,$sedate)
	{
	     
	    $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
			      $data['selectdate']=$sedate;
	   
	   
	    $this->load->Model('lessonplanmodel');
		$data['lessonplans']=$this->lessonplanmodel->getalllessonplansnotsubject();
		
		$data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssubnotsubject();
		//echo '<pre>';
		//print_r($data['getlessonplans']);
		//exit;
	    $datevar=explode('-',$data['selectdate']);
		$fromdate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
		$todate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
	  
	    $data['fromdate']=$data['selectdate'];	  
	    $data['todate']=$data['selectdate'];
	  
	    
		$weekday = date('l', strtotime($fromdate));
		
	    $data['dates'][$data['selectdate']]['week']=$weekday;		
		$data['dates'][$data['selectdate']]['date']=$data['selectdate'];
	  $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate);
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/printdayview',$data);
	
	}
	
	function assign($message=false)
	{
	  
	  if($message!=false)
	  {
		$data['message']=$message;
	  
	  }
	  $this->load->Model('memorandummodel');
	  
		//$this->load->Model('teachermodel');
	   //$data['teacher']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	  
	   $data['memorandum']=$this->memorandummodel->getapprovedmemorandums($this->session->userdata("observer_id"),$this->session->userdata("district_id"));
	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assign',$data);
	
	}
	function assignedmemoteacher()
	{
	  $this->load->Model('teachermodel');
	  $data['teacher']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	  $memorandum_id=$this->input->post('memorandum');
	  $this->load->Model('memorandummodel');
	  $memo=$this->memorandummodel->getapprovedmemobyid($memorandum_id);
	  $data['memo']=$memo[0]['text'];
	  $data['subject']=$memo[0]['subject'];
	  $data['approve_id']=$memo[0]['approve_id'];
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/memosubmit',$data);
	
	
	}
	
	/*function assignteacher($teacher_id=false,$memorandum_id=false)
	{
	 
	    if($teacher_id==false)
		{			
		$teacher_id=$this->input->post('teacher');
		$memorandum_id=$this->input->post('memorandum');
		$data['message']='';
		}
		else
		{
		 $data['message']='Failed Please Try Again';
		}
	  
	   if(!empty($teacher_id) && !empty($memorandum_id) )
	   {
	     
		 if(isset($teacher_id) && isset($memorandum_id)  )
		 {
		    
			$this->load->Model('teachermodel');
			$teacher=$this->teachermodel->getteacherById($teacher_id);
			$data['teacher_id']=$teacher[0]['teacher_id'];
			$data['teachername']=$teacher[0]['firstname'].' '.$teacher[0]['lastname'];
			$this->load->Model('memorandummodel');
		    $memo=$this->memorandummodel->getmemorandumById($memorandum_id);
			$data['memo']=$memo[0]['text'];
			$data['memo_id']=$memo[0]['memorandum_id'];
			
		 }
	   }
	   
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assignteacher',$data);

	
	}*/
	
	function memorandum_edit()
	{
	
	  $memorandum_id=$this->input->post('memorandum');
		$this->load->Model('teachermodel');
	  $data['teacher']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	  
	   if(!empty($memorandum_id) )
	   {
	     
		 if(isset($memorandum_id)  )
		 {
		    
			
			$this->load->Model('memorandummodel');
		    if($this->input->post('submit')=='Submit')
			{
			$memo=$this->memorandummodel->getmemorandumById($memorandum_id);
			}
			else
			{
			$memo=$this->memorandummodel->getsavememorandumById($memorandum_id);
			
			}
			$data['memo']=$memo[0]['text'];
			$data['subject']=$memo[0]['subject'];
			$data['memo_id']=$memo[0]['memorandum_id'];
			$this->load->Model('districtmodel');	  
			$data['district']=$this->districtmodel->getdist_userByDistrictId($this->session->userdata("district_id"));	 
			
		 }
	   }
	   
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assignteacher',$data);
	
	
	
	
	}
	
	function assignedteacher()
	{
	
	
	   $status=false;
	   $teacher_id=$this->input->post('teacher');
		$approve_id=$this->input->post('approve_id');
		$text=$this->input->post('text');
		
	  
	   if(isset($teacher_id) && isset($approve_id) && isset($text) )
		 {
	   if(!empty($teacher_id) && !empty($approve_id) && !empty($text) )
	   {
	     
		 
		    
			
			$this->load->Model('memorandummodel');
		    
			$status=$this->memorandummodel->assignteacher();
			$this->memorandummodel->memoapprovesubmit();
			
		 }
	   }
	   
	   if($status==true)
	   {
	      $this->assigned();
	  }
	  else
	  { 
	     $this->assign('Failed Please Try Again');
	  }
	
	
	
	
	}
	
	function memorandum_approve()
	{
	
	
	   $status=false;
	   $district=$this->input->post('district');
		$memorandum_id=$this->input->post('memorandum');
		$text=$this->input->post('text');
		$subject=$this->input->post('subject');
	   
	   if(!empty($memorandum_id) && !empty($text) && !empty($subject) )
	   {
	     
		 if( isset($memorandum_id) && isset($text)  && isset($subject))
		 {
		    $this->load->Model('memorandummodel');
			
			if($this->input->post('submit')=='Save')
			{
			
			
			$status=$this->memorandummodel->saveobserver();
			}
			else if($this->input->post('submit')=='Review')
			{
			
			
			$status=$this->memorandummodel->assigndistrict();
			}
			else if($this->input->post('submit')=='Submit')
			{
			   $status=$this->memorandummodel->assignteacherdirect();
			
			}
			
		 }
	   }
	   
	   if($status==true)
	   {
	      $this->memorandums('Submitted Successfully');
	  }
	  else
	  { 
	     $this->memorandums('Failed Please Try Again');
	  }
	
	
	
	
	}
	
	function assigned()
	{
	  $this->load->Model('schoolmodel');
	  $this->load->Model('memorandummodel');
	  $this->load->Model('teachermodel');
	  $data['teacher']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/assigned',$data);
	
	}
	
	function memorandums($message=false)
	{
	  if($message!=false)
	  {
	    $data['message']=$message;
	  
	  }
	  $this->load->Model('memorandummodel');
	  //$this->load->Model('districtmodel');
	  $data['memorandum']=$this->memorandummodel->getallmemorandums($this->session->userdata("district_id"));
      //$data['district']=$this->districtmodel->getdist_userByDistrictId($this->session->userdata("district_id"));	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/memorandums',$data);
	  
	
	}
	function savedmemorandums($message=false)
	{
	  if($message!=false)
	  {
	    $data['message']=$message;
	  
	  }
	  $this->load->Model('memorandummodel');
	  //$this->load->Model('districtmodel');
	  $data['memorandum']=$this->memorandummodel->getsavedmemorandums($this->session->userdata("observer_id"));
      //$data['district']=$this->districtmodel->getdist_userByDistrictId($this->session->userdata("district_id"));	  
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('memorandum/savememorandums',$data);
	  
	
	}
	
	
	
	
	function answer()
	{
		if($this->session->userdata('TE')==0)
			{
				redirect("index");
			}
	 $this->load->Model('schoolmodel');
	 $this->load->Model('teachermodel');
	 $data['teacher']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	 $data['view_path']=$this->config->item('view_path');
	 $this->load->view('observationplan/answer',$data);
	
	
	
	
	}
	
	function pdarchive()
	{
	 $data['idname']='tools';
	 $this->load->Model('schoolmodel');
	 $this->load->Model('teachermodel');
	 $data['teacher']=$this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
	 $data['view_path']=$this->config->item('view_path');
	 $this->load->view('observationgroup/pdarchive',$data);
	
	
	
	
	}
	
	function getpdarchive($page,$school_id,$teacher_id)
	{
	
	 $this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('observationgroupmodel');
		$total_records = $this->observationgroupmodel->getpdarchivecount($school_id,$teacher_id);
		
			$status = $this->observationgroupmodel->getpdarchive($page, $per_page,$school_id,$teacher_id);
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div align='center' class='htitle'>Professional Development</div>
		<table align='center' class='table table-striped table-hover table-bordered' id='editable-grade-day' style='width:50%;'>
		<tr>
		<td class='no-sorting sorting'>School</td>
		<td  class='no-sorting sorting'>Teacher</td>
		<td  class='no-sorting sorting'>Archived</td>
		</tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr  class="'.$c.'" >';
			
				print '<td>'.$val['school_name'].'</td>
				<td>'.$val['firstname'].' '.$val['lastname'].'</td>
				
				
				<td nowrap><a class="btn btn-purple" href="observerview/getpdarchived/'.$val['teacher_id'].'">View</a></td>
				</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'pdarchive');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>School</td><td>Teacher</td><td>Archived</td></tr>
			<tr><td valign='top' colspan='10'>No Data Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'pdarchive');
						print $pagination;	
		}
		
	
	
	
	}
	function answerview($teacher_id,$message=false)
	{
	
		$data['teacher_id']=$teacher_id;
		
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
		if($message!=false)
		{
			$data['message']=$message;
		
		}
		else
		{
		 $data['message']='';
		
		}
	
		$this->load->Model('observationplanmodel');
		$data['observationplan']=$this->observationplanmodel->getallplans();
		$data['observationplan_ans']=$this->observationplanmodel->getallanswerplans($teacher_id);
		if($data['observationplan']!=false)
		{
		  if($data['observationplan_ans']!=false)
		  {
		    foreach($data['observationplan'] as $key=>$plan)
			{
            foreach($data['observationplan_ans'] as $ans)
			{
			  if($plan['observation_plan_id']==$ans['observation_plan_id'] && $ans['archived']=='' )
			  {
			    $data['observationplan'][$key]['answer']=$ans['answer']; 
				 $data['observationplan'][$key]['observation_ans_id']=$ans['observation_ans_id']; 
				  $data['observationplan'][$key]['comment']=$ans['comment']; 
			  
			  
			  }
			  
			
			}

			}	
		  
		  
		  
		  
		  
		  }
		  
		
		
		
		}
		//print_r($data['observationplan']);
		$data['view_path']=$this->config->item('view_path');
	 $this->load->view('observationplan/answerview',$data);
		
	
	
	
	
	
	}
	function commentssave()
	{
	 $teacher_id=$this->input->post('teacher_id');
	 if($this->input->post('submit')=='Submit')
	 {
	 $this->load->Model('observationplanmodel');
	 $status=$this->observationplanmodel->commentssave();
	 $message="Comment Saved Sucessfully";
	 }
	 else if($this->input->post('submit')=='Archive')
	 {
	    if($this->input->post('an')==0)
		{
		$this->load->Model('observationplanmodel');
	 $status=$this->observationplanmodel->archived();
	 if($status==true)
	 {
		$message="Archived Sucessfully";
	  }
	  else
	  {
         $message="Archived Twice a Day For same Teacher Is Not Possible";
	  }	
	  }
	  else
	  {
	    $message="Please Ask Teacher To Answer";
	  
	  }
	 }
	 $this->answerview($teacher_id,$message);
	
	
	
	}
	
	function archiveprof()
	{
	
	 $data['status']=0;
	 $pdata=$this->input->post('pdata');
	 if(!empty($pdata))
	 {
	 if(isset($pdata['teacher'])  )
		 {
		    $this->load->Model('observationgroupmodel');
		    $status=$this->observationgroupmodel->archived();
			$data['status']=$status;
		 
		}
	    
	   
	}
	  
	echo json_encode($data);
	exit;

	 
	}
	
		function add_plan()
	{
	
	   
		
		$this->load->Model('schedulemodel');
		//$pstatus=$this->schedulemodel->check_plan_exists();
		$pstatus=1;
	if($pstatus==1)
		 {
		$status=$this->schedulemodel->add_plan();
		if($status!=0){
		       $data['message']="plan added Sucessfully" ;
			   $data['status']=1 ;
			   $data['date']=$this->input->post('date') ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Already Time is Scheduled" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="End Time Can not be Greater/Equal Than Start Time" ;
		    $data['status']=0 ;
			
		
		}
		else if($pstatus==3)
		{
			$data['message']="Scheduleing Past Dates Not Available" ;
		    $data['status']=0 ;
			
		
		}
		
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	
	function getcomments($id)
	{
	
	
	  $this->load->Model('lessonplanmodel');
	 $status=$this->lessonplanmodel->getcomments($id);
	 if($status[0]['comments']!='')
	 {
	 $data['status']=$status[0]['comments'];
	  }
	  else
	  {
	    $data['status']='';
	  }
	  echo json_encode($data);
		exit;	
	
	
	}
	function getcommentsbyid($id)
	{
	
	
	  $this->load->Model('lessonplanmodel');
	 $status=$this->lessonplanmodel->getcomments($id);
	  if($status[0]['comments']=='')
	  {
	    echo "<b>Observer:</b> No Comments Found.";
	  }
	  else
	  {
		echo '<b>Observer:</b> '.$status[0]['comments'];
	  }
	  if($status[0]['teacher_comments']=='')
	  {
	    echo "<br /><b>Teacher:</b> No Comments Found.";
	  }
	  else
	  {
		echo '<br /><b>Teacher:</b> '.$status[0]['teacher_comments'];
	  }	  
	  
	
	
	}
	
	function profdev()
	{
		$data['idname']='tools';
	  $this->load->Model('observationgroupmodel');
	  $this->load->Model('teachermodel');
	   $data['teachers']=$this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
	  $data['prof_groups']=$this->observationgroupmodel->getvideosbydistrictid($this->session->userdata('district_id'));
	  $data['article_groups']=$this->observationgroupmodel->getarticlesbydistrictid($this->session->userdata('district_id'));
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/profdev',$data);
	
	}
	
	function getvideosbyteacher($teacher_id)
	{
	  $this->load->Model('observationgroupmodel');
	  $data['video']=$this->observationgroupmodel->getvideosbyteacher($teacher_id);
	  echo json_encode($data);
	  exit;
	
	}
	
	function getarticlesbyteacher($teacher_id)
	{
	  $this->load->Model('observationgroupmodel');
	  $data['article']=$this->observationgroupmodel->getarticlesbyteacher($teacher_id);
	  echo json_encode($data);
	  exit;
	
	}
	
	function assignvideo()
	{
	   $data['status']=0;
	  
	  
	  
	  
	    $pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	    if(isset($pdata['profid']) && isset($pdata['teacher'])  )
		 {
		    $this->load->Model('observationgroupmodel');
		    $status=$this->observationgroupmodel->assignvideo();
			$data['status']=$status;
		 
		 }
	    
	   
	   }
	  
	    
	  
	 echo json_encode($data);
		exit;
	
	
	
	}
	function assignarticle()
	{
	   $data['status']=0;
	  
	  
	  
	  
	    $pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	    if(isset($pdata['profid']) && isset($pdata['teacher'])  )
		 {
		    $this->load->Model('observationgroupmodel');
		    $status=$this->observationgroupmodel->assignarticle();
			$data['status']=$status;
		 
		 }
	    
	   
	   }
	  
	    
	  
	 echo json_encode($data);
		exit;
	
	
	
	}
	
	function getarchived($teacher_id)
	{
	   $this->session->set_userdata('teacher_archive',$teacher_id);
	
	   redirect('observerview/archived');
	
	}
	
	function archived()
	{
	  $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($this->session->userdata('teacher_archive'));
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
	  $this->load->Model('observationplanmodel');
	  //Pagination Code Start
		$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/observerview/archived/';
		$config['total_rows'] = $this->observationplanmodel->getarchivedCount();
		
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		}
		//Pagination Code End

	  
	 
	  $data['archived']=$this->observationplanmodel->getarchivedprojects($start,$config['per_page']);
	  //print_r($data['archived']);
	  //exit;
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/archived',$data);
	
	
	}
	function getpdarchived($teacher_id)
	{
	   $this->session->set_userdata('teacher_archive',$teacher_id);
	
	   redirect('observerview/pdarchived');
	
	}
	
	function pdarchived()
	{
		 $data['idname']='tools';
	  $this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getteacherById($this->session->userdata('teacher_archive'));
		if($data['teacher']!=false)
		{
		  $data['teacher_name']=$data['teacher'][0]['firstname'].' '.$data['teacher'][0]['lastname'];
		
		
		}
		else
		{ 
		 $data['teacher_name']='';
		
		}
	  $this->load->Model('observationgroupmodel');
	  //Pagination Code Start
		/*$this->load->Model('utilmodel');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/observerview/pdarchived/';
		$config['total_rows'] = $this->observationgroupmodel->getpdarchivedCount();
		
		$config['per_page'] = $this->utilmodel->get_recperpage();
		$config['num_links'] = $this->utilmodel->get_paginationlinks();
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		$start=$this->uri->segment(3);
		if(trim($start)==""){
			$start = 0;
		}
		*/
		//Pagination Code End

	  
	 
	  $data['archived']=$this->observationgroupmodel->getpdarchivedprojects();
	  //print_r($data['archived']);
	  //exit;
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observerview/pdarchived',$data);
	
	
	}
	function add_desc()
	{
	  
	  $data['status']=0;
	  
	  
	  
	  
	    $pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	    if(isset($pdata['id']) && isset($pdata['desc'])  )
		 {
		    $this->load->Model('goalplanmodel');
		    $status=$this->goalplanmodel->add_desc();
			$data['status']=$status;
		 
		 }
	    
	   
	   }
	  
	    
	  
	 echo json_encode($data);
		exit;
	}
	
	function goalplans()
	{
		if($this->session->userdata('LP')==0)
			{
				redirect("index");
			
			}
		$data['view_path']=$this->config->item('view_path');
	     $this->load->view('goalplan/observer_desc',$data);
	
	
	
	
	
	
	}
	function getplaninfo($plan_id)
	{
		if(!empty($plan_id))
	  {
		$this->load->Model('goalplanmodel');
		$data['goalplan']=$this->goalplanmodel->getplanById($plan_id);
		$data['goalplan']=$data['goalplan'][0];
		echo json_encode($data);
		exit;
	  }
	
	
	}
	function update_plan()
	{
	$this->load->Model('goalplanmodel');
			
	   $status=$this->goalplanmodel->update_observer_plan();
		if($status==true){
		       $data['message']="Tab Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		
		echo json_encode($data);
		exit;		
	}

	function getgoalplans()
	{
	
	    
		
		
		$this->load->Model('goalplanmodel');
		
		
			$status = $this->goalplanmodel->getallplans();
		
		
		
		if($status!=FALSE){
		
		
		
		print "<div class='htitle'>Periodic Goal Setting</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['goal_plan_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['tab'].'</td>
				
				<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="planedit('.$val['goal_plan_id'].')"></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Tabs</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Periodic goal Plans Found.</td></tr></table>
			";
			
			
		}
		
	
	
	
	
	}
	
	function profileimage($msg=false)
	{
	  
	  if($msg==false)
	  {
	     $data['message']='';
	  
	  
	  }
	  else
	  {
	  
	    $data['message']=$msg;
	  
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('observer/profileimage',$data);
	
	
	
	
	}
	
	function change()
	{
	 if($this->session->userdata('login_type')=='user')
	  {
	     $this->load->Model('observermodel');
		$status= $this->observermodel->changeDistImage();
	  
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
         $this->load->Model('observermodel');
		$status= $this->observermodel->changeObserverImage();
	  
	  }
	  if($status==true)
	  {
        $msg='Changed Sucessfully';
	    $this->session->set_userdata('avatar_id',$this->input->post('avatar'));
	  }
	  else
	  {
	    $msg='Failed Please Try Again';
	  
	  }
	   $this->profileimage($msg);
	
	
	}
	function getother($id)
	{
	
	
	  $this->load->Model('lessonplanmodel');
	 $status=$this->lessonplanmodel->getother($id);
	  
		echo '<b>Standard:</b> '.$status[0]['standard'];
		echo '<br/>';
	  	echo '<b>Differentiated Instruction:</b> '.$status[0]['diff_instruction'];  
	  
	
	
	}
	
	function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    
	for($i=0; $i<7; $i++)
    {
        $dates[$i]['date'] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
		if($i==0)
		{
			$dates[$i]['week'] = 'Monday';
		}
		if($i==1)
		{
			$dates[$i]['week'] = 'Tuesday';
		}
		if($i==2)
		{
			$dates[$i]['week'] = 'Wednesday';
		}
		if($i==3)
		{
			$dates[$i]['week'] = 'Thursday';
		}
		if($i==4)
		{
			$dates[$i]['week'] = 'Friday';
		}
		if($i==5)
		{
			$dates[$i]['week'] = 'Saturday';
		}
		if($i==6)
		{
			$dates[$i]['week'] = 'Sunday';
		}	
    }

    return $dates;
}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active '>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}