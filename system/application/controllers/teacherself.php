<?php

$view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');
include($view_path . 'inc/class/pData.class.php');
include($view_path . 'inc/class/pDraw.class.php');
include($view_path . 'inc/class/pImage.class.php');

class Teacherself extends MY_Auth {

    function __Construct() {
        parent::Controller();
        if ($this->is_teacher() == false && $this->is_observer() == false && $this->is_parent() == false && $this->is_user() == false) {
            //These functions are available only to schools - So redirect to the login page
            redirect("index");
        }
    }

    function index() {

        $data['idname'] = 'implementation';
        if ($this->session->userdata('TE') == 0) {
            redirect("index");
        }

        if ($this->is_teacher() == true || $this->is_observer() == true || $this->is_user() == true) {

            $data['view_path'] = $this->config->item('view_path');
            $this->load->Model('teachermodel');
            $this->load->Model('schoolmodel');
            $this->load->Model('observermodel');

            $data['subjects'] = $this->schoolmodel->getallsubjects();

            $data['grades'] = $this->schoolmodel->getallgrades();

            if ($this->session->userdata('login_type') == 'teacher') {
                $data['teachers'] = $this->teachermodel->getteacherById($this->session->userdata("teacher_id"));

            } elseif ($this->session->userdata('login_type') == 'observer') {
                $this->load->Model('teachermodel');
                $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            } else if ($this->session->userdata('login_type') == 'user') {

                $data['school'] = $this->schoolmodel->getschoolbydistrict();

                if ($data['school'] != false) {
                    $this->load->Model('teachermodel');

                    $data['teachers'] = $this->teachermodel->getTeachersBySchool($data['school'][0]['school_id']);
                    $data['observers'] = $this->observermodel->getobserverByschoolId($data['school'][0]['school_id']);
                } else {
                    $data['teacher'] = false;
                    $data['observers'] = false;
                }
            } else {
                $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
                $data['observers'] = $this->observermodel->getobserverById($this->session->userdata("observer_id"));
            }
            $this->load->view('teacherself/index', $data);
        }
    }

    function create($report_id = false, $sno = false) {

        $data['idname'] = 'implementation';

        if ($this->is_teacher() == true || $this->is_observer() == true || $this->is_user() == true) {

            $this->load->Model('teacherselfmodel');

            if ($report_id == false) {

                $report_id = $this->teacherselfmodel->createReport();

                $form = $this->input->post('form');
                $this->load->Model('reportmodel');

                if ($this->session->userdata('login_type') == 'teacher')
                    $report_number = $this->reportmodel->getreportnumber('teacher');
                else

                    $report_number = $this->reportmodel->getreportnumber();
					

                $this->session->set_userdata('report_number_new', $report_number);
            }

            if ($report_id != 0) {
                if ($sno != false) {
                    $this->session->set_userdata('report_number_new', $sno);
                }

                $data['reportdata'] = $this->teacherselfmodel->getReportData($report_id);

                if ($data['reportdata'] != false) {
                    $edit = true;
                    $data['reportdata'] = $data['reportdata'][0];
                    $form = $data['reportdata']['report_form'];
                    $lockstatus = $data['reportdata']['status'];
                    if ($lockstatus == 'LOCKED') {
                        $edit = false;
                    } else {
                        $this->session->set_userdata('newform', $form);
                    }
                } else {

                    $edit = false;
                }
            }

            if ($report_id != 0 && $edit == true) {


                if ($form == 'forma' || $form == 'formc') {

                    $data['view_path'] = $this->config->item('view_path');
                    $this->session->set_userdata('new_report_id', $report_id);

                    if ($form == 'forma') {

                        $this->load->Model('observationgroupmodel');
                        $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
                    } else if ($form == 'formc') {

                        $this->load->Model('lickertgroupmodel');
                        $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
                    }

                    if (!empty($data['groups'])) {
                        $group_id = $data['groups'][0]['group_id'];
                        if (isset($data['groups'][1]['group_id'])) {
                            $next_group_id = $data['groups'][1]['group_id'];
                        } else {
                            $next_group_id = 0;
                        }
                        foreach ($data['groups'] as $val) {
                            if ($val['group_id'] == $group_id) {
                                $data['group_name'] = $val['group_name'];
                                $data['description'] = $val['description'];
                            }
                        }
                        if ($form == 'forma') {

                            $this->load->Model('observationpointmodel');
                            $data['points'] = $this->observationpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
                            $data['getpoints'] = $this->teacherselfmodel->obgetReportPoints($this->session->userdata('new_report_id'));
                        } else if ($form == 'formc') {
                            $this->load->Model('lickertpointmodel');
                            $data['points'] = $this->lickertpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
                            $data['getpoints'] = $this->teacherselfmodel->ligetReportPoints($this->session->userdata('new_report_id'));
                        }
                    } else {
                        $group_id = 0;
                        $next_group_id = 0;
                    }
                    if ($data['getpoints'] != false && $data['points'] != false) {
                        $data['getreportpoint'] = $data['getpoints'];
                        foreach ($data['points'] as $pointval) {
                            foreach ($data['getreportpoint'] as $reportval) {
                                if ($pointval['ques_type'] == 'checkbox' && $pointval['group_type_id'] == 2) {
                                    if ($pointval['point_id'] == $reportval['point_id']) {
                                        $data['getreportpoints'][$reportval['point_id']][$reportval['response']] = $reportval['response'];
                                    }
                                } else if ($pointval['point_id'] == $reportval['point_id']) {
                                    $data['getreportpoints'][$reportval['point_id']]['response'] = $reportval['response'];
                                }
                                if ($reportval['group_id']) {
                                    $data['getreportpoints'][$reportval['group_id']]['response-text'] = $reportval['responsetext'];
                                }
                            }
                        }
                    } else {
                        $data['getreportpoints'][0] = '';
                    }
                    $data['group_id'] = $group_id;
                    $data['next_group_id'] = $next_group_id;

                    $this->load->view('teacherself/observationpoints', $data);
                }
            } else {
                $this->index();
            }
        }
    }

    function save() {
        $data['idname'] = 'implementation';
            if ($this->is_teacher() == true || $this->is_observer() == true || $this->is_user() == true) {
            if ($this->session->userdata('new_report_id')) {

                $group_id = $this->input->post('group_id');
                if (isset($group_id)) {
                    $next_group_id = $this->input->post('next_group_id');
                    if ($this->session->userdata('newform') == 'forma') {
                        $this->load->Model('teacherselfmodel');
                        $status = $this->teacherselfmodel->obsavereport();
                    } else if ($this->session->userdata('newform') == 'formc') {
                        $this->load->Model('teacherselfmodel');
                        $status = $this->teacherselfmodel->lisavereport();
                    }

                    if ($status == true) {
                        if ($next_group_id == 0) {
                            if ($this->input->post('prev_group_id')) {
                                $prev_group_id = $this->input->post('group_id');
                                //$prev_group_id++;
                                $this->finish($prev_group_id);
                            } else {

                                $this->finish();
                            }
                        } else {
                            $this->observationpoints($next_group_id);
                        }
                    } else {

                        $this->observationpoints($group_id);
                    }
                }
            } else {
                $this->index();
            }
        }
    }

    function observationpoints($group_id) {
        $data['idname'] = 'implementation';
      if ($this->is_teacher() == true || $this->is_observer() == true || $this->is_user() == true) {
            if ($this->session->userdata('new_report_id')) {



                $this->load->Model('teacherselfmodel');

                $data['reportdata'] = $this->teacherselfmodel->getReportData($this->session->userdata('new_report_id'));
                $data['reportdata'] = $data['reportdata'][0];
                $data['view_path'] = $this->config->item('view_path');
                if ($this->session->userdata('newform') == 'forma') {
                    $this->load->Model('observationgroupmodel');
                    $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
                } else if ($this->session->userdata('newform') == 'formc') {
                    $this->load->Model('lickertgroupmodel');
                    $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
                }
                if ($data['groups'] != false) {
                    foreach ($data['groups'] as $key => $val) {
                        if ($val['group_id'] == $group_id) {
                            $data['group_name'] = $val['group_name'];
                            $data['description'] = $val['description'];

                            $key--;
                            if ($key >= 0) {
                                if (isset($data['groups'][$key]['group_id'])) {
                                    $prev_group_id = $data['groups'][$key]['group_id'];
                                } else {
                                    $prev_group_id = 0;
                                }
                            } else {

                                $prev_group_id = 0;
                            }
                            $key = $key + 2;
                            if (isset($data['groups'][$key]['group_id'])) {
                                $next_group_id = $data['groups'][$key]['group_id'];
                            } else {
                                $next_group_id = 0;
                            }
                        }
                    }
                }
                if ($this->session->userdata('newform') == 'forma') {
                    $this->load->Model('observationpointmodel');
                    $data['points'] = $this->observationpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
                    $data['getpoints'] = $this->teacherselfmodel->obgetReportPoints($this->session->userdata('new_report_id'));
                } else if ($this->session->userdata('newform') == 'formc') {
                    $this->load->Model('lickertpointmodel');
                    $data['points'] = $this->lickertpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
                    $data['getpoints'] = $this->teacherselfmodel->ligetReportPoints($this->session->userdata('new_report_id'));
                }
                //echo "<pre>";
                //print_r($data['getpoints']);
                //exit;
                if ($data['getpoints'] != false && $data['points'] != false) {
                    $data['getreportpoint'] = $data['getpoints'];
                    foreach ($data['points'] as $pointval) {
                        foreach ($data['getreportpoint'] as $reportval) {
                            if ($pointval['ques_type'] == 'checkbox' && $pointval['group_type_id'] == 2) {
                                if ($pointval['point_id'] == $reportval['point_id']) {
                                    $data['getreportpoints'][$reportval['point_id']][$reportval['response']] = $reportval['response'];
                                }
                            } else if ($pointval['point_id'] == $reportval['point_id']) {
                                $data['getreportpoints'][$reportval['point_id']]['response'] = $reportval['response'];
                            }
                            if ($reportval['group_id']) {
                                $data['getreportpoints'][$reportval['group_id']]['response-text'] = $reportval['responsetext'];
                            }
                        }
                    }
                } else {
                    $data['getreportpoints'][0] = '';
                }
                $data['group_id'] = $group_id;
                $data['next_group_id'] = $next_group_id;
                $data['prev_group_id'] = $prev_group_id;

                $this->load->view('teacherself/observationpoints', $data);
            } else {
                $this->index();
            }
        }
    }

    function finish($prev_group_id = false) {
        $data['idname'] = 'implementation';
            if ($this->is_teacher() == true || $this->is_observer() == true || $this->is_user() == true) {
            if ($this->session->userdata('new_report_id')) {
                $report_id = $this->session->userdata('new_report_id');
                $this->load->Model('teacherselfmodel');
                $data['reportdata'] = $this->teacherselfmodel->getReportData($report_id);
                $data['reportdata'] = $data['reportdata'][0];
                if ($this->session->userdata('newform') == 'forma') {
                    $this->load->Model('observationgroupmodel');
                    $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
                } else if ($this->session->userdata('newform') == 'formc') {
                    $this->load->Model('lickertgroupmodel');
                    $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
                }

                if ($prev_group_id != false) {

                    $data['prev_group_id'] = $prev_group_id;
                }
                $data['view_path'] = $this->config->item('view_path');
                $this->load->view('teacherself/finish', $data);
            } else {
                $this->index();
            }
        }
    }

    function finishall() {
  if ($this->is_teacher() == true || $this->is_observer() == true || $this->is_user() == true) {
            if ($this->input->post('submit') == 'Lock') {
                $this->load->Model('teacherselfmodel');
                $this->teacherselfmodel->LockData($this->session->userdata('new_report_id'));
            }
            $this->session->unset_userdata('new_report_id');
            $this->session->unset_userdata('newform');
            $this->index();
        }
    }

    function lockdata() {

        if ($this->input->post('status') == 'LOCKED') {
            $this->load->Model('teacherselfmodel');
            $this->teacherselfmodel->LockData($this->input->post('report_id'));
        }
        $this->selfreport();
    }

    function selfreport() {
        $data['idname'] = 'implementation';
        if ($this->session->userdata('TE') == 0) {
            redirect("index");
        }
        if ($this->session->userdata("login_type") == 'observer' || $this->session->userdata("login_type") == 'user') {

            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
        }
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/selfreport', $data);
    }

    function getteacherreport() {
        $data['idname'] = 'implementation';
        if ($this->input->post('teacher_id')) {
            $this->session->set_userdata('report_teacher_id', $this->input->post('teacher_id'));
        }
        $this->load->Model('teacherselfmodel');
        if ($this->session->userdata('report_teacher_id')) {
            if ($this->input->post('form')) {
                $this->session->set_userdata('reportform', $this->input->post('form'));
            }
            if ($this->input->post('submit')) {
                $this->session->set_userdata('submitbutton', $this->input->post('submit'));
            }
            if ($this->session->userdata('submitbutton') == 'Retrieve Full Report') {
                //Pagination Code Start
                $this->load->Model('utilmodel');
                $this->load->library('pagination');
                $config['base_url'] = base_url() . '/teacherself/getteacherreport/';
                $config['total_rows'] = $this->teacherselfmodel->getReportTeacherCount();
                $config['per_page'] = $this->utilmodel->get_recperpage();
                $config['num_links'] = $this->utilmodel->get_paginationlinks();
                $config['full_tag_open'] = '<p>';
                $config['full_tag_close'] = '</p>';
                $this->pagination->initialize($config);
                $start = $this->uri->segment(3);
                if (trim($start) == "") {
                    $start = 0;
                    $data['sno'] = 1;
                } else {
                    $data['sno'] = $start + 1;
                }
                //Pagination Code End
                $data['reports'] = $this->teacherselfmodel->getReportByTeacher($start, $config['per_page']);
                $data['teacher_id'] = $this->session->userdata('report_teacher_id');
                $data['view_path'] = $this->config->item('view_path');
                if ($this->session->userdata("login_type") == 'observer') {
                    $this->load->Model('teachermodel');
                    $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata("school_id"));
                }
                $this->load->view('teacherself/selfreport', $data);
            } else if ($this->input->post('submit') == 'Retrieve Qualitative Data Report') {
                $this->session->set_userdata('report_criteria_id', $this->input->post('teacher_id'));
                $this->session->set_userdata('report_criteria', 'teacher');
                $this->load->Model('teachermodel');
                $teacher_name = $this->teachermodel->getteacherById($this->input->post('teacher_id'));
                $name = $teacher_name[0]['firstname'] . ' ' . $teacher_name[0]['lastname'];
                $this->session->set_userdata('report_name', $name);
                $this->session->set_userdata('report_from', $this->input->post('from'));
                $this->session->set_userdata('report_to', $this->input->post('to'));
                if ($this->session->userdata('reportform') != 'formb') {
                    $this->retrieve_qualitative_report(false, 'teacher');
                } else {
                    $this->qualitativereportform(false, false, 'teacher');
                }
            } else if ($this->input->post('submit') == 'Retrieve Sectional Report') {
                $this->session->set_userdata('report_criteria_id', $this->input->post('teacher_id'));
                $this->session->set_userdata('report_criteria', 'teacher');
                $this->load->Model('teachermodel');
                $teacher_name = $this->teachermodel->getteacherById($this->input->post('teacher_id'));
                $name = $teacher_name[0]['firstname'] . ' ' . $teacher_name[0]['lastname'];
                $this->session->set_userdata('report_name', $name);

                if ($this->session->userdata('reportform') != 'formb') {
                    $this->retriev_sectionalreport(false, 'teacher');
                } else {
                    $this->sectionalform(false, false, 'teacher');
                }
            }
        } else {
            $this->index();
        }
    }

    function qualitativereport($group_id = false, $report) {

        $this->load->Model('teacherselfmodel');
        $data['view_path'] = $this->config->item('view_path');
        if ($this->session->userdata('reportform') == 'forma') {
            $this->load->Model('observationgroupmodel');
            $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
        } else if ($this->session->userdata('reportform') == 'formc') {
            $this->load->Model('lickertgroupmodel');
            $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
        }
        $data['selfqualitative'] = 1;
        if (!empty($data['groups'])) {
            if ($group_id) {
                
            } else {
                $group_id = $data['groups'][0]['group_id'];
            }

            foreach ($data['groups'] as $val) {
                if ($val['group_id'] == $group_id) {
                    $data['group_name'] = $val['group_name'];
                    $data['description'] = $val['description'];
                }
            }
            $data['qualitative'] = $this->teacherselfmodel->getAllqualitative($group_id, $report);
        } else {
            $group_id = 0;
        }
        $data['group_id'] = $group_id;

        if (!isset($data['qualitative'])) {
            $data['qualitative'] = false;
        }
        $this->load->view('report/qualitative', $data);
        //$this->load->view('teacherself/selfreport',$data);
    }

    function retrieve_qualitative_report($group_id = false, $report) {
        $data['idname'] = 'implementation';
        $this->load->Model('teacherselfmodel');
        $data['view_path'] = $this->config->item('view_path');
        if ($this->session->userdata('reportform') == 'forma') {
            $this->load->Model('observationgroupmodel');
            $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
        } else if ($this->session->userdata('reportform') == 'formc') {
            $this->load->Model('lickertgroupmodel');
            $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
        }
        $data['selfqualitative'] = 1;
        if (!empty($data['groups'])) {
            if ($group_id) {
                
            } else {
                $group_id = $data['groups'][0]['group_id'];
            }

            foreach ($data['groups'] as $val) {
                if ($val['group_id'] == $group_id) {
                    $data['group_name'] = $val['group_name'];
                    $data['description'] = $val['description'];
                }
            }
            $data['qualitative'] = $this->teacherselfmodel->getAllqualitative($group_id, $report);
        } else {
            $group_id = 0;
        }
        $data['group_id'] = $group_id;

        if (!isset($data['qualitative'])) {
            $data['qualitative'] = false;
        }
        $this->load->view('report/retriev_qualitative', $data);
        //$this->load->view('teacherself/selfreport',$data);
    }

    function sectionalreport($group_id = false, $report) {
        $this->load->Model('teacherselfmodel');
        $data['view_path'] = $this->config->item('view_path');
        if ($this->session->userdata('reportform') == 'forma') {
            $this->load->Model('observationgroupmodel');
            $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
        } else if ($this->session->userdata('reportform') == 'formc') {
            $this->load->Model('lickertgroupmodel');
            $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
        }
        $data['selfsectional'] = 1;

        if (!empty($data['groups'])) {
            if ($group_id) {
                
            } else {
                $group_id = $data['groups'][0]['group_id'];
            }

            foreach ($data['groups'] as $val) {
                if ($val['group_id'] == $group_id) {
                    $data['group_name'] = $val['group_name'];
                }
            }


            if ($this->session->userdata('reportform') == 'forma') {
                $this->load->Model('observationpointmodel');
                $data['points'] = $this->observationpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
            } else if ($this->session->userdata('reportform') == 'formc') {
                $this->load->Model('lickertpointmodel');
                $data['points'] = $this->lickertpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
            }

            if ($this->session->userdata('reportform') != 'formp') {
                $data['reports'] = $this->teacherselfmodel->getAllReport($report);
                $data['sectional'] = $this->teacherselfmodel->getAllsectional($group_id, $report);
            }

            if ($data['sectional'] != false) {
                $data['countsectional'] = count($data['sectional']);
            } else {
                $data['countsectional'] = 0;
            }
            if ($data['reports'] != false) {
                $data['countreport'] = count($data['reports']);
            } else {
                $data['countreport'] = 0;
            }
        }
        if (isset($data['points'])) {
            $data['group_set'] = 0;

            if ($data['points'] != '') {
                foreach ($data['points'] as $val) {
                    if ($data['group_set'] != 1) {
                        if ($val['group_type_id'] == 1 || $val['group_type_id'] == 2) {
                            $data['group_set'] = 1;
                            $data['point_set'] = $val['point_id'];
                        }
                    }
                }
            }
        }
        if (!isset($data['points'])) {
            $data['points'] = false;
        }
        $this->load->view('report/sectional', $data);
        //$this->load->view('teacherself/selfreport',$data);
    }

    function retriev_sectionalreport($group_id = false, $report) {
        $data['idname'] = 'implementation';
        $this->load->Model('teacherselfmodel');
        $data['view_path'] = $this->config->item('view_path');
        if ($this->session->userdata('reportform') == 'forma') {
            $this->load->Model('observationgroupmodel');
            $data['groups'] = $this->observationgroupmodel->getallobservationgroups();
        } else if ($this->session->userdata('reportform') == 'formc') {
            $this->load->Model('lickertgroupmodel');
            $data['groups'] = $this->lickertgroupmodel->getalllickertgroups();
        }
        $data['selfsectional'] = 1;

        if (!empty($data['groups'])) {
            if ($group_id) {
                
            } else {
                $group_id = $data['groups'][0]['group_id'];
            }

            foreach ($data['groups'] as $val) {
                if ($val['group_id'] == $group_id) {
                    $data['group_name'] = $val['group_name'];
                }
            }


            if ($this->session->userdata('reportform') == 'forma') {
                $this->load->Model('observationpointmodel');
                $data['points'] = $this->observationpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
            } else if ($this->session->userdata('reportform') == 'formc') {
                $this->load->Model('lickertpointmodel');
                $data['points'] = $this->lickertpointmodel->getAllPoints($group_id, $this->session->userdata("district_id"));
            }

            if ($this->session->userdata('reportform') != 'formp') {
                $data['reports'] = $this->teacherselfmodel->getAllReport($report);
                $data['sectional'] = $this->teacherselfmodel->getAllsectional($group_id, $report);
            }

            if ($data['sectional'] != false) {
                $data['countsectional'] = count($data['sectional']);
            } else {
                $data['countsectional'] = 0;
            }
            if ($data['reports'] != false) {
                $data['countreport'] = count($data['reports']);
            } else {
                $data['countreport'] = 0;
            }
        }
        if (isset($data['points'])) {
            $data['group_set'] = 0;

            if ($data['points'] != '') {
                foreach ($data['points'] as $val) {
                    if ($data['group_set'] != 1) {
                        if ($val['group_type_id'] == 1 || $val['group_type_id'] == 2) {
                            $data['group_set'] = 1;
                            $data['point_set'] = $val['point_id'];
                        }
                    }
                }
            }
        }
        if (!isset($data['points'])) {
            $data['points'] = false;
        }
        $this->load->view('report/retriev_sectional', $data);
        //$this->load->view('teacherself/selfreport',$data);
    }

    function viewreport($report_id, $sno) {

        $data['sno'] = $sno;
        if (!empty($report_id)) {
            $this->load->Model('teacherselfmodel');



            $data['reportdata'] = $this->teacherselfmodel->getReportData($report_id);
            $data['reportdata'] = $data['reportdata'][0];


            if ($this->session->userdata('reportform') == 'forma') {
                $this->load->Model('observationpointmodel');
                $data['points'] = $this->observationpointmodel->getAllGroupPoints();
                $data['getpoints'] = $this->teacherselfmodel->obgetReportPoints($report_id);
            } else if ($this->session->userdata('reportform') == 'formc') {
                $this->load->Model('lickertpointmodel');
                $data['points'] = $this->lickertpointmodel->getAllGroupPoints();
                $data['getpoints'] = $this->teacherselfmodel->ligetReportPoints($report_id);
            }

            if ($data['getpoints'] != false) {
                $data['getreportpoint'] = $data['getpoints'];
                if ($data['points'] != false) {
                    foreach ($data['points'] as $pointval) {
                        foreach ($data['getreportpoint'] as $reportval) {
                            if ($pointval['ques_type'] == 'checkbox' && $pointval['group_type_id'] == 2) {
                                if ($pointval['point_id'] == $reportval['point_id']) {
                                    $data['getreportpoints'][$reportval['point_id']][$reportval['response']] = $reportval['response'];
                                }
                            } else if ($pointval['point_id'] == $reportval['point_id']) {
                                $data['getreportpoints'][$reportval['point_id']]['response'] = $reportval['response'];
                            }
                        }
                    }
                }
            } else {
                $data['getreportpoints'][0] = '';
            }
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('teacherself/viewreport', $data);
        }
    }

    function observer_feature($msg = false) {


        $data['idname'] = 'implementation';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        if ($this->session->userdata('login_type') == 'observer') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $data['message'] = $msg;
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('teacherself/observer_feature', $data);
        } else if ($this->session->userdata('login_type') == 'user') {

            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'implementation');

            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $data['message'] = $msg;
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('teacherself/observer_feature', $data);
        }
    }

    function value_feature($msg = false) {
//	print_r($this->session->all_userdata());exit;

        $data['idname'] = 'implementation';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }

        if ($this->session->userdata('login_type') != 'teacher') {
            if ($this->input->post('submit') && $this->input->post('submit') == 'Next') {
				
                $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
            }
        }

        $this->load->Model('lessonplanmodel');

        if ($this->session->userdata('login_type') != 'teacher') {
            $data['lessonplans'] = $this->lessonplanmodel->getlessonplansbyteacherperiod_id($this->session->userdata("observer_feature_id"), date('Y-m-d'));
			
        } else {
			
            $data['lessonplans'] = $this->lessonplanmodel->getlessonplansbyteacherperiod_id(false, date('Y-m-d'));

        }
//		echo $this->db->last_query();
        if ($msg != false) {
            $message = $msg;
        } else {
            $message = '';
        }
        $data['message'] = $msg;
        $data['view_path'] = $this->config->item('view_path');

        $this->load->view('teacherself/value_feature', $data);
    }

    function observer_value_homework() {
        $this->load->Model('teachermodel');
        $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/value_homework', $data);
    }

    function getlessonplanByTeacher() {
        $this->session->set_userdata('observer_homework_feature_id', $this->input->post('teacher_id'));
        $this->load->Model('lessonplanmodel');
        $data['lessonplans'] = $this->lessonplanmodel->getlessonplansbyteacherperiod_id($this->session->userdata("observer_homework_feature_id"), date('Y-m-d'));
        echo json_encode($data['lessonplans']);
    }

        function value_homework($msg = false) {
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'implementation');
        }
        $data['idname'] = 'implementation';

        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }


        if ($this->session->userdata('login_type') != 'teacher') {

            if ($this->input->post('submit') && $this->input->post('submit') == 'Submit') {
                $this->session->set_userdata('observer_homework_feature_id', $this->input->post('teacher_id'));
            }
        }

        $this->load->Model('lessonplanmodel');
        if ($this->session->userdata('login_type') != 'teacher') {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        } else {

            $data['lessonplans'] = $this->lessonplanmodel->getlessonplansbyteacherperiod_id(false, date('Y-m-d'));
        }
//echo $this->db->last_query();exit;
//        print_r($data);exit;

        if ($msg != false) {
            $message = $msg;
        } else {
            $message = '';
        }
        $data['message'] = $msg;
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/value_homework', $data);
    }

    function observer_value_eld($msg = false) {

        $data['idname'] = 'implementation';
        $this->load->Model('teachermodel');
        $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        $data['view_path'] = $this->config->item('view_path');
        if ($msg != false) {
            $message = $msg;
        } else {
            $message = '';
        }
        $data['message'] = $msg;
        $this->load->view('teacherself/observer_value_eld', $data);
    }

    function get_value_eld() {
        $teacher_id = $this->input->post("teacher_id");
        $this->load->Model('lessonplanmodel');
        $eldplans = $this->lessonplanmodel->geteldplans($teacher_id, date('Y-m-d'), date('Y-m-d'));
        if ($eldplans != false) {
            $data['lessonplans'][0]['lesson_week_plan_id'] = $eldplans[0]['lesson_week_plan_id'];
            $data['lessonplans'][0]['starttime'] = $eldplans[0]['starttime'];
            $data['lessonplans'][0]['endtime'] = $eldplans[0]['endtime'];
        } else {
            $data['lessonplans'] = false;
        }


        echo json_encode($data);
    }

    function value_eld($msg = false) {
//            echo 'test';exit;
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'implementation');
        }
        $data['idname'] = 'implementation';

        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }



        if ($this->session->userdata('login_type') == 'teacher') {

            $teacher_id = $this->session->userdata("teacher_id");

            $this->load->Model('lessonplanmodel');
            $eldplans = $this->lessonplanmodel->geteldplans($teacher_id, date('Y-m-d'), date('Y-m-d'));
            //print_r($this->db->last_query());exit;

            if ($eldplans != false) {
                $data['lessonplans'][0]['lesson_week_plan_id'] = $eldplans[0]['lesson_week_plan_id'];
                $data['lessonplans'][0]['starttime'] = $eldplans[0]['starttime'];
                $data['lessonplans'][0]['endtime'] = $eldplans[0]['endtime'];
            } else {
                $data['lessonplans'] = false;
            }

            if ($msg != false) {
                $message = $msg;
            } else {
                $message = '';
            }
            $data['message'] = $msg;
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('teacherself/value_eld', $data);
        } else

        if ($this->session->userdata('login_type') == 'observer') {
            $this->load->Model('teachermodel');

            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('teacherself/value_eld', $data);
        }
    }

    public function value_eld_observer($msg = false) {
        $teacher_id = $this->input->post("teacher_id");

        $this->load->Model('lessonplanmodel');
        $eldplans = $this->lessonplanmodel->geteldplans($teacher_id, date('Y-m-d'), date('Y-m-d'));

        if ($eldplans != false) {
            $data['lessonplans'][0]['lesson_week_plan_id'] = $eldplans[0]['lesson_week_plan_id'];
            $data['lessonplans'][0]['starttime'] = $eldplans[0]['starttime'];
            $data['lessonplans'][0]['endtime'] = $eldplans[0]['endtime'];
        } else {
            $data['lessonplans'] = false;
        }

        if ($msg != false) {
            $message = $msg;
        } else {
            $message = '';
        }
        $data['message'] = $msg;
        echo json_encode($data);
    }

    function createvalue($date = false, $interval = false) {

        $data['idname'] = 'implementation';
        if ($date == false) {

            $report_date = $this->input->post('report_date');
            $d = explode('-', $report_date);
            @$ds = $d['2'] . '-' . $d['0'] . '-' . $d['1'];
            $report_date = $ds;
            $interval = $this->input->post('time');
        } else {
            $report_date = date('Y-m-d');
        }
        if ($this->session->userdata('login_type') != 'teacher') {
            $teacher_id = $this->session->userdata('observer_feature_id');
        } else {
            $teacher_id = $this->session->userdata('teacher_id');
        }

        //echo $teacher_id;exit;
        $this->load->Model('valueplanmodel');
        $valueplans = $this->valueplanmodel->getallplans();

        $this->load->Model('classwork_proficiencymodel');
        $classworks = $this->classwork_proficiencymodel->getallplans();
        
        //print_r($classworks);exit;

        if ($this->input->post('lesson_week_plan_id') == '') {
            $this->value_feature('Please Select Lesson Plan ');
        } else if ($valueplans == false) {

            $this->value_feature('No Value Plans Found For This District Please Add Plans In District Panel  ');
        } else if ($classworks == false) {
            $this->value_feature('No Classwork Proficiency  Found For This District Please Add Plans In District Panel  ');
        } else {
            $this->load->Model('parentmodel');
            $caption = '';
            $valuestr = ':;';
            foreach ($valueplans as $valueplansvalue) {
                $keyvalue = $valueplansvalue['value_plan_id'];
                $keytext = $valueplansvalue['tab'];
                $valuestr.="$keyvalue:$keytext;";
            }
            $valuestr = substr($valuestr, 0, -1);
            $classworkstr = ':;';
            foreach ($classworks as $classworksvalue) {
                $keyvalue = $classworksvalue['classwork_proficiency_id'];
                $keytext = $classworksvalue['tab'];
                $classworkstr.="$keyvalue:$keytext;";
            }
            $classworkstr = substr($classworkstr, 0, -1);
            @list($yr, $mn, $dt) = split('-', $report_date);    // separate year, month and date
            $timeStamp = mktime(0, 0, 0, $mn, $dt, $yr);
            $dayinfo = getdate($timeStamp);
            $day = $dayinfo['weekday'];
            if ($day == 'Monday') {
                $dayn = 0;
            }
            if ($day == 'Tuesday') {
                $dayn = 1;
            }
            if ($day == 'Wednesday') {
                $dayn = 2;
            }
            if ($day == 'Thursday') {
                $dayn = 3;
            }
            if ($day == 'Friday') {
                $dayn = 4;
            }
            if ($day == 'Saturday') {
                $dayn = 5;
            }
            if ($day == 'Sunday') {
                $dayn = 6;
            }

            $this->session->set_userdata('current_plan_id', $this->input->post('lesson_week_plan_id'));
            $this->session->set_userdata('current_day', $dayn);
            $studentresult = $this->parentmodel->get_students_class_all($this->input->post('lesson_week_plan_id'), $dayn, $teacher_id);
            if ($studentresult != false) {
                $headers = '';
                $jh = 0;
                $this->load->Model('lessonplanmodel');
                $lessonplans = $this->lessonplanmodel->getlessonplansbyid($this->input->post('lesson_week_plan_id'));
                foreach ($lessonplans as $key => $value) { {
                        $starttime = $value['starttime'];
                        $endtime = $value['endtime'];
                        $result = $this->lessonplanmodel->current_value_feature($this->input->post('lesson_week_plan_id'));
                        if ($result == false) {
                            $time_intervals = $this->splitTimeIntoIntervals($starttime, $endtime, null, null, $interval);
                        } else {
                            if ($interval)
                                $time_intervals = $this->splitTimeIntoIntervals($starttime, $endtime, null, null, $interval);
                            else
                                $time_intervals = $this->splitTimeIntoIntervals($starttime, $endtime, null, null, $result[0]['interval']);
                        }

                        $start3 = explode(':', $starttime);
                        if ($start3[0] >= 12) {
                            if ($start3[0] == 12) {
                                $start4 = ($start3[0]) . ':' . $start3[1] . ' pm';
                            } else {
                                $start4 = ($start3[0] - 12) . ':' . $start3[1] . ' pm';
                            }
                        } else if ($start3[0] == 0) {
                            $start4 = ($start3[0] + 12) . ':' . $start3[1] . ' am';
                        } else {
                            $start4 = ($start3[0]) . ':' . $start3[1] . ' am';
                        }
                        $end1 = explode(':', $endtime);
                        if ($end1[0] >= 12) {
                            if ($end1[0] == 12) {
                                $end2 = ($end1[0]) . ':' . $end1[1] . ' pm';
                            } else {
                                $end2 = ($end1[0] - 12) . ':' . $end1[1] . ' pm';
                            }
                        } else if ($end1[0] == 0) {
                            $end2 = ($end1[0] + 12) . ':' . $end1[1] . ' am';
                        } else {
                            $end2 = ($end1[0]) . ':' . $end1[1] . ' am';
                        }

                        $ctime_interval = count($time_intervals);
                        $add_times = array();
                        $s = 0;
                        foreach ($time_intervals as $timevalue) {
                            $start1 = explode(':', $timevalue);
                            if ($start1[0] >= 12) {
                                if ($start1[0] == 12) {
                                    $start2 = ($start1[0]) . ':' . $start1[1] . ' pm';
                                } else {

                                    $start2 = ($start1[0] - 12) . ':' . $start1[1] . ' pm';
                                }
                            } else if ($start1[0] == 0) {
                                $start2 = ($start1[0] + 12) . ':' . $start1[1] . ' am';
                            } else {
                                $start2 = ($start1[0]) . ':' . $start1[1] . ' am';
                            }
                            if ($s == 0) {

                                $head = array();
                                $head["startColumnName"] = 'value_' . $jh;
                                $head["numberOfColumns"] = $ctime_interval + 1;
                                $head["titleText"] = $value['subject_name'] . '(' . $start4 . ' To ' . $end2 . ')';
                                $head = json_encode($head);
                                $headers.=$head . ',';


                                $times[] = $start2;
                            } else {
                                $times[] = $start2;
                            }

                            $add_times[] = $start2;
                            $s++;
                        }

                        $times[] = $value['lesson_week_plan_id'] . '_classwork';

                        foreach ($studentresult as $students) {
                $checkresult = $this->lessonplanmodel->check_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id']);

                


    /*if($checkresult!=0)
        {
            $this->session->set_flashdata('message','all ready created class');
            redirect(base_url() . 'teacherself/value_feature');
        }
*/
                            if ($checkresult == false) {

                                foreach ($add_times as $key => $tivalue) {
                                   $this->lessonplanmodel->add_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'], $tivalue, $interval, $value['subject_id']);
                                }
                                $this->lessonplanmodel->add_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'], 'Classwork', $interval, $value['subject_id']);
                            } else {
                                $this->lessonplanmodel->remove_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'], $value['subject_id']);
                                foreach ($add_times as $key => $tivalue) {
                                    
                                   $this->lessonplanmodel->add_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'], $tivalue, $interval, $value['subject_id']);
                                }
                                $this->lessonplanmodel->add_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'], 'Classwork', $interval, $value['subject_id']);    
                            }
                    /*  if ($checkresult!=0) {
                            //print_r($value['subject_id']);exit;
                            foreach ($add_times as $key => $tivalue) {
                            
       $this->lessonplanmodel->update_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'],$checkresult[0]['value_id'], $tivalue, $interval, $value['subject_id']);
                        }
         $this->lessonplanmodel->update_value_feature($value['lesson_week_plan_id'], $students['teacher_student_id'],$checkresult[0]['value_id'], 'Classwork', $interval, $value['subject_id']);
        
              }*/
                            }   
                            
                     

                        $jh++;
                    }
                }
                $timestr = '';

                $cols = '';
                foreach ($times as $key => $value) {
                    $cla = explode('_', $value);

                    if (isset($cla[1]) && $cla[1] == 'classwork') {
                        /* $col = array();
                          $col["name"] = "value_".$key;
                          $col["index"] = "time_".$key;
                          $col["width"] = 115;
                          $col["align"] = "left"; // render as select
                          $col["editable"] = true;
                          $col["edittype"] = "select"; // render as select
                          //$col["editoptions"] = array("value"=>'1:Off Task;2:On Task'); // with these values "key:value;key:value;key:value"

                          $col["editoptions"] = array("value"=>$classworkstr); // with these values "key:value;key:value;key:value"
                          $newas=json_encode($col);
                          $cols.=$newas.','; */

                        $timestr.="'Classwork Proficiency',";
                    } else {
                        /* $col = array();
                          $col["name"] = "value_".$key;
                          $col["index"] = "time_".$key;
                          $col["width"] = 75;
                          $col["align"] = "left"; // render as select
                          $col["editable"] = true;
                          $col["edittype"] = "select"; // render as select
                          //$col["editoptions"] = array("value"=>'1:Off Task;2:On Task'); // with these values "key:value;key:value;key:value"

                          $col["editoptions"] = array("value"=>$valuestr); // with these values "key:value;key:value;key:value"
                          $newas=json_encode($col);
                          $cols.=$newas.','; */

                        $timestr.="'" . $value . "',";
                    }
                }

                $timesstr = substr($timestr, 0, -1);


                // $cols=substr($cols,0,-1);         
                $data['times'] = $timesstr;
                $data['timesvalue'] = $times;
                $data['valuestr'] = $valuestr;
                $data['classworkstr'] = $classworkstr;


                $data['cols'] = $cols;
                $headers = substr($headers, 0, -1);
                $data['headers'] = $headers;
                /* lessonplan view */

                $this->load->Model('teachermodel');
                $data['teacher'] = $this->teachermodel->getteacherById($teacher_id);
                if ($data['teacher'] != false) {
                    $data['teacher_name'] = $data['teacher'][0]['firstname'] . ' ' . $data['teacher'][0]['lastname'];
                } else {
                    $data['teacher_name'] = '';
                }

                $data['selectdate'] = date('m-d-Y');


                $this->load->Model('lessonplanmodel');
                $data['lessonplansdup'] = $this->lessonplanmodel->getalllessonplansnotsubject();
                $data['lessonplansub'] = $this->lessonplanmodel->getalllessonplanssubnotsubject();
                //echo '<pre>';
                //print_r($data['getlessonplans']);
                //exit;
                $datevar = explode('-', $data['selectdate']);
                $fromdate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];
                $todate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];

                $data['fromdate'] = $data['selectdate'];
                $data['todate'] = $data['selectdate'];


                $weekday = date('l', strtotime($fromdate));

                $data['dates'][$data['selectdate']]['week'] = $weekday;
                $data['dates'][$data['selectdate']]['date'] = $data['selectdate'];
                $data['getlessonplans'] = $this->lessonplanmodel->getlessonplansbyteacher($teacher_id, $fromdate, $todate);

//                print_r($data);exit;
                /* end lessonplan  */
                $data['view_path'] = $this->config->item('view_path');

                $this->load->view('teacherself/value_create', $data);
            } else {

                $this->value_feature('No Students Found');
            }
        }
    }
	
function check_value_feature(){
		$date = $this->input->post('date');
		$lesson_plan_id = $this->input->post('lesson_plan_id');

		$this->load->Model('lessonplanmodel');
		echo $checkresult = $this->lessonplanmodel->check_value_feature_duplicate($date, $lesson_plan_id);
		
	}

    function valueblsave($v, $id, $rowid) {

        $this->load->Model('lessonplanmodel');
        $cid = explode('_', $rowid);
        //model function updateRow writes and executes an UPDATE query with the data supplied
        $r = $this->lessonplanmodel->updateblRow('value_feature', $v, $cid[$id]);
        //$_POST contains all the editable data when we hit the save button in the grid
        echo $r;
    }

    function valuehwsave($v, $id, $rowid) {
        $this->load->Model('lessonplanmodel');
        $cid = explode('_', $rowid);
        //model function updateRow writes and executes an UPDATE query with the data supplied
        $r = $this->lessonplanmodel->updateblRow('homework_feature', $v, $cid[$id]); //$_POST contains all the editable data when we hit the save button in the grid
        echo $r;
    }

    function valueeldsave($v, $id, $rowid) {
        $this->load->Model('lessonplanmodel');
        $cid = explode('_', $rowid);
        //model function updateRow writes and executes an UPDATE query with the data supplied
        $r = $this->lessonplanmodel->updateblRow('eld_feature', $v, $cid[$id]); //$_POST contains all the editable data when we hit the save button in the grid
        echo $r;
    }

    function createhomework($date = false) {

        if ($date == false) {
            $report_date = $this->input->post('report_date');
            $d = explode('/', $report_date);
            @$ds = $d['2'] . '-' . $d['0'] . '-' . $d['1'];
            $report_date = $ds;
        } else {
            $report_date = date('Y-m-d');
        }
        if ($this->session->userdata('login_type') != 'teacher') {
            $teacher_id = $this->session->userdata('observer_homework_feature_id');
        } else {
            $teacher_id = $this->session->userdata('teacher_id');
        }
        $this->load->Model('homework_proficiencymodel');
        $valueplans = $this->homework_proficiencymodel->getallplans();
        /* $this->load->Model('dist_subjectmodel');
          $subjects=$this->dist_subjectmodel->getdist_subjectsById(); */
        if ($this->input->post('lesson_week_plan_id') == '') {
            $this->value_homework('Please Select Lesson Plan  ');
        } else if ($valueplans == false) {
            $this->value_homework('No Homework Proficiencys Found For This District Please Add Plans In District Panel  ');
        } else {
            $caption = '';

            $valuestr = ':;';
            foreach ($valueplans as $valueplansvalue) {
                $keyvalue = $valueplansvalue['homework_proficiency_id'];
                $keytext = $valueplansvalue['tab'];
                $valuestr.="$keyvalue:$keytext;";
            }
            $valuestr = substr($valuestr, 0, -1);




            @list($yr, $mn, $dt) = split('-', $report_date);    // separate year, month and date
            $timeStamp = mktime(0, 0, 0, $mn, $dt, $yr);
            $dayinfo = getdate($timeStamp);
            $day = $dayinfo['weekday'];
            if ($day == 'Monday') {
                $dayn = 0;
            }
            if ($day == 'Tuesday') {
                $dayn = 1;
            }
            if ($day == 'Wednesday') {
                $dayn = 2;
            }
            if ($day == 'Thursday') {
                $dayn = 3;
            }
            if ($day == 'Friday') {
                $dayn = 4;
            }
            if ($day == 'Saturday') {
                $dayn = 5;
            }
            if ($day == 'Sunday') {
                $dayn = 6;
            }

            $this->session->set_userdata('current_plan_id', $this->input->post('lesson_week_plan_id'));
            $this->session->set_userdata('current_day', $dayn);
            $this->load->Model('parentmodel');
            $studentresult = $this->parentmodel->get_students_class_all($this->input->post('lesson_week_plan_id'), $dayn, $teacher_id);

            if ($studentresult != false) {

                $this->load->Model('lessonplanmodel');
                $subjects = $this->lessonplanmodel->getlessonplansbyid($this->input->post('lesson_week_plan_id'));


                foreach ($subjects as $subjectvalue) {
                    foreach ($studentresult as $students) {
                        $checkresult = $this->lessonplanmodel->check_value_homework($subjectvalue['subject_id'], $students['teacher_student_id']);
                        if ($checkresult == false) {

                            $this->lessonplanmodel->add_value_homework($subjectvalue['subject_id'], $students['teacher_student_id']);
                        }
                    }
                }

                $timestr = '';

                $cols = '';
                foreach ($subjects as $key => $value) {

                    /* $col = array();
                      $col["name"] = "value_".$key;
                      $col["index"] = "time_".$key;
                      $col["width"] = 115;
                      $col["align"] = "left"; // render as select
                      $col["editable"] = true;
                      $col["edittype"] = "select"; // render as select
                      //$col["editoptions"] = array("value"=>'1:Off Task;2:On Task'); // with these values "key:value;key:value;key:value"

                      $col["editoptions"] = array("value"=>$valuestr); // with these values "key:value;key:value;key:value"
                      $newas=json_encode($col);
                      $cols.=$newas.','; */

                    $timestr.="'" . $value['subject_name'] . "',";
                }

                $timesstr = substr($timestr, 0, -1);
                //$result=$this->lessonplanmodel->value_homework();

                /* foreach($result as $resultvalue)
                  {
                  if($resultvalue['value_id']<=4)
                  {

                  $col = array();
                  $col["name"] = "value_".$resultvalue['value_id'];
                  $col["index"] = "time_".$resultvalue['value_id'];
                  $col["width"] = 35;
                  $col["align"] = "left"; // render as select
                  $col["editable"] = true;
                  $col["edittype"] = "select"; // render as select
                  $col["editoptions"] = array("value"=>'No:Off Task;Yes:On Task'); // with these values "key:value;key:value;key:value"
                  $newas=json_encode($col);
                  $cols.=$newas.',';
                  }



                  } */

                $cols = substr($cols, 0, -1);
                $data['times'] = $timesstr;

                $data['cols'] = $cols;
                $data['subjects'] = $subjects;
                $data['valuestr'] = $valuestr;

                //$headers=substr($headers,0,-1);		 
                $head = array();
                $head["startColumnName"] = 'value_0';
                $head["numberOfColumns"] = count($subjects);
                $head["titleText"] = 'Subject';
                $head = json_encode($head);

                $data['headers'] = $head;
                /* lessonplan view */

                $this->load->Model('teachermodel');
                $data['teacher'] = $this->teachermodel->getteacherById($teacher_id);
                if ($data['teacher'] != false) {
                    $data['teacher_name'] = $data['teacher'][0]['firstname'] . ' ' . $data['teacher'][0]['lastname'];
                } else {
                    $data['teacher_name'] = '';
                }

                /* $data['selectdate']=date('m-d-Y');


                  $this->load->Model('lessonplanmodel');
                  $data['lessonplansdup']=$this->lessonplanmodel->getalllessonplans();

                  $data['lessonplansub']=$this->lessonplanmodel->getalllessonplanssub();
                  //echo '<pre>';
                  //print_r($data['getlessonplans']);
                  //exit;
                  $datevar=explode('-',$data['selectdate']);
                  $fromdate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];
                  $todate=$datevar[2].'-'.$datevar[0].'-'.$datevar[1];

                  $data['fromdate']=$data['selectdate'];
                  $data['todate']=$data['selectdate'];


                  $weekday = date('l', strtotime($fromdate));

                  $data['dates'][$data['selectdate']]['week']=$weekday;
                  $data['dates'][$data['selectdate']]['date']=$data['selectdate'];
                  $data['getlessonplans']=$this->lessonplanmodel->getlessonplansbyteacher($teacher_id,$fromdate,$todate); */


                /* end lessonplan  */
                $data['view_path'] = $this->config->item('view_path');
                $this->load->view('teacherself/value_create_homework', $data);
                //  $this->load->view('teacherself/value_homework',$data);
            } else {

                $this->value_homework('No Students Found');
            }
        }
    }

    function createeld($date = false) {

 $data['idname'] = 'implementation';
        if ($date == false) {

            $report_date = $this->input->post('report_date');
            $d = explode('-', $report_date);
            @$ds = $d['2'] . '-' . $d['0'] . '-' . $d['1'];
            $report_date = $ds;
        } else {

            $report_date = date('Y-m-d');
        }

        if ($this->session->userdata('teacher_id'))
            $teacher_id = $this->session->userdata('teacher_id');
        else
            $teacher_id = $this->input->post('teacher_id');



        $this->load->Model('lessonplanmodel');
        $eldplans = $this->lessonplanmodel->geteldplans($teacher_id, date('Y-m-d'), date('Y-m-d'));
//        echo $this->db->last_query();exit;
        if ($eldplans != false) {
            $this->load->Model('eldrubricmodel');
            //$valueplans = $this->eldrubricmodel->getAllsubgroupsgroup($eldplans[0]['subject_id']);
            //echo $this->db->last_query();exit;
            $valueplans = $this->eldrubricmodel->geteldRubricsbydistrict();
            $subjects[0]['subject_name'] = $eldplans[0]['subject_name'];
            $subjects[0]['subject_id'] = $eldplans[0]['subject_id'];
            $lesson_week_plan_id = $eldplans[0]['lesson_week_plan_id'];
        }


        /* if($this->input->post('class_room_id')=='')
          {
          $this->value_eld('Please Select Class Room  ');
          }
          else */

        $this->load->Model('curriculummodel');
        $data['curriculum'] = $this->curriculummodel->checkcurriculum();
                
        if ($eldplans == false) {
            if ($this->is_observer())
                $this->observer_value_eld('No ELD Lesson Plan  Found  ');
            else
                $this->value_eld('No ELD Lesson Plan  Found  ');
        }
        else if ($valueplans == false && $data['curriculum']=='standards') {
            if ($this->is_observer())
                $this->observer_value_eld('No Eld Rubrics Found For This District Please Add Rubrics In District Panel  ');
            else
                $this->value_eld('No Eld Rubrics Found For This District Please Add Rubrics In District Panel  ');
        }
        else {

            //$this->session->set_userdata('eld_class_room_id',$this->input->post('class_room_id'));
            $this->load->Model('parentmodel');

            $caption = '';



            $valuestr = ':;';
            $groupheader = '';
            $headerinfocols = '';
            $mydatainfodata = '';
            foreach ($valueplans as $key => $valueplansvalue) {
                $keyvalue = $valueplansvalue['rubric_data_id'];
                $keytext = $valueplansvalue['sub_group_name'];
                $valuestr.="$keyvalue:$keytext;";
                $groupheader.="'" . $valueplansvalue['sub_group_name'] . "',";


                $headerinfo = array();
                $headerinfo["name"] = "value_" . $key;
                $headerinfo["index"] = "time_" . $key;
                $headerinfo["width"] = 115;
                $headerinfo["align"] = "left"; // render as select

                $hnewas = json_encode($headerinfo);
                $headerinfocols.=$hnewas . ',';




                $mydatakey = "value_" . $key;
                $mydatavalue = $valueplansvalue['sub_group_text'];

                $mydatainfodata.="$mydatakey:'$mydatavalue',";
            }

            $valuestr = substr($valuestr, 0, -1);
            $groupheaderstr = substr($groupheader, 0, -1);
            $mydata = substr($mydatainfodata, 0, -1);

            list($yr, $mn, $dt) = explode('-', $report_date);    // separate year, month and date
            

				$timeStamp            =    mktime(0,0,0,$mn,$dt,$yr); 
            $dayinfo = getdate($timeStamp);
            $day = $dayinfo['weekday'];
            if ($day == 'Monday') {
                $dayn = 0;
            }
            if ($day == 'Tuesday') {
                $dayn = 1;
            }
            if ($day == 'Wednesday') {
                $dayn = 2;
            }
            if ($day == 'Thursday') {
                $dayn = 3;
            }
            if ($day == 'Friday') {
                $dayn = 4;
            }
            if ($day == 'Saturday') {
                $dayn = 5;
            }
            if ($day == 'Sunday') {
                $dayn = 6;
            }
            $studentresult = $this->parentmodel->get_students_class_all($lesson_week_plan_id, $dayn, $teacher_id);

            //print_r($studentresult);exit;
            if ($studentresult != false) {

                $this->load->Model('lessonplanmodel');

                foreach ($subjects as $subjectvalue) {
                    foreach ($studentresult as $students) {
                        $checkresult = $this->lessonplanmodel->check_value_eld($subjectvalue['subject_id'], $students['teacher_student_id']);
                        
                        if ($checkresult == false) {

                            $this->lessonplanmodel->add_value_eld($subjectvalue['subject_id'], $students['teacher_student_id']);
                            
                        }
                    }
                }

                $timestr = '';


                $cols = '';
                foreach ($subjects as $key => $value) {

                    /* $col = array();
                      $col["name"] = "value_".$key;
                      $col["index"] = "time_".$key;
                      $col["width"] = 115;
                      $col["align"] = "left"; // render as select
                      $col["editable"] = true;
                      $col["edittype"] = "select"; // render as select
                      //$col["editoptions"] = array("value"=>'1:Off Task;2:On Task'); // with these values "key:value;key:value;key:value"

                      $col["editoptions"] = array("value"=>$valuestr); // with these values "key:value;key:value;key:value"
                      $newas=json_encode($col);
                      $cols.=$newas.','; */

                    $timestr.="'" . $value['subject_name'] . "',";
                }

                $timesstr = substr($timestr, 0, -1);
                //$result=$this->lessonplanmodel->value_homework();

                /* foreach($result as $resultvalue)
                  {
                  if($resultvalue['value_id']<=4)
                  {

                  $col = array();
                  $col["name"] = "value_".$resultvalue['value_id'];
                  $col["index"] = "time_".$resultvalue['value_id'];
                  $col["width"] = 35;
                  $col["align"] = "left"; // render as select
                  $col["editable"] = true;
                  $col["edittype"] = "select"; // render as select
                  $col["editoptions"] = array("value"=>'No:Off Task;Yes:On Task'); // with these values "key:value;key:value;key:value"
                  $newas=json_encode($col);
                  $cols.=$newas.',';
                  }



                  } */

                $cols = substr($cols, 0, -1);
                $headerinformation = substr($headerinfocols, 0, -1);

                $data['times'] = $timesstr;
                $data['subjects'] = $subjects;
                $data['valuestr'] = $valuestr;
                $data['groupheader'] = $groupheaderstr;
                $data['groupinfo'] = $headerinformation;

                $data['mydata'] = $mydata;
                $data['cols'] = $cols;
                //$headers=substr($headers,0,-1);		 
                $head = array();
                $head["startColumnName"] = 'value_0';
                $head["numberOfColumns"] = count($subjects);
                $head["titleText"] = 'Subject';
                $head = json_encode($head);

                $data['headers'] = $head;
                /* lessonplan view */

                $this->load->Model('teachermodel');
                $data['teacher'] = $this->teachermodel->getteacherById($teacher_id);
                if ($data['teacher'] != false) {
                    $data['teacher_name'] = $data['teacher'][0]['firstname'] . ' ' . $data['teacher'][0]['lastname'];
                } else {
                    $data['teacher_name'] = '';
                }


                $data['lessonplansdup'] = $this->lessonplanmodel->getalllessonplansnotsubject();
                $data['lessonplansub'] = $this->lessonplanmodel->getalllessonplanssubnotsubject();
                $data['selectdate'] = date('m-d-Y');



                $datevar = explode('-', $data['selectdate']);
                $fromdate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];
                $todate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];

                $data['fromdate'] = $data['selectdate'];
                $data['todate'] = $data['selectdate'];


                $weekday = date('l', strtotime($fromdate));

                $data['dates'][$data['selectdate']]['week'] = $weekday;
                $data['dates'][$data['selectdate']]['date'] = $data['selectdate'];
                $data['getlessonplans'] = $this->lessonplanmodel->geteldplansnew($teacher_id, date('Y-m-d'), date('Y-m-d'));


                
                /* end lessonplan  */
                $data['view_path'] = $this->config->item('view_path');
                $this->load->view('teacherself/value_create_eld', $data);
            } else {
                if ($this->is_observer())
                    $this->observer_value_eld('No Students Found');
                else
                    $this->value_eld('No Students Found');
            }
        }
    }

    function getlessonplan($report_date = false) {
        if ($report_date == false) {
            $data['report_date'] = date('m-d-Y');
        } else {
            $data['report_date'] = $report_date;
        }
        if ($this->session->userdata('login_type') != 'teacher') {
            $teacher_id = $this->session->userdata('observer_feature_id');
        } else {
            $teacher_id = $this->session->userdata('teacher_id');
        }
        $this->load->Model('lessonplanmodel');

        $lessonplansdup = $this->lessonplanmodel->getalllessonplansnotsubject();

        $lessonplansub = $this->lessonplanmodel->getalllessonplanssubnotsubject();


        //echo '<pre>';
        //print_r($data['getlessonplans']);
        //exit;
        $datevar = explode('-', $data['report_date']);
        $fromdate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];
        $todate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];



        $weekday = date('l', strtotime($fromdate));

        $dates[$data['report_date']]['week'] = $weekday;
        $dates[$data['report_date']]['date'] = $data['report_date'];
        $getlessonplans = $this->lessonplanmodel->getlessonplansbyteacher($teacher_id, $fromdate, $todate);


        $k = 0;
        foreach ($dates as $val) {
             $pdfdatevar = explode('-', $val['date']);
            $pdfdate = $datevar[2] . '-' . $datevar[0] . '-' . $datevar[1];
            $lesdas = count($lessonplansdup) + 3;
            $lesdas = count($lessonplansdup) + 3;
            $d = explode('-', $val['date']);
            $ds = $d[0] . '/' . $d['1'] . '/' . $d['2'];
            $message = '<table align="center" style="width:1070px;margin-left:10px;border:1px #999 solid; font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; font-size:12px; color:#666;" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan=' . $lesdas . ' align="center" bgcolor="#FFFFFF" style="color:#88888"><b> Lesson Plan Book &nbsp;&nbsp;&nbsp; Date:&nbsp;' . $ds . ' &nbsp;&nbsp;&nbsp;Day:&nbsp;' . $val['week'] . '</b></td>
  </tr>
  <tr align="center" style="color:#333;">
    <td width="100px" bgcolor="#CCCCCC">Time</td>
	<td width="100px" bgcolor="#CCCCCC">Subject</td>
	';

            $j = 3;
            if ($lessonplansdup != false) {
                foreach ($lessonplansdup as $plan) {
                    $j++;

                    $message.='<td width="100px" bgcolor="#CCCCCC">' . $plan['tab'] . '</td>';
                }
                $message.='<td width="50px" bgcolor="#CCCCCC">Details</td>';
            }
            $message.=' </tr>
    <tr align="center"  >
	<td colspan="' . $j . '">
	<table   id="' . $val['date'] . '" >';
            if ($getlessonplans != false) {

                $le = 0;
                $je = 0;
                // echo "<pre>";
                //print_r($getlessonplans);
                foreach ($getlessonplans as $getplan) {
                    if ($val['date'] == $getplan['date']) {
                        $je++;
                        if ($le != $getplan['lesson_week_plan_id']) {
                            $les = 0;
                            if ($je == 1) {

                                $message.='<tr align="center">';
                            } else {
                                if ($val['date'] >= date('m-d-Y')) {




      $message.="<td>NA</td><td width='132px'><a class='standard' target='_blank' href='".base_url()."planningmanager/createlessonplanpdf/$teacher_id/".$getlessonplans[0]['subject_id']."/".$pdfdate."' title='Other' >View</a></td> </tr><tr align='center'>";
                                } else {


$message.="<td>NA</td><td width='132px'><a class='standard' target='_blank' href='".base_url()."planningmanager/createlessonplanpdf/$teacher_id/".$getlessonplans[0]['subject_id']."/".$pdfdate."' title='Other' >View</a></td> </tr><tr align='center'>";
                                    //$message.="<td width='132px'><a class='standard' href='#' title='Other' rel='teacherself/getother/$le'>View</a></td> </tr><tr align='center'>";
                                }
                            }


                            $message.='<td width="286px">';
                            $start1 = explode(':', $getplan['starttime']);
                            $end1 = explode(':', $getplan['endtime']);
                            if ($start1[0] >= 12) {

                                if ($start1[0] == 12) {
                                    $start2 = ($start1[0]) . ':' . $start1[1] . ' pm';
                                } else {
                                    $start2 = ($start1[0] - 12) . ':' . $start1[1] . ' pm';
                                }
                            } else if ($start1[0] == 0) {
                                $start2 = ($start1[0] + 12) . ':' . $start1[1] . ' am';
                            } else {
                                $start2 = ($start1[0]) . ':' . $start1[1] . ' am';
                            }
                            if ($end1[0] >= 12) {
                                if ($end1[0] == 12) {
                                    $end2 = ($end1[0]) . ':' . $end1[1] . ' pm';
                                } else {
                                    $end2 = ($end1[0] - 12) . ':' . $end1[1] . ' pm';
                                }
                            } else if ($end1[0] == 0) {
                                $end2 = ($end1[0] + 12) . ':' . $end1[1] . ' am';
                            } else {
                                $end2 = ($end1[0]) . ':' . $end1[1] . ' am';
                            }
                            $message.=$start2 . " to " . $end2;
                            $message.='</td><td width="147px">' . $getplan['subject_name'] . '</td><td width="295px">' . $getplan['subtab'] . '</td>';
                        } else {
                            $les++;

                            $message.='<td width="219px">' . $getplan['subtab'] . '</td>';
                        }

                        $le = $getplan['lesson_week_plan_id'];
                        $lecomments[$le] = $getplan['comments'];
                    }
                }
                if ($je == 0) {

                    $message.='<tr align="center">
		<td colspan="' . $j . '">No Plans Found </td>';
                } if ($le != 0) {
                    /* if(($les+1)>=count($lessonplansdup))
                      {


                      }
                      else
                      {
                      ?>
                      <td width="<?php echo (count($lessonplansdup)-$les-1)*100;?>px" ></td>
                      <?php } */
                    if ($val['date'] >= date('m-d-Y')) {
                        
                    } else {
                        
                    }
                } $message.="<td width='128px'><a class='standard' target='_blank' href='".base_url()."planningmanager/createlessonplanpdf/$teacher_id/".$getlessonplans[0]['subject_id']."/".$pdfdate."' title='Other' >View</a></td></tr></table>";//rel='teacherself/getother/$le'
            } else {
                $message.='<tr><td colspan="' . $j . '">No Plans Found  </td></tr></table></td></tr>';
            }

            if ($val['date'] >= date('m-d-Y')) {
                
            }
            $message.='</table>
	</td>
  </tr>
 
  
  
</table>
<br />';
        }
        echo $message;
    }

  
  function gridServerPart() {
        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        $this->load->Model('valueplanmodel');

        $valueplans = $this->valueplanmodel->getallplans();
        $valuetext[''] = '';
        foreach ($valueplans as $valueplanvalue) {
            $valuetext[$valueplanvalue['value_plan_id']] = $valueplanvalue['tab'];
        }

        $this->load->Model('classwork_proficiencymodel');
        $classworks = $this->classwork_proficiencymodel->getallplans();
        $classworktext[''] = '';
        foreach ($classworks as $classworksvalue) {
            $classworktext[$classworksvalue['classwork_proficiency_id']] = $classworksvalue['tab'];
        }

        if (!$sidx)
            $sidx = 1;

        $this->load->Model('lessonplanmodel');
        $this->load->Model('parentmodel');
        $lesson_week_plan_id = $this->session->userdata('current_plan_id');
        $dayn = $this->session->userdata('current_day');

        if ($this->session->userdata('login_type') == 'teacher') {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id = $this->session->userdata('observer_feature_id');
        }
        $studentresult = $this->parentmodel->get_students_class_all($lesson_week_plan_id, $dayn, $teacher_id);

        $count = count($studentresult);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value


        $result = $this->lessonplanmodel->current_value_feature($lesson_week_plan_id);  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($studentresult != false) {
            foreach ($studentresult as $key => $studentvalue) {
                $ids = '';
                foreach ($result as $row) {
                    if ($row['student_id'] == $studentvalue['teacher_student_id'])
                        $ids.=$row['value_id'] . '_';
                }
                $ids = substr($ids, 0, -1);
                echo "<row id='" . $ids . "'>";
                echo "<cell><![CDATA[<input type='checkbox' name='studentvalues' checked=checked value=" . $studentvalue['student_id'] . " id='studentvalues'> " . $studentvalue['firstname'] . " " . $studentvalue['lastname'] . "]]></cell>";
                echo "<cell><![CDATA[act]]></cell>";
                foreach ($result as $row) {
                    if ($row['student_id'] == $studentvalue['teacher_student_id'])
                        if ($row['time_interval'] == 'Classwork') {
                            echo "<cell><![CDATA[" . $classworktext[$row['task']] . "]]></cell>";
                        } else {
                            echo "<cell><![CDATA[" . $valuetext[$row['task']] . "]]></cell>";
                        }
                }

                echo "</row>";
                /* foreach($result as $row) {
                  echo "<row id='".$row['value_id']."'>";
                  echo "<cell><![CDATA[".$studentvalue['firstname']."]]></cell>";
                  echo "<cell><![CDATA[".$row['value_id']."]]></cell>";
                  echo "<cell><![CDATA[".$row['task']."]]></cell>";
                  echo "<cell><![CDATA[act]]></cell>";

                  echo "</row>";
                  } */
            }
        }
        echo "</rows>";
    }

    function edit_test_save() {
        if (isset($_POST)) {
            $this->load->Model('lessonplanmodel');
            //model function updateRow writes and executes an UPDATE query with the data supplied
            $r = $this->lessonplanmodel->updateRow('value_feature', $_POST, array('id' => $_POST['id'])); //$_POST contains all the editable data when we hit the save button in the grid
            echo $r;
        }
    }
  
  
  

    function gridServerParthomework() {
        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        $this->load->Model('homework_proficiencymodel');

        $valueplans = $this->homework_proficiencymodel->getallplans();
        $valuetext[''] = '';
        foreach ($valueplans as $valueplanvalue) {
            $valuetext[$valueplanvalue['homework_proficiency_id']] = $valueplanvalue['tab'];
        }

        if (!$sidx)
            $sidx = 1;
        $this->load->Model('lessonplanmodel');
        $this->load->Model('parentmodel');
        $lesson_week_plan_id = $this->session->userdata('current_plan_id');
        $dayn = $this->session->userdata('current_day');

        if ($this->session->userdata('login_type') == 'teacher') {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id = $this->session->userdata('observer_homework_feature_id');
        }
        $studentresult = $this->parentmodel->get_students_class_all($lesson_week_plan_id, $dayn, $teacher_id);
        $count = count($studentresult);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value


        $lessonplans = $this->lessonplanmodel->getlessonplansbyid($lesson_week_plan_id);

        $result = $this->lessonplanmodel->current_home_work($lessonplans[0]['subject_id'], $teacher_id);  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($studentresult != false) {
            foreach ($studentresult as $key => $studentvalue) {
                $ids = '';
                foreach ($result as $row) {
                    if ($row['student_id'] == $studentvalue['teacher_student_id'])
                        $ids.=$row['homework_feature_id'] . '_';
                }
                $ids = substr($ids, 0, -1);
                echo "<row id='" . $ids . "'>";
                echo "<cell><![CDATA[<input type='checkbox' name='studentvalues' checked=checked value=" . $studentvalue['student_id'] . " id='studentvalues'> " . $studentvalue['firstname'] . "]]></cell>";
                echo "<cell><![CDATA[act]]></cell>";
                foreach ($result as $row) {
                    if ($row['student_id'] == $studentvalue['teacher_student_id'])
                        echo "<cell><![CDATA[" . $valuetext[$row['task']] . "]]></cell>";
                }

                echo "</row>";
                /* foreach($result as $row) {
                  echo "<row id='".$row['value_id']."'>";
                  echo "<cell><![CDATA[".$studentvalue['firstname']."]]></cell>";
                  echo "<cell><![CDATA[".$row['value_id']."]]></cell>";
                  echo "<cell><![CDATA[".$row['task']."]]></cell>";
                  echo "<cell><![CDATA[act]]></cell>";

                  echo "</row>";
                  } */
            }
        }
        echo "</rows>";
    }

    function gridServerParteld() {
        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        $teacher_id = $this->session->userdata('teacher_id');
        $this->load->Model('lessonplanmodel');
        $eldplans = $this->lessonplanmodel->geteldplans($teacher_id, date('Y-m-d'), date('Y-m-d'));
        if ($eldplans != false) {
            $this->load->Model('eldrubricmodel');
            // $valueplans = $this->eldrubricmodel->getAllsubgroupsgroup($eldplans[0]['subject_id']);
            $valueplans = $this->eldrubricmodel->geteldRubricsbydistrict();
            $subjects[0]['subject_name'] = $eldplans[0]['subject_name'];
            $subjects[0]['subject_id'] = $eldplans[0]['subject_id'];
            $lesson_week_plan_id = $eldplans[0]['lesson_week_plan_id'];
        }
// echo $this->db->last_query();
// print_r($valueplans);exit;
        $valuetext[''] = '';
        foreach ($valueplans as $valueplanvalue) {
            // if($valueplanvalue['rubric_data_id']!='')
            $valuetext[$valueplanvalue['rubric_data_id']] = $valueplanvalue['sub_group_name'];
			
        }
// print_r($valuetext);exit;


        if (!$sidx)
            $sidx = 1;


        $this->load->Model('parentmodel');
        list($yr, $mn, $dt) = explode('-', date('Y-m-d'));    // separate year, month and date
        $timeStamp = mktime(0, 0, 0, $mn, $dt, $yr);
        $dayinfo = getdate($timeStamp);
        $day = $dayinfo['weekday'];
        if ($day == 'Monday') {
            $dayn = 0;
        }
        if ($day == 'Tuesday') {
            $dayn = 1;
        }
        if ($day == 'Wednesday') {
            $dayn = 2;
        }
        if ($day == 'Thursday') {
            $dayn = 3;
        }
        if ($day == 'Friday') {
            $dayn = 4;
        }
        if ($day == 'Saturday') {
            $dayn = 5;
        }
        if ($day == 'Sunday') {
            $dayn = 6;
        }

        $studentresult = $this->parentmodel->get_students_class_all($lesson_week_plan_id, $dayn, $teacher_id);

        $count = count($studentresult);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value


        $result = $this->lessonplanmodel->current_eld($subjects[0]['subject_id'], $teacher_id);  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($studentresult != false) {
            foreach ($studentresult as $key => $studentvalue) {
                $ids = '';
                foreach ($result as $row) {
                    if ($row['student_id'] == $studentvalue['teacher_student_id'])
                        $ids.=$row['eld_feature_id'] . '_';
                }
                $ids = substr($ids, 0, -1);
                echo "<row id='" . $ids . "'>";
                echo "<cell><![CDATA[<input type='checkbox' name='studentvalues' checked=checked value=" . $studentvalue['student_id'] . " id='studentvalues'> " . $studentvalue['firstname'] ." " . $studentvalue['lastname'] ."]]></cell>";
                echo "<cell><![CDATA[act]]></cell>";
                foreach ($result as $row) {
                    if ($row['student_id'] == $studentvalue['teacher_student_id'])
                        echo "<cell><![CDATA[" . $valuetext[$row['task']] . "]]></cell>";
                }

                echo "</row>";
                /* foreach($result as $row) {
                  echo "<row id='".$row['value_id']."'>";
                  echo "<cell><![CDATA[".$studentvalue['firstname']."]]></cell>";
                  echo "<cell><![CDATA[".$row['value_id']."]]></cell>";
                  echo "<cell><![CDATA[".$row['task']."]]></cell>";
                  echo "<cell><![CDATA[act]]></cell>";

                  echo "</row>";
                  } */
            }
        }
        echo "</rows>";
    }

    function edit_homework_save() {
        if (isset($_POST)) {
            $this->load->Model('lessonplanmodel');
            //model function updateRow writes and executes an UPDATE query with the data supplied
            $r = $this->lessonplanmodel->updateHomeworkRow('homework_feature', $_POST, array('id' => $_POST['id'])); //$_POST contains all the editable data when we hit the save button in the grid
            echo $r;
        }
    }

    function edit_eld_save() {
        if (isset($_POST)) {
            $this->load->Model('lessonplanmodel');
            //model function updateRow writes and executes an UPDATE query with the data supplied
            $r = $this->lessonplanmodel->updateeldRow('eld_feature', $_POST, array('id' => $_POST['id'])); 

			//$_POST contains all the editable data when we hit the save button in the grid
            echo $r;
        }
    }

    function splitTimeIntoIntervals($work_starts, $work_ends, $break_starts = null, $break_ends = null, $minutes_per_interval = 60) {
        $intervals = array();
        $time = date("H:i", strtotime($work_starts));
        $first_after_break = false;
        while (strtotime($time) < strtotime($work_ends)) {
// if at least one of the arguments associated with breaks is mising - act like there is no break
            if ($break_starts == null || $break_starts == null) {
                $time_starts = date("H:i", strtotime($time));
                $time_ends = date("H:i", strtotime($time_starts . "+$minutes_per_interval minutes"));
            }
// if the break start/end time is specified
            else {
                if ($first_after_break == true) {//first start time after break
                    $time = (date("H:i", strtotime($break_ends)));
                    $first_after_break = false;
                }
                $time_starts = (date("H:i", strtotime($time)));
                $time_ends = date("H:i", strtotime($time_starts . "+$minutes_per_interval minutes"));
//if end_time intersects break_start and break_end times
                if (timesIntersects($time_starts, $time_ends, $break_starts, $break_ends)) {
                    $time_ends = date("H:i", strtotime($break_starts));
                    $first_after_break = true;
                }
            }
            //if end_time is after work_ends
            if (date("H:i", strtotime($time_ends)) > date("H:i", strtotime($work_ends))) {
                $time_ends = date("H:i", strtotime($work_ends));
            }
            $intervals[] = $time_ends;
            $time = $time_ends;
        }
        return $intervals;
    }

//time intersects if order of parametrs is one of the following 1342 or 1324

    function sendemailtostudents() {
        $pdata = $this->input->post('pdata');
        foreach ($pdata as $pvalue) {
            $this->load->Model('lessonplanmodel');
            $result = $this->lessonplanmodel->getstudentdata($pvalue);
            $classresult = $this->lessonplanmodel->getstudentclassworkdata($pvalue);
            $homeworkresult = $this->lessonplanmodel->getstudenthomeworkdata($pvalue);

            //print_r($result);
            $sendemail = 0;
            $taskdata = '';

            foreach ($result as $resultvalue) {
                $classwork = '';
                $homework = '';

                $task = explode(',', $resultvalue['task']);
                $time_interval = explode(',', $resultvalue['time_interval']);
                $email = $resultvalue['email'];
                $emailsub = $resultvalue['emailsub'];
                $date = $resultvalue['current_date'];
                $name = $resultvalue['parentsname'];
                $namesub = $resultvalue['parentsnamesub'];
                $studentname = $resultvalue['studentname'];
                $subjectname = $resultvalue['subtab'];
                if ($classresult != false) {
                    foreach ($classresult as $classvalue) {
                        if ($classvalue['lesson_plan_sub_id'] == $resultvalue['lesson_plan_sub_id']) {
                            $classwork = $classvalue['tab'];
                        }
                    }
                }
                if ($homeworkresult != false) {
                    foreach ($homeworkresult as $homevalue) {
                        if ($homevalue['lesson_plan_sub_id'] == $resultvalue['lesson_plan_sub_id']) {
                            $homework = $homevalue['tab'];
                        }
                    }
                }

                foreach ($task as $taskvalue) {
                    if ($taskvalue != 'N.A.') {
                        $sendemail = 1;
                    }
                    $taskd[] = $taskvalue;
                }
                $taskdata.="<tr><td>Subject:</td><td>Timings:</td><td>B&L</td><td>Classwork Proficiency</td><td>Homework</td></tr>";
                $subco = 1;
                $taskcount = count($time_interval);
                foreach ($time_interval as $keyt => $timevalue) {

                    if ($subco == 1) {
                        $taskdata.="<tr><td rowspan=" . $taskcount . "> $subjectname </td><td>$timevalue</td><td>" . $task[$keyt] . "</td><td rowspan=" . $taskcount . ">$classwork</td><td rowspan=" . $taskcount . ">$homework</td></tr>";
                    } else {

                        $taskdata.="<tr><td>$timevalue</td><td>" . $task[$keyt] . "</td></tr>";
                    }
                    $subco++;
                }
            }

            if ($sendemail == 1) {

                if ($this->valid_email($email)) {

                    $headers = "From: Workshop  <info@ueisworkshop.com>" . PHP_EOL;
                    $headers .= 'MIME-Version: 1.0' . PHP_EOL;
                    $headers .= 'Content-Type: text/html; charset=iso-8859-1' . PHP_EOL;
                    $headers .= 'X-Mailer: PHP/' . phpversion() . PHP_EOL;
                    $subject = "Behavior & Learning Details For $date";
                    $message = "<html><head><title>Behavior & Learning Details For $date</title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> Details </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' border='1' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> Greetings $name,</td></tr>
				 <tr><td>Student Name:</td><td> $studentname </td></tr> $taskdata
				<tr> <td colspan='2'> Powered By, </td></tr><tr><td colspan='2'> Workshop </td></tr></table></body></html>";

                    mail($email, $subject, $message, $headers);
                }

                if ($this->valid_email($emailsub)) {

                    $headers = "From: Workshop  <info@ueisworkshop.com>" . PHP_EOL;
                    $headers .= 'MIME-Version: 1.0' . PHP_EOL;
                    $headers .= 'Content-Type: text/html; charset=iso-8859-1' . PHP_EOL;
                    $headers .= 'X-Mailer: PHP/' . phpversion() . PHP_EOL;
                    $subject = " Behavior & Learning Details For $date";
                    $message = "<html><head><title>Behavior & Learning Details For $date</title></head><body><table height='40px'  width='100%' style='background-color:#ccc'><tr><td><font color='white' size='5px'> Details </font></td></tr></table><table height='10px'  width='100%' ><tr><td></td></tr></table><table cellpadding='4' cellspacing='1' width='100%' style='border: 10px solid #ccc;'><tr><td colspan='2'> Greetings $namesub,</td></tr>
				 <tr><td>Student Name:</td><td> $studentname </td></tr> $taskdata
				<tr> <td colspan='2'> Powered By, </td></tr><tr><td colspan='2'> Workshop </td></tr></table></body></html>";

                    mail($emailsub, $subject, $message, $headers);
                }
            }
        }


        $data['status'] = 'success';
        echo json_encode($data);
        exit;
    }

    function getstudentreport() {
        $data['idname'] = 'tools';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));
            $data['students'] = $this->parentmodel->get_students_all();
        } else {

	 if($this->session->userdata("login_type")=='user')
		  {
			  
				$this->load->model('selectedstudentreportmodel');
				/*$data['records'] = $this->Studentdetailmodel->getstuddetail();*/
				$district_id = $this->session->userdata('district_id');
				
				$data['school_type'] = $this->selectedstudentreportmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentdetailmodel->getcountries($data['records']);
			}
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
			
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/value_view', $data);
    }

        function gethomeworkrecord() {
        $this->load->Model('parentmodel');
        //$data['$studentresult'] = $this->parentmodel->get_students_all();
        $this->load->Model('lessonplanmodel');
        $this->load->Model('parentmodel');
        $data['homework_profeciency'] = $this->lessonplanmodel->hw_proficiency_by_district();
       // print_r($data['homework_profeciency']);exit;
//        echo $this->session->userdata('current_plan_id');exit;
        if ($this->session->userdata('current_plan_id')) {
            $lesson_week_plan_id = $this->session->userdata('current_plan_id');
        } else {
            $lesson_week_plan_id = $this->input->post('lesson_week_plan_id');
        }
        
//        echo $this->input->post('report_date');exit;
            if($this->input->post('report_date')) {
            $ndays = date("t", strtotime($this->input->post('report_date')));

            $dayn = $this->week_from_monday($this->input->post('report_date'), $ndays);
            $reportdateArr = explode('-', $this->input->post('report_date'));
            $properdate = $reportdateArr[2] . '-' . $reportdateArr[0] . '-' . $reportdateArr[1];
            $dayn = date('N', strtotime($properdate));
            if($dayn==7) $dayn=0;
//            print_r($dayn);exit;
        } else {
             $dayn = $this->session->userdata('current_day');
        }
        //print_r($dayn);exit;
        
        if ($this->session->userdata('login_type') == 'teacher') {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id = $this->session->userdata('observer_homework_feature_id');
        }
        if($teacher_id=='' && $this->input->post(teacher_id)){
            $teacher_id = $this->input->post(teacher_id);
        }
        
        $studentresult = $data['studentresult'] = $this->parentmodel->get_students_class_all($lesson_week_plan_id, $dayn, $teacher_id);
//        echo $this->db->last_query();
//print_r($studentresult);exit;
    if ($data['studentresult'] != false) {

            $this->load->Model('lessonplanmodel');
            $subjects = $this->lessonplanmodel->getlessonplansbyid($lesson_week_plan_id);
            foreach ($subjects as $subjectvalue) {
                $studentcnt = 0;
                foreach ($studentresult as $students) {
                    $checkresult = $this->lessonplanmodel->check_value_homework($subjectvalue['subject_id'], $students['student_id']);
//                    print_r($checkresult);exit;
                    $data['studentresult'][$studentcnt]['homework_feature_id'] =  $studentresult[$studentcnt]['homework_feature_id'] = $checkresult[0]['homework_feature_id'];
                    $data['studentresult'][$studentcnt]['task'] =  $studentresult[$studentcnt]['task'] = $checkresult[0]['task'];
                    $data['studentresult'][$studentcnt]['assignment_type'] =  $studentresult[$studentcnt]['assignment_type'] = $checkresult[0]['assignment_type'];
                    $data['studentresult'][$studentcnt]['assignment_name'] =  $studentresult[$studentcnt]['assignment_name'] = $checkresult[0]['assignment_name'];
                    if ($checkresult == false) {
                                
                        $result_id = $this->lessonplanmodel->add_value_homework($subjectvalue['subject_id'], $students['student_id']);
                        $data['studentresult'][$studentcnt]['homework_feature_id']=$studentresult[$studentcnt]['homework_feature_id'] = $result_id;
                    }
                    $studentcnt++;
                }
            }

//            print_r($data['studentresult']);exit;
            $this->load->Model('teachermodel');
            $data['teacher'] = $this->teachermodel->getteacherById($teacher_id);
            if ($data['teacher'] != false) {
                $data['teacher_name'] = $data['teacher'][0]['firstname'] . ' ' . $data['teacher'][0]['lastname'];
            } else {
                $data['teacher_name'] = '';
            }


            /* end lessonplan  */
//            print_r($data);exit;
            $data['view_path'] = $this->config->item('view_path');
            $this->load->view('teacherself/enterhomeworkajax', $data);
            //  $this->load->view('teacherself/value_homework',$data);
        } else {

//            $this->value_homework('No Students Found');
            echo "No Student Found";exit;
        }
        //print_r($data);exit;
    }

    function gethomeworkstudentreport() {
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'tools/implementation_manager');
        }
        $data['idname'] = 'tools';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));

            $data['students'] = $this->parentmodel->get_students_all();
        } else {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/value_homework_view', $data);
    }

    function get_retrieve_homework_record() {
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'implementation');
        }
        $data['idname'] = 'implementation';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
//            echo $this->db->last_query();exit;
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));

            $data['students'] = $this->parentmodel->get_students_all();
        } else {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/retrieve_homework_record', $data);
    }

    function geteldstudentreport() {

        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'tools/implementation_manager');
        }
        $data['idname'] = 'tools';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));

            $data['students'] = $this->parentmodel->get_students_all();
        } else {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/value_eld_view', $data);
    }

    function getclassworkstudentreport() {
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'tools/implementation_manager');
        }
        $data['idname'] = 'tools';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));
            $data['students'] = $this->parentmodel->get_students_all();
        } else {
            
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/value_classwork', $data);
    }
	
	 function getclassworkstudentgraph() {
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'tools/implementation_manager');
        }
        $data['idname'] = 'tools';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));
            $data['students'] = $this->parentmodel->get_students_all();
        } else {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/value_classwork_graph', $data);
    }

    function get_retrieve_grade() {
        if ($this->session->userdata('login_type') == 'user') {
            $this->session->set_flashdata('permission', 'Additional Permissions Required');
            redirect(base_url() . 'implementation');
        }
        $data['idname'] = 'implementation';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');

        if ($this->session->userdata('login_type') == 'teacher') {
            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();

			
        } else if ($this->session->userdata('login_type') == 'parent') {
            $this->load->Model('parentmodel');
            $data['teachers'] = $this->parentmodel->get_teachers_by_parentid($this->session->userdata('login_id'));
            $data['students'] = $this->parentmodel->get_students_all();
        } else {
            $this->load->Model('teachermodel');
            $data['teachers'] = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));

            $this->load->Model('parentmodel');
            $data['students'] = $this->parentmodel->get_students_all();
        }
        $this->load->view('teacherself/retrieve_grade', $data);
    }

    function get_student_report() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');

        $data['studentname'] = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        $data['month'] = $month;

        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select 

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select 

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);                 
        $this->load->Model('lessonplanmodel');
        $this->load->Model('valueplanmodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }
        $studentresult = $data['studentresult'] = $this->lessonplanmodel->getweekdata($month, $year, $student_id, $ndays);
        $valueplans = $data['valueplans'] = $this->valueplanmodel->getallplans();
        for($pj=0;$pj<count($weeks);$pj++)
        {
          if(!empty($studentresult))
          {
          foreach($valueplans as $key=>$valupl)
            {
            $exist=0;
          foreach($studentresult as $studenvalue)
          {
            
            
            
            if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab'])
            {
                
                $notify[$key][$pj]=intval($studenvalue['count']);
                $exist=1;
            }
            
            
           }
           if($exist==0)
            {
              $notify[$key][$pj]=0;
            }
          }
          
          }
          else
          {
          foreach($valueplans as $key=>$valupl)
            {
            $notify[$key][$pj]=0;
            }
          }
         $weeksdata[]=$weeks[$pj]['week']   ;
        }
        
            
      
        foreach($valueplans as $key=>$valupl)
            {
            
        
        //$MyData->addPoints($notify[$key],$valupl['tab']);
//      $$chart->series[] = array('name' => $valupl['tab'],'data' => $notify[$key]);    
        
        $high['series'][] = array('name' => $valupl['tab'],'data' => $notify[$key]);
        }
                $high['xAxis']['categories'] = $weeksdata;
                 $high['chart']['type'] = "column";         
            $high['title']['text'] = "Academic Behaviors";
//                        $high['xAxis']['labels']['rotation'] = -50;   
                        $high['yAxis']['min'] = 0;
            $high['yAxis']['allowDecimals'] =false;
                        $high['yAxis']['title']['text'] = "";
                        $high['yAxis']['stackLabels']['enabled'] = 1;
                        $high['yAxis']['stackLabels']['style']['fontWeight'] = "bold";
                        $high['legend']['layout'] = 'horizontal';
                        $high['legend']['align'] = 'top';
                        $high['legend']['verticalAlign'] = 'top';
                        $high['legend']['x'] = 20;
                        $high['legend']['y'] = 15;
                        $high['credits']['enabled'] = false;
                        $high['legend']['itemStyle'] = array("fontWeight"=>"normal");
                         $high['xAxis']['labels']['autoRotation'] = array(-70);
//                        echo json_encode($high);exit;
                        //echo WORKSHOP_FILES;exit;
                        $myfile = fopen(WORKSHOP_FILES."mediagenerated/newfile_$student_id.json", "w") or die("Unable to open file!");
                        $txt = json_encode($high);
                        fwrite($myfile, $txt);
                        fclose($myfile);
                        
                        unlink("/var/www/html/system/application/views/inc/logo/effort_$student_id.png");
                        
                        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/newfile_$student_id.json -outfile /var/www/html/system/application/views/inc/logo/effort_$student_id.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
                         exec($command,$output);
        
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/value_report', $data);
        // $this->load->view('teacherself/value_view',$data);
    }

    function get_student_homework_report() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');

        $data['studentname'] = $studentdata[0]['firstname'] . ' ' . $studentdata[0]['lastname'];
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                $dsa = $ds[0] . '/' . $ds[1] . '/' . substr($ds[2], 2, 2);

                $weeks[$key]['week'] = $dsa . " (" . substr($weekval['week'], 0, 3) . ")";
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select	

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                $dsa = $ds[0] . '/' . $ds[1] . '/' . substr($ds[2], 2, 2);

                $weeks[$key]['week'] = $dsa . " (" . substr($weekval['week'], 0, 3) . ")";
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select	

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);		 		 	

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/value_homework_report', $data);
        //$this->load->view('teacherself/value_homework_view',$data);
    }

    function get_retrieve_homework_report() {
        $data['idname'] = 'implementation';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentdetails($student_id);
        
        

        $data['studentname'] = $studentdata[0]->firstname . ' ' . $studentdata[0]->lastname;
        
        
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                $dsa = $ds[0] . '/' . $ds[1] . '/' . substr($ds[2], 2, 2);

                $weeks[$key]['week'] = $dsa . " (" . substr($weekval['week'], 0, 3) . ")";
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select	

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                $dsa = $ds[0] . '/' . $ds[1] . '/' . substr($ds[2], 2, 2);

                $weeks[$key]['week'] = $dsa . " (" . substr($weekval['week'], 0, 3) . ")";
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select	

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);		 		 	

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/retrieve_homework_report', $data);
        //$this->load->view('teacherself/value_homework_view',$data);
    }

    function get_student_eld_report() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');
        
         

        $data['studentname'] = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select 

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select 

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);                 
        $this->load->Model('lessonplanmodel');
        $this->load->Model('eldrubricmodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }
        $studentresult = $data['studentresult'] = $this->lessonplanmodel->geteldweekdata($month, $year, $student_id, $ndays);
        $valueplans = $data['valueplans'] = $this->eldrubricmodel->getAllsubgroupsbydistrict();
        
        for($pj=0;$pj<count($weeks);$pj++)
        {
          if(!empty($studentresult))
          {
          foreach($valueplans as $key=>$valupl)
            {
            $exist=0;
          foreach($studentresult as $studenvalue)
          {
            
            
            
            if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab'])
            {
                
                $notify[$key][$pj]=intval($studenvalue['count']);
                $exist=1;
            }
            
            
           }
           if($exist==0)
            {
              $notify[$key][$pj]=0;
            }
          }
          
          }
          else
          {
          foreach($valueplans as $key=>$valupl)
            {
            $notify[$key][$pj]=0;
            }
          }
         $weeksdata[]=$weeks[$pj]['week']   ;
        }
        
            
      
        foreach($valueplans as $key=>$valupl)
            {
            
        
        //$MyData->addPoints($notify[$key],$valupl['tab']);
//      $$chart->series[] = array('name' => $valupl['tab'],'data' => $notify[$key]);    
        $high['series'][] = array('name' => $valupl['tab'],'data' => $notify[$key]);
        
        }
                $high['xAxis']['categories'] = $weeksdata;
                $high['chart']['type'] = "column";          
            $high['title']['text'] = "MONTHLY ENGLISH LANGUAGE DVELOPMENT";
//                        $high['xAxis']['labels']['rotation'] = -50;   
                        $high['yAxis']['min'] = 0;
            $high['yAxis']['allowDecimals'] =false;
                        $high['yAxis']['title']['text'] = "";
                        $high['yAxis']['stackLabels']['enabled'] = 1;
                        $high['yAxis']['stackLabels']['style']['fontWeight'] = "bold";
                        $high['legend']['layout'] = 'horizontal';
                        $high['legend']['align'] = 'top';
                        $high['legend']['verticalAlign'] = 'top';
                        $high['legend']['x'] = 20;
                        $high['legend']['y'] = 15;
                        $high['credits']['enabled'] = false;
                        $high['legend']['itemStyle'] = array("fontWeight"=>"normal");
                         $high['xAxis']['labels']['autoRotation'] = array(-70);
//                        echo json_encode($high);exit;
                        
                        
                        $myfile = fopen(WORKSHOP_FILES."mediagenerated/newfile_$student_id.json", "w") or die("Unable to open file!");
                        $txt = json_encode($high);
                        fwrite($myfile, $txt);
                        fclose($myfile);
                        
                        unlink("/var/www/html/system/application/views/inc/logo/eld_$student_id.png");
                        
                        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/newfile_$student_id.json -outfile /var/www/html/system/application/views/inc/logo/eld_$student_id.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
                         exec($command,$output);
                
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/value_eld_report', $data);
        // $this->load->view('teacherself/value_eld_view',$data);
    }

    function getstudentreportdata($month, $year, $student_id) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        $this->load->Model('valueplanmodel');

        $valueplans = $this->valueplanmodel->getallplans();


        if (!$sidx)
            $sidx = 1;

        //$this->load->Model('lessonplanmodel');
        //$this->load->Model('parentmodel');
        //$result = $this->parentmodel->get_students_all();


        $this->load->Model('lessonplanmodel');
        $studentresult = $this->lessonplanmodel->getmonthdata($month, $year, $student_id);
        $studentresulthead = $this->lessonplanmodel->getmonthdatacol($month, $year, $student_id);


        foreach ($studentresulthead as $value) {
            $newstudentresulthead[] = date("H:i", strtotime($value['time_interval']));
        }
        sort($newstudentresulthead);
        $sa = 0;
        foreach ($newstudentresulthead as $value) {
            $studentresulthead[$sa]['time_interval'] = date("g:i A", strtotime($value));

            $sa++;
        }
        /* echo '<pre>';
          print_r($studentresult);
          print_r($studentresulthead);
          exit; */

        $count = count($studentresulthead);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA





        if ($studentresulthead != false) {
            foreach ($studentresulthead as $key => $studentvaluehead) {


                echo "<row id='" . $key . "'>";
                echo "<cell><![CDATA[" . $studentvaluehead['time_interval'] . "]]></cell>";
                if ($month == 'all') {
                    for ($i = 1; $i <= 12; $i++) {
                        $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                        $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                        foreach ($days as $daysvalue) {

                            $k = 0;
                            foreach ($studentresult as $studvalue) {


                                if ($daysvalue['date'] == $studvalue['dates'] && $studentvaluehead['time_interval'] == $studvalue['time_interval']) {
                                    $k = 1;
                                    echo "<cell><![CDATA[" . $studvalue['tab'];
                                    if ($studvalue['tab'] != '') {
                                        echo "(" . $studvalue['interval'] . " Min)";
                                    }
                                    echo "]]></cell>";
                                }
                            }
                            if ($k == 0) {

                                echo "<cell><![CDATA[]]></cell>";
                            }
                        }
                    }
                } else {
                    $ndays = date("t", strtotime($year . '-' . $month . '-01'));
                    $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);

                    foreach ($days as $daysvalue) {

                        $k = 0;
                        foreach ($studentresult as $studvalue) {


                            if ($daysvalue['date'] == $studvalue['dates'] && $studentvaluehead['time_interval'] == $studvalue['time_interval']) {
                                $k = 1;
                                echo "<cell><![CDATA[" . $studvalue['tab'];
                                if ($studvalue['tab'] != '') {
                                    echo "(" . $studvalue['interval'] . " Min)";
                                }
                                echo "]]></cell>";
                            }
                        }
                        if ($k == 0) {

                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                }
                echo "</row>";
            }
        }
        echo "</rows>";
    }

    function geteldstudentreportdata($month, $year, $student_id,$teacher_id='') {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
//        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        
        $this->load->Model('eldrubricmodel');

        $valueplans = $this->eldrubricmodel->getAllsubgroupsbydistrict();

        
        if (!$sidx)
            $sidx = 1;

        //$this->load->Model('lessonplanmodel');
        //$this->load->Model('parentmodel');
        //$result = $this->parentmodel->get_students_all();


        $this->load->Model('lessonplanmodel');
        //$this->load->Model('lessonplansubmodel');
        $studentresult = $this->lessonplanmodel->geteldmonthdata($month, $year, $student_id,$teacher_id);
        //$studentresulthead = $this->lessonplansubmodel->getlessonplansubById(1); 
//        print_r($studentresult);exit;

        $studentresulthead = $this->eldrubricmodel->getAllsubgroupsbydistrict();
        // print_r($studentresulthead);




        $count = count($studentresulthead);
        // print_r($studentresult);
        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA





        if ($studentresulthead != false) {
            $s = 0;
            //foreach($studentresulthead as $key=>$studentvaluehead)
            {
                $s++;
                //if($s==1)
                {

                    echo "<row id='1'>";
                    echo "<cell><![CDATA[" . $studentresulthead[0]['subject_name'] . "]]></cell>";
                    if ($month == 'all') {
                        for ($i = 1; $i <= 12; $i++) {
                            $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                            $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                            foreach ($days as $daysvalue) {

                                $k = 0;
                                foreach ($studentresult as $studvalue) {


                                    if ($daysvalue['date'] == $studvalue['dates']) {
                                        $k = 1;
                                        echo "<cell><![CDATA[" . $studvalue['tab'];

                                        echo "]]></cell>";
                                    }
                                }
                                if ($k == 0) {

                                    echo "<cell><![CDATA[]]></cell>";
                                }
                            }
                        }
                    } else {
                        $ndays = date("t", strtotime($year . '-' . $month . '-01'));
                        $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
                        //print_r($days);
                        foreach ($days as $daysvalue) {

                            $k = 0;
                            foreach ($studentresult as $studvalue) {


                                if ($daysvalue['date'] == $studvalue['dates']) {
                                    $k = 1;
                                    echo "<cell><![CDATA[" . $studvalue['tab'];

                                    echo "]]></cell>";
                                }
                            }
                            if ($k == 0) {

                                echo "<cell><![CDATA[]]></cell>";
                            }
                        }
                    }
                    echo "</row>";
                }
            }
        }
        echo "</rows>";
    }

    function gethomeworkstudentreportdata($month, $year, $student_id) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        $this->load->Model('homework_proficiencymodel');

        $valueplans = $this->homework_proficiencymodel->getallplans();


        if (!$sidx)
            $sidx = 1;

        //$this->load->Model('lessonplanmodel');
        //$this->load->Model('parentmodel');
        //$result = $this->parentmodel->get_students_all();


        $this->load->Model('lessonplanmodel');
        //$this->load->Model('lessonplansubmodel');
        $studentresult = $this->lessonplanmodel->gethomeworkmonthdata($month, $year, $student_id);
//        echo $this->db->last_query();exit;
        //$studentresulthead = $this->lessonplansubmodel->getlessonplansubById(1); 

        $this->load->Model('dist_subjectmodel');
        //$studentresulthead=$this->dist_subjectmodel->getdist_subjectsById();
        if ($this->session->userdata('login_type') != 'teacher') {
            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('observer_feature_id'), $student_id);
        } else {

            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('teacher_id'), $student_id);
        }



//echo $this->db->last_query();exit;
        $count = count($studentresulthead);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA





        if ($studentresulthead != false) {
            foreach ($studentresulthead as $key => $studentvaluehead) {


                echo "<row id='" . $key . "'>";
                echo "<cell><![CDATA[" . $studentvaluehead['subject_name'] . "]]></cell>";
                if ($month == 'all') {
                    for ($i = 1; $i <= 12; $i++) {
                        $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                        $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                        
                        foreach ($days as $daysvalue) {

                            $k = 0;
                            foreach ($studentresult as $studvalue) {


                                if ($daysvalue['date'] == $studvalue['dates'] && $studentvaluehead['subject_id'] == $studvalue['lesson_plan_sub_id']) {
                                    $k = 1;
                                    echo "<cell><![CDATA[" . $studvalue['tab'];

                                    echo "]]></cell>";
                                }
                            }
                            if ($k == 0) {

                                echo "<cell><![CDATA[]]></cell>";
                            }
                        }
                    }
                } else {
                    $ndays = date("t", strtotime($year . '-' . $month . '-01'));
                    $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
                    
                  
                    foreach ($days as $daysvalue) {

                        $k = 0;
                        foreach ($studentresult as $studvalue) {
                            
//echo $daysvalue['date'].' == '.$studvalue['dates'];
                           // print('Next Date ' . date('m/d/Y', strtotime('-1 day', strtotime($studvalue['dates']))));
                            
                            if ($daysvalue['date'] == $studvalue['dates'] && $studentvaluehead['subject_id'] == $studvalue['lesson_plan_sub_id']) {
                                $k = 1;
                                echo "<cell><![CDATA[" . $studvalue['tab'];

                                echo "]]></cell>";
                            }
                        }
                        if ($k == 0) {

                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                }
                echo "</row>";
            }
        }
        echo "</rows>";
    }

    function get_class_student_report() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');

        $data['studentname'] = $studentdata[0]['firstname'] . ' ' . $studentdata[0]['lastname'];
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select	

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select	

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
                $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);                 
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classwork_proficiencymodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }
        $studentresult = $data['studentresult'] = $this->lessonplanmodel->getclassweekdata($month, $year, $student_id, $ndays);
        $valueplans = $data['valueplans'] = $this->classwork_proficiencymodel->getallplans();
        
              for($pj=0;$pj<count($weeks);$pj++)
        {
          if(!empty($studentresult))
          {
          foreach($valueplans as $key=>$valupl)
            {
            $exist=0;
          foreach($studentresult as $studenvalue)
          {
            
            
            
            if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab'])
            {
                
                $notify[$key][$pj]=intval($studenvalue['count']);
                $exist=1;
            }
            
            
           }
           if($exist==0)
            {
              $notify[$key][$pj]=0;
            }
          }
          
          }
          else
          {
          foreach($valueplans as $key=>$valupl)
            {
            $notify[$key][$pj]=0;
            }
          }
         $weeksdata[]=$weeks[$pj]['week']   ;
        }
//      exit;
                
//                print_r($notify);exit;
            
      
        foreach($valueplans as $key=>$valupl)
            {
            
        
        //$MyData->addPoints($notify[$key],$valupl['tab']);
        $high['series'][] = array('name' => $valupl['tab'],'data' => $notify[$key]);
                
        }
               
//                    $high['yAxis']['max'] = 50;
                $high['xAxis']['categories'] = $weeksdata;
               
                $high['chart']['type'] = "column";          
            $high['title']['text'] = "MONTHLY CLASSWORK PERFORMANCE";
//                        $high['xAxis']['labels']['rotation'] = -50;   
                        $high['yAxis']['min'] = 0;
            $high['yAxis']['allowDecimals'] =false;
                        $high['yAxis']['title']['text'] = "";
                        $high['yAxis']['stackLabels']['enabled'] = 1;
                        $high['yAxis']['stackLabels']['style']['fontWeight'] = "bold";
                        $high['legend']['layout'] = 'horizontal';
                        $high['legend']['align'] = 'top';
                        $high['legend']['verticalAlign'] = 'top';
                        $high['legend']['x'] = 20;
                        $high['legend']['y'] = 15;
                        $high['credits']['enabled'] = false;
                        $high['legend']['itemStyle'] = array("fontWeight"=>"normal");
                         $high['xAxis']['labels']['autoRotation'] = array(-70);
                        
//                        echo json_encode($high);exit;
                        
                        $myfile = fopen(WORKSHOP_FILES."mediagenerated/newfile_$student_id.json", "w") or die("Unable to open file!");
                        $txt = json_encode($high);
                        fwrite($myfile, $txt);
                        fclose($myfile);
                        
                        unlink("/var/www/html/system/application/views/inc/logo/chart_$student_id.png");
                        
                        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/newfile_$student_id.json -outfile /var/www/html/system/application/views/inc/logo/chart_$student_id.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
                         exec($command,$output);
        $data['studentresult'] = $this->lessonplanmodel->getclassweekdata($month, $year, $student_id, $ndays);
        $data['valueplans'] = $this->classwork_proficiencymodel->getallplans();
        $data['view_path'] = $this->config->item('view_path');

        $this->load->view('teacherself/value_class_report', $data);
    }
	
	 function get_class_student_graph() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {

            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');

        $data['studentname'] = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;

        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {

                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }


            for ($i = 1; $i <= 12; $i++) {



                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                /* $monthhead = array();
                  $monthhead["startColumnName"] = "value_0_".$i;
                  $monthhead["numberOfColumns"] =$ndays ;
                  $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
                  $monthhead=json_encode($monthhead);
                  $monthheaders.=$monthhead.',' */

                foreach ($days as $key => $value) {

                    if ($value['week'] == 'Sunday') {

                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {

                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select	

                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();


                $head["startColumnName"] = $startColumnName;


                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);

                //$dsa=$ds[0].'/'.$ds[1].'/'.substr($ds[2],2,2);
                $dsa = $ds[0] . '/' . $ds[1];
                //$weeks[$key]['week']=$dsa." (".substr($weekval['week'],0,3).")";
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            /* $monthhead = array();
              $monthhead["startColumnName"] = "value_0_".$i;
              $monthhead["numberOfColumns"] =$ndays ;
              $monthhead["titleText"] = date( 'F', mktime(0, 0, 0, $i) );
              $monthhead=json_encode($monthhead);
              $monthheaders.=$monthhead.',' */
            foreach ($days as $key => $value) {

                if ($value['week'] == 'Sunday') {

                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {

                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select	

                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();


            $head["startColumnName"] = $startColumnName;


            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        /* echo $cols;
          echo '<br/>';
          echo $daysa;
          echo '<br/>';
          echo $headers;
          echo '<br/>';
          exit; */
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
//$data['monthheaders']=substr($monthheaders,0,-1);		 		 	
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classwork_proficiencymodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
        }

        $studentresult = $data['studentresult'] = $this->lessonplanmodel->getclassweekdata($month, $year, $student_id, $ndays);
        $valueplans = $data['valueplans'] = $this->classwork_proficiencymodel->getallplans();
        
              for($pj=0;$pj<count($weeks);$pj++)
        {
          if(!empty($studentresult))
          {
          foreach($valueplans as $key=>$valupl)
            {
            $exist=0;
          foreach($studentresult as $studenvalue)
          {
            
            
            
            if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab'])
            {
                
                $notify[$key][$pj]=intval($studenvalue['count']);
                $exist=1;
            }
            
            
           }
           if($exist==0)
            {
              $notify[$key][$pj]=0;
            }
          }
          
          }
          else
          {
          foreach($valueplans as $key=>$valupl)
            {
            $notify[$key][$pj]=0;
            }
          }
         $weeksdata[]=$weeks[$pj]['week']   ;
        }
//      exit;
                
//                print_r($notify);exit;
            
      
        foreach($valueplans as $key=>$valupl)
            {
            
        
        //$MyData->addPoints($notify[$key],$valupl['tab']);
        $high['series'][] = array('name' => $valupl['tab'],'data' => $notify[$key]);
                
        }
               
//                    $high['yAxis']['max'] = 50;
                $high['xAxis']['categories'] = $weeksdata;
               
                $high['chart']['type'] = "column";          
            $high['title']['text'] = "MONTHLY CLASSWORK PERFORMANCE";
//                        $high['xAxis']['labels']['rotation'] = -50;   
                        $high['yAxis']['min'] = 0;
            $high['yAxis']['allowDecimals'] =false;
                        $high['yAxis']['title']['text'] = "";
                        $high['yAxis']['stackLabels']['enabled'] = 1;
                        $high['yAxis']['stackLabels']['style']['fontWeight'] = "bold";
                        $high['legend']['layout'] = 'horizontal';
                        $high['legend']['align'] = 'top';
                        $high['legend']['verticalAlign'] = 'top';
                        $high['legend']['x'] = 20;
                        $high['legend']['y'] = 15;
                        $high['credits']['enabled'] = false;
                        $high['legend']['itemStyle'] = array("fontWeight"=>"normal");
                         $high['xAxis']['labels']['autoRotation'] = array(-70);
                        
//                        echo json_encode($high);exit;
                        
                        $myfile = fopen(WORKSHOP_FILES."mediagenerated/newfile_$student_id.json", "w") or die("Unable to open file!");
                        $txt = json_encode($high);
                        fwrite($myfile, $txt);
                        fclose($myfile);
                        
                        unlink("/var/www/html/system/application/views/inc/logo/chart_$student_id.png");
                        
                        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/newfile_$student_id.json -outfile /var/www/html/system/application/views/inc/logo/chart_$student_id.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
                         exec($command,$output);

        $data['studentresult'] = $this->lessonplanmodel->getclassweekdata($month, $year, $student_id, $ndays);
        $data['valueplans'] = $this->classwork_proficiencymodel->getallplans();
        $data['view_path'] = $this->config->item('view_path');

        $this->load->view('teacherself/value_class_graph', $data);
    }

    function retrieve_grade_report() {
        $data['idname'] = 'implementation';
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        if ($this->session->userdata('login_type') != 'teacher') {
            $this->session->set_userdata('observer_feature_id', $this->input->post('teacher_id'));
        }
        $student_id = $this->input->post('student');
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');
        $data['studentname'] = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        $data['month'] = $month;
        if ($month == 'all') {
            $data['monthname'] = 'All';
        } else {
            $data['monthname'] = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $data['student_id'] = $student_id;
        $cols = '';
        $daysa = '';
        $weeks = array();
        $headers = '';
        $monthheaders = '';
        $k = 1;
        if ($month == 'all') {
            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);
                $dsa = $ds[0] . '/' . $ds[1];
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            for ($i = 1; $i <= 12; $i++) {
                $k = 1;
                $week = 0;
                $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                foreach ($days as $key => $value) {
                    if ($value['week'] == 'Sunday') {   
                        $week++;
                        $head = array();
                        if ($week == 1) {
                            $head["startColumnName"] = 'value_0_' . $i;
                        } else {
                            $head["startColumnName"] = $startColumnName;
                        }
                        $head["numberOfColumns"] = $k;
                        $head["titleText"] = 'Week' . $week;
                        $head = json_encode($head);
                        $headers.=$head . ',';
                        $k = 0;
                    }
                    if ($value['week'] == 'Monday') {
                        $startColumnName = 'value_' . $key . '_' . $i;
                    }
                    $k++;
                    $col = array();
                    $col["name"] = "value_" . $key . '_' . $i;
                    $col["index"] = "time_" . $key . '_' . $i;
                    $col["width"] = 100;
                    $col["align"] = "left"; // render as select 
                    $newas = json_encode($col);
                    $cols.=$newas . ',';
                    $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
                }
                $week++;
                $head = array();
                $head["startColumnName"] = $startColumnName;
                $head["numberOfColumns"] = $k - 1;
                $head["titleText"] = 'Week' . $week;
                $head = json_encode($head);
                $headers.=$head . ',';
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);
                $dsa = $ds[0] . '/' . $ds[1];
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $week = 0;
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($days as $key => $value) {
                if ($value['week'] == 'Sunday') {
                    $week++;
                    $head = array();
                    if ($week == 1) {
                        $head["startColumnName"] = 'value_0';
                    } else {
                        $head["startColumnName"] = $startColumnName;
                    }
                    $head["numberOfColumns"] = $k;
                    $head["titleText"] = 'Week' . $week;
                    $head = json_encode($head);
                    $headers.=$head . ',';
                    $k = 0;
                }
                if ($value['week'] == 'Monday') {
                    $startColumnName = 'value_' . $key;
                }
                $k++;
                $col = array();
                $col["name"] = "value_" . $key;
                $col["index"] = "time_" . $key;
                $col["width"] = 100;
                $col["align"] = "left"; // render as select 
                $newas = json_encode($col);
                $cols.=$newas . ',';
                $daysa.="'" . $value['date'] . "(" . substr($value['week'], 0, 3) . ")',";
            }
            $week++;
            $head = array();
            $head["startColumnName"] = $startColumnName;
            $head["numberOfColumns"] = $k;
            $head["titleText"] = 'Week' . $week;
            $head = json_encode($head);
            $headers.=$head . ',';
        }
        $data['cols'] = substr($cols, 0, -1);
        $data['dates'] = substr($daysa, 0, -1);
        $data['weeks'] = $weeks;
        $data['headers'] = substr($headers, 0, -1);
        $this->load->Model('lessonplanmodel');
        $this->load->Model('classwork_proficiencymodel');
        if ($month == 'all') {
            $ndays = date("t", strtotime($year . '-01-01'));
        }
        $studentresult = $data['studentresult'] = $this->lessonplanmodel->getclassweekdata($month, $year, $student_id, $ndays);
        $valueplans = $data['valueplans'] = $this->classwork_proficiencymodel->getallplans();
        for($pj=0;$pj<count($weeks);$pj++) {
            if(!empty($studentresult)) {
                foreach($valueplans as $key=>$valupl) {
                    $exist=0;
                    foreach($studentresult as $studenvalue) {
                        if($studenvalue['dates']==$weeks[$pj]['dates'] && $valupl['tab']==$studenvalue['tab']) {
                            $notify[$key][$pj]=intval($studenvalue['count']);
                            $exist=1;
                        }
                    }
                    if($exist==0) {
                        $notify[$key][$pj]=0;
                    }
                }
            } else {
                foreach($valueplans as $key=>$valupl) {
                    $notify[$key][$pj]=0;
                }
            }
            $weeksdata[]=$weeks[$pj]['week']   ;
        }
        foreach($valueplans as $key=>$valupl) {
            $high['series'][] = array('name' => $valupl['tab'],'data' => $notify[$key]);
        }
        $high['xAxis']['categories'] = $weeksdata;
        $high['chart']['type'] = "column";          
        $high['title']['text'] = "MONTHLY CLASSWORK PERFORMANCE";
        $high['yAxis']['min'] = 0;
        $high['yAxis']['allowDecimals'] =false;
        $high['yAxis']['title']['text'] = "";
        $high['yAxis']['stackLabels']['enabled'] = 1;
        $high['yAxis']['stackLabels']['style']['fontWeight'] = "bold";
        $high['legend']['layout'] = 'horizontal';
        $high['legend']['align'] = 'top';
        $high['legend']['verticalAlign'] = 'top';
        $high['legend']['x'] = 20;
        $high['legend']['y'] = 15;
        $high['credits']['enabled'] = false;
        $high['legend']['itemStyle'] = array("fontWeight"=>"normal");
        $high['xAxis']['labels']['autoRotation'] = array(-70);
        $myfile = fopen(WORKSHOP_FILES."mediagenerated/newfile_$student_id.json", "w") or die("Unable to open file!");
        $txt = json_encode($high);
        fwrite($myfile, $txt);
        fclose($myfile);
        unlink("/var/www/html/system/application/views/inc/logo/chart_$student_id.png");
        $command = "/usr/local/bin/phantomjs /var/www/html/phantomjs/highcharts-convert.js -infile ".WORKSHOP_FILES."mediagenerated/newfile_$student_id.json -outfile /var/www/html/system/application/views/inc/logo/chart_$student_id.png -scale 2.5 -width 700 -constr Chart -callback /usr/local/bin/callback.js 2>&1";
        exec($command,$output);
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/get_retrieve_grade_report', $data);
    }

    function getclassstudentreportdata($month, $year, $student_id) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction
        $this->load->Model('classwork_proficiencymodel');

        $valueplans = $this->classwork_proficiencymodel->getallplans();


        if (!$sidx)
            $sidx = 1;

        //$this->load->Model('lessonplanmodel');
        //$this->load->Model('parentmodel');
        //$result = $this->parentmodel->get_students_all();


        $this->load->Model('lessonplanmodel');
        $this->load->Model('dist_subjectmodel');
        //$studentresulthead=$this->dist_subjectmodel->getdist_subjectsById();
        if ($this->session->userdata('login_type') != 'teacher') {
            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('observer_feature_id'), $student_id);
        } else {

            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('teacher_id'), $student_id);
        }
        $studentresult = $this->lessonplanmodel->getclassmonthdata($month, $year, $student_id);
        //print_r($studentresult);
        //$studentresulthead = $this->lessonplansubmodel->getlessonplansubById(1);  
        //print_r($studentresulthead);
        //exit;

        /* foreach($studentresulthead as $value)
          {
          $newstudentresulthead[]=date("H:i", strtotime($value['time_interval']));



          }
          sort($newstudentresulthead);
          $sa=0;
          foreach($newstudentresulthead as $value)
          {
          $studentresulthead[$sa]['time_interval']=date("g:i a", strtotime($value));

          $sa++;

          } */


        $count = count($studentresulthead);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA





        if ($studentresulthead != false) {
            foreach ($studentresulthead as $key => $studentvaluehead) {


                echo "<row id='" . $key . "'>";
                echo "<cell><![CDATA[" . $studentvaluehead['subject_name'] . "]]></cell>";
                if ($month == 'all') {
                    for ($i = 1; $i <= 12; $i++) {
                        $ndays = date("t", strtotime($year . '-' . $i . '-01'));
                        $days = $this->week_from_monday('01-' . $i . '-' . $year, $ndays);
                        foreach ($days as $daysvalue) {

                            $k = 0;
                            foreach ($studentresult as $studvalue) {


                                if ($daysvalue['date'] == $studvalue['dates'] && $studentvaluehead['subject_id'] == $studvalue['lesson_plan_sub_id']) {
                                    $k = 1;
                                    echo "<cell><![CDATA[" . $studvalue['tab'];

                                    echo "]]></cell>";
                                }
                            }
                            if ($k == 0) {

                                echo "<cell><![CDATA[]]></cell>";
                            }
                        }
                    }
                } else {
                    $ndays = date("t", strtotime($year . '-' . $month . '-01'));
                    $days = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);

                    foreach ($days as $daysvalue) {

                        $k = 0;
                        foreach ($studentresult as $studvalue) {


                            if ($daysvalue['date'] == $studvalue['dates'] && $studentvaluehead['subject_id'] == $studvalue['lesson_plan_sub_id']) {
                                $k = 1;
                                echo "<cell><![CDATA[" . $studvalue['tab'];

                                echo "]]></cell>";
                            }
                        }
                        if ($k == 0) {

                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                }
                echo "</row>";
            }
        }
        echo "</rows>";
    }

    function studentdatapdf($month, $year, $student_id) {
        
        $data['view_path'] = $this->config->item('view_path');
        $this->load->Model('lessonplanmodel');
        $studentresult = $this->lessonplanmodel->getmonthdata($month, $year, $student_id);
        $studentresulthead = $this->lessonplanmodel->getmonthdatacol($month, $year, $student_id);
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');
        $this->load->Model('grade_subjectmodel');
        $data['grade'] = $this->grade_subjectmodel->getGradeById($studentdata[0]['grade']);
        $this->load->Model('valueplanmodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
             $start = $year . '-' . $month . '-01';
            $end = $year . '-12-31';
             $data['weeksnumber'] = $this->getWeeks($start,$end);
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
             $start = $year . '-' . $month . '-01';
            $end = $year . '-' . $month . '-'.$ndays;
             $data['weeksnumber'] = $this->getWeeks($start,$end);
        }
        $studentresultweek = $this->lessonplanmodel->getweekdata($month, $year, $student_id, $ndays);
        $valueplans = $this->valueplanmodel->getallplans();


        $studentname = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];

        if ($studentresulthead != false) {
            foreach ($studentresulthead as $value) {
                $newstudentresulthead[] = date("H:i", strtotime($value['time_interval']));
            }
            sort($newstudentresulthead);
            $sa = 0;
            foreach ($newstudentresulthead as $value) {
                $studentresulthead[$sa]['time_interval'] = date("g:i A", strtotime($value));

                $sa++;
            }
        }

      /*if ($month == 'all') {
            $ndays = date("t", strtotime($year . '-01-01'));
            $weekdays = $this->week_from_monday('01-01-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);
                $dsa = $ds[0] . '/' . $ds[1];
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }

            $j = 0;
            for ($p = 1; $p <= 12; $p++) {
                $i = 0;
                if ($p != 1) {
                    $j++;
                }
                $ndays = date("t", strtotime($year . '-' . $p . '-01'));
                $dayss = $this->week_from_monday('01-' . $p . '-' . $year, $ndays, true);

                foreach ($dayss as $key => $dayssplits) {

                    if ($i % 7 == 0) {
                        $j++;
                    }
                    $dayssplit[$j][] = $dayssplits;
                    $i++;
                }
            }
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $weekdays = $this->week_from_monday('01-' . $month . '-' . $year, $ndays);
            foreach ($weekdays as $key => $weekval) {
                $ds = explode('/', $weekval['date']);
                $dsa = $ds[0] . '/' . $ds[1];
                $weeks[$key]['week'] = $dsa;
                $weeks[$key]['dates'] = $weekval['date'];
            }
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $dayss = $this->week_from_monday('01-' . $month . '-' . $year, $ndays, true);
            $i = 0;
            $j = 0;
            foreach ($dayss as $key => $dayssplits) {
                if ($i % 7 == 0) {
                    $j++;
                }
                $dayssplit[$j][] = $dayssplits;
                $i++;
            }
        }*/

       

        //$str.= "</table></td></tr></table>";
        


       
        if ($month != 'all') {
            $month = date('F', mktime(0, 0, 0, $month));
        }
        $cach = date("H:i:s");
        $login_type = $this->session->userdata('login_type');

        if ($login_type == 'teacher') {
            $login_id = $this->session->userdata('teacher_id');
        } else if ($login_type == 'observer') {
            $login_id = $this->session->userdata('observer_id');
        }
        $MyData = new pData();

        for ($pj = 0; $pj < count($weeks); $pj++) {
            if (!empty($studentresultweek)) {
                foreach ($valueplans as $key => $valupl) {
                    $exist = 0;
                    foreach ($studentresultweek as $studenvalue) {



                        if ($studenvalue['dates'] == $weeks[$pj]['dates'] && $valupl['tab'] == $studenvalue['tab']) {

                            $notify[$key][$pj] = $studenvalue['count'];
                            $exist = 1;
                        }
                    }
                    if ($exist == 0) {
                        $notify[$key][$pj] = 0;
                    }
                }
            }
            $weeksdata[] = $weeks[$pj]['week'];
        }


        if($this->session->userdata('login_type')=='teacher'){
            $teachername = $this->session->userdata('teacher_name');
        } else {
            $this->load->model('teachermodel');
            $teacher_data = $this->teachermodel->getteacherById($studentresult[0]['teacher_id']);
//            print_r($teacher_data);exit;
            $teachername = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        }
       
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(1);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(1);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $data['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }
        $data['teachername'] = $teachername;
         $data['studentname'] = $studentname;
         $data['year'] = $year;
         $data['month'] = $month;
         $data['date'] = $date;
        
//         $data['dayssplit'] = $dayssplit;
         $data['studentresulthead'] = $studentresulthead;
         $data['studentresult'] = $studentresult;
         $data['student_id'] = $student_id;
        $cach = date("H:i:s");
        $this->output->enable_profiler(false);
        $this->load->library('parser');
//        print_r($data['teachername']);exit;
        $ostr = $this->parser->parse('teacherself/studentdatapdf', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();




        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();
    }

    function eldstudentdatapdf($month, $year, $student_id) {
        ini_set('memory_limit', '256M');
        $view_path = $this->config->item('view_path');
        $this->load->Model('lessonplanmodel');
        $this->load->Model('eldrubricmodel');
        $studentresult = $this->lessonplanmodel->geteldmonthdata($month, $year, $student_id);
        $studentresulthead = $this->eldrubricmodel->getAllsubgroupsbydistrict();
        $data['student_id'] = $student_id;
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');

        $this->load->Model('grade_subjectmodel');
        $data['grade'] = $this->grade_subjectmodel->getGradeById($studentdata[0]['grade']);
        if ($month == 'all') {
            $ndays = date("t", strtotime($year . '-01-01'));
            $start = $year . '-' . $month . '-01';
            $end = $year . '-12-31';
             $data['weeksnumber'] = $this->getWeeks($start,$end);
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $start = $year . '-' . $month . '-01';
            $end = $year . '-' . $month . '-'.$ndays;
             $data['weeksnumber'] = $this->getWeeks($start,$end);
             $data['monthnumber'] = $month;
        }
        $studentresultweek = $this->lessonplanmodel->geteldweekdata($month, $year, $student_id, $ndays);
        $valueplans = $this->eldrubricmodel->getAllsubgroupsbydistrict();
        $studentname = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        if($this->session->userdata('login_type')=='teacher'){
            $teachername = $this->session->userdata('teacher_name');
        } else {
            $this->load->model('teachermodel');
            $teacher_data = $this->teachermodel->getteacherById($studentresult[0]['teacher_id']);
            $teachername = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        }
       if ($month != 'all') {
            $month = date('F', mktime(0, 0, 0, $month));
        }
        $cach = date("H:i:s");
        $login_type = $this->session->userdata('login_type');
        if ($login_type == 'teacher') {
            $login_id = $this->session->userdata('teacher_id');
        } else if ($login_type == 'observer') {
            $login_id = $this->session->userdata('observer_id');
        }
        $MyData = new pData();
        for ($pj = 0; $pj < count($weeks); $pj++) {
            if (!empty($studentresultweek)) {
                foreach ($valueplans as $key => $valupl) {
                    $exist = 0;
                    foreach ($studentresultweek as $studenvalue) {
                        if ($studenvalue['dates'] == $weeks[$pj]['dates'] && $valupl['tab'] == $studenvalue['tab']) {
                            $notify[$key][$pj] = $studenvalue['count'];
                            $exist = 1;
                        }
                    }
                    if ($exist == 0) {
                        $notify[$key][$pj] = 0;
                    }
                }
            }
            $weeksdata[] = $weeks[$pj]['week'];
        }
        $this->load->Model('report_disclaimermodel');
        $reportdes = $this->report_disclaimermodel->getallplans(3);
        if ($reportdes != false) {

            $data['dis'] = $reportdes[0]['tab'];
            $data['fontsize'] = $reportdes[0]['size'];
            $data['fontcolor'] = $reportdes[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(3);
        $des = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }
        $data['view_path'] = $this->config->item('view_path');
        $data['teachername'] = $teachername;
        $data['studentname'] = $studentname;
        $data['year'] = $year;
        $data['month'] = $month;
        $data['date'] = $date;
        $data['studentresulthead'] = $studentresulthead;
        $data['studentresult'] = $studentresult;
        $cach = date("H:i:s");
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $ostr = $this->parser->parse('teacherself/eld_tracker_pdf', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();
    } 

    function classstudentdatapdf($month, $year, $student_id) {
        $data['student_id'] = $student_id;
        $data['view_path'] = $view_path = $this->config->item('view_path');
        $this->load->Model('lessonplanmodel');
        $studentresult = $this->lessonplanmodel->getclassmonthdata($month, $year, $student_id);
        $this->load->Model('dist_subjectmodel');
        if ($this->session->userdata('login_type') != 'teacher') {
            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('observer_feature_id'), $student_id);
        } else {

            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('teacher_id'), $student_id);
        }
        
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');
        if($studentdata){
       $this->load->Model('grade_subjectmodel');
        $data['grade'] = $this->grade_subjectmodel->getGradeById($studentdata[0]['grade']);
        }

        $this->load->Model('classwork_proficiencymodel');
        if ($month == 'all') {

            $ndays = date("t", strtotime($year . '-01-01'));
            $start = $year . '-' . $month . '-01';
        $end = $year . '-12-31';
         $data['weeksnumber'] = $this->getWeeks($start,$end);
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $start = $year . '-' . $month . '-01';
            $end = $year . '-' . $month . '-'.$ndays;
             $data['weeksnumber'] = $this->getWeeks($start,$end);
             $data['monthnumber'] = $month;
        }
        $studentresultweek = $this->lessonplanmodel->getclassweekdata($month, $year, $student_id, $ndays);
        
        $valueplans = $this->classwork_proficiencymodel->getallplans();
        $data['studentname'] = $studentname = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        if($this->session->userdata('login_type')=='teacher'){
            $teachername = $this->session->userdata('teacher_name');
        } else if($this->session->userdata('login_type')=='user'){
            $this->load->model('teachermodel');
            $teacher_data = $this->teachermodel->getteacherById($studentresult[0]['teacher_id']);
            $teachername = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        } else if($this->session->userdata('login_type')=='observer'){
            $this->load->model('teachermodel');
            $teacher_data = $this->teachermodel->getteacherById($this->session->userdata('observer_feature_id'));
            $teachername = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        }

             $data['teachername'] = $teachername;
        $data['studentresulthead'] = $studentresulthead;
        $data['studentresult'] = $studentresult;
        if ($month != 'all') {
            $data['month'] = $month = date('F', mktime(0, 0, 0, $month));
        }
        $data['year'] = $year;
        $cach = date("H:i:s");
        $login_type = $this->session->userdata('login_type');

        if ($login_type == 'teacher') {
            $login_id = $this->session->userdata('teacher_id');
        } else if ($login_type == 'observer') {
            $login_id = $this->session->userdata('observer_id');
        }

        for ($pj = 0; $pj < count($weeks); $pj++) {
            if (!empty($studentresultweek)) {
                foreach ($valueplans as $key => $valupl) {
                    $exist = 0;
                    foreach ($studentresultweek as $studenvalue) {
                        if ($studenvalue['dates'] == $weeks[$pj]['dates'] && $valupl['tab'] == $studenvalue['tab']) {
                            $notify[$key][$pj] = $studenvalue['count'];
                            $exist = 1;
                        }
                    }
                    if ($exist == 0) {
                        $notify[$key][$pj] = 0;
                    }
                }
            }
            $weeksdata[] = $weeks[$pj]['week'];
        }
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(2);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {

            $dis = $reportdis[0]['tab'];
            $fontsize = $reportdis[0]['size'];
            $fontcolor = $reportdis[0]['color'];
        }
        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(2);
        if ($reportdes != false) {

            $data['des'] = $reportdes[0]['tab'];
            $des['desfontsize'] = $reportdes[0]['size'];
            $data['desfontcolor'] = $reportdes[0]['color'];
        }
       $data['fontcolor'] = $fontcolor;
       $data['fontsize'] = $fontsize;
        $cach = date("H:i:s");
        $data['dis'] = $dis;
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $ostr = $this->parser->parse('teacherself/grade_tracker_pdf', $data, TRUE);
        
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();
    }

    function homeworkstudentdatapdf($month, $year, $student_id) {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', '60');
        $view_path = $this->config->item('view_path');
        $this->load->Model('lessonplanmodel');
        $studentresult = $this->lessonplanmodel->gethomeworkmonthdata($month, $year, $student_id);
        $this->load->Model('dist_subjectmodel');
        if ($this->session->userdata('login_type') != 'teacher') {
            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('observer_feature_id'), $student_id);
        } else {

            $studentresulthead = $this->dist_subjectmodel->student_subjects($this->session->userdata('teacher_id'), $student_id);
        }
        $this->load->Model('parentmodel');
        $studentdata = $this->parentmodel->getstudentById($student_id, 'student');
        $this->load->Model('grade_subjectmodel');
        $data['grade'] = $this->grade_subjectmodel->getGradeById($studentdata[0]['grade']);
        $this->load->Model('homework_proficiencymodel');
        if ($month == 'all') {
            $ndays = date("t", strtotime($year . '-01-01'));
            $start = $year . '-' . $month . '-01';
            $end = $year . '-12-31';
             $data['weeksnumber'] = $this->getWeeks($start,$end);
        } else {
            $ndays = date("t", strtotime($year . '-' . $month . '-01'));
            $start = $year . '-' . $month . '-01';
            $end = $year . '-' . $month . '-'.$ndays;
             $data['weeksnumber'] = $this->getWeeks($start,$end);
             $data['monthnumber'] = $month;
        }
        $valueplans = $this->homework_proficiencymodel->getallplans();
        $studentname = $studentdata[0]['firstname'] . '' . $studentdata[0]['lastname'];
        if($this->session->userdata('login_type')=='teacher'){
            $teachername = $this->session->userdata('teacher_name');
        } else if($this->session->userdata('login_type')=='user'){
            $this->load->model('teachermodel');
            $teacher_data = $this->teachermodel->getteacherById($studentresult[0]['teacher_id']);
            $teachername = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        }else if($this->session->userdata('login_type')=='observer'){
            $this->load->model('teachermodel');
             $teacher_data = $this->teachermodel->getteacherById($this->session->userdata('observer_feature_id'));
            $teachername = $teacher_data[0]['firstname'].' '.$teacher_data[0]['lastname'];
        }
        if ($month != 'all') {
            $month = date('F', mktime(0, 0, 0, $month));
        } else {
            $month = 'All';
        }
        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(7);
        $dis = '';
        $fontsize = 0;
        $fontcolor = 'cccccc';
        if ($reportdis != false) {
            $dis = $reportdis[0]['tab'];
            $fontsize = $reportdis[0]['size'];
            $fontcolor = $reportdis[0]['color'];
        }
        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(7);
        if ($reportdes != false) {
            $data['des'] = $reportdes[0]['tab'];
            $data['fontsize'] = $reportdes[0]['size'];
            $data['fontcolor'] = $reportdes[0]['color'];
        }
        $data['fontcolor'] = $fontcolor;
        $data['fontsize'] = $fontsize;
        $cach = date("H:i:s");
        $data['dis'] = $dis;
        $data['teachername'] = $teachername;
        $data['studentname'] = $studentname;
        $data['year'] = $year;
        $data['month'] = $month;
        $data['studentresulthead'] = $studentresulthead;
        $data['studentresult'] = $studentresult;
        $data['view_path'] = $view_path = $this->config->item('view_path');
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $ostr = $this->parser->parse('teacherself/homeworkstudentpdf', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();
    }
	
	
    function getstudents($id) {
        $this->load->Model('parentmodel');
        $data['students'] = $this->parentmodel->get_students_by_parentid($id);
        echo json_encode($data);
        exit;
    }

    function getparents($id) {
        $this->load->Model('parentmodel');
        $data['parents'] = $this->parentmodel->get_parents_all($id);
        echo json_encode($data);
        exit;
    }

    function getteacher_observation() {
        $data['idname'] = 'tools';
        if ($this->session->userdata('TE') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_observation', $data);
    }

    function getteacher_notification() {
        $data['idname'] = 'tools';

        if ($this->session->userdata('login_type') == 'user') {
            $this->load->Model('schoolmodel');
            $this->load->Model('school_typemodel');
            $data['school_types'] = $this->school_typemodel->getallplans();
            $data['schools'] = $this->schoolmodel->getschoolbydistrictwithtype($data['school_types'][0]['school_type_id']);
        }
//                 print_r($data);exit;
        $data['view_path'] = $this->config->item('view_path');


        $this->load->view('teacherself/teacher_notification', $data);
    }

    function getteacher_professional() {
        $data['idname'] = 'tools';
        if ($this->session->userdata('LP') == 0) {
            redirect("index");
        }
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_professional', $data);
    }

    function get_teacher_observation() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $data['year'] = $year;
        $cols = '';
        $headers = '';
        $form = '';
        for ($i = 1; $i <= 12; $i++) {
            $j = 1;
            $head = array();
            $head["startColumnName"] = "value_" . $i . '_' . $j;
            $head["numberOfColumns"] = 4;
            $head["titleText"] = date('F', mktime(0, 0, 0, $i));
            $head = json_encode($head);
            $headers.=$head . ',';

            for ($j = 1; $j <= 4; $j++) {
                $col = array();
                $col["name"] = "value_" . $i . '_' . $j;
                $col["index"] = "time_" . $i . '_' . $j;
                $col["width"] = 60;
                $col["align"] = "center"; // render as select	
                if ($j == 1) {
                    $form.="'Checklist',";
                }
                if ($j == 2) {
                    $form.="'Scale',";
                }
                if ($j == 3) {
                    $form.="'Proficiency',";
                }
                if ($j == 4) {
                    $form.="'Likert',";
                }
                $newas = json_encode($col);
                $cols.=$newas . ',';
            }
        }

        $data['cols'] = substr($cols, 0, -1);

        $data['headers'] = substr($headers, 0, -1);
        $data['form'] = substr($form, 0, -1);

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_observation_report', $data);
        //$this->load->view('teacherself/teacher_observation',$data);
    }

    function getteacher_consolidated() {
        $data['idname'] = 'tools';
        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_consolidated', $data);
    }

    function get_teacher_consolidated() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $data['year'] = $year;

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_consolidated_report', $data);
        //$this->load->view('teacherself/teacher_consolidated',$data);
    }

    function get_teacher_professional() {
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $data['year'] = $year;
        $cols = '';
        $headers = '';
        $form = '';


        for ($j = 1; $j <= 4; $j++) {
            $col = array();
            $col["name"] = "value_" . $j;
            $col["index"] = "time_" . $j;
            $col["width"] = 120;
            $col["align"] = "center"; // render as select	
            if ($j == 1) {
                $form.="'PLC Discussions',";
            }
            if ($j == 2) {
                $form.="'Individual Articles',";
            }
            if ($j == 3) {
                $form.="'Individual Videos',";
            }
            if ($j == 4) {
                $form.="'Individual Artifacts',";
            }
            $newas = json_encode($col);
            $cols.=$newas . ',';
        }




        $data['cols'] = substr($cols, 0, -1);


        $data['form'] = substr($form, 0, -1);

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_professional_report', $data);
    }

    function get_teacher_notification() {
        if ($this->input->post('teacher_id'))
            $this->session->set_userdata('temp_teacher_id', $this->input->post('teacher_id'));
        $data['idname'] = 'tools';
        $year = $this->input->post('year');
        $data['year'] = $year;
        $cols = '';
        $headers = '';
        $form = '';
        for ($i = 1; $i <= 12; $i++) {
            $j = 1;
            $head = array();
            $head["startColumnName"] = "value_" . $i . '_' . $j;
            $head["numberOfColumns"] = 3;
            $head["titleText"] = date('F', mktime(0, 0, 0, $i));
            $head = json_encode($head);
            $headers.=$head . ',';

            for ($j = 1; $j <= 3; $j++) {
                $col = array();
                $col["name"] = "value_" . $i . '_' . $j;
                $col["index"] = "time_" . $i . '_' . $j;
                $col["width"] = 60;
                $col["align"] = "center"; // render as select	
                if ($j == 1) {
                    $form.="'LP',";
                }
                if ($j == 2) {
                    $form.="'HN',";
                }
                if ($j == 3) {
                    $form.="'BL',";
                }

                $newas = json_encode($col);
                $cols.=$newas . ',';
            }
        }

        $data['cols'] = substr($cols, 0, -1);

        $data['headers'] = substr($headers, 0, -1);
        $data['form'] = substr($form, 0, -1);

        $data['view_path'] = $this->config->item('view_path');
        $this->load->view('teacherself/teacher_notification_report', $data);
        //$this->load->view('teacherself/teacher_notification',$data);
    }

    function getteacherobservationreportdata($year) {
        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction

        if (!$sidx)
            $sidx = 1;

        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        }


        $count = count($teachers);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($teachers != false) {

            foreach ($teachers as $key => $teachervalue) {
                $reportdata = $this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'], $year);

                echo "<row id='" . $key . "'>";
                echo "<cell><![CDATA[" . $teachervalue['firstname'] . "  " . $teachervalue['lastname'] . "]]></cell>";
                if ($reportdata != false) {
                    for ($m = 1; $m <= 12; $m++) {
                        $forma = 0;
                        $formaformal = 0;
                        $formainformal = 0;
                        $formb = 0;
                        $formbformal = 0;
                        $formbinformal = 0;
                        $formc = 0;
                        $formcformal = 0;
                        $formcinformal = 0;
                        $formp = 0;
                        $formpformal = 0;
                        $formpinformal = 0;
                        foreach ($reportdata as $reportvalue) {

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'forma') {
                                $forma = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formaformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formainformal = $reportvalue['count'];
                                }
                            }

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formb') {
                                $formb = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formbformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formbinformal = $reportvalue['count'];
                                }
                            }

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formc') {
                                $formc = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formcformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formcinformal = $reportvalue['count'];
                                }
                            }

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formp') {
                                $formp = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formpformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formpinformal = $reportvalue['count'];
                                }
                            }
                        }
                        if ($forma == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formaformal . "</font>,&nbsp;<font color='#DD6435'>" . $formainformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                        if ($formb == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formbformal . "</font>,&nbsp;<font color='#DD6435'>" . $formbinformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                        if ($formp == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formpformal . "</font>,&nbsp;<font color='#DD6435'>" . $formpinformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                        if ($formc == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formcformal . "</font>,&nbsp;<font color='#DD6435'>" . $formcinformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                } else {
                    for ($m = 1; $m <= 12; $m++) {
                        echo "<cell><![CDATA[]]></cell>";
                        echo "<cell><![CDATA[]]></cell>";
                        echo "<cell><![CDATA[]]></cell>";
                        echo "<cell><![CDATA[]]></cell>";
                    }
                }
                echo "</row>";
            }
        } else {
            echo "<row id='no'>";
            echo "<cell><![CDATA[No Teachers Found]]></cell>";
            echo "</row>";
        }
        echo "</rows>";
    }

    function getteacherobservationconsolidated($year) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction



        if (!$sidx)
            $sidx = 1;

        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        }


        $count = 4;

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($teachers != false) {

            foreach ($teachers as $key => $teachervalue) {
                $reportdata = $this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'], $year);

                echo "<row id='checklist'>";
                echo "<cell><![CDATA[Checklist]]></cell>";
                if ($reportdata != false) {
                    for ($m = 1; $m <= 12; $m++) {
                        $forma = 0;
                        $formaformal = 0;
                        $formainformal = 0;

                        foreach ($reportdata as $reportvalue) {

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'forma') {
                                $forma = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formaformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formainformal = $reportvalue['count'];
                                }
                            }
                        }
                        if ($forma == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formaformal . "</font>,&nbsp;<font color='#DD6435'>" . $formainformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                } else {
                    for ($m = 1; $m <= 12; $m++) {
                        echo "<cell><![CDATA[]]></cell>";
                    }
                }
                echo "</row>";

                echo "<row id='scale'>";
                echo "<cell><![CDATA[Scale]]></cell>";
                if ($reportdata != false) {
                    for ($m = 1; $m <= 12; $m++) {

                        $formb = 0;
                        $formbformal = 0;
                        $formbinformal = 0;

                        foreach ($reportdata as $reportvalue) {

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formb') {
                                $formb = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formbformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formbinformal = $reportvalue['count'];
                                }
                            }
                        }
                        if ($formb == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formbformal . "</font>,&nbsp;<font color='#DD6435'>" . $formbinformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                } else {
                    for ($m = 1; $m <= 12; $m++) {
                        echo "<cell><![CDATA[]]></cell>";
                    }
                }
                echo "</row>";
                echo "<row id='proficiency'>";
                echo "<cell><![CDATA[Proficiency]]></cell>";
                if ($reportdata != false) {
                    for ($m = 1; $m <= 12; $m++) {

                        $formp = 0;
                        $formpformal = 0;
                        $formpinformal = 0;
                        foreach ($reportdata as $reportvalue) {

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formp') {
                                $formp = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formpformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formpinformal = $reportvalue['count'];
                                }
                            }
                        }
                        if ($formp == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formpformal . "</font>,&nbsp;<font color='#DD6435'>" . $formpinformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                } else {
                    for ($m = 1; $m <= 12; $m++) {
                        echo "<cell><![CDATA[]]></cell>";
                    }
                }
                echo "</row>";
                echo "<row id='likert'>";
                echo "<cell><![CDATA[Likert]]></cell>";
                if ($reportdata != false) {
                    for ($m = 1; $m <= 12; $m++) {

                        $formc = 0;
                        $formcformal = 0;
                        $formcinformal = 0;

                        foreach ($reportdata as $reportvalue) {

                            if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formc') {
                                $formc = 1;
                                if ($reportvalue['period_lesson'] == 'Formal') {
                                    $formcformal = $reportvalue['count'];
                                }
                                if ($reportvalue['period_lesson'] == 'Informal') {
                                    $formcinformal = $reportvalue['count'];
                                }
                            }
                        }
                        if ($formc == 1) {
                            echo "<cell><![CDATA[<font color='#5987D3'>" . $formcformal . "</font>,&nbsp;<font color='#DD6435'>" . $formcinformal . "</font>]]></cell>";
                        } else {
                            echo "<cell><![CDATA[]]></cell>";
                        }
                    }
                } else {
                    for ($m = 1; $m <= 12; $m++) {
                        echo "<cell><![CDATA[]]></cell>";
                    }
                }
                echo "</row>";
            }
        } else {
            echo "<row id='no'>";
            echo "<cell><![CDATA[No Teachers Found]]></cell>";
            echo "</row>";
        }


        echo "</rows>";
    }

    function getteacherprofessionalconsolidated($year) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction



        if (!$sidx)
            $sidx = 1;

        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
            $school_id = $this->session->userdata('school_summ_id');
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $school_id = $this->session->userdata('school_id');
        }


        $count = 4;

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($teachers != false) {

            foreach ($teachers as $key => $teachervalue) {
                $this->load->Model('banktimemodel');
                $plctotal = $this->banktimemodel->getschoolplcall($school_id, $year);
                $plcteacher = $this->banktimemodel->getteacherplcall($school_id, $teachervalue['teacher_id'], $year);
                $articletotalassign = $this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'], $year);
                $articleteacherjournal = $this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'], $year);
                $videototalassign = $this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'], $year);
                $videoteacherjournal = $this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'], $year);
                $artifacts = $this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'], $year);


                echo "<row id='plc'>";
                echo "<cell><![CDATA[Small Learning Community Discussion]]></cell>";
                echo "<cell><![CDATA[" . $plcteacher . " of " . $plctotal . "]]></cell>";
                echo "</row>";
                echo "<row id='articles'>";
                echo "<cell><![CDATA[Individual Activity]]></cell>";
                echo "<cell><![CDATA[" . $articleteacherjournal . " of " . $articletotalassign . "]]></cell>";
                echo "</row>";
                echo "<row id='video'>";
                echo "<cell><![CDATA[Individual Videos]]></cell>";
                echo "<cell><![CDATA[" . $videoteacherjournal . " of " . $videototalassign . "]]></cell>";
                echo "</row>";
                echo "<row id='artifacts'>";
                echo "<cell><![CDATA[Individual Artifacts]]></cell>";
                echo "<cell><![CDATA[" . $artifacts . "]]></cell>";
                echo "</row>";
            }
        } else {
            echo "<row id='no'>";
            echo "<cell><![CDATA[No Teachers Found]]></cell>";
            echo "</row>";
        }


        echo "</rows>";
    }

    function getteachernotificationreportdata($year) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction



        if (!$sidx)
            $sidx = 1;

        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
        } else if ($this->session->userdata('login_type') == 'user') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('temp_teacher_id'));
            $this->session->unset_userdata('temp_teacher_id');
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        }


        $count = count($teachers);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($teachers != false) {

            foreach ($teachers as $key => $teachervalue) {
                $this->load->Model('lessonplanmodel');
                $lessondata = $this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'], $year);
                $homeworkdata = $this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'], $year);
                $behaviordata = $this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'], $year);

                echo "<row id='" . $key . "'>";
                echo "<cell><![CDATA[" . $teachervalue['firstname'] . "  " . $teachervalue['lastname'] . "]]></cell>";

                for ($m = 1; $m <= 12; $m++) {
                    $lp = 0;
                    $hn = 0;
                    $bl = 0;
                    $lpvalue = 0;
                    $hnvalue = 0;
                    $blvalue = 0;


                    if ($lessondata != false) {
                        foreach ($lessondata as $lessonvalue) {
                            if ($lessonvalue['month'] == $m) {
                                $lp = 1;

                                $lpvalue = $lessonvalue['count'];
                            }
                        }
                    }
                    if ($homeworkdata != false) {
                        foreach ($homeworkdata as $homevalue) {
                            if ($homevalue['month'] == $m) {
                                $hn = 1;

                                $hnvalue = $homevalue['count'];
                            }
                        }
                    }
                    if ($behaviordata != false) {
                        foreach ($behaviordata as $bevalue) {
                            if ($bevalue['month'] == $m) {
                                $bl = 1;

                                $blvalue = $bevalue['count'];
                            }
                        }
                    }
                    if ($lp == 1) {
                        echo "<cell><![CDATA[" . $lpvalue . "]]></cell>";
                    } else {
                        echo "<cell><![CDATA[0]]></cell>";
                    }
                    if ($hn == 1) {
                        echo "<cell><![CDATA[" . $hnvalue . "]]></cell>";
                    } else {
                        echo "<cell><![CDATA[0]]></cell>";
                    }
                    if ($bl == 1) {
                        echo "<cell><![CDATA[" . $blvalue . "]]></cell>";
                    } else {
                        echo "<cell><![CDATA[0]]></cell>";
                    }
                }



                echo "</row>";
            }
        } else {
            echo "<row id='no'>";
            echo "<cell><![CDATA[No Teachers Found]]></cell>";
            echo "</row>";
        }


        echo "</rows>";
    }

    function getteachernotificationconsolidated($year) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction



        if (!$sidx)
            $sidx = 1;

        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
        }


        $count = 3;

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($teachers != false) {

            foreach ($teachers as $key => $teachervalue) {
                $this->load->Model('lessonplanmodel');
                $lessondata = $this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'], $year);
                $homeworkdata = $this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'], $year);
                $behaviordata = $this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'], $year);

                echo "<row id='lp'>";
                echo "<cell><![CDATA[Lesson Plan]]></cell>";

                for ($m = 1; $m <= 12; $m++) {
                    $lp = 0;

                    $lpvalue = 0;



                    if ($lessondata != false) {
                        foreach ($lessondata as $lessonvalue) {
                            if ($lessonvalue['month'] == $m) {
                                $lp = 1;

                                $lpvalue = $lessonvalue['count'];
                            }
                        }
                    }

                    if ($lp == 1) {
                        echo "<cell><![CDATA[" . $lpvalue . "]]></cell>";
                    } else {
                        echo "<cell><![CDATA[0]]></cell>";
                    }
                }
                echo "</row>";


                echo "<row id='hn'>";
                echo "<cell><![CDATA[Home Notification]]></cell>";

                for ($m = 1; $m <= 12; $m++) {

                    $hn = 0;


                    $hnvalue = 0;



                    if ($homeworkdata != false) {
                        foreach ($homeworkdata as $homevalue) {
                            if ($homevalue['month'] == $m) {
                                $hn = 1;

                                $hnvalue = $homevalue['count'];
                            }
                        }
                    }

                    if ($hn == 1) {
                        echo "<cell><![CDATA[" . $hnvalue . "]]></cell>";
                    } else {
                        echo "<cell><![CDATA[0]]></cell>";
                    }
                }
                echo "</row>";


                echo "<row id='bl'>";
                echo "<cell><![CDATA[Behavior& Learning ]]></cell>";

                for ($m = 1; $m <= 12; $m++) {

                    $bl = 0;

                    $blvalue = 0;


                    if ($behaviordata != false) {
                        foreach ($behaviordata as $bevalue) {
                            if ($bevalue['month'] == $m) {
                                $bl = 1;

                                $blvalue = $bevalue['count'];
                            }
                        }
                    }




                    if ($bl == 1) {
                        echo "<cell><![CDATA[" . $blvalue . "]]></cell>";
                    } else {
                        echo "<cell><![CDATA[0]]></cell>";
                    }
                }
                echo "</row>";
            }
        } else {
            echo "<row id='no'>";
            echo "<cell><![CDATA[No Teachers Found]]></cell>";
            echo "</row>";
        }


        echo "</rows>";
    }

    function getteacherprofessionalreportdata($year) {

        $page = isset($_POST['page']) ? $_POST['page'] : 1; // get the requested page
        $limit = isset($_POST['rows']) ? $_POST['rows'] : 10; // get how many rows we want to have into the grid
        $sidx = isset($_POST['sidx']) ? $_POST['sidx'] : 'name'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord']) ? $_POST['sord'] : ''; // get the direction



        if (!$sidx)
            $sidx = 1;

        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
            $school_id = $this->session->userdata('school_summ_id');
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $school_id = $this->session->userdata('school_id');
        }


        $count = count($teachers);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);    //calculating total number of pages
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;

        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $start = ($start < 0) ? 0 : $start;  // make sure that $start is not a negative value
        //$result = $this->lessonplanmodel->value_feature();  //here DB_Func is a model handling DB interaction

        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml;charset=utf-8");
        } else {
            header("Content-type: text/xml;charset=utf-8");
        }
        $et = ">";

        echo "<?xml version='1.0' encoding='utf-8'?$et\n";
        echo "<rows>";
        echo "<page>" . $page . "</page>";
        echo "<total>" . $total_pages . "</total>";
        echo "<records>" . $count . "</records>";
        // be sure to put text data in CDATA




        if ($teachers != false) {

            foreach ($teachers as $key => $teachervalue) {
                $this->load->Model('banktimemodel');
                $plctotal = $this->banktimemodel->getschoolplcall($school_id, $year);
                $plcteacher = $this->banktimemodel->getteacherplcall($school_id, $teachervalue['teacher_id'], $year);
                $articletotalassign = $this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'], $year);
                $articleteacherjournal = $this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'], $year);
                $videototalassign = $this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'], $year);
                $videoteacherjournal = $this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'], $year);
                $artifacts = $this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'], $year);


                echo "<row id='" . $key . "'>";
                echo "<cell><![CDATA[" . $teachervalue['firstname'] . "  " . $teachervalue['lastname'] . "]]></cell>";






                echo "<cell><![CDATA[" . $plcteacher . " of " . $plctotal . "]]></cell>";
                echo "<cell><![CDATA[" . $articleteacherjournal . " of " . $articletotalassign . "]]></cell>";
                echo "<cell><![CDATA[" . $videoteacherjournal . " of " . $videototalassign . "]]></cell>";
                echo "<cell><![CDATA[" . $artifacts . "]]></cell>";







                echo "</row>";
            }
        } else {
            echo "<row id='no'>";
            echo "<cell><![CDATA[No Teachers Found]]></cell>";
            echo "</row>";
        }


        echo "</rows>";
    }

    function teacher_observation_pdf($year) {
        if ($this->session->userdata('login_type') == 'teacher') {
            $view_path = $this->config->item('view_path');
            $header = '';
            $style = '';
            $message = '';
            $orgmessage = '';
            $this->load->Model('teachermodel');
            if ($this->session->userdata('login_type') == 'teacher') {
                $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
                $school_id = $this->session->userdata('school_summ_id');
            } else {
                $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
                $school_id = $this->session->userdata('school_id');
            }
            $style.='<style type="text/css">
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
	width:50px;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:12.2px;}
.tad{

width:395px;
}
</style>
';

            if ($teachers != false) {




                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)

                    $this->load->Model('banktimemodel');
                    $plctotal = $this->banktimemodel->getschoolplcall($school_id, $year);
                    $plcteacher = $this->banktimemodel->getteacherplcall($school_id, $teachervalue['teacher_id'], $year);
                    $articletotalassign = $this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'], $year);
                    $articleteacherjournal = $this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'], $year);
                    $videototalassign = $this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'], $year);
                    $videoteacherjournal = $this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'], $year);
                    $artifacts = $this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'], $year); {
                        if ($k == 0) {
                            $header.='<table class="gridtable">';



                            $header.='<tr ><th width="200px" >Small Learning Community Discussion</th>';
                            $header.= "<td class='tad'>" . $plcteacher . " of " . $plctotal . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
                            $header.='<tr ><td >Individual Activity</td>';
                            $header.= "<td class='tad'>" . $articleteacherjournal . " of " . $articletotalassign . "</td></tr>";
                            $header.='<tr ><td >Individual Videos</td>';
                            $header.= "<td class='tad'>" . $videoteacherjournal . " of " . $videototalassign . "</td></tr>";
                            $header.='<tr ><td >Individual Artifacts</td>';

                            $header.= "<td class='tad'>" . $artifacts . "</td></tr>";
                        }






















                        $k++;
                    }
                }
                $orgmessage.=$header . '</table>';
            } else {
                $orgmessage.='<table border="1"><tr>';
                $orgmessage.= "<td>No Teachers Found</td>";
                $orgmessage.='</tr></table>';
            }




//2	
            $headern = '';
            $orgmessagen = '';
            if ($teachers != false) {



                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)
                    {
                        if ($k == 0) {
                            $headern.='<table class="gridtable" ><tr>';
                            $headern.='<td ><b>Month</b></td>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('M', mktime(0, 0, 0, $m));
                                $headern.='<td>' . $monthname . '</td>';
                            }
                            $headern.='</tr>';
                        }

                        $this->load->Model('lessonplanmodel');
                        $lessondata = $this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'], $year);
                        $homeworkdata = $this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'], $year);
                        $behaviordata = $this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'], $year);


                        $headern.='<tr><td>Lesson Plan</td>';
                        for ($m = 1; $m <= 12; $m++) {
                            $lp = 0;

                            $lpvalue = 0;



                            if ($lessondata != false) {
                                foreach ($lessondata as $lessonvalue) {
                                    if ($lessonvalue['month'] == $m) {
                                        $lp = 1;

                                        $lpvalue = $lessonvalue['count'];
                                    }
                                }
                            }

                            if ($lp == 1) {
                                $headern.= "<td>" . $lpvalue . "</td>";
                            } else {
                                $headern.= "<td>0</td>";
                            }
                        }
                        $headern.= "</tr>";

                        $headern.='<tr><th >Home Notification</th>';
                        for ($m = 1; $m <= 12; $m++) {
                            $hn = 0;

                            $hnvalue = 0;



                            if ($homeworkdata != false) {
                                foreach ($homeworkdata as $homevalue) {
                                    if ($homevalue['month'] == $m) {
                                        $hn = 1;

                                        $hnvalue = $homevalue['count'];
                                    }
                                }
                            }

                            if ($hn == 1) {
                                $headern.= "<td>" . $hnvalue . "</td>";
                            } else {
                                $headern.= "<td>0</td>";
                            }
                        }
                        $headern.= "</tr>";


                        $headern.='<tr><td>Behavior& Learning</td>';
                        for ($m = 1; $m <= 12; $m++) {
                            $bl = 0;

                            $blvalue = 0;



                            if ($behaviordata != false) {
                                foreach ($behaviordata as $bevalue) {
                                    if ($bevalue['month'] == $m) {
                                        $bl = 1;

                                        $blvalue = $bevalue['count'];
                                    }
                                }
                            }

                            if ($bl == 1) {
                                $headern.= "<td>" . $blvalue . "</td>";
                            } else {
                                $headern.= "<td>0</td>";
                            }
                        }
                        $headern.= "</tr>";

                        $headern.= "</table>";
                        $k++;
                    }
                }
                $orgmessagen.=$headern;
            } else {
                $orgmessagen.='<table border="1"><tr>';
                $orgmessagen.= "<td>No Teachers Found</td>";
                $orgmessagen.='</tr></table>';
            }
            //2
            //3
            $headerb = '';
            $orgmessageb = '';
            if ($teachers != false) {



                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)
                    {
                        if ($k == 0) {
                            $headerb.='<table class="gridtable"><tr>';
                            $headerb.='<td><b>Month</b></td>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('M', mktime(0, 0, 0, $m));
                                $headerb.='<td>' . $monthname . '</td>';
                            }
                            $headerb.='</tr>';
                        }

                        $reportdata = $this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'], $year);


                        $headerb.='<tr><td >Checklist&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $forma = 0;
                                $formaformal = 0;
                                $formainformal = 0;

                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'forma') {
                                        $forma = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formaformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formainformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($forma == 1) {

                                    $headerb.="<td><span class='blue'>" . $formaformal . "</span>,&nbsp;<span class='yellow'>" . $formainformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";

                        $headerb.='<tr><td>Scale</td>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $formb = 0;
                                $formbformal = 0;
                                $formbinformal = 0;
                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formb') {
                                        $formb = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formbformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formbinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($formb == 1) {

                                    $headerb.="<td><span class='blue'>" . $formbformal . "</span>,&nbsp;<span class='yellow'>" . $formbinformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";

                        $headerb.='<tr><th>Proficiency</th>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $formp = 0;
                                $formpformal = 0;
                                $formpinformal = 0;

                                foreach ($reportdata as $reportvalue) {


                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formp') {
                                        $formp = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formpformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formpinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($formp == 1) {

                                    $headerb.="<td><span class='blue'>" . $formpformal . "</span>,&nbsp;<span class='yellow'>" . $formpinformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";

                        $headerb.='<tr><td>Likert</td>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $formc = 0;
                                $formcformal = 0;
                                $formcinformal = 0;

                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formc') {
                                        $formc = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formcformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formcinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($formc == 1) {

                                    $headerb.="<td><span class='blue'>" . $formcformal . "</span>,&nbsp;<span class='yellow'>" . $formcinformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";
                        $headerb.= "</table>";
                        $k++;
                    }
                }
                $orgmessageb.=$headerb;
            } else {
                $orgmessageb.='<table border="1"><tr>';
                $orgmessageb.= "<td>No Teachers Found</td>";
                $orgmessageb.='</tr></table>';
            }

            //3

            $t = $this->session->userdata('teacher_name');
            $this->load->Model('report_disclaimermodel');
            $reportdis = $this->report_disclaimermodel->getallplans(4);
            $dis = '';
            $fontsize = '';
            $fontcolor = '';
            if ($reportdis != false) {

                $dis = $reportdis[0]['tab'];
                $fontsize = $reportdis[0]['size'];
                $fontcolor = $reportdis[0]['color'];
            }

            $str = '<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              <div style="width:650px;font-size:' . $fontsize . 'px;color:#' . $fontcolor . ';">' . $dis . '</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="' . $view_path . 'inc/logo/logo150.png"/>
		<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>' . ucfirst($this->session->userdata('district_name')) . '</b> 		
		<br />';
            if ($this->session->userdata('login_type') == 'teacher') {
                $str.='<b>' . ucfirst($this->session->userdata('school_self_name')) . '</b>';
            } else {
                $str.='<b>' . ucfirst($this->session->userdata('school_name')) . '</b>';
            }




            $str.='</div>
		</div>	
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />

		<b><div style="font-size:14px;">Teacher Name:&nbsp;' . $t . '</div></b><br/><br/>
		<b><div style="font-size:14px;">Year:&nbsp;' . $year . '</div></b><br/><br/>		
		<b><div style="font-size:14px;">Instructional Observation Activity  :</div></b><br/>		
		<br/><br/><b><div style="font-size:12px;">Legend:&nbsp;&nbsp;&nbsp;&nbsp;Blue:&nbsp;&nbsp;<span class="blue">Formal</span> &nbsp; Red:&nbsp;&nbsp;<span class="yellow">Informal</span></div></b><br/>		
		' . $style . $orgmessageb . '
		<br/><br/><br/><br/><b><div style="font-size:14px;">Notification Activity :</div></b><br/><br/><br/>		
		' . $orgmessagen . '		
		<br/><br/><br/><br/><b><div style="font-size:14px;"> Professional Development Activity:</div></b>
		<br/>		
		' . $orgmessage . '
		<br />
		<br />
   </page> ';


            $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(18, 20, 20, 2));
            $content = ob_get_clean();




            $html2pdf->WriteHTML($str);
            $html2pdf->Output();
        } else {
            $view_path = $this->config->item('view_path');
            $header = '';
            $style = '';
            $message = '';
            $orgmessage = '';
            $this->load->Model('teachermodel');
            if ($this->session->userdata('login_type') == 'teacher') {
                $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
            } else {
                $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            }
            $style.='<style type="text/css">
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
</style>
';

            if ($teachers != false) {



                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)
                    {
                        if ($k == 0) {
                            $header.='<table  border="1" style="font-size:10px;"><tr>';
                            $header.='<td><table ><tr><td><b>Month Name</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('F', mktime(0, 0, 0, $m));
                                $header.='<tr><td>' . $monthname . '</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                            }
                            $header.='</table></td>';
                            $header.='<td><table><tr><td><b>Forms</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $header.='<tr><td>Checklist</td></tr>';
                                $header.='<tr><td>Scale</td></tr>';
                                $header.='<tr><td>Proficiency</td></tr>';
                                $header.='<tr><td>Likert</td></tr>';
                            }
                            $header.='</table></td>';
                        }
                        if ($k % 6 == 0 && $k != 0) {
                            $orgmessage.=$header . $message . '</tr></table>';
                            $header = '';
                            $message = '';
                            $header.='<table border="1" style="font-size:10px;"><tr>';
                            $header.='<td><table><tr><td><b>Month Name</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('F', mktime(0, 0, 0, $m));
                                $header.='<tr><td>' . $monthname . '</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                            }
                            $header.='</table></td>';
                            $header.='<td><table><tr><td><b>Forms</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $header.='<tr><td>Checklist</td></tr>';
                                $header.='<tr><td>Scale</td></tr>';
                                $header.='<tr><td>Likert</td></tr>';
                                $header.='<tr><td>Proficiency</td></tr>';
                            }
                            $header.='</table></td>';
                        }
                        $reportdata = $this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'], $year);

                        $message.= "<td><table>";
                        $message.= "<tr>";
                        $message.= "<td ><b>" . $teachervalue['firstname'] . "  " . $teachervalue['lastname'] . "</b></td>";
                        $message.= "</tr>";
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $forma = 0;
                                $formaformal = 0;
                                $formainformal = 0;
                                $formb = 0;
                                $formbformal = 0;
                                $formbinformal = 0;
                                $formc = 0;
                                $formcformal = 0;
                                $formcinformal = 0;
                                $formp = 0;
                                $formpformal = 0;
                                $formpinformal = 0;
                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'forma') {
                                        $forma = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formaformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formainformal = $reportvalue['count'];
                                        }
                                    }

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formb') {
                                        $formb = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formbformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formbinformal = $reportvalue['count'];
                                        }
                                    }

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formc') {
                                        $formc = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formcformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formcinformal = $reportvalue['count'];
                                        }
                                    }

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formp') {
                                        $formp = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formpformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formpinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($forma == 1) {
                                    $message.= "<tr>";
                                    $message.="<td><span class='blue'>" . $formaformal . "</span>,&nbsp;<span class='yellow'>" . $formainformal . "</span></td>";
                                    $message.= "</tr>";
                                } else {
                                    $message.= "<tr>";
                                    $message.= "<td>&nbsp;</td>";
                                    $message.= "</tr>";
                                }
                                if ($formb == 1) {
                                    $message.= "<tr>";
                                    $message.="<td><span class='blue'>" . $formbformal . "</span>,&nbsp;<span class='yellow'>" . $formbinformal . "</span></td>";
                                    $message.= "</tr>";
                                } else {
                                    $message.= "<tr>";
                                    $message.= "<td>&nbsp;</td>";
                                    $message.= "</tr>";
                                }
                                if ($formp == 1) {
                                    $message.= "<tr>";
                                    $message.="<td><span class='blue'>" . $formpformal . "</span>,&nbsp;<span class='yellow'>" . $formpinformal . "</span></td>";
                                    $message.= "</tr>";
                                } else {
                                    $message.= "<tr>";
                                    $message.= "<td>&nbsp;</td>";
                                    $message.= "</tr>";
                                }
                                if ($formc == 1) {
                                    $message.= "<tr>";
                                    $message.="<td><span class='blue'>" . $formcformal . "</span>,&nbsp;<span class='yellow'>" . $formcinformal . "</span></td>";
                                    $message.= "</tr>";
                                } else {
                                    $message.= "<tr>";
                                    $message.= "<td>&nbsp;</td>";
                                    $message.= "</tr>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $message.= "<tr>";
                                $message.= "<td>&nbsp;</td>";
                                $message.= "</tr>";
                                $message.= "<tr>";
                                $message.= "<td>&nbsp;</td>";
                                $message.= "</tr>";
                                $message.= "<tr>";
                                $message.= "<td>&nbsp;</td>";
                                $message.= "</tr>";
                                $message.= "<tr>";
                                $message.= "<td>&nbsp;</td>";
                                $message.= "</tr>";
                            }
                        }
                        $message.= "</table></td>";
                        $k++;
                    }
                }
                $orgmessage.=$header . $message . '</tr></table>';
            } else {
                $orgmessage.='<table border="1"><tr>';
                $orgmessage.= "<td>No Teachers Found</td>";
                $orgmessage.='</tr></table>';
            }


            $this->load->Model('report_disclaimermodel');
            $reportdis = $this->report_disclaimermodel->getallplans(4);
            $dis = '';
            $fontsize = '';
            $fontcolor = '';
            if ($reportdis != false) {

                $dis = $reportdis[0]['tab'];
                $fontsize = $reportdis[0]['size'];
                $fontcolor = $reportdis[0]['color'];
            }

            $str = '<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              <div style="width:650px;font-size:' . $fontsize . 'px;color:#' . $fontcolor . ';">' . $dis . '</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="' . $view_path . 'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>' . ucfirst($this->session->userdata('district_name')) . '</b> 		
		<br />';
            if ($this->session->userdata('login_type') == 'teacher') {
                $str.='<b>' . ucfirst($this->session->userdata('school_self_name')) . '</b>';
            } else {
                $str.='<b>' . ucfirst($this->session->userdata('school_name')) . '</b>';
            }




            $str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />


		<b><div style="font-size:14px;">Observation Count:</div></b><br/><br/><br/>
		<b><div style="font-size:12px;">Year:&nbsp;' . $year . '&nbsp;Legend:</div>
		<div style="font-size:12px;">Blue:&nbsp;&nbsp;Formal &nbsp; Red:&nbsp;&nbsp;Informal  </div></b><br/>		
		' . $style . $orgmessage . '
		<br />
		<br />
   </page> ';

            $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(18, 20, 20, 2));
            $content = ob_get_clean();




            $html2pdf->WriteHTML($str);
            $html2pdf->Output();
        }
    }

    function teacher_professional_pdf($year) {
        $view_path = $this->config->item('view_path');
        $header = '';
        $style = '';
        $message = '';
        $orgmessage = '';
        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
            $school_id = $this->session->userdata('school_summ_id');
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $school_id = $this->session->userdata('school_id');
        }
        $style.='<style type="text/css">
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:95.2px;}
</style>
';

        if ($teachers != false) {


            $orgmessage.="<table class='gridtable'><tr><td>Teacher Name</td><td>SLC Discussion&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Article Reviews&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Video Reviews&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Artifacts&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
            $k = 0;

            foreach ($teachers as $key => $teachervalue) {
                $this->load->Model('banktimemodel');
                $plctotal = $this->banktimemodel->getschoolplcall($school_id, $year);
                $plcteacher = $this->banktimemodel->getteacherplcall($school_id, $teachervalue['teacher_id'], $year);
                $articletotalassign = $this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'], $year);
                $articleteacherjournal = $this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'], $year);
                $videototalassign = $this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'], $year);
                $videoteacherjournal = $this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'], $year);
                $artifacts = $this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'], $year);
                $orgmessage.="<tr><td>" . $teachervalue['firstname'] . "  " . $teachervalue['lastname'] . "</td>";

                $orgmessage.="<td>" . $plcteacher . " of " . $plctotal . "</td>";
                $orgmessage.="<td>" . $articleteacherjournal . " of " . $articletotalassign . "</td>";
                $orgmessage.="<td>" . $videoteacherjournal . " of " . $videototalassign . "</td>";
                $orgmessage.="<td>" . $artifacts . "</td>";
                $orgmessage.="</tr>";
            }
            $orgmessage.="</table>";
        } else {
            $orgmessage.='<table border="1"><tr>';
            $orgmessage.= "<td>No Teachers Found</td>";
            $orgmessage.='</tr></table>';
        }


        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(6);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $dis = $reportdis[0]['tab'];
            $fontsize = $reportdis[0]['size'];
            $fontcolor = $reportdis[0]['color'];
        }
        $str = '<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
             <div style="width:650px;font-size:' . $fontsize . 'px;color:#' . $fontcolor . ';">' . $dis . '</div> 
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="' . $view_path . 'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>' . ucfirst($this->session->userdata('district_name')) . '</b> 		
		<br />';
        $this->load->Model('schoolmodel');
        $schools = $this->schoolmodel->getschoolById($school_id);


        $str.='<b>' . $schools[0]['school_name'] . '</b>';





        $str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />
		<b><div style="font-size:14px;">Professional Development Activity:</div></b><br/><br/><br/>		
		' . $style . $orgmessage . '
		<br />
		<br />
   </page> ';


        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(18, 20, 20, 2));
        $content = ob_get_clean();




        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    function teacher_consolidated_pdf($year) {
        $view_path = $this->config->item('view_path');
        $header = '';
        $style = '';
        $message = '';
        $orgmessage = '';
        $this->load->Model('teachermodel');
        if ($this->session->userdata('login_type') == 'teacher') {
            $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
            $school_id = $this->session->userdata('school_summ_id');
        } else {
            $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            $school_id = $this->session->userdata('school_id');
        }
        $style.='<style type="text/css">
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
	width:50px;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:12.2px;}
.tad{

width:395px;
}
</style>
';

        if ($teachers != false) {




            $k = 0;

            foreach ($teachers as $key => $teachervalue) {
                //for($j=1;$j<=7;$j++)

                $this->load->Model('banktimemodel');
                $plctotal = $this->banktimemodel->getschoolplcall($school_id, $year);
                $plcteacher = $this->banktimemodel->getteacherplcall($school_id, $teachervalue['teacher_id'], $year);
                $articletotalassign = $this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'], $year);
                $articleteacherjournal = $this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'], $year);
                $videototalassign = $this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'], $year);
                $videoteacherjournal = $this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'], $year);
                $artifacts = $this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'], $year); {
                    if ($k == 0) {
                        $header.='<table class="gridtable">';



                        $header.='<tr ><th width="200px" >Small Learning Community Discussion</th>';
                        $header.= "<td class='tad'>" . $plcteacher . " of " . $plctotal . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
                        $header.='<tr ><td >Individual Activity</td>';
                        $header.= "<td class='tad'>" . $articleteacherjournal . " of " . $articletotalassign . "</td></tr>";
                        $header.='<tr ><td >Individual Videos</td>';
                        $header.= "<td class='tad'>" . $videoteacherjournal . " of " . $videototalassign . "</td></tr>";
                        $header.='<tr ><td >Individual Artifacts</td>';

                        $header.= "<td class='tad'>" . $artifacts . "</td></tr>";
                    }

                    $k++;
                }
            }
            $orgmessage.=$header . '</table>';
        } else {
            $orgmessage.='<table border="1"><tr>';
            $orgmessage.= "<td>No Teachers Found</td>";
            $orgmessage.='</tr></table>';
        }




//2	
        $headern = '';
        $orgmessagen = '';
        if ($teachers != false) {



            $k = 0;

            foreach ($teachers as $key => $teachervalue) {
                //for($j=1;$j<=7;$j++)
                {
                    if ($k == 0) {
                        $headern.='<table class="gridtable" ><tr>';
                        $headern.='<td><b>Month</b></td>';
                        for ($m = 1; $m <= 12; $m++) {
                            $monthname = date('M', mktime(0, 0, 0, $m));
                            $headern.='<td>' . $monthname . '</td>';
                        }
                        $headern.='</tr>';
                    }

                    $this->load->Model('lessonplanmodel');
                    $lessondata = $this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'], $year);
                    $homeworkdata = $this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'], $year);
                    $behaviordata = $this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'], $year);


                    $headern.='<tr><td>Lesson Plan</td>';
                    for ($m = 1; $m <= 12; $m++) {
                        $lp = 0;

                        $lpvalue = 0;



                        if ($lessondata != false) {
                            foreach ($lessondata as $lessonvalue) {
                                if ($lessonvalue['month'] == $m) {
                                    $lp = 1;

                                    $lpvalue = $lessonvalue['count'];
                                }
                            }
                        }

                        if ($lp == 1) {
                            $headern.= "<td>" . $lpvalue . "</td>";
                        } else {
                            $headern.= "<td>0</td>";
                        }
                    }
                    $headern.= "</tr>";

                    $headern.='<tr><th>Home Notification</th>';
                    for ($m = 1; $m <= 12; $m++) {
                        $hn = 0;

                        $hnvalue = 0;



                        if ($homeworkdata != false) {
                            foreach ($homeworkdata as $homevalue) {
                                if ($homevalue['month'] == $m) {
                                    $hn = 1;

                                    $hnvalue = $homevalue['count'];
                                }
                            }
                        }

                        if ($hn == 1) {
                            $headern.= "<td>" . $hnvalue . "</td>";
                        } else {
                            $headern.= "<td>0</td>";
                        }
                    }
                    $headern.= "</tr>";


                    $headern.='<tr><td>Behavior& Learning</td>';
                    for ($m = 1; $m <= 12; $m++) {
                        $bl = 0;

                        $blvalue = 0;



                        if ($behaviordata != false) {
                            foreach ($behaviordata as $bevalue) {
                                if ($bevalue['month'] == $m) {
                                    $bl = 1;

                                    $blvalue = $bevalue['count'];
                                }
                            }
                        }

                        if ($bl == 1) {
                            $headern.= "<td>" . $blvalue . "</td>";
                        } else {
                            $headern.= "<td>0</td>";
                        }
                    }
                    $headern.= "</tr>";

                    $headern.= "</table>";
                    $k++;
                }
            }
            $orgmessagen.=$headern;
        } else {
            $orgmessagen.='<table border="1"><tr>';
            $orgmessagen.= "<td>No Teachers Found</td>";
            $orgmessagen.='</tr></table>';
        }
        //2
        //3
        $headerb = '';
        $orgmessageb = '';
        if ($teachers != false) {



            $k = 0;

            foreach ($teachers as $key => $teachervalue) {
                //for($j=1;$j<=7;$j++)
                {
                    if ($k == 0) {
                        $headerb.='<table class="gridtable"><tr>';
                        $headerb.='<td><b>Month</b></td>';
                        for ($m = 1; $m <= 12; $m++) {
                            $monthname = date('M', mktime(0, 0, 0, $m));
                            $headerb.='<td>' . $monthname . '</td>';
                        }
                        $headerb.='</tr>';
                    }

                    $reportdata = $this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'], $year);


                    $headerb.='<tr><td>Checklist&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                    if ($reportdata != false) {
                        for ($m = 1; $m <= 12; $m++) {
                            $forma = 0;
                            $formaformal = 0;
                            $formainformal = 0;

                            foreach ($reportdata as $reportvalue) {

                                if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'forma') {
                                    $forma = 1;
                                    if ($reportvalue['period_lesson'] == 'Formal') {
                                        $formaformal = $reportvalue['count'];
                                    }
                                    if ($reportvalue['period_lesson'] == 'Informal') {
                                        $formainformal = $reportvalue['count'];
                                    }
                                }
                            }
                            if ($forma == 1) {

                                $headerb.="<td><span class='blue'>" . $formaformal . "</span>,&nbsp;<span class='yellow'>" . $formainformal . "</span></td>";
                            } else {

                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                    } else {
                        for ($m = 1; $m <= 12; $m++) {
                            $headerb.= "<td>&nbsp;</td>";
                        }
                    }
                    $headerb.= "</tr>";

                    $headerb.='<tr><td>Scale</td>';
                    if ($reportdata != false) {
                        for ($m = 1; $m <= 12; $m++) {
                            $formb = 0;
                            $formbformal = 0;
                            $formbinformal = 0;
                            foreach ($reportdata as $reportvalue) {

                                if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formb') {
                                    $formb = 1;
                                    if ($reportvalue['period_lesson'] == 'Formal') {
                                        $formbformal = $reportvalue['count'];
                                    }
                                    if ($reportvalue['period_lesson'] == 'Informal') {
                                        $formbinformal = $reportvalue['count'];
                                    }
                                }
                            }
                            if ($formb == 1) {

                                $headerb.="<td><span class='blue'>" . $formbformal . "</span>,&nbsp;<span class='yellow'>" . $formbinformal . "</span></td>";
                            } else {

                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                    } else {
                        for ($m = 1; $m <= 12; $m++) {
                            $headerb.= "<td>&nbsp;</td>";
                        }
                    }
                    $headerb.= "</tr>";

                    $headerb.='<tr><th>Proficiency</th>';
                    if ($reportdata != false) {
                        for ($m = 1; $m <= 12; $m++) {
                            $formp = 0;
                            $formpformal = 0;
                            $formpinformal = 0;

                            foreach ($reportdata as $reportvalue) {


                                if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formp') {
                                    $formp = 1;
                                    if ($reportvalue['period_lesson'] == 'Formal') {
                                        $formpformal = $reportvalue['count'];
                                    }
                                    if ($reportvalue['period_lesson'] == 'Informal') {
                                        $formpinformal = $reportvalue['count'];
                                    }
                                }
                            }
                            if ($formp == 1) {

                                $headerb.="<td><span class='blue'>" . $formpformal . "</span>,&nbsp;<span class='yellow'>" . $formpinformal . "</span></td>";
                            } else {

                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                    } else {
                        for ($m = 1; $m <= 12; $m++) {
                            $headerb.= "<td>&nbsp;</td>";
                        }
                    }
                    $headerb.= "</tr>";

                    $headerb.='<tr><td>Likert</td>';
                    if ($reportdata != false) {
                        for ($m = 1; $m <= 12; $m++) {
                            $formc = 0;
                            $formcformal = 0;
                            $formcinformal = 0;

                            foreach ($reportdata as $reportvalue) {

                                if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formc') {
                                    $formc = 1;
                                    if ($reportvalue['period_lesson'] == 'Formal') {
                                        $formcformal = $reportvalue['count'];
                                    }
                                    if ($reportvalue['period_lesson'] == 'Informal') {
                                        $formcinformal = $reportvalue['count'];
                                    }
                                }
                            }
                            if ($formc == 1) {

                                $headerb.="<td><span class='blue'>" . $formcformal . "</span>,&nbsp;<span class='yellow'>" . $formcinformal . "</span></td>";
                            } else {

                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                    } else {
                        for ($m = 1; $m <= 12; $m++) {
                            $headerb.= "<td>&nbsp;</td>";
                        }
                    }
                    $headerb.= "</tr>";
                    $headerb.= "</table>";
                    $k++;
                }
            }
            $orgmessageb.=$headerb;
        } else {
            $orgmessageb.='<table border="1"><tr>';
            $orgmessageb.= "<td>No Teachers Found</td>";
            $orgmessageb.='</tr></table>';
        }

        //3

        $t = $this->session->userdata('teacher_name');

        $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(6);
        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $dis = $reportdis[0]['tab'];
            $fontsize = $reportdis[0]['size'];
            $fontcolor = $reportdis[0]['color'];
        }
        $str = '<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              <div style="width:650px;font-size:' . $fontsize . 'px;color:#' . $fontcolor . ';">' . $dis . '</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="' . $view_path . 'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>' . ucfirst($this->session->userdata('district_name')) . '</b> 		
		<br />';
        if ($this->session->userdata('login_type') == 'teacher') {
            $str.='<b>' . ucfirst($this->session->userdata('school_self_name')) . '</b>';
        } else {
            $str.='<b>' . ucfirst($this->session->userdata('school_name')) . '</b>';
        }





        $str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />

		<b><div style="font-size:14px;">Teacher Name:&nbsp;' . $t . '</div></b><br/><br/>
		<b><div style="font-size:14px;">Year:&nbsp;' . $year . '</div></b><br/><br/>		
		<b><div style="font-size:14px;">Professional Development Activity:</div></b><br/><br/><br/>		
		' . $style . $orgmessage . '
		<br/><br/><br/><br/><b><div style="font-size:14px;">Notification Activity :</div></b><br/><br/><br/>		
		' . $orgmessagen . '		
		<br/><br/><br/><br/><b><div style="font-size:14px;">Instructional Observation Activity  :</div></b>
		<br/><br/><b><div style="font-size:12px;">Legend:&nbsp;&nbsp;&nbsp;&nbsp;Blue:&nbsp;&nbsp;Formal &nbsp; Red:&nbsp;&nbsp;Informal</div></b><br/>		
		' . $orgmessageb . '
		<br />
		<br />
   </page> ';


        $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(18, 20, 20, 2));
        $content = ob_get_clean();




        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
    }

    function teacher_notification_pdf($year) {

        if ($this->session->userdata('login_type') == 'teacher') {
            $view_path = $this->config->item('view_path');
            $header = '';
            $style = '';
            $message = '';
            $orgmessage = '';
            $this->load->Model('teachermodel');
            if ($this->session->userdata('login_type') == 'teacher') {
                $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
                $school_id = $this->session->userdata('school_summ_id');
            } else {
                $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
                $school_id = $this->session->userdata('school_id');
            }
            $style.='<style type="text/css">
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
 table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
	width:50px;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:12.2px;}
.tad{

width:395px;
}
</style>
';

            if ($teachers != false) {




                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)

                    $this->load->Model('banktimemodel');
                    $plctotal = $this->banktimemodel->getschoolplcall($school_id, $year);
                    $plcteacher = $this->banktimemodel->getteacherplcall($school_id, $teachervalue['teacher_id'], $year);
                    $articletotalassign = $this->banktimemodel->getteacherassignedarticle($teachervalue['teacher_id'], $year);
                    $articleteacherjournal = $this->banktimemodel->getteacherassignedarticlejournaling($teachervalue['teacher_id'], $year);
                    $videototalassign = $this->banktimemodel->getteacherassignedvideo($teachervalue['teacher_id'], $year);
                    $videoteacherjournal = $this->banktimemodel->getteacherassignedvideojournaling($teachervalue['teacher_id'], $year);
                    $artifacts = $this->banktimemodel->getteacherartifacts($teachervalue['teacher_id'], $year); {
                        if ($k == 0) {
                            $header.='<table class="gridtable">';



                            $header.='<tr ><th width="200px" >Small Learning Community Discussion</th>';
                            $header.= "<td class='tad'>" . $plcteacher . " of " . $plctotal . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
                            $header.='<tr ><td >Individual Activity</td>';
                            $header.= "<td class='tad'>" . $articleteacherjournal . " of " . $articletotalassign . "</td></tr>";
                            $header.='<tr ><td >Individual Videos</td>';
                            $header.= "<td class='tad'>" . $videoteacherjournal . " of " . $videototalassign . "</td></tr>";
                            $header.='<tr ><td >Individual Artifacts</td>';

                            $header.= "<td class='tad'>" . $artifacts . "</td></tr>";
                        }






















                        $k++;
                    }
                }
                $orgmessage.=$header . '</table>';
            } else {
                $orgmessage.='<table border="1"><tr>';
                $orgmessage.= "<td>No Teachers Found</td>";
                $orgmessage.='</tr></table>';
            }




//2	
            $headern = '';
            $orgmessagen = '';
            if ($teachers != false) {



                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)
                    {
                        if ($k == 0) {
                            $headern.='<table class="gridtable" ><tr>';
                            $headern.='<td><b>Month</b></td>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('M', mktime(0, 0, 0, $m));
                                $headern.='<td>' . $monthname . '</td>';
                            }
                            $headern.='</tr>';
                        }

                        $this->load->Model('lessonplanmodel');
                        $lessondata = $this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'], $year);
                        $homeworkdata = $this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'], $year);
                        $behaviordata = $this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'], $year);


                        $headern.='<tr><td>Lesson Plan</td>';
                        for ($m = 1; $m <= 12; $m++) {
                            $lp = 0;

                            $lpvalue = 0;



                            if ($lessondata != false) {
                                foreach ($lessondata as $lessonvalue) {
                                    if ($lessonvalue['month'] == $m) {
                                        $lp = 1;

                                        $lpvalue = $lessonvalue['count'];
                                    }
                                }
                            }

                            if ($lp == 1) {
                                $headern.= "<td>" . $lpvalue . "</td>";
                            } else {
                                $headern.= "<td>0</td>";
                            }
                        }
                        $headern.= "</tr>";

                        $headern.='<tr><th>Home Notification</th>';
                        for ($m = 1; $m <= 12; $m++) {
                            $hn = 0;

                            $hnvalue = 0;



                            if ($homeworkdata != false) {
                                foreach ($homeworkdata as $homevalue) {
                                    if ($homevalue['month'] == $m) {
                                        $hn = 1;

                                        $hnvalue = $homevalue['count'];
                                    }
                                }
                            }

                            if ($hn == 1) {
                                $headern.= "<td>" . $hnvalue . "</td>";
                            } else {
                                $headern.= "<td>0</td>";
                            }
                        }
                        $headern.= "</tr>";


                        $headern.='<tr><td>Behavior& Learning</td>';
                        for ($m = 1; $m <= 12; $m++) {
                            $bl = 0;

                            $blvalue = 0;



                            if ($behaviordata != false) {
                                foreach ($behaviordata as $bevalue) {
                                    if ($bevalue['month'] == $m) {
                                        $bl = 1;

                                        $blvalue = $bevalue['count'];
                                    }
                                }
                            }

                            if ($bl == 1) {
                                $headern.= "<td>" . $blvalue . "</td>";
                            } else {
                                $headern.= "<td>0</td>";
                            }
                        }
                        $headern.= "</tr>";

                        $headern.= "</table>";
                        $k++;
                    }
                }
                $orgmessagen.=$headern;
            } else {
                $orgmessagen.='<table border="1"><tr>';
                $orgmessagen.= "<td>No Teachers Found</td>";
                $orgmessagen.='</tr></table>';
            }
            //2
            //3
            $headerb = '';
            $orgmessageb = '';
            if ($teachers != false) {



                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)
                    {
                        if ($k == 0) {
                            $headerb.='<table class="gridtable"><tr>';
                            $headerb.='<td><b>Month</b></td>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('M', mktime(0, 0, 0, $m));
                                $headerb.='<td>' . $monthname . '</td>';
                            }
                            $headerb.='</tr>';
                        }

                        $reportdata = $this->teachermodel->getTeacherObservationReport($teachervalue['teacher_id'], $year);


                        $headerb.='<tr><td>Checklist&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $forma = 0;
                                $formaformal = 0;
                                $formainformal = 0;

                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'forma') {
                                        $forma = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formaformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formainformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($forma == 1) {

                                    $headerb.="<td><span class='blue'>" . $formaformal . "</span>,&nbsp;<span class='yellow'>" . $formainformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";

                        $headerb.='<tr><td>Scale</td>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $formb = 0;
                                $formbformal = 0;
                                $formbinformal = 0;
                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formb') {
                                        $formb = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formbformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formbinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($formb == 1) {

                                    $headerb.="<td><span class='blue'>" . $formbformal . "</span>,&nbsp;<span class='yellow'>" . $formbinformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";

                        $headerb.='<tr><th>Proficiency</th>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $formp = 0;
                                $formpformal = 0;
                                $formpinformal = 0;

                                foreach ($reportdata as $reportvalue) {


                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formp') {
                                        $formp = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formpformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formpinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($formp == 1) {

                                    $headerb.="<td><span class='blue'>" . $formpformal . "</span>,&nbsp;<span class='yellow'>" . $formpinformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";

                        $headerb.='<tr><td>Likert</td>';
                        if ($reportdata != false) {
                            for ($m = 1; $m <= 12; $m++) {
                                $formc = 0;
                                $formcformal = 0;
                                $formcinformal = 0;

                                foreach ($reportdata as $reportvalue) {

                                    if ($reportvalue['month'] == $m && $reportvalue['report_form'] == 'formc') {
                                        $formc = 1;
                                        if ($reportvalue['period_lesson'] == 'Formal') {
                                            $formcformal = $reportvalue['count'];
                                        }
                                        if ($reportvalue['period_lesson'] == 'Informal') {
                                            $formcinformal = $reportvalue['count'];
                                        }
                                    }
                                }
                                if ($formc == 1) {

                                    $headerb.="<td><span class='blue'>" . $formcformal . "</span>,&nbsp;<span class='yellow'>" . $formcinformal . "</span></td>";
                                } else {

                                    $headerb.= "<td>&nbsp;</td>";
                                }
                            }
                        } else {
                            for ($m = 1; $m <= 12; $m++) {
                                $headerb.= "<td>&nbsp;</td>";
                            }
                        }
                        $headerb.= "</tr>";
                        $headerb.= "</table>";
                        $k++;
                    }
                }
                $orgmessageb.=$headerb;
            } else {
                $orgmessageb.='<table border="1"><tr>';
                $orgmessageb.= "<td>No Teachers Found</td>";
                $orgmessageb.='</tr></table>';
            }

            //3

            $t = $this->session->userdata('teacher_name');
            $this->load->Model('report_disclaimermodel');
            $reportdis = $this->report_disclaimermodel->getallplans(5);
            $dis = '';
            $fontsize = '';
            $fontcolor = '';
            if ($reportdis != false) {

                $dis = $reportdis[0]['tab'];
                $fontsize = $reportdis[0]['size'];
                $fontcolor = $reportdis[0]['color'];
            }
            $str = '<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
               <div style="width:650px;font-size:' . $fontsize . 'px;color:#' . $fontcolor . ';">' . $dis . '</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="' . $view_path . 'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>' . ucfirst($this->session->userdata('district_name')) . '</b> 		
		<br />';
            if ($this->session->userdata('login_type') == 'teacher') {
                $str.='<b>' . ucfirst($this->session->userdata('school_self_name')) . '</b>';
            } else {
                $str.='<b>' . ucfirst($this->session->userdata('school_name')) . '</b>';
            }





            $str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />

		<b><div style="font-size:14px;">Teacher Name:&nbsp;' . $t . '</div></b><br/><br/>
		<b><div style="font-size:14px;">Year:&nbsp;' . $year . '</div></b><br/><br/>		
		<b><div style="font-size:14px;">Notification Activity :</div></b><br/><br/><br/>		
		' . $style . $orgmessagen . '
		<br/><br/><br/><br/><b><div style="font-size:14px;">Professional Development Activity:</div></b><br/><br/><br/>		
		' . $orgmessage . '		
		<br/><br/><br/><br/><b><div style="font-size:14px;">Instructional Observation Activity  :</div></b>
		<br/><br/><b><div style="font-size:12px;">Legend:&nbsp;&nbsp;&nbsp;&nbsp;Blue:&nbsp;&nbsp;<span class="blue">Formal</span> &nbsp; Red:&nbsp;&nbsp;<span class="yellow">Informal</span></div></b><br/>		
		' . $orgmessageb . '
		<br />
		<br />
   </page> ';


            $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(18, 20, 20, 2));
            $content = ob_get_clean();




            $html2pdf->WriteHTML($str);
            $html2pdf->Output();
        } else {
//observer
            $view_path = $this->config->item('view_path');
            $header = '';
            $style = '';
            $message = '';
            $orgmessage = '';
            $this->load->Model('teachermodel');
            if ($this->session->userdata('login_type') == 'teacher') {
                $teachers = $this->teachermodel->getteacherById($this->session->userdata('teacher_id'));
            } else {
                $teachers = $this->teachermodel->getTeachersBySchool($this->session->userdata('school_id'));
            }
            $style.='<style type="text/css">
table.gridtable {	
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
width:46.2px;}
.blue{
color:#5987D3;
}
.yellow{
color:#DD6435;
}
</style>
';

            if ($teachers != false) {



                $k = 0;

                foreach ($teachers as $key => $teachervalue) {
                    //for($j=1;$j<=7;$j++)
                    {
                        if ($k == 0) {
                            $header.='<table border="1"><tr>';
                            $header.='<td><table><tr><td><b>Month Name</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('F', mktime(0, 0, 0, $m));
                                $header.='<tr><td>' . $monthname . '</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                            }
                            $header.='</table></td>';
                            $header.='<td><table><tr><td><b>Tasks</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $header.='<tr><td>LP</td></tr>';
                                $header.='<tr><td>HN</td></tr>';
                                $header.='<tr><td>BL</td></tr>';
                            }
                            $header.='</table></td>';
                        }
                        if ($k % 5 == 0 && $k != 0) {
                            $orgmessage.=$header . $message . '</tr></table>';
                            $header = '';
                            $message = '';
                            $header.='<table border="1"><tr>';
                            $header.='<td><table><tr><td><b>Month Name</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $monthname = date('F', mktime(0, 0, 0, $m));
                                $header.='<tr><td>' . $monthname . '</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                                $header.='<tr><td>&nbsp;</td></tr>';
                            }
                            $header.='</table></td>';
                            $header.='<td><table><tr><td><b>Tasks</b></td></tr>';
                            for ($m = 1; $m <= 12; $m++) {
                                $header.='<tr><td>LP</td></tr>';
                                $header.='<tr><td>HN</td></tr>';
                                $header.='<tr><td>BL</td></tr>';
                            }
                            $header.='</table></td>';
                        }
                        $this->load->Model('lessonplanmodel');
                        $lessondata = $this->lessonplanmodel->getTeacherLessonPlanReport($teachervalue['teacher_id'], $year);
                        $homeworkdata = $this->lessonplanmodel->getTeacherHomeworkPlanReport($teachervalue['teacher_id'], $year);
                        $behaviordata = $this->lessonplanmodel->getTeacherBehaviorPlanReport($teachervalue['teacher_id'], $year);

                        $message.= "<td><table>";
                        $message.= "<tr>";
                        $message.= "<td><b>" . $teachervalue['firstname'] . "  " . $teachervalue['lastname'] . "</b></td>";
                        $message.= "</tr>";

                        for ($m = 1; $m <= 12; $m++) {
                            $lp = 0;
                            $hn = 0;
                            $bl = 0;
                            $lpvalue = 0;
                            $hnvalue = 0;
                            $blvalue = 0;


                            if ($lessondata != false) {
                                foreach ($lessondata as $lessonvalue) {
                                    if ($lessonvalue['month'] == $m) {
                                        $lp = 1;

                                        $lpvalue = $lessonvalue['count'];
                                    }
                                }
                            }
                            if ($homeworkdata != false) {
                                foreach ($homeworkdata as $homevalue) {
                                    if ($homevalue['month'] == $m) {
                                        $hn = 1;

                                        $hnvalue = $homevalue['count'];
                                    }
                                }
                            }
                            if ($behaviordata != false) {
                                foreach ($behaviordata as $bevalue) {
                                    if ($bevalue['month'] == $m) {
                                        $bl = 1;

                                        $blvalue = $bevalue['count'];
                                    }
                                }
                            }
                            if ($lp == 1) {
                                $message.= "<tr><td>" . $lpvalue . "</td></tr>";
                            } else {
                                $message.= "<tr><td>0</td></tr>";
                            }
                            if ($hn == 1) {
                                $message.= "<tr><td>" . $hnvalue . "</td></tr>";
                            } else {
                                $message.= "<tr><td>0</td></tr>";
                            }
                            if ($bl == 1) {
                                $message.= "<tr><td>" . $blvalue . "</td></tr>";
                            } else {
                                $message.= "<tr><td>0</td></tr>";
                            }
                        }


                        $message.= "</table></td>";
                        $k++;
                    }
                }
                $orgmessage.=$header . $message . '</tr></table>';
            } else {
                $orgmessage.='<table border="1"><tr>';
                $orgmessage.= "<td>No Teachers Found</td>";
                $orgmessage.='</tr></table>';
            }



            $this->load->Model('report_disclaimermodel');
            $reportdis = $this->report_disclaimermodel->getallplans(5);
            $dis = '';
            $fontsize = '';
            $fontcolor = '';
            if ($reportdis != false) {

                $dis = $reportdis[0]['tab'];
                $fontsize = $reportdis[0]['size'];
                $fontcolor = $reportdis[0]['color'];
            }
            $str = '<page backtop="7mm" backbottom="7mm" backleft="1mm" backright="10mm"> 
        <page_header> 
             
        </page_header> 
        <page_footer> 
              <div style="width:650px;font-size:' . $fontsize . 'px;color:#' . $fontcolor . ';">' . $dis . '</div>
        </page_footer> 		
		<div style="padding-left:508px;position:relative;">
		<img alt="Logo"  src="' . $view_path . 'inc/logo/logo150.png"/>
<div style="font-size:13px;color:#cccccc;position:absolute;top:10px;width: 400px">
		<b>' . ucfirst($this->session->userdata('district_name')) . '</b> 		
		<br />';
            if ($this->session->userdata('login_type') == 'teacher') {
                $str.='<b>' . ucfirst($this->session->userdata('school_self_name')) . '</b>';
            } else {
                $str.='<b>' . ucfirst($this->session->userdata('school_name')) . '</b>';
            }




            $str.='</div>
		</div>
		<br />
		<div style="height:6px;width:650px;border:2px solid #cccccc;background-color:#cccccc;"> </div><br />

		<b><div style="font-size:14px;">Notification Activity Count:</div></b><br/><br/><br/>
		<b><div style="font-size:12px;">Year:&nbsp;' . $year . '&nbsp;Legend:</div>
		<div>LP:&nbsp;Lesson Plan &nbsp; HN:&nbsp;Home Notification &nbsp;BL:&nbsp;Behavior & Learning  </div></b><br/>		
		' . $style . $orgmessage . '
		<br />
		<br />
   </page> ';

            $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(18, 20, 20, 2));
            $content = ob_get_clean();




            $html2pdf->WriteHTML($str);
            $html2pdf->Output();
        }
    }
	
	
	
	  
	

    function valid_email($str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }

    function timesIntersects($time1_from, $time1_till, $time2_from, $time2_till) {
        $out;
        $time1_st = strtotime($time1_from);
        $time1_end = strtotime($time1_till);
        $time2_st = strtotime($time2_from);
        $time2_end = strtotime($time2_till);
        $duration1 = $time1_end - $time1_st;
        $duration2 = $time2_end - $time2_st;
        $time1_length = date("i", strtotime($time1_till . "-$time1_from"));
        if (
                (($time1_st <= $time2_st && $time2_st <= $time1_end && $time1_end <= $time2_end) ||
                ($time1_st <= $time2_st && $time2_st <= $time2_end && $time2_end <= $time1_end) &&
                ($duration1 >= $duration2)
                ) ||
                ( $time1_st <= $time2_st && $time2_st <= $time1_end && $time1_end <= $time2_end) &&
                ($duration1 < $duration2)
        ) {
            return true;
        }


        return false;
    }

    function week_from_monday($date, $days, $sk = false) {
        // Assuming $date is in format DD-MM-YYYY
        list($day, $month, $year) = explode("-", $date);

        // Get the weekday of the given date
        $wkday = date('l', mktime('0', '0', '0', $month, $day, $year));


        switch ($wkday) {
            case 'Monday': $numDaysToMon = 0;
                break;
            case 'Tuesday': $numDaysToMon = 1;
                break;
            case 'Wednesday': $numDaysToMon = 2;
                break;
            case 'Thursday': $numDaysToMon = 3;
                break;


            case 'Friday': $numDaysToMon = 4;
                break;
            case 'Saturday': $numDaysToMon = 5;
                break;
            case 'Sunday': $numDaysToMon = 6;
                break;
        }

        // Timestamp of the monday for that week
        $monday = mktime('0', '0', '0', $month, $day, $year);

        $seconds_in_a_day = 86400;

        // Get date for 7 days from Monday (inclusive)

        $j = $numDaysToMon;

        for ($i = 0; $i < $days; $i++) {




            if ($j > 6) {
                $j = 0;
            }

            $dates[$i]['date'] = date('m/d/Y', $monday + ($seconds_in_a_day * $i));

            if ($j == 0) {
                $dates[$i]['week'] = 'Monday';
            }
            if ($j == 1) {
                $dates[$i]['week'] = 'Tuesday';
            }
            if ($j == 2) {
                $dates[$i]['week'] = 'Wednesday';
            }
            if ($j == 3) {
                $dates[$i]['week'] = 'Thursday';
            }
            if ($j == 4) {
                $dates[$i]['week'] = 'Friday';
            }
            if ($j == 5) {
                $dates[$i]['week'] = 'Saturday';
            }
            if ($j == 6) {
                $dates[$i]['week'] = 'Sunday';
            }
            $j++;
        }

        if ($sk == true && $days != 28) {
            $days++;
            for ($i = $days; $i <= 35; $i++) {

                $dates[$i]['week'] = '';
                $dates[$i]['date'] = '';
            }
        }
        return $dates;
    }

    function getother($id) {


        $this->load->Model('lessonplanmodel');
        $status = $this->lessonplanmodel->getother($id);

        echo '<b>Standard:</b> ' . $status[0]['standard'];
        echo '<br/>';
        echo '<b>Differentiated Instruction:</b> ' . $status[0]['diff_instruction'];
    }

    function getteacherBySchoolId() {
        $this->load->model('teachermodel');
        $school_id = $this->input->post('school_id');
        $teachers = $this->teachermodel->getTeachersBySchoolApi($school_id);
        echo json_encode($teachers);
        exit;
    }
     public function valuehomeworksave() {
        $this->load->Model('lessonplanmodel');
        $r = $this->lessonplanmodel->update_homework(); //$_POST contains all the editable data when we hit the save button in the grid
//        echo $this->db->last_query();exit;
        echo $r;
    }
    
    function getstudents_by_teacher($teacher_id='')
	{
		 parse_str($_SERVER['QUERY_STRING'], $_GET); 
		 
		$q = strtolower($_GET["q"]);
		
		if (!$q) return;		

	$this->load->Model('parentmodel');
      	$students = $this->parentmodel->get_students_By_teacher($q);
	//print_r($students);exit;
		if($students!=false)
		{	
			foreach($students as $student)
			{
				$sname = $student['firstname'].' '.$student['lastname'];			
				$sid = $student['student_id'];				
				echo "$sname|$sid\n";	
				
			}	
		
		}
		else
		{
		
			echo 'No Students Found.|';
		}
		
			
	
	}	

         function getobserverBySchoolId() {
        $this->load->model('observermodel');
        $school_id = $this->input->post('school_id');
        $observer = $this->observermodel->getobserverByschoolId($school_id);
        echo json_encode($observer);
        exit;
    }

    function getWeeks($startDate, $endDate) {
        $startDateUnix = strtotime($startDate);
        $endDateUnix = strtotime($endDate);

        $currentDateUnix = $startDateUnix;

        $weekNumbers = array();
        $cnt = 0;
        while ($currentDateUnix < $endDateUnix) {
            $weekNumbers[$cnt]['week'] = date('W', $currentDateUnix)-1;
            $weekNumbers[$cnt]['year'] = date('Y', $currentDateUnix);
            $currentDateUnix = strtotime('+1 week', $currentDateUnix);
            $cnt++;
        }

        return $weekNumbers;
    }
    function getSubjectByLessonPlan($lesson_week_plan_id){
         $this->load->Model('lessonplanmodel');
         $lesson_plan_details = $this->lessonplanmodel->getlessonplansbyid($lesson_week_plan_id);
         echo $lesson_plan_details[0]['subject_name'];
         exit;
    }

}