<?php 
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
$view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');


class Teacherclusterreport extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_teacher()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{ 
	 	
        $data['idname']='tools';
// error_reporting(0);
		 
		 if($this->session->userdata("login_type")=='teacher')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('teacherclusterreportmodel');
			    $teacher_id = $this->session->userdata('teacher_id');
				if($this->session->userdata('district_id')){
					$district_id = $this->session->userdata('district_id');
				} else {
				$data['dist'] = $this->teacherclusterreportmodel->getteacherDistrictId($teacher_id);
				$district_id = $data['dist'][0]['district_id'];
				}
				$this->load->Model('videooptionmodel');
        		$data['video_option'] = $this->videooptionmodel->get_option();
        		
				$data['records'] = $this->teacherclusterreportmodel->getallcategoryAndAssessment($district_id,$teacher_id);
				$this->load->view('teacherclusterreport/index',$data);
	   
	 	 }
 		 
	}
	
	function getTestclusterhtml()
	{
		error_reporting(0);
		$ass_id = $_REQUEST['assignment_id']; 
		
 	 	 $teacher_id = $this->session->userdata('teacher_id'); 
		 $data = array();
		 $this->load->model('teacherclusterreportmodel');
		 $data= $this->teacherclusterreportmodel->getTestItem($teacher_id,$ass_id);
		  $strClass ='';
		 $strClass .= '<select class="span12 chzn-select" style="width: 300px;" name="test_cluster" id="test_cluster" onchange="updateTestcluster(this.value)">
        <option value="-1"  selected="selected">-Please Select-</option>' ;
		
		if(count($data[0]['test_cluster'])>0)
		{
	 	   $strClass .= '<option value="all" >All</option>';
		 }
      	   foreach($data as $key => $value)
			{
				 $testclu = htmlspecialchars($value['test_cluster']);
				 $strClass .='<option value="'.$testclu.'">'.utf8_decode($value['test_cluster']).'</option>';
		 	}
         $strClass .= '</select> ';
 		 echo $strClass;
	}
	
	function getReportHtml()
	{
	 	//$teacherid= $_REQUEST['teacherid'];
		error_reporting(1);
		 $teacherid = $this->session->userdata('teacher_id');
		 $this->load->model('teacherclusterreportmodel');
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		$test_item= $_REQUEST['test_cluster'];
			
		 $data['dist'] = $this->teacherclusterreportmodel->getteacherDistrictId($teacherid);
		$district_id = $data['dist'][0]['district_id'];
				
		$catid= $_REQUEST['catid'];
		
		if($catid != -1)
		{	 
		  	 //<td>All</td>
			 $htmlTable = '<div class="outer">  <div class="inner"><table id="innertab"> ';
			 if($test_item == 'all')
			 {
				 $htmlTable .= '<tr class="tchead"><th width="170" align="left"  bgcolor="#5e3364" style="color:#fff;font-size:13px;" class="rotated_cell_sn" >  Student Name </th>';
				 
				$data['Testitem']=$this->teacherclusterreportmodel->getTestItem($teacherid,$ass_id); 
			 	$rows =  count($data['Testitem'][0]); 
				$row =  count($data['Testitem'][0]); 
				if($row > 0)
				{
				 	foreach($data['Testitem'] as $key => $value)
					{
						$htmlTable .= "<td  class='rotated_cell'> <div class='rotate_text'>".utf8_decode($value['test_cluster'])." </div></td>";
					}
					$htmlTable .="</tr>";
				}
				// <script> $(".rotated_cell").each(function(){$(this).height($(this).width())}) </script
				echo ' <style type="text/css">  
				 table #innertab{
					 table-layout: fixed;
					 }</style> ';
				echo '<script type="text/javascript" > $(document).ready(function () {
						var heights = $("#innertab td").map(function() {
   						 return $(this).width();
						}).get();

				var maxHeight = Math.max.apply(Math, heights) +20;
				var newwidth =50;
				  $(".rotated_cell").height(maxHeight);
				 $(".rotated_cell_sn").height(maxHeight);
		 		$(".rotated_cell").width(newwidth);}) </script>' ;
// $(".rotated_cell").css({"white-space":"nowrap"});
			 }
			 else
			 {
				 $htmlTable .= '<tr class="tchead"><th width="170" align="left"  bgcolor="#5E3364" style="color:#fff;font-size:13px;" >  Student Name </th><td  style="color:#fff;font-size:13px;" bgcolor="#5E3364" style="white-space:nowrap;" valign="top">'.utf8_decode($test_item).'</td></tr>';
			 }
			$data['student'] = $this->teacherclusterreportmodel->getteacherstudent($teacherid);
			$rows =  count($data['student']);
			$row1 =  count($data['student'][0]); 
			$tablebody = ''; 
			if($row1 > 0)
			{
				$tablerow ='';
				for($i = 0 ;$i<$rows; $i++)
				{
					$tablerow .= "<tr  bgcolor='#EEF9E3'> <th style='height:inherit' bgcolor='' >   ".$data['student'][$i]['Name']." ".$data['student'][$i]['Surname']." </th>";
					 if($test_item == 'all')
					 {
						foreach($data['Testitem'] as $key => $value)
						{
							$Per = $this->teacherclusterreportmodel->getPercentageByStudentAndCluster($data['student'][$i]['UserID'],$catid,$value['test_cluster'],$district_id,$fDate,$tDate,$ass_id);
							$tablerow .= "<td style='height:inherit'>".$Per."</td>";
						}
					 }
					 else
					 {
						 $Per = $this->teacherclusterreportmodel->getPercentageByStudentAndCluster($data['student'][$i]['UserID'],$catid,$test_item,$district_id,$fDate,$tDate,$ass_id);
							$tablerow .= "<td style='height:inherit' >".$Per."</td>";
					 }
					$tablerow .="</tr>";
				}
				 $tablebody .= $tablerow;
			}				
			else
			{
				$tablebody = '<tr><td></td><td colspan="3" align="center"> No Record Found.</td></tr>';
			}
		$htmlTable .= $tablebody;
		$htmlTable .= "</table> </div></div>";
		}	
		else
		 {
			$htmlTable = "Please select Category.";
		 }
		 echo $htmlTable;
			
	}
	
	function getCsv()
	{
		$teacherid = $this->session->userdata('teacher_id');
		$catid= $_REQUEST['catid'];
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		$test_item= $_REQUEST['test_cluster'];
		
		
		if($catid != -1)
		{	 
			$this->load->model('teacherclusterreportmodel');
		$data['school'] = $this->teacherclusterreportmodel->getteacherschool($teacherid);
			
 			$district_id = $data['school'][0]['district_id'];
			
			$school_name = $data['school'][0]['school_name'];
			if($_SERVER['HTTP_HOST'] == 'localhost')
			{
				$path = $_SERVER['DOCUMENT_ROOT']."/WAPTrunk/system/application/views/teacherclusterreport/";
			}
			else
			{
				$path = $_SERVER['DOCUMENT_ROOT']."/system/application/views/teacherclusterreport/";
			}
			$filename = $path."teacherclusterreport.csv";
			$handle = fopen($filename, 'w+');
			
			$columnTitle = array();
			$columnTitle[0] = 'Student Name';
			
			 if($test_item == 'all')
			 { 	
				$data['Testitem']=$this->teacherclusterreportmodel->getTestItem($teacherid,$ass_id);
				foreach($data['Testitem'] as $key => $value)
				{
					$columnTitle[]= utf8_decode($value['test_cluster']);
				}
			 }
			 else
			 {
				 $columnTitle[] = $test_item;
			 }
			if($catid != 0) 
			{
				$data['catname'] = $this->teacherclusterreportmodel->getcategorynamebyId($catid);
				 $catname = $data['catname'][0]['cat_name']; 
			} 
			else {
			 $catname = "All Category";} 
			$header =  "Raw Data Report of Category - ".$catname;
			fputcsv($handle,array(''));
			fputcsv($handle,array('','',$header));
			fputcsv($handle,array(''));
			fputcsv($handle,$columnTitle);
			
	    	$data['student'] = $this->teacherclusterreportmodel->getteacherstudent($teacherid);
		    $rows =  count($data['student']);
		 	if($rows > 0)
			{  
		  	 	for($i = 0 ;$i<$rows; $i++)
				{
					$rowarray = array();
			 		$rowarray[0] = $data['student'][$i]['Name']." ".$data['student'][$i]['Surname'];
					if($test_item == 'all')
			 		{
						foreach($data['Testitem'] as $key => $value)
						{
							$Per = $this->teacherclusterreportmodel->getPercentageByStudentAndCluster($data['student'][$i]['UserID'],$catid,$value['test_cluster'],$district_id,$fDate,$tDate,$ass_id);
							$rowarray[] = $Per;
						}
					}
					else
					{
						$Per = $this->teacherclusterreportmodel->getPercentageByStudentAndCluster($data['student'][$i]['UserID'],$catid,$test_item,$district_id,$fDate,$tDate,$ass_id);
						$rowarray[] = $Per;
					}
					fputcsv($handle,$rowarray);
			 	}
			}
			
			fclose($handle);
		 	$base =  base_url();
			$b = explode('index.php',$base);
			$baseUrl = $b[0];
		 	echo" <a href='".$baseUrl."system/application/views/teacherclusterreport/teacherclusterreport.csv'> <input type='button' id='csvbutton' value='Download CSV File' /> </a>";
			
		}
		else
		{
			echo "Please select School.";
		}

 		 exit;
	}
	
	function getPDF()
	{
		error_reporting(1);
		$teacherid = $this->session->userdata('teacher_id');
		$this->load->model('teacherclusterreportmodel');
		$catid= $_REQUEST['catid'];
		$data['fdate'] = $fDate= $_REQUEST['fdate'];
		$data['tdate'] = $tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		$test_item= $_REQUEST['test_cluster'];
		
		$data['dist'] = $this->teacherclusterreportmodel->getteacherDistrictId($teacherid);
		$district_id = $data['dist'][0]['district_id'];
				
		$catid= $_REQUEST['catid'];
		$data['assessment_time'] = $this->teacherclusterreportmodel->getassessmenttime($ass_id);
		$this->load->Model('selectedstudentreportmodel');
		$data['assignment'] = $this->selectedstudentreportmodel->getassessment($assignment_id);
 		$assignment_name = $data['assignment'][0]['assignment_name'];	

		$data['school'] = $this->teacherclusterreportmodel->getteacherschool($teacherid);
	    $data['school_name'] = $data['school'][0]['school_name'];

	    //Code for disclaimer added by Hardik
	    $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(2);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(2);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdes != false) {

            $data['description'] = $reportdes[0]['tab'];
            $data['description_fontsize'] = $reportdes[0]['size'];
            $data['description_fontcolor'] = $reportdes[0]['color'];
        }
	 
 		$data['teachername'] = $this->teacherclusterreportmodel->getteachername($teacherid);
		$data['teacher_name'] = $data['teachername'][0]['firstname'].' '.$data['teachername'][0]['lastname'];

		$data['student'] = $this->teacherclusterreportmodel->getteacherstudent($teacherid);
		$data['totalstudent'] = count($data['student']);
		$rowst =  count($data['student']);
		$row1st =  count($data['student'][0]); 
		$htmlTable ='';
		 if($test_item == 'all')
		 {
		 	$data['Testitem']=$this->teacherclusterreportmodel->getTestItem($teacherid,$ass_id); 
		 	
			$cluster =  array();
			foreach($data['Testitem'] as $key => $value)
			{
				$cluster[] = $value['test_cluster'];
			}
			$cntcluster = count($cluster);
			$perpagecluster = 5;
			// echo "<pre>" ;print_r($cluster);
			if($cntcluster > 0)
			{
			 	if($row1st > 0)
				{
					 $cnttab = ceil($cntcluster/$perpagecluster);
				    $cntclstrecordend = 0;
					$cntclstrecordstart = 0;

					 for($tab = 1 ;$tab <= $cnttab ; $tab++)
					 {
						 $cntclstrecordstart = $cntclstrecordend;
						if($cnttab != $tab){
						 $cntclstrecordend = $cntclstrecordstart + $perpagecluster;
						 }
						 else{
							 $cntclstrecordend = $cntcluster;
						 }
						 
						 $htmlTableinner = '';
						 $htmlTableinner .= '<table class="gridtable"><tr><td style="width:140px"></td>';
							$clstrecord =0;
						for($clstrecord = $cntclstrecordstart ;$clstrecord<$cntclstrecordend; $clstrecord++)
						{
			 				$htmlTableinner .= '<td style="width:81px;vertical-align: top; text-align: center;background-color: #939393;font-size: 11px;">'.(str_replace('Ã','í',$cluster[$clstrecord])).'</td>';
						}
						$htmlTableinner .="</tr>";
						$tablebody = '';
					 
						foreach($data['student'] as $key => $value)
						{
							$tablerow ='';
							$tablerow .= "<tr ><td class='blue' style=\"width:140px;\">".$value['Name']." ".$value['Surname']."</td>";
							for($clstrecord = $cntclstrecordstart ;$clstrecord<$cntclstrecordend; $clstrecord++)
							{
								$Per = $this->teacherclusterreportmodel->getPercentageByStudentAndCluster($value['UserID'],$catid,$cluster[$clstrecord],$district_id,$fDate,$tDate,$ass_id);
								$tablerow .= "<td align='center' style='text-align:center;' >".$Per."</td>";
							}
							
							$tablerow .="</tr>";
						 $tablebody .= $tablerow;
	 					}
						$tablebody.= "</table> <br /><br /><br />";
						$htmlTableinner.= $tablebody;
						$htmlTable.= $htmlTableinner;
 					 } //echo	$htmlTable;
				}				
				else
				{
					$tablebody = '<tr><td></td><td colspan="3" align="center"> No Record Found.</td></tr>';
				}
			}
 			// <script> $(".rotated_cell").each(function(){$(this).height($(this).width())}) </script
			
		 }
		 else
		 {
			 $htmlTable .= '<table border="1" width="60%" style="font-size:12"><tr><td>   </td><td bgcolor="#C3D79A" style="white-space:nowrap;" valign="top">'.$test_item.'</td></tr>';
			 
			 	if($row1st > 0)
				{
					$tablerow ='';
					for($i = 0 ;$i<$rowst; $i++)
					{
						$tablerow .= "<tr > <td width='8%' style='white-space:nowrap;'>".$data['student'][$i]['Name']." ".$data['student'][$i]['Surname']." </td>";
						 $Per = $this->teacherclusterreportmodel->getPercentageByStudentAndCluster($data['student'][$i]['UserID'],$catid,$test_item,$district_id,$fDate,$tDate,$ass_id);
						$tablerow .= "<td align='center' style='text-align:center;' >".$Per."</td>";
						$tablerow .="</tr>";
					}
					 $tablebody .= $tablerow;
				}				
				else
				{
					$tablebody = '<tr><td></td><td colspan="3" align="center"> No Record Found.</td></tr>';
				}
				$htmlTable .= $tablebody;
				$htmlTable .= "</table>";
 		 }
		
		$data['htmlTable'] = $htmlTable;

 			
		$currdate = date('Y-m-d');
		$data['grade'] = $this->teacherclusterreportmodel->getteachergrade($teacherid);

		 $classroom = $this->teacherclusterreportmodel->getRoomByTeacherId($teacherid);
		 $data['room'] = $classroom[0]['name']; 
		
		$data['view_path'] = $this->config->item('view_path');
		$cach = date("H:i:s");
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        $ostr = $this->parser->parse('teacherclusterreport/teacherclusterpdf', $data, TRUE);
        //echo $ostr;exit;
        $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();
	}
	 
	
}	//class end