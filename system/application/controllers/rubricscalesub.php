<?php
/**
 * rubricscalesub Controller.
 *
 */
class Rubricscalesub extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false ){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $this->load->Model('rubricscalemodel');
	  if($data['district'][0]['district_id'])
	  {
		$data['rubricscale']=$this->rubricscalemodel->getallrubricscalesbydistrictId($data['district'][0]['district_id']);
	  }
	  else
	  {
		$data['rubricscale']=false;
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('rubricscalesub/index',$data);
	  }
	  else if($this->session->userdata("login_type")=='user')
	  {
	  
	    $this->load->Model('rubricscalemodel');
	  if($this->session->userdata('district_id'))
	  {
		$data['rubricscale']=$this->rubricscalemodel->getallrubricscalesbydistrictId($this->session->userdata('district_id'));
	  }
	  else
	  {
		$data['rubricscale']=false;
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('rubricscalesub/all',$data);
	  
	  
	  }
	
	}
	
	function getallrubricscalesbydistrictId($district_id)
	{
		$this->load->Model('rubricscalemodel');
		$data['rubricscale']=$this->rubricscalemodel->getallrubricscalesbydistrictId($district_id);
		echo json_encode($data);
		exit;
	
	
	}
	
	function copy()
	{
	  $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);	
	  $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  $this->load->Model('rubricscalemodel');
	  if($data['district'][0]['district_id'])
	  {
		$data['rubricscale']=$this->rubricscalemodel->getallrubricscalesbydistrictId($data['district'][0]['district_id']);
	  }
	  else
	  {
		$data['rubricscale']=false;
	  }
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('rubricscalesub/copy',$data);
	
	}
	function pointcopy()
	{
		$data['status']=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    $this->load->Model('rubricscalesubmodel');
		    $status=$this->rubricscalesubmodel->addpoints();
			$data['status']=$status;
		 }
	   }
	   echo json_encode($data);
		exit;
	
	}
	function getrubricscalesubs($page,$state_id,$country_id,$group_id=false,$district_id=false)
	{
		if($group_id==false)
		{
		  $group_id='all';
		}
		
		
		$this->load->Model('rubricscalesubmodel');
		if($page!='all')
		{
			$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
			$total_records = $this->rubricscalesubmodel->getrubricscalesubCount($state_id,$country_id,$group_id,$district_id);
		
			$status = $this->rubricscalesubmodel->getrubricscalesubs($page, $per_page,$state_id,$country_id,$group_id,$district_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Rubric Scales</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Scale Name</td><td>State</td><td>District</td><td>Scale Sub Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['scale_id'].'" class="'.$c.'" >';
			
				print '
				<td>'.$val['scale_name'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districts_name'].'</td>
				<td>'.$val['sub_scale_name'].'</td>
				
				
				<td nowrap><input  class="btnsmall" title="Edit" type="button" value="Edit" name="Edit" onclick="pointedit('.$val['scale_id'].')"><input class="btnsmall" title="Delete"  type="button" name="Delete" value="Delete" onclick="pointdelete('.$val['sub_scale_id'].')" ></td>		</tr>';
				$i++;}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'rubricscalesubs');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Scale Name</td><td>State</td><td>District</td><td>Scale Sub Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Rubric Scales Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'rubricscalesubs');
						print $pagination;	
		}
		
		}
		else
		{
			$status = $this->rubricscalesubmodel->getrubricscalesubs($page, 'all',$state_id,$country_id,$group_id,$district_id);
		
		
		
		if($status!=FALSE){
			
			$chk="'chk_'";
			print '<div class="htitle">Rubric Scales</div><table class="tabcontent" cellspacing="0" cellpadding="0" id="conf" ><tr class="tchead"><td><input id="chk"	name="chk_grp" type="checkbox"	value="1" onclick= "toggleCheckboxes('.$chk.',this.checked)" /></td><td>Scale Name</td><td>State</td><td>District</td><td>Scale Sub Name</td></tr>';
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['sub_scale_id'].'" class="'.$c.'" >';
			
				print '
				<td><input type="checkbox" id="chk_'.$val['sub_scale_id'].'" name="point_id[]" value="'.$val['sub_scale_id'].'"></td>
				<td>'.$val['scale_name'].'</td>
				<td>'.$val['name'].'</td>
				<td>'.$val['districts_name'].'</td>
				<td>'.$val['sub_scale_name'].'</td>
				
				
						</tr>';
				$i++;}
				print '</table>';
               
				
                        
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td>Scale Name</td><td>State</td><td>District</td><td>Scale Sub Name</td></tr>
			<tr><td valign='top' colspan='10'>No Rubric Scales Found.</td></tr></table>
			";
			
			
		}
		
		
		}
	}
	
	function getrubricscalesubinfo($point_id)
	{
		if(!empty($point_id))
	  {
		$this->load->Model('rubricscalesubmodel');
		
		$data['rubricscalesub']=$this->rubricscalesubmodel->getrubricscalesubById($point_id);
		$data['rubricscalesub']=$data['rubricscalesub'];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	
	
	function add_rubricscalesub()
	{
	
	
		$this->load->Model('rubricscalesubmodel');
	
		$status=$this->rubricscalesubmodel->add_rubricscalesub();
		if($status!=0){
		       $data['message']="rubricscalesub added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_rubricscalesub()
	{
	$this->load->Model('rubricscalesubmodel');
	    $status=$this->rubricscalesubmodel->update_rubricscalesub();
		if($status==true){
		       $data['message']="rubricscalesub Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		echo json_encode($data);
		exit;		
	}
	
	function delete($dist_id)
	{
		
		$this->load->Model('rubricscalesubmodel');
		$result = $this->rubricscalesubmodel->deleterubricscalesub($dist_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	
	
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
/*						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
					*/	

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
						/*if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	