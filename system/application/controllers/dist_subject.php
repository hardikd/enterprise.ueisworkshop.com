<?php
/**
 * dist_subject Controller.
 *
 */
class Dist_subject extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_user()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{	  
	   	
		$data['view_path']=$this->config->item('view_path');
	    $this->load->view('dist_subject/all',$data);	  
	  
	
	}
	function getdist_subjects($page,$state_id,$country_id,$district_id=false)
	{
		if($district_id==false)
		{
		  $district_id='all';
		}
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('dist_subjectmodel');
		$total_records = $this->dist_subjectmodel->getdist_subjectCount($state_id,$country_id,$district_id);
		
			$status = $this->dist_subjectmodel->getdist_subjects($page, $per_page,$state_id,$country_id,$district_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Subjects</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td> Subject Name</td><td  >State</td><td >District Name</td><td>ELD</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['dist_subject_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['subject_name'].'</td>
				
				<td>'.$val['name'].'</td>
				<td>'.$val['districtsname'].'</td>';
				if($val['eld']==1)
				{
				
				print '<td>Yes</td>';
				
				}
				else
				{
				
				print '<td>No</td>';
				
				}
				
				print '<td nowrap><input title="Edit" class="btnsmall" type="button" value="Edit" name="Edit" onclick="dist_subjectedit('.$val['dist_subject_id'].')"><input title="Delete" class="btnsmall" type="button" name="Delete" value="Delete" onclick="dist_subjectdelete('.$val['dist_subject_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'dist_subjects');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td> Subject Name</td><td  >State</td><td >District Name</td><td>ELD</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No Subjects Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'dist_subjects');
						print $pagination;	
		}
		
		
	}
	
	function getdist_subjectinfo($dist_subject_id)
	{
		if(!empty($dist_subject_id))
	  {
		$this->load->Model('dist_subjectmodel');
		$this->load->Model('districtmodel');
		
		
		$data['dist_subject']=$this->dist_subjectmodel->getdist_subjectById($dist_subject_id);
		$data['district']=$this->districtmodel->getalldistricts();
		$data['dist_subject']=$data['dist_subject'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_dist_subject()
	{
	
	
		$this->load->Model('dist_subjectmodel');
		$pstatus=$this->dist_subjectmodel->check_dist_subject_exists();
	
		 if($pstatus==1)
		 {	 
			$estatus=$this->dist_subjectmodel->check_dist_eld_exists();
			if($estatus==1)
			{
			
			$status=$this->dist_subjectmodel->add_dist_subject();
			if($status!=0){
				   $data['message']="Subject added Sucessfully" ;
				   $data['status']=1 ;
		
		
					}
					else
					{
					  $data['message']="Contact Technical Support Update Failed" ;
					  $data['status']=0 ;
					
					
					}
					
			}
			else
			{
				$data['message']="ELD  Already Selected" ;
		    $data['status']=0 ;
			
			
			}	
		}
		else if($pstatus==0)
		{
			$data['message']="Subject With Same Name   Already Exists" ;
		    $data['status']=0 ;
		}
				
		echo json_encode($data);
		exit;	
	
	
	
	
	}
	function update_dist_subject()
	{
	$this->load->Model('dist_subjectmodel');
	$pstatus=$this->dist_subjectmodel->check_dist_subject_update();
	if($pstatus==1)
		 {
		 $estatus=$this->dist_subjectmodel->check_dist_eld_update();
		 if($estatus==1)
		 {
	   $status=$this->dist_subjectmodel->update_dist_subject();
		if($status==true){
		       $data['message']="Subject Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
			}				
			else
			{
				$data['message']="ELD  Already Selected" ;
		    $data['status']=0 ;
			
			
			}		
		}
		else if($pstatus==0)
		{
			$data['message']="Subject With Same Name   Already Exists" ;
		    $data['status']=0 ;
		}
			
		echo json_encode($data);
		exit;		
	}
	
	function delete($dist_subject_id)
	{
		
		$this->load->Model('dist_subjectmodel');
		$result = $this->dist_subjectmodel->deletedist_subject($dist_subject_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	
	
	
	}
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br /><br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
					/*	if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}
*/
						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>←&nbsp;Prev</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>←&nbsp;Prev</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#ddd;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next&nbsp;→</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next&nbsp;→</li>";
						}

						// TO ENABLE THE END BUTTON
					/*	if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
					*/	
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
}	