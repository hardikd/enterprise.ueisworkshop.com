<?php
/**
 * teacher Controller.
 *
 */
 ob_start();
$view_path = "system/application/views/";
require_once($view_path . 'inc/html2pdf/html2pdf.class.php');
class Teacher extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("admin/index");
		}
		
	}
	
	function index()
	{

		$data['idname']='attendance';
	  if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('schoolmodel');
	   $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  if($data['district']!=false)
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict($data['district'][0]['district_id']);
	  }
	  else
	  {
	    $data['school']='';
	  }
	  
	  //$data['school']=$this->schoolmodel->getallschools();
	  $data['view_path']=$this->config->item('view_path');
	  $this->load->view('teacher/index',$data);
	  }
	  else if($this->session->userdata("login_type")=='user')
	  {
	if($this->session->userdata('login_special') == 'district_management'){	
		   $this->load->Model('teachermodel');
		$this->load->Model('schoolmodel');
		$data['schools']=$this->schoolmodel->getschoolbydistrict();
//		print_r($data['school']);exit;
			foreach($data['schools'] as $schoolbyobserver ){

                    $schools[] = $data['school'][]=$this->schoolmodel->getschoolById($schoolbyobserver['school_id']);
					//print_r($data['school']);exit;
			}
              
		$data['view_path']=$this->config->item('view_path');
                foreach($schools as $school){
	
            $data['teachers'][] = $this->teachermodel->getteachersbydistrict(false, false,$school[0]['state_id'],$school[0]['country_id'],$school[0]['district_id'],$school[0]['school_id']);
                }
//		print_r($data);exit;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacher/all',$data);	
	}else{
	  
		    	$this->session->set_flashdata ('permission','Additional Permissions Required');
                redirect(base_url().'attendance/assessment');
	}
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
			$this->load->Model('schoolmodel');
                $this->load->Model('teachermodel');
                $schoolsbyobserver = $this->schoolmodel->getschoolbyobserver();
                foreach($schoolsbyobserver as $schoolbyobserver ){
                    $schools[] = $data['school'][]=$this->schoolmodel->getschoolById($schoolbyobserver['school_id']);

                }

		$data['view_path']=$this->config->item('view_path');
                foreach($schools as $school){
                    $data['teachers'][] = $this->teachermodel->getteachersbydistrict(false, false,$school[0]['state_id'],$school[0]['country_id'],$school[0]['district_id'],$school[0]['school_id']);

				}
                

		$this->load->view('teacher/all',$data);	
	  
	  
	  }
	  
	
	}

	function getteachers($page,$state_id,$country_id,$district_id=false,$school_id=false)
	{
		
		
		if($school_id==false)
		{
		  $school_id='all';
		}
		if($district_id==false)
		{
		  $district_id='all';
		}
		if($school_id=='empty' || $district_id=='empty' )
		{	
			print "<table class='table table-striped table-bordered' id='editable-sample'>
			<thead>
			<tr>
			<td >First Name</td>
			<td class='sorting'>Last Name</td>
			<td class='no-sorting'>User Name</td>
			<td class='no-sorting'>School Name</td>
			<td class='no-sorting'>Actions</td>
			</tr>
			
			<tr>
			<td valign='top' colspan='10'>No teachers Found.</td>
			</tr>
			</thead></table>
			";
		}
		else
		{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('teachermodel');
		$total_records = $this->teachermodel->getteacherCountbydistrict($state_id,$country_id,$district_id,$school_id);
		
			$status = $this->teachermodel->getteachersbydistrict($page, $per_page,$state_id,$country_id,$district_id,$school_id);
		
		
		
		if($status!=FALSE){
			
			
			print"<table class='table table-striped table-bordered' id='editable-sample'>
			<thead>
			<tr>
			<td >First Name</td>
			<td class='sorting'>Last Name</td>
			<td class='no-sorting'>User Name</td>
			<td class='no-sorting'>School Name</td>
			<td class='no-sorting'>Actions</td>
			</tr>
			</thead>";
			
			$i=1;
			foreach($status as $val){
				if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tbody><tr id="'.$val['teacher_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['firstname'].'</td>
					  <td class="hidden-phone">'.$val['lastname'].'</td>
					  <td class="hidden-phone">'.$val['username'].'</td>
					   <td class="center hidden-phone">'.$val['school_name'].'</td>
				
				<td class="center hidden-phone" nowrap>
				<button class="btn btn-primary" title="Edit" type="button"  value="Edit" name="Edit" onclick="teacheredit('.$val['teacher_id'].')"><i class="icon-pencil"></i></button>

				<button class="btn btn-danger" title="Delete" type="button"  name="Delete" value="Delete" onclick="teacherdelete('.$val['teacher_id'].')"><i class="icon-trash"></i></button>
				</td></tr></tbody>';
				
				
				
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination($total_records,$per_page,$page,'teachers');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='table table-striped table-bordered' id='editable-sample'>
			<thead>
			<tr>
			<td >First Name</td>
			<td class='sorting'>Last Name</td>
			<td class='no-sorting'>User Name</td>
			<td class='no-sorting'>School Name</td>
			<td class='no-sorting'>Actions</td>
			</tr>
			<tr>
			<td valign='top' colspan='10'>No teachers Found.</td>
			</tr>
			</thead></table>
			";
			
			$pagination=$this->do_pagination($total_records,$per_page,$page,'teachers');
						print $pagination;	
		}
		
		}
	}
	
	function getteacherinfo($teacher_id)
	{
		if(!empty($teacher_id))
	  {
		$this->load->Model('teachermodel');
		
		$data['teacher']=$this->teachermodel->getteacherById($teacher_id);
		$data['teacher']=$data['teacher'][0];
		echo json_encode($data);
		exit;
	  }
	
	}
	
	function add_teacher()
	{
	
	
		$this->load->Model('teachermodel');
		$pstatus=$this->teachermodel->check_teacher_exists();
	if($pstatus==1)
		 {
			 		$firstname = $this->input->post('firstname');
					$lastname = $this->input->post('lastname');
					$school_id = $this->input->post('school_id');
                    $username=$this->input->post('username');
		            $password= md5($this->input->post('password'));
					$copy_password= base64_encode($this->input->post('password'));
					$email= $this->input->post('email');
					$avatar= $this->input->post('avatar');
              		$phone= $this->input->post('phone');
			 
			 $this->load->library('email');
					$config['protocol'] = 'sendmail';
					$config['mailtype'] = 'html';
					//$filename = $_SERVER['DOCUMENT_ROOT'].'/waptrunk/email_templates/register_email.txt';
					
					//$handle = fopen($firstname, "r");
					//$contents = fread($handle, filesize($firstname));
					//fclose($handle);
					$contents = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Welcome to [[sitename]]!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Welcome to [[sitename]]!</h2>
Thanks for joining [[sitename]]. We listed your sign in details below, make sure you keep them safe.<br />
To verify your email address, please follow this link:<br />
<br />
<br />
Link doesn\'t work? Copy the following link to your browser address bar:<br />
<br />
Please verify your email within [[days]] , otherwise your registration will become invalid and you will have to register again.<br />
<br />
<br />
[[name]]<br />
Your email address: [[email]]<br />
Your Password: [[password]]
<br />
<br />
Have fun!<br />
The [[sitename]] Team
</td>
</tr>
</table>
</div>
</body>
</html>';
				//	echo $contents;exit;
					$contents  = str_replace('[[sitename]]','ueisworkshop.com',$contents);
					$contents  = str_replace('[[days]]','45 days',$contents);
					$contents  = str_replace('[[email]]',$email,$contents);
					$contents  = str_replace('[[password]]',$this->input->post('password'),$contents);
					$this->email->initialize($config);
					$this->email->from('admin@ueisworkshop.com', 'ueisworkshop admin');
					$this->email->reply_to('admin@ueisworkshop.com', 'ueisworkshop admin');
					$this->email->to($email);
					$this->email->subject('Welcome to ueisworkshop.com');
					$this->email->message($contents);
					$this->email->send();
			 
			 
		$status=$this->teachermodel->add_teacher();
		if($status!=0){
		       $data['message']="teacher added Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Teacher With Same FirstName In this School  Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="Teacher With User Name   Already Exists" ;
		    $data['status']=0 ;
		}
		echo json_encode($data);
		exit;	
	
	}
	function update_teacher()
	{
		$this->load->Model('teachermodel');
	    $pstatus=$this->teachermodel->check_teacher_update();
		if($pstatus==1)
		 {

				$firstname = $this->input->post('firstname');
				 $lastname = $this->input->post('lastname');	
	              $school_id = $this->input->post('school_id');
				   $username = $this->input->post('username');
					$password = $this->input->post('password');
                    $email= $this->input->post('email');
					
					
				 	$this->load->library('email');
					$config['protocol'] = 'sendmail';
					$config['mailtype'] = 'html';
					//$filename = $_SERVER['DOCUMENT_ROOT'].'/waptrunk/email_templates/register_email.txt';
					
					//$handle = fopen($firstname, "r");
					//$contents = fread($handle, filesize($firstname));
					//fclose($handle);
					$contents = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Welcome to [[sitename]]!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Welcome to [[sitename]]!</h2>
Thanks for joining [[sitename]]. We listed your sign in details below, make sure you keep them safe.<br />
To verify your email address, please follow this link:<br />
<br />
<br />
Link doesn\'t work? Copy the following link to your browser address bar:<br />
<br />
Please verify your email within [[days]] , otherwise your registration will become invalid and you will have to register again.<br />
<br />
<br />
[[name]]<br />
Your email address: [[email]]<br />
Your Password: [[password]]
<br />
<br />
Have fun!<br />
The [[sitename]] Team
</td>
</tr>
</table>
</div>
</body>
</html>';
				//	echo $contents;exit;
					$contents  = str_replace('[[sitename]]','ueisworkshop.com',$contents);
					$contents  = str_replace('[[days]]','45 days',$contents);
					$contents  = str_replace('[[email]]',$email,$contents);
					$contents  = str_replace('[[password]]',$this->input->post('password'),$contents);
					$this->email->initialize($config);
					$this->email->from('admin@ueisworkshop.com', 'ueisworkshop admin');
					$this->email->reply_to('admin@ueisworkshop.com', 'ueisworkshop admin');
					$this->email->to($email);
					$this->email->subject('Welcome to ueisworkshop.com');
					$this->email->message($contents);
					$this->email->send();	

		$status=$this->teachermodel->update_teacher();
		if($status==true){
		       $data['message']="teacher Updated Sucessfully" ;
			   $data['status']=1 ;
	
	
				}
				else
				{
				  $data['message']="Contact Technical Support Update Failed" ;
				  $data['status']=0 ;
				
				
				}
		}
		else if($pstatus==0)
		{
			$data['message']="Teacher With Same FirstName In this School  Already Exists" ;
		    $data['status']=0 ;
		}
		else if($pstatus==2)
		{
			$data['message']="Teacher With User Name   Already Exists" ;
		    $data['status']=0 ;
		}	
		echo json_encode($data);
		exit;		
	}
	
	function delete($teacher_id)
	{
		
		$this->load->Model('teachermodel');
		$result = $this->teachermodel->deleteteacher($teacher_id);
		if($result==true){
			$data['status']=1;
		}else{
			$data['status']=0;
			$date['error_msg'] = $result;
		}
		echo json_encode($data);
		exit;
		
	}
	
	function getTeachersBySchool($school_id)
	{
	
		$this->load->Model('teachermodel');
		$data['teacher']=$this->teachermodel->getTeachersBySchool($school_id);
		echo json_encode($data);
		exit;
	
	
	
	}
	
	function do_pagination($count,$per_page,$cur_page,$paginationdetails)

	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = false;
			$last_btn = false;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='dataTables_paginate paging_bootstrap pagination'><ul>";


      // FOR ENABLING THE FIRST BUTTON
      if ($first_btn && $cur_page > 1) {
       $string.= "<li p='1' class='active'>First</li>";
      } else if ($first_btn) {
       $string.= "<li p='1' class='inactive'>First</li>";
      }


						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'><a href='javascript:void(0);'>← Prev</a></li>";
						} else if ($previous_btn) {
							$string.= "<li class='prev disabled'><a href='javascript:void(0);'>← Prev</a></li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {



							if ($cur_page == $i)
								$string.= "<li p='$i'  class='active'><a href='javascript:void(0);'>{$i}</a></li>";
							else
								$string.= "<li p='$i' class='active'><a href='javascript:void(0);'>{$i}</a></li>";
						}



						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'><a href='javascript:void(0);'>Next →</a></li>";
						} else if ($next_btn) {
							$string.= "<li class='next disabled'><a href='javascript:void(0);'>Next →</a> </li>";
						}



      // TO ENABLE THE END BUTTON
      if ($last_btn && $cur_page < $no_of_paginations) {
       $string.="<li p='$no_of_paginations' class='active'>Last</li>";
      } else if ($last_btn) {
       $string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
      }
      //$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
      $goto ='';
      $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
//      $count,$per_page
                                                        
                                                $string.= "</ul>" . $goto .  "</div><br />";  // Content for pagination
 
 
     return $string;
 
 
 
 
 }

	
	function bulkupload()
	{
	   if($this->session->userdata("login_type")=='admin')
	  {
		 $this->load->Model('schoolmodel');
	   $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  if($data['district']!=false)
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict($data['district'][0]['district_id']);
	  }
	  else
	  {
	    $data['school']='';
	  }
	  
		  $data['view_path']=$this->config->item('view_path');
		  $this->load->view('teacher/bulkupload',$data);
	  }
	   else if($this->session->userdata("login_type")=='user')
	  {
		   $this->load->Model('schoolmodel');
		   $data['school']=$this->schoolmodel->getschoolbydistrict();
		   $data['view_path']=$this->config->item('view_path');
		   $this->load->view('teacher/bulkuploadall',$data);
	  
	  
	  }
	  else if($this->session->userdata("login_type")=='observer')
	  {
		$this->load->Model('schoolmodel');
		$data['school']=$this->schoolmodel->getschoolById($this->session->userdata('school_id'));
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('teacher/bulkuploadall',$data);	
	  
	  
	  }
	
	}
	function sendfile()
	{
	
	if($this->input->post('countries'))
	{
		if($this->input->post('school')=='all' || $this->input->post('school')=='empty' )
		{
			$pdata['status']=2;
			$pdata['msg']='Please Select School.';
			echo json_encode($pdata);
	  	  exit;
		
		}
	
	}
	
	$pdata['status']=0;
	if($_FILES['upload']['size']>0 )
		{
			if($this->input->post('countries'))
			{
			$school_id=$_POST['school'];
			}
			else
			{
				$school_id=$_POST['school_id'];
			}
			$this->load->Model('teachermodel');
			
			$filename=explode('.',$_FILES['upload']['name']);
			$target_path = WORKSHOP_FILES."file_uploads/";
			$target_path = $target_path . basename( $_FILES['upload']['name']);
			if($filename[1]=='xlsx')
			{
			 $pdata['status']=2;
			 $pdata['msg']='Please Upload Excel 97-2003 ';
			 echo json_encode($pdata);
			 exit;
			
			
			
			}
			else if($filename[1]=='xls') 
			{
			$this->load->library('Spreadsheet_Excel_Reader');
			

			if(move_uploaded_file($_FILES['upload']['tmp_name'], $target_path))
			{
				//$this->spreadsheet_excel_reader->setOutputEncoding('CP1251'); // Set output Encoding.
				$this->spreadsheet_excel_reader->read($target_path); // relative path to .xls that was uploaded earlier

				$rows = $this->spreadsheet_excel_reader->sheets[0]['cells'];
				$cell_count = count($this->spreadsheet_excel_reader->sheets[0]['cells']);
				
				$row_count = $this->spreadsheet_excel_reader->sheets[0]['numCols'];
				$data['rows']=$row_count;
				$data['cols']=$cell_count;
				
				//for ($j = 1; $j <= $row_count; $j++) {
	
				for ($i = 2; $i <= $cell_count; $i++)
				{
					for($j=1;$j<=$row_count;$j++)
					{
						if(isset($rows[$i][$j]))
						{
							$text = $rows[$i][$j];
					       $data[$i][$j]=trim($text);
							//$data[$i][$j]=trim($rows[$i][$j]);
						}	
					}	
					
					

				}
				
				//echo $b="asdlsad";
				//}
				
				//print_r($data);
				//exit;
				//echo json_encode($data);
	  	       // exit;
				
				
			}
			
			}
			else if($filename[1]=='csv')
			{
			
			move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);
			
			 $row = 1;
		$file = fopen($target_path, "r");
		while (($fdata = fgetcsv($file)) !== FALSE) {
		  $num = count($fdata);
		  if(!empty($fdata))
		  $sdata[]=$fdata;
		  
		
		$row++;
		/*for ($c=0; $c < $num; $c++) {
        //echo $fdata[$c] . "<br>";
		 $mobile.=trim($fdata[$c]);*/
		 
		 
		}
		$data['cols']=count($sdata);
		
		//echo "<pre>";
		//print_r($sdata);
		//exit;
		$m=1;
        for ($i = 1; $i <count($sdata); $i++)
				{
			 		
					$pdata=$sdata[$i];	
					$data['rows']=count($pdata);
					$n=1;
					for($j=0;$j<count($pdata);$j++)
					{
					   $text =$pdata[$j];
					   $data[$m][$n]=trim($text);
					
					 $n++;
					}
					$m++;
					
					

				}
                //print_r($data); 				
				//exit;
	        
		 fclose($file);

        
				//echo json_encode($data);
	  	       // exit;
			
			
			}
			//echo '<pre>';
			//print_r($data);
			//exit;
			//$i=1;
			foreach($data as $key=>$val)
			{
			 if($key>0)
			 {
			 if(isset($val[1]) && isset($val[2]) && isset($val[3]) && isset($val[4]) )
			 {
			 if(!empty($val[1]) && !empty($val[2]) && !empty($val[3]) && !empty($val[4]) )
			 {
			    $pstatus=$this->teachermodel->check_teacher_exists($val[1],$val[3],$school_id);
				if($pstatus==1)
					 {
					    if($val[5]>0)
						{
							$this->teachermodel->add_bulk_teacher($val[1],$val[2],$school_id,$val[3],$val[4],$val[5],$val[6]);
						}
						else
						{
							$this->teachermodel->add_bulk_teacher($val[1],$val[2],$school_id,$val[3],$val[4],0,$val[6]);
						}	
					 
					 }
			 
			 }
			 
			 }
			 }
			
			// $i++;
			}
			$pdata['status']=1;
			
		}
	     echo json_encode($pdata);
	  	  exit;
		 
    }
    function getteachers_admin($page,$state_id,$country_id,$district_id=false,$school_id=false)
	{
		
		
		if($school_id==false)
		{
		  $school_id='all';
		}
		if($district_id==false)
		{
		  $district_id='all';
		}
		if($school_id=='empty' || $district_id=='empty' )
		{	
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' align='center'><td >First Name</td><td>Last Name</td><td>User Name</td><td>School Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No teachers Found.</td></tr></table>
			";
		}
		else
		{
		$this->load->Model('utilmodel');
		$per_page = $this->utilmodel->get_recperpage();
		
		$this->load->Model('teachermodel');
		$total_records = $this->teachermodel->getteacherCountbydistrict($state_id,$country_id,$district_id,$school_id);
		
			$status = $this->teachermodel->getteachersbydistrict($page, $per_page,$state_id,$country_id,$district_id,$school_id);
		
		
		
		if($status!=FALSE){
			
			
			print "<div class='htitle'>Teachers</div><table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead'><td >First Name</td><td>Last Name</td><td>User Name</td><td>School Name</td><td>Actions</td></tr>";
					
		
		
			$i=1;
			foreach($status as $val){
					if($i%2==0)
					{
					 $c='tcrow2';
					}
					else
					{
					 $c='tcrow1';
					
					}
					
				print '<tr id="'.$val['teacher_id'].'" class="'.$c.'" >';
			
				print '<td>'.$val['firstname'].'</td>
					  <td>'.$val['lastname'].'</td>
					  <td>'.$val['username'].'</td>
					   <td>'.$val['school_name'].'</td>
				
				<td nowrap><input class="btnsmall" title="Edit" type="button"  value="Edit" name="Edit" onclick="teacheredit('.$val['teacher_id'].')"><input class="btnsmall" title="Delete" type="button"  name="Delete" value="Delete" onclick="teacherdelete('.$val['teacher_id'].')" ></td>		</tr>';
				$i++;
				}
				print '</table>';
               
				
                        $pagination=$this->do_pagination_admin($total_records,$per_page,$page,'teachers');
						print $pagination;	
						
                        
		}else{
			
			print "<table class='tabcontent' cellspacing='0' cellpadding='0' id='conf' ><tr class='tchead' align='center'><td >First Name</td><td>Last Name</td><td>User Name</td><td>School Name</td><td>Actions</td></tr>
			<tr><td valign='top' colspan='10'>No teachers Found.</td></tr></table>
			";
			
			$pagination=$this->do_pagination_admin($total_records,$per_page,$page,'teachers');
						print $pagination;	
		}
		
		}
	}
        
        function do_pagination_admin($count,$per_page,$cur_page,$paginationdetails)
	{
	  $string='';
	
	        
			$previous_btn = true;
			$next_btn = true;
			$first_btn = true;
			$last_btn = true;
			
	
	$no_of_paginations = ceil($count / $per_page);
						   /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
						if ($cur_page >= 7) {
							$start_loop = $cur_page - 3;
							if ($no_of_paginations > $cur_page + 3)
								$end_loop = $cur_page + 3;
							else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
								$start_loop = $no_of_paginations - 6;
								$end_loop = $no_of_paginations;
							} else {
								$end_loop = $no_of_paginations;
							}
						} else {
							$start_loop = 1;
							if ($no_of_paginations > 7)
								$end_loop = 7;
							else
								$end_loop = $no_of_paginations;
						}
						/* ----------------------------------------------------------------------------------------------------------- */
						$string.= "<br />";
						$string.=  "<div id='paginationall' class='$paginationdetails'><ul>";

						// FOR ENABLING THE FIRST BUTTON
						if ($first_btn && $cur_page > 1) {
							$string.= "<li p='1' class='active'>First</li>";
						} else if ($first_btn) {
							$string.= "<li p='1' class='inactive'>First</li>";
						}

						// FOR ENABLING THE PREVIOUS BUTTON
						if ($previous_btn && $cur_page > 1) {
							$pre = $cur_page - 1;
							$string.= "<li p='$pre' class='active'>Previous</li>";
						} else if ($previous_btn) {
							$string.= "<li class='inactive'>Previous</li>";
						}
						for ($i = $start_loop; $i <= $end_loop; $i++) {

							if ($cur_page == $i)
								$string.= "<li p='$i' style='color:#fff;background-color:#07acc4;' class='active'>{$i}</li>";
							else
								$string.= "<li p='$i' class='active'>{$i}</li>";
						}

						// TO ENABLE THE NEXT BUTTON
						if ($next_btn && $cur_page < $no_of_paginations) {
							$nex = $cur_page + 1;
							$string.= "<li p='$nex' class='active'>Next</li>";
						} else if ($next_btn) {
							$string.= "<li class='inactive'>Next</li>";
						}

						// TO ENABLE THE END BUTTON
						if ($last_btn && $cur_page < $no_of_paginations) {
							$string.="<li p='$no_of_paginations' class='active'>Last</li>";
						} else if ($last_btn) {
							$string.= "<li p='$no_of_paginations' class='inactive'>Last</li>";
						}
						//$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
						$goto ='';
						$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
						$string.= "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	
					return $string;
	
	
	
	
	}
	
	function get_teacher_data()
	{ 
            
             if($this->session->userdata("login_type")=='admin')
	  {
	  $this->load->Model('schoolmodel');
	   $this->load->Model('countrymodel');
	  $data['countries']=$this->countrymodel->getCountries();
	  $data['states']=$this->countrymodel->getStates($data['countries'][0]['id']);
	   $this->load->Model('districtmodel');
	  if($data['states']!=false)
	  {
		$data['district']=$this->districtmodel->getDistrictsByStateId($data['states'][0]['state_id']);
	  }
	  else
	  {
	    $data['district']='';
	  }
	  if($data['district']!=false)
	  {
		$data['school']=$this->schoolmodel->getschoolbydistrict($data['district'][0]['district_id']);
	  }
	  else
	  {
	    $data['school']='';
	  }
	  
	  
	  }
	 	// error_reporting(0);
		$data['view_path']=$this->config->item('view_path');
//		print_r($data);exit;
		$this->load->view('teacher/get_teacher',$data);
 		 
	}	



	 function retrieve_teacher() {
		// redirect(base_url().'teacher/teacherList/'.$schools_id,);

if ($this->input->post('submit')) {
			$schools_id = $this->input->post('school_id'); 
		
	$this->load->Model('teachermodel');
			$data['teacher'] = $this->teachermodel->getschool_byteacher_pdf($schools_id);

//print_r($data['teacher']);exit;
        $data['view_path'] = $this->config->item('view_path');
//    $str = $this->load->view('classroom/createsstpdf',$data,true);
        $this->output->enable_profiler(false);
        $this->load->library('parser');
        echo $str = $this->parser->parse('teacher/teachers_pdf', $data, TRUE);

//	$str = $this->load->view('classroom/createsstpdf',$data,true);
        
		
		 $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'ISO-8859-15', array(4, 20, 20, 20));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($str);
        $html2pdf->Output();
		
        /*$html2pdf->WriteHTML($str);
		$newname='test.pdf';//$data['teacher'][0]->school_name.'_teachers.pdf';
		$html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->Output();*/
    }
	 }
	 
	 public function generatePassword($length=9, $strength=8) {
			$vowels = 'aeuy';
			$consonants = 'bdghjmnpqrstvz';
			if ($strength & 1) {
				$consonants .= 'BDGHJLMNPQRSTVWXZ';
			}
			if ($strength & 2) {
				$vowels .= "AEUY";
			}
			if ($strength & 4) {
				$consonants .= '23456789';
			}
			if ($strength & 8) {
				$consonants .= '@#$%';
			}
		 
			$password = '';
			$alt = time() % 2;
			for ($i = 0; $i < $length; $i++) {
				if ($alt == 1) {
					$password .= $consonants[(rand() % strlen($consonants))];
					$alt = 0;
				} else {
					$password .= $vowels[(rand() % strlen($vowels))];
					$alt = 1;
				}
			}
			echo $password;
}

}	