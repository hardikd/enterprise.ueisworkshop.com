<?php
/**
 * teacher Controller.
 *
 */
class Allschoolgraph extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_observer()==false 
		 && $this->is_teacher()==false){
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->no_cache();
	}
	
		function no_cache()
		{
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
		
	function index()
	{	 
	
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
		if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/enterprise/index.php/";</script>';
			}
			else{
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{	 
		$this->load->Model('classroommodel');
		//$data['getAllAssData']=$this->assessscoretypemodel->getAllassignmentsScore();
		$district_id = $this->session->userdata('district_id');
		$data['view_path']=$this->config->item('view_path');	
		
		
		$data['grades'] = $this->classroommodel->getschoolgrades($district_id);   // grade
		
		$data['schools_type'] = $this->classroommodel->getschooltype($district_id);		
		
		$data['allschools'] = $this->showallschools();
						
	    $this->load->view('schooldetailgraph/allSchools',$data); 

		}
	}
	
	function getschoolbyschooltype()
	{
		$school_type_id = $_REQUEST['id'];
		$this->load->model('Reportschooltestmodel');
		
	$data['schools'] =$this->Reportschooltestmodel->getallschoolsbytype($school_type_id);
		echo '<option value="all">All</option>';
		foreach($data['schools'] as $schools)
		{
			$schoolid = $schools['school_id'];
			$school_name = $schools['school_name'];
			
			echo '<option value="'.$schoolid.'">'.$school_name.'</option>';
		}
	} 
	
	
	function showallschools()
	{
				 
		 $district_id = $this->session->userdata('district_id');
		 		
		$this->load->model('classroommodel');		
		
		//get all school
		
		$data['allschools'] =$this->classroommodel->getAllSchoolwithDid($district_id);
			
		if(empty($data['allschools']))
		{		
			$this->session->set_flashdata('item', 'No Record Found..!!');
		}
		
		return $data['allschools'];
		
	}
	
	
}

?>