<?php
/**
 * prod_spec Controller.
 *
 */
class Prod_spec_user extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if(!$this->session->userdata('product_login_type')){
			//These functions are available only to admins - So redirect to the login page
			redirect("index");
		}
		
		
		
	}
	
	function dist_user($id)
	{
	  $this->load->Model('districtmodel');
	  $data['dist_user']=$this->districtmodel->getdist_userByDistrictId($id);
	  echo json_encode($data);
	  exit;
	
	}
	
	function school($id)
	{
	  $this->load->Model('schoolmodel');
	  $data['schools']=$this->schoolmodel->getschoolbydistrict($id);
		
		echo json_encode($data);
		exit;
	
	}
	function observer($id)
	{
	  $this->load->Model('observermodel');
	  $data['observer']=$this->observermodel->getobserverByschoolId($id);
		
		echo json_encode($data);
		exit;
	
	}
	function teacher($id)
	{
	  $this->load->Model('teachermodel');
	  $data['teacher']=$this->teachermodel->getTeachersBySchool($id);
		
		echo json_encode($data);
		exit;
	
	}
	
	}