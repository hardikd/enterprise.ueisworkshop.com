<?php   
 $path_file =  'libchart/classes/libchart.php';
include_once($path_file);
 

class Teacherarchivedata extends	MY_Auth{
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_user()==false && $this->is_teacher()==false){
//These functions are available only to admins - So redirect to the login page
			redirect("index/index"); 
		}
		
	}
	
	function index()
	{ 
            $data['idname']='tools';
	 	 error_reporting(0);
		 if($this->session->userdata("login_type")=='teacher')
		  {
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('teacherarchivedatamodel');
				$teacher_id = $this->session->userdata('teacher_id');
				$data['records'] = $this->teacherarchivedatamodel->getassessment($teacher_id);
	 			$this->load->view('teacherarchivedata/index',$data);
 	 	 }
  	}
	
 	function getReportHtml()
	{
		error_reporting(0);
		
		$teacher_id = $this->session->userdata('teacher_id');
		$fDate= $_REQUEST['fdate'];
		$tDate= $_REQUEST['tdate'];
		$ass_id = $_REQUEST['assignment_id']; 
		
		if($ass_id != -1)
		{
			$this->load->model('teacherarchivedatamodel');
			$data['student'] = $this->teacherarchivedatamodel->getstudents($teacher_id,$ass_id,$fDate,$tDate);
			//	echo "<pre>";print_r($data['gradename']);
			 
			$data['Testitem']=$this->teacherarchivedatamodel->getTestItem($teacher_id,$ass_id,$fDate,$tDate);
			$htmlTable = '<div class="outer">  <div class="inner"><table id="innertab">  
       			 <tr class="tchead"><th width="150" align="left"  bgcolor="#8ca275" valign="top" style="color:#fff;font-size:13px;" >  Student Name </th>';
			foreach($data['Testitem'] as $value)
			{
				$htmlTable .= "<td style='white-space:nowrap;' valign='top'>".utf8_decode($value['cluster_item'])."</td>";
			}
			$htmlTable .="</tr>";
			
			// echo "<pre>";print_r($data['Testitem']);
			$rw =  count($data['Testitem'][0]);
			if($rw >= 1)
			{		 
	 	 
			$rows =  count($data['student']);
			$row =  count($data['student'][0]);
			
			
			$tablebody = '';
	 		if($row > 0)
			{
		 		$tablerow ='';
			 	for($i = 0 ;$i<$rows; $i++)
				{
					//.preg_replace('/[^(\x20-\x7F)]*/','',$data['Testitem'][$i]['cluster_item'])
					$tablerow .= "<tr bgcolor='#EEF9E3'> <th>".$data['student'][$i]['first_name']." ".$data['student'][$i]['last_name']." </th>";
			 		$studentrno = $data['student'][$i]['stud_roll_no'];
					
				 	foreach($data['Testitem'] as $key => $value)
					{
					  $cluster_item = $value['cluster_item'];
					   $gradewisePer =$this->teacherarchivedatamodel->getPercentageByGradeAndTestCluster($teacher_id,$cluster_item,$studentrno,$fDate,$tDate,$ass_id);		$resper ='';
					   	if($gradewisePer[0]['marks'] !='')
						  $resper =  number_format($gradewisePer[0]['marks'],2);
						  else
						  $resper =number_format(0,2);
						  
					    $tablerow .= "<td>".$resper."%</td>";						 
						
					}
				
				 	$tablerow .="</tr>";
			 	}
				 $tablebody .= $tablerow;
			}
			else
			{
				$cols =  count($data['Testitem'])+1;
				$tablebody = '<tr><td colspan="'.$cols.'" align="center"> No Record Found.</td></tr>';
			}
			}
			else
			{
				$cols =  count($data['Testitem'])+1;
				$tablebody = '<tr><td colspan="'.$cols.'" align="center"> No Record Found.</td></tr>';
			}
			
			$htmlTable .= $tablebody;
			$htmlTable .= "</table>";
	 	}
	   echo $htmlTable;
	}
	
 	 
	
}	
