<?php

class Api extends	Controller {

	function __construct()
	{
		parent::Controller();
	}
	
	
	function report($username=null,$password=null,$type=null)
	{
	
	  
	 
	 if(empty($username) || empty($password) || empty($type)  )
	 {
	    echo "error|Required 3 Fields";
		exit;
	 
	 
	 
	 }
	 
	 
	
	 $status=$this->check_login($username,$password,$type);
	 if($status!=false){
        
		$this->load->Model('teachermodel');
	  $this->load->Model('schoolmodel');
	  $this->load->Model('observermodel');
	  $this->load->Model('districtmodel');
	  
	  $data['subjects']=$this->schoolmodel->getallsubjects($status[0]['district_id']);
	  $data['grades']=$this->schoolmodel->getallgrades($status[0]['district_id']);
		if($type=='district')
		{
		  
		  $today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$status[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			if($status[0]['type']=='demo' && $status[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($status[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$status[0]['district_id']);
			  }	
			
			
			}
			
			if($status[0]['type']=='demo' && $status[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			   
			   echo "error|Please Login From <a href='http://enterprise.ueisworkshop.com'>enterprise.ueisworkshop.com</a> and submit feedback form";
		       exit;
			   
			}
			else if($status[0]['type']=='demo'  && $days>=90  && $status[0]['username']!='bhasker73' )
			{			
			 
			 
			 if($status[0]['feedback']==0)
			 {
			     echo "error|Please Login From <a href='http://enterprise.ueisworkshop.com'>enterprise.ueisworkshop.com</a> and submit feedback form";
		        exit;
			 
			 }
			 else
			 {
			 	 
				echo "error|Your trial subscription period of 90 has ended. Please convert your account to a regular account.";
		        exit;
			  }
			 
			}
			else
			{
			 
			 $this->session->set_userdata("login_type",'user');
		   $this->session->set_userdata("district_id",$status[0]['district_id']);
		 $data['info'][0]['username']=$username;
		 $data['info'][0]['districtname']=$status[0]['districts_name'];
		 $data['school']=$this->schoolmodel->getschoolbydistrict();
		
		if($data['school']!=false)
		{
		 foreach($data['school'] as $schoolkey=>$schoolval)
		 {	      
		    
		  $data['school'][$schoolkey]['teachers']=$this->teachermodel->getTeachersBySchoolApi($schoolval['school_id']);
	     $data['school'][$schoolkey]['observers']=$this->observermodel->getobserverByschoolId($schoolval['school_id']);
	   
	     }
	   }
			}
			
			
		  
		  
		 
	   
		
		
		}
		else if($type=='observer')
		{
		   
		   
			
			$distdetails=$this->districtmodel->getdemodistrict($status[0]['district_id']);
			$today=date('Y-m-d');
			$tstr=strtotime($today);
			$mod=explode(' ',$distdetails[0]['modified']);
			$otherdate=$mod[0];
			$otstr=strtotime($otherdate);
			$str=($tstr-$otstr)/86400;
			$days=floor($str);
			
			if($distdetails[0]['type']=='demo' && $distdetails[0]['copydistrict']==0)
			{
			
			  $from_district_id=$this->districtmodel->getDistrictBystateid($distdetails[0]['state_id']);
			  if($from_district_id!=0)
			  {
				$this->copydistrict($from_district_id,$status[0]['district_id']);
				$this->copygoalplan($from_district_id,$status[0]['district_id']);
			   $this->copymemorandums($from_district_id,$status[0]['district_id']);
			  }	
			   
			
			}
			
			if($distdetails[0]['type']=='demo' && $distdetails[0]['feedback']==0 && $days>=30 && $days<90   )
			{			
			 echo "error|Please Login From <a href='http://demo.ueisworkshop.com'>demo.ueisworkshop.com</a> and submit feedback form";
		       exit;
			}
			else if($distdetails[0]['type']=='demo'  && $days>=90   )
			{			
			 
			 if($distdetails[0]['feedback']==0)
			 {
			    echo "error|Please Login From <a href='http://demo.ueisworkshop.com'>demo.ueisworkshop.com</a> and submit feedback form";
		       exit;
			 
			 }
			 else
			 {
			 	 
				echo "error|Your trial subscription period of 90 has ended. Please convert your account to a regular account.";
		        exit;
			  }	
			}
			else
			{
			//set the session
			
			$this->session->set_userdata("login_type",'observer');
		   $this->session->set_userdata("district_id",$status[0]['district_id']);
			$this->load->Model('districtmodel');	
			$dname=$this->districtmodel->getDistrictById($status[0]['district_id']);
		 $data['info'][0]['username']=$status[0]['observer_name'];
		 $data['info'][0]['districtname']=$dname[0]['districts_name'];
		  $data['school'][0]['school_id']=$status[0]['school_id'];
		  $data['school'][0]['school_name']=$status[0]['school_name'];
		  $data['school'][0]['teachers'] = $this->teachermodel->getTeachersBySchoolApi($status[0]['school_id']);
	      $data['school'][0]['observers']=$this->observermodel->getobserverById($status[0]['observer_id']);
			}
		   
		   
		  
		
		}
		else
		{
		   echo "error|Type is incorrect";
		   exit;
		
		}
		
		
		
		
		
		    $this->load->Model('observationpointmodel');
			$data['checklist']=$this->observationpointmodel->getAllGroupPoints();
			
			$this->load->Model('rubricscalemodel');
			$rdata['scaledrubric1']=$this->rubricscalemodel->getallrubricsubscales($this->session->userdata("district_id"));
		
		    if($rdata['scaledrubric1']!=false)
			{
			  foreach($rdata['scaledrubric1'] as $datascale)
			  {
			  
			    $datascale['description']=html_entity_decode($datascale['description']);
				$data['scaledrubric'][]=$datascale;
			  
			  
			  
			  }
			
			
			
			
			
			
			
			
			}
			else
			{
			 $data['scaledrubric']=false;
			
			
			}
			$this->load->Model('proficiencypointmodel');
			$data['proficiencyrubric']=$this->proficiencypointmodel->getAllGroupPoints();
			
			$this->load->Model('lickertpointmodel');
			$data['likert']=$this->lickertpointmodel->getAllGroupPoints();
		
		
		
		
		
		$pdata['data']=$data;
		$pdata['view_path']=$this->config->item('view_path');
		$this->load->view('api/api.php',$pdata);
		/*echo $json=json_encode($data);
		//var_dump(json_decode($json, true));
	    exit;*/
	 
	 }
	 else
	 {
	 
	   echo "error|User Name or password are incorrect";
	 
	 }
	 
	
	
	
	
	
	
	}
	
	function check_login($username,$password,$type)
	{
	 $this->load->Model('ApiModel');
	 $status=$this->ApiModel->login($username,$password,$type);
	  return $status;
	
	
	}
	function copydistrict($fromdist_id,$todist_id)
	{
	   //$fromdist_id=$this->config->item('dist_id');
	   $this->load->Model('observationgroupmodel');
	   $this->load->Model('rubricscalemodel');
	   $this->load->Model('lickertgroupmodel');
	    $this->load->Model('proficiencygroupmodel');
		$this->load->Model('districtmodel');
	   $this->observationgroupmodel->copyobservationgroup($fromdist_id,$todist_id);
	   $this->rubricscalemodel->copyrubricscale($fromdist_id,$todist_id);
	   $this->lickertgroupmodel->copylickertgroup($fromdist_id,$todist_id);
	   $this->proficiencygroupmodel->copyproficiencygroup($fromdist_id,$todist_id);
	   $this->districtmodel->savecopydistrict($todist_id);
	   
	   
	  
	 
	  
	
	}
	function copygoalplan($fromdist_id,$todist_id)
	{
	   //$fromdist_id=$this->config->item('dist_id');
	   $this->load->Model('goalplanmodel');
	   
	   $this->goalplanmodel->copygoal($fromdist_id,$todist_id);  
	
	}
	function copymemorandums($fromdist_id,$todist_id)
	{
	   //$fromdist_id=$this->config->item('dist_id');
	   $this->load->Model('memorandummodel');
	   
	   $this->memorandummodel->copymemorandum($fromdist_id,$todist_id);	
	}
	
	
	
	}
	?>