<?php
 $path_file =  'libchart/classes/libchart.php';
 include_once($path_file);
/**
 * Studentdetail Controller.
 *
 */
class Studentprogress extends	MY_Auth {
function __Construct()
	{
		parent::Controller();
		if($this->is_admin()==false && $this->is_teacher()==false && $this->is_observer()==false && $this->is_user()==false){
		//&& $this->is_user()==false && $this->is_observer()==false  
			//These functions are available only to admins - So redirect to the login page
			redirect("index/index");
		}
		$this->load->library('paginationnew');
		$this->load->helper('url');
		$this->no_cache();
	}
	
   function no_cache()
	{
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	 
	
	function index()
	{

		 $data['idname']='tools';
 		$login_required = $this->session->userdata('login_required');
		
		 /*  if($this->session->userdata("login_type")=='user')
		  {
			
			 
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentprogressmodel');
				//$data['records'] = $this->Studentprogressmodel->getstuddetail();
				$district_id = $this->session->userdata('district_id');
				$data['school_type'] = $this->Studentprogressmodel->getschooltype($district_id);
			
				$config = array();
				$config["base_url"] = base_url() . "studentprogress/index";
				$config["total_rows"] = $this->Studentprogressmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentprogressmodel->getstuddetail($config["per_page"], $page);
								
				$this->load->view('studentprogress/studetail',$data);
	   
	   
	   
	   
	 	 }
		
		else
		*/ 
		  if($this->session->userdata("login_type")=='teacher')
		  {
			
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentprogressmodel');
				//$data['records'] = $this->Studentprogressmodel->getstuddetail();
				$district_id = $this->session->userdata('district_id');
			//	$data['school_type'] = $this->Studentprogressmodel->getschooltype($district_id);
				//$data['countries'] = $this->Studentprogressmodel->getcountries($data['records']);
				$teacher_id = $this->session->userdata('teacher_id');
				$data['school_grade'] = $this->Studentprogressmodel->getteacher_schoolandgrade($teacher_id,$district_id);

				$data['assessment'] = $this->Studentprogressmodel->getteacher_assessment($teacher_id,$district_id);
//				print_r($this->db->last_query());exit;
				
				$config = array();
				$config["base_url"] = base_url() . "studentprogress/index";
				$config["total_rows"] = $this->Studentprogressmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentprogressmodel->getstuddetail($config["per_page"], $page);
				$this->load->Model('videooptionmodel');
        		$data['video_option'] = $this->videooptionmodel->get_option();
				$this->load->view('studentprogress/studetail',$data);
	   
	 	 }
		 
		else if($this->session->userdata("login_type")=='observer')
		  {
				
                                
                                $data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentprogressmodel');
				//$data['records'] = $this->Studentprogressmodel->getstuddetail();
				//$data['countries'] = $this->Studentprogressmodel->getcountries($data['records']);
				$district_id = $this->session->userdata('district_id');
			//	$data['school_type'] = $this->Studentprogressmodel->getschooltype($district_id);
				
				$observer_id = $this->session->userdata('observer_id');
				$data['school_grade'] = $this->Studentprogressmodel->getobserver_schoolandgrade($observer_id,$district_id);
				
                $config = array();
				$config["base_url"] = base_url() . "studentprogress/index";
				$config["total_rows"] = $this->Studentprogressmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;
				
				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentprogressmodel->getstuddetail($config["per_page"], $page);
				

				
				$this->load->view('studentprogress/studetail',$data);
	   
	 	 } else if($this->session->userdata("login_type")=='user'){
                                $this->load->Model('observermodel');
//                                print_r($this->session->all_userdata());exit;
                                $state_id = $this->session->userdata('dis_state_id');
                                $country_id = $this->session->userdata('dis_country_id');
                                $district_id = $this->session->userdata('district_id');
                                $data['observers'] = $this->observermodel->getobservers(false, false,$state_id,$country_id,$district_id);
//                                echo $this->db->last_query();exit;
				$data['view_path']=$this->config->item('view_path');
				$this->load->model('Studentprogressmodel');
//				$data['records'] = $this->Studentprogressmodel->getstuddetail();

				//$data['countries'] = $this->Studentprogressmodel->getcountries($data['records']);
//				$district_id = $this->session->userdata('district_id');
//				$data['school_type'] = $this->Studentprogressmodel->getschooltype($district_id);
	
//				$observer_id = $this->session->userdata('observer_id');
                                
                                foreach($data['observers'] as $observer){
                                    $observerIdArr[] = $observer['observer_id'];
                                }
                                $observer_id = implode(',',$observerIdArr);

				$data['school_grade'] = $this->Studentprogressmodel->getobserver_SchoolGrade($observer_id,$district_id);
                                
//	print_r($data['school_grade']);exit;
                $config = array();
				$config["base_url"] = base_url() . "studentprogress/index";
				$config["total_rows"] = $this->Studentprogressmodel->record_count();
				$config["per_page"] = 10;
				$config['num_links'] = 5;


				$config['full_tag_open'] = '<div id="pagination">';
				$config['full_tag_close'] = '</div>';
				$config["uri_segment"] = 3;
				
				$this->paginationnew->initialize($config);
			    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['records'] = $this->Studentprogressmodel->getstuddetails($observer_id);
//                                echo $this->db->last_query();exit;
				$this->load->view('studentprogress/studetail',$data);
	   
	 	 }   
	}
        
        
	
/*	
	function getstudentDetail()
	{
		$userid = $_REQUEST['stuid'];
		$this->load->model('Studentprogressmodel');
		$data['records']=$this->Studentprogressmodel->getrecord_student($userid);
		
		foreach($data['records'] as $key => $value)
		{
			
			echo $value['UserID']. "|";
			echo $value['Name']. "|";
			echo $value['Surname']. "|";
			echo $value['email'];
 		}
		
	}
	*/

	function getstudentperformance()
	{
	  	$userid = $_REQUEST['userid'];
	 	$this->load->model('Studentprogressmodel');
		$data['records']=$this->Studentprogressmodel->getstuperformance($userid);
		if(!empty($data['records'][0]['assignment_id']) && $data['records'][0]['assignment_id']!="")
		{
			$assessment_id = @$data['records'][0]['assignment_id'];
		}
		$data['assessment']=$this->Studentprogressmodel->getassessment($assessment_id);
			
			if(count($data['records'])>0)
			{
				if(!empty($data['records']) && $data['records']!="")
					{
		
						echo @$data['records'][0]['added_date']. "|";
						echo @$data['records'][0]['pass_score_point']. "|";
						echo @$data['records'][0]['pass_score_perc']. "|";
						echo @$data['assessment'][0]['assignment_name'];
						
					}
			}
			else
			{
				echo "norecfound";
			}
	}
	
	/* Student graph detail*/
	function studentreportaspdf()
	{
		error_reporting(0);
		//$stuid = $_REQUEST['user_id'];
		//$assessmentid = $_REQUEST['assessment_id'];  
		
		$studentid = $_REQUEST['user_id'];
		$assignment_id = $_REQUEST['assessment_id'];
		$teacherid = $this->session->userdata('teacher_id');
		$this->load->model('selectedstudentreportmodel'); 
		//get student according to userid
		$data['studentrecord']=$this->selectedstudentreportmodel->getrecord_student($studentid);
		
	 	if(!empty($data['studentrecord'][0]['Name']))
		{
			$data['studentname'] = $data['studentrecord'][0]['Name'].' '.$data['studentrecord'][0]['Surname'];
		}
	 	
	 	$classroom = $this->selectedstudentreportmodel->get_roomByStudent($data['studentrecord'][0]['student_id']);
	 	//print_r($classroom);exit;
	 	$data['room'] = $classroom[0]['name']; 

		$data['assignment'] = $this->selectedstudentreportmodel->getassessment($assignment_id);
 		$data['assignment_name'] = $data['assignment'][0]['assignment_name'];		
	 	$quiz_id = $data['assignment'][0]['quiz_id'];

	 	$quizdetails = $this->selectedstudentreportmodel->getassessmentreportbyid($assignment_id,$studentid);
	 	//print_r($quizdetails);exit;

	 	if($quizdetails[0]['finish_date']!=''){
	 		$data['assessmentdate'] = date('F d, Y',strtotime($quizdetails[0]['finish_date']));
	 	} else {
	 		$data['assessmentdate'] = date('F d, Y',strtotime($quizdetails[0]['added_date']));
	 	}

	 	//Code for disclaimer added by Hardik
	    $this->load->Model('report_disclaimermodel');
        $reportdis = $this->report_disclaimermodel->getallplans(8);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdis != false) {

            $data['dis'] = $reportdis[0]['tab'];
            $data['fontsize'] = $reportdis[0]['size'];
            $data['fontcolor'] = $reportdis[0]['color'];
        }

        $this->load->Model('report_descriptionmodel');
        $reportdes = $this->report_descriptionmodel->getallplans(8);

        $dis = '';
        $fontsize = '';
        $fontcolor = '';
        if ($reportdes != false) {

            $data['description'] = $reportdes[0]['tab'];
            $data['description_fontsize'] = $reportdes[0]['size'];
            $data['description_fontcolor'] = $reportdes[0]['color'];
        }

        $this->load->model('selectedstudentreportmodel');
        $this->load->Model('studentprogressmodel');
		$data['individualtestdetail']=$this->selectedstudentreportmodel->getassessmentreportbyid($assignment_id,$studentid);
		$data['userdetail'] =$this->selectedstudentreportmodel->getrecord_student($studentid);
		$data['schoolname'] =$this->selectedstudentreportmodel->getschoolname($this->session->userdata('school_id'));
		$data['testname'] =$this->selectedstudentreportmodel->getassessment($assignment_id);
		$data['quizid'] =$this->selectedstudentreportmodel->getquizidbytestid($data['testname'][0]['id']);
		$this->load->model('teacherclusterreportmodel');
		$data['teachername'] = $this->teacherclusterreportmodel->getteachername($teacherid);
		$data['teacher_name'] = $data['teachername'][0]['firstname'].' '.$data['teachername'][0]['lastname'];

		$data['view_path'] = $view_path = $this->config->item('view_path');
		$cach = date("H:i:s");
       	$this->output->enable_profiler(false);
        $this->load->library('parser');
        $ostr = $this->parser->parse('studentprogress/studentreportaspdf', $data, TRUE);
        $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(4, 20, 20, 2));
        $content = ob_get_clean();
        $html2pdf->WriteHTML($ostr);
        $html2pdf->Output();

  	}
	
	function allstudentreportaspdf()
	{
	 	$assessmentid = $_REQUEST['assessment_id'];
		$gradeid = $_REQUEST['grade_id'];  
		$schoolid = $_REQUEST['school_id'];    
		
		$this->load->model('studentprogressmodel');
		
		$students=$this->studentprogressmodel->getstudents($schoolid,$gradeid,$assessmentid);
		//echo"<pre>";print_r($students); exit;
		 $htmlall=''; $pp =0;
		if(!empty($students))
		foreach($students as $k=>$v)
		{ 
			$studentid ='';
			$studentid =$v['UserID'];
			$data['studentrecord']=$this->studentprogressmodel->getrecord_student($studentid);
			
			if(!empty($data['studentrecord'][0]['Name']) && $data['studentrecord'][0]['Name']!="")
			{
			@$studentname = $data['studentrecord'][0]['Name'].' '.$data['studentrecord'][0]['Surname'];
			}
			if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
			{		
			@$data['schoolname']=$this->studentprogressmodel->getschoolname($data['studentrecord'][0]['school_id']);
			}
			if(!empty($data['studentrecord'][0]['country_id']) && $data['studentrecord'][0]['country_id']!="")
			{	
			@$data['countryname']=$this->studentprogressmodel->getcountry($data['studentrecord'][0]['country_id']);
			}
			if(!empty($data['studentrecord'][0]['district_id']) && $data['studentrecord'][0]['district_id']!="")
			{	
			@$data['districtname']=$this->studentprogressmodel->getdistrict($data['studentrecord'][0]['district_id']);
			}
			if(!empty($data['districtname'][0]['state_id']) && $data['districtname'][0]['state_id']!="")
			{
			@$data['statename']=$this->studentprogressmodel->getstate($data['districtname'][0]['state_id']);
			}
			/*if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
			{
		
			@$data['teachername']=$this->studentprogressmodel->getteachername($data['studentrecord'][0]['school_id']);
			} */
			$assessname =array(); $studentassessment =array();
			@$assessname= $this->studentprogressmodel->getassessment($assessmentid);
			@$studentassessment= $this->studentprogressmodel->getstudassessment($studentid,$assessmentid);
		 
			 require_once('tcpdf/config/lang/eng.php');
			 require_once('tcpdf/tcpdf.php');
		
	 	 		//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, false, 'UTF-8', false,true);
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('Workshop');
				$pdf->SetTitle('Student Performance Report');
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Student Performance Report", PDF_HEADER_STRING);
				$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
				$pdf->SetFooterData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH,'Copyright UEIS Corp. All rights reserved',PDF_HEADER_STRING);
				
				$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				$pdf->setLanguageArray($l);
				$pdf->SetFont('times', '', 12);
				 
				$pdf->AddPage();
				
				$currdate = date('Y-m-d');
				$studentassessment[0]['finish_date']; 
				if($studentassessment[0]['finish_date']== NULL || $studentassessment[0]['finish_date']=='' ){ 
					$testdate1 = strtotime($studentassessment[0]['added_date']); }
				else{
					$testdate1 = strtotime($studentassessment[0]['finish_date']); }
				$testdate = date("Y-m-d",$testdate1);
				$testdate = date("Y-m-d",$testdate1);
			 
				$html='<p><u>
				'.$currdate.'</u><br/><br/>	 
				Dear Parent Or Caregiver,<br/><br/>
				<u>'.@$studentname.' </u> was assessed and received the following score listed below in the summery.To assist in supporting '.@$studentname.' an itemized breakdown according to the skills assessed is provided below. We hope that the skills information will be of benefit.</br></p> 
				<table>
				<tr>
				<td colspan="4"><strong>Student Marks Detail</strong> :</td></tr>
				<tr><td colspan="4"></td></tr>
				<tr><td width="20%">Student Name:</td><td width="35%">'.@$studentname.'</td>
				<td width="20%">Test Date : </td><td td width="20%">'.@$testdate.'</td>
				</tr>
				<tr>
				<td>School Name:</td><td>'.@$data['schoolname'][0]['school_name'].'</td>
				</tr>
				</table>
				<br/><br/>
				<table border="1px"  cellpadding="5px" cellspacing="0">
				<tr>
				<th width="34%"><span style="font-weight:bold;">Assignment Name</span></th> <th width="19%"><span style="font-weight:bold; ">Number Correct</span></th><th width="22%"><span style="font-weight:bold; ">Percentage Correct</span></th><th width="25%"><span style="font-weight:bold; ">Assessment Pass Score</span></th>
				</tr>';
			if(!empty($assessname))
			{
				$test = $assessname[0]["assignment_name"];
				$point =  $studentassessment[0]['pass_score_point'];
				$percen =  $studentassessment[0]['pass_score_perc'];
				$totalscoreofassessment =  $assessname[0]['pass_score'];
			$html .=
			'<tr>
			 <td width="34%">'.$test.'</td>
			 <td width="19%">'.$point.'</td>
			 <td width="22%">'.$percen.'</td>
			 <td width="25%">'.$totalscoreofassessment.'</td>
			
			</tr>';
			}
			else
			{
			$html .='<tr>	 <td></td>	 <td></td>	<td>The Student has not attempted any assessment yet.</td>
			 <td></td>	</tr>';				
			}
	
			$html.='</table><br/><br/>';
			$clusterArray =array();
			$clusterArray = $this->studentprogressmodel->getclusterinfo($assessmentid);
			if(!empty($clusterArray))
			{
				$clusterhtml = '';
				$clusterhtml .= '<table border="1px"  cellpadding="5px" cellspacing="0" width="100%">
						<tr>
						<th width="50%"><span style="font-weight:bold;">Skill</span></th> <th width="25%"><span style="font-weight:bold; ">Number of Questions</span></th><th width="12%"><span style="font-weight:bold; ">Correct</span></th><th width="13%"><span style="font-weight:bold; ">InCorrect</span></th>
						</tr>';
				
				foreach($clusterArray as $k=>$v)
				{
					$clusterhtml.='<tr> <td>'.utf8_decode($v["test_cluster"]).' </td><td align="center">'.$v['cnt'].'</td>';
				
				$res= $this->studentprogressmodel->displayQuestionsResult($assessmentid,$v["test_cluster"],$studentid);
				
				$clusterhtml.='<td align="center">'.$res["disptrueQuecount"].'</td><td align="center">'.$res["dispfalseQuecount"].'</td> </tr>';	
				}
				$clusterhtml .= '</table>';
			}
			$html = $html.$clusterhtml;
			
			//$htmlall = $htmlall.$html;
			$pdf->lastPage();
			$pdf->writeHTML($html, true, false, true, false, '');
			$pdf->Output($studentname.'_'.'Performance_Report_'.$currdate.'.pdf', 'I');
		}
		
	
  	}

	
	function pdf()
	{  
		$studentid = $this->session->userdata('studentid');
		$this->load->model('studentprogressmodel');
		//get student according to userid
		$data['studentrecord']=$this->studentprogressmodel->getrecord_student($studentid);
		
		if(!empty($data['studentrecord'][0]['Name']) && $data['studentrecord'][0]['Name']!="")
		{
		@$studentname = $data['studentrecord'][0]['Name'];
		}
		if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
		{		
		@$data['schoolname']=$this->studentprogressmodel->getschoolname($data['studentrecord'][0]['school_id']);
		}
		if(!empty($data['studentrecord'][0]['country_id']) && $data['studentrecord'][0]['country_id']!="")
		{	
		@$data['countryname']=$this->studentprogressmodel->getcountry($data['studentrecord'][0]['country_id']);
		}
		if(!empty($data['studentrecord'][0]['district_id']) && $data['studentrecord'][0]['district_id']!="")
		{	
		@$data['districtname']=$this->studentprogressmodel->getdistrict($data['studentrecord'][0]['district_id']);
		}
		if(!empty($data['districtname'][0]['state_id']) && $data['districtname'][0]['state_id']!="")
		{
		@$data['statename']=$this->studentprogressmodel->getstate($data['districtname'][0]['state_id']);
		}
		//get student performance detail according to their student id
		@$data['studentperformance'] = $this->studentprogressmodel->getstuperformance($studentid);
		
	if(!empty($data['studentrecord'][0]['school_id']) && $data['studentrecord'][0]['school_id']!="")
		{
	
	@$data['teachername']=$this->studentprogressmodel->getteachername($data['studentrecord'][0]['school_id']);
		}
		$assessname =array();
		
		
			foreach($data['studentperformance'] as $k => $v)
			{
				$assessmentid = $v['assignment_id'];
				// get assessment name
				$assessname[] = $this->studentprogressmodel->getassessment($assessmentid);
			}
		@$data['assessmentname'] = $assessname;
		
	

		 require_once('tcpdf/config/lang/eng.php');
		 require_once('tcpdf/tcpdf.php');
		
	 			
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('Workshop');
				$pdf->SetTitle('Student Performance Report');
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Student Performance Report", PDF_HEADER_STRING);
				$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
				$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				$pdf->setLanguageArray($l);
				$pdf->SetFont('times', '', 12);
			
				$pdf->AddPage();
				$currdate = date('Y-m-d');
$html='
<br/><br/>
<table>
<tr>
<td colspan="4"><strong>Student Marks Detail</strong> :</td></tr>
<tr><td colspan="4"></td></tr>
<tr><td>Student Name:</td><td>'.@$studentname.'</td>
<td>Report Date : </td><td>'.@$currdate.'</td>
</tr>
<tr>
<td>School Name:</td><td>'.@$data['schoolname'][0]['school_name'].'</td>
</tr>
</table>
<br/><br/>
<table border="1px"  cellpadding="5px" cellspacing="0">
<tr>
<th><span style="font-weight:bold;">Assignment Name</span></th> <th><span style="font-weight:bold; ">Number Correct</span></th><th><span style="font-weight:bold; ">Percentage Correct</span></th><th><span style="font-weight:bold; ">Assessment Pass Score</span></th>
</tr>';
if(!empty($data["assessmentname"]))
{
for($i=0;$i<count($data["assessmentname"]);$i++)
{
	
	@$test = $data["assessmentname"][$i][0]["assignment_name"];
	@$point =  $data["studentperformance"][$i]['pass_score_point'];
@$percen =  $data["studentperformance"][$i]['pass_score_perc'];
@$totalscoreofassessment =  $data["assessmentname"][$i][0]['pass_score'];
$html .=
'<tr>
 <td>'.$test.'</td>
 <td>'.$point.'</td>
 <td>'.$percen.'</td>
 <td>'.$totalscoreofassessment.'</td>

</tr>';
}
}
else
{
$html .=
'<tr>
 <td></td>
 <td></td>
 <td>The Student has not attempted any assessment yet.</td>
 <td></td>
</tr>';	

}

	$html.='</table><br/><br/>';
	$pdf->writeHTML($html, true, false, true, false, '');

	$pdf->lastPage();
	$pdf->Output($studentname.'_'.'Performance_Report_'.$currdate.'.pdf', 'I');
	}

/*	function getschools()
	{
		@$schooltypeid= $_REQUEST['school_type_id'];
		$this->load->model('studentprogressmodel');
		$data['result']=$this->studentprogressmodel->getallschools($schooltypeid);
		echo '<option value="">Please Select</option>';
		foreach($data['result'] as $key => $value)
		{
			echo '<option value="'.$value['school_id'].'">'.$value['school_name'].'</option>';
		}
		
	} */
/*	function getgrades()
	{
		$schoolid = $_REQUEST['schoolid'];
		$this->load->model('studentprogressmodel');
		$data['grades']=$this->studentprogressmodel->getgrades();
		echo '<option value="">Please Select</option>';
		foreach($data['grades'] as $key => $value)
		{
			echo '<option value="'.$value['dist_grade_id'].'">'.@$value['grade_name'].'</option>';
		}
	} */
	
	function getstudentdetailtable()
	{
		$login_required = $this->session->userdata('login_required');
		/* if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{  */
 			@$schoolid = $_REQUEST['schools'];
			@$gradeid = $_REQUEST['grades'];
			@$assignment_id = $_REQUEST['assignment_id'];
			$this->load->model('studentprogressmodel');
 			//get all student according to selected school
			$tablehtml =''; $students =array();
			$students=$this->studentprogressmodel->getstudents($schoolid,$gradeid,$assignment_id);
			$tablehtml = '<table width="100%" align="center" id="reporthtml" class="table table-striped table-bordered">
          <tbody> <tr class="tcrow2"> <td align="left" colspan="6"  ><b><u> Student Details</u> </b></td>
		  <td  height="30px" width="25%" id="printall" align="right"> <a class="btnperformance btn btn-small btn-purple" onclick = "allstudentreport()" >Print All Progress Report</a> </td>
  
           </tr>  <tr class="tchead"> 
		   <th> Sr.No</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
			  <th>Grade</th>
              <th>School Name</th> <th>View Detail</th></tr> ';
			
			if(!empty($students))
			{
				$cnt =0;
				foreach($students as $k=>$v)
				{
					$schoolname =array(); $grade =array();
					$cnt++;
					$schoolname=$this->studentprogressmodel->getschoolname($schoolid);
					$grade=$this->studentprogressmodel->getgradename($gradeid);
				//	echo"<pre>";print_r($schoolname);
					$tablehtml .= '<tr class="tcrow2">';
					$tablehtml .= '<td align="center" width="5%">'.$cnt.'</td>';
              		$tablehtml .= '<td align="center" width="14%">'.$v['Name'].'</td>';
					$tablehtml .= '<td align="center" width="14%">'.$v['Surname'].'</td>';
					$tablehtml .= '<td align="center" width="20%">'.$v['email'].'</td>';
					$tablehtml .= '<td align="center" width="12%">'.$grade[0]['grade_name'].'</td>';
					$tablehtml .= '<td align="center" width="15%">'.$schoolname[0]['school_name'].'</td>';
					$tablehtml .= '<td align="center" width="20%"> <a  target="_blank" class="btnperformance btn btn-small btn-purple" 
 href="'.base_url().'studentprogress/studentreportaspdf?user_id='.$v['UserID'].'&assessment_id='.$assignment_id.'" >Print Progress Report</a> </td>';
 					$tablehtml .= '<input type="hidden" id="studidarray[]" name="studidarray[]" value="'.$v['UserID'].'" />';
					$tablehtml .= '</tr>';
				}
			}
			else{ 
				$tablehtml .= '<td align="center" colspan="7" height="30px"> No Record Available.  </td></tr>';
			 } 
		$tablehtml .= '<tr><input type="hidden" id ="selectedassignment" name="selectedassignment" value="'.$assignment_id.'" /> </td> </tr>';
			$tablehtml .= '</tbody></table>'; 
			echo $tablehtml;
	 // 	}
 	}
	
 /*	function getsinglestudentdetail($userid)
	{
		$login_required = $this->session->userdata('login_required');
		if(empty($login_required) && $login_required =='')
		{
			if($_SERVER["HTTP_HOST"]=="localhost"){
			echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="www.nanowebtech.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/testbank/workshop/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="dev.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
			else if($_SERVER["HTTP_HOST"]=="login.ueisworkshop.com"){
				echo '<script>document.location.href="'.$_SERVER['HTTP_HOST'].'/index.php/";</script>';
			}
		}
		else
		{
	
		$this->load->model('studentprogressmodel');
		//get student according to userid
		$data['studentrecord']=$this->studentprogressmodel->getrecord_student($userid);
				
		$data['schoolname']=$this->studentprogressmodel->getschoolname($data['studentrecord'][0]['school_id']);
		$data['countryname']=$this->studentprogressmodel->getcountry($data['studentrecord'][0]['country_id']);
		
		$data['districtname']=$this->studentprogressmodel->getdistrict($data['studentrecord'][0]['district_id']);
		$data['statename']=$this->studentprogressmodel->getstate($data['districtname'][0]['state_id']);
		//get student performance detail according to their student id
		$data['studentperformance'] = $this->studentprogressmodel->getstuperformance($userid);
		if(empty($data['studentperformance']))
		{
			$data['view_path']=$this->config->item('view_path');
			$this->load->view('studentdetail/recordnotfound',$data);	
		}
		else
		{
			
		$assessname =array();
		foreach($data['studentperformance'] as $k => $v)
		{
			$assessmentid = $v['assignment_id'];
			// get assessment name
			$assessname[] = $this->studentprogressmodel->getassessment($assessmentid);
		}
		$data['assessmentname'] = $assessname;
		$data['view_path']=$this->config->item('view_path');
		$this->load->view('studentdetail/individualstudentchart',$data);
		
		
		}
		
		}
	
	}
	
	*/
	
}	