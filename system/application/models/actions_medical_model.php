<?php
class Actions_medical_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_all_data($page=false,$per_page=false,$need_medical_child_id='')
	{
		if($page!='all')
	{
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		}
	
	$this->db->select('*');
	$this->db->from('actions_medical');
	$this->db->join('intervention_strategies_medical','actions_medical.need_medical_child_id = intervention_strategies_medical.id','LEFT');
	$this->db->where('actions_medical.need_medical_child_id',$need_medical_child_id);
	$this->db->where('actions_medical.is_delete',0);
	$this->db->where('actions_medical.district_id',$this->session->userdata('district_id'));
	$query = $this->db->get();
	
		//print_r($this->db->last_query());exit;
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}	

public function get_all_medical_data($page=false,$per_page=false)
	{
		if($page!='all')
	{
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		}
	
	$this->db->select('*');
	$this->db->from('actions_medical');
	$this->db->join('intervention_strategies_medical','actions_medical.need_medical_child_id = intervention_strategies_medical.id','LEFT');
	$this->db->where('actions_medical.is_delete',0);
	$this->db->where('actions_medical.district_id',$this->session->userdata('district_id'));
	$query = $this->db->get();
	
		//print_r($this->db->last_query());exit;
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}	

	
	
	public function get_all_dataCount($need_medical_child_id='')
	{
		if($need_medical_child_id!=''){
			$this->db->where('actions_medical.need_medical_child_id',$need_medical_child_id);
			}
	$this->db->where('is_delete',0);
		$query = $this->db->get('actions_medical');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
/*public function delete($table,$where)
	{
		return $this->db->delete($table,$where);
		
	
	}*/
	
	public function delete_actions_medical($table,$field)
	   {
		$this->db->where('action_medical_id',$this->input->post('action_medical_id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_actions_medicalById($where){
		$query = $this->db->get_where('actions_medical',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
	public function get_all_action_medical()
	{
       $this->db->select('*');
	$this->db->from('actions_medical');
	$this->db->join('intervention_strategies_medical','actions_medical.need_medical_child_id = intervention_strategies_medical.id','LEFT');
	$this->db->where('actions_medical.is_delete',0);
	$this->db->where('actions_medical.district_id',$this->session->userdata('district_id'));
	$query = $this->db->get();
	
		//print_r($this->db->last_query());exit;
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	
public function get_need_medical_by_action_medical($needs_action_medical)
	{
    $this->db->select('*');
	$this->db->from('actions_medical');
	$this->db->join('intervention_strategies_medical','actions_medical.need_medical_child_id = intervention_strategies_medical.id','LEFT');
	$this->db->where('actions_medical.need_medical_child_id',$needs_action_medical);
	$this->db->where('actions_medical.is_delete',0);
	$this->db->where('actions_medical.district_id',$this->session->userdata('district_id'));
	$query = $this->db->get();
	
		//print_r($this->db->last_query());exit;
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}	
}
?>