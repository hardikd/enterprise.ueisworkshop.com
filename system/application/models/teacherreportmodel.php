<?php
class Teacherreportmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getReportTeacherCount()
	{
	
		 $teacher_id=$this->session->userdata('report_teacher_id');
		 $form=$this->session->userdata('reportform');
		$yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery.=" and year(r.report_date)='$year' ";
		
		}
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $yearquery.=" and r.period_lesson='$formtype' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		 if($form=='formp')
		{
		 $qry = "select count(*) as count from proficiencyreports r where  r.teacher_id=$teacher_id and r.report_form='$form' $yearquery  ";
		}
		else
		{
			 $qry = "select count(*) as count from reports r where  r.teacher_id=$teacher_id and r.report_form='$form' $yearquery ";
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportByTeacher($start, $per_page)
	{
	
		$teacher_id=$this->session->userdata('report_teacher_id');
		$form=$this->session->userdata('reportform');
		$yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery.=" and year(r.report_date)='$year' ";
		
		}
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $yearquery.=" and r.period_lesson='$formtype' ";
		
		}
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		if($form=='formp')
		{
		 $qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where  r.teacher_id=$teacher_id and r.report_form='$form' and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 $yearquery limit $start, $per_page ";
		}
		else
		{
			 $qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where  r.teacher_id=$teacher_id and r.report_form='$form' and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 $yearquery limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getReportobserverCount()
	{
	
		 $observer_id=$this->session->userdata('report_observer_id');
		 if($this->session->userdata('teacher_id')){
			$teacher_id=$this->session->userdata('teacher_id');
		 } else {
			 $teacher_id = $this->input->post('teacher_id');
		}
		$form=$this->session->userdata('reportform');
		 $formtypeqery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypeqery.=" and r.period_lesson='$formtype' ";
		
		}
		  if($form=='formp')
		{
		 $qry = "select count(*) as count from proficiencyreports r where r.teacher_id=$teacher_id and r.observer_id=$observer_id and r.report_form='$form' $formtypeqery ";
		}
		else
		{
			$qry = "select count(*) as count from reports r where r.teacher_id=$teacher_id and r.observer_id=$observer_id and r.report_form='$form' $formtypeqery ";
		}
		 
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportByobserver($start, $per_page)
	{
	
		$observer_id=$this->session->userdata('report_observer_id');
		
		if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	
		}
		
		$form=$this->session->userdata('reportform');
		 $formtypeqery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypeqery.=" and r.period_lesson='$formtype' ";
		
		}
		 if($form=='formp')
		{
		 $qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.teacher_id=$teacher_id  $formtypeqery and r.report_form='$form' and r.observer_id=$observer_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		else
		{
			$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.teacher_id=$teacher_id  $formtypeqery and r.report_form='$form' and r.observer_id=$observer_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	
	
	}
	function getReportgradeCount()
	{
	
		 $grade_id=$this->session->userdata('report_grade_id');
		$teacher_id=$this->session->userdata('teacher_id');
		$form=$this->session->userdata('reportform');
		  $formtypeqery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypeqery.=" and r.period_lesson='$formtype' ";
		
		}
		 if($form=='formp')
		{
		 $qry = "select count(*) as count from proficiencyreports r where r.teacher_id=$teacher_id and r.grade_id=$grade_id and r.report_form='$form' $formtypeqery  ";
		}
		else
		{
			$qry = "select count(*) as count from reports r where r.teacher_id=$teacher_id and r.grade_id=$grade_id and r.report_form='$form' $formtypeqery  ";
		}
		 
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportBygrade($start, $per_page)
	{
	
		$grade_id=$this->session->userdata('report_grade_id');
		$teacher_id=$this->session->userdata('teacher_id');
		$form=$this->session->userdata('reportform');
		 $formtypeqery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypeqery.=" and r.period_lesson='$formtype' ";
		
		}
		if($form=='formp')
		{
		 $qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.teacher_id=$teacher_id  $formtypeqery and r.report_form='$form' and r.grade_id=$grade_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		else
		{
			$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.teacher_id=$teacher_id  $formtypeqery and r.report_form='$form' and r.grade_id=$grade_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getAllqualitative($group_id,$report)
	{
		$from=$this->session->userdata('report_from');
		$to=$this->session->userdata('report_to');
		$form=$this->session->userdata('reportform');
if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	

		}
		
		$district_id=$this->session->userdata('district_id');
		
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		if($form=='forma')
		{
			$qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id  and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else  if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getAllqualitativerubiscale($group_id,$sub_id,$report)
	{
		$from=$this->session->userdata('report_from');
		$to=$this->session->userdata('report_to');
		$form=$this->session->userdata('reportform');
		if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	

		}
		$district_id=$this->session->userdata('district_id');
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		
			if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
		    }
			else
			{
				 $qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.point_id=$sub_id " ;	
			}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		 if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
		}
		else 
		{
			$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and  r.report_form='$form' and  r.report_id=ob.report_id and    ob.point_id=$sub_id " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
			}
		else  
		{
			$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.point_id=$sub_id " ;
		}
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getAllReport($report)
	{
	//print_r($report);exit;
	if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	

		}
	    $form=$this->session->userdata('reportform');
		//$teacher_id=$this->session->userdata('teacher_id');
		$district_id=$this->session->userdata('district_id');
		
		$formtypequery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		 $yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery=" and year(r.report_date)='$year' ";
		
		}
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $yearquery.=" and r.period_lesson='$formtype' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
			
			$qry = "select report_id from reports r where r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_form='$form' $yearquery $formtypequery " ;
		
		}
		
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		
		  $qry = "select report_id from reports r where r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id  and r.report_form='$form' $formtypequery " ;
		
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		 
			 $qry = "select report_id from reports r where r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id  and r.report_form='$form' $formtypequery " ;
		
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getAllsectional($group_id,$report)
	{
		
		$form=$this->session->userdata('reportform');
		
		if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	
		}
		
		$district_id=$this->session->userdata('district_id');
		$formtypequery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($report=='teacher')
		{
		
		$yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		
		
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		
		$teacher_id=$this->session->userdata('report_criteria_id');	
		if($form=='forma')
		{
			
			$qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.teacher_id=$teacher_id and r.district_id=$district_id and  b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $yearquery $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.teacher_id=$teacher_id and r.district_id=$district_id and  b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $yearquery $formtypequery ; " ;
		
		}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.grade_id=$grade_id and r.teacher_id=$teacher_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.grade_id=$grade_id and r.teacher_id=$teacher_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		
		}
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllsectionalrubiscale($group_id,$sub_id,$report)
	{
		
		
	if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	
		}
	$district_id=$this->session->userdata('district_id');
		$formtypequery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($report=='teacher')
		{
		   $yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		
		   $teacher_id=$this->session->userdata('report_criteria_id');	
		
			if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.teacher_id=$teacher_id  and r.district_id=$district_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $yearquery $formtypequery " ;
		    }
			else
			{
				 $qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.teacher_id=$teacher_id  and r.district_id=$district_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id  $yearquery $formtypequery " ;
			}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		 if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.observer_id=$observer_id and r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $formtypequery " ;
		}
		else 
		{
			$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.observer_id=$observer_id and r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id $formtypequery " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.grade_id=$grade_id and r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $formtypequery " ;
			}
		else  
		{
			$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.grade_id=$grade_id and r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id $formtypequery " ;
		}
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
}	