<?php
class Districtmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function is_valid_login($id=false){
	
		if($id==false)
		{
		$username = $this->input->post('username');
		$password = md5(trim($this->input->post('password')));
		$qry="SELECT u.avatar,d.copydistrict,d.districts_name,d.feedback,d.type,d.modified,u.username,d.state_id,d.country_id,u.district_id,u.dist_user_id from  dist_users u,districts d  WHERE u.username='".$username."' AND u.password='".$password."' and u.district_id=d.district_id ";
		}
		else
		{
		$qry="SELECT u.avatar,d.copydistrict,d.districts_name,d.feedback,d.type,d.modified,u.username,d.state_id,d.country_id,u.district_id,u.dist_user_id from  dist_users u,districts d  WHERE u.dist_user_id=$id and u.district_id=d.district_id ";
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getDistrictBystateid($state_id)
	{
	
		$qry="select district_id as dist from districts where copystate=1 and state_id=$state_id";
	 $query = $this->db->query($qry);
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->dist;	
		}else{
			return 0;
		}
			
	
	
	
	}
	
	function savecopydistrict($dist_id)
	{
	   $qry = "update districts set copydistrict=1 where district_id=$dist_id ";
		$query = $this->db->query($qry);
	
	
	}
	function getemail($name)
	{
	
		$qry="SELECT email from subsriptions where districtname='$name'";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
	
	}
	function getdemodistrict($id)
	{
	
		$qry="Select d.state_id,d.copydistrict,d.districts_name,d.feedback,d.type,d.modified,c.country,s.name from districts d,countries c,states s where d.district_id=$id  and d.country_id=c.id  and d.state_id=s.state_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
	
	}
	function savefeedback()
	{
	
	 $pdata=$_POST['selected'];
	  $feedback=$pdata['feedback'];
	  $dist_id=$pdata['dist_id'];
	  
	  $qry = "insert into feedback 
	(dist_id, 
	feedback
	)
	values
	($dist_id, 
	'$feedback'
	);
 ";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			
			 $qry = "update districts set feedback=1 where district_id=$dist_id ";
		$query = $this->db->query($qry);
			
			return true;
		}else{
			return false;
		}
	
	
	}
	function getDistrictCount($state_id,$country_id)
	{
	
		if($state_id=='all')
		{
		$qry="Select count(*) as count 
	  			from districts where country_id=$country_id " ;
		}
		else
		{
			$qry="Select count(*) as count 
	  			from districts where country_id=$country_id and  state_id=$state_id " ;

		}	
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getDistricts($page,$per_page,$state_id,$country_id)
	{
		
		
		$page -= 1;
		$start = $page * $per_page;
		if($state_id=='all')
		{
			$qry="Select d.*,c.country,s.name from districts d,countries c,states s where d.country_id=$country_id and d.country_id=c.id  and d.state_id=s.state_id limit $start, $per_page ";
		
		}
		else
		{
			$qry="Select d.*,c.country,s.name from districts d,countries c,states s  where d.country_id=$country_id and d.state_id=$state_id and d.country_id=c.id  and d.state_id=s.state_id limit $start, $per_page ";
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getDistrictById($dist_id)
	{
		
		$qry = "Select district_id,districts_name,country_id,state_id,copystate from districts where district_id=".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function getDistrictsByStateId($state_id)
	{
		
		$qry = "Select district_id,districts_name,country_id,state_id from districts where state_id=".$state_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function check_district_exists()
	{
	
		$name=$this->input->post('district_name');
		$qry="SELECT  district_id from districts where districts_name='$name' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	
	
	}
	
	function check_district_update() {
	
	
		$name=$this->input->post('district_name');
		$id=$this->input->post('district_id');
		$qry="SELECT district_id from districts where districts_name='$name' and district_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	
	}
	
	function check_copystate()
	{
	
	 $state_id=$this->input->post('state_id');
	 $qry="select count(*) as count from districts where copystate=1 and state_id=$state_id";
	 $query = $this->db->query($qry);
		$row = $query->row();
			return $row->count;		
		
	
	}
	
	function check_copystate_update()
	{
	
	
	$district_id=$this->input->post('district_id');
	 $state_id=$this->input->post('state_id');
	 $qry="select count(*) as count from districts where copystate=1 and state_id=$state_id and district_id!=$district_id ";
	 $query = $this->db->query($qry);
		$row = $query->row();
			return $row->count;	
	
	
	
	
	}
	function add_district()
	{
	  if($this->input->post('copystate'))
	  {
	    $copystate=1;
		
	  }
	  else
	  {
	    $copystate=0;
	  
	  }
	  
	  $data = array('districts_name' => $this->input->post('district_name'),
					'country_id' => $this->input->post('country_id'),
					'state_id' => $this->input->post('state_id'),
					'copystate' => $copystate
		
		
		);
	   try{
			$str = $this->db->insert_string('districts', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_district()
	{
	
		if($this->input->post('copystate'))
	  {
	    $copystate=1;
	  }
	  else
	  {
	    $copystate=0;
	  
	  }
		
		$district_id=$this->input->post('district_id');
	$data = array('districts_name' => $this->input->post('district_name'),
					'country_id' => $this->input->post('country_id'),
					'state_id' => $this->input->post('state_id'),
					'copystate' => $copystate
		
		
		);
		
			
		$where = "district_id=".$district_id;		
		
		try{
			$str = $this->db->update_string('districts', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteDistrict($dist_id)
	{
		$qry = "delete from districts where district_id=$dist_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getalldistricts()
	{
	
	 $qry = "Select district_id,districts_name from districts ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getdistrictIDByteacherID($teacher_id)
	
	{
		$qry = "Select s.district_id from teachers t,schools s where t.school_id=s.school_id and t.teacher_id=$teacher_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function getdist_userCount()
	{
	    $dist_id=$this->session->userdata('user_dist_id');
		$qry="Select count(*) as count 
	  			from dist_users where district_id=$dist_id " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getdist_users($page,$per_page)
	{
		 $dist_id=$this->session->userdata('user_dist_id');
		$page -= 1;
		$start = $page * $per_page;
		$qry="Select b.dist_user_id as dist_user_id,b.username as username from dist_users b where b.district_id=$dist_id limit $start, $per_page ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getdist_userById($dist_user_id)
	{
		
		$qry = "Select dist_user_id,username,email from dist_users where dist_user_id=".$dist_user_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function check_dist_user_exists()
	{
	
		$name=$this->input->post('username');
		$qry="SELECT  dist_user_id from dist_users where username='$name'  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	
	
	}
	
	
	//update district user
	function check_dist_user_update() {
	
	
		$name=$this->input->post('username');
		$id=$this->input->post('dist_user_id');
		$qry="SELECT dist_user_id from dist_users where username='$name'  and dist_user_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	
	}
	function add_dist_user()
	{
	  $data = array('district_id' => $this->input->post('dist_user_id_insert'),
		'password'=> md5($this->input->post('password')),
		'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email')
		
		
		);
		
		/*$newdata=array(
		'district_id' => $this->input->post('dist_user_id_insert'),
		'password'=> md5($this->input->post('password')),
		'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email'),
		'user_type'=>1
			
		);*/
		
		
		
	   try{
			$str = $this->db->insert_string('dist_users', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
		
	}
	
	function getdistusers($dist_user_id)
	{
		
		$qry="SELECT * FROM dist_users where dist_user_id='".$dist_user_id."'";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function adddistrictuser($dist_user_id,$dist_username,$dist_userpassword,$dist_user_email,$dist_id)
	{
		$data = array(
   'dist_user_id' => $dist_user_id ,
   'UserName' => $dist_username ,
   'Password' => $dist_userpassword,
   'email' => $dist_user_email,
   'district_id' => $dist_id,
   'user_type'=>1,
   'approved' =>1
);

	$this->db->insert('users', $data); 
	
	}
	function update_dist_user()
	{
	
	
		$password=$this->input->post('password');
		if($password!='torvertex')
		{
		 
		 $password=md5($password);
		 
		$data = array('password'=>$password,
		           'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email')	
		
		
		);
		
		
		
		
		}
		
		else
		{
		
		
			$data = array( 'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email')
		
		
		);
		
		
		}
		
			$newdata = array( 'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email')
		
		
		);
		
		
		$dist_user_id=$this->input->post('dist_user_id');
	
		$where = "dist_user_id=".$dist_user_id;	
		$this->db->where('dist_user_id', $dist_user_id);
		$this->db->update('users', $newdata); 	
		
		try{
			$str = $this->db->update_string('dist_users', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletedist_user($dist_user_id)
	{
		$qry = "delete from dist_users where dist_user_id=$dist_user_id";
		$sql = "delete from users where dist_user_id=$dist_user_id";
		$this->db->query($sql);
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getdist_userByDistrictId($dist_id)
	{
		
		$qry = "Select dist_user_id,username from dist_users where district_id=".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function checkpass()
	{
	   $password = md5(trim($this->input->post('current')));
	   if($this->session->userdata("login_type")=='user') {
	     $login_id=$this->session->userdata("dist_user_id");
		 
		 $qry=" select dist_user_id from dist_users where dist_user_id=$login_id and password='$password' ";
	   
	   }else if($this->session->userdata("login_type")=='teacher') {
		 $login_id=$this->session->userdata("teacher_id");  
		 $qry=" select teacher_id from teachers where teacher_id=$login_id and password='$password' ";
		}
		else if($this->session->userdata("login_type")=='observer') {
		   $login_id=$this->session->userdata("observer_id");
		$qry=" select observer_id from observers where observer_id=$login_id and password='$password' ";
		}
	
	
	  $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return true;		
		}else{
			return false;
		}
	
	
	
	
	
	}
	
	function changepass()
	{
	
	   $password = md5(trim($this->input->post('newp')));
	   if($this->session->userdata("login_type")=='user') {
		   
	   $login_id=$this->session->userdata("dist_user_id");
	    $username = $this->session->userdata('username');
		 $qry=" update dist_users set  password='".$password."' where dist_user_id='".$login_id."'  ";
		  $qry1="update users set Password='".$password."' where UserName='".$username."'";
		    $query = $this->db->query($qry1);
	   
	   }else if($this->session->userdata("login_type")=='teacher') {
		 $login_id=$this->session->userdata("teacher_id");  
		  $qry=" update teachers set  password='$password' where teacher_id=$login_id  ";
		}
		else if($this->session->userdata("login_type")=='observer') {
		   $login_id=$this->session->userdata("observer_id");
		 $qry=" update observers set  password='$password' where observer_id=$login_id  ";
		}
	
	
	  $query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return false;
		}
	
	
	
	
	
	}
	function checkusername()
	{
	   $username = $this->input->post('username');
	   $type = $this->input->post('loginas');
	   if($type=='district') {
	     
		 
		 $qry=" select dist_user_id as login_id,email from dist_users where username='$username' ";
	   
	   }else if($type=='observer') {
		 
		 $qry=" select observer_id as login_id,email from observers where username='$username' ";
		 
		}
		else if($type=='teacher') {
		  
		 $qry=" select teacher_id as login_id,email from teachers where username='$username' ";
		}
	
	
	  $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();	
		}else{
			return false;
		}
	
	
	
	
	
	}
	function changerandompassword($password,$login_id)
	{
	   
	   $password = md5($password);
	   $type = $this->input->post('loginas');
	   if($type=='district') {
	     
		 
		 $qry=" update dist_users set  password='$password' where dist_user_id=$login_id  ";
	   
	   }else if($type=='teacher') {
		 
		  $qry=" update teachers set  password='$password' where teacher_id=$login_id  ";
		}
		else if($type=='observer') {
		   
		 $qry=" update observers set  password='$password' where observer_id=$login_id  ";
		}
	
	
	  $query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return false;
		}
	
	
	
	}
        
        function copymasterdata($newdistrict,$olddistrict){
			
            $query = $this->db->get_where('dist_grades',array('district_id'=>$olddistrict));
            $grades = $query->result();
            foreach($grades as $grade){
                $newgrade = array('grade_name'=>$grade->grade_name,'district_id'=>$newdistrict);
               // print_r($newgrade);exit;
                $this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('dist_grades', $newgrade);
                $oldgrade[$grade->dist_grade_id] = $this->db->insert_id();
            }
//            print_r($oldgrade);exit;
            //For subjects
            $querysub = $this->db->get_where('dist_subjects',array('district_id'=>$olddistrict));
            $subjects = $querysub->result();
            foreach($subjects as $subject){
                $newsubject = array('subject_name'=>$subject->subject_name,'district_id'=>$newdistrict,'eld'=>$subject->eld);
               // print_r($newgrade);exit;
                $this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('dist_subjects', $newsubject);
            }
            
            $this->db->select('*');
            $this->db->from('standards');
            $this->db->join('dist_subjects','standards.subject_id = dist_subjects.dist_subject_id');
            $this->db->where('standards.district_id',$olddistrict);
            $querystrand = $this->db->get();
//            echo $this->db->last_query();exit;
            $resultstrands = $querystrand->result();
            
            foreach($resultstrands as $resultstrand){
                
                $querysubject_id = $this->db->get_where('dist_subjects',array('district_id'=>$newdistrict,'subject_name'=>$resultstrand->subject_name));
//                echo $this->db->last_query();exit;
                $resultsubject_id = $querysubject_id->result();
//                     print_r($resultsubject_id);exit;   
                $standards = array('district_id'=>$newdistrict,'subject_id'=>$resultsubject_id[0]->dist_subject_id,'status'=>$resultstrand->status);
                $this->db->set('created', 'NOW()', FALSE);
               $this->db->insert('standards', $standards);
               $oldstrand[$resultstrand->standard_id] = $this->db->insert_id();
            }
            
//            print_r($oldstrand);exit;
            $this->db->select('*');
            $this->db->from('standarddata');
            $this->db->join('dist_grades','standarddata.grade = dist_grades.dist_grade_id','LEFT');
            $this->db->join('standards','standarddata.standard_id = standards.standard_id','LEFT');
            $this->db->where('dist_grades.district_id',$olddistrict);
            $querystrandarddata = $this->db->get();
            $resultstandarddatas = $querystrandarddata->result();
//             echo $this->db->last_query();exit;
//            print_r($resultstandarddatas);exit;
            foreach($resultstandarddatas as $resultstandarddata){
                
                $standarddataarr = array('grade'=>$oldgrade[$resultstandarddata->grade],'standard_id'=>$oldstrand[$resultstandarddata->standard_id],'strand'=>$resultstandarddata->strand,'standard'=>$resultstandarddata->standard);
                $this->db->insert('standarddata',$standarddataarr);
//                echo $this->db->last_query();
                
                
            }
            
            
             //For subjects
            $querycustom_differentiated = $this->db->get_where('custom_differentiated',array('district_id'=>$olddistrict));
            $custom_differentiateds = $querycustom_differentiated->result();
            foreach($custom_differentiateds as $custom_differentiated){
                $newcustom_differentiated = array('tab'=>$custom_differentiated->tab,'district_id'=>$newdistrict);
               // print_r($newgrade);exit;
                $this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('custom_differentiated', $newcustom_differentiated);
            }
            
            $queryrubric_data = $this->db->get_where('rubric_data',array('district_id'=>$olddistrict));
            $rubric_datas = $queryrubric_data->result();
            foreach($rubric_datas as $rubric_data){
                $newrubric_data = array('tab'=>$rubric_data->tab,'district_id'=>$newdistrict);
               // print_r($newgrade);exit;
                $this->db->set('created', 'NOW()', FALSE);
                $this->db->set('modified', 'NOW()', FALSE);
                $this->db->insert('rubric_data', $newrubric_data);
            }
            
            $querystandard_language = $this->db->get_where('standard_language',array('district_id'=>$olddistrict));
            $standard_datas = $querystandard_language->result();
            foreach($standard_datas as $standard_data){
                $newstandard_language = array('standard_language'=>$standard_data->standard_language,'district_id'=>$newdistrict,'status'=>'Active','is_delete'=>$standard_data->is_delete);
               // print_r($newgrade);exit;
                $this->db->insert('standard_language', $newstandard_language);
            }
            return true;
		
        }
		
		

		
	function checklist_rubricscale_data($newdistrict,$olddistrict){
	
            $query = $this->db->get_where('rubricscale',array('district_id'=>$olddistrict));
            $rubri = $query->result();
		    foreach($rubri as $rubris){
				$new_rubri = array('scale_name'=>$rubris->scale_name,'district_id'=>$newdistrict,'description'=>$rubris->description);
				//$this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('rubricscale',$new_rubri);
              }
            return true;
	
        }
		
		
			function checklist_rubricscale_sub_data($newdistrict,$olddistrict){
	
            $query = $this->db->get_where('rubricscale_sub',array('district_id'=>$olddistrict));
            $rubri_sub = $query->result();
		    foreach($rubri_sub as $rubris_sub){
				$new_rubri_sub = array('sub_scale_name'=>$rubris_sub->sub_scale_name,'district_id'=>$newdistrict,'scale_id'=>$rubris_sub->scale_id);
					//$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('rubricscale_sub',$new_rubri_sub);
              }
            return true;
	
        }
		
		
	
		
	
		
		

function teacher_status($newdistrict,$olddistrict){
	
			$query = $this->db->get_where('status',array('district_id'=>$olddistrict));
            $teacher_status = $query->result();
		    foreach($teacher_status as $teacher_statu){
			$new_teacher_statu = array('status_name'=>$teacher_statu->status_name,'district_id'=>$newdistrict);
					//$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('status',$new_teacher_statu);
              }
            return true;
	
        }
	
function teacher_performance_rating($newdistrict,$olddistrict){
			$query = $this->db->get_where('score',array('district_id'=>$olddistrict));
            $teacher_score = $query->result();
		    foreach($teacher_score as $teacher_scores){
			$new_teacher_score = array('score_name'=>$teacher_scores->score_name,'district_id'=>$newdistrict);
					//$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('score',$new_teacher_score);
              }
            return true;
	
        }	

    function sst_meeting_type($newdistrict,$olddistrict){
			$query = $this->db->get_where('sst_meeting_type',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_meeting_type = array('meeting_type'=>$meeting_type->meeting_type,'district_id'=>$newdistrict);
				$this->db->insert('sst_meeting_type',$new_meeting_type);
            }
            return true;
	}

	function sst_program_change($newdistrict,$olddistrict){
			$query = $this->db->get_where('sst_program_change',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('program_change'=>$meeting_type->program_change,'district_id'=>$newdistrict);
				$this->db->insert('sst_program_change',$new_program_change);
            }
            return true;
	}
	function sst_progress_monitoring($newdistrict,$olddistrict){
			$query = $this->db->get_where('sst_progress_monitoring',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('progress_monitoring'=>$meeting_type->progress_monitoring,'district_id'=>$newdistrict);
				$this->db->insert('sst_progress_monitoring',$new_program_change);
            }
            return true;
	}
	function sst_referral_reason($newdistrict,$olddistrict){
			$query = $this->db->get_where('sst_referral_reason',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('referral_reason'=>$meeting_type->referral_reason,'district_id'=>$newdistrict);
				$this->db->insert('sst_referral_reason',$new_program_change);
            }
            return true;
	}
	function intervention_strategies_home($newdistrict,$olddistrict){
			$query = $this->db->get_where('intervention_strategies_home',array('district_id'=>$olddistrict,'parent_id'=>0));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('intervention_strategies_home'=>$meeting_type->intervention_strategies_home,'district_id'=>$newdistrict,'parent_id'=>0);
				$this->db->insert('intervention_strategies_home',$new_program_change);
				$last_id = $this->db->insert_id();
				//echo $meeting_type->id;exit;
				$this->db->select('*');
				$this->db->from('intervention_strategies_home');
				$this->db->where(array('district_id'=>$olddistrict,'parent_id'=>$meeting_type->id));
				$query_child = $this->db->get();
//				$query_child = $this->db->where('intervention_strategies_home',array('district_id'=>$olddistrict,'parent_id'=>$meeting_type->id));
				//echo $this->db->last_query();exit;

				$childs = $query_child->result();
				foreach($childs as $child){
					$new_child = array('intervention_strategies_home'=>$child->intervention_strategies_home,'district_id'=>$newdistrict,'parent_id'=>$last_id);
					$this->db->insert('intervention_strategies_home',$new_child);
					
				}
            }
            return true;
	}

	function intervention_strategies_medical($newdistrict,$olddistrict){
			$query = $this->db->get_where('intervention_strategies_medical',array('district_id'=>$olddistrict,'parent_id'=>0));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('intervention_strategies_medical'=>$meeting_type->intervention_strategies_medical,'district_id'=>$newdistrict,'parent_id'=>0);
				$this->db->insert('intervention_strategies_medical',$new_program_change);
				$last_id = $this->db->insert_id();
				//echo $meeting_type->id;exit;
				$this->db->select('*');
				$this->db->from('intervention_strategies_medical');
				$this->db->where(array('district_id'=>$olddistrict,'parent_id'=>$meeting_type->id));
				$query_child = $this->db->get();
				$childs = $query_child->result();
				foreach($childs as $child){
					$new_child = array('intervention_strategies_medical'=>$child->intervention_strategies_medical,'district_id'=>$newdistrict,'parent_id'=>$last_id);
					$this->db->insert('intervention_strategies_medical',$new_child);
					
				}
            }
            return true;
	}
	function intervention_strategies_school($newdistrict,$olddistrict){
			$query = $this->db->get_where('intervention_strategies_school',array('district_id'=>$olddistrict,'parent_id'=>0));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('intervention_strategies_school'=>$meeting_type->intervention_strategies_school,'district_id'=>$newdistrict,'parent_id'=>0);
				$this->db->insert('intervention_strategies_school',$new_program_change);
				$last_id = $this->db->insert_id();
				//echo $meeting_type->id;exit;
				$this->db->select('*');
				$this->db->from('intervention_strategies_school');
				$this->db->where(array('district_id'=>$olddistrict,'parent_id'=>$meeting_type->id));
				$query_child = $this->db->get();
				$childs = $query_child->result();
				foreach($childs as $child){
					$new_child = array('intervention_strategies_school'=>$child->intervention_strategies_school,'district_id'=>$newdistrict,'parent_id'=>$last_id);
					$this->db->insert('intervention_strategies_school',$new_child);
					
				}
            }
            return true;
	}

	function interventions_prior($newdistrict,$olddistrict){
			$query = $this->db->get_where('interventions_prior',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('interventions_prior'=>$meeting_type->interventions_prior,'district_id'=>$newdistrict);
				$this->db->insert('interventions_prior',$new_program_change);
            }
            return true;
	}
	function behavior_teacher_student($newdistrict,$olddistrict){
			$query = $this->db->get_where('behavior_teacher_student',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('behavior_name'=>$meeting_type->behavior_name,'district_id'=>$newdistrict);
				$this->db->insert('behavior_teacher_student',$new_program_change);
            }
            return true;
	}
	function ideas_for_parent_student($newdistrict,$olddistrict){
			$query = $this->db->get_where('ideas_for_parent_student',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('ideas_for_parent_student'=>$meeting_type->ideas_for_parent_student,'district_id'=>$newdistrict);
				$this->db->insert('ideas_for_parent_student',$new_program_change);
            }
            return true;
	}
	function indicate_parent_consent($newdistrict,$olddistrict){
			$query = $this->db->get_where('indicate_parent_consent',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('indicate_parent_consent'=>$meeting_type->indicate_parent_consent,'district_id'=>$newdistrict);
				$this->db->insert('indicate_parent_consent',$new_program_change);
            }
            return true;
	}
	function behavior_location($newdistrict,$olddistrict){
			$query = $this->db->get_where('behavior_location',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('behavior_location'=>$meeting_type->behavior_location,'district_id'=>$newdistrict);
				$this->db->insert('behavior_location',$new_program_change);
            }
            return true;
	}
	
	function dist_prob_behaviour($newdistrict,$olddistrict){
			$query = $this->db->get_where('dist_prob_behaviour',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('behaviour_name'=>$meeting_type->behaviour_name,'district_id'=>$newdistrict);
				$this->db->insert('dist_prob_behaviour',$new_program_change);
            }
            return true;
	}
	function concerns($newdistrict,$olddistrict){
			$query = $this->db->get_where('concerns',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('concerns_name'=>$meeting_type->concerns_name,'district_id'=>$newdistrict);
				$this->db->insert('concerns',$new_program_change);
            }
            return true;
	}
	function strengths($newdistrict,$olddistrict){
			$query = $this->db->get_where('strengths',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('strengths_name'=>$meeting_type->strengths_name,'district_id'=>$newdistrict);
				$this->db->insert('strengths',$new_program_change);
            }
            return true;
	}
	function history($newdistrict,$olddistrict){
			$query = $this->db->get_where('history',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('history_name'=>$meeting_type->history_name,'district_id'=>$newdistrict);
				$this->db->insert('history',$new_program_change);
            }
            return true;
	}
	function strategy($newdistrict,$olddistrict){
			$query = $this->db->get_where('strategy',array('district_id'=>$olddistrict));
            $meeting_types = $query->result();
		    foreach($meeting_types as $meeting_type){
				$new_program_change = array('strategy_name'=>$meeting_type->strategy_name,'district_id'=>$newdistrict);
				$this->db->insert('strategy',$new_program_change);
            }
            return true;
	}
		 function checklist_standards_data($newdistrict,$olddistrict){
			
            $query = $this->db->get_where('observation_groups',array('district_id'=>$olddistrict));
            $groups = $query->result();
		    foreach($groups as $group){
				$newgroup = array('group_name'=>$group->group_name,'district_id'=>$newdistrict,'description'=>$group->description);
				//$this->db->set('created', 'NOW()', FALSE);
                $this->db->insert('observation_groups',$newgroup);
				$oldgroup_id[$group->group_id] = $this->db->insert_id();
			
			}
		
            $query = $this->db->get_where('observ_points',array('district_id'=>$olddistrict));
            $observ = $query->result();

		    foreach($observ as $observs){

			$newgroup = array('group_id'=>$oldgroup_id[$observs->group_id],'group_type_id'=>$observs->group_type_id,'district_id'=>$newdistrict,'ques_type_id'=>$observs->ques_type_id,'question'=>$observs->question);

			
				$this->db->insert('observ_points',$newgroup);
				$point_id = $this->db->insert_id();
				$this->db->select('*');
				$this->db->where('point_id',$observs->point_id);	
				$this->db->from('observation_sub_groups');	
				$querysubgrp = $this->db->get();
				$resultsubgrps = $querysubgrp->result();
				foreach($resultsubgrps as $resultsubgrp){
					$standarddataarr = array('point_id'=>$point_id,'sub_group_name'=>$resultsubgrp->sub_group_name);	
					$this->db->insert('observation_sub_groups',$standarddataarr);	
				}
			}
            return true;
			

        }
		
function proficiency_rubric_standards($newdistrict,$olddistrict){
		 $query = $this->db->get_where('proficiency_groups',array('district_id'=>$olddistrict));
            $prof_group = $query->result();
		    foreach($prof_group as $prof_groups){
				$new_prof_groups = array('group_name'=>$prof_groups->group_name,'district_id'=>$newdistrict,'description'=>$prof_groups->description);
					//$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('proficiency_groups',$new_prof_groups);
				$pro_oldgroup_id[$prof_groups->group_id] = $this->db->insert_id();
              }
    

			$query = $this->db->get_where('proficiency_points',array('district_id'=>$olddistrict));
            $prof_point = $query->result();
		    foreach($prof_point as $prof_points){
				$new_prof_points = array('group_type_id'=>$prof_points->group_type_id,'ques_type_id'=>$prof_points->ques_type_id,'group_id'=>$pro_oldgroup_id[$prof_points->group_id],'district_id'=>$newdistrict,'question'=>$prof_points->question);
				$this->db->insert('proficiency_points',$new_prof_points);
				$prof_point_id[$prof_points->point_id] = $this->db->insert_id();
          }
			$this->db->select('*');
            $this->db->from('proficiency_sub_groups');
            $this->db->join('proficiency_points','proficiency_sub_groups.point_id = proficiency_points.point_id','LEFT');
            $this->db->where('proficiency_points.district_id',$olddistrict);
            $query_prof_group_data = $this->db->get();
            $result_prof = $query_prof_group_data->result();
//            echo $this->db->last_query();exit;
//            print_r($resultstandarddatas);exit;
            foreach($result_prof as $result_prof_group){
               $prof_aarr = array('point_id'=>$prof_point_id[$result_prof_group->point_id],'sub_group_name'=>$result_prof_group->sub_group_name,'sub_group_text'=>$result_prof_group->sub_group_text);
			  	 //print_r($prof_aarr);
				 $this->db->insert('proficiency_sub_groups',$prof_aarr);
                //echo $this->db->last_query();
              }
			
		return true;
	      }	
	
function likert_standards($newdistrict,$olddistrict){
	
            $query = $this->db->get_where('lickert_groups',array('district_id'=>$olddistrict));
            $lickert_group = $query->result();
		    foreach($lickert_group as $lickert_groups){
			$new_lickert_groups = array('group_name'=>$lickert_groups->group_name,'district_id'=>$newdistrict,'description'=>$lickert_groups->description);
					//$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('lickert_groups',$new_lickert_groups);
				$lickert_groups_id[$lickert_groups->group_id] = $this->db->insert_id();
              }
			$query = $this->db->get_where('lickert_points',array('district_id'=>$olddistrict));
            $lickert_point = $query->result();

		    foreach($lickert_point as $lickert_points){
				if($lickert_groups_id[$lickert_points->group_id]!=''){
				$new_lickert_points = array('group_type_id'=>$lickert_points->group_type_id,'ques_type_id'=>$lickert_points->ques_type_id,'group_id'=>$lickert_groups_id[$lickert_points->group_id],'district_id'=>$newdistrict,'question'=>$lickert_points->question);
				$this->db->insert('lickert_points',$new_lickert_points);
				$lickert_point_id[$lickert_points->point_id] = $this->db->insert_id();
				}
          }
		  	$this->db->select('*');
            $this->db->from('lickert_sub_groups');
            $this->db->join('lickert_points','lickert_sub_groups.point_id = lickert_points.point_id','LEFT');
            $this->db->where('lickert_points.district_id',$olddistrict);
            $lickert_sub_groups_data = $this->db->get();
            $result_lickert = $lickert_sub_groups_data->result();
			
            foreach($result_lickert as $result_lickert_point){
					if($lickert_point_id[$result_lickert_point->point_id]!=''){
            $lickert_aarr = array('point_id'=>$lickert_point_id[$result_lickert_point->point_id],'sub_group_name'=>$result_lickert_point->sub_group_name);
			  
			$this->db->insert('lickert_sub_groups',$lickert_aarr);
					}
			//echo $this->db->last_query();
              }
			
		return true;
	      }	

function leadership_rubric_standards($newdistrict,$olddistrict){
			$query = $this->db->get_where('lubric_groups',array('district_id'=>$olddistrict));
            $lubric_group = $query->result();
		    foreach($lubric_group as $lubric_groups){
			$new_lubric_groups = array('group_name'=>$lubric_groups->group_name,'district_id'=>$newdistrict,'description'=>$lubric_groups->description);
					//$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('lubric_groups',$new_lubric_groups);
			$lub_groups_id[$lubric_groups->group_id] = $this->db->insert_id();
              }
			
			$query = $this->db->get_where('lubric_points',array('district_id'=>$olddistrict));
            $lub_point = $query->result();
		    foreach($lub_point as $lub_points){
				$new_prof_points = array('group_type_id'=>$lub_points->group_type_id,'ques_type_id'=>$lub_points->ques_type_id,'group_id'=>$lub_groups_id[$lub_points->group_id],'district_id'=>$newdistrict,'question'=>$lub_points->question);
				$this->db->insert('lubric_points',$new_prof_points);
				$lub_point_id[$lub_points->point_id] = $this->db->insert_id();
          }
			$this->db->select('*');
            $this->db->from('lubric_sub_groups');
            $this->db->join('lubric_points','lubric_sub_groups.point_id = lubric_points.point_id','LEFT');
            $this->db->where('lubric_points.district_id',$olddistrict);
            $lub_sub_group_data = $this->db->get();
            $result_lub = $lub_sub_group_data->result();
//            echo $this->db->last_query();exit;
//            print_r($resultstandarddatas);exit;
            foreach($result_lub as $result_lub_group){
               $prof_aarr = array('point_id'=>$lub_point_id[$result_lub_group->point_id],'sub_group_name'=>$result_lub_group->sub_group_name,'sub_group_text'=>$result_lub_group->sub_group_text);
			  	 //print_r($prof_aarr);
				 $this->db->insert('lubric_sub_groups',$prof_aarr);
                //echo $this->db->last_query();
              }
		
            return true;
	
        }
	


}