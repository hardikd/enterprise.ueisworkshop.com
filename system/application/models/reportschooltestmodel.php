<?php
class Reportschooltestmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getschoolbydistrict($district_id)
	{
		
	$qry="Select * from schools where district_id='".$district_id."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getschoolgrades($district_id)
	{
		
	$qry="Select * from dist_grades where district_id='".$district_id."'";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getschooltestname($district_id)
	{
		
	$qry="Select * from  dist_subjects where district_id='".$district_id."'";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	/*function getscoretype($district_id)
	{
		
	$qry="Select * from score where district_id='".$district_id."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}*/

	function getschooltype($district_id)
	{
		
	$qry="Select * from  school_type where district_id='".$district_id."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getallschoolsbytype($schooltypeid)
	{
		 $districtid = $this->session->userdata('district_id');
		
			$qry="Select * from  schools where  school_type_id='".$schooltypeid."' and district_id='".$districtid."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	
		function getallschoolscore($testname)
			{
			
				$qry="select * from  user_quizzes where  assignment_id	='".$testname."' ";
				$query = $this->db->query($qry);
				if($query->num_rows()>0)
				{
					return $query->result_array();
				}
					else
					{
						return false;
					}
				
			}
	
function getallusers($school_id,$gradeid)
	{
	
/*		  echo  $qry="Select * from  users where  school_id='".$school_id."' and grade_id='".$gradeid."' ";
			
			$query = $this->db->query($qry);
			 $query =  $query->result_array();
			 
			 return  array_values(array_filter($query));*/
		
 
		$temp=array();
		for($i=0;$i<count($school_id);$i++)
			{
			    $qry="Select * from  users where  school_id='".$school_id[$i]."' and grade_id='".$gradeid."' ";
			
			$query = $this->db->query($qry);
			$temp[] =  $query->result_array();
			}
			
			$temp = array_values(array_filter($temp));
			
			return $temp;	 	
			
		
	}
	
	function gettestnamebycat($catid,$seriesfrom,$seriesto)
	{
		

/* $qry="Select assignments.id,assignments.assignment_name from  assignments,quizzes  where quizzes.cat_id='".$catid."' and assignments.quiz_id = quizzes.id and  assignments.createtestdate between '".$seriesfrom."' and '".$seriesto."' ";*/

 $qry="Select assignments.id,assignments.assignment_name from  assignments,quizzes  where quizzes.cat_id='".$catid."' and assignments.quiz_id = quizzes.id and  assignments.createtestdate between '".$seriesfrom."' and '".$seriesto."' ";
				
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){

	return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
		function gettestname()
	{
		
	$qry="Select id,assignment_name from  assignments";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getfinaldata($ssid,$assignmentid,$from,$to,$testcluster)
	{
		$temp=array();
	   
   
$qry = "select avg(pass_score_point) as schoolaverage,test_cluster from user_quizzes,users ,questions where user_quizzes.assignment_id='$assignmentid' and users.school_id='$ssid' and  users.UserID = user_quizzes.user_id and user_quizzes.date_year between '".$from."' and '".$to."' and questions.test_cluster='".$testcluster."' order by pass_score_point limit 1 ";	

	
	
	$query = $this->db->query($qry);
	$temp[] =  $query->result_array();
	$count=0;

	foreach($temp as $h=>$v)
	{
		if(is_array($v))
		{
			foreach($v as $j=>$ji)
			{
				if(is_array($ji))
				{
					foreach($ji as $j2=>$ji2)
					{
						if(empty($ji2))
						{
							$count++;
						}
				
				
					}
				}
				else
				{
						if(empty($ji))
						{
							$count++;
						}
						
				}
			}
		}
		else
		{
			if(empty($v))
				{
					$count++;
				}
				
		}
	}
	
	$temp[] =  $ssid;
	
	 $qry2 = "select * from schools where school_id='$ssid'";	
	
	

	$query2 = $this->db->query($qry2);
	$temp[] =  $query2->result_array();
	return $temp;
	
	
		
	}
	
	function getschoolregardinguser($userid)
	{
		$temp=array();
		for($i=0;$i<count($userid);$i++)
		{
			
	$qry="Select * from  users where  UserID='".$userid[$i]."'";
				$query = $this->db->query($qry);
				
					$temp[] = $query->result_array();
				
		}
		return $temp;
	}
	
	
	function getfinaldataMultiple($ssid,$assignmentid,$from,$to)
	{	
		

		$temp=array();
	
	
	$qry = "select avg(pass_score_point) as schoolaverage,assignment_id   from user_quizzes,users where user_quizzes.assignment_id='$assignmentid' and users.school_id='$ssid' and  users.UserID = user_quizzes.user_id
  and user_quizzes.date_year between '".$from."' and '".$to."' order by pass_score_point ASC limit 5 ";	

		$query = $this->db->query($qry);
		
		
		$temp[] =  $query->result_array();

	
		$temp[] =  $ssid;
	
		 $qry2 = "select * from schools where school_id='$ssid'";	
		
		$query2 = $this->db->query($qry2);
		$temp[] =  $query2->result_array();
		
		return $temp;
		
		
	} 
	
	function getobdetail($obid)
	{
	$qry="Select * from observers where observer_id='".$obid."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
		function getCategory()
	{
		
		$district_id = $this->session->userdata('district_id');
		
		$qry="Select * from cats where district_id='".$district_id."'";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function checkschools($schooltypeid,$schoolid)
	{
	$qry="Select * from schools where school_type_id='".$schooltypeid."' and school_id='".$schoolid."'";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
		
		// add new method
		
	function getallusersob($school_id,$gradeid)
		{
		
			 $qry="Select * from  users where  school_id='".$school_id."' and grade_id='".$gradeid."' ";
			
			$query = $this->db->query($qry);
			return  $query->result_array();
		
		}
		
	// ob new method
	
	function getfinaldataob($ssid,$assignmentid,$from,$to)
	{
		$result=array();
	
	$str = "select * from assignments where quiz_id=".$assignmentid."";
		
		$d =  mysql_query($str);
		
		$rowdata = mysql_fetch_array($d);
		//print_r($rowdata);
		
		$quiz_id = $rowdata['id'];	
 $qry = "select avg(pass_score_point) as schoolaverage   from user_quizzes,users where user_quizzes.assignment_id='$assignmentid' and users.school_id='$ssid' and  users.UserID = user_quizzes.user_id
  and user_quizzes.date_year between '".$from."' and '".$to."' order by pass_score_perc limit 5 ";	


	
	$query = $this->db->query($qry);
	$result[] =  $query->result_array();
	
	$result[] =  $ssid;
	
	 $qry2 = "select * from schools where school_id='$ssid'";	
	
	

	$query2 = $this->db->query($qry2);
	$result[] =  $query2->result_array();
	return $result;



		
	}
	
	// add observer multiple
	
		function getfinaldataMultipleob($ssid,$assignmentid,$from,$to)
	{		
	
	
		$temp=array();		

	
	  $qry = "select avg(pass_score_point) as schoolaverage,assignment_id   from user_quizzes,users where user_quizzes.assignment_id='$assignmentid' and users.school_id='$ssid' and  users.UserID = user_quizzes.user_id
  and user_quizzes.date_year between '".$from."' and '".$to."' order by pass_score_perc limit 5 ";	
	

		$query = $this->db->query($qry);
		$temp[] =  $query->result_array();
		
		$count=0;
	
	foreach($temp as $h=>$v)
	{
		if(is_array($v))
		{
			foreach($v as $j=>$ji)
			{
				if(is_array($ji))
				{
					foreach($ji as $j2=>$ji2)
					{
						if(empty($ji2))
						{
							$count++;
						}
				
				
					}
				}
				else
				{
						if(empty($ji))
						{
							$count++;
						}
						
				}
			}
		}
		else
		{
			if(empty($v))
				{
					$count++;
				}
				
		}
	}
		
		
		
		
		$temp[] =  $ssid;
	
		 $qry2 = "select * from schools where school_id='$ssid'";	
		
		$query2 = $this->db->query($qry2);
		$temp[] =  $query2->result_array();
		
		
		return $temp;
		
		
		
	} 
	
	function getschoolbyteacher($teacherid)
	{
	$qry="Select * from teachers where teacher_id='".$teacherid."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	// teacher multiple
	
	
		function getfinaldataMultipleteacher($ssid,$assignmentid,$from,$to)
	{		
	

	
		$temp=array();	
		
					
	
 $qry = "select avg(pass_score_point) as schoolaverage, assignment_id   from user_quizzes,users where user_quizzes.assignment_id='$assignmentid' and users.school_id='$ssid' and  users.UserID = user_quizzes.user_id
  and user_quizzes.date_year between '".$from."' and '".$to."' order by pass_score_perc limit 5 ";	
	

		$query = $this->db->query($qry);
		
		
		
		$temp[] =  $query->result_array();
		
		$count=0;
	
	foreach($temp as $h=>$v)
	{
		if(is_array($v))
		{
			foreach($v as $j=>$ji)
			{
				if(is_array($ji))
				{
					foreach($ji as $j2=>$ji2)
					{
						if(empty($ji2))
						{
							$count++;
						}
				
				
					}
				}
				else
				{
						if(empty($ji))
						{
							$count++;
						}
						
				}
			}
		}
		else
		{
			if(empty($v))
				{
					$count++;
				}
				
		}
	}
		
		
		
		
		$temp[] =  $ssid;
	
		 $qry2 = "select * from schools where school_id='$ssid'";	
		
		$query2 = $this->db->query($qry2);
		$temp[] =  $query2->result_array();
		
		
		return $temp;
	
		
		
		
	} 
	
	function getclusterbytestid($testid)
	{
		 $qry="Select org_quiz_id from assignments where id='".$testid."' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$data =  $query->result_array();	
						
			 $sql="SELECT distinct test_cluster FROM questions WHERE `quiz_id`='".$data[0]['org_quiz_id']."' ";
			$q = $this->db->query($sql);
			return  $final  =  $q->result_array();
			 
			
		}else{
			return false;
		}
	}
	
	function getproficiency($assessmentid)
	{
	$qry="Select * from proficiency where assessment_id='".$assessmentid."' ";
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
				return $query->result_array();		
		
		}else{
			return false;
		}	
	}
	
	
}
