<?php
class Lubricpointmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getlubricpointCount($state_id,$country_id,$group_id,$district_id)
	{
	     if($group_id=='all' && $district_id!='all')
		{
			$qry="Select count(*) as count from lubric_points op,lubric_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=op.district_id and  op.group_id=og.group_id and og.district_id=$district_id and  op.district_id=$district_id " ;
		}
		else if($group_id!='all' && $district_id!='all')
		{
			$qry="Select count(*) as count from lubric_points op,lubric_groups og ,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=op.district_id and op.group_id=og.group_id and og.district_id=$district_id and  op.group_id=$group_id  and op.district_id=$district_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getlubricpoints($page,$per_page,$state_id,$country_id,$group_id,$district_id)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		
		 if($group_id=='all' && $district_id!='all')
		{
			$qry="Select s.name,d.districts_name as districts_name ,op.point_id point_id,og.group_name as group_name,op.question question,q.ques_type ques_type from lubric_groups og,lubric_points op,ques_types q,districts d,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and  op.ques_type_id=q.ques_type_id and   op.group_id=og.group_id and og.district_id=$district_id and op.district_id=$district_id and op.district_id=d.district_id $limit ";
		
		}
		else if($group_id!='all' && $district_id!='all')
		{
			$qry="Select s.name,d.districts_name as districts_name ,op.point_id point_id,og.group_name as group_name,op.question question,q.ques_type ques_type from lubric_groups og,lubric_points op,ques_types q,districts d ,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and   op.ques_type_id=q.ques_type_id and   op.group_id=og.group_id and og.district_id=$district_id and op.group_id=$group_id and op.district_id=$district_id and op.district_id=d.district_id $limit ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
		
	function getlubricpointById($point_id)
	{
		
		$qry = "Select d.country_id,d.state_id,op.point_id,op.group_id,op.ques_type_id,op.question,op.group_type_id,op.district_id from lubric_points  op,districts d where op.district_id=d.district_id and op.point_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function getAllsubgroups($point_id)
	{
		$qry = "Select sub_group_id,point_id,sub_group_name,sub_group_text from lubric_sub_groups where point_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	
	
	
	function add_lubricpoint()
	{
	  $data = array('group_type_id' => $this->input->post('group_type_id'),
					'ques_type_id' => $this->input->post('ques_type_id'),
					'group_id' => $this->input->post('group_id'),
					'question' => $this->input->post('question'),
					'district_id' => $this->input->post('district_id')
		
		
		);
	   try{
			$str = $this->db->insert_string('lubric_points', $data);
			if($this->db->query($str))
			{
				if($this->input->post('group_type_id')==2)
				{
				   $last_id=$this->db->insert_id();
				   $c=$this->input->post('countergroup');
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!='' && $this->input->post('namebox'.$i)!='' )
					 {
					 $sdata=array('point_id' =>$last_id,
					 'sub_group_name'=>$this->input->post('textbox'.$i),
					  'sub_group_text'=>$this->input->post('namebox'.$i),	
					 );
					
					 $sstr = $this->db->insert_string('lubric_sub_groups', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return $last_id;
				}
				else
				{
					return $this->db->insert_id();
				}
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_lubricpoint()
	{
	
		
		 $point_id=$this->input->post('point_id');
		
	$data = array('group_type_id' => $this->input->post('group_type_id'),
					'ques_type_id' => $this->input->post('ques_type_id'),
					'group_id' => $this->input->post('group_id'),
					'question' => $this->input->post('question'),
					'district_id' => $this->input->post('district_id')
		
		
		);
		
			
		$where = "point_id=".$point_id;		
		
		try{
			$str = $this->db->update_string('lubric_points', $data,$where);
			if($this->db->query($str))
			{
				if($this->input->post('group_type_id')==2)
				{
				   $dqry = "delete from lubric_sub_groups where point_id=$point_id";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('countergroup');
				   //exit;
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!=''  && $this->input->post('textboxup'.$i)!='' && $this->input->post('namebox'.$i)!='' )
					 {
					    $sub_group_id=$this->input->post('textboxup'.$i);
						
						$sdata=array('sub_group_name'=>$this->input->post('textbox'.$i),
									'sub_group_text'=>$this->input->post('namebox'.$i),
						             'point_id'=>$point_id,
									 'sub_group_id'=>$sub_group_id
						
						);
                   					 
					   //$where = "point_id=$point_id and sub_group_id=$sub_group_id";		
		
		
			                $str = $this->db->insert_string('lubric_sub_groups', $sdata);
					          $this->db->query($str);
					 
					 }
					  else if($this->input->post('textbox'.$i)!='' && $this->input->post('namebox'.$i)!='' )
					 {
					 $sdata=array('point_id' =>$point_id,
					 'sub_group_name'=>$this->input->post('textbox'.$i),
					  'sub_group_text'=>$this->input->post('namebox'.$i)				   	
					 );
					 
					 $sstr = $this->db->insert_string('lubric_sub_groups', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return true;
				}
				else
				{
					$dqry = "delete from lubric_sub_groups where point_id=$point_id";
		           $dquery = $this->db->query($dqry);
					return true;
				}
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletelubricpoint($ob_id)
	{
		$qry = "delete from lubric_points where point_id=$ob_id";
		$query = $this->db->query($qry);
		$qry = "delete from lubric_sub_groups where point_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
	function getAllquestions()
	{
	
	 $qry = "Select ques_type_id,ques_type from ques_types ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getAllgrouptypes()
	{
	
	 $qry = "Select group_type_id,group_type from group_types ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getAllPoints($group_id,$district_id)
	{
		$qry = "Select osg.sub_group_id,osg.sub_group_name,osg.sub_group_text,op.point_id ,qt.ques_type,op.question,op.group_type_id from ques_types qt,lubric_points op left join lubric_sub_groups osg on  osg.point_id=op.point_id  where op.ques_type_id=qt.ques_type_id and op.group_id=$group_id and op.district_id=$district_id " ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function savereport()
	{
	  $status=true;
	  $group_id=$this->input->post('group_id');
	  
	  $report_id=$this->session->userdata('new_report_id');
	  if(!empty($group_id))
	  {
	   $points=$this->getAllPoints($group_id,$this->session->userdata("district_id"));
	  // echo "<pre>";
	  // print_r($points);
	   //exit;
	   if($points!=false)
	   {
	   foreach($points as $val)
	   {
	     
		 if($val['group_type_id']!=2)
		 {
		    $acpoint_id=$val['point_id'];
			if($this->input->post($val['point_id'])==1)
			{
			 $value=1;
			}
			else
			{
			 $value=0;
			}
			
			$dqry = "delete from leaderships where point_id=".$val['point_id']." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
		}
        else
		{
            
			if($val['ques_type']=='checkbox')
			{
				
			     $acpoint_id=$val['point_id'];
				 $post_id=$val['point_id'].'_'.$val['sub_group_id'];
			     $value=$this->input->post($post_id);
				 $dqry = "delete from leaderships where point_id=".$val['point_id']." and report_id=$report_id and response='".$val['sub_group_id']."'";
		        $dquery = $this->db->query($dqry);
				 
			    	
			
			}
			else
			{
			  $acpoint_id=$val['point_id'];
				$value=$this->input->post($val['point_id']);
				$dqry = "delete from leaderships where point_id=".$val['point_id']." and report_id=$report_id";
		       $dquery = $this->db->query($dqry);
			
			}
				

		}	
			
			//$dqry = "delete from lubrics where point_id=".$val['point_id']." and report_id=$report_id";
		    //$dquery = $this->db->query($dqry);
			
			if($value!=0)
			{
			$data =array('report_id' => $report_id,
					'point_id' => $acpoint_id,
					'response' => $value
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('leaderships', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		  
		 }
		 
	   
	   }
	   }
	   
	   
	   $dqry = "delete from leaderships where group_id=".$group_id." and report_id=$report_id";
		$dquery = $this->db->query($dqry);
	   if($this->input->post('response-text'))
	   {
	     
	    
		$data =array('report_id' => $report_id,
					 'group_id' => $group_id,
					 'responsetext'=>$this->input->post('response-text')
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('leaderships', $data);
			
			$this->db->query($str);
	   
	   }
	  
	  }
	 
	   return $status;
	
	}
	function selfsavereport()
	{
	  $status=true;
	  $group_id=$this->input->post('group_id');
	  
	  $report_id=$this->session->userdata('new_report_id');
	  if(!empty($group_id))
	  {
	   $points=$this->getAllPoints($group_id,$this->session->userdata("district_id"));
	  // echo "<pre>";
	  // print_r($points);
	   //exit;
	   if($points!=false)
	   {
	   foreach($points as $val)
	   {
	     
		 if($val['group_type_id']!=2)
		 {
		    $acpoint_id=$val['point_id'];
			if($this->input->post($val['point_id'])==1)
			{
			 $value=1;
			}
			else
			{
			 $value=0;
			}
			
			$dqry = "delete from leadershipsself where point_id=".$val['point_id']." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
		}
        else
		{
            
			if($val['ques_type']=='checkbox')
			{
				
			     $acpoint_id=$val['point_id'];
				 $post_id=$val['point_id'].'_'.$val['sub_group_id'];
			     $value=$this->input->post($post_id);
				 $dqry = "delete from leadershipsself where point_id=".$val['point_id']." and report_id=$report_id and response='".$val['sub_group_id']."'";
		        $dquery = $this->db->query($dqry);
				 
			    	
			
			}
			else
			{
			  $acpoint_id=$val['point_id'];
				$value=$this->input->post($val['point_id']);
				$dqry = "delete from leadershipsself where point_id=".$val['point_id']." and report_id=$report_id";
		       $dquery = $this->db->query($dqry);
			
			}
				

		}	
			
			//$dqry = "delete from lubrics where point_id=".$val['point_id']." and report_id=$report_id";
		    //$dquery = $this->db->query($dqry);
			
			if($value!=0)
			{
			$data =array('report_id' => $report_id,
					'point_id' => $acpoint_id,
					'response' => $value
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('leadershipsself', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		  
		 }
		 
	   
	   }
	   }
	   
	   
	   $dqry = "delete from leadershipsself where group_id=".$group_id." and report_id=$report_id";
		$dquery = $this->db->query($dqry);
	   if($this->input->post('response-text'))
	   {
	     
	    
		$data =array('report_id' => $report_id,
					 'group_id' => $group_id,
					 'responsetext'=>$this->input->post('response-text')
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('leadershipsself', $data);
			
			$this->db->query($str);
	   
	   }
	  
	  }
	 
	   return $status;
	
	}
	function getAllGroupPoints()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select g.group_id,g.group_name,g.description ,osg.sub_group_id,osg.sub_group_name,osg.sub_group_text,op.point_id ,qt.ques_type,op.question,op.group_type_id from ques_types qt,lubric_groups g,lubric_points op left join lubric_sub_groups osg on  osg.point_id=op.point_id  where op.group_id=g.group_id and op.district_id=$district_id  and g.district_id=$district_id and op.ques_type_id=qt.ques_type_id order by g.group_id,op.point_id,osg.sub_group_id  " ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getReportPoints($report_id)
	{
		$qry = "Select group_id,responsetext,report_id,point_id,response from leaderships where report_id=".$report_id ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getReportselfPoints($report_id)
	{
		$qry = "Select group_id,responsetext,report_id,point_id,response from leadershipsself where report_id=".$report_id ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getReportselfPointsbyobserverid($observer_id)
	{
		$qry = "Select l.group_id,l.responsetext,l.report_id,l.point_id,l.response from leadershipsself l,leadershipreportsself lr where lr.observer_id=$observer_id and lr.report_id=l.report_id  and lr.completed=1"  ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllqualitative($group_id,$report)
	{
		$from=$this->session->userdata('report_from');
		$to=$this->session->userdata('report_to');
		
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		 
		  $qry = "select ob.responsetext,r.report_date from leaderships ob,leadershipreports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.school_id=$school_id and r.district_id=$district_id  and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllReport($report)
	{
	
	    
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		
		  $qry = "select report_id from leadershipreports r where r.observer_id=$observer_id and r.school_id=$school_id  and r.district_id=$district_id " ;
		
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getAllsectional($group_id,$report)
	{
		
		
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  
		  $qry = "select b.report_id,b.point_id,b.response from leadershipreports r,lubric_groups g,lubric_points ob left join leaderships b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) && isset($pdata['group_id']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getlubricpointById($val);
			 
			  
			  if($data!=false && $data[0]['group_id']!=$pdata['group_id'])
			  {
					$status=1;	
				   $sdata = array('group_type_id' => $data[0]['group_type_id'],
					'ques_type_id' => $data[0]['ques_type_id'],
					'group_id' => $pdata['group_id'],
					'question' => $data[0]['question'],
					'district_id' => $pdata['end']
		
		
					);
					$str = $this->db->insert_string('lubric_points', $sdata);
					$this->db->query($str);
					$last_id=$this->db->insert_id();
					$subdata=$this->getAllsubgroups($val);
					if($subdata!=false)
					{
					  foreach($subdata as $val)
					  {
					  $subsavedata=array('point_id' =>$last_id,
					 'sub_group_name'=> $val['sub_group_name'],
					 'sub_group_text'=> $val['sub_group_text']
					 );
					
					 $sstr = $this->db->insert_string('lubric_sub_groups', $subsavedata);
					  $this->db->query($sstr);
					 }
					
					
					}
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
}