<?php
class Greetings_textmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getgreetings_textscount($district_id)
	{
			$qry="Select count(*) as count 
	  			from greeting_text  where district_id = $district_id " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getgreetings_texts($page,$per_page,$district_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		
		$qry="Select * from greeting_text where district_id=$district_id  limit $start, $per_page ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	function add_plan()
	{
	  $data = array('greeting_text' => $this->input->post('tab'),
	  'report' => $this->input->post('report'),
	  'size' => $this->input->post('size'),
	  'district_id'=> $this->input->post('district_id')
					);
	   try{
			$str = $this->db->insert_string('greeting_text', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function check_plan()
	{
	
	$district_id=$this->input->post('district_id');
	$report=$this->input->post('report');
	$qry="Select * from greeting_text where district_id=$district_id and report=$report ";
	
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}
	}
	function update_check_plan()
	{
	
	$district_id=$this->input->post('district_id');
	$plan_id=$this->input->post('plan_id');
	$report=$this->input->post('report');
	$qry="Select * from greeting_text where district_id=$district_id and report=$report and greeting_text_id!=$plan_id ";
	
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 1;
		}else{
			return 0;
		}
	
	}
	function update_plan()
	{
		
		$plan_id=$this->input->post('plan_id');
		$data = array('greeting_text' => $this->input->post('tab'),
	  'report' => $this->input->post('report'),
	  'size' => $this->input->post('size'),
		'district_id'=> $this->input->post('district_id')
					);
			
	 	$where = "greeting_text_id=".$plan_id;		
		
		try{
			$str = $this->db->update_string('greeting_text', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
 	
	}
 	
	function deleteplan($plan_id)
	{
		$qry = "delete from greeting_text where greeting_text_id=$plan_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getplanById($plan_id)
	{
	    $qry = "Select * from greeting_text where greeting_text_id=".$plan_id;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getallplans($report)
	{
	 
		$district_id=$this->session->userdata('district_id');
	 
	  $qry = "Select * from greeting_text where district_id=$district_id and report=$report";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
	
}