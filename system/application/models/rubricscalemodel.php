<?php
class Rubricscalemodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getrubricscaleCount($state_id,$country_id,$district_id)
	{
		if($district_id=='all')
		{
			$qry="Select count(*) as count 
	  			from rubricscale og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id " ;
		}
		else
		{
		
			$qry="Select count(*) as count 
	  			from rubricscale og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id and  og.district_id=$district_id " ;
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getrubricscales($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		if($district_id=='all')
		{
			$qry="Select s.name,og.scale_id,og.scale_name,d.districts_name from rubricscale og,districts d,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id $limit ";
			
		}
		else
		{
			$qry="Select s.name,og.scale_id,og.scale_name,d.districts_name from rubricscale og,districts d ,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id and og.district_id=$district_id $limit ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getrubricscaleById($val);
			 
			  
			  if($data!=false && $data[0]['district_id']!=$pdata['end'])
			  {
					$status=1;	
				   $sdata = array('scale_name' => $data[0]['scale_name'],
					'description' => $data[0]['description'],
					'district_id' => $pdata['end']	
					);
					$str = $this->db->insert_string('rubricscale', $sdata);
					$this->db->query($str);
					
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	function copyrubricscale($from,$to)
	{
	  $groups=$this->getallrubricscalesbydistrictIddesc($from);
	  if($groups!=false)
	  {
	    foreach($groups as $groupval)
			{
			  
					
				   $sdata = array('scale_name' => $groupval['scale_name'],
					'description' => $groupval['description'],
					'district_id' => $to	
					);
					$str = $this->db->insert_string('rubricscale', $sdata);
					$this->db->query($str);
					$last_group_id=$this->db->insert_id();
					$points=$this->getAllsubs($groupval['scale_id'],$from);
					if($points!=false)
					{
					
					foreach($points as $pointval)
					{
					  $sdata = array('sub_scale_name' => $pointval['sub_scale_name'],
					'scale_id' => $last_group_id,
					'district_id' => $to	
					);
					$str = $this->db->insert_string('rubricscale_sub', $sdata);
					$this->db->query($str);
					
					
					}
					
					
					}
			  
			  
			
			}
	  
	  
	  
	  
	  }
	
	
	
	}
	
	function getAllsubs($group_id,$dist_id)
	{
	  $qry = "select rs.sub_scale_id,rs.scale_id,rs.sub_scale_name,rs.district_id,d.country_id,d.state_id from rubricscale_sub rs,districts d where rs.district_id=d.district_id and rs.scale_id=$group_id and rs.district_id=$dist_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getrubricscaleById($ob_id)
	{
		
		$qry = "Select og.scale_id,og.scale_name,og.description,og.district_id,d.state_id,d.country_id from rubricscale og,districts d where og.district_id=d.district_id and  og.scale_id=".$ob_id;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function add_rubricscale()
	{
	  $data = array('scale_name' => $this->input->post('scale_name'),
					'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')
		
		
		);
	   try{
			$str = $this->db->insert_string('rubricscale', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_rubricscale()
	{
	
		
		$scale_id=$this->input->post('scale_id');
	$data = array('scale_name' => $this->input->post('scale_name'),
				   'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')	
		
		
		);
		
			
		$where = "scale_id=".$scale_id;		
		
		try{
			$str = $this->db->update_string('rubricscale', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleterubricscale($ob_id)
	{
		$qry = "delete from rubricscale where scale_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getallrubricscales()
	{
	
	 $qry = "Select scale_id,scale_name from rubricscale order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallrubricscalesbydistrictId($district_id)
	{
	
	 $qry = "Select scale_id,scale_name from rubricscale where district_id=$district_id order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallrubricscalesbydistrictIddesc($district_id)
	{
	
	 $qry = "Select scale_id,scale_name,description from rubricscale where district_id=$district_id order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallrubricsubscales($district_id)
	{
	
	 
	 $qry = " Select r.scale_id,r.scale_name,r.description,rs.sub_scale_name,rs.sub_scale_id,rs.district_id from rubricscale r left join rubricscale_sub rs on r.scale_id=rs.scale_id where r.district_id=$district_id order by r.scale_id,rs.sub_scale_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row=$query->result_array();
			foreach($row as $val)
			{
				if($val['sub_scale_id']!=null)
				{
				 if($val['district_id']==$district_id)
				 {
				  	$srow[]=$val;
				}	
			    }
				else
				{
				 $srow[]=$val;
				
				}
			
			}
			
			return $srow;			
		}else{
			return false;
		}
	
	
	}
	function getallrubricsubscalesform($district_id)
	{

	 $qry = " Select r.scale_id,r.scale_name,rs.sub_scale_name,rs.sub_scale_id,rs.district_id from rubricscale r left join rubricscale_sub rs on r.scale_id=rs.scale_id  where r.district_id=$district_id order by r.scale_id,rs.sub_scale_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			$row=$query->result_array();
			foreach($row as $val)
			{
				if($val['sub_scale_id']!=null)
				{
				 if($val['district_id']==$district_id)
				 {
				  	$srow[]=$val;
				}	
			    }
				else
				{
				 $srow[]=$val;
				
				}
			
			}
			
			return $srow;		
		}else{
			return false;
		}
	
	
	}
	function getallrubricsubscalesformpoints($report_id)
	{
	
	$qry = "  Select r.group_id,r.point_id,r.strengths,r.concerns,r.score from rubrics r where   r.report_id=$report_id ";
	 
	 
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallpoints($group_id,$sub_scale_id)
	{
	 $report_id=$this->session->userdata('new_report_id');
	 if($sub_scale_id!=0)
	 {
	   $qry = " Select r.group_id,r.point_id,r.strengths,r.concerns,r.score from rubrics r where  r.group_id=$group_id and r.point_id=$sub_scale_id and r.report_id=$report_id ";
	 }
	 else
	 {
	  $qry = "  Select r.group_id,r.point_id,r.strengths,r.concerns,r.score from rubrics r where  r.group_id=$group_id and r.report_id=$report_id ";
	 
	 }
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function savereport()
	{
	  $status=true;
	  $group_id=$this->input->post('group_id');
	  
	  
	  
	  $report_id=$this->session->userdata('new_report_id');
	$sub_scale_id=$this->input->post('sub_scale_id');
	
	  
	  
	  if(!empty($group_id))
	  {
	    if($sub_scale_id!=0)
		{
			$dqry = "delete from rubrics where point_id=".$sub_scale_id." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
			$data =array('report_id' => $report_id,
					'point_id' => $sub_scale_id,
					'strengths' => $this->input->post('strengths'),
					'concerns'  => $this->input->post('concerns'),
					'score'=> $this->input->post('score'),
					'group_id'=> $this->input->post('group_id')
					);
			
			$str = $this->db->insert_string('rubrics', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		
		
		
		
		}
		else
		{
		
			$dqry = "delete from rubrics where group_id=".$group_id." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
			$data =array('report_id' => $report_id,
					'strengths' => $this->input->post('strengths'),
					'concerns'  => $this->input->post('concerns'),
					'score'=> $this->input->post('score'),
					'group_id'=> $this->input->post('group_id')
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('rubrics', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		
		
		
		}
	   
	   
	  
	  
	  }
	 
	   return $status;
	
	}
	function getAllqualitative($group_id,$sub_id,$report)
	{
		$from=$this->session->userdata('report_from');
		$to=$this->session->userdata('report_to');
		$form=$this->session->userdata('reportform');
		$school_id=$this->session->userdata('school_id');
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		
			if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.school_id=$school_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
		    }
			else
			{
				 $qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.school_id=$school_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.point_id=$sub_id " ;	
			}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		 if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.school_id=$school_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
		}
		else 
		{
			$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.school_id=$school_id and  r.report_form='$form' and  r.report_id=ob.report_id and    ob.point_id=$sub_id " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.school_id=$school_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
			}
		else  
		{
			$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.school_id=$school_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.point_id=$sub_id " ;
		}
		
		}
		if($report=='school')
		{
		  
			if($sub_id==0)
			{
		 $qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and  r.school_id=$school_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null " ;
		}
		else  
		{
			$qry = "select ob.strengths,ob.concerns,r.report_date from rubrics ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and  r.school_id=$school_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.point_id=$sub_id  " ;
		
		}
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getAllsectional($group_id,$sub_id,$report)
	{
	
		$school_id=$this->session->userdata('school_id');
		$formtypequery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		
			$yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		
			if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.teacher_id=$teacher_id and r.school_id=$school_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $yearquery $formtypequery " ;
		    }
			else
			{
				 $qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.teacher_id=$teacher_id and r.school_id=$school_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id $yearquery $formtypequery " ;
			}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		 if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.observer_id=$observer_id and r.school_id=$school_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $formtypequery" ;
		}
		else 
		{
			$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.observer_id=$observer_id and r.school_id=$school_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id  $formtypequery " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($sub_id==0)
			{
				$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.grade_id=$grade_id and r.school_id=$school_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $formtypequery  " ;
			}
		else  
		{
			$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where  r.grade_id=$grade_id and r.school_id=$school_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id  $formtypequery " ;
		}
		
		}
		if($report=='school')
		{
		  
			if($sub_id==0)
			{
		 $qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where   r.school_id=$school_id and  r.report_id=ob.report_id and  ob.group_id=$group_id and ob.point_id is null $formtypequery " ;
		}
		else  
		{
			$qry = "select ob.strengths,ob.concerns,ob.score,r.report_id from rubrics ob,reports r where   r.school_id=$school_id and  r.report_id=ob.report_id and  ob.point_id=$sub_id  $formtypequery " ;
		
		}
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
}