<?php
class Dist_subjectmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getdist_subjectCount($state_id,$country_id,$district_id)
	{
	
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from dist_subjects s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id ";
		}
		else
		{
				$qry="Select count(*) as count 
	  			from dist_subjects  s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and  s.district_id=$district_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getdist_subjects($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($district_id=='all')
		{
		$qry="Select s.eld,st.name as name,s.dist_subject_id as dist_subject_id,s.subject_name as subject_name,d.district_id as district_id,d.districts_name as districtsname from dist_subjects s,districts d,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id  limit $start, $per_page ";
		}
		else
		{
			$qry="Select s.eld,st.name as name,s.dist_subject_id as dist_subject_id,s.subject_name as subject_name,d.district_id as district_id,d.districts_name as districtsname from dist_subjects s,districts d ,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id and s.district_id=$district_id  limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getdist_subjectById($dist_id)
	{
		
		$qry = "Select s.eld,s.dist_subject_id,s.subject_name,d.state_id,s.district_id,d.country_id from dist_subjects s,districts d where s.district_id=d.district_id and s.dist_subject_id=".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getsubjectbyteacher($date,$teacher_id)
	{
		
		$qry = "Select s.dist_subject_id as subject_id,s.subject_name from dist_subjects s,districts d,lesson_week_plans l where s.district_id=d.district_id and l.date='$date' and l.teacher_id=$teacher_id and l.subject_id=s.dist_subject_id and l.diff_instruction!='' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getdist_subjectsById($dist=false)
	{
		if($dist==false)
		{
		$district_id=$this->session->userdata('district_id');
		}
		else
		{
			$district_id=$dist;
		
		}
		$qry = "Select s.dist_subject_id as subject_id ,s.subject_name from dist_subjects s,districts d where s.district_id=d.district_id and s.district_id=".$district_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function student_subjects($teacher_id,$student_id)
	{
	
		 $qry ="select distinct(t.subject_id) as subject_id,d.subject_name from time_table t ,teacher_students ts,dist_subjects d where t.class_room_id=ts.class_room_id and t.period_id=ts.period_id and t.current_date=ts.day and t.teacher_grade_subject_id=ts.teacher_id and t.subject_id=d.dist_subject_id and ts.teacher_id=$teacher_id and ts.student_id=$student_id ";
		
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	
	
	}
	function check_dist_subject_exists()
	{
	
		$name=$this->input->post('subject_name');
		$district_id=$this->input->post('district_id');
		$qry="SELECT  dist_subject_id from dist_subjects where subject_name='$name' and district_id=$district_id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	
	}
	function check_dist_subject_update() {
	
	
		$name=$this->input->post('subject_name');
		$id=$this->input->post('dist_subject_id');
		$district_id=$this->input->post('district_id');
		$qry="SELECT dist_subject_id from dist_subjects where subject_name='$name'  and district_id=$district_id and dist_subject_id!=$id ";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	}
	function check_dist_eld_exists()
	{
	
		if($this->input->post('eld')==1)
		{
		$district_id=$this->input->post('district_id');
		$qry="SELECT  dist_subject_id from dist_subjects where eld=1 and district_id=$district_id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	   }
	   else
	   {
		return 1;
	   
	   
	   }
	
	}
	function check_dist_eld_update() {
	
	
		if($this->input->post('eld')==1)
		{
		$id=$this->input->post('dist_subject_id');
		$district_id=$this->input->post('district_id');
		$qry="SELECT dist_subject_id from dist_subjects where eld=1  and district_id=$district_id and dist_subject_id!=$id ";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
		}else
	   {
		return 1;
	   
	   
	   }
	
	}
	function add_dist_subject()
	{
	 
	  $data = array('subject_name' => $this->input->post('subject_name'), 
					'district_id'=> $this->input->post('district_id'),
					'created'=>date('Y-m-d H:i:s'),
					'eld'=> $this->input->post('eld')
		
		
		);
	   try{
			$str = $this->db->insert_string('dist_subjects', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_dist_subject()
	{
	
		
		$dist_subject_id=$this->input->post('dist_subject_id');
		
		
			$data = array('subject_name' => $this->input->post('subject_name'), 
					'district_id'=> $this->input->post('district_id'),
					'eld'=> $this->input->post('eld')
		
		
		);
		
		
		
	
	
	
		
		
	
		
			
		$where = "dist_subject_id=".$dist_subject_id;		
		
		try{
			$str = $this->db->update_string('dist_subjects', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletedist_subject($dist_id)
	{
		$qry = "delete from dist_subjects where dist_subject_id=$dist_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
}