<?php
class Dist_grademodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getdist_gradeCount($state_id,$country_id,$district_id)
	{
	
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from dist_grades s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id ";
		}
		else
		{
				$qry="Select count(*) as count 
	  			from dist_grades  s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and  s.district_id=$district_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getdist_grades($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($district_id=='all')
		{
		$qry="Select st.name as name,s.dist_grade_id as dist_grade_id,s.grade_name as grade_name,d.district_id as district_id,d.districts_name as districtsname from dist_grades s,districts d,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id  limit $start, $per_page ";
		}
		else
		{
			$qry="Select st.name as name,s.dist_grade_id as dist_grade_id,s.grade_name as grade_name,d.district_id as district_id,d.districts_name as districtsname from dist_grades s,districts d ,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id and s.district_id=$district_id  limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getdist_gradeById($dist_id)
	{
		
		$qry = "Select s.dist_grade_id,s.grade_name,d.state_id,s.district_id,d.country_id from dist_grades s,districts d where s.district_id=d.district_id and s.dist_grade_id=".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getdist_gradesById()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select s.dist_grade_id as grade_id ,s.grade_name from dist_grades s,districts d where s.district_id=d.district_id and s.district_id=".$district_id;
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function check_dist_grade_exists()
	{
	
		$name=$this->input->post('grade_name');
		$district_id=$this->input->post('district_id');
		$qry="SELECT  dist_grade_id from dist_grades where grade_name='$name' and district_id=$district_id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	
	}
	function check_dist_grade_exists_name($name)
	{
	
		$district_id=$this->session->userdata('district_id');
		$qry="SELECT  dist_grade_id from dist_grades where grade_name='$name' and district_id=$district_id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->dist_grade_id;	
		}else{
			
		  return 0;
		
		}
	
	
	}
	function check_dist_grade_update() {
	
	
		$name=$this->input->post('grade_name');
		$id=$this->input->post('dist_grade_id');
		$district_id=$this->input->post('district_id');
		$qry="SELECT dist_grade_id from dist_grades where grade_name='$name'  and district_id=$district_id and dist_grade_id!=$id ";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	}
	
	
	function add_dist_grade()
	{
	 
	  $data = array('grade_name' => $this->input->post('grade_name'), 
					'district_id'=> $this->input->post('district_id'),
					'created'=>date('Y-m-d H:i:s')
		
		
		);
	   try{
			$str = $this->db->insert_string('dist_grades', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_dist_grade()
	{
	
		
		$dist_grade_id=$this->input->post('dist_grade_id');
		
		
			$data = array('grade_name' => $this->input->post('grade_name'), 
					'district_id'=> $this->input->post('district_id')
		
		
		);
		
		
		
	
	
	
		
		
	
		
			
		$where = "dist_grade_id=".$dist_grade_id;		
		
		try{
			$str = $this->db->update_string('dist_grades', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletedist_grade($dist_id)
	{
		$qry = "delete from dist_grades where dist_grade_id=$dist_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}

	function getGradeByTeacher($teacher_id=''){
    	if($this->session->userdata('teacher_id')){
    		$teacher_id = $this->session->userdata('teacher_id');
    	}

    	$this->db->Select('grade_subject_id');
    	$this->db->from('teacher_grade_subjects');
    	$this->db->where('teacher_id',$teacher_id);
    	$this->db->group_by('grade_subject_id');
    	$query = $this->db->get();
    	$result = $query->result_array();
    	foreach($result as $grade_subject){
    		$grade_subject_arr[] = 	$grade_subject['grade_subject_id'];
    	}

    	$subject_array = mysql_real_escape_string(implode(',', $grade_subject_arr));
    	if($subject_array!=''){
    		$this->db->select('*');
    		$this->db->from('grade_subjects');
    		$this->db->join('dist_grades','dist_grades.dist_grade_id=grade_subjects.grade_id');
    		$this->db->where_in('grade_subjects.grade_subject_id',$grade_subject_arr);
    		$this->db->group_by('dist_grade_id');
    		$querygrade = $this->db->get();
    		$resultgrades = $querygrade->result_array();
    	}

    	//print_r($subject_array);
    	if($resultgrades){
    		return $resultgrades;
    	} else {
    		return false;
    	}
    }

    function create_classrooms(){
//    	print_r($_REQUEST);exit;
    	foreach($_POST as $key=>$class){
    		if($class!='create'){
    		$keyarr = explode('_',$key);
    		$grade_id = $keyarr[1];
    		$school_id = $this->session->userdata('school_id');
    		$name = $class;
    		$data = array('name'=>$name,'school_id'=>$school_id,'grade_id'=>$grade_id,'created'=>date('Y-m-d H:i:s'));
    		$this->db->insert('class_rooms',$data);
    	//	echo $this->db->last_query();
    	}
    	}
    }
	
}