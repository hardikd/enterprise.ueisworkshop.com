<?php
class Schooldistrictreportmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
   
  function getallschools($district_id)
   {
	   $qry='select * from schools where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
     function getobserverschool($observer_id)
   {
	  $qry='select s.*,o.observer_id from schools s left join observers o on s.school_id = o.school_id where o.observer_id= '.$observer_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
 
   public   function getgrades()
  	 {
	  	$district_id = $this->session->userdata('district_id');
	 $qry='select * from  dist_grades where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   function getobservergrade($observer_id)
   {
	   $qry='select d.* from  dist_grades d left join schools s on d.district_id = s.district_id left join observers o on s.school_id = o.school_id  where o.observer_id="'.$observer_id.'"';
	   $query = $this->db->query($qry);
	   $array = array();
	   $array[0] = array("observer_id"=>$observer_id);
	  	$array[] = $query->result_array();
	   	if($query->num_rows()>0){
			return $array;
		}else{
			return false;
		}  
   }
   
    function getobservergradeheader($observer_id)
   {
	   $qry='select d.* from  dist_grades d left join schools s on d.district_id = s.district_id left join observers o on s.school_id = o.school_id  where o.observer_id="'.$observer_id.'"';
	   $query = $this->db->query($qry);
	   	if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }

   
   function getgradenamebyId($gradeid)
   {
 	 $qry = "SELECT grade_name FROM  dist_grades where dist_grade_id='".$gradeid."'";
	 	$query = $this->db->query($qry);
		$temp =$query->result_array();	
	    return $temp;
   }
 
 	function getassessment($school_id,$district_id)
   {
	   if($school_id == 0)
		{	
			$str = '';
			$data['records'] = $this->getallschools($district_id);
			foreach($data['records'] as $key => $value)
			{
		 		$str .= $value['school_id'].',';
				
			}
			$str = substr($str,0,-1);
			$and = "and u.school_id in (".$str.")";
		}
		else
		{
			$and = "and u.school_id=".$school_id ;
		}
		
  		$qry= "select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq, users u where a.id=uq.assignment_id and uq.user_id = u.UserId ".$and." Order by a.id DESC";  
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
   function getassessmentofObserver($school_id,$grade_id)
   {
	   if($grade_id == 0)
		{	
			$and ="and u.school_id=".$school_id;
	 	}
		else
		{
			$and = "and u.school_id=".$school_id." and u.grade_id =".$grade_id;
		}
		
  		$qry= "select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq, users u where a.id=uq.assignment_id and uq.user_id = u.UserId ".$and." Order by a.id DESC";  
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
 
 	function getTestItem($school_id,$district_id,$ass_id)
    {
		if($ass_id == 0)
		$andAss = '';
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;
		if($school_id == 0)
		{	
			$str = '';
			$data['records'] = $this->getallschools($district_id);
			foreach($data['records'] as $key => $value)
			{
		 		$str .= $value['school_id'].',';
				
			}
			$str = substr($str,0,-1);
			$and = "and u.school_id in (".$str.")";
		}
		else
		{
			$and = "and u.school_id=".$school_id ;
		}
	//	select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq, users u where a.id=uq.assignment_id and uq.user_id = u.UserId
	$qry= "SELECT Distinct test_cluster FROM `questions` where id in(select question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss.") and test_cluster!='' and district_id=".$district_id." group by test_cluster order by id";  
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 	
	 }
	
	function getPercentageAllByTestCluster($school_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id)
    {
		$andDate = " AND DATE(ua.added_date) between '".$fDate."' ANd '".$tDate."' ";
		if($ass_id == 0)
		$andAss = '';
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;

		if($school_id == 0)
		{	
			$str = '';
			$data['records'] = $this->getallschools($district_id);
			foreach($data['records'] as $key => $value)
			{
		 		$str .= $value['school_id'].',';
				
			}
			$str = substr($str,0,-1);
			$and = "and u.school_id in (".$str.")";
			$group_by = "UserId,question_id ";
			$group_by_radio = "UserId,ua.id";
		}
		else
		{
			$and = "and u.school_id=".$school_id ;
			$group_by = 'UserId,question_id';
			$group_by_radio = "UserId,ua.id";
		}
 		$qry_Primary= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.") and is_score_ans=0".$andDate."  group by ".$group_by; 

		$query = $this->db->query($qry_Primary);
  		 $sum1 = $query->num_rows(); 
		 
 		$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_score_ans=1".$andDate."  group by ".$group_by; 
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows();
		 
		 $qry_radio= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.") and is_score_ans=0".$andDate."  and is_radio_ans_correct IS NOT NULL group by ".$group_by_radio; 

		$query = $this->db->query($qry_radio);
  		 $sum3 = $query->num_rows()."<br>"; 
	
		
	 	$ToalQes = $sum1 + $sum2 + $sum3 ;
		
	 	$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_correct_ans=1".$andDate."  and is_score_ans=0 group by ".$group_by; 
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
		 
		 $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_correct_ans=1".$andDate."  and is_score_ans=1 group by ".$group_by; 
		$query = $this->db->query($qry_Sec_correct);
		$c_sum2 = $query->num_rows();
		
		 $qry_radio_correct= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_radio_ans_correct = 1 and is_score_ans=0".$andDate."  group by ".$group_by_radio; 
		$query = $this->db->query($qry_radio_correct);
		$c_sum3 = $query->num_rows();
	
	
		$Curr_Qes = $c_sum1 + $c_sum2+$c_sum3;
		
		if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =0;
		 	$per = number_format($per,2,'.','');
 		return $per;
	 }
	 
	
	function getPercentageByGradeAndTestCluster($school_id,$test_cluster,$grade_id,$district_id,$fDate,$tDate,$ass_id)
    {
		$andDate = " AND DATE(ua.added_date) between '".$fDate."' ANd '".$tDate."' ";
		if($ass_id == 0)
		$andAss = '';
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;

		if($school_id == 0)
		{	
			$str = '';
			$data['records'] = $this->getallschools($district_id);
			foreach($data['records'] as $key => $value)
			{
		 		$str .= $value['school_id'].',';
				
			}
			$str = substr($str,0,-1);
			$and = "and u.school_id in (".$str.")";
			$group_by = "UserId,question_id ";
			$group_by_radio = "UserId,ua.id";
		}
		else
		{
			$and = "and u.school_id=".$school_id ;
			$group_by = 'UserId,question_id';
			$group_by_radio = "UserId,ua.id";
		}
	 	$qry_Primary= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and u.grade_id =".$grade_id." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_score_ans=0".$andDate."  group by ".$group_by; 			
		
		$query = $this->db->query($qry_Primary);
  		 $sum1 = $query->num_rows(); 
		
		$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and u.grade_id =".$grade_id." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_score_ans=1".$andDate."  group by ".$group_by;
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows();
		 
		 $qry_radio= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and u.grade_id =".$grade_id." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_score_ans=0".$andDate." and is_radio_ans_correct IS NOT NULL group by ".$group_by_radio; 			
	 	$query = $this->db->query($qry_radio);
  		 $sum3 = $query->num_rows(); 
		
		 $ToalQes = $sum1 + $sum2 + $sum3;
		 
	$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and u.grade_id =".$grade_id." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_correct_ans=1 and is_score_ans=0".$andDate."  group by ".$group_by;
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
		 	 
 	 
		 $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and u.grade_id =".$grade_id." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_correct_ans=1 and is_score_ans=1".$andDate."  group by ".$group_by;
		$query = $this->db->query($qry_Sec_correct);
		 $c_sum2 = $query->num_rows();
		 
		 $qry_radio_correct= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$and."".$andAss." and u.grade_id =".$grade_id." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id." ) and is_radio_ans_correct = 1 and is_score_ans=0".$andDate." group by ".$group_by_radio;
		$query = $this->db->query($qry_radio_correct);
		 $c_sum3 = $query->num_rows();
		  
	  $Curr_Qes = $c_sum1 + $c_sum2 + $c_sum3;
	 	if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =0;
		 	$per = number_format($per,2,'.','');
			 
 		return $per;
	 }
	
	
   function getcschools($data)
   {
	 	  
 	 $qry = "SELECT * FROM  schools where school_id='".$data."'";
	
		$query = $this->db->query($qry);
		$temp =$query->result_array();	
				
	   
	   return $temp;
   }
   
	 
} // class end

?>