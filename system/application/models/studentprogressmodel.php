<?php
class Studentprogressmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
/*	function getstuddetail()
	{
		
		$qry="SELECT * FROM users where user_type=2";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		
		
		}*/
		
   function getcschools($data)
   {
	 	  

	 $qry="SELECT * FROM  schools where school_id='".$data."'";
	
		$query = $this->db->query($qry);
		$temp =$query->result_array();	
				
	   
	   return $temp;
   }
   
   function getGreeting_text($reportid,$dist_id)
   {
 	 $qry="SELECT * FROM  greeting_text where district_id=$dist_id and report = $reportid";
	
		$query = $this->db->query($qry);
		$temp =$query->result_array();	
				
	   
	   return $temp;
   }
   
   function getReport_disclaimer($reportid,$dist_id)
   { 
 	 $qry="SELECT * FROM  report_disclaimer where district_id=$dist_id and report = $reportid";
	
		$query = $this->db->query($qry);
		$temp =$query->result_array();	
				
	   
	   return $temp;
   }
     
   function getrecord_student($userid)
   {
	   $qry="SELECT * FROM users where UserID='".$userid."'";
	
		$query = $this->db->query($qry);
		return $query->result_array();	
		    
   }
   function getteacher_schoolandgrade($teacher_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM teachers where teacher_id='".$teacher_id."'";
 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		return $arr;
		    
   }
   function getobserver_schoolandgrade($observer_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM observers where observer_id='".$observer_id."'";

 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		return $arr;
   } 
   
   function getobserver_SchoolGrade($observer_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM observers where observer_id IN (".$observer_id.")";

 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		return $arr;
   }
  
   function student_update($data)
   {
	
	   $userid = $data['userid'];
	   $name = $data['student_name'];
	   $surname = $data['student_surname'];
	   $email = $data['student_email'];
	   
	    $data = array(
               'Name' => $name,
               'Surname' => $surname,
               'email' => $email
            );

		$this->db->where('UserID', $userid);
		$this->db->update('users', $data); 

	   
   }
   
   function getstuperformance($userid)
   {
	   $qry="SELECT * FROM user_quizzes where user_id='".$userid."' order by added_date Asc";
	   $query = $this->db->query($qry);
		return $query->result_array();	 
   }
   
    function getteacher_assessment($teacher_id,$district_id)
   {
	 $qry='select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq where a.id=uq.assignment_id and uq.user_id in (select Distinct u.UserID from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= '.$teacher_id.') and a.district_id = '.$district_id.' order by a.id DESC';

	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
   function getassessment($assess)
   {
	 $qry="SELECT * FROM assignments where id='".$assess."'";
	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
   function getstudassessment($stuid,$assessmentid)
   {
		 $qry="SELECT * FROM user_quizzes where assignment_id=".$assessmentid." and user_id =".$stuid; 
	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
   function getsubdetail($subid)
   {
	 $qry="SELECT * FROM  dist_subjects where dist_subject_id='".$subid."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   function getclusterinfo($a_id)
	{
		$qry="select count(q.id) as cnt, test_cluster from questions q , assignments a where a.quiz_id = q.quiz_id and a.id=".$a_id." and test_cluster!='' group by test_cluster order by q.id" ;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
	}
	function getquestionsdetail($aid,$cluster)
   {
	  $sql = "select q.* from questions q , assignments a where q.quiz_id = a.quiz_id and q.test_cluster = '".$cluster."' and a.id =".$aid;
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   function getquestionresult($aid,$qid,$studu_id)
   {
	  $sql = "select is_correct_ans from user_answers ua , user_quizzes uq  where uq.id = ua.user_quiz_id and uq.user_id = '".$studu_id."' and uq.assignment_id =".$aid." and ua.question_id = ".$qid." group by question_id" ;
	  
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getquestionresult6($aid,$qid,$studu_id)
   {
	 $sql = "select ua.is_correct_ans, ua.is_radio_ans_correct from user_answers ua , user_quizzes uq where uq.id = ua.user_quiz_id and uq.user_id = '".$studu_id."' and uq.assignment_id =".$aid." and ua.question_id = ".$qid." and ua.is_radio_ans_correct is not null";
	  
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
	function displayQuestionsResult($aid,$cluster,$studu_id)
	{
			$questions = $this->getquestionsdetail($aid,$cluster);
			$totques ='';
			$trueQuecount = 0;
			$falseQuecount = 0;
			$ParttrueQuecount = 0;
			foreach($questions as $val)
			{
			 	$ans = '';
				$qid = $val['id'];
			 	if($val['question_type_id']!= 6)
				{	
					 $queresult = $this->getquestionresult($aid,$qid,$studu_id);
				 	 if($queresult[0]['is_correct_ans'] == 1)
						 $ans = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
					 	$ans = 0;
				}
	 			else 
				{
					 $queresult = $this->getquestionresult6($aid,$qid,$studu_id);
					 $tot = count($queresult); $cc ='';
					 if($queresult[0]['is_correct_ans'] == 1)
							 $cc = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
							$cc = 0;
					  $rc = 0; //echo"<pre>"; print_r($queresult);
					  if($tot >1)
					  {
						foreach($queresult as $qr)
						{
							if($qr['is_radio_ans_correct'] == 1)
							 $rc++;
						}
					  }
						if($cc == 1 && $rc == $tot)
							 $ans = 1;
						else if($cc == 1 && $rc < $tot)
				 			 $ans = 2;
						else if($cc == 0 && $rc <= $tot && $rc >0)
				 			 $ans = 2;
						if($cc != 1 && $rc != $tot)
							 $ans = 0;	 
				}
				 if($ans == 1)
					 $trueQuecount++;
				 if($ans == 0)
					 $falseQuecount++;
				 if($ans == 2)
					 $ParttrueQuecount++;
	  		}
				$disp ='';
				if($ParttrueQuecount >=1)
				{
					$trueQuecount = $trueQuecount + $ParttrueQuecount;
					$disptrueQuecount = '<font color="#FF9900">'.$trueQuecount.'</font>';
				}
				else
				{
					$disptrueQuecount = '<font color="#009900">'.$trueQuecount.'</font>';
				}
				
				$dispfalseQuecount = '<font color="#FF0000">'.$falseQuecount.'</font>';
			 $result = array("disptrueQuecount"=>$disptrueQuecount,"dispfalseQuecount"=>$dispfalseQuecount);
			 return $result;
	}
	
     
    function getteachername($school_id)
   {
	 $qry="SELECT * FROM  teachers where school_id='".$school_id."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   function getschoolname($school_id)
   {
	  $districtid=$this->session->userdata('district_id');
	   $qry="SELECT * FROM  schools where school_id='".$school_id."' and district_id='".$districtid."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   function getschooltype($district_id)
   {
	   $qry='select * from school_type where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
   }
   
   function getallschools($schooltypeid)
   {
	   $qry='select * from schools where school_type_id="'.$schooltypeid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   function getstudents($schid,$gradeid,$assignment_id)
   {
   	 $district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');
	 
	 // $qry='select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq where a.id=uq.assignment_id and uq.user_id in (select Distinct u.UserID from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= '.$teacher_id.') and a.district_id = '.$district_id.' order by a.id DESC';
	  if($assignment_id == 0)
	  {
		  $assessment = '';
	  }
	  else
	  {
		 $assessment =" and a.id=".$assignment_id;
	  }
	  
	   
	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students t ,assignments a, user_quizzes uq where u.student_id=t.student_id  and uq.user_id = u.UserID and a.id = uq.assignment_id".$assessment." and u.user_type=2 and t.teacher_id = ".$teacher_id." and u.grade_id=".$gradeid." and u.school_id=".$schid." and u.district_id =".$district_id." order by u.UserID";
	  }
	 else if(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u ,observers o, assignments a, user_quizzes uq where   u.school_id=o.school_id and uq.user_id = u.UserID and a.id = uq.assignment_id ".$assessment." and u.user_type=2 and o.observer_id = ".$observer_id." and u.grade_id=".$gradeid." and u.school_id=".$schid." and u.district_id =".$district_id." order by u.UserID";
	  }
	 
	    $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
	
	  function getgrades()
   {
	  	$district_id = $this->session->userdata('district_id');
	 $qry='select * from  dist_grades where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
     function getgradename($gradeid)
   {
 	 $qry='select * from  dist_grades where dist_grade_id="'.$gradeid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   function getcountry($country_id)
   {
	    $qry='select * from  countries_quiz where id="'.$country_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
    function getdistrict($distid)
   {
	    $qry='select * from  districts where district_id="'.$distid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
    function getstate($stateid)
   {
	    $qry='select * from  states where state_id ="'.$stateid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   public function record_count() {
  
  	 $district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');

	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students ts ,teachers t where u.student_id=ts.student_id and t.teacher_id = ts.teacher_id and u.school_id =t.school_id and u.user_type=2 and t.teacher_id = ".$teacher_id." and u.district_id ='".$district_id."'"; 
	 }
elseif(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , observers o where u.school_id=o.school_id and u.user_type=2 and o.observer_id = ".$observer_id." and u.district_id ='".$district_id."'"; 

	 }
	 else
	 {	 
		 $qry="SELECT DISTINCT * FROM users where user_type=2 and district_id ='".$district_id."'";

	 } 
 		$query = $this->db->query($qry);
		return $query->num_rows();
    }
	
    public function getstuddetail($limit, $start) {
     
	$district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');
	 
	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students ts ,teachers t where u.student_id=ts.student_id and t.teacher_id = ts.teacher_id and u.school_id =t.school_id and u.user_type=2 and t.teacher_id = ".$teacher_id." and u.district_id ='".$district_id."' limit ".$start." , ".$limit." "; 
	 }
	elseif(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , observers o where u.school_id=o.school_id and u.user_type=2 and o.observer_id = ".$observer_id." and u.district_id ='".$district_id."' limit ".$start." , ".$limit." "; 
	 }
	 else
	 {	 
		 $qry="SELECT DISTINCT * FROM users where user_type=2 and district_id ='".$district_id."' limit ".$start." , ".$limit." ";
	 }
         
	 //	 echo $qry; exit; 
	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}

	 }
         
         public function getstuddetails($observer_id) {
     
	$district_id = $this->session->userdata('district_id');
//	$teacher_id = $this->session->userdata('teacher_id');
//	$observer_id = $this->session->userdata('observer_id');
	 
	 
	
		$qry="SELECT DISTINCT u.* FROM users u , observers o where u.school_id=o.school_id and u.user_type=2 and o.observer_id IN (".$observer_id.") and u.district_id ='".$district_id."'"; 
	 
         
	 //	 echo $qry; exit; 
	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}

	 }
	 
   function getobservergrade($observer_id)
   {
	   $qry='select d.* from  dist_grades d left join schools s on d.district_id = s.district_id left join observers o on s.school_id = o.school_id  where o.observer_id="'.$observer_id.'"';
	   $query = $this->db->query($qry);
	   $array = array();
	   $array[0] = array("observer_id"=>$observer_id);
	  	$array[] = $query->result_array();
	   	if($query->num_rows()>0){
			return $array;
		}else{
			return false;
		}  
   }
   
   function getassessmentofObserver($school_id,$grade_id)
   {
	   if($grade_id == 0)
		{	
			$and ="and u.school_id=".$school_id;
	 	}
		else
		{
			$and = "and u.school_id=".$school_id." and u.grade_id =".$grade_id;
		}
		
  		$qry= "select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq, users u where a.id=uq.assignment_id and uq.user_id = u.UserId ".$and." Order by a.id DESC";  
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
	
     function getobserverschool($observer_id)
   {
	  $qry='select s.*,o.observer_id from schools s left join observers o on s.school_id = o.school_id where o.observer_id= '.$observer_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }

}
?>