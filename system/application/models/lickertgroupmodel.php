<?php
class Lickertgroupmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getlickertgroupCount($state_id,$country_id,$district_id)
	{
	
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from lickert_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id " ;
		
		}
		else
		{
			$qry="Select count(*) as count 
	  			from lickert_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id and  og.district_id=$district_id " ;
		
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getlickertgroups($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		if($district_id=='all')
		{
		$qry="Select s.name,og.group_id,og.group_name,og.description,d.districts_name from lickert_groups og,districts d,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id $limit ";
		}
		else
		{
			$qry="Select s.name,og.group_id,og.group_name,og.description,d.districts_name from lickert_groups og,districts d ,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id and og.district_id=$district_id $limit ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getlickertgroupById($val);
			 
			  
			  if($data!=false && $data[0]['district_id']!=$pdata['end'])
			  {
					$status=1;	
				   $sdata = array('group_name' => $data[0]['group_name'],
					'description' => $data[0]['description'],
					'district_id' => $pdata['end']	
					);
					$str = $this->db->insert_string('lickert_groups', $sdata);
					$this->db->query($str);
					
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
	function copylickertgroup($from,$to)
	{
	  $groups=$this->getalllickertgroupsbyDistrictID($from);
	  if($groups!=false)
	  {
	    foreach($groups as $val)
		{
		  $sdata = array('group_name' => $val['group_name'],
					'description' => $val['description'],
					'district_id' => $to	
					);
					$str = $this->db->insert_string('lickert_groups', $sdata);
					$this->db->query($str);
					$last_group_id=$this->db->insert_id();
					
					$points=$this->getAllPoints($val['group_id'],$from);
					if($points!=false)
					{
					
					foreach($points as $pointval)
					{
					
					$sdata = array('group_type_id' => $pointval['group_type_id'],
					'ques_type_id' => $pointval['ques_type_id'],
					'group_id' => $last_group_id,
					'question' => $pointval['question'],
					'district_id' => $to
		
		
					);
					$str = $this->db->insert_string('lickert_points', $sdata);
					$this->db->query($str);
					$last_id=$this->db->insert_id();
					$subdata=$this->getAllsubgroups($pointval['point_id']);
					if($subdata!=false)
					{
					  foreach($subdata as $subval)
					  {
					  $subsavedata=array('point_id' =>$last_id,
					 'sub_group_name'=> $subval['sub_group_name']
					 );
					
					 $sstr = $this->db->insert_string('lickert_sub_groups', $subsavedata);
					  $this->db->query($sstr);
					
					 }
					
					}
			
			  
					
					}
					
					
					}
					
					
		
		
		}
	  
	  
	  
	  
	  }
	
	
	
	}
	
	function getAllPoints($group_id,$dist_id)
	{
	  $qry = "Select d.country_id,d.state_id,op.point_id,op.group_id,op.ques_type_id,op.question,op.group_type_id,op.district_id from lickert_points  op,districts d where op.district_id=d.district_id and op.group_id=$group_id and op.district_id=$dist_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllsubgroups($point_id)
	{
		$qry = "Select sub_group_id,point_id,sub_group_name from lickert_sub_groups where point_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getlickertgroupById($ob_id)
	{
		
		$qry = "Select og.group_id,og.group_name,og.description,og.district_id,d.state_id,d.country_id from lickert_groups og,districts d where og.district_id=d.district_id and  og.group_id=".$ob_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function add_lickertgroup()
	{
	  $data = array('group_name' => $this->input->post('group_name'),
					'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')	
		
		);
	   try{
			$str = $this->db->insert_string('lickert_groups', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_lickertgroup()
	{
	
		
		$group_id=$this->input->post('group_id');
	$data = array('group_name' => $this->input->post('group_name'),
				   'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')
			
		
		
		);
		
			
		$where = "group_id=".$group_id;		
		
		try{
			$str = $this->db->update_string('lickert_groups', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletelickertgroup($ob_id)
	{
		$qry = "delete from lickert_groups where group_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getalllickertgroups()
	{
	 $district_id=$this->session->userdata("district_id");
	 $qry = "Select group_id,group_name,description from lickert_groups where district_id=$district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getalllickertgroupsbyDistrictID($district_id)
	{
	
	 $qry = "Select group_id,group_name,description from lickert_groups where district_id=$district_id order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
}