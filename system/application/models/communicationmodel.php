<?php
class Communicationmodel extends Model
{

function save_staff($teacher_id,$comm,$type,$value)
{


	  $data = array(
	  'teacher_id' => $teacher_id,	 
	  'communication' => $comm,
	   'type' => $type,
	    'value' => $value
					);
	   try{
			$str = $this->db->insert_string('staff_communication', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}

}
function parent_staff($parent_id,$comm,$type,$value)
{


	  $data = array(
	  'parent_id' => $parent_id,	 
	  'communication' => $comm,
	   'type' => $type,
	    'value' => $value
					);
	   try{
			$str = $this->db->insert_string('parent_communication', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}

}
function getstaffreportcount($type,$value)
	{
	
	   $school_id=$this->session->userdata("school_id");
	   if($type!='grade')
		{
		if($type=='individual')
		{
			$qry="Select count(*) as count 
	  			from staff_communication s, teachers t where t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type' and s.teacher_id=$value ";
		
		}
		
		else
		{
		$qry="Select count(*) as count 
	  			from staff_communication s, teachers t where t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type' ";
				
		}		
		}
		else
		{
				if($value!='all')
				{				
				
				$qry="Select count(*) as count 
	  			from staff_communication s, teachers t where t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type' and s.value='$value' ";
				}
				else
				{
				$qry="Select count(*) as count 
	  			from staff_communication s, teachers t where t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type'  ";
				
				}
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}


function getstaffreport($page,$per_page,$type,$value)
	{
	
		$school_id=$this->session->userdata("school_id");
		$page -= 1;
		$start = $page * $per_page;
		$limit=" order by s.created desc limit $start, $per_page ";
		
		
	  if($type!='grade')
		{
			if($type=='individual')
		{
			$qry="Select s.*,t.firstname,t.lastname 
	  			from staff_communication s, teachers t where  t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type' and s.teacher_id=$value $limit ";
		
		}
		
		else
		{
			$qry="Select s.*,t.firstname,t.lastname
	  			from staff_communication s, teachers t where  t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type' $limit ";
		
		}
		}
		else
		{
				if($value!='all')
				{
				$qry="Select s.*,t.firstname,t.lastname 
	  			from staff_communication s, teachers t where  t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type' and s.value='$value' $limit ";
				}
				else
				{
				$qry="Select s.*,t.firstname,t.lastname 
	  			from staff_communication s, teachers t where  t.school_id=$school_id and s.teacher_id=t.teacher_id  and s.type='$type'  $limit ";
				
				}
		}	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getparentreportcount($type,$value)
	{
	
	   $school_id=$this->session->userdata("school_id");
	   if($type=='grade' || $type=='all')
		{
			if($value!='all')
				{				
				
				$qry="Select count(*) as count 
	  			from parent_communication s, parents t where  t.school_id=$school_id and s.parent_id=t.parents_id  and s.type='$type' and s.value='$value' ";
				}
				else
				{
				$qry="Select count(*) as count 
	  			from parent_communication s, parents t where  t.school_id=$school_id and s.parent_id=t.parents_id  and s.type='$type'  ";
				
				}
		}
		else
		{
					if($value=='all')
					{
					
						$value='';
					}
					$qry="Select count(*) as count 
	  			from parent_communication s, parents t where  t.school_id=$school_id and s.parent_id=t.parents_id  and s.type='$type' and s.value='$value' ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}


function getparentreport($page,$per_page,$type,$value)
	{
	
		$school_id=$this->session->userdata("school_id");
		$page -= 1;
		$start = $page * $per_page;
		$limit=" order by s.created desc limit $start, $per_page ";
		
		
	   if($type=='grade' || $type=='all')
		{
			if($value!='all')
				{				
				
				$qry="Select s.*,t.firstname,t.lastname 
	  			from parent_communication s, parents t where t.school_id=$school_id and s.parent_id=t.parents_id  and s.type='$type' and s.value='$value' $limit ";
				}
				else
				{
				$qry="Select s.*,t.firstname,t.lastname 
	  			from parent_communication s, parents t where t.school_id=$school_id and s.parent_id=t.parents_id  and s.type='$type'  $limit ";
				
				}
		}
		else
		{
				if($value=='all')
					{
					
						$value='';
					}
				$qry="Select s.*,t.firstname,t.lastname 
	  			from parent_communication s, parents t where t.school_id=$school_id and s.parent_id=t.parents_id  and s.type='$type' and s.value='$value'  $limit ";
		}	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getparentcomm($id)
	
	{
		$qry="select * from parent_communication where parent_communication_id=$id";
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getstaffcomm($id)
	
	{
		$qry="select * from staff_communication where staff_communication_id=$id";
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
}