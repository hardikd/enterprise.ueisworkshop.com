<?php
class Assessmentcalendarmodel extends Model
{
function generate($year,$month)
		{
			
	$conf=array(
					'start_day' => 'monday',
					'show_next_prev' => true,
					'next_prev_url' => base_url().'assessmentcalendar/index',
		
		);
		
		$conf['template']='  
		
		
	 	{table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}
			
			{heading_row_start}<tr>{/heading_row_start}
			
			{heading_previous_cell}<th class="next-Previous"><a href="{previous_url}">Back</a></th>{/heading_previous_cell}
			{heading_title_cell}<th colspan="{colspan}" id="year-month-heading">{heading}</th>{/heading_title_cell}
			{heading_next_cell}<th class="next-Previous"><a href="{next_url}">Next</a></th>{/heading_next_cell}
			
			{heading_row_end}</tr>{/heading_row_end}
			
			{week_row_start}<tr>{/week_row_start}
			{week_day_cell}<td>{week_day}</td>{/week_day_cell}
			{week_row_end}</tr>{/week_row_end}
			
			{cal_row_start}<tr class="days">{/cal_row_start}
			{cal_cell_start}<td class="day">{/cal_cell_start}
			
			{cal_cell_content}
				<div class="day_num">{day}</div>
				<div class="contents">{content}</div>
			{/cal_cell_content}
			{cal_cell_content_today}
				<div class="day_num highlight">{day}</div>
				<div class="contents">{content}</div>
			{/cal_cell_content_today}
			
			{cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
			{cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
			
			{cal_cell_blank}&nbsp;{/cal_cell_blank}
			
			{cal_cell_end}</td>{/cal_cell_end}
			{cal_row_end}</tr>{/cal_row_end}
			
			{table_close}</table>{/table_close}';

		$this->load->library('calendar',$conf);
		
		$cal_data = $this->get_calendar_data($year, $month);
		
		return $this->calendar->generate($year,$month,$cal_data);
		}
			
			
		function add_calendar_data($date, $data,$userId) {
			
			$userId=$this->session->userdata('id');
		
			if ($this->db->select('createtestdate')->from('assignments')->where('login_user_id', $userId)->where('date', $date)->count_all_results()) {
			
			$this->db->where('createtestdate', $date)->where('login_user_id', $userId)->update('createtestdate', array(
				'createtestdate' => $date,
			/*	'data' => $data,*/
						
			));
			
			
			echo "update";
			} 
		else 
		{
			
			$userId=$this->session->userdata('dist_user_id');
			
			$this->db->insert('assignments', array(
				'createtestdate' => $date,
				/*'data' => $data,*/
			/*	'login_user_id' => $userId*/			
			));
			
			echo "inserted";
		}	
		}
			
		
		function get_calendar_data($year,$month) 
		{
		
		/*$query = $this->db->select('createtestdate,assignment_name,subject_id,quiz_id')->from('assignments')->like('createtestdate', "$year-$month", 'after')->get();*/
		$district_id=$this->session->userdata('district_id');
		$query = $this->db->select('createtestdate,assignment_name,subject_id,quiz_id')->from('assignments')->where('district_id',$district_id)->like('createtestdate', "$year-$month", 'after')->get();
	
		
		$userId=$this->session->userdata('dist_user_id');
		
		/*$query = $this->db->select('date')->from('assignments')->where('login_user_id',$userId)->like('date', "$year-$month", 'after')->get();*/
		@$data = $query->result_array();
		
		
		
		//@$subject_id = $data[0]['subject_id'];
		 @$quiz_id = $data[0]['quiz_id'];
		
			$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
			

		
		//$sql = $this->db->select('subject_id,subject_name')->from('subjects')->where('subject_id',$subject_id)->get();
		@$data1 = $sql->result_array();
		
		
		$cal_data = array();
		
		foreach ($data as $row) 
		{
		if(substr($row['createtestdate'],8,2) <= '09')
		{	
		
		$quiz_id = $row['quiz_id'];
		$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
		$data1 = $sql->result_array();
		
					
		if($row['createtestdate']< date('Y-m-d'))
		{
		
					$quiz_id = $row['quiz_id'];
		$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
		$data1 = $sql->result_array();
		
		$test = array_merge(array($row['createtestdate']),array($row['assignment_name']));
		if(!empty($test))
		{
$cal_data[substr($row['createtestdate'],9,1)] = "<p style='color:grey;'>".@$test[1]."<br />".@$test[0]."<br/>".@$data1[0]['quiz_name']."</p>";
		}
		}
		else
		{
						$quiz_id = $row['quiz_id'];
		$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
		$data1 = $sql->result_array();
		
		$test = array_merge(array($row['createtestdate']),array($row['assignment_name']));
		if(!empty($test))
		{
		$cal_data[substr($row['createtestdate'],9,1)] = "<p style='color:green;'>".@$test[1]."<br />".@$test[0]."<br/>".@$data1[0]['quiz_name']."</p>";	
		}
		}
		
		
		}
		
		else if(substr($row['createtestdate'],8,2) >= '09') 
		{
			
			$quiz_id = $row['quiz_id'];
		$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
		$data1 = $sql->result_array();
			
			
		if($row['createtestdate']>= date('Y-m-d'))
		{
						$quiz_id = $row['quiz_id'];
		$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
		$data1 = $sql->result_array();
		
		$test = array_merge(array($row['createtestdate']),array($row['assignment_name']));
		if(!empty($test))
		{
		$cal_data[substr($row['createtestdate'],8,2)] = "<p style='color:green;'>".@$test[1]."<br />".@$test[0]."<br/>".@$data1[0]['quiz_name']."</p>";
		}
		}
		else
		{
						$quiz_id = $row['quiz_id'];
		$sql = $this->db->select('id,quiz_name')->from('quizzes')->where('id',$quiz_id)->get();
		$data1 = $sql->result_array();
		
		$test = array_merge(array($row['createtestdate']),array($row['assignment_name']));
		if(!empty($test))
		{
		$cal_data[substr($row['createtestdate'],8,2)] = "<p style='color:grey;'>".@$test[1]."<br />".@$test[0]."<br/>".@$data1[0]['quiz_name']."</p>";	
		}
		}
		}
		}		
		return $cal_data;
		
		}
		
       
       }
        ?>
        