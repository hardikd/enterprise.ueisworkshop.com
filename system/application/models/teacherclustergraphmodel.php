<?php
class Teacherclustergraphmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
  function getAssessment($teacher_id)
   {
	  	$qry='select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq where a.id=uq.assignment_id and uq.user_id in (select Distinct u.UserID from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= '.$teacher_id.') order by a.id DESC';
		$query = $this->db->query($qry);
//                echo $this->db->last_query();exit;
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 	 
   }
 
  function getallcategory()
   {
	   $qry='select * from cats';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   function getTestItem($teacher_id,$ass_id)
    {
		if($ass_id == 0)
		$andAss = " AND uq.assignment_id<>''";
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;
	//$qry= "SELECT test_cluster FROM `questions` where id in(select question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id in (select Distinct u.UserID from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= ".$teacher_id.") ".$andAss.") and test_cluster!='' group by test_cluster";  
//echo 		 $qry= "SELECT q.test_cluster FROM `questions` q,user_answers ua,user_quizzes uq, users u where q.id = ua.question_id and  ua.user_quiz_id=uq.id and uq.user_id in (select Distinct us.UserID from users us, teacher_students t where us.student_id = t.student_id and t.teacher_id=".$teacher_id.") ".$andAss." and q.test_cluster!='' group by q.test_cluster";
		$qry= "SELECT q.test_cluster FROM `questions` q,user_answers ua,user_quizzes uq, users u ,teacher_students t where q.id = ua.question_id and ua.user_quiz_id=uq.id and uq.user_id = u.UserID and u.student_id = t.student_id and t.teacher_id=".$teacher_id."".$andAss." and q.test_cluster!='' group by q.test_cluster";
		 	  
		$query = $this->db->query($qry);
               // echo $this->db->last_query();exit; 
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 	
	 }
	
 
   function getGraphData($test_item,$teacherid,$district_id,$fDate,$tDate,$ass_id)
	{
		$data['student'] = $this->getteacherstudent($teacherid);
		 $rows =  count($data['student']);
		// echo"<pre>"; print_r($rows);
		if($rows > 0)
		{  
			$arrResult  =array();
			for($i = 0 ;$i<$rows; $i++)
			{
		 		$arrResult[$i]['per'] = $this->getPercentageByStudentAndClusterForGraph($data['student'][$i]['UserID'],$test_item,$district_id,$fDate,$tDate,$ass_id);
				
				$arrResult[$i]['student_name'] = $data['student'][$i]['Name']." ".$data['student'][$i]['Surname'];
			}
			return $arrResult;
		} 
		
		
	}
	
	
	 function getteacherschool($teacher_id)
 	  {
	   $qry='select s.*,t.teacher_id from schools s left join teachers t on s.school_id = t.school_id where t.teacher_id= '.$teacher_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
 
   
   function getteacherstudent($teacher_id)
   {
	   $qry='select Distinct u.* from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= '.$teacher_id.' order by Name,Surname ';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   
  function getPercentageByStudentAndClusterForGraph($user_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id)
    { 
		if($ass_id == 0)
		$andAss = '';
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;
		
		$andDate = " AND DATE(ua.added_date) between '".$fDate."' ANd '".$tDate."' ";
		if($test_cluster == 'all')
		{
			$and = " ";
			$andp = " ";
		}
		else
		{
			$and = "and q.test_cluster='".$test_cluster."'";
			$andp = "and test_cluster='".$test_cluster."'";
		}
	$group_by = "question_id ";
	$group_by_radio = "ua.id";
	
	
 		$qry_Primary= "select * from  questions where quiz_id in (select distinct a.quiz_id from user_answers ua,user_quizzes uq, assignments a where ua.user_quiz_id=uq.id and uq.assignment_id = a.id and uq.user_id = ".$user_id." ".$andAss." ".$andDate.")".$andp."";
	 	$query = $this->db->query($qry_Primary); 
  		$sum1 = $query->num_rows();
		
		$qry_radio= "select count(aq.id) as cnt from answers_quiz aq, question_groups qg, questions q where aq.group_id=qg.id and qg.question_id = q.id and q.quiz_id in (select distinct a.quiz_id from user_answers ua,user_quizzes uq, assignments a where ua.user_quiz_id=uq.id and uq.assignment_id = a.id and uq.user_id = ".$user_id." ".$andAss." ".$andDate.")".$and." and aq.radio_ans = 1";
	 
	 	$query = $this->db->query($qry_radio); 
  		$sumr = $query->result_array();
		$sum3 = $sumr[0]['cnt'];
		/*$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." ".$and." and uq.assignment_id=a.id".$andAss." and is_score_ans=1".$andDate." group by ".$group_by;
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows(); */
		
		$ToalQes = $sum1 + $sum3; //+ $sum2;
		
	 	$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, questions q where ua.user_quiz_id=uq.id and ua.question_id = q.id and uq.user_id = ".$user_id." ".$and." ".$andAss." and is_correct_ans=1 and is_score_ans=0".$andDate."  group by ".$group_by;	
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
	
		/* $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq, questions q where ua.user_quiz_id=uq.id and ua.question_id = q.id and uq.user_id = ".$user_id." ".$and." ".$andAss." and is_correct_ans=1 and is_score_ans=1".$andDate."  group by ".$group_by; 
		 $query = $this->db->query($qry_Sec_correct);
		 $c_sum2 = $query->num_rows();
		
		 */
	 $qry_radio_correct= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq, questions q where ua.user_quiz_id=uq.id and ua.question_id = q.id and uq.user_id = ".$user_id." ".$and." ".$andAss."  and is_score_ans=0 and is_radio_ans_correct = 1".$andDate." group by ".$group_by_radio;
	 	$query = $this->db->query($qry_radio_correct); 
  		 $c_sum3 = $query->num_rows(); 
		
	 	$Curr_Qes = $c_sum1 + $c_sum3 ;//+ $c_sum3;
		if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =-1;
		 $per = number_format($per,2,'.','');
			
 		return $per;
	 }

/*
   function getPercentageByStudentAndCluster($user_id,$category_id,$test_cluster)
    {
	
		if($category_id == 0)
		{
			$and = "";
		}
		else
		{
			$and = " and a.quiz_id in (select id from quizzes where cat_id=".$category_id.")";
		}
		
	 	$qry_Primary= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' )  and is_score_ans=0 group by question_id"; 
		$query = $this->db->query($qry_Primary);
  		 $sum1 = $query->num_rows(); 
		
		
		$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' )  and is_score_ans=1 group by question_id"; 
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows();
		
		$ToalQes = $sum1 + $sum2;
		
		$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' )  and is_correct_ans=1 and is_score_ans=0  group by question_id";	
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
		 
		 $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' )  and is_correct_ans=1 and is_score_ans=1  group by question_id"; 
		$query = $this->db->query($qry_Sec_correct);
		 $c_sum2 = $query->num_rows();
		
		$Curr_Qes = $c_sum1 + $c_sum2;
		if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =0;
		 	$per = number_format($per,2,'.','');
 		return $per;
	 }
	 
 	 
   
  function getPercentageByAllStudent($user_id,$category_id)
    {
	
		if($category_id == 0)
		{
			$and = "";
		}
		else
		{
			$and = " and a.quiz_id in (select id from quizzes where cat_id=".$category_id.")";
		}
		
	 	$qry_Primary= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_score_ans=0 group by question_id"; 
		$query = $this->db->query($qry_Primary);
  		 $sum1 = $query->num_rows(); 
		
		
		$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_score_ans=1 group by question_id"; 
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows();
		
		$ToalQes = $sum1 + $sum2;
		
		$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_correct_ans=1 and is_score_ans=0 group by question_id"; 		
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
		 
		 $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_correct_ans=1 and is_score_ans=1 group by question_id";  
		$query = $this->db->query($qry_Sec_correct);
		 $c_sum2 = $query->num_rows();
		
		$Curr_Qes = $c_sum1 + $c_sum2;
		if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =0;
		 	$per = number_format($per,2,'.','');
 		return $per;
	 }
	*/
 
	 
} // class end

?>