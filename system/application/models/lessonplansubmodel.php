<?php
class Lessonplansubmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getlessonplansubCount($group_id)
	{
	    if($group_id=='all')
		{

		$qry="Select count(*) as count from lesson_plan_sub op,lesson_plans og where   op.lesson_plan_id=og.lesson_plan_id " ;
		
		}
		else
		{
			$qry="Select count(*) as count from lesson_plan_sub op,lesson_plans og where   op.lesson_plan_id=og.lesson_plan_id and  op.lesson_plan_id=$group_id   " ;	
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getlessonplansubs($page,$per_page,$group_id)
	{
		
		
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		
		if($group_id=='all')
		{
			$qry=" Select rs.lesson_plan_sub_id lesson_plan_sub_id,rs.subtab as subtab,r.tab,r.lesson_plan_id from  lesson_plans r,lesson_plan_sub rs where  rs.lesson_plan_id=r.lesson_plan_id  $limit  ";
			
		}
		else
		{
			$qry=" Select rs.lesson_plan_sub_id lesson_plan_sub_id,rs.subtab as subtab,r.tab,r.lesson_plan_id from  lesson_plans r,lesson_plan_sub rs where  rs.lesson_plan_id=r.lesson_plan_id and  r.lesson_plan_id=$group_id  $limit  ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getlessonplansubById($point_id)
	{
		
		$qry = "Select rs.lesson_plan_sub_id,rs.lesson_plan_id,rs.subtab from lesson_plan_sub rs where  rs.lesson_plan_id=".$point_id;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getlessonplansubBysubId($point_id)
	{
		
		$qry = "select rs.lesson_plan_sub_id,rs.lesson_plan_id,rs.subtab from lesson_plan_sub rs where rs.lesson_plan_sub_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	
	
	function add_lessonplansub()
	{
	     $lesson_plan_id=$this->input->post('lesson_plan_id');
		 
	   	
			if($lesson_plan_id)
			{
				   $c=$this->input->post('counterscale');
				   
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!='' )
					 {
					 $sdata=array('lesson_plan_id' =>$lesson_plan_id,
					 'subtab'=>$this->input->post('textbox'.$i)
					 );
					
					 $sstr = $this->db->insert_string('lesson_plan_sub', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   $last_id=$this->db->insert_id();
				   
				   
				   return $last_id;
				
			}
			else{
				return 0;
			}
		
	}
	function update_lessonplansub()
	{
			$lesson_plan_id=$this->input->post('lesson_plan_id');
			$actual_lesson_plan_id=$this->input->post('actual_lesson_plan_id');
			
		if($lesson_plan_id==$actual_lesson_plan_id )
			{
			$dqry = "delete from lesson_plan_sub where lesson_plan_id=$lesson_plan_id ";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('counterscale');
		for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!=''  && $this->input->post('textboxup'.$i)!='' )
					 {
					    $lesson_plan_sub_id=$this->input->post('textboxup'.$i);
						
						$sdata=array('subtab'=>$this->input->post('textbox'.$i),
						             'lesson_plan_id'=>$lesson_plan_id,
									 'lesson_plan_sub_id'=>$lesson_plan_sub_id
						
						);
                   					 
					   //$where = "point_id=$point_id and sub_group_id=$sub_group_id";		
		
		
			                $str = $this->db->insert_string('lesson_plan_sub', $sdata);
					          $this->db->query($str);
					 
					 }
					  else if($this->input->post('textbox'.$i)!='' )
					 {
					 $sdata=array('lesson_plan_id' =>$lesson_plan_id,
					 'subtab'=>$this->input->post('textbox'.$i)   
					 );
					 
					 $sstr = $this->db->insert_string('lesson_plan_sub', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return true;	
		}
		else if($lesson_plan_id!=$actual_lesson_plan_id)
		{
			$dqry = "delete from lesson_plan_sub where lesson_plan_id=$actual_lesson_plan_id ";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('counterscale');
		for($i=1;$i<=$c;$i++)
				   {
				     
					 $name=$this->input->post('textbox'.$i);
					 if($name!='0'){
					 $sdata=array('lesson_plan_id' =>$lesson_plan_id,
					 'subtab'=>$this->input->post('textbox'.$i)   
					 );
					 
					 $sstr = $this->db->insert_string('lesson_plan_sub', $sdata);
					 $this->db->query($sstr);
					}
					
				   }
				   
				   return true;	
		
		
		}
		else
		{
		
			return false;
		}
		
	
	
	
	}
	
	function deletelessonplansub($ob_id)
	{
		$qry = "delete from lesson_plan_sub where lesson_plan_sub_id=$ob_id";
		$query = $this->db->query($qry);
		
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
	
	
}