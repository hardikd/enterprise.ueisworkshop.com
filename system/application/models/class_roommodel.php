<?php
class Class_roommodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getclass_roomCount($school_id)
	{
	
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		 $qry="Select count(*) as count 
	  			from class_rooms t,schools s,dist_grades g where t.school_id=s.school_id and t.grade_id=g.dist_grade_id and s.district_id=$district_id " ;
		
		}
		else
		{
		
			$qry="Select count(*) as count 
	  			from class_rooms " ;
		}
			
		}
		else
		{
		 $qry="Select count(*) as count 
	  			from class_rooms t,schools s,dist_grades g where t.school_id=s.school_id and t.grade_id=g.dist_grade_id and t.school_id=$school_id" ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getclass_rooms($page,$per_page,$school_id)
	{
		
		
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
			$district_id=$this->session->userdata('district_id');
		
			$qry="Select b.name,b.class_room_id as class_room_id,s.school_name as school_name,g.grade_name as grade_name from class_rooms b,schools s,dist_grades g where b.school_id=s.school_id and b.grade_id=g.dist_grade_id and s.district_id=$district_id ";
		}
		else
		{
			
			$qry="Select b.username,b.class_room_id as class_room_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from class_rooms b,schools s where b.school_id=s.school_id ";
		
		}
		}
		else
		{
			$qry="Select b.name,b.class_room_id as class_room_id,s.school_name as school_name,g.grade_name as grade_name from class_rooms b,schools s,dist_grades g where b.school_id=s.school_id and b.grade_id=g.dist_grade_id  and b.school_id=$school_id ";
		}
                
                if($page!=false && $per_page!=false){
                    $page -= 1;
		$start = $page * $per_page;
                $qry .=" limit $start, $per_page ";
                }
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getclass_roomById($class_room_id)
	{
		
		$qry = "Select class_room_id,name,school_id,grade_id from class_rooms where class_room_id=".$class_room_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getclassroom($school_id,$grade_id)
	{
		
		$qry = "Select class_room_id,name from class_rooms where school_id=".$school_id." and grade_id=".$grade_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getclassroombyschool($school_id)
	{
		if($this->session->userdata('login_type')=='teacher'){
		$qry = "Select class_room_id,name from class_rooms where school_id=".$school_id ;
		}
		else if(($this->session->userdata('login_type')=='observer')) {
		$qry = "Select class_room_id,name from class_rooms where school_id=".$school_id ;
		}else{
		
		$district_id = $this->session->userdata('district_id');
		
		$qrydist = "Select s.school_id from schools s where  s.district_id=$district_id  ";
		$querydist = $this->db->query($qrydist);
		$resultdist = $querydist->result();
//		print_r($resultdist);exit;
		foreach($resultdist as $school){
			$schoolid[] = 	$school->school_id;
		}
		$schoolids = implode(",",$schoolid);
		
		$qry = "Select class_room_id,name from class_rooms where school_id IN ($schoolids)" ;
			
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function check_class_room_exists($name=false,$school_id=false)
	{
	
		if($name==false)
		{
		$name=$this->input->post('name');
		$school_id=$this->input->post('school_id');
		
		}
		$qry="SELECT  class_room_id from class_rooms where name='$name' and school_id=$school_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						return 0;

			
		}else{
			
			
		  return 1;
		
		}
	
	
	}
	function check_class_room_update() {
	
	
		$name=$this->input->post('name');
		$school_id=$this->input->post('school_id');
		$id=$this->input->post('class_room_id');
		
		$qry="SELECT class_room_id from class_rooms where name='$name' and school_id=$school_id and class_room_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	}
	function add_class_room()
	{
	 // $data = array('name' => $this->input->post('name'),					
//					'school_id' => $this->input->post('school_id'),
//                    'grade_id'=>$this->input->post('grade_id'),
//					'created'=>date('Y-m-d H:i:s'),
//				    'district_id'=> $this->input->post('district_class_id'),
//		
//		);
		  $data = array('name' => $this->input->post('name'),					
					'school_id' => $this->input->post('school_id'),
                    'grade_id'=>$this->input->post('grade_id'),
					'created'=>date('Y-m-d H:i:s'),
		
		);
		
	   try{
			$str = $this->db->insert_string('class_rooms', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function update_class_room()
	{
	
		
		$class_room_id=$this->input->post('class_room_id');
	
		
		
			$data = array('name' => $this->input->post('name'),
					'school_id' => $this->input->post('school_id'),				  
	               'grade_id'=>$this->input->post('grade_id')
		
		         );
		
		
			
		$where = "class_room_id=".$class_room_id;		
		
		try{
			$str = $this->db->update_string('class_rooms', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteclass_room($class_room_id)
	{
		$qry = "delete from class_rooms where class_room_id=$class_room_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
	
	
}