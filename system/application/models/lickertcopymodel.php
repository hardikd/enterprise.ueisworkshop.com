<?php
class Lickertcopymodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getallobservationgroupsbyDistrictID($district_id)
	{
	
	 $qry = "Select group_id,group_name,description from observation_groups where district_id=$district_id order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function copy($from,$to)
	{
	  $groups=$this->getallobservationgroupsbyDistrictID($from);
	  if($groups!=false)
	  {
	    foreach($groups as $val)
		{
		  $sdata = array('group_name' => $val['group_name'],
					'description' => $val['description'],
					'district_id' => $to	
					);
					$str = $this->db->insert_string('lickert_groups', $sdata);
					$this->db->query($str);
					$last_group_id=$this->db->insert_id();
					
					$points=$this->getAllPoints($val['group_id'],$from);
					if($points!=false)
					{
					
					foreach($points as $pointval)
					{
					
					$sdata = array('group_type_id' => 2,
					'ques_type_id' => 2,
					'group_id' => $last_group_id,
					'question' => $pointval['question'],
					'district_id' => $to
		
		
					);
					$str = $this->db->insert_string('lickert_points', $sdata);
					$this->db->query($str);
					$last_id=$this->db->insert_id();
					
					  for($k=0;$k<=3;$k++)
					  {
					  if($k==0)
					  {
						$sub_text='Advanced';
					  
					  }
					  if($k==1)
					  {
						$sub_text='Proficient';
					  
					  }
					  if($k==2)
					  {
						$sub_text='Emerging';
					  
					  }
					  if($k==3)
					  {
						$sub_text='Not Evident';
					  
					  }
					  $subsavedata=array('point_id' =>$last_id,
					 'sub_group_name'=> $sub_text
					 );
					
					 $sstr = $this->db->insert_string('lickert_sub_groups', $subsavedata);
					  $this->db->query($sstr);
					
					 }
					
					
			
			  
					
					}
					
					
					}
					
					
		
		
		}
	  
	  
	  
	  
	  }
	
	
	
	}
	function getAllPoints($group_id,$dist_id)
	{
	 $qry = "Select op.point_id,op.group_id,op.ques_type_id,op.question,op.group_type_id,op.district_id from observ_points  op,districts d where op.district_id=d.district_id and op.group_id=$group_id and op.district_id=$dist_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	

	
	
}	