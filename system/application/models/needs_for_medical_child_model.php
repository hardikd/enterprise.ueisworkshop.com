<?php
class Needs_for_medical_child_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_all_data($page=false,$per_page=false,$parent='')
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		}
	
	$this->db->select('*,ish1.intervention_strategies_medical as need_medical');
	$this->db->from('intervention_strategies_medical ish1');
	$this->db->join('intervention_strategies_medical ish2','ish2.parent_id = ish1.id','INNER');
	$this->db->where('ish1.district_id',$this->session->userdata('district_id'));
	$this->db->where('ish2.is_delete','0');
	
	if($parent!=''){
			$this->db->where('ish2.parent_id',$parent);
			}
	 //$this->db->order_by('id','desc');
	
	
	$query = $this->db->get();
//print_r($this->db->last_query());exit;
		
		$result = $query->result();
		return $result;	
	}
	public function get_all_dataCount()
	{
		
		$this->db->where('is_delete',0);
		$query = $this->db->get('intervention_strategies_medical');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
public function get_all_medical_child_data($page=false,$per_page=false)
	{
			$this->db->select('*,ish1.intervention_strategies_medical as need_medical');
	$this->db->from('intervention_strategies_medical ish1');
	$this->db->join('intervention_strategies_medical ish2','ish2.parent_id = ish1.id','INNER');
	$this->db->where('ish1.district_id',$this->session->userdata('district_id'));
	$this->db->where('ish2.is_delete','0');
	 //$this->db->order_by('id','desc');
	
	
	$query = $this->db->get();
//print_r($this->db->last_query());exit;
		
		$result = $query->result();
		return $result;
	}

	public function delete_medical_child($table,$field)
	   {
		$this->db->where('id',$this->input->post('id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_medical_childById($where){
		$query = $this->db->get_where('intervention_strategies_medical',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
        
        

}
?>