<?php
class Scheduletaskmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getscheduletaskscount()
	{
	
	$qry="Select count(*) as count from scheduletasks " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	
	function getscheduletasks($page,$per_page)
	{
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
	
		$qry="Select task,schedule_task_id from scheduletasks  $limit ";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function add_plan()
	{
	  $data = array('task' => $this->input->post('task')
					);
	   try{
			$str = $this->db->insert_string('scheduletasks', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_plan()
	{
	
		
		$plan_id=$this->input->post('plan_id');
		$data = array('task' => $this->input->post('task')
					);
			
			
		$where = "schedule_task_id=".$plan_id;		
		
		try{
			$str = $this->db->update_string('scheduletasks', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteplan($plan_id)
	{
		$qry = "delete from scheduletasks where schedule_task_id=$plan_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getplanById($plan_id)
	{
	
		$qry = "Select schedule_task_id,task from scheduletasks where schedule_task_id=".$plan_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getallscheduletasks()
	{
	  $qry = "Select schedule_task_id,task from scheduletasks ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
		
}