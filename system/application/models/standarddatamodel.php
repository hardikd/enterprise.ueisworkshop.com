<?php
class Standarddatamodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getstandarddataCount($standard_id,$grade_id)
	{
		$district_id=$this->session->userdata('district_id');
		
		if($grade_id=='all')
		{
		$qry="Select count(*) as count 
	  			from standarddata  s, standards d,dist_grades g,dist_subjects st where d.standard_id=s.standard_id and s.grade=g.dist_grade_id and g.district_id=$district_id and  s.standard_id=$standard_id and d.subject_id=st.dist_subject_id " ;
		}
		else
		{
		 $qry="Select count(*) as count 
	  			from standarddata  s, standards d,dist_grades g,dist_subjects st where d.standard_id=s.standard_id and  s.grade=g.dist_grade_id and g.district_id=$district_id and s.standard_id=$standard_id and s.grade='$grade_id' and d.subject_id=st.dist_subject_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getGrades($standard_id)
	{
	
		$qry="select distinct(grade) from standarddata where  standard_id=$standard_id " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $query->result_array();		
		}else{
			return FALSE;
		}
	}
	
	function getActiveGrades($subject_id,$grade)
	{
	
		$district_id=$this->session->userdata("district_id");
		$qry="select distinct(g.grade_name) as grade,s.standard_id,d.eld,g.dist_grade_id as grade_id from standarddata sd ,standards s,dist_grades g,dist_subjects d where sd.grade='$grade' and s.subject_id=d.dist_subject_id and sd.grade=g.dist_grade_id and s.district_id=$district_id and s.status=1 and s.subject_id=$subject_id and s.standard_id=sd.standard_id ;" ;
		
		$query = $this->db->query($qry);

		
	if($query->num_rows()>0){
			$row = $query->row();
			return $query->result_array();		
		}else{
			return FALSE;
		}
	}
	function getstrand($grade,$standard_id)
	{
	
		
		$qry="select distinct(strand) from standarddata where  standard_id=$standard_id and grade='$grade';" ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $query->result_array();		
		}else{
			return FALSE;
		}
	}
	function getstandard($strand,$grade,$standard_id)
	{
	
		
		$qry="select distinct(standard),standarddata_id from standarddata where  standard_id=$standard_id  and  grade='$grade' and strand='$strand' ;" ;
		
		$query = $this->db->query($qry);

		if($query->num_rows()>0){
			$row = $query->row();
			return $query->result_array();		
		}else{
			return FALSE;
		}
	}
	
	
	
	function getstandarddata($page,$per_page,$standard_id,$grade_id)
	{
		
		$district_id=$this->session->userdata("district_id");
		$page -= 1;
		$start = $page * $per_page;
		
		if($grade_id=='all')
		{
		$qry="Select st.subject_name as subject,s.standarddata_id,g.grade_name as grade,s.strand,s.standard  from standarddata s,standards d ,dist_grades g ,dist_subjects st where  s.standard_id=d.standard_id and s.standard_id=$standard_id and s.grade=g.dist_grade_id and g.district_id=$district_id and d.subject_id=st.dist_subject_id  limit $start, $per_page ";
		}
		else
		{		
		$qry="Select st.subject_name as subject,s.standarddata_id,g.grade_name as grade,s.strand,s.standard  from standarddata s,standards d ,dist_grades g ,dist_subjects st where  s.standard_id=d.standard_id and s.standard_id=$standard_id and s.grade=g.dist_grade_id and g.district_id=$district_id and s.grade='$grade_id' and d.subject_id=st.dist_subject_id   limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
	
	function getstandarddataById($dist_id)
	{
		
		$qry = "Select s.standarddata_id,s.grade,s.strand,s.standard  from standarddata s where  s.standarddata_id=".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	
	function add_standarddata()
	{
	 
		$grade=mysql_real_escape_string(trim($this->input->post('grade')));
		$strand=mysql_real_escape_string(trim($this->input->post('strand')));
		$standard=mysql_real_escape_string(trim($this->input->post('standard')));
	 $data = array('grade' => $grade, 
					'strand'=> $strand,
					'standard'=> $standard,
					'standard_id'=>$this->input->post('standard_id')
		
		
		);
	   try{
			$str = $this->db->insert_string('standarddata', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function delete($dist_id)
	{
		$qry = "delete from standarddata where standarddata_id=$dist_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	function update_standarddata()
	{
	
		$standarddata_id=$this->input->post('standarddata_id');
		$grade=mysql_real_escape_string(trim($this->input->post('grade')));
		$strand=mysql_real_escape_string(trim($this->input->post('strand')));
		$standard=mysql_real_escape_string(trim($this->input->post('standard')));
	 $data = array('grade' => $grade, 
					'strand'=> $strand,
					'standard'=> $standard
					
		
		
		);
		
		
		
	
	
	
		
		
	
		
			
		$where = "standarddata_id=".$standarddata_id;		
		
		try{
			$str = $this->db->update_string('standarddata', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	
}