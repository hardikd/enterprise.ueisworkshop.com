<?php
class Teacher_grade_subjectmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getteacher_grade_subjectCount($school_id)
	{
	
		$district_id=$this->session->userdata('district_id');
		if($school_id=='all')
		{			
		
		 $qry="Select count(*) as count
	  			from teacher_grade_subjects s,schools sc,teachers t,dist_grades g,dist_subjects d,grade_subjects gs where s.teacher_id=t.teacher_id and t.school_id=sc.school_id  and s.grade_subject_id=gs.grade_subject_id and gs.grade_id=g.dist_grade_id and gs.subject_id=d.dist_subject_id and sc.district_id=$district_id " ;
		
		
			
		}
		else
		{
		 $qry="Select count(*) as count
	  			from teacher_grade_subjects s,schools sc,teachers t,dist_grades g,dist_subjects d,grade_subjects gs where s.teacher_id=t.teacher_id and t.school_id=sc.school_id  and s.grade_subject_id=gs.grade_subject_id and gs.grade_id=g.dist_grade_id and gs.subject_id=d.dist_subject_id and sc.district_id=$district_id and sc.school_id=$school_id  " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getteacher_grade_subjects($page,$per_page,$school_id)
	{
		
		
		$district_id=$this->session->userdata('district_id');
		if($school_id=='all')
		{			
		
		 $qry="Select s.teacher_grade_subject_id,t.firstname,t.lastname,g.grade_name,g.dist_grade_id,d.subject_name,sc.school_name
	  			from teacher_grade_subjects s,schools sc,teachers t,dist_grades g,dist_subjects d,grade_subjects gs where s.teacher_id=t.teacher_id and t.school_id=sc.school_id  and s.grade_subject_id=gs.grade_subject_id and gs.grade_id=g.dist_grade_id and gs.subject_id=d.dist_subject_id and sc.district_id=$district_id " ;
		
		
			
		}
		else
		{
		 $qry="Select s.teacher_grade_subject_id,t.firstname,t.lastname,g.grade_name,d.subject_name,sc.school_name
	  			from teacher_grade_subjects s,schools sc,teachers t,dist_grades g,dist_subjects d,grade_subjects gs where s.teacher_id=t.teacher_id and t.school_id=sc.school_id  and s.grade_subject_id=gs.grade_subject_id and gs.grade_id=g.dist_grade_id and gs.subject_id=d.dist_subject_id and sc.district_id=$district_id and sc.school_id=$school_id" ;
		}
		if($page && $per_page){
			$page -= 1;
			$start = $page * $per_page;
			$qry.=" limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
//                echo $this->db->last_query();exit;
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function staff_teacher_grade($grade_id=false)
	{
		$grade='';
		if($grade_id!=false)
		{
			$grade="and dg.dist_grade_id=$grade_id ";
			
		}	
		$school_id=$this->session->userdata('school_id');
	    $qry="select t.* from teachers t,teacher_grade_subjects tgs,grade_subjects gs,dist_grades dg  where t.school_id=$school_id and t.teacher_id=tgs.teacher_id and tgs.grade_subject_id=gs.grade_subject_id and gs.grade_id=dg.dist_grade_id $grade group by t.teacher_id  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	function staff_parent_grade($grade_id=false)
	{

		$grade='';
		if($grade_id!=false)
		{
			$grade="and dg.dist_grade_id=$grade_id ";
			
		}
		if($this->session->userdata('login_type')=='teacher')
		{	
		$school_id=$this->session->userdata('school_id');
		
			    $qry="select p.* from parents p,teacher_students ts,students s,dist_grades dg  where s.school_id=$school_id and ts.student_id=s.student_id and s.parents_id=p.parents_id and s.grade=dg.dist_grade_id $grade group by p.parents_id  ";
		}else{
			$district_id = $this->session->userdata('district_id');
		
		$qrydist = "Select s.school_id from schools s where  s.district_id=$district_id  ";
		$querydist = $this->db->query($qrydist);
		$resultdist = $querydist->result();
//		print_r($resultdist);exit;
		foreach($resultdist as $school){
			$schoolid[] = 	$school->school_id;
		}
		$schoolids = implode(",",$schoolid);
		
	    $qry="select p.* from parents p,teacher_students ts,students s,dist_grades dg  where s.school_id IN ($schoolids) and ts.student_id=s.student_id and s.parents_id=p.parents_id and s.grade=dg.dist_grade_id $grade group by p.parents_id  ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	function staff_parent_class_grade()
	{
		
		$pdata=$this->input->post('pdata');
		$grade=$pdata['classgradebox'];
		$day=$pdata['daybox'];
		$classroom=$pdata['classroombox'];
		$period=$pdata['periodbox'];
		
			
			
		$school_id=$this->session->userdata('school_id');
	    $qry="select p.* from parents p,teacher_students ts,students s,dist_grades dg  where s.school_id=$school_id and ts.student_id=s.student_id and s.parents_id=p.parents_id and s.grade=dg.dist_grade_id and dg.dist_grade_id=$grade and ts.class_room_id=$classroom and ts.period_id=$period and ts.day=$day group by p.parents_id  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	function search_staff_parent_class_grade()
	{
		
		$pdata=$this->input->post('pdata');
		$grade=$pdata['grade'];
		$firstname=$pdata['firstname'];
		$lastname=$pdata['lastname'];
		$search='';
		if($firstname!='')
		{
		
			$search.=" and s.firstname like '%$firstname%' ";
		}
		if($lastname!='')
		{
		
			$search.=" s.lastname like '%$lastname%' ";
		}
			
			
		$school_id=$this->session->userdata('school_id');
	    $qry="select p.*,concat(s.firstname,' ',s.lastname,'(',s.student_number,')') as studentname from parents p,students s,dist_grades dg  where s.school_id=$school_id  and s.parents_id=p.parents_id and s.grade=dg.dist_grade_id and dg.dist_grade_id=$grade  $search  group by p.parents_id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	function getteacher_grade_subjectById($teacher_grade_subject_id)
	{
		
		$qry = "Select gs.grade_id ,ts.grade_subject_id,ts.teacher_grade_subject_id,ts.teacher_id,s.school_id from teacher_grade_subjects ts ,grade_subjects gs,teachers s where ts.grade_subject_id=gs.grade_subject_id and ts.teacher_id=s.teacher_id and ts.teacher_grade_subject_id=".$teacher_grade_subject_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function check_teacher_grade_subject_exists($name=false,$username=false,$school_id=false)
	{
	
		if($name==false)
		{
		
		$teacher_id=$this->input->post('teacher_id');
		$grade_subject_id=$this->input->post('grade_subject_id');
		}
		$qry="SELECT  teacher_grade_subject_id from teacher_grade_subjects where teacher_id=$teacher_id and grade_subject_id=$grade_subject_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						return 0;

			
		}else{
			
			
		  return 1;
		
		}
	
	
	}
	function check_teacher_grade_subject_update() {
	
	
		$teacher_id=$this->input->post('teacher_id');
		$grade_subject_id=$this->input->post('grade_subject_id');
		$id=$this->input->post('teacher_grade_subject_id');
		
		$qry="SELECT teacher_grade_subject_id from teacher_grade_subjects where teacher_id=$teacher_id and grade_subject_id=$grade_subject_id and teacher_grade_subject_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	}
	function add_teacher_grade_subject()
	{
	  $data = array('teacher_id' => $this->input->post('teacher_id'),
					'grade_subject_id' => $this->input->post('grade_subject_id'),                    
					'created'=>date('Y-m-d H:i:s')
		
		);
	   try{
			$str = $this->db->insert_string('teacher_grade_subjects', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function update_teacher_grade_subject()
	{
	
		
		$teacher_grade_subject_id=$this->input->post('teacher_grade_subject_id');
			
			  $data = array('teacher_id' => $this->input->post('teacher_id'),
					'grade_subject_id' => $this->input->post('grade_subject_id')
		
		);
		
		
			
		$where = "teacher_grade_subject_id=".$teacher_grade_subject_id;		
		
		try{
			$str = $this->db->update_string('teacher_grade_subjects', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteteacher_grade_subject($teacher_grade_subject_id)
	{
		$qry = "delete from teacher_grade_subjects where teacher_grade_subject_id=$teacher_grade_subject_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}

	function teacher_grade_subject_add($grade_subject_id)
	{
	  $data = array('teacher_id' => $this->session->userdata('teacher_id'),
					'grade_subject_id' => $grade_subject_id,                    
					'created'=>date('Y-m-d H:i:s')
		
		);
	   try{
			$str = $this->db->insert_string('teacher_grade_subjects', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	
	
	
}