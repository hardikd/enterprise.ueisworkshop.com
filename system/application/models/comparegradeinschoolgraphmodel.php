<?php
class Comparegradeinschoolgraphmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
 
 
  function getclassrooms($grade_id,$school_id)
   {
	   $qry="select * from class_rooms where school_id=".$school_id." and grade_id=".$grade_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
  
   
    function getcategoryandgrade($district_id)
   {
	   $qry='select * from cats where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
	
	   $array = array();
		if($query->num_rows()>0){
			  $array['catarray'] = $query->result_array();
		} 
	 
	  $qry='select * from schools where district_id="'.$district_id.'"';
	    $query = $this->db->query($qry);
		 
	   if($query->num_rows()>0){
			  $array['schoolarray'] = $query->result_array();
		}
		
		$qry='select * from  dist_grades where district_id ="'.$district_id.'"';
	    $query = $this->db->query($qry);
		 
	   if($query->num_rows()>0){
			  $array['gradearray'] = $query->result_array();
		}
		
	//		 echo "<pre>";
		//print_r($array); exit;
		return $array;
		
  }
   
    function getdistrictgrade($district_id)
    {
	   $qry='select * from  dist_grades where district_id ="'.$district_id.'"';
	   $query = $this->db->query($qry);
	   	if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }  
 
 
 	function getGraphData($district_id,$cat_id,$grade_id,$school_id,$fDate,$tDate)
	{
		 error_reporting(0);
	  if($grade_id != '-1' || $cat_id != '-1' || $school_id!= '-1')	
	  {
	  // echo $cat_id;
	 	if($grade_id == 0)
		{
			$and = "";
		}
		else 
		{
			$and = "and u.grade_id = ".$grade_id;
		}
			$query_testname  = "select distinct a.assignment_name from assignments a, quizzes qz, assignment_users au , users u where a.quiz_id=qz.id AND qz.cat_id =".$cat_id." and a.id = au.assignment_id and au.user_id = u.UserID ".$and." and u.school_id = ".$school_id." and u.district_id = ".$district_id." order by a.assignment_name";
	
		$query = $this->db->query($query_testname);
		$result = array();
		$result = $query->result_array();
		$testname =array();
		$rec =  count($result);
	//	echo "<pre>";
	//	print_r($result);exit;
		if($rec > 0)
		{ 
	 		$IntCount=0;
			foreach($result as $key => $value)
			{			
				$testname['tname'][] = $value['assignment_name'];
			//	$testname['tid'][] = $value['id'];
			}
 			if($grade_id == 0)
			{
				$gradearray = $this->getdistrictgrade($district_id);
			}
			else
			{
				$classroomarray = $this->getclassrooms($grade_id,$school_id);
			}
	 		$arrResult = array();
			if($grade_id == 0 && count($gradearray) > 0)
			{
				foreach($gradearray as $key=>$value)
				{
					for($intTest=0;$intTest<count($testname['tname']);$intTest++)
					{ 
						$testNAME ='';
					 	$testNAME = $testname['tname'][$intTest];
						$arrResult[$testNAME][$value['grade_name']] = $this->getAvgScoreAllByTestName($grade_id,$value['dist_grade_id'],$testname['tname'][$intTest],$district_id,$school_id,$fDate,$tDate);
						//print_r($arrResult);exit;
					 }
	 			}
	 	//echo"<pre>"; print_r($arrResult); exit;
				return $arrResult;
			}
			
			else if($grade_id > 0 && count($classroomarray) > 0)
			{ 
				foreach($classroomarray as $key=>$value)
				{  
					for($intTest=0;$intTest<count($testname['tname']);$intTest++)
					{ 
						$testNAME ='';
					 	$testNAME = $testname['tname'][$intTest];
						$arrResult[$testNAME][$value['name']] = $this->getAvgScoreAllByTestName($grade_id,$value['class_room_id'],$testname['tname'][$intTest],$district_id,$school_id,$fDate,$tDate);
		 			}
					//$arrResult[$IntCount]['display_name'] = $value['school_name'];
				}
				 return $arrResult;
			}
			
		}
		else
		{
			return false;
		}
	  }
	  else
	  {
	  	return false;
	   }
	}
	
	function getAvgScoreAllByTestName($grade_id,$gradeOrclassid,$test_name,$district_id,$school_id,$fDate,$tDate)
	{	$and ='';
		if($grade_id == 0)
		{
		   $and = "and u.grade_id = ".$gradeOrclassid." and u.school_id = ".$school_id." and u.district_id=".$district_id;
			$anduid = "user_id in (SELECT UserID from users where grade_id = ".$gradeOrclassid." and school_id = ".$school_id." and district_id=".$district_id.")";   // gradeid-
		}
		else
		{  // classid
			$and = "and u.student_id in (SELECT student_id from teacher_students where class_room_id = ".$gradeOrclassid.") and u.school_id = ".$school_id." and u.district_id=".$district_id;
			$anduid = "user_id in (SELECT uu.UserID from users uu ,teacher_students ts where uu.student_id = ts.student_id and  ts.class_room_id = ".$gradeOrclassid." and uu.school_id = ".$school_id." and uu.district_id=".$district_id.")";
		}
		
  	$query_score = "SELECT avg(pass_score_perc) as avgscore FROM `user_quizzes` where  `assignment_id` in (SELECT a.id  FROM assignments a, assignment_users au , users u where a.id = au.assignment_id and au.user_id = u.UserID and a.assignment_name ='".$test_name."' and u.user_type=2 ".$and." ) and ".$anduid." and pass_score_perc IS NOT NULL AND DATE(finish_date) between '".$fDate."' ANd '".$tDate."' ORDER BY id ";

		 $query = $this->db->query($query_score);
		 $result = array();
		if($query->num_rows()>0){
			$result = $query->result_array();
		//	echo $result[0]['avgscore'];
			return $result[0]['avgscore'];
		}else{
			return false;
		}  
 	
	}
	
	
	 
  

}
?>