<?php
class Countrymodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	function getCountries()
	{
	
		$qry="Select * from countries " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	function getStates($id)
	{
	
		$qry="Select * from states where country_id=$id " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
}	