<?php
class Importdatamodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
  
 	function getallschools()
   {
	   $dist_id = $this->session->userdata("district_id");
	 	$qry= "select Distinct school_name from archive_student_result_data where district_id = ".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
	function getassessment($school_id)
   {
	   	$dist_id = $this->session->userdata("district_id");
   		if($school_id == '0')
		$where = "where district_id = ".$dist_id;
		else
		$where = "where school_name ='".$school_id."' and district_id = ".$dist_id;
	  $qry= "select distinct assessment_name from archive_student_result_data ".$where." order by a_order ASC"; 
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
	
   function getTestItem($school_id,$ass_id,$fDate,$tDate)
   {
	    $dist_id = $this->session->userdata("district_id");
		if($ass_id == '0')
		$andAss = '';
		else
		$andAss  = " AND assessment_name='".$ass_id."'";
		if($school_id == '0')
		 $andschool  = "";
		 else
		 $andschool = " and school_name='".$school_id."'";

		$qry= "select Distinct cluster_item from archive_student_result_data where DATE(a_date) between '".$fDate."' And '".$tDate."'".$andAss."".$andschool." and district_id = $dist_id"; 
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
 
  	
	
   function getgrades($school_name)
   {	
	  $dist_id = $this->session->userdata("district_id");
  		if($school_name == '0')
		 $andschool  = "";
		 else
		 $andschool = " where school_name='".$school_name."'";
		 
		 $qry= "select Distinct grade from archive_student_result_data".$andschool;
	 	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   function getstudents($school_name,$ass_id,$fDate,$tDate)
   {	
   		 $dist_id = $this->session->userdata("district_id");
  		 $andDate = " AND DATE(a_date) between '".$fDate."' And '".$tDate."' ";
  	 $qry= "select distinct stud_roll_no , first_name,last_name from archive_student_result_data where school_name='".$school_name."' and assessment_name = '".$ass_id."'".$andDate." and district_id = ".$dist_id;
	 	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
	function getPercentageByGradeAndTestCluster($school_id,$grade,$cluster,$fDate,$tDate,$ass_id)
    {
		 $dist_id = $this->session->userdata("district_id");
 		$andDate = " AND DATE(a_date) between '".$fDate."' And '".$tDate."' ";
		$andAss = '';$andschool  = "";
 
		if($ass_id == '0')
		$andAss = '';
		else
		$andAss  = " AND assessment_name='".$ass_id."'";
		if($school_id == '0')
		 $andschool  = "";
		 else
		 $andschool = " and school_name='".$school_id."'";
  
 	$qry= "select AVG(marks) as marks from archive_student_result_data where cluster_item = '".$cluster."' And grade= '".$grade."' ".$andDate."".$andAss."".$andschool." and district_id = ".$dist_id;  	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
	 }
	
	
	 
} // class end

?>