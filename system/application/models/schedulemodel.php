<?php
class Schedulemodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}

function getschedulesbyteacher($teacher_id=false,$fromdate,$todate)
	{
	  if($teacher_id==false)
	  {
		$teacher_id=$this->session->userdata('teacher_id');
			
	  }	
	
	  $qry = "Select w.task as task_id,s.task,w.comments,w.schedule_week_plan_id,w.starttime,w.endtime,date_format(w.date,'%m-%d-%Y') as date from schedule_week_plans w,scheduletasks s where w.teacher_id=$teacher_id and w.date>='$fromdate' and w.date<='$todate' and w.task=s.schedule_task_id  order by w.date ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function check_plan_exists()
	{
	    
		if($this->session->userdata('login_type')=='teacher')
	  {
		$teacher_id=$this->session->userdata('teacher_id');
		$role_id=$this->session->userdata('teacher_id');
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
        $role_id=$this->session->userdata('observer_id');
		$teacher_id=$this->session->userdata("teacher_observer_id");
		

	  }	
		
		$error=array();
		for($i=1;$i<=5;$i++)
		{
		if($this->input->post("date$i") && $this->input->post("start$i") && $this->input->post("end$i") && $this->input->post("task$i") && $this->input->post("comments$i")   ) 
		{
		$dates=$this->input->post("date$i");
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
	  $task=$this->input->post("task$i");
		$start=$this->input->post("start$i");
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 
		 
		 }
		 else
		 {
		 
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	   $end=$this->input->post("end$i");
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 
		 }
		 else
		 {
		 
			$end3=$end2[0]+12;
		 }
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
		
    
	/*if($end2[0]==12 && $start2[0]!=12)
	{
	if($endtime>$starttime)
	{

		
		return 2;

	}
	}	 
	else if($start2[0]==12 && $end2[0]==12 )
	{
		if($starttime>=$endtime)
	{

		
		return 2;

	}
	}
	else if($start2[0]!=12 && $end2[0]!=12 )
	{
		if($starttime>=$endtime)
	{

		
		return 2;

	}
	}
	
	*/
	if($starttime>=$endtime)
	{

		$error[$i]=2;
		return $error;

	}
	if($date<date('Y-m-d'))
	{

		
		$error[$i]=3;
		return $error;

	}
		
		
		
		
		$qry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and teacher_id=$teacher_id ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						$error[$i]=0;
						return $error;

			
		}
			
		$sqry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND starttime>='$starttime' and teacher_id=$teacher_id  ";
		
		$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
						$jqry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND endtime<='$endtime' and teacher_id=$teacher_id ";
		
					$jquery = $this->db->query($jqry);
					if($jquery->num_rows()>0){
					
						$error[$i]=0;
						return $error;
						
						}

			
		}

        //start temorary tables

			
			$tempqry="SELECT  schedule_week_plan_id from temp_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and teacher_id=$teacher_id ; ";
		
		$tempquery = $this->db->query($tempqry);
		if($tempquery->num_rows()>0){
		
						$error[$i]=0;
						return $error;

			
		}
			
		$tempsqry="SELECT  schedule_week_plan_id from temp_week_plans where date='$date'  AND starttime>='$starttime' and teacher_id=$teacher_id  ";
		
		$tempsquery = $this->db->query($tempsqry);
		if($tempsquery->num_rows()>0){
		
						$tempjqry="SELECT  schedule_week_plan_id from temp_week_plans where date='$date'  AND endtime<='$endtime' and teacher_id=$teacher_id ";
		
					$tempjquery = $this->db->query($tempjqry);
					if($tempjquery->num_rows()>0){
					
						$error[$i]=0;
						return $error;
						
						}

			
		}
		
		// check periodic goal setting exists
			if($task==3)
			{
			$pyear=date('Y');
			if($date1[2]!=$pyear)
			{
				$error[$i]=5;
				return $error;
			
			}
			
			  $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where date_format(created,'%Y')='$pyear'  AND task=3 and teacher_id=$teacher_id ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					if($periodicjquery->num_rows()>0){
					
						$error[$i]=4;
						return $error;
						
						}
			
			  $periodictempjqry="SELECT  schedule_week_plan_id from temp_week_plans where  task=3 and teacher_id=$teacher_id ";
		    
					$periodictempjquery = $this->db->query($periodictempjqry);
					if($periodictempjquery->num_rows()>0){
					
						$error[$i]=6;
						return $error;
						
						}
			
			
			 }
			 
			 
			//end periodic goal setting exists
			
			// check observation plan exists
			if($task==2)
			{
			
			  $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where task=2 and  teacher_id=$teacher_id  ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					if($periodicjquery->num_rows()>0){
					    
						$temp=$periodicjquery->result_array();
						foreach($temp as $value)
						{
						$sid=$value['schedule_week_plan_id'];
						$tempquery="SELECT  archived from observation_ans  where schedule_week_plan_id=$sid ";	
						$tempqueryob = $this->db->query($tempquery);
					if($tempqueryob->num_rows()>0){
					    $temp1=$tempqueryob->result_array();
						if($temp1[0]['archived']=='')
						{
						  $error[$i]=7;
						  return $error;
						}
						
					}else
					{
					   $error[$i]=7;
					   return $error;
					
					}
						}	
						
						}
			
			  $periodictempjqry="SELECT  schedule_week_plan_id from temp_week_plans where  task=2 and teacher_id=$teacher_id ";
		    
					$periodictempjquery = $this->db->query($periodictempjqry);
					if($periodictempjquery->num_rows()>0){
					
						$error[$i]=8;
						return $error;
						
						}
			
			
			 }
			//end observation plan exists
			if($task==1)
			{
				$qry="SELECT  lesson_week_plan_id from lesson_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and teacher_id=$teacher_id ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						$error[$i]=0;
						return $error;

			
		}
			
		 $sqry="SELECT  lesson_week_plan_id from lesson_week_plans where date='$date'  AND starttime>='$starttime'  and teacher_id=$teacher_id ";
		
		$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
						$jqry="SELECT  lesson_week_plan_id from lesson_week_plans where date='$date'  AND endtime<='$endtime' and teacher_id=$teacher_id  ";
		
					$jquery = $this->db->query($jqry);
					if($jquery->num_rows()>0){
					
						$error[$i]=0;
						return $error;
						
						}

			
		}
			
			
			
			
			
			
			
			
			
			
			
			
			}
			
			$inqry="insert into temp_week_plans 
	(starttime, 
	endtime, 
	teacher_id,
	role_id,	
	date,
	task
	)
	values
	('$starttime', 
	'$endtime', 
	$teacher_id,
    $role_id,	
	'$date',
	$task
	);";
		$inquery = $this->db->query($inqry);
		
		
		
		  
		 }  
		}  
		  return 1;
		
		
	
	
	
	
	
	
	}
	function check_plan_exists_all()
	{
	    
		if($this->session->userdata('login_type')=='teacher')
	  {
		//$teacher_id=$this->session->userdata('teacher_id');
		$role_id=$this->session->userdata('teacher_id');
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
        $role_id=$this->session->userdata('observer_id');
		//$teacher_id=$this->session->userdata("teacher_observer_id");
		

	  }	
		
		$error=array();
		for($i=1;$i<=5;$i++)
		{
		if($this->input->post("date$i") && $this->input->post("start$i") && $this->input->post("end$i") && $this->input->post("task$i") && $this->input->post("comments$i") && $this->input->post("teacher$i")   ) 
		{
		
		$teacher_id=$this->input->post("teacher$i");
		$dates=$this->input->post("date$i");
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
	  $task=$this->input->post("task$i");
	  
		$start=$this->input->post("start$i");
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 
		 
		 }
		 else
		 {
		 
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	   $end=$this->input->post("end$i");
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 
		 }
		 else
		 {
		 
			$end3=$end2[0]+12;
		 }
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
		
    
	/*if($end2[0]==12 && $start2[0]!=12)
	{
	if($endtime>$starttime)
	{

		
		return 2;

	}
	}	 
	else if($start2[0]==12 && $end2[0]==12 )
	{
		if($starttime>=$endtime)
	{

		
		return 2;

	}
	}
	else if($start2[0]!=12 && $end2[0]!=12 )
	{
		if($starttime>=$endtime)
	{

		
		return 2;

	}
	}
	
	*/
	if($starttime>=$endtime)
	{

		$error[$i]=2;
		return $error;

	}
	if($date<date('Y-m-d'))
	{

		
		$error[$i]=3;
		return $error;

	}
		
		
		
		
		$qry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and teacher_id=$teacher_id ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						$error[$i]=0;
						return $error;

			
		}
			
		$sqry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND starttime>='$starttime' and teacher_id=$teacher_id  ";
		
		$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
						$jqry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND endtime<='$endtime' and teacher_id=$teacher_id ";
		
					$jquery = $this->db->query($jqry);
					if($jquery->num_rows()>0){
					
						$error[$i]=0;
						return $error;
						
						}

			
		}

        //start temorary tables

			
			$tempqry="SELECT  schedule_week_plan_id from temp_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and teacher_id=$teacher_id ; ";
		
		$tempquery = $this->db->query($tempqry);
		if($tempquery->num_rows()>0){
		
						$error[$i]=0;
						return $error;

			
		}
			
		$tempsqry="SELECT  schedule_week_plan_id from temp_week_plans where date='$date'  AND starttime>='$starttime' and teacher_id=$teacher_id  ";
		
		$tempsquery = $this->db->query($tempsqry);
		if($tempsquery->num_rows()>0){
		
						$tempjqry="SELECT  schedule_week_plan_id from temp_week_plans where date='$date'  AND endtime<='$endtime' and teacher_id=$teacher_id ";
		
					$tempjquery = $this->db->query($tempjqry);
					if($tempjquery->num_rows()>0){
					
						$error[$i]=0;
						return $error;
						
						}

			
		}
			
			// check periodic goal setting exists
			if($task==3)
			{
			$pyear=date('Y');
			if($date1[2]!=$pyear)
			{
				$error[$i]=5;
				return $error;
			
			}
			  $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where date_format(created,'%Y')='$pyear'  AND task=3 and teacher_id=$teacher_id ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					if($periodicjquery->num_rows()>0){
					
						$error[$i]=4;
						return $error;
						
						}
			
			  $periodictempjqry="SELECT  schedule_week_plan_id from temp_week_plans where  task=3 and teacher_id=$teacher_id ";
		    
					$periodictempjquery = $this->db->query($periodictempjqry);
					if($periodictempjquery->num_rows()>0){
					
						$error[$i]=6;
						return $error;
						
						}
			
			
			 }
			//end periodic goal setting exists
			
			// check observation plan exists
			if($task==2)
			{
			
			  $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where task=2 and  teacher_id=$teacher_id  ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					if($periodicjquery->num_rows()>0){
					    
						$temp=$periodicjquery->result_array();
						foreach($temp as $value)
						{
						$sid=$value['schedule_week_plan_id'];
						$tempquery="SELECT  archived from observation_ans  where schedule_week_plan_id=$sid ";	
						$tempqueryob = $this->db->query($tempquery);
					if($tempqueryob->num_rows()>0){
					    $temp1=$tempqueryob->result_array();
						if($temp1[0]['archived']=='')
						{
						  $error[$i]=7;
						  return $error;
						}
						
					}else
					{
					   $error[$i]=7;
					   return $error;
					
					}
						}	
						
						}
			
			  $periodictempjqry="SELECT  schedule_week_plan_id from temp_week_plans where  task=2 and teacher_id=$teacher_id ";
		    
					$periodictempjquery = $this->db->query($periodictempjqry);
					if($periodictempjquery->num_rows()>0){
					
						$error[$i]=8;
						return $error;
						
						}
			
			
			 }
			//end observation plan exists
			
			if($task==1)
			{
				$qry="SELECT  lesson_week_plan_id from lesson_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and teacher_id=$teacher_id ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						$error[$i]=0;
						return $error;

			
		}
			
		 $sqry="SELECT  lesson_week_plan_id from lesson_week_plans where date='$date'  AND starttime>='$starttime'  and teacher_id=$teacher_id ";
		
		$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
						$jqry="SELECT  lesson_week_plan_id from lesson_week_plans where date='$date'  AND endtime<='$endtime' and teacher_id=$teacher_id  ";
		
					$jquery = $this->db->query($jqry);
					if($jquery->num_rows()>0){
					
						$error[$i]=0;
						return $error;
						
						}

			
		}
			
			
			
			
			
			
			
			
			
			
			
			
			}
			
			
			$inqry="insert into temp_week_plans 
	(starttime, 
	endtime, 
	teacher_id,
	role_id,	
	date,
	task
	)
	values
	('$starttime', 
	'$endtime', 
	$teacher_id,
    $role_id,	
	'$date',
	$task
	);";
		$inquery = $this->db->query($inqry);
		
		
		
		  
		 }  
		}  
		  return 1;
		
		
	
	
	
	
	
	
	}
	
	function check_plan_update_exists()
	{
	
	
			if($this->session->userdata('login_type')=='teacher')
	  {
		$teacher_id=$this->session->userdata('teacher_id');
		
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
        
		$teacher_id=$this->session->userdata("teacher_observer_id");
		

	  }	

		$dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
	  $task=$this->input->post('task');
		$start=$this->input->post('start');
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 
		 
		 }
		 else
		 {
		 
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	   $end=$this->input->post('end');
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 
		 }
		 else
		 {
		 
			$end3=$end2[0]+12;
		 }
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
		$schedule_week_plan_id=$this->input->post('schedule_week_plan_id');
		/*if($end2[0]==12 && $start2[0]!=12)
	{
	if($endtime>$starttime)
	{

		
		return 2;

	}
	}	 
	else if($start2[0]==12 && $end2[0]==12 )
	{
		if($starttime>=$endtime)
	{

		
		return 2;

	}
	}
	else if($start2[0]!=12 && $end2[0]!=12 )
	{
		if($starttime>=$endtime)
	{

		
		return 2;

	}
	}
	*/	
	if($starttime>=$endtime)
	{

		
		return 2;

	}
	if($date<date('Y-m-d'))
	{

		
		return 3;

	}
		$qry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND ('$starttime'>starttime AND '$starttime'<endtime OR '$endtime'>starttime AND '$endtime'<endtime) and schedule_week_plan_id!=$schedule_week_plan_id and teacher_id=$teacher_id ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						return 0;

			
		}
			
		$sqry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND starttime>='$starttime' and schedule_week_plan_id!=$schedule_week_plan_id and teacher_id=$teacher_id  ";
		
		$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
						$jqry="SELECT  schedule_week_plan_id from schedule_week_plans where date='$date'  AND endtime<='$endtime'  and schedule_week_plan_id!=$schedule_week_plan_id and teacher_id=$teacher_id  ";
		
					$jquery = $this->db->query($jqry);
					if($jquery->num_rows()>0){
					
						return 0;
						
						}

			
		}	
		
		if($task==3)
			{
			$pyear=date('Y');
			if($date1[2]!=$pyear)
			{
				
				return 5;
			
			}
			  $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where date_format(created,'%Y')='$pyear'  and schedule_week_plan_id!=$schedule_week_plan_id AND task=3 and teacher_id=$teacher_id ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					if($periodicjquery->num_rows()>0){
					
						
						return 4;
						
						}
						
			}
			if($task==2)
			{
			
			  $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where task=2 and  schedule_week_plan_id!=$schedule_week_plan_id and  teacher_id=$teacher_id  ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					if($periodicjquery->num_rows()>0){
					    
						$temp=$periodicjquery->result_array();
						foreach($temp as $value)
						{
						$sid=$value['schedule_week_plan_id'];
						$tempquery="SELECT  archived from observation_ans  where schedule_week_plan_id=$sid ";	
						$tempqueryob = $this->db->query($tempquery);
					if($tempqueryob->num_rows()>0){
					    $temp1=$tempqueryob->result_array();
						if($temp1[0]['archived']=='')
						{
						  return 6;
						}
						
					}else
					{
					   return 6;
					
					}
						}	
						
						}		
			}	
		  
		  
		  return 1;
		
	
	
	
	
	}
	
	function delete_temp_plan()
	{
	  if($this->session->userdata('login_type')=='teacher')
	  {
		
		$role_id=$this->session->userdata('teacher_id');
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
        $role_id=$this->session->userdata('observer_id');
		
		

	  }	
	
	 $dqry="delete from temp_week_plans where  role_id=$role_id ";
	 $this->db->query($dqry);
	
	
	
	
	}
	function add_plan()
	{
	  
	  if($this->session->userdata('login_type')=='teacher')
	  {
		$teacher_id=$this->session->userdata('teacher_id');
		$role_id=$this->session->userdata('teacher_id');
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
        $role_id=$this->session->userdata('observer_id');
		$teacher_id=$this->session->userdata("teacher_observer_id");
		

	  }	
	  
	  $n=date('Y-m-d h:i:s');
	   for($i=1;$i<=5;$i++)
		{
		if($this->input->post("date$i") && $this->input->post("start$i") && $this->input->post("end$i") && $this->input->post("task$i") && $this->input->post("comments$i")   ) 
		{
	   $dates=$this->input->post("date$i");
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		$start=$this->input->post("start$i");
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 }
		 else
		 {
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }
	  
	  }
	   $end=$this->input->post("end$i");
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 }
		 else
		 {
			$end3=$end2[0]+12;
		 }	
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
	  $data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' =>$teacher_id ,
					'task' => $this->input->post("task$i"),
					'comments' => $this->input->post("comments$i"),
					'role_id' => $role_id,
					'role' => $this->session->userdata('login_type'),
                    'created'=>$n,					
		            'modified'=> $n	
		
		);
	   
			$str = $this->db->insert_string('schedule_week_plans', $data);
			if($this->db->query($str))
			{
				if($this->input->post("task$i")==1)
				{
					$last_id=$this->db->insert_id();
					
					$sdata = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' => $teacher_id,
					'schedule_week_plan_id'=>$last_id,
                    'created'=>$n,					
		            'modified'=> $n	
		
				);
				
				$squery=$this->db->insert_string('lesson_week_plans', $sdata);
				$this->db->query($squery);
				
				}	
			}
			else{
				return 0;
			}
		
		
		}
		}
		
		return 1;
	}
	function add_plan_all()
	{
	  
	  if($this->session->userdata('login_type')=='teacher')
	  {
		//$teacher_id=$this->session->userdata('teacher_id');
		$role_id=$this->session->userdata('teacher_id');
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
        $role_id=$this->session->userdata('observer_id');
		//$teacher_id=$this->session->userdata("teacher_observer_id");
		

	  }	
	  
	  $n=date('Y-m-d h:i:s');
	   for($i=1;$i<=5;$i++)
		{
		if($this->input->post("date$i") && $this->input->post("start$i") && $this->input->post("end$i") && $this->input->post("task$i") && $this->input->post("comments$i") && $this->input->post("teacher$i")  ) 
		{
		$teacher_id=$this->input->post("teacher$i");
	   $dates=$this->input->post("date$i");
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		$start=$this->input->post("start$i");
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 }
		 else
		 {
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }
	  
	  }
	   $end=$this->input->post("end$i");
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 }
		 else
		 {
			$end3=$end2[0]+12;
		 }	
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
	  $data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' =>$teacher_id ,
					'task' => $this->input->post("task$i"),
					'comments' => $this->input->post("comments$i"),
					'role_id' => $role_id,
					'role' => $this->session->userdata('login_type'),
                    'created'=>$n,					
		            'modified'=> $n	
		
		);
	   
			$str = $this->db->insert_string('schedule_week_plans', $data);
			if($this->db->query($str))
			{ 
			  if($this->input->post("task$i")==1)
				{
					$last_id=$this->db->insert_id();
					
					$sdata = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' => $teacher_id,
					'schedule_week_plan_id'=>$last_id,
                    'created'=>$n,					
		            'modified'=> $n	
		
				);
				
				$squery=$this->db->insert_string('lesson_week_plans', $sdata);
				$this->db->query($squery);
				
				}
				
			}
			else{
				return 0;
			}
		
		
		}
		}
		
		return 1;
	}
	function update_plan()
	{
	
		
		$schedule_week_plan_id=$this->input->post('schedule_week_plan_id');
		 if($this->session->userdata('login_type')=='teacher')
	  {
		$teacher_id=$this->session->userdata('teacher_id');
		$role_id=$this->session->userdata('teacher_id');
	  }
	else if($this->session->userdata('login_type')=='observer')
	  {
        $role_id=$this->session->userdata('observer_id');
		$teacher_id=$this->session->userdata('teacher_observer_id');

	  }		  
	  $n=date('Y-m-d h:i:s');
	   $dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		$start=$this->input->post('start');
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 }
		 else
		 {
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }
	  
	  }
	   $end=$this->input->post('end');
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 }
		 else
		 {
			$end3=$end2[0]+12;
		 }	
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
	  $data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' =>$teacher_id ,
					'task' => $this->input->post('task'),
					'comments' => $this->input->post('comments'),
					'role_id' => $role_id,
					'role' => $this->session->userdata('login_type'),
                    'modified'=> $n	
		
		);
			
			
		$where = "schedule_week_plan_id=".$schedule_week_plan_id;		
		
		try{
			$str = $this->db->update_string('schedule_week_plans', $data,$where);
			if($this->db->query($str))
			{
				return 1;
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteplan($plan_id)
	{
		 $sqry=" select date_format(date,'%m-%d-%Y') as date from schedule_week_plans where schedule_week_plan_id=$plan_id";
	 $squery = $this->db->query($sqry);
	 
		$qry = "delete from schedule_week_plans where schedule_week_plan_id=$plan_id";
		$query = $this->db->query($qry);
		$qry = "delete from lesson_week_plans where schedule_week_plan_id=$plan_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return $squery->result_array();
		}else{
			return false;
		}
	}
	
	function getscheduleplaninfo($schedule_week_plan_id)
	{
	
	  //$teacher_id=$this->session->userdata('teacher_id');
	  $qry = "Select w.schedule_week_plan_id,w.starttime,w.endtime,date_format(w.date,'%m-%d-%Y') as date,w.task,w.comments,w.role_id from schedule_week_plans w where  w.schedule_week_plan_id=$schedule_week_plan_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallscheduleplans($fromdate,$todate)
	{
	   if($this->session->userdata('login_type')=='teacher')
	  {
		$teacher_id=$this->session->userdata('teacher_id');
	  }	
	  else if($this->session->userdata('login_type')=='observer')
	  {
        
		$teacher_id=$this->session->userdata('teacher_observer_id');

	  }	
	
	  $qry = "Select w.comments,w.schedule_week_plan_id,w.starttime,w.endtime,date_format(w.date,'%m-%d-%Y') as date,s.task,w.comments from schedule_week_plans w,scheduletasks s where w.teacher_id=$teacher_id  and w.date>='$fromdate' and w.date<='$todate' and w.task=s.schedule_task_id order by  w.date  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	}
	function getschedulesasignedcount($school_id,$teacher_id)
	{
	
	 if($teacher_id=='all')
		{
			$qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  " ;
		}
		else
		{
			 $qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  and t.teacher_id=$teacher_id  " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function getschedulesasigned($page, $per_page,$school_id,$teacher_id)
	{
	    $page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		if($teacher_id=='all')
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id $limit " ;
		}
		else
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id  and t.teacher_id=$teacher_id $limit " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
}

?>	