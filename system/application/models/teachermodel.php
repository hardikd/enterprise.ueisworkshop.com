<?php
class Teachermodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function is_valid_login($id=false){
		
		if($id==false)
		{
		$username = $this->input->post('username');
		if($username!='')
		{
		$password = md5(trim($this->input->post('password')));
		$qry="SELECT ob.*,s.*,d.districts_name from teachers ob,schools s,districts d  WHERE ob.username='".$username."' AND ob.password='".$password."' and ob.school_id=s.school_id and s.district_id=d.district_id ";
		//$qry="SELECT teacher_id,firstname,lastname,school_id FROM teachers WHERE username='".$username."' AND password='".$password."' ";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		
		}
		else
		{
		  return false;
		
		}
		
		}
		else
		{
		$qry="SELECT ob.*,s.*,d.districts_name from teachers ob,schools s,districts d  WHERE ob.teacher_id=$id and ob.school_id=s.school_id and s.district_id=d.district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		
		
		}
	}
	function getteacherCount($school_id)
	{
	
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		 $qry="Select count(*) as count 
	  			from teachers t,schools s where t.school_id=s.school_id and s.district_id=$district_id " ;
		
		}
		else
		{
		
			$qry="Select count(*) as count 
	  			from teachers " ;
		}
			
		}
		else
		{
		 $qry="Select count(*) as count 
	  			from teachers  where school_id=$school_id" ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	function getteacherCountbydistrict($state_id,$country_id,$district_id,$school_id)
	{
	
		if($school_id=='all')
		{
			 $qry="Select count(*) as count 
	  			from teachers t,schools s,districts d where d.district_id=s.district_id and s.school_id=t.school_id and d.district_id=$district_id and d.state_id=$state_id and  d.country_id=$country_id " ;
		}
		else
		{
			$qry="Select count(*) as count 
	  			from teachers t,schools s,districts d where d.district_id=s.district_id and s.school_id=t.school_id and d.district_id=$district_id and d.state_id=$state_id and  d.country_id=$country_id and s.school_id=$school_id " ;
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getteachers($page,$per_page,$school_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
			$district_id=$this->session->userdata('district_id');
		
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id and s.district_id=$district_id  limit $start, $per_page ";
		}
		else
		{
			
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id  limit $start, $per_page ";
		
		}
		}
		else
		{
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id and b.school_id=$school_id  limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getteachersbydistrict($page,$per_page,$state_id,$country_id,$district_id,$school_id)
	{

            if($page!=false && $per_page!=false){
		$page -= 1;
		$start = $page * $per_page;
            }
		if($school_id=='all')
		{
			 $qry="Select b.phone as phone,b.email as email,b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id
	  			from teachers b,schools s,districts d where d.district_id=s.district_id and s.school_id=b.school_id and d.district_id=$district_id and d.state_id=$state_id and  d.country_id=$country_id " ;
		}
		else
		{
			$qry="Select b.phone as phone,b.email as email,b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id
	  			from teachers b,schools s,districts d where d.district_id=s.district_id and s.school_id=b.school_id and d.district_id=$district_id and d.state_id=$state_id and  d.country_id=$country_id  and s.school_id=$school_id " ;
		
		}
		
                if($page!=false && $per_page!=false){
                    $qry.="limit $start, $per_page";
                }
                
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getteacherById($teacher_id)
	{
		
		$qry = "Select s.school_name,t.phone,t.teacher_id,t.firstname,t.school_id,t.lastname,t.username,t.email,t.avatar,d.country_id,d.state_id,d.district_id  from teachers t,schools s,districts d where d.district_id=s.district_id and s.school_id=t.school_id and  t.teacher_id=".$teacher_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getTeachersBySchool($school_id) {

		if($this->session->userdata("login_type")=='admin' || $school_id!=''){
			$qry = "Select * from teachers where school_id=".$school_id." ";
		}else {
			$district_id = $this->session->userdata('district_id');
			$qrydist = "Select s.school_id from schools s where  s.district_id=$district_id  ";
			$querydist = $this->db->query($qrydist);
			$resultdist = $querydist->result();
			foreach($resultdist as $school){
				$schoolid[] = 	$school->school_id;
			}
            if($this->session->userdata('school_id')){
            	$schoolids = $this->session->userdata('school_id');
            } else {
            	$schoolids = implode(",",$schoolid);
            }
			$qry = "Select * from teachers where school_id IN ($schoolids) ";
		}
        $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return '';
		}
	}
	
	function getTeachersBySchoolApi($school_id)
	{
	$qry = "Select teacher_id,firstname,lastname from teachers where school_id=".$school_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return '';
		}
	
	
	
	}
      
		

	function check_teacher_exists($name=false,$username=false,$school_id=false)
	{
	
		if($name==false)
		{
		$name=$this->input->post('firstname');
		$school_id=$this->input->post('school_id');
		$username=$this->input->post('username');
		}
		/*$qry="SELECT  teacher_id from teachers where firstname='$name' and school_id=$school_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						return 0;

			
		}
		else*/
		{
			
			$sqry="SELECT  teacher_id from teachers where username='$username'  ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
		}
	
	
	}
	function check_teacher_update() {
	
	
		$name=$this->input->post('firstname');
		$school_id=$this->input->post('school_id');
		$id=$this->input->post('teacher_id');
		$username=$this->input->post('username');
		/*$qry="SELECT teacher_id from teachers where firstname='$name' and school_id=$school_id and teacher_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else*/
		{
			$sqry="SELECT  teacher_id from teachers where username='$username' and teacher_id!=$id ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
		}
	
	}
	function add_teacher()
	{
	  $data = array('firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),	
					'school_id' => $this->input->post('school_id'),
                    'username'=>$this->input->post('username'),					
		            'password'=> md5($this->input->post('password')),
					'copy_password'=> base64_encode($this->input->post('password')),
					'email'=> $this->input->post('email'),
					'avatar'=>$this->input->post('avatar'),
              		'phone'=>$this->input->post('phone')
		
		);
	   try{
			$str = $this->db->insert_string('teachers', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function add_bulk_teacher($firstname,$lastname,$school_id,$username,$password,$employee_number,$email)
	{
	  $data = array('firstname' => $firstname,
					'lastname' => $lastname,	
					'school_id' => $school_id,
                    'username'=>$username,					
		            'password'=> md5($password),
					'emp_number'=>$employee_number,
                    'email'=>$email					
		
		);
	   try{
			$str = $this->db->insert_string('teachers', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_teacher()
	{
	
		
		$teacher_id=$this->input->post('teacher_id');
	$passwords=$this->input->post('password');
	//print_r($passwords);exit;
		if($passwords!='torvertex')
		{
		 $password=md5($passwords);
	$data = array('firstname' => $this->input->post('firstname'),
				  'lastname' => $this->input->post('lastname'),	
	               'school_id' => $this->input->post('school_id'),
				   'username' => $this->input->post('username'),
					'password'=> $password,
					'copy_password'=> base64_encode($passwords),
                    'email'=> $this->input->post('email'),
		'avatar'=>$this->input->post('avatar'),	
                'phone'=>$this->input->post('phone')
		
		
		);
		}
		else
		{
		
			$data = array('firstname' => $this->input->post('firstname'),
				  'lastname' => $this->input->post('lastname'),
					'school_id' => $this->input->post('school_id'),				  
	               'username' => $this->input->post('username'),
                    'email'=> $this->input->post('email') ,
					'avatar'=>$this->input->post('avatar'),
                     'phone'=>$this->input->post('phone')
		
		         );
		
		}
			
		$where = "teacher_id=".$teacher_id;		
		
		try{
			$str = $this->db->update_string('teachers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteteacher($teacher_id)
	{
		$qry = "delete from teachers where teacher_id=$teacher_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function pdfreportsave($filesave,$status,$score,$dates,$teacher_id,$comments)
	{
	
	 $login_type=$this->session->userdata('login_type');
             if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}
			$n=date('Y-m-d H:i:s');
	 $data = array('teacher_id' => $teacher_id,
					'score' => $score,	
					'status' => $status,
                    'dates'=>$dates,					
		            'file_path'=> $filesave,
					'type'=>$login_type,	
		            'user_id'=>$login_id,
					'comments'=>$comments,
					'created'=>$n						
		);
	   try{
			$str = $this->db->insert_string('summative', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	function changeTeacherEmail()
	
	{
	$data = array(	'emailnotifylesson'=>$this->input->post('emailnotifylesson')	
		
		
		);
		
		
		
		$teacher_id=$this->session->userdata('teacher_id');
	
		
			
		$where = "teacher_id=".$teacher_id;		
		
		try{
			$str = $this->db->update_string('teachers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function getemailnotify()
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	
	 $qry = "Select emailnotifylesson from teachers where teacher_id=".$teacher_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getTeacherObservationReport($teacher_id,$year)
	{
	  
	
	  $qry = "select count(report_id) as count ,MONTH(report_date) as month ,report_form,period_lesson from reports where teacher_id=$teacher_id and year(report_date)='$year' group by YEAR(report_date), MONTH(report_date),report_form,period_lesson union select count(report_id) as count ,MONTH(report_date) as month ,report_form,period_lesson from proficiencyreports where teacher_id=$teacher_id and year(report_date)='$year' group by YEAR(report_date), MONTH(report_date),report_form,period_lesson ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
public function getschool_byteacher_pdf($schools_id)
	{
	
/*	$qryselect = 'select * from teachers';
	$res = $this->db->query($qryselect)->result();
	foreach($res as $r){
	echo $qry = "UPDATE teachers set copy_password = '".base64_encode($r->username)."' where teacher_id = ".$r->teacher_id;
	$res1 = $this->db->query($qry);
	}exit;*/
	$this->db->select('teachers.*,schools.school_name as school_name');
	$this->db->from('teachers');
	$this->db->join('schools','teachers.school_id = schools.school_id','LEFT');
	$this->db->where('teachers.school_id',$schools_id);
	$query = $this->db->get();

        $result = $query->result();
	return $result;	
	}
	
}