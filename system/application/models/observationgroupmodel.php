<?php
class Observationgroupmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getobservationgroupCount($state_id,$country_id,$district_id)
	{
		
		
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from observation_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id " ;
		}
		else
		{
			$qry="Select count(*) as count 
	  			from observation_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id and  og.district_id=$district_id " ;
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getobservationgroups($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		if($district_id=='all')
		{
		$qry="Select s.name,og.group_id,og.group_name,og.description,d.districts_name from observation_groups og,districts d,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id $limit ";
		
		}
		else
		{
			$qry="Select s.name,og.group_id,og.group_name,og.description,d.districts_name from observation_groups og,districts d ,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id and og.district_id=$district_id $limit ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getobservationgroupById($val);
			  
			  
			  if($data!=false && $data[0]['district_id']!=$pdata['end'])
			  {
					$status=1;	
				   $sdata = array('group_name' => $data[0]['group_name'],
					'description' => $data[0]['description'],
					'district_id' => $pdata['end']	
					);
					$str = $this->db->insert_string('observation_groups', $sdata);
					$this->db->query($str);
					$last_group_id=$this->db->insert_id();
					
					$adata=$this->getarticlesbygroupById($val);
					  if($adata!=false)
					  {
						foreach($adata as $aval)
						{
						  $adata=array('name' => $aval['name'],
										'link'=> $aval['link'],
							'group_id' => $last_group_id );
							
							$astr = $this->db->insert_string('article_dev', $adata);
							$this->db->query($astr);
							$last_article_id=$this->db->insert_id();
							$file=WORKSHOP_FILES.'articles/'.$aval['article_dev_id'].'.'.$aval['link'];
							$newfile=WORKSHOP_FILES.'articles/'.$last_article_id.'.'.$aval['link'];
							copy($file, $newfile);
						}
					  
					  }
					  
					  $vdata=$this->getvideosbygroupById($val);
					  if($vdata!=false)
					  {
						foreach($vdata as $vval)
						{
						  $vdata=array('name' => $vval['name'],
										'link'=> $vval['link'],
										'descr'=>$vval['descr'],
							'group_id' => $last_group_id );
							
							$vstr = $this->db->insert_string('prof_dev', $vdata);
							$this->db->query($vstr);
							
						}
					  
					  }
					
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
	function copyobservationgroup($from,$to)
	{
	  $groups=$this->getallobservationgroupsbyDistrictID($from);
	  if($groups!=false)
	  {
	    foreach($groups as $val)
		{
		  $sdata = array('group_name' => $val['group_name'],
					'description' => $val['description'],
					'district_id' => $to	
					);
					$str = $this->db->insert_string('observation_groups', $sdata);
					$this->db->query($str);
					$last_group_id=$this->db->insert_id();
					
					$points=$this->getAllPoints($val['group_id'],$from);
					if($points!=false)
					{
					
					foreach($points as $pointval)
					{
					
					$sdata = array('group_type_id' => $pointval['group_type_id'],
					'ques_type_id' => $pointval['ques_type_id'],
					'group_id' => $last_group_id,
					'question' => $pointval['question'],
					'district_id' => $to
		
		
					);
					$str = $this->db->insert_string('observ_points', $sdata);
					$this->db->query($str);
					$last_id=$this->db->insert_id();
					$subdata=$this->getAllsubgroups($pointval['point_id']);
					if($subdata!=false)
					{
					  foreach($subdata as $subval)
					  {
					  $subsavedata=array('point_id' =>$last_id,
					 'sub_group_name'=> $subval['sub_group_name']
					 );
					
					 $sstr = $this->db->insert_string('observation_sub_groups', $subsavedata);
					  $this->db->query($sstr);
					
					 }
					
					}
			
			  
					
					}
					
					
					}
					
					$adata=$this->getarticlesbygroupById($val['group_id']);
					  if($adata!=false)
					  {
						foreach($adata as $aval)
						{
						  $adata=array('name' => $aval['name'],
										'link'=> $aval['link'],
							'group_id' => $last_group_id );
							
							$astr = $this->db->insert_string('article_dev', $adata);
							$this->db->query($astr);
							$last_article_id=$this->db->insert_id();
							$file=WORKSHOP_FILES.'articles/'.$aval['article_dev_id'].'.'.$aval['link'];
							$newfile=WORKSHOP_FILES.'articles/'.$last_article_id.'.'.$aval['link'];
							copy($file, $newfile);
						}
					  
					  }
					  
					  $vdata=$this->getvideosbygroupById($val['group_id']);
					  if($vdata!=false)
					  {
						foreach($vdata as $vval)
						{
						  $vdata=array('name' => $vval['name'],
										'link'=> $vval['link'],
										'descr'=>$vval['descr'],
							'group_id' => $last_group_id );
							
							$vstr = $this->db->insert_string('prof_dev', $vdata);
							$this->db->query($vstr);
							
						}
					  
					  }
		
		
		}
	  
	  
	  
	  
	  }
	
	
	
	}
	
	function getAllPoints($group_id,$district_id)
	{
		$qry = "Select d.country_id,d.state_id,op.point_id,op.group_id,op.ques_type_id,op.question,op.group_type_id,op.district_id from observ_points  op,districts d where op.district_id=d.district_id and op.district_id=$district_id and op.group_id=$group_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllsubgroups($point_id)
	{
		$qry = "Select sub_group_id,point_id,sub_group_name from observation_sub_groups where point_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getobservationgroupById($ob_id)
	{
		
		$qry = "Select og.group_id,og.group_name,og.description,og.district_id,d.state_id,d.country_id,d.districts_name from observation_groups og,districts d where og.district_id=d.district_id and  og.group_id=".$ob_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function getarticlesbygroupById($ob_id)
	{
		
		$qry = "Select article_dev_id,link,name from article_dev where group_id=".$ob_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getvideosbygroupById($ob_id)
	{
		
		$qry = "Select prof_dev_id,link,name,descr from prof_dev where group_id=".$ob_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	
	function add_observationgroup()
	{
	  $data = array('group_name' => $this->input->post('group_name'),
					'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')
		
		
		);
	   try{
			$str = $this->db->insert_string('observation_groups', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_observationgroup()
	{
	
		
		$group_id=$this->input->post('group_id');
	$data = array('group_name' => $this->input->post('group_name'),
				   'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')	
		
		
		);
		
			
		$where = "group_id=".$group_id;		
		
		try{
			$str = $this->db->update_string('observation_groups', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteobservationgroup($ob_id)
	{
		
		$adata=$this->getarticlesbygroupById($ob_id);
					  if($adata!=false)
					  {
						foreach($adata as $aval)
						{
						   $aid=$aval['article_dev_id'];
						   $alink=$aval['link'];
						   unlink(WORKSHOP_FILES.'articles/'.$aid.'.'.$alink);
							$qry = "delete from article_dev where article_dev_id=$aid";
							$query = $this->db->query($qry);
						}
					  
					  }
					  
					  $vdata=$this->getvideosbygroupById($ob_id);
					  if($vdata!=false)
					  {
						foreach($vdata as $vval)
						{
						    $pid=$vval['prof_dev_id'];
							$qry = "delete from prof_dev where prof_dev_id=$pid";
							$query = $this->db->query($qry);
						}
					  
					  }
		
		$qry = "delete from observation_groups where group_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getallobservationgroups()
	{
	
	 $district_id=$this->session->userdata("district_id");
	 $qry = "Select group_id,group_name,description from observation_groups where district_id=$district_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getallobservationgroupsbyDistrictID($district_id)
	{
	
	 $qry = "Select group_id,group_name,description from observation_groups where district_id=$district_id order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getvideos($id)
	{
	
	  $qry = "Select g.group_id,g.group_name,p.link,p.name,p.descr,p.prof_dev_id from observation_groups g,districts d,prof_dev p where g.district_id=d.district_id  and g.group_id=p.group_id and p.group_id=$id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getarticles($id)
	{
	
	  $qry = "Select g.group_id,g.group_name,p.link,p.name,p.article_dev_id from observation_groups g,districts d,article_dev p where g.district_id=d.district_id  and g.group_id=p.group_id and p.group_id=$id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getartifactsByTeachers($teacher_id=false)
	{
	     if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		
		$n=date('Y-m-d');
		$qry = "Select pa.artifact_id,pa.archived from artifacts pa where  pa.teacher_id=$teacher_id and pa.archived='$n' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function getcurrentartifactsByTeachers($teacher_id=false)
	{
	     if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		
		
		$qry = "Select pa.artifact_id,pa.archived from artifacts pa where  pa.teacher_id=$teacher_id and pa.archived is null ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getvideosByTeachers($teacher_id=false)
	{
	     if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		
		$n=date('Y-m-d');
		$qry = "Select pa.archived,pa.prof_dev_assign_id,g.group_id,g.group_name,p.link,p.name,p.descr,p.prof_dev_id from observation_groups g,districts d,prof_dev p,prof_dev_assign pa where g.district_id=d.district_id  and g.group_id=p.group_id and p.prof_dev_id=pa.prof_dev_id and pa.teacher_id=$teacher_id and pa.archived='$n' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function getcurrentvideosByTeachers($teacher_id=false)
	{
	     if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		
		
		$qry = "Select pa.archived,pa.prof_dev_assign_id,g.group_id,g.group_name,p.link,p.name,p.descr,p.prof_dev_id from observation_groups g,districts d,prof_dev p,prof_dev_assign pa where g.district_id=d.district_id  and g.group_id=p.group_id and p.prof_dev_id=pa.prof_dev_id and pa.teacher_id=$teacher_id and pa.archived is null ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function getarticlesbyTeachers($teacher_id=false)
	{
	     $n=date('Y-m-d');
		 if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		$qry = "Select pa.archived,pa.article_dev_assign_id,g.group_id,g.group_name,p.link,p.name,p.article_dev_id from observation_groups g,districts d,article_dev p,article_dev_assign pa where g.district_id=d.district_id  and g.group_id=p.group_id and p.article_dev_id=pa.article_dev_id and pa.teacher_id=$teacher_id and pa.archived='$n' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
			
	
	}
	function getcurrentarticlesbyTeachers($teacher_id=false)
	{
	     
		 if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		$qry = "Select pa.archived,pa.article_dev_assign_id,g.group_id,g.group_name,p.link,p.name,p.article_dev_id from observation_groups g,districts d,article_dev p,article_dev_assign pa where g.district_id=d.district_id  and g.group_id=p.group_id and p.article_dev_id=pa.article_dev_id and pa.teacher_id=$teacher_id and pa.archived is null ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
			
	
	}
	function getvideosByTeacher($teacher_id=false)
	{
	     if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		$qry = "Select pa.archived,pa.prof_dev_assign_id,g.group_id,g.group_name,p.link,p.name,p.descr,p.prof_dev_id from observation_groups g,districts d,prof_dev p,prof_dev_assign pa where g.district_id=d.district_id  and g.group_id=p.group_id and p.prof_dev_id=pa.prof_dev_id and pa.teacher_id=$teacher_id and pa.archived is null ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	
	}
	function getarticlesbyTeacher($teacher_id=false)
	{
	     if($teacher_id==false)
		 {
			$teacher_id=$this->session->userdata('teacher_id');
		 }	
		$qry = "Select pa.archived,pa.article_dev_assign_id,g.group_id,g.group_name,p.link,p.name,p.article_dev_id from observation_groups g,districts d,article_dev p,article_dev_assign pa where g.district_id=d.district_id  and g.group_id=p.group_id and p.article_dev_id=pa.article_dev_id and pa.teacher_id=$teacher_id and pa.archived is null ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	
	}
	
	function addteachervideocomments($id,$comments)
	{
	
	  
	$data = array('teacher_comments' => $comments
		
		
		);
		
			
		$where = "prof_dev_assign_id=".$id;		
		
		try{
			$str = $this->db->update_string('prof_dev_assign', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	
	
	}
	function addteacherarticlecomments($id,$comments)
	{
	
	  
	$data = array('teacher_comments' => $comments
		
		
		);
		
			
		$where = "article_dev_assign_id=".$id;		
		
		try{
			$str = $this->db->update_string('article_dev_assign', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	
	
	}
	function addobservervideocomments($id,$comments)
	{
	
	  
	$data = array('observer_comments' => $comments
		
		
		);
		
			
		$where = "prof_dev_assign_id=".$id;		
		
		try{
			$str = $this->db->update_string('prof_dev_assign', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	
	
	}
	function addobserverarticlecomments($id,$comments)
	{
	
	  
	$data = array('observer_comments' => $comments
		
		
		);
		
			
		$where = "article_dev_assign_id=".$id;		
		
		try{
			$str = $this->db->update_string('article_dev_assign', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	
	
	}
	function add_video()
	{
	  $data = array('group_id' => $this->input->post('group_id'),
	               'name' => $this->input->post('name'),
				   'descr' => $this->input->post('desc'),
					'link' => $this->input->post('link')
		
		
		);
	   try{
			$str = $this->db->insert_string('prof_dev', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function add_article()
	{
		
		preg_match("/\.([^\.]+)$/", $_FILES['upload']['name'], $matches);
			$ext = $matches[1];
		
  	 $data = array('group_id' => $this->input->post('group_id'),
	               'name' => $this->input->post('name'),
				   'link' => $ext
		
		
		);
	   try{
			$str = $this->db->insert_string('article_dev', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_video()
	{
	
		
		$prof_dev_id=$this->input->post('prof_dev_id');
	$data = array('name' => $this->input->post('name'),
				   'descr' => $this->input->post('desc'),
					'link' => $this->input->post('link')	
		
		
		);
		
			
		$where = "prof_dev_id=".$prof_dev_id;		
		
		try{
			$str = $this->db->update_string('prof_dev', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	function article_update()
	{
	
		
		$prof_dev_id=$this->input->post('article_dev_id');
	preg_match("/\.([^\.]+)$/", $_FILES['upload']['name'], $matches);
			$ext = $matches[1];
	
  	 if($_FILES['upload']['size']>0 )
		{
	 $data = array('name' => $this->input->post('name'),
				   'link' => $ext);
		}
		else
		{
		
		 $data = array('name' => $this->input->post('name'));
		}
			
		$where = "article_dev_id=".$prof_dev_id;		
		
		try{
			$str = $this->db->update_string('article_dev', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function delete_video($ob_id)
	{
		$qry = "delete from prof_dev where prof_dev_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function delete_article($ob_id)
	{
		$sqry="select * from article_dev where article_dev_id=$ob_id";
		$squery = $this->db->query($sqry);
		
		$qry = "delete from article_dev where article_dev_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return $squery->result_array();	
		}else{
			return false;
		}
	}
	function getvideoinfo($ob_id)
	{
		
		 $qry = "Select group_id,name,descr,link,prof_dev_id from prof_dev where prof_dev_id=".$ob_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function getarticleinfo($id)
	{
	
	
	 $qry = "Select p.group_id,og.group_name,p.name,p.link,p.article_dev_id from article_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id and p.article_dev_id=$id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getvideosbydistrictid($dist_id)
	{
	
	 $qry = "Select og.group_name,p.name,p.descr,p.link,p.prof_dev_id from prof_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id and og.district_id=$dist_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getallvideosCount()
	{
	  $qry = "Select count(*) as count from prof_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;	
		}else{
			return false;
		}
	
	
	}
	function getallvideos($start, $per_page)
	{
	
		$limit=" limit $start, $per_page ";
		
	  $qry = "Select og.group_name,p.name,p.link,p.descr,p.prof_dev_id,d.districts_name from prof_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id $limit";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallarticlesCount()
	{
	  $qry = "Select count(*) as count from article_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;	
		}else{
			return false;
		}
	
	
	}
	function getallarticles($start, $per_page)
	{
	
		$limit=" limit $start, $per_page ";
		
	  $qry = "Select og.group_name,p.name,p.link,p.article_dev_id,d.districts_name from article_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id $limit";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getarticlesbydistrictid($dist_id)
	{
	
	 $qry = "Select og.group_name,p.name,p.link,p.article_dev_id from article_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id and og.district_id=$dist_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getvideoinfobyid($dist_id)
	{
	
	 $qry = "Select og.group_name,p.name from prof_dev p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id and p.prof_dev_id=$dist_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function assignvideo()
	{
	    $status=0;	
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 
		 
		 
		 if(isset($pdata['profid']) && isset($pdata['teacher'])  )
		{			  
					$pid=$pdata['profid'];
					$teacher_id=$pdata['teacher'];
					$query="select * from prof_dev_assign where prof_dev_id=$pid and teacher_id=$teacher_id and archived is null ";
					$querys=$this->db->query($query);
					if($querys->num_rows()>0){
					return 2;
					}
					
					$status=1;
					$n=date('Y-m-d H:i:s');					
				   $sdata = array('prof_dev_id' => $pdata['profid'],
					'teacher_id' => $pdata['teacher'],
					'created'=>$n
					);
					$str = $this->db->insert_string('prof_dev_assign', $sdata);
					$this->db->query($str);
					
			
			  
			  
			
			
		 }
		 
		} 	
	
	       return $status;
	
	}
	function assignarticle()
	{
	    $status=0;	
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 
		 
		 
		 if(isset($pdata['profid']) && isset($pdata['teacher'])  )
		{			  
					$pid=$pdata['profid'];
					$teacher_id=$pdata['teacher'];
					$query="select * from article_dev_assign where article_dev_id=$pid and teacher_id=$teacher_id and archived is null ";
					$querys=$this->db->query($query);
					if($querys->num_rows()>0){
					return 2;
					}
					
					$status=1;
					$n=date('Y-m-d H:i:s');					
				   $sdata = array('article_dev_id' => $pdata['profid'],
					'teacher_id' => $pdata['teacher'],
					'created'=>$n
					);
					$str = $this->db->insert_string('article_dev_assign', $sdata);
					$this->db->query($str);
					
			
			  
			  
			
			
		 }
		 
		} 	
	
	       return $status;
	
	}
	
	function getTeachersassigned($id)
	{
	
	
	 $qry = "Select concat(t.firstname,' ',t.lastname) as teachername  from teachers t,prof_dev_assign p where p.teacher_id=t.teacher_id and p.prof_dev_id=$id and p.archived is null ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	}
	
	function getarticleassigned($id)
	{
	
	
	  $qry = "Select concat(t.firstname,' ',t.lastname) as teachername  from teachers t,article_dev_assign p where p.teacher_id=t.teacher_id and p.article_dev_id=$id and p.archived is null  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getvideocomments($id)
	{
	  
	   
	  $qry = "Select observer_comments,teacher_comments,prof_dev_assign_id from prof_dev_assign where prof_dev_assign_id=$id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function getarticlecomments($id)
	{
	  
	   
	  $qry = "Select observer_comments,teacher_comments,article_dev_assign_id from article_dev_assign where article_dev_assign_id=$id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function archived()
	{
	   $pdata=$this->input->post('pdata');
	 if(!empty($pdata))
	 {
	 if(isset($pdata['teacher'])  )
		 {
		    $teacher_id=$pdata['teacher'];
			
			 $artifacts=$this->getartifactsByTeachers($teacher_id);
			 $currentartifacts=$this->getcurrentartifactsByTeachers($teacher_id);
			 $n=date('Y-m-d');
			 $t=0;
			$j=0;
			if($artifacts!=false)
			{ 
			  
			  
			  
				 $t=1;
				
				
			}
			
			if($t!=1)
			{
	          if($currentartifacts!=false)
			{
			  $j=1;
			  foreach($currentartifacts as $val)
			  {		 
				    $sdata=array( 'archived'=>$n
                         		
	
	

								);
					
					 
					$where = "artifact_id=".$val['artifact_id'];		
		
		
			$str = $this->db->update_string('artifacts', $sdata,$where);
			$this->db->query($str);
				 
				 
				 
			  
			  }
			}	  
	  
	  
			}
			
			 
			 $videos=$this->getvideosByTeachers($teacher_id);
			 $currentvideos=$this->getcurrentvideosByTeachers($teacher_id);
		     
			
			if($videos!=false)
			{ 
			  
			  
			  
				 $t=1;
				
				
			}
			
		     if($t!=1)
			{
	          if($currentvideos!=false)
			{
			  $j=1;
			  foreach($currentvideos as $val)
			  {
				 $getvideodata=$this->getvideoinfo($val['prof_dev_id']);
				 if($getvideodata!=false)
				 {
				    $sdata=array( 'archived'=>$n,
                         		'group_id'=>$getvideodata[0]['group_id'],	
								'link'=>$getvideodata[0]['link'],
								'name'=>$getvideodata[0]['name'],
								'descr'=>$getvideodata[0]['descr']
	
	

								);
					
					 
					$where = "prof_dev_assign_id=".$val['prof_dev_assign_id'];		
		
		
			$str = $this->db->update_string('prof_dev_assign', $sdata,$where);
			$this->db->query($str);
				 
				 
				 }
			  
			  }
			}	  
	  
	  
			}
		 
		 }
		 
		 $articles=$this->getarticlesByTeachers($teacher_id);
		 $currentarticles=$this->getcurrentarticlesByTeachers($teacher_id);
		 if($articles!=false)
			{ 
			  
				 $t=1;
				
				
			}
		     if($t!=1)
			{
	          if($currentarticles!=false)
			{
			   $j=1;
			  foreach($currentarticles as $val)
			  {
				 $getarticledata=$this->getarticleinfo($val['article_dev_id']);
				 if($getarticledata!=false)
				 {
				    $sdata=array( 'archived'=>$n,
                         		'group_id'=>$getarticledata[0]['group_id'],	
								'link'=>$getarticledata[0]['article_dev_id'].'.'.$getarticledata[0]['link'],
								'name'=>$getarticledata[0]['name']
	
	

								);
					
					 
					$where = "article_dev_assign_id=".$val['article_dev_assign_id'];		
		
		
			$str = $this->db->update_string('article_dev_assign', $sdata,$where);
			$this->db->query($str);
						$file=WORKSHOP_FILES.'articles/'.$getarticledata[0]['article_dev_id'].'.'.$getarticledata[0]['link'];
							$newfile=WORKSHOP_FILES.'archived/'.$getarticledata[0]['article_dev_id'].'.'.$getarticledata[0]['link'];
							copy($file, $newfile);
				 
				 
				 }
			  
			  }
			}	  
	  
	  
			}
			
			if($t==1)
			{
			 return 'already';
			
			}
			
			if($j==0)
			{
			
			 return 'nodata';
			}			
			else
			{
			  return 'sucess';
			 
			}
		 
	}	 
	  
	
	
	
	}
	function getpdarchivecount($school_id,$teacher_id)
	{
	if($teacher_id=='all')
		{
			$qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  " ;
		}
		else
		{
			 $qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  and t.teacher_id=$teacher_id  " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	}
	
	function getpdarchive($page,$per_page,$school_id,$teacher_id)
	{
		 $page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		if($teacher_id=='all')
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id $limit " ;
		}
		else
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id  and t.teacher_id=$teacher_id $limit " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getpdarchivedprojects()
	{
	if($this->session->userdata('login_type')=='teacher')
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	}
	else if($this->session->userdata('login_type')=='observer')
	{
		$teacher_id=$this->session->userdata('teacher_archive');
	
	}
	  
	
	 $qry="select date_format(m.archived,'%m-%d-%Y') as archived from prof_dev_assign m ,schools s,teachers t where s.school_id=t.school_id and m.teacher_id=t.teacher_id  and m.teacher_id=$teacher_id   and m.archived!='' group by m.archived  union  select date_format(m.archived,'%m-%d-%Y') as archived from article_dev_assign m ,schools s,teachers t where s.school_id=t.school_id and m.teacher_id=t.teacher_id  and m.teacher_id=$teacher_id   and m.archived!='' group by m.archived union  select date_format(m.archived,'%m-%d-%Y') as archived from artifacts m ,schools s,teachers t where s.school_id=t.school_id and m.teacher_id=t.teacher_id  and m.teacher_id=$teacher_id   and m.archived!='' group by m.archived ";
	
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->result_array();
			foreach($row as $values)
			{
			 $new[]=$values['archived'];
			
			}
			$new=array_unique($new);
			return $new;
            			
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function getpdarchivedCount()
	{
		if($this->session->userdata('login_type')=='teacher')
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	}
	else if($this->session->userdata('login_type')=='observer')
	{
		$teacher_id=$this->session->userdata('teacher_archive');
	
	}
	
	 $qry="select count(distinct(m.archived)) as count from prof_dev_assign m ,schools s,teachers t where s.school_id=t.school_id and m.teacher_id=t.teacher_id  and m.teacher_id=$teacher_id and m.archived!='' union all  select count(distinct(m.archived)) as count from article_dev_assign m ,schools s,teachers t where s.school_id=t.school_id and m.teacher_id=t.teacher_id  and m.teacher_id=$teacher_id and m.archived!=''; ";
	 
	$query = $this->db->query($qry);
		$count=0;
		foreach($query->result_array() as $val)
		{
		  $count+=$val['count'];
		
		}
		if($query->num_rows()>0){
			
			return $count;			
		}else{
			return FALSE;
		}
	
	}
	
	function getvideosdata($date,$teacher_id)
	{
	    $date1=explode('-',$date);
		
		$newdate=$date1[2].'-'.$date1[0].'-'.$date1[1];
	   $qry = "Select p.group_id,og.group_name,p.name,p.link,p.prof_dev_id,p.descr,p.teacher_comments,p.observer_comments from prof_dev_assign p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id and p.archived='$newdate' and p.teacher_id=$teacher_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getarticlesdata($date,$teacher_id)
	{
	    $date1=explode('-',$date);
		
		$newdate=$date1[2].'-'.$date1[0].'-'.$date1[1];
	   $qry = "Select p.group_id,og.group_name,p.name,p.link,p.article_dev_id,p.teacher_comments,p.observer_comments from article_dev_assign p,observation_groups og,districts d where p.group_id=og.group_id and og.district_id=d.district_id and p.archived='$newdate' and p.teacher_id=$teacher_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
}