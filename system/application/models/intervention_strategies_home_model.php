<?php
class Intervention_strategies_home_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_all_data($page=false,$per_page=false,$parent='')
	{
		//echo $parent;exit;
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		
		}
			if($parent!=''){
			$this->db->where('parent_id',$parent);
			} else {
				$this->db->where('parent_id',0);
			}
		
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		//$this->db->order_by('id','DESC');
		$query = $this->db->get('intervention_strategies_home');

		$result = $query->result();
		return $result;	
	}
	public function get_all_dataCount($parent='')
	{

		if($parent!=''){
		$this->db->where('parent_id',$parent);	
		}else{
		$this->db->where('parent_id',0);	
		}
		
		$this->db->where('is_delete',0);
		if($this->session->userdata('district_id'))
			$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('intervention_strategies_home');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
/*public function delete($table,$where)
	{
		return $this->db->delete($table,$where);
		
	
	}*/
	
	public function delete_intervention_strategies_home($table,$field)
	   {
		$this->db->where('id',$this->input->post('id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_intervention_strategies_homeById($where){
		$query = $this->db->get_where('intervention_strategies_home',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
	public function get_all_intervention_home()
	{
		/*if($parent!=''){
			$this->db->where('parent_id',$parent);
		} else {
			$this->db->where('parent_id',0);
		}*/
		$this->db->where('parent_id',0);
                $this->db->where('district_id',$this->session->userdata('district_id'));
		$this->db->where('is_delete',0);
		$this->db->order_by('id','DESC');
		$query = $this->db->get('intervention_strategies_home');
		$result = $query->result();
		
		return $result;	
	}

	public function get_meeting_type(){
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$this->db->where('is_delete',0);
		$this->db->order_by('id','ASC');
		$query = $this->db->get('sst_meeting_type');
		$result = $query->result_array();
		
		return $result;
	}
	
	public function get_referral_reason(){
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$this->db->where('is_delete',0);
		$this->db->order_by('id','ASC');
		$query = $this->db->get('sst_referral_reason');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_progress_monitoring(){
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$this->db->where('is_delete',0);
		$this->db->order_by('id','ASC');
		$query = $this->db->get('sst_progress_monitoring');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_program_change (){
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$this->db->where('is_delete',0);
		$this->db->order_by('id','ASC');
		$query = $this->db->get('sst_program_change');
		$result = $query->result_array();
		
		return $result;

	}
	
}
?>