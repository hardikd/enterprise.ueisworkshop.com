<?php

class Periodmodel extends Model {

    function __construct() {
        parent::__construct();
    }

    function getperiodCount($school_id) {

        if ($school_id == 'all') {

            $district_id = $this->session->userdata('district_id');
            $qry = "Select count(*) as count 
	  			from periods t,schools s where t.school_id=s.school_id and s.district_id=$district_id ";
        } else {
            $qry = "Select count(*) as count 
	  			from periods t,schools s  where  t.school_id=s.school_id and t.school_id=$school_id";
        }
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->count;
        } else {
            return FALSE;
        }
    }

    function getperiods($page, $per_page, $school_id) {

       
        if ($school_id == 'all') {
            $district_id = $this->session->userdata('district_id');

            $qry = "Select time_format(b.start_time,'%h:%i %p') as start_time,time_format(b.end_time,'%h:%i %p') as end_time,b.period_id as period_id,s.school_name as school_name,s.school_id as school_id from periods b,schools s where b.school_id=s.school_id  and s.district_id=$district_id ";
        } else {
            $qry = "Select time_format(b.start_time,'%h:%i %p') as start_time,time_format(b.end_time,'%h:%i %p') as end_time,b.period_id as period_id,s.school_name as school_name,s.school_id as school_id from periods b,schools s where b.school_id=s.school_id and b.school_id=$school_id ";
        }
			if($page && $per_page){
			$page -= 1;
			$start = $page * $per_page;
			$qry.=" limit $start, $per_page ";
		}
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getperiodById($period_id) {

        $qry = "Select period_id,school_id,time_format(start_time,'%h:%i %p') as start_time,time_format(end_time,'%h:%i %p') as end_time from periods where period_id=" . $period_id;
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function check_period_exists($start='',$end='') {
        if(!$this->session->userdata('school_id')){
            $school_id = $this->input->post('school_id');
        } else {
            $school_id = $this->session->userdata('school_id');
        }

        if ($start==''){
            $start = $this->input->post('start_time');
        }
        $start1 = explode(' ', $start);
        if ($start1[1] == 'PM') {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = $start2[0];
            } else {

                $start3 = $start2[0] + 12;
            }

            $starttime = $start3 . ':' . $start2[1];
        } else {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = '00';
                $starttime = $start3 . ':' . $start2[1];
            } else {

                $starttime = $start1[0];
            }
        }
        if($end==''){
            $end = $this->input->post('end_time');
        }
        $end1 = explode(' ', $end);
        if ($end1[1] == 'PM') {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = $end2[0];
            } else {

                $end3 = $end2[0] + 12;
            }
            $endtime = $end3 . ':' . $end2[1];
        } else {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = '00';
                $endtime = $end3 . ':' . $end2[1];
            } else {

                $endtime = $end1[0];
            }
        }

        $qry = "SELECT  count(*) as count from periods where  school_id=$school_id ; ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {

            $row = $query->row();
            if ($row->count == 10) {
                return 3;
            }
        }




        if ($starttime >= $endtime) {


            return 2;
        }

        $qry = "SELECT  period_id from periods where  ('$starttime'>start_time AND '$starttime'<end_time OR '$endtime'>start_time AND '$endtime'<end_time) and school_id=$school_id ; ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {

            return 0;
        }

        $sqry = "SELECT  period_id from periods where  start_time>='$starttime'  and school_id=$school_id ";

        $squery = $this->db->query($sqry);
        if ($squery->num_rows() > 0) {

            $jqry = "SELECT  period_id from periods where end_time<='$endtime' and school_id=$school_id  ";

            $jquery = $this->db->query($jqry);
            if ($jquery->num_rows() > 0) {

                return 0;
            }
        }


        return 1;
    }

    function check_period_update() {

        $school_id = $this->input->post('school_id');

        $start = $this->input->post('start_time');
        $start1 = explode(' ', $start);
        if ($start1[1] == 'PM') {
            $start2 = explode(':', $start1[0]);

            if ($start2[0] == 12) {
                $start3 = $start2[0];
            } else {

                $start3 = $start2[0] + 12;
            }
            $starttime = $start3 . ':' . $start2[1];
        } else {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = '00';
                $starttime = $start3 . ':' . $start2[1];
            } else {

                $starttime = $start1[0];
            }
        }
        $end = $this->input->post('end_time');
        $end1 = explode(' ', $end);
        if ($end1[1] == 'PM') {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = $end2[0];
            } else {

                $end3 = $end2[0] + 12;
            }
            $endtime = $end3 . ':' . $end2[1];
        } else {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = '00';
                $endtime = $end3 . ':' . $end2[1];
            } else {

                $endtime = $end1[0];
            }
        }



        if ($starttime >= $endtime) {


            return 2;
        }
        $period_id = $this->input->post('period_id');


        $qry = "SELECT  period_id from periods where  ('$starttime'>start_time AND '$starttime'<end_time OR '$endtime'>start_time AND '$endtime'<end_time) and period_id!=$period_id and school_id=$school_id ; ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {

            return 0;
        }

        $sqry = "SELECT  period_id from periods where  start_time>='$starttime' and period_id!=$period_id and school_id=$school_id  ";

        $squery = $this->db->query($sqry);
        if ($squery->num_rows() > 0) {

            $jqry = "SELECT  period_id from periods where  end_time<='$endtime'  and period_id!=$period_id and school_id=$school_id  ";

            $jquery = $this->db->query($jqry);
            if ($jquery->num_rows() > 0) {

                return 0;
            }
        }


        return 1;
    }

    function add_period($start='',$end='',$grade_id,$grade_subject_id) {
        if($this->session->userdata('school_id')){
            $school_id = $this->session->userdata('school_id');
        } else {
            $school_id = $this->input->post('school_id');
        }
        if($start==''){
            $start = $this->input->post('start_time');
        }
        $start1 = explode(' ', $start);
        if ($start1[1] == 'PM') {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = $start2[0];
            } else {
                $start3 = $start2[0] + 12;
            }
            $starttime = $start3 . ':' . $start2[1];
        } else {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = '00';
                $starttime = $start3 . ':' . $start2[1];
            } else {

                $starttime = $start1[0];
            }
        }
        if($end==''){
            $end = $this->input->post('end_time');
        }
        $end1 = explode(' ', $end);
        if ($end1[1] == 'PM') {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = $end2[0];
            } else {
                $end3 = $end2[0] + 12;
            }
            $endtime = $end3 . ':' . $end2[1];
        } else {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = '00';
                $endtime = $end3 . ':' . $end2[1];
            } else {

                $endtime = $end1[0];
            }
        }
        $data = array('start_time' => $starttime,
            'end_time' => $endtime,
            'school_id' => $school_id,
            'created' => date('Y-m-d H:i:s')
        );
        try {
            $str = $this->db->insert_string('periods', $data);
            if ($this->db->query($str)) {
                $period_id =  $this->db->insert_id();

                $this->db->select('*');
                $this->db->from('class_rooms');
                $this->db->where('grade_id',$grade_id);
                $this->db->where('school_id',$this->session->userdata('school_id'));
                $queryclass = $this->db->get();
                //echo $this->db->last_query();
                $resultclass = $queryclass->result_array();

                $this->db->select('*');
                $this->db->from('grade_subjects');
                $this->db->where('grade_subject_id',$grade_subject_id);
                $querygradesub = $this->db->get();
                //echo $this->db->last_query();
                $resultgradesub = $querygradesub->result_array();
                $days = 0;    
                for($days;$days<=6;$days++){
                    $time_table = array('class_room_id'=>$resultclass[0]['class_room_id'],
                                        'period_id'=>$period_id,
                                        'current_date'=>$days,
                                        'subject_id'=>$resultgradesub[0]['subject_id'],
                                        'teacher_grade_subject_id'=>$this->session->userdata('teacher_id'),
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->db->insert('time_table',$time_table);
                }
               
            } else {
                return 0;
            }
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    function update_period() {


        $period_id = $this->input->post('period_id');
        $start = $this->input->post('start_time');
        $start1 = explode(' ', $start);
        if ($start1[1] == 'PM') {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = $start2[0];
            } else {
                $start3 = $start2[0] + 12;
            }
            $starttime = $start3 . ':' . $start2[1];
        } else {
            $start2 = explode(':', $start1[0]);
            if ($start2[0] == 12) {
                $start3 = '00';
                $starttime = $start3 . ':' . $start2[1];
            } else {

                $starttime = $start1[0];
            }
        }
        $end = $this->input->post('end_time');
        $end1 = explode(' ', $end);
        if ($end1[1] == 'PM') {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = $end2[0];
            } else {
                $end3 = $end2[0] + 12;
            }
            $endtime = $end3 . ':' . $end2[1];
        } else {
            $end2 = explode(':', $end1[0]);
            if ($end2[0] == 12) {
                $end3 = '00';
                $endtime = $end3 . ':' . $end2[1];
            } else {

                $endtime = $end1[0];
            }
        }

        $data = array('start_time' => $starttime,
            'end_time' => $endtime,
            'school_id' => $this->input->post('school_id')
        );



        $where = "period_id=" . $period_id;

        try {
            $str = $this->db->update_string('periods', $data, $where);
            if ($this->db->query($str)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    function deleteperiod($period_id) {
        $qry = "delete from periods where period_id=$period_id";
        $query = $this->db->query($qry);
        if (mysql_error() == "") {
            return true;
        } else {
            return mysql_error();
        }
    }

    function getperiodbyschoolid($school_id = false) {
if($this->session->userdata('login_type')=='teacher' || $this->session->userdata('login_type')=='observer' ) {
	    if ($school_id == false) {

		if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
        }
        $qry = "select time_format(b.start_time,'%h:%i %p') as start_time,time_format(b.end_time,'%h:%i %p') as end_time,b.period_id as period_id   from periods b where b.school_id=$school_id";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }}else if($this->session->userdata('login_type')=='user'){
		
		$district_id = $this->session->userdata('district_id');
		
		$qrydist = "Select s.school_id from schools s where  s.district_id=$district_id  ";
		$querydist = $this->db->query($qrydist);
		$resultdist = $querydist->result();
//		print_r($resultdist);exit;
		foreach($resultdist as $school){
			$schoolid[] = 	$school->school_id;
		}
		$schoolids = implode(",",$schoolid);
                if($this->input->post('school_id')){
                    $schoolids = $this->input->post('school_id');
                }
                
//                echo $this->input->post('school_id');exit;
		
        $qry = "select time_format(b.start_time,'%h:%i %p') as start_time,time_format(b.end_time,'%h:%i %p') as end_time,b.period_id as period_id   from periods b where b.school_id IN ($schoolids)";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }}
		
    }
    
    function getperiodbyperiodid($period_id) {
        $qry = "select time_format(b.start_time,'%h:%i %p') as start_time,time_format(b.end_time,'%h:%i %p') as end_time,b.period_id as period_id   from periods b where b.period_id=$period_id";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    

}
