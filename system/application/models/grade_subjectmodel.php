<?php
class Grade_subjectmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getgrade_subjectCount($grade_id)
	{
		$district_id=$this->session->userdata('district_id');
		if($grade_id=='all')
		{
			
		
		 $qry="Select count(*) as count 
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=$district_id and s.district_id=$district_id " ;
		
		
			
		}
		else
		{
		 $qry="Select count(*) as count 
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=$district_id and s.district_id=$district_id and t.grade_id=$grade_id" ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getgrade_subjects($page,$per_page,$grade_id)
	{
		
		
		$district_id=$this->session->userdata('district_id');
		if($grade_id=='all')
		{
			
		
		 $qry="Select t.grade_subject_id,s.subject_name,g.grade_name
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=$district_id and s.district_id=$district_id  " ;
		
		
			
		}
		else
		{
		 $qry="Select t.grade_subject_id,s.subject_name,g.grade_name
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=$district_id and s.district_id=$district_id and t.grade_id=$grade_id " ;
		}
		if($page && $per_page){
			$page -= 1;
			$start = $page * $per_page;
			$qry.=" limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		//print_r($this->db->last_query());exit;
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getgrade_subjectById($grade_subject_id)
	{
		
		$qry = "Select grade_subject_id,subject_id,grade_id from grade_subjects where grade_subject_id=".$grade_subject_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getgrade_subjectall($grade_id)
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select t.subject_id,t.grade_subject_id,s.subject_name,g.grade_name,s.dist_subject_id
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.grade_id=$grade_id and t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=$district_id and s.district_id=$district_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
        
        
	
	function check_grade_subject_exists($name=false,$grade_id=false)
	{
	
		if($name==false)
		{
		$subject_id=$this->input->post('subject_id');
		$grade_id=$this->input->post('grade_id');
		
		}
		$qry="SELECT  grade_subject_id from grade_subjects where subject_id=$subject_id and grade_id=$grade_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						return 0;

			
		}else{
			
			
		  return 1;
		
		}
	
	
	}
	function check_grade_subject_update() {
	
	
		$subject_id=$this->input->post('subject_id');
		$grade_id=$this->input->post('grade_id');
		$id=$this->input->post('grade_subject_id');
		
		$qry="SELECT grade_subject_id from grade_subjects where subject_id=$subject_id and grade_id=$grade_id and grade_subject_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	}
	function add_grade_subject()
	{
	  $data = array('subject_id'=>$this->input->post('subject_id'),					
					'grade_id' => $this->input->post('grade_id'),                    
					'created'=>date('Y-m-d H:i:s')
		
		);
		 
	   try{
			$str = $this->db->insert_string('grade_subjects', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function update_grade_subject()
	{
	
		
		$grade_subject_id=$this->input->post('grade_subject_id');
	
		
		
			$data = array('subject_id'=>$this->input->post('subject_id'),					
					'grade_id' => $this->input->post('grade_id')
		
		         );
		
		
			
		$where = "grade_subject_id=".$grade_subject_id;		
		
		try{
			$str = $this->db->update_string('grade_subjects', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletegrade_subject($grade_subject_id)
	{
		$qry = "delete from grade_subjects where grade_subject_id=$grade_subject_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	function getGradeById($grade_id){
            
            $qry="Select * from dist_grades where dist_grade_id=$grade_id " ;
            $query = $this->db->query($qry);
		if(mysql_error()==""){
			return $query->result_array();	
		}else{
			return mysql_error();
		}
        }


	
	
	
}