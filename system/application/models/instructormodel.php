<?php
class Instructormodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getinstructor()
	{
		
		$this->db->select('*');
		$this->db->from('instructor');
		$this->db->where('observer_id',$this->session->userdata('observer_id'));
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	//update instructor table lastLogin field
	function updateinstructor()
	{
		$now = date('Y-m-d H:i:s');
		$update = array('lastLogin'=>$now);
		$this->db->where('observer_id',$this->session->userdata('observer_id'));
		$updateqry = $this->db->update('instructor',$update);
		if($updateqry){
			return true;		
		}else{
			return false;
		}
		
	}
	//update instructor table lastLogin field
	function updatesteps($update)
	{
		
		$this->db->where('observer_id',$this->session->userdata('observer_id'));
		$updateqry = $this->db->update('instructor',$update);
		if($updateqry){
			return true;		
		}else{
			return false;
		}
		
	}

	function updatepayment($paymentId,$plan,$subscription_amount,$additional_quantity){
		$now = date('Y-m-d H:i:s');
		$update = array('subscription_date'=>$now,
						'payment_id'=>$paymentId,
						'subscription_amount'=>$subscription_amount,
						'additional'=>$additional_quantity,
						'plan_type'=>$plan);
		$this->db->where('observer_id',$this->session->userdata('observer_id'));
		$updateqry = $this->db->update('instructor',$update);
		if($updateqry){
			return true;		
		}else{
			return false;
		}
	}
	
}