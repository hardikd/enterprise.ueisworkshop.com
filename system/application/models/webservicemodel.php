<?php
class Webservicemodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function check_school_exists($school_name,$username)
	{
	
		$name=$this->input->post('school_name');
		$username=$this->input->post('username');
		$qry="SELECT  school_id from schools where school_name='$name' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 0;
		}else{
			$sqry="SELECT  school_id from schools where username='$username'  ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
		}
	
	
	}
	
	function add_school($school_name,$password,$firstname,$lastname)
	{
	  $password=$this->input->post('password');
	  $password=md5($password);
	  $data = array('school_name' => $school_name, 
					'district_id'=> 248,
					'password'=> $password,
					'username'=> $school_name,
					'school_type_id'=> 72
		
		
		);
	   try{
			$str = $this->db->insert_string('schools', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function getallgrades($dist=false)
	{
	if($dist==false)
	{
	$district_id=$this->session->userdata('district_id');
	}
	else
	{
		$district_id=$dist;
	
	}
	$qry = "Select s.dist_grade_id as grade_id ,s.grade_name from dist_grades s,districts d where s.district_id=d.district_id and s.district_id=".$district_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}

	function check_observer_exists($username)
	{
		$name=$username;
		$qry="SELECT  observer_id from observers where username='$name'  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}
	function add_observer($username,$password,$school_id,$firstname,$lastname)
	{
	  $data = array('observer_name' => $username,
	   'school_id' => $school_id,
		'password'=> md5($password),
		'username'=>$username,
		'email'=>$username,
		'avatar'=>1
		
		);
	   try{
			$str = $this->db->insert_string('observers', $data);
			if($this->db->query($str))
			{
				$now = date('Y-m-d H:i:s');
				$observer_id = $this->db->insert_id();

				$instructor = array('email'=>$username,
									'registerDate'=>$now,
									'password'=>$password,
									'observer_id'=>$observer_id,
									'school_id'=>$school_id,
									'firstname'=>$firstname,
									'lastname'=>$lastname);
				$instinsert = $this->db->insert_string('instructor', $instructor);
				$this->db->query($instinsert);
				
				return $observer_id;
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}

	function add_dummy_teacher($username,$password,$school_id,$firstname,$lastname,$school)
	{
		$grade = 193;
		$stdcnt = 1;
	  	for($teachercnt=1;$teachercnt<=2;$teachercnt++) {
		  $teacher = array('email'=>'teacher'.$teachercnt.'@ueiscorp.com',
		  					'username'=>$school.'_teacher'.$teachercnt,
							'password'=>md5('Teacher'.$teachercnt),
							'avatar'=>1,
							'emailnotifylesson'=>1,
							'school_id'=>$school_id,
							'firstname'=>'Teacher'.$teachercnt,
							'lastname'=>'',
							'user_type'=>2);
		  $teachers[] = $teacher;
		   try{
				$str = $this->db->insert_string('teachers', $teacher);
				$this->db->query($str);
				$teacher_id = $this->db->insert_id();
				$datacurriculum = array('teacher_id'=>$teacher_id,'curriculum'=>'materials','date_modified'=>date('Y-m-d H:i:s'));
				$strcurriculum = $this->db->insert_string('curriculum', $datacurriculum);
				$this->db->query($strcurriculum);
				$stdstartcnt = ((($teachercnt-1)*5) + 1);
				$stdendcnt = (($teachercnt)*5);
				for($studentcnt=$stdstartcnt;$studentcnt<=$stdendcnt;$studentcnt++){

					$data = array('firstname' => 'Student',
						'lastname' => $studentcnt,						
	                    'school_id'=> $school_id,
						'grade'=>$grade,
						'student_number'=>$school.'st'.$studentcnt
					);	

					$str = $this->db->insert_string('students', $data);
					$this->db->query($str);
					$student_id = $this->db->insert_id();

					
					$qry='select grade_name from dist_grades where dist_grade_id ="'.$grade.'"';
	 		 		$query = $this->db->query($qry);
					$data = $query->result_array();
					$gradename = $data[0]['grade_name'];

					$dataUsers =  array(
									'Name'=>'Student',
									'Surname'=>$studentcnt,
									'UserName'=>'Student'.$studentcnt,
									'Password'=>'Student'.$studentcnt,
									'added_date'=>date('Y-m-d H:i:s'),
									'user_type'=>2,
									'approved'=>1,
									'student_id'=>$student_id,
									'school_id'=>$school_id,
									'pass'=>'Student'.$studentcnt,
									'grade_id'=>$grade,
									'grade_name'=>$gradename,
									'studentnumebr'=>'stu'.$school.$studentcnt,
									'district_id'=> 248
								);
					$this->db->insert('users', $dataUsers);	
					$user_id = $this->db->insert_id();

					$relation_arr = array('student_id'=>$student_id,'teacher_id'=>$teacher_id,'users_id'=>$user_id);
					$strrelation = $this->db->insert_string('student_teacher', $relation_arr);
					$this->db->query($strrelation);

					$qry = 'update students set UserID = '.$user_id.' where student_id = '.$student_id;
					$query = $this->db->query($qry);

				}
				
				$data = array('name' => 'Room '.$teachercnt,					
							'school_id' => $school_id,
				            'grade_id'=>$grade,
							'created'=>date('Y-m-d H:i:s'),
							);
				$str = $this->db->insert_string('class_rooms', $data);
				$this->db->query($str);		


				//This is to relate teacher with grade subject
				$qry = "Select t.subject_id,t.grade_subject_id,s.subject_name,g.grade_name,s.dist_subject_id
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.grade_id=$grade and t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=248 and s.district_id=248";
					$query = $this->db->query($qry);
					$subjectsByGrade = $query->result_array();
					//print_r($subjectsByGrade);exit;
					foreach($subjectsByGrade as $key=>$value){
						$qry="SELECT  teacher_grade_subject_id from teacher_grade_subjects where teacher_id=$teacher_id and grade_subject_id= ".$value['grade_subject_id'];
						$query = $this->db->query($qry);
						if($query->num_rows()>0){
							//
						}else{
							$data = array('teacher_id' => $teacher_id,
								'grade_subject_id' => $value['grade_subject_id'],                    
								'created'=>date('Y-m-d H:i:s')
							);
							$str = $this->db->insert_string('teacher_grade_subjects', $data);
							$this->db->query($str);
						}	
					}

				$grade++;
			}
			catch (Exception $ex)
			{
				die($ex->getMessage());

			}
		}

		return $teachers;
	}

	function add_periods($school_id){
		$query = $this->db->get_where('periods',array('school_id'=>223));
		$results = $query->result_array();
		//print_r($results);exit;
		foreach($results as $result){
			$data = array('start_time' => $result['start_time'],
            'end_time' => $result['end_time'],
            'school_id' => $school_id,
            'created' => date('Y-m-d H:i:s')
        );

        $str = $this->db->insert_string('periods', $data);	
		$this->db->query($str);
		} 

		
	}

	function time_table($school_id){
		$qryclassrooms = $this->db->get_where('class_rooms', array('school_id'=>$school_id));
		$resultclassrooms = $qryclassrooms->result_array();

		$qryperiods = $this->db->get_where('periods', array('school_id'=>$school_id));
		$resultperiods = $qryperiods->result_array();

		$this->db->select('teacher_id');
		$qryteacher = $this->db->get_where('teachers',array('school_id'=>$school_id));
		$resultteacher = $qryteacher->result_array();		



		$subarray = array('193'=>array(246,247,248,249,250,251),'194'=>array(252,253,254,261,259,260));

		foreach($resultclassrooms as $resultclassroom){
			for($date=0;$date<=6;$date++){
				foreach($resultperiods as $key=>$resultperiod){
					//print_r($subarray[$resultclassroom['grade_id']][$key]);
					$qry = "SELECT * from teacher_grade_subjects where grade_subject_id = ".$subarray[$resultclassroom['grade_id']][$key]." and teacher_id IN (SELECT teacher_id from teachers where school_id = ".$school_id.")";
					//echo "<br />";
					$query = $this->db->query($qry);
					$resultvalue = $query->result_array();

					$qry = "Select t.subject_id,t.grade_subject_id,s.subject_name,g.grade_name,s.dist_subject_id
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.grade_id=".$resultclassroom['grade_id']." and t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=248 and s.district_id=248 and t.grade_subject_id=".$subarray[$resultclassroom['grade_id']][$key];
					$query = $this->db->query($qry);
					$subjectsByGrade = $query->result_array();



					$data = array('period_id' => $resultperiod['period_id'],
					  'class_room_id' => $resultclassroom['class_room_id'],	 
					  'current_date' => "$date",
					  'subject_id' => $subjectsByGrade[0]['subject_id'],
					  'teacher_grade_subject_id'=>$resultvalue[0]['teacher_id'],
					  'created'=>date('Y-m-d H:i:s')
					);
					$str = $this->db->insert_string('time_table', $data);	
					$this->db->query($str);

					$qrystu = "SELECT * FROM student_teacher where teacher_id = ".$resultvalue[0]['teacher_id'];
					$qrystudent = $this->db->query($qrystu);
					$students = $qrystudent->result_array();
					foreach($students as $student) {
						$dataroster = array('period_id' => $resultperiod['period_id'],
					  'class_room_id' => $resultclassroom['class_room_id'],	 
					  'day' => "$date",
					  'teacher_id'=>$resultvalue[0]['teacher_id'],
					  'created'=>date('Y-m-d H:i:s'),
					  'email'=>1,
					  'student_id'=>$student['student_id']
					);
					$str = $this->db->insert_string('teacher_students', $dataroster);	
					$this->db->query($str);
					}					
				}
			}
		}		

		

	}

	function add_student(){
		
		$firstname=$this->input->post('firstname');
		$lastname=$this->input->post('lastname');
		$grade=$this->input->post('grade');
		$student_number=$this->input->post('student_number');	
		$school_id = $this->session->userdata('school_id');
		$data = array('firstname' => $firstname,
					'lastname' => $lastname,						
                    'school_id'=> $school_id,
					'grade'=>$grade,
					'student_number'=>$student_number
		);	

		$str = $this->db->insert_string('students', $data);
		$this->db->query($str);
		$student_id = $this->db->insert_id();

		$firstname = $this->input->post('firstname');
		$student_id =  $this->db->insert_id();
		$passwordid = $this->input->post('firstname');
		$lastname =  $this->input->post('lastname');
		$loginname = $this->input->post('firstname').''.$this->input->post('lastname');
		$student_number = $this->input->post('student_number');
		$newpasswordid = md5($this->input->post('firstname'));
		

		$qry='select grade_name from dist_grades where dist_grade_id ="'.$grade.'"';
	 		 $query = $this->db->query($qry);
				$data = $query->result_array();
				$gradename = $data[0]['grade_name'];
				
			 $district_id= $this->session->userdata('district_id');
			 $dataUsers =  array(
									'Name'=>$firstname,
									'Surname'=>$lastname,
									'UserName'=>$loginname,
									'Password'=>$newpasswordid,
									'added_date'=>date('Y-m-d H:i:s'),
									'user_type'=>2,
									'approved'=>1,
									'student_id'=>$student_id,
									'school_id'=>$school_id,
									'pass'=>$passwordid,
									'grade_id'=>$grade,
									'grade_name'=>$gradename,
									'studentnumebr'=>$student_number,
									'district_id'=> $district_id
								);
					$this->db->insert('users', $dataUsers);		

					$insert_id=$this->db->insert_id();
					$sql='Update students set UserID="'.$insert_id.'" where firstname="'.$firstname.'" ';
					$update = $this->db->query($sql);
					$datateacher = array('firstname' => $firstname,
						'lastname' => $lastname,
						'username' => $firstname.''.$lastname,					
	                    'school_id'=> $school_id,
						'password'=>md5($firstname)
						);	
					$strteacher = $this->db->insert_string('teachers', $datateacher);
					$this->db->query($strteacher);
					$teacher_id = $this->db->insert_id();

					$relation_arr = array('student_id'=>$student_id,'teacher_id'=>$teacher_id,'users_id'=>$insert_id);
					$strrelation = $this->db->insert_string('student_teacher', $relation_arr);
					$this->db->query($strrelation);

					$datacurriculum = array('teacher_id'=>$teacher_id,'curriculum'=>'materials','date_modified'=>date('Y-m-d H:i:s'));
					$strcurriculum = $this->db->insert_string('curriculum', $datacurriculum);
					$this->db->query($strcurriculum);

					$qry = "Select s.dist_grade_id as grade_id ,s.grade_name from dist_grades s,districts d where s.district_id=d.district_id and s.district_id=248";
					$query = $this->db->query($qry);
					$grades = $query->result_array();

					$qry = "Select t.subject_id,t.grade_subject_id,s.subject_name,g.grade_name,s.dist_subject_id
	  			from grade_subjects t,dist_subjects s,dist_grades g where t.grade_id=$grade and t.subject_id=s.dist_subject_id and t.grade_id=g.dist_grade_id and g.district_id=248 and s.district_id=$district_id";
					$query = $this->db->query($qry);
					$subjectsByGrade = $query->result_array();
					//print_r($subjectsByGrade);exit;
					foreach($subjectsByGrade as $key=>$value){
						$qry="SELECT  teacher_grade_subject_id from teacher_grade_subjects where teacher_id=$teacher_id and grade_subject_id= ".$value['grade_subject_id'];
						$query = $this->db->query($qry);
						if($query->num_rows()>0){
							//
						}else{
							$data = array('teacher_id' => $teacher_id,
								'grade_subject_id' => $value['grade_subject_id'],                    
								'created'=>date('Y-m-d H:i:s')
							);
							$str = $this->db->insert_string('teacher_grade_subjects', $data);
							$this->db->query($str);
						}	
					}
					
					//Add homeroom
					$classroomname = $this->input->post('firstname').' '.$this->input->post('lastname');
						$qry="SELECT  class_room_id from class_rooms where name='$classroomname' and school_id=$school_id ";
						$query = $this->db->query($qry);
						if($query->num_rows()>0){
							return 0;
						}else{
						  $data = array('name' => $classroomname,					
										'school_id' => $school_id,
					                    'grade_id'=>$grade,
										'created'=>date('Y-m-d H:i:s'),
										);
						  $str = $this->db->insert_string('class_rooms', $data);
						  $this->db->query($str);

						}
					if($update){
						return true;
					} else {
						return false;
					}
	}
	
}