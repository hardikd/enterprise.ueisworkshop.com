<?php
class Standard_name_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 
 function getgradeBysubject($grade_id)
	{
	$this->db->select('*');
	$this->db->from('grade_subjects');
	$this->db->join('dist_subjects','grade_subjects.subject_id = dist_subjects.dist_subject_id','LEFT');
	$this->db->where('grade_subjects.grade_id',$grade_id);
	//print_r($this->db->last_query());exit;
	$query = $this->db->get();
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
public function get_all_data($page=false,$per_page=false)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		
		}
	$this->db->select('*');
	$this->db->from('standard_name');
	$this->db->join('dist_subjects','standard_name.subject_id = dist_subjects.dist_subject_id','LEFT');
	$this->db->where('standard_name.district_id',$this->session->userdata('district_id'));
	//$this->db->join('standard_language','standard_name.standard_language_id = standard_language.id','LEFT');
	$this->db->where('is_delete','0');
	 //$this->db->order_by('standard_name_id','desc');
	
	
	$query = $this->db->get();
//print_r($this->db->last_query());exit;
		
		$result = $query->result();
		return $result;	
	}
	
	public function get_all_dataCount()
	{
		
		$this->db->where('is_delete',0);
		if($this->session->userdata('district_id'))
			$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('standard_name');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
public function get_all_standard_name_data($page=false,$per_page=false)
	{
		$this->db->where('is_delete',0);
		$this->db->order_by('standard_name_id','DESC');
                if($this->session->userdata('district_id')){
                    $this->db->where('district_id',$this->session->userdata('district_id'));
                }
		$query = $this->db->get('standard_name');
                
		$result = $query->result();
		return $result;	
	}

	public function delete_statements_name($table,$field)
	   {
		$this->db->where('standard_name_id',$this->input->post('standard_name_id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_statements_nameById($where){
		$query = $this->db->get_where('standard_name',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
        
        

}
?>