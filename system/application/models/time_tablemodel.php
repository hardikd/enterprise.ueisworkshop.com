<?php
class Time_tablemodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function time_feature($fromdate,$todate)
	{
		 
		
		$class_room_id=$this->session->userdata('time_table_class_work_id');
		$school_id=$this->session->userdata('time_table_school_id');
		$sql="select t.* from time_table t ,periods p,class_rooms c  where  t.current_date>='$fromdate' and t.current_date<='$todate' and t.class_room_id=$class_room_id and t.class_room_id=c.class_room_id and t.period_id=p.period_id and p.school_id=$school_id ";
		
		$query = $this->db->query($sql);
//                echo $this->db->last_query();exit;
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function check_time_feature($date,$period_id,$class_room_id)
	{
	   
		
		$sql="select v.* from time_table v  where    v.current_date='$date' and v.period_id=$period_id and v.class_room_id=$class_room_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getteachersbysubjectid($subject_id)
	{
	   
		$grade_id=$this->session->userdata('time_table_grade_id');
		$school_id=$this->session->userdata('time_table_school_id');
		
		
		$sql="select t.*,d.dist_subject_id,gs.grade_id from  teachers t,grade_subjects gs ,teacher_grade_subjects ts,dist_subjects d where gs.grade_id=$grade_id and gs.subject_id=$subject_id and gs.subject_id=d.dist_subject_id and gs.grade_subject_id=ts.grade_subject_id and ts.teacher_id=t.teacher_id and t.school_id=$school_id ";
		
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function gettimetablebyid($id)
	{
	   
		
		$sql="select t.* from time_table t where t.time_table_id=$id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function gettimetablebyteacher($day)
	{
	   if($this->session->userdata('teacher_id'))
		$teacher_id=$this->session->userdata('teacher_id');
           else 
               $teacher_id=$this->input->post('teacher_id');
                
		$sql="select p.period_id,c.grade_id,p.start_time,p.end_time,t.subject_id  from time_table t,periods p,class_rooms c where t.class_room_id=c.class_room_id and t.teacher_grade_subject_id='$teacher_id' and t.`current_date`=$day and t.subject_id is not null and t.period_id=p.period_id ; ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function checkteacheralready($id,$period_id,$date,$teacher_id)
	{
	   
		
		$sql="select t.time_table_id from time_table t where t.period_id=$period_id and t.current_date='$date' and t.teacher_grade_subject_id=$teacher_id  and t.time_table_id!=$id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return 1;	
		}else{
			return 0;
		}
	
	
	
	}
	function getsubjects($period_id,$weekday)
	{
	   $teacher_id=$this->session->userdata("teacher_id");
		
		$sql="select d.dist_subject_id as subject_id , d.subject_name from time_table t,dist_subjects d where t.period_id=$period_id and t.current_date=$weekday and t.teacher_grade_subject_id=$teacher_id and t.subject_id=d.dist_subject_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getallperiods($day)
	{
	   	if($this->session->userdata("teacher_id")){
	   		$teacher_id=$this->session->userdata("teacher_id");
		} else {
            $pdata = $this->input->post('pdata');
            if($pdata['teacher_id']!=''){
                $teacher_id = $pdata['teacher_id'];
            }
        }
		
		$sql=" select * from time_table t where teacher_grade_subject_id=$teacher_id and `current_date`=$day ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	
	
	function add_time_feature($date,$period_id,$class_room_id)
	{
	  
	  $data = array('period_id' => $period_id,
	  'class_room_id' => $class_room_id,	 
	  'current_date' => "$date",
	  'created'=>date('Y-m-d H:i:s')
					);
	   try{
			$str = $this->db->insert_string('time_table', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function updateRow($table,$data,$where=null)
	{
		$sql = "UPDATE `$table` SET";
		
		$idas=explode('_',$data['id']);
		$i=0;
		
		
		
		foreach($idas as $idvalue)
		{
		  if(isset($data['value_'.$i.'_'.$i]) && $data['value_'.$i.'_'.$i]!='')
		  {
		  $value_id=$idvalue;
		  $sql.=" subject_id='".mysql_escape_string($data['value_'.$i])."'";
		  $sql .= " WHERE time_table_id=$value_id";
		 mysql_query($sql);				
				if(mysql_error())
				{
					return mysql_error()."<br>$sql";
				}
				$sql = "UPDATE `$table` SET";
				$sql.=" teacher_grade_subject_id='".mysql_escape_string($data['value_'.$i.'_'.$i])."'";
		  $sql .= " WHERE time_table_id=$value_id";
		 mysql_query($sql);				
		 if(mysql_error())
				{
					return mysql_error()."<br>$sql";
				}
				$sql = "UPDATE `$table` SET";
		 
			}	
				$i++;
				
		}
		
		
		
 
		
		return '';
	}
}