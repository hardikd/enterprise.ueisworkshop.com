<?php
class Problem_incident_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_all_data($page=false,$per_page=false)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		
		}
		
		
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		//$this->db->order_by('id','DESC');
		$query = $this->db->get('dist_prob_incident');
		
		$result = $query->result();
		return $result;	
	}
	public function get_all_dataCount()
	{
		
		$this->db->where('is_delete',0);
		if($this->session->userdata('district_id'))
			$this->db->where('district_id',$this->session->userdata('district_id'));
		
		$query = $this->db->get('dist_prob_incident');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
/*public function delete($table,$where)
	{
		return $this->db->delete($table,$where);
		
	
	}*/
	
	public function delete_incident($table,$field)
	   {
		$this->db->where('id',$this->input->post('id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_prob_incidentById($where){
		$query = $this->db->get_where('dist_prob_incident',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
public function get_all_behaviour_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('dist_prob_behaviour');
		$result = $query->result();
		return $result;	
		}
public function get_all_possible_motivation_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('possible_motivation');
		$result = $query->result();
		return $result;	
		}
public function get_all_interventions_prior_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('interventions_prior');
		$result = $query->result();
		return $result;	
		}	
public function get_all_student_has_multiple_referrals_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('student_has_multiple_referrals');
		$result = $query->result();
		return $result;	
	}	

	//added for incident record
	public function get_all_incident_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('dist_prob_incident');
		$result = $query->result();
		return $result;	
		}
public function get_incident_possible_motivation_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('incident_possible_motivation');
		$result = $query->result();
		return $result;	
		}
public function get_incident_interventions_prior_data()	
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('incident_interventions_prior');
		$result = $query->result();
		return $result;	
		}	

	public function get_incident_student_has_multiple_referrals_data() {
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('incident_student_has_multiple_referrals');
		$result = $query->result();
		return $result;	
	}

	
}
?>