<?php
class King_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	function retrieve_students(){
		$query = $this->db->get('kindergarten');
		return $query->result_array();
	}
	
	function is_valid_login(){
		$username = $this->input->post('username');
		if($username!='')
		{
		$password = md5(trim($this->input->post('password')));
		$qry="SELECT ob.*,s.* from teachers ob,schools s  WHERE ob.username='".$username."' AND ob.password='".$password."' and ob.school_id=s.school_id ";
		//$qry="SELECT teacher_id,firstname,lastname,school_id FROM teachers WHERE username='".$username."' AND password='".$password."' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		}
		else
		{
		  return false;
		
		}
	}
	function getteacherCount($school_id)
	{
	
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		 $qry="Select count(*) as count 
	  			from teachers t,schools s where t.school_id=s.school_id and s.district_id=$district_id " ;
		
		}
		else
		{
		
			$qry="Select count(*) as count 
	  			from teachers " ;
		}
			
		}
		else
		{
		 $qry="Select count(*) as count 
	  			from teachers  where school_id=$school_id" ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	function getteacherCountbydistrict()
	{
	
	
		$qry = "select count(*) as count from kindergarten";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	
	
	function getteacherCountbydistrict1()
	{
	
	
		$qry = "select count(*) as count from users where grade_id=4";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	function getteachers($page,$per_page,$school_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
			$district_id=$this->session->userdata('district_id');
		
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id and s.district_id=$district_id  limit $start, $per_page ";
		}
		else
		{
			
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id  limit $start, $per_page ";
		
		}
		}
		else
		{
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id and b.school_id=$school_id  limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getteachersbydistrict($page,$per_page)
	{		
		$page -= 1;
		$start = $page * $per_page;
		
			 //$qry="select * from assignments,assignment_users ,users,quizzes where users.UserID=assignment_users.user_id and assignments.id = assignment_users.assignment_id  and quizzes.id=assignments.quiz_id  limit $start, $per_page " ;
		 $qry= "SELECT * FROM kindergarten  limit $start, $per_page " ;
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function getteachersbydistrict1()
	{		
		//$page -= 1;
		//$start = $page * $per_page;
		
			 //$qry="select * from assignments,assignment_users ,users,quizzes where users.UserID=assignment_users.user_id and assignments.id = assignment_users.assignment_id  and quizzes.id=assignments.quiz_id  limit $start, $per_page " ;
		 $thid = $this->session->userdata("teacher_id");
		 $qry= "SELECT  distinct(student_id) FROM  teacher_students where teacher_id=".$thid."" ;				
		 $query = $this->db->query($qry)->result_array();
		 		
					$uid =  array();
			foreach($query as $key => $val)
			{
				$str2 = "select UserID,Name,Surname from users where grade_id=4 and student_id=".$query[$key]['student_id']."";
				$query2 = $this->db->query($str2)->result_array(); 				
				
				if(!empty($query2[0]['UserID']))
				{
					$uid[] = $query2[0]['UserID'];
				}	
				
			}
		
	return $uid;
		 
	
	}
	function getteacherById($teacher_id)
	{		
		$qry = "Select *  from kindergarten  where kindergarten_id=".$teacher_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}		
	}
	function getTeachersBySchool($school_id)
	{
	$qry = "Select * from teachers where school_id=".$school_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return '';
		}
	
	
	
	}
	
	function getTeachersBySchoolApi($school_id)
	{
	$qry = "Select teacher_id,firstname,lastname from teachers where school_id=".$school_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return '';
		}
	
	
	
	}
	function check_add_stud_exists()
	{			
	
		$username = $_REQUEST['student_number'];
			$sqry="SELECT  kindergarten_id from kindergarten where kindergarten_number='$username'  ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
	
	
	}
	function check_teacher_update() {
	
	
		$name=$this->input->post('firstname');
		$school_id=$this->input->post('school_id');
		$id=$this->input->post('teacher_id');
		$username=$this->input->post('username');
		/*$qry="SELECT teacher_id from teachers where firstname='$name' and school_id=$school_id and teacher_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else*/
		{
			$sqry="SELECT  teacher_id from teachers where username='$username' and teacher_id!=$id ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
		}
	
	}
	function add_teacher()
	{
	
	
	
	
	  $data = array('firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),	
					'school_id' => $this->input->post('schoolname'),
                    'subject_id'=>$this->input->post('subject'),					
		            'grade'=> $this->input->post('grade'),
                    'kindergarten_number'=> $this->input->post('student_number'),
		'obtained_marks'=>$this->input->post('obtainedmark'),	
		'gender'=>$this->input->post('gender'),
		'total_marks'=>$this->input->post('totalmarks'),
		'status'=>'1',
		'time_stamp'=>date('Y-m-d H:i:s'),
		'address'=>$this->input->post('address')
		);
	   try{
			$str = $this->db->insert_string('kindergarten', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
				
				
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function add_bulk_teacher($firstname,$lastname,$school_id,$username,$password,$employee_number,$email)
	{
	  $data = array('firstname' => $firstname,
					'lastname' => $lastname,	
					'school_id' => $school_id,
                    'username'=>$username,					
		            'password'=> md5($password),
					'emp_number'=>$employee_number,
                    'email'=>$email					
		
		);
	   try{
			$str = $this->db->insert_string('teachers', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_teacher()
	{		
		$teacher_id=$this->input->post('student_id');
		//$password=$this->input->post('password');

		 $data = array('firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),	
					'school_id' => $this->input->post('schoolname'),
                    'subject_id'=>$this->input->post('subject'),					
		            'grade'=> $this->input->post('grade'),
                    'kindergarten_number'=> $this->input->post('student_number'),
		'obtained_marks'=>$this->input->post('obtainedmark'),	
		'gender'=>$this->input->post('gender'),
		'total_marks'=>$this->input->post('totalmarks'),		
		'address'=>$this->input->post('address')
		);
		
			
		$where = "kindergarten_id=".$teacher_id;		
		
		try{		
		
			$str = $this->db->update_string('kindergarten', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteteacher($studentuserid)
	{
	echo $qry = "delete from kindergarten where kindergarten_id=".$studentuserid."  ";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function pdfreportsave($filesave,$status,$score,$dates,$teacher_id,$comments)
	{
	
	 $login_type=$this->session->userdata('login_type');
             if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}
			$n=date('Y-m-d H:i:s');
	 $data = array('teacher_id' => $teacher_id,
					'score' => $score,	
					'status' => $status,
                    'dates'=>$dates,					
		            'file_path'=> $filesave,
					'type'=>$login_type,	
		            'user_id'=>$login_id,
					'comments'=>$comments,
					'created'=>$n						
		);
	   try{
			$str = $this->db->insert_string('summative', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	function changeTeacherEmail()
	
	{
	$data = array(	'emailnotifylesson'=>$this->input->post('emailnotifylesson')	
		
		
		);
		
		
		
		$teacher_id=$this->session->userdata('teacher_id');
	
		
			
		$where = "teacher_id=".$teacher_id;		
		
		try{
			$str = $this->db->update_string('teachers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function getemailnotify()
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	
	 $qry = "Select emailnotifylesson from teachers where teacher_id=".$teacher_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getTeacherObservationReport($teacher_id,$year)
	{
	  
	
	  $qry = "select count(report_id) as count ,MONTH(report_date) as month ,report_form,period_lesson from reports where teacher_id=$teacher_id and year(report_date)='$year' group by YEAR(report_date), MONTH(report_date),report_form,period_lesson union select count(report_id) as count ,MONTH(report_date) as month ,report_form,period_lesson from proficiencyreports where teacher_id=$teacher_id and year(report_date)='$year' group by YEAR(report_date), MONTH(report_date),report_form,period_lesson ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getKingstudent($sid,$teacher_id)
	{
	   $qry = "select * from kingrecord where studentid='$sid' and teacherid='$teacher_id'";	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
}