<?php
class Reportmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}	
	
	function ApiCreateReport($data){
		
	  
	   try{
			$str = $this->db->insert_string('reports', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function createReport(){
		
	  $data = array('report_date' => $this->input->post('report_date'), 
					'school_id'=> $this->input->post('school_id'),
					'teacher_id'=> $this->input->post('teacher_id'),
					'subject_id'=> $this->input->post('subject_id'),
					'grade_id'=> $this->input->post('grade_id'),
					'students'=> $this->input->post('students'),
					'paraprofessionals'=> $this->input->post('paraprofessionals'),
					'observer_id'=> $this->input->post('observer_id'),
					'report_form'=> $this->input->post('form'),
					'period_lesson'=> $this->input->post('period_lesson'),
					'lesson_correlation'=> $this->input->post('lesson_correlation'),
					'district_id'=>$this->session->userdata('district_id')
					
		
		
		);
	   try{
			$str = $this->db->insert_string('reports', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function createleadership(){
		
	  $n=date('Y-m-d H:i:s');
	  $data = array('report_date' => $this->input->post('report_date'),
					'school_id'=> $this->input->post('school_id'),	  
					'observer_id'=> $this->input->post('observer_id'),
					'district_id'=>$this->session->userdata('district_id'),
					'created'=>$n
					
		
		
		);
	   try{
			$str = $this->db->insert_string('leadershipreports', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function createproficiency(){
		
	   $data = array('report_date' => $this->input->post('report_date'), 
					'school_id'=> $this->input->post('school_id'),
					'teacher_id'=> $this->input->post('teacher_id'),
					'subject_id'=> $this->input->post('subject_id'),
					'grade_id'=> $this->input->post('grade_id'),
					'students'=> $this->input->post('students'),
					'paraprofessionals'=> $this->input->post('paraprofessionals'),
					'observer_id'=> $this->input->post('observer_id'),
					'report_form'=> $this->input->post('form'),
					'period_lesson'=> $this->input->post('period_lesson'),
					'lesson_correlation'=> $this->input->post('lesson_correlation'),
					'district_id'=>$this->session->userdata('district_id')
					
		
		
		);
	   try{
			$str = $this->db->insert_string('proficiencyreports', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function ApiCreateproficiency($data){
		
	   
	   try{
			$str = $this->db->insert_string('proficiencyreports', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function createleadershipself(){
		
	  $n=date('Y-m-d H:i:s');
	  $data = array('report_date' => $this->input->post('report_date'),
					'school_id'=> $this->input->post('school_id'),	  
					'observer_id'=> $this->input->post('observer_id'),
					'district_id'=>$this->session->userdata('district_id'),
					'created'=>$n
					
		
		
		);
	   try{
			$str = $this->db->insert_string('leadershipreportsself', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function getsummativeReportCount()
	{
	
		 $teacher_id=$this->session->userdata('summative_teacher_id');
		 if($this->session->userdata("login_type")=='user')
		{
		  $school_id=$this->session->userdata('summative_school_id');
		}
		else if($this->session->userdata("login_type")=='observer')
		{
		$school_id=$this->session->userdata('school_id');
		}
		else if($this->session->userdata("login_type")=='teacher')
		{
		$school_id=$this->session->userdata('school_summ_id');
		} 	
		 
		$status=$this->session->userdata('summative_status');
		$score=$this->session->userdata('summative_score');
		$str ='';
		if($status!='')
		{
		 $str.=" and r.status='$status' ";
		
		}
		
		if($score!='')
		{
		 $str.=" and r.score='$score' ";
		
		}
		if($this->session->userdata("login_type")=='teacher')
		{
		  $tea=$this->session->userdata("teacher_id");
		  $str.=" and r.teacher_id='$tea' ";
		
		} else if($teacher_id!='all')
		{
		 $str.=" and r.teacher_id='$teacher_id' ";
		
		}
		
		
			$qry = "select count(*) as count from summative r,teachers s  where  s.teacher_id=r.teacher_id and s.school_id=$school_id  $str ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getsummativeReportByTeacher($start, $per_page)
	{
	
		$teacher_id=$this->session->userdata('summative_teacher_id');
		
		if($this->session->userdata("login_type")=='user')
		{
		  $school_id=$this->session->userdata('summative_school_id');
		}
		else if($this->session->userdata("login_type")=='observer')
		{
		$school_id=$this->session->userdata('school_id');
		}
		else if($this->session->userdata("login_type")=='teacher')
		{
		$school_id=$this->session->userdata('school_summ_id');
		} 		
		$status=$this->session->userdata('summative_status');
		$score=$this->session->userdata('summative_score');
		$str ='';
		if($status!='')
		{
		 $str.=" and r.status='$status' ";
		
		}
		
		if($score!='')
		{
		 $str.=" and r.score='$score' ";
		
		}
		if($this->session->userdata("login_type")=='teacher')
		{
		  $tea=$this->session->userdata("teacher_id");
		  $str.=" and r.teacher_id='$tea' ";
		
		} else if($teacher_id!='all')
		{
		 $str.=" and r.teacher_id='$teacher_id' ";
		
		}  	
			$qry="Select  r.file_path,r.status,r.score,concat(t.firstname,'',t.lastname) as teacher_name from summative r,
		teachers t where   t.school_id=$school_id and r.teacher_id=t.teacher_id $str limit $start, $per_page ";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	
	
	}
	function getReportTeacherCount()
	{
	
		 $teacher_id=$this->session->userdata('report_teacher_id');
		 $district_id=$this->session->userdata('district_id');
		$school_id=$this->session->userdata('school_id');
		$form=$this->session->userdata('reportform');
		
		$yearquery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $yearquery.=" and r.period_lesson='$formtype' ";
		
		}
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery.=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		
		if($form=='formp')
		{
		$qry = "select count(*) as count from proficiencyreports r where r.school_id=$school_id  and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and r.district_id=$district_id $yearquery ";
		}
		else
		{
			$qry = "select count(*) as count from reports r where r.school_id=$school_id  and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and r.district_id=$district_id $yearquery ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportByTeacherpdf($school_id,$teacher_id,$form)
	{
	 $district_id=$this->session->userdata('district_id');
	 if($form=='formp')
		{
		$qry="Select  r.report_id ,r.report_date from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id   and r.report_form='$form' and r.teacher_id=$teacher_id and r.district_id=$district_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		  ";
		}
		else
		{
			$qry="Select  r.report_id,r.report_date from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id  and r.report_form='$form' and r.teacher_id=$teacher_id and r.district_id=$district_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		  ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getReportByTeacher($start, $per_page)
	{
	
		$teacher_id=$this->session->userdata('report_teacher_id');
		$district_id=$this->session->userdata('district_id');
		$school_id=$this->session->userdata('school_id');
		$form=$this->session->userdata('reportform');
		$yearquery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $yearquery.=" and r.period_lesson='$formtype' ";
		
		}
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery.=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		if($form=='formp')
		{
		$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id and r.report_form='$form' and r.teacher_id=$teacher_id and r.district_id=$district_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 $yearquery limit $start, $per_page ";
		}
		else
		{
			$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id and r.report_form='$form' and r.teacher_id=$teacher_id and r.district_id=$district_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 $yearquery limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	
	
	}
	function getReportobserverCount()
	{
	
		 $observer_id=$this->session->userdata('report_observer_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		$form=$this->session->userdata('reportform');
		 $fortypequery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $fortypequery.=" and r.period_lesson='$formtype' ";
		
		}
		 if($form=='formp')
		{
		  $qry = "select count(*) as count from proficiencyreports r where r.school_id=$school_id and r.district_id=$district_id and r.observer_id=$observer_id and r.report_form='$form' $fortypequery ";
		}
		else
		{
			$qry = "select count(*) as count from reports r where r.school_id=$school_id and r.district_id=$district_id and r.observer_id=$observer_id and r.report_form='$form' $fortypequery ";
		}
		 
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getleadershipReportobserverCount()
	{
	
		 $observer_id=$this->session->userdata('report_observer_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		 $qry = "select count(*) as count from leadershipreports r where r.school_id=$school_id and r.district_id=$district_id and r.observer_id=$observer_id  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getproficiencyReportobserverCount()
	{
	
		 $observer_id=$this->session->userdata('report_observer_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		 $qry = "select count(*) as count from proficiencyreports r where r.school_id=$school_id and r.district_id=$district_id and r.observer_id=$observer_id  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportByobserver($start, $per_page)
	{
	
		$observer_id=$this->session->userdata('report_observer_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		$form=$this->session->userdata('reportform');
		 $fortypequery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $fortypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($form=='formp')
		{
		$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id $fortypequery and r.district_id=$district_id and r.report_form='$form' and r.observer_id=$observer_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		else
		{
			$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id $fortypequery and r.district_id=$district_id and r.report_form='$form' and r.observer_id=$observer_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getleadershipReportByobserver($start, $per_page)
	{
	
		$observer_id=$this->session->userdata('report_observer_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		$qry="Select  r.report_id,r.report_date,ob.observer_name from leadershipreports r,observers ob where r.school_id=$school_id and r.district_id=$district_id and r.observer_id=$observer_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getproficiencyReportByobserver($start, $per_page)
	{
	
		$observer_id=$this->session->userdata('report_observer_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		
		$qry="Select  r.report_id,r.report_date,ob.observer_name from proficiencyreports r,observers ob where r.school_id=$school_id and r.district_id=$district_id and r.observer_id=$observer_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getReportgradeCount()
	{
	
		 $grade_id=$this->session->userdata('report_grade_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		$form=$this->session->userdata('reportform');
		 $fortypequery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $fortypequery.=" and r.period_lesson='$formtype' ";
		
		}
		 if($form=='formp')
		{
		$qry = "select count(*) as count from proficiencyreports r where r.school_id=$school_id and r.district_id=$district_id and r.grade_id=$grade_id and r.report_form='$form' $fortypequery ";
		}
		else
		{
			$qry = "select count(*) as count from reports r where r.school_id=$school_id and r.district_id=$district_id and r.grade_id=$grade_id and r.report_form='$form' $fortypequery ";
		}
		 
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportBygrade($start, $per_page)
	{
	
		$grade_id=$this->session->userdata('report_grade_id');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		$form=$this->session->userdata('reportform');
		$fortypequery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $fortypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($form=='formp')
		{
		$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id  $fortypequery and r.district_id=$district_id and r.report_form='$form' and r.grade_id=$grade_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		else
		{
			$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id  $fortypequery and r.district_id=$district_id and r.report_form='$form' and r.grade_id=$grade_id and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getReportschoolCount()
	{
	
		 $school_id=$this->session->userdata('report_school_id');
		 $district_id=$this->session->userdata('district_id');
		 $form=$this->session->userdata('reportform');
		$fortypequery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $fortypequery.=" and r.period_lesson='$formtype' ";
		
		}
		 if($form=='formp')
		{
		$qry = "select count(*) as count from proficiencyreports r where r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' $fortypequery  ";
		}
		else
		{
			$qry = "select count(*) as count from reports r where r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' $fortypequery  ";
		}
		 
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportByschool($start, $per_page)
	{
	
		$school_id=$this->session->userdata('report_school_id');
		$district_id=$this->session->userdata('district_id');
		$form=$this->session->userdata('reportform');
		$fortypequery='';
		 if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $fortypequery.=" and r.period_lesson='$formtype' ";
		
		}
		if($form=='formp')
		{
		$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from proficiencyreports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id  $fortypequery and r.district_id=$district_id and r.report_form='$form' and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		
		}
		else
		{
			$qry="Select  r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,ob.observer_name from reports r,
		teachers t,dist_subjects s,dist_grades g,observers ob where r.school_id=$school_id  $fortypequery  and r.district_id=$district_id and r.report_form='$form' and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id and r.observer_id=ob.observer_id 
		 limit $start, $per_page ";
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getReportData($report_id)
	{
	
		$qry="Select  r.teacher_id,r.report_id,r.school_id,r.subject_id,r.grade_id,r.lesson_correlation as lesson_correlation_id, CASE WHEN r.lesson_correlation=1 THEN 'Walkthrough'
         WHEN r.lesson_correlation = 2 THEN 'Lesson Plan'
         ELSE 'Observation' END AS lesson_correlation,r.report_form,r.report_id,r.report_date,ob.observer_name,concat(t.firstname,' ',t.lastname) as teacher_name,sc.school_name,s.subject_name,g.grade_name,r.period_lesson,r.students,r.paraprofessionals from reports r,observers ob, teachers t,schools sc,dist_subjects s,dist_grades g  where report_id=".$report_id." and r.observer_id=ob.observer_id and r.teacher_id=t.teacher_id and r.school_id=sc.school_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getleadershipReportData($report_id)
	{
	
		$qry="Select r.observer_id,r.report_id,r.report_date,ob.observer_name,sc.school_name from leadershipreports r,observers ob,schools sc  where report_id=".$report_id." and r.observer_id=ob.observer_id and r.school_id=sc.school_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getproficiencyReportData($report_id)
	{
		$qry="Select r.teacher_id,r.report_id,r.school_id,r.subject_id,r.grade_id,r.lesson_correlation as lesson_correlation_id, CASE WHEN r.lesson_correlation=1 THEN 'Walkthrough'
         WHEN r.lesson_correlation = 2 THEN 'Lesson Plan'
         ELSE 'Observation' END AS lesson_correlation,r.report_form,r.report_id,r.report_date,ob.observer_name,concat(t.firstname,' ',t.lastname) as teacher_name,sc.school_name,s.subject_name,g.grade_name,r.period_lesson,r.students,r.paraprofessionals from proficiencyreports r,observers ob, teachers t,schools sc,dist_subjects s,dist_grades g  where report_id=".$report_id." and r.observer_id=ob.observer_id and r.teacher_id=t.teacher_id and r.school_id=sc.school_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id";
		//$qry="Select r.observer_id,r.report_id,r.report_date,ob.observer_name,sc.school_name from proficiencyreports r,observers ob,schools sc  where report_id=".$report_id." and r.observer_id=ob.observer_id and r.school_id=sc.school_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getleadershipselfReportData($report_id)
	{
	
		$qry="Select r.report_id,r.report_date,ob.observer_name,sc.school_name from leadershipreportsself r,observers ob,schools sc  where report_id=".$report_id." and r.observer_id=ob.observer_id and r.school_id=sc.school_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function check_self()
	{
	  $observer_id=$this->session->userdata('observer_id');
	 $qry="Select r.report_id,r.report_date,ob.observer_name,sc.school_name,r.completed from leadershipreportsself r,observers ob,schools sc  where r.observer_id=".$observer_id." and r.observer_id=ob.observer_id and r.school_id=sc.school_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();			
			
		}else{
			return false;
		}
	
	
	
	
	}
	
	function finishself()
	{
	  $observer_id=$this->session->userdata('observer_id');
	 $qry="update leadershipreportsself set completed=1 where observer_id=$observer_id ";
		$query = $this->db->query($qry);
	
	
	
	}
	
	function getNumberofteachers($school_id)
	{
	
	    $qry="select count(distinct(t.teacher_id)) as count  from teachers t,observers ob where  ob.school_id=t.school_id and ob.school_id=$school_id ";
	
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function gettotalsubmittedteachers($school_id,$observer_id=false)
	{
	
	     $fromdate='01-01-'.date('Y');
		 $todate='31-12-'.date('Y').'23:59:59';
		 if($observer_id==false)
		 {
		 $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=3;";
		 }
		 else
		 {
		    $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  tp.role_id=$observer_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=3;";
		 
		 }
		 
		 //$qry="select count(distinct(tp.teacher_id)) as count from teacher_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created=curdate() ;";
	
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	
	}
	function gettotalsubmittedteachernames($school_id,$observer_id=false)
	{
	
	     $fromdate='01-01-'.date('Y');
		 $todate='31-12-'.date('Y').'23:59:59';
		 if($observer_id==false)
		 {
		 $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=3;";
		 }
		 else
		 {
		    $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where tp.role_id=$observer_id and   ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=3;";
		 }
		 
		 //$qry="select count(distinct(tp.teacher_id)) as count from teacher_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created=curdate() ;";
	
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	function getsubmittedteachernames($school_id,$observer_id=false)
	{
	     
		 if($observer_id==false)	 
		 {
	     $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans swp,teacher_plans tp,observers ob,teachers t where  swp.schedule_week_plan_id=tp.schedule_week_plan_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created=curdate() ;";
	      }
		  else
		  {
		  
		    $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans swp,teacher_plans tp,observers ob,teachers t where  swp.role_id=$observer_id and swp.schedule_week_plan_id=tp.schedule_week_plan_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created=curdate() ;";
		  
		  }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function getsubmittedteachernamescount($school_id,$observer_id=false)
	{
	     
	     if($observer_id==false)	 
		 {
	     $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans swp,teacher_plans tp,observers ob,teachers t where swp.schedule_week_plan_id=tp.schedule_week_plan_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created=curdate() ;";
	     }
		 else
		 {
		    $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans swp,teacher_plans tp,observers ob,teachers t where swp.role_id=$observer_id and swp.schedule_week_plan_id=tp.schedule_week_plan_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created=curdate() ;";
		 
		 }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	
	}
	// start of lesson plan
	function getsubmittedteacherslesson($school_id,$fromdate,$todate,$observer_id=false)
	{	
	
	  if($observer_id==false)
	  {
	  $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans swp ,lesson_week_plans tp,observers ob,teachers t where  swp.schedule_week_plan_id=tp.schedule_week_plan_id and  tp.status='Completed' and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' ;";
	  }
	  else
	  {
	    $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans swp ,lesson_week_plans tp,observers ob,teachers t where  swp.role_id=$observer_id and swp.schedule_week_plan_id=tp.schedule_week_plan_id and  tp.status='Completed' and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' ;";
	  
	  }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}	
	
	}
	function getsubmittedteachernameslesson($school_id,$fromdate,$todate,$observer_id=false)
	{
	    if($observer_id==false)
	  {
	     $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans swp , lesson_week_plans tp,observers ob,teachers t where  swp.schedule_week_plan_id=tp.schedule_week_plan_id and  tp.status='Completed' and  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' ;";
	  }
	  else
	  {
	     $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans swp , lesson_week_plans tp,observers ob,teachers t where  swp.role_id=$observer_id and swp.schedule_week_plan_id=tp.schedule_week_plan_id and  tp.status='Completed' and  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' ;";
	  
	  }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function gettotalsubmittedteacherslesson($school_id,$fromdate,$todate,$observer_id=false)
	{	
	
	   if($observer_id==false)
	   {
		$qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=1;";
	   }
	   else
	   {
	     $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  tp.role_id=$observer_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=1;";
	   
	   }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}	
	
	}
	function gettotalsubmittedteacherslessonnames($school_id,$fromdate,$todate,$observer_id=false)
	{
	    if($observer_id==false)
	   {
	     $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=1;";
	   }
	   else
	   {
	    $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where  tp.role_id=$observer_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=1;";
	   
	   }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	
	// end of lesson plan
	
	// start of observation plan
	function getsubmittedteachersobservation($school_id,$fromdate,$todate,$observer_id=false)
	{	
	
	  if($observer_id==false)
	  {
	  $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans swp, observation_ans tp,observers ob,teachers t where  swp.schedule_week_plan_id=tp.schedule_week_plan_id and  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created>='$fromdate' and tp.created<='$todate' ;";
	  }
	  else
	  {
	    $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans swp, observation_ans tp,observers ob,teachers t where  swp.role_id=$observer_id and  swp.schedule_week_plan_id=tp.schedule_week_plan_id and  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created>='$fromdate' and tp.created<='$todate' ;";
	  
	  
	  }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}	
	
	}
	function getsubmittedteachernamesobservation($school_id,$fromdate,$todate,$observer_id=false)
	{
	    if($observer_id==false)
	  {
	     $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans swp, observation_ans tp,observers ob,teachers t where  swp.schedule_week_plan_id=tp.schedule_week_plan_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created>='$fromdate' and tp.created<='$todate' ;";
	  }
	  else
	  {
	     $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans swp, observation_ans tp,observers ob,teachers t where swp.role_id=$observer_id and   swp.schedule_week_plan_id=tp.schedule_week_plan_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.created>='$fromdate' and tp.created<='$todate' ;";
	  
	  }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	function gettotalsubmittedteachersobservation($school_id,$fromdate,$todate,$observer_id=false)
	{	
	
	    if($observer_id==false)
	   {
	   $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=2;";
	  }
	  else
	  {
	    $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where tp.role_id=$observer_id and  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=2;";
	  
	  }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}	
	
	}
	function gettotalsubmittedteachernamesobservation($school_id,$fromdate,$todate,$observer_id=false)
	{
	
	     if($observer_id==false)
	   {
		 $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=2;";
	   }
	   else
	   {
           $qry="select distinct(tp.teacher_id) as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where tp.role_id=$observer_id and  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=2;";
	   
	   }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	
	// end of observation plan
	
	// start of observations
	function getsubmittedteachersobservations($school_id,$fromdate,$todate,$observer_id=false)
	{	
	
	   if($observer_id==false)
		 {
	   $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and  tp.task=5;";
	   }
	   else
	   {
	    $qry="select count(distinct(tp.teacher_id)) as count from schedule_week_plans tp,observers ob,teachers t where  tp.role_id=$observer_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=5;";
	   
	   }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}	
	
	}
	function getsubmittedteachernamesobservations($school_id,$fromdate,$todate,$observer_id=false)
	{
	
	     if($observer_id==false)
		 {
		 $qry="select distinct(tp.teacher_id) as teacher_id,ob.observer_id,date_format(tp.date,'%m/%d/%Y') as created ,date_format(tp.starttime,'%l:%i %p')as starttime,date_format(tp.endtime,'%l:%i %p') as endtime,ob.observer_name,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where  tp.role_id=ob.observer_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=5 order by tp.created desc;";
	     }
		 else
		 {
		    $qry="select distinct(tp.teacher_id) as teacher_id,ob.observer_id,date_format(tp.date,'%m/%d/%Y') as created,date_format(tp.starttime,'%l:%i %p')as starttime,date_format(tp.endtime,'%l:%i %p') as endtime,ob.observer_name,t.firstname,t.lastname from schedule_week_plans tp,observers ob,teachers t where  tp.role_id=$observer_id and tp.role_id=ob.observer_id and ob.school_id=t.school_id and ob.school_id=$school_id and tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and tp.task=5 order by tp.created desc;";
		 
		 }
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	}
	
	
	// end of observations
	
	
	function getreportnumber($type=false)
	{
	
	 if($type=='teacher')
	 {
	    if($this->session->userdata('login_type')=='teacher')
	 {
		$form=$this->input->post('form');
		$school_id=$this->input->post('school_id');		
		$subject_id=$this->input->post('subject_id');
		$grade_id=$this->input->post('grade_id');		
		$adquery='';

		$adquery=" and report_form='$form' and school_id=$school_id  and subject_id=$subject_id and grade_id=$grade_id   ";
		
		$teacher_id=$this->session->userdata('teacher_id');
	 
		 
		 $qry="select count(report_id) as count from teacherself where teacher_id=$teacher_id $adquery  ";
		 
		 
		 
	 }
	 
	 
	 }
	 
	 else if($type=='leader')
	 {
	    if($this->session->userdata('login_type')=='user')
	 {

		$district_id=$this->session->userdata('district_id');

		$school_id=$this->input->post('school_id');

		$observer_id=$this->input->post('observer_id');

		$adquery='';
	    	 $adquery=" and school_id=$school_id and observer_id=$observer_id  ";
		 
		 	$qry="select count(report_id) as count from leadershipreports where district_id=$district_id $adquery ";
		 
	 }
	 if($this->session->userdata('login_type')=='observer')
	 {
	    return 1;
		 
	 }
	 
	 }
	 
	 else
	 {
	 	$form=$this->input->post('form');
		$school_id=$this->input->post('school_id');
		$teacher_id=$this->input->post('teacher_id');
		$subject_id=$this->input->post('subject_id');
		$grade_id=$this->input->post('grade_id');
		$observer_id=$this->input->post('observer_id');
		$adquery='';
		
	  
	 if($this->session->userdata('login_type')=='user')
	 {

	$adquery=" and report_form='$form' and school_id=$school_id and teacher_id=$teacher_id and subject_id=$subject_id and grade_id=$grade_id and observer_id=$observer_id";
//print_r($adquery);exit;
	 $district_id=$this->session->userdata('district_id');
	 if($this->input->post('form')=='formp')
		{
	      

		 $qry="select count(report_id) as count from proficiencyreports where district_id=$district_id $adquery ";
		}
		 else
		 {
		 
		 $qry="select count(report_id) as count from reports where district_id=$district_id $adquery ";
		 
		 
		 }
	 }

	 if($this->session->userdata('login_type')=='observer')
	 {
		
	    
		if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
		
		$observer_id=$this->session->userdata('observer_id');
		
		
		$adquery=" and report_form='$form' and school_id=$school_id and teacher_id=$teacher_id and subject_id=$subject_id and grade_id=$grade_id  ";
		
	 if($this->input->post('form')=='formp')
		{
	 
		 
		 $qry="select count(report_id) as count from proficiencyreports where observer_id=$observer_id $adquery";
		}
		 else
		 {
		 
		 $qry="select count(report_id) as count from reports where observer_id=$observer_id $adquery ";
		 }
	 }
	 	
	}	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 1;
		}	
	
	
	
	
	}
	
	function getalltotaltask($fromdate,$todate)
	{
	
	 
		 
	$qry="select tp.starttime,tp.endtime,t.email,date_format(tp.date,'%m/%d/%Y') as date,tp.schedule_week_plan_id,tp.task,tp.teacher_id as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,teachers t where    tp.teacher_id=t.teacher_id and tp.date>='$fromdate' and tp.date<='$todate' and (tp.task=2 or tp.task=3 or tp.task=1) order by tp.teacher_id;";
		 
		 
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	
	}
	function getalllassonplantask()
	{
	
	 $n=date('Y-m-d');
		 
	$qry="select tp.subject_id,tp.lesson_week_plan_id,l.tab,s.subtab,tp.starttime,tp.endtime,t.email,date_format(tp.date,'%m/%d/%Y') as date,tp.teacher_id as teacher_id,t.firstname,t.lastname,t.emailnotifylesson from lesson_week_plans tp,lesson_weel_plan_details lewe,lesson_plans l,lesson_plan_sub s,teachers t where   tp.status='Completed' and tp.lesson_week_plan_id=lewe.lesson_week_plan_id and lewe.lesson_plan_id=l.lesson_plan_id and lewe.lesson_plan_sub_id=s.lesson_plan_sub_id and tp.teacher_id=t.teacher_id and tp.date='$n'  order by tp.teacher_id,tp.lesson_week_plan_id,l.lesson_plan_id;";
		 
		 
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	
	}
	
	
	function getalltotaltaskhourly($starttime,$endtime)
	{
	
	    $n=date('Y-m-d');
		 
	$qry="select tp.starttime,tp.endtime,tp.role_id,t.email,date_format(tp.date,'%m/%d/%Y') as date,tp.schedule_week_plan_id,tp.task,tp.teacher_id as teacher_id,t.firstname,t.lastname from schedule_week_plans tp,teachers t where    tp.teacher_id=t.teacher_id and tp.date='$n'  and tp.starttime>='$starttime' and starttime<='$endtime' and (tp.task=2 or tp.task=1 or tp.task=3 or task=5) order by tp.teacher_id;";
		 
		 
	 	$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();					
		}else{
			return FALSE;
		}
	
	
	
	
	}
	
	
	//dashboard
	
	function getplanbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select count(lesson_week_plan_id) as count from lesson_week_plans lp,schools s,teachers t where lp.diff_instruction!='' and  s.school_id=t.school_id and s.school_id=$school_id and lp.teacher_id=t.teacher_id  and month(lp.date)=$m and year(lp.date)=$y;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getplaneldbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select count(lesson_week_plan_id) as count  from lesson_week_plans lp,schools s,teachers t,dist_subjects d where  lp.diff_instruction!='' and lp.teacher_id=t.teacher_id  and lp.subject_id=d.dist_subject_id and d.eld=1 and s.school_id=t.school_id and s.school_id=$school_id and lp.grade=$grade_id  and month(lp.date)=$m and year(lp.date)=$y;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getplanbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select count(lesson_week_plan_id) as count from lesson_week_plans lp,schools s,teachers t,dist_subjects d where lp.diff_instruction!='' and  lp.teacher_id=t.teacher_id  and lp.subject_id=d.dist_subject_id and d.eld=0 and s.school_id=t.school_id and s.school_id=$school_id and lp.grade=$grade_id  and month(lp.date)=$m and year(lp.date)=$y;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getpdlcbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	 $qry = "select  count(*) as count from banking_time where FROM_UNIXTIME(created, '%m')=$m and FROM_UNIXTIME(created, '%Y')=$y and school_id=$school_id and subject_id!='' and grade_id!='' ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getpdlcbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	 $qry = "select  count(*) as count from banking_time where FROM_UNIXTIME(created, '%m')=$m and FROM_UNIXTIME(created, '%Y')=$y and school_id=$school_id and subject_id!='' and grade_id=$grade_id ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getchecklistbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from reports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='forma' ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getchecklistbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from reports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='forma' and grade_id=$grade_id ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getscaledbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from reports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='formb' ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getscaledbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from reports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='formb' and grade_id=$grade_id ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getproficiencybymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from proficiencyreports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='formp' ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getproficiencybygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from proficiencyreports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='formp' and grade_id=$grade_id ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getlikertbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from reports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='formc' ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getlikertbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select  count(*) as count from reports where month(report_date)=$m and year(report_date)=$y and school_id=$school_id and report_form='formc' and grade_id=$grade_id ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getnotificationbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select count(homework_id) as count from homework lp,schools s,teachers t where  s.school_id=t.school_id and s.school_id=$school_id and lp.teacher_id=t.teacher_id  and month(lp.entered)=$m and year(lp.entered)=$y ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getbehaviourbymonth($school_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	 $qry = "select count(tab.counts) as count,month(tab.cdate) as month from ( select if(count(v.value_id)!=0,1,0) as counts ,v.current_date as cdate  from value_feature v left join teacher_students s on v.student_id=s.teacher_student_id  left join teachers t on s.teacher_id =t.teacher_id left join schools sl on t.school_id=sl.school_id where sl.school_id=$school_id and year(v.current_date)=$y and month(v.current_date)=$m group by v.current_date) as tab group by month(tab.cdate) ";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getnotificationbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select count(lp.homework_id) as count from homework lp,schools s,teachers t where  s.school_id=t.school_id and s.school_id=$school_id and lp.teacher_id=t.teacher_id and lp.grade_id=$grade_id  and month(lp.entered)=$m and year(lp.entered)=$y ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function getbehaviourbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	 $qry = "select count(tab.counts) as count from ( select if(count(v.value_id)!=0,1,0) as counts ,v.current_date as cdate  from lesson_week_plans l,value_feature v left join teacher_students s on v.student_id=s.teacher_student_id  left join teachers t on s.teacher_id =t.teacher_id left join schools sl on t.school_id=sl.school_id where v.lesson_week_plan_id=l.lesson_week_plan_id and l.grade=$grade_id and sl.school_id=$school_id and year(v.current_date)=$y and month(v.current_date)=$m group by v.current_date) as tab group by month(tab.cdate); ";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	function gethomeworkbygrademonth($school_id,$grade_id)
	{
	  
	  $m=date('m');
	  $y=date('Y');
	  $qry = "select count(tab.counts) as count from ( select if(count(v.homework_feature_id)!=0,1,0) as counts ,v.current_date as cdate  from lesson_week_plans l,homework_feature v left join teacher_students s on v.student_id=s.teacher_student_id  left join teachers t on s.teacher_id =t.teacher_id left join schools sl on t.school_id=sl.school_id where v.lesson_week_plan_id=l.lesson_week_plan_id and l.grade=$grade_id and sl.school_id=$school_id and year(v.current_date)=$y and month(v.current_date)=$m group by v.current_date) as tab group by month(tab.cdate); ";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return 0;
		}
	
	
	}
	
}
