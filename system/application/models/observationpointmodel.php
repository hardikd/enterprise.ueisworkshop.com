<?php
class Observationpointmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getobservationpointCount($state_id,$country_id,$group_id,$district_id)
	{
	     if($group_id=='all' && $district_id!='all')
		{
			$qry="Select count(*) as count from observ_points op,observation_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=op.district_id and  op.group_id=og.group_id and og.district_id=$district_id and  op.district_id=$district_id " ;
		}
		else if($group_id!='all' && $district_id!='all')
		{
			$qry="Select count(*) as count from observ_points op,observation_groups og ,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=op.district_id and op.group_id=og.group_id and og.district_id=$district_id and  op.group_id=$group_id  and op.district_id=$district_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getobservationpoints($page,$per_page,$state_id,$country_id,$group_id,$district_id)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		
		 if($group_id=='all' && $district_id!='all')
		{
			$qry="Select s.name,d.districts_name as districts_name ,op.point_id point_id,og.group_name as group_name,op.question question,q.ques_type ques_type from observation_groups og,observ_points op,ques_types q,districts d,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and  op.ques_type_id=q.ques_type_id and   op.group_id=og.group_id and og.district_id=$district_id and op.district_id=$district_id and op.district_id=d.district_id $limit ";
		
		}
		else if($group_id!='all' && $district_id!='all')
		{
			$qry="Select s.name,d.districts_name as districts_name ,op.point_id point_id,og.group_name as group_name,op.question question,q.ques_type ques_type from observation_groups og,observ_points op,ques_types q,districts d ,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and   op.ques_type_id=q.ques_type_id and   op.group_id=og.group_id and og.district_id=$district_id and op.group_id=$group_id and op.district_id=$district_id and op.district_id=d.district_id $limit ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) && isset($pdata['group_id']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getobservationpointById($val);
			 
			  
			  if($data!=false && $data[0]['group_id']!=$pdata['group_id'])
			  {
					$status=1;	
				   $sdata = array('group_type_id' => $data[0]['group_type_id'],
					'ques_type_id' => $data[0]['ques_type_id'],
					'group_id' => $pdata['group_id'],
					'question' => $data[0]['question'],
					'district_id' => $pdata['end']
		
		
					);
					$str = $this->db->insert_string('observ_points', $sdata);
					$this->db->query($str);
					$last_id=$this->db->insert_id();
					$subdata=$this->getAllsubgroups($val);
					if($subdata!=false)
					{
					  foreach($subdata as $val)
					  {
					  $subsavedata=array('point_id' =>$last_id,
					 'sub_group_name'=> $val['sub_group_name']
					 );
					
					 $sstr = $this->db->insert_string('observation_sub_groups', $subsavedata);
					  $this->db->query($sstr);
					
					 }
					
					}
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
	function getobservationpointById($point_id)
	{
		
		$qry = "Select d.country_id,d.state_id,op.point_id,op.group_id,op.ques_type_id,op.question,op.group_type_id,op.district_id from observ_points  op,districts d where op.district_id=d.district_id and op.point_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function getAllsubgroups($point_id)
	{
		$qry = "Select sub_group_id,point_id,sub_group_name from observation_sub_groups where point_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllPoints($group_id,$district_id)
	{
		$qry = "Select osg.sub_group_id,osg.sub_group_name,op.point_id ,qt.ques_type,op.question,op.group_type_id from ques_types qt,observ_points op left join observation_sub_groups osg on  osg.point_id=op.point_id  where op.ques_type_id=qt.ques_type_id and op.group_id=$group_id and op.district_id=$district_id " ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllqualitative($group_id,$report)
	{
		$from=$this->session->userdata('report_from');
		$to=$this->session->userdata('report_to');
		$form=$this->session->userdata('reportform');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		if($form=='forma')
		{
			$qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.school_id=$school_id and r.district_id=$district_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.school_id=$school_id and r.district_id=$district_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else  if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		
		}
		if($report=='school')
		{
		  
			 if($form=='forma')
		{
		 $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and  r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else  if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and  r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getAllsectional($group_id,$report)
	{
		
		$form=$this->session->userdata('reportform');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		$formtypequery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypequery=" and r.period_lesson='$formtype' ";
		
		}
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		$yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		if($form=='forma')
		{
			
			$qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.teacher_id=$teacher_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $yearquery $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.teacher_id=$teacher_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $yearquery $formtypequery ; " ;
		
		}
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.grade_id=$grade_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.grade_id=$grade_id and r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		
		}
		
		}
		if($report=='school')
		{
		  
			if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where   r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where   r.school_id=$school_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id $formtypequery ; " ;
		
		}
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getAllReport($report)
	{
	
	    $form=$this->session->userdata('reportform');
		$school_id=$this->session->userdata('school_id');
		$district_id=$this->session->userdata('district_id');
		$formtypequery='';
		if($this->session->userdata('formtype') && $this->session->userdata('formtype')!='all')
		{
		  $formtype=$this->session->userdata('formtype');
		 $formtypequery=" and r.period_lesson='$formtype' ";
		
		}
		if($report=='teacher')
		{
		
		$yearquery='';
		if($this->session->userdata('reportyear') && $this->session->userdata('reportyear')!='all')
		{
		  $year=$this->session->userdata('reportyear');
		 $yearquery=" and year(r.report_date)='$year' ";
		
		}
		
		if($this->session->userdata('reportmonth') && $this->session->userdata('reportmonth')!='all')
		{
		  $month=$this->session->userdata('reportmonth');
		 $yearquery.=" and month(r.report_date)='$month' ";
		
		}
		
		if($this->session->userdata('report_query_subject') && $this->session->userdata('report_query_subject')!='all')
		{
		  $subject=$this->session->userdata('report_query_subject');
		 $yearquery.=" and r.subject_id=$subject ";
		
		}
		$teacher_id=$this->session->userdata('report_criteria_id');	
		
			
			$qry = "select report_id from reports r where r.teacher_id=$teacher_id and r.school_id=$school_id and r.district_id=$district_id  and r.report_form='$form' $yearquery $formtypequery " ;
		
		}
		if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		
		  $qry = "select report_id from reports r where r.observer_id=$observer_id and r.school_id=$school_id  and r.district_id=$district_id and r.report_form='$form' $formtypequery " ;
		
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		 
			 $qry = "select report_id from reports r where r.grade_id=$grade_id and r.school_id=$school_id  and r.district_id=$district_id and r.report_form='$form' $formtypequery " ;
		
		
		}
		if($report=='school')
		{
		  
			 $qry = "select report_id from reports r where  r.school_id=$school_id and r.district_id=$district_id and r.report_form='$form' $formtypequery " ;
			
		
		
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getAllGroupPoints()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select g.group_id,g.group_name,replace(replace(g.description,'\n',' '),'\r',' ') as description ,osg.sub_group_id,replace(replace(osg.sub_group_name,'\n',' '),'\r',' ') as sub_group_name ,op.point_id ,qt.ques_type,replace(replace(op.question,'\n',' '),'\r',' ') as question,op.group_type_id from ques_types qt,observation_groups g,observ_points op left join observation_sub_groups osg on  osg.point_id=op.point_id  where op.group_id=g.group_id and op.district_id=$district_id  and g.district_id=$district_id and op.ques_type_id=qt.ques_type_id order by g.group_id,op.point_id,osg.sub_group_id  " ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getReportPoints($report_id)
	{
		$qry = "Select group_id,responsetext,report_id,point_id,response from observations where report_id=".$report_id ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function savereport()
	{
	  $status=true;
	  $group_id=$this->input->post('group_id');
	  
	  $report_id=$this->session->userdata('new_report_id');
	  if(!empty($group_id))
	  {
	   $points=$this->getAllPoints($group_id,$this->session->userdata("district_id"));
	  // echo "<pre>";
	  // print_r($points);
	   //exit;
	   if($points!=false)
	   {
	   foreach($points as $val)
	   {
	     
		 if($val['group_type_id']!=2)
		 {
		    $acpoint_id=$val['point_id'];
			if($this->input->post($val['point_id'])==1)
			{
			 $value=1;
			}
			else
			{
			 $value=0;
			}
			
			$dqry = "delete from observations where point_id=".$val['point_id']." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
		}
        else
		{
            
			if($val['ques_type']=='checkbox')
			{
				
			     $acpoint_id=$val['point_id'];
				 $post_id=$val['point_id'].'_'.$val['sub_group_id'];
			     $value=$this->input->post($post_id);
				 $dqry = "delete from observations where point_id=".$val['point_id']." and report_id=$report_id and response='".$val['sub_group_id']."'";
		        $dquery = $this->db->query($dqry);
				 
			    	
			
			}
			else
			{
			  $acpoint_id=$val['point_id'];
				$value=$this->input->post($val['point_id']);
				$dqry = "delete from observations where point_id=".$val['point_id']." and report_id=$report_id";
		       $dquery = $this->db->query($dqry);
			
			}
				

		}	
			
			//$dqry = "delete from observations where point_id=".$val['point_id']." and report_id=$report_id";
		    //$dquery = $this->db->query($dqry);
			
			if($value!=0)
			{
			$data =array('report_id' => $report_id,
					'point_id' => $acpoint_id,
					'response' => $value
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('observations', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		  
		 }
		 
	   
	   }
	   }
	   
	   
	   $dqry = "delete from observations where group_id=".$group_id." and report_id=$report_id";
		$dquery = $this->db->query($dqry);
	   if($this->input->post('response-text'))
	   {
	     
	    
		$data =array('report_id' => $report_id,
					 'group_id' => $group_id,
					 'responsetext'=>$this->input->post('response-text')
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('observations', $data);
			
			$this->db->query($str);
	   
	   }
	  
	  }
	 
	   return $status;
	
	}
	function add_observationpoint()
	{
	  $data = array('group_type_id' => $this->input->post('group_type_id'),
					'ques_type_id' => $this->input->post('ques_type_id'),
					'group_id' => $this->input->post('group_id'),
					'question' => $this->input->post('question'),
					'district_id' => $this->input->post('district_id')
		
		
		);
	   try{
			$str = $this->db->insert_string('observ_points', $data);
			if($this->db->query($str))
			{
				if($this->input->post('group_type_id')==2)
				{
				   $last_id=$this->db->insert_id();
				   $c=$this->input->post('countergroup');
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!='' )
					 {
					 $sdata=array('point_id' =>$last_id,
					 'sub_group_name'=>$this->input->post('textbox'.$i)				   
					 );
					
					 $sstr = $this->db->insert_string('observation_sub_groups', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return $last_id;
				}
				else
				{
					return $this->db->insert_id();
				}
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_observationpoint()
	{
	
		
		 $point_id=$this->input->post('point_id');
		
	$data = array('group_type_id' => $this->input->post('group_type_id'),
					'ques_type_id' => $this->input->post('ques_type_id'),
					'group_id' => $this->input->post('group_id'),
					'question' => $this->input->post('question'),
					'district_id' => $this->input->post('district_id')
		
		
		);
		
			
		$where = "point_id=".$point_id;		
		
		try{
			$str = $this->db->update_string('observ_points', $data,$where);
			if($this->db->query($str))
			{
				if($this->input->post('group_type_id')==2)
				{
				   $dqry = "delete from observation_sub_groups where point_id=$point_id";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('countergroup');
				   //exit;
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!=''  && $this->input->post('textboxup'.$i)!='' )
					 {
					    $sub_group_id=$this->input->post('textboxup'.$i);
						
						$sdata=array('sub_group_name'=>$this->input->post('textbox'.$i),
						             'point_id'=>$point_id,
									 'sub_group_id'=>$sub_group_id
						
						);
                   					 
					   //$where = "point_id=$point_id and sub_group_id=$sub_group_id";		
		
		
			                $str = $this->db->insert_string('observation_sub_groups', $sdata);
					          $this->db->query($str);
					 
					 }
					  else if($this->input->post('textbox'.$i)!='' )
					 {
					 $sdata=array('point_id' =>$point_id,
					 'sub_group_name'=>$this->input->post('textbox'.$i)				   
					 );
					 
					 $sstr = $this->db->insert_string('observation_sub_groups', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return true;
				}
				else
				{
					$dqry = "delete from observation_sub_groups where point_id=$point_id";
		           $dquery = $this->db->query($dqry);
					return true;
				}
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteobservationpoint($ob_id)
	{
		$qry = "delete from observ_points where point_id=$ob_id";
		$query = $this->db->query($qry);
		$qry = "delete from observation_sub_groups where point_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
	function getAllquestions()
	{
	
	 $qry = "Select ques_type_id,ques_type from ques_types ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getAllgrouptypes()
	{
	
	 $qry = "Select group_type_id,group_type from group_types ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function savenotes($report_id,$group_id,$notes)
	{
	
	 $data = array('report_id' => $report_id,
					'group_id' => $group_id,
					'responsetext' => $notes
		
		
		);
	
	   $str=$this->db->insert_string('observations', $data);
	
	  $this->db->query($str);
	
	}
	
	function savepoint($report_id,$point_id)
	{
	
	 $data = array('report_id' => $report_id,
					'point_id' => $point_id,
					'response' => '1'
		
		
		);
	
	   
	   $str=$this->db->insert_string('observations', $data);
	
	  $this->db->query($str);
	
	
	
	
	}
	
	function savesubgroup($report_id,$point_id,$sub_group_id)
	{
	
	  $data = array('report_id' => $report_id,
					'point_id' => $point_id,
					'response' => $sub_group_id
		
		
		);
	
	    $str=$this->db->insert_string('observations', $data);
	
	  $this->db->query($str);
	
	
	
	}
}