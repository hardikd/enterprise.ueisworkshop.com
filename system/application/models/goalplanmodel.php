<?php
class Goalplanmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getgoalplanscount($state_id,$country_id,$district_id)
	{
	
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from goal_plans s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id ";
		}
		else
		{
				$qry="Select count(*) as count 
	  			from goal_plans  s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and  s.district_id=$district_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getgoalplans($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($district_id=='all')
		{
		$qry="Select st.name,s.tab,s.goal_plan_id as goal_plan_id,d.district_id as district_id,d.districts_name as districtsname from goal_plans s,districts d,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id  limit $start, $per_page ";
		}
		else
		{
			$qry="Select st.name,s.tab,s.goal_plan_id as goal_plan_id,d.district_id as district_id,d.districts_name as districtsname from goal_plans s,districts d ,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id and s.district_id=$district_id  limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function add_plan()
	{
	  $data = array('tab' => $this->input->post('tab'),
	  'description' => $this->input->post('desc'),
	  'district_id'=> $this->input->post('district_id')
					);
	   try{
			$str = $this->db->insert_string('goal_plans', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_plan()
	{
	
		
		$plan_id=$this->input->post('plan_id');
		$data = array('tab' => $this->input->post('tab'),
		'description' => $this->input->post('desc'),
		'district_id'=> $this->input->post('district_id')
					);
			
			
		$where = "goal_plan_id=".$plan_id;		
		
		try{
			$str = $this->db->update_string('goal_plans', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	function update_observer_plan()
	{
	
		
		$plan_id=$this->input->post('plan_id');
		$data = array(
		'school_description' => $this->input->post('desc')
		
					);
			
			
		$where = "goal_plan_id=".$plan_id;		
		
		try{
			$str = $this->db->update_string('goal_plans', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteplan($plan_id)
	{
		$qry = "delete from goal_plans where goal_plan_id=$plan_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getplanById($plan_id)
	{
	    $qry = "Select s.school_description,s.description,s.goal_plan_id,s.tab,d.state_id,s.district_id,d.country_id from goal_plans s,districts d where s.district_id=d.district_id and s.goal_plan_id=".$plan_id;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getallplans()
	{
	 if($this->session->userdata("login_type")=='admin')
		{
		 $district_id=$this->session->userdata('district_goal_tabs');
		}
		else
		{
	 
		$district_id=$this->session->userdata('district_id');
	  }
	  $qry = "Select goal_plan_id,tab,description,school_description from goal_plans where district_id=$district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getallplansbyteacher($year,$teacher_id=false)
	{
	    if($teacher_id==false)
		{
		 $teacher_id=$this->session->userdata("teacher_id");
		
		}
	    $qry = "Select t.teacher_plan_id,t.goal_plan_id,t.text,date_format(added,'%m/%d/%Y') as added from teacher_plans t where  t.teacher_id=$teacher_id and t.created='$year' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getallplansdescbyteacher($year,$teacher_id=false)
	{
	    if($teacher_id==false)
		{
		 $teacher_id=$this->session->userdata("teacher_id");
		
		}
	    $qry = "Select ob.observer_desc_id,ob.goal_plan_id,ob.text from observer_desc ob where  ob.teacher_id=$teacher_id and ob.created='$year' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getplansdescbyteacher($id,$year,$teacher_id=false)
	{
	  if($teacher_id==false)
		{
		 $teacher_id=$this->session->userdata("teacher_id");
		
		}
	    $qry = "Select ob.observer_desc_id,ob.goal_plan_id,ob.text from observer_desc ob where  ob.teacher_id=$teacher_id and ob.created='$year' and ob.goal_plan_id=$id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getcommentsbyplanid($plan_id)
	{
	  
	   
	  $qry = "Select g.teacher_plan_id,g.role,g.comments,date_format(g.created,'%m/%d/%Y %r') as created,IF(du.username IS NOT NULL, du.avatar, IF(ob.observer_name IS NOT NULL, ob.avatar, t.avatar))  as avatar from goal_comments g LEFT JOIN dist_users du ON g.role='District' AND du.dist_user_id = g.added_id
LEFT JOIN observers ob ON g.role='Observer' AND ob.observer_id = g.added_id 
LEFT JOIN teachers t ON g.role='Teacher' AND t.teacher_id = g.added_id where g.teacher_plan_id=$plan_id order by g.goal_comment_id desc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function getteacherplanid($id,$teacher_id=false,$year)
	{
		if($teacher_id==false)
		{
			$teacher_id=$this->session->userdata("teacher_id");
		}	
	 $qry = "Select goal_plan_id,teacher_plan_id,date_format(added,'%m/%d/%Y') as added,text from teacher_plans where goal_plan_id=$id and teacher_id=$teacher_id and created='$year' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row=$query->result_array();
			return $row;		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function addcomment()
	{
	   $role='';
	   $added_id=0;
	    $status=0;
		if($this->session->userdata('login_type')=='teacher')
	  {
		$role='Teacher';
		$added_id=$this->session->userdata('teacher_id');
	  }
	  else if($this->session->userdata('login_type')=='admin')
	  {
		$role='Admin';
		$added_id=$this->session->userdata('login_id');
	  }
	  else if($this->session->userdata('login_type')=='observer')
	  {
		$role='Observer';
		$added_id=$this->session->userdata('observer_id');
	  }
	  else if($this->session->userdata('login_type')=='user')
	  {
		$role='District';
		$added_id=$this->session->userdata('dist_user_id');
	  }
	  
	  if($role!='')
	  {
	  
	   
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['id']) && isset($pdata['comments'])  )
		 {
		    
			  
			 
			  
			  
					$status=1;	
				   $sdata = array('teacher_plan_id' => $pdata['id'],
					'role' => $role,
					'comments' => $pdata['comments'],
					'added_id'=>$added_id
					);
					$str = $this->db->insert_string('goal_comments', $sdata);
					$this->db->query($str);
					
			
			  
			  
			
			
		 }
		 
		} 
	
	
	 
	
	  
	  
	  
	  }
	  
	  return $status;
	}
	function add_desc()
	{
	   
		
		$observer_id=$this->session->userdata('observer_id');
	   
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['id']) && isset($pdata['desc'])  )
		 {
		    
			  
			     $result=$this->getplansdescbyteacher($pdata['id'],$pdata['year'],$pdata['teacher_id']);
				 if($result==false)
				 {
			  
			  
					$status=1;	
				   $sdata = array('goal_plan_id' => $pdata['id'],
					'text' => $pdata['desc'],
					'teacher_id' => $pdata['teacher_id'],
					'created' => $pdata['year'],
					'observer_id'=>$observer_id
					);
					$str = $this->db->insert_string('observer_desc', $sdata);
					$this->db->query($str);
					
			    }
				else
				{
				$status=1;	
				
				 $data = array('text' => $pdata['desc']
					);
			
			
		$where = "observer_desc_id=".$result[0]['observer_desc_id'];		
		
		
			$str = $this->db->update_string('observer_desc', $data,$where);
			$this->db->query($str);
			
			
			
				
				
				}
			  
			  
			
			
		 }
		 
		} 
	
	
	 
	
	  
	  
	  
	  
	  
	  return $status;
	}
	
	
	function getscheduleteacherplans($year)
	{
	
	 $teacher_id=$this->session->userdata("teacher_id");
	 $qry="Select schedule_week_plan_id from schedule_week_plans where date_format(created,'%Y')='$year'  AND task=3 and teacher_id=$teacher_id " ;
		
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getscheduleobservationplans()
	{
	 $teacher_id=$this->session->userdata("teacher_id");
	 $periodicjqry="SELECT  schedule_week_plan_id from schedule_week_plans where task=2  and  teacher_id=$teacher_id  ";
		     
					$periodicjquery = $this->db->query($periodicjqry);
					$sidd=0;
					if($periodicjquery->num_rows()>0){
					    
						$temp=$periodicjquery->result_array();
						
						foreach($temp as $value)
						{
						$sid=$value['schedule_week_plan_id'];
						$tempquery="SELECT  archived from observation_ans  where schedule_week_plan_id=$sid ";	
						$tempqueryob = $this->db->query($tempquery);
					if($tempqueryob->num_rows()>0){
					    
						
					}else
					{
					   $sidd=$sid;
					
					}
						}	
						
						}
						else
						{
							$sidd=0;
						
						}
						
						return $sidd;
	
	
	
	}
	function goalstatus($id)
	{
	
	 $qry="Select schedule_week_plan_id from teacher_plans  where  schedule_week_plan_id=$id " ;
		
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function observationplanstatus($id)
	{
	 $qry="Select schedule_week_plan_id from observation_ans  where  schedule_week_plan_id=$id " ;
		
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function lessonplanstatus($id)
	{
	 $qry="Select status from lesson_week_plans  where  schedule_week_plan_id=$id " ;
		
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function addgoal($id,$text,$year,$schedule_week_plan)
	
	{  
	        $status=false;	
	         if($text!='')
			 {
		           
				   $sdata = array('goal_plan_id' => $id,
					'text' => $text,
					'teacher_id' => $this->session->userdata("teacher_id"),
					'created'=>$year,
					'schedule_week_plan_id'=>$schedule_week_plan
					
					);
					$str = $this->db->insert_string('teacher_plans', $sdata);
					$this->db->query($str);
					$status=$last_id=$this->db->insert_id();
			}
			return $status;		
	
	}
	
	function getcommentssasignedcount($school_id,$teacher_id)
	{
	
	 if($teacher_id=='all')
		{
			$qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  " ;
		}
		else
		{
			 $qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  and t.teacher_id=$teacher_id  " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function getcommentssasigned($page, $per_page,$school_id,$teacher_id)
	{
            
		if($teacher_id=='all')
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id  " ;
		}
		else
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id  and t.teacher_id=$teacher_id " ;
		
		}
                if($page && $per_page){
                    $page -= 1;
                    $start = $page * $per_page;
                    $limit=" limit $start, $per_page ";
                    $qry.=" $limit";
                }
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	
	//Media Start
	function Insert_Media($school_id,$file_upload,$description) 
	{
	
	   $time=time();
	    if($this->session->userdata("login_type")=='user')
		{
		  $login_id=$this->session->userdata("dist_user_id");
		  
        
		}
		else if($this->session->userdata("login_type")=='observer')
		{
          $login_id=$this->session->userdata("observer_id");
		  
        }
		else if($this->session->userdata("login_type")=='teacher')
		{
          $login_id=$this->session->userdata("teacher_id");
		  
        }
		$type=$this->session->userdata("login_type");
        $district_id=$this->session->userdata("district_id");				
        
        
			 $query = mysql_query("INSERT INTO `goalmedia_library` (user_id, type,district_id,school_id,file_path,description,created) VALUES ('$login_id', '$type','$district_id','$school_id','$file_upload','$description','$time')") or die(mysql_error());
			
			
			 
			 return true;
        
		
       
    }
	
	function getgoalmediaCount()
	{
	     if($this->session->userdata("login_type")=='user')
		{
		  $district_id=$this->session->userdata("district_id");				
		  //$login_id=$this->session->userdata("dist_user_id");
		//$query = mysql_query("SELECT M.banking_time_id, M.user_id, M.message, M.created, U.username,M.type FROM banking_time M, dist_users U  WHERE M.user_id=U.dist_user_id and M.user_id=$login_id order by M.banking_time_id desc ") or die(mysql_error());
         
		  $searchquery='';
		 if($this->session->userdata('search_goalmedia_school_id'))
		 {
		    $school_id=$this->session->userdata('search_goalmedia_school_id');
			$searchquery=" and M.school_id=$school_id ";
		 }
		 
		 
		 
		 $qry="SELECT count(*) as count FROM  goalmedia_library M LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.district_id='$district_id' $searchquery ";
		
		 
		}
		else 
		{ 
		  if($this->session->userdata("login_type")=='observer')
		  {
			$school_id=$this->session->userdata("school_id");
		  }
		  else
		  {
            $school_id=$this->session->userdata("school_summ_id");
          }	
		  $searchquery='';
		  
		 
		  $qry="SELECT count(*) as count FROM goalmedia_library M LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' $searchquery ";

        }
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
        
        function getgoalmediaCountSmart()
	{
	     if($this->session->userdata("login_type")=='user')
		{
		  $district_id=$this->session->userdata("district_id");				
		  //$login_id=$this->session->userdata("dist_user_id");
		//$query = mysql_query("SELECT M.banking_time_id, M.user_id, M.message, M.created, U.username,M.type FROM banking_time M, dist_users U  WHERE M.user_id=U.dist_user_id and M.user_id=$login_id order by M.banking_time_id desc ") or die(mysql_error());
         
		  $searchquery='';
		 if($this->session->userdata('search_goalmedia_school_id'))
		 {
		    $school_id=$this->session->userdata('search_goalmedia_school_id');
			$searchquery=" and M.school_id=$school_id ";
		 }
		 
		 
		 
		 $qry="SELECT count(*) as count FROM  goalmedia_library M JOIN smartgoal ON smartgoal.name_id = M.name_id LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.district_id='$district_id' $searchquery   ";
		
		 
		}
		else 
		{ 
		  if($this->session->userdata("login_type")=='observer')
		  {
			$school_id=$this->session->userdata("school_id");
		  }
		  else
		  {
            $school_id=$this->session->userdata("school_summ_id");
          }	
		  $searchquery='';
		  
		 $year = $this->input->post('year');
                 $subject = $this->input->post('subject_id');
                 $grade = $this->input->post('grade_id');
		  $qry="SELECT count(*) as count FROM goalmedia_library M JOIN smartgoal ON smartgoal.name_id = M.name_id LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' $searchquery AND YEAR(smartgoal.goal_date) = '$year' AND smartgoal.grade_id = $grade AND smartgoal.subject_id = $subject";

        }
		
		
		$query = $this->db->query($qry);
//                echo $this->db->last_query();
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getgoalmedia($page,$per_page)
	{
	    $page -= 1;
		$start = $page * $per_page;
		
		if($this->session->userdata("login_type")=='user')
		{
		  $district_id=$this->session->userdata("district_id");				
		  //$login_id=$this->session->userdata("dist_user_id");
		//$query = mysql_query("SELECT M.banking_time_id, M.user_id, M.message, M.created, U.username,M.type FROM banking_time M, dist_users U  WHERE M.user_id=U.dist_user_id and M.user_id=$login_id order by M.banking_time_id desc ") or die(mysql_error());
         
		 $searchquery='';
		 if($this->session->userdata('search_goalmedia_school_id'))
		 {
		    $school_id=$this->session->userdata('search_goalmedia_school_id');
			$searchquery=" and M.school_id=$school_id ";
		 }
		 
		 
		 
		 $qry="SELECT M.description,M.file_path,s.school_name,M.media_library_id, M.user_id,  M.created, (CASE WHEN (du.username IS NULL) THEN ob.observer_name  WHEN (ob.observer_name IS NULL) THEN du.username   END)  as username,M.type FROM goalmedia_library M LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.district_id='$district_id' $searchquery order by M.media_library_id desc  limit $start,$per_page ";
		
		 
		}
		else if($this->session->userdata("login_type")=='observer')
		{ 
		  if($this->session->userdata("login_type")=='observer')
		  {
			$school_id=$this->session->userdata("school_id");
		  }
		  else
		  {
            $school_id=$this->session->userdata("school_summ_id");
          }	
		  $searchquery='';
		  	
		  $qry="SELECT M.description,M.file_path,s.school_name,M.media_library_id, M.user_id,  M.created, (CASE WHEN (du.username IS NULL) THEN ob.observer_name  WHEN (ob.observer_name IS NULL) THEN du.username   END)  as username,M.type FROM goalmedia_library M LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' $searchquery order by M.media_library_id desc  limit $start,$per_page ";

        } else 
		{ 
//		  if($this->session->userdata("login_type")=='observer')
//		  {
//			$school_id=$this->session->userdata("school_id");
//		  }
//		  else
//		  {
//            $school_id=$this->session->userdata("school_summ_id");
//          }	
          $school_id=$this->session->userdata("school_id");
          
		  $searchquery='';
		  	
		  $qry="SELECT M.description,M.file_path,s.school_name,M.media_library_id, M.user_id,  M.created, CONCAT(ob.firstname,' ',ob.lastname)  as username,M.type FROM goalmedia_library M LEFT JOIN teachers du ON M.type='teacher' AND du.teacher_id = M.user_id
LEFT JOIN teachers ob ON M.type='teacher' AND ob.teacher_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' $searchquery order by M.media_library_id desc  limit $start,$per_page ";

        }
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
	
        
        function getgoalmediaSmart($page,$per_page)
	{
	    $page -= 1;
		$start = $page * $per_page;
		
		if($this->session->userdata("login_type")=='user')
		{
		  $district_id=$this->session->userdata("district_id");				
		  //$login_id=$this->session->userdata("dist_user_id");
		//$query = mysql_query("SELECT M.banking_time_id, M.user_id, M.message, M.created, U.username,M.type FROM banking_time M, dist_users U  WHERE M.user_id=U.dist_user_id and M.user_id=$login_id order by M.banking_time_id desc ") or die(mysql_error());
         
		 $searchquery='';
		 if($this->session->userdata('search_goalmedia_school_id'))
		 {
		    $school_id=$this->session->userdata('search_goalmedia_school_id');
			$searchquery=" and M.school_id=$school_id ";
		 }
		 
		 
		 
		 $qry="SELECT M.description,M.file_path,s.school_name,M.media_library_id, M.user_id,  M.created, (CASE WHEN (du.username IS NULL) THEN ob.observer_name  WHEN (ob.observer_name IS NULL) THEN du.username   END)  as username,M.type FROM goalmedia_library M LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.district_id='$district_id' $searchquery order by M.media_library_id desc  limit $start,$per_page ";
		
		 
		}
		else if($this->session->userdata("login_type")=='observer')
		{ 
		  if($this->session->userdata("login_type")=='observer')
		  {
			$school_id=$this->session->userdata("school_id");
		  }
		  else
		  {
            $school_id=$this->session->userdata("school_summ_id");
          }	
		  $searchquery='';
		  	$year = $this->input->post('year');
                        $subject = $this->input->post('subject_id');
                 $grade = $this->input->post('grade_id');
		  $qry="SELECT M.description,M.file_path,s.school_name,M.media_library_id, M.user_id,  M.created, (CASE WHEN (du.username IS NULL) THEN ob.observer_name  WHEN (ob.observer_name IS NULL) THEN du.username   END)  as username,M.type FROM goalmedia_library M JOIN smartgoal ON smartgoal.name_id = M.name_id LEFT JOIN dist_users du ON M.type='user' AND du.dist_user_id = M.user_id
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' $searchquery AND YEAR(smartgoal.goal_date) = '$year' AND smartgoal.grade_id = $grade AND smartgoal.subject_id = $subject order by M.media_library_id desc  limit $start,$per_page ";

        } else 
		{ 
//		  if($this->session->userdata("login_type")=='observer')
//		  {
//			$school_id=$this->session->userdata("school_id");
//		  }
//		  else
//		  {
//            $school_id=$this->session->userdata("school_summ_id");
//          }	
          $school_id=$this->session->userdata("school_id");
          
		  $searchquery='';
		  	$year = $this->input->post('year');
                        $subject = $this->input->post('subject_id');
                 $grade = $this->input->post('grade_id');
		  $qry="SELECT M.description,M.file_path,s.school_name,M.media_library_id, M.user_id,  M.created, CONCAT(ob.firstname,' ',ob.lastname)  as username,M.type FROM goalmedia_library M JOIN smartgoal ON smartgoal.name_id = M.name_id LEFT JOIN teachers du ON M.type='teacher' AND du.teacher_id = M.user_id
LEFT JOIN teachers ob ON M.type='teacher' AND ob.teacher_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' $searchquery AND YEAR(smartgoal.goal_date) = '$year' AND smartgoal.grade_id = $grade AND smartgoal.subject_id = $subject order by M.media_library_id desc  limit $start,$per_page ";

        }
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
	// Media End
	
	function copygoal($from,$to)
	{
	  $groups=$this->getallgoalplansbydistrictIddesc($from);
	  if($groups!=false)
	  {
	    foreach($groups as $groupval)
			{
			  
					
				   $sdata = array('tab' => $groupval['tab'],
					'description' => $groupval['description'],
					'district_id' => $to	
					);
					$str = $this->db->insert_string('goal_plans', $sdata);
					$this->db->query($str);
					
			  
			
			}	  
	  
	  }
	
	
	}
	function getallgoalplansbydistrictIddesc($district_id)
	{
	
	 $qry = "Select tab,description from goal_plans where district_id=$district_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
        function savesmartgoal($description,$file_upload,$school_id,$name_id,$goalplanid){
            
            $time=time();
	    if($this->session->userdata("login_type")=='user')
		{
		  $login_id=$this->session->userdata("dist_user_id");
                  $type=$this->session->userdata("login_type");
		  
        
		}
		else if($this->session->userdata("login_type")=='observer')
		{
          $login_id=$this->input->post('teacher_id');//$this->session->userdata("observer_id");
          $type='teacher';
		  
        }
		else if($this->session->userdata("login_type")=='teacher')
		{
          $login_id=$this->session->userdata("teacher_id");
          $type=$this->session->userdata("login_type");
		  
        }
		
        $district_id=$this->session->userdata("district_id");				
        
        
                $query = mysql_query("INSERT INTO `goalmedia_library` (user_id, type,district_id,school_id,file_path,description,created,goal_plan_id,name_id) VALUES ('$login_id', '$type','$district_id','$school_id','$file_upload','$description','$time','$goalplanid','$name_id')") or die(mysql_error());
//			print_r($query);
                return true;
//                $media_library_id = $this->db->insert_id();
//                         
//                
//                $data = array('media_library_id'=>$media_library_id,'goal_date'=>$goaldate,'grade_id'=>$grade,'subject_id'=>$subject,'name_id'=>$name_id);
//           
//                $resultq = $this->db->insert('smartgoal', $data);
//                if ($resultq)
//                {
//                    return true;
//                } else 
//                {
//                    return false;
//                }
        }
        
        function updatesmartgoal($description,$file_upload,$school_id,$name_id,$goalplanid){
            
            $time=time();
	    if($this->session->userdata("login_type")=='user')
		{
		  $login_id=$this->session->userdata("dist_user_id");
                  $type=$this->session->userdata("login_type");
		}
		else if($this->session->userdata("login_type")=='observer')
		{
          $login_id=$this->input->post("teacher_id");
          $type='teacher';//$this->session->userdata("login_type");
		  
        }
		else if($this->session->userdata("login_type")=='teacher')
		{
          $login_id=$this->session->userdata("teacher_id");
          $type=$this->session->userdata("login_type");
		  
        }
		
        $district_id=$this->session->userdata("district_id");				
            $data =     array("user_id"=>$login_id, "type"=>$type,"district_id"=>$district_id,"school_id"=>$school_id,"file_path"=>$file_upload,"description"=>$description,"goal_plan_id"=>$goalplanid,"name_id"=>$name_id);
            
            if ($file_path!='' && $file_path!='NoFile')
            {
                $query = mysql_query("UPDATE `goalmedia_library` SET description='$description',file_path='$file_path' WHERE user_id = '$login_id' AND school_id = '$school_id' AND goal_plan_id='$goalplanid' AND name_id='$name_id'");
            } else 
            {
                $query = mysql_query("UPDATE `goalmedia_library` SET description='$description' WHERE user_id = '$login_id' AND school_id = '$school_id' AND goal_plan_id='$goalplanid' AND name_id='$name_id'");
            }
            
            return true;
                
                }
        
        function addsmartgoalname($goalname,$goaldate,$grade,$subject){
            $namedata = array('smart_goal_name'=>$goalname,'goal_date'=>$goaldate,'grade_id'=>$grade,'subject_id'=>$subject);
            $resultname = $this->db->insert('smartgoal', $namedata);
            return $name_id = $this->db->insert_id();
        }
        
        function editsmartgoalname($name_id,$goaldate,$grade,$subject){
            $namedata = array('goal_date'=>$goaldate,'grade_id'=>$grade,'subject_id'=>$subject);
            $this->db->where('name_id',$name_id);
            $resultname = $this->db->update('smartgoal', $namedata);
            return $name_id;
        }
        function getsmartgoals(){
//            print_r($this->session->all_userdata());exit;
            $this->db->select('*');
            $this->db->from('smartgoal');
            $this->db->join('goalmedia_library', 'smartgoal.name_id = goalmedia_library.name_id');
            $this->db->where(array('goalmedia_library.school_id'=>$this->session->userdata('school_id'),'goalmedia_library.district_id'=>$this->session->userdata('district_id')));
            if($this->session->userdata('login_type')=='teacher'){
                $this->db->where('goalmedia_library.user_id',$this->session->userdata('teacher_id'));
            }
            
            if ($this->input->post('goalyear')){
                $this->db->where('YEAR(smartgoal.goal_date)',$this->input->post('goalyear'));
            }
            
            $this->db->group_by('smartgoal.name_id');
            $query = $this->db->get();
//           echo $this->db->last_query();exit;
            $goalresult = $query->result();
//            print_r($goalresult);exit;
            return $goalresult;
        }
        
        function getsmartgoalsarray(){
//            print_r($this->session->all_userdata());exit;
            $this->db->select('*, `smartgoal`.`subject_id` as subject_id');
            $this->db->from('smartgoal');
            $this->db->join('goalmedia_library', 'smartgoal.name_id = goalmedia_library.name_id');
            $this->db->join('schools', 'schools.school_id = goalmedia_library.school_id');
            $this->db->join('dist_subjects', 'dist_subjects.dist_subject_id = smartgoal.subject_id');
            $this->db->join('grade_subjects','grade_subjects.grade_subject_id = smartgoal.grade_id');
            $this->db->join('dist_grades','dist_grades.dist_grade_id = grade_subjects.grade_id');
//            $this->db->join('dist_grades', 'dist_grades.dist_grade_id = smartgoal.grade_id');
//            $this->db->join('teachers', 'teachers.teacher_id = goalmedia_library.teacher_id');
            $this->db->where(array('goalmedia_library.school_id'=>$this->session->userdata('school_id'),'goalmedia_library.district_id'=>$this->session->userdata('district_id')));
            if($this->session->userdata('login_type')=='teacher'){
                $this->db->where('goalmedia_library.user_id',$this->session->userdata('teacher_id'));
            }
            
            if ($this->input->post('goalyear')){
                $this->db->where('YEAR(smartgoal.goal_date)',$this->input->post('goalyear'));
            }
            
            if ($this->input->post('year')){
                $this->db->where('YEAR(smartgoal.goal_date)',$this->input->post('year'));
            }
            
            $this->db->group_by('smartgoal.name_id');
            $query = $this->db->get();
//           echo $this->db->last_query();exit;
            $goalresult = $query->result_array();
//            print_r($goalresult);exit;
            return $goalresult;
        }
        
       
        function getsmartgoalsbyid($name_id){
            $this->db->select('*');
            $this->db->from('smartgoal');
            $this->db->join('goalmedia_library', 'smartgoal.name_id = goalmedia_library.name_id');
            $this->db->where(array('goalmedia_library.school_id'=>$this->session->userdata('school_id'),'goalmedia_library.district_id'=>$this->session->userdata('district_id'),'goalmedia_library.name_id'=>$name_id));
            $query = $this->db->get();
            $goalresult = $query->result();
            return $goalresult;
        }
        
        function getsmartgoalsbyteacherid($teacher_id){
            $this->db->select('*');
            $this->db->from('smartgoal');
            $this->db->join('goalmedia_library', 'smartgoal.name_id = goalmedia_library.name_id');
            $this->db->where(array('goalmedia_library.school_id'=>$this->session->userdata('school_id'),'goalmedia_library.district_id'=>$this->session->userdata('district_id'),'goalmedia_library.user_id'=>$teacher_id));
            $this->db->group_by('goalmedia_library.name_id');
            $query = $this->db->get();
            $goalresult = $query->result();
            return $goalresult;
        }

 public function savegoals_pdf($subject='', $grade='', $goaldate='', $name_id='',$user_id='')
	{

	$this->db->select('smg.name_id,smg.smart_goal_name,smg.goal_date,smg.grade_id,smg.subject_id,dg.dist_grade_id,dg.grade_name,ds.dist_subject_id,ds.subject_name,gl.media_library_id,gl.user_id,gl.school_id,gl.district_id,gl.goal_plan_id,gl.name_id,gl.file_path,gl.description,gl.type,gl.created,du.dist_user_id,du.username,dist.district_id,dist.districts_name,gp.goal_plan_id,gp.tab,sch.school_id,sch.school_name');
	$this->db->from('smartgoal as smg');
	//$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
	$this->db->join('dist_grades as dg','smg.grade_id = dg.dist_grade_id','LEFT');
	$this->db->join('dist_subjects as ds','smg.subject_id = ds.dist_subject_id','LEFT');
	$this->db->join('goalmedia_library as gl','smg.name_id = gl.name_id','LEFT');
	$this->db->join('dist_users as du','gl.user_id = du.dist_user_id');	
	$this->db->join('districts as dist','gl.district_id = dist.district_id','LEFT');
	$this->db->join('goal_plans as gp','gl.goal_plan_id = gp.goal_plan_id','LEFT');
	$this->db->join('schools as sch','gl.school_id = sch.school_id','LEFT');
	if((isset($subject) && $subject!=''))
		$this->db->where('smg.subject_id',$subject);	
	if((isset($grade) && $grade!=''))
		$this->db->where('smg.grade_id',$grade);
	if((isset($goaldate) && $goaldate!=''))
		$this->db->where('smg.goal_date',$goaldate);
	if(isset($name_id) && $name_id!='')
	$this->db->where('smg.name_id',$name_id);
	if(isset($teacher_id) && $teacher_id!='')
		$this->db->where('gl.user_id',$goaldate);
		
	$query = $this->db->get();
	
//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}
 
 function getcommentssasigned_old($page, $per_page,$school_id,$teacher_id)
	{
            
$this->db->select('smg.name_id,smg.smart_goal_name,smg.goal_date,smg.grade_id,smg.subject_id,dg.dist_grade_id,dg.grade_name,ds.dist_subject_id,ds.subject_name,gl.media_library_id,gl.user_id,gl.school_id,gl.district_id,gl.goal_plan_id,gl.name_id,gl.file_path,gl.description,gl.type,gl.created,du.dist_user_id,du.username,dist.district_id,dist.districts_name,gp.goal_plan_id,gp.tab,sch.school_id,sch.school_name');
	$this->db->from('smartgoal as smg');
	//$this->db->join('teachers','parent_teacher_conference.user_id = teachers.teacher_id','LEFT');
	$this->db->join('dist_grades as dg','smg.grade_id = dg.dist_grade_id','LEFT');
	$this->db->join('dist_subjects as ds','smg.subject_id = ds.dist_subject_id','LEFT');
	$this->db->join('goalmedia_library as gl','smg.name_id = gl.name_id','LEFT');
	$this->db->join('dist_users as du','gl.user_id = du.dist_user_id');	
	$this->db->join('districts as dist','gl.district_id = dist.district_id','LEFT');
	$this->db->join('goal_plans as gp','gl.goal_plan_id = gp.goal_plan_id','LEFT');
	$this->db->join('schools as sch','gl.school_id = sch.school_id','LEFT');
	$this->db->where('gl.school_id',$school_id);	
	$this->db->where('gl.user_id',$teacher_id);
	
	$query = $this->db->get();


//print_r($this->db->last_query());exit;
	
	$query = $this->db->get();
	
//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;		

	}
        
        function get_details_by_subject($subject_id='',$grade_id='',$teacher_id,$school_id,$date,$id=''){
            
            $this->db->select('*');
            $this->db->from('smartgoal');
            $this->db->join('goalmedia_library','goalmedia_library.name_id = smartgoal.name_id');
            $this->db->join('dist_subjects','dist_subjects.dist_subject_id = smartgoal.subject_id');
            $this->db->join('grade_subjects','grade_subjects.grade_subject_id = smartgoal.grade_id');
            $this->db->join('dist_grades','dist_grades.dist_grade_id = grade_subjects.grade_id');
			
			
            if($grade_id!='')
                $this->db->where('smartgoal.grade_id',$grade_id);
            if($subject_id)
                $this->db->where('smartgoal.subject_id',$subject_id);
            if($teacher_id!='all'){
                $this->db->where('goalmedia_library.user_id',$teacher_id);
                $this->db->where('goalmedia_library.type','teacher');
            }
            $this->db->where('goalmedia_library.district_id',$this->session->userdata('district_id'));
            $this->db->where('YEAR(FROM_UNIXTIME(goalmedia_library.created))',$date);
            if($id!='')
                $this->db->where('smartgoal.name_id',$id);
            
            
            $this->db->order_by('goalmedia_library.goal_plan_id','asc');
            $query = $this->db->get();
//            echo $this->db->last_query();
            
            $results = $query->result();
//            print_r($results);exit;
            if($results){
                foreach($results as $result){
                    $this->db->select('*');
                    $this->db->from('goal_plans');
                    $this->db->where('goal_plan_id',$result->goal_plan_id);
                    $this->db->order_by('goal_plan_id','asc');
                    $qrygoal_plan = $this->db->get();
                    $resultgoalplan = $qrygoal_plan->result();
					
					$this->db->select('*');
                    $this->db->from('schools');
                    $this->db->where('school_id',$result->school_id);
                    $qrygschool = $this->db->get();
                    $resultschool = $qrygschool->result();
					
                    
                    $resultarr[$result->name_id]['smart_goal_name'] =  $result->smart_goal_name;
                    $resultarr[$result->name_id]['subject'] =  $result->subject_name;
                    $resultarr[$result->name_id]['grade'] =  $result->grade_name;
                    $resultarr[$result->name_id]['goal_date'] =  $result->goal_date;
                    $resultarr[$result->name_id]['goal_date'] =  $result->goal_date;
                    $resultarr[$result->name_id]['goal_plan'][$resultgoalplan[0]->tab]['description'] =  $result->description;
                    $resultarr[$result->name_id]['goal_plan'][$resultgoalplan[0]->tab]['file_path'] =  $result->file_path;
					$resultarr[$result->name_id]['school_name'] =  $resultschool[0]->school_name;
                    $resultarr[$result->name_id]['teacher_id'] =  $result->user_id;
                    $resultarr[$result->name_id]['subject_id'] =  $result->subject_id;
                    $resultarr[$result->name_id]['grade_id'] =  $result->grade_id;
//                    $resultarr[$result->name_id]['description'][] =  $result->description;
//                    $resultarr[$result->name_id]['file_path'][] =  $result->file_path;
                    
                }
                return $resultarr;
            } else {
                return false;
            }
        }
}