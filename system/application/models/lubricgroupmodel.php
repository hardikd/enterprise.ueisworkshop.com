<?php
class Lubricgroupmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getlubricgroupCount($state_id,$country_id,$district_id)
	{
		
		
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from lubric_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id " ;
		}
		else
		{
			$qry="Select count(*) as count 
	  			from lubric_groups og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=og.district_id and  og.district_id=$district_id " ;
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getlubricgroups($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		if($district_id=='all')
		{
		$qry="Select s.name,og.group_id,og.group_name,og.description,d.districts_name from lubric_groups og,districts d,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id $limit ";
		
		}
		else
		{
			$qry="Select s.name,og.group_id,og.group_name,og.description,d.districts_name from lubric_groups og,districts d ,states s  where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and og.district_id=d.district_id and og.district_id=$district_id $limit ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
	
	function getlubricgroupById($ob_id)
	{
		
		$qry = "Select og.group_id,og.group_name,og.description,og.district_id,d.state_id,d.country_id,d.districts_name from lubric_groups og,districts d where og.district_id=d.district_id and  og.group_id=".$ob_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	
	
	
	function add_lubricgroup()
	{
	  $data = array('group_name' => $this->input->post('group_name'),
					'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')
		
		
		);
	   try{
			$str = $this->db->insert_string('lubric_groups', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_lubricgroup()
	{
	
		
		$group_id=$this->input->post('group_id');
	$data = array('group_name' => $this->input->post('group_name'),
				   'description' => $this->input->post('description'),
					'district_id' => $this->input->post('district_id')	
		
		
		);
		
			
		$where = "group_id=".$group_id;		
		
		try{
			$str = $this->db->update_string('lubric_groups', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletelubricgroup($ob_id)
	{
		
		
		
		$qry = "delete from lubric_groups where group_id=$ob_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	function getalllubricgroupsbyDistrictID($district_id)
	{
	
	 $qry = "Select group_id,group_name,description from lubric_groups where district_id=$district_id order by sortorder asc ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getlubricgroupById($val);
			  
			  
			  if($data!=false && $data[0]['district_id']!=$pdata['end'])
			  {
					$status=1;	
				   $sdata = array('group_name' => $data[0]['group_name'],
					'description' => $data[0]['description'],
					'district_id' => $pdata['end']	
					);
					$str = $this->db->insert_string('lubric_groups', $sdata);
					$this->db->query($str);
					
					
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
	
}