<?php
class Incident_location_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_all_data($page=false,$per_page=false)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		
		}
		
		
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		//$this->db->order_by('id','DESC');
		$query = $this->db->get('incident_location');
		
		$result = $query->result();
		return $result;	
	}
	public function get_all_dataCount()
	{
		$this->db->where('is_delete',0);
		$this->db->where('district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('incident_location');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
public function get_all_incident_location_data($page=false,$per_page=false)
	{
		$this->db->where('is_delete',0);
		$this->db->order_by('id','DESC');
                if($this->session->userdata('district_id')){
                    $this->db->where('district_id',$this->session->userdata('district_id'));
                }
		$query = $this->db->get('incident_location');
                
		$result = $query->result();
		return $result;	
	}

	public function delete_incident_location($table,$field)
	   {
		$this->db->where('id',$this->input->post('id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_incident_locationById($where){
		$query = $this->db->get_where('incident_location',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
        
	

}
?>