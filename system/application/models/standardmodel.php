<?php
class Standardmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getallsubjects()
	{
		 if($this->session->userdata("login_type")=='admin')
	  {
	  
	  $qry = "Select s.dist_subject_id as subject_id ,s.subject_name from dist_subjects s,districts d where s.district_id=d.district_id ";
	  
	  }
	  else
	  {
		$district_id=$this->session->userdata('district_id');
		
		$qry = "Select s.dist_subject_id as subject_id ,s.subject_name from dist_subjects s,districts d where s.district_id=d.district_id and s.district_id=".$district_id;
	  
	  
	  }
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getstandardCount($state_id,$country_id,$district_id)
	{
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from standards s, districts d,dist_subjects ds where s.district_id=d.district_id and d.state_id=$state_id and d.country_id=$country_id and  s.subject_id=ds.dist_subject_id";
		}
		else
		{
	
		$qry="Select count(*) as count 
	  			from standards  s, districts d ,dist_subjects ds where s.district_id=d.district_id and  s.district_id=$district_id and  s.subject_id=ds.dist_subject_id" ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getstandards($page,$per_page,$state_id,$country_id,$district_id)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		
		
		
			if($district_id=='all')
		{
		$qry="Select d.districts_name,st.subject_name as subject,s.standard_id,s.status  from standards s,districts d ,dist_subjects st where  d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id  and s.subject_id=st.dist_subject_id  $limit ";
		}
		else
		{
			$qry="Select d.districts_name,st.subject_name as subject,s.standard_id,s.status  from standards s,districts d ,dist_subjects st where  d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id  and s.subject_id=st.dist_subject_id   and s.district_id=$district_id  $limit ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getstandardById($val);
			  
			  
			  if($data!=false && $data[0]['district_id']!=$pdata['end'])
			  {
					$already=$this->check_standard_exists_copy($data[0]['subject_id'],$pdata['end']);
					$status=1;	
				  if($already==false)
				  {
				  $sdata = array('subject_id' => $data[0]['subject_id'],
					'status' => 1,
					'district_id' => $pdata['end']	
					);
					$str = $this->db->insert_string('standards', $sdata);
					$this->db->query($str);
					$already=$this->db->insert_id();
					}
					
					$adata=$this->getstandarddataall($data[0]['standard_id']);
					  if($adata!=false)
					  {
						foreach($adata as $aval)
						{
						  $adata=array('grade' => $aval['grade'],
										'strand'=> $aval['strand'],
										'standard'=> $aval['standard'],
							'standard_id' => $already );
							
							$astr = $this->db->insert_string('standarddata', $adata);
							$this->db->query($astr);
							
						}
					  
					  }
					  
					  
					
			
			  
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
	function getstandarddataall($standard_id)
	{
		
				
		$qry="Select s.grade,s.strand,s.standard  from standarddata s,standards d  where  s.standard_id=d.standard_id and s.standard_id=$standard_id  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getstandardById($dist_id)
	{
		
		 $qry = "Select s.subject_id,st.subject_name as subject,s.standard_id,s.status,d.state_id,s.district_id,d.country_id  from standards s,dist_subjects st ,districts d where s.district_id=d.district_id and s.subject_id=st.dist_subject_id and s.standard_id=".$dist_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function check_standard_exists()
	{
	
		$subject_id=$this->input->post('subject_id');
		$district_id=$this->input->post('district_id');
		$qry="SELECT  standard_id from standards where subject_id=$subject_id and district_id=$district_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 0;
		}else{
			
		  return 1;
		
		}
	
	
	}
	function check_standard_exists_copy($subject_id,$district_id)
	{
	
		
		$qry="SELECT  standard_id from standards where subject_id=$subject_id and district_id=$district_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->standard_id;		
		}else{
			
		  return false;
		
		}
	
	
	}
	
	function add_standard()
	{
	 
	  $data = array('subject_id' => $this->input->post('subject_id'), 
					'district_id'=> $this->input->post('district_id'),
					'status'=> $this->input->post('status')
		
		
		);
	   try{
			$str = $this->db->insert_string('standards', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_standard()
	{
	
		
		$standard_id=$this->input->post('standard_id');
		
			$data = array('status' => $this->input->post('status')
		
		
		);
		
		
		
	
	
	
		
		
	
		
			
		$where = "standard_id=".$standard_id;		
		
		try{
			$str = $this->db->update_string('standards', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}

	function standardByDistrict()
	{
	
		//$subject_id=$this->input->post('subject_id');
		$district_id=$this->session->userdata('district_id');
		$qry="SELECT  standard_id from standards where district_id=$district_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return 1;

		}else{
			
		  return 0;
		
		}
	
	
	}

	function reportstandards(){
		$district_id = $this->session->userdata('district_id');
		$this->db->select('*');
		$this->db->where('district_id',$district_id);
		$this->db->from('report_standards');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function reportGrades(){
		$district_id = $this->session->userdata('district_id');
		$this->db->select('*');
		$this->db->where('district_id',$district_id);
		$this->db->from('report_grades');
		$query = $this->db->get();
		return $query->result_array();
	}

	function reportEfforts(){
		$district_id = $this->session->userdata('district_id');
		$this->db->select('*');
		$this->db->where('district_id',$district_id);
		$this->db->from('report_efforts');
		$query = $this->db->get();
		return $query->result_array();
	}
	
}