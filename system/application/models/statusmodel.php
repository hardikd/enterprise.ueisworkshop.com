<?php
class Statusmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getstatusCount()
	{
	
		if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		$qry="Select count(*) as count 	from status s where  s.district_id=$district_id " ;
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getstatuss($page,$per_page)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		$qry="Select b.status_id as status_id,b.status_name as status_name from status b where b.district_id=$district_id  limit $start, $per_page" ;
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getstatusById($status_id)
	{
		
		$qry = "Select status_id,status_name from status where status_id=".$status_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getstatusbydistrict()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select status_id,status_name from status where district_id=".$district_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function add_status()
	{
	  $district_id=$this->session->userdata('district_id');
	  $data = array('status_name' => $this->input->post('status_name'),
	                'district_id'=>$district_id
		
		);
	   try{
			$str = $this->db->insert_string('status', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_status()
	{
	
		
		 
		$data = array('status_name' => $this->input->post('status_name')
		
		
		);
		
		
		
		
		
		$status_id=$this->input->post('status_id');
	
		
			
		$where = "status_id=".$status_id;		
		
		try{
			$str = $this->db->update_string('status', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletestatus($status_id)
	{
		$qry = "delete from status where status_id=$status_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
}
?>