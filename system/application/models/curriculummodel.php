<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Curriculummodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
        
        function savecurriculum (){
            if($this->session->userdata('login_type')=='teacher'){
                $teacher_id = $this->session->userdata('teacher_id');
            }
            $curriculum = $this->input->post('curriculum');
            
            $now = date('Y-m-d H:i:s');
            $data = array('teacher_id'=>$teacher_id,'curriculum'=>$curriculum,'date_modified'=>$now);
            $result = $this->db->insert('curriculum',$data);
            if($result) {
                return true;
            } else {
                return false;
            }
        }
        function updatecurriculum (){
            if($this->session->userdata('login_type')=='teacher'){
                $teacher_id = $this->session->userdata('teacher_id');
            }
            $curriculum = $this->input->post('curriculum');
            $now = date('Y-m-d H:i:s');
            $data = array('teacher_id'=>$teacher_id,'curriculum'=>$curriculum,'date_modified'=>$now);
            $result = $this->db->update('curriculum',$data);
            if($result) {
                return true;
            } else {
                return false;
            }
        }
        function checkcurriculum (){
            if($this->session->userdata('login_type')=='teacher'){
                $teacher_id = $this->session->userdata('teacher_id');
            } else {
                $teacher_id = $this->input->post('teacher_id');
            }
//            echo $teacher_id;exit;
            $curriculum = $this->input->post('curriculum');
            $now = date('Y-m-d H:i:s');
            $where = array('teacher_id'=>$teacher_id);
            $query = $this->db->get_where('curriculum',$where);
            $result = $query->result();
//            echo $this->db->last_query();
//            print_r($result);exit;
            if($result) {
                return $result[0]->curriculum;
            } else {
                return false;
            }
        }
        function savecurriculumatlogin ($curriculum){
            if($this->session->userdata('login_type')=='teacher'){
                $teacher_id = $this->session->userdata('teacher_id');
            }
            //$curriculum = $this->input->post('curriculum');
            
            $now = date('Y-m-d H:i:s');
            $data = array('teacher_id'=>$teacher_id,'curriculum'=>$curriculum,'date_modified'=>$now);
            $result = $this->db->insert('curriculum',$data);
            if($result) {
                return true;
            } else {
                return false;
            }
        }
}