<?php
class Iep_tracker_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_iep_tracker_data_1($student_id)
	{
   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
	
	/*$this->db->select('*');
	$this->db->from('basic_student_information_form');
	$this->db->join('students','basic_student_information_form.student_id = students.student_id','LEFT');
	$this->db->join('dist_grades','basic_student_information_form.grade_id = dist_grades.dist_grade_id','LEFT');
//	$this->db->join('dates_description','basic_student_information_form.id = dates_description.iep_id','LEFT');
	$this->db->where('basic_student_information_form.user_id',$user_id);
	$this->db->where('basic_student_information_form.student_id',$student_id);
	$this->db->group_by('basic_student_information_form.id');
	$this->db->order_by('basic_student_information_form.id','desc');
	*/
	$district_id = $this->session->userdata('district_id');
	$qry = "SELECT *, (select dates_description from dates_description JOIN dates on dates_description.dates_id = dates.id where dates.dates = 'date of present meeting' and district_id = $district_id and dates_description.iep_id = basic_student_information_form.id) as present_date FROM basic_student_information_form LEFT JOIN `students` ON `basic_student_information_form`.`student_id` = `students`.`student_id` LEFT JOIN `dist_grades` ON `basic_student_information_form`.`grade_id` = `dist_grades`.`dist_grade_id` WHERE `basic_student_information_form`.`user_id` = $user_id AND `basic_student_information_form`.`student_id` = $student_id GROUP BY `basic_student_information_form`.`id` ORDER BY present_date desc";
	
	$query = $this->db->query($qry);
//	print_r($this->db->last_query());exit;
	$result = $query->result();
	
		
		return $result;	
	}

public function get_iep_tracker_data_by_teacher($student_id,$teacher_id)
	{
   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}
	
	/*$this->db->select('*');
	$this->db->from('basic_student_information_form');
	$this->db->join('students','basic_student_information_form.student_id = students.student_id','LEFT');
	$this->db->join('dist_grades','basic_student_information_form.grade_id = dist_grades.dist_grade_id','LEFT');
//	$this->db->join('dates_description','basic_student_information_form.id = dates_description.iep_id','LEFT');
	$this->db->where('basic_student_information_form.user_id',$user_id);
	$this->db->where('basic_student_information_form.student_id',$student_id);
	$this->db->group_by('basic_student_information_form.id');
	$this->db->order_by('basic_student_information_form.id','desc');
	*/
	$district_id = $this->session->userdata('district_id');
	$qry = "SELECT *, (select dates_description from dates_description JOIN dates on dates_description.dates_id = dates.id where dates.dates = 'date of present meeting' and district_id = $district_id and dates_description.iep_id = basic_student_information_form.id) as present_date FROM basic_student_information_form LEFT JOIN `students` ON `basic_student_information_form`.`student_id` = `students`.`student_id` LEFT JOIN `dist_grades` ON `basic_student_information_form`.`grade_id` = `dist_grades`.`dist_grade_id` WHERE `basic_student_information_form`.`user_id` = $user_id AND `basic_student_information_form`.`student_id` = $student_id AND `basic_student_information_form`.`teacher_id` = $teacher_id GROUP BY `basic_student_information_form`.`id` ORDER BY `basic_student_information_form`.`id` desc";
	
	$query = $this->db->query($qry);
//	print_r($this->db->last_query());exit;
	$result = $query->result();
	
		
		return $result;	
	}




 public function get_all_dates_data()
	{

	$this->db->select('*');
	$this->db->from('dates');
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
  
 	
	
	
public function get_iep_tracker_data()
	{
   if($this->session->userdata('login_type')=='teacher'){
                $user_id = $this->session->userdata('teacher_id');
            } else if($this->session->userdata('login_type')=='observer'){
                $user_id = $this->session->userdata('observer_id');
            }  else if($this->session->userdata('login_type')=='user'){
                $user_id = $this->session->userdata('dist_user_id');
			}

	$this->db->select('*');
	$this->db->from('basic_student_information_form');
	$this->db->join('students','basic_student_information_form.student_id = students.student_id','LEFT');
	$this->db->join('dist_grades','basic_student_information_form.grade_id = dist_grades.dist_grade_id','LEFT');
	$this->db->where('basic_student_information_form.user_id',$user_id);
	
	$query = $this->db->get();
	
		$result = $query->result();
		return $result;	
	}	
	
public function get_all_eligibility_data($result)
	{
   $this->db->select('*');
	$this->db->from('iep_eligibility');
	$this->db->where('iep_eligibility.iep_id ',$result);
	
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}
public function get_all_comprehensive_academic_achievement_data($result)
	{
   $this->db->select('*');
	$this->db->from('comprehensive_academic_achievement');
	$this->db->where('comprehensive_academic_achievement.iep_id ',$result);
	
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}	
public function get_all_iep_goal_data($iep_id)
	{
    $this->db->select('*');
	$this->db->from('create_student_goals');
	$this->db->where('create_student_goals.iep_id ',$iep_id);
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}	
public function get_all_english_language_development_data($result)
	{
   $this->db->select('*');
	$this->db->from('english_language_development');
	$this->db->where('english_language_development.iep_id ',$result);
	
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}
public function get_all_student_goals_data($result)
	{
   $this->db->select('*');
	$this->db->from('create_student_goals');
	$this->db->join('dist_grades','create_student_goals.grade = dist_grades.dist_grade_id','LEFT');
	$this->db->join('dist_subjects','create_student_goals.subject_id = dist_subjects.dist_subject_id','LEFT');
	$this->db->where('create_student_goals.iep_id ',$result);
	
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}		

public function get_determine_student_data($result)
	{
   $this->db->select('*');
	$this->db->from('determine_student_assessments');
	$this->db->where('determine_student_assessments.iep_id ',$result);
	
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}		

public function get_upload_iep_data($id)
	{
		

	$this->db->select('*');
	$this->db->from('basic_student_information_form');
	$this->db->join('teachers','basic_student_information_form.teacher_id = teachers.teacher_id','LEFT');
	$this->db->join('schools','basic_student_information_form.school_id = schools.school_id','LEFT');
	$this->db->join('dist_grades','basic_student_information_form.grade_id = dist_grades.dist_grade_id','LEFT');
	
	$this->db->where('basic_student_information_form.id',$id);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}

public function get_upload_iep_data_data($id)
	{
	$this->db->select('*');
	$this->db->from('basic_student_information_form');
	$this->db->join('students','basic_student_information_form.student_id = students.student_id','LEFT');
	$this->db->where('basic_student_information_form.id',$id);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}

 
public function get_meeting_attendees($id)
	{
		

	$this->db->select('*');
	$this->db->from('iep_meeting_attendees');
	$this->db->where('iep_meeting_attendees.iep_id',$id);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
 
 public function get_dates_data($id)
	{
		

	$this->db->select('*');
	$this->db->from('dates_description');
	$this->db->where('dates_description.iep_id',$id);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
  
 
       
 public function update_data($table,$field,$where)
	  {
  	  	// $where1 = ( id = $where );	
	//		print_r($where);
	//	exit;
		 $this->db->where('id', $where);
		 return $this->db->update($table,$field); 
			
		 
	  }	  
 public function iep_update_data($table,$field,$where)
	  {
  	  	// $where1 = ( id = $where );	
	//		print_r($where);
	//	exit;
		 $this->db->where('iep_id', $where);
		 return $this->db->update($table,$field); 
			
		 
	  }	  

 public function iep_goal_update_data($table,$field,$where)
	  {
  	  	// $where1 = ( id = $where );	
	//		print_r($where);
	//	exit;
		 $this->db->where('student_goals_id', $where);
		 return $this->db->update($table,$field); 
			
		 
	  }
	  
	  
public function delete_data($table,$where)
	{
		return $this->db->delete($table,$where);
	
	}	
	  
function check_lessonplan_exists($d,$start,$end)
	{
	
	
	if($teacher_id=$this->session->userdata('teacher_id'))
            $teacher_id=$this->session->userdata('teacher_id');
        else 
            $teacher_id=$this->input->post('teacher_id');
	  	
	
	   $qry = "Select subject_id from create_student_goals where date='$d' and teacher_id=$teacher_id and start='$start' and end='$end' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
function add_planbytimetable($start,$end,$date,$grade_id)
	{
		  $n=date('Y-m-d h:i:s');
		
		 if($this->session->userdata('teacher_id')){
                  $data = array('start' => $start,
					'end' => $end,	
					'date'=>$date,
					'teacher_id' => $this->session->userdata('teacher_id'),
                    'created'=>$n,					
		            'modified'=> $n	,						
					'grade'=>$grade_id,
					
		);
                 } else {
                    $data = array('start' => $start,
					'end' => $end,	
					'date'=>$date,
					'teacher_id' => $this->input->post('teacher_id'),
                    'created'=>$n,					
		            'modified'=> $n	,						
					'grade'=>$grade_id,
					
		); 
                 }
		
		
		
	   try{
			
			 
				$str = $this->db->insert_string('create_student_goals', $data);
				return 0;
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}		
	
	}    

function gettimetablebyteacher($day)
	{
	   if($this->session->userdata('teacher_id'))
		$teacher_id=$this->session->userdata('teacher_id');
           else 
               $teacher_id=$this->input->post('teacher_id');
                
		$sql="select p.period_id,c.grade_id,p.start_time,p.end_time  
		from time_table t,periods p,class_rooms c where t.class_room_id=c.class_room_id and t.teacher_grade_subject_id='$teacher_id' ; ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
function getalllessonplansnotsubject()
	{
	  $qry = "Select lesson_plan_id,tab from lesson_plans where lesson_plan_id!=1";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
	
	
public function get_summary_of_iep_plan($result)
	{
	$this->db->select('*');
	$this->db->from('summary_of_iep_plan');
	$this->db->where('summary_of_iep_plan.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
	
public function get_plan_eligibility($result)
	{
	$this->db->select('*');
	$this->db->from('plan_eligibility_data');
	$this->db->where('plan_eligibility_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
public function get_plan_curriculum($result)
	{
	$this->db->select('*');
	$this->db->from('plan_curriculum_data');
	$this->db->where('plan_curriculum_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}

public function get_plan_placement($result)
	{
	$this->db->select('*');
	$this->db->from('plan_placement_data');
	$this->db->where('plan_placement_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
public function get_instructionl_setting($result)
	{
	$this->db->select('*');
	$this->db->from('instructionl_setting_data');
	$this->db->where('instructionl_setting_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
public function get_additional_factors($result)
	{
	$this->db->select('*');
	$this->db->from('additional_factors_data');
	$this->db->where('additional_factors_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
public function get_accommodation_modifications($result)
	{
	$this->db->select('*');
	$this->db->from('accommodation_modifications_data');
	$this->db->where('accommodation_modifications_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
public function get_preparation($result)
	{
	$this->db->select('*');
	$this->db->from('preparation_data');
	$this->db->where('preparation_data.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
public function get_description_of_iep_services($result)
	{
	$this->db->select('*');
	$this->db->from('description_of_iep_services');
	$this->db->where('description_of_iep_services.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
public function get_agreement($result)
	{
	$this->db->select('*');
	$this->db->from('iep_agreement');
	$this->db->where('iep_agreement.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}
	
public function get_agreement_data($result)
	{
	$this->db->select('*');
	$this->db->from('iep_agreement');
	$this->db->join('indicate_parent_consent','iep_agreement.indicate_parent_consent = indicate_parent_consent.id','LEFT');
	$this->db->where('iep_agreement.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
	
public function get_iep_tracker_pdf($iep_id)
	{
   
	$this->db->select('*');
	$this->db->from('basic_student_information_form');
	$this->db->join('students','basic_student_information_form.student_id = students.student_id','LEFT');
	$this->db->join('dist_grades','basic_student_information_form.grade_id = dist_grades.dist_grade_id','LEFT');
	$this->db->join('iep_eligibility','basic_student_information_form.id = iep_eligibility.iep_id','LEFT');
	$this->db->join('determine_student_assessments','basic_student_information_form.id = determine_student_assessments.iep_id','LEFT');
	$this->db->join('description_of_iep_services','basic_student_information_form.id = description_of_iep_services.iep_id','LEFT');
	$this->db->join('english_language_development','basic_student_information_form.id = english_language_development.iep_id','LEFT');
	
	
	$this->db->where('basic_student_information_form.id',$iep_id);
	
	$query = $this->db->get();
	
	//print_r($this->db->last_query());exit;
	
		$result = $query->result();
		return $result;	
	}
	
	
	public function get_summary_of_academic_achievement($result)
	{
		

	$this->db->select('*');
	$this->db->from('summary_of_academic_achievement');
	$this->db->where('summary_of_academic_achievement.iep_id',$result);
	$query = $this->db->get();
	//print_r($this->db->last_query());exit;
	$result = $query->result();
		return $result;	
	}	
public function check_eligibility($iep_id) 
	{
			$this->db->where('iep_id',$iep_id);
			$this->db->from('plan_eligibility_data');
			$query = $this->db->get();
			return $query->num_rows;
		}
public function check_curriculum($iep_id) 
	{
			$this->db->where('iep_id',$iep_id);
			$this->db->from('plan_curriculum_data');
			$query = $this->db->get();
			return $query->num_rows;
		}
public function check_placement($iep_id) 
	{
			$this->db->where('iep_id',$iep_id);
			$this->db->from('plan_placement_data');
			$query = $this->db->get();
			return $query->num_rows;
		}


/*IEP goal plan*/
      function lessonplansbyteacherbytimetable($teacher_id,$subject_id)
	{
	  
	  if($teacher_id==false)
	  {
              if($this->session->userdata('teacher_id'))
		$teacher_id=$this->session->userdata('teacher_id');
              else
                  $teacher_id=$this->input->post('teacher_id');
	  }
	
	    $qry = "Select w.teacher_id,w.subject_id,ds.subject_name,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date from lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id  and w.subject_id=ds.dist_subject_id ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
       
/*end goal plan*/
	   function get_subject_in_standard()
	{
		
		 $district_id = $this->session->userdata('district_id');
		
		$qry="Select d.districts_name,st.subject_name as subject,s.subject_id,s.standard_id,s.status  from standards s,districts d ,dist_subjects st where  s.subject_id=st.dist_subject_id  and s.district_id=$district_id GROUP BY subject ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
   function get_all_standard($subject_id)
	{
		
		$qry="Select d.districts_name,st.subject_name as subject,s.subject_id,s.standard_id,s.status  from standards s,districts d ,dist_subjects st where  s.subject_id=st.dist_subject_id  and s.district_id=$district_id GROUP BY subject ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}	
public function get_all_standard_data($subject_id)
	{
   
	$this->db->select('*');
	$this->db->from('standards');
	$this->db->join('standarddata','standards.standard_id = standarddata.standard_id','LEFT');
	$this->db->where('standards.subject_id',$subject_id);
	
	$query = $this->db->get();
		$result = $query->result();
		return $result;	
	}

function get_subject_By_standards($subject_id)
	{
	$this->db->select('*');
	$this->db->from('standards');
	$this->db->join('standarddata','standards.standard_id = standarddata.standard_id','LEFT');
	$this->db->where('standards.subject_id',$subject_id);
	$this->db->group_by('standarddata.strand');
	$query = $this->db->get();
//	print_r($this->db->last_query());exit;
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}

	function getStudentByTeacherName($teacher_id,$name){


	$this->db->select('*');
	$this->db->from('teacher_students');
	$this->db->join('students','teacher_students.student_id = students.student_id','LEFT');
	$this->db->where('teacher_students.teacher_id',$teacher_id);
	$this->db->group_by('teacher_students.student_id');
	$query = $this->db->get();
		$result = $query->result();
		return $result;	

        }
function getparentById($student_goals_id)
	{
	 $this->db->select('*');
	$this->db->from('create_student_goals');
	$this->db->join('dist_grades','create_student_goals.grade = dist_grades.dist_grade_id','LEFT');
	$this->db->join('dist_subjects','create_student_goals.subject_id = dist_subjects.dist_subject_id','LEFT');
	$this->db->where('create_student_goals.student_goals_id ',$student_goals_id);	
	$query = $this->db->get();
//	print_r($this->db->last_query());exit;
    if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
function get_all_summary_of_academic_achievement($iep_id){


	$this->db->select('*');
	$this->db->from('summary_of_academic_achievement');
	$this->db->where('summary_of_academic_achievement',$iep_id);
	$this->db->group_by('summary_of_academic_achievement.iep_id');
	$query = $this->db->get();
		$result = $query->result();
		return $result;	

        }


}
?>