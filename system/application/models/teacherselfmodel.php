<?php
class Teacherselfmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	
	
	
	function createReport(){
		
	  $data = array('report_date' => $this->input->post('report_date'), 
					'school_id'=> $this->input->post('school_id'),
					'teacher_id'=> $this->input->post('teacher_id'),
					'subject_id'=> $this->input->post('subject_id'),
					'grade_id'=> $this->input->post('grade_id'),
					'students'=> $this->input->post('students'),
					'paraprofessionals'=> $this->input->post('paraprofessionals'),
					'observer_id'=> $this->input->post('observer_id'),
					'report_form'=> $this->input->post('form'),
					'period_lesson'=> $this->input->post('period_lesson'),
					'district_id'=>$this->session->userdata('district_id')
					
		
		
		);
	   try{
			$str = $this->db->insert_string('teacherself', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function getReportData($report_id)
	{
	
		if($this->session->userdata('login_type')=='teacher')
		{
		  $teacher_id=$this->session->userdata('teacher_id');
		  $qry="Select r.status,r.report_form,r.report_id,r.report_date,r.observer_id as observer_name,concat(t.firstname,' ',t.lastname) as teacher_name,sc.school_name,s.subject_name,g.grade_name,r.period_lesson,r.students,r.paraprofessionals from teacherself r, teachers t,schools sc,dist_subjects s,dist_grades g  where report_id=".$report_id." and r.teacher_id=".$teacher_id."  and r.teacher_id=t.teacher_id and r.school_id=sc.school_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id";
		}
		else
		{
		 $qry="Select r.status,r.report_form,r.report_id,r.report_date,r.observer_id as observer_name,concat(t.firstname,' ',t.lastname) as teacher_name,sc.school_name,s.subject_name,g.grade_name,r.period_lesson,r.students,r.paraprofessionals from teacherself r, teachers t,schools sc,dist_subjects s,dist_grades g  where report_id=".$report_id." and  r.teacher_id=t.teacher_id and r.school_id=sc.school_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id";
		
		}
		//echo $qry;exit;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function LockData($report_id)
	{
	   $data = array('status' => 'LOCKED'
		
		         );
		
		
			
		$where = "report_id=".$report_id;		
		
		try{
			$str = $this->db->update_string('teacherself', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	
	
	}
	function obsavereport()
	{
	  $status=true;
	  $group_id=$this->input->post('group_id');
	  
	  $report_id=$this->session->userdata('new_report_id');
	  if(!empty($group_id))
	  {
	    
	   $points=$this->obgetAllPoints($group_id,$this->session->userdata("district_id"));
	  // echo "<pre>";
	  // print_r($points);
	   //exit;
	   if($points!=false)
	   {
	   foreach($points as $val)
	   {
	     
		 if($val['group_type_id']!=2)
		 {
		    $acpoint_id=$val['point_id'];
			if($this->input->post($val['point_id'])==1)
			{
			 $value=1;
			}
			else
			{
			 $value=0;
			}
			
			$dqry = "delete from selfobservations where point_id=".$val['point_id']." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
		}
        else
		{
            
			if($val['ques_type']=='checkbox')
			{
				
			     $acpoint_id=$val['point_id'];
				 $post_id=$val['point_id'].'_'.$val['sub_group_id'];
			     $value=$this->input->post($post_id);
				 $dqry = "delete from selfobservations where point_id=".$val['point_id']." and report_id=$report_id and response='".$val['sub_group_id']."'";
		        $dquery = $this->db->query($dqry);
				 
			    	
			
			}
			else
			{
			  $acpoint_id=$val['point_id'];
				$value=$this->input->post($val['point_id']);
				$dqry = "delete from selfobservations where point_id=".$val['point_id']." and report_id=$report_id";
		       $dquery = $this->db->query($dqry);
			
			}
				

		}	
			
			//$dqry = "delete from selfobservations where point_id=".$val['point_id']." and report_id=$report_id";
		    //$dquery = $this->db->query($dqry);
			
			if($value!=0)
			{
			$data =array('report_id' => $report_id,
					'point_id' => $acpoint_id,
					'response' => $value
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('selfobservations', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		  
		 }
		 
	   
	   }
	   }
	   
	   
	   $dqry = "delete from selfobservations where group_id=".$group_id." and report_id=$report_id";
		$dquery = $this->db->query($dqry);
	   if($this->input->post('response-text'))
	   {
	     
	    
		$data =array('report_id' => $report_id,
					 'group_id' => $group_id,
					 'responsetext'=>$this->input->post('response-text')
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('selfobservations', $data);
			
			$this->db->query($str);
	   
	   }
	  
	  }
	 
	   return $status;
	
	}
	function obgetAllPoints($group_id,$district_id)
	{
		$qry = "Select osg.sub_group_id,osg.sub_group_name,op.point_id ,qt.ques_type,op.question,op.group_type_id from ques_types qt,observ_points op left join observation_sub_groups osg on  osg.point_id=op.point_id  where op.ques_type_id=qt.ques_type_id and op.group_id=$group_id and op.district_id=$district_id " ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function lisavereport()
	{
	  $status=true;
	  $group_id=$this->input->post('group_id');
	  
	  $report_id=$this->session->userdata('new_report_id');
	  if(!empty($group_id))
	  {
	   $points=$this->ligetAllPoints($group_id,$this->session->userdata("district_id"));
	  // echo "<pre>";
	  // print_r($points);
	   //exit;
	   if($points!=false)
	   {
	   foreach($points as $val)
	   {
	     
		 if($val['group_type_id']!=2)
		 {
		    $acpoint_id=$val['point_id'];
			if($this->input->post($val['point_id'])==1)
			{
			 $value=1;
			}
			else
			{
			 $value=0;
			}
			
			$dqry = "delete from selflickerts where point_id=".$val['point_id']." and report_id=$report_id";
		    $dquery = $this->db->query($dqry);
		}
        else
		{
            
			if($val['ques_type']=='checkbox')
			{
				
			     $acpoint_id=$val['point_id'];
				 $post_id=$val['point_id'].'_'.$val['sub_group_id'];
			     $value=$this->input->post($post_id);
				 $dqry = "delete from selflickerts where point_id=".$val['point_id']." and report_id=$report_id and response='".$val['sub_group_id']."'";
		        $dquery = $this->db->query($dqry);
				 
			    	
			
			}
			else
			{
			  $acpoint_id=$val['point_id'];
				$value=$this->input->post($val['point_id']);
				$dqry = "delete from selflickerts where point_id=".$val['point_id']." and report_id=$report_id";
		       $dquery = $this->db->query($dqry);
			
			}
				

		}	
			
			//$dqry = "delete from selflickerts where point_id=".$val['point_id']." and report_id=$report_id";
		    //$dquery = $this->db->query($dqry);
			
			if($value!=0)
			{
			$data =array('report_id' => $report_id,
					'point_id' => $acpoint_id,
					'response' => $value
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('selflickerts', $data);
			
			if($this->db->query($str))
			{
			  $status=true;
			}
			else
			{
			 $status=false;
			}
			if($status==false)
			{
				return $status;
			
			}
		  
		 }
		 
	   
	   }
	   }
	   
	   
	   $dqry = "delete from selflickerts where group_id=".$group_id." and report_id=$report_id";
		$dquery = $this->db->query($dqry);
	   if($this->input->post('response-text'))
	   {
	     
	    
		$data =array('report_id' => $report_id,
					 'group_id' => $group_id,
					 'responsetext'=>$this->input->post('response-text')
					);
			//print_r($data);
			//exit;
			$str = $this->db->insert_string('selflickerts', $data);
			
			$this->db->query($str);
	   
	   }
	  
	  }
	 
	   return $status;
	
	}
	function ligetAllPoints($group_id,$district_id)
	{
		$qry = "Select osg.sub_group_id,osg.sub_group_name,op.point_id ,qt.ques_type,op.question,op.group_type_id from ques_types qt,lickert_points op left join lickert_sub_groups osg on  osg.point_id=op.point_id  where op.ques_type_id=qt.ques_type_id and op.group_id=$group_id and op.district_id=$district_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function obgetReportPoints($report_id)
	{
		$qry = "Select group_id,responsetext,report_id,point_id,response from selfobservations where report_id=".$report_id ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function ligetReportPoints($report_id)
	{
		$qry = "Select group_id,responsetext,report_id,point_id,response from selflickerts where report_id=".$report_id ;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getReportTeacherCount()
	{
	
		 $teacher_id=$this->session->userdata('report_teacher_id');
		 $form=$this->session->userdata('reportform');
		
		 
			 $qry = "select count(*) as count from teacherself r where  r.teacher_id=$teacher_id and r.report_form='$form' ";
		
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getReportByTeacher($start, $per_page)
	{
	
		$teacher_id=$this->session->userdata('report_teacher_id');
		$form=$this->session->userdata('reportform');
		
		
			 $qry="Select  r.status,r.report_id,r.report_date,concat(t.firstname,'',t.lastname) as teacher_name,s.subject_name,g.grade_name,r.observer_id as observer_name from teacherself r,
		teachers t,dist_subjects s,dist_grades g where  r.teacher_id=$teacher_id and r.report_form='$form' and r.teacher_id=t.teacher_id and r.subject_id=s.dist_subject_id and r.grade_id=g.dist_grade_id 
		 limit $start, $per_page ";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getAllReport($report)
	{
	
	    $form=$this->session->userdata('reportform');
		$teacher_id=$this->session->userdata('teacher_id');
		$district_id=$this->session->userdata('district_id');
		
		
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		
			
			$qry = "select report_id from teacherself r where r.teacher_id=$teacher_id and r.district_id=$district_id and  r.report_form='$form' " ;
		
		}
		/*if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		
		  $qry = "select report_id from reports r where r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id  and r.report_form='$form' " ;
		
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		 
			 $qry = "select report_id from reports r where r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id  and r.report_form='$form' " ;
		
		
		}*/
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getAllsectional($group_id,$report)
	{
		
		$form=$this->session->userdata('reportform');
		$teacher_id=$this->session->userdata('teacher_id');
		$district_id=$this->session->userdata('district_id');
		
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		if($form=='forma')
		{
			
			$qry = "select b.report_id,b.point_id,b.response from teacherself r,observation_groups g,observ_points ob left join selfobservations b on ob.point_id=b.point_id where  r.teacher_id=$teacher_id and r.district_id=$district_id and  b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from teacherself r,lickert_groups g,lickert_points ob left join selflickerts b on ob.point_id=b.point_id where  r.teacher_id=$teacher_id and r.district_id=$district_id and  b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		
		}
		}
/*if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select b.report_id,b.point_id,b.response from reports r,observation_groups g,observ_points ob left join observations b on ob.point_id=b.point_id where  r.grade_id=$grade_id and r.teacher_id=$teacher_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		}
		else if($form=='formc')
		{
			$qry = "select b.report_id,b.point_id,b.response from reports r,lickert_groups g,lickert_points ob left join lickerts b on ob.point_id=b.point_id where  r.grade_id=$grade_id and r.teacher_id=$teacher_id and r.district_id=$district_id and b.report_id=r.report_id and g.group_id=ob.group_id and g.group_id=$group_id; " ;
		
		}
		
		}*/
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllqualitative($group_id,$report)
	{
		$from=$this->session->userdata('report_from');
		$to=$this->session->userdata('report_to');
		$form=$this->session->userdata('reportform');
		$teacher_id=$this->session->userdata('teacher_id');
		$district_id=$this->session->userdata('district_id');
		
		if($report=='teacher')
		{
		$teacher_id=$this->session->userdata('report_criteria_id');	
		if($form=='forma')
		{
			$qry = "select ob.responsetext,r.report_date from selfobservations ob,teacherself r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from selflickerts ob,teacherself r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.teacher_id=$teacher_id  and r.district_id=$district_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		}
		/*if($report=='observer')
		{
		  $observer_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.observer_id=$observer_id and r.district_id=$district_id and r.teacher_id=$teacher_id and  r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		
		}
		
		}
		if($report=='grade')
		{
		  $grade_id=$this->session->userdata('report_criteria_id');	
		  if($form=='forma')
		{
		  $qry = "select ob.responsetext,r.report_date from observations ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		else  if($form=='formc')
		{
			$qry = "select ob.responsetext,r.report_date from lickerts ob,reports r where year(r.report_date)>=$from and year(r.report_date)<=$to and r.grade_id=$grade_id and r.district_id=$district_id and r.teacher_id=$teacher_id and r.report_form='$form' and  r.report_id=ob.report_id and  ob.group_id=$group_id " ;
		}
		
		}*/
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
}