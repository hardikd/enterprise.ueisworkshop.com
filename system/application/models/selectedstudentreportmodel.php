<?php
class Selectedstudentreportmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getstuddetail()
	{
		
		$qry="SELECT * FROM users where user_type=2";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		
		
		}
		
   function getcschools($data)
   {
	 	  

	 $qry="SELECT * FROM  schools where school_id='".$data."'";
	
		$query = $this->db->query($qry);
		$temp =$query->result_array();	
				
	   
	   return $temp;
   }
    function getteacher_schoolandgrade($teacher_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM teachers where teacher_id='".$teacher_id."'";
 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		$qry="select Distinct c.class_room_id, c.name from class_rooms c, teacher_students ts ,teachers t where ts.class_room_id = c.class_room_id  AND ts.teacher_id = t.teacher_id AND t.school_id = c.school_id AND ts.teacher_id=".$teacher_id;  
	   $query = $this->db->query($qry);
	   $arr["class"]= $query->result_array();
		return $arr;
		    
   }
   function getobserver_schoolandgrade($observer_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM observers where observer_id='".$observer_id."'";
 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		return $arr;
   }
   
   function student_delete($userid)
   {
	
	$this->db->delete('users', array('UserID' => $userid)); 
		
		   
   }
   
   function getrecord_student($userid)
   {
	   $qry="SELECT * FROM users where UserID='".$userid."'";
	
		$query = $this->db->query($qry);
		return $query->result_array();	
		    
   }
   
   function student_update($data)
   {
	
	   $userid = $data['userid'];
	   $name = $data['student_name'];
	   $surname = $data['student_surname'];
	   $email = $data['student_email'];
	   
	    $data = array(
               'Name' => $name,
               'Surname' => $surname,
               'email' => $email
            );

		$this->db->where('UserID', $userid);
		$this->db->update('users', $data); 

	   
   }
   
   function getstuperformance($userid)
   {
	   $qry="SELECT * FROM user_quizzes where user_id='".$userid."'";
	   $query = $this->db->query($qry);
		return $query->result_array();	 
   }
   
   function getassessment($assess)
   {
	 $qry="SELECT * FROM assignments where id='".$assess."'";
	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
   function getsubdetail($subid)
   {
	 $qry="SELECT * FROM  dist_subjects where dist_subject_id='".$subid."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
    function getobservername($school_id)
   {
	 $qry="SELECT * FROM  observers where school_id='".$school_id."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
    function getteachername($school_id)
   {
	 $qry="SELECT * FROM  teachers where school_id='".$school_id."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   function getschoolname($school_id)
   {
	  $districtid=$this->session->userdata('district_id');
	   $qry="SELECT * FROM  schools where school_id='".$school_id."' and district_id='".$districtid."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   
   function getschooltype($district_id)
   {
	   $qry='select * from school_type where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
   }
   
   function getallschools($schooltypeid)
   {
	   $qry='select * from schools where school_type_id="'.$schooltypeid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   
   // get students
   
   function getstud($classroomid,$schoolid,$gradeid)
   {
	  $district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');
//		$qry='select distinct student_id from  teacher_students where class_room_id="'.$classroomid.'" ';
 
	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students t where u.student_id=t.student_id and u.user_type=2 and t.teacher_id = ".$teacher_id." and t.class_room_id = ".$classroomid." and u.grade_id=".$gradeid." and u.school_id=".$schoolid." and u.district_id ='".$district_id."'"; 
	 }elseif(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u ,teacher_students t where u.student_id=t.student_id and u.user_type=2 and t.teacher_id = ".$classroomid." and u.grade_id=".$gradeid." and u.school_id=".$schoolid." and u.district_id ='".$district_id."'"; 
	 }
	 else
	 {	 
	 	$qry="SELECT DISTINCT u.* FROM users u ,teacher_students t where u.student_id=t.student_id and u.user_type=2 and t.teacher_id = ".$classroomid." and u.grade_id=".$gradeid." and u.school_id=".$schoolid." and u.district_id ='".$district_id."'";
	 }
	 
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}     
	  
	$qry='select distinct student_id from  teacher_students where class_room_id="'.$classroomid.'" ';
		
	   $query = $this->db->query($qry)->result_array();
	
	return $query;	  
		/*if($query->num_rows()>0){
			
			
			$ff=array();
			
			$data =  $query->result_array();
			$ff[]=$data;
			
			$userids=array();
			
			foreach($data as $k => $v)
			{
				$userids[] = $data[$k]['UserID'];
			}
			
			
			$testids=array();
			for($i=0;$i<count($userids);$i++)
			{
			$sql='select assignment_id from user_quizzes where user_id="'.$userids[$i].'" ';
			$ss = $this->db->query($sql);
			$testids[] =  $ss->result_array();
			}
			$assessmentids=array();
			foreach($testids as $g => $h)
			{
			$assessmentids[]=$testids[$g];
			}
			$finaltestids=array();
			for($j=0;$j<count($assessmentids);$j++)
			{
				for($k=0;$k<count($assessmentids[$j]);$k++)
				{
					 $finalassessids= $assessmentids[$j][$k]['assignment_id'];
					 $zz='select id,assignment_name from assignments where id="'.$finalassessids.'" ';
					$result = $this->db->query($zz);
					$finaltestids[] = $result->result_array();
					
				}
			}
			$ff[]=$finaltestids;
			echo '<pre>';
			print_r($finaltestids);
			exit;*/
			 
			 
		//}else{
		//	return false;
		//}   
   }
	
		public   function assNameByUserid($userid)
  	 {
	   
	 		$sql='select assignment_id from user_quizzes where user_id="'.$userid.'" ';
			$ss = $this->db->query($sql);
			$testids =  $ss->result_array();
			
			//print_r($testids);
		
			$finaltestids=array();
			
			foreach($testids as $key=>$val)
			{					
				$zz='select id,assignment_name from assignments where id="'.$testids[$key]['assignment_id'].'" ';
				$result = $this->db->query($zz);
				$finaltestids[] = $result->result_array();
			}
			
			return $finaltestids;
			
	   
   }
   
   function getquestionsdetail($aid,$cluster)
   {
	  $sql = "select q.* from questions q , assignments a where q.quiz_id = a.quiz_id and q.test_cluster = '".$cluster."' and a.id =".$aid;
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getClustersByClusterGroup($aid,$cluster)
   {
	  $sql = "select Distinct test_cluster from questions q , assignments a where q.quiz_id = a.quiz_id and q.test_cluster ".utf8_decode($cluster)." and a.id =".$aid." order by q.id";
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   function getClustersByClusterGroup2($aid,$cluster)
   {
	    $sql = "select  q.test_cluster,aq.answer_text from questions q, answers_quiz aq, question_groups qg, assignments a where aq.group_id = qg.id and qg.question_id = q.id and q.quiz_id = a.quiz_id and radio_ans=1 and q.test_cluster ".$cluster." and a.id =".$aid." group by q.test_cluster order by q.id";
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
    
   function getquestionresult($aid,$qid,$studu_id)
   {
	  $sql = "select is_correct_ans from user_answers ua , user_quizzes uq  where uq.id = ua.user_quiz_id and uq.user_id = '".$studu_id."' and uq.assignment_id =".$aid." and ua.question_id = ".$qid." group by question_id" ;
	  
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getquestionresult6yes_no($aid,$qid,$studu_id)
   {
	 $sql = "select ua.user_answer_text, ua.is_radio_ans_correct from user_answers ua , user_quizzes uq where uq.id = ua.user_quiz_id and uq.user_id = '".$studu_id."' and uq.assignment_id =".$aid." and ua.question_id = ".$qid." and ua.is_radio_ans_correct is not null";
	  
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getquestionresult6($aid,$qid,$studu_id)
   {
	 $sql = "select aq.answer_text, ua.is_correct_ans, ua.is_radio_ans_correct,ua.cant_decode,ua.with_difficulty from answers_quiz aq, user_answers ua , user_quizzes uq where ua.answer_id = aq.id and uq.id = ua.user_quiz_id and uq.user_id = '".$studu_id."' and uq.assignment_id =".$aid." and ua.question_id = ".$qid." and ua.is_radio_ans_correct is null";
	  
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getquestionresultYesNo_Oraciones($aid,$ansid,$studu_id)
   {
	  $sql = "select ua.user_answer_text, ua.is_radio_ans_correct from user_answers ua , user_quizzes uq where uq.id = ua.user_quiz_id and uq.user_id = '".$studu_id."' and uq.assignment_id =".$aid." and ua.answer_id = ".$ansid." ";
	 
	    $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getquestionYesNo_Oraciones($qid)
   {
	 $sql = "select aq.id,aq.answer_text from  answers_quiz aq,question_groups qg where aq.group_id = qg.id and qg.question_id = ".$qid." and aq.radio_ans = 1";
	 
	    $query = $this->db->query($sql);
	   if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 
   }
   
   function getassessmentreportbyid($testid,$userid)
   {
	    $qry='select * from  user_quizzes where assignment_id="'.$testid.'" and user_id="'.$userid.'"';
	
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
			
			
		
		}else{
			return false;
		} 
   }
	
	public   function getgrades()
   {
	  	$district_id = $this->session->userdata('district_id');
	 $qry='select * from  dist_grades where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   function getcountry($country_id)
   {
	    $qry='select * from  countries_quiz where id="'.$country_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
    function getdistrict($distid)
   {
	    $qry='select * from  districts where district_id="'.$distid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
    function getstate($stateid)
   {
	    $qry='select * from  states where state_id ="'.$stateid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   function getquizid($assessmentids)
   {
	
	
	$quizids=array();
	for($i=0;$i<count($assessmentids);$i++)
	{
	 $qry="select quiz_id from  assignments where id ={$assessmentids[$i]}";	
	   $query = $this->db->query($qry);
		$quizids[]= $query->result_array();
		   
	}
	return $quizids;
   }
	
	  function getquestionsquiz($quizids)
   {
	
	
	$questions=array();
	for($i=0;$i<count($quizids);$i++)
	{
	 $qry="select * from  questions where quiz_id ={$quizids[$i]}";	
	   $query = $this->db->query($qry);
		$questions[]= $query->result_array();
		   
	}
	return $questions;
   }
   
   function getassessments($assessmentids)
   {
	   
	$usertests=array();
	for($i=0;$i<count($assessmentids);$i++)
	{
	 $qry="select * from  user_quizzes where user_id ={$assessmentids[$i]}";	
	   $query = $this->db->query($qry);
		$usertests[]= $query->result_array();
		   
	}
	return $usertests;
   }
   
   function getassessmentnames($testids)
   {
	 	$finaldata=array();
	for($i=0;$i<count($testids);$i++)
	{	$usertests=array();
		for($j=0;$j<count($testids[$i]);$j++)
		{
			$testids[$i][$j];
	  $qry="select id,assignment_name from  assignments where id ={$testids[$i][$j]}";
	
		
	   $query = $this->db->query($qry);
	   
		$usertests[]= $query->result_array();
		
		}
		$finaldata[]=$usertests;
	}
	
	return $finaldata;  
	
   }
   
  function getquizidbytestid($quizid)
   {
	 $qry="SELECT quiz_id FROM assignments where id='".$quizid."'";
	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
    function getquestionsbyquizid($quizid)
   {
	 
//$qry="SELECT * FROM questions where quiz_id='".$quizid."'";
	 $qry="SELECT distinct(test_cluster) FROM questions where quiz_id='".$quizid."'";
	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
    function getschoolteacher($grade_id,$school_id)
   {
	   $qry="select * from teachers where school_id=".$school_id;  
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   function getclassrooms($gradeid,$schoolid)
   {
	 $qry='select * from  class_rooms where grade_id="'.$gradeid.'" and school_id="'.$schoolid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}     
   }
   
   function getstuddetails($studentids,$schoolid,$gradeid)
   {
	   $data=array();
	 for($i=0;$i<count($studentids);$i++)
		{
	  $qry='select * from  users where student_id="'.$studentids[$i]['student_id'].'" and school_id="'.$schoolid.'" and grade_id="'.$gradeid.'"';
		$query = $this->db->query($qry);
		$data[] =  $query->result_array();
		}
		
		return array_values(array_filter($data));
	
	   
   }
  	
	function getrepeatclusterinfo($quiz_id)
	{
		
		//$qry='select id from questions where test_cluster="'.$cluster.'" and quiz_id="'.$quiz_id.'"';
	$qry="select count(id) as cnt, test_cluster from questions where quiz_id=".$quiz_id." and test_cluster!='' group by test_cluster order by id" ;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
	}
	
	function displayQuestionsResult($aid,$cluster,$studu_id)
	{
			$questions = $this->getquestionsdetail($aid,$cluster);
			$totques ='';
			$trueQuecount = 0;
			$falseQuecount = 0;
			$ParttrueQuecount = 0;
			foreach($questions as $val)
			{
			 	$ans = '';
				$qid = $val['id'];
			 	if($val['question_type_id']!= 6)
				{	
					 $queresult = $this->getquestionresult($aid,$qid,$studu_id);
				 	 if($queresult[0]['is_correct_ans'] == 1)
						 $ans = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
					 	$ans = 0;
				}
	 			else 
				{
					 $queresult = $this->getquestionresult6($aid,$qid,$studu_id);
					 $tot = count($queresult); $cc ='';
					 if($queresult[0]['is_correct_ans'] == 1)
							 $cc = 1;
					 elseif($queresult[0]['is_correct_ans'] == 0)
							$cc = 0;
					  $rc = 0; //echo"<pre>"; print_r($queresult);
					  if($tot >1)
					  {
						foreach($queresult as $qr)
						{
							if($qr['is_radio_ans_correct'] == 1)
							 $rc++;
						}
					  }
						if($cc == 1 && $rc == $tot)
							 $ans = 1;
						else if($cc == 1 && $rc < $tot)
				 			 $ans = 2;
						else if($cc == 0 && $rc <= $tot && $rc >0)
				 			 $ans = 2;
						if($cc != 1 && $rc != $tot)
							 $ans = 0;	 
				}
				 if($ans == 1)
					 $trueQuecount++;
				 if($ans == 0)
					 $falseQuecount++;
				 if($ans == 2)
					 $ParttrueQuecount++;
	  		}
				$disp ='';
				if($ParttrueQuecount >=1)
				{
					$trueQuecount = $trueQuecount + $ParttrueQuecount;
					$disptrueQuecount = '<font color="#FF9900">'.$trueQuecount.'</font>';
				}
				else
				{
					$disptrueQuecount = '<font color="#009900">'.$trueQuecount.'</font>';
				}
				
				$dispfalseQuecount = '<font color="#FF0000">'.$falseQuecount.'</font>';
			 $result = array("disptrueQuecount"=>$disptrueQuecount,"dispfalseQuecount"=>$dispfalseQuecount);
			 return $result;
	}
	
   
}

?>