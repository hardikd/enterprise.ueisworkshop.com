<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Assessmentmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
        
        public  function GetStudentByTeacherId($id,$school_id)
        {
            $sql = "SELECT * FROM `users` where `student_id` in (SELECT DISTINCT `student_id` FROM `teacher_students` WHERE `teacher_id` =".$id.") and `school_id` = ".$school_id;
            $query = $this->db->query($sql);
            $students =$query->result_array();	
				
	   
	   return $students;
        }
        
        public function getStudentDetails($student_id){
            $sql= "select u.* from users u "."left join students s on u.UserID = s.UserID "."where u.UserID =".$student_id;
            $query = $this->db->query($sql);
            return $studentdetails =$query->result_array();		
        }
        
        public function getAllAssignmentCats($district_id){
            $this->db->select('*');
            $this->db->where('district_id',$district_id);
            return  $this->db->get('cats')->result();
        }
        
        public function getAssignmentByStudent($cat_id,$student_id){
//            $this->db->select('assignment_id');
//            $studentassignments = $this->db->get_where('assignment_users',array('user_id'=>$student_id))->result();
////            print_r($studentassignments);exit;
//            
//            $this->db->select('assignments.id');
//            $this->db->from('quizzes');
//            $this->db->join('assignments', 'quizzes.id=assignments.quiz_id');
//            $this->db->where('quizzes.cat_id',$cat_id);
//            $query = $this->db->get();
//            $quizes = $query->result();
////            echo $this->db->last_query();
//            
////            $categoryassignments = array_intersect($studentassignments,$quizes);
//            $assignmentidarr = array();
//            foreach($studentassignments as $studentsval){
//                foreach($quizes as $categoryval){
//                    if($studentsval->assignment_id==$categoryval->id){
//                        $assignmentidarr[] = $categoryval->id;
//                    }
//                }
//            }
////            print_r($assignmentidarr);exit;
//            $this->db->select('*,assignments.id as assid,user_quizzes.status as quiz_status,user_quizzes.id as user_quizzes_id ');
//            $this->db->where_in('assignments.id',$assignmentidarr);
//            $this->db->where('user_id',$student_id);
//            $this->db->from('assignments');
//            $this->db->join('quizzes','quizzes.id=assignments.quiz_id');
//            $this->db->join('user_quizzes','user_quizzes.assignment_id=assignments.id');
//            $assignmentqry = $this->db->get();
            
            $sql = "select a.id as assid,a.*, q.* ,ifnull(ua.status,0) as user_quiz_status,ua.status as quiz_status,ua.id as user_quizzes_id from assignments a left join quizzes q on a.quiz_id = q.id left join user_quizzes ua on ua.assignment_id=a.id and ua.archived=0 and ua.user_id=$student_id where a.status = 1 and a.id in ( select assignment_id from assignment_users where user_id = $student_id) AND q.cat_id = $cat_id order by a.added_date desc";
            $assignmentqry = $this->db->query($sql);
//            echo $this->db->last_query();
            $assignmentdetails = $assignmentqry->result();
//            print_r($assignmentdetails);exit;
           return $assignmentdetails;
        }
        
        function resetAssignmentByStudent($id,$user_id){
            $this->db->where('id',$id);
            $this->db->where('user_id',$user_id);
           return $update = $this->db->update('user_quizzes',array('status'=>1,'finish_date'=>NULL,'pass_score_point'=>NULL,'pass_score_perc'=>NULL));
            
        }
}