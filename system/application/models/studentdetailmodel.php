<?php
class Studentdetailmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
/*	function getstuddetail()
	{
		
		$qry="SELECT * FROM users where user_type=2";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		
		
		}*/
		
   function getcschools($data)
   {
	 	  

	 $qry="SELECT * FROM  schools where school_id='".$data."'";
	
		$query = $this->db->query($qry);
		$temp =$query->result_array();	
				
	   
	   return $temp;
   }
   
   function student_delete($userid)
   {
	
	$this->db->delete('users', array('UserID' => $userid)); 
		
		   
   }
   
   function getrecord_student($userid)
   {
	   $qry="SELECT * FROM users where UserID='".$userid."'";
	
		$query = $this->db->query($qry);
		return $query->result_array();	
		    
   }
   function getteacher_schoolandgrade($teacher_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM teachers where teacher_id='".$teacher_id."'";
 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		return $arr;
		    
   }
   function getobserver_schoolandgrade($observer_id,$district_id)
   {
	   $arr = array();
	   $qry="SELECT school_id FROM observers where observer_id='".$observer_id."'";
 		$query = $this->db->query($qry);
		$arr["school_id"]= $query->result_array();	
	  $qry='select * from  dist_grades where district_id="'.$district_id.'"';
   		$query = $this->db->query($qry);
		$arr["grades"]= $query->result_array();
		return $arr;
   }
   function student_update($data)
   {
	
	   $userid = $data['userid'];
	   $name = $data['student_name'];
	   $surname = $data['student_surname'];
	   $email = $data['student_email'];
	   
	    $data = array(
               'Name' => $name,
               'Surname' => $surname,
               'email' => $email
            );

		$this->db->where('UserID', $userid);
		$this->db->update('users', $data); 

	   
   }
   
   function getstuperformance($userid)
   {
	   $qry="SELECT * FROM user_quizzes where user_id='".$userid."' order by added_date Asc";
	   $query = $this->db->query($qry);
		return $query->result_array();	 
   }
   
   function getassessment($assess)
   {
	 $qry="SELECT * FROM assignments where id='".$assess."'";
	   $query = $this->db->query($qry);
		return $query->result_array();  
   }
   
   function getsubdetail($subid)
   {
	 $qry="SELECT * FROM  dist_subjects where dist_subject_id='".$subid."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
    function getobservername($school_id)
   {
	 $qry="SELECT * FROM  observers where school_id='".$school_id."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
    function getteachername($school_id)
   {
	 $qry="SELECT * FROM  teachers where school_id='".$school_id."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   function getschoolname($school_id)
   {
	  $districtid=$this->session->userdata('district_id');
	   $qry="SELECT * FROM  schools where school_id='".$school_id."' and district_id='".$districtid."'";
	
	   $query = $this->db->query($qry);
		return $query->result_array();  
		
   }
   
   function getschooltype($district_id)
   {
	   $qry='select * from school_type where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
   }
   
   function getallschools($schooltypeid)
   {
	   $qry='select * from schools where school_type_id="'.$schooltypeid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   function getstud($schid,$gradeid)
   {
   	 $district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');
	 
	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students t where u.student_id=t.student_id and u.user_type=2 and t.teacher_id = ".$teacher_id." and u.grade_id=".$gradeid." and u.school_id=".$schid." and u.district_id ='".$district_id."'"; 
	 }elseif(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , observers o where u.school_id=o.school_id and u.user_type=2 and o.observer_id = ".$observer_id." and u.grade_id=".$gradeid." and u.school_id=".$schid." and u.district_id ='".$district_id."'"; 
	 }
	 else
	 {	 
		$qry='select * from users where school_id="'.$schid.'" and grade_id="'.$gradeid.'" and district_id="'.$district_id.'"';
		 }
	
 
	 
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
	
	  function getgrades()
   {
	  	$district_id = $this->session->userdata('district_id');
	 $qry='select * from  dist_grades where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   function getcountry($country_id)
   {
	    $qry='select * from  countries_quiz where id="'.$country_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
    function getdistrict($distid)
   {
	    $qry='select * from  districts where district_id="'.$distid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
    function getstate($stateid)
   {
	    $qry='select * from  states where state_id ="'.$stateid.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
   public function record_count() {
  
  	 $district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');
	 
	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students t where u.student_id=t.student_id and u.user_type=2 and t.teacher_id = ".$teacher_id." and u.district_id ='".$district_id."'"; 
	 }elseif(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , observers o where u.school_id=o.school_id and u.user_type=2 and o.observer_id = ".$observer_id." and u.district_id ='".$district_id."'"; 
	 }
	 else
	 {	 
		 $qry="SELECT DISTINCT * FROM users where user_type=2 and district_id ='".$district_id."'";
	 }
	
		$query = $this->db->query($qry);
		return $query->num_rows();
		
    }
	
	 public function getstuddetail($limit, $start) {
     
	$district_id = $this->session->userdata('district_id');
	$teacher_id = $this->session->userdata('teacher_id');
	$observer_id = $this->session->userdata('observer_id');
	 
	 if(isset($teacher_id) && $teacher_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , teacher_students t where u.student_id=t.student_id and u.user_type=2 and t.teacher_id = ".$teacher_id." and u.district_id ='".$district_id."' limit ".$start." , ".$limit." "; 
	 }elseif(isset($observer_id) && $observer_id !='')
	 {
		$qry="SELECT DISTINCT u.* FROM users u , observers o where u.school_id=o.school_id and u.user_type=2 and o.observer_id = ".$observer_id." and u.district_id ='".$district_id."' limit ".$start." , ".$limit." "; 
	 }
	 else
	 {	 
		 $qry="SELECT DISTINCT * FROM users where user_type=2 and district_id ='".$district_id."' limit ".$start." , ".$limit." ";
	 }
//	 echo $qry; exit;
	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}

	 }

}
?>