<?php
class School_typemodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getschool_typescount($state_id,$country_id,$district_id)
	{
	
		if($district_id=='all')
		{
		$qry="Select count(*) as count 
	  			from school_type s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id ";
		}
		else
		{
				$qry="Select count(*) as count 
	  			from school_type  s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and  s.district_id=$district_id " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getschool_types($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		
                if($district_id=='all')
		{
		$qry="Select st.name,s.tab,s.school_type_id as school_type_id,d.district_id as district_id,d.districts_name as districtsname from school_type s,districts d,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id ";
		}
		else
		{
			$qry="Select st.name,s.tab,s.school_type_id as school_type_id,d.district_id as district_id,d.districts_name as districtsname from school_type s,districts d ,states st where st.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and s.district_id=d.district_id and s.district_id=$district_id  ";
		}
                
                if($page!=false && $per_page!=false){
                    $page -= 1;
                    $start = $page * $per_page;
                    $qry .="limit $start, $per_page";
                }
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function add_plan()
	{
	  $data = array('tab' => $this->input->post('tab'),
	  'district_id'=> $this->input->post('district_id')
					);
	   try{
			$str = $this->db->insert_string('school_type', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_plan()
	{
	
		
		$plan_id=$this->input->post('plan_id');
		$data = array('tab' => $this->input->post('tab'),
		'district_id'=> $this->input->post('district_id')
					);
			
			
		$where = "school_type_id=".$plan_id;		
		
		try{
			$str = $this->db->update_string('school_type', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	
	function deleteplan($plan_id)
	{
		$qry = "delete from school_type where school_type_id=$plan_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getplanById($plan_id)
	{
	    $qry = "Select s.school_type_id,s.tab,d.state_id,s.district_id,d.country_id from school_type s,districts d where s.district_id=d.district_id and s.school_type_id=".$plan_id;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getallplans($district_id=false)
	{
	 if($district_id==false)
	 {
	 if($this->session->userdata("login_type")=='admin')
		{
		 $district_id=$this->session->userdata('district_value_tabs');
		}
		else
		{
	 
		$district_id=$this->session->userdata('district_id');
	  }
	  }
	  $qry = "Select school_type_id,tab from school_type where district_id=$district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
		
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
	
}