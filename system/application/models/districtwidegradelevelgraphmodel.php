<?php
class Districtwidegradelevelgraphmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
 
 
  function getallschools($district_id)
   {
	   $qry='select * from schools where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
  
   
    function getcategoryandgrade($district_id)
   {
	   $qry='select * from cats where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
	
	   $array = array();
		if($query->num_rows()>0){
			  $array['cat'] = $query->result_array();
		} 
	 
	 	$qry='select * from  dist_grades where district_id ="'.$district_id.'" order by dist_grade_id';
	    $query = $this->db->query($qry);
		 
	   if($query->num_rows()>0){
			  $array['grade'] = $query->result_array();
		}
		
	//		 echo "<pre>";
		//print_r($array); exit;
		return $array;
		
  }
   
    function getdistrictgrade($district_id)
    {
	   $qry='select * from  dist_grades where district_id ="'.$district_id.'"';
	   $query = $this->db->query($qry);
	   	if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }  
 
 
 	function getTestName($district_id,$cat_id,$grade_id,$fDate,$tDate)
	{
	 error_reporting(0);
	  if($grade_id != '-1' || $cat_id != '-1')	
	  {
	  // echo $cat_id;
	 	if($grade_id == 0)
		{
		 	$query_testname = "SELECT distinct ass.assignment_name FROM assignments ass, quizzes qz WHERE ass.quiz_id=qz.id and ass.district_id = ".$district_id." AND qz.cat_id =".$cat_id." order by ass.id";
		}
		else
		{
			$query_testname  = "select distinct a.assignment_name from assignments a, `quizzes` qz, assignment_users au , users u where a.quiz_id=qz.id AND qz.cat_id =".$cat_id." and a.id = au.assignment_id and au.user_id = u.UserID and u.grade_id = ".$grade_id." and u.district_id = ".$district_id." order by a.id"; 
	
		} 
	//	echo $query_testname;  
		$query = $this->db->query($query_testname);
		$result = array();
		$result = $query->result_array();
		$testname =array();
		$rec =  count($result[0]);
	//	echo "<pre>";
	//	print_r($result);exit;
		if($rec > 0)
		{ 
	 		$IntCount=0;
			foreach($result as $key => $value)
			{			
				$testname['tname'][] = $value['assignment_name'];
				//$testname['tid'][] = $value['id'];
			}
		//	echo "<pre>";
		//	print_r($testname);exit;
			if($grade_id == 0)
			{
				$gradearray = $this->getdistrictgrade($district_id);
			}
			else
			{
				$schoolarray = $this->getallschools($district_id);
			}
			//print_r($gradearray);exit;
		//	$IntCount = 0;
			$arrResult = array();
			if($grade_id == 0 && count($gradearray) > 0)
			{
				foreach($gradearray as $key=>$value)
				{
					for($intTest=0;$intTest<count($testname['tname']);$intTest++)
					{ 
					
						$testNAME ='';
					 	$testNAME = $testname['tname'][$intTest];
						$arrResult[$testNAME][$value['grade_name']] = $this->getAvgScoreAllByTestName($grade_id,$value['dist_grade_id'],$testname['tname'][$intTest],$district_id,$fDate,$tDate);
						//print_r($arrResult);exit;
					 }
					//$arrResult[$value['grade_name']]['display_name'] = $value['grade_name'];
			//	 	$IntCount++;
				}
				//add test name into array
				//for($intTest=0;$intTest<count($testname);$intTest++)
//				{ 
	//				$arrResult['testname'][$intTest]= $testname['tname'][$intTest];
		//		}
		//echo"<pre>"; print_r($arrResult); exit;
				return $arrResult;
			}
			
			else if($grade_id > 0 && count($schoolarray) > 0)
			{ 
				foreach($schoolarray as $key=>$value)
				{  
					for($intTest=0;$intTest<count($testname['tname']);$intTest++)
					{ 
						$testNAME ='';
					 	$testNAME = $testname['tname'][$intTest];
						$arrResult[$testNAME][$value['school_name']] = $this->getAvgScoreAllByTestName($grade_id,$value['school_id'],$testname['tname'][$intTest],$district_id,$fDate,$tDate);
		 			}
					//$arrResult[$IntCount]['display_name'] = $value['school_name'];
				}
				 return $arrResult;
			}
			
		}
		else
		{
			return false;
		}
	  }
	  else
	  {
	  	return false;
	   }
	}
	
	function getAvgScoreAllByTestName($grade_id,$gradeOrSchool_id,$test_name,$district_id,$fDate,$tDate)
	{
		if($grade_id == 0)
		{
			$and = "and u.grade_id = ".$gradeOrSchool_id;
			$anduid = "user_id in (SELECT UserID from users where grade_id = ".$gradeOrSchool_id." and district_id = ".$district_id.")";   
		}
		else
		{
			$and = "and u.school_id = ".$gradeOrSchool_id." AND u.grade_id=".$grade_id;
			$anduid = "user_id in (SELECT UserID from users where district_id = ".$district_id." AND grade_id=".$grade_id." and school_id = ".$gradeOrSchool_id.")";   
		}
		
	 	 	$query_score = "SELECT avg(pass_score_perc) as avgscore FROM `user_quizzes` where `assignment_id` in (SELECT a.id  FROM assignments a, assignment_users au , users u where a.id = au.assignment_id and au.user_id = u.UserID and a.assignment_name ='".$test_name."' and u.user_type=2 ".$and." )  and ".$anduid." and pass_score_perc IS NOT NULL AND DATE(finish_date) between '".$fDate."' ANd '".$tDate."' ORDER BY id ";
	//echo $query_score ;exit;
	 
		 $query = $this->db->query($query_score);
		 $result = array();
		if($query->num_rows()>0){
			$result = $query->result_array();
		//	echo $result[0]['avgscore'];
			return $result[0]['avgscore'];
		}else{
			return false;
		}  
 	
	}
	
 
}
?>