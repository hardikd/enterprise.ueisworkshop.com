<?php
class Articraftsmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	function add_photo()
	{
	 
	preg_match("/\.([^\.]+)$/", $_FILES['photo']['name'], $matches);
			$ext = $matches[1];
  	 $data = array('teacher_id' => $this->session->userdata('teacher_id'),
	               'name' => $this->input->post('name'),
				   'file_type' => $ext,
	         	   'type' => 'photo');
	   try{
			$str = $this->db->insert_string('artifacts', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}	
	
	}
	function add_video()
	{
	 
	 preg_match("/\.([^\.]+)$/", $_FILES['video']['name'], $matches);
			$ext = $matches[1];
  	 $data = array('teacher_id' => $this->session->userdata('teacher_id'),
	               'name' => $this->input->post('videoname'),
				   'file_type' => $ext,
	         	   'type' => 'video');
	   try{
			$str = $this->db->insert_string('artifacts', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}	
	
	}
	
	function add_file()
	{
	 
	 preg_match("/\.([^\.]+)$/", $_FILES['file']['name'], $matches);
			$ext = $matches[1]; 
  	 $data = array('teacher_id' => $this->session->userdata('teacher_id'),
	               'name' => $this->input->post('filename'),
				   'file_type' => $ext,
	         	   'type' => 'file');
	   try{
			$str = $this->db->insert_string('artifacts', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}	
	
	}
	
	function getphotosCount($date)
	{
	    if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		
		if($date==false)
		{
		$qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='photo' and archived is null " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		
		 $qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='photo' and archived='$date2' " ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getphotos($page,$per_page,$date)
	{
	    $page -= 1;
		$start = $page * $per_page;
		if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select file_type,name,artifact_id	from artifacts where teacher_id=$teacher_id and type='photo' and archived is null limit $start, $per_page " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 
		 $qry="Select file_type,name,artifact_id	from artifacts where teacher_id=$teacher_id and type='photo' and archived='$date2' limit $start, $per_page " ;
		 
		 
		 }
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
	
	function addnotes($notes)
	{
	  
	  
	  $notesdata=$this->getnotes(false);
	  if($notesdata!=false)
	  {
	  
	  $data = array('notes' => $notes
		
		
		);
		
			
		$where = "artifact_id=".$notesdata[0]['artifact_id'];		
		
		try{
			$str = $this->db->update_string('artifacts', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	  }
	else {

         $teacher_id=$this->session->userdata('teacher_id');
    $data = array('notes' => $notes,
				  'teacher_id'=>$teacher_id,
                   'type'=>'notes'				  
		
		
		);
		
			
		
		
		try{
			$str = $this->db->insert_string('artifacts', $data);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}



	}		
	
	}
	
	function getnotes($date)
	{
	 if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		
		if($date==false)
		{
			  $qry="select artifact_id,notes from artifacts where teacher_id=$teacher_id and type='notes' and archived is null ";
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 
		  $qry="select artifact_id,notes from artifacts where teacher_id=$teacher_id and type='notes' and archived='$date2' ";
		 
		}
	 
	
	
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return false;
		}
	}
	
	function add_link()
	{
	 
	 
  	 $data = array('teacher_id' => $this->session->userdata('teacher_id'),
	               'name' => $this->input->post('linkname'),
				   'link' => $this->input->post('link'),
	         	   'type' => 'link');
	   try{
			$str = $this->db->insert_string('artifacts', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}	
	
	}
	
	function getlinksCount($date)
	{
	    if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='link' and archived is null " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 $qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='link' and archived='$date2' " ;
		 
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getlinks($page,$per_page,$date)
	{
	    $page -= 1;
		$start = $page * $per_page;
		if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select link,name,artifact_id	from artifacts where teacher_id=$teacher_id and type='link' and archived is null limit $start, $per_page " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 $qry="Select link,name,artifact_id	from artifacts where teacher_id=$teacher_id and type='link' and archived='$date2' limit $start, $per_page " ;
		 
		}

		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
	function getvideosCount($date)
	{
	    if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='video' and archived is null " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 $qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='video' and archived='$date2' " ;
		 
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getvideos($page,$per_page,$date)
	{
	    $page -= 1;
		$start = $page * $per_page;
		if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select name,artifact_id,file_type	from artifacts where teacher_id=$teacher_id and type='video' and archived is null  limit $start, $per_page " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 $qry="Select name,artifact_id,file_type	from artifacts where teacher_id=$teacher_id and type='video' and archived='$date2'  limit $start, $per_page " ;
		 
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
	function getfilesCount($date)
	{
	    if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='file' and archived is null " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 $qry="Select count(*) as count 
	  			from artifacts where teacher_id=$teacher_id and type='file' and archived='$date2' " ;
		 
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getfiles($page,$per_page,$date)
	{
	    $page -= 1;
		$start = $page * $per_page;
		if($this->session->userdata('login_type')=='teacher')
		{
		
		 $teacher_id=$this->session->userdata('teacher_id');
		}
		else
		{
		
			$teacher_id=$this->session->userdata('arti_teacher_id');
		
		
		}
		if($date==false)
		{
			$qry="Select name,artifact_id,file_type	from artifacts where teacher_id=$teacher_id and type='file' and archived is null limit $start, $per_page " ;
		}
		else
		{
		 $date1=explode('-',$date);
		 $date2=$date1[2].'-'.$date1[0].'-'.$date1[1];
		 $qry="Select name,artifact_id,file_type	from artifacts where teacher_id=$teacher_id and type='file' and archived='$date2' limit $start, $per_page " ;
		 
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
}