<?php
class Classroomteacherreportmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getTeacherByGidSidDid($district_id,$scid,$gid)
	{		
		//$qry="Select b.name,b.class_room_id as class_room_id,s.school_name as school_name,g.grade_name as grade_name from class_rooms b,schools s,dist_grades g where b.school_id=s.school_id and b.grade_id=g.dist_grade_id and s.district_id='$district_id' and g.dist_grade_id='$gid' and s.school_id='$scid'";
		$qry ="SELECT s.teacher_grade_subject_id, t.firstname, t.lastname, t.teacher_id, g.grade_name, d.subject_name,d.dist_subject_id , sc.school_name
FROM teacher_grade_subjects s, schools sc, teachers t, dist_grades g, dist_subjects d, grade_subjects gs
WHERE s.teacher_id = t.teacher_id
AND t.school_id = sc.school_id
AND s.grade_subject_id = gs.grade_subject_id
AND gs.grade_id = g.dist_grade_id
AND gs.subject_id = d.dist_subject_id
AND sc.district_id ='$district_id'
AND sc.school_id ='$scid' order by g.grade_name ASC ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	
	function getAllTeacher($arr,$stid)
	{
	
		$techarr = array();
		foreach($arr as $key => $val)
		{
			$name = $arr[$key]['name'];
			$class_room_id = $arr[$key]['class_room_id'];
			$school_name = $arr[$key]['school_name'];
			$grade_name = $arr[$key]['grade_name'];			
		
			$qry="Select * from teacher_students where class_room_id='".$class_room_id."' ";
			
			$techarr[] = $this->db->query($qry)->result_array();			
		
		}
		
		return $techarr;
			
	}
	
	
	function totalTecherStudent($tid)
	{		
		$qry="Select * from  teacher_students where teacher_id='".$tid."' group by student_id ";
		
		$query = $this->db->query($qry)->result_array();
		
		return $query;
		
		
	}
	function totalAssStudent($assidarr,$totalStudent)
	{		
		//echo '<pre>';
		//print_r($totalStudent);
		
		$takStarray = array();
		
		foreach($assidarr as $key => $val)
		{			
			@$assid = $assidarr[$key];
			foreach($totalStudent as $kk => $val)
			{	
			
				$qry="Select UserID from  users  where  student_id='".$totalStudent[$kk]['student_id']."' ";		
				$query = $this->db->query($qry)->result_array();		
				$UserID = @$query[0]['UserID'];
				
				$qry1="Select * from  assignment_users  where  user_id='".$UserID."' and assignment_id = '$assid' ";		
				$query1 = $this->db->query($qry1)->result_array();		
					
					$takStarray[] = $query1;
				
				
			}			
			
		}
		
		return $takStarray;
		
		//return $query;
		
	}
	
	function getStudentBysid($sid)
	{		
		$qry="Select * from  assignments where subject_id='".$sid."' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	function getAssmentByid($sid)
	{		
		$qry="Select * from  assignments where subject_id='".$sid."' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	function getAssmentName($sub)
	{
		$qry="Select * from  assignments where subject_id='".$sub."' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
		function countassessment($ass_id)
	{
		$qry="Select * from  user_quizzes where assignment_id ='".$ass_id."' ";
		
		$query = $this->db->query($qry);
		return $query->result_array();		
		
	}
	
		function getQuizName($quizid)
	{
		$qry="Select quiz_name from  quizzes where id='".$quizid."' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	function getAllTeacherById($tid)
	{		
		$qry="Select * from  teachers where teacher_id='".$tid."' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	function getTotalstudentBttid($tid,$crid)
	{		
		$qry="Select * from  teacher_students where teacher_id='".$tid."' and class_room_id='$crid' ";
		
		$query = $this->db->query($qry)->result_array();		
		
		
		echo '<pre>';
		print_r($query);
		//return  $query;
	
	
	}
	
	
	
	
	function getPeriods($tid,$classid)
	{		
		$qry="Select * from  teacher_students  where teacher_id='$tid' and class_room_id='$classid' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	
	
	
	function getschooltype($district_id)
	{		
		$qry="Select * from  school_type where district_id='".$district_id."' ";
		
		$query = $this->db->query($qry);
		
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	}
	
	function getschoolgrades($district_id)
	{
		
	$qry="Select * from dist_grades where district_id='".$district_id."'";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function getStudent($scid,$gid)
	{		
	 $qry="Select * from users where school_id='".$scid."' and 	grade_id='".$gid."' limit 5";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0)
		{
			return $query->result_array();		
		}
		else
		{
			return false;
		}	
	}
	
	
	function getSubjectById($tid)
	{
		$qry="Select * from teacher_grade_subjects where teacher_id='".$tid."' ";
		
		$query = $this->db->query($qry)->result_array();
		
		$assgn = array();
		
		foreach($query as $key => $val)
		{
			$grade_subject_id = $query[$key]['grade_subject_id'];
			
			$qry1= "Select * from assignments where subject_id='".$grade_subject_id."' ";
			
			$assgn[] = $query1 = $this->db->query($qry1)->result_array();
			//echo '<pre>';
		
		
		}
		
		return $assgn;	
		
	}
	
	
	function getClassroom($scid,$gid)
	{
		
		 $qry="Select * from class_rooms where school_id='".$scid."' and 	grade_id='".$gid."' limit 5 ";		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	public function fetchGradeName($grid)
	{
		$str =  "select grade_name from dist_grades where dist_grade_id='$grid' ";
		$data = $this->db->query($str)->result_array();
		$grade_name =  @$data[0]['grade_name'];
		return $grade_name;
		exit;
		
	}
	public function fetchSchoolTypeName($styid)
	{
		$str =  "select tab from school_type where school_type_id='$styid' ";
		$data = $this->db->query($str)->result_array();
		$tab =  @$data[0]['tab'];
		return $tab;
		exit;
		
	}
	public function fetchSchoolName($sid)
	{
		$str =  "select school_name from schools where school_id='$sid' ";
		$data = $this->db->query($str)->result_array();
		$school_name =  @$data[0]['school_name'];
		return $school_name;
		exit;
		
	}
	public function fetchSubjectName($sid)
	{
		$str =  "select * from dist_subjects where dist_subject_id='$sid' ";
		$data = $this->db->query($str)->result_array();
		
		return $data;
		exit;
		
	}
	
	
	function getallSchoolIdByClassid($scid)
	{
		
		$qry="Select * from teacher_students where class_room_id='".$scid."'  group by student_id limit 3 ";		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$res =  $query->result_array();		
		
			
			return $res;
			
			
		}else{
			return false;
		}
	
	}
	
	
	
	function getallusers($school_id,$gradeid)
	{
		$temp=array();
		for($i=0;$i<count($school_id);$i++)
			{
			  $qry="Select * from  users where  school_id='".$school_id[$i]."' and grade_id='".$gradeid."' and user_type='2'  limit 5";
			
			$query = $this->db->query($qry);
			$temp[] =  $query->result_array();
			}
			
			
		foreach($temp as $r=>$v)
		{
			if(empty($v))
			{
			
				return $temp = 0;
			}
			else
			{
			
				return $temp;
			}
		}
	}
	
	function studentscore($ssid,$usern,$uid)
	{  
		$temp = array();
		$qry = "select * from user_quizzes where user_id='$ssid' limit 3 ";	
		
		$query2 = $this->db->query($qry);
		$temp[] =  $query2->result_array();		
		$temp[] = $usern;
		$temp[] = $uid;
		
		return $temp;
		
	}
	function studentscoreByid($ssid)
	{	
		$qry4 = "select * from user_quizzes where user_id='$ssid' limit 3 ";			
		$query4 = $this->db->query($qry4);
		$temp =  $query4->result_array();	
		return $temp;		
	}
	
	
// 	<h1> Individual School Report</h1>	

	function studentscoreByWrkid($ssid)
	{	
 $qry4 = "select * from users,user_quizzes where users.student_id='$ssid' and user_quizzes.user_id = users.UserID";			
		$query4 = $this->db->query($qry4);
		$temp =  $query4->result_array();		
		
		return $temp;		
	}

// all school by grade working
	public function getAllSchool($district_id,$grdid)
	{
	
		$str1 = "select * from schools where district_id='$district_id'";
		$query41 = $this->db->query($str1);
		$schlId =  $query41->result_array();		
		
		//echo '<pre>';
		//print_r($schlId);
		
		$stdata_by_gr_arr = array();		
		
		foreach($schlId as $key => $val)
		{
			 $school_id = $schlId[$key]['school_id'];
			 $school_name = $schlId[$key]['school_name'];
			
				$str = "select * from users where grade_id='$grdid' and school_id='$school_id'   limit 3";
				$query4 = $this->db->query($str);
							
				$stdata_by_gr_arr[] =  $query4->result_array();
			
			
		}
		
	
		return $stdata_by_gr_arr;
		
	}
	
	public function schoolNameFun($scid)
	{
		$str = "select * from schools where  school_id='$scid'";
		$query4 = $this->db->query($str)->result_array();
		
		$stname = $query4[0]['school_name'];
		return $stname;
	}
	public function schoolStdAvg($uid)
	{
		$str = "select avg(pass_score_perc) from user_quizzes where  user_id='$uid'";
		$query4 = $this->db->query($str)->result_array();			
			
		return $query4;
	}
// all school working


public function getAllSchoolwithDid($district_id)
	{
	
		$str1 = "select * from schools where district_id='$district_id' limit 5";
		$query41 = $this->db->query($str1);
		$schlId =  $query41->result_array();		
		
		foreach($schlId as $key => $val)
		{
			 $school_id = $schlId[$key]['school_id'];
			 $school_name = $schlId[$key]['school_name'];
			
				$str = "select * from users where school_id='$school_id'   limit 3";
				$query4 = $this->db->query($str);
							
				$stdata_by_gr_arr[] =  $query4->result_array();
			
			
		}
		
	
		return $stdata_by_gr_arr;
		
	
	
		
	}
	
}