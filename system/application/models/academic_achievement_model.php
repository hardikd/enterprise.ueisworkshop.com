<?php
class Academic_achievement_model extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert($table,$field)
   {
 	return $this->db->insert($table,$field);
   }
 	
public function get_all_data($page=false,$per_page=false)
	{
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		//$limit=" limit $start, $per_page ";
		$this->db->limit($per_page, $start);
		}
		else
		{
			$limit='';
		
		}
	$this->db->select('*');
	$this->db->from('academic_achievement');
	$this->db->join('dist_subjects','academic_achievement.subject_id = dist_subjects.dist_subject_id','LEFT');
	$this->db->where('academic_achievement.district_id',$this->session->userdata('district_id'));
	//$this->db->join('standard_language','standard_name.standard_language_id = standard_language.id','LEFT');
	$this->db->where('is_delete','0');
	 //$this->db->order_by('id','desc');
	
	
	$query = $this->db->get();
//print_r($this->db->last_query());exit;
		
		$result = $query->result();
		return $result;	
	}
	public function get_all_dataCount()
	{
		
		$this->db->where('is_delete',0);
		$this->db->where('academic_achievement.district_id',$this->session->userdata('district_id'));
		$query = $this->db->get('academic_achievement');
		$result = $query->num_rows();
		return $result;	
	}
  
  public function fetchobject($res)
	  {
     	 return $res->result();
	  }
public function get_per($table,$where)
	{
		return $this->db->get_where($table,$where);
	}	
public function update($table,$field,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$field);
	}
public function get_all_academic_achievement_data($page=false,$per_page=false)
	{
		$this->db->where('is_delete',0);
		$this->db->order_by('id','DESC');
                if($this->session->userdata('district_id')){
                    $this->db->where('district_id',$this->session->userdata('district_id'));
                }
		$query = $this->db->get('academic_achievement');
                
		$result = $query->result();
		return $result;	
	}

	public function delete_academic_achievement($table,$field)
	   {
		$this->db->where('id',$this->input->post('id'));
		$this->db->update($table,$field);
		//print_r($this->db->last_query());exit;
		return true;
	   }	
	   
	public function get_academic_achievementById($where){
		$query = $this->db->get_where('academic_achievement',$where);
		if($query){
			return $query->result();	
		} else {
			return false;	
		}
		
	}
        
        

}
?>