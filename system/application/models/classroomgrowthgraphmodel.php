<?php
class Classroomgrowthgraphmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
 
 
  function getclassrooms($grade_id,$school_id)
   {
	   $qry="select * from class_rooms where school_id=".$school_id." and grade_id=".$grade_id;  
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   function getschoolteacher($grade_id,$school_id)
   {
	   $qry="select * from teachers where school_id=".$school_id;  
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
     
    function getcategoryandgrade($district_id)
   {
	   $qry='select * from cats where district_id="'.$district_id.'"';
	   $query = $this->db->query($qry);
	
	   $array = array();
	   $array['0'] = array("district_id"=>$district_id);
		if($query->num_rows()>0){
			  $array['catarray'] = $query->result_array();
		} 
	 
	  $qry='select * from schools where district_id="'.$district_id.'"';
	    $query = $this->db->query($qry);
		 
	   if($query->num_rows()>0){
			  $array['schoolarray'] = $query->result_array();
		}
		
		$qry='select * from  dist_grades where district_id ="'.$district_id.'"';
	    $query = $this->db->query($qry);
		 
	   if($query->num_rows()>0){
			  $array['gradearray'] = $query->result_array();
		}
 		return $array;
		
  }
  
   function getcategoryandgradeObserver($observer_id)
   {
	   $qry='select c.* from cats c ,schools s, observers o where c.district_id=s.district_id AND s.school_id = o.school_id AND o.observer_id = '.$observer_id;
	   $query = $this->db->query($qry);
	
	   $array = array();
	      $array['0'] = array("observer_id"=>$observer_id);
	 
		if($query->num_rows()>0){
			  $array['catarray'] = $query->result_array();
		} 
	 
	 $qry='select * from  dist_grades d left join schools s on d.district_id = s.district_id left join observers o on s.school_id = o.school_id  where o.observer_id="'.$observer_id.'"';
	   $query = $this->db->query($qry);
  	  
	   	if($query->num_rows()>0){
				$array['gradearray'] = $query->result_array();
		}
//			 echo "<pre>";
//		print_r($array); exit;
		return $array;
		
  }
  function getcategoryandclassofTeacher($teacher_id)
   {
	   $qry='select c.* from cats c ,schools s, teachers t where c.district_id=s.district_id AND s.school_id = t.school_id AND t.teacher_id = '.$teacher_id;
	   $query = $this->db->query($qry);
	
	   $array = array();
	      $array['0'] = array("teacher_id"=>$teacher_id);
	 
		if($query->num_rows()>0){
			  $array['catarray'] = $query->result_array();
		} 
		
		$qry="select Distinct c.class_room_id, c.name from class_rooms c, teacher_students ts ,teachers t where ts.class_room_id = c.class_room_id  AND ts.teacher_id = t.teacher_id AND t.school_id = c.school_id AND ts.teacher_id=".$teacher_id;  
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			$array['classarray'] = $query->result_array();
		} 

//			 echo "<pre>";
//		print_r($query->result_array()); exit;
		return $array;
		
  }
   
   
    function getdistrictgrade($district_id)
    {
	   $qry='select * from  dist_grades where district_id ="'.$district_id.'"';
	   $query = $this->db->query($qry);
	   	if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }  
 	function getobserverschool($observer_id)
   {
     $qry='select s.*,o.observer_id from schools s left join observers o on s.school_id = o.school_id where o.observer_id= '.$observer_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
 
   function getstudents($class_id)
   {	
		$and='';
		if($this->session->userdata('teacher_id')!='')
			$and =" and ts.teacher_id=".$this->session->userdata('teacher_id')." and ts.class_room_id = ".$class_id;
		elseif($this->session->userdata('observer_id')!='')
			$and =" and ts.teacher_id = ".$class_id;
		elseif($this->session->userdata('district_id')!='')
			$and =" and ts.teacher_id = ".$class_id." and u.district_id=".$this->session->userdata('district_id');
			
	   $qry="select Distinct u.UserID,s.firstname,s.lastname from students s, teacher_students ts, users u where s.student_id = ts.student_id and u.student_id = ts.student_id ".$and;
	   //echo "fdsf";
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
 
 	function getGraphData($cat_id,$grade_id,$school_id,$class_id,$fDate,$tDate)
	{
		 error_reporting(0);
	 if($cat_id != -1 && $class_id != -1)	
	  {
	  	$where = ''; $and ='';
	  	if($school_id !='' && $grade_id !='')
		{
			$where = " and u.grade_id = ".$grade_id." and u.school_id =".$school_id;
			$and = "and u.student_id in (SELECT student_id from teacher_students where teacher_id = ".$class_id.")";
		}
		else if($grade_id !='')
		{
			$where = " and u.grade_id = ".$grade_id;
			$and = "and u.student_id in (SELECT student_id from teacher_students where teacher_id = ".$class_id.")";
		}
		else
		{	  	
 		$and = "and u.student_id in (SELECT student_id from teacher_students where class_room_id = ".$class_id.")";
		}
	 	$query_testname  = "select distinct a.assignment_name from assignments a, quizzes qz, assignment_users au , users u where a.quiz_id=qz.id AND qz.cat_id =".$cat_id." and a.id = au.assignment_id and au.user_id = u.UserID ".$and."".$where."  order by a.assignment_name"; //distinct a.id,
		
		//echo $query_testname ; exit;
	 	$query = $this->db->query($query_testname); 
		$result = array();
		$result = $query->result_array();
		$testname =array();
		$rec =  count($result);

		if($rec > 0)
		{ 
	 		$IntCount=0;
			foreach($result as $key => $value)
			{			
				$testname['tname'][] = $value['assignment_name'];
				//$testname['tid'][] = $value['id'];
			}
			$classroomarray = $this->getstudents($class_id);
		 
	 		$arrResult = array();
			
			if($class_id > 0 && count($classroomarray) > 0)
			{ 
				foreach($classroomarray as $key=>$value)
				{  
			//	echo count($testname['tid'])."-";
					for($intTest=0;$intTest<count($testname['tname']);$intTest++)
					{ 
						$testNAME ='';$avgscore =0;
					 	$testNAME = $testname['tname'][$intTest];
						$studentname = $value['firstname']." ".$value['lastname'];
						$avgscore = $this->getAvgScoreAllByTestName($value['UserID'],$testname['tname'][$intTest],$fDate,$tDate);
		 	 			$arrResult[$testNAME][$studentname] = number_format($avgscore,2,'.','');
					}
				}
		 
				 return $arrResult;
			}
			
		}
		else
		{
			return false;
		}
	  }
	  else
	  {
	  	return false;
	   }
	}
	
	function getAvgScoreAllByTestName($userid,$test_name,$fDate,$tDate)
	{	
	  	$query_score = "SELECT avg(pass_score_perc) as avgscore FROM `user_quizzes` where  `assignment_id` in (SELECT a.id  FROM assignments a, assignment_users au , users u where a.id = au.assignment_id and au.user_id = u.UserID and u.UserID = ".$userid." and a.assignment_name ='".$test_name."' and u.user_type=2) and user_id = ".$userid." and pass_score_perc IS NOT NULL AND DATE(finish_date) between '".$fDate."' ANd '".$tDate."' ORDER BY id ";
	//echo 	$query_score; exit;
 	 $query = $this->db->query($query_score);
		 $result = array();
		if($query->num_rows()>0){
			$result = $query->result_array();
		//	echo $result[0]['avgscore'];
			return $result[0]['avgscore'];
		}else{
			return false;
		}  
 	
	}
	
	
	 
  

}
?>
