<?php
class eldrubricmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function geteldrubricCount($subject_id)
	{
	     
			
			$qry="Select count(*) as count from standards s,standarddata sd,dist_subjects d where   d.dist_subject_id=s.subject_id and s.subject_id=$subject_id and s.standard_id=sd.standard_id  " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function geteldrubrics($page,$per_page,$subject_id)
	{
		
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		
		
		$qry="Select  sd.standarddata_id as subject_id ,d.subject_name as subject,dg.grade_name as grade,sd.strand,sd.standard from standards s,standarddata sd , dist_subjects d,dist_grades dg where   d.dist_subject_id=s.subject_id and dg.dist_grade_id=sd.grade and s.subject_id=$subject_id and s.standard_id=sd.standard_id $limit " ;
			
			//$qry="Select ds.dist_subject_id as subject_id,ds.subject_name from dist_subjects ds ,districts d where  d.district_id=ds.district_id and  ds.district_id=$district_id and ds.eld=1 $limit " ;
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
		
	
	
	function getAllsubgroups($point_id)
	{
		$qry = "Select e.rubric_data_id,e.eld_rubric_id,e.subject_id,r.tab as sub_group_name,e.sub_group_text from eld_rubrics e,rubric_data r where e.rubric_data_id=r.rubric_data_id and e.subject_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllsubgroupsgroup($point_id)
	{
		$qry = "Select r.tab as sub_group_name,e.eld_rubric_id,e.subject_id,e.rubric_data_id,e.sub_group_text from eld_rubrics e,rubric_data r where e.rubric_data_id=r.rubric_data_id and  e.subject_id=".$point_id;//." group by e.rubric_data_id" ;Commented by Hardik for ELD data issue on July 15, 2014
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getAllsubgroupsbydistrict()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select sd.grade,d.subject_name,e.eld_rubric_id,e.subject_id,r.tab as tab,e.rubric_data_id,e.sub_group_text from eld_rubrics e,rubric_data r,standarddata sd,standards s,dist_subjects d where e.rubric_data_id=r.rubric_data_id and e.subject_id=sd.standarddata_id and sd.standard_id=s.standard_id and s.subject_id=d.dist_subject_id and d.district_id=$district_id and d.eld=1 group by r.rubric_data_id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function geteldbydistrict()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select dist_subject_id as subject_id,subject_name from dist_subjects where district_id=$district_id and eld=1";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}

	function geteldRubricsbydistrict()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select rubric_data_id,tab as sub_group_name from rubric_data where district_id=$district_id";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	
	
	
	
	
	
	function update_eldrubric()
	{
	
		
		 $point_id=$this->input->post('subject_id');
		
	
				   $dqry = "delete from eld_rubrics where subject_id=$point_id";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('countergroup');
				   //exit;
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!=''  && $this->input->post('textboxup'.$i)!='' && $this->input->post('namebox'.$i)!='' )
					 {
					    $sub_group_id=$this->input->post('textboxup'.$i);
						
						$sdata=array('rubric_data_id'=>$this->input->post('textbox'.$i),
									'sub_group_text'=>$this->input->post('namebox'.$i),
						             'subject_id'=>$point_id,
									 'eld_rubric_id'=>$sub_group_id
						
						);
                   					 
					   //$where = "point_id=$point_id and sub_group_id=$sub_group_id";		
		
		
			                $str = $this->db->insert_string('eld_rubrics', $sdata);
					          $this->db->query($str);
					 
					 }
					  else if($this->input->post('textbox'.$i)!='' && $this->input->post('namebox'.$i)!='' )
					 {
					 $sdata=array('subject_id' =>$point_id,
					 'rubric_data_id'=>$this->input->post('textbox'.$i),
					  'sub_group_text'=>$this->input->post('namebox'.$i)				   	
					 );
					 
					 $sstr = $this->db->insert_string('eld_rubrics', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return true;
				
	
	
	
	}
	
	
	
	
	
	
	
}