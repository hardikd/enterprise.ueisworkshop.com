<?php
class Observermodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function is_valid_login($id=false){
		if($id==false)
		{
		$username = $this->input->post('username');
		$password = md5(trim($this->input->post('password')));
		$qry="SELECT ob.*,s.*,d.state_id,d.country_id,d.districts_name from observers ob,schools s,districts d  WHERE d.district_id=s.district_id and ob.school_id=s.school_id and ob.username='".$username."' AND ob.password='".$password."'   ";
		}
		else
		{
		$qry="SELECT ob.*,s.*,d.state_id,d.country_id,d.districts_name from observers ob,schools s,districts d  WHERE d.district_id=s.district_id and ob.school_id=s.school_id and ob.observer_id=$id  ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getobserverCount($state_id,$country_id,$district_id)
	{
		
		
		
		
		if($district_id=='all')
		{
		
		$qry="Select count(*) as count 
	  			from observers o,schools s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and o.school_id=s.school_id " ;
		}
		else
		{
	$qry="Select count(*) as count 
	  			from observers o,schools s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and o.school_id=s.school_id and s.district_id=$district_id " ;

		}	
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getobservers($page,$per_page,$state_id,$country_id,$district_id)
	{
		
		if($district_id=='all')
		{
			$qry="Select d.districts_name,b.observer_id as observer_id,b.observer_name as observer_name,s.school_name as school_name,s.school_id as school_id from observers b,schools s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and  b.school_id=s.school_id  " ;
		}
		
		else
		{
		
		$qry="Select d.districts_name,b.observer_id as observer_id,b.observer_name as observer_name,s.school_name as school_name,s.school_id as school_id from observers b,schools s, districts d where d.state_id=$state_id and d.country_id=$country_id and d.district_id=s.district_id and  b.school_id=s.school_id and s.district_id=$district_id " ;
		}
                
                if($page && $per_page){
		$page -= 1;
		$start = $page * $per_page;
                 $qry .= " limit $start, $per_page";
                }
		/*if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		$qry="Select b.observer_id as observer_id,b.observer_name as observer_name,s.school_name as school_name,s.school_id as school_id from observers b,schools s where b.school_id=s.school_id and s.district_id=$district_id  limit $start, $per_page" ;
		}
		else
		{
		$qry="Select b.observer_id as observer_id,b.observer_name as observer_name,s.school_name as school_name,s.school_id as school_id from observers b,schools s where b.school_id=s.school_id  limit $start, $per_page ";
		}*/
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getobserverById($observer_id)
	{
		
		
		$qry = "Select b.observer_id,b.observer_name,b.school_id,b.username,b.email,b.avatar,d.district_id,d.country_id,d.state_id from observers b,schools s,districts d where d.district_id=s.district_id and s.school_id=b.school_id and b.observer_id=".$observer_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getobserverByschoolId($school_id)
	{
		if($this->session->userdata('login_type')=='observer') {
			
			if($this->session->userdata("school_id")){
			$school_id = $this->session->userdata("school_id");
		} else {
			$school_id = $this->input->post('school_id');	
		}
		}else{
		$qry = "Select observer_id,observer_name,school_id from observers where school_id=".$school_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		}
	}
	function getobserverByTeacherId($teacher_id)
	{
		
		$qry = "Select o.observer_id,o.observer_name,s.school_id from observers o,teachers s where s.teacher_id=$teacher_id and o.school_id=s.school_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function check_observer_exists()
	{
	
		$name=$this->input->post('username');
		$qry="SELECT  observer_id from observers where username='$name'  ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	
	
	}
	function check_observer_update() {
	
	
		$name=$this->input->post('username');
		$id=$this->input->post('observer_id');
		$qry="SELECT observer_id from observers where username='$name'  and observer_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	
	}
	function add_observer()
	{
	  $data = array('observer_name' => $this->input->post('observer_name'),
	   'school_id' => $this->input->post('school_id'),
		'password'=> md5($this->input->post('password')),
		'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email'),
		'avatar'=>$this->input->post('avatar')
		
		);
	   try{
			$str = $this->db->insert_string('observers', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_observer()
	{
	
		$password=$this->input->post('password');
		if($password!='torvertex')
		{
		 
		 $password=md5($password);
		 
		$data = array('observer_name' => $this->input->post('observer_name'),
	               'school_id' => $this->input->post('school_id'),
				   'password'=>$password,
		           'username'=>$this->input->post('username'),
				   'email'=>$this->input->post('email'),
		'avatar'=>$this->input->post('avatar')	
		
		
		);
		
		
		
		
		}
		
		else
		{
		
			$data = array('observer_name' => $this->input->post('observer_name'),
	               'school_id' => $this->input->post('school_id'),
		           'username'=>$this->input->post('username'),
				   'email'=>$this->input->post('email'),
		'avatar'=>$this->input->post('avatar')	
		
		
		);
		
		
		}
		$observer_id=$this->input->post('observer_id');
	
		
			
		$where = "observer_id=".$observer_id;		
		
		try{
			$str = $this->db->update_string('observers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function changeObserverImage()
	
	{
	$data = array(	'avatar'=>$this->input->post('avatar')	
		
		
		);
		
		
		
		$observer_id=$this->session->userdata('observer_id');
	
		
			
		$where = "observer_id=".$observer_id;		
		
		try{
			$str = $this->db->update_string('observers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function changeDistImage()
	
	{
	$data = array(	'avatar'=>$this->input->post('avatar')	
		
		
		);
		
		
		
		$dist_user_id=$this->session->userdata('dist_user_id');
	
		
			
		$where = "dist_user_id=".$dist_user_id;		
		
		try{
			$str = $this->db->update_string('dist_users', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function changeTeacherImage()
	
	{
	$data = array(	'avatar'=>$this->input->post('avatar')	
		
		
		);
		
		
		
		$teacher_id=$this->session->userdata('teacher_id');
	
		
			
		$where = "teacher_id=".$teacher_id;		
		
		try{
			$str = $this->db->update_string('teachers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
		
	
	
	
	function deleteobserver($observer_id)
	{
		$qry = "delete from observers where observer_id=$observer_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
}