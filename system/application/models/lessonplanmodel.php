<?php
class Lessonplanmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getlessonplanscount()
	{
	
	$qry="Select count(*) as count from lesson_plans " ;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	
	function getlessonplans($page,$per_page)
	{
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
	
		$qry="Select tab,lesson_plan_id from lesson_plans  $limit ";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	function add_plan()
	{
	  $data = array('tab' => $this->input->post('tab')
					);
	   try{
			$str = $this->db->insert_string('lesson_plans', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_plan()
	{
	
		
		$plan_id=$this->input->post('plan_id');
		$data = array('tab' => $this->input->post('tab')
					);
			
			
		$where = "lesson_plan_id=".$plan_id;		
		
		try{
			$str = $this->db->update_string('lesson_plans', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteplan($plan_id)
	{
		$qry = "delete from lesson_plans where lesson_plan_id=$plan_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function getplanById($plan_id)
	{
	
		$qry = "Select lesson_plan_id,tab from lesson_plans where lesson_plan_id=".$plan_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getalllessonplans()
	{
	  $qry = "Select lesson_plan_id,tab from lesson_plans ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	function getalllessonplansnotsubject()
	{
	  $qry = "Select lesson_plan_id,tab from lesson_plans where lesson_plan_id!=1";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	}
	
	function getlessonplansbyteacher($teacher_id,$fromdate,$todate)
	{
	  if($teacher_id==false)
	  {
		$teacher_id=$this->session->userdata('teacher_id');
	  }	
	
	    $qry = "Select w.subject_id,ds.subject_name,s.subtab,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date,s.subtab from lesson_plans l,lesson_plan_sub s,lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and wd.lesson_plan_sub_id=s.lesson_plan_sub_id and l.lesson_plan_id=s.lesson_plan_id and w.date>='$fromdate' and w.date<='$todate' and w.subject_id=ds.dist_subject_id  order by w.starttime ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getlessonplansbysubjectandteacher($teacher_id,$fromdate,$todate,$subject_id)
	{
	 
//            print_r($this->input->post());exit;
	     $qry = "Select w.period_id,w.grade,w.subject_id,ds.subject_name,s.subtab,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date,s.subtab from lesson_plans l,lesson_plan_sub s,lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and wd.lesson_plan_sub_id=s.lesson_plan_sub_id and l.lesson_plan_id=s.lesson_plan_id and w.date='$todate' and w.subject_id=$subject_id and w.subject_id=ds.dist_subject_id  order by w.starttime ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getlessonplansbyteacherbytimetable($teacher_id,$fromdate,$todate)
	{
	  if($teacher_id==false)
	  {
              if($this->session->userdata('teacher_id'))
		$teacher_id=$this->session->userdata('teacher_id');
              else 
                  $teacher_id=$this->input->post('teacher_id');
	  }	
	
	    $qry = "Select w.teacher_id,w.subject_id,ds.subject_name,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date from lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id   and w.date>='$fromdate' and w.date<='$todate' and w.subject_id=ds.dist_subject_id order by w.starttime ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
        
        //added by hardik to get the lesson plan obaed on date & time entered
        function lessonplansbyteacherbytimetable($teacher_id,$fromdate,$todate,$starttime)
	{
	  
	  if($teacher_id==false)
	  {
              if($this->session->userdata('teacher_id'))
		$teacher_id=$this->session->userdata('teacher_id');
              else
                  $teacher_id=$this->input->post('teacher_id');
	  }
	
	    $qry = "Select w.teacher_id,w.subject_id,ds.subject_name,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date from lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id   and w.date>='$fromdate' and w.date<='$todate' and w.subject_id=ds.dist_subject_id  AND w.starttime='$starttime' order by w.starttime ; ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
        
	function check_lessonplan_exists($d,$start_time,$end_time)
	{
	
	
	if($teacher_id=$this->session->userdata('teacher_id'))
            $teacher_id=$this->session->userdata('teacher_id');
        else 
            $teacher_id=$this->input->post('teacher_id');
	  	
	
	   $qry = "Select subject_id from lesson_week_plans where date='$d' and teacher_id=$teacher_id and starttime='$start_time' and endtime='$end_time' ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}

        //Added by Hardik to get the lessong week plan
        function get_lesson_week_plan($d,$start_time)
	{
		$teacher_id=$this->session->userdata('teacher_id');
                $qry = "Select subject_id from lesson_week_plans where date='$d' and teacher_id=$teacher_id and starttime='$start_time'";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}
        
	function geteldplans($teacher_id,$fromdate,$todate)
	{


	   $qry = "Select w.standarddata_id as subject_id ,ds.subject_name,s.subtab,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,time_format(w.starttime,'%l:%i %p') as starttime,time_format(w.endtime,'%l:%i %p') as endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date,s.subtab from lesson_plans l,lesson_plan_sub s,lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and wd.lesson_plan_sub_id=s.lesson_plan_sub_id and l.lesson_plan_id=s.lesson_plan_id and w.date>='$fromdate' and w.date<='$todate' and w.subject_id=ds.dist_subject_id and ds.eld=1 order by w.starttime ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function geteldplansnew($teacher_id,$fromdate,$todate)
	{


	   $qry = "Select w.subject_id as subject_id ,ds.subject_name,s.subtab,w.standard,w.diff_instruction,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,time_format(w.starttime,'%l:%i %p') as starttime,time_format(w.endtime,'%l:%i %p') as endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date,s.subtab from lesson_plans l,lesson_plan_sub s,lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and wd.lesson_plan_sub_id=s.lesson_plan_sub_id and l.lesson_plan_id=s.lesson_plan_id and w.date>='$fromdate' and w.date<='$todate' and w.subject_id=ds.dist_subject_id and ds.eld=1 order by w.starttime ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getlessonplansbyteacherperiod_id($teacher_id,$fromdate)
	{
	  if($teacher_id==false)
	  {
		$teacher_id=$this->session->userdata('teacher_id');
	  }	
	  
	 
	    
	    $qry = "Select w.lesson_week_plan_id,w.subject_id,time_format(w.starttime,'%l:%i %p') as starttime,time_format(w.endtime,'%l:%i %p') as endtime from lesson_week_plans w,dist_subjects ds where w.teacher_id=$teacher_id and w.date>='$fromdate' and w.date<='$fromdate' and w.subject_id=ds.dist_subject_id   order by w.starttime ";
		

		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getlessonplansallbydate($fromdate,$todate)
	{
	  	
	
	   $qry = "Select w.teacher_id,t.firstname,t.email from lesson_plans l,lesson_plan_sub s,lesson_week_plans w,lesson_weel_plan_details wd,teachers t where w.teacher_id=t.teacher_id and  wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and wd.lesson_plan_sub_id=s.lesson_plan_sub_id and l.lesson_plan_id=s.lesson_plan_id and w.date>='$fromdate' and w.date<='$todate' group by w.teacher_id order by w.starttime ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getother($id)
	{
	  	
	
	   $qry = "Select w.standard,w.diff_instruction from lesson_week_plans w where w.lesson_week_plan_id=$id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function getlessonplansbyid($id)
	{
	  	
	
	   $qry = "Select w.starttime,w.endtime,w.lesson_week_plan_id,w.subject_id,s.subject_name from lesson_week_plans w,dist_subjects s where w.subject_id=s.dist_subject_id and w.lesson_week_plan_id=$id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	
	function getpendinglessonplansbyteacher($teacher_id,$fromdate,$todate)
	{
	
	if($teacher_id==false)
	  {
		$teacher_id=$this->session->userdata('teacher_id');
	  }	
	
	   $qry = "Select w.comments,w.lesson_week_plan_id,w.starttime,w.endtime,date_format(w.date,'%m-%d-%Y') as date from lesson_week_plans w,schedule_week_plans s where w.schedule_week_plan_id=s.schedule_week_plan_id and  w.teacher_id=$teacher_id  and w.date>='$fromdate' and w.date<='$todate' order by w.starttime ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	
	function getalllessonplanssub()
	{	
	
		$qry = "Select s.lesson_plan_id,s.lesson_plan_sub_id,s.subtab from lesson_plan_sub s ,lesson_plans l where l.lesson_plan_id=s.lesson_plan_id  ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	
	
	}
	function getalllessonplanssubnotsubject()
	{	
	
		$qry = "Select s.lesson_plan_id,s.lesson_plan_sub_id,s.subtab from lesson_plan_sub s ,lesson_plans l where l.lesson_plan_id=s.lesson_plan_id and l.lesson_plan_id!=1 ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}	
	
	
	}
	function add_plan_teacher()
	{
	  $n=date('Y-m-d h:i:s');
	   $dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		
          $start=$this->input->post('start');
			 
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 }
		 else
		 {
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	  
			$end=$this->input->post('end');
		
		
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 }
		 else
		 {
			$end3=$end2[0]+12;
		 }	
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
	  if($this->input->post('lesson_week_plan_id_pending'))
	  {
	    if($this->input->post('pendingstandard_id')!='')
		{
		 $standard=$this->input->post("pendingstandarddata");
		$data = array('status'=>'Completed',
						'standard'=>$standard,
						'diff_instruction'=>$this->input->post('pending_diff_instruction'),
						'grade'=>$this->input->post('pendinggrade'),
						'strand'=>$this->input->post('pendingstrand'),
						'standard_id'=>$this->input->post('pendingstandard_id'),
						'starttime' => $starttime,
						'endtime' => $endtime,	
						'period_id'=>$this->input->post('pendingperiod_id'),
						'subject_id'=>$this->input->post('pendingsubject_id'),
						'standarddata_id'=>$this->input->post('pendingstandarddata_id')
		);
		
		}
		else
		{
		$data = array('status'=>'Completed',
						'standard'=>$this->input->post('pending_standard'),
						'diff_instruction'=>$this->input->post('pending_diff_instruction'),
						'starttime' => $starttime,
						'endtime' => $endtime,
						'grade'=>$this->input->post('pendinggrade'),						
						'period_id'=>$this->input->post('pendingperiod_id'),
						'subject_id'=>$this->input->post('pendingsubject_id')
		);
		
		
		}
		
		
		$where = "lesson_week_plan_id=".$this->input->post('lesson_week_plan_id_pending');		
		}
		else
		{
		 
		 if($this->input->post('standard_id')!='')
		{
		$standard=$this->input->post("standarddata");
		$data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' => $this->session->userdata('teacher_id'),
                    'created'=>$n,					
		            'modified'=> $n	,
						'standard'=>$standard,
						'diff_instruction'=>$this->input->post('diff_instruction'),
						'grade'=>$this->input->post('grade'),
						'strand'=>$this->input->post('strand'),
						'standard_id'=>$this->input->post('standard_id'),
						'subject_id'=>$this->input->post('subject_id'),
						'period_id'=>$this->input->post('period_id'),
						'standarddata_id'=>$this->input->post('standarddata_id')
		
		);
		
		}
		else
		{
		 $data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' => $this->session->userdata('teacher_id'),
                    'created'=>$n,					
		            'modified'=> $n	,
						'standard'=>$this->input->post('standard'),
						'diff_instruction'=>$this->input->post('diff_instruction'),
						'grade'=>$this->input->post('grade'),
						'subject_id'=>$this->input->post('subject_id'),
						'period_id'=>$this->input->post('period_id')
		
		);
		}
		
		}
	   try{
			
			 if($this->input->post('lesson_week_plan_id_pending'))
			{
			$str = $this->db->update_string('lesson_week_plans', $data,$where);
			}
			else
			{
				$str = $this->db->insert_string('lesson_week_plans', $data);

			}		
			
			if($this->db->query($str))
			{
				if($this->input->post('lesson_week_plan_id_pending'))
				{
				 $last_id=$this->input->post('lesson_week_plan_id_pending');
				}
				else
				{
				 $last_id=$this->db->insert_id();
				
				}
				$lessonplan=$this->getalllessonplansnotsubject();
				foreach($lessonplan as $val)
				{
				 $lesson_plan_id=$val['lesson_plan_id'];
				 if($this->input->post('lesson_week_plan_id_pending'))
				{
				 $sdata=array('lesson_week_plan_id'=>$last_id,
							   'lesson_plan_id'=>$lesson_plan_id,
								'lesson_plan_sub_id'=>$this->input->post("pending".$lesson_plan_id),
								'created'=>$n,					
								'modified'=> $n			
				 
				 );
				 }
				 else
				 {
				 
				 $sdata=array('lesson_week_plan_id'=>$last_id,
							   'lesson_plan_id'=>$lesson_plan_id,
								'lesson_plan_sub_id'=>$this->input->post("$lesson_plan_id"),
								'created'=>$n,					
								'modified'=> $n			
				 
				 );
				 
				 
				 
				 
				 
				 }
				
				 $sstr = $this->db->insert_string('lesson_weel_plan_details', $sdata);
				 $this->db->query($sstr);
				 
				 
				
				}
				
				if($this->input->post('lesson_week_plan_id_pending'))
				{
					$customplan=$this->getallcustomplans();
				if($customplan!=false)
				{
				foreach($customplan as $val)
				{
				 $custom_id=$val['custom_differentiated_id'];
				 
				 
		
		$sdata=array( 'lesson_week_plan_id'=>$last_id,
		'custom_differentiated_id'=>$custom_id,
		'answer'=>$this->input->post("p_c_".$custom_id),
							  'created'=> $n			
				 
				 );
				
				$sstr = $this->db->insert_string('lesson_customs', $sdata);
				 $this->db->query($sstr);
			
		
				 
				
				
				}
				}
				
				}
				return 1;
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function add_planbytimetable($starttime,$endtime,$date,$grade_id,$subject_id,$period_id)
	{
		  $n=date('Y-m-d h:i:s');
		
		 if($this->session->userdata('teacher_id')){
                  $data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' => $this->session->userdata('teacher_id'),
                    'created'=>$n,					
		            'modified'=> $n	,						
					'grade'=>$grade_id,
					'subject_id'=>$subject_id,
					'period_id'=>$period_id
		
		);
                 } else {
                    $data = array('starttime' => $starttime,
					'endtime' => $endtime,	
					'date'=>$date,
					'teacher_id' => $this->input->post('teacher_id'),
                    'created'=>$n,					
		            'modified'=> $n	,						
					'grade'=>$grade_id,
					'subject_id'=>$subject_id,
					'period_id'=>$period_id
		
		); 
                 }
		
		
		
	   try{
			
			 
				$str = $this->db->insert_string('lesson_week_plans', $data);

					
			
			if($this->db->query($str))
			{
				
				 $last_id=$this->db->insert_id();
				
				
				$lessonplan=$this->getalllessonplansnotsubject();
				foreach($lessonplan as $val)
				{
				 $lesson_plan_id=$val['lesson_plan_id'];
				 
				 
				 $sdata=array('lesson_week_plan_id'=>$last_id,
							   'lesson_plan_id'=>$lesson_plan_id,								
								'created'=>$n,					
								'modified'=> $n			
				 
				 );
				 
				 
				 
				 
				 
				
				
				 $sstr = $this->db->insert_string('lesson_weel_plan_details', $sdata);
				 $this->db->query($sstr);
				
				}
				
				return 1;
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	function update_plan_teacher()
	{	
	   
	   $n=date('Y-m-d h:i:s');
	    $dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		$start=$this->input->post('start');
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 }
		 else
		 {
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	   $end=$this->input->post('end');
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 }
		 else
		 {
			$end3=$end2[0]+12;
		 }
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }	 
		
		
	   
	   $lesson_week_plan_id=$this->input->post('lesson_week_plan_id');
//		 echo $this->input->post('standard_id');exit;
		 if($this->input->post('standard_id')!='')
		{
		$standard=$this->input->post('standarddata');
		$data = array('starttime' => $starttime,
					'endtime' => $endtime,
					'date'=>$date,
		            'modified'=> $n	,
						'standard'=>$standard,
						'diff_instruction'=>$this->input->post('diff_instruction'),
						'grade'=>$this->input->post('grade'),
						'strand'=>$this->input->post('strand'),
						'standard_id'=>$this->input->post('standard_id'),
						'subject_id'=>$this->input->post('subject_id'),
						'period_id'=>$this->input->post('period_id'),
						'standarddata_id'=>$this->input->post('standarddata_id'),
                                                 'material_id'=>$this->input->post('selectedmaterial')
		);
		
		}
		else
		{
		 
		 $data = array('starttime' => $starttime,
					'endtime' => $endtime,
					'date'=>$date,
		            'modified'=> $n	,
						'standard'=>$this->input->post('standard'),
						'diff_instruction'=>$this->input->post('diff_instruction'),
						'grade'=>$this->input->post('grade'),
						'strand'=>'',
						'standard_id'=>null,
						'subject_id'=>$this->input->post('subject_id'),
						'period_id'=>$this->input->post('period_id'),
                                                'material_id'=>$this->input->post('selectedmaterial')
		
		
		);
			
		}	
		$where = "lesson_week_plan_id=".$lesson_week_plan_id;		
//		print_r($data);exit;
                
		try{
			$str = $this->db->update_string('lesson_week_plans', $data,$where);
			if($this->db->query($str))
			{
				$lessonplan=$this->getalllessonplansnotsubject();
				foreach($lessonplan as $val)
				{
				 $lesson_plan_id=$val['lesson_plan_id'];
				 $sdata=array('lesson_plan_sub_id'=>$this->input->post("$lesson_plan_id"),
							  'modified'=> $n			
				 
				 );
				 $swhere = "lesson_week_plan_id=$lesson_week_plan_id and lesson_plan_id=$lesson_plan_id";	
				$sstr = $this->db->update_string('lesson_weel_plan_details', $sdata,$swhere);
				 $this->db->query($sstr);
				
				}
				
				
				$customplan=$this->getallcustomplans();
				if($customplan!=false)
				{
				foreach($customplan as $val)
				{
				 $custom_id=$val['custom_differentiated_id'];
				 
				 $qry = "Select lesson_custom_id from lesson_customs where custom_differentiated_id=$custom_id and lesson_week_plan_id=$lesson_week_plan_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			 $sdata=array('answer'=>$this->input->post("c_".$custom_id)
				 
				 );
				 $swhere = "lesson_week_plan_id=$lesson_week_plan_id and custom_differentiated_id=$custom_id";	
				$sstr = $this->db->update_string('lesson_customs', $sdata,$swhere);
				 $this->db->query($sstr);
		}else{
		
		$sdata=array( 'lesson_week_plan_id'=>$lesson_week_plan_id,
		'custom_differentiated_id'=>$custom_id,
		'answer'=>$this->input->post("c_".$custom_id),
							  'created'=> $n			
				 
				 );
				
				$sstr = $this->db->insert_string('lesson_customs', $sdata);
				 $this->db->query($sstr);
			
		}
				 
				
				
				}
				}
				return 1;
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
		
	
	
	}
	function getallcustomplans()
	{
	 if($this->session->userdata("login_type")=='admin')
		{
		 $district_id=$this->session->userdata('district_value_tabs');
		}
		else
		{
	 
		$district_id=$this->session->userdata('district_id');
	  }
	  $qry = "Select custom_differentiated_id,tab from custom_differentiated where district_id=$district_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function delete_plan($lesson_week_plan_id)
	{
	
	 $sqry=" select date_format(date,'%m-%d-%Y') as date from lesson_week_plans where lesson_week_plan_id=$lesson_week_plan_id";
	 $squery = $this->db->query($sqry);
	 
	 $qry = "delete from lesson_week_plans where lesson_week_plan_id=$lesson_week_plan_id";
		$query = $this->db->query($qry);
	  $qry = "delete from lesson_weel_plan_details where lesson_week_plan_id=$lesson_week_plan_id";
		$query = $this->db->query($qry);
		
		if(mysql_error()==""){
			return $squery->result_array();
		}else{
			return false;
		}
	
	
	
	}
	
	function check_plan_pending()
	{
	     $teacher_id=$this->session->userdata('teacher_id');
		 $dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
	  
	  $sub=$this->input->post('pendingperiod_id');
		  $subjectquery=" SELECT  l.lesson_week_plan_id from lesson_week_plans l where   l.date='$date' and l.teacher_id=$teacher_id and l.period_id=$sub   "; 		 
		 $subjectquerye = $this->db->query($subjectquery);
		 if($subjectquerye->num_rows()>0){
		    return 3;
		 }
		  return 1;
	  }
	function check_plan_exists()
	{
	     $teacher_id=$this->session->userdata('teacher_id');
		 $dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		$start=$this->input->post('start');
	  $start1=explode(' ',$start);
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 
		 
		 }
		 else
		 {
		 
			$start3=$start2[0]+12;
		 }
		 
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	   $end=$this->input->post('end');
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 
		 }
		 else
		 {
		 
			$end3=$end2[0]+12;
		 }
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
         $sub=$this->input->post('period_id');
		  $subjectquery=" SELECT  l.lesson_week_plan_id from lesson_week_plans l,lesson_weel_plan_details ld where l.lesson_week_plan_id=ld.lesson_week_plan_id and  l.date='$date' and l.teacher_id=$teacher_id and l.period_id=$sub    "; 		 
		 $subjectquerye = $this->db->query($subjectquery);
		 if($subjectquerye->num_rows()>0){
		    return 3;
		 }
		  return 1;
	}
	
	function check_plan_update_exists()
	{
            
		if($this->session->userdata('teacher_id'))
		
			$teacher_id=$this->session->userdata('teacher_id');
		else 
			$teacher_id=$this->input->post('teacher_id');
	
	    $dates=$this->input->post('date');
	  $date1=explode('-',$dates);
	  $date=$date1[2].'-'.$date1[0].'-'.$date1[1];
		$start=$this->input->post('start');
	  $start1=explode(' ',$start);
//          print_r($start1);exit;
	  if($start1[1]=='pm')
	  {
	     $start2=explode(':',$start1[0]);
		 
		 if($start2[0]==12)
		 {
		    $start3=$start2[0];
		 
		 
		 }
		 else
		 {
		 
			$start3=$start2[0]+12;
		 }
		 $starttime=$start3.':'.$start2[1];
	  
	  }
	  else
	  {
	     $start2=explode(':',$start1[0]);
		 if($start2[0]==12)
		 {
		    $start3='00';
		   $starttime=$start3.':'.$start2[1];
		 
		 }
		 else
		 {
	    
			$starttime=$start1[0];
		 }	
	  
	  }
	   $end=$this->input->post('end');
	  $end1=explode(' ',$end);
	   if($end1[1]=='pm')
	  {
	     $end2=explode(':',$end1[0]);
		if($end2[0]==12)
		 {
		    $end3=$end2[0];
		 
		 
		 }
		 else
		 {
		 
			$end3=$end2[0]+12;
		 }
		 $endtime=$end3.':'.$end2[1];
	  
	  }
	  else
	  {
	     $end2=explode(':',$end1[0]);
		 if($end2[0]==12)
		 {
		    $end3='00';
		   $endtime=$end3.':'.$end2[1];
		 
		 }
		 else
		 {
	    
			$endtime=$end1[0];
		 }
	  
	  }
		
		  $sub=$this->input->post('period_id');
		  $lesson_week_plan_id=$this->input->post('lesson_week_plan_id');
		  $subjectquery=" SELECT  l.lesson_week_plan_id from lesson_week_plans l,lesson_weel_plan_details ld where l.lesson_week_plan_id=ld.lesson_week_plan_id and   l.lesson_week_plan_id!=$lesson_week_plan_id and l.date='$date' and l.teacher_id=$teacher_id and l.period_id=$sub   "; 		 
		 $subjectquerye = $this->db->query($subjectquery);
		 if($subjectquerye->num_rows()>0){
		    return 3;
		 }
		  return 1;
	}
	function getallteacherplans($date)
	{
	 $teacher_id=$this->session->userdata('teacher_id');
	
	   $qry = "Select ds.subject_name,w.comments,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date,s.subtab from lesson_plans l,lesson_plan_sub s,lesson_week_plans w,lesson_weel_plan_details wd,dist_subjects ds where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and wd.lesson_plan_sub_id=s.lesson_plan_sub_id and l.lesson_plan_id=s.lesson_plan_id and w.date='$date' and w.subject_id=ds.dist_subject_id order by w.starttime ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	
	
	}
	
	function getteacherplaninfo($lesson_week_plan_id)
	{
	
	  if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	
		}
	  
	  $qry = "Select w.standarddata_id,w.period_id,w.subject_id,w.grade,w.strand,w.standard_id,w.standard,w.diff_instruction,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date from lesson_plans l,lesson_week_plans w,lesson_weel_plan_details wd where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and  w.lesson_week_plan_id=$lesson_week_plan_id ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
	function getcommentssasignedcount($school_id,$teacher_id)
	{
	
	 if($teacher_id=='all')
		{
			$qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  " ;
		}
		else
		{
			 $qry="Select count(t.teacher_id) as count from schools s,teachers t where s.school_id=t.school_id  and  s.school_id=$school_id  and t.teacher_id=$teacher_id  " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	
	}
	
	function getcommentssasigned($page, $per_page,$school_id,$teacher_id)
	{
	     $page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		if($teacher_id=='all')
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id $limit " ;
		}
		else
		{
			$qry="Select s.school_name,t.firstname,t.lastname,t.teacher_id  from schools s,teachers t where s.school_id=t.school_id   and s.school_id=$school_id  and t.teacher_id=$teacher_id $limit " ;
		
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function addcomments($lesson_week_plan_id,$comments)
	{
		$n=date('Y-m-d h:i:s');	 
	 $data = array('comments' => $comments,
					'modified'=> $n	
		
		);
			
			
		$where = "lesson_week_plan_id=".$lesson_week_plan_id;		
		
		
			$str = $this->db->update_string('lesson_week_plans', $data,$where);
			
			if($this->db->query($str))
			{
				return true;
			}
			else
			{
              return false;

			}	
	
	
	
	}
	function addcommentsteacher($lesson_week_plan_id,$comments)
	{
		$n=date('Y-m-d h:i:s');	 
	 $data = array('teacher_comments' => $comments,
					'modified'=> $n	
		
		);
			
			
		$where = "lesson_week_plan_id=".$lesson_week_plan_id;		
		
		
			$str = $this->db->update_string('lesson_week_plans', $data,$where);
			
			if($this->db->query($str))
			{
				return true;
			}
			else
			{
              return false;

			}	
	
	
	
	}
	
	function getcomments($id)
	{
	
	 $qry="select comments,teacher_comments from lesson_week_plans where lesson_week_plan_id=$id";
	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getcommentsbyid($id)
	{
	
	 $qry="select schedule_week_plan_id,comments,teacher_comments from lesson_week_plans where lesson_week_plan_id=$id";
	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function updateRow($table,$data,$where=null)
	{
		$sql = "UPDATE `$table` SET";
		
		$ids=explode('_',$data['id']);
		$i=0;
		foreach($data as $key => $value)
		{
			if($key!='id' && $key!='oper')
			{
				$value_id=$ids[$i];
				$sql.=" task='".mysql_escape_string($value)."'";
				$sql .= " WHERE value_id=$value_id";
				
				mysql_query($sql);				
				if(mysql_error())
				{
					return mysql_error()."<br>$sql";
				}
				$sql = "UPDATE `$table` SET";
				$i++;
				
			}	
		}
		
 
		
		return '';
	}
	function updateblRow($table,$value,$id)
	{
		if($value==0)
		{
		$value='';
		
		}
		$sql = "UPDATE `$table` SET";
		
		
				$sql.=" task='".mysql_escape_string($value)."'";
				if($table=='value_feature')
				{
				$sql .= " WHERE value_id=$id";
				}
				if($table=='homework_feature')
				{
				$sql .= " WHERE homework_feature_id=$id";
				}
				if($table=='eld_feature')
				{
				$sql .= " WHERE eld_feature_id=$id";
				}
				mysql_query($sql);				
				
		
 
		
		return $value;
	}
	function updateHomeworkRow($table,$data,$where=null)
	{
		$sql = "UPDATE `$table` SET";
		
		$ids=explode('_',$data['id']);
		$i=0;
		foreach($data as $key => $value)
		{
			if($key!='id' && $key!='oper')
			{
				$value_id=$ids[$i];
				$sql.=" task='".mysql_escape_string($value)."'";
				$sql .= " WHERE homework_feature_id=$value_id";
				
				mysql_query($sql);				
				if(mysql_error())
				{
					return mysql_error()."<br>$sql";
				}
				$sql = "UPDATE `$table` SET";
				$i++;
				
			}	
		}
		
 
		
		return '';
	}
	function updateeldRow($table,$data,$where=null)
	{
		$sql = "UPDATE `$table` SET";
		
		$ids=explode('_',$data['id']);
		$i=0;
		foreach($data as $key => $value)
		{
			if($key!='id' && $key!='oper')
			{
				$value_id=$ids[$i];
				$sql.=" task='".mysql_escape_string($value)."'";
				$sql .= " WHERE eld_feature_id=$value_id";
				
				mysql_query($sql);				
				if(mysql_error())
				{
					return mysql_error()."<br>$sql";
				}
				$sql = "UPDATE `$table` SET";
				$i++;
				
			}	
		}
		
 
		
		return '';
	}
	
	function value_feature($date=false)
	{
		 if($this->session->userdata('login_type')=='teacher')
		{
			$teacher_id=$this->session->userdata('teacher_id');
		}
		else 
		{
			$teacher_id=$this->session->userdata('observer_feature_id');
			
	
		}	  
		if($date==false)
		{
		
		$current_date=date('Y-m-d');
		
		}
		else
		{
		
		$current_date=$date;
		}
		//$sql="select v.* from value_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id and p.teacher_student_id=v.student_id and v.current_date='$current_date'";
		$sql="select v.* from value_feature v where  v.current_date='$current_date' group by v.lesson_week_plan_id";
		
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			 $row=$query->result_array();
			 foreach($row as $value)
			 {
			    $re[$value['lesson_week_plan_id']]=1;
			 
			 }
			return $re;	
		}else{
			return false;
		}
	
	
	}
function current_value_feature($lesson_week_plan_id)
	{
		 if($this->session->userdata('login_type')=='teacher')
		{
			$teacher_id=$this->session->userdata('teacher_id');
		}
		else 
		{
			$teacher_id=$this->session->userdata('observer_feature_id');
			
	
		}	  
		
		$sql="select v.* from value_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id and p.teacher_student_id=v.student_id and v.lesson_week_plan_id=$lesson_week_plan_id";
		
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	}

	public function check_enter_grade($lesson_week_plan_id,$report_date) 
	{
			$this->db->where('lesson_week_plan_id',$lesson_week_plan_id);
			$this->db->where('current_date',$report_date);
			$this->db->from('value_feature');
			$query = $this->db->get();
			return $query->num_rows;
			
	}
	
	
	function current_home_work($subject_id,$teacher_id)
	{
		 
		$current_date=date('Y-m-d');
		$sql="select v.* from homework_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id  and p.teacher_student_id=v.student_id and v.current_date='$current_date' and v.lesson_plan_sub_id=$subject_id ";
		
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function current_eld($subject_id,$teacher_id)
	{
		 
		$current_date=date('Y-m-d');
		$sql="select v.* from eld_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id  and p.teacher_student_id=v.student_id and v.current_date='$current_date' and v.lesson_plan_sub_id=$subject_id ";
		
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function value_homework()
	{
		 if($this->session->userdata('login_type')=='teacher')
		{
			$teacher_id=$this->session->userdata('teacher_id');
		}
		else 
		{
			$teacher_id=$this->session->userdata('observer_feature_id');
			
	
		}	  
		$current_date=date('Y-m-d');
		$sql="select v.* from homework_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id and p.teacher_student_id=v.student_id and v.current_date='$current_date'";
		
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function check_value_feature($lesson_week_plan_id,$student_id)
	{
	   if($this->session->userdata('login_type')=='teacher')
		{
			$teacher_id=$this->session->userdata('teacher_id');
		}
		else 
		{
			$teacher_id=$this->session->userdata('observer_feature_id');
	
		}
		 $current_date=date('Y-m-d');
		$sql="select v.* from value_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id and p.teacher_student_id=v.student_id and   v.current_date='$current_date' and v.lesson_week_plan_id=$lesson_week_plan_id and v.student_id=$student_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function check_value_homework($lesson_plan_sub_id,$student_id)
	{
	   if($this->session->userdata('login_type')=='teacher')
		{
			$teacher_id=$this->session->userdata('teacher_id');
		}
		else 
		{
			$teacher_id=$this->session->userdata('observer_homework_feature_id');
	
		}
                if($this->input->post('report_date')) {
            $ndays = date("t", strtotime($this->input->post('report_date')));
            $reportdateArr = explode('-', $this->input->post('report_date'));
            $current_date = $reportdateArr[2] . '-' . $reportdateArr[0] . '-' . $reportdateArr[1];
                } else {
		 $current_date=date('Y-m-d');
                }
		$sql="select v.* from homework_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id and p.student_id=v.student_id and v.current_date='$current_date' and v.lesson_plan_sub_id=$lesson_plan_sub_id and v.student_id=$student_id group by v.homework_feature_id";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function check_value_eld($lesson_plan_sub_id,$student_id)
	{
	   
			$teacher_id=$this->session->userdata('teacher_id');
		 $current_date=date('Y-m-d');
		$sql="select v.* from eld_feature v ,students s,teacher_students p where p.student_id=s.student_id and p.teacher_id=$teacher_id and p.teacher_student_id=v.student_id and v.current_date='$current_date' and v.lesson_plan_sub_id=$lesson_plan_sub_id and v.student_id=$student_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	
	}
	function value_feature_all()
	{
		 $teacher_id=$this->session->userdata('teacher_id');
		$sql="select  group_concat(v.value_id) from value_feature v ,students s,parents p where p.parents_id=s.parents_id and p.teacher_id=$teacher_id and s.student_id=v.student_id group by v.lesson_week_plan_id,v.student_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function add_value_feature($lesson_week_plan_id,$student_id,$time_interval,$interval,$sub_id)
	{
	  $current_date=date('Y-m-d');
	  $data = array('lesson_week_plan_id' => $lesson_week_plan_id,
	  'student_id' => $student_id,
	  'time_interval' => "$time_interval",
	  'current_date' => "$current_date",
	  'interval' => "$interval",
	  'lesson_plan_sub_id'=>$sub_id
					);
	   try{
			$str = $this->db->insert_string('value_feature', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	
	function remove_value_feature($lesson_week_plan_id,$student_id,$sub_id)
	{
	  $current_date=date('Y-m-d');
		$where = array(
			'lesson_week_plan_id' => $lesson_week_plan_id,
	  		'student_id' => $student_id,
			'current_date' => "$current_date",
	  		'lesson_plan_sub_id'=>$sub_id
		);
	   try{
		   	$this->db->where($where);
			$this->db->delete('value_feature');
			
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	
	
	
	function add_value_homework($lesson_plan_sub_id,$student_id)
	{
            
//	  $lesson_week_plan_id=$this->session->userdata('current_plan_id');
        if ($this->session->userdata('current_plan_id')) {
            $lesson_week_plan_id = $this->session->userdata('current_plan_id');
        } else {
            $lesson_week_plan_id = $this->input->post('lesson_week_plan_id');
        }
	  
	  
          if($this->input->post('report_date')) {
              $reportdateArr = explode('-', $this->input->post('report_date'));
                $current_date = $reportdateArr[2] . '-' . $reportdateArr[0] . '-' . $reportdateArr[1];
          } else {
              $current_date=date('Y-m-d');
          }
//          print_r($current_date);exit;
	  $data = array(
	  'student_id' => $student_id,
	 
	  'current_date' => "$current_date",
	 
	  'lesson_plan_sub_id'=>$lesson_plan_sub_id,
	  'lesson_week_plan_id'=>$lesson_week_plan_id
					);
	   try{
			$str = $this->db->insert_string('homework_feature', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function add_value_eld($lesson_plan_sub_id,$student_id)
	{
	  $current_date=date('Y-m-d');
	  $data = array(
	  'student_id' => $student_id,
	 
	  'current_date' => "$current_date",
	 
	  'lesson_plan_sub_id'=>$lesson_plan_sub_id
					);
	   try{
			$str = $this->db->insert_string('eld_feature', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function getstudentdata($student_id)
	{
		 
		$d=date('Y-m-d');
		$sql="select lw.standard,v.lesson_plan_sub_id,v.current_date,l.subject_name as subtab,group_concat(If(vp.tab IS NULL ,'N.A.',vp.tab) ORDER BY v.value_id ) as task,group_concat(v.time_interval ORDER BY v.value_id) as time_interval,concat(s.firstname,' ',s.lastname)as studentname from dist_subjects l,students s,schools sh,teacher_students p,lesson_week_plans lw,value_feature v left join value_plans vp on v.task=vp.value_plan_id  where v.lesson_week_plan_id=lw.lesson_week_plan_id and s.school_id=sh.school_id and sh.district_id=l.district_id and v.current_date='$d' and v.student_id=$student_id and v.time_interval!='Classwork' and v.lesson_plan_sub_id=l.dist_subject_id and  v.student_id=p.teacher_student_id and p.student_id=s.student_id group by v.lesson_week_plan_id; ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getstudentclassworkdata($student_id)
	{
		 
		$d=date('Y-m-d');
		$sql="select  c.tab,v.lesson_plan_sub_id from value_feature v,classwork_proficiency c where  v.time_interval='Classwork' and v.current_date='$d' and v.student_id=$student_id and v.task=c.classwork_proficiency_id";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getstudenthomeworkdata($student_id)
	{
		 
		$d=date('Y-m-d');
		$sql="select  lw.standard,s.firstname,s.lastname,l.subject_name as subtab,hp.tab,h.lesson_plan_sub_id from homework_feature h,homework_proficiency hp,dist_subjects l,teacher_students ts,students s,schools sh,lesson_week_plans lw where h.lesson_week_plan_id=lw.lesson_week_plan_id and s.school_id=sh.school_id and sh.district_id=l.district_id and h.current_date='$d' and h.student_id=$student_id and h.task=hp.homework_proficiency_id and l.dist_subject_id=h.lesson_plan_sub_id and ts.teacher_student_id=h.student_id and ts.student_id=s.student_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getstudenthomeworkdataall($student_id)
	{
		 
		$d=date('Y-m-d');
		$sql="select  s.firstname,s.lastname,l.subtab,hp.tab,h.lesson_plan_sub_id from homework_feature h,homework_proficiency hp,lesson_plan_sub l,teacher_students ts,students s where h.current_date='$d' and h.student_id=$student_id and h.task=hp.homework_proficiency_id and l.lesson_plan_sub_id=h.lesson_plan_sub_id and ts.teacher_student_id=h.student_id and ts.student_id=s.student_id ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getmonthdata($month,$year,$student_id)
	{
		 if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
			
	 if($month=='all')
	  {
		$sql="select v.interval,vp.tab,date_format(v.current_date,'%m/%d/%Y') as dates,time_format(v.time_interval,'%l:%i %p') as time_interval from teacher_students ts, value_feature v left join value_plans vp on v.task=vp.value_plan_id  where  v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id and year(v.current_date)=$year  and v.time_interval!='Classwork' ;";
	  }
	  else
	  {
	  $sql="select v.interval,vp.tab,date_format(v.current_date,'%m/%d/%Y') as dates,time_format(v.time_interval,'%l:%i %p') as time_interval from teacher_students ts, value_feature v left join value_plans vp on v.task=vp.value_plan_id  where v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id and month(v.current_date)=$month and year(v.current_date)=$year  and v.time_interval!='Classwork' ;";
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
function gethomeworkmonthdata($month,$year,$student_id)
	{
	  if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
	 if($month=='all')
	  {
		$sql="select ts.teacher_id,v.lesson_plan_sub_id,vp.tab,date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts,homework_feature v left join homework_proficiency vp on v.task=vp.homework_proficiency_id  where  v.student_id = ts.student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and year(v.current_date)=$year GROUP BY v.lesson_plan_sub_id,`dates`";// group by v.current_date;";
                
	  }
	  else
	  {
	  $sql="select ts.teacher_id,v.lesson_plan_sub_id,vp.tab,date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts, homework_feature v left join homework_proficiency vp on v.task=vp.homework_proficiency_id  where v.student_id = ts.student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and month(v.current_date)=$month and year(v.current_date)=$year GROUP BY v.lesson_plan_sub_id,`dates`";// group by v.current_date;";
	  }
//          echo $sql;exit;
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
	function geteldmonthdata($month,$year,$student_id,$teacher_id='')
	{
	    if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$teacher_id;
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
	  if($month=='all')
	  {
		$sql="select v.lesson_plan_sub_id,vp.tab as tab,date_format(v.current_date,'%m/%d/%Y') as dates,ts.teacher_id from teacher_students ts,eld_feature v left join rubric_data vp on v.task=vp.rubric_data_id  where  v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and year(v.current_date)=$year  ;";
	  }
	  else
	  {
	  $sql="select v.lesson_plan_sub_id,vp.tab as tab ,date_format(v.current_date,'%m/%d/%Y') as dates, ts.teacher_id from teacher_students ts, eld_feature v left join rubric_data vp on v.task=vp.rubric_data_id  where v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and month(v.current_date)=$month and year(v.current_date)=$year  ;";
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getclassmonthdata($month,$year,$student_id)
	{
	   if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
			
	  if($month=='all')
	  {
		$sql="select v.lesson_plan_sub_id,group_concat(vp.tab) as tab ,date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts,value_feature v left join classwork_proficiency vp on v.task=vp.classwork_proficiency_id  where  v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and v.time_interval='Classwork' and  year(v.current_date)=$year  group by v.current_date,v.lesson_plan_sub_id;";
	  }
	  else
	  {
	  $sql="select v.lesson_plan_sub_id,group_concat(vp.tab) as tab,date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts,value_feature v left join classwork_proficiency vp on v.task=vp.classwork_proficiency_id  where v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and  v.time_interval='Classwork' and  month(v.current_date)=$month and year(v.current_date)=$year  group by v.current_date,v.lesson_plan_sub_id;";
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getweekdata($month,$year,$student_id,$ndays)
	{
	  if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
			
			
	  if($month=='all')
	  {
		$fromdate=$year.'-01-01';
		$todate=$year.'-01-'.$ndays;
		$sql="select vp.tab,count(vp.tab) as count ,date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts,value_feature v left join value_plans vp on v.task=vp.value_plan_id  where v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and vp.tab!='' and  v.current_date>='$fromdate' and v.current_date<='$todate'   and v.time_interval!='Classwork' group by v.current_date,v.task order by vp.value_plan_id;";
	  }
	  else
	  {
	  $fromdate=$year.'-'.$month.'-01';
	  $todate=$year.'-'.$month.'-'.$ndays;
	  $sql="select vp.tab,count(vp.tab) as count, date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts,value_feature v left join value_plans vp on v.task=vp.value_plan_id  where v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and vp.tab!='' and v.current_date>='$fromdate' and v.current_date<='$todate' and v.time_interval!='Classwork' group by v.current_date,v.task order by vp.value_plan_id;";
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function geteldweekdata($month,$year,$student_id,$ndays)
	{
	 if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
	  if($month=='all')
	  {
		$fromdate=$year.'-01-01';
		$todate=$year.'-01-'.$ndays;
		$sql="select rd.tab as tab,count(rd.tab) as count ,date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts, eld_feature v left join rubric_data rd  on v.task=rd.rubric_data_id  where  v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and rd.rubric_data_id!='' and  v.current_date>='$fromdate' and v.current_date<='$todate'    group by v.current_date,v.task order by  rd.rubric_data_id;";
	  }
	  else
	  {
	  $fromdate=$year.'-'.$month.'-01';
	  $todate=$year.'-'.$month.'-'.$ndays;
	  $sql="select rd.tab as tab,count(rd.tab) as count, date_format(v.current_date,'%m/%d/%Y') as dates from teacher_students ts, eld_feature v left join rubric_data rd on v.task=rd.rubric_data_id  where   v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and rd.rubric_data_id!='' and v.current_date>='$fromdate' and v.current_date<='$todate'   group by v.current_date,v.task order by rd.rubric_data_id;";
	  
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getclassweekdata($month,$year,$student_id,$ndays)
	{
	   if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
			
	  if($month=='all')
	  {
		$fromdate=$year.'-01-01';
		$todate=$year.'-01-'.$ndays;
		$sql="select vp.tab,count(vp.tab) as count ,date_format(v.current_date,'%m/%d/%Y') as dates from time_table t,dist_subjects d,teacher_students ts, value_feature v left join classwork_proficiency vp on v.task=vp.classwork_proficiency_id  where   v.student_id = ts.teacher_student_id and t.class_room_id=ts.class_room_id and t.period_id=ts.period_id and t.current_date=ts.day and t.teacher_grade_subject_id=ts.teacher_id and t.subject_id=d.dist_subject_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and vp.tab!='' and v.time_interval='Classwork' and  v.current_date>='$fromdate' and v.current_date<='$todate'  group by v.current_date,v.task order by vp.classwork_proficiency_id;";
	  }
	  else
	  {
	  $fromdate=$year.'-'.$month.'-01';
	  $todate=$year.'-'.$month.'-'.$ndays;
	  $sql="select vp.tab,count(vp.tab) as count, date_format(v.current_date,'%m/%d/%Y') as dates from time_table t,dist_subjects d,teacher_students ts, value_feature v left join classwork_proficiency vp on v.task=vp.classwork_proficiency_id  where  v.student_id = ts.teacher_student_id and t.class_room_id=ts.class_room_id and t.period_id=ts.period_id and t.current_date=ts.day and t.teacher_grade_subject_id=ts.teacher_id and t.subject_id=d.dist_subject_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and vp.tab!='' and v.time_interval='Classwork' and v.current_date>='$fromdate' and v.current_date<='$todate'  group by v.current_date,v.task order by vp.classwork_proficiency_id;";
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getmonthdatacol($month,$year,$student_id)
	{
	   if($this->session->userdata('login_type')!='teacher')
			{
			
			$teacher_id=$this->session->userdata('observer_feature_id');
			}
			else
			{
			
			$teacher_id=$this->session->userdata('teacher_id');
			
			}
			
	  if($month=='all')
	  {
		$sql="select time_format(v.time_interval,'%l:%i %p') as time_interval from teacher_students ts,value_feature v   where  v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and year(v.current_date)=$year  and v.time_interval!='Classwork' group by v.time_interval order by time_interval  ;";
	  }
	  else
	  {
	  $sql="select time_format(v.time_interval,'%l:%i %p') as time_interval from teacher_students ts,value_feature v   where v.student_id = ts.teacher_student_id AND ts.student_id =$student_id AND ts.teacher_id =$teacher_id  and  month(v.current_date)=$month and year(v.current_date)=$year  and v.time_interval!='Classwork' group by v.time_interval order by time_interval  ;";
	  }
	  $query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getTeacherLessonPlanReport($teacher_id,$year)
	{
	  
	
	  $qry = "select count(lesson_week_plan_id) as count ,MONTH(date) as month  from lesson_week_plans where teacher_id=$teacher_id and year(date)='$year' AND standard != '' group by YEAR(date), MONTH(date)  ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getTeacherHomeworkPlanReport($teacher_id,$year)
	{
	  
	
	  $qry = "select count(homework_id) as count ,MONTH(entered) as month  from homework where teacher_id=$teacher_id and year(entered)='$year' group by YEAR(entered), MONTH(entered)  ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getTeacherBehaviorPlanReport($teacher_id,$year)
	{
	  
	
	  $qry = "select count(tab.counts) as count,month(tab.cdate) as month from ( select if(count(v.value_id)!=0,1,0) as counts ,v.current_date as cdate  from value_feature v left join teacher_students s on v.student_id=s.teacher_student_id  left join teachers p on s.teacher_id=p.teacher_id where p.teacher_id=$teacher_id and year(v.current_date)='$year' group by v.current_date) as tab group by month(tab.cdate) ";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getstandard($date,$teacher_id,$subject_id)
	{
	  
	
	  $qry = "select standard,diff_instruction from lesson_week_plans where teacher_id=$teacher_id and date='$date' and subject_id=$subject_id ";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getsubtab($ls)
	{
		$qry = "select subtab from lesson_plan_sub where lesson_plan_sub_id=$ls";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->subtab;			
		}else{
			return FALSE;
		}
	}

        public function hw_proficiency_by_district(){
            if($this->session->userdata('district_id')){
                $district_id = $this->session->userdata('district_id');
            }
            $query = $this->db->get_where('homework_proficiency',array('district_id'=>$district_id));
            return $query->result();
        }
        
        public function update_homework(){
            $where = array('homework_feature_id'=>$this->input->post('lesson_homework_id'));
            $query = $this->db->get_where('homework_feature',$where);
            $ishomework = $query->result();
            if(count($ishomework) > 0){
            
            $data = array(
                            'task'=>$this->input->post('task'),
                            'assignment_type'=>$this->input->post('assignment_type'),
                            'assignment_name'=>$this->input->post('assignment_name')
                        );
            $this->db->where($where);
            $update = $this->db->update('homework_feature',$data);
            } else {
                $data = array(
                            'task'=>$this->input->post('task'),
                            'assignment_type'=>$this->input->post('assignment_type'),
                            'assignment_name'=>$this->input->post('assignment_name')
                        );
                $update = $this->db->insert('homework_feature',$data);
            }
            if($update)
                return true;
            else
                return false;
         
        }


function attendance_manager_data($strDateFrom, $strDateTo, $student_id)
	{
	$this->db->select('*');
	$this->db->from('student_roll');
    $this->db->join('students','student_roll.student_id = students.student_id','LEFT');
	//$this->db->join('teachers','student_roll.teacher_id = teachers.teacher_id','LEFT');
	$this->db->join('teacher_students','student_roll.student_id = teacher_students.teacher_student_id','LEFT');
	
	$this->db->where('student_roll.student_id',$student_id);
	$this->db->where('student_roll.date >= ',$strDateFrom);
    $this->db->where('student_roll.date <= ',$strDateTo);
	$query = $this->db->get();
	 //echo $this->db->last_query();exit;
        $result = $query->result();
	return $result;	
}

function check_value_feature_duplicate($date, $lesson_plan_id){
	$datearr = explode('-',$date);
	$date = $datearr[2].'-'.$datearr[0].'-'.$datearr[1];
	$where = array(
		'current_date'=>$date, 'lesson_week_plan_id'=>$lesson_plan_id
	);
	
	$query = $this->db->get_where('value_feature',$where);

	if($query->num_rows() > 0)
		return true;
	else 
		return false;
	}

function getteacherplanmaterial($lesson_week_plan_id)
	{
	
	  if($this->session->userdata("teacher_id")){
			$teacher_id = $this->session->userdata("teacher_id");
		} else {
			$teacher_id = $this->input->post('teacher_id');	
		}
	  
	   $qry = "Select w.standarddata_id,w.period_id,w.subject_id,w.grade,w.strand,w.standard_id,w.standard,w.diff_instruction,w.lesson_week_plan_id,wd.lesson_plan_id,w.starttime,w.endtime,wd.lesson_plan_sub_id,date_format(w.date,'%m-%d-%Y') as date, w.material_id,lpm.name,lpm.file_path from lesson_plan_material lpm, lesson_plans l,lesson_week_plans w,lesson_weel_plan_details wd where w.teacher_id=$teacher_id and wd.lesson_week_plan_id=w.lesson_week_plan_id and wd.lesson_plan_id=l.lesson_plan_id and  w.lesson_week_plan_id=$lesson_week_plan_id and lpm.lesson_plan_material_id = w.material_id group by lpm.lesson_plan_material_id";
//          exit;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
       /* function attendance_manager_data($strDateFrom, $strDateTo, $student_id){
           //$query = $this->db->get_where('student_roll',array('student_id'=>$student_id,'date'=>$date));
		   $query = $this->db->query("SELECT * FROM student_roll WHERE student_id = $student_id and date BETWEEN '$strDateFrom' AND '$strDateTo'");
		   echo $this->db->last_query();exit;
           $result = $query->result();
           if($result){
               return $result[0]->status;
           } else {
               return false;
           }
        }
  
*/
}