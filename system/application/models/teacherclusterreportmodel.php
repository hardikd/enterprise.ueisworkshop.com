<?php
class Teacherclusterreportmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
 
 
  function getallcategoryAndAssessment($district_id,$teacher_id)
   {
	   $qry='select * from cats where district_id ='.$district_id;
	   $query = $this->db->query($qry);
	   $array = array();
		if($query->num_rows()>0){
		  $array['cat'] = $query->result_array();
		}
		
		$qry='select Distinct a.id,a.assignment_name from assignments a, user_quizzes uq where a.id=uq.assignment_id and uq.user_id in (select Distinct u.UserID from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= '.$teacher_id.') order by a.id DESC';
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		  $array['Assessments'] = $query->result_array();
		}
		return $array;	 
   }
   
     function getteacherDistrictId($teacher_id)
   {
	   $qry='select s.*,t.teacher_id from schools s left join teachers t on s.school_id = t.school_id where t.teacher_id= '.$teacher_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
    function getteacherschool($teacher_id)
   {
	   $qry='select s.*,t.teacher_id from schools s left join teachers t on s.school_id = t.school_id where t.teacher_id= '.$teacher_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
  
 /* function getTestItem($school_id,$district_id,$ass_id)
    {
		if($ass_id == 0)
		$andAss = '';
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;

		if($school_id == 0)
		{
			$and = "";
		}
		else
		{
			$and = " and u.school_id=".$school_id ;
		}
		//and district_id=".$district_id." 
  		$qry= "SELECT test_cluster FROM `questions` where id in(select question_id from user_answers ua,user_quizzes uq, users u where ua.user_quiz_id=uq.id and uq.user_id = u.UserId ".$andAss."".$and.") and test_cluster!='' and district_id=".$district_id." group by test_cluster"; 
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 	
	 }
	*/
	function getTestItem($teacher_id,$ass_id)
    {
		if($ass_id == 0)
		$andAss = '';
		else
		$andAss  = " AND uq.assignment_id=".$ass_id;
		$qry= "SELECT q.test_cluster FROM `questions` q,user_answers ua,user_quizzes uq, users u ,teacher_students t where q.id = ua.question_id and ua.user_quiz_id=uq.id and uq.user_id = u.UserID and u.student_id = t.student_id and t.teacher_id=".$teacher_id."".$andAss." and q.test_cluster!='' group by q.test_cluster order by q.id";
		
		 	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		} 	
	 }
   
   function getteacherstudent($teacher_id)
   {
	   $qry='select Distinct u.* from users  u, teacher_students t  where u.student_id = t.student_id and t.teacher_id= '.$teacher_id.' order by Name,Surname ';
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
    function getteachergrade($teacher_id)
   {
	   $qry=' SELECT * FROM  dist_grades dg, grade_subjects gs ,teacher_grade_subjects tgs where dg.dist_grade_id = gs.grade_id and gs.grade_subject_id = tgs.grade_subject_id and tgs.teacher_id= '.$teacher_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   function getteachername($teacher_id)
   {
	   $qry='SELECT * FROM  teachers where teacher_id= '.$teacher_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   function getassessmenttime($ass_id)
   {
	   $qry='SELECT * FROM  assignments where id= '.$ass_id;
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   /*
  function getPercentageByAllStudent($user_id,$category_id)
    {
	
		if($category_id == 0)
		{
			$and = "";
		}
		else
		{
			$and = " and a.quiz_id in (select id from quizzes where cat_id=".$category_id.")";
		}
		
	 	$qry_Primary= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_score_ans=0 group by question_id"; 
		$query = $this->db->query($qry_Primary);
  		 $sum1 = $query->num_rows(); 
		
		
		$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_score_ans=1 group by question_id"; 
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows();
		
		$ToalQes = $sum1 + $sum2;
		
		$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_correct_ans=1 and is_score_ans=0 group by question_id"; 		
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
		 
		 $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id." and uq.assignment_id=a.id ".$and." and is_correct_ans=1 and is_score_ans=1 group by question_id";  
		$query = $this->db->query($qry_Sec_correct);
		 $c_sum2 = $query->num_rows();
		
		$Curr_Qes = $c_sum1 + $c_sum2;
		if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =0;
		 	$per = number_format($per,2,'.','');
 		return $per;
	 }
	*/
   function getPercentageByStudentAndCluster($user_id,$category_id,$test_cluster,$district_id,$fDate,$tDate,$ass_id)
    {
		if($ass_id == 0)
			$andAss = '';
		else
			$andAss  = " AND a.id=".$ass_id;
		$andDate = " AND DATE(ua.added_date) between '".$fDate."' ANd '".$tDate."' ";
		if($category_id == 0)
		{
			$and = "";
		}
		else
		{
			$and = " and a.quiz_id in (select id from quizzes where cat_id=".$category_id.")";
		}
		$group_by = "user_id,question_id ";
		$group_by_radio = "user_id,ua.id";
		
		
 	$qry_Primary= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and." ".$andAss." and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.")  and is_score_ans=0".$andDate." group by ".$group_by;
		$query = $this->db->query($qry_Primary);
  		 $sum1 = $query->num_rows(); 
		
		
		$qry_Sec= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and." ".$andAss."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.")  and is_score_ans=1".$andDate." group by ".$group_by; 
		$query = $this->db->query($qry_Sec);
		 $sum2 = $query->num_rows();
		 
		 $qry_radio= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and." ".$andAss."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.")  and is_score_ans=0".$andDate." and is_radio_ans_correct IS NOT NULL group by ".$group_by_radio;
		$query = $this->db->query($qry_radio);
  		 $sum3 = $query->num_rows();
		
		$ToalQes = $sum1 + $sum2+$sum3;
		
		$qry_Pri_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and." ".$andAss."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.")  and is_correct_ans=1 and is_score_ans=0".$andDate."  group by ".$group_by;	
		$query = $this->db->query($qry_Pri_correct);
		 $c_sum1 = $query->num_rows();
		 
		 $qry_Sec_correct= "select count(0) as cnt, question_id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and." ".$andAss."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.")  and is_correct_ans=1 and is_score_ans=1".$andDate."  group by ".$group_by; 
		$query = $this->db->query($qry_Sec_correct);
		 $c_sum2 = $query->num_rows();
		 
		  $qry_radio_correct= "select count(0) as cnt, ua.id from user_answers ua,user_quizzes uq,  assignments a where ua.user_quiz_id=uq.id and uq.user_id = ".$user_id."  and uq.assignment_id=a.id ".$and." ".$andAss."  and ua.question_id in (select id from questions where test_cluster='".$test_cluster."' and district_id=".$district_id.") and is_radio_ans_correct = 1 and is_score_ans=0".$andDate." group by ".$group_by_radio;
		$query = $this->db->query($qry_radio_correct);
  		 $c_sum3 = $query->num_rows();
		
		$Curr_Qes = $c_sum1 + $c_sum2 + $c_sum3;
		if($ToalQes != 0)	
			$per = $Curr_Qes*100/$ToalQes;
		else
			$per =0;
		 	$per = number_format($per,2,'.','');
 		return $per;
	 }
	 
   function getcategorynamebyId($catid)
   {
 	 $qry = "SELECT * FROM  cats where id='".$catid."'";
	 	$query = $this->db->query($qry);
		$temp =$query->result_array();	
	    return $temp;
   }
 
 
 
	 
} // class end

?>