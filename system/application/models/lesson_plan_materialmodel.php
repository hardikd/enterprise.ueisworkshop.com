<?php
class Lesson_plan_materialmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function Insert_Media($school_id,$file_upload,$subject_id,$grade_id,$name) 
	{
	
	   $time=time();
	    
          if($this->session->userdata('login_tupe')=='observer')
		  {
		  $login_id=$this->session->userdata("observer_id");
		  }
		  else
		  {
		  $login_id=$this->session->userdata("dist_user_id");
		  }
        
		$type=$this->session->userdata("login_type");
        $district_id=$this->session->userdata("district_id");				
        
        
			$q="INSERT INTO `lesson_plan_material` (user_id,name ,type,district_id,school_id,file_path,created,subject_id,grade_id) VALUES ('$login_id','$name', '$type','$district_id','$school_id','$file_upload','$time',$subject_id,$grade_id)";
			 			
			 $query = mysql_query($q) or die(mysql_error());

			
			
			 
			 return true;
        
		
       
    }
	
	function getmediaCount()
	{
	     
		  
			$school_id=$this->session->userdata('search_media_school_id');
		  
		  $searchquery='';
		  if($this->session->userdata('search_mediasearch_school_id'))
		 {
		    $search=$this->session->userdata('search_mediasearch_school_id');
			$searchquery.=" and M.name like '%$search%' ";
		 }
		  $qry="SELECT count(*) as count FROM dist_subjects ds,dist_grades g,lesson_plan_material M LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' and M.subject_id=ds.dist_subject_id and M.grade_id=g.dist_grade_id $searchquery ";

        
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	
	
	}
	function getmedia($page,$per_page)
	{
	    $page -= 1;
		$start = $page * $per_page;
		
		
		 
		  
			$school_id=$this->session->userdata('search_media_school_id');
		  	
		  $searchquery='';
		  if($this->session->userdata('search_mediasearch_school_id'))
		 {
		    $search=$this->session->userdata('search_mediasearch_school_id');
			$searchquery.=" and M.name like '%$search%' ";
		 }
		  	
		  $qry="SELECT ds.subject_name,g.grade_name,M.name,M.file_path,s.school_name,M.lesson_plan_material_id, M.user_id,  M.created,  ob.observer_name    as username,M.type FROM dist_subjects ds,dist_grades g,lesson_plan_material M 
LEFT JOIN observers ob ON M.type='observer' AND ob.observer_id = M.user_id LEFT JOIN schools s ON  M.school_id = s.school_id where M.school_id='$school_id' and M.subject_id=ds.dist_subject_id and M.grade_id=g.dist_grade_id $searchquery order by M.lesson_plan_material_id desc  limit $start,$per_page ";

        
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			
			return $query->result_array();	
		}else{
			return FALSE;
		}
	
	
	}
	
	function delete($id)
	{
		$qry = "delete from lesson_plan_material where lesson_plan_material_id=$id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	function getfile($id)
	{
		$qry = "select file_path from lesson_plan_material where lesson_plan_material_id=$id";
		$query = $this->db->query($qry);		
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->file_path;			
		}else{
		
			return mysql_error();
		}
	}
	function getmaterial($subject_id,$grade_id,$school_id=false)
	{
		if($school_id==false && $this->session->userdata("school_summ_id"))
		{
		$school_id=$this->session->userdata("school_summ_id");
		} else if($this->session->userdata("school_id")){
			$school_id=$this->session->userdata("school_id");
		}
		$qry = "select * from lesson_plan_material lpm JOIN dist_subjects ds ON lpm.subject_id = ds.dist_subject_id JOIN dist_grades g ON lpm.grade_id = g.dist_grade_id where lpm.subject_id=$subject_id and lpm.school_id=$school_id";
		$query = $this->db->query($qry);		
		if($query->num_rows()>0){
			return $query->result_array();			
		}else{
		
			return false;
		}
	}
	
	// Media End
	
	
	
	
	
}