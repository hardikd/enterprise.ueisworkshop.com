<?php
class Utilmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function get_recperpage()
	{
		
		$qry = "select UTIL_VALUE from util where UTIL_CODE='recsperpage'";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->UTIL_VALUE;			
		}else{
			return FALSE;
		}
		
	}
	
	function get_paginationlinks()
	{
		$qry = "select UTIL_VALUE from util where UTIL_CODE='paginationlinks'";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->UTIL_VALUE;			
		}else{
			return FALSE;
		}
	}
	
}