<?php
class Assessscoringmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function is_valid_login(){
		$username = $this->input->post('username');
		if($username!='')
		{
		$password = md5(trim($this->input->post('password')));
		$qry="SELECT ob.*,s.* from teachers ob,schools s  WHERE ob.username='".$username."' AND ob.password='".$password."' and ob.school_id=s.school_id ";
		//$qry="SELECT teacher_id,firstname,lastname,school_id FROM teachers WHERE username='".$username."' AND password='".$password."' ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		}
		else
		{
		  return false;
		
		}
	}
	function getteacherCount($school_id)
	{
	
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		 $qry="Select count(*) as count 
	  			from teachers t,schools s where t.school_id=s.school_id and s.district_id=$district_id " ;
		
		}
		else
		{
		
			$qry="Select count(*) as count 
	  			from teachers " ;
		}
			
		}
		else
		{
		 $qry="Select count(*) as count 
	  			from teachers  where school_id=$school_id" ;
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	function getteacherCountbydistrict()
	{
	
	
		//$district_id=$this->session->userdata('district_id');
		//$qry = "select count(*) as count from assignments,assignment_users ,users,quizzes where users.UserID=assignment_users.user_id and assignments.id = assignment_users.assignment_id  and quizzes.id=assignments.quiz_id and assignments.district_id='".$district_id."' group by assignments.id order by assignments.id desc";
		
		$district_id=$this->session->userdata('district_id');
	
	$qry = "SELECT count(*) as count
FROM assignments ass, quizzes q
WHERE ass.district_id ='".$district_id."'
AND ass.quiz_id = q.id";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getteachers($page,$per_page,$school_id)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($school_id=='all')
		{
			if($this->session->userdata('login_type')=='user')
		{
			$district_id=$this->session->userdata('district_id');
		
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id and s.district_id=$district_id  limit $start, $per_page ";
		}
		else
		{
			
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id  limit $start, $per_page ";
		
		}
		}
		else
		{
			$qry="Select b.username,b.teacher_id as teacher_id,b.firstname as firstname,b.lastname as lastname,s.school_name as school_name,s.school_id as school_id from teachers b,schools s where b.school_id=s.school_id and b.school_id=$school_id  limit $start, $per_page ";
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getteachersbydistrict($page,$per_page)
	{		
		$page -= 1;
		$start = $page * $per_page;
		
			 //$qry="select * from assignments,assignment_users ,users,quizzes where users.UserID=assignment_users.user_id and assignments.id = assignment_users.assignment_id  and quizzes.id=assignments.quiz_id  limit $start, $per_page " ;
		/* $qry="SELECT assignments.id,assignments.added_date,assignments.pass_score,assignments.assignment_name,users.UserID,users.UserName,users.Name,quizzes.quiz_name,quizzes.quiz_desc
FROM assignments, assignment_users, users, quizzes
WHERE users.UserID = assignment_users.user_id
AND assignments.id = assignment_users.assignment_id
AND quizzes.id = assignments.quiz_id  limit $start, $per_page " ; */

	$district_id = $this->session->userdata('district_id');

	$qry = "SELECT ass.id, ass.added_date, ass.pass_score, ass.assignment_name, q.quiz_name, q.quiz_desc FROM assignments ass,quizzes q where ass.district_id = $district_id and ass.quiz_id=q.id limit $start, $per_page";
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	}
	
	function getteacherById($teacher_id)
	{
		
		$qry = "Select t.teacher_id,t.firstname,t.school_id,t.lastname,t.username,t.email,t.avatar,d.country_id,d.state_id,d.district_id  from teachers t,schools s,districts d where d.district_id=s.district_id and s.school_id=t.school_id and  t.teacher_id=".$teacher_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getTeachersBySchool($school_id)
	{
	$qry = "Select * from teachers where school_id=".$school_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return '';
		}
	
	
	
	}
	
	function getTeachersBySchoolApi($school_id)
	{
	$qry = "Select teacher_id,firstname,lastname from teachers where school_id=".$school_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return '';
		}
	
	
	
	}
	function check_teacher_exists($name=false,$username=false,$school_id=false)
	{
	
		if($name==false)
		{
		$name=$this->input->post('firstname');
		$school_id=$this->input->post('school_id');
		$username=$this->input->post('username');
		}
		/*$qry="SELECT  teacher_id from teachers where firstname='$name' and school_id=$school_id ";
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
		
						return 0;

			
		}
		else*/
		{
			
			$sqry="SELECT  teacher_id from teachers where username='$username'  ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
		}
	
	
	}
	function check_teacher_update() {
	
	
		$name=$this->input->post('firstname');
		$school_id=$this->input->post('school_id');
		$id=$this->input->post('teacher_id');
		$username=$this->input->post('username');
		/*$qry="SELECT teacher_id from teachers where firstname='$name' and school_id=$school_id and teacher_id!=$id";
		
		$query = $this->db->query($qry);
		//echo $query->num_rows();
		//exit;
		if($query->num_rows()>0){
			return 0;
		}else*/
		{
			$sqry="SELECT  teacher_id from teachers where username='$username' and teacher_id!=$id ";
			$squery = $this->db->query($sqry);
		if($squery->num_rows()>0){
		
		 return 2;
		}
		else
		{
		  return 1;
		}
		}
	
	}
	function add_teacher()
	{
	  $data = array('firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),	
					'school_id' => $this->input->post('school_id'),
                    'username'=>$this->input->post('username'),					
		            'password'=> md5($this->input->post('password')),
                    'email'=> $this->input->post('email'),
		'avatar'=>$this->input->post('avatar')	
		
		);
	   try{
			$str = $this->db->insert_string('teachers', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function add_bulk_teacher($firstname,$lastname,$school_id,$username,$password,$employee_number,$email)
	{
	  $data = array('firstname' => $firstname,
					'lastname' => $lastname,	
					'school_id' => $school_id,
                    'username'=>$username,					
		            'password'=> md5($password),
					'emp_number'=>$employee_number,
                    'email'=>$email					
		
		);
	   try{
			$str = $this->db->insert_string('teachers', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_teacher()
	{
	
		
		$teacher_id=$this->input->post('teacher_id');
	$password=$this->input->post('password');
		if($password!='torvertex')
		{
		 $password=md5($password);
	$data = array('firstname' => $this->input->post('firstname'),
				  'lastname' => $this->input->post('lastname'),	
	               'school_id' => $this->input->post('school_id'),
				   'username' => $this->input->post('username'),
					'password'=> $password,
                    'email'=> $this->input->post('email'),
		'avatar'=>$this->input->post('avatar')	
		
		
		);
		}
		else
		{
		
			$data = array('firstname' => $this->input->post('firstname'),
				  'lastname' => $this->input->post('lastname'),
					'school_id' => $this->input->post('school_id'),				  
	               'username' => $this->input->post('username'),
                    'email'=> $this->input->post('email') ,
		'avatar'=>$this->input->post('avatar')	
		
		         );
		
		}
			
		$where = "teacher_id=".$teacher_id;		
		
		try{
			$str = $this->db->update_string('teachers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deleteteacher($teacher_id,$studentuserid)
	{
echo	$qry = "delete from assignment_users where assignment_id=".$studentuserid." and user_id=".$teacher_id." ";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	function pdfreportsave($filesave,$status,$score,$dates,$teacher_id,$comments)
	{
	
	 $login_type=$this->session->userdata('login_type');
             if($login_type=='observer')
			{
				$login_id=$this->session->userdata('observer_id');

			}
			else if($login_type=='user')
			{
				$login_id=$this->session->userdata('dist_user_id');

			}
			$n=date('Y-m-d H:i:s');
	 $data = array('teacher_id' => $teacher_id,
					'score' => $score,	
					'status' => $status,
                    'dates'=>$dates,					
		            'file_path'=> $filesave,
					'type'=>$login_type,	
		            'user_id'=>$login_id,
					'comments'=>$comments,
					'created'=>$n						
		);
	   try{
			$str = $this->db->insert_string('summative', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	function changeTeacherEmail()
	
	{
	$data = array(	'emailnotifylesson'=>$this->input->post('emailnotifylesson')	
		
		
		);
		
		
		
		$teacher_id=$this->session->userdata('teacher_id');
	
		
			
		$where = "teacher_id=".$teacher_id;		
		
		try{
			$str = $this->db->update_string('teachers', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function getemailnotify()
	{
	  $teacher_id=$this->session->userdata('teacher_id');
	
	 $qry = "Select emailnotifylesson from teachers where teacher_id=".$teacher_id." ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	function getTeacherObservationReport($teacher_id,$year)
	{
	  
	
	  $qry = "select count(report_id) as count ,MONTH(report_date) as month ,report_form,period_lesson from reports where teacher_id=$teacher_id and year(report_date)='$year' group by YEAR(report_date), MONTH(report_date),report_form,period_lesson union select count(report_id) as count ,MONTH(report_date) as month ,report_form,period_lesson from proficiencyreports where teacher_id=$teacher_id and year(report_date)='$year' group by YEAR(report_date), MONTH(report_date),report_form,period_lesson ;";
	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	
}