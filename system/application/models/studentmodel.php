<?php

class Studentmodel extends Model {

    function __construct() {
        parent::__construct();
    }

    function getGrades() {

        $district_id = $this->session->userdata('district_id');
        $qry = "select distinct(s.grade) from students s,parents p where s.parents_id=p.parents_id and p.district_id=$district_id  ";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getstudentCount($day, $period_id, $teacher_id = false) {
        if ($teacher_id == false) {
            $teacher_id = $this->session->userdata('teacher_id');
        }

        $qry = "Select count(*) as count	from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.current_date=$day and t.class_room_id=ts.class_room_id and ts.teacher_id=$teacher_id and ts.period_id=$period_id and ts.day=$day and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->count;
        } else {
            return FALSE;
        }
    }

    function getstudentsdata($day, $period_id, $page, $per_page) {
        $teacher_id = $this->session->userdata('teacher_id');

        $page -= 1;
        $start = $page * $per_page;

        $qry = "Select d.subject_name,c.class_room_id,c.name as class_name,s.*,g.grade_name as grade,ts.teacher_student_id,ts.email from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.current_date=$day and t.class_room_id=ts.class_room_id and ts.teacher_id=$teacher_id and ts.period_id=$period_id and ts.day=$day and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id limit $start, $per_page ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getallstudentsdata($day = false, $period_id, $teacher = '') {
        
        $qry = "Select d.subject_name,c.class_room_id,c.name as class_name,s.*,g.grade_name as grade,ts.teacher_student_id,ts.email from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.period_id=$period_id  and t.class_room_id=ts.class_room_id  and ts.period_id=$period_id and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id ";

        $qry .= " and ts.day=$day and t.current_date=$day ";

        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
            $qry .= " and ts.teacher_id=$teacher_id and t.teacher_grade_subject_id=$teacher_id ";
        } else if ($teacher != '') {
            $qry .= " and ts.teacher_id=$teacher and t.teacher_grade_subject_id=$teacher ";
        }
		
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function changestatus($type, $id) {

        $teacher_id = $this->session->userdata('teacher_id');

        $data = array('email' => $type);
        $str = $this->db->update('teacher_students', $data, "student_id = $id and teacher_id=$teacher_id");

        if (!($str)) {
            return 0; //fail
        } else {
            return 1; //success
        }
    }

    function getstudents() {
        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
        } else if ($this->session->userdata('school_summ_id')) {
            $school_id = $this->session->userdata('school_summ_id');
        }

        $pdata = $this->input->post('pdata');
        $grade = $pdata['id'];
        $search = '';
        if ($pdata['fname'] != '') {
            $fname = $pdata['fname'];
            $search.=" and s.firstname like '%$fname%' ";
        }
        if ($pdata['lname'] != '') {
            $lname = $pdata['lname'];
            $search.=" and s.lastname like '%$lname%' ";
        }
        $qry = "select s.* from students s where  s.school_id=$school_id and s.grade='$grade' $search ";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getstudentsall() {
        if ($this->session->userdata('school_id')) {
            $school_id = $this->session->userdata('school_id');
        } else if ($this->session->userdata('school_summ_id')) {
            $school_id = $this->session->userdata('school_summ_id');
        }

        $pdata = $this->input->post('pdata');
        $grade = $pdata['id'];
        $search = '';
        if ($pdata['fname'] != '') {
            $fname = $pdata['fname'];
            $search.=" and s.firstname like '%$fname%' ";
        }
        if ($pdata['lname'] != '') {
            $lname = $pdata['lname'];
            $search.=" and s.lastname like '%$lname%' ";
        }
        $qry = "select s.* from students s where  s.school_id=$school_id and s.grade='$grade' $search ";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getstudentnumber() {


        $pdata = $this->input->post('pdata');
        $student = $pdata['id'];

        $qry = "select s.student_number from students s where student_id=$student ";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function check_student_exists($day = false, $period_id = false, $student_id = false) {


        if ($student_id == false) {
            $student_id = $this->input->post('student_id');

            $period_id = $this->input->post('period_id');
            $day = $this->input->post('day');
        }
        if ($this->session->userdata('teacher_id'))
            $teacher_id = $this->session->userdata('teacher_id');
        else if ($this->input->post('teacher_id'))
            $teacher_id = $this->input->post('teacher_id');
        else {
            $pdata = $this->input->post('pdata');
            if($pdata['teacher_id']!=''){
                $teacher_id = $pdata['teacher_id'];
            }
        }

        $qry = "SELECT  s.student_id from teacher_students s where s.student_id=$student_id  and s.teacher_id=$teacher_id  and s.period_id=$period_id and s.day=$day ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {

            return 'Student Already Added To Your This Class. ';
        } else {
            return 'success';
        }
    }

    function add_student($day = false, $period_id = false, $class_room_id = false, $student_id = false) {
        if ($this->session->userdata('teacher_id'))
            $teacher_id = $this->session->userdata('teacher_id');
        else if ($this->input->post('teacher_id'))
            $teacher_id = $this->input->post('teacher_id');
        else {
            $pdata = $this->input->post('pdata');
            if($pdata['teacher_id']!=''){
                $teacher_id = $pdata['teacher_id'];
            }
        }

//            echo $teacher_id;exit;

        if ($student_id == false) {
            $student_id = $this->input->post('student_id');
            $class_room_id = $this->input->post('class_room_id');
            $day = $this->input->post('day');
            $period_id = $this->input->post('period_id');
        }


        $date = date('y:m:d', time());
        $qry = "select distinct assignment_id  from assignment_users au, assignments a where au.assignment_id = a.id and  a.status = 1 and au.user_id in (select DISTINCT u.UserID from users u inner join teacher_students ts on u.student_id = ts.student_id where teacher_id = $teacher_id and class_room_id =  $class_room_id and period_id = $period_id)";
        $query = $this->db->query($qry);
//            echo $this->db->last_query();exit;
        $assessment_array = $query->result_array();

        $qrys = "select UserID from users where student_id = $student_id";
        $querys = $this->db->query($qrys);
        $uid_array = $querys->result_array();
        $uid = @$uid_array[0]['UserID'];

        if (!empty($assessment_array)) {
            foreach ($assessment_array as $aarray) {
                $aid = $aarray['assignment_id'];
                $insertquery = "insert into assignment_users(id,assignment_id,user_type,user_id) values (NULL,$aid,1,$uid)";


                $qins = $this->db->query($insertquery);
//                   echo $this->db->last_query();exit;
            }
        }

        $data = array('student_id' => $student_id,
            'teacher_id' => $teacher_id,
            'class_room_id' => $class_room_id,
            'day' => $day,
            'period_id' => $period_id
        );
        try {
            $str = $this->db->insert_string('teacher_students', $data);
            if ($this->db->query($str)) {
                return $this->db->insert_id();
            } else {
                return 0;
            }
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    function getclassroom($day, $period_id, $teacher_id = '') {
        if ($this->session->userdata('teacher_id'))
            $teacher_id = $this->session->userdata('teacher_id');


        $qry = "Select c.name as class_name,d.subject_name,c.class_room_id	from class_rooms c,time_table t,dist_subjects d where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.current_date=$day and  t.class_room_id=c.class_room_id   ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getallclassroom($day = false) {

        $qry = "Select t.period_id,c.class_room_id,time_format(p.start_time,'%h:%i %p') as start_time,time_format(p.end_time,'%h:%i %p') as end_time from class_rooms c,time_table t,dist_subjects d,periods p where t.period_id=p.period_id and t.subject_id=d.dist_subject_id and  t.class_room_id=c.class_room_id";

        $qry .= " and t.current_date=$day ";
        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
            $qry.=" and t.teacher_grade_subject_id=$teacher_id  ";
        } else if ($this->input->post('teacher_id')) {
            $teacher_id = $this->input->post('teacher_id');
            $qry.=" and t.teacher_grade_subject_id=$teacher_id  ";
        } else {
            $pdata=$this->input->post('pdata');  
            if($pdata['teacher_id']!='') {
                $teacher_id = $pdata['teacher_id'];
            $qry.=" and t.teacher_grade_subject_id=$teacher_id  ";
            }
        }

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getalldaystudents($day, $period) {
        if($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $pdata=$this->input->post('pdata'); 
            $teacher_id = $pdata['teacher_id'];
        }
        $qry = "Select t.student_id from teacher_students t where t.day=$day and t.period_id=$period and t.teacher_id=$teacher_id ";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function deletestudent($student_id) {
        $teacher_id = $this->session->userdata('teacher_id');
        $qry = "delete from teacher_students where teacher_student_id=$student_id ";
        if ($teacher_id) {
            $qry.=" and teacher_id=$teacher_id ";
        }
        $query = $this->db->query($qry);
        if (mysql_error() == "") {
            return true;
        } else {
            return mysql_error();
        }
    }

    function updatestudentroll($student_id, $teacher_student_id, $date, $period, $rollstatus, $teacher_id) {

        $check = $this->checkstundntroll($student_id, $teacher_student_id, $date, $period, $teacher_id);
        if ($check) {
            $this->db->where('student_roll_id', $check);
            $this->db->update('student_roll', array('student_id' => $student_id, 'teacher_student_id' => $teacher_student_id, 'period_id' => $period, 'date' => $date, 'status' => $rollstatus, 'teacher_id' => $teacher_id));
        } else {
            $this->db->insert('student_roll', array('student_id' => $student_id, 'teacher_student_id' => $teacher_student_id, 'period_id' => $period, 'date' => $date, 'status' => $rollstatus, 'teacher_id' => $teacher_id));
        }
    }

    function checkstundntroll($student_id, $teacher_student_id, $date, $period, $teacher_id) {
        $query = $this->db->get_where('student_roll', array('student_id' => $student_id, 'teacher_student_id' => $teacher_student_id, 'period_id' => $period, 'date' => $date, 'teacher_id' => $teacher_id));
        $result = $query->result();
        if ($result) {
            return $result[0]->student_roll_id;
        } else {
            return false;
        }
    }

    function getstundentroll($student_id, $teacher_student_id, $date, $period,$teacher_id='') {
        if($teacher_id!=''){
            $check = $this->checkstundntroll($student_id, $teacher_student_id, $date, $period, $teacher_id);

            if (!$check) {
                $this->db->insert('student_roll', array('student_id' => $student_id, 'teacher_student_id' => $teacher_student_id, 'period_id' => $period, 'date' => $date, 'status' => 'present', 'teacher_id' => $teacher_id));
            }
        }

        $query = $this->db->get_where('student_roll', array('student_id' => $student_id, 'teacher_student_id' => $teacher_student_id, 'period_id' => $period, 'date' => $date));
        $result = $query->result();
        if ($result) {
            return $result[0]->status;
        } else {
            return false;
        }
    }

    //added to get the students for the date range
    function getStudentsByRange($period_id,$teacher_id='') {
        if ($this->session->userdata('teacher_id'))
            $teacher_id = $this->session->userdata('teacher_id');
        else if ($teacher_id=='')
            $teacher_id = $this->input->post('teacher_id');

        $qry = "Select d.subject_name,c.class_room_id,c.name as class_name,s.*,g.grade_name as grade,ts.teacher_student_id,ts.email from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.class_room_id=ts.class_room_id and ts.teacher_id=$teacher_id and ts.period_id=$period_id and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id group by s.student_id";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getstudentroll() {
        if ($this->input->post('student_id') != 'all') {
            $student_id = $this->input->post('student_id');
            $this->db->where('student_id', $student_id);
        }

        $period_id = $this->input->post('period_id');


        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id = $this->input->post('teacher_id');
        }

        $this->db->where('period_id', $period_id);
        $this->db->where('teacher_id', $teacher_id);
        $query = $this->db->get('student_roll');
//                        echo $this->db->last_query();exit;

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getStudentDetails($student_id) {
        $this->db->where('student_id', $student_id);
        $query = $this->db->get('students');
        return $query->result();
    }

    function getStudentsByRange_data($period_id, $teacher_id = '') {
        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id;
        }
        $qry = "Select d.subject_name,c.class_room_id,c.name as class_name,s.*,g.grade_name as grade,ts.teacher_student_id,ts.email from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.class_room_id=ts.class_room_id and ts.teacher_id=$teacher_id and ts.period_id=$period_id and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id group by s.student_id";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getstudentroll_data($student_id, $period_id, $teacher_id = '') {

        if ($student_id != 'all') {

            $this->db->where('student_id', $student_id);
        }

        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id;
        }

        $this->db->where('period_id', $period_id);
        $this->db->where('teacher_id', $teacher_id);
        $query = $this->db->get('student_roll');
        //echo $this->db->last_query();exit;

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getstudentrollBy_teacher_data($student_id, $period_id, $teacher_id = '') {

        if ($student_id != 'all') {
            $this->db->where('student_roll.student_id', $student_id);
        }

        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id;
        }
        $this->db->join('teachers', 'student_roll.teacher_id = teachers.teacher_id', 'LEFT');
        $this->db->join('teacher_students', 'student_roll.teacher_student_id = teacher_students.teacher_student_id', 'LEFT');
        $this->db->join('class_rooms', 'teacher_students.class_room_id = class_rooms.class_room_id', 'LEFT');
        $this->db->where('student_roll.period_id', $period_id);
        $this->db->where('student_roll.teacher_id', $teacher_id);
        $query = $this->db->get('student_roll');
        //echo $this->db->last_query();exit;

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

//    function getStudentsByRange_data($period_id, $teacher_id = '') {
//        if ($this->session->userdata('teacher_id')) {
//            $teacher_id = $this->session->userdata('teacher_id');
//        } else {
//            $teacher_id;
//        }
//        $qry = "Select d.subject_name,c.class_room_id,c.name as class_name,s.*,g.grade_name as grade,ts.teacher_student_id,ts.email from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.class_room_id=ts.class_room_id and ts.teacher_id=$teacher_id and ts.period_id=$period_id and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id group by s.student_id";
//
//        $query = $this->db->query($qry);
//        //print_r($this->db->last_query());exit;	
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        } else {
//            return false;
//        }
//    }

    function getTeacherByRange_data($period_id, $teacher_id = '') {
        if ($this->session->userdata('teacher_id')) {
            $teacher_id = $this->session->userdata('teacher_id');
        } else {
            $teacher_id;
        }
        $qry = "Select d.subject_name,c.class_room_id,c.name as class_name,s.*,g.grade_name as grade,ts.teacher_student_id,ts.email from teacher_students ts,students s,class_rooms c,time_table t,dist_subjects d ,dist_grades g where t.subject_id=d.dist_subject_id and t.teacher_grade_subject_id=$teacher_id and t.period_id=$period_id and t.class_room_id=ts.class_room_id and ts.teacher_id=$teacher_id and ts.period_id=$period_id and ts.class_room_id=c.class_room_id and ts.student_id=s.student_id  and s.grade=g.dist_grade_id group by s.student_id";

        $query = $this->db->query($qry);
        print_r($this->db->last_query());
        exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    public  function GetStudentByTeacherId($id,$school_id)
        {
            $sql = "SELECT * FROM `students` where `student_id` in (SELECT DISTINCT `student_id` FROM `teacher_students` WHERE `teacher_id` =".$id.") and `school_id` = ".$school_id;
            $query = $this->db->query($sql);
            $students =$query->result_array();	
				
	   
	   return $students;
        }

        public  function GetTeacherByStudentId($id,$period_id)
        {
            $sql = "SELECT * FROM `teachers` where `teacher_id` in (SELECT DISTINCT `teacher_id` FROM `teacher_students` WHERE `student_id` =".$id." AND period_id = ".$period_id.")";
            $query = $this->db->query($sql);
            $students =$query->result_array();  
            return $students;
        }

        

}

?>	