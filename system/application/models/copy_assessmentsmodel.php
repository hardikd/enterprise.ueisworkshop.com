<?php
class Copy_assessmentsmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
 
 
  function getDistAssessments($district_id)
   {
	 	$qry='select id,quiz_name from quizzes where district_id= '.$district_id.' and parent_id = 0 order by id DESC';
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
   }
   
     function getDistricts()
   {
	   $qry="select * from districts where type='active' order by districts_name";
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
   function getToDistricts($dist_id)
   {
	   $qry="select * from districts where type='active' AND district_id !=".$dist_id." order by districts_name";
	   $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
  
  function copyAssessments($dist_id,$to_dist,$a_id)
   {
     error_reporting(0);
	 $success = '';$successAss = 0; $successQue = 0;$successAns = 0;
	  $date = date('Y-m-d H:i:s');
      $qry="INSERT INTO quizzes(cat_id,quiz_name,quiz_desc,parent_id,show_intro,intro_text) SELECT cat_id,quiz_name,quiz_desc,parent_id, show_intro,intro_text FROM quizzes where district_id = ".$dist_id." And id= ".$a_id;
	  if($query = $this->db->query($qry)) //if assessment inserted
	  {
		$new_a_id = $this->db->insert_id();
		$successAss = 1;
	 	$qry="UPDATE quizzes SET added_date = '$date',district_id =$to_dist where id=$new_a_id";
		$query = $this->db->query($qry);
	// for question
		 $qry = "SELECT * from questions WHERE district_id = ".$dist_id." And quiz_id= ".$a_id;
		  $query = $this->db->query($qry);
		  $questionArray =array();
		 if($query->num_rows()>0)
		 {
			
			 $questionArray = $query->result_array();
			 $totalQuestion = count($questionArray); 
			 for($i=0;$i<$totalQuestion;$i++)
			 {
			 	$data = array();
				$point ='';$parent_id ='';
				if($questionArray[$i]['point'] == '')
				$point = 0;
				else $point = $questionArray[$i]['point'];
				if($questionArray[$i]['secondary_point'] == '')
				$secondary_point = 0;
				else $secondary_point = $questionArray[$i]['secondary_point'];
					
				$data = array(
			   'question_text' => $questionArray[$i]['question_text'] ,
			   'question_type_id' => $questionArray[$i]['question_type_id'] ,
			   'priority' => $questionArray[$i]['priority'],
			   'quiz_id' => $new_a_id,
			   'added_date' => $date,
			   'point'=>$point,
			   'parent_id' =>$questionArray[$i]['parent_id'],
			   'question_total' => $questionArray[$i]['question_total'] ,
			   'check_total' => $questionArray[$i]['check_total'] ,
			   'header_text' => $questionArray[$i]['header_text'],
			   'footer_text' => $questionArray[$i]['footer_text'],
			   'question_text_eng' => $questionArray[$i]['question_text_eng'],
			   'help_image'=>$questionArray[$i]['help_image'],
			   'penalty_point' =>$questionArray[$i]['penalty_point'],
			   'test_cluster' => $questionArray[$i]['test_cluster'],
			   'district_id' =>$to_dist,
			   'sec_question_type_id' => $questionArray[$i]['sec_question_type_id'] ,
			   'secondary_point' => $secondary_point,
			   'have_score_ans' => $questionArray[$i]['have_score_ans'],
					);
//                                if(substr_count($questionArray[$i]['test_cluster'], "Oraciones")>0){
//                                echo "<br />";
//                                echo $questionArray[$i]['test_cluster'];
//                                echo "<br />";
//                                print_r($data);
//                                
//                                }
				if($this->db->insert('questions', $data)) // if new questiiion insert
				{
					$successQue = 1;
					$ins_que_id = $this->db->insert_id();
					
					$current_que_id = $questionArray[$i]['id'];
				
						// for question group
					 $qry = "SELECT * from question_groups WHERE question_id = ".$current_que_id;
					 $query = $this->db->query($qry);
					 $que_group_Array =array();
					
					if($query->num_rows()>0)
					{
						$que_group_Array = $query->result_array();
						 $totalQue_groups= count($que_group_Array); 
						 for($qg=0;$qg<$totalQue_groups;$qg++)
						 {
							$data = array();
							$group_name ='';$show_header ='';$group_total ='';$parent_id ='';$group_total = '';
							if($que_group_Array[$qg]['group_name'] == '')
							  $group_name = ' '; else $group_name = $que_group_Array[$qg]['group_name'];
							if($que_group_Array[$qg]['show_header'] == '')
							  $show_header = ' '; else $show_header = $que_group_Array[$qg]['show_header'];
							 if($que_group_Array[$qg]['group_total'] == '' || $que_group_Array[$qg]['group_total'] == 0 )
							  $group_total = 0; else $group_total = $que_group_Array[$qg]['group_total'];
							 if($que_group_Array[$qg]['parent_id'] == '' || $que_group_Array[$qg]['parent_id'] == 0 )
							  $parent_id = 0;  else $parent_id = $que_group_Array[$qg]['parent_id'];
							
							$data = array(
						   'group_name' => $group_name,
						   'show_header' => $show_header,
						   'group_total' => $group_total,
						   'show_footer' => $que_group_Array[$qg]['show_footer'],
						   'check_total' =>$que_group_Array[$qg]['check_total'],
						   'question_id'=>$ins_que_id,
						   'group_name_eng' =>$que_group_Array[$qg]['group_name_eng'],
						   'parent_id' =>$parent_id,
						   'added_date' => $date,
						   'is_secondary_group' => $que_group_Array[$qg]['is_secondary_group'] 
						   );
						   if($this->db->insert('question_groups',$data)) //if  new question group inserted
						   {
								$ins_que_grp_id = $this->db->insert_id();
								$current_que_grp_id = $que_group_Array[$qg]['id'];
				
								// for answer varient
								 $qry = "SELECT * from answers_quiz WHERE  group_id = ".$current_que_grp_id;
								 $query = $this->db->query($qry);
								 $Ans_Array =array();
								 
								if($query->num_rows()>0)
								{
									$Ans_Array = $query->result_array();
									$total_ans= count($Ans_Array); 
									 for($a=0;$a<$total_ans;$a++)
									 {
										$data = array();
										$correct_answer ='';$answer_pos ='';$parent_id ='';$text_unit ='';$answer_desc ='';
										if($Ans_Array[$a]['correct_answer'] == '' || $Ans_Array[$a]['correct_answer'] == 0)
										$correct_answer = 0; else $correct_answer = $Ans_Array[$a]['correct_answer'];
										if($Ans_Array[$a]['answer_pos'] == '' || $Ans_Array[$a]['answer_pos'] == 0)
										$answer_pos = 0; else $answer_pos = $Ans_Array[$a]['answer_pos'];
										if($Ans_Array[$a]['parent_id'] == '' || $Ans_Array[$a]['parent_id'] == 0)
										$parent_id = 0; else $parent_id = $Ans_Array[$a]['parent_id'];
										if($Ans_Array[$a]['text_unit'] == '' || $Ans_Array[$a]['text_unit'] == ' ')
										$text_unit = ' '; else $text_unit = $Ans_Array[$a]['text_unit'];
										if($Ans_Array[$a]['answer_desc'] == '')
										$answer_desc = ' '; else $answer_desc = $Ans_Array[$a]['answer_desc'];
											
										$data = array(
									   'group_id' => $ins_que_grp_id ,
									   'answer_text' => $Ans_Array[$a]['answer_text'] ,
									   'answer_image' => $Ans_Array[$a]['answer_image'],
									   'correct_answer' => $correct_answer,
										'priority' => $Ans_Array[$a]['priority'],
									   'correct_answer_text'=>$Ans_Array[$a]['correct_answer_text'],
										'answer_pos' => $answer_pos,
									   'parent_id' => $parent_id,
									   'answer_text_eng' => $Ans_Array[$a]['answer_text_eng'],
										'control_type' => $Ans_Array[$a]['control_type'] ,
									  'answer_parent_id' => $Ans_Array[$a]['answer_parent_id'] ,
									   'text_unit' => $text_unit,
									   'answer_desc' => $answer_desc,
									  'is_score_ans' => $Ans_Array[$a]['is_score_ans'] ,
									   'radio_ans' => $Ans_Array[$a]['radio_ans'],
									  'score' => $Ans_Array[$a]['score'] ,
									   'is_difficult' => $Ans_Array[$a]['is_difficult']);
									   if($this->db->insert('answers_quiz',$data))
									   $successAns = 1;
									}
								 }
							 } // if  new question group inserted
						}	// end for totalQue_groups
					 }// end if que_group_Array
				}// if new qestion insert
	 	     } //	end for totalQuestion
	 
		 } // end if ques query
	 
 	 } //end //if assessment inserted
	/*  if($successAss == 1)
	  $success .= "Assessment copied ";
	  if($successQue == 1)
	    $success .= " with Questions";
	  if($successAns == 1)
	    $success .= ",Answers ";
	 $success .= "successfully."; */
	   if($successAss == 1)
	  $success .= "Assessment has been copied successfully.";
	 
 	return $success ;	 			
   } // end function
	 
} // class end

?>