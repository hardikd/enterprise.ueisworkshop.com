<?php
class Scoremodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	
	function getscoreCount()
	{
	
		if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		$qry="Select count(*) as count 	from score s where  s.district_id=$district_id " ;
		}
		
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getscores($page,$per_page)
	{
		
		$page -= 1;
		$start = $page * $per_page;
		if($this->session->userdata('login_type')=='user')
		{
		$district_id=$this->session->userdata('district_id');
		$qry="Select b.score_id as score_id,b.score_name as score_name from score b where b.district_id=$district_id  limit $start, $per_page" ;
		}
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getscoreById($score_id)
	{
		
		$qry = "Select score_id,score_name from score where score_id=".$score_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getscorebydistrict()
	{
		$district_id=$this->session->userdata('district_id');
		$qry = "Select score_id,score_name from score where district_id=".$district_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function add_score()
	{
	  $district_id=$this->session->userdata('district_id');
	  $data = array('score_name' => $this->input->post('score_name'),
	                'district_id'=>$district_id
		
		);
	   try{
			$str = $this->db->insert_string('score', $data);
			if($this->db->query($str))
			{
				return $this->db->insert_id();
			}
			else{
				return 0;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	}
	function update_score()
	{
	
		
		 
		$data = array('score_name' => $this->input->post('score_name')
		
		
		);
		
		
		
		
		
		$score_id=$this->input->post('score_id');
	
		
			
		$where = "score_id=".$score_id;		
		
		try{
			$str = $this->db->update_string('score', $data,$where);
			if($this->db->query($str))
			{
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());

		}
	
	
	
	}
	
	function deletescore($score_id)
	{
		$qry = "delete from score where score_id=$score_id";
		$query = $this->db->query($qry);
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	
	
}
?>