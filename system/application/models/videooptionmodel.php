<?php
class Videooptionmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
public function insert()
   {
 	if ($this->session->userdata('login_type') == 'teacher') {
          $this->db->select('*');
          $this->db->from('video_tutorial');
          $this->db->where('user_id',$this->session->userdata('teacher_id'));
          $this->db->where('user_type',$this->session->userdata('login_type'));
          $query = $this->db->get();
          
          if($query->num_rows()>0){
              $results = $query->result_array();
            
              $this->db->where('user_id',$this->session->userdata('teacher_id'));
            $this->db->where('user_type',$this->session->userdata('login_type'));
            $updatearr = array('user_id'=>$this->session->userdata('teacher_id'),'user_type'=>$this->session->userdata('login_type'),'popup_cntr'=>$results[0]['popup_cntr']+1);
              //print_r($updatearr);exit;
              $this->db->update('video_tutorial',$updatearr);
              return $query->result();
          } else {
              $insertarr = array('user_id'=>$this->session->userdata('teacher_id'),'user_type'=>$this->session->userdata('login_type'),'popup_cntr'=>1);
              $this->db->insert('video_tutorial',$insertarr);
              return true;
          }
          
        } else if ($this->session->userdata('login_type') == 'user') {
           
        } else if ($this->session->userdata('login_type') == 'observer') {
           $this->db->select('*');
          $this->db->from('video_tutorial');
          $this->db->where('user_id',$this->session->userdata('observer_id'));
          $this->db->where('user_type',$this->session->userdata('login_type'));
          $query = $this->db->get();
          //echo $this->db->last_query();exit;
          if($query->num_rows()>0){
              $results = $query->result_array();
            
              $this->db->where('user_id',$this->session->userdata('observer_id'));
            $this->db->where('user_type',$this->session->userdata('login_type'));
            $updatearr = array('user_id'=>$this->session->userdata('observer_id'),'user_type'=>$this->session->userdata('login_type'),'popup_cntr'=>$results[0]['popup_cntr']+1);
              //print_r($updatearr);exit;
              $this->db->update('video_tutorial',$updatearr);

              return $query->result();
          } else {
              $insertarr = array('user_id'=>$this->session->userdata('observer_id'),'user_type'=>$this->session->userdata('login_type'),'popup_cntr'=>1);
              $this->db->insert('video_tutorial',$insertarr);
              
              return true;
          }
        }
   }
   
   public function update($option,$showpopup){
       //echo $showpopup;exit;
       if($showpopup!='false'){
           $updatearr = array('user_id'=>$this->session->userdata('teacher_id'),'user_type'=>$this->session->userdata('login_type'),'popup_cntr'=>6,'video_option'=>$option);
       } else {
          
           $updatearr = array('user_id'=>$this->session->userdata('teacher_id'),'user_type'=>$this->session->userdata('login_type'),'video_option'=>$option);
       }
      // print_r($updatearr);exit;
       
            $this->db->where('user_id',$this->session->userdata('teacher_id'));
            $this->db->where('user_type',$this->session->userdata('login_type'));
            
              //print_r($updatearr);exit;
              $this->db->update('video_tutorial',$updatearr);
   }
   
/*   public function get_option(){
       if($this->session->userdata('login_type') == 'teacher'){

           $this->db->select('*');
           $this->db->from('video_tutorial');
           $this->db->where('user_id',$this->session->userdata('teacher_id'));
           $this->db->where('user_type',$this->session->userdata('login_type'));
           $query = $this->db->get();
           if($query->num_rows()>0){
               $result = $query->result_array();
               return $result[0]['video_option'];
           } else {
               return false;
           }
       }
   }
*/
   public function get_option(){
       if($this->session->userdata('login_type') == 'teacher'){
        if($this->session->userdata('popupcntr')!=1){ 
           $this->db->select('*');
           $this->db->from('video_tutorial');
           $this->db->where('user_id',$this->session->userdata('teacher_id'));
           $this->db->where('user_type',$this->session->userdata('login_type'));
           $query = $this->db->get();
           if($query->num_rows()>0){
               $result = $query->result_array();
               return $result[0]['video_option'];
           } else {
               return false;
           }
        } else {
          return 'with_support';
        }
       } else {
        if($this->session->userdata('popupcntr')!=1){ 
        $this->db->select('*');
           $this->db->from('video_tutorial');
           $this->db->where('user_id',$this->session->userdata('observer_id'));
           $this->db->where('user_type',$this->session->userdata('login_type'));
           $query = $this->db->get();
           if($query->num_rows()>0){
               $result = $query->result_array();
               return $result[0]['video_option'];
           } else {
               return false;
           }
          } else {
            return 'with_support';
          }
       }
   }

      public function get_view(){
       if($this->session->userdata('login_type') == 'teacher'){
           $this->db->select('view_count');
           $this->db->from('video_tutorial');
           $this->db->where('user_id',$this->session->userdata('teacher_id'));
           $this->db->where('user_type',$this->session->userdata('login_type'));
           $query = $this->db->get();
           if($query->num_rows()>0){
               $result = $query->result_array();
               return $result[0]['view_count'];
           } else {
               return false;
           }
       } else {
        $this->db->select('view_count');
           $this->db->from('video_tutorial');
           $this->db->where('user_id',$this->session->userdata('observer_id'));
           $this->db->where('user_type',$this->session->userdata('login_type'));
           $query = $this->db->get();
           if($query->num_rows()>0){
              $result = $query->result_array();
              if($this->session->userdata('popupcntr')!=1){ 
                $this->db->where('user_id',$this->session->userdata('observer_id'));
                $this->db->where('user_type',$this->session->userdata('login_type'));
                $this->db->update('video_tutorial',array('view_count'=>$result[0]['view_count']+1));
              }
               return $result[0]['view_count'];
           } else {
               return false;
           }
       }
   }   

}
?>