<?php
class Importdatagraphmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
  
 	function getallschools()
   {
	 	$qry= "select Distinct school_name from archive_data ";
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
	function getassessment($school_id)
   {
   		if($school_id == '0')
		$where = '';
		else
		$where = "where school_name ='".$school_id."'";
	 	$qry= "select distinct assessment_name from archive_data ".$where;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
 	}
	
   function getTestItem($school_id,$fDate,$tDate,$ass_id)
   {
		if($ass_id == '0')
		$andAss = '';
		else
		$andAss  = " AND assessment_name='".$ass_id."'";
		if($school_id == '0')
		 $andschool  = "";
		 else
		 $andschool = " and school_name='".$school_id."'";
	 
	 	$qry= "select Distinct cluster_item from archive_data where DATE(date) between '".$fDate."' And '".$tDate."'".$andAss."".$andschool; 
		$query = $this->db->query($qry);
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
			$IntCount=0;
			foreach($result as $key => $value)
			{
				$per = $this->getPercentageAllByTestCluster($school_id,$value['cluster_item'],$fDate,$tDate,$ass_id);
				$arrResult[$IntCount]['per'] =$per[0]['marks'];
				$arrResult[$IntCount]['cluster_item'] = $value['cluster_item'];
				$IntCount++;  
			}
 			return $arrResult;
		}
		else
		{
			return false;
		}
 	}
 
  	
	
   function getgrades($school_name)
   {	
  		if($school_name == '0')
		 $andschool  = "";
		 else
		 $andschool = " where school_name='".$school_name."'";
		 
		 $qry= "select Distinct grade from archive_data".$andschool;
	 	 $query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}  
   }
   
	function getPercentageAllByTestCluster($school_id,$cluster_item,$fDate,$tDate,$ass_id)
    {
 		$andDate = " AND DATE(date) between '".$fDate."' ANd '".$tDate."' ";
		$andAss = '';$andschool  = "";
		
		/*$data['records'] = $this->getgrades($school_id);
		foreach($data['records'] as $key => $value)
		{
			$str .= $value['grade'].',';
			
		}
		$grade_str = substr($str,0,-1);
	*/
 		if($ass_id == '0')
		$andAss = '';
		else
		$andAss  = " AND assessment_name='".$ass_id."'";
		if($school_id == '0')
		 $andschool  = "";
		 else
		 $andschool = " and school_name='".$school_id."'";
  
  	$qry= "select Avg(marks) as marks from archive_data where cluster_item = '".$cluster_item."'".$andDate."".$andAss."".$andschool; 
 	
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();
	/*		$totgradePer = 0;$per = 0; $noGrade =0;
			foreach($query->result_array() as $value)
			{
				$totgradePer = array_sum($value);
				$noGrade = count($value);
				if($noGrade != 0)	
				$per = $totgradePer/$noGrade;
				 $per= number_format($per,2,'.','');
				 return $per;
			}
	*/
			 
	 	}else{
			return false;
		}  
	 }
	
 } // class end

?>