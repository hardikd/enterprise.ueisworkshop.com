<?php
class Trainingmaterialmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function addtraingmaterial($title,$desc,$filename,$doctype)
	{
		$dist_id = $this->session->userdata('district_id');
		$dist_user_id = $this->session->userdata('dist_user_id');		
		
		$data = array(
		'title' => $title ,
		'description' => $desc ,
		'filename' => $filename,
		'datetime' => date('Y/m/d H:i:s'),
		'district_user_id' => $dist_user_id ,
		'district_id' => $dist_id ,
		'doctyp' => $doctype
		);
		
		return $this->db->insert('trainingmaterial', $data); 
	}
	
	 public function record_count() {
		 $district_id=$this->session->userdata('district_id');
		 
       $qry="SELECT * FROM trainingmaterial where district_id='".$district_id."'";
		$query = $this->db->query($qry);
		return $query->num_rows();
		
    }
	
	function gettrainingmaterial($limit, $start)
	{
		$district_id=$this->session->userdata('district_id');
		$query = $this->db->query("select * from trainingmaterial where district_id='".$district_id."' limit ".$start." , ".$limit." ");
		//$query = $this->db->get('documents');
		return $query->result_array();
	}
	
	function deletetrainingmaterialdoc($docid)
	{
		return $this->db->query("delete from trainingmaterial where doc_id='".$docid."'");
	}
	
}
?>