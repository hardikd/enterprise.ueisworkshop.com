<?php
class Proficiencymodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getprodetail()
	{
		$query = $this->db->get('proficiency');
		return $query->result_array();
	}
	function setproficiency($belowbasicfrom,$belowbasicto,$basicfrom,$basicto,$proficientfrom,$proficientto,$proficient_id,$assessmentid)
	{
	$data = array(
   'bbasicfrom' => $belowbasicfrom ,
   'bbasicto' => $belowbasicto ,
    'basicfrom' => $basicfrom ,
   'basicto' => $basicto ,
    'pro_from' => $proficientfrom ,
   'pro_to' => $proficientto,
   'assessment_id' => $assessmentid
	);
	$this->db->insert('proficiency', $data); 
		
	}
	
	function getprof()
	{
		$query = $this->db->get('proficiency');
		return $query->result_array();
	}
	
	function getassessments()
	{
		$district_id = $this->session->userdata('district_id');
		$query=$this->db->query('select * from assignments where district_id="'.$district_id.'"');
		return $query->result_array();
	}
   
	function getdata($testname)
	{
	
	$qry="Select * from proficiency where assessment_id='".$testname."' ";
	$query = $this->db->query($qry);
		if($query->num_rows()>0){
				return $query->result_array();		
		
		}else{
			return false;
		}		
		
	}
	
	function updateproficiency($belowbasicfrom,$belowbasicto,$basicfrom,$basicto,$proficientfrom,$proficientto,$assessment_id)
	{
		$data = array(
   'bbasicfrom' => $belowbasicfrom ,
   'bbasicto' => $belowbasicto ,
    'basicfrom' => $basicfrom ,
   'basicto' => $basicto ,
    'pro_from' => $proficientfrom ,
   'pro_to' => $proficientto
	);
	$this->db->where('assessment_id', $assessment_id);
	$this->db->update('proficiency', $data);
	}
}
?>