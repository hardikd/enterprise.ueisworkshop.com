<?php
class Rubricscalesubmodel extends Model
{
	function __construct()
	{
		parent::__construct();

	}
	
	function getrubricscalesubCount($state_id,$country_id,$group_id,$district_id)
	{
	    if($group_id=='all')
		{

		$qry="Select count(*) as count from rubricscale_sub op,rubricscale og,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=op.district_id and  op.scale_id=og.scale_id and og.district_id=$district_id and  op.district_id=$district_id " ;
		
		}
		else
		{
			$qry="Select count(*) as count from rubricscale_sub op,rubricscale og ,districts d where d.state_id=$state_id and d.country_id=$country_id  and d.district_id=op.district_id and op.scale_id=og.scale_id and og.district_id=$district_id and  op.scale_id=$group_id  and op.district_id=$district_id " ;	
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			$row = $query->row();
			return $row->count;			
		}else{
			return FALSE;
		}
	}
	
	function getrubricscalesubs($page,$per_page,$state_id,$country_id,$group_id,$district_id)
	{
		
		if($page!='all')
		{
		
		$page -= 1;
		$start = $page * $per_page;
		$limit=" limit $start, $per_page ";
		}
		else
		{
			$limit='';
		
		}
		if($group_id=='all')
		{
			$qry="Select s.name,d.districts_name,rs.sub_scale_id sub_scale_id,rs.sub_scale_name as sub_scale_name,r.scale_name,r.scale_id from  rubricscale r,rubricscale_sub rs,districts d ,states s where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and rs.district_id=d.district_id and rs.scale_id=r.scale_id and r.district_id=$district_id and  rs.district_id=$district_id    $limit  ";
			
		}
		else
		{
			$qry="Select s.name,d.districts_name,rs.sub_scale_id sub_scale_id,rs.sub_scale_name as sub_scale_name,r.scale_name,r.scale_id from  rubricscale r,rubricscale_sub rs,districts d ,states s where s.state_id=d.state_id and d.state_id=$state_id and d.country_id=$country_id and rs.district_id=d.district_id and rs.scale_id=r.scale_id and r.district_id=$district_id and  rs.district_id=$district_id  and rs.scale_id=$group_id  $limit  ";
		
		}
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
	
	
	}
	
	function getrubricscalesubById($point_id)
	{
		
		$qry = "Select rs.sub_scale_id,rs.scale_id,rs.sub_scale_name,rs.district_id,d.country_id,d.state_id from rubricscale_sub rs,districts d where rs.district_id=d.district_id and rs.scale_id=".$point_id;
		
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	function getrubricscalesubBysubId($point_id)
	{
		
		$qry = "select rs.sub_scale_id,rs.scale_id,rs.sub_scale_name,rs.district_id,d.country_id,d.state_id from rubricscale_sub rs,districts d where rs.district_id=d.district_id and rs.sub_scale_id=".$point_id;
		$query = $this->db->query($qry);
		if($query->num_rows()>0){
			return $query->result_array();		
		}else{
			return false;
		}
		
	}
	
	function addpoints()
	{
		$status=0;
		$pdata=$this->input->post('pdata');
	  
	   if(!empty($pdata))
	   {
	     
		 if(isset($pdata['point_id']) && isset($pdata['start']) && isset($pdata['end']) )
		 {
		    foreach($pdata['point_id'] as $val)
			{
			  $data=$this->getrubricscalesubBysubId($val);
			 if($data!=false)
			 {
			 foreach($data as $val)
					  {
			  
			  if($val['scale_id']!=$pdata['scale_id'])
			  {
					$status=1;	
				   
				   $sdata = array('sub_scale_name' => $val['sub_scale_name'],
					'scale_id' => $pdata['scale_id'],
					'district_id' => $pdata['end']	
					);
					$str = $this->db->insert_string('rubricscale_sub', $sdata);
					$this->db->query($str);
					
			
			  
			  }
			  }
			  }
			
			}
		 }
		 
		} 
	
	
	  return $status;
	
	}
	
	function add_rubricscalesub()
	{
	     $scale_id=$this->input->post('scale_id');
		 
	   	
			if($scale_id)
			{
				   $c=$this->input->post('counterscale');
				   
				   
				   for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!='' )
					 {
					 $sdata=array('scale_id' =>$scale_id,
					 'sub_scale_name'=>$this->input->post('textbox'.$i),
					 'district_id'=>$this->input->post('district_id')
					 );
					
					 $sstr = $this->db->insert_string('rubricscale_sub', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   $last_id=$this->db->insert_id();
				   
				   
				   return $last_id;
				
			}
			else{
				return 0;
			}
		
	}
	function update_rubricscalesub()
	{
			$scale_id=$this->input->post('scale_id');
			$actual_scale_id=$this->input->post('actual_scale_id');
			$actual_district_id=$this->input->post('actual_district_id');
			$district_id=$this->input->post('district_id');
		if($scale_id==$actual_scale_id && $district_id==$actual_district_id)
			{
			$dqry = "delete from rubricscale_sub where scale_id=$scale_id and district_id=$district_id";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('counterscale');
		for($i=1;$i<=$c;$i++)
				   {
				     if($this->input->post('textbox'.$i)!=''  && $this->input->post('textboxup'.$i)!='' )
					 {
					    $sub_scale_id=$this->input->post('textboxup'.$i);
						
						$sdata=array('sub_scale_name'=>$this->input->post('textbox'.$i),
						             'scale_id'=>$scale_id,
									 'sub_scale_id'=>$sub_scale_id,
									 'district_id'=>$district_id
						
						);
                   					 
					   //$where = "point_id=$point_id and sub_group_id=$sub_group_id";		
		
		
			                $str = $this->db->insert_string('rubricscale_sub', $sdata);
					          $this->db->query($str);
					 
					 }
					  else if($this->input->post('textbox'.$i)!='' )
					 {
					 $sdata=array('scale_id' =>$scale_id,
					 'sub_scale_name'=>$this->input->post('textbox'.$i),
					 'district_id'=>$district_id				   
					 );
					 
					 $sstr = $this->db->insert_string('rubricscale_sub', $sdata);
					 $this->db->query($sstr);
					
					 }
				   }
				   
				   return true;	
		}
		else if($scale_id!=$actual_scale_id)
		{
			$dqry = "delete from rubricscale_sub where scale_id=$actual_scale_id ";
		           $dquery = $this->db->query($dqry);
				   $c=$this->input->post('counterscale');
		for($i=1;$i<=$c;$i++)
				   {
				     
					 $name=$this->input->post('textbox'.$i);
					 if($name!='0'){
					 $sdata=array('scale_id' =>$scale_id,
					 'sub_scale_name'=>$this->input->post('textbox'.$i),
					 'district_id'=>$district_id				   
					 );
					 
					 $sstr = $this->db->insert_string('rubricscale_sub', $sdata);
					 $this->db->query($sstr);
					}
					
				   }
				   
				   return true;	
		
		
		}
		else
		{
		
			return false;
		}
		
	
	
	
	}
	
	function deleterubricscalesub($ob_id)
	{
		$qry = "delete from rubricscale_sub where sub_scale_id=$ob_id";
		$query = $this->db->query($qry);
		
		if(mysql_error()==""){
			return true;
		}else{
			return mysql_error();
		}
	}
	function savepoint($report_id,$scale_id,$strengths,$concerns,$score)
	{
	
	 $data = array('report_id' => $report_id,
					'group_id'=> $scale_id,
					'strengths'=> $strengths,
					'concerns'=> $concerns,
					'score'=> $score
		
		
		);
	
	   
	   $str=$this->db->insert_string('rubrics', $data);
	
	  $this->db->query($str);
	
	
	
	
	}
	
	function savesubgroup($report_id,$scale_id,$sub_scale_id,$strengths,$concerns,$score)
	{
	
	 $data = array('report_id' => $report_id,
					'group_id'=> $scale_id,
					'point_id'=> $sub_scale_id,
					'strengths'=> $strengths,
					'concerns'=> $concerns,
					'score'=> $score
		
		
		);
	
	    $str=$this->db->insert_string('rubrics', $data);
	
	  $this->db->query($str);
	
	
	
	}
	
	
}