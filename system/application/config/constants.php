<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 					'ab');
define('FOPEN_READ_WRITE_CREATE', 				'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 			'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
define('SITEURLM',		'http://district.ueisworkshop.com/');
define('REDIRECTURL',		'http://district.ueisworkshop.com/index.php/');
define('LOADERURL','http://district.ueisworkshop.com/images/loading11.gif');
} else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
define('SITEURLM',		'http://enterprise.ueisworkshop.com/');
define('REDIRECTURL',		'http://enterprise.ueisworkshop.com/index.php/');
define('LOADERURL','http://enterprise.ueisworkshop.com/images/loading11.gif');
}
else if($_SERVER["HTTP_HOST"]=="localhost"){
define('SITEURLM',		'http://localhost/demo/');
define('REDIRECTURL',		'http://localhost/enterprise/index.php/');
define('LOADERURL','http://localhost/enterprise/images/loading11.gif');
}

define('MEDIA_PATH',		'system/application/views/');
define('BANK_TIME',		'');



if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
define('WORKSHOP_FILES',		'/var/www/html/workshop_files/');
define('WORKSHOP_DISPLAY_FILES',		'http://enterprise.ueisworkshop.com/workshop_files/');
} else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
define('WORKSHOP_FILES',		'/var/www/html/workshop_files/');
define('WORKSHOP_DISPLAY_FILES',		'http://district.ueisworkshop.com/workshop_files/');
}
else if($_SERVER["HTTP_HOST"]=="localhost"){
define('WORKSHOP_FILES',		'/Library/WebServer/Documents/enterprise/workshop_files/');
define('WORKSHOP_DISPLAY_FILES',		'http://localhost/enterprise/');
}

/* End of file constants.php */
/* Location: ./system/application/config/constants.php */