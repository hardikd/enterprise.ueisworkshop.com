<?php
class MY_Auth extends Controller
{
	function __construct()
	{
		parent::Controller();
		//error_reporting(0);

		//Check if the user logged in
		/*if(trim($this->session->userdata("login_email"))==""){
			//Redirect to the login page
			redirect("login");
		}*/
			
	}
	
	function is_admin(){
		if(trim($this->session->userdata("login_type"))=="admin"){
			return true;
		}else{
			return false;
		}
	}
	
	function is_user(){
		if(trim($this->session->userdata("login_type"))=="user"){
			return true;
		}else{
			return false;
		}
	}
	
	function is_teacher(){
		if(trim($this->session->userdata("login_type"))=="teacher"){
			return true;
		}else{
			return false;
		}
	}
	
	function is_observer(){
		if(trim($this->session->userdata("login_type"))=="observer"){
			return true;
		}else{
			return false;
		}
	}
	function is_parent(){
		if(trim($this->session->userdata("login_type"))=="parent"){
			return true;
		}else{
			return false;
		}
	}

}