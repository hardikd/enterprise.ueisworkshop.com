<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Muhammad Surya Ikhsanudin 
 *  License    : Protected 
 *  Email      : mutofiyah@gmail.com 
 *   
 *  Dilarang merubah, mengganti dan mendistribusikan 
 *  ulang tanpa sepengetahuan Author 
 *  ======================================= 
 */  
include_once $_SERVER['DOCUMENT_ROOT']."/PHPExcel/Classes/PHPExcel.php"; 
class Excel extends PHPExcel { 
    public function __construct() { 
        parent::__construct(); 
    } 
}