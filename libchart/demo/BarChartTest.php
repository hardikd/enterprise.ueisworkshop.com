<?php
	/* Libchart - PHP chart library
	 * Copyright (C) 2005-2011 Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
	 * 
	 * This program is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
	 * 
	 */
	
	/*
	 * Vertical bar chart demonstration
	 *
	 */

	include "C:/wamp/www/WAPtrunk/libchart/classes/libchart.php";

	$chart = new VerticalBarChart();
		//uncomment below code for color change
		
		/*$chart->getPlot()->getPalette()->setBarColor(array(
		new Color(255, 0, 0),
		));*/
	
	// Here Define subject Name and values
	$dataSet = new XYDataSet();
	$dataSet->addPoint(new Point("Math", 20));
	$dataSet->addPoint(new Point("English", 90));
	$dataSet->addPoint(new Point("Science", 40));
	$dataSet->addPoint(new Point("Hindi ", 80));
	
	$chart->setDataSet($dataSet);
	
	//End here subject name
	
	$chart->setTitle("ANkit chart");
	$chart->render("generated/demo1.png");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Libchart vertical bars demonstration</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
</head>
<body>
	<img alt="Vertical bars chart" src="generated/demo1.png" style="border: 1px solid gray;"/>
</body>
</html>
