<?php if(!isset($RUN)) { exit(); } ?>
<!DOCTYPE html>
<html lang="en" class="login_page">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $PAGE_TITLE ?></title>
    
        <!-- Bootstrap framework -->
                    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.min.css" />
   
    
        <!-- tooltips-->
            <link rel="stylesheet" href="lib2/qtip2/jquery.qtip.min.css" />

        <!-- main styles -->
            <link rel="stylesheet" href="css/style.css" />
			

	
        <!-- Favicon -->
            <link rel="shortcut icon" href="favicon.ico" />
		

        <!--[if lte IE 8]>
            <script src="js/ie/html5.js"></script>
			<script src="js/ie/respond.min.js"></script>
        <![endif]-->
<script language="JavaScript" type="text/javascript" >

function selectdistrictsgrd(str)
{
		
	$.ajax({
		url:'modules/add_edit_user_ajax.php?id='+str,
		success:function(result)
		{
			$('#selectgrade').html(result);
			
		}
		}); 
		$.ajax({
		url:'modules/add_edit_user_ajax.php?s_id='+str,
		success:function(result)
		{
			
			$('#selectschool').html(result);
		}
		}); 

}
</script>		
    </head>
    <body>
        
<script language="JavaScript" type="text/javascript" src="lib/validations.js"></script>
<?php echo $val->DrowJsArrays(); ?>

<script language="javascript">

function checkform()
{   
	document.form1.btnRegister.disabled=true;         
	var user_name= document.getElementById('txtLogin').value;
	var email= document.getElementById('txtEmail').value;
       // alert(user_name);
        var status=validate();
        if(status)
        {
             $.post("register.php", { login_to_check: user_name,email: email, ajax: "yes" },
             function(data){
                 if(data=="0")
                 {
                     document.forms["form1"].submit();
                 }
                 else
                 {
                    alert(data);
                    document.form1.btnRegister.disabled=false;
                 }

            });
        }
        else
        {
            document.form1.btnRegister.disabled=false;
        } 
}

</script>

		<div class="login_box">
			
			
			<form method="post" name="form1" id="form1">
                            <div style="display:<?php echo $step1_display ?>">
				<div class="top_b"><?php echo SIGN_UP_TO ?> <?php echo $SYSTEM_NAME ?></div>
				<div class="alert alert-login">
					<?php echo T_ACCEPT ?> <a data-toggle="modal" href="#terms"><?php echo T_AND_S ?></a>.
				</div>
				<div id="terms" class="modal hide fade" style="display:none">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h3><?php echo T_AND_C ?></h3>
					</div>
					<div class="modal-body">
						<p>
							<?php echo T_AND_C_TEXT ?>
						</p>
					</div>
					<div class="modal-footer">
						<a data-dismiss="modal" class="btn" href="#"><?php echo CLOSE ?></a>
					</div>
				</div>
				<div class="cnt_b">
					
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input maxlength=25 type=text name=txtName title="<?php echo R_NAME ?>" class="ttip_r" id=txtName placeholder="<?php echo R_NAME ?>"  />
						</div>
					</div>
                                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input maxlength=25 type=text name=txtSurname id=txtSurname placeholder="<?php echo R_SURNAME ?>" title="<?php echo R_SURNAME ?>" class="ttip_r" />
						</div>
					</div>
                      
                      
                      
                      
                      
                      
                        <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span>                                                         
                                                        <select id="selectdistricts" name="selectdistricts" onchange="selectdistrictsgrd(this.value);"><option><?php echo R_DEFAULT_DISTRICT ?></option><?Php
				foreach($Districts_rows as $key =>$val)
				{
				?>
		<option value="<?Php echo @$Districts_rows[$key]['district_id'];?>" <?php if(@$district_id==@$Districts_rows[$key]['district_id']){ ?> <?Php } ?>> <?Php echo @$Districts_rows[$key]['districts_name'];?> </option>
				<?Php				
				}
				?>
                                                            <?php //echo $country_options ?>
                                                        </select>        
						</div>
					</div>
                     
                     
                     
                     
                     
                     
                       <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span>                                                         
                                                        <select id="selectgrade" name="selectgrade"><option <?php if(!empty($grade_id)){ ?> value = "<?Php echo $grade_id; ?>" <?Php } else { ?> "" <?Php } ?>> 

			<?php if(!empty($grade_name)){ echo $grade_name?> <?Php } else { ?> Select Grade <?Php } ?> 
	
		</option>
                                                            <?php //echo $country_options ?>
                                                        </select>        
						</div>
					</div>
                    
                    
                    
                    
                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input maxlength=25 type=text name="studentnumebr" id="studentnumebr" placeholder="<?php echo R_ROLLNUMBER ?>" title="<?php echo R_ROLLNUMBER ?>" class="ttip_r" />
						</div>
					</div> 
                    
                    
                    
                    
                    
                      <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span>                                                         
                                                        <select id="selectschool" name="selectschool"><option <?php if(!empty($school_id)){ ?> value = "<?Php echo $school_id; ?>" <?Php } else { ?> "" <?Php } ?>> Select School </option>
                                                            <?php //echo $country_options ?>
                                                        </select>        
						</div>
					</div>
                    
                    
                    
                    
                                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input maxlength=20 type=password name=txtLogin id=txtLogin placeholder="<?php echo R_LOGIN ?>" title="<?php echo R_LOGIN ?>" class="ttip_r" />
						</div>
					</div>
                                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span><input type="text" maxlength=20 name=txtPass id=txtPass placeholder="<?php echo R_PASSWORD ?>" title="<?php echo R_PASSWORD ?>" class="ttip_r" />
						</div>
					</div>
                                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span>                                                         
                                                        <select id="drpCountries" name="drpCountries">
                                                            <?php echo $country_options ?>
                                                        </select>        
						</div>
					</div>
                                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input maxlength=25 type=text name=txtAddr  placeholder="<?php echo R_ADDRESS ?>" title="<?php echo R_ADDRESS ?>" class="ttip_r"  />
						</div>
					</div>
                                       <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input maxlength=25 type=text name=txtPhone  placeholder="<?php echo R_PHONE ?>"  title="<?php echo R_PHONE ?>" class="ttip_r" />
						</div>
					</div>
					
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on">@</span><input type="text" id="txtEmail" name="txtEmail" placeholder="<?php echo EMAIL ?>"  title="<?php echo EMAIL ?>" class="ttip_r" />
						</div>
						<small><?php echo EMAIL_TEXT ?></small>
					</div>
					 
				</div>
				<div class="btm_b tac">
					<button class="btn btn-inverse" type="button" onclick="checkform()" id="btnRegister" name="btnRegister"><?php echo SIGN_IN ?></button>
                                        <!--<button class="btn btn-inverse" type="button" onclick="javascript:window.location.href='login.php'" id="btnRegister" name="btnRegister"><?php //echo CANCEL ?></button>-->
                                        
<button class="btn btn-inverse" type="button" onclick="document.location.href='<?php echo SITEURLM ?>'" id="btnRegister" name="btnRegister"><?php echo CANCEL ?></button>                                        
                                        
                                        
				</div>  
                                </div>
			</form>
			
			<div class="links_b links_btm clearfix">
				
				

				<span>
               <a href="<?php echo SITEURLM;?>"><?php echo R_LOGIN_PAGE ?></a>
                </span>	

			</div>
                    
                     
                      <div style="display:<?php echo $step2_display ?>">
                            <div class="top_b"><?php echo SIGN_UP_TO ?> <?php echo $SYSTEM_NAME ?></div>
				<div class="alert alert-login">
					<?php echo $msg ?>
				</div>
                            </div> <br>
                    
		</div>

                      
		
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.actual.min.js"></script>
        <script src="lib/validation/jquery.validate.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                
				//* boxes animation
				form_wrapper = $('.login_box');
				function boxHeight() {
					form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
				};
				form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
                $('.linkform a,.link_reg a').on('click',function(e){
					var target	= $(this).attr('href'),
						target_height = $(target).actual('height');
					$(form_wrapper).css({
						'height'		: form_wrapper.height()
					});	
					$(form_wrapper.find('form:visible')).fadeOut(400,function(){
						form_wrapper.stop().animate({
                            height	 : target_height,
							marginTop: ( - (target_height/2) - 24)
                        },500,function(){
                            $(target).fadeIn(400);
                            $('.links_btm .linkform').toggle();
							$(form_wrapper).css({
								'height'		: ''
							});	
                        });
					});
					e.preventDefault();
				});
				
				//* validation
				$('#login_form').validate({
					onkeyup: false,
					errorClass: 'error',
					validClass: 'valid',
					rules: {
						username: { required: true, minlength: 3 },
						password: { required: true, minlength: 3 }
					},
					highlight: function(element) {
						$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
				});
            });
        </script>

                 <script src="js/jquery.min.js"></script>
		
			<script src="bootstrap/js/bootstrap.min.js"></script>
			<!-- bootstrap plugins -->
			<script src="js/bootstrap.plugins.min.js"></script>
			<!-- tooltips -->
			<script src="lib2/qtip2/jquery.qtip.min.js"></script>                        
	
			<script src="lib2/UItoTop/jquery.ui.totop.min.js"></script>
		
			<script src="js/gebo_common.js"></script>
			
			<script src="lib2/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
         
    </body>
</html>
