<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("1");

    require "db/questions_db.php";

    $quiz_id = util::GetKeyID("quiz_id", "?module=questions");

    $txtQsts="";
    $answers_count=4;
	 $answers_count_sec=4;
    $selected = "-1";    
	
	$district_id = $_SESSION['district_id'];
    if(isset($_GET["id"]))
    {
		//echo $_GET["id"];
		
        $id = util::GetID("?module=questions");
		
        $rs_qsts=orm::Select("questions", array(), array("id"=>$id,"district_id"=>$district_id), "");

        if(db::num_rows($rs_qsts) == 0 ) header("location:?module=questions");

        $row_qsts=db::fetch($rs_qsts);
        $txtQsts = $row_qsts["question_text"];
        $txtPoint = $row_qsts["point"]; 
		 $testitemcluster = $row_qsts["test_cluster"];        
         $txtHeader = $row_qsts["header_text"];
		$have_score_ans = $row_qsts["have_score_ans"];
        $txtFooter = $row_qsts["footer_text"];
        $selected = $row_qsts["question_type_id"];
        $txtPenaltyPoint=$row_qsts["penalty_point"];

		  $txtPointSec = $row_qsts["secondary_point"];
		    $selectedSec = $row_qsts["sec_question_type_id"];
        $rs_grp=orm::Select("question_groups", array(), array("question_id"=>$id,"is_secondary_group"=>0), "added_date");
        $row_grp = db::fetch($rs_grp);
		//echo"<pre>"; "is_secondary_group"=>0
		//print_r($row_grp); exit;
         $txtGroupName=$row_grp["group_name"];
      $rs_ans = orm::Select("answers_quiz", array(), array("group_id"=>$row_grp["id"],"is_score_ans"=>0), "priority");

        $answers_count = db::num_rows($rs_ans);
		
	     $i = 0;
	//	 echo"<pre>";
	
        while($row_ans=db::fetch($rs_ans))
        {
            $i++;
            ${"txtChoise".$i} = $row_ans["answer_text"];
            ${"txtCrctAnswer".$i} = $row_ans["correct_answer_text"];
            ${"ans_selected".$i} = $row_ans["correct_answer"]=="1" ? "checked" : "";
            ${"txtDesc".$i} = $row_ans["answer_desc"];
		 ${"radio_ans".$i} = $row_ans["radio_ans"];
		 ${"score".$i} = $row_ans["score"];
		  ${"txtPriority".$i} = $row_ans["priority"];
		  ${"scv_ans".$i} = $row_ans["selfcorrectsvowel"];
			// ${"is_diff_radio".$i} = $row_ans["is_difficult"]=="1" ? "1" : "0"; //for diff selection
	//		print_r($row_ans); 
        }
//exit;
		 $rs_grp_sec=orm::Select("question_groups", array(), array("question_id"=>$id,"is_secondary_group"=>1), "added_date");
        $row_grp_sec = db::fetch($rs_grp_sec);
		 $txtGroupName_Sec=$row_grp_sec["group_name"];
	
        $rs_ans_sec = orm::Select("answers_quiz", array(), array("group_id"=>$row_grp_sec["id"],"is_score_ans"=>1), "priority");

        $answers_count_sec = db::num_rows($rs_ans_sec);
		if($answers_count_sec < 1)
		$answers_count_sec = 4;
		
		$i = 0;
		 while($row_ans_sec=db::fetch($rs_ans_sec))
        {
            $i++;
            ${"txtChoiseSec".$i} = $row_ans_sec["answer_text"];
            ${"txtCrctAnswerSec".$i} = $row_ans_sec["correct_answer_text"];
            ${"ans_selectedSec".$i} = $row_ans_sec["correct_answer"]=="1" ? "checked" : "";
            ${"txtDescSec".$i} = $row_ans_sec["answer_desc"];
    	}

    }
	else
	{
		//$have_score_ans = 0;
	}
    $results = orm::Select("question_types", array(), array() , "id");
    //$temp_options = webcontrols::GetOptions($results, "id", "question_type",$selected);
    $temp_options= webcontrols::BuildOptions($QUESTION_TYPE, $selected);
	$question_type_sec = $QUESTION_TYPE;
	unset($question_type_sec['5']);
	unset($question_type_sec['6']);
	$temp_options_sec= webcontrols::BuildOptions($question_type_sec, $selectedSec);


  //  $val = new validations("btnSave");
  //  $val->AddValidator("txtName", "empty", "Name cannot be empty","");


	$ScoringTool = 1;

    if(isset($_POST["btnSave"]))
    {
		
        $db = new db();
        $db->connect();
        $db->begin();        
        try
        {
            $question_type=$_POST["drpTemplate"];
            if(!isset($_GET["id"]))
            {
			 		 
                $query = orm::GetInsertQuery("questions", array("question_text"=>trim($_POST["txtQstsEd"]),
                                                   "question_type_id"=>$_POST["drpTemplate"],
                                                   "priority"=>"(select ifnull(max(priority)+1,1) from questions where quiz_id=$quiz_id)",
                                                   "quiz_id"=>$quiz_id,
                                                   "point"=>$_POST["txtPoint"],
                                                   "penalty_point"=>$_POST["txtPenaltyPoint"],
                                                   "parent_id"=>"0",
                                                   "footer_text"=>$_POST["txtFooter"],
                                                   "header_text"=>$_POST["txtHeader"],
												   	"test_cluster"=> trim($_POST["testitemcluster"]),
													"have_score_ans"=>$_POST["SecScoringTool"],
													"secondary_point" =>$_POST["txtPointSec"],
													 "sec_question_type_id"=>$_POST["drpTemplateSec"],
								                   	"district_id" => $district_id
                                                   ));
                $db->query($query);
                $question_id = $db->last_id();
                $db->query(questions_db::UpdatePriorityQuery($quiz_id, $question_id));
            }
            else
            {	
                 $query = orm::GetUpdateQuery("questions", array("question_text"=>trim($_POST["txtQstsEd"]),
                                                   "question_type_id"=>$_POST["drpTemplate"],
                                                   "point"=>$_POST["txtPoint"],
                                                    "penalty_point"=>$_POST["txtPenaltyPoint"],
                                                   "footer_text"=>$_POST["txtFooter"],
                                                   "header_text"=>$_POST["txtHeader"],
												  	"test_cluster" => trim($_POST["testitemcluster"]),
													"have_score_ans"=>$_POST["SecScoringTool"],
													"secondary_point" =>$_POST["txtPointSec"],
													 "sec_question_type_id"=>$_POST["drpTemplateSec"],
													 "district_id" => $district_id
                                                   ),
                                                   array("id"=>$id,"district_id"=>$district_id)
                                            );
                 
                $db->query($query);
                $question_id = $id;
                $db->query(questions_db::GetAnswerDeleteQuery($question_id));
                $db->query(questions_db::GetGroupDeleteQuery($question_id));
            }
            
      	// if($_POST["txtQstsEd"] !='') //if blank que
		// {	   
		   
		    if($question_type==0)
            {
                $group_name= trim($_POST["txtMultiGroupName"]);
            }
            else if($question_type==1)
            {
                $group_name= trim($_POST["txtOneGroupName"]);
            }
            else if($question_type==3)
            {
                $group_name= trim($_POST["txtAreaGroupName"]);
            }
            else if($question_type==4)
            {
                $group_name= trim($_POST["txtMultiTextGroupName"]);
            }
			else if($question_type==5)
            {
                $group_name= trim($_POST["txtMultiGroupName"]);
            }
			else if($question_type==6)
            {
                $group_name= trim($_POST["txtMultiRedioGroupName"]);
            }
            
	 
            $query= orm::GetInsertQuery("question_groups", array("group_name"=>$group_name,
                                                                 "show_header"=>$group_name =="" ? 0 : 1,
                                                                 "question_id"=>$question_id,
                                                                 "parent_id"=>"0"
                                                            ));

            $db->query($query);
            $group_id = $db->last_id();
		//	echo"<pre>";
		//		print_r($_POST);
   		if($_POST["txtQstsEd"] !='')
		{
              
            for($i=1;;$i++)
            {                
				$radio_ans = ''; $priority = '';
                if($question_type==0)
                {
                    if(!isset($_POST["txtMulti".$i])) break;

                    $answer_text = trim($_POST["txtMulti".$i]);
                    $answer_desc= trim($_POST["txtMultiDesc".$i]);
                    $correct_answer = isset($_POST["chkMulti".$i]) ==true ? 1 : 0;
                    $correct_answer_text="";
					$priority = $_POST["txtPriorityM".$i];
					
                }
                else if($question_type==1)
                {
                    if(!isset($_POST["txtOne".$i])) break;

                    $answer_text = trim($_POST["txtOne".$i]);
                    $answer_desc= trim($_POST["txtOneDesc".$i]);
                    $correct_answer = isset($_POST["rdOne"]) ==true && $_POST["rdOne"]==$i? 1 : 0;
                    $correct_answer_text="";
					$priority = $_POST["txtPriorityR".$i];
                }
                else if($question_type==3)
                {
                    if(!isset($_POST["txtArea".$i])) break;

                    $answer_text = "";
                    $correct_answer = 0;
                    $correct_answer_text=trim($_POST["txtArea".$i]);
                }
                else if($question_type==4)
                {
                    if(!isset($_POST["txtMultiText".$i])) break;

                    $answer_text = trim($_POST["txtMultiText".$i]);
                    $answer_desc= trim($_POST["txtMultiTextDesc".$i]);
                    $correct_answer = 0;
                    $correct_answer_text=trim($_POST["txtMultiCrctAnswer".$i]);
					$priority = $_POST["txtPriorityMT".$i];
                }
				
				else if($question_type==5)
                {
                    if(!isset($_POST["txtMulti".$i])) break;

                    $answer_text = trim($_POST["txtMulti".$i]);
                    $answer_desc= trim($_POST["txtMultiDesc".$i]);
                    $correct_answer = isset($_POST["chkMulti".$i]) ==true ? 1 : 0;
                    $correct_answer_text="";
						$priority = $_POST["txtPriorityM".$i];
					
                }
				else if($question_type==6)
				{
					break;
				}
				  $query=orm::GetInsertQuery("answers_quiz", array("group_id"=>$group_id,
                                                            "answer_text"=>$answer_text,
                                                            "correct_answer"=>$correct_answer ,
                                                            "correct_answer_text"=>$correct_answer_text ,
                                                            "priority"=>$priority,
                                                            "answer_pos"=>"1",
                                                            "parent_id"=>"0",
                                                            "control_type"=>"1",
                                                            "answer_parent_id"=>"0",
															"is_score_ans"=>"0",
                                                            "answer_desc"=>$answer_desc)
                                        );
                
                $db->query($query);
            }
			   $db->commit();
	
			if($question_type==6)
             {// echo"<pre>";
			 // print_r($_POST); // exit;
			 	 for($i=1;$i<10;$i++)
				{    
					$radio_ans = 0; $score = '';
					 $answer_text ='';$answer_desc='';$correct_answer='';$is_difficult='';
					 $priority = $_POST["txtPriority".$i]; $selfcorrectvowel = NULL;
						
						 if($_POST["chkRadio".$i] == '1')  // (isset($_POST["chkMultiR".$i]))||
						{  
							$answer_text = trim($_POST["txtMultiRadio".$i]);
							$answer_desc= trim($_POST["txtMultiRadioDesc".$i]);
							$correct_answer = isset($_POST["chkMultiR".$i]) ==true ? 1 : 0;
							$correct_answer_text="";
							if($i<=2)
							$selfcorrectvowel = isset($_POST["chkscv".$i]) == true?1 : 0;
						   
						}
						else if(isset($_POST["cRadio".$i]) || !isset($_POST["chkRadio".$i])) //|| $_POST["chkRadio".$i] != '1'))
						 { 
						 	$answer_text ='';$answer_desc='';$correct_answer=''; $is_difficult='';
							$answer_text = trim($_POST["txtMultiRadio".$i]);
							$answer_desc= trim($_POST["txtMultiRadioDesc".$i]);
							   //$correct_answer = isset($_POST["rdOne"]) ==true && $_POST["rdOne"]==$i? 1 : 0;
							if($_POST["cRadio".$i]==1 || $_POST["cRadio".$i]==$i)
								{$correct_answer = 1;} else{ $correct_answer=0;}
							if($_POST["cRadio".$i]==2)
								$radio_ans = 2;
								else $radio_ans = 1;
							$correct_answer_text="";
							$score = $_POST["txtscore".$i];
						 }
				 
				 		 if($answer_text!='')
						{ 		
							$query=orm::GetInsertQuery("answers_quiz", array("group_id"=>$group_id,
                                                            "answer_text"=>$answer_text,
                                                            "correct_answer"=>$correct_answer ,
                                                            "correct_answer_text"=>$correct_answer_text ,
                                                        	"priority"=>$priority,
                                                            "answer_pos"=>"1",
                                                            "parent_id"=>"0",
                                                            "control_type"=>"1",
                                                            "answer_parent_id"=>"0",
															"is_score_ans"=>"0",
															"radio_ans"=>$radio_ans,
															"score"=>$score,
	                                                         "answer_desc"=>$answer_desc,
															 "selfcorrectsvowel"=>$selfcorrectvowel)
                                        );
						   $db->query($query);		 //	"is_difficult"=>$is_difficult, "priority"=>"(select isnull(max(priority)+1,1) from answers_quiz where group_id=$group_id)",					   
				      	}
						else
						{
						 continue;
						 }
		 	 		} // end for
					// echo $is_difficult; 
						 
		      }
		 }
	        $db->commit();  
  //   exit;        
			if($_POST["SecScoringTool"] == 1)    //  for secondary question
			{
				  
				$question_type_sec=$_POST["drpTemplateSec"];
				$group_name ='';
				
				 if($question_type_sec==0)
				{
					$group_name= trim($_POST["txtMultiGroupNameSec"]);
				}
				else if($question_type_sec==1)
				{
					$group_name= trim($_POST["txtOneGroupNameSec"]);
				}
				else if($question_type_sec==3)
				{
					$group_name= trim($_POST["txtAreaGroupNameSec"]);
				}
				else 
				{
					$group_name= trim($_POST["txtMultiTextGroupNameSec"]);
				}
				$query= orm::GetInsertQuery("question_groups", array("group_name"=>$group_name,
																	 "show_header"=>$group_name =="" ? 0 : 1,
																	 "question_id"=>$question_id,
																	 "parent_id"=>"0",
																	 "is_secondary_group"=>1
																));
	
				$db->query($query);
				$group_id_sec = $db->last_id();
			    $db->commit();
					
				for($i=1;;$i++)
				{ 
					if($question_type_sec==0)
					{
						if(!isset($_POST["txtMultiSec".$i])) break;
	
						$answer_text = trim($_POST["txtMultiSec".$i]);
						$answer_desc= trim($_POST["txtMultiDescSec".$i]);
						$correct_answer = isset($_POST["chkMultiSec".$i]) ==true ? 1 : 0;
						$correct_answer_text="";
						
				   }
					else if($question_type_sec==1)
					{
						if(!isset($_POST["txtOneSec".$i])) break;
		
						$answer_text = trim($_POST["txtOneSec".$i]);
						$answer_desc= trim($_POST["txtOneDescSec".$i]);
						$correct_answer = isset($_POST["rdOneSec"]) ==true && $_POST["rdOneSec"]==$i? 1 : 0;
						$correct_answer_text="";
					}
					else if($question_type_sec==3)
					{
						if(!isset($_POST["txtAreaSec".$i])) break;
		
						$answer_text = "";
						$correct_answer = 0;
						$correct_answer_text=trim($_POST["txtAreaSec".$i]);
					}
					else 
					{
						if(!isset($_POST["txtMultiTextSec".$i])) break;
		
						$answer_text = trim($_POST["txtMultiTextSec".$i]);
						$answer_desc= trim($_POST["txtMultiTextDescSec".$i]);
						$correct_answer = 0;
						$correct_answer_text=trim($_POST["txtMultiCrctAnswerSec".$i]);
					}
			

					$query=orm::GetInsertQuery("answers_quiz", array("group_id"=>$group_id_sec,
																"answer_text"=>$answer_text,
																"correct_answer"=>$correct_answer ,
																"correct_answer_text"=>$correct_answer_text ,
																"priority"=>"(select isnull(max(priority)+1,1) from answers_quiz where group_id=$group_id)",
																"answer_pos"=>"1",
																"parent_id"=>"0",
																"control_type"=>"1",
																"answer_parent_id"=>"0",
																"answer_desc"=>$answer_desc,
																"is_score_ans"=>"1")
											);
					
					$db->query($query);
			
					}
				}	
            	  $db->commit();
		//}	//end if blank que
			  
            if(ISSET($_GET['qstbank'])) header("location:?module=questions_bank");
            else header("location:?module=questions&quiz_id=$quiz_id");
        }
        catch(Exception $e)
        {
            $db->rollback();
        }

        $db->close_connection();
		

    }
    
    include_once "ckeditor/ckeditor.php";
    $CKEditor = new CKEditor();
    $CKEditor->config['filebrowserBrowseUrl']='ckeditor/kcfinder/browse.php?type=files';
    $CKEditor->config['filebrowserImageBrowseUrl']='ckeditor/kcfinder/browse.php?type=images';
    $CKEditor->config['filebrowserFlashBrowseUrl']='ckeditor/kcfinder/browse.php?type=flash';
    $CKEditor->basePath = 'ckeditor/';

   function desc_func()
   {
        return ADD_EDIT_QUESTION;
   }

?>
