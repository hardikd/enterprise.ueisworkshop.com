<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("1");
require "db/asg_db.php";
$asg_id = $_REQUEST['asg_id'];

	$que_array = array();
   $que_array =  asgDB::GetAssessmentQuestion($asg_id);

	$stud_array = array();
   $stud_array =  asgDB::GetAssessmentStudent($asg_id);
  
 	error_reporting(E_ALL);
  	require_once 'Classes/PHPExcel.php';

	$objPHPExcel = new PHPExcel();
/*	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");
*/
  
	
	$cntStudent = count($stud_array);
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setCellValue('A2', "ID")
									 ->setCellValue('B2', "Student Name");
								  
	for($i=0; $i<$cntStudent;$i++)
	{
		$j=$i+3;
		$a ="A".$j;
		$b = "B".$j;
		$objPHPExcel->getActiveSheet()->setCellValue($a,$stud_array[$i]['UserID']);
		$name = '';
		$name = $stud_array[$i]['Name']." ".$stud_array[$i]['Surname'];
		$objPHPExcel->getActiveSheet()->setCellValue($b,$name);
	}
	
	$cntQuestion = count($que_array);
	$columnfieldArray = array(0=>"C",1=>"D",2=>"E",3=>"F",4=>"G",5=>"H",6=>"I",7=>"J",8=>"K",9=>"L",10=>"M",
11=>"N",12=>"O",13=>"P",14=>"Q",15=>"R",16=>"S",17=>"T",18=>"U",19=>"V",20=>"W",
21=>"X",22=>"Y",23=>"Z",24=>"AA",25=>"AB",26=>"AC",27=>"AD",28=>"AE",29=>"AF",30=>"AG",
31=>"AH",32=>"AI",33=>"AJ",34=>"AK",35=>"AL",36=>"AM",37=>"AN",38=>"AO",39=>"AP",40=>"AQ",
41=>"AR",42=>"AS",43=>"AT",44=>"AU",45=>"AV",46=>"AW",47=>"AX",48=>"AY",49=>"AZ",50=>"BA",
51=>"BB",52=>"BC",53=>"BD",54=>"BE",55=>"BF",56=>"BG",57=>"BH",58=>"BI",59=>"BJ",60=>"BK",
61=>"BL",62=>"BM",63=>"BN",64=>"BO",65=>"BP",66=>"BQ",67=>"BR",68=>"BS",69=>"BT",70=>"BU",
71=>"BV",72=>"BW",73=>"BX",74=>"BY",75=>"BZ",76=>"CA",77=>"CB",78=>"CC",79=>"CD",80=>"CE",
81=>"CF",82=>"CG",83=>"CH",84=>"CI",85=>"CJ",86=>"CK",87=>"CL",88=>"CM",89=>"CN",90=>"CO",
91=>"CP",92=>"CQ",93=>"CR",94=>"CS",95=>"CT",96=>"CU",97=>"CV",98=>"CW",99=>"CX",100=>"CY",
101=>"CZ",102=>"DA",103=>"DB",104=>"DC",105=>"DD",106=>"DE",107=>"DF",108=>"DG",109=>"DH",110=>"DI",
111=>"DJ",112=>"DK",113=>"DL",114=>"DM",115=>"DN",116=>"DO",117=>"DP",118=>"DQ",119=>"DR",120=>"DS",
121=>"DT",122=>"DU",123=>"DV",124=>"DW",125=>"DX",126=>"DY",127=>"DZ",128=>"EA",129=>"EB",130=>"EC",
131=>"ED",132=>"EE",133=>"EF",134=>"EG",135=>"EH",136=>"EI",137=>"EJ",138=>"EK",139=>"EL",140=>"EM",
141=>"EN",142=>"EO",143=>"EP",144=>"EQ",145=>"ER",146=>"ES",147=>"ET",148=>"EU",149=>"EV",150=>"EW",
151=>"EX",152=>"EY",153=>"EZ",154=>"FA",155=>"FB",156=>"FC",157=>"FD",158=>"FE",159=>"FF",160=>"FG",
161=>"FH",162=>"FI",163=>"FJ",164=>"FK",165=>"FL",166=>"FM",167=>"FN",168=>"FO",169=>"FP",170=>"FQ",
171=>"FR",172=>"FS",173=>"FT",174=>"FU",175=>"FV",176=>"FW",177=>"FX",178=>"FY",179=>"FZ",180=>"GA",
181=>"GB",182=>"GC",183=>"GD",184=>"GE",185=>"GF",186=>"GG",187=>"GH",188=>"GI",189=>"GJ",190=>"GK",
191=>"GL",192=>"GM",193=>"GN",194=>"GO",195=>"GP",196=>"GQ",197=>"GR",198=>"GS",199=>"GT",200=>"GU",	
201=>"GV",202=>"GW",203=>"GX",204=>"GY",205=>"GZ",206=>"HA",207=>"HB",208=>"HC",209=>"HD",210=>"HE",
211=>"HF",212=>"HG",213=>"HH",214=>"HI",215=>"HJ",216=>"HK",217=>"HL",218=>"HM",219=>"HN",220=>"HO",
221=>"HP",222=>"HQ",223=>"HR",224=>"HS",225=>"HT",226=>"HU",227=>"HV",228=>"HW",229=>"HX",230=>"HY",
231=>"HZ");
$subqueArray = array(0=>"A",1=>"B",2=>"C",3=>"D",4=>"E");
//count($columnfieldArray);
	//echo"<pre>";print_r($columnfieldArray); exit;
//	echo"<pre>"; print_r($que_array); 
$cid =0; 
	for($q=0; $q<$cntQuestion;$q++)
	{
		//echo $q;
		$ansArray =array(); $question_title =''; $queansArray =array();
		$qcell =''; $qhcell ='';
		$question_title = strip_tags($que_array[$q]['question_text']);
		$question_title = str_replace('&nbsp;','',$question_title);
		$question_title = trim($question_title); 
		$question_title = html_entity_decode($question_title, ENT_QUOTES);
		$qhcell = $columnfieldArray[$cid]."1";
	 	$qcell = $columnfieldArray[$cid]."2";
		//echo $question_title; echo"</br>";
		$question_title = utf8_encode($question_title); 
		$h=0; $h = $q+1; $question_head ='';
		$question_head = "Question".$h;
		$objPHPExcel->getActiveSheet()->setCellValue($qhcell,$question_head);
		$objPHPExcel->getActiveSheet()->setCellValue($qcell,$question_title);
		
		if($que_array[$q]['question_type_id'] == 1 || $que_array[$q]['question_type_id'] == 5 || $que_array[$q]['question_type_id'] == 6)
		{
			if($que_array[$q]['question_type_id'] != 6)
			 	$ansArray =  asgDB::getQuestionAnswers($que_array[$q]['id']);
			elseif($que_array[$q]['question_type_id'] == 6)
				$ansArray =  asgDB::getQuestionAnswersofqtypesix($que_array[$q]['id']);
			$cntansArray = count($ansArray) ;
			 if($cntansArray >= 1)
			 {
				$strAns ='';
				foreach($ansArray as $ans)
				{
					$ansoption ='';
					$ansoption = trim($ans['answer_text']);
					$strAns.=$ansoption.",";
		 		}
					$strAns = substr($strAns,0,-1);
					$strAns = '"'.$strAns.'"';
					//echo $strAns = "'".$strAns."'"; echo"</br>";
					
				for($st=0; $st<$cntStudent;$st++)
				{
					$sa=$st+3;
					$anscell = "".$columnfieldArray[$cid].$sa.""; 
					$objPHPExcel->getActiveSheet()->setCellValue($anscell,$ansArray[0]['answer_text']);
					$objValidation = $objPHPExcel->getActiveSheet()->getCell($anscell)->getDataValidation();
					$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
					$objValidation->setAllowBlank(false);
					$objValidation->setShowDropDown(true);
					$objValidation->setFormula1($strAns);	 
					$objPHPExcel->setActiveSheetIndex(0);
				}
  			 }
 		}
			
	 	if($que_array[$q]['question_type_id'] == 6)
		{
			$quetitleArray =  asgDB::getQuestionofqtypesix($que_array[$q]['id']);
	 		$cntquetitleArray = count($quetitleArray) ;
			$subq=0;
			 if($cntquetitleArray >= 1)
			 {
				foreach($quetitleArray as $que)
				{
					$cid++; $question_tit = '';$qhcell='';$subquestion_head='';
					$subquestion_head = "Question".$h.$subqueArray[$subq]; 
					$qcell = $columnfieldArray[$cid]."2";
					$qhcell = $columnfieldArray[$cid]."1";
					$question_tit = strip_tags($que['answer_text']);
					$question_tit = trim($question_tit);
					//$question_tit = str_replace('&nbsp;','',$question_tit);
					$objPHPExcel->getActiveSheet()->setCellValue($qhcell,$subquestion_head);
					$objPHPExcel->getActiveSheet()->setCellValue($qcell,$question_tit);
					$subq++;
					
	 				for($st=0; $st<$cntStudent;$st++)
					{
						$sa=$st+3;
						$anscell = "".$columnfieldArray[$cid].$sa.""; 
						$objPHPExcel->getActiveSheet()->setCellValue($anscell,'Yes');
						$objValidation = $objPHPExcel->getActiveSheet()->getCell($anscell)->getDataValidation();
						$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
						$objValidation->setAllowBlank(false);
						$objValidation->setShowDropDown(true);
						$objValidation->setFormula1('"Yes,No"');	 
						$objPHPExcel->setActiveSheetIndex(0);
					}
	 			}
 			 }
 		}
		$cid++;
	}
//exit;
 
 	$assessment_name=  asgDB::GetAssessmentName($asg_id);
	$file_name = $assessment_name[0]['assignment_name'].".xls";
	if($_SERVER['HTTP_HOST'] == 'localhost')
	{
		$path = $_SERVER['DOCUMENT_ROOT']."WAPTrunk/estrellita/exportedAssessments/";
		$fileurl = $_SERVER['HTTP_HOST']."/WAPTrunk/estrellita/exportedAssessments/";
	}
	else
	{
		$path = $_SERVER['DOCUMENT_ROOT']."estrellita/exportedAssessments/";
		$fileurl = $_SERVER['HTTP_HOST']."/estrellita/exportedAssessments/";
	}
	 $filename1 = $path.$file_name;
	$fileurl = $fileurl.$file_name;
	if(file_exists($filename1))
	{
		unlink($filename1);
	}
  	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save($filename1);
	$callEndTime = microtime(true);
	
	 
//	 chmod($filename,0755);
 	if(file_exists($filename1))
	{	
	 	//$link = __DIR__;
		//echo dirname ($link);
	 	$success = 'File has been created successfully.'; 
	  	$link = "../estrellita/exportedAssessments/".$file_name;
	//echo $link =$fileurl;
	}
 	  
	  function desc_func()
    {
        return ASSESSMENT_EXPORT;
    }

?>
