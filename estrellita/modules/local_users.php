<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("1");

    require "extgrid.php";
    require "db/users_db.php";

    $hedaers = array("&nbsp;",LOGIN,  USER_NAME, USER_SURNAME, ADDED_DATE, USER_TYPE, EMAIL,"&nbsp;","&nbsp;","&nbsp;");
    $columns = array("UserName"=>"text", "Name"=>"text","Surname"=>"Surname","added_date"=>"short date","type_name"=>"text","email"=>"text");

    $grd = new extgrid($hedaers,$columns, "index.php?module=local_users");
    $grd->edit_link="index.php?module=add_edit_user";
    $grd->id_column="UserID";
    $grd->column_override=array("type_name"=>"user_type_override", "UserName"=>"login_override");
    $grd->auto_id=true;
    $grd->id_links=array(QUIZZES=>"?module=old_assignments");

    function login_override($row)
    {
        $login = $row['UserName'];
        $user_photo_file = util::get_img_file($row['user_photo']);
        $href= "index.php?module=add_edit_user&id=".$row['UserID'];
       // $thumb = util::get_thumb($user_photo_file);
        $res = "<a href=\"$href\" class=\"ttip_b\" title=\"<img style='width:200px' src='user_photos/$user_photo_file' />\">$login</a>";
        //class="ttip_b" title="<b><i>salam</i></b>" 
      //  echo "user_photos/$user_photo_file";
        return $res;
    }
    function user_type_override($row)
    {
        global $USER_TYPE;
        return @$USER_TYPE[$row['user_type']];
    }
    
    function user_photo_override($row)
    {
        $user_photo_file = $row['user_photo'];
        $thumb = util::get_thumb($user_photo_file);
        $res = "<a href=\"user_photos/$user_photo_file\"  class=\"cbox_single thumbnail cboxElement\">";
        $res.="<img style='width:200px' src=\"user_photos/$thumb\" ></a>";
       // $res.="Photo</a>";
       // return "salam";
        return $res;
    }

    if($grd->IsClickedBtnDelete())
    {
       $resultsd = orm::Select("users", array("user_photo"), array("UserID"=>$grd->process_id), "UserID");
       $rowd=db::fetch($resultsd);
       if($rowd['user_photo']!="")
       {
           $user_photo=$rowd['user_photo'];
           $arrold = explode(".", $user_photo);
           $extold = end($arrold);  
           @unlink("user_photos".DIRECTORY_SEPARATOR.$user_photo);
           @unlink("user_photos".DIRECTORY_SEPARATOR.$user_photo.".thumb.".$extold);
       }
       orm::Delete("users", array("UserID"=>$grd->process_id));
	  
	   //DElete student from student table
	   
	   orm::Delete("students", array("UserID"=>$grd->process_id));
    }

    $query = users_db::GetUsersQuery();
    $grd->DrowTable($query);
    $grid_html = $grd->table;

    $search_html = $grd->DrowSearch(array(LOGIN, USER_NAME, USER_SURNAME),array("UserName", "Name", "Surname"));

    if(isset($_POST["ajax"]))
    {
         echo $grid_html;
    }

    function desc_func()
    {
        return LOCAL_USERS;
    }

?>
