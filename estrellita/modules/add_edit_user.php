<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("1");

    $val = new validations("btnSave");
	//$val->AddValidator("drpUserType", "empty", SELECT_USER_TYPE_VAL,"");
	$val->AddValidator("txtName", "empty", USERNAME_VAL,"");
	$val->AddValidator("txtSurname", "empty", SURNAME_VAL,"");
	$val->AddValidator("selectdistricts", "empty", DISTRICT_TYPE_VAL,"");
	//$val->AddValidator("studentnumebr", "empty", STUDENT_VAL,"");
    $val->AddValidator("txtLogin", "empty", LOGIN_VAL,"");
    $val->AddValidator("txtPassword", "empty", PASSWORD_VAL,"");
    $val->AddValidator("drpUserType", "notequal", USER_TYPE_VAL , "-1");
    $val->AddValidator("drpCountries", "notequal", COUNTRY_VAL , "-1");

    $selected="-1";
    $selected_country = DEFAULT_COUNTRY;
    $pswlbl_display="none";
    $psw_display="";
    $login_disabled="";
    $mode="add";
    $chkApproved = "checked";
    
    $user_photo_file = "";
    $photo_thumb = "";
    
    if(isset($_GET["id"]))
    {
        $pswlbl_display="";
        $psw_display="none";
        $login_disabled="read-only";
        $mode="edit";
        $id = util::GetID("?module=local_users");
        $rs_users=orm::Select("users", array(), array("UserID"=>$id), "");

        if(db::num_rows($rs_users) == 0 ) header("location:?module=local_users");

        $row_users=db::fetch($rs_users);
        $txtName = $row_users["Name"];
        $txtSurname = $row_users["Surname"];
        $txtEmail = $row_users["email"];
        $txtLogin1 = $row_users["UserName"];
        $selected = $row_users["user_type"];
        $selected_country = $row_users["country_id"];
        
		//new
	$district_id = $row_users["district_id"]; 
		
		$grade_name = $row_users["grade_name"];
		$grade_id = $row_users["grade_id"];
		$school_id = $row_users["school_id"];
		//echo '<pre>';echo $row_users['studentnumebr'];exit;
     
// end new
		
		
        $user_photo_file=trim($row_users["user_photo"]);
        
        if($user_photo_file!="")
        $photo_thumb=util::get_img($user_photo_file,true);

        $txtPasswordValue="********";

	$txtAddr = $row_users["address"];
	$txtPhone = $row_users["phone"];
        $chkApproved = $row_users["approved"] == "1" ? "checked" : "";
	$chkDisabled = $row_users["disabled"] == "1" ? "checked" : "";
    }
    
    $results = orm::Select("user_types", array(), array() , "id");
    $countries_res = orm::Select("countries_quiz", array(), array(), "country_name");
    //$user_type_options = webcontrols::GetOptions($results, "id", "type_name",$selected);
    $user_type_options = webcontrols::BuildOptions($USER_TYPE, $selected);
    $country_options = webcontrols::GetOptions($countries_res, "id", "country_name", $selected_country);
    
	/* new 4 may */
	
		 $db = new db();
		 $sql='SELECT * FROM grades';
		 $grades_rows = db::GetResultsAsArray($sql);
		 
		 $sql2='SELECT * FROM schools';
		 $schools_rows = db::GetResultsAsArray($sql2);
	
		 $sql3='SELECT * FROM districts where district_id="'.$_SESSION['district_id'].'"';
		 $Districts_rows = db::GetResultsAsArray($sql3);
	
	/* end*/
	
	

    if(isset($_POST["btnSave"]) && $val->IsValid())
    {
        if(!isset($_GET["id"]))
        {
		
		 $sql='SELECT * FROM dist_grades where dist_grade_id="'.$_POST["selectgrade"].'"';
		 $grades_rows = db::GetResultsAsArray($sql);		 
		 @$grade_name = $grades_rows[0]['grade_name'];
		 
		if($_SERVER["HTTP_HOST"]=="localhost"){
			mysql_connect('localhost','root','') or die('could not connect with mysql'.mysql_error());
			mysql_select_db('tor') or die('could not select the database'.mysql_error());
		}
		else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
			mysql_connect('localhost','root','harddisk') or die('could not connect with mysql'.mysql_error());
		 	mysql_select_db('client') or die('could not select the database'.mysql_error());
	    }else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
			mysql_connect('localhost','root','harddisk') or die('could not connect with mysql'.mysql_error());
		 	mysql_select_db('client') or die('could not select the database'.mysql_error());
	    }
	
	 $qqq = "select * from users where UserName='".$_POST["txtLogin"]."' ";
		 $dd= mysql_query($qqq);
		 $rows = mysql_fetch_assoc($dd);
		 if($rows['UserName'] == $_POST["txtLogin"])
		 {
			 echo '<script>alert("User Already Exists")</script>';
			 echo '<script>window.history.back()</script>';
			 exit();
		}
		 else
		 {
		
		 
		 
            add_file();
			if(isset($_POST["studentnumebr"])&& $_POST["studentnumebr"]!='')
			{
				
				
				if(isset($_POST["txtEmail"]) && $_POST["txtEmail"]!="")
				{
					
			$db = new db();
			$query1='SELECT * from users where email ="'.$_POST["txtEmail"].'"';
			$check_email_exists = db::GetResultsAsArray($query1);
			if(count($check_email_exists)>0)
			{
			echo "<script>alert('Email Already Exists')</script>";
			echo '<script>window.history.back()</script>';
			 exit();
			}
					
					
				}
				
		
            orm::Insert("users", array("Name"=>trim($_POST["txtName"]),
                                    "Surname"=>trim($_POST["txtSurname"]),
                                    "UserName"=>trim($_POST["txtLogin"]),
                                     "Password"=>md5(trim($_POST["txtPassword"])),
                                     "added_date"=>util::Now(),
                                     "email"=>trim($_POST["txtEmail"]),
                                     "address"=>trim($_POST["txtAddr"]),
                                     "phone"=>trim($_POST["txtPhone"]),
  				     "approved"=>isset($_POST["chkApproved"]) ? 1:0,
				     "disabled"=>isset($_POST["chkDisabled"]) ? 1:0,
                                     "user_type"=>trim($_POST["drpUserType"]),
                                     "country_id"=>trim($_POST["drpCountries"]),
                                    "user_photo"=>$filename,
									"grade_id"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
									"grade_name"=>trim($grade_name),
									"district_id"=>trim($_POST["selectdistricts"]),
									"studentnumebr"=>trim($_POST["studentnumebr"])
                                   ));
								   }
								   else
								   {  orm::Insert("users", array("Name"=>trim($_POST["txtName"]),
                                    "Surname"=>trim($_POST["txtSurname"]),
                                    "UserName"=>trim($_POST["txtLogin"]),
                                     "Password"=>md5(trim($_POST["txtPassword"])),
                                     "added_date"=>util::Now(),
                                     "email"=>trim($_POST["txtEmail"]),
                                     "address"=>trim($_POST["txtAddr"]),
                                     "phone"=>trim($_POST["txtPhone"]),
  				     "approved"=>isset($_POST["chkApproved"]) ? 1:0,
				     "disabled"=>isset($_POST["chkDisabled"]) ? 1:0,
                                     "user_type"=>trim($_POST["drpUserType"]),
                                     "country_id"=>trim($_POST["drpCountries"]),
                                    "user_photo"=>$filename,
									"grade_id"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
									"grade_name"=>trim($grade_name),
									"district_id"=>trim($_POST["selectdistricts"]),
									"studentnumebr"=>trim($_POST["empid"])
                                   ));
								   }
								   
						//16 may 2013		   
								   
						$db = new db();
			$sql='SELECT max(UserID) FROM users';
			$grades_rows = db::GetResultsAsArray($sql);
			$lastinid = $grades_rows[0][0]; 				//find last inserted id 
			
		
								   
			 orm::Insert("students", array("firstname"=>trim($_POST["txtName"]),
                                    "lastname"=>trim($_POST["txtSurname"]),
									"grade"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
																	
									"student_number"=>trim($_POST["studentnumebr"]),	
										"UserID"=>trim($lastinid)	
									
                                   ));
								   
					    //16 may 2013	
						
			$db = new db();
			$sql1='SELECT max(student_id) FROM students';
			$grades_rows1 = db::GetResultsAsArray($sql1);
			$lastinidStudent = $grades_rows1[0][0]; 				//find last id :  student table 
			
		 }
		$db = new db();
		
		//echo "update users set school_id=".$lastinidStudent." where UserID=".$lastinid." ";
		
		//$stupdate = mysql_query("update users set school_id=".$lastinidStudent." where UserID=".$lastinid." ");
								   
		// orm::Update("update users set school_id=".$lastinidStudent." where UserID=".$lastinid." ");

		$arr_columns1 =  array(
							'student_id' => $lastinidStudent
								);
		 
		  orm::Update("users", $arr_columns1, array("UserID"=>$lastinid));  // update user table with student id
						
						
						
			if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
			$path ="http://district.ueisworkshop.com/Quiz";
			} else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
			$path ="http://enterprise.ueisworkshop.com/Quiz";
			}
			else if($_SERVER["HTTP_HOST"]=="localhost"){
			$path ="http://localhost/testbank/workshop/Quiz";
			} 

						
						
						
						// send mail to users  18may 2013
		
		$to = $_POST["txtEmail"];
		$subject = "Test Bank : Your Password";
		
		$body = "Your Username and Password on Test Bank Assessment";
		$body .="<br>Username :".$_POST["txtLogin"]." ";
		$body .="<br>Password :".$_POST["txtPassword"]." ";
		$body .="<br>Login in  :<a href='".$path."'>'".$path."'</a> and check your Assessment";
		$headers  = 'MIME-Version: 1.0'."\r\n";
	    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$from.' "\r\n"';
							
                            @mail($to, $subject, $body, $headers);
		
					// end send mail to user 18may 2013
				
								   
        }
        else
        {
			if(isset($_POST["studentnumebr"])&& $_POST["studentnumebr"]!='')
			{
			
			$sql='SELECT * FROM dist_grades where dist_grade_id="'.$_POST["selectgrade"].'"';
			$grades_rows = db::GetResultsAsArray($sql);
			$grade_name = $grades_rows[0]['grade_name'];
			
            add_file();
            $arr_columns=array("Name"=>trim($_POST["txtName"]),
                                    "Surname"=>trim($_POST["txtSurname"]),
                                     "email"=>trim($_POST["txtEmail"]),
				     "address"=>trim($_POST["txtAddr"]),
                                     "phone"=>trim($_POST["txtPhone"]),
  				     "approved"=>isset($_POST["chkApproved"]) ? 1:0,
				     "disabled"=>isset($_POST["chkDisabled"]) ? 1:0,
                                     "user_type"=>trim($_POST["drpUserType"]),
                                     "country_id"=>trim($_POST["drpCountries"]),
									 "grade_id"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
									"grade_name"=>trim($grade_name),
									"district_id"=>trim($_POST["selectdistricts"]),
									"studentnumebr"=>trim($_POST["studentnumebr"])
                                   );
								      //16 may 2013	
							$students = array("firstname"=>trim($_POST["txtName"]),
                                    "lastname"=>trim($_POST["txtSurname"]),
									"grade"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
									"student_number"=>trim($_POST["studentnumebr"])
                                   );}else{$arr_columns=array("Name"=>trim($_POST["txtName"]),
                                    "Surname"=>trim($_POST["txtSurname"]),
                                     "email"=>trim($_POST["txtEmail"]),
				     "address"=>trim($_POST["txtAddr"]),
                                     "phone"=>trim($_POST["txtPhone"]),
  				     "approved"=>isset($_POST["chkApproved"]) ? 1:0,
				     "disabled"=>isset($_POST["chkDisabled"]) ? 1:0,
                                     "user_type"=>trim($_POST["drpUserType"]),
                                     "country_id"=>trim($_POST["drpCountries"]),
									 "grade_id"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
									"grade_name"=>trim($grade_name),
									"district_id"=>trim($_POST["selectdistricts"]),
									"studentnumebr"=>trim($_POST["empid"])
                                   );
								      //16 may 2013	
							$students = array("firstname"=>trim($_POST["txtName"]),
                                    "lastname"=>trim($_POST["txtSurname"]),
									"grade"=>trim($_POST["selectgrade"]),
									"school_id"=>trim($_POST["selectschool"]),
									"student_number"=>trim($_POST["empid"])
                                   );
									   }
								      //16 may 2013	
            
            if(trim($filename)!="") 
            {
                $arr_columns["user_photo"]=$filename;
                if(trim($user_photo_file)!="")
                {
                        @unlink("user_photos".DIRECTORY_SEPARATOR.$filename);
                        @unlink("user_photos".DIRECTORY_SEPARATOR.$thumb);
                }
            }
            
            if(isset($_POST["chkEdit"]))
            {
                $arr_columns["Password"]=md5(trim($_POST["txtPassword"]));
            }
            orm::Update("users", $arr_columns, array("UserID"=>$id));
			orm::Update("students", $students, array("UserID"=>$id));
        }

        header("location:?module=local_users");
    }


    if(isset($_POST["ajax"]))
    {
	
         $results = orm::Select("users", array(), array("UserName"=>$_POST["login_to_check"]) , "");
         $count = db::num_rows($results);
         echo $count;
    }

    function desc_func()
    {
        return ADD_EDIT_USER;
    }
    
    $filename = "";
    $thumb = "";
    
    function add_file()
    {    
        global $filename,$thumb;  
        if($_FILES['userphoto']['size']>0)
        {
                $filename=basename( $_FILES['userphoto']['name']);
                $arr = explode(".", $filename);
                $ext = end($arr);                
                $filename=md5(util::GUID()).".".$ext;
                $target_path = "user_photos/";
                $target_path = $target_path . $filename;

                move_uploaded_file($_FILES['userphoto']['tmp_name'], $target_path);     
               
                util::createThumbnail($target_path,90);
                $thumb=".thumb.".$ext;
        }

    }

?>
