<?php if(!isset($RUN)) { exit(); } ?>
<?php

    access::allow("1");

    require "extgrid.php";
    require "db/questions_db.php";
    
    $hedaers = array("&nbsp;",QUIZ_NAME,QUESTION,"Test Cluster", TYPE, POINT, ADDED_DATE, "&nbsp;","&nbsp;","&nbsp;");
    $columns = array("quiz_name"=>"text", "question_text"=>"text","test_cluster"=>"text","question_type"=>"text" ,"point"=>"text","added_date"=>"short date");

    $grd = new extgrid($hedaers,$columns, "index.php?module=questions_bank");
    $grd->edit_link="index.php?module=add_question&quiz_id=-1";
    $grd->edit_link_override="edit_link";
    $grd->column_override=array("question_type"=>"question_type_override");
    $grd->jslinks=array(PREVW=>"ShowPreview(\"[id]\",event.pageY)");
    $grd->auto_id=true;
    $grd->search_mode=2;
    $grd->search=true;

    function edit_link($row)
    {
        $quiz_id = $row["quiz_id"];
        $id= $row["id"];
        return "<a href=\"index.php?module=add_question&qstbank=1&id=$id&quiz_id=$quiz_id\">".EDIT."</a>";
    }
    function question_type_override($row)
    {
        global $QUESTION_TYPE;
        return $QUESTION_TYPE[$row['question_type_id']];
    }
    
    if($grd->IsClickedBtnDelete())
    {
        $rs_qsts=orm::Select("questions", array(), array("id"=>$grd->process_id), "");
        $row_qst = db::fetch($rs_qsts);
        $quiz_id=$row_qst['quiz_id'];
        questions_db::DeleteQuestion($grd->process_id);    
        if($quiz_id!="-1")
        {
            $priority = questions_db::GetMinPriority($quiz_id);
            if($priority!=-1)
            questions_db::UpdatePriority($quiz_id,$priority);  
        }
    }
    
    $query = questions_db::GetQuestionsBankQuery();    
    $grd->DrowTable($query);
    $grid_html = $grd->table;
    
    $search_html = $grd->DrowSearch(array(QUIZ_NAME, QUESTION),array("quiz_name", "question_text"));

    if(isset($_POST["ajax"]))
    {
         echo $grid_html;
    } 
    
   function desc_func()
   {
        return QUESTIONS_BANK;
   }
    
?>    