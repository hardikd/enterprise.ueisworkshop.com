<?php if(!isset($RUN)) { exit(); } ?>
<script language="JavaScript" type="text/javascript" src="rte/html2xhtml.js"></script>
<script language="JavaScript" type="text/javascript" src="rte/richtext.js"></script>
<script language="JavaScript" type="text/javascript" src="lib/validations.js"></script>



<form id="form1" method="post" onsubmit="return submitForm();">
<table class="desc_text" style="width:850px">
    <tr>
        <td valign="top">
            <?php echo QUESTION ?> :
        </td>
        <td>
            <?php $CKEditor->editor("txtQstsEd", $txtQsts) ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo POINT ?> :
        </td>
        <td>
            <input style="width:100px" type="text" id="txtPoint" value="<?php echo util::GetData("txtPoint") ?>" name="txtPoint">
        </td>
    </tr>
  
    <tr>
        <td>
            <?php echo TESTITEMCLUSTER ?> :
        </td>
        <td>
            <input style="width:100%;height:70px" type="text" id="testitemcluster" value="<?php echo util::GetData("testitemcluster") ?>" name="testitemcluster">
        </td>
    </tr>
    
    
       <tr>
        <td>
            <?php echo PENALTY_POINT ?> :
        </td>
        <td>
            <input style="width:100px" type="text" id="txtPenaltyPoint" value="<?php echo util::GetData("txtPenaltyPoint") ?>" name="txtPenaltyPoint">
        </td>
    </tr>
    <tr>
        <td valign="top">
            <?php echo HEADER_TEXT ?> :
        </td>
        <td>
            <textarea style="width:100%;height:70px" id="txtHeader" name="txtHeader"><?php echo util::GetData("txtHeader") ?></textarea>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <?php echo FOOTER_TEXT ?> :
        </td>
        <td>
            <textarea style="width:100%;height:70px" id="txtFooter" name="txtFooter"><?php echo util::GetData("txtFooter") ?></textarea>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo SELECT_TEMP ?> :
        </td>
        <td>
            <select id="drpTemplate" name="drpTemplate" style="width:100%" onchange="ChangeTemplate()">
                <?php echo $temp_options ?>
            </select>
        </td>
    </tr>
</table>

<table style="width:880px">
    <tr style="display:none" id="trMulti">
        <td align="center">
            <table class="desc_text" id="tblMulti" >
                <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName") ?>"  name="txtMultiGroupName" id="txtMultiGrpName"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><hr></td>
                </tr>
                <tr>                    
                    <td><?php echo PRIORITY."       ".ANSWER_VARIANTS; ?>
                    </td>
                    
                      <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                    <td class="desc_text"><?php echo CORRECT_ANSWER ?> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count;$i++) { ?>
                <tr>
                    
                    <td>
                                         <?php $pr = util::GetData("txtPriority$i"); ?>
                    <input type="text" id="txtPriorityM<?php echo $i ?>" value="<?php if($pr!='') echo $pr; else 
					echo $i;?>" name="txtPriorityM<?php echo $i ?>"  style="position:relative;width:20px;" /> 

                        <input type="text" id="txtChoise1" value="<?php echo util::GetData("txtChoise$i") ?>" name="txtMulti<?php echo $i ?>"></td>
                     <td>
                            <input type="text" id="txtDesc<?php echo $i ?>" name="txtMultiDesc<?php echo $i ?>" value="<?php echo util::GetData("txtDesc$i") ?>">
                       </td>
                    <td><input <?php echo util::GetData("ans_selected$i") ?> type="checkbox" id="chkMulti<?php echo $i ?>" name="chkMulti<?php echo $i ?>" ></td>
                </tr>
                <?php } ?>
            </table>
            <table width="170px">
                <tr>

                    <td align="center"><input style="width:25px" type="button" value=" + " onclick="addRow('tblMulti','txtMulti')" />
                        <input style="width:25px" type="button" value=" - " onclick="deleteRow('tblMulti')" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="display:none" id="trOne" >
        <td align="center">
            <table id="tblOne" class="desc_text" >
                <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName") ?>" name="txtOneGroupName"></td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td colspan="3"><hr></td>
                </tr>
                <tr>

                    <td><?php echo PRIORITY."       ".ANSWER_VARIANTS; ?>
                   </td>
                   <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                    <td class="desc_text"><?php echo CORRECT_ANSWER ?> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count;$i++) { ?>
                <tr>
                    <td>
                        <?php $pr = util::GetData("txtPriority$i"); ?>
                    <input type="text" id="txtPriorityR<?php echo $i ?>" value="<?php if($pr!='') echo $pr; else 
					echo $i;?>" name="txtPriorityR<?php echo $i ?>"  style="position:relative;width:20px;" > 
                                        
                    
                        <input type="text" id="txtChoise<?php echo $i ?>" name="txtOne<?php echo $i ?>" value="<?php echo util::GetData("txtChoise$i") ?>">
                    </td>
                       <td>
                            <input type="text" id="txtDesc<?php echo $i ?>" name="txtOneDesc<?php echo $i ?>" value="<?php echo util::GetData("txtDesc$i") ?>">
                       </td>
                    <td><input <?php echo util::GetData("ans_selected$i") ?> type="radio" name="rdOne" value="<?php echo $i ?>"></td>
                </tr>
                <?php } ?>
            </table>
            <table width="170px">
                <tr>

                    <td align="center"><input style="width:25px" type="button" value=" + " onclick="addRow('tblOne','txtOne')" />
                        <input style="width:25px" type="button" value=" - " onclick="deleteRow('tblOne')" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="display:none" id="trArea">
        <td align="center">
            <table id="tblArea" class="desc_text">
                 <tr>
                     <td valign="top" align="right">
                         <?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):
                     </td>
                    <td>
                        <input style="width:300px" type="text" value="<?php echo util::GetData("txtGroupName") ?>" name="txtAreaGroupName"></td>

                </tr>
                <tr>
                    <td valign="top" align="right">
                         <?php echo ENTER_CORRECT_ANSWER ?> (<?php echo CAN_BE_EMPTY ?>):
                     </td>
                    <td>
                        <textarea style="width:300px;height:100px" name="txtArea1"><?php echo util::GetData("txtCrctAnswer1") ?></textarea>
                    <td>
                </tr>
            </table>
        </td>
    </tr>
        <tr style="display:none" id="trMultiText">
        <td align="center">
            <table id="tblMultiText" class="desc_text">
                <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName") ?>" name="txtMultiTextGroupName"></td>
                    <td>&nbsp;</td>
                </tr>
               <tr>
                    <td colspan="3"><hr></td>
                </tr>
               <tr>
                    <td><?php echo PRIORITY."       ".ANSWER_VARIANTS; ?>
                    </td>
                      <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                    <td class="desc_text"><?php echo CORRECT_ANSWER ?> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count;$i++) { ?>
                <tr>

                    <td  width="265px">                     <?php $pr = util::GetData("txtPriority$i"); ?>
                    <input type="text" id="txtPriorityMT<?php echo $i ?>" value="<?php if($pr!='') echo $pr; else 
					echo $i;?>" name="txtPriorityMT<?php echo $i ?>"  style="position:relative;width:20px;" /> 

                        <input type="text" id="txtChoise<?php echo $i ?>" name="txtMultiText<?php echo $i ?>" value="<?php echo util::GetData("txtChoise$i") ?>"></td>
                     <td>
                            <input type="text" id="txtDesc<?php echo $i ?>" name="txtMultiTextDesc<?php echo $i ?>" value="<?php echo util::GetData("txtDesc$i") ?>">
                       </td>
                    <td><input type="text" id="txtText<?php echo $i ?>" name="txtMultiCrctAnswer<?php echo $i ?>" value="<?php echo util::GetData("txtCrctAnswer$i") ?>"></td>
                </tr>
                <?php } ?>
            </table>          
             <table width="320px">
                <tr>

                    <td align="center"><input style="width:25px" class="btn"  type="button" value=" + " onclick="addRow('tblMultiText','txtMultiText')" />
                        <input style="width:25px" type="button" class="btn"  value=" - " onclick="deleteRow('tblMultiText')" />
                    </td>
                </tr>
            </table>
              <table style="display:none">
                <tr>
                    <td><input type="checkbox" id="chkAllowNumbers" name="chkAllowNumbers" /><label id="lbl1" for="chkAllowNumbers">Allow users to enter only numbers</label></td>
                </tr><tr>
                    <td><input type="checkbox" id="chkDontCalc" name="chkDontCalc" /><label id="lbl1" for="chkDontCalc">Do not calculate results of this question</label></td>
                </tr>
            </table>
        </td>
    </tr>
   
   <tr style="display:none" id="trMultiRadio">
        <td align="center">
            <table class="desc_text" id="tblMultiRadio" >
                <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName") ?>"  name="txtMultiRedioGroupName" id="txtMultiGrpName"/></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><hr></td>
                </tr>
                <tr>                    
                   <td><?php  echo PRIORITY."       ".ANSWER_VARIANTS ?></td>
                    <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                   <td >Checkbox ? &nbsp; <?php echo CORRECT_ANSWER ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Score &nbsp;&nbsp;Self-corrects vowel?</td>
                    <td class="desc_text"> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count;$i++) { ?>
                <tr>
                     <td width="265px">
                     <?php $pr = util::GetData("txtPriority$i"); ?>
                    <input type="text" id="txtPriority<?php echo $i ?>" value="<?php if($pr!='') echo $pr; else 
					echo $i;?>" name="txtPriority<?php echo $i ?>"  style="position:relative;width:20px;" /> 
                         <input type="text" id="txtMultiRadio<?php echo $i ?>" value="<?php echo util::GetData("txtChoise$i") ?>" name="txtMultiRadio<?php echo $i ?>"/></td>
                     <td> 
                            <input type="text" id="txtMultiRadioDesc<?php echo $i ?>" name="txtMultiRadioDesc<?php echo $i ?>" value="<?php echo util::GetData("txtDesc$i") ?>"/>
                       </td>
                       <?php 
				 	  $radio_ans = util::GetData("radio_ans$i");
				 	  $ans_selected = util::GetData("ans_selected$i");
					//  $is_diff_radio =  util::GetData("is_diff_radio$i");
						$chkRadio = ''; $chkMultiR=''; //exit;
	 				    ?> 
                        <td width="420" ><input type="checkbox" id="chkRadio<?php echo $i ?>" name="chkRadio<?php echo $i ?>"  <?php if($radio_ans == 0) echo 'checked="checked"'; ?> onchange="getCheckboxRadio(<?php echo $i ?>)" value="1" />
                          <input type="hidden" id="chkcount" value="<?php echo $i ?>"/>
                
                     <span  id="chkRadioSpace<?php echo $i; ?>" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span> 
                    <input  type="checkbox" id="chkMultiR<?php echo $i ?>" name="chkMultiR<?php echo $i ?>" <?php if($radio_ans == 0 &&$ans_selected== 'checked') echo 'checked="checked"';?> style="display:none" value="" /> 
                  <!--  <div  align="right" style="width:100; height:20; display:none" id="cRadio<?php echo $i; ?>" > -->
                    <span  id="cRadioSpace<?php echo $i; ?>" style="display:none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; </span>
                    <input type="radio"  name="cRadio<?php echo $i ?>" value="1" id="cRadioY<?php echo $i; ?>" <?php
					 if($radio_ans== 1 && $ans_selected == 'checked') echo 'checked="checked"';?>  style="display:none"/>
                      <span id="cRadioYes<?php echo $i; ?>"  style="display:none">  Yes </span>
                     <input type="radio"  name="cRadio<?php echo $i ?>" value="0" id="cRadioN<?php echo $i; ?>" <?php
					  if($radio_ans == 1 && $ans_selected== '') echo 'checked="checked"';?>  style="display:none"/>
                    <span  id="cRadioNo<?php echo $i; ?>" style="display:none" >  No </span>
                    
                    <input type="radio"  name="cRadio<?php echo $i ?>" value="2" id="cRadioD<?php echo $i; ?>" <?php
					  if($radio_ans == 2) echo 'checked="checked"';?>  style="display:none"/>
                    <span  id="cRadioDiff<?php echo $i; ?>" style="display:none" >  Difficult&nbsp;&nbsp; </span>
                     
                      <input type="text"   style="width:20px; display:none"  id="txtscore<?php echo $i ?>" value="<?php echo util::GetData("score$i") ?>" name="txtscore<?php echo $i ?>"/>
                      
                      <?php if($i <= 2)
				{  $scv_ans = util::GetData("scv_ans$i");  ?>
                	<span id="scvSpace<?php echo $i;?>" style ="display:">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</span> 
                    <input type="checkbox" id="chkscv<?php echo $i ?>" name="chkscv<?php echo $i ?>" <?php if($scv_ans== 1)  echo 'checked="checked"'; ?> />
                <!--	<input type="radio" name="scvRadio<?php // echo $i ?>" id="scvRadioY<?php // echo $i ?>" value="1" style="display:none" <?php // if($scv_ans== 1)  echo 'checked="checked"'; ?> />
                    <span id="scvRadioYes<?php echo $i; ?>"  style="display:none">  Yes </span>
                    <input type="radio" name="scvRadio<?php echo $i ?>" id="scvRadioN<?php echo $i ?>" value="0" style="display:none" <?php //if($scv_ans== '0')  echo 'checked="checked"'; ?> />
                    <span  id="scvRadioNo<?php echo $i; ?>" style="display:none" >  No </span>-->
					<?php 
				}?> 
                     </td>
                </tr>
                <?php } ?>
            </table>
            <table width="170px">
                <tr>

                    <td align="center"><input style="width:25px" type="button" value=" + " onclick="addRow('tblMultiRadio','txtMultiRadio')" />
                        <input style="width:25px" type="button" value=" - " onclick="deleteRow('tblMultiRadio')" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    

</table>
    <br>
     <hr />
     <br>
     
 <table name="tblSecScore" style="width:850px">
  <tr>
        <td width="25%">
          Display Secondary Scoring Tool : <?php  $ScoringTool = util::GetData("have_score_ans"); ?>
        </td>
        <td width="10%">
            <select id="SecScoringTool" name="SecScoringTool" style="width:100%" onchange="loadSecTemplate();">
                 <option value="0" <?php if($ScoringTool == 0) echo"selected"; ?> >No</option>
                <option value="1" <?php if($ScoringTool == 1) echo"selected"; ?> >Yes</option>
            </select>
        </td><td></td>
        
   </tr>
   </table>
    <br>
     <hr />
     <br>
   <table  style="width:850px;display:none " id="tblpointTempSec">
   <tr>     
        <td width="15%"> Point : </td>
         <td width="80%">
             <input style="width:100px" type="text" id="txtPointSec" value="<?php echo util::GetData("txtPointSec") ?>" name="txtPointSec" onblur="return comparePriorityPoint()">
         	(Only for Assessment.)
         </td>
        
    </tr>
    <tr>
        <td>
            <?php echo SELECT_TEMP ?> :
        </td>
        <td >
            <select id="drpTemplateSec" name="drpTemplateSec" style="width:100%" onchange="ChangeTemplateSec()">
                <?php echo $temp_options_sec ?>
            </select>
        </td>
    </tr>
 </table>
     
     <table style="width:850px">
    <tr style="display:none" id="trMultiSec">
        <td align="center">
            <table class="desc_text" id="tblMultiSec" >
         <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName_Sec") ?>"  name="txtMultiGroupNameSec" id="txtMultiGrpNameSec"></td>
                    <td>&nbsp;</td>
                </tr>
                
               <tr>
                    <td colspan="3"><hr></td>
                </tr>
                <tr>                    
                    <td><?php echo ANSWER_VARIANTS ?>
                    </td>
                    
                      <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                    <td class="desc_text"><?php echo CORRECT_ANSWER ?> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count_sec;$i++) { ?>
                <tr>
                    
                    <td>
                        <input type="text" id="txtChoiseSec1" value="<?php echo util::GetData("txtChoiseSec$i") ?>" name="txtMultiSec<?php echo $i ?>"></td>
                     <td>
                            <input type="text" id="txtDescSec<?php echo $i ?>" name="txtMultiDescSec<?php echo $i ?>" value="<?php echo util::GetData("txtDescSec$i") ?>">
                       </td>
                    <td><input <?php echo util::GetData("ans_selectedSec$i") ?> type="checkbox" id="chkMultiSec<?php echo $i ?>" name="chkMultiSec<?php echo $i ?>" ></td>
                </tr>
                <?php } ?>
            </table>
            <table width="170px">
                <tr>

                    <td align="center"><input style="width:25px" type="button" value=" + " onclick="addRow('tblMultiSec','txtMultiSec')" />
                        <input style="width:25px" type="button" value=" - " onclick="deleteRow('tblMultiSec')" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr style="display:none" id="trOneSec" >
        <td align="center">
            <table id="tblOneSec" class="desc_text" >
                 <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName_Sec") ?>" name="txtOneGroupNameSec"></td>
                    <td>&nbsp;</td>
                </tr>
                  <tr>
                    <td colspan="3"><hr></td>
                </tr>
                <tr>
 		          <td><?php echo ANSWER_VARIANTS ?>
                   </td>
                   <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                    <td class="desc_text"><?php echo CORRECT_ANSWER ?> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count_sec;$i++) { ?>
                <tr>
                    <td>
                        <input type="text" id="txtChoiseSec<?php echo $i ?>" name="txtOneSec<?php echo $i ?>" value="<?php echo util::GetData("txtChoiseSec$i") ?>">
                    </td>
                       <td>
                            <input type="text" id="txtDescSec<?php echo $i ?>" name="txtOneDescSec<?php echo $i ?>" value="<?php echo util::GetData("txtDescSec$i") ?>">
                       </td>
                    <td><input <?php echo util::GetData("ans_selectedSec$i") ?> type="radio" name="rdOneSec" value="<?php echo $i ?>"></td>
                </tr>
                <?php } ?>
            </table>
            <table width="170px">
                <tr>

                    <td align="center"><input style="width:25px" type="button" value=" + " onclick="addRow('tblOneSec','txtOneSec')" />
                        <input style="width:25px" type="button" value=" - " onclick="deleteRow('tblOneSec')" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="display:none" id="trAreaSec">
        <td align="center">
            <table id="tblAreaSec" class="desc_textSec">
                  <tr>
                     <td valign="top" align="right">
                         <?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):
                     </td>
                    <td>
                        <input style="width:300px" type="text" value="<?php echo util::GetData("txtGroupName_Sec") ?>" name="txtAreaGroupNameSec"></td>

                </tr>
                 <tr>
                    <td colspan="3"><hr></td> 
 		         </tr>
                <tr>
                    <td valign="top" align="right">
                         <?php echo ENTER_CORRECT_ANSWER ?> (<?php echo CAN_BE_EMPTY ?>):
                     </td>
                    <td>
                        <textarea style="width:300px;height:100px" name="txtAreaSec1"><?php echo util::GetData("txtCrctAnswerSec1") ?></textarea>
                    <td>
                </tr>
            </table>
        </td>
    </tr>
        <tr style="display:none" id="trMultiTextSec">
        <td align="center">
            <table id="tblMultiTextSec" class="desc_text">
                <tr>
                    <td align="right"><?php echo HEADER_TEXT ?> (<?php echo CAN_BE_EMPTY ?>):</td>
                    <td><input type="text" value="<?php echo util::GetData("txtGroupName_Sec") ?>" name="txtMultiTextGroupNameSec"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><hr></td>
                </tr>
               <tr>
                    <td><?php echo ANSWER_VARIANTS ?>
                    </td>
                      <td><?php echo ANSWER_DESCRIPTION ?>
                   </td>
                    <td class="desc_text"><?php echo CORRECT_ANSWER ?> </td>
                </tr>
                <?php for($i=1;$i<=$answers_count_sec;$i++) { ?>
                <tr>

                    <td>
                        <input type="text" id="txtChoiseSec<?php echo $i ?>" name="txtMultiTextSec<?php echo $i ?>" value="<?php echo util::GetData("txtChoiseSec$i") ?>"></td>
                     <td>
                            <input type="text" id="txtDescSec<?php echo $i ?>" name="txtMultiTextDescSec<?php echo $i ?>" value="<?php echo util::GetData("txtDescSec$i") ?>">
                       </td>
                    <td><input type="text" id="txtTextSec<?php echo $i ?>" name="txtMultiCrctAnswerSec<?php echo $i ?>" value="<?php echo util::GetData("txtCrctAnswerSec$i") ?>"></td>
                </tr>
                <?php } ?>
            </table>          
             <table width="320px">
                <tr>

                    <td align="center"><input style="width:25px" class="btn"  type="button" value=" + " onclick="addRow('tblMultiTextSec','txtMultiTextSec')" />
                        <input style="width:25px" type="button" class="btn"  value=" - " onclick="deleteRow('tblMultiTextSec')" />
                    </td>
                </tr>
            </table>
              <table style="display:none">
                <tr>
                    <td><input type="checkbox" id="chkAllowNumbersSec" name="chkAllowNumbersSec" /><label id="lbl1Sec" for="chkAllowNumbersSec">Allow users to enter only numbers</label></td>
                </tr><tr>
                    <td><input type="checkbox" id="chkDontCalcSec" name="chkDontCalcSec" /><label id="lbl1Sec" for="chkDontCalcSec">Do not calculate results of this question</label></td>
                </tr>
            </table>
        </td>
    </tr>
   
    

</table>
     <table style="width:850px">
         <tr>
        <td align="center">
            <input class="btn" type="submit" id="btnSave" name="btnSave" value="<?php echo SAVE ?>" style="width:150px">
            <input class="btn" type="button" id="btnCancel" name="btnCancel" value="<?php echo CANCEL ?>" style="width:150px" onclick="javascript:window.location.href='?module=questions'">
        </td>
    </tr>
     </table>
<script type="text/javascript">
CKEDITOR.config.width ='740px';
</script>
    <SCRIPT language="javascript">
	
        ChangeTemplate();
		var e = document.getElementById('SecScoringTool');
		var selected = e.options[e.selectedIndex].value;
		if(selected == 1)
		{	
			ChangeTemplateSec();
			document.getElementById('tblpointTempSec').style.display="";

		}
		if(selected == 0)
		{	DisableAllSecTemplates();
			document.getElementById('tblpointTempSec').style.display="none";
		}
        //var c_multi = 4;

        var counters = new Array();
        var answer_count = <?php echo $answers_count ?>;
        counters["tblMulti"] = answer_count;
        counters["tblOne"] = answer_count;
        counters["tblArea"] = answer_count;
        counters["tblMultiText"] = answer_count;
		counters["tblMultiRadio"] = answer_count;

        function addRow(tableID, textboxID ) {

            counters[tableID]++;  
		               
            var table = document.getElementById(tableID);

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            var colCount = table.rows[0].cells.length;
            
            for(var i=0; i<colCount; i++) {

                var newcell = row.insertCell(i);

                newcell.innerHTML = table.rows[3].cells[i].innerHTML.replace(new RegExp("1",'g'),counters[tableID]);
                //alert(newcell.childNodes[0].type);                
				//alert(newcell.innerHTML);
               
                switch(newcell.childNodes[0].type) {
                    case "text":                            
                            newcell.childNodes[0].value = "";
                            var txtname=newcell.childNodes[0].name;
                            var newname=txtname.substr(0,txtname.length-1)+counters[tableID];
                            newcell.childNodes[0].id=newname;
                            newcell.childNodes[0].name=newname;
                            break;
                    case "checkbox":					
                            newcell.childNodes[0].checked = false;
                            newcell.childNodes[0].id="chkMulti"+counters[tableID];
                            newcell.childNodes[0].name="chkMulti"+counters[tableID];
							if(tableID == 'tblMultiRadio')
							{	
  								newcell.childNodes[0].checked = true;
								newcell.childNodes[0].id="chkRadio"+counters[tableID];
								newcell.childNodes[0].name="chkRadio"+counters[tableID];
								newcell.childNodes[0].name="chkRadio"+counters[tableID];
								newcell.childNodes[0].value="1";
						
							//	newcell.childNodes[1].checked = false;
							//	newcell.childNodes[1].id="chkMultiR"+counters[tableID];
							//	newcell.childNodes[1].name="chkMultiR"+counters[tableID];
							
							//	alert(counters[tableID]);
								getCheckboxRadio(counters[tableID]);
								
			 				}

                            break;
                    case "select-one":
                            newcell.childNodes[0].selectedIndex = 0;
                            newcell.childNodes[0].value=counters[tableID];
                            break;
                    
                }
            }
            
        }

        function deleteRow(tableID) {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
            if(rowCount==4)
                {
                    alert('<?php echo CANNOT_DELETE_LAST ?>');
                    return;
                }
            table.deleteRow(rowCount-1);
            counters[tableID]--;
        }

        function ChangeTemplate()
        {
            DisableAllTemplates();
            var val = document.getElementById('drpTemplate').options[document.getElementById('drpTemplate').selectedIndex].value;
            
            if(val ==0)
            {
                document.getElementById('trMulti').style.display="";
            }
            else if(val==1)
            {
                  document.getElementById('trOne').style.display="";
            }
            else if(val==3)
            {
                  document.getElementById('trArea').style.display="";
            }
            else if(val==4)
            {
                  document.getElementById('trMultiText').style.display="";
            }
			else if(val ==5)
            {
                document.getElementById('trMulti').style.display="";
            }
			else if(val ==6)
            {
                document.getElementById('trMultiRadio').style.display="";
				DispayCheckboxRadio();
	
            }
			
        }

		function loadSecTemplate()
		{
			var  e = document.getElementById('SecScoringTool');
			var selected = e.options[e.selectedIndex].value;
			
		 	if(selected == 1)
			{		
				document.getElementById('tblpointTempSec').style.display="";
				ChangeTemplateSec();
	 	 	}
			else
			{
				document.getElementById('tblpointTempSec').style.display="none";
				DisableAllSecTemplates();
			}
	 	}
		
		function comparePriorityPoint()
		{
		 	var pri_point = parseInt(document.getElementById('txtPoint').value);
			var sec_point = parseInt(document.getElementById('txtPointSec').value)	;
			if(document.getElementById('txtPoint').value == '')
			pri_point = 0;
		 	if(sec_point > pri_point)
			{
				alert("Points for the secondary score should not be greater than question point [" + pri_point + "]" );
				document.getElementById('txtPointSec').focus();
				document.getElementById('txtPointSec').value = '0';
				
				return false;
	 		}
			return true;
		}
		
		function getCheckboxRadio(i)
		{
			//DispayCheckboxRadio();
		  var check = document.getElementById('chkRadio'+i).checked;
		  var chkMulti = 'chkMultiR'+i ;
		  var chkRadioSpace = 'chkRadioSpace'+i ;
 		  
		  var cRadioNo = 'cRadioNo'+i ;
		  var cRadioYes = 'cRadioYes'+i ;
		  var cRadioDiff = 'cRadioDiff'+i ;
		  var cRadioSpace = 'cRadioSpace'+i ;
		  var cRadioY = 'cRadioY'+i ;
		  var cRadioN = 'cRadioN'+i ;
		  var cRadioD = 'cRadioD'+i ;
		  var txtscore = 'txtscore'+i ;
		  var scvSpace = 'scvSpace'+i;
		  var chkscv = 'chkscv'+i; 
		//   var scvRadioYes = 'scvRadioYes'+i;
		//   var scvRadioNo = 'scvRadioNo'+i;
		//   var scvRadioN = 'scvRadioN'+i;
		//	var scvRadioY = 'scvRadioY'+i;

  
			if(check == true)
			{
				document.getElementById(chkRadioSpace).style.display="";
				document.getElementById(chkMulti).style.display="";
				document.getElementById(cRadioNo).style.display="none";
				document.getElementById(cRadioYes).style.display="none";
				document.getElementById(cRadioDiff).style.display="none";
				document.getElementById(cRadioSpace).style.display="none";
				document.getElementById(cRadioY).style.display="none";
				document.getElementById(cRadioN).style.display="none";
				document.getElementById(cRadioD).style.display="none";
				document.getElementById(txtscore).style.display="none";
				if(i<=2)
				{
					document.getElementById(scvSpace).style.display="";
					document.getElementById(chkscv).style.display="";
			/*		document.getElementById(scvRadioYes).style.display="";
					document.getElementById(scvRadioNo).style.display="";
					document.getElementById(scvRadioN).style.display="";
					document.getElementById(scvRadioY).style.display="";*/
				}
				if(i>2)
				{
					document.getElementById(scvSpace).style.display="none";
					document.getElementById(chkscv).style.display="none";
				/*	document.getElementById(scvRadioYes).style.display="none";
					document.getElementById(scvRadioNo).style.display="none";
					document.getElementById(scvRadioN).style.display="none";
					document.getElementById(scvRadioY).style.display="none"; */
				}
				
			}
			else if(check == false)
			{
				document.getElementById(chkRadioSpace).style.display="none";
				document.getElementById(chkMulti).style.display="none";
				document.getElementById(cRadioNo).style.display="";
				document.getElementById(cRadioYes).style.display="";
				document.getElementById(cRadioDiff).style.display="";
				document.getElementById(cRadioSpace).style.display="";
				document.getElementById(cRadioY).style.display="";
				document.getElementById(cRadioN).style.display="";
				document.getElementById(cRadioD).style.display="";
				document.getElementById(txtscore).style.display="";
				if(i<=2)
				{
					document.getElementById(scvSpace).style.display="none";
					document.getElementById(chkscv).style.display="none";
					/* document.getElementById(scvRadioYes).style.display="none";
					document.getElementById(scvRadioNo).style.display="none";
					document.getElementById(scvRadioN).style.display="none";
					document.getElementById(scvRadioY).style.display="none"; */
				}
			}
		}
		
		function DispayCheckboxRadio()
		{
			var noofrow = <?php echo $answers_count; ?>;
		//	var noofrow = document.getElementById('chkRadio').length();
			for(var i=1;i<=noofrow;i++)
			{ 
			 	var chkRadio ='chkRadio'+i;
				var check = document.getElementById('chkRadio'+i).checked;
				  var chkRadioSpace = 'chkRadioSpace'+i ;		   
		  		var chkMulti = 'chkMultiR'+i ;
		 		 var cRadioNo = 'cRadioNo'+i ;
				  var cRadioYes = 'cRadioYes'+i ;
				   var cRadioDiff = 'cRadioDiff'+i ;
				  var cRadioSpace = 'cRadioSpace'+i ;
				  var cRadioY = 'cRadioY'+i ;
				  var cRadioN = 'cRadioN'+i ;
				   var cRadioD = 'cRadioD'+i ;
				  var txtscore = 'txtscore'+i ;
				  var scvSpace = 'scvSpace'+i;
				   var chkscv = 'chkscv'+i; 
				//   var scvRadioYes = 'scvRadioYes'+i;
			//	   var scvRadioNo = 'scvRadioNo'+i;
			//	   var scvRadioN = 'scvRadioN'+i;
			//		var scvRadioY = 'scvRadioY'+i;
		
				 if(check == true)
				{
					document.getElementById(chkMulti).style.display="";
					document.getElementById(chkRadioSpace).style.display="";
					 document.getElementById(cRadioNo).style.display="none";
					 document.getElementById(cRadioDiff).style.display="none";
					document.getElementById(cRadioYes).style.display="none";
					document.getElementById(cRadioSpace).style.display="none";
					document.getElementById(cRadioY).style.display="none";
					document.getElementById(cRadioN).style.display="none";
					document.getElementById(cRadioD).style.display="none";
					document.getElementById(txtscore).style.display="none";
					if(i<=2)
					{
						document.getElementById(scvSpace).style.display="";
						document.getElementById(chkscv).style.display="";
						/*document.getElementById(scvRadioYes).style.display="";
						document.getElementById(scvRadioNo).style.display="";
						document.getElementById(scvRadioN).style.display="";
						document.getElementById(scvRadioY).style.display="";
		*/			
					}
 				}
				else if(check == false)
				{
					document.getElementById(chkRadioSpace).style.display="none";
					document.getElementById(chkMulti).style.display="none";
					document.getElementById(cRadioNo).style.display="";
					document.getElementById(cRadioYes).style.display="";
					 document.getElementById(cRadioDiff).style.display="";
					document.getElementById(cRadioSpace).style.display="";
					document.getElementById(cRadioY).style.display="";
					document.getElementById(cRadioN).style.display="";
					document.getElementById(cRadioD).style.display="";
					document.getElementById(txtscore).style.display="";
					if(i<=2)
					{
						document.getElementById(scvSpace).style.display="none";
						document.getElementById(chkscv).style.display="none";
						/*document.getElementById(scvRadioYes).style.display="none";
						document.getElementById(scvRadioNo).style.display="none";
						document.getElementById(scvRadioN).style.display="none";
						document.getElementById(scvRadioY).style.display="none";  */
					}
				}
		 	}
		
		}
		function ChangeTemplateSec()
        {
			DisableAllSecTemplates();
		  	var e1 = document.getElementById('drpTemplateSec');
			var selectedTemp = e1.options[e1.selectedIndex].value;
			
			if(selectedTemp ==0)
			{
				document.getElementById('trMultiSec').style.display="";
			}
			else if(selectedTemp==1)
			{
				  document.getElementById('trOneSec').style.display="";
			}
			else if(selectedTemp==3)
			{
				  document.getElementById('trAreaSec').style.display="";
			}
			else if(selectedTemp==4)
			{
				  document.getElementById('trMultiTextSec').style.display="";
			}
		}
       
		
        function DisableAllTemplates()
        {
            document.getElementById('trMulti').style.display="none";
            document.getElementById('trOne').style.display="none";
            document.getElementById('trArea').style.display="none";
            document.getElementById('trMultiText').style.display="none";
			document.getElementById('trMultiRadio').style.display="none";
        }
		function DisableAllSecTemplates()
        {
            document.getElementById('trMultiSec').style.display="none";
            document.getElementById('trOneSec').style.display="none";
            document.getElementById('trAreaSec').style.display="none";
            document.getElementById('trMultiTextSec').style.display="none";
        }

    </SCRIPT>
</form>

