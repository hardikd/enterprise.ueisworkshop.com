<?php	
    class qst_viewer
    {
        var $html = "";
        var $show_next = true;
        var $show_prev = true;
        var $page_name = "";
        var $show_finish = false;
        var $show_correct_answers = false;

        var $read_only_text = "";

        var $user_quiz_id=-1;
        var $ans_priority="1";
        var $control_unq;
        var $ids = "";

        public function qst_viewer($page)
        {
            $this->page_name = $page;
        }

        public function BuildQuestion($row,$totalPages,$asg_id)
		{      
			 $template = qst_viewer::ReadTemplate();
			  $noOfQues = count($row);   
			 //$widthSize = round(100/$noOfQues);
			 //$widthSize = $widthSize."%";
			 $template_start ='<table align="left" cellpadding="5" cellspacing="5" border="0" width="100%">';
				
				if($_REQUEST['pageId'] == '')
				{
					$_REQUEST['pageId'] = 1;
				}
				if($_REQUEST['pageId']<$totalPages)
				{
					$next_page = intval($_REQUEST['pageId'])+1;
					$button_value = "Save and Next";
					$button_name = "save_and_next";
				}  
				else //if($_REQUEST['pageId']>=$totalPages)
				{
					$next_page = $totalPages;
					$button_value = "Finish";
					$button_name = "finished";
				}
				
				if($totalPages ==1)
				{
					$next_page = $totalPages;
					$button_value = "Finish";
					$button_name = "finished";
				}
				if($_REQUEST['pageId'] == '')
				{
					$next_page = 2;
				}
				if($_REQUEST['pageId']!=1)
				{
					$prev_page = intval($_REQUEST['pageId'])-1;
			 	}  
				else
				{
					$prev_page = 1;
				}
			 for($i = 0 ;$i<$noOfQues;$i++)
			{ 
				if($this->user_quiz_id>-1)
				{
					$ans_results = questions_db::GetAnswersByQstID2($row[$i]['id'],$this->user_quiz_id);
					$ans_results_sec = questions_db::GetAnswersByQstID2sec($row[$i]['id'],$this->user_quiz_id);
				}
				else
				{
					$ans_results = questions_db::GetAnswersByQstID($row[$i]['id']);
					$ans_results_sec = questions_db::GetAnswersByQstIDsec($row[$i]['id']);
					
				} 
				$pri_que_type = $row[$i]['question_type_id'];
				$sec_que_type = $row[$i]['sec_question_type_id'];
	 	 	 	$answers_html = $this->BuildAnswers($ans_results,$ans_results_sec,$pri_que_type,$sec_que_type,$row[$i]['id']);
				
				$headerTitle = $row[$i]["header_text"];
				$questionTitle = $row[$i]["question_text"];
				$group_name = $row[$i]["group_name"];
				$footer_text = $row[$i]["footer_text"];
				$template_body .='
				<tr width="100%" ><td>
					<table width="100%" >';
					if($footer_text!="")
				    {	
						$template_body .='<tr><td valign="top" >'.$headerTitle.'</td></tr>';
					}	
                $template_body .='<tr>
                        <td> 
                            '.$questionTitle.'
                        </td>
                    </tr>    
                    <tr>
                        <td align="left" valign="top">
                           <table border="0"  width="100%">
                                <tr>
                                    <td  colspan="2">
                                        '.$group_name.'
                                    </td>
                                </tr>
                                '.$answers_html.'
                            </table>            
                        </td>
                    </tr>';
				    if($footer_text!="")
				    { 
						$template_body .='<tr>
							<td valign="top">'.$footer_text.'
							</td></tr>';
					}
					$template_body .='</table></td></tr>';
				}
			 
				$pagination = $this->getRecords($totalPages,$asg_id);
				$template_end = "<tr><td align='center'>".$pagination."</td></tr>";				
						
				$template_end .='</table>';
	
				$template = $template_start.$template_body.$template_buttn.$template_end;
	   
    		$this->html =  $template;
	 		return $row[0];
	   }
	   function getRecords($totalPages,$asg_id)
	   {
			return $pagination;
	   }
		
		public static function get_answer_text($answer_text)
		{
			$results = $answer_text;
			if(USE_MATH=="yes")
			{
				$results=mathfilter($answer_text,"14","mathpublisher/img/");				
			}
			return $results;
		}
        
        public function BuildQuestionWithQuery($query)
        {
             $rows = db::exec_sql($query);
             $row=db::fetch($rows);
             $this->BuildQuestion($row);
        }

        public function BuildQuestionWithResultset($resultset,$totalPages,$asg_id)
        {
           $this->BuildQuestion($resultset,$totalPages,$asg_id); 
        }
        
        public function GetIDS()
        {
            $ids = $this->ids;
            if($ids!="")
            {
                $ids = substr($ids,1); 
            }
            return $ids;
        }
   			     
        public function BuildAnswers($ans_results,$ans_results_sec,$question_type,$sec_que_type,$qid)
        {
             $control_unq = $this->control_unq;             
             $answers_html="";
           //	$IntCountQue=1;
             while($row=db::fetch($ans_results))
             {  
			   $correct_answer = "";
                  $answers_val="";
                  $answer_desc = $row['answer_text'];
                  $id= $row['a_id'];
                  $this->ids .= ",$id";
                  switch($question_type) {
                  case 0:
				  		$answers_html.="<ul>";                      
                  	  if($this->show_correct_answers==true && $row['correct_answer']=="1")
					 {	
						$correct_answer = "$tabs [".CORRECT_ANSWER."]";
						$Attr='style="color: black;"';
					 }
					 else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					    $answers_val = "checked";
						$Attr='style="color: red;"';
					 }
					  else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					 	$Attr='style="color: green;"';
					 }
                      $answers_html.= "<li ".$Attr.">".$row['answer_text'].$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                  case 1:
				  		$answers_html.="<ul>"; 
                      if($this->show_correct_answers==true && $row['correct_answer']=="1")
					 {	
						$correct_answer = "$tabs [".CORRECT_ANSWER."]";
						$Attr='style="color: black;"';
					 }
					 else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					    $answers_val = "checked";
						$Attr='style="color: red;"';
					 }
					  else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					 	$Attr='style="color: green;"';
					 }
                      $answers_html.= "<li ".$Attr.">".$row['answer_text'].$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                  case 3:
				  		$answers_html.="<ul>"; 
                      if($this->show_correct_answers==true) $correct_answer = "<br>".CORRECT_ANSWER." : ".$row['correct_answer_text']."";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
					  $answers_html.= "<li>".$answers_val.$row['answer_text']."  ".$answers_val."  ".$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                  case 4:
				  		$answers_html.="<ul>"; 
                      if($this->show_correct_answers==true) $correct_answer = "$tabs<font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.= "<li>".$answers_val."  ".$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
				  
				   case 5:
				  		$answers_html.="<ul>"; 
                      if($this->show_correct_answers==true && $row['correct_answer']=="1")
					 {	
						$correct_answer = "$tabs [".CORRECT_ANSWER."]";
						$Attr='style="color: black;"';
					 }
					 else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					    $answers_val = "checked";
						$Attr='style="color: red;"';
					 }
					  else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					 	$Attr='style="color: green;"';
					 }
                      $answers_html.= "<li ".$Attr.">".$row['answer_text'].$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
			 	 
				   case 6:
				  	 if($row['radio_ans']!= 1)
					 {
						$answers_html.="<ul>";                      
						  if($this->show_correct_answers==true && $row['correct_answer']=="1")
						 {	
							$correct_answer = "$tabs [".CORRECT_ANSWER."]";
							$Attr='style="color: black;"';
						 }
						 else
						 {
							$Attr='style="color: black;"';
						 }
						 if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
						 {
							$answers_val = "checked";
							$Attr='style="color: red;"';
						 }
						  else
						 {
							$Attr='style="color: black;"';
						 }
						 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
						 {
							$Attr='style="color: green;"';
						 }
						// echo"<pre>";
						// print_r($row);
						$s_c_v_text ='';
						if($this->user_quiz_id>-1 && $row['user_answer_id']!="" && $row['selfcorrectsvowel'] == 1)
						{
							$s_c_v_text = SELF_CORRECTS_VOWEL;
							if($row['self_c_vowel'] =='1')
							{
								$s_c_v_text .= " - Yes";
							}elseif($row['self_c_vowel'] == '0')
							{
								$s_c_v_text .= " - No";
							}else{$s_c_v_text .= '';}	
						}
						  $answers_html.= "<li ".$Attr.">".$row['answer_text'].$correct_answer."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;".$s_c_v_text."</li>";
						  $answers_html.="</ul>"; 
					 }
					 else if($row['radio_ans']== 1)
					 {
					 	 $answers_html.="<ul>"; 
						  if($this->show_correct_answers==true && $row['correct_answer']=="1")
						 {	
							$correct_answer = "$tabs [".CORRECT_ANSWER."- Yes]";
							$Attr='style="color: black;"';
						 }
						 else if($this->show_correct_answers==true && $row['correct_answer']=="0")
						 {	
							$correct_answer = "$tabs [".CORRECT_ANSWER."- No]";
							$Attr='style="color: black;"';
						 }
						 else
						 {
							$Attr='style="color: black;"';
						 }
						
						 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
						 {
							$Attr='style="color: green;"';
						 }
						  if($this->show_correct_answers==true && $row['correct_answer']=="0" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
						 {
							$Attr='style="color: green;"';
						 }
						  if($this->user_quiz_id>-1 && $row['user_answer_id']!="" && ($row['correct_answer']!=$row['user_answer_text']))
						 {
							$answers_val = "checked";
							$Attr='style="color: red;"';
						 }
						  $answers_html.= "<li ".$Attr.">".$row['answer_text'].$correct_answer."</li>";
						  $answers_html.="</ul>"; 
                  
					 } 
                  break;
				 }
             }
			 
			 if($ans_results_sec->num_rows >0)
			 {
				 $answers_html.= "<tr><td colspan ='2'> Scoring Answers: </td> </tr>";
				 $answers_html.= "<tr><td colspan ='2'>&nbsp;</td> </tr>";
			 }
			 while($row=db::fetch($ans_results_sec))
             {      
			
			 	  $correct_answer = "";
                  $answers_val="";
                  $answer_desc = $row['answer_text'];
                  $id= $row['a_id'];
                  $this->ids .= ",$id";
				   
                  switch($sec_que_type) {
                  case 0: 
				  		$answers_html.="<ul type='square'>"; 	                     
                       if($this->show_correct_answers==true && $row['correct_answer']=="1")
					 {	
						$correct_answer = "$tabs [".CORRECT_ANSWER."]";
						$Attr='style="color: black;"';
					 }
					 else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					    $answers_val = "checked";
						$Attr='style="color: red;"';
					 }
					  else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					 	$Attr='style="color: green;"';
					 }
                      $answers_html.= "<li ".$Attr." >".$row['answer_text'].$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                  case 1:
				 	  $answers_html.="<ul>"; 
                       if($this->show_correct_answers==true && $row['correct_answer']=="1")
					 {	
						$correct_answer = "$tabs [".CORRECT_ANSWER."]";
						$Attr='style="color: black;"';
					 }
					 else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					    $answers_val = "checked";
						$Attr='style="color: red;"';
					 }
					  else
					 {
					 	$Attr='style="color: black;"';
					 }
					 if($this->show_correct_answers==true && $row['correct_answer']=="1" && $this->user_quiz_id>-1 && $row['user_answer_id']!="")
					 {
					 	$Attr='style="color: green;"';
					 }
                      $answers_html.=  "<li ".$Attr." >".$row['answer_text'].$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                  case 3:
				 	  $answers_html.="<ul>"; 
                      if($this->show_correct_answers==true) $correct_answer = "<br><font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.=  "<li>".$row['answer_text']."  ".$answers_val."  ".$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                  case 4:
				 	  $answers_html.="<ul>"; 
                      if($this->show_correct_answers==true) $correct_answer = "$tabs<font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.=  "<li>".$row['answer_text']."  ".$answers_val."  ".$correct_answer."</li>";
					  $answers_html.="</ul>"; 
                  break;
                        }
             }
             return $answers_html;
        }


        public static function ReadTemplate()
        {
            $file = file_get_contents('tmps/question_templatepdf.xml', true);
            return $file;
        }

        public function SetReadOnly()
        {
            $this->read_only_text="disabled";
        }

        public function GetPriority()
         {
             $priority = 1;
             if(isset($_POST['hdnPriority']))
             {
                 $priority = intval($_POST['hdnPriority']);
             }
             return $priority;
         }

        public function GetNextPriority()
         {
             $priority = 1;
             if(isset($_POST['next_priority']))
             {
                 $priority = $_POST['next_priority'];
             }
             return $priority;
         }

         public function GetPrevPriority()
         {
             $priority = 1;
             if(isset($_POST['prev_priority']))
             {
                 $priority = $_POST['prev_priority'];
             }
             return $priority;
         }
		 //Start code Bhavesh.rakhshiya@maven-infotech.com Note: Don't modify below function
		 public function BuildQuestionOfAnswerWithResultset($resultset)
        {
		error_reporting(0);
		
            $this->BuildQuestionAnswer($resultset);
        }
		  public function BuildQuestionAnswer($row)
        {         
           if($row['question_text'] !='')
		   {
		    if($this->user_quiz_id>-1)
				{
					$ans_results = questions_db::GetAnswersByQstID2($row['id'],$this->user_quiz_id);
					$ans_results_sec = questions_db::GetAnswersByQstID2sec($row['id'],$this->user_quiz_id);
				}
				else
				{
					$ans_results = questions_db::GetAnswersByQstID($row[$i]['id']);
					$ans_results_sec = questions_db::GetAnswersByQstIDsec($row[$i]['id']);
					
				}
				$question_note_text  ='';
				$question_note = questions_db::GetQuestion_note($row['id'],$this->user_quiz_id,$_SESSION['user_id']);  					                $question_note_row=db::fetch($question_note);
 
				$pri_que_type = $row['question_type_id'];
				$sec_que_type = $row['sec_question_type_id'];

            $template = qst_viewer::ReadTemplate();
            $template = str_replace("[question_text]", qst_viewer::get_answer_text($row['question_text']), $template);
            $template = str_replace("[footer_text]", $row['footer_text'], $template);
            $template = str_replace("[header_text]", $row['header_text'], $template);
			$template = str_replace("[buttons]", $row['buttons'], $template);
			$template = str_replace("[hiddens]", $row['hiddens'], $template);
            $template = str_replace("[group_name]", $row['group_name'], $template);
            $answers_html = $this->BuildAnswers($ans_results,$ans_results_sec,$row['question_type_id'],$row['sec_question_type_id'],$row['id']);
            $template = str_replace("[answers]", $answers_html, $template);
			 $template = str_replace("[notes]", $question_note_row['user_note_text'], $template);
            $this->html =  $template;
			}	
            return $row;
     }
//End code Bhavesh.rakhshiya@maven-infotech.com
 }
?>