<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("1");

 require "extgrid.php";
 require "db/users_db.php";
 require "db/asg_db.php";
 require "db/quiz_db.php";
 require "db/questions_db.php";

 
 $val = new validations("btnSave");
 $val->AddValidator("drpCats", "notequal", CATEGORY_VAL, "-1");
 $val->AddValidator("drpTests", "notequal", TEST_VAL, "-1");
 $val->AddValidator("txtSuccessP", "empty", SUCCESS_VAL,"");
 $val->AddValidator("txtTestTime", "empty", TIME_VAL,"");
 $val->AddValidator("txtSuccessP", "numeric", SUCCESS_NUM_VAL,"");
 $val->AddValidator("txtTestTime", "numeric", TIME_NUM_VAL,"");

	
$selected = "-1";
$selected_test = "-1";
$selected_test_bnk = "-1";
$selected_type = "-1";
$selected_showres = "-1";
$selected_results = "-1";
$selected_q_order = "-1";
$selected_a_order = "-1";
$selected_review = "-1";
$selected_qtype ="-1";
$selected_qchnage = "-1";
$selected_send = "-1";
$selected_enablenew_options = "-1";
$asg_quiz_type="1";
$divQstBank = "";

$local_user_ids = array();
$imp_user_ids= array();
$bank_questions_ids = array();

	$district_id = $_SESSION['district_id'];
		
	  $db = new db();
	  $sql='SELECT * FROM dist_subjects where district_id="'.$district_id.'"';
	  $subjects_rows = db::GetResultsAsArray($sql);
	//echo $_GET['q_id'];	 
		 
//Hitesh.patel@maven-infotech.com
if($_POST["quizId"]!='')
{	
		$sql='SELECT * FROM questions where quiz_id ='.$_POST["quizId"].' order by priority';
		$subjects_rows = db::GetResultsAsArray($sql);
		$intQuestions = count($subjects_rows);
		$perpageQue = $_REQUEST['intPages'];
		//echo"<pre>";
		//print_r($subjects_rows);
		$intStopparaQues = $intQuestions - $perpageQue;  
		
		 $rowTextbox ='<table>';
		 $page = 0;
		 for($intCount = 0; $intCount<$intStopparaQues;$intCount+=$perpageQue)   //intStopparaQues
		 {  
		 	$page++;
		 	if($subjects_rows[$intCount]['question_type_id'] == 6)
			{ 
			 	$query  = "select aq.* from answers_quiz aq , question_groups qg where aq.group_id = qg.id and qg.is_secondary_group = 0 and qg.question_id = ".$subjects_rows[$intCount]['id']." ORDER BY aq.id";
				$ans_rows = db::GetResultsAsArray($query);
				$intAns_row= count($ans_rows);
				$rowTextbox .='<tr><td width="290px"><label class="desc_text"> Stop Parameter for Page '.$page.' : </label></td>';
				$rowTextbox .='<td style="width:170px">';
			 	$chkText = 0;$rid =0; $scvid = 0;
				 for($intC = 0; $intC<$intAns_row; $intC++)
				 {
			 	 	if($ans_rows[$intC]['radio_ans'] == 0)
					{
					 	if($chkText == 0)
						{ 
							$chkText++;
							$rowTextbox .='&nbsp;<input style="width:25px"  type="text" placeholder="CH"  maxlength="1" name="txtstopPara'.$page.'_chk" id="txtstopPara'.$page.'_chk" />';
							
						}
					}
					elseif($ans_rows[$intC]['radio_ans'] == 1)
					{
						$rid = $rid+1;
						$rowTextbox .='&nbsp;<input style="width:25px"  type="text" maxlength="1" placeholder="R'.$rid.'" name="txtstopPara'.$page.'_radio_'.$rid.'" id="txtstopPara'.$page.'_radio_'.$rid.'" />';
					}
					elseif($ans_rows[$intC]['radio_ans'] == 2)
					{
						$rowTextbox .='&nbsp;<input style="width:28px"  type="text" maxlength="1" placeholder="DIFF" name="txtstopPara'.$page.'_difficulty" />';
					}
					if($ans_rows[$intC]['selfcorrectsvowel'] == 1)
					{
						$scvid = $scvid +1;
						$rowTextbox .='&nbsp;<input style="width:26px"  type="text" maxlength="1" placeholder="SC'.$scvid.'" name="txtstopPara'.$page.'_scv_'.$scvid.'" id="txtstopPara'.$page.'_scv_'.$scvid.'" />';
					}
					
				}	 
				$rowTextbox .= '<input type="hidden" name="stopPara'.$page.'" value="stopPara'.$page.'" /><input type="hidden" name="radioAnsCountForPage'.$page.'" value="'.$rid.'" /></td></tr>';
			}
 			else
			{ 
			$rowTextbox .='<tr><td width="290px"><label class="desc_text"> Stop Parameter for Page '.$page.' : </label></td><td style="width:170px"><input style="width:170px"  type="text" maxlength="1"  name="txtstopPara'.$page.'" id="txtstopPara'.$page.'" /><input type="hidden" name="stopPara'.$page.'" value="stopPara'.$page.'" /></td></tr>';
			}
		 }
	 	 $rowTextbox .='<input type="hidden" name="countPage" value="'.$page.'" /></table>';
		 echo $rowTextbox;
		exit;
	
}

//End		 
if(isset($_GET["id"]))
{
        $id = util::GetID("?module=assignments");
        $rs_asg=asgDB::GetAsgQueryById($id);

        if(db::num_rows($rs_asg) == 0 ) header("location:?module=assignments");

        $row_asg=db::fetch($rs_asg);

        $selected = $row_asg["cat_id"];
        $selected_test= $row_asg["org_quiz_id"];
        $selected_test_bnk = $selected_test;
        $copied_quiz_id= $row_asg["quiz_id"];        
        $selected_type = $row_asg["quiz_type"];
        $selected_showres= $row_asg["show_results"];
        $selected_results =$row_asg["results_mode"];
		 $selected_per_pages = $row_asg["per_page_question"];
        $selected_q_order =$row_asg["qst_order"];
        $selected_a_order =$row_asg["answer_order"];
        $selected_review=$row_asg["allow_review"];
	$selected_qtype =$row_asg["limited"];
	$selected_qchnage = $row_asg["affect_changes"];
	$selected_send = $row_asg["send_results"];
	$selected_enablenew_options = $row_asg["accept_new_users"];
        $txtSuccessP = $row_asg["pass_score"];
        $txtTestTime = $row_asg["quiz_time"];
        $txtAssignmentName=$row_asg["assignment_name"];
        $asg_quiz_type=$row_asg["asg_quiz_type"];
	     
		 $subject_id=$row_asg["subject_id"];
	      $createtestdate=$row_asg["createtestdate"];
	  
	 
       
        $local_user_ids=db::GetResultsAsArrayByColumn(orm::GetSelectQuery("assignment_users", array(), array("assignment_id"=>$id, "user_type"=>"1"), ""), "user_id");
        $imp_user_ids=db::GetResultsAsArrayByColumn(orm::GetSelectQuery("assignment_users", array(), array("assignment_id"=>$id, "user_type"=>"2"), ""), "user_id");

        if($row_asg["asg_quiz_type"]=="2")
        {
            $selected_test_bnk="-100";   
            $bank_questions_ids= db::GetResultsAsArrayByColumn(orm::GetSelectQuery("questions", array(), array("quiz_id"=>$copied_quiz_id), "id"), "parent_id");           
            $divQstBank = GetQuestionsBank();
        }

}

$type_options = webcontrols::BuildOptions(array("1"=>O_QUIZ, "2"=>O_SURVEY,"3"=>O_ASSESSMENT), $selected_type);
$show_per_page_questions = webcontrols::BuildOptions(array("1"=>1, "2"=>2,"3"=>3,"4"=>4,"5"=>5), $selected_per_pages);
$showres_options = webcontrols::BuildOptions(array("1"=>O_YES, "2"=>O_NO), $selected_showres);
$review_options = webcontrols::BuildOptions(array("2"=>O_NO,"1"=>O_YES), $selected_review);
$result_options = webcontrols::BuildOptions(array("1"=>O_POINT, "2"=>O_PERCENT), $selected_results);
$questions_order_options=webcontrols::BuildOptions(array("1"=>BY_PRIORITY, "2"=>RANDOM), $selected_q_order);
$answers_order_options=webcontrols::BuildOptions(array("1"=>BY_PRIORITY, "2"=>RANDOM), $selected_a_order);
$qtype_options = webcontrols::BuildOptions(array("1"=>ASG_ONCE, "2"=>ASG_NO_LIMIT), $selected_qtype);
$qchange_options = webcontrols::BuildOptions(array("2"=>DONT_AFFECT, "1"=>AFFECT), $selected_qchnage);  
$sending_options = webcontrols::BuildOptions(array("2"=>ASG_SEND_MAN, "1"=>ASG_SEND_AUTO), $selected_send); 
$enablenew_options = webcontrols::BuildOptions(array("2"=>O_NO, "1"=>O_YES), $selected_enablenew_options);  


$results = orm::Select("cats", array(), array('district_id'=>$district_id),"");
$cat_options = webcontrols::GetOptions($results, "id", "cat_name", $selected);
//$cat_options = webcontrols::AddOptions($cat_options, "-100", QUESTIONS_BANK, "");

$chk_all_html = "<input type=checkbox name=chkAll2 onclick='grd_select_all(document.getElementById(\"form1\"),\"chklcl\",\"this.checked\")'>";
$hedaers = array($chk_all_html,LOGIN,USER_NAME,USER_SURNAME,EMAIL,'Grade');
$columns = array("UserName"=>"text", "Name"=>"text","Surname"=>"Surname","email"=>"text","grade_name"=>"text");

$grd = new extgrid($hedaers,$columns, "index.php?module=add_assignment");
$grd->delete=false;
$grd->edit=false;
$grd->checkbox=true;
$grd->id_column="UserID";
$grd->selected_ids=$local_user_ids;
$grd->chk_class="chklcl";

$query = users_db::GetUsersQuery("where disabled=0 and approved=1 and user_type=2");
$grd->DrowTable($query);
$grid_html = $grd->table;


$chk_all_html = "<input type=checkbox name=chkAll2 onclick='grd_select_all(document.getElementById(\"form1\"),\"chkimp\",\"this.checked\")'>";
$hedaers_imp = array($chk_all_html,LOGIN,USER_NAME,USER_SURNAME);
$columns_imp = array("UserName"=>"text", "Name"=>"text","Surname"=>"Surname");

$grd_imp = new extgrid($hedaers_imp,$columns_imp, "index.php?module=add_assignment");
$grd_imp->delete=false;
$grd_imp->edit=false;
$grd_imp->checkbox=true;
$grd_imp->id_column="UserID";
$grd_imp->identity="imp";
$grd_imp->selected_ids=$imp_user_ids;
$grd_imp->chk_class="chkimp";

$query_imp = users_db::GetImportedUsersQuery();
$grd_imp->DrowTable($query_imp);
$imported_grid_html = $grd_imp->table;




if(isset($_POST["btnSave"]) && $val->IsValid())
{
	
    $db = new db();
    $db->connect();
    $db->begin();

    $selected_quiz_type=$_POST["drpType"];
    $selected_show_res = $_POST["drpShowRes"];
    if($selected_quiz_type=="2") $selected_show_res="2";
    try
    {
        $org_quiz_id=$_POST["drpTests"];
        if(!isset($_GET["id"]))
        {
            if($org_quiz_id!="-100")
            {
                if($_POST["drpQChange"] == "2")
                $quiz_id = CopyQuiz($org_quiz_id);
				
				
				
                else $quiz_id = $org_quiz_id;
            }
            else
            {
                $quiz_id = CreateQuizFromBank();
            }
		

            $query = orm::GetInsertQuery("assignments", array("quiz_id"=>$quiz_id,
                                                   "results_mode"=>$_POST["drpResultsBy"],
                                                   "added_date"=>util::Now(),
												   "per_page_question"=>$_POST["drpQueperpage"],
                                                   "quiz_time"=>trim($_POST["txtTestTime"]),
                                                   "show_results"=>$selected_show_res,
                                                   "org_quiz_id"=>$org_quiz_id,
                                                   "pass_score"=>$_POST["txtSuccessP"],
                                                   "quiz_type"=>$selected_quiz_type,
                                                    "quiz_type"=>$_POST["drpType"],
                                                    "status"=>"0",
                                                    "qst_order"=>$_POST["drpQO"],
                                                    "answer_order"=>$_POST["drpAO"],
                                                    "allow_review"=>$_POST["drpAR"],
						    "limited"=>$_POST["drpQType"],
                                                    "affect_changes"=>@$_POST["drpQChange"],
                                                    "send_results"=>$_POST["drpSendRes"],
                                                    "accept_new_users"=>$_POST["drpNewUsers"],
                                                    "assignment_name"=>$_POST['txtAssignmentName'],
                                                    "asg_quiz_type"=>$org_quiz_id=="-100" ? 2 : 1,
													"subject_id"=>$_POST['selectsubject'],
													"createtestdate"=>$_POST['checkIn'],
													"district_id"=>$district_id
                                                   ));
            $db->query($query);
		  $asg_id = $db->last_id();
				 $db->commit();
          
			
			for($i=1;$i<=$_POST["countPage"];$i++)
			{
			   if(isset($_POST["txtstopPara".$i])&& $_POST["txtstopPara".$i]!='')    // if(isset($_POST["stopPara".$i])) 
			   {
			   	 	$query_stop_para = "insert into `quiz_stop_parameter` ( `id` ,`assignment_id` ,`quiz_id` ,`page_no` ,`stop_parameter`) VALUES('',".$asg_id.",".$quiz_id." ,".$i." ,".$_POST["txtstopPara".$i].")"; 
					 $db->query($query_stop_para);
					  $db->commit();
				}
				else 
				{
					if(isset($_POST["txtstopPara".$i."_chk"]) && $_POST["txtstopPara".$i."_chk"]!='') 
				   {
						 $query_stop_para = "insert into `quiz_stop_parameter` ( `id` ,`assignment_id` ,`quiz_id` ,`page_no` ,`stop_parameter`,`ans_type`) VALUES('',".$asg_id.",".$quiz_id." ,".$i." ,".$_POST["txtstopPara".$i."_chk"].",'c')"; 
						 $db->query($query_stop_para);
						 $db->commit();
						// echo"<br>";
					}
					
					if(isset($_POST["txtstopPara".$i."_difficulty"]) && $_POST["txtstopPara".$i."_difficulty"]!='') 
				   {
						 $query_stop_para = "insert into `quiz_stop_parameter` ( `id` ,`assignment_id` ,`quiz_id` ,`page_no` ,`stop_parameter`,`ans_type`) VALUES('',".$asg_id.",".$quiz_id." ,".$i." ,".$_POST["txtstopPara".$i."_difficulty"].",'d')"; 
						 $db->query($query_stop_para);
						 $db->commit();
						// echo"<br>";
					}
					
				 	if(isset($_POST["radioAnsCountForPage".$i]) && $_POST["radioAnsCountForPage".$i]!='') 
				  	 { 
						for($r = 1;$r<=$_POST["radioAnsCountForPage".$i];$r++)
						{ 
							$rdStopPara = 0;
					 		if(isset($_POST["txtstopPara".$i."_radio_".$r])&& $_POST["txtstopPara".$i."_radio_".$r]!='')
							{
								$rdStopPara = $_POST["txtstopPara".$i."_radio_".$r];
							}
						 		$anstype = 'r'.$r;
					 			$query_stop_para = "insert into `quiz_stop_parameter` ( `id` ,`assignment_id` ,`quiz_id` ,`page_no` ,`stop_parameter`,`ans_type`) VALUES('',".$asg_id.",".$quiz_id." ,".$i." ,".$rdStopPara.",'".$anstype."')"; 
					 			 $db->query($query_stop_para);
								  $db->commit();
						}
					 }
	 
	 				 for($s = 1;$s<=2;$s++)
					{ 
						if(isset($_POST["txtstopPara".$i."_scv_".$s]))
						{
							$scvStopPara = 0;
							if(isset($_POST["txtstopPara".$i."_scv_".$s])&& $_POST["txtstopPara".$i."_scv_".$s]!='')
							{
								$scvStopPara = $_POST["txtstopPara".$i."_scv_".$s];
							}
							$anstype = 's'.$s;
							$query_stop_para = "insert into `quiz_stop_parameter` ( `id` ,`assignment_id` ,`quiz_id` ,`page_no` ,`stop_parameter`,`ans_type`) VALUES('',".$asg_id.",".$quiz_id." ,".$i." ,".$scvStopPara.",'".$anstype."')"; 
							 $db->query($query_stop_para);
							  $db->commit();
						}
					}
					
				 }  // end if
 	  		}    //end for loop
        }  // end  if isset($_GET["id"]
		 
        else
        {
             
             global $copied_quiz_id,$selected_test;      
	     if($copied_quiz_id!=$selected_test) $db->query(quizDB::DeleteChildQuizByIdQuery($copied_quiz_id));
           
             if($org_quiz_id!="-100")
             {
                if($_POST["drpQChange"] == "2")
                $quiz_id = CopyQuiz($org_quiz_id);
                else $quiz_id = $org_quiz_id;
             }
             else
             {
                $quiz_id = CreateQuizFromBank();
             }
 
             $query = orm::GetUpdateQuery("assignments", array("quiz_id"=>$quiz_id,
                                                   "results_mode"=>$_POST["drpResultsBy"],
                                                   //"added_date"=>util::Now(),
												    "per_page_question"=>$_POST["drpQueperpage"],
                                                   "quiz_time"=>trim($_POST["txtTestTime"]),
                                                    "org_quiz_id"=>$org_quiz_id,
                                                   "show_results"=>$selected_show_res,
                                                   "pass_score"=>$_POST["txtSuccessP"],
                                                   "quiz_type"=>$selected_quiz_type,
                                                    "quiz_type"=>$_POST["drpType"],
                                                    "qst_order"=>$_POST["drpQO"],
                                                    "answer_order"=>$_POST["drpAO"],
                                                    "allow_review"=>$_POST["drpAR"],
						    "limited"=>$_POST["drpQType"],
                                                    "affect_changes"=>$_POST["drpQChange"],
                                                    "send_results"=>$_POST["drpSendRes"],
						    "accept_new_users"=>$_POST["drpNewUsers"],
                                                     "assignment_name"=>$_POST['txtAssignmentName'],
                                                    "asg_quiz_type"=>$org_quiz_id=="-100" ? 2 : 1,
													"subject_id"=>$_POST['selectsubject'],
													"createtestdate"=>$_POST['checkIn']
                                                   // "status"=>"1" ,
                                                   ) ,
                                                   array("id"=>$id,"district_id"=>$district_id)
                                         );
            $db->query($query);
			 $db->commit();
            $asg_id = $id;
           
			for($i=1;;$i++)
			{
			   if(isset($_POST["txtstopPara".$i])) 
			   {
			   	 	$query_stop_para = "insert into `quiz_stop_parameter` ( `id` ,`assignment_id` ,`quiz_id` ,`page_no` ,`stop_parameter`)
					VALUES('',".$asg_id.",".$quiz_id." ,".$i." ,".$_POST["txtstopPara".$i].")";
					 $db->query($query_stop_para);
					  $db->commit();
			   }
			   else	
			   {
			   		break;
				}
 			}
           
            $db->query(orm::GetDeleteQuery("assignment_users", array("assignment_id"=>$asg_id)));
            //$db->query(questions_db::GetGroupDeleteQuery($question_id));
			 $db->commit();
        }
		
/**************************************************************/
$db_class = new db();
$answer=@$_REQUEST['good_imp'];
if(!empty($answer)){
$ans=implode(',',$answer);
$sql_room="select * from  teacher_students where class_room_id IN ($ans)";
$room_rows = db::GetResultsAsArray($sql_room);
$studentClassRooms=array();
foreach($room_rows as $key=>$val)
{
	  $studentClassRooms[]=$room_rows[$key]['student_id'];
}
$abc['better']=array_unique($studentClassRooms);
$reply=@$abc['better'];
if(!empty($reply)){
$ans_reply=implode(',',$reply);
$answer_room="select * from   users where student_id IN ($ans_reply)";
$better_rows = db::GetResultsAsArray($answer_room);
$dumpRooms=array();
foreach($better_rows as $key11=>$val)
{
	  $dumpRooms[]=$better_rows[$key11]['UserID'];
}
$guest['chkgrd']=$dumpRooms;
//echo '<pre>';
//print_r($guest);		
//print_r($_REQUEST);
        //$chkgrd=$_POST['chkgrd'];
		$chkgrd = $guest;
		
		$chkgrd = $chkgrd['chkgrd'];
}
/**************************************************************/	
        while (list ($key,$val) = @each ($chkgrd))
        {
            $query_lcl = orm::GetInsertQuery("assignment_users", array("assignment_id"=>$asg_id,
                                                                       "user_type"=>"1",
                                                                       "user_id"=>$val
                                            ));
            $db->query($query_lcl);
        }

        $chkgrdimp=$_POST['chkgrdimp'];
		
        while (list ($key,$val) = @each ($chkgrdimp))
        {
            $query_imp = orm::GetInsertQuery("assignment_users", array("assignment_id"=>$asg_id,
                                                                       "user_type"=>"2",
                                                                       "user_id"=>$val
                                            ));
            $db->query($query_imp);
        }

        $db->commit();
        header("location:?module=assignments&asg_id=$asg_id");

    }}
    catch(Exception $e)
    {
        echo $e->getMessage();
        $db->rollback();
    }
    $db->close_connection();
}



if(isset($_POST["ajax"]))
{
         if(isset($_POST["fill_tests"]))
         {             
            $cat_id=$_POST["cat_id"];            
            $results_test = orm::Select("quizzes", array(), array("cat_id"=>$cat_id,"parent_id"=>"0"),"");
            $add_options = webcontrols::BuildOptions(array("-100"=>QUESTIONS_BANK), $selected_test_bnk, "style='font-weight: bold'");
            $tests_drop = webcontrols::GetDropDown("drpTests",$results_test, "id", "quiz_name", $selected_test,$add_options);
            echo $tests_drop;
         }
         if(isset($_POST["show_qst_bank"]))
         {
            $html = GetQuestionsBank();
            echo $html;
         }
}

function GetQuestionsBank()
{
    global $bank_questions_ids;
    $chk_all_html = "<input type=checkbox name=chkAll2 onclick='grd_select_all(document.getElementById(\"form1\"),\"chk_qst\",\"this.checked\")'>";
    $hedaers = array($chk_all_html,QUESTION, TYPE, POINT, "&nbsp;");
    $columns = array( "question_text"=>"text","question_type"=>"text" ,"point"=>"text");
// for($i=0;$i<count($bank_questions_ids);$i++) echo $bank_questions_ids[$i]."<br>";
    $grd = new extgrid($hedaers,$columns, "index.php?module=questions_bank");
    $grd->selected_ids=$bank_questions_ids;
    $grd->edit=false;
    $grd->delete=false;
    $grd->column_override=array("question_type"=>"question_type_override");
    $grd->jslinks=array(PREVW=>"ShowPreview(\"[id]\",event.pageY)");
    $grd->auto_id=false;
    $grd->checkbox=true;
    $grd->chk_class="chk_qst";
    $grd->identity="bank";
    $grd->search=false;
        
    $query = questions_db::GetQuestionsBankQuery();    
    $grd->DrowTable($query);
    return $grd->table;
}

function question_type_override($row)
{
    global $QUESTION_TYPE;
    return $QUESTION_TYPE[$row['question_type_id']];
}

function CreateQuizFromBank()
{

    $date = util::Now();
    global $db;
    $query = orm::GetInsertQuery("quizzes", array("cat_id"=>$_POST['drpCats'], "quiz_name"=>$_POST['txtAssignmentName'],
                                                  "added_date"=>util::Now(), "parent_id"=>"-100", "show_intro"=>"0"
        ));
    
    $last_quiz_id=$db->insert_query($query);
        
    
    @$chkgrdbank=$_POST['chkgrdbank'];
    $str_in = "";
    while (list ($key,$val) = @each ($chkgrdbank))
    {
          $str_in.=",".$val;
    }
    $str_in = "-1".$str_in;
 
    $res_qst = $db->query("select * from questions where id in($str_in)");
    
    CopyQuestions($res_qst,$last_quiz_id);
    
    return $last_quiz_id;
}

function CopyQuiz($quiz_id)
{

      global $db;
      $last_quiz_id=$db->insert_query("insert into quizzes (cat_id,quiz_name,quiz_desc,added_date,parent_id,show_intro,intro_text,district_id) select cat_id,quiz_name,quiz_desc,added_date,id,show_intro,intro_text,district_id from quizzes where parent_id=0 and id=$quiz_id");

      $res_qst = $db->query(orm::GetSelectQuery("questions", array(), array("quiz_id"=>$quiz_id,"parent_id"=>"0"), "priority"));

      CopyQuestions($res_qst,$last_quiz_id);
      
      return $last_quiz_id;

}

function CopyQuestions($res_qst,$last_quiz_id)
{
	

	
      global $db;
      $q = 0;
      while($row_qst=$db->fetch($res_qst))
      {
          $q++;
          $query = orm::GetInsertQuery("questions", array("question_text"=>$row_qst['question_text'],
                                                          "question_type_id"=>$row_qst['question_type_id'],
														  "sec_question_type_id"=>$row_qst['sec_question_type_id'],
                                                          "priority"=>$q,
                                                          "quiz_id"=>$last_quiz_id,
                                                          "point"=>$row_qst['point'],
														  "penalty_point"=>$row_qst['penalty_point'],
														  "secondary_point"=>$row_qst['secondary_point'],
                                                          "parent_id"=>$row_qst['id'],
                                                          "added_date"=>util::Now(),
                                                          "header_text"=>$row_qst['header_text'],
                                                          "footer_text"=>$row_qst['footer_text'],
                                                          "help_image"=>$row_qst['help_image'],
														  "have_score_ans"=>$row_qst['have_score_ans'],
														  "test_cluster"=>$row_qst['test_cluster'],
														  "district_id"=>$row_qst['district_id']
                                    ));
          //echo $query;
          $last_qst_id=$db->insert_query($query);

          $res_grp = $db->query(orm::GetSelectQuery("question_groups", array(), array("question_id"=>$row_qst['id'],"parent_id"=>"0"), ""));

          while($row_grp=$db->fetch($res_grp))
          {
              $query = orm::GetInsertQuery("question_groups", array("group_name"=>$row_grp['group_name'],
                                                                    "show_header"=>$row_grp['show_header'],
																	 "show_footer"=>$row_grp['show_footer'],
                                                                    "question_id"=>$last_qst_id,
                                                                    "parent_id"=>$row_grp['id'],
                                                                    "added_date"=>util::Now(),
																	"is_secondary_group"=>$row_grp['is_secondary_group']

              ));
              $last_grp_id=$db->insert_query($query);

              $res_ans = $db->query(orm::GetSelectQuery("answers_quiz", array(), array("group_id"=>$row_grp['id'],"parent_id"=>"0"), ""));

              while($row_ans=$db->fetch($res_ans))
              {
                  $query = orm::GetInsertQuery("answers_quiz", array("group_id"=>$last_grp_id,
                                                                    "answer_text"=>$row_ans['answer_text'],
																	"answer_image"=>$row_ans['answer_image'],
                                                                      "correct_answer"=>$row_ans['correct_answer'],
                                                                      "priority"=>$row_ans['priority'],
                                                                      "correct_answer_text"=>$row_ans['correct_answer_text'],
                                                                      "answer_pos"=>$row_ans['answer_pos'],
                                                                      "parent_id"=>$row_ans['id'],
                                                                      "control_type"=>$row_ans['control_type'],
                                                                      "answer_parent_id"=>$row_ans['answer_parent_id'],
                                                                      "text_unit"=>$row_ans['text_unit'],
																	  "answer_desc"=>$row_ans['answer_desc'],
																	  "is_score_ans"=>$row_ans['is_score_ans'],
																	  "radio_ans"=>$row_ans['radio_ans'],
																	  "selfcorrectsvowel"=>$row_ans['selfcorrectsvowel'],
																	  "score"=>$row_ans['score']
                  ));
                  $last_ans_id=$db->insert_query($query);
              }

          }

      }

}

function desc_func()
{
        return ADD_ASSIGNMENT;
}



?>
