<?php

class asgDB
{
    public static function GetAsgQuery()
    {
        //$sql = "select asg.*, q.quiz_name from assignments asg left join quizzes q on q.id=asg.quiz_id [{where}] order by asg.added_date desc";
			 $district_id=$_SESSION['district_id'];
			
		   $sql = "select asg.*, q.quiz_name from assignments asg left join quizzes q on q.id=asg.quiz_id  where asg.district_id='".$district_id."'  order by asg.added_date desc";

        return $sql;
    }
	public static function GetStudentByTeacherId($id,$school_id)
    {
        //$sql = "SELECT DISTINCT ts.student_id,u.* FROM `teacher_students` ts,users u where ts.student_id = u.student_id and ts.teacher_id = $id";	
		
		//$sql = "SELECT * FROM `users` where UserID in (SELECT DISTINCT UserId FROM  students s, teacher_students ts where s.student_id = ts.student_id and ts.teacher_id =".$id.")";	
		$sql = "SELECT * FROM `users` where `student_id` in (SELECT DISTINCT `student_id`
FROM `teacher_students`
WHERE `teacher_id` =".$id.") and `school_id` = ".$school_id;
		
        return db::GetResultsAsArray($sql);
    }

	public static function GetAssessmentName($id)
    {
         $sql = "SELECT assignment_name FROM `assignments` where id =".$id;
        return db::GetResultsAsArray($sql);
    }
	public static function getQuestionAnswers($id)
    {
         $sql = "SELECT * FROM `answers_quiz` aq, question_groups qg WHERE qg.id = aq.group_id AND qg.question_id =".$id;
        return db::GetResultsAsArray($sql);
    }
	
	public static function getQuestionAnswersofqtypesix($id)
    {
         $sql = "SELECT * FROM `answers_quiz` aq, question_groups qg WHERE qg.id = aq.group_id AND qg.question_id =".$id." And radio_ans != 1";
        return db::GetResultsAsArray($sql);
    }
	public static function getQuestionofqtypesix($id)
    {
         $sql = "SELECT answer_text FROM `answers_quiz` aq, question_groups qg WHERE qg.id = aq.group_id AND qg.question_id =".$id." And radio_ans = 1 ";
        return db::GetResultsAsArray($sql);
    }
	public static function GetSchoolByTeacherId($id)
    {
        $sql = "SELECT school_id FROM `teachers` WHERE `teacher_id` =".$id;
        return db::GetResultsAsArray($sql);
    }
	
    public static function GetAsgQueryById($id)
    {
        $sql = "select * from assignments asg left join quizzes q on q.id=asg.quiz_id where asg.id=$id";
        return db::exec_sql($sql);
    }

    public static function DeleteAsgById($id)
    {
        //$sql = "delete from assignment_users where assignment_id=$id ;";
        //$sql = "delete from quizzes where parent_id<>0 and id in (select quiz_id from assignments where id=$id) ;";
        $sql = " delete from assignments where id=$id";
        db::exec_sql($sql);
    }

    public static function DeleteRelatedQuiz($asg_id)
    {
	$sql = "delete from quizzes where parent_id<>0 and id in (select quiz_id from assignments where id=$asg_id) ";
	db::exec_sql($sql);
    }

    public static function ChangeStat($stat,$id)
    {
        $sql = "update assignments set status=$stat where id=$id";
        return db::exec_sql($sql);
    }

    public static function GetActAsgByUserIDQuery($user_id)
    {
        $sql="select a.id as asg_id,a.*,".
	" q.* ,ifnull(ua.status,0) as user_quiz_status ".
        " from assignments a ".
        " left join quizzes q on a.quiz_id = q.id ".
        " left join user_quizzes ua on ua.assignment_id=a.id and ua.archived=0 and ua.user_id=".$user_id.
        " where a.status = 1 and a.id in  (".
        " select assignment_id from assignment_users where user_id = ".$user_id.
        ") order by a.added_date desc";
       // echo $sql;
        return $sql;
    }

    public static function GetActAsgByUserID($user_id, $asg_id)
    {
        $sql="select a.id as asg_id,a.*,q.quiz_name,".
	" q.* ,ifnull(ua.status,0) as user_quiz_status , ua.id as user_quiz_id ,  ua.finish_date as uq_finish_date, ua.added_date as uq_added_date".
        " from assignments a ".
        " left join quizzes q on a.quiz_id = q.id ".
        " left join user_quizzes ua on ua.assignment_id =a.id and ua.archived=0 and ua.user_id=".$user_id.
        " where a.status = 1 and  a.id in  (".
        " select assignment_id from assignment_users where user_id = ".$user_id.
        ") and a.id=".$asg_id."  order by a.added_date desc";
        //echo $sql;
        return db::exec_sql($sql);
    }

    public static function GetOldAssignmentsQuery($user_id,$mode)
    {
        $sql ="select uq.*,q.quiz_name,asg.quiz_type,asg.results_mode,asg.show_results ,asg.allow_review,".
        " (case show_results when 1 then asg.pass_score  else '' end) pass_score,".
        " (CASE show_results when 2 then 'Not enabled' ELSE (case success when 1 then 'Yes' else 'No' end) end) is_success ,".
        " (CASE show_results when 1 then (case results_mode when 1 THEN pass_score_point else pass_score_perc end) else '' end) total_point".
        " from user_quizzes uq left join assignments asg on asg.id=uq.assignment_id ".
        " left join quizzes q on q.id=asg.quiz_id ".
        " where asg.quiz_type in (1,3) and uq.user_id=$user_id order by uq.added_date desc";        
        return $sql;
        //return db::exec_sql($sql);
    }

    public static function GetAsgById($asg_id)
    {
        $sql="select c.cat_name, q.quiz_name,a.*".
        " from assignments a ".
        " left join quizzes q on a.quiz_id=q.id ".
        " left join cats c on c.id=q.cat_id ".
        " where a.id=$asg_id ";        
        return db::exec_sql($sql);
    }
    
    public static function GetUserQuizById($user_quiz_id)
    {
        $sql ="select * from user_quizzes uq ".
        " left join assignments a on a.id=uq.assignment_id ".
        " where uq.id = $user_quiz_id ";
        return db::exec_sql($sql);
    }

    public static function GetUserResultsQuery($asg_id,$user_type)
    {
      $table_name = "users";
      if($user_type=="2") $table_name="v_imported_users";
      
      $sql = "select asg.id,u.user_id,Name,".
                "Surname, ".
                "UserName, ".
                "user_photo,UserID,".
                "ifnull(ua.status,0) as status_id, ".
                "(case ifnull(ua.status,0) ".
                "when 0 then 'Not started' when 1 then 'Started' when 2 then 'Finished' ".
                "when 3 then 'Time ended' when 4 then 'Manually stopped' ".
                "    end ) as status_name, ".
                "ua.pass_score_point, ".
                "ua.pass_score_perc, ".
                "(CASE quiz_type when 2 then 'Not enabled' ELSE (case success when 1 then 'Yes' else 'No' end) end) is_success, ".
                "(CASE quiz_type when 1 then (case results_mode when 1 THEN pass_score_point else pass_score_perc end) else '' end) total_point, ".
                "ua.id as user_quiz_id, ".
                "mu.user_id as start_sent, mu2.user_id as results_sent ".
            "from assignment_users u	 ".
            "left join $table_name lu on lu.UserID = u.user_id ".
            "left join mailed_users mu on mu.user_id = u.user_id and mu.assignment_id=u.assignment_id and mu.user_type=$user_type and mu.mail_type=1 ".
            "left join user_quizzes ua on ua.user_id = lu.UserID and ua.assignment_id = u.assignment_id ".
            "left join mailed_users mu2 on mu2.user_id = u.user_id and mu2.assignment_id=u.assignment_id and mu2.user_type=$user_type and mu2.mail_type=2 and mu2.user_quiz_id=ua.id ".
            "left join assignments asg on asg.id=u.assignment_id ".
            "where u.assignment_id=$asg_id and u.user_type=$user_type  " ;
      
        return $sql;
    }

public static function UpdateUserQuiz1($user_quiz_id,$status,$date)
     {
   /*    // $quiz_res = db::exec_sql("CALL p_quiz_results(\"$user_quiz_id\");");
       // $row = db::fetch($quiz_res);

	     $a_id_query  = "select a.* from assignments a 
							left join user_quizzes uq on a.id = uq.assignment_id 
							where uq.id=". $user_quiz_id;
	    $a_id_array = db::GetResultsAsArray($a_id_query);
	     $a_id = $a_id_array[0]['quiz_id'];
		 $pass_score = $a_id_array[0]['pass_score'];
		 $q_type = $a_id_array[0]['quiz_type'];
		 $results_mode = $a_id_array[0]['results_mode'];
		 $show_results = $a_id_array[0]['show_results'];
		 
	if ($q_type == 1 || 3)
	{
		$que_query1 = "select SUM(point) as total_pri_point , SUM(secondary_point) as total_sec_point  from questions  q 
		left join  assignments a  on q.quiz_id = a.quiz_id
		where a.quiz_id =".$a_id;
	  	$que_array1 = db::GetResultsAsArray($que_query1);
		$total_primary_point = $que_array1[0]['total_pri_point'];
		$total_sec_point = $que_array1[0]['total_sec_point'];
		$total_quiz_point = $total_primary_point + $total_sec_point;
		
		$que_query = "select q.* from questions  q 
		left join  assignments a  on q.quiz_id = a.quiz_id
		where a.quiz_id =".$a_id;
	  	$que_array = db::GetResultsAsArray($que_query);
	  //	echo"<pre>";print_r($que_array);exit;
		for($i=0;$i<count($que_array);$i++)
		 {	
			$ques_Id_arrays[$i] = $que_array[$i]['id'];
			$que_type_array[$i] = $que_array[$i]['question_type_id'];
			$sec_que_type_array[$i] = $que_array[$i]['sec_question_type_id'];
			$que_point[$i] = $que_array[$i]['point'];
			$que_sec_point[$i] = $que_array[$i]['secondary_point'];
		 }
		
		 $Tot_ans_point =0;

		 for($j=0; $j<count($ques_Id_arrays);$j++)
		 {
		 	$que_ans_point =0;
		 	$que = $ques_Id_arrays[$j];
		    $que_type = $que_type_array[$j];
			 $sec_que_type = $sec_que_type_array[$j];
			//echo "j=".$j." - que-point = ". $que_sec_point[$j]."</br>";
			if($que_type == 0)  // for multi answer checkbox
			{
			 	$user_ans_query = "select answer_id from user_answers where is_score_ans = 0 and question_id = ".$que;
				 $user_ans_Id = db::GetResultsAsArray($user_ans_query);
				  $userAnsIdArray =array();
				 for($i=0;$i<count($user_ans_Id);$i++)
				 {
					$userAnsIdArray[$i] = $user_ans_Id[$i]['answer_id'];
				 }
			 	 $IntuserAnsIdArray = count($userAnsIdArray);
			
				$ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
				 $ans = db::GetResultsAsArray($ans_query);
				$ansIdArray = array();
				$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansIdArray[$i] = $ans[$i]['id'];
				 }
				 
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$ans_point = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if(in_array($userAnsIdArray[$i],$ansIdArray))
						{
							$ans_point = $que_point[$j];
						}
			 		}
				 }
			 }
	
              elseif($que_type == 1)  // for radio button
			 {
		 	   $user_ans_query = "select answer_id from user_answers where is_score_ans = 0 and question_id = ".$que;
				 $user_ans_Id = db::GetResultsAsArray($user_ans_query);
			      $userAns = $user_ans_Id[0]['answer_id'];
	   	
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
				$ans = db::GetResultsAsArray($ans_query);
			 	$ansRadio = $ans[0]['id'];
				$ans_point = 0;
				if($userAns == $ansRadio)
				  $ans_point = $que_point[$j];
			  }		
			
			elseif($que_type == 3)
			 {
			 	$user_ans_query = "select user_answer_text from user_answers where is_score_ans = 0 and question_id = ".$que;
				$user_ans_Id = db::GetResultsAsArray($user_ans_query);
			    $userAns = $user_ans_Id[0]['user_answer_text'];
				
				$ans_query =  "select correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
				$ansTextarray = db::GetResultsAsArray($ans_query);
			 	$ansText = $ansTextarray[0]['correct_answer_text'];
				$ans_point = 0;
				 $ans1 = strcasecmp($userAns,$ansText);
				  if($ans1 == 0)
					 $ans_point = $que_point[$j];
			 }
			
			  elseif($que_type == 4) //for multi text
			 {
		 	     $user_ans_query = "select answer_id,user_answer_text from user_answers where is_score_ans = 0 and question_id = '".$que."' order by id Asc";
				 $user_ans_text = db::GetResultsAsArray($user_ans_query);
				  $userAnsTextArray =array();
				 for($i=0;$i<count($user_ans_text);$i++)
				 {
					$userAnsTextArray[$i] = $user_ans_text[$i]['user_answer_text'];
				 }
			 	 $IntuserAnsIdArray = count($userAnsTextArray);
				
				$ans_query =  "select id,correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) order by id"; 
				$ans = db::GetResultsAsArray($ans_query);
				$ansTextArray = array();
			  	$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansTextArray[$i] = $ans[$i]['correct_answer_text'];
				 }
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$equl = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if( trim($userAnsTextArray[$i]) == trim($ansTextArray[$i]))
						{
							$equl = $equl + 1;
						}
					}
				 }
				  
				 $ans_point = 0;
				  if($equl ==($IntuserAnsIdArray))
				 	 $ans_point = $que_point[$j];
				
		 	 }
		// secondary 
		  
			if($sec_que_type == 0)  // for multi answer checkbox
			{
				$user_ans_query = "select answer_id from user_answers where is_score_ans = 1 and question_id = ".$que;
				//$user_ans_query = "select answer_id from user_answers where question_id = '".$que."' and is_score_ans = 1" ;
				 $user_ans_Id = db::GetResultsAsArray($user_ans_query);
				 $userAnsIdArray =array();
				 for($i=0;$i<count($user_ans_Id);$i++)
				 {
					  $userAnsIdArray[$i] = $user_ans_Id[$i]['answer_id'];
				 }
			 	  $IntuserAnsIdArray = count($user_ans_Id);
			//	echo "<pre>"; print_r($userAnsIdArray); 
				$ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1) "; 
				 $ans = db::GetResultsAsArray($ans_query);
				$ansIdArray = array();
				$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansIdArray[$i] = $ans[$i]['id'];
				 }
				//echo "<pre>"; print_r($ansIdArray); 
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$ans_point_sec = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if(in_array($userAnsIdArray[$i],$ansIdArray))
						{
							$ans_point_sec = $que_sec_point[$j];
						}
			 		}
				 }
			 }
	
              elseif($sec_que_type == 1)  // for radio button
			 {
		 	   $user_ans_query = "select answer_id from user_answers where is_score_ans = 1 and question_id = ".$que;
				 $user_ans_Id = db::GetResultsAsArray($user_ans_query);
			      $userAns = $user_ans_Id[0]['answer_id'];
	   	
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1)";
				$ans = db::GetResultsAsArray($ans_query);
			 	$ansRadio = $ans[0]['id'];
				$ans_point_sec = 0;
				if($userAns == $ansRadio)
				  $ans_point_sec = $que_sec_point[$j];
			  }		
			
			elseif($sec_que_type == 3)
			 {
			 	$user_ans_query = "select user_answer_text from user_answers where is_score_ans = 1 and question_id = ".$que;
				$user_ans_Id = db::GetResultsAsArray($user_ans_query);
			    $userAns = $user_ans_Id[0]['user_answer_text'];
				
				$ans_query =  "select correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1)";
				$ansTextarray = db::GetResultsAsArray($ans_query);
			 	$ansText = $ansTextarray[0]['correct_answer_text'];
				$ans_point_sec = 0;
				 $ans1 = strcasecmp($userAns,$ansText);
				  if($ans1 == 0)
					 $ans_point_sec = $que_sec_point[$j];
			 }
			
			  elseif($sec_que_type == 4) //for multi text
			 {
		 	     $user_ans_query = "select answer_id,user_answer_text from user_answers where is_score_ans = 1 and question_id = '".$que."' order by id Asc";
				 $user_ans_text = db::GetResultsAsArray($user_ans_query);
				 $userAnsTextArray =array();
				 for($i=0;$i<count($user_ans_text);$i++)
				 {
					$userAnsTextArray[$i] = $user_ans_text[$i]['user_answer_text'];
				 }
			 	 $IntuserAnsIdArray = count($userAnsTextArray);
		 
				$ans_query =  "select id,correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1) order by id"; 
				$ans = db::GetResultsAsArray($ans_query);
				$ansTextArray = array();
			  	$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansTextArray[$i] = $ans[$i]['correct_answer_text'];
				 }
			 
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$equl = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if( trim($userAnsTextArray[$i]) == trim($ansTextArray[$i]))
						{
							$equl = $equl + 1;
						}
					}
				 }
				 $ans_point_sec = 0;
				
				  if($equl ==($IntuserAnsIdArray))
				 	 $ans_point_sec = $que_sec_point[$j];
				
		 	 }
			 $que_ans_point = $ans_point + $ans_point_sec;
			 $Tot_ans_point = $Tot_ans_point + $que_ans_point;
			 
		 }
		echo "<script type='text/javascript'> alert('Quiz Total Score is :-".$Tot_ans_point."'); </script>";  
		
		  if($Tot_ans_point >= $pass_score)
		  $success = 1;
		  else 
		  $success = 0; 
		   $pass_score_perc = $Tot_ans_point * 100/$total_quiz_point; 
		 
	}//end if quz
	/*elseif($q_type == 3)
	{
		if($status ==1)
		 $success = 0; 
		 else if($status == 2)
		  $success = 1;
	} */	
/*	else
	{
		 $success = 1; 
	} 
	exit;
	 
		 
        $query = orm::GetUpdateQuery("user_quizzes",
                                          array("success"=>$success,
                                                "status"=>$status,
                                                "finish_date"=>$date,
                                                "pass_score_point"=>$Tot_ans_point,
                                                "pass_score_perc"=>$pass_score_perc
                                                ),
                                          array("id"=>$user_quiz_id));
        db::exec_sql($query);
	 
		$row =  array("quiz_success"=>$success,
					"show_results"=>$show_results,
					"pass_score"=>$pass_score,
					"total_ans_point"=>$Tot_ans_point,
					"results_mode"=>$results_mode,
					"total_perc"=>$pass_score_perc
					);
        return $row;  
		*/
    }  

	 public static function GetAssessmentQuestion($asg_id)
     {
		  $que_query  = "select q.* from questions q, assignments a where q.quiz_id = a.quiz_id and a.id=".$asg_id;
		  return db::GetResultsAsArray($que_query);
	 }
	 public static function GetAssessmentStudent($asg_id)
     {
		  $stud_query  = "SELECT distinct * from users u , assignment_users au where au.user_id = u.UserID and au.assignment_id =".$asg_id;
		  return db::GetResultsAsArray($stud_query);
	 }
   
     public static function UpdateUserQuiz($user_quiz_id,$status,$date)
     {
       // $quiz_res = db::exec_sql("CALL p_quiz_results(\"$user_quiz_id\");");
       // $row = db::fetch($quiz_res);
	     $a_id_query  = "select a.* from assignments a 
							left join user_quizzes uq on a.id = uq.assignment_id 
							where uq.id=". $user_quiz_id;
	    $a_id_array = db::GetResultsAsArray($a_id_query);
	     $a_id = $a_id_array[0]['quiz_id'];
		 $pass_score = $a_id_array[0]['pass_score'];
		 $q_type = $a_id_array[0]['quiz_type'];
		 $results_mode = $a_id_array[0]['results_mode'];
		 $show_results = $a_id_array[0]['show_results'];
		 $Tot_ans_point =0;
		if ($q_type == 1 || $q_type ==  3)
		{
			/*$que_query1 = "select SUM(point) as total_pri_point , SUM(secondary_point) as total_sec_point  from questions  q 
			left join  assignments a  on q.quiz_id = a.quiz_id
			where a.quiz_id =".$a_id." " ;
			 */
			 $total_primary_point ='';$total_sec_point ='';$total_quiz_point='';$total_penalty_point='';
			 $true_primary_point  ='';$true_sec_point ='';$total_Qtype6checkbox_point='';$total_Qtype6radio_point='';
			
			$que_query1 = "SELECT SUM(q.point) as total_pri_point from questions q,assignments a,user_quizzes uq where q.quiz_id = a.quiz_id and a.id=uq.assignment_id and question_type_id <> 6 and uq.id = ".$user_quiz_id ;
			//	$que_query1 = "select SUM(point) as total_pri_point from questions where question_type_id <> 6 and id in(select question_id from user_answers where user_quiz_id = ".$user_quiz_id.")";
	  		$que_array1 = db::GetResultsAsArray($que_query1);
			$total_primary_point = $que_array1[0]['total_pri_point'];

			$que_query1 = "select SUM(q.secondary_point) as total_sec_point from questions q,assignments a,user_quizzes uq where q.quiz_id = a.quiz_id and a.id=uq.assignment_id and question_type_id <> 6 and uq.id = ".$user_quiz_id ;
			$que_array1 = db::GetResultsAsArray($que_query1);
		 	$total_sec_point = $que_array1[0]['total_sec_point'];
			$total_quiz_point = $total_primary_point + $total_sec_point;
			
			$que_queryD = "select SUM(q.penalty_point) as total_penalty_point from questions q,assignments a,user_quizzes uq where q.quiz_id = a.quiz_id and a.id=uq.assignment_id and uq.id = ".$user_quiz_id ;
			$que_arrayD = db::GetResultsAsArray($que_queryD);
		 	$total_penalty_point = $que_arrayD[0]['total_penalty_point'];
		
	 		$que_query1 = "select SUM(point) as total_pri_point from questions where question_type_id <> 6 and id  in(select question_id from user_answers where is_correct_ans = 1 and user_quiz_id = ".$user_quiz_id." and is_score_ans=0 group by question_id )";
			$que_array1 = db::GetResultsAsArray($que_query1);
			$true_primary_point = $que_array1[0]['total_pri_point'];
		
			$que_query2 = "select SUM(secondary_point) as total_sec_point from questions where id in(select question_id from user_answers where is_correct_ans = 1 and user_quiz_id = ".$user_quiz_id." and is_score_ans=1 group by question_id )";
			$que_array2 = db::GetResultsAsArray($que_query2);
			$true_sec_point = $que_array2[0]['total_sec_point'];
			
			$Tot_ans_point = $true_primary_point + $true_sec_point;
		 }
		//echo "<script type='text/javascript'> alert('Quiz Total Score is :-".$Tot_ans_point."');  
		
			//$que_query1 = "select SUM(point) as total_pri_point from questions where question_type_id = 6 and id in(select question_id from user_answers where user_quiz_id = ".$user_quiz_id.")";
		
			$que_query1 = "SELECT SUM(q.point) as total_pri_point from questions q,assignments a,user_quizzes uq where q.quiz_id = a.quiz_id and a.id=uq.assignment_id and question_type_id = 6 and uq.id = ".$user_quiz_id ;
		 	$que_array1 = db::GetResultsAsArray($que_query1);
			$total_Qtype6checkbox_point = $que_array1[0]['total_pri_point'];
			
			$que_query1 = "SELECT SUM(score) as total_score_point from answers_quiz where radio_ans = 1 and group_id in (
			select qg.id from question_groups qg,questions q,assignments a,user_quizzes uq where qg.question_id  =q.id and q.quiz_id = a.quiz_id and a.id=uq.assignment_id and q.question_type_id = 6 and uq.id = ".$user_quiz_id.")" ;
			
			$que_array1 = db::GetResultsAsArray($que_query1);
			$total_Qtype6radio_point = $que_array1[0]['total_score_point'];
		
			$que_query = "select * from questions where question_type_id = 6 and id in(select question_id from user_answers where user_quiz_id = ".$user_quiz_id.")";
	  		$que_array = db::GetResultsAsArray($que_query);
			  //	echo"<pre>";print_r($que_array);exit;
				$TotalanspPoint6 =0;
				for($j=0;$j<count($que_array);$j++)
				 {	
					$ansScore =0;
					$que = $que_array[$j]['id'];
				 	$que_point = $que_array[$j]['point'];
				 
					$user_ans_query = "select distinct answer_id from user_answers where is_score_ans = 0 and user_answer_text is NULL and question_id = ".$que." and user_quiz_id = ".$user_quiz_id;  //for chk answer
				 	 $user_ans_Id = db::GetResultsAsArray($user_ans_query);
			 		$userAnsId = $user_ans_Id[0]['answer_id']; 
				 
					$ans_query =  "select id from answers_quiz where correct_answer = 1 AND radio_ans = 0 AND group_id in (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
					 $ans = db::GetResultsAsArray($ans_query);
					$ansIdArray = array();
				 	$IntAnsIdArray = count($ans);
				 	for($i=0;$i<$IntAnsIdArray;$i++)
					 {
						$ansIdArray[$i] = $ans[$i]['id'];
					 }
				 	$checkboxPoint = 0;
					if(in_array($userAnsId,$ansIdArray))
					{
						 $checkboxPoint = $que_point;
					}
				  	 	 
					 //for radio
					 $user_ans_query = "select distinct answer_id,user_answer_text from user_answers where is_score_ans = 0 and user_answer_text IS NOT NULL and question_id = ".$que." and user_quiz_id = ".$user_quiz_id;
					 $user_ans_Id = db::GetResultsAsArray($user_ans_query);
					  $userAnsIdArray =array();
					  $userAnswer_text = array();
				 	 $totRadioScore = 0;
					 for($i=0;$i<count($user_ans_Id);$i++)
					 {
					 	$radio_score = 0;
						$userAnsIdArray[$i] = $user_ans_Id[$i]['answer_id'];
					 	$userAnswer_text[$i] = $user_ans_Id[$i]['user_answer_text'];
						$ans_query =  "select * from answers_quiz where radio_ans = 1 AND id=".$userAnsIdArray[$i];
						$ans = db::GetResultsAsArray($ans_query);
						$correct_answer = $ans[0]['correct_answer'];
					
						$score = $ans[0]['score'];
						if($userAnswer_text[$i] == $correct_answer)
						{
							$radio_score = $score;
						}
					 	$totRadioScore = $totRadioScore+$radio_score;
				 	 }
				/*	echo "chk-".$checkboxPoint;
					
					echo "radio-".$totRadioScore;  
					echo"<br>";*/
					 $ansScore = $checkboxPoint + $totRadioScore;
					 $TotalanspPoint6 = $TotalanspPoint6 + $ansScore;	 
			 }
			/* echo "TotalanspPoint6".$TotalanspPoint6;echo"<br>";
			 echo  "Tot_ans_point".$Tot_ans_point;echo"<br>";
			*/
			 $Tot_ans_point = $Tot_ans_point + $TotalanspPoint6;
		 	 $total_quiz_point = $total_quiz_point + $total_Qtype6radio_point + $total_Qtype6checkbox_point;
		 $Tot_ans_point_after_penalty = $Tot_ans_point - $total_penalty_point;
		
		  if($Tot_ans_point_after_penalty >= $pass_score)
		  $success = 1;
		  else 
		  $success = 0; 
		  if($total_quiz_point != 0)
		  {
		  	$pass_score_perc = $Tot_ans_point_after_penalty * 100/$total_quiz_point; 
		 }
		 
		  if($status == 5)
		  {
			  $success = 0; 
		  }
		
	        $query = orm::GetUpdateQuery("user_quizzes",
                                          array("success"=>$success,
                                                "status"=>$status,
                                                "finish_date"=>$date,
                                                "pass_score_point"=>$Tot_ans_point_after_penalty,
                                                "pass_score_perc"=>$pass_score_perc
                                                ),
                                          array("id"=>$user_quiz_id));
        db::exec_sql($query);
	
		$row =  array("quiz_success"=>$success,
					"show_results"=>$show_results,
					"pass_score"=>$pass_score,
					"total_ans_point"=>$Tot_ans_point_after_penalty,
					"results_mode"=>$results_mode,
					"total_perc"=>$pass_score_perc
					);
					
        return $row; 
    }
	
	
	public static function UpdateUserAssessment($user_quiz_id,$status,$date)
    { 
     
	  /*   $a_id_query  = "select a.* from assignments a 
							left join user_quizzes uq on a.id = uq.assignment_id 
							where uq.id=". $user_quiz_id;
	    $a_id_array = db::GetResultsAsArray($a_id_query);
	     $a_id = $a_id_array[0]['quiz_id'];
		 $pass_score = $a_id_array[0]['pass_score'];
		 $q_type = $a_id_array[0]['quiz_type'];
		 $results_mode = $a_id_array[0]['results_mode'];
		 $show_results = $a_id_array[0]['show_results'];
		 
		if ($q_type == 3)
		{
			$que_query1 = "select SUM(point) as total_pri_point , SUM(secondary_point) as total_sec_point  from questions  q 
			left join  assignments a  on q.quiz_id = a.quiz_id
			where a.quiz_id =".$a_id;
			$que_array1 = db::GetResultsAsArray($que_query1);
			$total_primary_point = $que_array1[0]['total_pri_point'];
			$total_sec_point = $que_array1[0]['total_sec_point'];
			$total_quiz_point = $total_primary_point + $total_sec_point;
				  
				$que_query1 = "select SUM(point) as total_pri_point from questions where id  in(select question_id from user_answers where is_correct_ans = 1 and user_quiz_id = ".$user_quiz_id." and is_score_ans=0 group by question_id )";
				$que_array1 = db::GetResultsAsArray($que_query1);
				$true_primary_point = $que_array1[0]['total_pri_point'];
				
				$que_query2 = "select SUM(point) as total_sec_point from questions where id  in(select question_id from user_answers where is_correct_ans = 1 and user_quiz_id = ".$user_quiz_id." and is_score_ans=1 group by question_id )";
				$que_array2 = db::GetResultsAsArray($que_query2);
				$true_sec_point = $que_array2[0]['total_sec_point'];
				
				$Tot_ans_point = $true_primary_point + $true_sec_point;
			   
			
			  $success = 0; 
			  if($total_quiz_point != 0)
			  {
				$pass_score_perc = $Tot_ans_point * 100/$total_quiz_point; 
				}
		}	
	 */
	 
	 	$res = asgDB::UpdateUserQuiz($user_quiz_id,$status,$date);
	 		
			$success = 0;
			$Tot_ans_point = $res["pass_score_point"];
			$pass_score_perc =$res["pass_score_perc"];
			
		  $query = orm::GetUpdateQuery("user_quizzes",
											  array("success"=>$success,
													"status"=>$status,
													"finish_date"=>$date,
													"pass_score_point"=>$Tot_ans_point,
													"pass_score_perc"=>$pass_score_perc
													),
											  array("id"=>$user_quiz_id));
			db::exec_sql($query);
		
		/*	$row =  array("quiz_success"=>$success,
						"show_results"=>$show_results,
						"pass_score"=>$pass_score,
						"total_ans_point"=>$Tot_ans_point,
						"results_mode"=>$results_mode,
						"total_perc"=>$pass_score_perc
						);  */
			return true;
    }

    public static function GetUserInfoByAsgId($asg_id,$user_id,$user_type,$user_quiz_id)
    {
	$users ="users";
	$user_quiz_where = "";
	if($user_type=="2") $users ="v_imported_users";
	if($user_quiz_id!="") $user_quiz_where = " and uq.id=$user_quiz_id ";
	$query = "select usr.*,q.*,asg.*,uq.id as user_quiz_id , uq.added_date,uq.finish_date,pass_score_point,pass_score_perc,uq.success from  $users usr, assignments asg left join user_quizzes uq on uq.assignment_id=asg.id and uq.user_id = $user_id $user_quiz_where left join quizzes q on q.id=asg.quiz_id where usr.UserID=$user_id and asg.id=$asg_id";

	return db::exec_sql($query);
    }

    public static function AcceptNewUser($user_id)
    {
	$sql = "insert into assignment_users  (assignment_id,user_type,user_id)".
	"select id, 1, $user_id from assignments ".
	"where accept_new_users = 1 and status in (0,1)";
	db::exec_sql($sql);
     
    }

}
?>
