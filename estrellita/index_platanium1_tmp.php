<?php 
session_start();
//print_r($_SESSION);
if($_REQUEST['assessment_name']){
$assessnamearr = explode('_',$_REQUEST['assessment_name']);
} else {
    $assessnamearr = explode('_',$_SESSION['assessment_name']);
}
if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
define('SITEURLM',		'http://enterprise.ueisworkshop.com/');
define('REDIRECTURL',		'http://enterprise.ueisworkshop.com/index.php/');
define('LOADERURL','http://enterprise.ueisworkshop.com/images/loading11.gif');
} else if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
define('SITEURLM',    'http://district.ueisworkshop.com/');
define('REDIRECTURL',   'http://district.ueisworkshop.com/index.php/');
define('LOADERURL','http://district.ueisworkshop.com/images/loading11.gif');
}
else if($_SERVER["HTTP_HOST"]=="localhost"){
define('SITEURLM',		'http://localhost/client/');
define('REDIRECTURL',		'http://localhost/client/index.php/');
define('LOADERURL','http://localhost/client/images/loading11.gif');
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>UEIS Workshop</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="../assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="../css_new/style.css" rel="stylesheet" />
   <link href="../css_new/style-responsive.css" rel="stylesheet" />
   <link href="../css_new/style-purple.css" rel="stylesheet" id="style_color" />
   <link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" />
<script src="../js/jquery-1.8.3.min.js"></script>
<script src="lib2/sticky/sticky.min.js"></script>
<?php  include "modules/".$module_name.".php";?>
</head>

<body class="fixed-top">
   <!-- BEGIN HEADER -->
  <div id="header" class="navbar navbar-inverse navbar-fixed-top">
       <!-- BEGIN TOP NAVIGATION BAR -->
       <div class="navbar-inner">
           <div class="container-fluid">
               <!--BEGIN SIDEBAR TOGGLE-->
               <div class="sidebar-toggle-box hidden-phone">
                   <div class="icon-reorder"></div>
               </div>
               <!--END SIDEBAR TOGGLE-->
               <!-- BEGIN LOGO -->
               <a class="brand" href="<?php echo SITEURLM;?>">
                   <img src="../img/logo.png" alt="UEIS Workshop" />
               </a>
               <!-- END LOGO -->
               <!-- BEGIN RESPONSIVE MENU TOGGLER -->
               <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="arrow"></span>
               </a>
               <!-- END RESPONSIVE MENU TOGGLER -->
               <div id="top_menu" class="nav notify-row">
                   <!-- BEGIN NOTIFICATION -->
                   <ul class="nav top-menu">
                   </div>  
               <div class="top-nav ">
                   <ul class="nav pull-right top-menu" >
                      
                       <!-- BEGIN USER LOGIN DROPDOWN -->
                       <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                               <i class="icon-user"></i> &nbsp;
                               <span class="username">Welcome, <B><?php echo $fullname ?></b> | <B>Level:</B> Classroom</span>
                               <b class="caret"></b>                           </a>
                           <ul class="dropdown-menu extended logout">
                               
                              <li><a href="<?php echo REDIRECTURL;?>admin/logout"><i class="icon-unlock"></i> Log Out</a></li>
                           </ul>
                       </li>
                       <!-- END USER LOGIN DROPDOWN -->
                   </ul>
                 <!-- END TOP NAVIGATION MENU -->
               </div>
           </div>
       </div>
       <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->
      <div class="sidebar-scroll">
        <div id="sidebar" class="nav-collapse collapse">

         
        <!-- BEGIN SIDEBAR MENU -->
          <ul class="sidebar-menu" >
              <li class="sub-menu">
                  <a class="" href="../index.html">
                      <i class="icon-home"></i>
                      <span>Home</span>
                  </a>
              </li>
                <!-- FIRST LEVEL MENU Planning Manager-->
            <li class="sub-menu" >
                  <a href="javascript:;" class="lesson">
                      <i class="icon-book"></i>
                      <span>Planning Manager</span>
                      <span class="arrow"></span>
                  </a>
                  
                    <!-- SECOND LEVEL MENU -->
            	<ul class="sub">
              
                 	<li><a class="" href="../planning-manager/lesson-plan.html">Lesson Plan Creator</a></li>
                    <!-- THIRD LEVEL MENU-->                       
                <ul class="sub" id="third">
                    <li><a class="" href="../planning-manager/lesson-plan.html">Create Lesson Plan</a></li></ul>
                    
                     <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../planning-manager/create-plan.html">Add Plan</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../planning-manager/upload-plan.html">Upload Plan</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/edit-plan.html">Edit Lesson Plan</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/completed-plan.html">Retrieve Lesson Plan</a></li>
                </ul>
                
                   <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="../planning-manager/smart-goals.html">SMART Goals Creator</a></li>
                     
                      <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/create-goal.html">Create Goal</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/edit-goal.html">Edit Goal</a></li>
                </ul>
                
                <!-- SECOND LEVEL MENU -->
                     <li><a class="" href="../planning-manager/calendar.html">Calendar Creator</a></li>
                             
                         <!-- THIRD LEVEL MENU-->
                <ul class="sub" id="third">
                     <li><a class="" href="../planning-manager/new-event.html">Create New Event</a></li>
                </ul>
                                    
                <ul class="sub">
                     <li><a class="" href="../planning-manager/retrieve-event.html">Retrieve Event</a></li>
                </ul>
                
                </ul>
              </li>
              
                  <!-- FIRST LEVEL MENU ATTENDANCE MANAGER-->
                <li class="sub-menu" >
                  <a href="javascript:;" class="attendance">
                      <i class="icon-calendar"></i>
                      <span>Attendance Manager</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/instruction.html">Instruction</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/daily-attendance.html">Daily Attendance</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/take-roll.html">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/retrieve-roll.html">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="../attendance/new-student-enrollment.html">New Student Enrollment</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/modify-enrollment.html">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/enroll-student-enrollment.html">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../attendance/roster.html">Student Roster</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="../attendance/create-roster.html">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/edit-roster.html">Edit</a></li>
                </ul>  
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
					  <li><a class="" href="../attendance/data.html">Add-Map Data</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/add-parent-info.html">Add Parent Information</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/map-parent.html">Map Parent/Student Data</a></li>
                </ul>  
                
                
                <!-- SECOND LEVEL MENU -->
                
                      <li><a class="" href="../attendance/assessment.html">Assessment</a></li>
                       <!-- THIRD LEVEL MENU -->
                 <ul class="sub">
                      <li><a class="" href="../attendance/daily-attendance.html">Daily Attendance</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/take-roll.html">Take Roll</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/retrieve-roll.html">Retrieve Roll Sheet</a></li>
                </ul>  
                   <!-- THIRD LEVEL MENU -->
                   <ul class="sub">
                      <li><a class="" href="../attendance/new-student-enrollment.html">New Student Enrollment</a></li>
                      </ul>
                      <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/modify-enrollment.html">Modify Enrollment</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/enroll-student-enrollment.html">Enroll Student</a></li>
                </ul>  
                  <!-- THIRD LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../attendance/roster.html">Student Roster</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth" >
                     <li><a class="" href="../attendance/create-roster.html">Create</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/edit-roster.html">Edit</a></li>
                </ul>  
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
					  <li><a class="" href="../attendance/data.html">Add-Map Data</a></li>
                      </ul>
                       <!-- FOURTH LEVEL MENU -->
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/add-parent-info.html">Add Parent Information</a></li>
                </ul>  
                <ul class="sub" id="fourth">
                     <li><a class="" href="../attendance/map-parent.html">Map Parent/Student Data</a></li>
                </ul>  
                      
                      
                  </ul>
              </li>
              <!-- FIRST LEVEL MENU IMPLEMENTATION MANAGER-->
              
                <li class="sub-menu" >
                  <a href="javascript:;" class="implementation">
                      <i class="icon-pencil"></i>
                      <span>Implementation Manager</span>
                      <span class="arrow"></span>
                  </a>
                <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../implementation/grade-tracker.html">Grade Tracker</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-grade.html">Enter Grade</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-grade.html">Retrieve Grade</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                       <li><a class="" href="../implementation/homework-tracker.html">Homework Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-homework-record.html">Enter Homework Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-homework-record.html">Retrieve Homework Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->
                        <li><a class="" href="../implementation/iep-tracker.html">IEP Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-iep-record.html">Enter IEP Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-iep-record.html">Retrieve IEP Record</a></li>
                </ul>  
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="../implementation/eld-tracker.html">ELD Tracker</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/enter-eld-record.html">Enter ELD Record</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../implementation/retrieve-eld-record.html">Retrieve ELD Record</a></li>
                </ul>  
                
                <!-- SECOND LEVEL MENU -->                        
                        <li><a class="" href="../implementation/instructional-efficacy.html">Instructional Efficacy</a></li>
                        <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/observation.html">Lesson Observation Feedback</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/observation-full-report.html">Retrieve Full Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/observation-sectional-report.html">Retrieve Sectional Data Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../implementation/self-reflection.html">Self-Reflection</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/generate-report.html">Create/Edit Report</a></li></ul>
              
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../implementation/retrieve-report.html">Retrieve Report</a></li>
                </ul>
                
                
                  </ul>
              </li>
              
              
              
                <!-- FIRST LEVEL MENU CLASSROOM MANAGEMENT ASSISTANT-->
              <li class="sub-menu" >
                  <a href="javascript:;" class="classroom">
                      <i class="icon-group"></i>
                      <span>Classroom Management</span>
                      <span class="arrow"></span>
                  </a>
                  <!-- SECOND LEVEL MENU -->
                <ul class="sub">
                      <li><a class="" href="../classroom/progress-report.html">Student Progress Report</a></li>
                      <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-progress-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-progress-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-progress-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                          <li><a class="" href="../classroom/success-team.html">Student Success Team</a></li>
                           <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-success-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-success-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-success-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                              <li><a class="" href="../classroom/parent-teacher.html">Parent/Teacher Conference</a></li>
                              <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-parent-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-parent-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-parent-report.html">Retrieve</a></li>
                </ul>   
                <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="../classroom/teacher-student.html">Teacher/Student Conference</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-student-report.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/upload-student-report.html">Upload</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-student-report.html">Retrieve</a></li>
                </ul>   
                
                  <!-- SECOND LEVEL MENU -->  
                                     <li><a class="" href="../classroom/behavior-record.html">Behavior Running Record</a></li>
                                     <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../classroom/create-behavior.html">Create</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../classroom/edit-behavior.html">Edit</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../classroom/retrieve-behavior.html">Upload</a></li>
                </ul>   
                
                  </ul>
              </li>
              
              
                 <!-- FIRST LEVEL MENU ASSESSMENT MANAGER-->
              <li class="sub-menu"  id="assessment">
                  <a href="javascript:;" class="assessment">
                      <i class="icon-bar-chart"></i>
                      <span>Assesment Manager</span>
                      <span class="arrow"></span>
                  </a>
                     <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                      <li><a class="" href="../assessment/diagnostic.html">Diagonstic Screener</a></li>
                      <li><a class="" href="../assessment/progress.html">Progress Monitoring</a></li>
                      <li><a class="" href="../assessment/benchmark.html">Benchmark Assesment</a></li>
 </ul>
              </li>
              
               <!-- FIRST LEVEL TOOLS & RESOURCES-->
              <li class="sub-menu">
                  <a href="javascript:;" class="tools">
                      <i class="icon-wrench"></i>
                      <span>Tools &amp; Resources</span>
                      <span class="arrow"></span>
                  </a>
                   <!-- SECOND LEVEL MENU -->
                  <ul class="sub">
                  	  <li><a class="" href="../tools/data-tracker.html">Data Tracker</a></li>
                       <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/data-planning-manager.html">Planning Manager</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/assessment-manager.html">Assessment Manager</a></li>
                </ul>  
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/growth.html">View a Classroom's Growth</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/class-standard.html">Class Performance Score by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/graphical-report.html">Class Performance Graphs by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-summary.html">Individual Student Performance Summary</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-standard.html">Individual Student Performance by Standard</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/progress-report.html">Student Progress Report</a></li>
                </ul>
                
                <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/attendance-manager.html">Attendance Manager</a></li>
                </ul> 
                <ul class="sub">
                     <li><a class="" href="../tools/implementation-manager.html">Implementation Manager</a></li>
                </ul>   
                
                  <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/grade-tracker.html">Grade Tracker</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/homework-tracker.html">Homework Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/iep-tracker.html">IEP Tracker</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/eld-tracker.html">ELD Tracker</a></li>
                </ul>
                
                   <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/observation.html">Lesson Observations</a></li>
                </ul>

<ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/viewer-observation.html">Observation by Viewer</a></li>
                </ul>
                
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/grade-observation.html">Observation by Grade</a></li>
                </ul>
                
                <ul class="sub" id="fourth" style="padding-left: 10px;">
                    <li><a class="" href="../tools/count-observation.html">Observation Count</a></li>
                </ul>

  <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/pd-activity.html">Professional Development Activity</a></li>
                </ul>
                
                
                
                   <ul class="sub">
                     <li><a class="" href="../tools/classroom-management.html">Classroom Management</a></li>
                     
                </ul> 
                
                 <!-- FOURTH LEVEL MENU-->                       
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/student-progress-report.html">Student Progress Report</a></li></ul>
                 
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/success-team.html">Student Success Team Meeting Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/parent-teacher.html">Parent/Teacher Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/teacher-student.html">Teacher/Student Conference Report</a></li>
                </ul>
                
                <ul class="sub" id="fourth">
                    <li><a class="" href="../tools/behavior-record.html">Behavior Learning & Running Record Report</a></li>
                </ul>
                <!-- SECOND LEVEL MENU -->  
                      <li><a class="" href="../tools/development.html">Professional Development</a></li>
                         <!-- THIRD LEVEL MENU -->
                <ul class="sub">
                     <li><a class="" href="../tools/pd-individual.html">Individual</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/pd-group.html">Group</a></li>
                </ul> 
                <!-- SECOND LEVEL MENU -->
                      <li><a class="" href="../tools/bridge.html">Parent Bridge</a></li> 
                     <!-- THIRD LEVEL MENU -->
                      <ul class="sub">
                     <li><a class="" href="../tools/send-notification.html">Parent Notification</a></li>
                </ul>  
                <ul class="sub">
                     <li><a class="" href="../tools/notification-records.html">Parent Notification Records</a></li>
                </ul>                  
                      <li><a class="" href="../tools/guide.html">User/Training Guide</a></li>
                      
                  </ul><BR><BR>
              </li>
             
          </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE --> 
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
                 <h3 class="page-title">
                      <i class="icon-bar-chart"></i>&nbsp; Assessment Manager
                   </h3>
                   <ul class="breadcrumb" >
                       
                       <li>
                           UEIS Workshop
                           <span class="divider">&nbsp; | &nbsp;</span>
                       </li>
                       
                        <li>
                           <i class="icon-home"></i> <a href="<?php echo SITEURLM;?>">Home</a>
                           <span class="divider">></span>
                       </li>
                       
                       <li>
                            <a href="<?php echo SITEURLM;?>/index.php/assessment">Assessment Manager</a>
                          <span class="divider">></span>
                       </li>
                       
                        <li>
                            <a href="<?php echo SITEURLM;?>/index.php/assessment/assessments/<?php echo $cat_id;?>/<?php echo $_REQUEST['assessment_name'];?>"><?php echo ucfirst($assessnamearr[0]).' '.ucfirst($assessnamearr[1]);?></a>
                          
                       </li>
                                              
                      
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
             <div class="row-fluid">
                 <div class="span12">
                     <!-- BEGIN BLANK PAGE PORTLET-->
                     <div class="widget orange">
                         <div class="widget-title">
                             <h4><?php echo ucfirst($assessnamearr[0]).' '.ucfirst($assessnamearr[1]);?></h4>
                          
                         </div>
                         <div class="widget-body">
<!--                            <form class="form-horizontal" action="#">-->
                                <div id="pills" class="custom-wizard-pills-orange">
                                 <ul>
                                     <li><a href="#pills-tab1" data-toggle="tab" onclick="window.location='<?php echo SITEURLM;?>/index.php/assessment/assessments/<?php echo $cat_id;?>/<?php echo $_REQUEST['assessment_name'];?>#pills-tab1'" >Step 1</a></li>
                                     <li><a href="#pills-tab2" data-toggle="tab" id="step2" onclick="window.location='<?php echo SITEURLM;?>/index.php/assessment/assessments/<?php echo $cat_id;?>/<?php echo $_REQUEST['assessment_name'];?>#pills-tab2'">Step 2</a></li>
                                     <li><a href="#pills-tab3" data-toggle="tab">Step 3</a></li>
                                     
                                 </ul>
                                 <div class="progress progress-success-orange progress-striped active">
                                     <div class="bar"></div>
                                 </div>
                                 <div class="tab-content">
                                 
                                  <!-- BEGIN STEP 1-->
                                     <div class="tab-pane" id="pills-tab1">
                                         <h3 style="color:#000000;">STEP 1</h3>
<!--                                         <form class="form-horizontal" action="#">-->
                                         
                                <div class="control-group">
                                    <label class="control-label">Select Student</label>
                                    <div class="controls">
                                        <input type="text" id="student_id" value="<?php echo $_SESSION['user_id'];?>">
                                        
                                    </div>
                                </div>
                                
                                        
                                         
                                        
                                     </div>
                                     
                                  <!-- BEGIN STEP 2-->
<!--                                     <div class="tab-pane" id="pills-tab2">
                                         <h3 style="color:#000000;">STEP 2</h3>
                                          <div class="control-group">
                                            <label class="control-label">Select Student</label>
                                            <div class="controls">
                                                <select class="span12 chzn-select" data-placeholder="--Please Select--" tabindex="1" style="width: 300px;" id="assessmenttype">
                                                <option value=""></option>
                                                <option value="current" <?php if($module_name=='start_quiz') echo "selected='selected'";?>>Current Assessments</option>
                                                <option value="previous" <?php if($module_name=='old_assignments') echo "selected='selected'";?>>Previous Assessments</option>

                                            </select>
                                            </div>
                                </div>
                                        
                                     </div>-->
                                  
                                  <!-- BEGIN STEP 3-->
                                     <div class="tab-pane" id="pills-tab3">
                                         <h3 style="color:#000000;">STEP 3</h3>
                                                          <?php         
                                            
						include "modules/".$module_name."_tmp.php";
                                             ?>                        
                                     </div>
                                     
                                    
                                     <ul class="pager wizard">
                                         <li class="previous first orange"><a href="javascript:;">First</a></li>
                                         <li class="previous orange"><a href="javascript:;">Previous</a></li>
                                         <li class="next last orange"><a href="javascript:;">Last</a></li>
                                         <li class="next orange"><a  href="javascript:;" onclick="nextscreen();">Next</a></li>
                                     </ul>
                                 </div>
                             </div>
                             </form>
                         </div>
                     </div>
                     <!-- END BLANK PAGE PORTLET-->
                 </div>
             </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->

   <!-- BEGIN FOOTER -->
   <div id="footer">
       UEIS © Copyright 2012. All Rights Reserved
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   
   <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../js/jquery.blockui.js"></script>
   <script src="../assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/clockface/js/clockface.js"></script>
   <script type="text/javascript" src="../assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script src="../assets/fancybox/source/jquery.fancybox.pack.js"></script>
   <script type="text/javascript" src="../assets/gritter/js/jquery.gritter.js"></script>
   <script type="text/javascript" src="../js/jquery.pulsate.min.js"></script>
   <script type="text/javascript" src="../js/jquery.validate.js"></script>
   <!--common script for all pages-->
   <script src="../js/common-scripts.js"></script>
   <!--script for this page-->
<!--   <script src="../js/form-wizard.js"></script>-->
   <script src="../js/form-component.js"></script>

   <!-- END JAVASCRIPTS -->   
   <script>
       $( document ).ready(function() {
            $("#pills").bootstrapWizard("show",2);
       });
   </script>
   
   <script>
       var Script = function () {

    $('#pills').bootstrapWizard({'tabClass': 'nav nav-pills', 'debug': false, onShow: function(tab, navigation, index) {
        console.log('onShow');
    }, onNext: function(tab, navigation, index) {
        return false;
        console.log('onNext');
    }, onPrevious: function(tab, navigation, index) {
        console.log('onPrevious');
    }, onLast: function(tab, navigation, index) {
        console.log('onLast');
    }, onTabShow: function(tab, navigation, index) {
//        console.log(tab);
//        console.log(navigation);
//        console.log(index);
//        console.log('onTabShow1');
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#pills').find('.bar').css({width:$percent+'%'});
    }});

}();
       
       
       $('#student_id').change(function(){
           $.ajax({
                type: "POST",
                url: "<?php echo SITEURLM;?>/index.php/assessment/diagnostic_screener_session",
                data: { student_id: $('#student_id').val() }
              })
                .done(function( msg ) {
                  alert( "Data Saved: " + msg );
                });
       });
       
            function nextscreen (){
//                alert('test');
                 if($('#assessmenttype').val()=='current' && $('#student_id').val()!=''){
                     window.location='<?php echo SITEURLM?>/estrellita/index.php?module=start_quiz&id=55'
                 } else if($('#assessmenttype').val()=='previous' && $('#student_id').val()!=''){
                     window.location = '<?php echo SITEURLM?>/estrellita/index.php?module=old_assignments&mid=8';
                 }
            }
        
   </script>
</body>

    
</html>