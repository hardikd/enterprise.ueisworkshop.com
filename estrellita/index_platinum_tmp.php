<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $PAGE_TITLE; error_reporting(0); ?></title>      
        <script language ="javascript" src="jquery.js"></script>
        <script language ="javascript" src="extgrid.js"></script>
        <script language ="javascript" src="util.js"></script>
        <script src="cms.js" type="text/javascript"></script>
        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.min.css" />
        <!-- gebo blue theme-->
            <link rel="stylesheet" href="css/blue.css" id="link_theme" />
        <!-- breadcrumbs-->
            <link rel="stylesheet" href="lib2/jBreadcrumbs/css/BreadCrumb.css" />
        <!-- tooltips-->
            <link rel="stylesheet" href="lib2/qtip2/jquery.qtip.min.css" />
        <!-- colorbox -->
            <link rel="stylesheet" href="lib2/colorbox/colorbox.css" />    
        <!-- code prettify -->
            <link rel="stylesheet" href="lib2/google-code-prettify/prettify.css" />    
        <!-- notifications -->
            <link rel="stylesheet" href="lib2/sticky/sticky.css" />    
        <!-- splashy icons -->
            <link rel="stylesheet" href="img/splashy/splashy.css" />
		<!-- flags -->
            <link rel="stylesheet" href="img/flags/flags.css" />	
		<!-- calendar -->
       
        <!-- main styles -->
            <link rel="stylesheet" href="css/style.css" />
			

	
        <!-- Favicon -->
            <link rel="shortcut icon" href="favicon.ico" />
		
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/ie.css" />
            <script src="js/ie/html5.js"></script>
			<script src="js/ie/respond.min.js"></script>
			<script src="lib2/flot/excanvas.min.js"></script>
        <![endif]-->
		
		<script>
			//* hide all elements & show preloader
			document.documentElement.className += 'js';
		</script>
                
           
                
    </head>
    <body>
        
		<div id="maincontainer" class="clearfix">
			<!-- header -->
            <header>
                <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <div class="container-fluid">
                            <a class="brand" href="<?php echo $home ?>"><i class="icon-home icon-white"></i> <?php echo $SYSTEM_NAME ?></a>
                            <ul class="nav user_menu pull-right">
                                <li class="nb_boxes clearfix"><img id="imgAjaxLoader" style="display:none" src="img/ajax_loader.gif" />&nbsp;&nbsp;&nbsp;</li>
                                <li class="hidden-phone hidden-tablet">
                                    <div class="nb_boxes clearfix">
                                        <a data-toggle="modal" data-backdrop="static" href="#myMail" class="label ttip_b" title="<?php echo NOTIFICATIONS ?>"><span id="lblNotsCount">0</span> <i class="splashy-mail_light"></i></a>                                        
                                    </div>
                                </li>
								<!--<li class="divider-vertical hidden-phone hidden-tablet"></li>
                                <li class="dropdown">
                                 <?php //echo $languages_html ?>
                                </li>
                                <li class="divider-vertical hidden-phone hidden-tablet"></li>-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="img/user_avatar.png"  alt="" class="user_avatar" /> <?php echo $fullname ?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
										<li><a href="?module=myprofile"><?php echo MY_PROFILE ?></a></li>										
										<li class="divider"></li>
										<li><a href="logout.php"><?php echo LOGOUT ?></a></li>
                                    </ul>
                                </li>
                            </ul>
							<ul class="nav" id="mobile-nav">
							
								<?php echo $html; ?>
								
<!--								<li>
<a href="#">
                                <i class="icon-book icon-white"></i> Back To Workshop</a>
								</li>-->
								
							</ul>
                        </div>
                    </div>
                </div>
                <div class="modal hide fade" id="myMail">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">×</button>
                        <h3><?php echo NOTIFICATIONS ?></h3>
                    </div>
                    <div class="modal-body">
                        
                        <div id="divNots"></div>
                        <div style="display:none" id="divNotBody"></div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:ShowNotList()" class="btn"><?php echo NOTIFICATION_LIST ?></a>
                    </div>
                </div>
                
                
           <!-- Add Padding div-->
   			 <div class="padding"></div>
    
            </header>
            
            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                
                <?php if($module_name!="default_page1") { ?>    
                <h3 class="heading" id="pageheading">
                    <?php 
                        echo @desc_func();
                    ?>
                </h3>
                <?php } ?>
                    
				
                                            <?php         
                                            
						include "modules/".$module_name."_tmp.php";
                                             ?>
                        
                </div>
            </div>
            
			<!-- sidebar -->
            <a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
            <div class="sidebar">
				
				<div >
					<div >
						<div class="antiscroll-content">
					
							<div class="sidebar_inner">
                            <?php if ($_SESSION['teacher_id']=="")
							{
							?>
								<form action="index.php?module=search_content" class="input-append" method="post" >
									<input autocomplete="off" name="query" class="search_query input-medium" size="16" type="text" placeholder="<?php echo SEARCH ?>..." /><button type="submit" class="btn"><i class="icon-search"></i></button>
								</form> <?php ?>
								<div id="side_accordion" class="accordion">
									<?php
                                                                    for($z=0;$z<sizeof($main_modules);$z++){ 
                                                                        $in ="";
                                                                        if(isset($_GET['mid']))
                                                                        {
                                                                            if($_GET['mid']==$main_modules[$z]['id']) $in="in";
                                                                        }
                                                                        ?>
                                                                        
                 <?php //
													  //  echo "<pre>";
													  // print_r( $main_modules[$z]['module_name']);
													   												   
													    ?>
                                            <?php //echo $MODULES[$main_modules[$z]['module_name']] ?>                                                                  
                                                                        
                                                           
									<div class="accordion-group">
										<div class="accordion-heading">
											<a href="#collapseOne<?php echo $z ?>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
												<i class="icon-th"></i> <?php //echo $MODULES[$main_modules[$z]['module_name']] ?>
                                                
                                                 <?php echo $main_modules[$z]['module_name']; ?>
											</a>
										</div>
										<div class="accordion-body collapse <?php echo $in ?>" id="collapseOne<?php echo $z ?>">
											<div class="accordion-inner">
												<ul class="nav nav-list">
                                                                                                    <?php for($y=0;$y<sizeof($child_modules[$main_modules[$z]['id']]);$y++) {  ?>
                                                                                                    <li>
                                                                                                     <?php 
                                                                                                         $file_name = $child_modules[$main_modules[$z]['id']][$y]["file_name"];
 $module_id=$main_modules[$z]['id'];
//    echo "<a  href='index.php?module=$file_name&mid=$module_id'>".$MODULES[$child_modules[$main_modules[$z]['id']][$y]["module_name"]]."</a>";

 echo "<a  href='index.php?module=$file_name&mid=$module_id'>".$child_modules[$main_modules[$z]['id']][$y]["module_name"]."</a>";
                                                                                                    ?>
                                                                                                    </li>
                                                                                                    <?php } ?>
                                                                                                      
                                                                                                     
												</ul>
											</div>
										</div>
									</div>
                                                                        <?php } ?>
							<?php }//print_r($_SESSION);
							    if ($_SERVER["HTTP_HOST"] == "localhost")
		   $baseUrl = "../index.php/";
		   else
		    $baseUrl = "../index.php/";
		     
	
							  if($_SESSION['loginas1']== "teacher" || $_SESSION['teacher_id']!="")  {?>
							<div class="accordion-group">
								<div class="accordion-heading">
                                    <ul class="nav nav-list"><li>
                                        <a href="index.php?module=active_teacher_assignment" >
                                           <i class="icon-th"></i>  <?php echo Assessment_Administration ?>
                                        </a></li>
                                     </ul>
                             </div>
                             </div>
                                    
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                <i class="icon-th"></i> <?php echo Assessment_Reports ?></a>
                              	</div>    
                               
                                <div class="accordion-body collapse <?php echo $in ?>" id="collapseOne<?php echo $z ?>">
                                    <div class="accordion-inner">
                                        <ul class="nav nav-list">  
                                      <!--  <li><a href="/WAPTrunk/index.php/classroomgrowthgraph">abdjsadj sb</a></li> -->
                                      <li><a style="background:none;"href="<?php echo $baseUrl; ?>classroomgrowthgraph" >
                                        <?php echo VIEW_CLASSROOMS_GROWTH ?>
                                       </a></li>
                                       <li> <a target="none;" style="background:none;" href="<?php echo $baseUrl;?>teacherclusterreport" >
                                        <?php echo CLASS_PERFORMANCE_SCORES_BY_STANDARD ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl;?>teacherclustergraph" >
                                        <?php echo CLASS_PERFORMANCE_GRAPHS_BY_STANDARD ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl; ?>studentdetail" >
                                        <?php echo Individual_Student_Performance_Scores ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl; ?>selectedstudentreport" >
                                        <?php echo Individual_Student_Performance_by_Standard ?>
                                       </a></li>
                                        </ul>
                                    </div>
                                </div>
                           </div>  
                                     <?php }  
						 	
                              if($_SESSION['loginas1']== "district")  { ?>       
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                <a href="#collapseOne<?php echo $z ?>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                <i class="icon-th"></i> <?php echo Assessment_Reports ?></a>
                              	</div>    
                               
                                <div class="accordion-body collapse <?php echo $in ?>" id="collapseOne<?php echo $z ?>">
                                    <div class="accordion-inner">
                                        <ul class="nav nav-list">  
                                        
                                        <li> <a style="background:none;"  href="<?php echo $baseUrl; ?>classroomgrowthgraph" >
                                        <?php echo VIEW_CLASSROOMS_GROWTH ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl;?>schooldistrictreport" >
                                        <?php echo Performance_score_by_Standard ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl;?>schooldistrictgraph" >
                                        <?php echo Performance_Graphs_by_Standard ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl; ?>studentdetail" >
                                        <?php echo Individual_Student_Performance_Scores ?>
                                       </a></li>
                                       <li> <a style="background:none;" href="<?php echo $baseUrl; ?>selectedstudentreport" >
                                        <?php echo Individual_Student_Performance_by_Standard ?>
                                       </a></li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                                     <?php   } ?>
                                                                        <?php if(ENABLE_CALCULATOR=="yes") { ?>
								
									<div class="accordion-group">
										<div class="accordion-heading">
											<a href="#collapse7" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle" onClick="showCalculator()">
											   <i class="icon-th"></i> <?php echo CALCULATOR ?>
											</a>
										</div>
										<div class="accordion-body collapse" id="collapse7" style="display:none">
											<div class="accordion-inner">
												<form name="Calc" id="calc">
													<div class="formSep control-group input-append">
														<input type="text" style="width:130px" name="Input" /><button type="button" class="btn" name="clear" value="c" onClick="Calc.Input.value = ''"><i class="icon-remove"></i></button>
													</div>
													<div class="control-group">
														<input type="button" class="btn btn-large" name="seven" value="7" onClick="Calc.Input.value += '7'" />
														<input type="button" class="btn btn-large" name="eight" value="8" onClick="Calc.Input.value += '8'" />
														<input type="button" class="btn btn-large" name="nine" value="9" onClick="Calc.Input.value += '9'" />
														<input type="button" class="btn btn-large" name="div" value="/" onClick="Calc.Input.value += ' / '">
													</div>
													<div class="control-group">
														<input type="button" class="btn btn-large" name="four" value="4" onClick="Calc.Input.value += '4'" />
														<input type="button" class="btn btn-large" name="five" value="5" onClick="Calc.Input.value += '5'" />
														<input type="button" class="btn btn-large" name="six" value="6" onClick="Calc.Input.value += '6'" />
														<input type="button" class="btn btn-large" name="times" value="x" onClick="Calc.Input.value += ' * '" />
													</div>
													<div class="control-group">
														<input type="button" class="btn btn-large" name="one" value="1" onClick="Calc.Input.value += '1'" />
														<input type="button" class="btn btn-large" name="two" value="2" onClick="Calc.Input.value += '2'" />
														<input type="button" class="btn btn-large" name="three" value="3" onClick="Calc.Input.value += '3'" />
														<input type="button" class="btn btn-large" name="minus" value="-" onClick="Calc.Input.value += ' - '" />
													</div>
													<div class="formSep control-group">
														<input type="button" class="btn btn-large" name="dot" value="." onClick="Calc.Input.value += '.'" />
														<input type="button" class="btn btn-large" name="zero" value="0" onClick="Calc.Input.value += '0'" />
														<input type="button" class="btn btn-large" name="DoIt" value="=" onClick="Calc.Input.value = Math.round( eval(Calc.Input.value) * 1000)/1000" />
														<input type="button" class="btn btn-large" name="plus" value="+" onClick="Calc.Input.value += ' + '" />
													</div>													
												</form>
											</div>
										 </div>
									</div>
                                                                        <?php } ?>
								</div>
								
								<div class="push"></div>
							</div>
							   
						
						
						</div>
					</div>
				</div>
			
			</div>
            
            <script src="js/jquery.min.js"></script>
			<!-- smart resize event -->
			<script src="js/jquery.debouncedresize.min.js"></script>
			<!-- hidden elements width/height -->
			<script src="js/jquery.actual.min.js"></script>
			<!-- js cookie plugin -->
			<script src="js/jquery_cookie.min.js"></script>
			<!-- main bootstrap js -->
			<script src="bootstrap/js/bootstrap.min.js"></script>
			<!-- bootstrap plugins -->
			<script src="js/bootstrap.plugins.min.js"></script>
			<!-- tooltips -->
			<script src="lib2/qtip2/jquery.qtip.min.js"></script>
			<!-- jBreadcrumbs -->
			<script src="lib2/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
			<!-- lightbox -->
            <script src="lib2/colorbox/jquery.colorbox.min.js"></script>
            <!-- fix for ios orientation change -->
			<script src="js/ios-orientationchange-fix.js"></script>
			<!-- scrollbar -->
			<script src="lib2/antiscroll/antiscroll.js"></script>
			<script src="lib2/antiscroll/jquery-mousewheel.js"></script>
			<!-- to top -->
			<script src="lib2/UItoTop/jquery.ui.totop.min.js"></script>
			<!-- mobile nav -->
			<script src="js/selectNav.js"></script>
			<!-- common functions -->
			<script src="js/gebo_common.js"></script>
			
			<script src="lib2/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
            <!-- touch events for jquery ui-->
            <script src="js/forms/jquery.ui.touch-punch.min.js"></script>
            <!-- multi-column layout -->
            <script src="js/jquery.imagesloaded.min.js"></script>
            <script src="js/jquery.wookmark.js"></script>
            <!-- responsive table -->
            <script src="js/jquery.mediaTable.min.js"></script>
            <!-- small charts -->
            <script src="js/jquery.peity.min.js"></script>
            <!-- charts -->
            <script src="lib2/flot/jquery.flot.min.js"></script>
            <script src="lib2/flot/jquery.flot.resize.min.js"></script>
            <script src="lib2/flot/jquery.flot.pie.min.js"></script>
            <!-- calendar -->
            <!-- dashboard functions -->
            <script src="lib2/sticky/sticky.min.js"></script>

			<script>
				$(document).ready(function() {
					//* show all elements & remove preloader
					setTimeout('$("html").removeClass("js")',0);
				});
			</script>
                       
      		<script type="text/javascript">
			 function showCalculator()
			 {
				 document.getElementById("collapse7").style.display="";
			 }
			</script>
		
		</div>
	</body>
</html>