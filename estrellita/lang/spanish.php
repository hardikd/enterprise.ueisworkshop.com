<?php

define("EDIT","editar");
define("DELETE","borrar");
define("ADD","añadir");
define("SAVE","guardar");
define("CANCEL","cancelar");
define("SEND","enviar");
define("CCONTINUE","continuar");
define("UP","hasta");
define("DOWN","abajo");
define("NEXT","próximo");
define("PREVIOUS","anterior");
define("FINISH","terminar");
define("PAGES","Páginas");
define("ENABLE","permitir");
define("DISABLE","inhabilitar");
define("PRIORITY","prioridad");
define("BACK","espalda");
define("SEARCH","Buscar");
define("SHOW_ALL","Mostrar todo");
define("ADDED_DATE","Fecha de creación");
define("ARE_YOU_SURE","¿Está seguro?");
define("CLICK_OK_TO_COMPLETE_ASSESSMENT","Haga clic en Aceptar para completar la evaluación");
define("NO_RECORDS","No se encontraron registros");
define("ASSESSMENT_STOPPED","Evaluación detuvo: Estudiante no cumplir con el requisito mínimo.");
define("ASSESSMENT_UNSUCCESS","Evaluación terminó - el estudiante no cumple con referencia necesaria");
define("PLEASE_WAIT","Por favor, espere ...");
define("SELF_CORRECTS_VOWEL","Auto-corrige vocales (sonido final)");

define("WAIT","Espera ...");
define("MUST_ANSWER_ALL","Debe contestar todas las preguntas para continuar.");

define("CAT_DESC","Categorías");
define("CAT_NAME","nombre de la categoría");

define("Assessment_Reports","Informes de Evaluación");
define("VIEW_CLASSROOMS_GROWTH","Ver un crecimiento Aulas");
define("CLASS_PERFORMANCE_SCORES_BY_STANDARD","Clase puntuaciones de rendimiento por parte de Standard");
define("CLASS_PERFORMANCE_GRAPHS_BY_STANDARD","Clase gráficos de rendimiento por parte de Standard");
define("Individual_Student_Performance_Scores","Resultados Rendimiento del alumno individual");
define("Individual_Student_Performance_by_Standard","Rendimiento de los alumnos por parte de Standard Individual");

define("Grade_Level_Report","Reporte de Grado");
define("Performance_score_by_Standard","Puntuación de rendimiento por parte de Standard");
define("Performance_Graphs_by_Standard","Los gráficos de rendimiento por parte de Standard");

define("QUIZZES","Cuestionarios");
define("QUIZ_NAME","Cuestionario nombre");
define("QUIZ_DESC","descripción");
define("QUESTION","pregunta");
define("QUESTIONS","Preguntas");
define("NEW_QUIZ","Agregar evaluación");
define("ADD_EDIT_QUIZ","Agregar / Editar concurso");
define("CAT","categoría");
define("NAME","nombre");
define("DESC","descripción");
define("SHOW_INTRO","Ver introducción");
define("INTRO_TEXT","Introducción de texto");
define("QUIZ_NAME_VAL","El nombre no puede estar vacío");
define("QUIZ_DESC_VAL","La descripción no puede estar vacío");
define("QUIZ_CAT_VAL","Por favor, seleccione la categoría");
define("EXPORT","exportar");
define("ASSESSMENT_EXPORT","Evaluación de las Exportaciones");

define("TYPE","tipo");
define("POINT","punto");
define("NEW_QUESTION","Añadir nueva pregunta");
define("ADD_EDIT_QUESTION","Agregar / Editar pregunta");

define("HEADER_TEXT","texto del encabezado");
define("FOOTER_TEXT","texto de pie de página");
define("SELECT_TEMP","tipo de pregunta");
define("CAN_BE_EMPTY","Puede estar vacío");
define("ANSWER_VARIANTS","respuesta Variant");
define("CORRECT_ANSWER","respuesta correcta");
define("ENTER_CORRECT_ANSWER","Escriba respuesta correcta");
define("CANNOT_DELETE_LAST","No se puede borrar la última elección");

define("ADD_EDIT_USER","Agregar / Editar usuario");
define("LOCAL_USERS","Los usuarios locales");
define("LOGIN","login");
define("USER_NAME","nombre");
define("USER_SURNAME","apellido");
define("USER_TYPE","tipo de usuario");
define("USER_ID","ID de usuario");
define("EMAIL","E-mail");
define("NEW_USER","Añadir un nuevo usuario");
define("PASSWORD","contraseña");
define("LOGIN_ALREADY_EXISTS","Este inicio de sesión ya existe!");


define("LOGIN_VAL","Login no puede estar vacío");
define("PASSWORD_VAL","La contraseña no puede estar vacío");
define("USER_TYPE_VAL","Por favor, seleccione el tipo de usuario");


define("ASSIGNMENTS","Asignaciones");
define("NEW_ASSIGNMENT","Añadir nueva asignación");
define("START","iniciar");
define("STOP","Deténgase");
define("INFORMATION","información");
define("REPORTS","Informes");


define("ADD_ASSIGNMENT","Agregar asignación");
define("TEST","prueba");
define("NOT_SELECTED","No seleccionado");
define("SHOW_RESULTS","Mostrar resultados para el usuario");
define("RESULTS_BY","Resultados por");
define("SUCCESS_POINT_PERC","Punto de Éxito / ciento");
define("REVIEW_ANSWERS","Los usuarios pueden revisar las respuestas correctas");
define("TEST_TIME","El tiempo de prueba (en minutos)");
define("IMPORTED_USERS","los usuarios importados");
define("O_QUIZ","examen");
define("O_SURVEY","estudio");
define("O_YES","sí");
define("O_NO","No");
define("O_POINT","punto");
define("O_PERCENT","por ciento");
define("DETAILS","Detalles");
define("VIEW_DETAILS","Ver más detalles");
define("QUESTIONS_ORDER","para preguntas");
define("ANSWERS_ORDER","para Answers");
define("BY_PRIORITY","Por prioridad");
define("RANDOM","azar");



define("CATEGORY_VAL","Por favor, seleccione la categoría");
define("TEST_VAL","Por favor, seleccione la prueba");
define("SUCCESS_VAL","Por favor, introduzca punto de éxito");
define("TIME_VAL","Por favor, introduzca el tiempo de prueba");
define("SUCCESS_NUM_VAL","Por favor, introduzca punto de éxito en formato numérico");
define("TIME_NUM_VAL","Por favor, introduzca el tiempo de prueba en formato numérico");

define("VIEW_ASSIGNMENT","Ver asignación");
define("STATUS","estado");
define("SUCCESS","éxito");
define("TOTAL_POINT","Puntos totales");
define("ASG_CANNOT_BE_FOUND","Esta asignación no se puede encontrar");
define("PRIORITY","prioridad");
define("ACTIVE_ASSIGNMENTS","Las evaluaciones de Active");
define("ALREADY_FINISHED","Ya has terminado esta prueba / encuesta");
define("COMPLETED","terminado");
define("ASSESSMENT_COMPLETED","Evaluación completa.");
define("NO_ACTIVE_ASG","No assginments activos que se encuentran");


define("OLD_ASSIGNMENTS","Las evaluaciones históricas");
define("OLD_QUIZ_ASSIGNMENTS","Antiguo Concurso Asignaciones");
define("OLD_SURVEY_ASSIGNMENTS","Antiguo Encuesta Asignaciones");
define("ASSESSMENT_NAME","Nombre Evaluación");
define("START_DATE","fecha de inicio");
define("FINISH_DATE","fecha de finalización");
define("PASS_SCORE","Pase puntuación");
define("PREVIOUS_ASSESSMENTS","Las evaluaciones anteriores");
define("CURRENT_ASSESSMENTS","Las evaluaciones actuales");


define("INTRO","Introducción");
define("QUIZ_SURV","");  //Cuestionario / Encuesta
define("QUIZ_NO_ACCESS","Usted no tiene acceso a este cuestionario / encuesta");
define("QUIZ_FINISHED","Terminado. Gracias.");
define("YOUR_TOTAL_POINT","Su total de puntos");
define("SUCCESS_POINT","punto de éxito");
define("EXAM_SUCCESS","Pasaste examen con éxito!");
define("EXAM_UNSUCCESS","Lo sentimos, usted no pasar el examen con éxito");
define("TIME_ENDED","Hora de finalización");
define("TIME","tiempo");


//define("VIEW_DETAILS","View details");
define("NOT_ENABLED","No habilitado");


define("LOG_IN","login"); // button
define("LOGIN_INCORRECT","Usuario o contraseña son incorrectos");
define("LANGUAGE","idioma");


define("RATINGS","Valoraciones");
define("ADD_EDIT_RATING","Agregar / Editar calificación");
define("USER_RATES_DETAILS","Las tasas de usuario detalles");
define("RAT_TYPE","tipo");
define("DESCRIPTION","descripción");
define("USER_RATES","las tasas de usuario");
define("HTML_CODE","Código Html");
define("RAT_SELECT_TEMP","Seleccione la plantilla");
define("IMG_COUNT","n º de Imágenes");
define("USER_WAR","Garantía del usuario Unique");
define("BGCOLOR","El color de fondo");
define("RAT_USER_RATES","las tasas de usuario");
define("RAT_PRODUCT","producto");
define("AVG_RATE","tasa avarage");
define("MAX_RATE"," tasa de Max");
define("RAT_COUNT","Las tasas globales de contar");
define("RAT_DETAILS","Detalles");
define("BACK_TO_RAT","Realice una copia de Notas");
define("RATE","velocidad");
define("IP_ADDRESS"," dirección IP");
define("RAT_USER_NAME","nombre de usuario");
define("NEW_RAT","Nueva Clasificación");
define("DELETED_RATE","Esta clasificación ya se ha eliminado");

define("RATED","Calificación");
define("OUT_OFF","fuera de");
define("INT_RATINGS","calificaciones");
define("ALREADY_RATED","Ya has valorado");


define("RAT_DESC_VAL","La descripción no puede estar vacío");
define("RAT_IMG_VAL","N º de Imágenes no puede estar vacío");
define("RAT_IMG_NUMERIC_VAL","N º de Imágenes debe ser numérico");

define("ALWAYS","siempre");
define("NEVER","nunca");

define("REST_BY_IP","Restringir por Dirección IP");
define("REST_BY_LOGIN","Restringir haciendo caja de la conexión");
define("REST_BY_COOKIE","Restringir utilizando cookies");

define("GENERATED_HTML","Código HTML generado");
define("PREVW","avance");
define("PUT_THIS","Pon este código html en su página");
define("EDIT_THIS_RAT","Editar esta calificación ");


// translating registration form

define("R_REG_FORM","formulario de inscripción"); 
define("R_NAME","nombre"); 
define("R_SURNAME","apellido"); 
define("R_LOGIN","login"); 
define("R_PASSWORD","contraseña"); 
define("R_EMAIL","Email"); 
define("R_ADDRESS","dirección"); 
define("R_PHONE","número de teléfono"); 
define("R_REGISTER","registro"); 
define("R_CANCEL","cancelar"); 
define("R_APPROVED","aprobado"); 
define("R_DISABLED","discapacitado"); 

define("R_NAME_VAL","El nombre no puede estar vacío"); 
define("R_SURNAME_VAL","Apellido no puede estar vacío"); 
define("R_LOGIN_VAL","Por favor, introduzca el nombre de usuario en un formato válido (sólo caracteres alfanuméricos)"); 
define("R_PASSWORD_VAL","Por favor, introduzca la contraseña en un formato válido (sólo caracteres alfanuméricos)"); 
define("R_EMAIL_VAL","Por favor, introduzca la dirección de correo electrónico en un formato válido"); 

define("R_EMAIL_SENT", "Un correo electrónico ha sido enviado. Por favor, revise su correo y aprobar registro.");
define("R_APPROVE_REG", "Aprobar el registro");
define("R_URL_WRONG", "URL introducido no es correcto");
define("R_REG_APPROVED", "El registro ha sido aceptado!");
define("R_REG_ALREADY_APPROVED", "Su registro ha sido aceptado!");
define("R_GO_TO", "Ir a");
define("R_LOGIN_PAGE", "Ingresa página");

define("CHNG_PASS","Cambiar contraseña");
define("OLD_PASS","Old contraseña");
define("NEW_PASS","Nueva contraseña");

define("WRONG_PASS","Contraseña que ha introducido no es correcta");
define("PASS_CHANGED", "Su contraseña ha sido cambiada");
define("ONLY_LOCAL", "De Sólo los usuarios locales pueden cambiar la contraseña");

define("OLD_PASS_VAL","Por favor, introduzca la contraseña antigua en un formato válido (sólo caracteres alfanuméricos)");
define("NEW_PASS_VAL","Por favor, introduzca la nueva contraseña en un formato válido (sólo caracteres alfanuméricos)");

define("REG_FORGOT_PASS", "¿Olvidó su contraseña?");
define("REG_NEW_USER", "¿Nuevo usuario?");

define("EMAIL_TEMPLATES","plantillas de correo electrónico");
define("CANNOT_FIND_TEMP","Si no encuentra esta plantilla");
define("TEMP_SAVED", "Plantilla se ha guardado");

define("E_SUBJECT", "tema");
define("E_BODY", "cuerpo");
define("E_VARS", "Las variables disponibles (caso sentivity)");

define("ASG_HOW_MANY", "Los usuarios pueden tomar este examen");
define("ASG_ONCE", "sólo una vez");
define("ASG_NO_LIMIT", "sólo una vez...");

define("ASG_AFFECT_CHANGE", "cambios Freeze");
define("AFFECT", "No congele");
define("DONT_AFFECT", "congelar ");
define("ASG_SEND_RESULTS", "Enviar resultados de pruebas por correo");
define("ASG_SEND_AUTO", "Automaticaly de acabado");
define("ASG_SEND_MAN", "a mano");

define("START_SENT", "Iniciar electrónico");
define("RESULTS_SENT", "mail Resultados");

define("TAKE_AGAIN","Tome de nuevo");
define("ENABLE_FOR_NEW","Auto asignar a los usuarios recién registrados");

define("CONTENT_MAN","gestión de contenidos");
define("ADD_PAGE","Añadir nueva página");
define("MENU_NAME","nombre de menú");
define("MENU_NAME_VAL","Nombre del menú no puede estar vacío");
define("PAGE_CONTENT","contenido de la página");
define("MENU_PRIORITY_VAL","Por favor, introduzca la prioridad en un formato numérico");
define("MENU_PRIORITY_E_VAL","Por favor, introduzca prioridad");
define("EMAIL_ALREADY_EXISTS","El E-mail ya está existe!");

define("SQL_QUERIES","consultas SQL");
define("ENABLE_DEBUGGING", "Para ver las consultas SQL favor, active esta función desde el archivo config.php");

define("MAIL_ADDRESS", "dirección de correo");
define("CHECK_MAIL", "Por favor, consultar su correo electrónico");
define("TEST_MAIL", "electrónico de prueba");

define("PENALTY_POINT","punto de penalty");
define("ANSWER_DESCRIPTION","respuesta Descripción");
define("QUESTIONS_BANK","banco de preguntas");
define("ASSIGNMENT_NAME","nombre del trabajo");
define("CHILD_PAGES","páginas infantiles");
define("NOTIFICATIONS", "Notificaciones");
define("SUBJECT", "tema");
define("BODY", "cuerpo");
define("SENDER", "transmisor");
define("NOTIFICATION_LIST","lista de Notificaciones");
define("SendToAll", "Este mensaje se enviará a todos los usuarios de asignación");
define("YOUHAVE", "tiene");
define("MY_PROFILE", "Mi Perfil");
define("LOGOUT", "finalizar la sesión");
define("PHOTO", "foto");
define("UP_FILE_SIZE_MSG1","El tamaño del archivo subido no debe ser mucho más que");
define("UP_FILE_SIZE_MSG2","KB y debe estar en los formatos que figuran a continuación");
define("UP_FILE_SIZE_MSG3","Ancho de la imagen debe ser");
define("USERS_COUNT","Los usuarios cuentan");
define("QUESTIONS_COUNT","Preguntas cuentan");
define("EXAMS_COUNT","Exámenes cuenta");
define("SURVEYS_COUNT","Encuestas cuentan");
define("R_USERS","Usuarios");
define("R_ADD_NEW_QUIZ","Agregar evaluación");
define("R_ADD_NEW_USER","Añadir un nuevo usuario");
define("R_ADD_NEW_PAGE","Añadir nueva página");
define("R_CATS","Categorías");
define("R_QUIZZES","Las evaluaciones formales");
define("R_QUESTIONS_BANK","banco de preguntas");
define("R_ASSIGNMENTS","Las evaluaciones informales");
define("R_PAGES","Páginas");
define("DATE_REGISTERED","Fecha de registro");
define("USERS_COUNT_BY_COUNTRY","Los usuarios cuentan por país");

define("COUNTRY","país");
define("COUNTRY_VAL","País no puede estar vacío");

define("SING_IN_SYSTEM", "Iniciar sesión en el sistema");
define("ENTER_USERNAME", "All fecha protected under FERPA (20 USC § 1232g; 34 CFR Parte 99)
");
define("REGISTER_IN_SYSTEM","¿No estás registrado? Inscríbete aquí");
define("SIGN_IN_SCREEN","Enviar me regreso a la pantalla de inicio de sesión");
define("EMAIL_TEXT","La dirección de correo electrónico no se hace pública y sólo se utiliza si quiere recibir una contraseña nueva.");
define("EMAIL_NOT_EXISTS","Esta dirección de correo electrónico no existe en nuestra base de datos");
define("CLOSE", "cerrar");
define("T_AND_C", "Términos y Condiciones");

define("T_AND_C_TEXT", "Términos y Condiciones texto");
define("T_ACCEPT", "Rellenando el siguiente formulario y haga clic en el \"Sign Up \" botón, usted acepta y acuerda");
define("T_AND_S", "Términos de Servicio");
define("SIGN_UP_TO", "");
define("WRKSHOP_QUIZ_LOGIN", "Plataforma de Evaluación del Taller");

define("REQUEST_NEW_PASSWORD", "Solicitar una nueva contraseña");
define("ENTER_EMAIL_PASSWORD_R", "Introduzca su dirección de correo electrónico. Usted recibirá un enlace para crear una nueva contraseña por correo electrónico.");
define("CANT_SIGN_IN", "No se puede acceder?");
define("SIGN_IN", "registrarse");
define("PASSWORD_RESETED","Usted contraseña ha sido reseted y enviado a su correo electrónico");
define("LAST_REG_USES","Últimos 10 usuarios registrados");
define("LAST_EXAM","Mejores 5 usuarios de último examen");
define("CALCULATOR","calculadora");

define("SENT_START_SENT", "Enviar empezar electrónico");
define("SENT_RESULTS_SENT", "Envía los resultados de correo");

//$PAGE_HEADER_TEXT= "Quizzes and Surveys";    // uncomment this variable to override the page name - only for gold template
//$PAGE_SUB_HEADER_TEXT="Web based quiz software allowing to create quizzes and surveys"; // uncomment this variable to override the page name - only for gold template
//$PAGE_FOOTER_TEXT="Phone number : 00994556605925"; // uncomment this variable to override the page footer - only for gold template


$SENT['yes']='Enviado';
$SENT['no']='no enviados';

// translating menu

$MODULES['Categories']='Categorías';
$MODULES['Quizzes']='Cuestionarios';
$MODULES['Questions bank']='banco de preguntas';
$MODULES['Add question']='Añade pregunta';
$MODULES['Local users']='Los usuarios locales';
$MODULES['Assignments']='Asignaciones';
$MODULES['New Assignment']='Nueva asignación de';
$MODULES['Active Assignments']='Asignaciones de activos';
$MODULES['My old assigments']='Mis viejos asignaciones';
$MODULES['New User']='Nuevo usuario';
$MODULES['Change password']='Cambiar contraseña';
$MODULES['New Quiz']='New Quiz';
$MODULES['Users']='Usuarios';
$MODULES['Imported users']='los usuarios importados';
$MODULES['Ratings']='Valoraciones';
$MODULES['Add rating']='Nueva Clasificación';
$MODULES['Settings']='Configuración';
$MODULES['Email templates']='plantillas de correo electrónico';
$MODULES['Content management']='gestión de contenidos';
$MODULES['SQL Queries']='consultas SQL';
$MODULES['Test mail']='electrónico de prueba';


$QUESTION_TYPE['1']="Una respuesta (botón de radio)";
$QUESTION_TYPE['0']="Respuesta múltiple (casilla de verificación)";
$QUESTION_TYPE['3']="Texto libre (área de texto)";
$QUESTION_TYPE['4']="Texto múltiple (sólo números)";
$QUESTION_TYPE['5']="Casilla de selección múltiple (Opcional)";
$QUESTION_TYPE['6']="Radio / Mixta Checkbox";

$USER_TYPE['1'] = 'administración';
$USER_TYPE['2'] = 'usuario';

$YES_NO['No']='No';
$YES_NO['Yes']='sí';
$YES_NO['Not enabled']='No habilitado';

$ASG_STATUS['0']='No ha comenzado';
$ASG_STATUS['1']='Introducción';
$ASG_STATUS['2']='acabado';
$ASG_STATUS['3']='Hora de finalización';
$ASG_STATUS['4']='detenido manualmente';


define("LOGO_FILE", "estilo de / i / logo.gif");
define("HEADER_IMAGE_FILE", "style/i/bg_3.gif");
define("LOGOUT_BUTTON_FILE", "estilo de / i / logout.gif");
define("LOGIN_PAGE_LOGO_FILE", "estilo de / i / login_logo.gif");

?>
