<?php

define("EDIT","Edit");
define("DELETE","Delete");
define("ADD","Add");
define("SAVE","Save");
define("CANCEL","Cancel");
define("SEND","Send");
define("CCONTINUE","Continue");
define("UP","Up");
define("DOWN","Down");
define("NEXT","Next");
define("PREVIOUS","Previous");
define("FINISH","Finish");
define("PAGES","Pages");
define("ENABLE","Enable");
define("DISABLE","Disable");
define("PRIORITY","Priority");
define("BACK","Back");
define("SEARCH","Search");
define("SHOW_ALL","Show All");
define("ADDED_DATE","Date added");
define("ARE_YOU_SURE","Are you sure?");
define("CLICK_OK_TO_COMPLETE_ASSESSMENT","Click OK to complete assessment");
define("NO_RECORDS","No records found");
define("ASSESSMENT_STOPPED","Assessment stopped: Student did not meet minimum requirement.");
define("PLEASE_WAIT","Please , wait ...");
define("SELF_CORRECTS_VOWEL","Self-corrects vowel(ending sound)");
define("WAIT","Wait ...");
define("MUST_ANSWER_ALL","Must answer all questions to proceed.");


define("CAT_DESC","Categories");
define("CAT_NAME","Category name");

define("Assessment_Reports","Assessment Reports");
define("VIEW_CLASSROOMS_GROWTH","View a Classrooms Growth");
define("CLASS_PERFORMANCE_SCORES_BY_STANDARD","Class Performance Scores by Standard");
define("CLASS_PERFORMANCE_GRAPHS_BY_STANDARD","Class Performance Graphs by Standard");
define("Individual_Student_Performance_Scores","Individual Student Performance Scores");
define("Individual_Student_Performance_by_Standard","Individual Student Performance by Standard");

define("Grade_Level_Report","Grade Level Report");
define("Performance_score_by_Standard","Performance score by Standard");
define("Performance_Graphs_by_Standard","Performance Graphs by Standard");

define("QUIZZES","Assessments");
define("QUIZ_NAME","Quiz name");
define("QUIZ_DESC","Description");
define("QUESTION","Question");
define("QUESTIONS","Questions");
define("NEW_QUIZ","Add New Assessment");
define("ADD_EDIT_QUIZ","Add/Edit Assessment");
define("CAT","Category");
define("NAME","Name");
define("DESC","Description");
define("SHOW_INTRO","Show intro");
define("INTRO_TEXT","Intro text");
define("QUIZ_NAME_VAL","Name cannot be empty");
define("QUIZ_DESC_VAL","Description cannot be empty");
define("QUIZ_CAT_VAL","Please, select category");
define("EXPORT","Export");
define("ASSESSMENT_EXPORT","Assessment Export");

define("TYPE","Type");
define("PAGE_NO","Page No");
define("QUESTION_ORDER","Question order");
define("POINT","Point");
define("TESTITEMCLUSTER","Test Item Cluster");
define("NEW_QUESTION","Add new question");
define("ADD_EDIT_QUESTION","Add/Edit question");
define("PAGE_NO","Page No");
define("QUESTION_ORDER","Question order");

define("HEADER_TEXT","Header text");
define("FOOTER_TEXT","Footer text");
define("SELECT_TEMP","Question type");
define("CAN_BE_EMPTY","Can be empty");
define("ANSWER_VARIANTS","Answer Variant");
define("CORRECT_ANSWER","Correct Answer");
define("ENTER_CORRECT_ANSWER","Enter correct answer");
define("CANNOT_DELETE_LAST","Cannot delete last choise");

define("ADD_EDIT_USER","Add/Edit user");
define("LOCAL_USERS","Local users");
define("CLASS_ROOMS","Class rooms");
define("LOGIN","Login");
define("USER_NAME","Name");
define("USER_SURNAME","Last Name");
define("USER_TYPE","User type");
define("USER_ID","User ID");
define("EMAIL","E-mail");
define("NEW_USER","Add new user");
define("PASSWORD","Password");
define("LOGIN_ALREADY_EXISTS","This login is already exists !");


define("LOGIN_VAL","Login cannot be empty");
define("PASSWORD_VAL","Password cannot be empty");
define("USERNAME_VAL","Name cannot be empty");
define("SURNAME_VAL","Last Name cannot be empty");
define("STUDENT_VAL","Student Number/Employee ID cannot be empty");
define("SELECT_USER_TYPE_VAL","Please select user type");
define("USER_TYPE_VAL","Please, select user type");
define("DISTRICT_TYPE_VAL","Please select district");


define("ASSIGNMENTS","Assessment Administration");
define("NEW_ASSIGNMENT","Add new assignment");
define("START","Start");
define("STOP","Stop");
define("INFORMATION","Information");
define("REPORTS","Reports");


define("ADD_ASSIGNMENT","Assessments/Quizzes/ Surveys
");
define("TEST","Test");
define("NOT_SELECTED","Not Selected");
define("SHOW_RESULTS","Show results to user");
define("RESULTS_BY","Results by");
define("SUCCESS_POINT_PERC","Success point/percent");
define("REVIEW_ANSWERS","Users can review correct answers");
define("TEST_TIME","Test time (in minutes)");
define("IMPORTED_USERS","Imported users");
define("O_QUIZ","Quizzes");
define("O_SURVEY","Survey");
define("O_ASSESSMENT","Assessments");
define("O_YES","Yes");
define("O_NO","No");
define("O_POINT","Point");
define("O_PERCENT","Percent");
define("DETAILS","Details");
define("VIEW_DETAILS","View details");
define("QUESTIONS_ORDER","Questions order");
define("ANSWERS_ORDER","Answers order");
define("BY_PRIORITY","By priority");
define("RANDOM","Random");
define("PRIORITY","Priority");



define("CATEGORY_VAL","Please, select category");
define("TEST_VAL","Please, select test");
define("SUCCESS_VAL","Please, enter success point");
define("TIME_VAL","Please, enter test time");
define("SUCCESS_NUM_VAL","Please, enter success point in numeric format");
define("TIME_NUM_VAL","Please, enter test time in numeric format");

define("VIEW_ASSIGNMENT","View assignment");
define("STATUS","Status");
define("SUCCESS","Success");
define("TOTAL_POINT","Total Point");
define("ASG_CANNOT_BE_FOUND","This assignment cannot be found");

define("ACTIVE_ASSIGNMENTS","Active Assessments");
define("Assessment_Administration","Assessment Administration");
define("ALREADY_FINISHED","You have already finished this quiz/survey");
define("COMPLETED","Completed");
define("ASSESSMENT_COMPLETED","Assessment completed.");
define("NO_ACTIVE_ASG","No active assginments found");


define("OLD_ASSIGNMENTS","Historical Assessments");
define("OLD_QUIZ_ASSIGNMENTS","Old Quiz Assignments");
define("OLD_SURVEY_ASSIGNMENTS","Old Survey Assignments");
define("ASSESSMENT_NAME","Assessment Name");
define("START_DATE","Start date");
define("FINISH_DATE","Finish date");
define("PASS_SCORE","Pass score");
define("PREVIOUS_ASSESSMENTS","Previous Assessments");
define("CURRENT_ASSESSMENTS","Current Assessments");

define("INTRO","Intro");
define("QUIZ_SURV",""); //Quiz / Survey
define("QUIZ_NO_ACCESS","You don't have access to this quiz/survey");
define("QUIZ_FINISHED","Finished. Thank you.");
define("YOUR_TOTAL_POINT","Your total point");
define("SUCCESS_POINT","Success point");
define("EXAM_SUCCESS","You passed exam successfully!");
define("EXAM_UNSUCCESS","Sorry, you didn't pass exam successfully");
define("ASSESSMENT_UNSUCCESS","Assessment ended - student did not meet required benchmark");

define("TIME_ENDED","Time ended");
define("TIME","Time");


//define("VIEW_DETAILS","View details");
define("NOT_ENABLED","Not enabled");


define("LOG_IN","Login"); // button
define("LOGIN_INCORRECT","Login or password is incorrect");
define("LANGUAGE","Language");


define("RATINGS","Ratings");
define("ADD_EDIT_RATING","Add / Edit rating");
define("USER_RATES_DETAILS","User rates details");
define("RAT_TYPE","Type");
define("DESCRIPTION","Description");
define("USER_RATES","User rates");
define("HTML_CODE","Html Code");
define("RAT_SELECT_TEMP","Select template");
define("IMG_COUNT","Image count");
define("USER_WAR","Unique user warranty");
define("BGCOLOR","Background color");
define("RAT_USER_RATES","User rates");
define("RAT_PRODUCT","Product");
define("AVG_RATE","Avarage rate");
define("MAX_RATE"," Max rate");
define("RAT_COUNT","Total rates count");
define("RAT_DETAILS","Details");
define("BACK_TO_RAT","Back to Ratings");
define("RATE","Rate");
define("IP_ADDRESS"," IP address");
define("RAT_USER_NAME","User name");
define("NEW_RAT","New Rating");
define("DELETED_RATE","This rating has already been deleted");

define("RATED","Rated");
define("OUT_OFF","out of");
define("INT_RATINGS","ratings");
define("ALREADY_RATED","You have already rated");


define("RAT_DESC_VAL","Description cannot be empty");
define("RAT_IMG_VAL","Image count cannot be empty");
define("RAT_IMG_NUMERIC_VAL","Image count should be numeric");

define("ALWAYS","Always");
define("NEVER","Never");

define("REST_BY_IP","Restrict by IP Address");
define("REST_BY_LOGIN","Restrict by asking login box");
define("REST_BY_COOKIE","Restrict by using cookie");

define("GENERATED_HTML","Generated Html Code");
define("PREVW","Preview");
define("PUT_THIS","Put this HTML Code to your page");
define("EDIT_THIS_RAT","Edit this rating ");


// translating registration form

define("R_REG_FORM","Registration form"); 
define("R_NAME","First Name"); 
define("R_SURNAME","Last Name"); 
define("R_DEFAULT_DISTRICT","Select District"); 
//define("R_DEFAULT_GRADE","Select Grade"); 
define("R_ROLLNUMBER","Student Number"); 
//define("R_DEFAULT_SCHOOL","Select School"); 
define("R_LOGIN","Login"); 
define("R_PASSWORD","Password"); 
define("R_EMAIL","Email"); 
define("R_ADDRESS","Address"); 
define("R_PHONE","Phone number"); 
define("R_REGISTER","Register"); 
define("R_CANCEL","Cancel"); 
define("R_APPROVED","Approved"); 
define("R_DISABLED","Disabled"); 

define("R_NAME_VAL","First Name cannot be empty"); 
define("R_SURNAME_VAL","Last Name cannot be empty"); 
define("R_LOGIN_VAL","Please, enter the login in a valid format ( alphanumeric characters only )"); 
define("R_PASSWORD_VAL","Please, enter the password in a valid format ( alphanumeric characters only )"); 
define("R_EMAIL_VAL","Please, enter email address in a valid format"); 

define("R_EMAIL_SENT", "An email has been sent to you . Please, check your mail and approve registration .");
define("R_APPROVE_REG", "Approving registration");
define("R_URL_WRONG", "Entered URL is not correct");
define("R_REG_APPROVED", "Registration has been approved !");
define("R_REG_ALREADY_APPROVED", "Your registration has already been approved !");
define("R_GO_TO", "Go to");
define("R_LOGIN_PAGE", "Login page");

define("CHNG_PASS","Change password");
define("OLD_PASS","Old password");
define("NEW_PASS","New password");

define("WRONG_PASS","Entered password is not correct");
define("PASS_CHANGED", "Your password has been changed");
define("ONLY_LOCAL", "Only local users can change password");

define("OLD_PASS_VAL","Please, enter the old password in a valid format ( alphanumeric characters only )");
define("NEW_PASS_VAL","Please, enter the new password in a valid format ( alphanumeric characters only )");

define("REG_FORGOT_PASS", "Forgot password ?");
define("REG_NEW_USER", "New user ?");

define("EMAIL_TEMPLATES","Email templates");
define("CANNOT_FIND_TEMP","Cannot find this template");
define("TEMP_SAVED", "Template has been saved");

define("E_SUBJECT", "Subject");
define("E_BODY", "Body");
define("E_VARS", "Available variables ( case sentivity )");

define("ASG_HOW_MANY", "Users can take Assessment/ Quiz");
define("ASG_ONCE", "Only once");
define("ASG_NO_LIMIT", "No limits");

define("ASG_AFFECT_CHANGE", "Freeze changes");
define("AFFECT", "Do not freeze");
define("DONT_AFFECT", "Freeze ");
define("ASG_SEND_RESULTS", "Send quiz results by mail");
define("ASG_SEND_AUTO", "Automaticaly on finish");
define("ASG_SEND_MAN", "Manually");

define("START_SENT", "Start mail");
define("RESULTS_SENT", "Results mail");

define("TAKE_AGAIN","Take again");
define("ENABLE_FOR_NEW","Auto assign to newly registered users");

define("CONTENT_MAN","Content management");
define("ADD_PAGE","Add new page");
define("MENU_NAME","Menu name");
define("MENU_NAME_VAL","Menu name cannot be empty");
define("PAGE_CONTENT","Page content");
define("MENU_PRIORITY_VAL","Please, enter priority in a numeric format");
define("MENU_PRIORITY_E_VAL","Please, enter priority");
define("EMAIL_ALREADY_EXISTS","This E-mail is already exists !");

define("SQL_QUERIES","SQL Queries");
define("ENABLE_DEBUGGING", "To see SQL Queries please, enable this feature from config.php file");

define("MAIL_ADDRESS", "Mail address");
define("CHECK_MAIL", "Please, check your email");
define("TEST_MAIL", "Test mail");

define("PENALTY_POINT","Penalty point");
define("ANSWER_DESCRIPTION","Answer description");
define("QUESTIONS_BANK","Question bank");
define("ASSIGNMENT_NAME","Assignment name");
define("CHILD_PAGES","Child pages");
define("NOTIFICATIONS", "Notifications");
define("SUBJECT", "Subject");
define("BODY", "Body");
define("SENDER", "Sender");
define("NOTIFICATION_LIST","Notifications list");
define("SendToAll", "This message will be sent to all assignment users");
define("YOUHAVE", "You have");
define("MY_PROFILE", "My Profile");
define("LOGOUT", "Log Out");
define("PHOTO", "Photo");
define("UP_FILE_SIZE_MSG1","Uploaded file size should not be much than");
define("UP_FILE_SIZE_MSG2","KB and should be in below listed formats");
define("UP_FILE_SIZE_MSG3","Image width should be");
define("USERS_COUNT","Users count");
define("QUESTIONS_COUNT","Questions count");
define("EXAMS_COUNT","Exams count");
define("SURVEYS_COUNT","Surveys count");
define("R_USERS","Users");
define("R_ADD_NEW_QUIZ","Add New Assessment
");
define("R_ADD_NEW_USER","Add new user");
define("R_ADD_NEW_PAGE","Add new page");
define("R_CATS","Categories");
define("R_QUIZZES","Assessments");
define("R_QUESTIONS_BANK","Question bank");
define("R_ASSIGNMENTS","Quizzes");
define("R_PAGES","Pages");
define("DATE_REGISTERED","Date registered");
define("USERS_COUNT_BY_COUNTRY","Users count by country");

define("COUNTRY","Country");
define("COUNTRY_VAL","Country cannot be empty");

define("SING_IN_SYSTEM", "Sign in to system");
define("ENTER_USERNAME", "All data protected under FERPA (20 U.S.C. § 1232g; 34 CFR Part 99)
");
define("REGISTER_IN_SYSTEM","Not registered? Sign up here");
define("SIGN_IN_SCREEN","Send me back to the sign-in screen");
define("EMAIL_TEXT","The e-mail address is not made public and will only be used if you wish to receive a new password.");
define("EMAIL_NOT_EXISTS","This email doesn't exist in our database");
define("CLOSE", "Close");
define("T_AND_C", "Terms and Conditions");

define("T_AND_C_TEXT", "Terms and Conditions text");
define("T_ACCEPT", "By filling in the form bellow and clicking the \"Sign Up\" button, you accept and agree to");
define("T_AND_S", "Terms of Service");
define("SIGN_UP_TO", "");
define("WRKSHOP_QUIZ_LOGIN", "Workshop’s Assessment Platform");

define("REQUEST_NEW_PASSWORD", "Request New Password");
define("ENTER_EMAIL_PASSWORD_R", "Please enter your email address. You will receive a link to create a new password via email.");
define("CANT_SIGN_IN", "Can't sign in?");
define("SIGN_IN", "Submit");
define("PASSWORD_RESETED","You password has been reseted and sent to your email");
define("LAST_REG_USES","Last registered 10 users");
define("LAST_EXAM","Best 5 users of last exam");
define("CALCULATOR","Calculator");

define("SENT_START_SENT", "Send start mail");
define("SENT_RESULTS_SENT", "Send results mail");

//$PAGE_HEADER_TEXT= "Quizzes and Surveys";    // uncomment this variable to override the page name - only for gold template
//$PAGE_SUB_HEADER_TEXT="Web based quiz software allowing to create quizzes and surveys"; // uncomment this variable to override the page name - only for gold template
//$PAGE_FOOTER_TEXT="Phone number : 00994556605925"; // uncomment this variable to override the page footer - only for gold template


$SENT['yes']='Sent';
$SENT['no']='Not sent';

// translating menu

$MODULES['Categories']='Categories';
$MODULES['Quizzes']='Quizzes';
$MODULES['Questions bank']='Questions bank';
$MODULES['Add question']='Add question';
$MODULES['Local users']='Local users';
$MODULES['Assignments']='Assignments';
$MODULES['New Assignment']='New Assignment';
$MODULES['Active Assignments']='Active Assignments';
$MODULES['My old assigments']='My old assignments';
$MODULES['New User']='New user';
$MODULES['Change password']='Change password';
$MODULES['New Quiz']='New Quiz';
$MODULES['Users']='Users';
$MODULES['Imported users']='Imported users';
$MODULES['Ratings']='Ratings';
$MODULES['Add rating']='New Rating';
$MODULES['Settings']='Settings';
$MODULES['Email templates']='Email templates';
$MODULES['Content management']='Content management';
$MODULES['SQL Queries']='SQL Queries';
$MODULES['Test mail']='Test mail';


$QUESTION_TYPE['1']="One answer (radio button)";
$QUESTION_TYPE['0']="Multi answer (checkbox)";
$QUESTION_TYPE['3']="Free text (textarea)";
$QUESTION_TYPE['4']="Multi text (numbers only)";
$QUESTION_TYPE['5']="Multi Checkbox(Optional)";
$QUESTION_TYPE['6']="Radio/ Checkbox Combination";

$USER_TYPE['1'] = 'Admin';
$USER_TYPE['2'] = 'User';

$YES_NO['No']='No';
$YES_NO['Yes']='Yes';
$YES_NO['Not enabled']='Not enabled';

$ASG_STATUS['0']='Not started';
$ASG_STATUS['1']='Started';
$ASG_STATUS['2']='Finished';
$ASG_STATUS['3']='Time ended';
$ASG_STATUS['4']='Manually stopped';


define("LOGO_FILE", "style/i/logo.gif");
define("HEADER_IMAGE_FILE", "style/i/bg_3.gif");
define("LOGOUT_BUTTON_FILE", "style/i/logout.gif");
define("LOGIN_PAGE_LOGO_FILE", "style/i/login_logo.gif");

?>
