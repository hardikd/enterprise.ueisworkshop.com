<?php
define("EDIT","Редактировать");
define("DELETE","Удалить");
define("ADD","Добавить");
define("SAVE","Сохранить");
define("CANCEL","Отмена");
define("SEND","Отправить");
define("CCONTINUE","Продрожить");
define("UP","Вверх");
define("DOWN","Вниз");
define("NEXT","Следующий");
define("PREVIOUS","Предыдущий");
define("FINISH","Завершить");
define("PAGES","Страницы");
define("ENABLE","Включить");
define("DISABLE","Отключить");
define("PRIORITY","Приоритет");
define("BACK","Назад");
define("SEARCH","Искать");
define("SHOW_ALL","Показать все");
define("ADDED_DATE","Дата");
define("ARE_YOU_SURE","Вы уверены?");
define("CLICK_OK_TO_COMPLETE_ASSESSMENT","Нажмите OK для завершения оценки");
define("NO_RECORDS","Нет данных");
define("ASSESSMENT_STOPPED","Оценка остановился: Студент не отвечают минимальным требованиям.");
define("ASSESSMENT_UNSUCCESS","Оценка закончилось - студент не соответствовали требуемым тест");
define("SELF_CORRECTS_VOWEL","Self-исправляет гласных (заканчивается звук)");
	
define("PLEASE_WAIT","Ждите ...");
define("WAIT","Wait ...");
define("MUST_ANSWER_ALL","Должны ответить на все вопросы, чтобы продолжить.");

define("CAT_DESC","Категории");
define("CAT_NAME","Название");

define("Assessment_Reports","Доклады об оценке");
define("VIEW_CLASSROOMS_GROWTH","Посмотреть Классные комнаты Роста");
define("CLASS_PERFORMANCE_SCORES_BY_STANDARD","Класс показателях деятельности по стандартной");
define("CLASS_PERFORMANCE_GRAPHS_BY_STANDARD","Производительность класса графов Стандартный");
define("Individual_Student_Performance_Scores","Индивидуальные результаты успеваемости учащихся");
define("Individual_Student_Performance_by_Standard","Индивидуальной работы студента Стандартный");

define("Grade_Level_Report","Класс доклад уровня");
define("Performance_score_by_Standard","Оценка производительности стандартным");
define("Performance_Graphs_by_Standard","Los gráficos de rendimiento por parte de Standard");

define("QUIZZES","Тесты");
define("QUIZ_NAME","Название");
define("QUIZ_DESC","Описание");
define("QUESTION","Вопрос");
define("QUESTIONS","Вопросы");
define("NEW_QUIZ","Новый тест");
define("ADD_EDIT_QUIZ","Добавить/Изменить тест");
define("CAT","Категория");
define("NAME","Название");
define("DESC","Описание");
define("SHOW_INTRO","Показать интро");
define("INTRO_TEXT","Текст интро");
define("QUIZ_NAME_VAL","Введите название");
define("QUIZ_DESC_VAL","Введите описание");
define("QUIZ_CAT_VAL","Выберите категорию");
define("EXPORT","экспорт");
define("ASSESSMENT_EXPORT","Оценка экспорта");

define("TYPE","Тип");
define("POINT","Балл");
define("NEW_QUESTION","Добавить новый вопрос");
define("ADD_EDIT_QUESTION","Добавить/Изменить вопрос");

define("HEADER_TEXT","Заголовок");
define("FOOTER_TEXT","Footer text");
define("SELECT_TEMP","Выберите шаблон");
define("CAN_BE_EMPTY","Не обязательно");
define("ANSWER_VARIANTS","Ответы");
define("CORRECT_ANSWER","Правильный ответ");
define("ENTER_CORRECT_ANSWER","Введите правильный ответ");
define("CANNOT_DELETE_LAST","Нелзья удалить последний");

define("ADD_EDIT_USER","Добавить/Изменить пользователя");
define("LOCAL_USERS","Локальные пользователи");
define("LOGIN","Логин");
define("USER_NAME","Имя");
define("USER_SURNAME","Фамилия");
define("USER_TYPE","Тип юзера");
define("USER_ID","ID");
define("EMAIL","E-mail");
define("NEW_USER","Добавить пользователя");
define("PASSWORD","Пароль");
define("LOGIN_ALREADY_EXISTS","Такой пользователь уже есть !");

define("LOGIN_VAL","Введите логин");
define("PASSWORD_VAL","Введите пароль");
define("USER_TYPE_VAL","Выберите тип пользователя");


define("ASSIGNMENTS","Назначения");
define("NEW_ASSIGNMENT","Новое назначение");
define("START","Начать");
define("STOP","Остановить");
define("INFORMATION","Информация");
define("REPORTS","Репорты");



define("ADD_ASSIGNMENT","Добавить назначение");
define("TEST","Тест");
define("NOT_SELECTED","Выберите");
define("SHOW_RESULTS","Показать результаты");
define("RESULTS_BY","Резултаты");
define("SUCCESS_POINT_PERC","Проходной балл/процент");
define("REVIEW_ANSWERS","Пользователям можно просмотреть правильные ответы");
define("TEST_TIME","Время теста (в минутах)");
define("IMPORTED_USERS","Импортированные пользователи");
define("O_QUIZ","Тест");
define("O_SURVEY","Опрос");
define("O_YES","Да");
define("O_NO","Нет");
define("O_POINT","Балл");
define("O_PERCENT","Процент");
define("DETAILS","Детали");
define("VIEW_DETAILS","Показать детали");
define("QUESTIONS_ORDER","Последовательность вопросов");
define("ANSWERS_ORDER","Последовательность ответов");
define("BY_PRIORITY","По приоритету");
define("RANDOM","Случайно");

define("CATEGORY_VAL","Выберите категорию");
define("TEST_VAL","Выберите тест");
define("SUCCESS_VAL","Введите проходной бал");
define("TIME_VAL","Введите время теста");
define("SUCCESS_NUM_VAL","Проходной балл - только цифры");
define("TIME_NUM_VAL","Время теста - только цифры");

define("VIEW_ASSIGNMENT","Показать назначение");
define("STATUS","Статус");
define("SUCCESS","Удачно");
define("TOTAL_POINT","Собранный балл/процент");
define("ASG_CANNOT_BE_FOUND","Невозможно найти назначение");
define("PRIORITY","приоритет");

define("ACTIVE_ASSIGNMENTS","Активная оценки");
define("ALREADY_FINISHED","Вы уже завершили этот тест/опрос");
define("COMPLETED","завершенный");
define("ASSESSMENT_COMPLETED","оценка завершена.");
define("NO_ACTIVE_ASG","Нет активных тестов");


define("OLD_ASSIGNMENTS","Исторические оценки");
define("OLD_QUIZ_ASSIGNMENTS","Старые тесты");
define("OLD_SURVEY_ASSIGNMENTS","Старые опросы");
define("ASSESSMENT_NAME","Оценка Имя");
define("START_DATE","Дата запуска");
define("FINISH_DATE","Дата завершения");
define("PASS_SCORE","Проходной балл");
define("PREVIOUS_ASSESSMENTS","Предыдущие оценки");
define("CURRENT_ASSESSMENTS","Текущая оценка");


define("INTRO","Интро");
define("QUIZ_SURV",""); //Тест / Опрос
define("QUIZ_NO_ACCESS","У вас нету доступа на этот тест/опрос");
define("QUIZ_FINISHED","Завершено. Спасибо.");
define("YOUR_TOTAL_POINT","Ваш балл");
define("SUCCESS_POINT","Проходной балл");
define("EXAM_SUCCESS","Вы сдали удачно!");
define("EXAM_UNSUCCESS","Извините , вы не сдали удачно .");
define("TIME_ENDED","Время истекло");
define("TIME","Время");

define("NOT_ENABLED","Запрешено");

define("LOG_IN","Войти"); // button
define("LOGIN_INCORRECT","Неправильная пара логин/пароль");
define("LANGUAGE","Язык");

define("RATINGS","Рейтинги");
define("ADD_EDIT_RATING","Добавить / Изменить рейтинги");
define("USER_RATES_DETAILS","Детали");
define("RAT_TYPE","Тип");
define("DESCRIPTION","Описание");
define("USER_RATES","Рейтинги");
define("HTML_CODE","Html код");
define("RAT_SELECT_TEMP","Выберите шаблон");
define("IMG_COUNT","Всего изображений");
define("USER_WAR","Ограничение");
define("BGCOLOR","Цвет фона");
define("RAT_USER_RATES","Рейтинги");
define("RAT_PRODUCT","Продукт");
define("AVG_RATE","Средний коэффициент");
define("MAX_RATE"," Максимальный коэффициент");
define("RAT_COUNT","Всего оценок");
define("RAT_DETAILS","Детали");
define("BACK_TO_RAT","Перейти к рейтингам");
define("RATE","Оценить");
define("IP_ADDRESS","IP address");
define("RAT_USER_NAME","Логин");
define("NEW_RAT","Добавить рейтинг");
define("DELETED_RATE","Этот рейтинг был удален");

define("RATED","Всего оценок");
define("OUT_OFF","из возможных");
define("INT_RATINGS","оценок");
define("ALREADY_RATED","Вы уже голосовали");


define("RAT_DESC_VAL","Введите описание");
define("RAT_IMG_VAL","Введите количество изображений");
define("RAT_IMG_NUMERIC_VAL","Введите цифру");


define("ALWAYS","Да");
define("NEVER","Нет");

define("REST_BY_IP","Ограничение по IP Address");
define("REST_BY_LOGIN","Ограничение по Логину");
define("REST_BY_COOKIE","Ограничение по Сookie");

define("GENERATED_HTML","HTML Код");
define("PREVW","Предварительный просмотр");
define("PUT_THIS","Вставьте этот код себе на страницу");
define("EDIT_THIS_RAT","Изменить этот рейтинг");

// translating registration form

define("R_REG_FORM","Форма для регистрации"); 
define("R_NAME","Имя"); 
define("R_SURNAME","Фамилия"); 
define("R_LOGIN","Логин"); 
define("R_PASSWORD","Пароль"); 
define("R_EMAIL","Электронная почта"); 
define("R_ADDRESS","Адрес"); 
define("R_PHONE","Телефонный номер"); 
define("R_REGISTER","Зарегистрироватся"); 
define("R_CANCEL","Отмена"); 
define("R_APPROVED","Потверждено"); 
define("R_DISABLED","Отключен"); 

define("R_NAME_VAL","Введите имя"); 
define("R_SURNAME_VAL","Введите фамилию"); 
define("R_LOGIN_VAL","Введите логин в правильном формате (только цифры и буквы)"); 
define("R_PASSWORD_VAL","Введите пароль в правильном формате (только цифры и буквы)"); 
define("R_EMAIL_VAL","Введите электронную почту в правильном формате"); 

define("R_EMAIL_SENT", "Проверьте почту и потвердите регистрацию .");
define("R_APPROVE_REG", "Потвердите регистрацию");
define("R_URL_WRONG", "Не правильный адрес");
define("R_REG_APPROVED", "Регистрация была потверждена");
define("R_REG_ALREADY_APPROVED", "Ваша регистрация уже была потверждена");
define("R_GO_TO", "Перейти");
define("R_LOGIN_PAGE", "в Логин страницу");

define("CHNG_PASS","Изменить пароль");
define("OLD_PASS","Старый пароль");
define("NEW_PASS","Новый пароль");

define("WRONG_PASS","Не правильный пароль");
define("PASS_CHANGED", "Пароль был успешно изменен");
define("ONLY_LOCAL", "Только локальные юзеры могут изменить пароль");

define("OLD_PASS_VAL","Введите старый пароль в правильном формате (только цифры и буквы)");
define("NEW_PASS_VAL","Введите новый пароль в правильном формате (только цифры и буквы)");

define("REG_FORGOT_PASS", "Забыли пароль ?");
define("REG_NEW_USER", "Новый юзер ?");

define("EMAIL_TEMPLATES","Шаблоны электронной почты");
define("CANNOT_FIND_TEMP","Невозможно найти шаблон");
define("TEMP_SAVED", "Шаблон был сохранен");

define("E_SUBJECT", "Тема");
define("E_BODY", "Сообщение");
define("E_VARS", "Переменные ( case sentivity )");

define("ASG_HOW_MANY", "Сколько раз можно пройти этот тест ?");
define("ASG_ONCE", "Только один раз");
define("ASG_NO_LIMIT", "Нет лимита");

define("ASG_AFFECT_CHANGE", "Если вопросы будут изменены с админ панели");
define("AFFECT", "Повлиять");
define("DONT_AFFECT", "Не влиять");
define("ASG_SEND_RESULTS", "Отправить результаты теста по электронной почты");
define("ASG_SEND_AUTO", "Автоматически");
define("ASG_SEND_MAN", "В ручную");

define("START_SENT", "Письмо о начале");
define("RESULTS_SENT", "Письмо с результатами");

define("TAKE_AGAIN","Еще раз");
define("ENABLE_FOR_NEW","Доступен для новых юзеров");

define("CONTENT_MAN","Страницы");
define("ADD_PAGE","Добавить страницу");
define("MENU_NAME","Меню");
define("MENU_NAME_VAL","Введите название для меню");
define("PAGE_CONTENT","Контент");
define("MENU_PRIORITY_VAL","Введите приоритет (только цифры");
define("MENU_PRIORITY_E_VAL","Введите приоритет");
define("EMAIL_ALREADY_EXISTS","Введите другую электронную почту !");

define("SQL_QUERIES","SQL запросы");
define("ENABLE_DEBUGGING", "Что бы увидеть SQL запросы надо включить настройку с файла config.php");

define("MAIL_ADDRESS", "Электронная почта");
define("CHECK_MAIL", "Проверьте почту");
define("TEST_MAIL", "Тест электронной почты");

define("PENALTY_POINT","Штраф");
define("ANSWER_DESCRIPTION","Описание");
define("QUESTIONS_BANK","Банк вопросов");
define("ASSIGNMENT_NAME","Название");
define("CHILD_PAGES","Страницы");
define("NOTIFICATIONS", "Сообщения");
define("SUBJECT", "Тема");
define("BODY", "Тело");
define("SENDER", "Отправитель");
define("NOTIFICATION_LIST","Список сообщений");
define("SendToAll", "Это сообщение будет отправлено всем");
define("YOUHAVE", "У вас");
define("MY_PROFILE", "Мой профиль");
define("LOGOUT", "Выйти");
define("PHOTO", "Фото");
define("UP_FILE_SIZE_MSG1","Фотография не должно быть больше чем");
define("UP_FILE_SIZE_MSG2","KB . Формат фотографий должен быть");
define("UP_FILE_SIZE_MSG3","Ширина фотографий должно быть");
define("USERS_COUNT","Количество юзеров");
define("QUESTIONS_COUNT","Количество вопросов");
define("EXAMS_COUNT","Список экзаменов");
define("SURVEYS_COUNT","Список опросов");
define("R_USERS","Юзеры");
define("R_ADD_NEW_QUIZ","Создать Экз.");
define("R_ADD_NEW_USER","Создать Юз.");
define("R_ADD_NEW_PAGE","Создать Стр.");
define("R_CATS","Категории");
define("R_QUIZZES","Экзамены");
define("R_QUESTIONS_BANK","Банк вопросов");
define("R_ASSIGNMENTS","Назначения");
define("R_PAGES","Страницы");
define("DATE_REGISTERED","Дата регистрации");
define("USERS_COUNT_BY_COUNTRY","Список юзеров по странам");

define("COUNTRY","Страна");
define("COUNTRY_VAL","Выберите страну");

define("SING_IN_SYSTEM", "Войти в систему");
define("ENTER_USERNAME", "Введите логин и пароль");
define("REGISTER_IN_SYSTEM","Новый юзер ?");
define("SIGN_IN_SCREEN","Перейти на страницу логина");
define("EMAIL_TEXT","Ваша почта будет изпользована для отправки паролья");
define("EMAIL_NOT_EXISTS","Этот емаил не найдет");
define("CLOSE", "Закрыть");
define("T_AND_C", "Условия");

define("T_AND_C_TEXT", "Условия");
define("T_ACCEPT", "By filling in the form bellow and clicking the \"Sign Up\" button, you accept and agree to.");
define("T_AND_S", "Условия сервиса");
define("SIGN_UP_TO", "Войти в ");

define("REQUEST_NEW_PASSWORD", "Запросить новый пароль");
define("ENTER_EMAIL_PASSWORD_R", "Введите свой емаил");
define("CANT_SIGN_IN", "Забыли пароль ?");
define("SIGN_IN", "Войти");
define("PASSWORD_RESETED","Новый пароль отправлен на вашу почту");
define("LAST_REG_USES","Последние зарегистрированные");
define("LAST_EXAM","Лучшие результаты");
define("CALCULATOR","Калькулятор");

define("SENT_START_SENT", "Send start mail");
define("SENT_RESULTS_SENT", "Send results mail");

//$PAGE_HEADER_TEXT= "Quizzes and Surveys";    // uncomment this variable to override the page name - only for gold template
//$PAGE_SUB_HEADER_TEXT="Web based quiz software allowing to create quizzes and surveys"; // uncomment this variable to override the page name - only for gold template
//$PAGE_FOOTER_TEXT="Phone number : 00994556605925"; // uncomment this variable to override the page footer - only for gold template


$SENT['yes']='Отправлено';
$SENT['no']='Не отправлено';

// translating menu

$MODULES['Categories']='Категории';
$MODULES['Quizzes']='Тесты';
$MODULES['Questions bank']='Банк вопросов';
$MODULES['Add question']='Добавить вопрос';
$MODULES['Local users']='Пользователи';
$MODULES['Assignments']='Назначения';
$MODULES['New Assignment']='Новое Назначение';
$MODULES['Active Assignments']='Активные тесты';
$MODULES['My old assigments']='Старые тесты';
$MODULES['New User']='Новый Пользователь';
$MODULES['New Quiz']='Новый Тест';
$MODULES['Change password']='Изменить пароль';
$MODULES['Users']='Пользователи';
$MODULES['Imported users']='Импортированные пользователи';
$MODULES['Ratings']='Рейтинги';
$MODULES['Add rating']='Добавить Рейтинг';
$MODULES['Settings']='Настройки';
$MODULES['Email templates']='Шаблоны писем';
$MODULES['Content management']='Страницы';
$MODULES['SQL Queries']='SQL запросы';
$MODULES['Test mail']='Тест почты';


$QUESTION_TYPE['1']="One answer (radio button)";
$QUESTION_TYPE['0']="Multi answer (checkbox)";
$QUESTION_TYPE['3']="Free text (textarea)";
$QUESTION_TYPE['4']="Multi text (numbers only)";
$QUESTION_TYPE['5']="Multi Checkbox(Optional)";
$QUESTION_TYPE['6']="Radio/ Checkbox Combination";

$USER_TYPE['1'] = 'Админ';
$USER_TYPE['2'] = 'Пользователь';

$YES_NO['No']='Нет';
$YES_NO['Yes']='Да';
$YES_NO['Not enabled']='Not enabled';

$ASG_STATUS['0']='Not started';
$ASG_STATUS['1']='Started';
$ASG_STATUS['2']='Finished';
$ASG_STATUS['3']='Time ended';
$ASG_STATUS['4']='Manually stopped';

define("LOGO_FILE", "style/i/logo.gif");
define("HEADER_IMAGE_FILE", "style/i/bg_3.gif");
define("LOGOUT_BUTTON_FILE", "style/i/logout.gif");
define("LOGIN_PAGE_LOGO_FILE", "style/i/login_logo.gif");

?>
