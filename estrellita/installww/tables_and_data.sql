SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE IF NOT EXISTS `answers_quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `answer_text` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `answer_image` varchar(450) CHARACTER SET utf8 DEFAULT NULL,
  `correct_answer` int(11) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `correct_answer_text` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `answer_pos` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL,
  `answer_text_eng` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `control_type` int(11) DEFAULT NULL,
  `answer_parent_id` int(11) DEFAULT NULL,
  `text_unit` char(10) CHARACTER SET utf8 NOT NULL,
  `answer_desc` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=176691 ;

--
-- Дамп данных таблицы `answers_quiz`
--

INSERT INTO `answers_quiz` (`id`, `group_id`, `answer_text`, `answer_image`, `correct_answer`, `priority`, `correct_answer_text`, `answer_pos`, `parent_id`, `answer_text_eng`, `control_type`, `answer_parent_id`, `text_unit`, `answer_desc`) VALUES
(176608, 524, 'Michael Schumacher', NULL, 1, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Michael Schumacher'),
(176609, 524, 'Bill Gates', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Bill Gates'),
(176610, 524, 'Robert Miles', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Robert Miles'),
(176611, 524, 'Bruce Lee', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Bruce Lee'),
(176612, 525, '2+2=4', NULL, 1, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176613, 525, '2*2=4', NULL, 1, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176614, 525, '2+2=3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176615, 525, '2*2=3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176616, 525, '2+2=7', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176617, 525, '2/2=7', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176618, 526, '1+2=?', NULL, 0, 0, '3', 1, 0, NULL, 1, 0, '', ''),
(176619, 526, '1+3=?', NULL, 0, 0, '4', 1, 0, NULL, 1, 0, '', ''),
(176620, 526, '1+4=?', NULL, 0, 0, '5', 1, 0, NULL, 1, 0, '', ''),
(176621, 526, '1+5=?', NULL, 0, 0, '6', 1, 0, NULL, 1, 0, '', ''),
(176622, 527, '', NULL, 0, 0, 'Microsoft', 1, 0, NULL, 1, 0, '', ''),
(176623, 528, 'Answer1', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176624, 528, 'Answer2', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176625, 528, 'Answer3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176626, 528, 'Answer4', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176627, 529, 'Answer1', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176628, 529, 'Answer2', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176629, 529, 'Answer3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176630, 529, 'Answer4', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176646, 534, 'Michael Schumacher', NULL, 1, 0, '', 1, 176608, NULL, 1, 0, '', ''),
(176647, 534, 'Bill Gates', NULL, 0, 0, '', 1, 176609, NULL, 1, 0, '', ''),
(176648, 534, 'Robert Miles', NULL, 0, 0, '', 1, 176610, NULL, 1, 0, '', ''),
(176649, 534, 'Bruce Lee', NULL, 0, 0, '', 1, 176611, NULL, 1, 0, '', ''),
(176650, 535, '2+2=4', NULL, 1, 0, '', 1, 176612, NULL, 1, 0, '', ''),
(176651, 535, '2*2=4', NULL, 1, 0, '', 1, 176613, NULL, 1, 0, '', ''),
(176652, 535, '2+2=3', NULL, 0, 0, '', 1, 176614, NULL, 1, 0, '', ''),
(176653, 535, '2*2=3', NULL, 0, 0, '', 1, 176615, NULL, 1, 0, '', ''),
(176654, 535, '2+2=7', NULL, 0, 0, '', 1, 176616, NULL, 1, 0, '', ''),
(176655, 535, '2/2=7', NULL, 0, 0, '', 1, 176617, NULL, 1, 0, '', ''),
(176656, 536, '1+2=?', NULL, 0, 0, '3', 1, 176618, NULL, 1, 0, '', ''),
(176657, 536, '1+3=?', NULL, 0, 0, '4', 1, 176619, NULL, 1, 0, '', ''),
(176658, 536, '1+4=?', NULL, 0, 0, '5', 1, 176620, NULL, 1, 0, '', ''),
(176659, 536, '1+5=?', NULL, 0, 0, '6', 1, 176621, NULL, 1, 0, '', ''),
(176660, 537, '', NULL, 0, 0, 'Microsoft', 1, 176622, NULL, 1, 0, '', ''),
(176661, 538, 'Michael Schumacher', NULL, 1, 0, '', 1, 176608, NULL, 1, 0, '', ''),
(176662, 538, 'Bill Gates', NULL, 0, 0, '', 1, 176609, NULL, 1, 0, '', ''),
(176663, 538, 'Robert Miles', NULL, 0, 0, '', 1, 176610, NULL, 1, 0, '', ''),
(176664, 538, 'Bruce Lee', NULL, 0, 0, '', 1, 176611, NULL, 1, 0, '', ''),
(176665, 539, '2+2=4', NULL, 1, 0, '', 1, 176612, NULL, 1, 0, '', ''),
(176666, 539, '2*2=4', NULL, 1, 0, '', 1, 176613, NULL, 1, 0, '', ''),
(176667, 539, '2+2=3', NULL, 0, 0, '', 1, 176614, NULL, 1, 0, '', ''),
(176668, 539, '2*2=3', NULL, 0, 0, '', 1, 176615, NULL, 1, 0, '', ''),
(176669, 539, '2+2=7', NULL, 0, 0, '', 1, 176616, NULL, 1, 0, '', ''),
(176670, 539, '2/2=7', NULL, 0, 0, '', 1, 176617, NULL, 1, 0, '', ''),
(176671, 540, '1+2=?', NULL, 0, 0, '3', 1, 176618, NULL, 1, 0, '', ''),
(176672, 540, '1+3=?', NULL, 0, 0, '4', 1, 176619, NULL, 1, 0, '', ''),
(176673, 540, '1+4=?', NULL, 0, 0, '5', 1, 176620, NULL, 1, 0, '', ''),
(176674, 540, '1+5=?', NULL, 0, 0, '6', 1, 176621, NULL, 1, 0, '', ''),
(176675, 541, '', NULL, 0, 0, 'Microsoft', 1, 176622, NULL, 1, 0, '', ''),
(176676, 542, 'Michael Schumacher', NULL, 1, 0, '', 1, 176608, NULL, 1, 0, '', ''),
(176677, 542, 'Bill Gates', NULL, 0, 0, '', 1, 176609, NULL, 1, 0, '', ''),
(176678, 542, 'Robert Miles', NULL, 0, 0, '', 1, 176610, NULL, 1, 0, '', ''),
(176679, 542, 'Bruce Lee', NULL, 0, 0, '', 1, 176611, NULL, 1, 0, '', ''),
(176680, 543, '2+2=4', NULL, 1, 0, '', 1, 176612, NULL, 1, 0, '', ''),
(176681, 543, '2*2=4', NULL, 1, 0, '', 1, 176613, NULL, 1, 0, '', ''),
(176682, 543, '2+2=3', NULL, 0, 0, '', 1, 176614, NULL, 1, 0, '', ''),
(176683, 543, '2*2=3', NULL, 0, 0, '', 1, 176615, NULL, 1, 0, '', ''),
(176684, 543, '2+2=7', NULL, 0, 0, '', 1, 176616, NULL, 1, 0, '', ''),
(176685, 543, '2/2=7', NULL, 0, 0, '', 1, 176617, NULL, 1, 0, '', ''),
(176686, 544, '1+2=?', NULL, 0, 0, '3', 1, 176618, NULL, 1, 0, '', ''),
(176687, 544, '1+3=?', NULL, 0, 0, '4', 1, 176619, NULL, 1, 0, '', ''),
(176688, 544, '1+4=?', NULL, 0, 0, '5', 1, 176620, NULL, 1, 0, '', ''),
(176689, 544, '1+5=?', NULL, 0, 0, '6', 1, 176621, NULL, 1, 0, '', ''),
(176690, 545, '', NULL, 0, 0, 'Microsoft', 1, 176622, NULL, 1, 0, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL DEFAULT '0',
  `org_quiz_id` int(11) NOT NULL DEFAULT '0',
  `results_mode` int(11) NOT NULL DEFAULT '0',
  `added_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quiz_time` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0',
  `pass_score` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quiz_type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `allow_review` int(11) NOT NULL DEFAULT '2',
  `qst_order` int(11) NOT NULL DEFAULT '1',
  `answer_order` int(11) NOT NULL DEFAULT '1',
  `affect_changes` int(11) NOT NULL,
  `limited` int(11) NOT NULL,
  `send_results` int(11) NOT NULL,
  `accept_new_users` int(11) NOT NULL,
  `assignment_name` varchar(600) DEFAULT NULL,
  `asg_quiz_type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=44 ;

--
-- Дамп данных таблицы `assignments`
--

INSERT INTO `assignments` (`id`, `quiz_id`, `org_quiz_id`, `results_mode`, `added_date`, `quiz_time`, `show_results`, `pass_score`, `quiz_type`, `status`, `allow_review`, `qst_order`, `answer_order`, `affect_changes`, `limited`, `send_results`, `accept_new_users`, `assignment_name`, `asg_quiz_type`) VALUES
(43, 134, 130, 1, '2013-02-26 23:57:30', 25, 1, '5.00', 1, 2, 1, 1, 1, 2, 1, 2, 2, 'Simple Quiz', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `assignment_users`
--

CREATE TABLE IF NOT EXISTS `assignment_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assignment_id` (`assignment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=345 ;

--
-- Дамп данных таблицы `assignment_users`
--

INSERT INTO `assignment_users` (`id`, `assignment_id`, `user_type`, `user_id`) VALUES
(340, 43, 1, 50057),
(341, 43, 1, 50056),
(342, 43, 1, 50055),
(343, 43, 1, 50054),
(344, 43, 1, 50053);

-- --------------------------------------------------------

--
-- Структура таблицы `cats`
--

CREATE TABLE IF NOT EXISTS `cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Дамп данных таблицы `cats`
--

INSERT INTO `cats` (`id`, `cat_name`) VALUES
(61, 'Simple category');

-- --------------------------------------------------------

--
-- Структура таблицы `countries_quiz`
--

CREATE TABLE IF NOT EXISTS `countries_quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252 ;

--
-- Дамп данных таблицы `countries_quiz`
--

INSERT INTO `countries_quiz` (`id`, `country_name`) VALUES
(2, 'Afghanistan'),
(3, 'Egypt'),
(5, 'Albania'),
(6, 'Algeria'),
(7, 'American Samoa'),
(8, 'Virgin Islands, U.s.'),
(9, 'Andorra'),
(10, 'Angola'),
(11, 'Anguilla'),
(12, 'Antarctica'),
(13, 'Antigua And Barbuda'),
(14, 'Equatorial Guinea'),
(15, 'Argentina'),
(16, 'Armenia'),
(17, 'Aruba'),
(18, 'Ascension'),
(19, 'Azerbaijan'),
(20, 'Ethiopia'),
(21, 'Australia'),
(22, 'Bahamas'),
(23, 'Bahrain'),
(24, 'Bangladesh'),
(25, 'Barbados'),
(26, 'Belgium'),
(27, 'Belize'),
(28, 'Benin'),
(29, 'Bermuda'),
(30, 'Bhutan'),
(31, 'Bolivia'),
(32, 'Bosnia And Herzegovina'),
(33, 'Botswana'),
(34, 'Bouvet Island'),
(35, 'Brazil'),
(36, 'Virgin Islands, British'),
(37, 'British Indian Ocean Territory'),
(38, 'Brunei Darussalam'),
(39, 'Bulgaria'),
(40, 'Burkina Faso'),
(41, 'Burundi'),
(42, 'Chile'),
(43, 'China'),
(44, 'Cook Islands'),
(45, 'Costa Rica'),
(46, 'C?te D''ivoire'),
(47, 'Denmark'),
(48, 'Germany'),
(49, 'Saint Helena'),
(50, 'Diego Garcia'),
(51, 'Dominica'),
(52, 'Dominican Republic'),
(53, 'Djibouti'),
(54, 'Ecuador'),
(55, 'El Salvador'),
(56, 'Eritrea'),
(57, 'Estonia'),
(58, 'Europ?ische Union'),
(59, 'Falkland Islands (malvinas)'),
(60, 'Faroe Islands'),
(61, 'Fiji'),
(62, 'Finland'),
(63, 'France'),
(64, 'French Guiana'),
(65, 'French Polynesia'),
(66, 'French Southern Territories'),
(67, 'Gabon'),
(68, 'Gambia'),
(69, 'Georgia'),
(70, 'Ghana'),
(71, 'Gibraltar'),
(72, 'Grenada'),
(73, 'Greece'),
(74, 'Greenland'),
(75, 'European Union'),
(76, 'Guam'),
(77, 'Guatemala'),
(78, 'Guernsey'),
(79, 'Guinea'),
(80, 'Guinea-bissau'),
(81, 'Guyana'),
(82, 'Haiti'),
(83, 'Heard Island And Mcdonald Islands'),
(84, 'Honduras'),
(85, 'Hong Kong'),
(86, 'India'),
(87, 'Indonesia'),
(88, 'Isle Of Man'),
(89, 'Iraq'),
(90, 'Iran, Islamic Republic Of'),
(91, 'Ireland'),
(92, 'Iceland'),
(93, 'Israel'),
(94, 'Italy'),
(95, 'Jamaica'),
(96, 'Japan'),
(97, 'Yemen'),
(98, 'Jersey'),
(99, 'Jordan'),
(100, 'Cayman Islands'),
(101, 'Cambodia'),
(102, 'Cameroon'),
(103, 'Canada'),
(104, 'Kanarische Inseln'),
(105, 'Cape Verde'),
(106, 'Kazakhstan'),
(107, 'Qatar'),
(108, 'Kenya'),
(109, 'Kyrgyzstan'),
(110, 'Kiribati'),
(111, 'Cocos (keeling) Islands'),
(112, 'Colombia'),
(113, 'Comoros'),
(114, 'Congo, The Democratic Republic Of The'),
(115, 'Congo'),
(116, 'Korea, Democratic People''s Republic Of'),
(117, 'Korea, Republic Of'),
(118, 'Croatia'),
(119, 'Cuba'),
(120, 'Kuwait'),
(121, 'Lao People''s Democratic Republic'),
(122, 'Lesotho'),
(123, 'Latvia'),
(124, 'Lebanon'),
(125, 'Liberia'),
(126, 'Libyan Arab Jamahiriya'),
(127, 'Liechtenstein'),
(128, 'Lithuania'),
(129, 'Luxembourg'),
(130, 'Macao'),
(131, 'Madagascar'),
(132, 'Malawi'),
(133, 'Malaysia'),
(134, 'Maldives'),
(135, 'Mali'),
(136, 'Malta'),
(137, 'Morocco'),
(138, 'Marshall Islands'),
(139, 'Martinique'),
(140, 'Mauritania'),
(141, 'Mauritius'),
(142, 'Mayotte'),
(143, 'Macedonia, The Former Yugoslav Republic Of'),
(144, 'Mexico'),
(145, 'Micronesia, Federated States Of'),
(146, 'Moldova'),
(147, 'Monaco'),
(148, 'Mongolia'),
(149, 'Montserrat'),
(150, 'Mozambique'),
(151, 'Myanmar'),
(152, 'Namibia'),
(153, 'Nauru'),
(154, 'Nepal'),
(155, 'New Caledonia'),
(156, 'New Zealand'),
(157, 'Neutrale Zone'),
(158, 'Nicaragua'),
(159, 'Netherlands'),
(160, 'Netherlands Antilles'),
(161, 'Niger'),
(162, 'Nigeria'),
(163, 'Niue'),
(164, 'Northern Mariana Islands'),
(165, 'Norfolk Island'),
(166, 'Norway'),
(167, 'Oman'),
(168, 'Austria'),
(169, 'Pakistan'),
(170, 'Palestinian Territory, Occupied'),
(171, 'Palau'),
(172, 'Panama'),
(173, 'Papua New Guinea'),
(174, 'Paraguay'),
(175, 'Peru'),
(176, 'Philippines'),
(177, 'Pitcairn'),
(178, 'Poland'),
(179, 'Portugal'),
(180, 'Puerto Rico'),
(181, 'R?union'),
(182, 'Rwanda'),
(183, 'Romania'),
(184, 'Russian Federation'),
(185, 'Solomon Islands'),
(186, 'Zambia'),
(187, 'Samoa'),
(188, 'San Marino'),
(189, 'Sao Tome And Principe'),
(190, 'Saudi Arabia'),
(191, 'Sweden'),
(192, 'Switzerland'),
(193, 'Senegal'),
(194, 'Serbien und Montenegro'),
(195, 'Seychelles'),
(196, 'Sierra Leone'),
(197, 'Zimbabwe'),
(198, 'Singapore'),
(199, 'Slovakia'),
(200, 'Slovenia'),
(201, 'Somalia'),
(202, 'Spain'),
(203, 'Sri Lanka'),
(204, 'Saint Kitts And Nevis'),
(205, 'Saint Lucia'),
(206, 'Saint Pierre And Miquelon'),
(207, 'Saint Vincent And The Grenadines'),
(208, 'South Africa'),
(209, 'Sudan'),
(210, 'South Georgia And The South Sandwich Islands'),
(211, 'Suriname'),
(212, 'Svalbard And Jan Mayen'),
(213, 'Swaziland'),
(214, 'Syrian Arab Republic'),
(215, 'Tajikistan'),
(216, 'Taiwan'),
(217, 'Tanzania, United Republic Of'),
(218, 'Thailand'),
(219, 'Timor-leste'),
(220, 'Togo'),
(221, 'Tokelau'),
(222, 'Tonga'),
(223, 'Trinidad And Tobago'),
(224, 'Tristan da Cunha'),
(225, 'Chad'),
(226, 'Czech Republic'),
(227, 'Tunisia'),
(228, 'Turkey'),
(229, 'Turkmenistan'),
(230, 'Turks And Caicos Islands'),
(231, 'Tuvalu'),
(232, 'Uganda'),
(233, 'Ukraine'),
(234, 'Union der Sozialistischen Sowjetrepubliken'),
(235, 'Uruguay'),
(236, 'Uzbekistan'),
(237, 'Vanuatu'),
(238, 'Holy See (vatican City State)'),
(239, 'Venezuela'),
(240, 'United Arab Emirates'),
(241, 'United States'),
(242, 'United Kingdom'),
(243, 'Viet Nam'),
(244, 'Wallis And Futuna'),
(245, 'Christmas Island'),
(246, 'Belarus'),
(247, 'Western Sahara'),
(248, 'Central African Republic'),
(249, 'Cyprus'),
(250, 'Hungary'),
(251, 'Montenegro');

-- --------------------------------------------------------

--
-- Структура таблицы `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `vars` varchar(300) DEFAULT NULL,
  `body` text,
  `subject` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `email_templates`
--

INSERT INTO `email_templates` (`id`, `name`, `vars`, `body`, `subject`) VALUES
(1, 'register_user', '[UserName],[Name],[Surname],[email], [address], [phone], [url]', 'Dear [Name] [Surname] , \n\nYou have registered in our system . Here is the details of registration\n\nName : [Name] \nSurname  : [Surname]\nLogin : [UserName]\nAddress : [address]\nPhone : [phone]\nEmail : [email]\n\nTo approve your registration in our Quiz system , please open below link \n\n[url]\n\nThanks', 'Registration in Quiz System'),
(2, 'forgot_password', '[UserName],[Name],[Surname],[email],[address],[phone],[url],[random_password]', 'Dear [Name] [Surname] ,\n\nYou password has been reseted , please find your password below .\n\nLogin : [UserName]\nPassword : [random_password]\nEmail : [email]\n\nThanks', 'Restoring the password'),
(3, 'quiz_start_message', '[UserName],[Name],[Surname],[email],[address], [phone],[url],[quiz_name]', 'Dear [Name] [Surname] , \n\nThe below listed exam has been started . \n\nQuiz name : [quiz_name]\nName : [Name] \nSurname  : [Surname]\nLogin : [UserName]\n\nTo join to exam please , use below link\n\n[url]\n\nThanks', 'Online exam started'),
(4, 'quiz_results_success', '[UserName],[Name],[Surname],[email],[url],[quiz_name],[start_date],[finish_date],[pass_score],[user_score]', 'Dear [Name] [Surname] ,\n\nYou passed exam successfully . \n\nQuiz name : [quiz_name]\nStart date : [start_date]\nFinish date : [finish_date]\nPass score : [pass_score]\nYour score : [user_score]\n\nThanks,\nAdministrator', 'You passed exam succesfully'),
(5, 'quiz_results_not_success', '[UserName],[Name],[Surname],[email],[url],[quiz_name],[start_date],[finish_date],[pass_score],[user_score]', 'Dear [Name] [Surname] ,\n\nSorry , you didn''t pass exam succesfully . \n\nQuiz name : [quiz_name]\nStart date : [start_date]\nFinish date : [finish_date]\nPass score : [pass_score]\nYour score : [user_score]\n\nThanks,\nAdministrator', 'Sorry , you didn''t pass exam succesfully');

-- --------------------------------------------------------

--
-- Структура таблицы `imported_users`
--

CREATE TABLE IF NOT EXISTS `imported_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `surname` varchar(255) NOT NULL DEFAULT '',
  `user_name` varchar(150) NOT NULL DEFAULT '',
  `password` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `imported_users`
--

INSERT INTO `imported_users` (`id`, `name`, `surname`, `user_name`, `password`, `email`) VALUES
(1, 'test1', 'test2', 'user11', '21232f297a57a5a743894a0e4a801fc3', 'elshan999@mail.ru'),
(2, 'test2', 'test2', 'user2', 'ee11cbb19052e40b07aac0ca060c23ee', 'elshan999@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `mailed_users`
--

CREATE TABLE IF NOT EXISTS `mailed_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `mail_type` int(11) NOT NULL,
  `user_quiz_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mailed_users_ibfk_1` (`assignment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=182 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `priority` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Дамп данных таблицы `modules`
--

INSERT INTO `modules` (`id`, `module_name`, `file_name`, `parent_id`, `priority`) VALUES
(1, 'Quizzes', NULL, 0, '1'),
(2, 'Categories', 'cats', 1, '1'),
(3, 'Quizzes', 'quizzes', 1, '2'),
(4, 'Local users', 'local_users', 13, '4'),
(5, 'Assignments', NULL, 0, '2'),
(6, 'Assignments', 'assignments', 5, '6'),
(7, 'New Assignment', 'add_assignment', 5, '7'),
(8, 'Assignments', NULL, 0, '3'),
(9, 'Active Assignments', 'active_assignments', 8, '1'),
(10, 'My old assigments', 'old_assignments', 8, '2'),
(11, 'New User', 'add_edit_user', 13, '7'),
(12, 'New Quiz', 'add_edit_quiz', 1, '3'),
(13, 'Users', '', 0, '4'),
(17, 'Ratings', '', 0, '5'),
(18, 'Ratings', 'ratings', 17, '1'),
(19, 'Add rating', 'add_edit_rating', 17, '2'),
(20, 'Change password', 'change_password', 13, '10'),
(21, 'Settings', NULL, 0, '6'),
(22, 'Email templates', 'email_templates', 21, '0'),
(23, 'Content management', 'cms', 21, '1'),
(24, 'SQL Queries', 'sql_queries', 21, '2'),
(25, 'Test mail', 'test_mail', 21, '3'),
(26, 'Imported users', 'imported_users', 13, '5'),
(27, 'Questions bank', 'questions_bank', 1, '4'),
(28, 'Add question', 'add_question&quiz_id=-1&qstbank=1', 1, '4');

-- --------------------------------------------------------

--
-- Структура таблицы `nots`
--

CREATE TABLE IF NOT EXISTS `nots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asg_id` int(11) NOT NULL,
  `sent_by` int(11) NOT NULL,
  `body` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `nots`
--

INSERT INTO `nots` (`id`, `asg_id`, `sent_by`, `body`, `added_date`, `subject`) VALUES
(2, 30, 50001, 'message', '2013-02-08 22:31:16', 'subject'),
(4, 30, 50001, 'te', '2013-02-08 22:36:04', 'da'),
(5, 30, 50001, 'da', '2013-02-08 23:17:38', 'da'),
(6, 30, 50001, 'da', '2013-02-08 23:17:39', 'da'),
(7, 30, 50001, 'da', '2013-02-08 23:17:41', 'da'),
(8, 30, 50001, 'da', '2013-02-08 23:17:42', 'da'),
(9, 30, 50001, 'da', '2013-02-08 23:17:43', 'da'),
(10, 30, 50001, 'da', '2013-02-08 23:17:44', 'da'),
(11, 30, 50001, 'da', '2013-02-08 23:17:45', 'da'),
(12, 30, 50001, 'salam\nnecesen', '2013-02-08 23:31:08', 'salam'),
(13, 30, 50001, 'web', '2013-02-08 23:45:33', 'web'),
(14, 30, 50001, 'web', '2013-02-08 23:46:11', 'web'),
(15, 30, 50001, 'web', '2013-02-08 23:47:54', 'web'),
(16, 30, 50001, 'web', '2013-02-08 23:48:37', 'web'),
(17, 30, 50001, 'ss', '2013-02-10 01:45:19', 'ss'),
(18, 30, 50001, 'salam aleykum\nyaxshi ushaglarsiz', '2013-02-16 01:43:24', 'salam'),
(19, 34, 50001, 'da', '2013-02-24 00:06:34', 'da'),
(20, 34, 50001, 'web', '2013-02-24 00:25:54', 'web'),
(21, 34, 50001, 'web', '2013-02-24 00:26:04', 'web'),
(22, 35, 50001, 'test', '2013-02-24 03:47:13', 'test'),
(23, 39, 50001, 'asbdk bask dbaks dbaskjdbk a', '2013-02-26 16:42:46', 'salam'),
(24, 39, 50001, 'vtoraya', '2013-02-26 16:43:31', 'salam');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(800) NOT NULL,
  `page_content` text NOT NULL,
  `priority` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `page_name`, `page_content`, `priority`, `parent_id`) VALUES
(1, 'Home', '', 0, 0),
(2, 'About', '', 0, 0),
(4, 'Links', '', 0, 0),
(8, 'Products', '', 0, 0),
(9, 'Documentation', '', 0, 0),
(13, 'Information', '', 99, 0),
(22, 'Page3', '', 3, 1),
(21, 'Page2', '', 1, 1),
(20, 'Page1', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_text` varchar(3800) CHARACTER SET utf8 DEFAULT NULL,
  `question_type_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `point` decimal(18,0) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_id` int(11) NOT NULL,
  `question_total` decimal(18,0) DEFAULT NULL,
  `check_total` int(11) DEFAULT NULL,
  `header_text` varchar(1500) CHARACTER SET utf8 DEFAULT NULL,
  `footer_text` varchar(1500) CHARACTER SET utf8 DEFAULT NULL,
  `question_text_eng` varchar(1800) CHARACTER SET utf8 DEFAULT NULL,
  `help_image` varchar(550) CHARACTER SET utf8 DEFAULT NULL,
  `penalty_point` decimal(18,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `quiz_id` (`quiz_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=495 ;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `question_text`, `question_type_id`, `priority`, `quiz_id`, `point`, `added_date`, `parent_id`, `question_total`, `check_total`, `header_text`, `footer_text`, `question_text_eng`, `help_image`, `penalty_point`) VALUES
(473, '<p>\r\n	Who is in photo ?</p>\r\n<p>\r\n	<img alt="" src="/elsphpwebquiz_platinum/ckeditor/kcfinder/upload_img/images/michael_schumacher1.jpg" style="width: 300px; height: 300px;" /></p>', 1, 1, 130, '5', '2013-02-26 22:04:12', 0, NULL, NULL, '', '', NULL, NULL, '0'),
(474, '<p>\r\n	Which is correct ?</p>', 0, 2, 130, '5', '2013-02-26 22:08:34', 0, NULL, NULL, 'Simple header', 'Simple footer', NULL, NULL, '2'),
(475, '<p>\r\n	Please, answer below listed questions .</p>', 4, 3, 130, '4', '2013-02-26 22:11:27', 0, NULL, NULL, '', '', NULL, NULL, '0'),
(476, '<p>\r\n	Enter the name of the biggest software company in the world</p>', 3, 4, 130, '3', '2013-02-26 22:12:14', 0, NULL, NULL, '', '', NULL, NULL, '0'),
(477, '<p>\r\n	Question 1</p>', 1, 1, -1, '1', '2013-02-26 22:26:21', 0, NULL, NULL, '', '', NULL, NULL, '1'),
(478, '<p>\r\n	Question 2</p>', 1, 2, -1, '1', '2013-02-26 22:27:01', 0, NULL, NULL, '', '', NULL, NULL, '1'),
(483, '<p>\r\n	Who is in photo ?</p>\r\n<p>\r\n	<img alt="" src="/elsphpwebquiz_platinum/ckeditor/kcfinder/upload_img/images/michael_schumacher1.jpg" style="width: 300px; height: 300px;" /></p>', 1, 1, 132, '5', '2013-02-26 19:23:39', 473, NULL, NULL, '', '', NULL, '', '0'),
(484, '<p>\r\n	Which is correct ?</p>', 0, 2, 132, '5', '2013-02-26 19:23:39', 474, NULL, NULL, 'Simple header', 'Simple footer', NULL, '', '0'),
(485, '<p>\r\n	Please, answer below listed questions .</p>', 4, 3, 132, '4', '2013-02-26 19:23:39', 475, NULL, NULL, '', '', NULL, '', '0'),
(486, '<p>\r\n	Enter the name of the biggest software company in the world</p>', 3, 4, 132, '3', '2013-02-26 19:23:39', 476, NULL, NULL, '', '', NULL, '', '0'),
(487, '<p>\r\n	Who is in photo ?</p>\r\n<p>\r\n	<img alt="" src="/elsphpwebquiz_platinum/ckeditor/kcfinder/upload_img/images/michael_schumacher1.jpg" style="width: 300px; height: 300px;" /></p>', 1, 1, 133, '5', '2013-02-26 19:34:44', 473, NULL, NULL, '', '', NULL, '', '0'),
(488, '<p>\r\n	Which is correct ?</p>', 0, 2, 133, '5', '2013-02-26 19:34:44', 474, NULL, NULL, 'Simple header', 'Simple footer', NULL, '', '0'),
(489, '<p>\r\n	Please, answer below listed questions .</p>', 4, 3, 133, '4', '2013-02-26 19:34:44', 475, NULL, NULL, '', '', NULL, '', '0'),
(490, '<p>\r\n	Enter the name of the biggest software company in the world</p>', 3, 4, 133, '3', '2013-02-26 19:34:44', 476, NULL, NULL, '', '', NULL, '', '0'),
(491, '<p>\r\n	Who is in photo ?</p>\r\n<p>\r\n	<img alt="" src="/elsphpwebquiz_platinum/ckeditor/kcfinder/upload_img/images/michael_schumacher1.jpg" style="width: 300px; height: 300px;" /></p>', 1, 1, 134, '5', '2013-02-26 19:57:30', 473, NULL, NULL, '', '', NULL, '', '0'),
(492, '<p>\r\n	Which is correct ?</p>', 0, 2, 134, '5', '2013-02-26 19:57:30', 474, NULL, NULL, 'Simple header', 'Simple footer', NULL, '', '0'),
(493, '<p>\r\n	Please, answer below listed questions .</p>', 4, 3, 134, '4', '2013-02-26 19:57:30', 475, NULL, NULL, '', '', NULL, '', '0'),
(494, '<p>\r\n	Enter the name of the biggest software company in the world</p>', 3, 4, 134, '3', '2013-02-26 19:57:30', 476, NULL, NULL, '', '', NULL, '', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `question_groups`
--

CREATE TABLE IF NOT EXISTS `question_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(450) NOT NULL,
  `show_header` int(11) NOT NULL,
  `group_total` decimal(18,0) NOT NULL,
  `show_footer` int(11) DEFAULT NULL,
  `check_total` decimal(18,0) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `group_name_eng` varchar(450) DEFAULT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=546 ;

--
-- Дамп данных таблицы `question_groups`
--

INSERT INTO `question_groups` (`id`, `group_name`, `show_header`, `group_total`, `show_footer`, `check_total`, `question_id`, `parent_id`, `group_name_eng`, `added_date`) VALUES
(524, 'People', 1, '0', NULL, NULL, 473, 0, NULL, '2013-02-26 22:08:53'),
(525, '', 0, '0', NULL, NULL, 474, 0, NULL, '2013-02-26 22:09:30'),
(526, 'Header text', 1, '0', NULL, NULL, 475, 0, NULL, '2013-02-26 22:11:27'),
(527, 'Header text', 1, '0', NULL, NULL, 476, 0, NULL, '2013-02-26 22:12:14'),
(528, '', 0, '0', NULL, NULL, 477, 0, NULL, '2013-02-26 22:26:21'),
(529, '', 0, '0', NULL, NULL, 478, 0, NULL, '2013-02-26 22:27:01'),
(534, 'People', 1, '0', NULL, NULL, 483, 524, NULL, '2013-02-26 19:23:39'),
(535, '', 0, '0', NULL, NULL, 484, 525, NULL, '2013-02-26 19:23:39'),
(536, 'Header text', 1, '0', NULL, NULL, 485, 526, NULL, '2013-02-26 19:23:39'),
(537, 'Header text', 1, '0', NULL, NULL, 486, 527, NULL, '2013-02-26 19:23:39'),
(538, 'People', 1, '0', NULL, NULL, 487, 524, NULL, '2013-02-26 19:34:44'),
(539, '', 0, '0', NULL, NULL, 488, 525, NULL, '2013-02-26 19:34:44'),
(540, 'Header text', 1, '0', NULL, NULL, 489, 526, NULL, '2013-02-26 19:34:44'),
(541, 'Header text', 1, '0', NULL, NULL, 490, 527, NULL, '2013-02-26 19:34:44'),
(542, 'People', 1, '0', NULL, NULL, 491, 524, NULL, '2013-02-26 19:57:30'),
(543, '', 0, '0', NULL, NULL, 492, 525, NULL, '2013-02-26 19:57:30'),
(544, 'Header text', 1, '0', NULL, NULL, 493, 526, NULL, '2013-02-26 19:57:30'),
(545, 'Header text', 1, '0', NULL, NULL, 494, 527, NULL, '2013-02-26 19:57:30');

-- --------------------------------------------------------

--
-- Структура таблицы `question_types`
--

CREATE TABLE IF NOT EXISTS `question_types` (
  `id` int(11) NOT NULL,
  `question_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `question_types`
--

INSERT INTO `question_types` (`id`, `question_type`) VALUES
(0, 'Multi answer (checkbox)'),
(3, 'Free text (textarea)'),
(4, 'Multi text (numbers only)'),
(1, 'One answer (radio button)');

-- --------------------------------------------------------

--
-- Структура таблицы `quizzes`
--

CREATE TABLE IF NOT EXISTS `quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `quiz_name` varchar(500) CHARACTER SET utf8 NOT NULL,
  `quiz_desc` varchar(500) NOT NULL,
  `added_date` datetime NOT NULL,
  `parent_id` int(11) NOT NULL,
  `show_intro` int(11) NOT NULL,
  `intro_text` varchar(3850) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135 ;

--
-- Дамп данных таблицы `quizzes`
--

INSERT INTO `quizzes` (`id`, `cat_id`, `quiz_name`, `quiz_desc`, `added_date`, `parent_id`, `show_intro`, `intro_text`) VALUES
(130, 61, 'Simple quiz', 'Simple description', '2013-02-26 22:01:03', 0, 1, '<p>\r\n	&nbsp;</p>\r\n<p style="margin: 0px; padding: 0px; font-family: tahoma; line-height: 20px; text-align: justify;">\r\n	This is an example quiz&nbsp; . This software software written in&nbsp;<span style="margin: 0px; padding: 0px; color: rgb(255, 0, 0);">PHP</span>.</p>\r\n<p style="margin: 0px; padding: 0px; font-family: tahoma; line-height: 20px; text-align: justify;">\r\n	Please , contact if you will have any questions .</p>\r\n<p style="margin: 0px; padding: 0px; font-family: tahoma; line-height: 20px; text-align: justify;">\r\n	<a href="mailto:support@aspnetpower.com" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(85, 77, 66);">support@aspnetpower.com</a></p>'),
(134, 61, 'Simple quiz', 'Simple description', '2013-02-26 22:01:03', 130, 1, '<p>\r\n	&nbsp;</p>\r\n<p style="margin: 0px; padding: 0px; font-family: tahoma; line-height: 20px; text-align: justify;">\r\n	This is an example quiz&nbsp; . This software software written in&nbsp;<span style="margin: 0px; padding: 0px; color: rgb(255, 0, 0);">PHP</span>.</p>\r\n<p style="margin: 0px; padding: 0px; font-family: tahoma; line-height: 20px; text-align: justify;">\r\n	Please , contact if you will have any questions .</p>\r\n<p style="margin: 0px; padding: 0px; font-family: tahoma; line-height: 20px; text-align: justify;">\r\n	<a href="mailto:support@aspnetpower.com" style="margin: 0px; padding: 0px; text-decoration: none; color: rgb(85, 77, 66);">support@aspnetpower.com</a></p>');

-- --------------------------------------------------------

--
-- Структура таблицы `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(455) NOT NULL DEFAULT '',
  `temp_id` int(11) NOT NULL DEFAULT '0',
  `header_text` varchar(555) NOT NULL DEFAULT '',
  `footer_text` varchar(555) NOT NULL DEFAULT '',
  `img_count` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0',
  `restrict_user` int(11) NOT NULL DEFAULT '0',
  `bgcolor` varchar(255) NOT NULL DEFAULT '',
  `added_date` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `lang` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `ratings`
--

INSERT INTO `ratings` (`id`, `description`, `temp_id`, `header_text`, `footer_text`, `img_count`, `show_results`, `restrict_user`, `bgcolor`, `added_date`, `status`, `lang`) VALUES
(26, 'rating 1', 6, '', '', 5, 1, 1, '-1', '2011-12-20 12:23:00', 1, 'English'),
(31, 'rating 2', 3, '', '', 5, 1, 2, '-1', '2011-12-20 12:23:19', 1, 'English'),
(32, 'rating 3', 2, '', '', 5, 1, 1, '-1', '2011-12-20 12:23:31', 1, 'English');

-- --------------------------------------------------------

--
-- Структура таблицы `rating_temps`
--

CREATE TABLE IF NOT EXISTS `rating_temps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temp_name` varchar(255) NOT NULL DEFAULT '',
  `active_img` varchar(255) NOT NULL DEFAULT '',
  `inactive_img` varchar(255) NOT NULL DEFAULT '',
  `half_active_img` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `rating_temps`
--

INSERT INTO `rating_temps` (`id`, `temp_name`, `active_img`, `inactive_img`, `half_active_img`) VALUES
(1, 'Star1', '1.gif', '1_n.gif', '1_h.gif'),
(2, 'Star1', '7.gif', '7_n.gif', '7_h.gif'),
(3, 'Star2', '2.gif', '2_n.gif', '2_h.gif'),
(4, 'Star3', '3.gif', '3_n.gif', '3_h.gif'),
(5, 'Star4', '4.gif', '4_n.gif', '4_h.gif'),
(6, 'Star5', '5.gif', '5_n.gif', '5_h.gif'),
(7, 'Star6', '6.gif', '6_n.gif', '6_h.gif'),
(8, 'Star7', '8.gif', '8_n.gif', '8_h.gif');

-- --------------------------------------------------------

--
-- Структура таблицы `roles_rights`
--

CREATE TABLE IF NOT EXISTS `roles_rights` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Дамп данных таблицы `roles_rights`
--

INSERT INTO `roles_rights` (`Id`, `role_id`, `module_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 4),
(4, 1, 6),
(5, 1, 7),
(13, 1, 1),
(12, 1, 12),
(11, 1, 11),
(9, 2, 9),
(10, 2, 10),
(14, 1, 5),
(15, 1, 13),
(16, 2, 8),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 2, 20),
(22, 2, 13),
(23, 1, 21),
(24, 1, 22),
(25, 1, 23),
(26, 1, 24),
(27, 1, 26),
(28, 1, 25),
(29, 1, 27),
(30, 1, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Surname` varchar(150) NOT NULL,
  `added_date` datetime NOT NULL,
  `user_type` int(11) DEFAULT NULL,
  `email` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `random_str` varchar(100) DEFAULT NULL,
  `approved` int(11) NOT NULL,
  `disabled` int(11) NOT NULL,
  `user_photo` varchar(500) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50058 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`UserID`, `UserName`, `Password`, `Name`, `Surname`, `added_date`, `user_type`, `email`, `address`, `phone`, `random_str`, `approved`, `disabled`, `user_photo`, `country_id`) VALUES
(50006, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Polat', 'Alemdar', '0000-00-00 00:00:00', 1, 'elshan999@mail.ru', '', '', NULL, 1, 0, '5876658f6863dfbf8065e4b2cf697bea.jpg', 228),
(50053, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', 'user1', 'user1', '2013-02-26 22:13:06', 2, 'phpwebquizgold@mail.ru', '', '', NULL, 1, 0, '6529de195611cf69e394d3afab676a5c.jpg', 241),
(50054, 'user2', '7e58d63b60197ceb55a1c487989a3720', 'user2', 'user2', '2013-02-26 22:15:54', 2, 'phpwebquizgold@mail.ru1', '', '', NULL, 1, 0, 'f4a21b6657345f57c3cc009131dbe109.jpg', 63),
(50055, 'user3', '92877af70a45fd6a2ed7fe81e1236b78', 'user3', 'user3', '2013-02-26 22:17:20', 2, 'phpwebquizgold@mail.ru2', '', '', NULL, 1, 0, 'd25d086a9c64cccf1a62cf24a613c109.jpg', 48),
(50056, 'user4', '3f02ebe3d7929b091e3d8ccfde2f3bc6', 'user4', 'user4', '2013-02-26 22:24:05', 2, 'phpwebquizgold@mail.ru3', '', '', NULL, 1, 0, '9cce8627d30d2f4a150f52a3df153021.jpg', 154),
(50057, 'user5', '0a791842f52a0acfbb3a783378c066b8', 'user5', 'user5', '2013-02-26 22:25:21', 2, 'phpwebquizgold@mail.ru4', '', '', NULL, 1, 0, '6d02ca6efc062038cedd5f1768ee796f.jpeg', 63);

-- --------------------------------------------------------

--
-- Структура таблицы `user_answers`
--

CREATE TABLE IF NOT EXISTS `user_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_quiz_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `user_answer_id` int(11) DEFAULT NULL,
  `user_answer_text` varchar(3800) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_answers_ibfk_1` (`user_quiz_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=536 ;

--
-- Дамп данных таблицы `user_answers`
--

INSERT INTO `user_answers` (`id`, `user_quiz_id`, `question_id`, `answer_id`, `user_answer_id`, `user_answer_text`, `added_date`) VALUES
(509, 91, 491, 176676, 176676, NULL, '2013-02-26 23:57:40'),
(510, 91, 492, 176680, 176680, NULL, '2013-02-26 23:57:44'),
(511, 91, 492, 176681, 176681, NULL, '2013-02-26 23:57:44'),
(512, 91, 493, 176686, NULL, '3', '2013-02-26 23:57:47'),
(513, 91, 494, 176690, NULL, 'Microsoft', '2013-02-26 23:57:51'),
(514, 92, 491, 176676, 176676, NULL, '2013-02-26 23:58:42'),
(515, 92, 492, 176680, 176680, NULL, '2013-02-26 23:58:46'),
(516, 92, 492, 176681, 176681, NULL, '2013-02-26 23:58:46'),
(517, 92, 493, 176686, NULL, '3', '2013-02-26 23:58:53'),
(518, 92, 493, 176687, NULL, '4', '2013-02-26 23:58:53'),
(519, 92, 494, 176690, NULL, 'Microsoft', '2013-02-26 23:58:58'),
(520, 93, 491, 176676, 176676, NULL, '2013-02-27 00:01:10'),
(522, 93, 492, 176680, 176680, NULL, '2013-02-27 00:01:18'),
(523, 93, 492, 176681, 176681, NULL, '2013-02-27 00:01:18'),
(524, 93, 493, 176686, NULL, '3', '2013-02-27 00:01:21'),
(525, 93, 493, 176687, NULL, '4', '2013-02-27 00:01:21'),
(526, 93, 493, 176688, NULL, '5', '2013-02-27 00:01:21'),
(527, 93, 493, 176689, NULL, '6', '2013-02-27 00:01:21'),
(528, 93, 494, 176690, NULL, 'Microsoft', '2013-02-27 00:01:25'),
(529, 94, 491, 176676, 176676, NULL, '2013-02-27 00:01:56'),
(530, 94, 494, 176690, NULL, 'Microsoft', '2013-02-27 00:02:07'),
(531, 95, 491, 176677, 176677, NULL, '2013-02-27 00:02:20'),
(532, 95, 492, 176680, 176680, NULL, '2013-02-27 00:02:22'),
(533, 95, 493, 176686, NULL, '3', '2013-02-27 00:02:25'),
(534, 95, 493, 176687, NULL, '4', '2013-02-27 00:02:25'),
(535, 95, 494, 176690, NULL, '', '2013-02-27 00:02:28');

-- --------------------------------------------------------

--
-- Структура таблицы `user_quizzes`
--

CREATE TABLE IF NOT EXISTS `user_quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `success` int(11) DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `pass_score_point` decimal(10,2) DEFAULT NULL,
  `pass_score_perc` decimal(10,2) DEFAULT NULL,
  `archived` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assignment_id` (`assignment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=96 ;

--
-- Дамп данных таблицы `user_quizzes`
--

INSERT INTO `user_quizzes` (`id`, `assignment_id`, `user_id`, `status`, `added_date`, `success`, `finish_date`, `pass_score_point`, `pass_score_perc`, `archived`) VALUES
(91, 43, 50053, 2, '2013-02-26 23:57:38', 1, '2013-02-26 23:57:51', '14.00', '81.25', 0),
(92, 43, 50054, 2, '2013-02-26 23:58:39', 1, '2013-02-26 23:58:58', '15.00', '87.50', 0),
(93, 43, 50055, 2, '2013-02-27 00:01:08', 1, '2013-02-27 00:01:25', '17.00', '100.00', 0),
(94, 43, 50056, 2, '2013-02-27 00:01:53', 1, '2013-02-27 00:02:07', '8.00', '50.00', 0),
(95, 43, 50057, 2, '2013-02-27 00:02:17', 0, '2013-02-27 00:02:28', '4.50', '25.00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_ratings`
--

CREATE TABLE IF NOT EXISTS `user_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_id` int(11) NOT NULL DEFAULT '0',
  `product_id` varchar(1255) NOT NULL DEFAULT '',
  `point` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_ratings_ibfk_1` (`rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL,
  `type_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `user_types`
--

INSERT INTO `user_types` (`id`, `type_name`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_imported_users`
--
CREATE TABLE IF NOT EXISTS `v_imported_users` (
`UserID` int(11)
,`Name` varchar(250)
,`Surname` varchar(255)
,`UserName` varchar(150)
,`Password` varchar(150)
,`email` varchar(150)
,`user_photo` varchar(11)
);
-- --------------------------------------------------------

--
-- Структура для представления `v_imported_users`
--
DROP TABLE IF EXISTS `v_imported_users`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_imported_users` AS select `imported_users`.`id` AS `UserID`,`imported_users`.`name` AS `Name`,`imported_users`.`surname` AS `Surname`,`imported_users`.`user_name` AS `UserName`,`imported_users`.`password` AS `Password`,`imported_users`.`email` AS `email`,'nophoto.jpg' AS `user_photo` from `imported_users`;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answers_quiz`
--
ALTER TABLE `answers_quiz`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `question_groups` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `assignment_users`
--
ALTER TABLE `assignment_users`
  ADD CONSTRAINT `assignment_users_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `question_groups`
--
ALTER TABLE `question_groups`
  ADD CONSTRAINT `question_groups_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_answers`
--
ALTER TABLE `user_answers`
  ADD CONSTRAINT `user_answers_ibfk_1` FOREIGN KEY (`user_quiz_id`) REFERENCES `user_quizzes` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_quizzes`
--
ALTER TABLE `user_quizzes`
  ADD CONSTRAINT `user_quizzes_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_ratings`
--
ALTER TABLE `user_ratings`
  ADD CONSTRAINT `user_ratings_ibfk_1` FOREIGN KEY (`rate_id`) REFERENCES `ratings` (`id`) ON DELETE CASCADE;
