<?php

  /*  This code has been developed by Els . Els11@yandex.ru    
		In God I trust 				*/
		
if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
  define("SQL_IP", "localhost"); // ip address of mysql database 
  define("SQL_USER", "root");  // username for connecting to mysql
  define("SQL_PWD","harddisk"); // password for connecting to mysql
  define("SQL_DATABASE","client"); 	
  define('SITEURLM',		'http://demo.ueisworkshop.com');
} elseif ($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
  define("SQL_IP", "localhost"); // ip address of mysql database 
  define("SQL_USER", "root");  // username for connecting to mysql
  define("SQL_PWD","harddisk"); // password for connecting to mysql
  define("SQL_DATABASE","client");  
  define('SITEURLM',    'http://district.ueisworkshop.com');
}
else if($_SERVER["HTTP_HOST"]=="localhost"){

  define("SQL_IP", "52.27.109.252"); 
  define("SQL_USER", "root"); 
  define("SQL_PWD","harddisk"); 
  define("SQL_DATABASE","client");
  define('SITEURLM','http://localhost/client');
}

  define("WEB_SITE_URL","http://"); // the url where you installed this script . do not delete last slash  
  define("USE_MATH", "yes"); // yes , no . if you want to use math symbols , you have to enable it
  define("DEBUG_SQL","yes"); // enable it , if you want to view sql queries .
  define("PAGING","30");  // paging for all grids
  define("SHOW_MENU","[show_menu]"); // all = all users , registered = registered users  , nobody = menu will be disabled
  define("SHOW_MENU_ON_LOGIN_PAGE", "[show_menu_login]"); // yes , no

  define("MAIL_FROM", "Workshop Assessment Platform"); // 
  define("MAIL_CHARSET", "UTF-8");
  define("MAIL_USE_SMTP", "yes");
  define("MAIL_SERVER", ""); // only if smtp enabled
  define("MAIL_USER_NAME", ""); // only if smtp enabled
  define("MAIL_PASSWORD", ""); // only if smtp enabled

  define("REGISTRATION_ENABLED", "yes");
  define("ALLOW_AVATAR_CHANGE", "yes");
  define("SITE_TEMPLATE", "platinum"); 
  define("ENABLE_CALCULATOR", "yes");
  define("DEFAULT_COUNTRY","241");

/*  $SYSTEM_NAME="PHP Web Quiz";*/

  $SYSTEM_NAME="WorkShop Assessment Platform";
  
  $PAGE_TITLE = "Workshop Assesssment Platform";
  
  $allowed_avatar_formats=array("jpg","gif","jpeg","png");
  $max_avatar_size="500"; // kbs  
  
  function Imported_Users_Password_Hash($entered_password,$password_from_db)
  {
      return md5($entered_password);
  }

  @session_start();

  // LANGUAGE CONFIGURATION
  $LANGUAGES = array("english.php"=>"English","spanish.php"=>"Spanish");
  $DEFAULT_LANGUAGE_FILE="english.php";
  
  $FLAGS['English'] = "flag-gb";
/*  $FLAGS['English'] = "flag-gb";*/
  $FLAGS['Spanish'] = "flag-sp";

  //----------------------------do not touch the code below--------------------------------
  
  if(isset($_SESSION['lang_file']))
  {
      $DEFAULT_LANGUAGE_FILE = $_SESSION['lang_file'];
  }
  
  if(isset($_GET['lang']))
  {
      $lang_arr = util::translate_array($LANGUAGES);
      if(isset($lang_arr[$_GET['lang']])) $DEFAULT_LANGUAGE_FILE = $lang_arr[$_GET['lang']];
  }

  require "lang/".$DEFAULT_LANGUAGE_FILE;

  ini_set ('magic_quotes_gpc', 0);
  ini_set ('magic_quotes_runtime', 0);
  ini_set ('magic_quotes_sybase', 0);
  ini_set('session.bug_compat_42',0);
  ini_set('session.bug_compat_warn',0);  
  
  $avatar_width="200"; //px
  
  //----------------------------------------------------------------------
  
?>
