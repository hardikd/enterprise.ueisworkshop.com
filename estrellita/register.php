<?php
  $RUN = 1;
  require "lib/util.php";
  require 'config.php';
 
  require 'db/mysql2.php';
  require 'db/access_db.php';  
  require "lib/access.php";
  require "db/orm.php";
  require "lib/validations.php";
  require "lib/webcontrols.php";
  require "db/asg_db.php";

  if(!isset($LANGUAGES)) header("location:install/index.php");
  
   if(REGISTRATION_ENABLED!="yes") exit();
  
  include "lib/libmail.php";
  include "lib/cmail.php";

  $val = new validations("btnRegister");
  $val->AddValidator("txtName", "empty", R_NAME_VAL,"");
  $val->AddValidator("txtSurname", "empty", R_SURNAME_VAL,"");
  $val->AddValidator("txtLogin", "an", R_LOGIN_VAL , "");
  $val->AddValidator("txtPass", "an", R_PASSWORD_VAL , "");
  $val->AddValidator("txtEmail", "email", R_EMAIL_VAL , "");
  $val->AddValidator("drpCountries", "empty", COUNTRY_VAL , "");
  
  
  $countries_res = orm::Select("countries_quiz", array(), array(), "country_name");
  $country_options = webcontrols::GetOptions($countries_res, "id", "country_name", DEFAULT_COUNTRY);
  
  $step1_display="";
  $step2_display = "none";
  
  if(isset($_GET['step']))
  {
      $step1_display="none";          
      $step2_display = "";
      if($_GET['step']=="2")
      {
          $msg = R_EMAIL_SENT;        
      }
      else if($_GET['step']=="3")
      {
              if(!isset($_GET['g']))
              {
                      header("location:login.php");
              }

              $guid = trim($_GET['g']);

              $results = orm::Select("users",array(), array("random_str"=>$guid), "");
              $count = db::num_rows($results);
              if($count >0 ) 
              {
                      $row = db::fetch($results);
                      if($row['approved']=="0")
                      {
                              orm::Update("users", array("approved"=>1), array("random_str"=>$guid));
                              asgDB::AcceptNewUser($row["UserID"]);
                              $msg = R_REG_APPROVED." ".R_GO_TO." <a href='login.php'>".R_LOGIN_PAGE."</a>";
                      } else $msg = R_REG_ALREADY_APPROVED." ".R_GO_TO." <a href='login.php'>".R_LOGIN_PAGE."</a>";
              }
              else $msg = R_URL_WRONG;
      }
  }
  
if(isset($_POST["txtName"]) && $val->Isvalid())
{
	$guid = md5(util::Guid());
	orm::Insert("users" , array("UserName"=>$_POST['txtLogin'],
						"Password"=>md5(trim($_POST['txtPass'])),					
						"Name"=>$_POST['txtName'],
						"Surname"=>$_POST['txtSurname'],
						"added_date"=>util::Now(),
						"user_type"=>2,
						"email"=>$_POST['txtEmail'],
						"address"=>$_POST['txtAddr'],
						"phone"=>$_POST['txtPhone'],
                                                "country_id"=>$_POST['drpCountries'],
						"approved"=>0,
						"disabled"=>0,
						"random_str"=>$guid
					));

/*echo $url = WEB_SITE_URL1."register.php?step=3&g=".$guid;	*/
	
if($_SERVER["HTTP_HOST"]=="district.ueisworkshop.com"){
		$url ="http://district.ueisworkshop.com/Quiz/register.php?step=3&g=".$guid;
	} else if($_SERVER["HTTP_HOST"]=="enterprise.ueisworkshop.com"){
    $url ="http://enterprise.ueisworkshop.com/Quiz/register.php?step=3&g=".$guid;
  }
	else if($_SERVER["HTTP_HOST"]=="localhost"){
		 $url ="http://localhost/client/Quiz/register.php?step=3&g=".$guid;
	}


	$results = orm::Select("users", array(),array("UserName"=>$_POST['txtLogin']), "");
	
	
	$row = db::fetch($results);
	
	
	
	$cmail = new cmail("register_user",$row);

	$m= new Mail; 
	$m->From(MAIL_FROM ); 
	$m->To( trim($_POST['txtEmail']) );
	$m->Subject( $cmail->subject );
	$m->Body( str_replace("[url]", $url , $cmail->body) );  
	$m->Priority(3);
	//$m->Attach( "asd.gif","", "image/gif" ) ;
	
	if(MAIL_USE_SMTP=="")
	{
		$m->smtp_on(MAIL_SERVER, MAIL_USER_NAME, MAIL_PASSWORD ) ;    

	}
	
	$m->Send(); 

	header("location:register.php?step=2");
}

if(isset($_POST["ajax"]))
{
         $results = orm::Select("users", array(), array("UserName"=>$_POST["login_to_check"]) , "");
         $count = db::num_rows($results);
	 $msg = 0;
         if($count > 0)
	 {
		$msg= LOGIN_ALREADY_EXISTS ;
         }

 	 $results = orm::Select("users", array(), array("email"=>$_POST["email"]) , "");
         $count = db::num_rows($results);
         if($count > 0)
	 {
		$msg= EMAIL_ALREADY_EXISTS ;
         }

	 echo $msg;
}
else
{  
  include "register_tmp.php";
}


?>