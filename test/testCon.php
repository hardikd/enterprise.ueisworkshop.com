<?php
$con = mysql_connect("localhost","tor","torapp");
if($con){
mysql_select_db("tor");

$sql="CREATE TABLE IF NOT EXISTS `answers_quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `answer_text` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `answer_image` varchar(450) CHARACTER SET utf8 DEFAULT NULL,
  `correct_answer` int(11) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `correct_answer_text` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `answer_pos` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL,
  `answer_text_eng` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `control_type` int(11) DEFAULT NULL,
  `answer_parent_id` int(11) DEFAULT NULL,
  `text_unit` char(10) CHARACTER SET utf8 NOT NULL,
  `answer_desc` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=176691 ;

--
-- Dumping data for table `answers_quiz`
--

INSERT INTO `answers_quiz` (`id`, `group_id`, `answer_text`, `answer_image`, `correct_answer`, `priority`, `correct_answer_text`, `answer_pos`, `parent_id`, `answer_text_eng`, `control_type`, `answer_parent_id`, `text_unit`, `answer_desc`) VALUES
(176608, 524, 'Michael Schumacher', NULL, 1, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Michael Schumacher'),
(176609, 524, 'Bill Gates', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Bill Gates'),
(176610, 524, 'Robert Miles', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Robert Miles'),
(176611, 524, 'Bruce Lee', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Description for Bruce Lee'),
(176612, 525, '2+2=4', NULL, 1, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176613, 525, '2*2=4', NULL, 1, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176614, 525, '2+2=3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176615, 525, '2*2=3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176616, 525, '2+2=7', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176617, 525, '2/2=7', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', 'Some description'),
(176618, 526, '1+2=?', NULL, 0, 0, '3', 1, 0, NULL, 1, 0, '', ''),
(176619, 526, '1+3=?', NULL, 0, 0, '4', 1, 0, NULL, 1, 0, '', ''),
(176620, 526, '1+4=?', NULL, 0, 0, '5', 1, 0, NULL, 1, 0, '', ''),
(176621, 526, '1+5=?', NULL, 0, 0, '6', 1, 0, NULL, 1, 0, '', ''),
(176622, 527, '', NULL, 0, 0, 'Microsoft', 1, 0, NULL, 1, 0, '', ''),
(176623, 528, 'Answer1', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176624, 528, 'Answer2', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176625, 528, 'Answer3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176626, 528, 'Answer4', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176627, 529, 'Answer1', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176628, 529, 'Answer2', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176629, 529, 'Answer3', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176630, 529, 'Answer4', NULL, 0, 0, '', 1, 0, NULL, 1, 0, '', ''),
(176646, 534, 'Michael Schumacher', NULL, 1, 0, '', 1, 176608, NULL, 1, 0, '', ''),
(176647, 534, 'Bill Gates', NULL, 0, 0, '', 1, 176609, NULL, 1, 0, '', ''),
(176648, 534, 'Robert Miles', NULL, 0, 0, '', 1, 176610, NULL, 1, 0, '', ''),
(176649, 534, 'Bruce Lee', NULL, 0, 0, '', 1, 176611, NULL, 1, 0, '', ''),
(176650, 535, '2+2=4', NULL, 1, 0, '', 1, 176612, NULL, 1, 0, '', ''),
(176651, 535, '2*2=4', NULL, 1, 0, '', 1, 176613, NULL, 1, 0, '', ''),
(176652, 535, '2+2=3', NULL, 0, 0, '', 1, 176614, NULL, 1, 0, '', ''),
(176653, 535, '2*2=3', NULL, 0, 0, '', 1, 176615, NULL, 1, 0, '', ''),
(176654, 535, '2+2=7', NULL, 0, 0, '', 1, 176616, NULL, 1, 0, '', ''),
(176655, 535, '2/2=7', NULL, 0, 0, '', 1, 176617, NULL, 1, 0, '', ''),
(176656, 536, '1+2=?', NULL, 0, 0, '3', 1, 176618, NULL, 1, 0, '', ''),
(176657, 536, '1+3=?', NULL, 0, 0, '4', 1, 176619, NULL, 1, 0, '', ''),
(176658, 536, '1+4=?', NULL, 0, 0, '5', 1, 176620, NULL, 1, 0, '', ''),
(176659, 536, '1+5=?', NULL, 0, 0, '6', 1, 176621, NULL, 1, 0, '', ''),
(176660, 537, '', NULL, 0, 0, 'Microsoft', 1, 176622, NULL, 1, 0, '', ''),
(176661, 538, 'Michael Schumacher', NULL, 1, 0, '', 1, 176608, NULL, 1, 0, '', ''),
(176662, 538, 'Bill Gates', NULL, 0, 0, '', 1, 176609, NULL, 1, 0, '', ''),
(176663, 538, 'Robert Miles', NULL, 0, 0, '', 1, 176610, NULL, 1, 0, '', ''),
(176664, 538, 'Bruce Lee', NULL, 0, 0, '', 1, 176611, NULL, 1, 0, '', ''),
(176665, 539, '2+2=4', NULL, 1, 0, '', 1, 176612, NULL, 1, 0, '', ''),
(176666, 539, '2*2=4', NULL, 1, 0, '', 1, 176613, NULL, 1, 0, '', ''),
(176667, 539, '2+2=3', NULL, 0, 0, '', 1, 176614, NULL, 1, 0, '', ''),
(176668, 539, '2*2=3', NULL, 0, 0, '', 1, 176615, NULL, 1, 0, '', ''),
(176669, 539, '2+2=7', NULL, 0, 0, '', 1, 176616, NULL, 1, 0, '', ''),
(176670, 539, '2/2=7', NULL, 0, 0, '', 1, 176617, NULL, 1, 0, '', ''),
(176671, 540, '1+2=?', NULL, 0, 0, '3', 1, 176618, NULL, 1, 0, '', ''),
(176672, 540, '1+3=?', NULL, 0, 0, '4', 1, 176619, NULL, 1, 0, '', ''),
(176673, 540, '1+4=?', NULL, 0, 0, '5', 1, 176620, NULL, 1, 0, '', ''),
(176674, 540, '1+5=?', NULL, 0, 0, '6', 1, 176621, NULL, 1, 0, '', ''),
(176675, 541, '', NULL, 0, 0, 'Microsoft', 1, 176622, NULL, 1, 0, '', ''),
(176676, 542, 'Michael Schumacher', NULL, 1, 0, '', 1, 176608, NULL, 1, 0, '', ''),
(176677, 542, 'Bill Gates', NULL, 0, 0, '', 1, 176609, NULL, 1, 0, '', ''),
(176678, 542, 'Robert Miles', NULL, 0, 0, '', 1, 176610, NULL, 1, 0, '', ''),
(176679, 542, 'Bruce Lee', NULL, 0, 0, '', 1, 176611, NULL, 1, 0, '', ''),
(176680, 543, '2+2=4', NULL, 1, 0, '', 1, 176612, NULL, 1, 0, '', ''),
(176681, 543, '2*2=4', NULL, 1, 0, '', 1, 176613, NULL, 1, 0, '', ''),
(176682, 543, '2+2=3', NULL, 0, 0, '', 1, 176614, NULL, 1, 0, '', ''),
(176683, 543, '2*2=3', NULL, 0, 0, '', 1, 176615, NULL, 1, 0, '', ''),
(176684, 543, '2+2=7', NULL, 0, 0, '', 1, 176616, NULL, 1, 0, '', ''),
(176685, 543, '2/2=7', NULL, 0, 0, '', 1, 176617, NULL, 1, 0, '', ''),
(176686, 544, '1+2=?', NULL, 0, 0, '3', 1, 176618, NULL, 1, 0, '', ''),
(176687, 544, '1+3=?', NULL, 0, 0, '4', 1, 176619, NULL, 1, 0, '', ''),
(176688, 544, '1+4=?', NULL, 0, 0, '5', 1, 176620, NULL, 1, 0, '', ''),
(176689, 544, '1+5=?', NULL, 0, 0, '6', 1, 176621, NULL, 1, 0, '', ''),
(176690, 545, '', NULL, 0, 0, 'Microsoft', 1, 176622, NULL, 1, 0, '', '');";
$result = mysql_query($sql);


}else{
	die("could not connect!");
	}
?>