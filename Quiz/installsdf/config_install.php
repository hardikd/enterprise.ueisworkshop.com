<?php

  /*  This code has been developed by Els . Els11@yandex.ru    
		In God I trust 				*/

  define("SQL_IP", "[mysql_host]"); // ip address of mysql database 
  define("SQL_USER", "[mysql_user]");  // username for connecting to mysql
  define("SQL_PWD","[mysql_pass]"); // password for connecting to mysql
  define("SQL_DATABASE","[mysql_db]"); // database where you have executed sql scripts

  define("WEB_SITE_URL","[url]"); // the url where you installed this script . do not delete last slash  
  define("USE_MATH", "[use_math]"); // yes , no . if you want to use math symbols , you have to enable it
  define("DEBUG_SQL","no"); // enable it , if you want to view sql queries .
  define("PAGING","[paging]");  // paging for all grids
  define("SHOW_MENU","[show_menu]"); // all = all users , registered = registered users  , nobody = menu will be disabled
  define("SHOW_MENU_ON_LOGIN_PAGE", "[show_menu_login]"); // yes , no

  define("MAIL_FROM", "[mail_from]"); // 
  define("MAIL_CHARSET", "[mail_charset]");
  define("MAIL_USE_SMTP", "yes");
  define("MAIL_SERVER", "[mail_server]"); // only if smtp enabled
  define("MAIL_USER_NAME", "[mail_user]"); // only if smtp enabled
  define("MAIL_PASSWORD", "[mail_pass]"); // only if smtp enabled

  define("REGISTRATION_ENABLED", "[registration_enabled]");
  define("ALLOW_AVATAR_CHANGE", "[allow_avatar_change]");
  define("SITE_TEMPLATE", "platinum"); 
  define("ENABLE_CALCULATOR", "[enable_calculator]");
  define("DEFAULT_COUNTRY","[default_country]");

  $SYSTEM_NAME="[system_name]";
  $PAGE_TITLE = "[page_title]";
  
  $allowed_avatar_formats=array("jpg","gif","jpeg","png");
  $max_avatar_size="500"; // kbs  
  
  function Imported_Users_Password_Hash($entered_password,$password_from_db)
  {
      return md5($entered_password);
  }

  @session_start();

  // LANGUAGE CONFIGURATION
  $LANGUAGES = array("english.php"=>"English","russian.php"=>"Russian");
  $DEFAULT_LANGUAGE_FILE="english.php";
  
  $FLAGS['English'] = "flag-gb";
  $FLAGS['Russian'] = "flag-ru";

  //----------------------------do not touch the code below--------------------------------
  
  if(isset($_SESSION['lang_file']))
  {
      $DEFAULT_LANGUAGE_FILE = $_SESSION['lang_file'];
  }
  
  if(isset($_GET['lang']))
  {
      $lang_arr = util::translate_array($LANGUAGES);
      if(isset($lang_arr[$_GET['lang']])) $DEFAULT_LANGUAGE_FILE = $lang_arr[$_GET['lang']];
  }

  require "lang/".$DEFAULT_LANGUAGE_FILE;

  ini_set ('magic_quotes_gpc', 0);
  ini_set ('magic_quotes_runtime', 0);
  ini_set ('magic_quotes_sybase', 0);
  ini_set('session.bug_compat_42',0);
  ini_set('session.bug_compat_warn',0);  
  
  $avatar_width="200"; // px
  
  //----------------------------------------------------------------------
  
?>
