<?php if(!isset($RUN)) { exit(); } ?>
<!DOCTYPE html>
<html lang="en" class="login_page">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $PAGE_TITLE ?></title>
    
        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.min.css" />
        <!-- theme color-->
            <link rel="stylesheet" href="css/blue.css" />
        <!-- tooltip -->    
			<link rel="stylesheet" href="lib/qtip2/jquery.qtip.min.css" />
        <!-- main styles -->
            <link rel="stylesheet" href="css/style.css" />
    
        <!-- Favicon -->
            <link rel="shortcut icon" href="favicon.ico" />

        <!--[if lte IE 8]>
            <script src="js/ie/html5.js"></script>
			<script src="js/ie/respond.min.js"></script>
        <![endif]-->
	<script language="JavaScript" type="text/javascript" src="lib/validations.js"></script>
        <?php echo $val->DrowJsArrays(); ?>	
    </head>
    <body>
        
        <script language = "javascript">
            
function restore_password()
{
	document.pass_form.btnSend.disabled=true;

	var email= document.getElementById('txtEmail').value;

        var status=validate();

        if(status)
        {
			
		
             $.post("forgot_password.php", { email_for_restoring: email, ajax: "yes" },
             function(data){
             
                    alert(data);
                     document.pass_form.btnSend.disabled=false;
                 

            });
        }
        else
        {
            document.pass_form.btnSend.disabled=false;
        } 	
}
		
function checkuser()
{
	var u = document.login_form.drpUserType;
	var usertype = u.options[u.selectedIndex].value;
	if(usertype == '')
	{
		alert('Please Select user type');
		return false;
	}
}
</script>
<div style="width:100%;position:absolute;top:0px;left:0px;z-index:2;"> <img src="img/WorkshopLogo.jpg" height="100px" /> </div>
		<div class="login_box">
			
			<form method="post" name ="login_form" id="login_form" onSubmit="return checkuser();">
				<div class="top_b"><?php echo SIGN_UP_TO ?> <?php echo WRKSHOP_QUIZ_LOGIN ?></div>    
				<div class="alert alert-info alert-login">
					<?php echo ENTER_USERNAME ?><br>
                                        <font color=red><?php echo $msg ?></font>
				</div>
				<div class="cnt_b">
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" name="txtLogin" placeholder="<?php echo LOGIN ?>" value="" />
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span><input type="password" id="password" name="txtPass" placeholder="<?php echo PASSWORD ?>" value="" />
						</div>
					</div>
                                        <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span> <select name="drpLang" ><?php echo $language_options ?></select>
						</div>
					</div>
					
                    <div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span> <select name="drpUserType" >	
                            <option  value="" > Select </option>
                            <option  value="1" > Student </option>
                            <option  value="2" > Teacher </option>
                           
                            </select>
				</div>
					</div>
					
				</div>
				<div class="btm_b clearfix">
					<button class="btn btn-inverse pull-right" name="btnSubmit" type="submit"><?php echo SIGN_IN ?></button>
                                        <?php if(REGISTRATION_ENABLED=="yes") { ?>
					<span ><a href="register.php"><?php echo REGISTER_IN_SYSTEM ?></a></span>
                                        <?php } ?>
				</div>  
			</form>
			
			<form action="dashboard.html" method="post" id="pass_form" name="pass_form" style="display:none">
				<div class="top_b"><?php echo CANT_SIGN_IN ?></div>    
					<div class="alert alert-info alert-login">
					<?php echo ENTER_EMAIL_PASSWORD_R ?>
				</div>
				<div class="cnt_b">
					<div class="formRow clearfix">
						<div class="input-prepend">
							<span class="add-on">@</span><input type="text" id="txtEmail" name="txtEmail" placeholder="<?php echo EMAIL ?>" />
						</div>
					</div>
				</div>
				<div class="btm_b tac">
					<button class="btn btn-inverse" id="btnSend" type="button" onClick="restore_password()"><?php echo REQUEST_NEW_PASSWORD ?></button>
				</div>  
			</form>
			
			<form action="dashboard.html" method="post" id="reg_form" style="display:none">
				<div class="top_b"><?php echo SIGN_UP_TO ?><?php echo $SYSTEM_NAME ?></div>
				<div class="alert alert-login">
					<?php T_ACCEPT ?> <a data-toggle="modal" href="#terms"><?php echo T_AND_S ?></a>.
				</div>
				<div id="terms" class="modal hide fade" style="display:none">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h3><?php echo T_AND_C ?></h3>
					</div>
					<div class="modal-body">
						<p>
							<?php echo T_AND_C_TEXT ?>
						</p>
					</div>
					<div class="modal-footer">
						<a data-dismiss="modal" class="btn" href="#"><?php echo CLOSE ?></a>
					</div>
				</div>
				<div class="cnt_b">
					
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input type="text" placeholder="<?php echo LOGIN ?>" />
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span><input type="text" placeholder="<?php echo PASSWORD ?>" />
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on">@</span><input type="text"  placeholder="<?php echo EMAIL ?>" />
						</div>
						<small><?php echo EMAIL_TEXT ?></small>
					</div>
					 
				</div>
				<div class="btm_b tac">
					<button class="btn btn-inverse" type="submit">Sign Up</button>
				</div>  
			</form>
			
			<div class="links_b links_btm clearfix">
                             <?php if(REGISTRATION_ENABLED=="yes") { ?>
				<span class="linkform"><a href="#pass_form"><?php echo REG_FORGOT_PASS ?></a></span>
                             <?php } ?>
				<span class="linkform" style="display:none"><a href="#login_form"><?php echo SIGN_IN_SCREEN ?></a></span>
			</div>
		</div>
        
<div style="right:0px;bottom:0px;position:absolute;z-index:2;"> <img src="img/WorkshopLogo.png" height="60" /> </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.actual.min.js"></script>
        <script src="lib/validation/jquery.validate.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                
				//* boxes animation
				form_wrapper = $('.login_box');
				function boxHeight() {
					form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
				};
				form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
                $('.linkform a,.link_reg a').on('click',function(e){
					var target	= $(this).attr('href'),
						target_height = $(target).actual('height');
					$(form_wrapper).css({
						'height'		: form_wrapper.height()
					});	
					$(form_wrapper.find('form:visible')).fadeOut(400,function(){
						form_wrapper.stop().animate({
                            height	 : target_height,
							marginTop: ( - (target_height/2) - 24)
                        },500,function(){
                            $(target).fadeIn(400);
                            $('.links_btm .linkform').toggle();
							$(form_wrapper).css({
								'height'		: ''
							});	
                        });
					});
					e.preventDefault();
				});
				
				//* validation
				$('#login_form').validate({
					onkeyup: false,
					errorClass: 'error',
					validClass: 'valid',
					rules: {
						username: { required: true, minlength: 3 },
						password: { required: true, minlength: 3 }
					},
					highlight: function(element) {
						$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
				});
            });
        </script>
    </body>
</html>
