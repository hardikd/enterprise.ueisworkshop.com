<?php
//session_start();  
  
  require "lib/util.php";
  require 'config.php';
  require 'db/mysql2.php';
  require 'db/access_db.php';  
  require "lib/access.php";
  require "db/orm.php";
  require "lib/validations.php";
  require "lib/webcontrols.php";

  if(!isset($LANGUAGES)) header("location:install/index.php");

  if(USE_MATH=="yes") include("mathpublisher/mathpublisher.php");

//print_r($_SESSION);exit;
  $RUN = 1;

   //@session_start();

  $home=$_SESSION['home'];
  $module_name = GetModuleName();
  $expand =false;
  $autorized = false;

//$_SESSION['user'] = '';
	//if($_SESSION['user'] == "teacher" )
	//{
		//if($_SESSION['user_id'] == "teacher" )
		//$modules = access_db::GetModules2($_SESSION['txtLogin'], $_SESSION['txtPass'],$_SESSION['txtPassImp'],true);
  //	}
	//else
	//{
	$modules = access_db::GetModules($_SESSION['txtLogin'], $_SESSION['txtPass'],$_SESSION['txtPassImp'],true);
	
	//	}
		
  	$has_result = db::num_rows($modules);
  

  	if($has_result==0 && $_SESSION['teacher_id']=="")
  	{ 
        	header("location:login.php");
        	exit;
  	}

  	if($has_result!=0) {
  		$_SESSION['KCFINDER'] = array();
  		$_SESSION['KCFINDER']['disabled'] = false;
 	}
  	$autorized = true;
	$expand  = true;
  	ExpandModules($modules);
  
    
  function ExpandModules($modules)
  {
      global $child_modules,$main_modules;
      while($row=db::fetch($modules))
      {	  
          if($row['parent_id']=="0")
          {		  
              $main_modules[] = $row;
          }
          else
          {        
              $child_modules[$row['parent_id']][]=$row;
          }
      }
  }   

  $menus = orm::Select("pages", array("page_name","id"), array(), "priority");

  ShowModule();

  function ShowModule()
  {
        global $module_name,$module_t_name,$Util;

       // $module_name= GetModuleName() ;

        if(!file_exists("modules/$module_name".".php") || $module_name=="" || strpos($module_name,"../")!=0)
            $module_name="default";

        $module_t_name=$module_name."_tmp";

  }   
  
  
  $pages = db::GetResultsAsArray(orm::GetSelectQuery("pages", array(), array(), "priority")); 
  $html = "";
  AddTopMenu("0");
  
  function AddTopMenu($parent_id)
  {
      global $pages,$html;
      for($i=0;$i<count($pages);$i++)
      {                
          if($pages[$i]['parent_id']==$parent_id)
          {                      
              $child_items = GetChildMenu($pages[$i]['id']);
              $dropdown_icon = "";
              $href = "#";
              $class="";
              if($child_items!="") 
              {
                  $dropdown_icon="<b class=\"caret\"></b>";      
                  $class = "data-toggle=\"dropdown\" class=\"dropdown-toggle\" ";
              }
              else $href= "?module=show_page&id=".$pages[$i]['id'];
                           
              $page_name=$pages[$i]['page_name'];            
              $html.="<li class=\"dropdown\"><a $class href=\"$href\"><i class=\"icon-book icon-white\"></i> $page_name $dropdown_icon</a>\n";                          
              $html.=$child_items;
              $html.="</li>";
          }
      }     
  }
  
  function GetChildMenu($parent_id)
  {
      global $pages;
      $child_html = "";
      for($i=0;$i<count($pages);$i++)
      {                
          if($pages[$i]['parent_id']==$parent_id)
          {              
              $page_name=$pages[$i]['page_name'];
              $href= "?module=show_page&id=".$pages[$i]['id'];
              $child_html.="<li><a href=\"$href\">$page_name</a></li>";
          }
      }     
      if($child_html!="")
      {
          $child_html="<ul class=\"dropdown-menu\">$child_html</ul>\n";
      }
      return $child_html;
  }
  
  $fullname = $_SESSION['name']." ".$_SESSION['surname'];
  $currentlang = $_SESSION['lang_file'];
  $currentlang = $LANGUAGES[$currentlang];
  $flag = $FLAGS[$currentlang];
  $languages_html = "<a href=\"#\" class=\"dropdown-toggle nav_condensed\" data-toggle=\"dropdown\"><i class=\"$flag\"></i> <b class=\"caret\"></b></a>";
  $languages_html.="<ul class=\"dropdown-menu\">";
  $languages_html.=GetLanguageHtml();
  $languages_html.="</ul>";

 // echo $LANGUAGES[$currentlang];
  function GetLanguageHtml()
  {
      global $languages_html,$LANGUAGES,$FLAGS;
     // $lang_arr = util::translate_array($LANGUAGES);      
      foreach($LANGUAGES as $key=>$value)
      {
        //  $currentlang=$arr[$key];
         // echo $key;
          $flag = $FLAGS[$value];
          $lnghref ="intface_util.php?change_lang=true&lang=$value";
          $languages_html.="<li><a href=\"$lnghref\"><i class=\"$flag\"></i> $value</a></li>";
      }
  }

  function GetModuleName()
  {
	return isset($_GET["module"]) ? $_GET["module"] : "default" ;
  }

  include "modules/".$module_name.".php";
  if(!isset($_POST["ajax"]))
  {

	   $queries = debug::GetSQLs();
        include "index_".SITE_TEMPLATE."_tmp.php";
//	include "index_tmp.php";
  }  


?>
