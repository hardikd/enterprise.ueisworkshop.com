<?php 

require "extgrid.php";    

$id = util::GetID("?module=assignments");

$hedaers = array(SAVE, BODY, ADDED_DATE, "&nbsp;");
$columns = array("subject"=>"text","body"=>"test","added_date"=>"test");
    
$grd = new extgrid($hedaers,$columns, "index.php?module=nots&id=$id");

$grd->edit=false;

if(isset($_POST["add"]))
{
    $query = orm::GetInsertQuery("nots", array("asg_id"=>$id,"subject"=>$_POST["subject"],"body"=>$_POST["body"],"added_date"=>util::Now(), "sent_by"=>$_SESSION['user_id']));
    db::exec_insert($query);
}

$query = orm::GetSelectQuery("nots", array(), array("asg_id"=>$id), "added_date");
$grd->main_table="nots";
$grd->auto_delete=true;
$grd->DrowTable($query);
$grid_html = $grd->table;


 if(isset($_POST["ajax"]))
 {
     echo $grid_html;
 }


function desc_func()
{
        return NOTIFICATIONS;
}

?>