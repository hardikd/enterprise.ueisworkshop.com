<?php	
	
    class qst_viewer
    {
        var $html = "";
        var $show_next = true;
        var $show_prev = true;
        var $page_name = "";
        var $show_finish = false;
        var $show_correct_answers = false;

        var $read_only_text = "";

        var $user_quiz_id=-1;
        var $ans_priority="1";
        var $control_unq;
        var $ids = "";

        public function qst_viewer($page)
        {
            $this->page_name = $page;
        }

        private function BuildButtons($template,$row)
        {
            $buttons_html = "<tr><td align=left>";
            if($this->show_prev==true)
            {
                $prev_js = "javascript:PrevQst('".$this->page_name."',$row[question_type_id],$row[prev_priority],$row[id])";
                $buttons_html .= "<input type=button onclick=".$prev_js." style='width:110px' value='< ".PREVIOUS."'>&nbsp;";
            }
            if($this->show_next==true)
            {
                $next_js = "javascript:NextQst('".$this->page_name."',$row[question_type_id],$row[next_priority],$row[id],0)";
                $buttons_html .= "<input type=button onclick=".$next_js." style='width:110px' value='".NEXT." >'>";
            }
            if($this->show_finish==true)
            {
                $finish_js = "javascript:NextQst('".$this->page_name."',$row[question_type_id],$row[next_priority],$row[id],1)";
                $buttons_html .="<input onclick=".$finish_js." type=button style='width:110px' value='".FINISH."'>";
            }
            $buttons_html .= "</td></tr>";
            $template = str_replace("[buttons]", $buttons_html, $template);
            return $template;
        }

        public function BuildQuestion($row,$totalPages,$asg_id,$quiz_type)
			{      
			 $template = qst_viewer::ReadTemplate();
			  $noOfQues = count($row);   
		 $template_start =' <div id="result"><table align="left" cellpadding="5" cellspacing="5" border="0" width="100%">';
				
				if($_REQUEST['pageId'] == '')
				{
					$_REQUEST['pageId'] = 1;
				}
				if($_REQUEST['pageId']<$totalPages)
				{
					$next_page = intval($_REQUEST['pageId'])+1;
					$button_value = "Save and Next";
					$button_name = "save_and_next";
				}  
				else //if($_REQUEST['pageId']>=$totalPages)
				{
					$next_page = $totalPages;
					$button_value = "Finish";
					$button_name = "finished";
				}
				
				if($totalPages ==1)
				{
					$next_page = $totalPages;
					$button_value = "Finish";
					$button_name = "finished";
				}
				if($_REQUEST['pageId'] == '')
				{
					$next_page = 2;
				}
		/*		echo "<script type='text/javascript'> alert(' req page -".$_REQUEST['pageId']."next page- ".$next_page."'); </script>";
		*/
				if($_REQUEST['pageId']!=1)
				{
					$prev_page = intval($_REQUEST['pageId'])-1;
			 	}  
				else
				{
					$prev_page = 1;
				 }
				 
			 for($i = 0 ;$i<$noOfQues;$i++)
			{ 
				
			 if($row[$i]["question_text"]!='') //  if blank
			 {
				if($i==0)
				{
					$headerTitle ='';
					$headerTitle = $row[$i]["header_text"];
					$headerTitle = $headerTitle;
					define("HEADERTITLE",$headerTitle);
				}
				
				if($this->user_quiz_id>-1)
				{
					$ans_results = questions_db::GetAnswersByQstID2($row[$i]['id'],$this->user_quiz_id);
					$ans_results_sec = questions_db::GetAnswersByQstID2sec($row[$i]['id'],$this->user_quiz_id);
				}
				else
				{
					$ans_results = questions_db::GetAnswersByQstID($row[$i]['id']);
					$ans_results_sec = questions_db::GetAnswersByQstIDsec($row[$i]['id']);
					
				}
				$question_note_text  ='';
				$question_note = questions_db::GetQuestion_note($row[$i]['id'],$this->user_quiz_id,$_SESSION['user_id']);  $question_note_row=db::fetch($question_note);
				 $question_note_text = $question_note_row['user_note_text']; 
             
				 
				$pri_que_type = $row[$i]['question_type_id'];
				$sec_que_type = $row[$i]['sec_question_type_id'];
	 	 	 	$answers_html = $this->BuildAnswers($ans_results,$ans_results_sec,$pri_que_type ,$sec_que_type,$row[$i]['id']);
				
				
				$questionTitle = $row[$i]["question_text"];
				$ans_results1 = questions_db::GetAnswersByQstID($row[$i]['id']);
				$row11=db::fetch($ans_results1);
				$group_name = $row11["group_name"];
				$footer_text = $row[$i]["footer_text"];
				$template_body .='<input type="hidden" id="que_id[]" name="que_id[]" value="'.$row[$i]['id'].'">
				<input type="hidden" id="quiz_type" name="quiz_type" value="'.$quiz_type.'">
				<input type="hidden" name="que_type_'.$row[$i]['id'].'" id="que_type_'.$row[$i]['id'].'" value="'.$row[$i]['question_type_id'].'">
				<input type="hidden" name="sec_que_type_'.$row[$i]['id'].'" id="sec_que_type_'.$row[$i]['id'].'" value="'.$row[$i]['sec_question_type_id'].'">
				<tr width="100%" ><td  valign="top" >
					<table width="100%" >';
						
                $template_body .='<tr>
                        <td align="left"   valign="top"   > 
                            <font face="tahoma" size="4"><b>'.$questionTitle.'</b></font>
                        </td>
                    </tr>    
                    <tr>
                        <td align="left" class="qst_ans_table" valign="top"	>
                           <table border="0"  width="100%" class="table table-striped table-bordered">
                                <tr>
                                    <td  colspan="2">
                                        '.$group_name.'
                                    </td>
                                </tr>
                                '.$answers_html.'
								<tr>
                                    <td  colspan="2">
                                        Note : <input type="text" name="txtnote_'.$row[$i]['id'].'" id="txtnote" value="'.$question_note_text.'" style="width:500px" /> 
                                     </td>
                                </tr>
                            </table>            
                        </td>
                    </tr>';
				    if($footer_text!="")
				    { 
						$template_body .='<tr>
							<td class="footer_text" valign="top" valign="top" >'.$footer_text.'
							</td></tr>';
					} 
					$template_body .='</table></td></tr>';
				}// end if blank
				} // end for
			
	   			$template_buttn = '<tr  width="100%" ><td align="center"> <input type="button" id="previousbtn" value="Previous" onclick = "javascript:goBack();">&nbsp; <input type=hidden name=isFinish id=isFinish value=""><input type="hidden" name="pageId" value="'.$next_page.'"/> <input type="submit" id="btnsubmit" value="'.$button_value.'" name ="'.$button_name.'"';
				//if($button_name == 'finished' )
				$template_buttn .= 'onclick = "return validate();"';
				 $template_buttn .='/> </td></tr>';
				
				
				// Commenting Temp
				$pagination = $this->getRecords($totalPages,$asg_id);
				$template_end = '<tr ><td align="center">'.$pagination.'</td></tr>';				
				// End Commenting
				
				$template_end .='</table></div> ';
	
				$template = $template_start.$template_body.$template_buttn.$template_end;
	   
          	 	//$template = $this->BuildButtons($template,$row);
				
				//$hiddens = "<input type=hidden name=hdnPriority id=hdnPriority value=".$row[$i]['priority']."><input type=hidden name=hdnNextPriority id=hdnNextPriority value=".$row[$i]['next_priority'].">";
			//	$template = str_replace("[hiddens]", $hiddens, $template);
		 //	$template = str_replace("[widthSize]", $widthSize, $template);
			 
    		$this->html =  $template;
	 		return $row[0];
			
	   }
	   
	   public function BuildQuestion2($row,$asg_id)
			{      
			
			 $template = qst_viewer::ReadTemplate();
			
			$que_id = $row['id'];
			 $template_start =' <div id="result"><table align="left" cellpadding="5" cellspacing="5" border="0" width="100%">';
			 
		 		if($this->user_quiz_id>-1)
				{
					$ans_results = questions_db::GetAnswersByQstID2($que_id,$this->user_quiz_id);
					$ans_results_sec = questions_db::GetAnswersByQstID2sec($que_id,$this->user_quiz_id);
				}
				else
				{
					$ans_results = questions_db::GetAnswersByQstID($que_id);
					$ans_results_sec = questions_db::GetAnswersByQstIDsec($que_id);
					
				} 
				$pri_que_type = $row['question_type_id'];
				$sec_que_type = $row['sec_question_type_id'];
	 	 	 	$answers_html = $this->BuildAnswers($ans_results,$ans_results_sec,$pri_que_type ,$sec_que_type,$que_id);
				
				$headerTitle = $row["header_text"];
				$questionTitle = $row["question_text"];
				$group_name = $row["group_name"];
				$footer_text = $row["footer_text"];
				$template_body .='<input type="hidden" id="que_id[]" name="que_id[]" value="'.$que_id.'">
				<input type="hidden" id="quiz_type" name="quiz_type" value="'.$quiz_type.'">
				<input type="hidden" name="que_type_'.$que_id.'" id="que_type_'.$que_id.'" value="'.$row['question_type_id'].'">
				<input type="hidden" name="sec_que_type_'.$que_id.'" id="sec_que_type_'.$que_id.'" value="'.$row['sec_question_type_id'].'">
				<tr width="100%" ><td valign="top" >
					<table width="100%" >';
					if($headerTitle!="")
				    {	
						$template_body .='<tr><td class="header_text" valign="top"   style="border:thin">'.$headerTitle.'</td></tr>';
					}	
                $template_body .='<tr>
                        <td align="left"   valign="top"   > 
                            <font face="tahoma" size="4"><b>'.$questionTitle.'</b></font>
                        </td>
                    </tr>    
                    <tr>
                        <td align="left" class="qst_ans_table" valign="top"	>
                           <table border="0"  width="100%" class="table table-striped table-bordered">
                                <tr>
                                    <td  colspan="2">
                                        '.$group_name.'
                                    </td>
                                </tr>
                                '.$answers_html.'
                            </table>            
                        </td>
                    </tr>';
				    if($footer_text!="")
				    { 
						$template_body .='<tr>
							<td class="footer_text" valign="top" valign="top" >'.$footer_text.'
							</td></tr>';
					}
					$template_body .='</table></td></tr>';
			
	   			
				$template_end .='</table></div> ';
	
				$template = $template_start.$template_body.$template_end;
	   	$this->html =  $template;
	 		return $row[0];
	   }
	   
	   function getRecords($totalPages,$asg_id)
	   {
	   		for($intCount=1;$intCount<=$totalPages;$intCount++)
				{
					if($_REQUEST['pageId']==$intCount)
						$pagination.= '&nbsp;<u><a href="index.php?module=start_quiz&id='.$asg_id.'&pageId='.$intCount.'">' .$intCount. '</a></u>';
						
					else
						$pagination.= '&nbsp;<a href="index.php?module=start_quiz&id='.$asg_id.'&pageId='.$intCount.'">' .$intCount. '</a>';
				}
			return $pagination;
	   }
		
		public static function get_answer_text($answer_text)
		{
			$results = $answer_text;
			if(USE_MATH=="yes")
			{
				$results=mathfilter($answer_text,"14","mathpublisher/img/");				
			}
			return $results;
		}
        
        public function BuildQuestionWithQuery($query)
        {
             $rows = db::exec_sql($query);
             $row=db::fetch($rows);
             $this->BuildQuestion($row);
        }

        public function BuildQuestionWithResultset($resultset,$totalPages,$asg_id,$quiz_type)
        {
           $this->BuildQuestion($resultset,$totalPages,$asg_id,$quiz_type); 
        }
        
		public function BuildQuestionWithResultset2($resultset,$asg_id)
        {
           $this->BuildQuestion2($resultset,$asg_id); 
        }
        
        public function GetIDS()
        {
            $ids = $this->ids;
            if($ids!="")
            {
                $ids = substr($ids,1); 
            }
            return $ids;
        }
   			     
        public function BuildAnswers($ans_results,$ans_results_sec,$question_type,$sec_que_type,$qid)
        {
		   $control_unq = $this->control_unq;             
             $answers_html="";
             $tabs = "&nbsp;&nbsp;&nbsp;";
			 $int = 1;  $sint = 1;
             while($row=db::fetch($ans_results))
             {      
			 	//  echo "<pre>";
		// print_r($row); 
          
				  $correct_answer = "";
                  $answers_val="";
                  $answer_desc = $row['answer_text'];
                  $id= $row['a_id'];
                  $this->ids .= ",$id";
                  switch($question_type) {
                  case 0:                      
                      if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
                      $answers_html.= "<tr><td ><input ".$answers_val." type=checkbox id=chkAns_".$qid."[] ".$this->read_only_text." name=chkAns_".$qid."[] value='".$row['a_id']."'></td><td align=left style=\"width:99%\" ><span id=s$id class=\"ttip_t\" title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer</td></tr>";
                  break;
                  case 1:
                      if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
                      $answers_html.=  "<tr><td ><input ".$answers_val." type=radio id=rdAns_".$qid." $control_unq ".$this->read_only_text." name=rdAns_".$qid." $control_unq value='".$row['a_id']."'></td><td  align=left style=\"width:99%\" ><span id=s$id  class=ttip_t title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer</td></tr>";
                  break;
                  case 3:
                      if($this->show_correct_answers==true) $correct_answer = "<br><font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.=  "<tr><td class=desc_text_bg><textarea style='width:350px;height:100px' id=txtFree_".$qid." ".$this->read_only_text." name=txtFree_".$qid." value='".$row['a_id']."'>".$answers_val."</textarea>$correct_answer".
                                       "<input type=hidden name=txtFreeId_".$qid." id=txtFreeId_".$qid." value='".$row['a_id']."'></td></tr>";
                  break;
                  case 4:
                      if($this->show_correct_answers==true) $correct_answer = "$tabs<font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.=  "<tr><td  class=desc_text_bg>".qst_viewer::get_answer_text($row['answer_text'])."</td><td class=desc_text_bg align=left ><input type=text onkeypress='return onlyNumbers();' id=txtMultiAns_".$qid."[] ".$this->read_only_text." name=txtMultiAns_".$qid."[] value='".$answers_val."' >".
                                       "<input type=hidden id=txtMultiAnsId_".$qid."[] name=txtMultiAnsId_".$qid."[] value='".$row['a_id']."' >$correct_answer</td></tr>";
                  break;
				  case 5:
                      if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
                      $answers_html.=  "<tr><td ><input ".$answers_val." type=radio id=rdAnsMulti_".$qid." $control_unq ".$this->read_only_text." name=rdAnsMulti_".$qid." $control_unq value='".$row['a_id']."'></td><td  align=left style=\"width:99%\" ><span id=s$id  class=ttip_t title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer</td></tr>";
                  break;
		         case 6:                      
                 /*    if($cannotCheck == 1)
					 {
					  if($this->show_correct_answers==true && $row['cant_decode']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
					  if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
					  $answers_html.= "<tr><td ><input ".$answers_val." type=checkbox id=chkDecodeAns_".$qid." ".$this->read_only_text." name=chkDecodeAns_".$qid." value='".$row['a_id']."'></td><td align=left style=\"width:99%\" ><span id=s$id class=\"ttip_t\" title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer</td></tr>";
					  $cannotCheck = 0;
				 	  break;
					 }	
					   */ // to dispalay checkbox for cannot decode.
					   
					 if($row['radio_ans']!= 1)
					 {  
					 if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
					 $answers_val =''; $colspan ='';
                      if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
	 					 
					if($row['selfcorrectsvowel'] != '1' ){
						// $colspan = "cosplan='2'";
						 $selfcorrecttext=''; 
				 		  }
						 else { 
						 		$selfcorrectselectedy =''; $selfcorrectselectedn='';
						    if($this->user_quiz_id>-1 && $row['user_answer_id']!="") 
							{ 
 								 if($row['self_c_vowel'] == '1' && $row['user_answer_id']== $row['a_id'])
								 {  $selfcorrectselectedy = "checked='checked'"; } else { $selfcorrectselectedy ='';}
								 if($row['self_c_vowel'] == '0' && $row['user_answer_id']== $row['a_id'])
								 {  $selfcorrectselectedn = "checked='checked'";} else { $selfcorrectselectedn ='';}
						    }
						 	   $selfcorrecttext = " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".SELF_CORRECTS_VOWEL."&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='self_c_vowel_".$qid."' id = 'self_c_vowel_".$qid."'".$this->read_only_text." value = '1' ".$selfcorrectselectedy." onclick='return checkisselected(".$qid.",".$row['a_id'].")' />&nbsp;Yes&nbsp;&nbsp; <input type='radio' name='self_c_vowel_".$qid."' id = 'self_c_vowel_".$qid."'".$this->read_only_text." value = '0' ".$selfcorrectselectedn." onclick='return checkisselected(".$qid.",".$row['a_id'].")' /> &nbsp;No <input type='hidden' name='scv_".$sint."[]' value='".$row['a_id']."'/> " ;  
							   $sint++;
	 				}
						if($cannotCheck == 1){$cant_decode = "<input type='hidden' name=cant_decode_".$qid." value='".$row['a_id']."' />" ; } else {$cant_decode = ''; } 
                       $answers_html.= "<tr><td ><input ".$answers_val." type=radio id=chkRadioAns_".$qid." $control_unq ".$this->read_only_text." name=chkRadioAns_".$qid." $control_unq value='".$row['a_id']."'></td><td align=left style=\"width:99%\" ><span id=s$id class=\"ttip_t\" title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer ".$cant_decode." ".$selfcorrecttext."</td></tr>";
						$cannotCheck =0;
					   if($row['radio_ans']== 2)
					    $cannotCheck = 1;  // to dispalay checkbox for cannot decode.	
					   break;
				 	 }
					else if($row['radio_ans']== 1)
					{ 
					 if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER." Yes)</font>";
					 elseif($this->show_correct_answers==true && $row['correct_answer']=="0") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER." No)</font>";
					if($this->user_quiz_id>-1 && $row['user_answer_id']!="")
						{ $answers_yes ='';$answers_no=''; if($row['user_answer_text']== 1){ $answers_yes = "checked"; $answers_no = "";}
						 else if($row['user_answer_text']== 0) {$answers_yes = ""; $answers_no = "checked"; } }
						$answers_html.=  "<tr><td>&nbsp;</td><td  align=left style=\"width:99%\" ><span id=s$id  class=ttip_t title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ".$answers_yes." type=radio id=radioChkAns_".$qid."_".$int." ".$this->read_only_text." name=radioChkAns_".$qid."_".$int." value='1' />&nbsp;Yes&nbsp;&nbsp;<input ".$answers_no." type=radio id=radioChkAns_".$qid."_".$int." ".$this->read_only_text." name=radioChkAns_".$qid."_".$int." value='0' />&nbsp;No</td>  <input type='hidden' name=radioChkAnsId_".$qid."_".$int." value='".$row['a_id']."'/> </tr>";
					 $int++;
					 break;
					}	
			 		
				} 
	        }
			
			 $issecheader = 0;
			 while($row=db::fetch($ans_results_sec))
             {     
			 	if($issecheader < 1)
				{
					if($row['group_name'] == '')
					{
						$headtitlesec = 'Scoring Question';
					}
					else
					{
						$headtitlesec = $row['group_name'];
					}
				 	 $answers_html.= "<tr><td colspan ='2'><b>".$headtitlesec ."  </b></td> </tr>";
					$issecheader ++;
				}
				  $correct_answer = "";
                  $answers_val="";
                  $answer_desc = $row['answer_text'];
                  $id= $row['a_id'];
                  $this->ids .= ",$id";
				   
                  switch($sec_que_type) {
                  case 0:                      
                      if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
                      $answers_html.= "<tr><td colspan='2'><table width='100%'><tr><td ><input ".$answers_val." type=checkbox id=chkAnsSec_".$qid."[] ".$this->read_only_text." name=chkAnsSec_".$qid."[] value='".$row['a_id']."'></td><td align=left style=\"width:99%\" ><span id=sSec$id class=\"ttip_t\" title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer</td></tr></table></td></tr>";
                  break;
                  case 1:
                      if($this->show_correct_answers==true && $row['correct_answer']=="1") $correct_answer = "<font color=red>$tabs (".CORRECT_ANSWER.")</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_id']!="") $answers_val = "checked";
                      $answers_html.=  "<tr><td colspan='2'><table width='100%'><tr><td ><input ".$answers_val." type=radio id=rdAnsSec_".$qid." $control_unq ".$this->read_only_text." name=rdAnsSec_".$qid." $control_unq value='".$row['a_id']."'></td><td  align=left style=\"width:99%\" ><span id=sSec$id  class=ttip_t title=\"$answer_desc\">".qst_viewer::get_answer_text($row['answer_text'])."</span>$correct_answer</td></tr></table></td></tr>";
                  break;
                  case 3:
                      if($this->show_correct_answers==true) $correct_answer = "<br><font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.=  "<tr><td colspan='2'><table width='100%'><tr><td class=desc_text_bg><textarea style='width:350px;height:100px' id=txtFreeSec_".$qid." ".$this->read_only_text." name=txtFreeSec_".$qid." value='".$row['a_id']."'>".$answers_val."</textarea>$correct_answer".
                                       "<input type=hidden name=txtFreeIdSec_".$qid." id=txtFreeIdSec_".$qid." value='".$row['a_id']."'></td></tr></table></td></tr>";
                  break;
                  case 4:
                      if($this->show_correct_answers==true) $correct_answer = "$tabs<font color=red>".CORRECT_ANSWER." : ".$row['correct_answer_text']."</font>";
                      if($this->user_quiz_id>-1 && $row['user_answer_text']!="") $answers_val = $row['user_answer_text'];
                      $answers_html.=  "<tr><td  >".qst_viewer::get_answer_text($row['answer_text'])."</td><td class=desc_text_bg align=left ><input type=text onkeypress='return onlyNumbers();' id=txtMultiAnsSec_".$qid."[] ".$this->read_only_text." name=txtMultiAnsSec_".$qid."[] value='".$answers_val."' >".
                                       "<input type=hidden id=txtMultiAnsIdSec_".$qid."[] name=txtMultiAnsIdSec_".$qid."[] value='".$row['a_id']."' >$correct_answer</td></tr></table></td></tr>";
                  break;
                        }
             }
             return $answers_html;
        }


        public static function ReadTemplate()
        {
            $file = file_get_contents('tmps/question_template.xml', true);
            return $file;
        }

        public function SetReadOnly()
        {
            $this->read_only_text="disabled";
        }

        public function GetPriority()
         {
             $priority = 1;
             if(isset($_POST['hdnPriority']))
             {
                 $priority = intval($_POST['hdnPriority']);
             }
             return $priority;
         }

        public function GetNextPriority()
         {
             $priority = 1;
             if(isset($_POST['next_priority']))
             {
                 $priority = $_POST['next_priority'];
             }
             return $priority;
         }

         public function GetPrevPriority()
         {
             $priority = 1;
             if(isset($_POST['prev_priority']))
             {
                 $priority = $_POST['prev_priority'];
             }
             return $priority;
         }
    
	
//Start code Bhavesh.rakhshiya@maven-infotech.com Note: Don't modify below function
         public function BuildQuestionOfAnswerWithResultset($resultset)
        {
        error_reporting(0);
       
            $this->BuildQuestionAnswer($resultset);
        }
          public function BuildQuestionAnswer($row)
        {        
          /*  if($this->user_quiz_id>-1)
            {
                $ans_results = questions_db::GetAnswersByQstID2($row['id'],$this->user_quiz_id, $this->ans_priority);
            }
            else
            {
                $ans_results = questions_db::GetAnswersByQstID($row['id']);
            }*/

			if($this->user_quiz_id>-1)
				{
					$ans_results = questions_db::GetAnswersByQstID2($row['id'],$this->user_quiz_id);
					$ans_results_sec = questions_db::GetAnswersByQstID2sec($row['id'],$this->user_quiz_id);
				}
				else
				{
					$ans_results = questions_db::GetAnswersByQstID($row['id']);
					$ans_results_sec = questions_db::GetAnswersByQstIDsec($row['id']);
					
				} 
				$pri_que_type = $row['question_type_id'];
				$sec_que_type = $row['sec_question_type_id'];
			   $question_note_text  ='';
				$question_note = questions_db::GetQuestion_note($row['id'],$this->user_quiz_id,$_SESSION['user_id']);  					                $question_note_row=db::fetch($question_note);
				 
            $template = qst_viewer::ReadTemplate();
            $template = str_replace("[question_text]", qst_viewer::get_answer_text($row['question_text']), $template);
            $template = str_replace("[footer_text]", $row['footer_text'], $template);
            $template = str_replace("[header_text]", $row['header_text'], $template);
            $template = str_replace("[group_name]", $row['group_name'], $template);
            $answers_html = $this->BuildAnswers($ans_results,$ans_results_sec,$row['question_type_id'],$row['sec_question_type_id'],$row['id']);
            $template = str_replace("[answers]", $answers_html, $template);
			 $template = str_replace("[notes]", $question_note_row['user_note_text'], $template);
            $template = $this->BuildButtons($template,$row);

            $hiddens = "<input type=hidden name=hdnPriority id=hdnPriority value=".$row['priority']."><input type=hidden name=hdnNextPriority id=hdnNextPriority value=".$row['next_priority'].">";
            $template = str_replace("[hiddens]", $hiddens, $template);

            $this->html =  $template;

            return $row;

        }
       
         //End code Bhavesh.rakhshiya@maven-infotech.com     
    }
	
	
?>
	
 <script type="application/javascript">
function goBack()
{
	 window.history.back();

}
function checkisselected(queid,ansid)
{
 	//var ele=document.getElementById('self_c_vowel_'+queid);
	var e= document.getElementById('chkRadioAns_'+queid);
	var rSelval = $('input[name="chkRadioAns_'+queid+'"]:checked').val();
	
 /*	for(var i=0;i<len;i++)
	{
		alert(e[i]);
	 	if(e[i].checked)
		{
			rSelval=e[i].value;
		} 
	} */
//	alert(rSelval);
 	if( rSelval == undefined)
	{
		alert('Please select the answer first.');return false;
	}
	else if(rSelval != ansid)
	{
		alert('All test items must be completed to advance.');
		return false;
	}
	 else return true;
}

function validate()
{  
 	var a = 'formtest';
	var b=document.getElementById(a);
	var d=document.getElementById(a).elements.length;
	var msg = "<?php echo MUST_ANSWER_ALL;?>";
	var uniqueRadioArray = new Array();
	var elementArrayRadio = new Array();
	//for radio
	for(var i=0;i<d;i++)
	{
		if(b.elements[i].type=='radio')
		{
			if( !b.elements[i].id.match('self_c_vowel'))
			{
				elementArrayRadio.push(b.elements[i].name);
			}
	 	}
	}
  	uniqueRadioArray = ArrNoDupe(elementArrayRadio);
	for(var i=0;i<uniqueRadioArray.length;i++)
	{
		var eleLen = document.getElementsByName(uniqueRadioArray[i]).length;
		var formValid = false;
		var eleN = document.getElementsByName(uniqueRadioArray[i])
	   	for(var j=0;j<eleLen;j++)
		{
		   if(eleN[j].checked)
		   {
				formValid = true;
 		 	}
		}
		if(formValid == false)
		{
			alert(msg);
			return false;
		}
 	}

	var que = [];
	 $('input:hidden[name^="que_id"]').each(function() {
		que.push($(this).val());
	});
	var scv1array = [];
	 $('input:hidden[name^="scv_1"]').each(function() {
		scv1array.push($(this).val());
	});
	var scv2array = [];
	 $('input:hidden[name^="scv_2"]').each(function() {
		scv2array.push($(this).val());
	});
	
	var scvLen = que.length;
	for(var i=0;i<scvLen;i++)
	{
		 var q = que[i];
 	 	var rSelvalue = $('input[name="chkRadioAns_'+q+'"]:checked').val();
		var inarray1 = scv1array.indexOf(rSelvalue);
		var inarray2 = scv2array.indexOf(rSelvalue);
		if(inarray1 != -1 || inarray2 != -1)
		{
			if(!$('input[name="self_c_vowel_'+q+'"]').is(':checked'))
			{
				alert(msg);
				return false;
			}
 				
		}
 	}
 
	// checkbox
	var elementArraychk = new Array();
	var uniqueArraychk = new Array();
	for(var i=0;i<d;i++)
	{
		if(b.elements[i].type=='checkbox')
		{
			elementArraychk.push(b.elements[i].name);
	 	}
	}
	 uniqueArraychk = ArrNoDupe(elementArraychk);
 	for(var i=0;i<uniqueArraychk.length;i++)
	{
		var eleLen = document.getElementsByName(uniqueArraychk[i]).length;
		var formValid = false;
		if(eleLen > 1)
		{
			var eleN = document.getElementsByName(uniqueArraychk[i])
			for(var j=0;j<eleLen;j++)
			{
			   if(eleN[j].checked)
			   {
					formValid = true;
				}
			}
			if(formValid == false)
			{
				alert(msg);
				return false;
			}
		}
 	}
	
	//textarea
	var uniqueArrayTA = new Array();
	for(var i=0;i<d;i++)
	{
		if(b.elements[i].type=='textarea')
		{
			uniqueArrayTA.push(b.elements[i].name);
	 	}
	}
	for(var i=0;i<uniqueArrayTA.length;i++)
	{	
	 	var eleLen =  document.getElementById(uniqueArrayTA[i]).value;
	  	if(eleLen == '')
		{
			alert(msg);
			return false;
		}
	 }
	  
	 // for muti textbox
	var uniqueArrayT = new Array();
	var elementArrayT = new Array();
 	for(var i=0;i<d;i++)
	{
		if(b.elements[i].type=='text')
		{
			if(b.elements[i].id != 'txtnote')
			{
				elementArrayT.push(b.elements[i].name);
			}
	 	}
	}
  	uniqueArrayT = ArrNoDupe(elementArrayT);
 	for(var i=0;i<uniqueArrayT.length;i++)
	{
		var eleLen = document.getElementsByName(uniqueArrayT[i]).length;
		var formValid = false;
	 	var eleN = document.getElementsByName(uniqueArrayT[i]);
		
	   	for(var j=0;j<eleLen;j++)
		{	
	 	 	 if(eleN[j].value == '')
		   	{
				alert(msg);
				return false;
 		 	}
		}
		 
 	}
  
	if(document.getElementById('btnsubmit').value=='Finish')
	{
		document.getElementById('isFinish').value = 'isFinished';
		if(confirm(" <?php echo CLICK_OK_TO_COMPLETE_ASSESSMENT ;?> "))
		{
			 return true;
		}
		else
		{
			return false;
		}
	}
	
}

function ArrNoDupe(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}
</script>
