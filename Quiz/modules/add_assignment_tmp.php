<?php if(!isset($RUN)) { exit(); } ?>
<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<script type="text/javascript" language="javascript">
function profession_fun()
{
	$('#visible_id').hide();
}
function hid_continue()
{
	$('#visible_id').show();
	$('#prop_id').hide();
}

</script>
<script language="JavaScript" type="text/javascript" src="lib/validations.js"></script>
<!-- DATE JS AND CSS-->
                    
                                  <script src="js/jscal2.js"></script>
                                    <script src="js/lang/en.js"></script>
                                    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
                                    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
                                    <link rel="stylesheet" type="text/css" href="css/steel/steel.css" />
                                    
  <!-- DATE JS AND CSS-->
<?php echo $val->DrowJsArrays(); ?>

<script language="javascript">
    function drpTests_onchange()
    {        
        var test_id =$("#drpTests").val(); 
        var id = querySt("id") !="-1" ? "&id="+querySt("id") : "";    
       // alert(test_id);
        if(test_id=="-100")
        {               
            $("#drpQChange").val("2"); 
            $("#drpQChange").attr('disabled', 'disabled');
                 $.post("index.php?module=add_assignment"+id, {  ajax: "yes", show_qst_bank : "yes" },
                   function(data){             
                        //alert(data);
                        document.getElementById('divQstBank').innerHTML=data;
                    });
        }
        else {            
         document.getElementById('divQstBank').innerHTML="";
         $("#drpQChange").removeAttr('disabled');
        }
    }
    function ChangeCat()
    {
        var id = querySt("id") !="-1" ? "&id="+querySt("id") : "";                
        var p_cat_id =$("#drpCats").val();        
        
         $.post("index.php?module=add_assignment"+id, {  ajax: "yes", fill_tests : "yes", cat_id : p_cat_id },
         function(data){             
             document.getElementById('tdTests').innerHTML=data;
             document.getElementById('drpTests').style.width="170px";
             drpTests_onchange();
        });
    }

    function ShowUsers(type)
    {
        if(type=='local')
        {            
            document.getElementById('tdLocalUsers').style.display="";
            document.getElementById('tdImportedUsers').style.display="none";            
            document.getElementById('btnLcl').style.color="red";
            document.getElementById('btnImp').style.color="black";
        }
        else
        {
            document.getElementById('tdLocalUsers').style.display="none";
            document.getElementById('tdImportedUsers').style.display="";
            document.getElementById('btnLcl').style.color="black";
            document.getElementById('btnImp').style.color="red";
        }
    }

    function CheckForm()
    {        
        return validate();
    }

	
    function ShowOptions()
    {
        var type = getType();
        var display = "none";
        if(type=="1" || type=="3") display="";
        else
        {
            document.getElementById('txtSuccessP').value="0";
            document.getElementById('txtTestTime').value="0";
        }

        for(var i=0;i<6;i++)
        {
            document.getElementById("drpTr"+i).style.display=display;
        }        
	 	
		if(document.getElementById("drpType").value == 2 || document.getElementById("drpType").value == 1)
		 {
		 	document.getElementById('stoppara').style.display="none";
  		  }
		 else
		 {
		 	document.getElementById('stoppara').style.display="";
		 }

	 	 if(document.getElementById("drpType").value == 3)
		 {
		 	var id = querySt("id");
		 	document.getElementById('drpQO').options[1].style.display="none";
			document.getElementById('drpAO').options[1].style.display="none"; 
			var quiz_id = document.getElementById('drpTests').options[document.getElementById('drpTests').selectedIndex].value;
			var noofpages = document.getElementById('drpQueperpage').options[document.getElementById('drpQueperpage').selectedIndex].value;  			if(quiz_id != -1 )
			{
				calculateStopPara(quiz_id,noofpages);
			}
			
	 	 }
		 else
		 {
		 	document.getElementById('drpQO').options[1].style.display="";
			document.getElementById('drpAO').options[1].style.display="";
		 }
    }
	 
    function getType()
    {
        var type = document.getElementById('drpType').options[document.getElementById('drpType').selectedIndex].value;
        return type;
    }

	function calculateStopPara(quiz_id,noofpages)
	{
			$.post("index.php?module=add_assignment", { quizId : quiz_id, intPages: noofpages })
			.done(function(data) {
			$('#stoppara').html(data);
			});				
					
	}
	function noOfPageChange()
	{
		if(document.getElementById("drpType").value == 3)
		{
		document.getElementById('stoppara').style.display="none";
		var quiz_id = document.getElementById('drpTests').options[document.getElementById('drpTests').selectedIndex].value;
		var noofpages = document.getElementById('drpQueperpage').options[document.getElementById('drpQueperpage').selectedIndex].value;
		 if(quiz_id != -1 )
			{
				calculateStopPara(quiz_id,noofpages);
			} 	
		document.getElementById('stoppara').style.display="";	
		}		
	}

	
	

</script>
<body onLoad="profession_fun()">
<div id="prop_id">
<form id="form1" method="post">  
  
    <table width="800px">
        <tr>
            <td>
                
      
    <table width="400px">
        <tr>
            <td class="desc_text" width="290px">
                <label class="desc_text"><?php echo ASSIGNMENT_NAME ?> :</label>
            </td>
            <td>
                <input style="width:170px" type="text" name="txtAssignmentName" id="txtAssignmentName" value="<?php echo util::GetData("txtAssignmentName") ?>"  />
            </td>
        </tr>
		 <tr>
            <td class="desc_text" width="290px">
                <label class="desc_text"> Select Subject :</label>
            </td>
            <td>
			<?php 
			//echo '<pre>';
			//print_r($subjects_rows);
			?>
				<select name="selectsubject" style="width:170px"> 
						
				<option> Select Subject </option>					
					
					<?php 
				foreach($subjects_rows as $key => $val)
				{
					?>
<option value="<?Php echo $subjects_rows[$key]['dist_subject_id']; ?>"<?Php if(@$subject_id == $subjects_rows[$key]['dist_subject_id'] ){?> selected="selected" <?Php } ?>> <?Php echo $subjects_rows[$key]['subject_name']; ?></option>
					<?php 
				} ?>
				</select>
				
        
            </td>
        </tr>
        <tr>
            <td class="desc_text" width="290px">
                <label class="desc_text"><?php echo CAT ?> :</label>
            </td>
            <td>
                 <select style="width:170px" id="drpCats" name="drpCats" onChange="ChangeCat()">
                <?php echo $cat_options ?>
                </select>
            </td>
        </tr>
         <tr>
            <td class="text_desc">
                <label class="desc_text"><?php echo TEST ?> :</label>
            </td>
            <td id="tdTests">
                 <select style="width:170px" id="drpTests" name="drpTests">
                     <option value="-1"><?php echo NOT_SELECTED ?></option>
                </select>
            </td>
        </tr>
        
         <tr>   <?php  // code by hitesh patel // ?>
            <td class="text_desc">
                <label class="desc_text"> Question per page :</label> 
            </td>
            <td id="tdTests">
                 <select style="width:170px" id="drpQueperpage" name="drpQueperpage" onChange="noOfPageChange();">
                     <?php echo $show_per_page_questions ?>
                     </select>
            </td>
        </tr> 
        
        <tr>
            <td class="text_desc">
                <label class="desc_text"><?php echo TYPE ?> :</label>
            </td>
            <td>
                 <select style="width:170px" id="drpType" name="drpType" onChange="ShowOptions()">
                     <?php echo $type_options ?>
                </select>
            </td>
        </tr>
        <tr id="stopparatr">
        	<td colspan="2" id="stoppara">
            </td>
         </tr>
        <tr>
            <td class="text_desc">
                <label class="desc_text"><?php echo QUESTIONS_ORDER ?> :</label>
            </td>
            <td>
                 <select style="width:170px" id="drpQO" name="drpQO" onChange="ShowOptions()">
                     <?php echo $questions_order_options ?>
                </select>
            </td>
        </tr>
          <tr>
            <td class="text_desc">
                <label class="desc_text"><?php echo ANSWERS_ORDER ?> :</label>
            </td>
            <td>
                 <select style="width:170px" id="drpAO" name="drpAO" onChange="ShowOptions()">
                     <?php echo $answers_order_options ?>
                </select>
            </td>
        </tr>
         <tr id="drpTr4">
            <td class="text_desc" >
                <label class="desc_text"><?php echo REVIEW_ANSWERS ?> :</label>
            </td>
            <td>
                 <select style="width:170px" id="drpAR" name="drpAR" >
                     <?php echo $review_options ?>
                </select>
            </td>
        </tr>
           <tr id="drpTr0">
            <td class="text_desc">
               <label class="desc_text"><?php echo SHOW_RESULTS ?> :</label>
            </td>
            <td>
                 <select style="width:170px" id="drpShowRes" name="drpShowRes">
                     <?php echo $showres_options ?>
                </select>
            </td>
        </tr>
          <tr id="drpTr1">
            <td class="text_desc">
                <label class="desc_text"><?php echo RESULTS_BY ?> :</label>
            </td>
            <td>
                 <select style="width:170px"  id="drpResultsBy" name="drpResultsBy">
                     <?php echo $result_options ?>
                </select>
            </td>
        </tr>

          <tr>
            <td class="text_desc">
                <label class="desc_text"><?php echo ASG_HOW_MANY ?> :</label>
            </td>
            <td>
                 <select style="width:170px"  id="drpQType" name="drpQType">
                     <?php echo $qtype_options ?>
                </select>
            </td>
        </tr>

		<tr>
            <td class="text_desc">
                <label class="desc_text"><?php echo ASG_AFFECT_CHANGE ?> :</label>
            </td>
            <td>
                 <select style="width:170px"  id="drpQChange" name="drpQChange">
                     <?php echo $qchange_options ?>
                </select>
            </td>
        </tr>

    <tr id="drpTr5">
            <td class="text_desc">
                <label class="desc_text"><?php echo ASG_SEND_RESULTS ?> :</label>
            </td>
            <td>
                 <select style="width:170px"  id="drpSendRes" name="drpSendRes">
                     <?php echo $sending_options ?>
                </select>
            </td>
        </tr>

    <tr >
            <td class="text_desc">
                <label class="desc_text"><?php echo ENABLE_FOR_NEW ?> :</label>
            </td>
            <td>
                 <select style="width:170px"  id="drpNewUsers" name="drpNewUsers">
                     <?php echo $enablenew_options ?>
                </select>
            </td>
        </tr>

        <tr id="drpTr2">
            <td class="text_desc">
                <label class="desc_text"><?php echo SUCCESS_POINT_PERC ?> :</label>
            </td>
            <td>
                 <input style="width:50px" type="text" name="txtSuccessP" id="txtSuccessP" value="<?php echo util::GetData("txtSuccessP") ?>"  />
            </td>
        </tr>
        
        <tr id="drpTr3">
            <td class="text_desc">
                <label class="desc_text"><?php echo TEST_TIME ?> :</label>
            </td>
            <td>
                 <input style="width:50px"  type="text" name="txtTestTime" id="txtTestTime" value="<?php echo util::GetData("txtTestTime") ?>"  />
            </td>
        </tr>
		<tr id="drpTr3">
            <td class="text_desc">
                <label class="desc_text"> Select Test Date :</label>
            </td>
            <td>
                <input value="<?php echo @$createtestdate; ?>" name="checkIn" style="width:80px" type="text" class="name_input" id="f_date1"    value=""/>
				 <script type="text/javascript">//<![CDATA[
	
	
      Calendar.setup({
        inputField : "f_date1",
        trigger    : "f_date1",
        onSelect   : function() { this.hide();
		select_f_date1(); },
        showTime   : "%I:%M %p",
        dateFormat : "%Y-%m-%d ",
		min:new Date(),
		               
		
      });	 
	  
	  
	  
    //]]></script>
				
            </td>
        </tr>
        
    </table>
          </td>
          <td>&nbsp;</td>
            <td valign="top"> <div id="divx2" style="overflow : auto; height : 480px; width:400px; ">
                    <div id="divQstBank" ><?php echo $divQstBank; ?></div>
                </div>
            </td>
        </tr>
    </table>
<label class="drop"><b>Please click continue to assign the quiz to the appropriate users.
</b> </label><br />

<font color="#FF0000" style="background:#dfdfdf;  margin-left:309px; padding:5px; text-align:center;"  size="+3"><b onClick="hid_continue()">Continue</b></font>
    <br>
    <hr /></div>
    <div id="visible_id">
    <table width="500">
        <tr>
            <td colspan="2">
                <input id="btnLcl" type="button" onClick="ShowUsers('local')" value="<?php echo CLASS_ROOMS ?>" style="border:0;width:550px;color:red" />&nbsp;<!--<input id="btnImp" type="button" onClick="ShowUsers('imported')" value="<?php //echo IMPORTED_USERS ?>" style="border:0;width:200px" />-->
            </td>            
        </tr>
        <tr><td></td></tr>
        <tr>
            <td valign="top" id="tdLocalUsers">

                 <div id="div_grid" style="overflow : auto; height : 450px; ">
<!------------------------------------------------>
<SCRIPT language="javascript">
$(function(){
 
    // add multiple select / deselect functionality
    $("#selectall").click(function () {
          $('.good_imp').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".good_imp").click(function(){
 
        if($(".good_imp").length == $(".good_imp:checked").length) {
            $("#selectall").attr("checked", "checked");
        } else {
            $("#selectall").removeAttr("checked");
        }
 
    });
});
</SCRIPT>
<?php
$db = new db();
		 //$sql_grade='SELECT * FROM  grades INNER JOIN class_rooms ON grades.grade_id=class_rooms.grade_id ';
		 //$class_rows = db::GetResultsAsArray($sql_grade);
		 
		//echo 	$this->session->userdata('district_id');
		$district_id =  $_SESSION['district_id'];
		$sql='select * from schools where district_id="'.$district_id.'"'; 
        $grades_rows = db::GetResultsAsArray($sql);
		$var='';
		foreach($grades_rows as $key_dist=>$val_dist)
		{
			$var.=$val_dist['school_id'].',';
		}
	    $var_value = rtrim($var, ",");
		
		
        $sql='select * from class_rooms where school_id IN ('.$var_value.')';//exit;
        $grades_rows = db::GetResultsAsArray($sql);
?>
<table width="550" border="1" height="100" style="text-align:center">
  <tr>
    <td><input type="checkbox" style="margin-left:1px" id="selectall"></td>
    <td>Class Room</td>
    <td>Grade</td>
    <td>School</td>
  </tr>
  <tr><?php if(count($grades_rows)>0){foreach($grades_rows as $key=>$val){?>
    <td><input type="checkbox" style="margin-left:1px"class="good_imp" name="good_imp[]" value="<?php echo $grades_rows[$key]['class_room_id']; ?>" multiple id="<?php echo $grades_rows[$key]['class_room_id']; ?>"></td>
    <td><?php echo $grades_rows[$key]['name']; ?></td>
    <td><?php $id=$grades_rows[$key]['grade_id'];
	$grade='select * from  dist_grades where dist_grade_id="'.$id.'"';
	$std_rows = db::GetResultsAsArray($grade);
	 ?><?php if(count($std_rows)>0){foreach($std_rows as $key1=>$val){?><?php echo $std_rows[$key1]['grade_name']; ?></td>
    <td><?php $s_id=$grades_rows[$key]['school_id'];
	$school='select * from schools where school_id="'.$s_id.'"';
	$school_rows = db::GetResultsAsArray($school);?>
    <?php if(count($school_rows)>0){foreach($school_rows as $key2=>$val){?><?php echo $school_rows[$key2]['school_name']; ?>
	</td>
  </tr>
  <?php }}else{echo 'Sorry no records found.';}?><?php }}else{echo 'Sorry no records found.';}?><?php }}else{echo 'Sorry no records found.';}?>  
</table>
<!------------------------------------------------>				 
				 <?php //echo $grid_html  ?></div>

            </td>
            <td valign="top" id="tdImportedUsers" style="display:none">
                    <div id="div_grid_imp" style="overflow : auto; height : 380px; "><?php //echo $imported_grid_html ?></div>
            </td>
        </tr>
    </table>
    <br>
    <hr>
    <table>
        <tr>
            <td><input class="btn" onClick="return CheckForm()" style="width:100px" type="submit" id="btnSave" name="btnSave" value="<?php echo SAVE ?>" /></td>
            <td><input class="btn" onClick="javascript:window.location.href='index.php?module=assignments'" style="width:100px" type="button" id="btnCancel" name="btnCancel" value="<?php echo CANCEL ?>" /></td>
        </tr>
    </table>
</form>

 
<script language="javascript">
    ChangeCat();
    ShowOptions();    
</script>

   <table id="test_div" style="display: none;background-color:#F9DD93"  width="610px">
        <tr>
            <td colspan=2 align=right><a href="#" border="0" onClick="close_window()"><img src="style/i/close_button.gif" /></a></td>
        </tr>
        <tr>
            <td id="test_hr"  >

            </td>
        </tr>
    </table>    
     <div id="templateDiv" style="display: none;background-color:#F9DD93">
        <table width="610px" bgcolor="#767F86" align="center" border="0">
            <tr>
                <td align="center">
                    <font color="white" face=tahoma size="3"><b><?php echo PLEASE_WAIT ?></b></font>
                </td>
            </tr>
        </table>
    </div></div>
    </body>