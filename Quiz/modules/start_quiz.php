<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("2");

 require "grid.php";
 require "db/questions_db.php";
 require "db/asg_db.php";
 include "lib/libmail.php";
 require "lib/cmail.php";
 require "qst_viewer.php";

 $app_display = "";
 $msg_display = "none";
 $timer_display ="";
 $msg_text = "";
 $qst_html ="";
 $timer_script = "";
 $timer_display = "none";
 $pager_html="";
 $pager_display="";
 $dvCont_display = "";
 $emulate_goto=true;
 while($emulate_goto==true) { // emulating goto operator 

 $user_id = $_SESSION['user_id'];
 $asg_id = util::GetID("?module=active_assignments");
 $ID = $asg_id;
 $_SESSION['asg_id']=$asg_id;

 $active_asg = asgDB::GetActAsgByUserID($user_id, $asg_id);
 $asg_num = db::num_rows($active_asg);
 if($asg_num==0)
 {
     DisplayMsg("error",QUIZ_NO_ACCESS,false);
     break;
 }

 $asg_row = db::fetch($active_asg);



 $status = intval($asg_row['user_quiz_status']);
  $user_quiz_id = $asg_row['user_quiz_id'];
 $allow_review = $asg_row['allow_review'];
 $send_results = $asg_row['send_results'];
 $qst_order = $asg_row['qst_order'];
 $per_page_question = $asg_row['per_page_question'];
 $_SESSION['user_quiz_id']=$user_quiz_id;
 $quiz_type = $asg_row['quiz_type'];
 
 
 
 if($status==0)
 {
    $date = util::Now();
	
	//add "date_year"=>date('Y') 
	
     $user_quiz_id=db::exec_insert(orm::GetInsertQuery("user_quizzes", array("assignment_id"=>$asg_id,
                                                            "user_id"=>$user_id,
                                                            "status"=>"1",
                                                            "added_date"=>$date,
															"date_year"=>date('Y'),
                                                            "success"=>"0"
                                                       )));     
 }
 else if($status>=2)
 {     
    DisplayMsg("error",ALREADY_FINISHED,false);
    break;
 }

 if($quiz_type=="1") // if quiz
 {
    $timer_display= "";
    $ended=ShowTimer($status,$asg_row);
    if($ended==true) break;
 }

 $page = "?module=start_quiz&id=".$asg_id ;
 $qst_viewer = new qst_viewer($page);
 $qst_viewer->user_quiz_id=$user_quiz_id;
 $priority = $qst_viewer->GetPriority(); 

if(isset($_POST['save_and_next']))
 {
     UpdateValues();
      
 }

  if(isset($_POST['finished']))
 { 
 	
	UpdateValues();
	
 }
 if(isset($_POST['btnPrev']))
 {
    $priority =$qst_viewer->GetPrevPriority();
 }

 if(isset($_POST['load_question']))
 {
    $priority = $_POST['load_priority'];    
 }

 $qst_query = questions_db::GetQuestionsByPriority($priority, $asg_id, $user_id , $asg_row['qst_order'],$asg_row['quiz_id']);
 $row_qst = db::fetch(db::exec_sql($qst_query));


 //my code  start 
  
 $no_que_query = "select quiz_id,per_page_question from assignments where id =".$asg_id;   
 $row = db::fetch(db::exec_sql($no_que_query));
 $per_page_question = $row['per_page_question'];
 $quiz_id = $row['quiz_id'];
     
 $strTotalQue = "select count(0) as cnt from questions where quiz_id=".$quiz_id." order by priority ";
    
 $rsTotal = db::GetResultsAsArray($strTotalQue);
 $intTotalRecords = $rsTotal[0]['cnt'];
 
	 //for pagination
		if($_REQUEST['pageId']=="")
		{
			$pageId = 1 ;
		}
		else
		{
			$pageId = $_REQUEST['pageId'];
		}
		
		if ($pageId < 1) $pageId = 1;
		$resultsPerPage = $per_page_question;     
		//$resultsPerPage  = 5;
		$startResults = ($pageId - 1) * $resultsPerPage;
		$numberOfRows = $intTotalRecords;
		$totalPages = ceil($numberOfRows / $resultsPerPage);
		$pagelimit="LIMIT ".$startResults.",".$resultsPerPage;
	//end of pagination process
 if($qst_order == 1	)
 { 
    $sql_Questions = "select q.*, qt.question_type, qz.quiz_name from questions q left join quizzes qz on qz.id=q.quiz_id left join question_types qt on q.question_type_id=qt.id where q.quiz_id=".$quiz_id." order by priority ".$pagelimit;
 }
 else if($qst_order == 2)
 {
 	if($_REQUEST['pageId']=="")
	{
 		$orderType = array(0=>"added_date Asc",1=>"added_date Desc",2=>"priority Desc",3=>"point Asc",4=>"point Desc",5=>"priority Asc",
                        6 =>"question_type_id Asc");
		
		$i = rand()&7;
		
		$_SESSION['ordertype'] = $orderType[$i];
	}
	$orderBy = $_SESSION['ordertype'];
	if (isset($_SESSION['ordertype']) && $_SESSION['ordertype'] !='')
	{
	 $sql_Questions = "select q.*, qt.question_type, qz.quiz_name from questions q left join quizzes qz on qz.id=q.quiz_id left join question_types qt on q.question_type_id=qt.id where q.quiz_id=".$quiz_id." order by ".$orderBy."  ".$pagelimit;	
	 }
	 else
	 {
	 	$sql_Questions = "select q.*, qt.question_type, qz.quiz_name from questions q left join quizzes qz on qz.id=q.quiz_id left join question_types qt on q.question_type_id=qt.id where q.quiz_id=".$quiz_id." order by priority ".$pagelimit;
		}
 }
 
$rsQuestions = db::GetResultsAsArray($sql_Questions); 
    
  //select * from question_master where qu_id not in(select que_id from anser_master where
//qz_id=19 and user_id=5) where quez_id=19 order by priority asc
 
  
 if($priority==1)
 {
     $qst_viewer->show_prev=false;
 }
 else if($row_qst['next_priority']==-1)
 {
     $qst_viewer->show_next=false;
     $qst_viewer->show_finish=true;
 }
 
 
 //echo $qst_query; 
 
 $qst_viewer->ans_priority=$asg_row['answer_order'];
 
 $qst_viewer->BuildQuestionWithResultset($rsQuestions,$totalPages,$asg_id,$quiz_type); //rsQuestions
 /*if($_REQUEST['pageId']!='')
 { 
 $qst_html = $qst_viewer->html;
}
else
{*/
	$qst_html = $qst_viewer->html;
//}
 $ids=$qst_viewer->GetIDS();
 

  $row_num =  count($rsQuestions);

  if($row_num==0)
 {
     DisplayMsg("error",QUIZ_NO_ACCESS,false);
 }
 
// $pager_html = GetPager();

 if(isset($_POST['data_post']))
 {
    echo $qst_html."[{sep}]".$pager_html."[{sep}]".$ids;
 }

 $emulate_goto =false;

 }


 function UpdateValues()
 {
    global $user_quiz_id;
    
    $db = new db();
    $db->connect();
    $db->begin();
//print_r($_POST);
    try
    {
	 $arrQuestions = $_POST['que_id'];
	 foreach($arrQuestions as $key=>$intQuestionId)
	 {
		$db->query(orm::GetDeleteQuery("user_answers", array("user_quiz_id"=>$user_quiz_id , "is_score_ans"=>0, "question_id"=>intval($intQuestionId))));
		$db->query(orm::GetDeleteQuery("user_question_note", array("user_quiz_id"=>$user_quiz_id , "user_id"=>$_SESSION['user_id'], "question_id"=>intval($intQuestionId))));
		
		if($_POST['txtnote_'.$intQuestionId])
		{
			$query = orm::GetInsertQuery("user_question_note", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"user_id"=>$_SESSION['user_id'],
                                                             "user_note_text"=>trim($_POST['txtnote_'.$intQuestionId])
                                                              ));
            $db->query($query);  
		}
     $date = date('Y-m-d H:i:s');
 
    switch ($_POST['que_type_'.$intQuestionId]) {

         case 0 : // if checkbox
             //$chks = explode(";|",$_POST['post_data']);
			 $chks = $_POST['chkAns_'.$intQuestionId];			
             for($i=0;$i<sizeof($chks);$i++)
             {
                 $chk_value=trim($chks[$i]);
                 if($chk_value=="") continue;

                 $chk_value = intval($chk_value);
                 $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>0,
                                                                    "answer_id"=>$chk_value,
                                                                    "user_answer_id"=>$chk_value,
                                                                    "added_date"=>$date
                                                              ));
                 $db->query($query);
             }
			 
         break;
         case 1 : //if radio button
                  $chk_value=trim($_POST['rdAns_'.$intQuestionId]);  
                 if($chk_value!="")
                 {
                    $chk_value = intval($chk_value);
                    $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>0,
                                                                    "answer_id"=>$chk_value,
                                                                    "user_answer_id"=>$chk_value,
                                                                    "added_date"=>$date
                                                             ));
                    $db->query($query);
                 }
         break ;
         case 3 : // if free text area
                
                $answer_id= $_POST['txtFreeId_'.$intQuestionId];
                $answer_text=$_POST['txtFree_'.$intQuestionId];  
                 //if($chk_value!="")
                 //{                    
                    $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>0,
                                                                    "answer_id"=>$answer_id,
                                                                    "user_answer_text"=>$answer_text,
                                                                    "added_date"=>$date
                                                             ));
                    $db->query($query);
                // }
        break ;
        case 4 : // if muti text
            
			 
			$txtvals = $_POST['txtMultiAns_'.$intQuestionId];
			 $txts = $_POST['txtMultiAnsId_'.$intQuestionId];
			
             for($i=0;$i<sizeof($txts);$i++)
             {
                 $txt_key=$txts[$i];                 
                 if($txt_key=="") continue;

               	$txt_value = $txtvals[$i];
                 
                 if(trim($txt_key)=="" || trim($txt_value)=="") continue ;

                 $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>0,
                                                                    "answer_id"=>$txt_key,
                                                                    "user_answer_text"=>$txt_value,
                                                                    "added_date"=>$date
                                                              ));
                 $db->query($query);
             }
         
         break;
		 
		  case 5 : 
		  	
			    $chk_value=trim($_POST['rdAnsMulti_'.$intQuestionId]);  
                 if($chk_value!="")
                 {
                    $chk_value = intval($chk_value);
                    $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>0,
                                                                    "answer_id"=>$chk_value,
                                                                    "user_answer_id"=>$chk_value,
                                                                    "added_date"=>$date
                                                             ));
                    $db->query($query);
                 }
			 
         break;
			case 6 : 		// if checkbox/radio combo
      			// if checkbox/
				 $chk_value = $_POST['chkRadioAns_'.$intQuestionId];
				 $self_c_vowel = $_POST['self_c_vowel_'.$intQuestionId];			
			 	 if($chk_value!="")
				 {
				  	$chk_value = intval($chk_value);
					$cant_decode = 0;
					if(isset($_POST['cant_decode_'.$intQuestionId]) && $_POST['cant_decode_'.$intQuestionId] !='')
					{
						$cant_decode_value = intval($_POST['cant_decode_'.$intQuestionId]);
						if($chk_value == $cant_decode_value)
						$cant_decode = 1;
						 
					}
					$ansId =''; $with_diff = 0;
					$ans_query =  "select id from answers_quiz where radio_ans = 2 AND group_id in (select id from question_groups where question_id = ".$intQuestionId." AND is_secondary_group = 0) "; 
					 $ans = db::GetResultsAsArray($ans_query);
					 $ansId = $ans[0]['id'];
					if($chk_value == $ansId){
						$with_diff = 1;
					 }
					
					 	$query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
																		"question_id"=>intval($intQuestionId),
																		"is_score_ans"=>0,
																		"answer_id"=>$chk_value,
																		"user_answer_id"=>$chk_value,
																		"added_date"=>$date,
																		"cant_decode"=>$cant_decode,
																		"self_c_vowel"=>$self_c_vowel,
																		"with_difficulty"=>$with_diff
																  ));
				 	 $db->query($query);
					  
				 }
				
                  for($i=1;$i<=9;$i++)
				  {
				 	//
					 $radioChkAns = "radioChkAns_".$intQuestionId."_".$i;
					  if(!isset($_POST[$radioChkAns])) continue;
					  $radioChkAnsId = "radioChkAnsId_".$intQuestionId."_".$i;
					 
					$chk_value=trim($_POST[$radioChkAns]);
					 if($chk_value!="")
					 {
						$chk_value_id = trim($_POST[$radioChkAnsId]);
					 	$query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
																		"question_id"=>intval($intQuestionId),
																		"is_score_ans"=>0,
																		"answer_id"=>$chk_value_id,
																		"user_answer_id"=>$chk_value_id,
																		"user_answer_text"=>$chk_value,
																		"added_date"=>$date
																 ));
						$db->query($query);
					 }
				}
        	 break ;
 
     } //swich end


     	$db->commit();
		
	 	$db->query(orm::GetDeleteQuery("user_answers", array("user_quiz_id"=>$user_quiz_id ,"is_score_ans"=>1, "question_id"=>intval($intQuestionId))));
 
     $date = date('Y-m-d H:i:s');
     switch ($_POST['sec_que_type_'.$intQuestionId]) {

         case 0 : // if checkbox
             //$chks = explode(";|",$_POST['post_data']);
			 $chks = $_POST['chkAnsSec_'.$intQuestionId];			
             for($i=0;$i<sizeof($chks);$i++)
             {
                 $chk_value=trim($chks[$i]);
                 if($chk_value=="") continue;

                 $chk_value = intval($chk_value);
                 $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>1,
                                                                    "answer_id"=>$chk_value,
                                                                    "user_answer_id"=>$chk_value,
                                                                    "added_date"=>$date
                                                              ));
                 $db->query($query);
     }

         break;
         case 1 : //if radio button
                  $chk_value=trim($_POST['rdAnsSec_'.$intQuestionId]);  
                 if($chk_value!="")
                 {
                    $chk_value = intval($chk_value);
                    $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>1,
                                                                    "answer_id"=>$chk_value,
                                                                    "user_answer_id"=>$chk_value,
                                                                    "added_date"=>$date
                                                             ));
                    $db->query($query);
                 }
         break ;
         case 3 : // if free text area
                
                $answer_id= $_POST['txtFreeIdSec_'.$intQuestionId];
                $answer_text=$_POST['txtFreeSec_'.$intQuestionId];  
                 //if($chk_value!="")
                 //{                    
                    $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>1,
                                                                    "answer_id"=>$answer_id,
                                                                    "user_answer_text"=>$answer_text,
                                                                    "added_date"=>$date
                                                             ));
                    $db->query($query);
                // }
        break ;
        case 4 : // if muti text
            
			 
			$txtvals = $_POST['txtMultiAnsSec_'.$intQuestionId];
			 $txts = $_POST['txtMultiAnsIdSec_'.$intQuestionId];
			// print_r($txtvals);
			//print_r($txts);
			
             for($i=0;$i<sizeof($txts);$i++)
             {
                 $txt_key=$txts[$i];                 
                 if($txt_key=="") continue;

               	$txt_value = $txtvals[$i];
                 
                 if(trim($txt_key)=="" || trim($txt_value)=="") continue ;

                 $query = orm::GetInsertQuery("user_answers", array("user_quiz_id"=>$user_quiz_id,
                                                                    "question_id"=>intval($intQuestionId),
																	"is_score_ans"=>1,
                                                                    "answer_id"=>$txt_key,
                                                                    "user_answer_text"=>$txt_value,
                                                                    "added_date"=>$date
                                                              ));
                 $db->query($query);
             }
         
         break;

     } //swich end

     $db->commit();

	 
	 }	 // for each end
	  
	  		//code start for store user answer -1- 0  value  
	  
	  $page = $_REQUEST['pageId'] - 1;
	  $Pageques = $_POST['que_id'];
	  $IntPageques = count($Pageques);
	 for($j = 0; $j<$IntPageques;$j++)
     {                  
	  	$que = $Pageques[$j];
		if($_POST["que_type_".$que]  == 0) // for multi answer checkbox
		{
			 $userAnsIdArray  = $_POST["chkAns_".$que];
			 $IntuserAnsIdArray = count($userAnsIdArray);
			 //"select aq.id from answers_quiz aq left join  question_groups qg on aq.group_id = qg.id left  ";
			$ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
			 $ans = db::GetResultsAsArray($ans_query);
			$ansIdArray = array();
			$IntAnsIdArray = count($ans);
			for($i=0;$i<$IntAnsIdArray;$i++)
			 {
	   		 	$ansIdArray[$i] = $ans[$i]['id'];
			 }
			 
			 if($IntuserAnsIdArray == $IntAnsIdArray) 
			 {
			 	$equl = 0;
			 	for($i=0;$i<$IntuserAnsIdArray;$i++)
			 	{
					if(in_array($userAnsIdArray[$i],$ansIdArray))
					{
						$equl = $equl + 1;
					}
				}
		 	 }
			  $is_right_ans = 0;
			  if($equl ==$IntuserAnsIdArray && $equl!=0)
			  {
			 	$is_right_ans = 1;
			  }
			  $q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
	 	 }
		
		 elseif($_POST["que_type_".$que] == 1)  // for radio button
		 {
			   $userAns  = $_POST["rdAns_".$que];
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
			    $ans = db::GetResultsAsArray($ans_query);
				$ansRadio = $ans[0]['id'];
				 $is_right_ans = 0;
				if($userAns == $ansRadio)
				{
			 		$is_right_ans = 1;
				 } 	
			$q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit(); 
		  }	
		 elseif($_POST["que_type_".$que] == 3) // free text
		 {		
		 		$ans_query =  "select correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
				$ansTextarray = db::GetResultsAsArray($ans_query);
			 	$ansText = $ansTextarray[0]['correct_answer_text'];
				$is_right_ans = 0;
				 $ans1 = strcasecmp($_POST["txtFree_".$que],$ansText);
				 if($ans1 == 0)
				 {
					 $is_right_ans = 1;
				}
			$q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
			 
		 }
		 
		  elseif($_POST["que_type_".$que] == 4) //for multi text
		 {
		 	   $userAnsTextArray  = $_POST["txtMultiAns_".$que];
				 $IntuserAnsIdArray = count($userAnsTextArray);
				$ans_query =  "select id,correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) order by id"; 
				$ans = db::GetResultsAsArray($ans_query);
				$ansTextArray = array();
			  	$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansTextArray[$i] = $ans[$i]['correct_answer_text'];
				 }
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$equl = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if( trim($userAnsTextArray[$i]) == trim($ansTextArray[$i]))
						{
							$equl = $equl + 1;
						}
					}
				 }
				 $is_right_ans = 0;
				  if($equl == $IntuserAnsIdArray && $equl!=0)
				   {
					 $is_right_ans = 1;
					}
			$q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
		 }
		 
		  elseif($_POST["que_type_".$que] == 5)  // for radio-check button
		 {
			   $userAns  = $_POST["rdAnsMulti_".$que];
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
			    $ans = db::GetResultsAsArray($ans_query);
				$ansIdArray = array();
				$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansIdArray[$i] = $ans[$i]['id'];
				 }
				 $is_right_ans = 0;
				if(in_array($userAns,$ansIdArray))
				{
			 		$is_right_ans = 1;
				 }
			$q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
		  }	
		 
		 
		 elseif($_POST["que_type_".$que]  == 6) // for checkbox/radio combo
		{
			 $userAns  = $_POST["chkRadioAns_".$que];
			
			$ans_query =  "select id from answers_quiz where correct_answer = 1 AND radio_ans = 0 AND group_id in (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
		 	 $ans = db::GetResultsAsArray($ans_query);
			$ansIdArray = array();
			$IntAnsIdArray = count($ans);
			for($i=0;$i<$IntAnsIdArray;$i++)
			 {
	   		 	$ansIdArray[$i] = $ans[$i]['id'];
			 }
			  $is_right_ans = 0;
			
			if(in_array($userAns,$ansIdArray))
			{
				$is_right_ans = 1;
			}
		
				$q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id));
				
        			 db::exec_sql($q);
					  $db->commit();
		   			 //if radio button
			
			 $intQuestionId = $Pageques[$j];
			 for($i=1;$i<=9;$i++)
			 {
			 	$radioChkAns = '';
				 $radioChkAns = "radioChkAns_".$intQuestionId."_".$i;  
				  if(!isset($_POST[$radioChkAns])) continue;
				    $radioChkAnsId = "radioChkAnsId_".$intQuestionId."_".$i;
				 
				$userAns=trim($_POST[$radioChkAns]);
				 if($userAns!="")
				 {
					$chk_value_id = trim($_POST[$radioChkAnsId]);
	 			    $ans_query =  "select correct_answer from answers_quiz where radio_ans = 1 AND id=".$chk_value_id;
					$ans = db::GetResultsAsArray($ans_query);
					$correct_answer = $ans[0]['correct_answer'];
					$radio_ans = 0;
					if($userAns == $correct_answer)
					{
						$radio_ans = 1;
					 } 	
					 $q=orm::GetUpdateQuery("user_answers", array("is_radio_ans_correct"=>$radio_ans),array("answer_id"=>$chk_value_id,"is_score_ans"=>0,"user_quiz_id"=>$user_quiz_id,"question_id"=>$que));
				
        			 db::exec_sql($q);
					  $db->commit();
		 	 	 }
				 
	 		 }
 		  }	
		 
		  
		       //// secondary answer 
			   
		if($_POST["sec_que_type_".$que]  == 0) // for multi answer checkbox
		{
			 $userAnsIdArray  = $_POST["chkAnsSec_".$que];
			 $IntuserAnsIdArray = count($userAnsIdArray);
			 //"select aq.id from answers_quiz aq left join  question_groups qg on aq.group_id = qg.id left  ";
			$ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1) "; 
			 $ans = db::GetResultsAsArray($ans_query);
			$ansIdArray = array();
			$IntAnsIdArray = count($ans);
			for($i=0;$i<$IntAnsIdArray;$i++)
			 {
	   		 	$ansIdArray[$i] = $ans[$i]['id'];
			 }
			 
			 if($IntuserAnsIdArray == $IntAnsIdArray) 
			 {
			 	$equl = 0;
			 	for($i=0;$i<$IntuserAnsIdArray;$i++)
			 	{
					if(in_array($userAnsIdArray[$i],$ansIdArray))
					{
						$equl = $equl + 1;
					}
				}
		 	 }
			  $is_right_ans = 0;
			  if($equl ==$IntuserAnsIdArray && $equl!=0)
				{
				 	$is_right_ans = 1;
			 	}
			  $q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>1,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
	 	 }
		
		 elseif($_POST["sec_que_type_".$que] == 1)  // for radio button
		 {
			   $userAns  = $_POST["rdAnsSec_".$que];
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1)";
			    $ans = db::GetResultsAsArray($ans_query);
				$ansRadio = $ans[0]['id'];
				 $is_right_ans = 0;
				if($userAns == $ansRadio)
				{
			 		$is_right_ans = 1;
				 } 	
				  $q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>1,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
		  }	
		 elseif($_POST["sec_que_type_".$que] == 3) // free text
		 {		
		 		$ans_query =  "select correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1)";
				$ansTextarray = db::GetResultsAsArray($ans_query);
			 	$ansText = $ansTextarray[0]['correct_answer_text'];
				$is_right_ans = 0;
				 $ans1 = strcasecmp($_POST["txtFreeSec_".$que],$ansText);
				 if($ans1 == 0)
				 {
					 $is_right_ans = 1;
				}
			 $q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>1,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
			 
		 }
		 
		  elseif($_POST["sec_que_type_".$que] == 4) //for multi text
		 {
		 	   $userAnsTextArray  = $_POST["txtMultiAnsSec_".$que];
				 $IntuserAnsIdArray = count($userAnsTextArray);
				$ans_query =  "select id,correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 1) order by id"; 
				$ans = db::GetResultsAsArray($ans_query);
				$ansTextArray = array();
			  	$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansTextArray[$i] = $ans[$i]['correct_answer_text'];
				 }
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$equl = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if( trim($userAnsTextArray[$i]) == trim($ansTextArray[$i]))
						{
							$equl = $equl + 1;
						}
					}
				 }
				 $is_right_ans = 0;
			  if($equl == $IntuserAnsIdArray && $equl!=0)
			   {
				 $is_right_ans = 1;
				}
			 $q=orm::GetUpdateQuery("user_answers", array("is_correct_ans"=>$is_right_ans),array("question_id"=>$que,"is_score_ans"=>1,"user_quiz_id"=>$user_quiz_id));
        		 db::exec_sql($q);
				  $db->commit();
		 }
		
	   }
	 	   //code end for store user answer value 0-1
  	   
		 //check stop parameter       hitesh patel
	 if($_POST['finished'] == "Finish")
	 {	
	 	
	 }
	else if($_POST['quiz_type']=="3")  
	{   
		$page = $_REQUEST['pageId'] - 1;
	 $query_stop = "select stop_parameter from quiz_stop_parameter where ans_type IS NULL AND assignment_id  = ".$_SESSION['asg_id']." and page_no = ".$page ;
		 $stop_para = db::GetResultsAsArray($query_stop);
		  $stop_parameter= $stop_para[0]['stop_parameter'];
		 $Pageques = $_POST['que_id'];
		  $IntPageques = count($Pageques);
		  $IntTrueAns = 0;
	 for($j = 0; $j<$IntPageques;$j++)
	 {
	  	$que = $Pageques[$j];
		
		if($_POST["que_type_".$que]  == 0) // for multi answer checkbox
		{
			 $userAnsIdArray  = $_POST["chkAns_".$que];
			 $IntuserAnsIdArray = count($userAnsIdArray);
			 //"select aq.id from answers_quiz aq left join  question_groups qg on aq.group_id = qg.id left  ";
			$ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
			 $ans = db::GetResultsAsArray($ans_query);
			$ansIdArray = array();
			$IntAnsIdArray = count($ans);
			for($i=0;$i<$IntAnsIdArray;$i++)
			 {
	   		 	$ansIdArray[$i] = $ans[$i]['id'];
			 }
			 
			 if($IntuserAnsIdArray == $IntAnsIdArray) 
			 {
			 	$equl = 0;
			 	for($i=0;$i<$IntuserAnsIdArray;$i++)
			 	{
					if(in_array($userAnsIdArray[$i],$ansIdArray))
					{
						$equl = $equl + 1;
					}
				}
		 	 }
			if($equl ==$IntuserAnsIdArray && $equl!=0)
			 $IntTrueAns =$IntTrueAns + 1;    
	 	 }
		
		 elseif($_POST["que_type_".$que] == 1)  // for radio button
		 {
			   $userAns  = $_POST["rdAns_".$que];
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
			    $ans = db::GetResultsAsArray($ans_query);
				$ansRadio = $ans[0]['id'];
				if($userAns == $ansRadio){
				 	$IntTrueAns =$IntTrueAns + 1; }
		  }	
		 elseif($_POST["que_type_".$que] == 3)
		 {
		 	  if($_POST["txtFree_".$que] != '')
			  	$IntTrueAns = $IntTrueAns + 1;
		 }
		 
		  elseif($_POST["que_type_".$que] == 4) //for multi text
		 {
		 	   $userAnsTextArray  = $_POST["txtMultiAns_".$que];
				 $IntuserAnsIdArray = count($userAnsTextArray);
			 //"select aq.id from answers_quiz aq left join  question_groups qg on aq.group_id = qg.id left  ";
				$ans_query =  "select id,correct_answer_text from answers_quiz where group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) order by id"; 
				$ans = db::GetResultsAsArray($ans_query);
				$ansTextArray = array();
			  	$IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansTextArray[$i] = $ans[$i]['correct_answer_text'];
				 }
				 if($IntuserAnsIdArray == $IntAnsIdArray) 
				 {
					$equl = 0;
					for($i=0;$i<$IntuserAnsIdArray;$i++)
					{
						if( trim($userAnsTextArray[$i]) == trim($ansTextArray[$i]))
						{
							$equl = $equl + 1;
						}
					}
				 }
				  if($equl == $IntuserAnsIdArray && $equl!=0)
				 $IntTrueAns =$IntTrueAns + 1;
		 }
			
		 elseif($_POST["que_type_".$que] == 5)  // 
		 {
			   $userAns  = $_POST["rdAnsMulti_".$que];
			   $ans_query =  "select id from answers_quiz where correct_answer = 1 AND group_id = (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0)";
			   $ans = db::GetResultsAsArray($ans_query);
				 $IntAnsIdArray = count($ans);
				for($i=0;$i<$IntAnsIdArray;$i++)
				 {
					$ansIdArray[$i] = $ans[$i]['id'];
				 }
				 $is_right_ans = 0;
				if(in_array($userAns,$ansIdArray))
				{
				 	$IntTrueAns =$IntTrueAns + 1;
				}
		  }
		  
	/*	  elseif($_POST["que_type_".$que] == 6)  // 
		 {
			$selfcvowel  = $_POST["self_c_vowel_".$que];
			if($selfcvowel == 1)    
			{		
				echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>";  
				global $fdate,$send_results; 
				 $fdate = util::Now();
				$row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
				 $msg = GetAssessmentResults($row);
				 DisplayMsg("warning",$msg,true); 
				 break;
			} 

		 } */
	  } //for
		$QuizStop = 0;
		if($stop_parameter > $IntTrueAns)
		{		
				$QuizStop = 1;
				echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>";   
	 			 global $fdate,$send_results;
				  $fdate = util::Now();
				  //$row=asgDB::UpdateUserAssessment($user_quiz_id,1,$fdate);
				  $row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
				  $msg = GetAssessmentResults($row);
				  DisplayMsg("warning",$msg,true);
			  //if($send_results == "1") SendMail($row);  
		} 	
		$QuizStopscv  = 0;
		if($QuizStop == 0)
		{
		  if($_POST["que_type_".$que] == 6)  // 
		  {
			 $query_stop_s = "select * from quiz_stop_parameter where ans_type ='s1' AND assignment_id  = ".$_SESSION['asg_id']." and page_no = ".$page ;
			$stop_para_s = db::GetResultsAsArray($query_stop_s);
 			$countScp = count($stop_para_s);
			$stop_para_scv1  = $stop_para_s[0]['stop_parameter'];
		 	//$stop_para_scv2  = $stop_para_s[1]['stop_parameter'];
			 
			$scv1idarray = array(); 
			$scv2idarray = array(); 
			$scv1idarray = $_POST["scv_1"];
			$scv2idarray = $_POST["scv_2"];	
			$scv1cntyes = 0; $scv2cntyes=0;
			
			$scvstr = '';
			if(!empty($scv1idarray))
			{
				$scvidarray = $scv1idarray;
			}
			if(!empty($scv2idarray))
			{
				$scvidarray = $scv2idarray;
			}
			if(!empty($scv1idarray) && !empty($scv2idarray))
			{
				$scvidarray = array_merge($scv1idarray,$scv2idarray);
	 		}
	 		
			if(!empty($scvidarray))
			{
				 $scvstr = implode(',',$scvidarray);
				 $scv1_query =  "select count(id) as cntyes from user_answers where self_c_vowel = 1 AND user_quiz_id=".$user_quiz_id." AND answer_id in(".$scvstr.")";
			   $scv1res = db::GetResultsAsArray($scv1_query);
			   $scv1cntyes = $scv1res[0]['cntyes'];
 			} 
			
			
		/*	if(!empty($scv1idarray))
			{
				 $scv1str = implode(',',$scv1idarray);
				 $scv1_query =  "select count(id) as cntyes from user_answers where self_c_vowel = 1 AND user_quiz_id=".$user_quiz_id." AND answer_id in(".$scv1str.")";
			   $scv1res = db::GetResultsAsArray($scv1_query);
			   $scv1cntyes = $scv1res[0]['cntyes'];
 			}  
			if(!empty($scv2idarray))
			{
				 $scv2str = implode(',',$scv2idarray);
				 $scv2_query =  "select count(id) as cntyes from user_answers where self_c_vowel = 1 AND user_quiz_id=".$user_quiz_id." AND answer_id in(".$scv2str.")";
				 $scv2res = db::GetResultsAsArray($scv2_query);
				 $scv2cntyes = $scv2res[0]['cntyes'];
			}

			if(($stop_para_scv1 <= $scv1cntyes && $stop_para_scv1 != 0)||($stop_para_scv2 <= $scv2cntyes && $stop_para_scv2 != 0))    */
			if($stop_para_scv1 <= $scv1cntyes && $stop_para_scv1 != 0)  
	 		{		
				$QuizStopscv = 1;
				echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>"; 
				global $fdate,$send_results; 
				 $fdate = util::Now();
				$row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
				 $msg = GetAssessmentResults($row);
				 DisplayMsg("warning",$msg,true); 
			} 
 	      } 
		}
 	  //end q-6
		if($QuizStop == 0 && $QuizStopscv == 0)  // for qtye 6
		{
		   $query_stop = "select * from quiz_stop_parameter where ans_type IS NOT NULL AND assignment_id  = ".$_SESSION['asg_id']." and page_no = ".$page ;
		  $stop_para = db::GetResultsAsArray($query_stop);
	 		$stop_para_difficulty ='';$stop_para_check ='';
			$countSP = count($stop_para);
			$stop_para_radio = array();
			for($spCount = 0; $spCount<=$countSP; $spCount++)
			{
				 $stop_para[$spCount]['ans_type'];
				if($stop_para[$spCount]['ans_type'] == 'c')
				{
					 $stop_para_check = intval($stop_para[$spCount]['stop_parameter']);
				}
				else if($stop_para[$spCount]['ans_type'] == 'd')
				{
					 $stop_para_difficulty = intval($stop_para[$spCount]['stop_parameter']);
				}
				
				else if(($stop_para_difficulty !='') && ($stop_para_check !=''))
				{
				  $sr ='';
				  $sr = $spCount-1;
				  $ri = 'r'.$sr;
				}
				else if(($stop_para_difficulty !='') || ($stop_para_check !=''))
				{
					$ri = 'r'.$spCount;
				}
				else if(($stop_para_difficulty =='') && ($stop_para_check ==''))
				{
					$sr ='';
					  $sr = $spCount+1;
					  $ri = 'r'.$sr;
				} 
				//echo $ri;	
				if($stop_para[$spCount]['ans_type'] == $ri)
				{ 
					$stop_para_radio[$ri] = $stop_para[$spCount]['stop_parameter'];
				}
			}
			 $IntTrueAnsCheck =0;
			
			 for($spr = 1 ; $spr<=count($stop_para_radio);$spr++)
			 {
				$IntTrueAnsRadio[$spr] = 0;
			 }
			
			  for($j = 0; $j<$IntPageques;$j++)
			  { 
				 $que = $Pageques[$j];
				 if($_POST["que_type_".$que] == 6) // for checkbox /radio combo -- for radio-check
				 {
					$userAns  = $_POST["chkRadioAns_".$que];
				
					$ans_query =  "select id from answers_quiz where correct_answer = 1 AND radio_ans = 0 AND group_id in (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
					 $ans = db::GetResultsAsArray($ans_query);
					$ansIdArray = array();
					$IntAnsIdArray = count($ans);
					for($i=0;$i<$IntAnsIdArray;$i++)
					 {
						$ansIdArray[$i] = $ans[$i]['id'];
					 }
			 		if(in_array($userAns,$ansIdArray))
					{
						$IntTrueAnsCheck = $IntTrueAnsCheck + 1;
					}
				  }// end if q 6
			  }  // end for
				$stopQuizByChk = 0; 
				if($stop_para_check > $IntTrueAnsCheck && $stop_para_check !='')
				{		
					$stopQuizByChk = 1;
					echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>";  
					 global $fdate,$send_results;
					  $fdate = util::Now();
					 // $row=asgDB::UpdateUserAssessment($user_quiz_id,1,$fdate);
					 $row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
					  $msg = GetAssessmentResults($row);
					  DisplayMsg("warning",$msg,true);
				} 
				
				// for difficult question
			 	if($stopQuizByChk == 0 )		
				{
					$IntCountDiffCheck = '';
					for($j = 0; $j<$IntPageques;$j++)
				  	{ 
						 $que = $Pageques[$j];
						 if($_POST["que_type_".$que] == 6) // for checkbox /radio combo -- for difficulty
						 {
							$userAns  = $_POST["chkRadioAns_".$que];
							$ans_query =  "select id from answers_quiz where radio_ans = 2 AND group_id in (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
							 $ans = db::GetResultsAsArray($ans_query);
							 $ansId = $ans[0]['id'];
							if($userAns == $ansId){
								$IntCountDiffCheck = $IntCountDiffCheck + 1;
							 }
						  }// end if q 6
				 	 }  // end for
				} // end if diff
				
				$stopQuizByDiff = 0; 
				if($IntCountDiffCheck >= $stop_para_difficulty && $stop_para_difficulty!='')
				{		
					$stopQuizByDiff = 1;
					echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>";  
		  			 global $fdate,$send_results;
					  $fdate = util::Now();
					 // $row=asgDB::UpdateUserAssessment($user_quiz_id,1,$fdate);
					 $row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
					  $msg = GetAssessmentResults($row);
					  DisplayMsg("warning",$msg,true);
				} 
				// for self correct vowel 
				/*
				if($stopQuizByChk == 0 && $stopQuizByDiff ==0)		
				{
					
					 $radio1Arrary = array();
					 $radio1Arrary  = $_POST["scv_1"];
					 
					for($j = 0; $j<$IntPageques;$j++)
				  	{ 
						 $que = $Pageques[$j];
						 if($_POST["que_type_".$que] == 6) 
						 {
							
							  if(!isset($_POST[$radioChkAns])) continue;
						
							   $radioChkAnsId = "radioChkAnsId_".$intQuestionId."_".$i;
							 
							 $userAns=trim($_POST[$radioChkAns]);
							 if($userAns!="")
							 {
								$chk_value_id = trim($_POST[$radioChkAnsId]);
								$ans_query =  "select correct_answer from answers_quiz where radio_ans = 1 AND id=".$chk_value_id;
							 }
							
							$ans_query =  "select id from answers_quiz where radio_ans = 2 AND group_id in (select id from question_groups where question_id = ".$que." AND is_secondary_group = 0) "; 
							 $ans = db::GetResultsAsArray($ans_query);
							 $ansId = $ans[0]['id'];
							if($userAns == $ansId){
								$IntCountDiffCheck = $IntCountDiffCheck + 1;
							 }
						  }// end if q 6
				 	 }  // end for
				} // end if diff
				
				
				
				
				$stopQuizByDiff = 0; 
				if($IntCountDiffCheck >= $stop_para_difficulty && $stop_para_difficulty!='')
				{		
					$stopQuizByDiff = 1;
					echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>";  
		  			 global $fdate,$send_results;
					  $fdate = util::Now();
					 // $row=asgDB::UpdateUserAssessment($user_quiz_id,1,$fdate);
					 $row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
					  $msg = GetAssessmentResults($row);
					  DisplayMsg("warning",$msg,true);
				} 
				//end scv*/
				
				if($stopQuizByDiff == 0 && $stopQuizByChk==0)		
				{		//if radio button  
				 	$IntTrueAnsRadio = array();
				   for($j = 0; $j<$IntPageques;$j++)
				   { 
					  $intQuestionId = $Pageques[$j];
					  if($_POST["que_type_".$que]  == 6) // for checkbox/radio combo
					  {
						 for($i=1;$i<=9;$i++)
						 { 
							 $radioChkAns = "radioChkAns_".$intQuestionId."_".$i;
							  if(!isset($_POST[$radioChkAns])) continue;
						
							   $radioChkAnsId = "radioChkAnsId_".$intQuestionId."_".$i;
							 
							 $userAns=trim($_POST[$radioChkAns]);
							 if($userAns!="")
							 {
								$chk_value_id = trim($_POST[$radioChkAnsId]);
								$ans_query =  "select correct_answer from answers_quiz where radio_ans = 1 AND id=".$chk_value_id;
								$ans = db::GetResultsAsArray($ans_query);
								$correct_answer = $ans[0]['correct_answer'];
							//	echo "c-".$correct_answer."-u-".$userAns."---".$IntTrueAnsRadio[$i];
								if($userAns == $correct_answer)
								{ 
									$IntTrueAnsRadio[$i] = $IntTrueAnsRadio[$i] + 1;
								 } 
								 else
								 {
								 	$IntTrueAnsRadio[$i] = $IntTrueAnsRadio[$i] + 0;
								 }	
							 }
						  }// for  
				       } //if s
					}//for 
				//	echo"<pre> true";print_r($IntTrueAnsRadio);
				//	echo"<pre>";print_r($stop_para_radio); exit;
					for($sr = 1 ; $sr<=count($stop_para_radio);$sr++)
					 {  
							$spi ='';
							$spi = 'r'.$sr;
							if($stop_para_radio[$spi] > $IntTrueAnsRadio[$sr])    
							{		
								echo "<script type='text/javascript'> alert('".ASSESSMENT_STOPPED."'); </script>";  
				 				global $fdate,$send_results; 
								 $fdate = util::Now();
								// $row=asgDB::UpdateUserAssessment($user_quiz_id,1,$fdate);
								$row=asgDB::UpdateUserQuiz($user_quiz_id,5,$fdate);
								 $msg = GetAssessmentResults($row);
								 DisplayMsg("warning",$msg,true); 
								 break;
							} 
					 }
					
	 		 	}//if stopQuizByChk
			
		}// end if quiz stop
		
	} // end  if page finish
 
	 if($_POST['finished'] == "Finish")
     {        
		   global $fdate,$send_results;
		 	 $fdate = util::Now();
          $row=asgDB::UpdateUserQuiz($user_quiz_id,2,$fdate);
		
          $msg = GetQuizResults($row);
			  
          DisplayMsg("warning",$msg,true);
		  
		 $pass_score=$row['pass_score'];
		 $getpoints=$row['total_point'];
		 $getpercentage=$row['total_perc'];
		 @$quizname=$asg_row['quiz_name'];
		// SendMail($row);

		$to = $_SESSION['email'];
		$subject = "Quiz Result";
		
		$message="
		<html>
		<head>
		<title>Student Result</title>
		</head>
		<body>
		<p>Dear ".$_SESSION['name']."</p>
		<table>
		<tr>
		<td>Your Result</td>
		</tr>
		<tr>
		<td>Pass score:</td><td>".$pass_score."</td>
		</tr>
		<td>Your Score:</td><td>".$getpoints."</td>
		</tr>
		<tr>
		<td><br/></td>
		</tr>
		<tr>
		<td>Thanks,</td>
		</tr>
		<tr>
		<td>Administrator</td>
		</tr>
		</table>
		</body>
		</html>
		";
		
		//Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		
		// More headers
		$headers .= 'From: <webmaster@example.com>' . "\r\n";
		@mail($to,$subject,$message,$headers);

/*  if($send_results == "1") SendMail($row);*/
	
	 	$_SESSION['ordertype'] = null;
		unset($_SESSION['ordertype']);
	 }    
		
     $db->commit();

    }
    catch(Exception $e)
    {
        echo $e->getMessage();
        $db->rollback();
    }
    $db->close_connection();
    
 }



 function GetQuizResults($row)
 {
    global $quiz_type,$user_quiz_id,$allow_review,$success;
    define("HEADERTITLE",'');
    $msg = QUIZ_FINISHED." <br>";
    if($row['show_results']=="1" &&  $quiz_type!="2")
    {
      
			$total = $row['total_ans_point'];
			if($row['results_mode']=="2") $total = $row['total_perc']." %";        
			
			$msg.=YOUR_TOTAL_POINT.": $total . ".SUCCESS_POINT.": ".$row['pass_score'].".";
			$msg.="<br>";           
			
			if($row['quiz_success']=="1")
			{
				$msg.=EXAM_SUCCESS;		
			$success = true;
			}
			else
			{
				$msg.=EXAM_UNSUCCESS;
			$success =false;
			} 
		      
    }    
    if($allow_review=="1")
    {
        $msg.="<br><br><a href='?module=view_details&user_quiz_id=".$user_quiz_id."'>".VIEW_DETAILS."</a>";
    }
    return $msg;
 }
 
  function GetAssessmentResults($row)
 {
    global $quiz_type,$user_quiz_id,$allow_review,$success;
	define("HEADERTITLE",'');
    
     $total = $row['total_ans_point'];
        if($row['results_mode']=="2") $total = $row['total_perc']." %";        
       
//	$msg .= QUIZ_FINISHED." <br>";
    if($row['show_results']=="1" &&  $quiz_type=="3")
    {
         $msg.=YOUR_TOTAL_POINT.": $total . ";
        $msg.="<br>";           
		$msg.=ASSESSMENT_UNSUCCESS." .";
	    $success =false; 
     } 
	
    return $msg;
 }

 function SendMail($row)
 {
	global $success,$user_id,$asg_row,$user_quiz_id;

//	$results = orm::Select("user_quizzes", array() , array("id"=>$user_quiz_id), "");
//	$row = db::fetch($results);
	
	$temp = "quiz_results_success";
	if(!$success) $temp = "quiz_results_not_success";
	$cmail = new cmail($temp,"");

	$subject = $cmail->subject;
	$body = $cmail->body;

	$subject = ReplaceVars($subject, $asg_row , $row);
	$body = ReplaceVars($body, $asg_row , $row);

    try
    {
	$m= new Mail; 
	$m->From(MAIL_FROM ); 
	$m->To( trim($_SESSION['email']) );
	$m->Subject( $subject);
	$m->Body( $body);    	
	$m->Priority(3) ;    
	//$m->Attach( "asd.gif","", "image/gif" ) ;

	if(MAIL_USE_SMTP=="yes")
	{
		$m->smtp_on(MAIL_SERVER, MAIL_USER_NAME, MAIL_PASSWORD ) ;    
	}
	$m->Send(); 

    }
    catch(Exception $e)
    {
    //    echo $e->getMessage();
    //    $db->rollback();
    }
	
 }

 function ReplaceVars($var,$asg_row,$row)
 {
	global $fdate;
	$var = str_replace("[quiz_name]", $asg_row['quiz_name'],$var);
	$var = str_replace("[start_date]", $asg_row['uq_added_date'],$var);
	$var = str_replace("[finish_date]", $fdate,$var);
	$var = str_replace("[pass_score]", $row['pass_score'],$var);
	$var = str_replace("[user_score]", $row['results_mode']=="1" ? $row['total_point'] : $row['total_point']." %" ,$var);
	$var = str_replace("[UserName]", $_SESSION['txtLogin'],$var);
	$var = str_replace("[Name]", $_SESSION['name'],$var);
	$var = str_replace("[Surname]", $_SESSION['surname'],$var);
	$var = str_replace("[email]", $_SESSION['email'],$var);
	$var = str_replace("[url]", WEB_SITE_URL,$var);
	return $var;
 }

 function DisplayMsg($type,$msg,$isajax)
 {
    /*  if(isset($_POST['ajax'])) 
	  $isajax=true;
     
     if($isajax==true)
     {
        if($type=="error")
        {
            echo "error:".$msg;
        }
        else if($type=="warning")
        {
             echo "warni:".$msg;
        }
        else
        {
             echo $msg;
        }
     }
     else
	 
     {
     
	 */ //echo $msg;
	 
	    global $app_display,$msg_display,$msg_text,$timer_display,$pager_display,$dvCont_display;
        $app_display="none";
        $msg_display = "";
		
        $msg_text=$msg;
        $timer_display="none";
        $pager_display="none";
		$dvCont_display="none";
    // }

    // echo $msg;

 }

 function ShowTimer($status,$row)
 {
     $ended = false;
     $start_date =$row['uq_added_date'];
     if($status=="0") $start_date = util::Now();

     $diff = abs(strtotime(util::Now()) - strtotime($start_date));

     $total_minutes = intval($diff/60);

     $minuets = intval($row['quiz_time']) - $total_minutes -1;
     $seconds = 60-($diff%60);

     if($total_minutes>=intval($row['quiz_time']))
     {
        global $user_quiz_id;
        $row_results=asgDB::UpdateUserQuiz($user_quiz_id,3,util::Now());
        $msg=TIME_ENDED." <br>";
        $msg.=GetQuizResults($row_results);        
        DisplayMsg("message",$msg,false);
        $ended=true;
     }
     else
     {      
         global $timer_script;
         $timer_script="<script language=javascript>Init_Timer($minuets,$seconds)</script>";
     }
     return $ended;

 }

 function GetPager()
 {
      global $priority,$asg_id,$page,$asg_row,$user_id;
      $res_qst=db::exec_sql(questions_db::GetQuestionsByAsgIdQuery($asg_id,$asg_row['quiz_id'], $user_id,$asg_row['qst_order']));
      if(db::num_rows($res_qst)==0) return "";
      $i=0;
      $pager_html = "";
      $finish = 0;
      while($row=db::fetch($res_qst))
      {          
                  $i++;
                  $bgcolor="white";
                  if($priority==$row['priority'])
                  {
                     $bgcolor = "silver";
                  }
                 $pager_html.= "<u><a style='cursor:pointer;background-color:$bgcolor' onmouseout='HideObject(\"tblTip\")' ".
                               " onmouseover='ShowQst(event.pageX, event.pageY ,".$row['priority'].", ".$asg_row['qst_order'].", ".$asg_row['quiz_id'].", ".$asg_row['answer_order'].")' onclick='LoadQst(\"$page\",$row[question_type_id],$row[priority],$row[id],$finish)'>".$i."</a></u>&nbsp;";
      }

      return $pager_html."&nbsp;&nbsp;";
 }

 function desc_func()
 {
        return QUIZ_SURV;
 }

?>
<script language="javascript">
function GetPagination(reqpage)
{
/*	var reqpage;
		$.ajax({
	  type: "GET",
	  url: "index.php",
	  data: { module: "start_quiz", id: "81", pageId: reqpage }
	}).done(function( msg ) {
	 	$("#result").html( msg );
	}); */
}
</script>
<script type="application/javascript">

 
</script>

