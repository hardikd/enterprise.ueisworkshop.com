<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<?php 

 $cats = $_REQUEST['cats'];
 $data = $_REQUEST['data'];
 //$forPie = $_REQUEST['forPie'];

/*echo $cats = $_REQUEST['cats']."<br />";
echo $data = $_REQUEST['data']."<br />";
echo $forPie = $_REQUEST['forPie']."<br />";die;*/
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
            },
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: [<?php echo $cats;?>]
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' fruits';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
            labels: {
                items: [{
                    html: 'Best 5 users of last exam',
                    style: {
                        left: '40px',
                        top: '8px',
                        color: 'black'
                    }
                }]
            },
            series: [{
                type: 'column',
                name: 'Student',
				color:'#248AB5',
                data: [<?php echo $data;?>]
            }, {
                type: 'spline',
                name: 'Average',
                data: [<?php echo $data;?>],
                marker: {
                	lineWidth: 2,
                	lineColor: Highcharts.getOptions().colors[2],
                	fillColor: 'white'
                }
            }]
        });
    });
    

		</script>
</head>

<body>
<script src="../js/highcharts.js"></script>
<script src="../js/modules/exporting.js"></script>
        
        <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
</body>
</html>


        
           