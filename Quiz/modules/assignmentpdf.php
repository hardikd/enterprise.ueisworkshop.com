<?php
	require "grid.php";
	require "db/asg_db.php";
	require "db/questions_db.php";
	require "qst_viewerpdf.php"; 
 $id=util::GetKeyID("user_quiz_id", "?module=assignments");
 if($_SESSION['user_type']=="2") 
 {
     $uq_res = asgDB::GetUserQuizById($id);
     if(db::num_rows($uq_res)==0) header("location:?module=old_assignments");
     
     $row = db::fetch($uq_res);
     
     if($row['user_id']!=$_SESSION['user_id'] || $row['allow_review']!="1") header("location:?module=old_assignments");
 }

 $asg_res = questions_db::GetQuestionsByUserQuizId($id);
	
 $uq  = 0;
 function get_question($row)
 {
     global $id, $uq;

     $qst_viewer = new qst_viewer("#");
     $qst_viewer->user_quiz_id=$id;
     $qst_viewer->show_prev=false;
     $qst_viewer->show_next=false;
     $qst_viewer->show_finish=false;
     $qst_viewer->SetReadOnly();
     $qst_viewer->show_correct_answers=true;
     $qst_viewer->control_unq = $uq;     
     $qst_viewer->BuildQuestionOfAnswerWithResultset($row);
     $qst_html = $qst_viewer->html;
     $uq++;
     return $qst_html;
 }
// echo"<pre>"; print_r($asg_res); 
function desc_func()
{
        return VIEW_DETAILS;
}
//$AssName = $_REQUEST['assName'];

$sqla = "select a.assignment_name from assignments a, user_quizzes uq where uq.assignment_id = a.id and uq.id =".$_REQUEST['user_quiz_id'];     $rowa = db::GetResultsAsArray($sqla);
$AssName = $rowa[0]['assignment_name'];

 $sql = "select * from students where UserID = ".$_SESSION['user_id'];
    $row1 = db::GetResultsAsArray($sql);
$StudentName = $row1[0]['firstname'].'&nbsp;&nbsp;'.$row1[0]['lastname'];
$htmlResult = '<table border="1" cellpadding="10">
<tr>
<td colspan="2" >Assignmetnt name:&nbsp;&nbsp;&nbsp;&nbsp; '.$AssName.'</td>
</tr>
<tr>
<td colspan="2">Student name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$StudentName.'</td>

</tr>
<tr><td colspan="2">';

//$i = 0 ;
set_time_limit(0);
while($row = db::fetch($asg_res))
{
//if($i<100)
//{
//echo $i;echo"<pre>"; print_r($row);   

 $htmlResult .= get_question($row);
// }
 //$i++;
}

$htmlResult .= '</td></tr></table>';

//echo $htmlResult;
//exit;	
	include("../tcpdf/tcpdf.php"); 
	define ('PDF_MARGIN_TOP', 22); 
    define ('PDF_HEADER_TITLE', 'Assignment Answer');
    define ('PDF_HEADER_STRING','Assignment Answer');
    define ('PDF_CREATOR', 'Assignment Answer'); 

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor(PDF_CREATOR);
    $pdf->SetTitle('Assignment Answer');

    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings
    $pdf->setLanguageArray($l);

    // ---------------------------------------------------------

    // set font
    $pdf->SetFont('helvetica', '', 10);

    // add a page
    $pdf->AddPage();

    $matter =$htmlResult;

    $pdf->writeHTML($matter, true, false, true, false, '');

    // reset pointer to the last page
    $pdf->lastPage();
    //ob_flush();
	
    //Close and output PDF document
    $pdf->Output('example_061.pdf', 'I');
?>