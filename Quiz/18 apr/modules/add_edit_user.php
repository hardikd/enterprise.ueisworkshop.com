<?php if(!isset($RUN)) { exit(); } ?>
<?php

access::allow("1");

    $val = new validations("btnSave");
    $val->AddValidator("txtLogin", "empty", LOGIN_VAL,"");
    $val->AddValidator("txtPassword", "empty", PASSWORD_VAL,"");
    $val->AddValidator("drpUserType", "notequal", USER_TYPE_VAL , "-1");
    $val->AddValidator("drpCountries", "notequal", COUNTRY_VAL , "-1");

    $selected="-1";
    $selected_country = DEFAULT_COUNTRY;
    $pswlbl_display="none";
    $psw_display="";
    $login_disabled="";
    $mode="add";
    $chkApproved = "checked";
    
    $user_photo_file = "";
    $photo_thumb = "";
    
    if(isset($_GET["id"]))
    {
        $pswlbl_display="";
        $psw_display="none";
        $login_disabled="read-only";
        $mode="edit";
        $id = util::GetID("?module=local_users");
        $rs_users=orm::Select("users", array(), array("UserID"=>$id), "");

        if(db::num_rows($rs_users) == 0 ) header("location:?module=local_users");

        $row_users=db::fetch($rs_users);
        $txtName = $row_users["Name"];
        $txtSurname = $row_users["Surname"];
        $txtEmail = $row_users["email"];
        $txtLogin = $row_users["UserName"];
        $selected = $row_users["user_type"];
        $selected_country = $row_users["country_id"];
        
        $user_photo_file=trim($row_users["user_photo"]);
        
        if($user_photo_file!="")
        $photo_thumb=util::get_img($user_photo_file,true);

        $txtPasswordValue="********";

	$txtAddr = $row_users["address"];
	$txtPhone = $row_users["phone"];
        $chkApproved = $row_users["approved"] == "1" ? "checked" : "";
	$chkDisabled = $row_users["disabled"] == "1" ? "checked" : "";
    }
    
    $results = orm::Select("user_types", array(), array() , "id");
    $countries_res = orm::Select("countries_quiz", array(), array(), "country_name");
    //$user_type_options = webcontrols::GetOptions($results, "id", "type_name",$selected);
    $user_type_options = webcontrols::BuildOptions($USER_TYPE, $selected);
    $country_options = webcontrols::GetOptions($countries_res, "id", "country_name", $selected_country);
    

    if(isset($_POST["btnSave"]) && $val->IsValid())
    {
        if(!isset($_GET["id"]))
        {
            add_file();
            orm::Insert("users", array("Name"=>trim($_POST["txtName"]),
                                    "Surname"=>trim($_POST["txtSurname"]),
                                    "UserName"=>trim($_POST["txtLogin"]),
                                     "Password"=>md5(trim($_POST["txtPassword"])),
                                     "added_date"=>util::Now(),
                                     "email"=>trim($_POST["txtEmail"]),
                                     "address"=>trim($_POST["txtAddr"]),
                                     "phone"=>trim($_POST["txtPhone"]),
  				     "approved"=>isset($_POST["chkApproved"]) ? 1:0,
				     "disabled"=>isset($_POST["chkDisabled"]) ? 1:0,
                                     "user_type"=>trim($_POST["drpUserType"]),
                                     "country_id"=>trim($_POST["drpCountries"]),
                                    "user_photo"=>$filename
                                   ));
        }
        else
        {
            add_file();
            $arr_columns=array("Name"=>trim($_POST["txtName"]),
                                    "Surname"=>trim($_POST["txtSurname"]),
                                     "email"=>trim($_POST["txtEmail"]),
				     "address"=>trim($_POST["txtAddr"]),
                                     "phone"=>trim($_POST["txtPhone"]),
  				     "approved"=>isset($_POST["chkApproved"]) ? 1:0,
				     "disabled"=>isset($_POST["chkDisabled"]) ? 1:0,
                                     "user_type"=>trim($_POST["drpUserType"]),
                                     "country_id"=>trim($_POST["drpCountries"])
                                   );
            
            if(trim($filename)!="")  
            {
                $arr_columns["user_photo"]=$filename;
                if(trim($user_photo_file)!="")
                {
                        @unlink("user_photos".DIRECTORY_SEPARATOR.$filename);
                        @unlink("user_photos".DIRECTORY_SEPARATOR.$thumb);
                }
            }
            
            if(isset($_POST["chkEdit"]))
            {
                $arr_columns["Password"]=md5(trim($_POST["txtPassword"]));
            }
            orm::Update("users", $arr_columns, array("UserID"=>$id));
        }

        header("location:?module=local_users");
    }


    if(isset($_POST["ajax"]))
    {
         $results = orm::Select("users", array(), array("UserName"=>$_POST["login_to_check"]) , "");
         $count = db::num_rows($results);
         echo $count;
    }

    function desc_func()
    {
        return ADD_EDIT_USER;
    }
    
    $filename = "";
    $thumb = "";
    
    function add_file()
    {    
        global $filename,$thumb;  
        if($_FILES['userphoto']['size']>0)
        {
                $filename=basename( $_FILES['userphoto']['name']);
                $arr = explode(".", $filename);
                $ext = end($arr);                
                $filename=md5(util::GUID()).".".$ext;
                $target_path = "user_photos/";
                $target_path = $target_path . $filename;

                move_uploaded_file($_FILES['userphoto']['tmp_name'], $target_path);     
               
                util::createThumbnail($target_path,90);
                $thumb=".thumb.".$ext;
        }

    }

?>
