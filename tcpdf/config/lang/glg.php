<?php
//============================================================+
// File name   : glg.php
// Begin       : 2010-10-26
// Last Update : 2010-10-26
//
// Description : Language module for TCPDF
//               (contains translated texts)
//               Galician
//
// Author: Workshop
//
// (c) Copyright:
//               Workshop
//               Tecnick.com LTD
//               Manor Coach House, Church Hill
//               Aldershot, Hants, GU12 4RQ
//               UK
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * TCPDF language file (contains translated texts).
 * @package com.tecnick.tcpdf
 * @brief TCPDF language file: Galician
 * @author Workshop
 * @since 2010-10-26
 */

// Galician

global $l;
$l = Array();

// PAGE META DESCRIPTORS --------------------------------------

$l['a_meta_charset'] = 'UTF-8';
$l['a_meta_dir'] = 'ltr';
$l['a_meta_language'] = 'gl';

// TRANSLATIONS --------------------------------------
$l['w_page'] = 'Páxina';

//============================================================+
// END OF FILE
//============================================================+
