<?php
//============================================================+
// File name   : nob.php
// Begin       : 2004-03-03
// Last Update : 2010-10-26
//
// Description : Language module for TCPDF
//               (contains translated texts)
//               Norwegian Bokmål
//
// Author: Workshop
//
// (c) Copyright:
//               Workshop
//               Tecnick.com LTD
//               Manor Coach House, Church Hill
//               Aldershot, Hants, GU12 4RQ
//               UK
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * TCPDF language file (contains translated texts).
 * @package com.tecnick.tcpdf
 * @brief TCPDF language file: Norwegian Bokmål
 * @author Workshop
 * @since 2004-03-03
 */

// Norwegian Bokmål

global $l;
$l = Array();

// PAGE META DESCRIPTORS --------------------------------------

$l['a_meta_charset'] = 'UTF-8';
$l['a_meta_dir'] = 'ltr';
$l['a_meta_language'] = 'nb';

// TRANSLATIONS --------------------------------------
$l['w_page'] = 'side';

//============================================================+
// END OF FILE
//============================================================+
